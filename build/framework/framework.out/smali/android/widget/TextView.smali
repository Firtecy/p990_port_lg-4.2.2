.class public Landroid/widget/TextView;
.super Landroid/view/View;
.source "TextView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/TextView$5;,
        Landroid/widget/TextView$ChangeWatcher;,
        Landroid/widget/TextView$Marquee;,
        Landroid/widget/TextView$CharWrapper;,
        Landroid/widget/TextView$SavedState;,
        Landroid/widget/TextView$BufferType;,
        Landroid/widget/TextView$OnEditorActionListener;,
        Landroid/widget/TextView$Drawables;
    }
.end annotation


# static fields
.field private static final ANIMATED_SCROLL_GAP:I = 0xfa

.field private static final CHANGE_WATCHER_PRIORITY:I = 0x64

.field static final DEBUG_EXTRACT:Z = false

.field private static final DECIMAL:I = 0x4

.field private static final EMPTY_SPANNED:Landroid/text/Spanned; = null

.field private static final EMS:I = 0x1

.field static final ID_COPY:I = 0x1020021

.field static final ID_CUT:I = 0x1020020

.field static final ID_PASTE:I = 0x1020022

.field static final ID_SELECT_ALL:I = 0x102001f

.field static final ID_TEXTLINK:I = 0x20d003e

.field public static final IME_OPTION_DISABLE_BUBBLE_CUT:Ljava/lang/String; = "DISABLE_BUBBLE_POPUP_CUT"

.field public static final IME_OPTION_DISABLE_BUBBLE_PASTE:Ljava/lang/String; = "DISABLE_BUBBLE_POPUP_PASTE"

.field public static final IME_OPTION_DISABLE_BUBBLE_POPUP:Ljava/lang/String; = "DISABLE_BUBBLE_POPUP"

.field static LAST_CUT_OR_COPY_TIME:J = 0x0L

.field private static final LINES:I = 0x1

.field static final LOG_TAG:Ljava/lang/String; = "TextView"

.field private static final MARQUEE_FADE_NORMAL:I = 0x0

.field private static final MARQUEE_FADE_SWITCH_SHOW_ELLIPSIS:I = 0x1

.field private static final MARQUEE_FADE_SWITCH_SHOW_FADE:I = 0x2

.field private static final MONOSPACE:I = 0x3

.field private static final MULTILINE_STATE_SET:[I = null

.field private static final NO_FILTERS:[Landroid/text/InputFilter; = null

.field private static final PIXELS:I = 0x2

.field private static final SANS:I = 0x1

.field private static final SERIF:I = 0x2

.field private static final SIGNED:I = 0x2

.field private static final TEMP_RECTF:Landroid/graphics/RectF; = null

.field private static final UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics; = null

.field private static final VERY_WIDE:I = 0x100000

.field private static final VERY_WIDE_SKIA:I = 0x10000


# instance fields
.field private mAllowTransformationLengthChange:Z

.field private mAutoLinkMask:I

.field private mBoring:Landroid/text/BoringLayout$Metrics;

.field private mBufferType:Landroid/widget/TextView$BufferType;

.field public mCanCreateBubblePopup:Z

.field public mCanCutBubblePopup:Z

.field public mCanPasteBubblePopup:Z

.field private mChangeWatcher:Landroid/widget/TextView$ChangeWatcher;

.field private mCharWrapper:Landroid/widget/TextView$CharWrapper;

.field mCurConfig:Landroid/content/res/Configuration;

.field private mCurHintTextColor:I

.field private mCurTextColor:I

.field private volatile mCurrentTextServicesLocaleCache:Ljava/util/Locale;

.field private final mCurrentTextServicesLocaleLock:Ljava/util/concurrent/locks/ReentrantLock;

.field mCursorDrawableRes:I

.field private mDeferScroll:I

.field private mDesiredHeightAtMeasure:I

.field private mDiscardNextActionUp:Z

.field private mDispatchTemporaryDetach:Z

.field mDrawables:Landroid/widget/TextView$Drawables;

.field private mEditableFactory:Landroid/text/Editable$Factory;

.field private mEditor:Landroid/widget/Editor;

.field private mEllipsize:Landroid/text/TextUtils$TruncateAt;

.field private mFilters:[Landroid/text/InputFilter;

.field mFontFamily:Ljava/lang/String;

.field private mFreezesText:Z

.field private mGravity:I

.field mHighlightColor:I

.field private final mHighlightPaint:Landroid/graphics/Paint;

.field private mHighlightPath:Landroid/graphics/Path;

.field private mHighlightPathBogus:Z

.field private mHint:Ljava/lang/CharSequence;

.field private mHintBoring:Landroid/text/BoringLayout$Metrics;

.field private mHintLayout:Landroid/text/Layout;

.field private mHintTextColor:Landroid/content/res/ColorStateList;

.field private mHintTextDir:Landroid/text/TextDirectionHeuristic;

.field private mHorizontallyScrolling:Z

.field private mIncludePad:Z

.field private mIsShortLtrText:Z

.field private mLastLayoutDirection:I

.field private mLastScroll:J

.field private mLayout:Landroid/text/Layout;

.field private mLayoutAlignment:Landroid/text/Layout$Alignment;

.field private mLinkTextColor:Landroid/content/res/ColorStateList;

.field private mLinksClickable:Z

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/TextWatcher;",
            ">;"
        }
    .end annotation
.end field

.field private mMarquee:Landroid/widget/TextView$Marquee;

.field private mMarqueeAlwaysEnable:Z

.field private mMarqueeFadeMode:I

.field private mMarqueeRepeatLimit:I

.field private mMaxMode:I

.field private mMaxWidth:I

.field private mMaxWidthMode:I

.field private mMaximum:I

.field private mMinMode:I

.field private mMinWidth:I

.field private mMinWidthMode:I

.field private mMinimum:I

.field private mMovement:Landroid/text/method/MovementMethod;

.field private mNoEmojiEditMode:Z

.field private mOldMaxMode:I

.field private mOldMaximum:I

.field public mPasteHandler:Landroid/os/Handler;

.field private mPreDrawRegistered:Z

.field private mResolvedTextAlignment:I

.field private mRestartMarquee:Z

.field private mSavedHintLayout:Landroid/text/BoringLayout;

.field private mSavedLayout:Landroid/text/BoringLayout;

.field private mSavedMarqueeModeLayout:Landroid/text/Layout;

.field private mScroller:Landroid/widget/Scroller;

.field private mShadowDx:F

.field private mShadowDy:F

.field private mShadowRadius:F

.field private mSingleLine:Z

.field private mSpacingAdd:F

.field private mSpacingMult:F

.field private mSpannableFactory:Landroid/text/Spannable$Factory;

.field mStyleIndex:I

.field private mTempRect:Landroid/graphics/Rect;

.field private mTemporaryDetach:Z

.field private mText:Ljava/lang/CharSequence;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
    .end annotation
.end field

.field private mTextColor:Landroid/content/res/ColorStateList;

.field private mTextDir:Landroid/text/TextDirectionHeuristic;

.field mTextEditSuggestionItemLayout:I

.field private final mTextPaint:Landroid/text/TextPaint;

.field mTextSelectHandleLeftRes:I

.field mTextSelectHandleRes:I

.field mTextSelectHandleRightRes:I

.field private mTransformation:Landroid/text/method/TransformationMethod;

.field private mTransformed:Ljava/lang/CharSequence;

.field mTypefaceIndex:I

.field private mUserSetTextScaleX:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 284
    new-instance v1, Landroid/graphics/RectF;

    #@4
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    #@7
    sput-object v1, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@9
    .line 291
    new-array v1, v3, [Landroid/text/InputFilter;

    #@b
    sput-object v1, Landroid/widget/TextView;->NO_FILTERS:[Landroid/text/InputFilter;

    #@d
    .line 292
    new-instance v1, Landroid/text/SpannedString;

    #@f
    const-string v2, ""

    #@11
    invoke-direct {v1, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    #@14
    sput-object v1, Landroid/widget/TextView;->EMPTY_SPANNED:Landroid/text/Spanned;

    #@16
    .line 297
    new-array v1, v4, [I

    #@18
    const v2, 0x101034d

    #@1b
    aput v2, v1, v3

    #@1d
    sput-object v1, Landroid/widget/TextView;->MULTILINE_STATE_SET:[I

    #@1f
    .line 622
    new-instance v0, Landroid/graphics/Paint;

    #@21
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@24
    .line 623
    .local v0, p:Landroid/graphics/Paint;
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@27
    .line 625
    const-string v1, "H"

    #@29
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    #@2c
    .line 6408
    new-instance v1, Landroid/text/BoringLayout$Metrics;

    #@2e
    invoke-direct {v1}, Landroid/text/BoringLayout$Metrics;-><init>()V

    #@31
    sput-object v1, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 649
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 650
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 653
    const v0, 0x1010084

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 654
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 69
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 658
    invoke-direct/range {p0 .. p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 249
    const/16 v62, 0x1

    #@5
    move/from16 v0, v62

    #@7
    move-object/from16 v1, p0

    #@9
    iput-boolean v0, v1, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@b
    .line 250
    const/16 v62, 0x1

    #@d
    move/from16 v0, v62

    #@f
    move-object/from16 v1, p0

    #@11
    iput-boolean v0, v1, Landroid/widget/TextView;->mCanPasteBubblePopup:Z

    #@13
    .line 251
    const/16 v62, 0x1

    #@15
    move/from16 v0, v62

    #@17
    move-object/from16 v1, p0

    #@19
    iput-boolean v0, v1, Landroid/widget/TextView;->mCanCutBubblePopup:Z

    #@1b
    .line 312
    const/16 v62, 0x0

    #@1d
    move/from16 v0, v62

    #@1f
    move-object/from16 v1, p0

    #@21
    iput-boolean v0, v1, Landroid/widget/TextView;->mMarqueeAlwaysEnable:Z

    #@23
    .line 315
    const/16 v62, 0x0

    #@25
    move/from16 v0, v62

    #@27
    move-object/from16 v1, p0

    #@29
    iput-boolean v0, v1, Landroid/widget/TextView;->mDiscardNextActionUp:Z

    #@2b
    .line 316
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    #@2e
    move-result-object v62

    #@2f
    move-object/from16 v0, v62

    #@31
    move-object/from16 v1, p0

    #@33
    iput-object v0, v1, Landroid/widget/TextView;->mEditableFactory:Landroid/text/Editable$Factory;

    #@35
    .line 317
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    #@38
    move-result-object v62

    #@39
    move-object/from16 v0, v62

    #@3b
    move-object/from16 v1, p0

    #@3d
    iput-object v0, v1, Landroid/widget/TextView;->mSpannableFactory:Landroid/text/Spannable$Factory;

    #@3f
    .line 324
    const/16 v62, 0x0

    #@41
    move/from16 v0, v62

    #@43
    move-object/from16 v1, p0

    #@45
    iput-boolean v0, v1, Landroid/widget/TextView;->mIsShortLtrText:Z

    #@47
    .line 492
    const/16 v62, 0x3

    #@49
    move/from16 v0, v62

    #@4b
    move-object/from16 v1, p0

    #@4d
    iput v0, v1, Landroid/widget/TextView;->mMarqueeRepeatLimit:I

    #@4f
    .line 498
    const/16 v62, -0x1

    #@51
    move/from16 v0, v62

    #@53
    move-object/from16 v1, p0

    #@55
    iput v0, v1, Landroid/widget/TextView;->mLastLayoutDirection:I

    #@57
    .line 505
    const/16 v62, 0x0

    #@59
    move/from16 v0, v62

    #@5b
    move-object/from16 v1, p0

    #@5d
    iput v0, v1, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@5f
    .line 516
    sget-object v62, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    #@61
    move-object/from16 v0, v62

    #@63
    move-object/from16 v1, p0

    #@65
    iput-object v0, v1, Landroid/widget/TextView;->mBufferType:Landroid/widget/TextView$BufferType;

    #@67
    .line 534
    const v62, 0x800033

    #@6a
    move/from16 v0, v62

    #@6c
    move-object/from16 v1, p0

    #@6e
    iput v0, v1, Landroid/widget/TextView;->mGravity:I

    #@70
    .line 538
    const/16 v62, 0x1

    #@72
    move/from16 v0, v62

    #@74
    move-object/from16 v1, p0

    #@76
    iput-boolean v0, v1, Landroid/widget/TextView;->mLinksClickable:Z

    #@78
    .line 540
    const/high16 v62, 0x3f80

    #@7a
    move/from16 v0, v62

    #@7c
    move-object/from16 v1, p0

    #@7e
    iput v0, v1, Landroid/widget/TextView;->mSpacingMult:F

    #@80
    .line 541
    const/16 v62, 0x0

    #@82
    move/from16 v0, v62

    #@84
    move-object/from16 v1, p0

    #@86
    iput v0, v1, Landroid/widget/TextView;->mSpacingAdd:F

    #@88
    .line 543
    const v62, 0x7fffffff

    #@8b
    move/from16 v0, v62

    #@8d
    move-object/from16 v1, p0

    #@8f
    iput v0, v1, Landroid/widget/TextView;->mMaximum:I

    #@91
    .line 544
    const/16 v62, 0x1

    #@93
    move/from16 v0, v62

    #@95
    move-object/from16 v1, p0

    #@97
    iput v0, v1, Landroid/widget/TextView;->mMaxMode:I

    #@99
    .line 545
    const/16 v62, 0x0

    #@9b
    move/from16 v0, v62

    #@9d
    move-object/from16 v1, p0

    #@9f
    iput v0, v1, Landroid/widget/TextView;->mMinimum:I

    #@a1
    .line 546
    const/16 v62, 0x1

    #@a3
    move/from16 v0, v62

    #@a5
    move-object/from16 v1, p0

    #@a7
    iput v0, v1, Landroid/widget/TextView;->mMinMode:I

    #@a9
    .line 548
    move-object/from16 v0, p0

    #@ab
    iget v0, v0, Landroid/widget/TextView;->mMaximum:I

    #@ad
    move/from16 v62, v0

    #@af
    move/from16 v0, v62

    #@b1
    move-object/from16 v1, p0

    #@b3
    iput v0, v1, Landroid/widget/TextView;->mOldMaximum:I

    #@b5
    .line 549
    move-object/from16 v0, p0

    #@b7
    iget v0, v0, Landroid/widget/TextView;->mMaxMode:I

    #@b9
    move/from16 v62, v0

    #@bb
    move/from16 v0, v62

    #@bd
    move-object/from16 v1, p0

    #@bf
    iput v0, v1, Landroid/widget/TextView;->mOldMaxMode:I

    #@c1
    .line 551
    const v62, 0x7fffffff

    #@c4
    move/from16 v0, v62

    #@c6
    move-object/from16 v1, p0

    #@c8
    iput v0, v1, Landroid/widget/TextView;->mMaxWidth:I

    #@ca
    .line 552
    const/16 v62, 0x2

    #@cc
    move/from16 v0, v62

    #@ce
    move-object/from16 v1, p0

    #@d0
    iput v0, v1, Landroid/widget/TextView;->mMaxWidthMode:I

    #@d2
    .line 553
    const/16 v62, 0x0

    #@d4
    move/from16 v0, v62

    #@d6
    move-object/from16 v1, p0

    #@d8
    iput v0, v1, Landroid/widget/TextView;->mMinWidth:I

    #@da
    .line 554
    const/16 v62, 0x2

    #@dc
    move/from16 v0, v62

    #@de
    move-object/from16 v1, p0

    #@e0
    iput v0, v1, Landroid/widget/TextView;->mMinWidthMode:I

    #@e2
    .line 557
    const/16 v62, -0x1

    #@e4
    move/from16 v0, v62

    #@e6
    move-object/from16 v1, p0

    #@e8
    iput v0, v1, Landroid/widget/TextView;->mDesiredHeightAtMeasure:I

    #@ea
    .line 558
    const/16 v62, 0x1

    #@ec
    move/from16 v0, v62

    #@ee
    move-object/from16 v1, p0

    #@f0
    iput-boolean v0, v1, Landroid/widget/TextView;->mIncludePad:Z

    #@f2
    .line 559
    const/16 v62, -0x1

    #@f4
    move/from16 v0, v62

    #@f6
    move-object/from16 v1, p0

    #@f8
    iput v0, v1, Landroid/widget/TextView;->mDeferScroll:I

    #@fa
    .line 574
    sget-object v62, Landroid/widget/TextView;->NO_FILTERS:[Landroid/text/InputFilter;

    #@fc
    move-object/from16 v0, v62

    #@fe
    move-object/from16 v1, p0

    #@100
    iput-object v0, v1, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@102
    .line 577
    new-instance v62, Ljava/util/concurrent/locks/ReentrantLock;

    #@104
    invoke-direct/range {v62 .. v62}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@107
    move-object/from16 v0, v62

    #@109
    move-object/from16 v1, p0

    #@10b
    iput-object v0, v1, Landroid/widget/TextView;->mCurrentTextServicesLocaleLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@10d
    .line 581
    const v62, 0x6633b5e5

    #@110
    move/from16 v0, v62

    #@112
    move-object/from16 v1, p0

    #@114
    iput v0, v1, Landroid/widget/TextView;->mHighlightColor:I

    #@116
    .line 584
    const/16 v62, 0x1

    #@118
    move/from16 v0, v62

    #@11a
    move-object/from16 v1, p0

    #@11c
    iput-boolean v0, v1, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@11e
    .line 601
    const/16 v62, 0x0

    #@120
    move-object/from16 v0, v62

    #@122
    move-object/from16 v1, p0

    #@124
    iput-object v0, v1, Landroid/widget/TextView;->mFontFamily:Ljava/lang/String;

    #@126
    .line 602
    const/16 v62, -0x1

    #@128
    move/from16 v0, v62

    #@12a
    move-object/from16 v1, p0

    #@12c
    iput v0, v1, Landroid/widget/TextView;->mTypefaceIndex:I

    #@12e
    .line 603
    const/16 v62, -0x1

    #@130
    move/from16 v0, v62

    #@132
    move-object/from16 v1, p0

    #@134
    iput v0, v1, Landroid/widget/TextView;->mStyleIndex:I

    #@136
    .line 604
    new-instance v62, Landroid/content/res/Configuration;

    #@138
    invoke-direct/range {v62 .. v62}, Landroid/content/res/Configuration;-><init>()V

    #@13b
    move-object/from16 v0, v62

    #@13d
    move-object/from16 v1, p0

    #@13f
    iput-object v0, v1, Landroid/widget/TextView;->mCurConfig:Landroid/content/res/Configuration;

    #@141
    .line 608
    const/16 v62, 0x0

    #@143
    move-object/from16 v0, v62

    #@145
    move-object/from16 v1, p0

    #@147
    iput-object v0, v1, Landroid/widget/TextView;->mPasteHandler:Landroid/os/Handler;

    #@149
    .line 9485
    const/16 v62, 0x0

    #@14b
    move/from16 v0, v62

    #@14d
    move-object/from16 v1, p0

    #@14f
    iput-boolean v0, v1, Landroid/widget/TextView;->mNoEmojiEditMode:Z

    #@151
    .line 659
    const-string v62, ""

    #@153
    move-object/from16 v0, v62

    #@155
    move-object/from16 v1, p0

    #@157
    iput-object v0, v1, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@159
    .line 661
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@15c
    move-result-object v47

    #@15d
    .line 662
    .local v47, res:Landroid/content/res/Resources;
    invoke-virtual/range {v47 .. v47}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@160
    move-result-object v17

    #@161
    .line 664
    .local v17, compat:Landroid/content/res/CompatibilityInfo;
    new-instance v62, Landroid/text/TextPaint;

    #@163
    const/16 v63, 0x1

    #@165
    invoke-direct/range {v62 .. v63}, Landroid/text/TextPaint;-><init>(I)V

    #@168
    move-object/from16 v0, v62

    #@16a
    move-object/from16 v1, p0

    #@16c
    iput-object v0, v1, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@16e
    .line 665
    move-object/from16 v0, p0

    #@170
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@172
    move-object/from16 v62, v0

    #@174
    invoke-virtual/range {v47 .. v47}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@177
    move-result-object v63

    #@178
    move-object/from16 v0, v63

    #@17a
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    #@17c
    move/from16 v63, v0

    #@17e
    move/from16 v0, v63

    #@180
    move-object/from16 v1, v62

    #@182
    iput v0, v1, Landroid/text/TextPaint;->density:F

    #@184
    .line 666
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@188
    move-object/from16 v62, v0

    #@18a
    move-object/from16 v0, v17

    #@18c
    iget v0, v0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@18e
    move/from16 v63, v0

    #@190
    invoke-virtual/range {v62 .. v63}, Landroid/text/TextPaint;->setCompatibilityScaling(F)V

    #@193
    .line 668
    new-instance v62, Landroid/graphics/Paint;

    #@195
    const/16 v63, 0x1

    #@197
    invoke-direct/range {v62 .. v63}, Landroid/graphics/Paint;-><init>(I)V

    #@19a
    move-object/from16 v0, v62

    #@19c
    move-object/from16 v1, p0

    #@19e
    iput-object v0, v1, Landroid/widget/TextView;->mHighlightPaint:Landroid/graphics/Paint;

    #@1a0
    .line 669
    move-object/from16 v0, p0

    #@1a2
    iget-object v0, v0, Landroid/widget/TextView;->mHighlightPaint:Landroid/graphics/Paint;

    #@1a4
    move-object/from16 v62, v0

    #@1a6
    move-object/from16 v0, v17

    #@1a8
    iget v0, v0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@1aa
    move/from16 v63, v0

    #@1ac
    invoke-virtual/range {v62 .. v63}, Landroid/graphics/Paint;->setCompatibilityScaling(F)V

    #@1af
    .line 671
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getDefaultMovementMethod()Landroid/text/method/MovementMethod;

    #@1b2
    move-result-object v62

    #@1b3
    move-object/from16 v0, v62

    #@1b5
    move-object/from16 v1, p0

    #@1b7
    iput-object v0, v1, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@1b9
    .line 673
    const/16 v62, 0x0

    #@1bb
    move-object/from16 v0, v62

    #@1bd
    move-object/from16 v1, p0

    #@1bf
    iput-object v0, v1, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@1c1
    .line 675
    const/16 v54, 0x0

    #@1c3
    .line 676
    .local v54, textColorHighlight:I
    const/16 v53, 0x0

    #@1c5
    .line 677
    .local v53, textColor:Landroid/content/res/ColorStateList;
    const/16 v55, 0x0

    #@1c7
    .line 678
    .local v55, textColorHint:Landroid/content/res/ColorStateList;
    const/16 v56, 0x0

    #@1c9
    .line 679
    .local v56, textColorLink:Landroid/content/res/ColorStateList;
    const/16 v57, 0xf

    #@1cb
    .line 680
    .local v57, textSize:I
    const/16 v33, 0x0

    #@1cd
    .line 681
    .local v33, fontFamily:Ljava/lang/String;
    const/16 v59, -0x1

    #@1cf
    .line 682
    .local v59, typefaceIndex:I
    const/16 v51, -0x1

    #@1d1
    .line 683
    .local v51, styleIndex:I
    const/4 v6, 0x0

    #@1d2
    .line 685
    .local v6, allCaps:Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@1d5
    move-result-object v58

    #@1d6
    .line 693
    .local v58, theme:Landroid/content/res/Resources$Theme;
    sget-object v62, Lcom/android/internal/R$styleable;->TextViewAppearance:[I

    #@1d8
    const/16 v63, 0x0

    #@1da
    move-object/from16 v0, v58

    #@1dc
    move-object/from16 v1, p2

    #@1de
    move-object/from16 v2, v62

    #@1e0
    move/from16 v3, p3

    #@1e2
    move/from16 v4, v63

    #@1e4
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1e7
    move-result-object v5

    #@1e8
    .line 695
    .local v5, a:Landroid/content/res/TypedArray;
    const/4 v8, 0x0

    #@1e9
    .line 696
    .local v8, appearance:Landroid/content/res/TypedArray;
    const/16 v62, 0x0

    #@1eb
    const/16 v63, -0x1

    #@1ed
    move/from16 v0, v62

    #@1ef
    move/from16 v1, v63

    #@1f1
    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1f4
    move-result v7

    #@1f5
    .line 698
    .local v7, ap:I
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    #@1f8
    .line 699
    const/16 v62, -0x1

    #@1fa
    move/from16 v0, v62

    #@1fc
    if-eq v7, v0, :cond_208

    #@1fe
    .line 700
    sget-object v62, Lcom/android/internal/R$styleable;->TextAppearance:[I

    #@200
    move-object/from16 v0, v58

    #@202
    move-object/from16 v1, v62

    #@204
    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@207
    move-result-object v8

    #@208
    .line 703
    :cond_208
    if-eqz v8, :cond_262

    #@20a
    .line 704
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@20d
    move-result v40

    #@20e
    .line 705
    .local v40, n:I
    const/16 v35, 0x0

    #@210
    .local v35, i:I
    :goto_210
    move/from16 v0, v35

    #@212
    move/from16 v1, v40

    #@214
    if-ge v0, v1, :cond_25f

    #@216
    .line 706
    move/from16 v0, v35

    #@218
    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@21b
    move-result v9

    #@21c
    .line 708
    .local v9, attr:I
    packed-switch v9, :pswitch_data_b06

    #@21f
    .line 705
    :goto_21f
    add-int/lit8 v35, v35, 0x1

    #@221
    goto :goto_210

    #@222
    .line 710
    :pswitch_222
    move/from16 v0, v54

    #@224
    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    #@227
    move-result v54

    #@228
    .line 711
    goto :goto_21f

    #@229
    .line 714
    :pswitch_229
    invoke-virtual {v8, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@22c
    move-result-object v53

    #@22d
    .line 715
    goto :goto_21f

    #@22e
    .line 718
    :pswitch_22e
    invoke-virtual {v8, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@231
    move-result-object v55

    #@232
    .line 719
    goto :goto_21f

    #@233
    .line 722
    :pswitch_233
    invoke-virtual {v8, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@236
    move-result-object v56

    #@237
    .line 723
    goto :goto_21f

    #@238
    .line 726
    :pswitch_238
    move/from16 v0, v57

    #@23a
    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@23d
    move-result v57

    #@23e
    .line 727
    goto :goto_21f

    #@23f
    .line 730
    :pswitch_23f
    const/16 v62, -0x1

    #@241
    move/from16 v0, v62

    #@243
    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@246
    move-result v59

    #@247
    .line 731
    goto :goto_21f

    #@248
    .line 734
    :pswitch_248
    invoke-virtual {v8, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@24b
    move-result-object v33

    #@24c
    .line 735
    goto :goto_21f

    #@24d
    .line 738
    :pswitch_24d
    const/16 v62, -0x1

    #@24f
    move/from16 v0, v62

    #@251
    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@254
    move-result v51

    #@255
    .line 739
    goto :goto_21f

    #@256
    .line 742
    :pswitch_256
    const/16 v62, 0x0

    #@258
    move/from16 v0, v62

    #@25a
    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@25d
    move-result v6

    #@25e
    goto :goto_21f

    #@25f
    .line 747
    .end local v9           #attr:I
    :cond_25f
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@262
    .line 750
    .end local v35           #i:I
    .end local v40           #n:I
    :cond_262
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getDefaultEditable()Z

    #@265
    move-result v29

    #@266
    .line 751
    .local v29, editable:Z
    const/16 v36, 0x0

    #@268
    .line 752
    .local v36, inputMethod:Ljava/lang/CharSequence;
    const/16 v42, 0x0

    #@26a
    .line 753
    .local v42, numeric:I
    const/16 v18, 0x0

    #@26c
    .line 754
    .local v18, digits:Ljava/lang/CharSequence;
    const/16 v45, 0x0

    #@26e
    .line 755
    .local v45, phone:Z
    const/4 v11, 0x0

    #@26f
    .line 756
    .local v11, autotext:Z
    const/4 v10, -0x1

    #@270
    .line 757
    .local v10, autocap:I
    const/4 v13, 0x0

    #@271
    .line 758
    .local v13, buffertype:I
    const/16 v48, 0x0

    #@273
    .line 759
    .local v48, selectallonfocus:Z
    const/16 v21, 0x0

    #@275
    .local v21, drawableLeft:Landroid/graphics/drawable/Drawable;
    const/16 v25, 0x0

    #@277
    .local v25, drawableTop:Landroid/graphics/drawable/Drawable;
    const/16 v23, 0x0

    #@279
    .line 760
    .local v23, drawableRight:Landroid/graphics/drawable/Drawable;
    const/16 v19, 0x0

    #@27b
    .local v19, drawableBottom:Landroid/graphics/drawable/Drawable;
    const/16 v24, 0x0

    #@27d
    .local v24, drawableStart:Landroid/graphics/drawable/Drawable;
    const/16 v20, 0x0

    #@27f
    .line 761
    .local v20, drawableEnd:Landroid/graphics/drawable/Drawable;
    const/16 v22, 0x0

    #@281
    .line 762
    .local v22, drawablePadding:I
    const/16 v30, -0x1

    #@283
    .line 763
    .local v30, ellipsize:I
    const/16 v50, 0x0

    #@285
    .line 764
    .local v50, singleLine:Z
    const/16 v39, -0x1

    #@287
    .line 765
    .local v39, maxlength:I
    const-string v52, ""

    #@289
    .line 766
    .local v52, text:Ljava/lang/CharSequence;
    const/16 v34, 0x0

    #@28b
    .line 767
    .local v34, hint:Ljava/lang/CharSequence;
    const/16 v49, 0x0

    #@28d
    .line 768
    .local v49, shadowcolor:I
    const/16 v26, 0x0

    #@28f
    .local v26, dx:F
    const/16 v27, 0x0

    #@291
    .local v27, dy:F
    const/16 v46, 0x0

    #@293
    .line 769
    .local v46, r:F
    const/16 v43, 0x0

    #@295
    .line 770
    .local v43, password:Z
    const/16 v37, 0x0

    #@297
    .line 772
    .local v37, inputType:I
    sget-object v62, Lcom/android/internal/R$styleable;->TextView:[I

    #@299
    const/16 v63, 0x0

    #@29b
    move-object/from16 v0, v58

    #@29d
    move-object/from16 v1, p2

    #@29f
    move-object/from16 v2, v62

    #@2a1
    move/from16 v3, p3

    #@2a3
    move/from16 v4, v63

    #@2a5
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@2a8
    move-result-object v5

    #@2a9
    .line 775
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@2ac
    move-result v40

    #@2ad
    .line 776
    .restart local v40       #n:I
    const/16 v35, 0x0

    #@2af
    .restart local v35       #i:I
    :goto_2af
    move/from16 v0, v35

    #@2b1
    move/from16 v1, v40

    #@2b3
    if-ge v0, v1, :cond_6b9

    #@2b5
    .line 777
    move/from16 v0, v35

    #@2b7
    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@2ba
    move-result v9

    #@2bb
    .line 779
    .restart local v9       #attr:I
    packed-switch v9, :pswitch_data_b1c

    #@2be
    .line 776
    :cond_2be
    :goto_2be
    :pswitch_2be
    add-int/lit8 v35, v35, 0x1

    #@2c0
    goto :goto_2af

    #@2c1
    .line 781
    :pswitch_2c1
    move/from16 v0, v29

    #@2c3
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2c6
    move-result v29

    #@2c7
    .line 782
    goto :goto_2be

    #@2c8
    .line 785
    :pswitch_2c8
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@2cb
    move-result-object v36

    #@2cc
    .line 786
    goto :goto_2be

    #@2cd
    .line 789
    :pswitch_2cd
    move/from16 v0, v42

    #@2cf
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2d2
    move-result v42

    #@2d3
    .line 790
    goto :goto_2be

    #@2d4
    .line 793
    :pswitch_2d4
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@2d7
    move-result-object v18

    #@2d8
    .line 794
    goto :goto_2be

    #@2d9
    .line 797
    :pswitch_2d9
    move/from16 v0, v45

    #@2db
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2de
    move-result v45

    #@2df
    .line 798
    goto :goto_2be

    #@2e0
    .line 801
    :pswitch_2e0
    invoke-virtual {v5, v9, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2e3
    move-result v11

    #@2e4
    .line 802
    goto :goto_2be

    #@2e5
    .line 805
    :pswitch_2e5
    invoke-virtual {v5, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2e8
    move-result v10

    #@2e9
    .line 806
    goto :goto_2be

    #@2ea
    .line 809
    :pswitch_2ea
    invoke-virtual {v5, v9, v13}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2ed
    move-result v13

    #@2ee
    .line 810
    goto :goto_2be

    #@2ef
    .line 813
    :pswitch_2ef
    move/from16 v0, v48

    #@2f1
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2f4
    move-result v48

    #@2f5
    .line 814
    goto :goto_2be

    #@2f6
    .line 817
    :pswitch_2f6
    const/16 v62, 0x0

    #@2f8
    move/from16 v0, v62

    #@2fa
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2fd
    move-result v62

    #@2fe
    move/from16 v0, v62

    #@300
    move-object/from16 v1, p0

    #@302
    iput v0, v1, Landroid/widget/TextView;->mAutoLinkMask:I

    #@304
    goto :goto_2be

    #@305
    .line 821
    :pswitch_305
    const/16 v62, 0x1

    #@307
    move/from16 v0, v62

    #@309
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@30c
    move-result v62

    #@30d
    move/from16 v0, v62

    #@30f
    move-object/from16 v1, p0

    #@311
    iput-boolean v0, v1, Landroid/widget/TextView;->mLinksClickable:Z

    #@313
    goto :goto_2be

    #@314
    .line 825
    :pswitch_314
    sget-boolean v62, Landroid/widget/TextView;->mRtlLangAndLocaleMetaDataExist:Z

    #@316
    if-eqz v62, :cond_31d

    #@318
    .line 826
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@31b
    move-result-object v24

    #@31c
    goto :goto_2be

    #@31d
    .line 829
    :cond_31d
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@320
    move-result-object v21

    #@321
    .line 831
    goto :goto_2be

    #@322
    .line 834
    :pswitch_322
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@325
    move-result-object v25

    #@326
    .line 835
    goto :goto_2be

    #@327
    .line 838
    :pswitch_327
    sget-boolean v62, Landroid/widget/TextView;->mRtlLangAndLocaleMetaDataExist:Z

    #@329
    if-eqz v62, :cond_330

    #@32b
    .line 839
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@32e
    move-result-object v20

    #@32f
    goto :goto_2be

    #@330
    .line 842
    :cond_330
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@333
    move-result-object v23

    #@334
    .line 844
    goto :goto_2be

    #@335
    .line 847
    :pswitch_335
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@338
    move-result-object v19

    #@339
    .line 848
    goto :goto_2be

    #@33a
    .line 851
    :pswitch_33a
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@33d
    move-result-object v24

    #@33e
    .line 852
    goto :goto_2be

    #@33f
    .line 855
    :pswitch_33f
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@342
    move-result-object v20

    #@343
    .line 856
    goto/16 :goto_2be

    #@345
    .line 859
    :pswitch_345
    move/from16 v0, v22

    #@347
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@34a
    move-result v22

    #@34b
    .line 860
    goto/16 :goto_2be

    #@34d
    .line 863
    :pswitch_34d
    const/16 v62, -0x1

    #@34f
    move/from16 v0, v62

    #@351
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@354
    move-result v62

    #@355
    move-object/from16 v0, p0

    #@357
    move/from16 v1, v62

    #@359
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    #@35c
    goto/16 :goto_2be

    #@35e
    .line 867
    :pswitch_35e
    const/16 v62, -0x1

    #@360
    move/from16 v0, v62

    #@362
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@365
    move-result v62

    #@366
    move-object/from16 v0, p0

    #@368
    move/from16 v1, v62

    #@36a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxHeight(I)V

    #@36d
    goto/16 :goto_2be

    #@36f
    .line 871
    :pswitch_36f
    const/16 v62, -0x1

    #@371
    move/from16 v0, v62

    #@373
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@376
    move-result v62

    #@377
    move-object/from16 v0, p0

    #@379
    move/from16 v1, v62

    #@37b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    #@37e
    goto/16 :goto_2be

    #@380
    .line 875
    :pswitch_380
    const/16 v62, -0x1

    #@382
    move/from16 v0, v62

    #@384
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@387
    move-result v62

    #@388
    move-object/from16 v0, p0

    #@38a
    move/from16 v1, v62

    #@38c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHeight(I)V

    #@38f
    goto/16 :goto_2be

    #@391
    .line 879
    :pswitch_391
    const/16 v62, -0x1

    #@393
    move/from16 v0, v62

    #@395
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@398
    move-result v62

    #@399
    move-object/from16 v0, p0

    #@39b
    move/from16 v1, v62

    #@39d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinLines(I)V

    #@3a0
    goto/16 :goto_2be

    #@3a2
    .line 883
    :pswitch_3a2
    const/16 v62, -0x1

    #@3a4
    move/from16 v0, v62

    #@3a6
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@3a9
    move-result v62

    #@3aa
    move-object/from16 v0, p0

    #@3ac
    move/from16 v1, v62

    #@3ae
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinHeight(I)V

    #@3b1
    goto/16 :goto_2be

    #@3b3
    .line 887
    :pswitch_3b3
    const/16 v62, -0x1

    #@3b5
    move/from16 v0, v62

    #@3b7
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3ba
    move-result v62

    #@3bb
    move-object/from16 v0, p0

    #@3bd
    move/from16 v1, v62

    #@3bf
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxEms(I)V

    #@3c2
    goto/16 :goto_2be

    #@3c4
    .line 891
    :pswitch_3c4
    const/16 v62, -0x1

    #@3c6
    move/from16 v0, v62

    #@3c8
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@3cb
    move-result v62

    #@3cc
    move-object/from16 v0, p0

    #@3ce
    move/from16 v1, v62

    #@3d0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    #@3d3
    goto/16 :goto_2be

    #@3d5
    .line 895
    :pswitch_3d5
    const/16 v62, -0x1

    #@3d7
    move/from16 v0, v62

    #@3d9
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3dc
    move-result v62

    #@3dd
    move-object/from16 v0, p0

    #@3df
    move/from16 v1, v62

    #@3e1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEms(I)V

    #@3e4
    goto/16 :goto_2be

    #@3e6
    .line 899
    :pswitch_3e6
    const/16 v62, -0x1

    #@3e8
    move/from16 v0, v62

    #@3ea
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@3ed
    move-result v62

    #@3ee
    move-object/from16 v0, p0

    #@3f0
    move/from16 v1, v62

    #@3f2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setWidth(I)V

    #@3f5
    goto/16 :goto_2be

    #@3f7
    .line 903
    :pswitch_3f7
    const/16 v62, -0x1

    #@3f9
    move/from16 v0, v62

    #@3fb
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3fe
    move-result v62

    #@3ff
    move-object/from16 v0, p0

    #@401
    move/from16 v1, v62

    #@403
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinEms(I)V

    #@406
    goto/16 :goto_2be

    #@408
    .line 907
    :pswitch_408
    const/16 v62, -0x1

    #@40a
    move/from16 v0, v62

    #@40c
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@40f
    move-result v62

    #@410
    move-object/from16 v0, p0

    #@412
    move/from16 v1, v62

    #@414
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    #@417
    goto/16 :goto_2be

    #@419
    .line 911
    :pswitch_419
    const/16 v62, -0x1

    #@41b
    move/from16 v0, v62

    #@41d
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@420
    move-result v62

    #@421
    move-object/from16 v0, p0

    #@423
    move/from16 v1, v62

    #@425
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    #@428
    goto/16 :goto_2be

    #@42a
    .line 915
    :pswitch_42a
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@42d
    move-result-object v34

    #@42e
    .line 916
    goto/16 :goto_2be

    #@430
    .line 919
    :pswitch_430
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@433
    move-result-object v52

    #@434
    .line 920
    goto/16 :goto_2be

    #@436
    .line 923
    :pswitch_436
    const/16 v62, 0x0

    #@438
    move/from16 v0, v62

    #@43a
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@43d
    move-result v62

    #@43e
    if-eqz v62, :cond_2be

    #@440
    .line 924
    const/16 v62, 0x1

    #@442
    move-object/from16 v0, p0

    #@444
    move/from16 v1, v62

    #@446
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    #@449
    goto/16 :goto_2be

    #@44b
    .line 929
    :pswitch_44b
    move/from16 v0, v50

    #@44d
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@450
    move-result v50

    #@451
    .line 930
    goto/16 :goto_2be

    #@453
    .line 933
    :pswitch_453
    move/from16 v0, v30

    #@455
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@458
    move-result v30

    #@459
    .line 934
    goto/16 :goto_2be

    #@45b
    .line 937
    :pswitch_45b
    move-object/from16 v0, p0

    #@45d
    iget v0, v0, Landroid/widget/TextView;->mMarqueeRepeatLimit:I

    #@45f
    move/from16 v62, v0

    #@461
    move/from16 v0, v62

    #@463
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@466
    move-result v62

    #@467
    move-object/from16 v0, p0

    #@469
    move/from16 v1, v62

    #@46b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    #@46e
    goto/16 :goto_2be

    #@470
    .line 941
    :pswitch_470
    const/16 v62, 0x1

    #@472
    move/from16 v0, v62

    #@474
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@477
    move-result v62

    #@478
    if-nez v62, :cond_2be

    #@47a
    .line 942
    const/16 v62, 0x0

    #@47c
    move-object/from16 v0, p0

    #@47e
    move/from16 v1, v62

    #@480
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    #@483
    goto/16 :goto_2be

    #@485
    .line 947
    :pswitch_485
    const/16 v62, 0x1

    #@487
    move/from16 v0, v62

    #@489
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@48c
    move-result v62

    #@48d
    if-nez v62, :cond_2be

    #@48f
    .line 948
    const/16 v62, 0x0

    #@491
    move-object/from16 v0, p0

    #@493
    move/from16 v1, v62

    #@495
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCursorVisible(Z)V

    #@498
    goto/16 :goto_2be

    #@49a
    .line 953
    :pswitch_49a
    const/16 v62, -0x1

    #@49c
    move/from16 v0, v62

    #@49e
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4a1
    move-result v39

    #@4a2
    .line 954
    goto/16 :goto_2be

    #@4a4
    .line 957
    :pswitch_4a4
    const/high16 v62, 0x3f80

    #@4a6
    move/from16 v0, v62

    #@4a8
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@4ab
    move-result v62

    #@4ac
    move-object/from16 v0, p0

    #@4ae
    move/from16 v1, v62

    #@4b0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextScaleX(F)V

    #@4b3
    goto/16 :goto_2be

    #@4b5
    .line 961
    :pswitch_4b5
    const/16 v62, 0x0

    #@4b7
    move/from16 v0, v62

    #@4b9
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4bc
    move-result v62

    #@4bd
    move/from16 v0, v62

    #@4bf
    move-object/from16 v1, p0

    #@4c1
    iput-boolean v0, v1, Landroid/widget/TextView;->mFreezesText:Z

    #@4c3
    goto/16 :goto_2be

    #@4c5
    .line 965
    :pswitch_4c5
    const/16 v62, 0x0

    #@4c7
    move/from16 v0, v62

    #@4c9
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4cc
    move-result v49

    #@4cd
    .line 966
    goto/16 :goto_2be

    #@4cf
    .line 969
    :pswitch_4cf
    const/16 v62, 0x0

    #@4d1
    move/from16 v0, v62

    #@4d3
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@4d6
    move-result v26

    #@4d7
    .line 970
    goto/16 :goto_2be

    #@4d9
    .line 973
    :pswitch_4d9
    const/16 v62, 0x0

    #@4db
    move/from16 v0, v62

    #@4dd
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@4e0
    move-result v27

    #@4e1
    .line 974
    goto/16 :goto_2be

    #@4e3
    .line 977
    :pswitch_4e3
    const/16 v62, 0x0

    #@4e5
    move/from16 v0, v62

    #@4e7
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@4ea
    move-result v46

    #@4eb
    .line 978
    goto/16 :goto_2be

    #@4ed
    .line 981
    :pswitch_4ed
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isEnabled()Z

    #@4f0
    move-result v62

    #@4f1
    move/from16 v0, v62

    #@4f3
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4f6
    move-result v62

    #@4f7
    move-object/from16 v0, p0

    #@4f9
    move/from16 v1, v62

    #@4fb
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    #@4fe
    goto/16 :goto_2be

    #@500
    .line 985
    :pswitch_500
    move/from16 v0, v54

    #@502
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    #@505
    move-result v54

    #@506
    .line 986
    goto/16 :goto_2be

    #@508
    .line 989
    :pswitch_508
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@50b
    move-result-object v53

    #@50c
    .line 990
    goto/16 :goto_2be

    #@50e
    .line 993
    :pswitch_50e
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@511
    move-result-object v55

    #@512
    .line 994
    goto/16 :goto_2be

    #@514
    .line 997
    :pswitch_514
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@517
    move-result-object v56

    #@518
    .line 998
    goto/16 :goto_2be

    #@51a
    .line 1001
    :pswitch_51a
    move/from16 v0, v57

    #@51c
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@51f
    move-result v57

    #@520
    .line 1002
    goto/16 :goto_2be

    #@522
    .line 1005
    :pswitch_522
    move/from16 v0, v59

    #@524
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@527
    move-result v59

    #@528
    .line 1006
    goto/16 :goto_2be

    #@52a
    .line 1009
    :pswitch_52a
    move/from16 v0, v51

    #@52c
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@52f
    move-result v51

    #@530
    .line 1010
    goto/16 :goto_2be

    #@532
    .line 1013
    :pswitch_532
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@535
    move-result-object v33

    #@536
    .line 1014
    goto/16 :goto_2be

    #@538
    .line 1017
    :pswitch_538
    move/from16 v0, v43

    #@53a
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@53d
    move-result v43

    #@53e
    .line 1018
    goto/16 :goto_2be

    #@540
    .line 1021
    :pswitch_540
    move-object/from16 v0, p0

    #@542
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@544
    move/from16 v62, v0

    #@546
    move/from16 v0, v62

    #@548
    float-to-int v0, v0

    #@549
    move/from16 v62, v0

    #@54b
    move/from16 v0, v62

    #@54d
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@550
    move-result v62

    #@551
    move/from16 v0, v62

    #@553
    int-to-float v0, v0

    #@554
    move/from16 v62, v0

    #@556
    move/from16 v0, v62

    #@558
    move-object/from16 v1, p0

    #@55a
    iput v0, v1, Landroid/widget/TextView;->mSpacingAdd:F

    #@55c
    goto/16 :goto_2be

    #@55e
    .line 1025
    :pswitch_55e
    move-object/from16 v0, p0

    #@560
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@562
    move/from16 v62, v0

    #@564
    move/from16 v0, v62

    #@566
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@569
    move-result v62

    #@56a
    move/from16 v0, v62

    #@56c
    move-object/from16 v1, p0

    #@56e
    iput v0, v1, Landroid/widget/TextView;->mSpacingMult:F

    #@570
    goto/16 :goto_2be

    #@572
    .line 1029
    :pswitch_572
    const/16 v62, 0x0

    #@574
    move/from16 v0, v62

    #@576
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@579
    move-result v37

    #@57a
    .line 1030
    goto/16 :goto_2be

    #@57c
    .line 1033
    :pswitch_57c
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@57f
    .line 1034
    move-object/from16 v0, p0

    #@581
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@583
    move-object/from16 v62, v0

    #@585
    invoke-virtual/range {v62 .. v62}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@588
    .line 1035
    move-object/from16 v0, p0

    #@58a
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@58c
    move-object/from16 v62, v0

    #@58e
    move-object/from16 v0, v62

    #@590
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@592
    move-object/from16 v62, v0

    #@594
    move-object/from16 v0, p0

    #@596
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@598
    move-object/from16 v63, v0

    #@59a
    move-object/from16 v0, v63

    #@59c
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@59e
    move-object/from16 v63, v0

    #@5a0
    move-object/from16 v0, v63

    #@5a2
    iget v0, v0, Landroid/widget/Editor$InputContentType;->imeOptions:I

    #@5a4
    move/from16 v63, v0

    #@5a6
    move/from16 v0, v63

    #@5a8
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@5ab
    move-result v63

    #@5ac
    move/from16 v0, v63

    #@5ae
    move-object/from16 v1, v62

    #@5b0
    iput v0, v1, Landroid/widget/Editor$InputContentType;->imeOptions:I

    #@5b2
    goto/16 :goto_2be

    #@5b4
    .line 1040
    :pswitch_5b4
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@5b7
    .line 1041
    move-object/from16 v0, p0

    #@5b9
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5bb
    move-object/from16 v62, v0

    #@5bd
    invoke-virtual/range {v62 .. v62}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@5c0
    .line 1042
    move-object/from16 v0, p0

    #@5c2
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5c4
    move-object/from16 v62, v0

    #@5c6
    move-object/from16 v0, v62

    #@5c8
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@5ca
    move-object/from16 v62, v0

    #@5cc
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@5cf
    move-result-object v63

    #@5d0
    move-object/from16 v0, v63

    #@5d2
    move-object/from16 v1, v62

    #@5d4
    iput-object v0, v1, Landroid/widget/Editor$InputContentType;->imeActionLabel:Ljava/lang/CharSequence;

    #@5d6
    goto/16 :goto_2be

    #@5d8
    .line 1046
    :pswitch_5d8
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@5db
    .line 1047
    move-object/from16 v0, p0

    #@5dd
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5df
    move-object/from16 v62, v0

    #@5e1
    invoke-virtual/range {v62 .. v62}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@5e4
    .line 1048
    move-object/from16 v0, p0

    #@5e6
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5e8
    move-object/from16 v62, v0

    #@5ea
    move-object/from16 v0, v62

    #@5ec
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@5ee
    move-object/from16 v62, v0

    #@5f0
    move-object/from16 v0, p0

    #@5f2
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5f4
    move-object/from16 v63, v0

    #@5f6
    move-object/from16 v0, v63

    #@5f8
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@5fa
    move-object/from16 v63, v0

    #@5fc
    move-object/from16 v0, v63

    #@5fe
    iget v0, v0, Landroid/widget/Editor$InputContentType;->imeActionId:I

    #@600
    move/from16 v63, v0

    #@602
    move/from16 v0, v63

    #@604
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@607
    move-result v63

    #@608
    move/from16 v0, v63

    #@60a
    move-object/from16 v1, v62

    #@60c
    iput v0, v1, Landroid/widget/Editor$InputContentType;->imeActionId:I

    #@60e
    goto/16 :goto_2be

    #@610
    .line 1053
    :pswitch_610
    invoke-virtual {v5, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@613
    move-result-object v62

    #@614
    move-object/from16 v0, p0

    #@616
    move-object/from16 v1, v62

    #@618
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    #@61b
    goto/16 :goto_2be

    #@61d
    .line 1058
    :pswitch_61d
    const/16 v62, 0x0

    #@61f
    :try_start_61f
    move/from16 v0, v62

    #@621
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@624
    move-result v62

    #@625
    move-object/from16 v0, p0

    #@627
    move/from16 v1, v62

    #@629
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setInputExtras(I)V
    :try_end_62c
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_61f .. :try_end_62c} :catch_62e
    .catch Ljava/io/IOException; {:try_start_61f .. :try_end_62c} :catch_63e

    #@62c
    goto/16 :goto_2be

    #@62e
    .line 1059
    :catch_62e
    move-exception v28

    #@62f
    .line 1060
    .local v28, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v62, "TextView"

    #@631
    const-string v63, "Failure reading input extras"

    #@633
    move-object/from16 v0, v62

    #@635
    move-object/from16 v1, v63

    #@637
    move-object/from16 v2, v28

    #@639
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63c
    goto/16 :goto_2be

    #@63e
    .line 1061
    .end local v28           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_63e
    move-exception v28

    #@63f
    .line 1062
    .local v28, e:Ljava/io/IOException;
    const-string v62, "TextView"

    #@641
    const-string v63, "Failure reading input extras"

    #@643
    move-object/from16 v0, v62

    #@645
    move-object/from16 v1, v63

    #@647
    move-object/from16 v2, v28

    #@649
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64c
    goto/16 :goto_2be

    #@64e
    .line 1067
    .end local v28           #e:Ljava/io/IOException;
    :pswitch_64e
    const/16 v62, 0x0

    #@650
    move/from16 v0, v62

    #@652
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@655
    move-result v62

    #@656
    move/from16 v0, v62

    #@658
    move-object/from16 v1, p0

    #@65a
    iput v0, v1, Landroid/widget/TextView;->mCursorDrawableRes:I

    #@65c
    goto/16 :goto_2be

    #@65e
    .line 1071
    :pswitch_65e
    const/16 v62, 0x0

    #@660
    move/from16 v0, v62

    #@662
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@665
    move-result v62

    #@666
    move/from16 v0, v62

    #@668
    move-object/from16 v1, p0

    #@66a
    iput v0, v1, Landroid/widget/TextView;->mTextSelectHandleLeftRes:I

    #@66c
    goto/16 :goto_2be

    #@66e
    .line 1075
    :pswitch_66e
    const/16 v62, 0x0

    #@670
    move/from16 v0, v62

    #@672
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@675
    move-result v62

    #@676
    move/from16 v0, v62

    #@678
    move-object/from16 v1, p0

    #@67a
    iput v0, v1, Landroid/widget/TextView;->mTextSelectHandleRightRes:I

    #@67c
    goto/16 :goto_2be

    #@67e
    .line 1079
    :pswitch_67e
    const/16 v62, 0x0

    #@680
    move/from16 v0, v62

    #@682
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@685
    move-result v62

    #@686
    move/from16 v0, v62

    #@688
    move-object/from16 v1, p0

    #@68a
    iput v0, v1, Landroid/widget/TextView;->mTextSelectHandleRes:I

    #@68c
    goto/16 :goto_2be

    #@68e
    .line 1083
    :pswitch_68e
    const/16 v62, 0x0

    #@690
    move/from16 v0, v62

    #@692
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@695
    move-result v62

    #@696
    move/from16 v0, v62

    #@698
    move-object/from16 v1, p0

    #@69a
    iput v0, v1, Landroid/widget/TextView;->mTextEditSuggestionItemLayout:I

    #@69c
    goto/16 :goto_2be

    #@69e
    .line 1087
    :pswitch_69e
    const/16 v62, 0x0

    #@6a0
    move/from16 v0, v62

    #@6a2
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6a5
    move-result v62

    #@6a6
    move-object/from16 v0, p0

    #@6a8
    move/from16 v1, v62

    #@6aa
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    #@6ad
    goto/16 :goto_2be

    #@6af
    .line 1091
    :pswitch_6af
    const/16 v62, 0x0

    #@6b1
    move/from16 v0, v62

    #@6b3
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6b6
    move-result v6

    #@6b7
    goto/16 :goto_2be

    #@6b9
    .line 1095
    .end local v9           #attr:I
    :cond_6b9
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    #@6bc
    .line 1097
    sget-object v12, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@6be
    .line 1099
    .local v12, bufferType:Landroid/widget/TextView$BufferType;
    move/from16 v0, v37

    #@6c0
    and-int/lit16 v0, v0, 0xfff

    #@6c2
    move/from16 v60, v0

    #@6c4
    .line 1101
    .local v60, variation:I
    const/16 v62, 0x81

    #@6c6
    move/from16 v0, v60

    #@6c8
    move/from16 v1, v62

    #@6ca
    if-ne v0, v1, :cond_860

    #@6cc
    const/16 v44, 0x1

    #@6ce
    .line 1103
    .local v44, passwordInputType:Z
    :goto_6ce
    const/16 v62, 0xe1

    #@6d0
    move/from16 v0, v60

    #@6d2
    move/from16 v1, v62

    #@6d4
    if-ne v0, v1, :cond_864

    #@6d6
    const/16 v61, 0x1

    #@6d8
    .line 1105
    .local v61, webPasswordInputType:Z
    :goto_6d8
    const/16 v62, 0x12

    #@6da
    move/from16 v0, v60

    #@6dc
    move/from16 v1, v62

    #@6de
    if-ne v0, v1, :cond_868

    #@6e0
    const/16 v41, 0x1

    #@6e2
    .line 1108
    .local v41, numberPasswordInputType:Z
    :goto_6e2
    if-eqz v36, :cond_8b0

    #@6e4
    .line 1112
    :try_start_6e4
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6e7
    move-result-object v62

    #@6e8
    invoke-static/range {v62 .. v62}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_6eb
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6e4 .. :try_end_6eb} :catch_86c

    #@6eb
    move-result-object v14

    #@6ec
    .line 1118
    .local v14, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_6ec
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@6ef
    .line 1119
    move-object/from16 v0, p0

    #@6f1
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6f3
    move-object/from16 v63, v0

    #@6f5
    invoke-virtual {v14}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@6f8
    move-result-object v62

    #@6f9
    check-cast v62, Landroid/text/method/KeyListener;

    #@6fb
    move-object/from16 v0, v62

    #@6fd
    move-object/from16 v1, v63

    #@6ff
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;
    :try_end_701
    .catch Ljava/lang/InstantiationException; {:try_start_6ec .. :try_end_701} :catch_877
    .catch Ljava/lang/IllegalAccessException; {:try_start_6ec .. :try_end_701} :catch_882

    #@701
    .line 1126
    :try_start_701
    move-object/from16 v0, p0

    #@703
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@705
    move-object/from16 v63, v0

    #@707
    if-eqz v37, :cond_88d

    #@709
    move/from16 v62, v37

    #@70b
    :goto_70b
    move/from16 v0, v62

    #@70d
    move-object/from16 v1, v63

    #@70f
    iput v0, v1, Landroid/widget/Editor;->mInputType:I
    :try_end_711
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_701 .. :try_end_711} :catch_89f

    #@711
    .line 1218
    .end local v14           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_711
    move-object/from16 v0, p0

    #@713
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@715
    move-object/from16 v62, v0

    #@717
    if-eqz v62, :cond_72c

    #@719
    move-object/from16 v0, p0

    #@71b
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@71d
    move-object/from16 v62, v0

    #@71f
    move-object/from16 v0, v62

    #@721
    move/from16 v1, v43

    #@723
    move/from16 v2, v44

    #@725
    move/from16 v3, v61

    #@727
    move/from16 v4, v41

    #@729
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Editor;->adjustInputType(ZZZZ)V

    #@72c
    .line 1221
    :cond_72c
    if-eqz v48, :cond_747

    #@72e
    .line 1222
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@731
    .line 1223
    move-object/from16 v0, p0

    #@733
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@735
    move-object/from16 v62, v0

    #@737
    const/16 v63, 0x1

    #@739
    move/from16 v0, v63

    #@73b
    move-object/from16 v1, v62

    #@73d
    iput-boolean v0, v1, Landroid/widget/Editor;->mSelectAllOnFocus:Z

    #@73f
    .line 1225
    sget-object v62, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    #@741
    move-object/from16 v0, v62

    #@743
    if-ne v12, v0, :cond_747

    #@745
    .line 1226
    sget-object v12, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@747
    .line 1229
    :cond_747
    move-object/from16 v0, p0

    #@749
    move-object/from16 v1, v21

    #@74b
    move-object/from16 v2, v25

    #@74d
    move-object/from16 v3, v23

    #@74f
    move-object/from16 v4, v19

    #@751
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@754
    .line 1231
    move-object/from16 v0, p0

    #@756
    move-object/from16 v1, v24

    #@758
    move-object/from16 v2, v20

    #@75a
    invoke-direct {v0, v1, v2}, Landroid/widget/TextView;->setRelativeDrawablesIfNeeded(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@75d
    .line 1232
    move-object/from16 v0, p0

    #@75f
    move/from16 v1, v22

    #@761
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    #@764
    .line 1236
    move-object/from16 v0, p0

    #@766
    move/from16 v1, v50

    #@768
    invoke-direct {v0, v1}, Landroid/widget/TextView;->setInputTypeSingleLine(Z)V

    #@76b
    .line 1237
    move-object/from16 v0, p0

    #@76d
    move/from16 v1, v50

    #@76f
    move/from16 v2, v50

    #@771
    move/from16 v3, v50

    #@773
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/TextView;->applySingleLine(ZZZ)V

    #@776
    .line 1239
    if-eqz v50, :cond_782

    #@778
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@77b
    move-result-object v62

    #@77c
    if-nez v62, :cond_782

    #@77e
    if-gez v30, :cond_782

    #@780
    .line 1240
    const/16 v30, 0x3

    #@782
    .line 1243
    :cond_782
    packed-switch v30, :pswitch_data_bb8

    #@785
    .line 1265
    :goto_785
    if-eqz v53, :cond_a99

    #@787
    .end local v53           #textColor:Landroid/content/res/ColorStateList;
    :goto_787
    move-object/from16 v0, p0

    #@789
    move-object/from16 v1, v53

    #@78b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    #@78e
    .line 1266
    move-object/from16 v0, p0

    #@790
    move-object/from16 v1, v55

    #@792
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    #@795
    .line 1267
    move-object/from16 v0, p0

    #@797
    move-object/from16 v1, v56

    #@799
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    #@79c
    .line 1268
    if-eqz v54, :cond_7a5

    #@79e
    .line 1269
    move-object/from16 v0, p0

    #@7a0
    move/from16 v1, v54

    #@7a2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHighlightColor(I)V

    #@7a5
    .line 1271
    :cond_7a5
    move/from16 v0, v57

    #@7a7
    int-to-float v0, v0

    #@7a8
    move/from16 v62, v0

    #@7aa
    move-object/from16 v0, p0

    #@7ac
    move/from16 v1, v62

    #@7ae
    invoke-direct {v0, v1}, Landroid/widget/TextView;->setRawTextSize(F)V

    #@7b1
    .line 1273
    if-eqz v6, :cond_7c3

    #@7b3
    .line 1274
    new-instance v62, Landroid/text/method/AllCapsTransformationMethod;

    #@7b5
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@7b8
    move-result-object v63

    #@7b9
    invoke-direct/range {v62 .. v63}, Landroid/text/method/AllCapsTransformationMethod;-><init>(Landroid/content/Context;)V

    #@7bc
    move-object/from16 v0, p0

    #@7be
    move-object/from16 v1, v62

    #@7c0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@7c3
    .line 1277
    :cond_7c3
    if-nez v43, :cond_7cb

    #@7c5
    if-nez v44, :cond_7cb

    #@7c7
    if-nez v61, :cond_7cb

    #@7c9
    if-eqz v41, :cond_7da

    #@7cb
    .line 1278
    :cond_7cb
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    #@7ce
    move-result-object v62

    #@7cf
    move-object/from16 v0, p0

    #@7d1
    move-object/from16 v1, v62

    #@7d3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@7d6
    .line 1280
    sget-boolean v62, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@7d8
    if-eqz v62, :cond_aa1

    #@7da
    .line 1295
    :cond_7da
    :goto_7da
    move-object/from16 v0, p0

    #@7dc
    move-object/from16 v1, v33

    #@7de
    move/from16 v2, v59

    #@7e0
    move/from16 v3, v51

    #@7e2
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/TextView;->setTypefaceFromAttrs(Ljava/lang/String;II)V

    #@7e5
    .line 1297
    if-eqz v49, :cond_7f4

    #@7e7
    .line 1298
    move-object/from16 v0, p0

    #@7e9
    move/from16 v1, v46

    #@7eb
    move/from16 v2, v26

    #@7ed
    move/from16 v3, v27

    #@7ef
    move/from16 v4, v49

    #@7f1
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    #@7f4
    .line 1301
    :cond_7f4
    if-ltz v39, :cond_aa5

    #@7f6
    .line 1302
    const/16 v62, 0x1

    #@7f8
    move/from16 v0, v62

    #@7fa
    new-array v0, v0, [Landroid/text/InputFilter;

    #@7fc
    move-object/from16 v62, v0

    #@7fe
    const/16 v63, 0x0

    #@800
    new-instance v64, Landroid/text/InputFilter$LengthFilter;

    #@802
    move-object/from16 v0, v64

    #@804
    move/from16 v1, v39

    #@806
    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    #@809
    aput-object v64, v62, v63

    #@80b
    move-object/from16 v0, p0

    #@80d
    move-object/from16 v1, v62

    #@80f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    #@812
    .line 1307
    :goto_812
    move-object/from16 v0, p0

    #@814
    move-object/from16 v1, v52

    #@816
    invoke-virtual {v0, v1, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@819
    .line 1308
    if-eqz v34, :cond_822

    #@81b
    move-object/from16 v0, p0

    #@81d
    move-object/from16 v1, v34

    #@81f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    #@822
    .line 1315
    :cond_822
    sget-object v62, Lcom/android/internal/R$styleable;->View:[I

    #@824
    const/16 v63, 0x0

    #@826
    move-object/from16 v0, p1

    #@828
    move-object/from16 v1, p2

    #@82a
    move-object/from16 v2, v62

    #@82c
    move/from16 v3, p3

    #@82e
    move/from16 v4, v63

    #@830
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@833
    move-result-object v5

    #@834
    .line 1319
    move-object/from16 v0, p0

    #@836
    iget-object v0, v0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@838
    move-object/from16 v62, v0

    #@83a
    if-nez v62, :cond_842

    #@83c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@83f
    move-result-object v62

    #@840
    if-eqz v62, :cond_ab0

    #@842
    :cond_842
    const/16 v32, 0x1

    #@844
    .line 1320
    .local v32, focusable:Z
    :goto_844
    move/from16 v16, v32

    #@846
    .line 1321
    .local v16, clickable:Z
    move/from16 v38, v32

    #@848
    .line 1323
    .local v38, longClickable:Z
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@84b
    move-result v40

    #@84c
    .line 1324
    const/16 v35, 0x0

    #@84e
    :goto_84e
    move/from16 v0, v35

    #@850
    move/from16 v1, v40

    #@852
    if-ge v0, v1, :cond_acc

    #@854
    .line 1325
    move/from16 v0, v35

    #@856
    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@859
    move-result v9

    #@85a
    .line 1327
    .restart local v9       #attr:I
    sparse-switch v9, :sswitch_data_bc4

    #@85d
    .line 1324
    :goto_85d
    add-int/lit8 v35, v35, 0x1

    #@85f
    goto :goto_84e

    #@860
    .line 1101
    .end local v9           #attr:I
    .end local v16           #clickable:Z
    .end local v32           #focusable:Z
    .end local v38           #longClickable:Z
    .end local v41           #numberPasswordInputType:Z
    .end local v44           #passwordInputType:Z
    .end local v61           #webPasswordInputType:Z
    .restart local v53       #textColor:Landroid/content/res/ColorStateList;
    :cond_860
    const/16 v44, 0x0

    #@862
    goto/16 :goto_6ce

    #@864
    .line 1103
    .restart local v44       #passwordInputType:Z
    :cond_864
    const/16 v61, 0x0

    #@866
    goto/16 :goto_6d8

    #@868
    .line 1105
    .restart local v61       #webPasswordInputType:Z
    :cond_868
    const/16 v41, 0x0

    #@86a
    goto/16 :goto_6e2

    #@86c
    .line 1113
    .restart local v41       #numberPasswordInputType:Z
    :catch_86c
    move-exception v31

    #@86d
    .line 1114
    .local v31, ex:Ljava/lang/ClassNotFoundException;
    new-instance v62, Ljava/lang/RuntimeException;

    #@86f
    move-object/from16 v0, v62

    #@871
    move-object/from16 v1, v31

    #@873
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@876
    throw v62

    #@877
    .line 1120
    .end local v31           #ex:Ljava/lang/ClassNotFoundException;
    .restart local v14       #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_877
    move-exception v31

    #@878
    .line 1121
    .local v31, ex:Ljava/lang/InstantiationException;
    new-instance v62, Ljava/lang/RuntimeException;

    #@87a
    move-object/from16 v0, v62

    #@87c
    move-object/from16 v1, v31

    #@87e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@881
    throw v62

    #@882
    .line 1122
    .end local v31           #ex:Ljava/lang/InstantiationException;
    :catch_882
    move-exception v31

    #@883
    .line 1123
    .local v31, ex:Ljava/lang/IllegalAccessException;
    new-instance v62, Ljava/lang/RuntimeException;

    #@885
    move-object/from16 v0, v62

    #@887
    move-object/from16 v1, v31

    #@889
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@88c
    throw v62

    #@88d
    .line 1126
    .end local v31           #ex:Ljava/lang/IllegalAccessException;
    :cond_88d
    :try_start_88d
    move-object/from16 v0, p0

    #@88f
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@891
    move-object/from16 v62, v0

    #@893
    move-object/from16 v0, v62

    #@895
    iget-object v0, v0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@897
    move-object/from16 v62, v0

    #@899
    invoke-interface/range {v62 .. v62}, Landroid/text/method/KeyListener;->getInputType()I
    :try_end_89c
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_88d .. :try_end_89c} :catch_89f

    #@89c
    move-result v62

    #@89d
    goto/16 :goto_70b

    #@89f
    .line 1129
    :catch_89f
    move-exception v28

    #@8a0
    .line 1130
    .local v28, e:Ljava/lang/IncompatibleClassChangeError;
    move-object/from16 v0, p0

    #@8a2
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8a4
    move-object/from16 v62, v0

    #@8a6
    const/16 v63, 0x1

    #@8a8
    move/from16 v0, v63

    #@8aa
    move-object/from16 v1, v62

    #@8ac
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@8ae
    goto/16 :goto_711

    #@8b0
    .line 1132
    .end local v14           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v28           #e:Ljava/lang/IncompatibleClassChangeError;
    :cond_8b0
    if-eqz v18, :cond_8de

    #@8b2
    .line 1133
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@8b5
    .line 1134
    move-object/from16 v0, p0

    #@8b7
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8b9
    move-object/from16 v62, v0

    #@8bb
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8be
    move-result-object v63

    #@8bf
    invoke-static/range {v63 .. v63}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    #@8c2
    move-result-object v63

    #@8c3
    move-object/from16 v0, v63

    #@8c5
    move-object/from16 v1, v62

    #@8c7
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@8c9
    .line 1138
    move-object/from16 v0, p0

    #@8cb
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8cd
    move-object/from16 v63, v0

    #@8cf
    if-eqz v37, :cond_8db

    #@8d1
    move/from16 v62, v37

    #@8d3
    :goto_8d3
    move/from16 v0, v62

    #@8d5
    move-object/from16 v1, v63

    #@8d7
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@8d9
    goto/16 :goto_711

    #@8db
    :cond_8db
    const/16 v62, 0x1

    #@8dd
    goto :goto_8d3

    #@8de
    .line 1140
    :cond_8de
    if-eqz v37, :cond_8f8

    #@8e0
    .line 1141
    const/16 v62, 0x1

    #@8e2
    move-object/from16 v0, p0

    #@8e4
    move/from16 v1, v37

    #@8e6
    move/from16 v2, v62

    #@8e8
    invoke-direct {v0, v1, v2}, Landroid/widget/TextView;->setInputType(IZ)V

    #@8eb
    .line 1143
    invoke-static/range {v37 .. v37}, Landroid/widget/TextView;->isMultilineInputType(I)Z

    #@8ee
    move-result v62

    #@8ef
    if-nez v62, :cond_8f5

    #@8f1
    const/16 v50, 0x1

    #@8f3
    :goto_8f3
    goto/16 :goto_711

    #@8f5
    :cond_8f5
    const/16 v50, 0x0

    #@8f7
    goto :goto_8f3

    #@8f8
    .line 1144
    :cond_8f8
    if-eqz v45, :cond_91d

    #@8fa
    .line 1145
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@8fd
    .line 1146
    move-object/from16 v0, p0

    #@8ff
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@901
    move-object/from16 v62, v0

    #@903
    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    #@906
    move-result-object v63

    #@907
    move-object/from16 v0, v63

    #@909
    move-object/from16 v1, v62

    #@90b
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@90d
    .line 1147
    move-object/from16 v0, p0

    #@90f
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@911
    move-object/from16 v62, v0

    #@913
    const/16 v37, 0x3

    #@915
    move/from16 v0, v37

    #@917
    move-object/from16 v1, v62

    #@919
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@91b
    goto/16 :goto_711

    #@91d
    .line 1148
    :cond_91d
    if-eqz v42, :cond_970

    #@91f
    .line 1149
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@922
    .line 1150
    move-object/from16 v0, p0

    #@924
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@926
    move-object/from16 v64, v0

    #@928
    and-int/lit8 v62, v42, 0x2

    #@92a
    if-eqz v62, :cond_968

    #@92c
    const/16 v62, 0x1

    #@92e
    move/from16 v63, v62

    #@930
    :goto_930
    and-int/lit8 v62, v42, 0x4

    #@932
    if-eqz v62, :cond_96d

    #@934
    const/16 v62, 0x1

    #@936
    :goto_936
    move/from16 v0, v63

    #@938
    move/from16 v1, v62

    #@93a
    invoke-static {v0, v1}, Landroid/text/method/DigitsKeyListener;->getInstance(ZZ)Landroid/text/method/DigitsKeyListener;

    #@93d
    move-result-object v62

    #@93e
    move-object/from16 v0, v62

    #@940
    move-object/from16 v1, v64

    #@942
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@944
    .line 1152
    const/16 v37, 0x2

    #@946
    .line 1153
    and-int/lit8 v62, v42, 0x2

    #@948
    if-eqz v62, :cond_950

    #@94a
    .line 1154
    move/from16 v0, v37

    #@94c
    or-int/lit16 v0, v0, 0x1000

    #@94e
    move/from16 v37, v0

    #@950
    .line 1156
    :cond_950
    and-int/lit8 v62, v42, 0x4

    #@952
    if-eqz v62, :cond_95a

    #@954
    .line 1157
    move/from16 v0, v37

    #@956
    or-int/lit16 v0, v0, 0x2000

    #@958
    move/from16 v37, v0

    #@95a
    .line 1159
    :cond_95a
    move-object/from16 v0, p0

    #@95c
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@95e
    move-object/from16 v62, v0

    #@960
    move/from16 v0, v37

    #@962
    move-object/from16 v1, v62

    #@964
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@966
    goto/16 :goto_711

    #@968
    .line 1150
    :cond_968
    const/16 v62, 0x0

    #@96a
    move/from16 v63, v62

    #@96c
    goto :goto_930

    #@96d
    :cond_96d
    const/16 v62, 0x0

    #@96f
    goto :goto_936

    #@970
    .line 1160
    :cond_970
    if-nez v11, :cond_978

    #@972
    const/16 v62, -0x1

    #@974
    move/from16 v0, v62

    #@976
    if-eq v10, v0, :cond_9bb

    #@978
    .line 1163
    :cond_978
    const/16 v37, 0x1

    #@97a
    .line 1165
    packed-switch v10, :pswitch_data_bd2

    #@97d
    .line 1182
    sget-object v15, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@97f
    .line 1186
    .local v15, cap:Landroid/text/method/TextKeyListener$Capitalize;
    :goto_97f
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@982
    .line 1187
    move-object/from16 v0, p0

    #@984
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@986
    move-object/from16 v62, v0

    #@988
    invoke-static {v11, v15}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    #@98b
    move-result-object v63

    #@98c
    move-object/from16 v0, v63

    #@98e
    move-object/from16 v1, v62

    #@990
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@992
    .line 1188
    move-object/from16 v0, p0

    #@994
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@996
    move-object/from16 v62, v0

    #@998
    move/from16 v0, v37

    #@99a
    move-object/from16 v1, v62

    #@99c
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@99e
    goto/16 :goto_711

    #@9a0
    .line 1167
    .end local v15           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :pswitch_9a0
    sget-object v15, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    #@9a2
    .line 1168
    .restart local v15       #cap:Landroid/text/method/TextKeyListener$Capitalize;
    move/from16 v0, v37

    #@9a4
    or-int/lit16 v0, v0, 0x4000

    #@9a6
    move/from16 v37, v0

    #@9a8
    .line 1169
    goto :goto_97f

    #@9a9
    .line 1172
    .end local v15           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :pswitch_9a9
    sget-object v15, Landroid/text/method/TextKeyListener$Capitalize;->WORDS:Landroid/text/method/TextKeyListener$Capitalize;

    #@9ab
    .line 1173
    .restart local v15       #cap:Landroid/text/method/TextKeyListener$Capitalize;
    move/from16 v0, v37

    #@9ad
    or-int/lit16 v0, v0, 0x2000

    #@9af
    move/from16 v37, v0

    #@9b1
    .line 1174
    goto :goto_97f

    #@9b2
    .line 1177
    .end local v15           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :pswitch_9b2
    sget-object v15, Landroid/text/method/TextKeyListener$Capitalize;->CHARACTERS:Landroid/text/method/TextKeyListener$Capitalize;

    #@9b4
    .line 1178
    .restart local v15       #cap:Landroid/text/method/TextKeyListener$Capitalize;
    move/from16 v0, v37

    #@9b6
    or-int/lit16 v0, v0, 0x1000

    #@9b8
    move/from16 v37, v0

    #@9ba
    .line 1179
    goto :goto_97f

    #@9bb
    .line 1189
    .end local v15           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :cond_9bb
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isTextSelectable()Z

    #@9be
    move-result v62

    #@9bf
    if-eqz v62, :cond_9f4

    #@9c1
    .line 1191
    move-object/from16 v0, p0

    #@9c3
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9c5
    move-object/from16 v62, v0

    #@9c7
    if-eqz v62, :cond_9e5

    #@9c9
    .line 1192
    move-object/from16 v0, p0

    #@9cb
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9cd
    move-object/from16 v62, v0

    #@9cf
    const/16 v63, 0x0

    #@9d1
    move-object/from16 v0, v63

    #@9d3
    move-object/from16 v1, v62

    #@9d5
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@9d7
    .line 1193
    move-object/from16 v0, p0

    #@9d9
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9db
    move-object/from16 v62, v0

    #@9dd
    const/16 v63, 0x0

    #@9df
    move/from16 v0, v63

    #@9e1
    move-object/from16 v1, v62

    #@9e3
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@9e5
    .line 1195
    :cond_9e5
    sget-object v12, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@9e7
    .line 1197
    invoke-static {}, Landroid/text/method/ArrowKeyMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    #@9ea
    move-result-object v62

    #@9eb
    move-object/from16 v0, p0

    #@9ed
    move-object/from16 v1, v62

    #@9ef
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    #@9f2
    goto/16 :goto_711

    #@9f4
    .line 1198
    :cond_9f4
    if-eqz v29, :cond_a19

    #@9f6
    .line 1199
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@9f9
    .line 1200
    move-object/from16 v0, p0

    #@9fb
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9fd
    move-object/from16 v62, v0

    #@9ff
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@a02
    move-result-object v63

    #@a03
    move-object/from16 v0, v63

    #@a05
    move-object/from16 v1, v62

    #@a07
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@a09
    .line 1201
    move-object/from16 v0, p0

    #@a0b
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a0d
    move-object/from16 v62, v0

    #@a0f
    const/16 v63, 0x1

    #@a11
    move/from16 v0, v63

    #@a13
    move-object/from16 v1, v62

    #@a15
    iput v0, v1, Landroid/widget/Editor;->mInputType:I

    #@a17
    goto/16 :goto_711

    #@a19
    .line 1203
    :cond_a19
    move-object/from16 v0, p0

    #@a1b
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a1d
    move-object/from16 v62, v0

    #@a1f
    if-eqz v62, :cond_a2f

    #@a21
    move-object/from16 v0, p0

    #@a23
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a25
    move-object/from16 v62, v0

    #@a27
    const/16 v63, 0x0

    #@a29
    move-object/from16 v0, v63

    #@a2b
    move-object/from16 v1, v62

    #@a2d
    iput-object v0, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@a2f
    .line 1205
    :cond_a2f
    packed-switch v13, :pswitch_data_bdc

    #@a32
    goto/16 :goto_711

    #@a34
    .line 1207
    :pswitch_a34
    sget-object v12, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    #@a36
    .line 1208
    goto/16 :goto_711

    #@a38
    .line 1210
    :pswitch_a38
    sget-object v12, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@a3a
    .line 1211
    goto/16 :goto_711

    #@a3c
    .line 1213
    :pswitch_a3c
    sget-object v12, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@a3e
    goto/16 :goto_711

    #@a40
    .line 1245
    :pswitch_a40
    sget-object v62, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    #@a42
    move-object/from16 v0, p0

    #@a44
    move-object/from16 v1, v62

    #@a46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    #@a49
    goto/16 :goto_785

    #@a4b
    .line 1248
    :pswitch_a4b
    sget-object v62, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    #@a4d
    move-object/from16 v0, p0

    #@a4f
    move-object/from16 v1, v62

    #@a51
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    #@a54
    goto/16 :goto_785

    #@a56
    .line 1251
    :pswitch_a56
    sget-object v62, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    #@a58
    move-object/from16 v0, p0

    #@a5a
    move-object/from16 v1, v62

    #@a5c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    #@a5f
    goto/16 :goto_785

    #@a61
    .line 1254
    :pswitch_a61
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@a64
    move-result-object v62

    #@a65
    invoke-virtual/range {v62 .. v62}, Landroid/view/ViewConfiguration;->isFadingMarqueeEnabled()Z

    #@a68
    move-result v62

    #@a69
    if-eqz v62, :cond_a87

    #@a6b
    .line 1255
    const/16 v62, 0x1

    #@a6d
    move-object/from16 v0, p0

    #@a6f
    move/from16 v1, v62

    #@a71
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    #@a74
    .line 1256
    const/16 v62, 0x0

    #@a76
    move/from16 v0, v62

    #@a78
    move-object/from16 v1, p0

    #@a7a
    iput v0, v1, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@a7c
    .line 1261
    :goto_a7c
    sget-object v62, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@a7e
    move-object/from16 v0, p0

    #@a80
    move-object/from16 v1, v62

    #@a82
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    #@a85
    goto/16 :goto_785

    #@a87
    .line 1258
    :cond_a87
    const/16 v62, 0x0

    #@a89
    move-object/from16 v0, p0

    #@a8b
    move/from16 v1, v62

    #@a8d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    #@a90
    .line 1259
    const/16 v62, 0x1

    #@a92
    move/from16 v0, v62

    #@a94
    move-object/from16 v1, p0

    #@a96
    iput v0, v1, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@a98
    goto :goto_a7c

    #@a99
    .line 1265
    :cond_a99
    const/high16 v62, -0x100

    #@a9b
    invoke-static/range {v62 .. v62}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    #@a9e
    move-result-object v53

    #@a9f
    goto/16 :goto_787

    #@aa1
    .line 1283
    .end local v53           #textColor:Landroid/content/res/ColorStateList;
    :cond_aa1
    const/16 v59, 0x3

    #@aa3
    goto/16 :goto_7da

    #@aa5
    .line 1304
    :cond_aa5
    sget-object v62, Landroid/widget/TextView;->NO_FILTERS:[Landroid/text/InputFilter;

    #@aa7
    move-object/from16 v0, p0

    #@aa9
    move-object/from16 v1, v62

    #@aab
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    #@aae
    goto/16 :goto_812

    #@ab0
    .line 1319
    :cond_ab0
    const/16 v32, 0x0

    #@ab2
    goto/16 :goto_844

    #@ab4
    .line 1329
    .restart local v9       #attr:I
    .restart local v16       #clickable:Z
    .restart local v32       #focusable:Z
    .restart local v38       #longClickable:Z
    :sswitch_ab4
    move/from16 v0, v32

    #@ab6
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ab9
    move-result v32

    #@aba
    .line 1330
    goto/16 :goto_85d

    #@abc
    .line 1333
    :sswitch_abc
    move/from16 v0, v16

    #@abe
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ac1
    move-result v16

    #@ac2
    .line 1334
    goto/16 :goto_85d

    #@ac4
    .line 1337
    :sswitch_ac4
    move/from16 v0, v38

    #@ac6
    invoke-virtual {v5, v9, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ac9
    move-result v38

    #@aca
    goto/16 :goto_85d

    #@acc
    .line 1341
    .end local v9           #attr:I
    :cond_acc
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    #@acf
    .line 1343
    move-object/from16 v0, p0

    #@ad1
    move/from16 v1, v32

    #@ad3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    #@ad6
    .line 1344
    move-object/from16 v0, p0

    #@ad8
    move/from16 v1, v16

    #@ada
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    #@add
    .line 1345
    move-object/from16 v0, p0

    #@adf
    move/from16 v1, v38

    #@ae1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLongClickable(Z)V

    #@ae4
    .line 1347
    move-object/from16 v0, p0

    #@ae6
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@ae8
    move-object/from16 v62, v0

    #@aea
    if-eqz v62, :cond_af5

    #@aec
    move-object/from16 v0, p0

    #@aee
    iget-object v0, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@af0
    move-object/from16 v62, v0

    #@af2
    invoke-virtual/range {v62 .. v62}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@af5
    .line 1350
    :cond_af5
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getImportantForAccessibility()I

    #@af8
    move-result v62

    #@af9
    if-nez v62, :cond_b04

    #@afb
    .line 1351
    const/16 v62, 0x1

    #@afd
    move-object/from16 v0, p0

    #@aff
    move/from16 v1, v62

    #@b01
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    #@b04
    .line 1353
    :cond_b04
    return-void

    #@b05
    .line 708
    nop

    #@b06
    :pswitch_data_b06
    .packed-switch 0x0
        :pswitch_238
        :pswitch_23f
        :pswitch_24d
        :pswitch_229
        :pswitch_222
        :pswitch_22e
        :pswitch_233
        :pswitch_256
        :pswitch_248
    .end packed-switch

    #@b1c
    .line 779
    :pswitch_data_b1c
    .packed-switch 0x0
        :pswitch_4ed
        :pswitch_2be
        :pswitch_51a
        :pswitch_522
        :pswitch_52a
        :pswitch_508
        :pswitch_500
        :pswitch_50e
        :pswitch_514
        :pswitch_453
        :pswitch_419
        :pswitch_2f6
        :pswitch_305
        :pswitch_3c4
        :pswitch_35e
        :pswitch_408
        :pswitch_3a2
        :pswitch_2ea
        :pswitch_430
        :pswitch_42a
        :pswitch_4a4
        :pswitch_485
        :pswitch_34d
        :pswitch_36f
        :pswitch_380
        :pswitch_391
        :pswitch_3b3
        :pswitch_3d5
        :pswitch_3e6
        :pswitch_3f7
        :pswitch_436
        :pswitch_538
        :pswitch_44b
        :pswitch_2ef
        :pswitch_470
        :pswitch_49a
        :pswitch_4c5
        :pswitch_4cf
        :pswitch_4d9
        :pswitch_4e3
        :pswitch_2cd
        :pswitch_2d4
        :pswitch_2d9
        :pswitch_2c8
        :pswitch_2e5
        :pswitch_2e0
        :pswitch_2c1
        :pswitch_4b5
        :pswitch_322
        :pswitch_335
        :pswitch_314
        :pswitch_327
        :pswitch_345
        :pswitch_540
        :pswitch_55e
        :pswitch_45b
        :pswitch_572
        :pswitch_610
        :pswitch_61d
        :pswitch_57c
        :pswitch_5b4
        :pswitch_5d8
        :pswitch_65e
        :pswitch_66e
        :pswitch_67e
        :pswitch_2be
        :pswitch_2be
        :pswitch_69e
        :pswitch_2be
        :pswitch_2be
        :pswitch_64e
        :pswitch_68e
        :pswitch_6af
        :pswitch_33a
        :pswitch_33f
        :pswitch_532
    .end packed-switch

    #@bb8
    .line 1243
    :pswitch_data_bb8
    .packed-switch 0x1
        :pswitch_a40
        :pswitch_a4b
        :pswitch_a56
        :pswitch_a61
    .end packed-switch

    #@bc4
    .line 1327
    :sswitch_data_bc4
    .sparse-switch
        0x12 -> :sswitch_ab4
        0x1d -> :sswitch_abc
        0x1e -> :sswitch_ac4
    .end sparse-switch

    #@bd2
    .line 1165
    :pswitch_data_bd2
    .packed-switch 0x1
        :pswitch_9a0
        :pswitch_9a9
        :pswitch_9b2
    .end packed-switch

    #@bdc
    .line 1205
    :pswitch_data_bdc
    .packed-switch 0x0
        :pswitch_a34
        :pswitch_a38
        :pswitch_a3c
    .end packed-switch
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 241
    sget-boolean v0, Landroid/widget/TextView;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Landroid/widget/TextView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    invoke-direct {p0}, Landroid/widget/TextView;->shouldSpeakPasswordsForAccessibility()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Landroid/widget/TextView;Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 241
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->sendBeforeTextChanged(Ljava/lang/CharSequence;III)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/widget/TextView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Landroid/widget/TextView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/TextView;)Ljava/util/concurrent/locks/ReentrantLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Landroid/widget/TextView;->mCurrentTextServicesLocaleLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/TextView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 241
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextServicesLocaleLocked()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/TextView;)Landroid/text/Layout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/TextView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Landroid/widget/TextView;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(I)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    invoke-static {p0}, Landroid/widget/TextView;->isPasswordInputType(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Landroid/widget/TextView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 241
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private applySingleLine(ZZZ)V
    .registers 5
    .parameter "singleLine"
    .parameter "applyTransformation"
    .parameter "changeMaxLines"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 7242
    iput-boolean p1, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@3
    .line 7243
    if-eqz p1, :cond_15

    #@5
    .line 7244
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setLines(I)V

    #@8
    .line 7245
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    #@b
    .line 7246
    if-eqz p2, :cond_14

    #@d
    .line 7247
    invoke-static {}, Landroid/text/method/SingleLineTransformationMethod;->getInstance()Landroid/text/method/SingleLineTransformationMethod;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@14
    .line 7258
    :cond_14
    :goto_14
    return-void

    #@15
    .line 7250
    :cond_15
    if-eqz p3, :cond_1d

    #@17
    .line 7251
    const v0, 0x7fffffff

    #@1a
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    #@1d
    .line 7253
    :cond_1d
    const/4 v0, 0x0

    #@1e
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    #@21
    .line 7254
    if-eqz p2, :cond_14

    #@23
    .line 7255
    const/4 v0, 0x0

    #@24
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@27
    goto :goto_14
.end method

.method private assumeLayout()V
    .registers 8

    #@0
    .prologue
    .line 6030
    iget v0, p0, Landroid/widget/TextView;->mRight:I

    #@2
    iget v3, p0, Landroid/widget/TextView;->mLeft:I

    #@4
    sub-int/2addr v0, v3

    #@5
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@8
    move-result v3

    #@9
    sub-int/2addr v0, v3

    #@a
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@d
    move-result v3

    #@e
    sub-int v1, v0, v3

    #@10
    .line 6032
    .local v1, width:I
    const/4 v0, 0x1

    #@11
    if-ge v1, v0, :cond_14

    #@13
    .line 6033
    const/4 v1, 0x0

    #@14
    .line 6036
    :cond_14
    move v2, v1

    #@15
    .line 6038
    .local v2, physicalWidth:I
    iget-boolean v0, p0, Landroid/widget/TextView;->mHorizontallyScrolling:Z

    #@17
    if-eqz v0, :cond_21

    #@19
    .line 6040
    invoke-virtual {p0}, Landroid/widget/TextView;->isHardwareAccelerated()Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_2c

    #@1f
    const/high16 v1, 0x10

    #@21
    .line 6044
    :cond_21
    :goto_21
    sget-object v3, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@23
    sget-object v4, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@25
    const/4 v6, 0x0

    #@26
    move-object v0, p0

    #@27
    move v5, v2

    #@28
    invoke-virtual/range {v0 .. v6}, Landroid/widget/TextView;->makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    #@2b
    .line 6046
    return-void

    #@2c
    .line 6040
    :cond_2c
    const/high16 v1, 0x1

    #@2e
    goto :goto_21
.end method

.method private bringTextIntoView()Z
    .registers 16

    #@0
    .prologue
    const/16 v14, 0x50

    #@2
    const/4 v11, 0x1

    #@3
    .line 6780
    invoke-direct {p0}, Landroid/widget/TextView;->isShowingHint()Z

    #@6
    move-result v12

    #@7
    if-eqz v12, :cond_7d

    #@9
    iget-object v4, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@b
    .line 6781
    .local v4, layout:Landroid/text/Layout;
    :goto_b
    const/4 v6, 0x0

    #@c
    .line 6782
    .local v6, line:I
    iget v12, p0, Landroid/widget/TextView;->mGravity:I

    #@e
    and-int/lit8 v12, v12, 0x70

    #@10
    if-ne v12, v14, :cond_18

    #@12
    .line 6783
    invoke-virtual {v4}, Landroid/text/Layout;->getLineCount()I

    #@15
    move-result v12

    #@16
    add-int/lit8 v6, v12, -0x1

    #@18
    .line 6786
    :cond_18
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    #@1b
    move-result-object v0

    #@1c
    .line 6787
    .local v0, a:Landroid/text/Layout$Alignment;
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@1f
    move-result v1

    #@20
    .line 6788
    .local v1, dir:I
    iget v12, p0, Landroid/widget/TextView;->mRight:I

    #@22
    iget v13, p0, Landroid/widget/TextView;->mLeft:I

    #@24
    sub-int/2addr v12, v13

    #@25
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@28
    move-result v13

    #@29
    sub-int/2addr v12, v13

    #@2a
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@2d
    move-result v13

    #@2e
    sub-int v2, v12, v13

    #@30
    .line 6789
    .local v2, hspace:I
    iget v12, p0, Landroid/widget/TextView;->mBottom:I

    #@32
    iget v13, p0, Landroid/widget/TextView;->mTop:I

    #@34
    sub-int/2addr v12, v13

    #@35
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@38
    move-result v13

    #@39
    sub-int/2addr v12, v13

    #@3a
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@3d
    move-result v13

    #@3e
    sub-int v10, v12, v13

    #@40
    .line 6790
    .local v10, vspace:I
    invoke-virtual {v4}, Landroid/text/Layout;->getHeight()I

    #@43
    move-result v3

    #@44
    .line 6795
    .local v3, ht:I
    sget-object v12, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@46
    if-ne v0, v12, :cond_83

    #@48
    .line 6796
    if-ne v1, v11, :cond_80

    #@4a
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@4c
    .line 6803
    :cond_4c
    :goto_4c
    sget-object v12, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    #@4e
    if-ne v0, v12, :cond_96

    #@50
    .line 6809
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineLeft(I)F

    #@53
    move-result v12

    #@54
    invoke-static {v12}, Landroid/util/FloatMath;->floor(F)F

    #@57
    move-result v12

    #@58
    float-to-int v5, v12

    #@59
    .line 6810
    .local v5, left:I
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineRight(I)F

    #@5c
    move-result v12

    #@5d
    invoke-static {v12}, Landroid/util/FloatMath;->ceil(F)F

    #@60
    move-result v12

    #@61
    float-to-int v7, v12

    #@62
    .line 6812
    .local v7, right:I
    sub-int v12, v7, v5

    #@64
    if-ge v12, v2, :cond_8f

    #@66
    .line 6813
    add-int v12, v7, v5

    #@68
    div-int/lit8 v12, v12, 0x2

    #@6a
    div-int/lit8 v13, v2, 0x2

    #@6c
    sub-int v8, v12, v13

    #@6e
    .line 6828
    .end local v5           #left:I
    .end local v7           #right:I
    .local v8, scrollx:I
    :goto_6e
    if-ge v3, v10, :cond_b0

    #@70
    .line 6829
    const/4 v9, 0x0

    #@71
    .line 6838
    .local v9, scrolly:I
    :goto_71
    iget v12, p0, Landroid/widget/TextView;->mScrollX:I

    #@73
    if-ne v8, v12, :cond_79

    #@75
    iget v12, p0, Landroid/widget/TextView;->mScrollY:I

    #@77
    if-eq v9, v12, :cond_bb

    #@79
    .line 6839
    :cond_79
    invoke-virtual {p0, v8, v9}, Landroid/widget/TextView;->scrollTo(II)V

    #@7c
    .line 6842
    :goto_7c
    return v11

    #@7d
    .line 6780
    .end local v0           #a:Landroid/text/Layout$Alignment;
    .end local v1           #dir:I
    .end local v2           #hspace:I
    .end local v3           #ht:I
    .end local v4           #layout:Landroid/text/Layout;
    .end local v6           #line:I
    .end local v8           #scrollx:I
    .end local v9           #scrolly:I
    .end local v10           #vspace:I
    :cond_7d
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@7f
    goto :goto_b

    #@80
    .line 6796
    .restart local v0       #a:Landroid/text/Layout$Alignment;
    .restart local v1       #dir:I
    .restart local v2       #hspace:I
    .restart local v3       #ht:I
    .restart local v4       #layout:Landroid/text/Layout;
    .restart local v6       #line:I
    .restart local v10       #vspace:I
    :cond_80
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@82
    goto :goto_4c

    #@83
    .line 6798
    :cond_83
    sget-object v12, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@85
    if-ne v0, v12, :cond_4c

    #@87
    .line 6799
    if-ne v1, v11, :cond_8c

    #@89
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@8b
    :goto_8b
    goto :goto_4c

    #@8c
    :cond_8c
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@8e
    goto :goto_8b

    #@8f
    .line 6815
    .restart local v5       #left:I
    .restart local v7       #right:I
    :cond_8f
    if-gez v1, :cond_94

    #@91
    .line 6816
    sub-int v8, v7, v2

    #@93
    .restart local v8       #scrollx:I
    goto :goto_6e

    #@94
    .line 6818
    .end local v8           #scrollx:I
    :cond_94
    move v8, v5

    #@95
    .restart local v8       #scrollx:I
    goto :goto_6e

    #@96
    .line 6821
    .end local v5           #left:I
    .end local v7           #right:I
    .end local v8           #scrollx:I
    :cond_96
    sget-object v12, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@98
    if-ne v0, v12, :cond_a6

    #@9a
    .line 6822
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineRight(I)F

    #@9d
    move-result v12

    #@9e
    invoke-static {v12}, Landroid/util/FloatMath;->ceil(F)F

    #@a1
    move-result v12

    #@a2
    float-to-int v7, v12

    #@a3
    .line 6823
    .restart local v7       #right:I
    sub-int v8, v7, v2

    #@a5
    .line 6824
    .restart local v8       #scrollx:I
    goto :goto_6e

    #@a6
    .line 6825
    .end local v7           #right:I
    .end local v8           #scrollx:I
    :cond_a6
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineLeft(I)F

    #@a9
    move-result v12

    #@aa
    invoke-static {v12}, Landroid/util/FloatMath;->floor(F)F

    #@ad
    move-result v12

    #@ae
    float-to-int v8, v12

    #@af
    .restart local v8       #scrollx:I
    goto :goto_6e

    #@b0
    .line 6831
    :cond_b0
    iget v12, p0, Landroid/widget/TextView;->mGravity:I

    #@b2
    and-int/lit8 v12, v12, 0x70

    #@b4
    if-ne v12, v14, :cond_b9

    #@b6
    .line 6832
    sub-int v9, v3, v10

    #@b8
    .restart local v9       #scrolly:I
    goto :goto_71

    #@b9
    .line 6834
    .end local v9           #scrolly:I
    :cond_b9
    const/4 v9, 0x0

    #@ba
    .restart local v9       #scrolly:I
    goto :goto_71

    #@bb
    .line 6842
    :cond_bb
    const/4 v11, 0x0

    #@bc
    goto :goto_7c
.end method

.method private canMarquee()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 7375
    iget v2, p0, Landroid/widget/TextView;->mRight:I

    #@3
    iget v3, p0, Landroid/widget/TextView;->mLeft:I

    #@5
    sub-int/2addr v2, v3

    #@6
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@9
    move-result v3

    #@a
    sub-int/2addr v2, v3

    #@b
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@e
    move-result v3

    #@f
    sub-int v0, v2, v3

    #@11
    .line 7376
    .local v0, width:I
    if-lez v0, :cond_32

    #@13
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@15
    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineWidth(I)F

    #@18
    move-result v2

    #@19
    int-to-float v3, v0

    #@1a
    cmpl-float v2, v2, v3

    #@1c
    if-gtz v2, :cond_31

    #@1e
    iget v2, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@20
    if-eqz v2, :cond_32

    #@22
    iget-object v2, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@24
    if-eqz v2, :cond_32

    #@26
    iget-object v2, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@28
    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineWidth(I)F

    #@2b
    move-result v2

    #@2c
    int-to-float v3, v0

    #@2d
    cmpl-float v2, v2, v3

    #@2f
    if-lez v2, :cond_32

    #@31
    :cond_31
    const/4 v1, 0x1

    #@32
    :cond_32
    return v1
.end method

.method private canSelectText()Z
    .registers 2

    #@0
    .prologue
    .line 8150
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_16

    #@8
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a
    if-eqz v0, :cond_16

    #@c
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@e
    invoke-virtual {v0}, Landroid/widget/Editor;->hasSelectionController()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method private checkForRelayout()V
    .registers 10

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v8, -0x2

    #@2
    .line 6693
    iget-object v0, p0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@4
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@6
    if-ne v0, v8, :cond_14

    #@8
    iget v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@a
    iget v3, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@c
    if-ne v0, v3, :cond_93

    #@e
    iget v0, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@10
    iget v3, p0, Landroid/widget/TextView;->mMinWidth:I

    #@12
    if-ne v0, v3, :cond_93

    #@14
    :cond_14
    iget-object v0, p0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@16
    if-eqz v0, :cond_1c

    #@18
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@1a
    if-eqz v0, :cond_93

    #@1c
    :cond_1c
    iget v0, p0, Landroid/widget/TextView;->mRight:I

    #@1e
    iget v3, p0, Landroid/widget/TextView;->mLeft:I

    #@20
    sub-int/2addr v0, v3

    #@21
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@24
    move-result v3

    #@25
    sub-int/2addr v0, v3

    #@26
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@29
    move-result v3

    #@2a
    sub-int/2addr v0, v3

    #@2b
    if-lez v0, :cond_93

    #@2d
    .line 6699
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2f
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    #@32
    move-result v7

    #@33
    .line 6700
    .local v7, oldht:I
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@35
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    #@38
    move-result v1

    #@39
    .line 6701
    .local v1, want:I
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@3b
    if-nez v0, :cond_6d

    #@3d
    move v2, v6

    #@3e
    .line 6708
    .local v2, hintWant:I
    :goto_3e
    sget-object v3, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@40
    sget-object v4, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@42
    iget v0, p0, Landroid/widget/TextView;->mRight:I

    #@44
    iget v5, p0, Landroid/widget/TextView;->mLeft:I

    #@46
    sub-int/2addr v0, v5

    #@47
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@4a
    move-result v5

    #@4b
    sub-int/2addr v0, v5

    #@4c
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@4f
    move-result v5

    #@50
    sub-int v5, v0, v5

    #@52
    move-object v0, p0

    #@53
    invoke-virtual/range {v0 .. v6}, Landroid/widget/TextView;->makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    #@56
    .line 6712
    iget-object v0, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@58
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@5a
    if-eq v0, v3, :cond_8c

    #@5c
    .line 6714
    iget-object v0, p0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@5e
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@60
    if-eq v0, v8, :cond_74

    #@62
    iget-object v0, p0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@64
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@66
    const/4 v3, -0x1

    #@67
    if-eq v0, v3, :cond_74

    #@69
    .line 6716
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@6c
    .line 6743
    .end local v1           #want:I
    .end local v2           #hintWant:I
    .end local v7           #oldht:I
    :cond_6c
    :goto_6c
    return-void

    #@6d
    .line 6701
    .restart local v1       #want:I
    .restart local v7       #oldht:I
    :cond_6d
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@6f
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    #@72
    move-result v2

    #@73
    goto :goto_3e

    #@74
    .line 6722
    .restart local v2       #hintWant:I
    :cond_74
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@76
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    #@79
    move-result v0

    #@7a
    if-ne v0, v7, :cond_8c

    #@7c
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@7e
    if-eqz v0, :cond_88

    #@80
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@82
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    #@85
    move-result v0

    #@86
    if-ne v0, v7, :cond_8c

    #@88
    .line 6724
    :cond_88
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@8b
    goto :goto_6c

    #@8c
    .line 6731
    :cond_8c
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8f
    .line 6732
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@92
    goto :goto_6c

    #@93
    .line 6736
    .end local v1           #want:I
    .end local v2           #hintWant:I
    .end local v7           #oldht:I
    :cond_93
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@96
    .line 6737
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@99
    .line 6738
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@9c
    .line 6739
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@9e
    if-eqz v0, :cond_6c

    #@a0
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a2
    if-eqz v0, :cond_6c

    #@a4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a6
    invoke-virtual {v0}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@a9
    move-result v0

    #@aa
    if-eqz v0, :cond_6c

    #@ac
    .line 6740
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@ae
    invoke-virtual {v0}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@b1
    goto :goto_6c
.end method

.method private checkForResize()V
    .registers 5

    #@0
    .prologue
    const/4 v3, -0x2

    #@1
    .line 6652
    const/4 v1, 0x0

    #@2
    .line 6654
    .local v1, sizeChanged:Z
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@4
    if-eqz v2, :cond_21

    #@6
    .line 6656
    iget-object v2, p0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@8
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@a
    if-ne v2, v3, :cond_10

    #@c
    .line 6657
    const/4 v1, 0x1

    #@d
    .line 6658
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@10
    .line 6662
    :cond_10
    iget-object v2, p0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@12
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@14
    if-ne v2, v3, :cond_27

    #@16
    .line 6663
    invoke-direct {p0}, Landroid/widget/TextView;->getDesiredHeight()I

    #@19
    move-result v0

    #@1a
    .line 6665
    .local v0, desiredHeight:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    #@1d
    move-result v2

    #@1e
    if-eq v0, v2, :cond_21

    #@20
    .line 6666
    const/4 v1, 0x1

    #@21
    .line 6679
    .end local v0           #desiredHeight:I
    :cond_21
    :goto_21
    if-eqz v1, :cond_26

    #@23
    .line 6680
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@26
    .line 6683
    :cond_26
    return-void

    #@27
    .line 6668
    :cond_27
    iget-object v2, p0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@29
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2b
    const/4 v3, -0x1

    #@2c
    if-ne v2, v3, :cond_21

    #@2e
    .line 6669
    iget v2, p0, Landroid/widget/TextView;->mDesiredHeightAtMeasure:I

    #@30
    if-ltz v2, :cond_21

    #@32
    .line 6670
    invoke-direct {p0}, Landroid/widget/TextView;->getDesiredHeight()I

    #@35
    move-result v0

    #@36
    .line 6672
    .restart local v0       #desiredHeight:I
    iget v2, p0, Landroid/widget/TextView;->mDesiredHeightAtMeasure:I

    #@38
    if-eq v0, v2, :cond_21

    #@3a
    .line 6673
    const/4 v1, 0x1

    #@3b
    goto :goto_21
.end method

.method private compressText(F)Z
    .registers 9
    .parameter "width"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    const/high16 v5, 0x3f80

    #@5
    .line 6334
    invoke-virtual {p0}, Landroid/widget/TextView;->isHardwareAccelerated()Z

    #@8
    move-result v4

    #@9
    if-eqz v4, :cond_c

    #@b
    .line 6352
    :cond_b
    :goto_b
    return v2

    #@c
    .line 6337
    :cond_c
    cmpl-float v4, p1, v6

    #@e
    if-lez v4, :cond_b

    #@10
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@12
    if-eqz v4, :cond_b

    #@14
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    #@17
    move-result v4

    #@18
    if-ne v4, v3, :cond_b

    #@1a
    iget-boolean v4, p0, Landroid/widget/TextView;->mUserSetTextScaleX:Z

    #@1c
    if-nez v4, :cond_b

    #@1e
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@20
    invoke-virtual {v4}, Landroid/text/TextPaint;->getTextScaleX()F

    #@23
    move-result v4

    #@24
    cmpl-float v4, v4, v5

    #@26
    if-nez v4, :cond_b

    #@28
    .line 6339
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2a
    invoke-virtual {v4, v2}, Landroid/text/Layout;->getLineWidth(I)F

    #@2d
    move-result v1

    #@2e
    .line 6340
    .local v1, textWidth:F
    add-float v4, v1, v5

    #@30
    sub-float/2addr v4, p1

    #@31
    div-float v0, v4, p1

    #@33
    .line 6341
    .local v0, overflow:F
    cmpl-float v4, v0, v6

    #@35
    if-lez v4, :cond_b

    #@37
    const v4, 0x3d8f5c29

    #@3a
    cmpg-float v4, v0, v4

    #@3c
    if-gtz v4, :cond_b

    #@3e
    .line 6342
    iget-object v2, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@40
    sub-float v4, v5, v0

    #@42
    const v5, 0x3ba3d70a

    #@45
    sub-float/2addr v4, v5

    #@46
    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTextScaleX(F)V

    #@49
    .line 6343
    new-instance v2, Landroid/widget/TextView$2;

    #@4b
    invoke-direct {v2, p0}, Landroid/widget/TextView$2;-><init>(Landroid/widget/TextView;)V

    #@4e
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    #@51
    move v2, v3

    #@52
    .line 6348
    goto :goto_b
.end method

.method private convertFromViewportToContentCoordinates(Landroid/graphics/Rect;)V
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 7108
    invoke-virtual {p0}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@3
    move-result v0

    #@4
    .line 7109
    .local v0, horizontalOffset:I
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@6
    add-int/2addr v2, v0

    #@7
    iput v2, p1, Landroid/graphics/Rect;->left:I

    #@9
    .line 7110
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@b
    add-int/2addr v2, v0

    #@c
    iput v2, p1, Landroid/graphics/Rect;->right:I

    #@e
    .line 7112
    invoke-virtual {p0}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@11
    move-result v1

    #@12
    .line 7113
    .local v1, verticalOffset:I
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@14
    add-int/2addr v2, v1

    #@15
    iput v2, p1, Landroid/graphics/Rect;->top:I

    #@17
    .line 7114
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    #@19
    add-int/2addr v2, v1

    #@1a
    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    #@1c
    .line 7115
    return-void
.end method

.method private createEditorIfNeeded()V
    .registers 3

    #@0
    .prologue
    .line 8996
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_1e

    #@4
    .line 8997
    new-instance v0, Landroid/widget/Editor;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/Editor;-><init>(Landroid/widget/TextView;)V

    #@9
    iput-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@b
    .line 9000
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@d
    if-eqz v0, :cond_1e

    #@f
    .line 9001
    iget-object v0, p0, Landroid/widget/TextView;->mPasteHandler:Landroid/os/Handler;

    #@11
    if-nez v0, :cond_1e

    #@13
    .line 9002
    new-instance v0, Landroid/widget/TextView$4;

    #@15
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@18
    move-result-object v1

    #@19
    invoke-direct {v0, p0, v1}, Landroid/widget/TextView$4;-><init>(Landroid/widget/TextView;Landroid/os/Looper;)V

    #@1c
    iput-object v0, p0, Landroid/widget/TextView;->mPasteHandler:Landroid/os/Handler;

    #@1e
    .line 9017
    :cond_1e
    return-void
.end method

.method private static desired(Landroid/text/Layout;)I
    .registers 7
    .parameter "layout"

    #@0
    .prologue
    .line 6356
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    #@3
    move-result v2

    #@4
    .line 6357
    .local v2, n:I
    invoke-virtual {p0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    #@7
    move-result-object v3

    #@8
    .line 6358
    .local v3, text:Ljava/lang/CharSequence;
    const/4 v1, 0x0

    #@9
    .line 6363
    .local v1, max:F
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    add-int/lit8 v4, v2, -0x1

    #@c
    if-ge v0, v4, :cond_21

    #@e
    .line 6364
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineEnd(I)I

    #@11
    move-result v4

    #@12
    add-int/lit8 v4, v4, -0x1

    #@14
    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@17
    move-result v4

    #@18
    const/16 v5, 0xa

    #@1a
    if-eq v4, v5, :cond_1e

    #@1c
    .line 6365
    const/4 v4, -0x1

    #@1d
    .line 6372
    :goto_1d
    return v4

    #@1e
    .line 6363
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_a

    #@21
    .line 6368
    :cond_21
    const/4 v0, 0x0

    #@22
    :goto_22
    if-ge v0, v2, :cond_2f

    #@24
    .line 6369
    invoke-virtual {p0, v0}, Landroid/text/Layout;->getLineWidth(I)F

    #@27
    move-result v4

    #@28
    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    #@2b
    move-result v1

    #@2c
    .line 6368
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_22

    #@2f
    .line 6372
    :cond_2f
    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    #@32
    move-result v4

    #@33
    float-to-int v4, v4

    #@34
    goto :goto_1d
.end method

.method private doKeyDown(ILandroid/view/KeyEvent;Landroid/view/KeyEvent;)I
    .registers 11
    .parameter "keyCode"
    .parameter "event"
    .parameter "otherEvent"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 5556
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_b

    #@9
    move v2, v3

    #@a
    .line 5673
    :goto_a
    return v2

    #@b
    .line 5560
    :cond_b
    sparse-switch p1, :sswitch_data_104

    #@e
    .line 5617
    :cond_e
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@10
    if-eqz v2, :cond_d4

    #@12
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@14
    iget-object v2, v2, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@16
    if-eqz v2, :cond_d4

    #@18
    .line 5618
    invoke-virtual {p0}, Landroid/widget/TextView;->resetErrorChangedFlag()V

    #@1b
    .line 5620
    const/4 v0, 0x1

    #@1c
    .line 5621
    .local v0, doDown:Z
    if-eqz p3, :cond_b8

    #@1e
    .line 5623
    :try_start_1e
    invoke-virtual {p0}, Landroid/widget/TextView;->beginBatchEdit()V

    #@21
    .line 5624
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@23
    iget-object v6, v2, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@25
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@27
    check-cast v2, Landroid/text/Editable;

    #@29
    invoke-interface {v6, p0, v2, p3}, Landroid/text/method/KeyListener;->onKeyOther(Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z

    #@2c
    move-result v1

    #@2d
    .line 5626
    .local v1, handled:Z
    invoke-virtual {p0}, Landroid/widget/TextView;->hideErrorIfUnchanged()V
    :try_end_30
    .catchall {:try_start_1e .. :try_end_30} :catchall_af
    .catch Ljava/lang/AbstractMethodError; {:try_start_1e .. :try_end_30} :catch_b4

    #@30
    .line 5627
    const/4 v0, 0x0

    #@31
    .line 5628
    if-eqz v1, :cond_b5

    #@33
    .line 5635
    invoke-virtual {p0}, Landroid/widget/TextView;->endBatchEdit()V

    #@36
    move v2, v4

    #@37
    .line 5629
    goto :goto_a

    #@38
    .line 5562
    .end local v0           #doDown:Z
    .end local v1           #handled:Z
    :sswitch_38
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_e

    #@3e
    .line 5567
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@40
    if-eqz v2, :cond_64

    #@42
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@44
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@46
    if-eqz v2, :cond_64

    #@48
    .line 5570
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4a
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@4c
    iget-object v2, v2, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@4e
    if-eqz v2, :cond_64

    #@50
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@52
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@54
    iget-object v2, v2, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@56
    invoke-interface {v2, p0, v3, p2}, Landroid/widget/TextView$OnEditorActionListener;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_64

    #@5c
    .line 5573
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5e
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@60
    iput-boolean v5, v2, Landroid/widget/Editor$InputContentType;->enterDown:Z

    #@62
    move v2, v4

    #@63
    .line 5575
    goto :goto_a

    #@64
    .line 5582
    :cond_64
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    #@67
    move-result v2

    #@68
    and-int/lit8 v2, v2, 0x10

    #@6a
    if-nez v2, :cond_72

    #@6c
    invoke-direct {p0}, Landroid/widget/TextView;->shouldAdvanceFocusOnEnter()Z

    #@6f
    move-result v2

    #@70
    if-eqz v2, :cond_e

    #@72
    .line 5584
    :cond_72
    invoke-virtual {p0}, Landroid/widget/TextView;->hasOnClickListeners()Z

    #@75
    move-result v2

    #@76
    if-eqz v2, :cond_7a

    #@78
    move v2, v3

    #@79
    .line 5585
    goto :goto_a

    #@7a
    :cond_7a
    move v2, v4

    #@7b
    .line 5587
    goto :goto_a

    #@7c
    .line 5593
    :sswitch_7c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_e

    #@82
    .line 5594
    invoke-direct {p0}, Landroid/widget/TextView;->shouldAdvanceFocusOnEnter()Z

    #@85
    move-result v2

    #@86
    if-eqz v2, :cond_e

    #@88
    move v2, v3

    #@89
    .line 5595
    goto :goto_a

    #@8a
    .line 5601
    :sswitch_8a
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@8d
    move-result v2

    #@8e
    if-nez v2, :cond_96

    #@90
    invoke-virtual {p2, v5}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@93
    move-result v2

    #@94
    if-eqz v2, :cond_e

    #@96
    .line 5602
    :cond_96
    invoke-direct {p0}, Landroid/widget/TextView;->shouldAdvanceFocusOnTab()Z

    #@99
    move-result v2

    #@9a
    if-eqz v2, :cond_e

    #@9c
    move v2, v3

    #@9d
    .line 5603
    goto/16 :goto_a

    #@9f
    .line 5610
    :sswitch_9f
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a1
    if-eqz v2, :cond_e

    #@a3
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a5
    iget-object v2, v2, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@a7
    if-eqz v2, :cond_e

    #@a9
    .line 5611
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@ac
    move v2, v4

    #@ad
    .line 5612
    goto/16 :goto_a

    #@af
    .line 5635
    .restart local v0       #doDown:Z
    :catchall_af
    move-exception v2

    #@b0
    invoke-virtual {p0}, Landroid/widget/TextView;->endBatchEdit()V

    #@b3
    throw v2

    #@b4
    .line 5631
    :catch_b4
    move-exception v2

    #@b5
    .line 5635
    :cond_b5
    invoke-virtual {p0}, Landroid/widget/TextView;->endBatchEdit()V

    #@b8
    .line 5639
    :cond_b8
    if-eqz v0, :cond_d4

    #@ba
    .line 5640
    invoke-virtual {p0}, Landroid/widget/TextView;->beginBatchEdit()V

    #@bd
    .line 5641
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@bf
    iget-object v6, v2, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@c1
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@c3
    check-cast v2, Landroid/text/Editable;

    #@c5
    invoke-interface {v6, p0, v2, p1, p2}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@c8
    move-result v1

    #@c9
    .line 5643
    .restart local v1       #handled:Z
    invoke-virtual {p0}, Landroid/widget/TextView;->endBatchEdit()V

    #@cc
    .line 5644
    invoke-virtual {p0}, Landroid/widget/TextView;->hideErrorIfUnchanged()V

    #@cf
    .line 5645
    if-eqz v1, :cond_d4

    #@d1
    move v2, v5

    #@d2
    goto/16 :goto_a

    #@d4
    .line 5652
    .end local v0           #doDown:Z
    .end local v1           #handled:Z
    :cond_d4
    iget-object v2, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@d6
    if-eqz v2, :cond_101

    #@d8
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@da
    if-eqz v2, :cond_101

    #@dc
    .line 5653
    const/4 v0, 0x1

    #@dd
    .line 5654
    .restart local v0       #doDown:Z
    if-eqz p3, :cond_f0

    #@df
    .line 5656
    :try_start_df
    iget-object v5, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@e1
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@e3
    check-cast v2, Landroid/text/Spannable;

    #@e5
    invoke-interface {v5, p0, v2, p3}, Landroid/text/method/MovementMethod;->onKeyOther(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z
    :try_end_e8
    .catch Ljava/lang/AbstractMethodError; {:try_start_df .. :try_end_e8} :catch_ef

    #@e8
    move-result v1

    #@e9
    .line 5658
    .restart local v1       #handled:Z
    const/4 v0, 0x0

    #@ea
    .line 5659
    if-eqz v1, :cond_f0

    #@ec
    move v2, v4

    #@ed
    .line 5660
    goto/16 :goto_a

    #@ef
    .line 5662
    .end local v1           #handled:Z
    :catch_ef
    move-exception v2

    #@f0
    .line 5667
    :cond_f0
    if-eqz v0, :cond_101

    #@f2
    .line 5668
    iget-object v4, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@f4
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@f6
    check-cast v2, Landroid/text/Spannable;

    #@f8
    invoke-interface {v4, p0, v2, p1, p2}, Landroid/text/method/MovementMethod;->onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@fb
    move-result v2

    #@fc
    if-eqz v2, :cond_101

    #@fe
    .line 5669
    const/4 v2, 0x2

    #@ff
    goto/16 :goto_a

    #@101
    .end local v0           #doDown:Z
    :cond_101
    move v2, v3

    #@102
    .line 5673
    goto/16 :goto_a

    #@104
    .line 5560
    :sswitch_data_104
    .sparse-switch
        0x4 -> :sswitch_9f
        0x17 -> :sswitch_7c
        0x3d -> :sswitch_8a
        0x42 -> :sswitch_38
    .end sparse-switch
.end method

.method private fixFocusableAndClickableSettings()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1737
    iget-object v0, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@4
    if-nez v0, :cond_10

    #@6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    if-eqz v0, :cond_1a

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@e
    if-eqz v0, :cond_1a

    #@10
    .line 1738
    :cond_10
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    #@13
    .line 1739
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    #@16
    .line 1740
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setLongClickable(Z)V

    #@19
    .line 1746
    :goto_19
    return-void

    #@1a
    .line 1742
    :cond_1a
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    #@1d
    .line 1743
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    #@20
    .line 1744
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setLongClickable(Z)V

    #@23
    goto :goto_19
.end method

.method private getBottomVerticalOffset(Z)I
    .registers 9
    .parameter "forceNormal"

    #@0
    .prologue
    .line 4613
    const/4 v4, 0x0

    #@1
    .line 4614
    .local v4, voffset:I
    iget v5, p0, Landroid/widget/TextView;->mGravity:I

    #@3
    and-int/lit8 v1, v5, 0x70

    #@5
    .line 4616
    .local v1, gravity:I
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@7
    .line 4617
    .local v2, l:Landroid/text/Layout;
    if-nez p1, :cond_17

    #@9
    iget-object v5, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@b
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@e
    move-result v5

    #@f
    if-nez v5, :cond_17

    #@11
    iget-object v5, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@13
    if-eqz v5, :cond_17

    #@15
    .line 4618
    iget-object v2, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@17
    .line 4621
    :cond_17
    const/16 v5, 0x50

    #@19
    if-eq v1, v5, :cond_3a

    #@1b
    .line 4624
    iget-object v5, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@1d
    if-ne v2, v5, :cond_3b

    #@1f
    .line 4625
    invoke-virtual {p0}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@22
    move-result v5

    #@23
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@26
    move-result v6

    #@27
    sub-int/2addr v5, v6

    #@28
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@2b
    move-result v6

    #@2c
    sub-int v0, v5, v6

    #@2e
    .line 4631
    .local v0, boxht:I
    :goto_2e
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    #@31
    move-result v3

    #@32
    .line 4633
    .local v3, textht:I
    if-ge v3, v0, :cond_3a

    #@34
    .line 4634
    const/16 v5, 0x30

    #@36
    if-ne v1, v5, :cond_4b

    #@38
    .line 4635
    sub-int v4, v0, v3

    #@3a
    .line 4640
    .end local v0           #boxht:I
    .end local v3           #textht:I
    :cond_3a
    :goto_3a
    return v4

    #@3b
    .line 4628
    :cond_3b
    invoke-virtual {p0}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@3e
    move-result v5

    #@3f
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@42
    move-result v6

    #@43
    sub-int/2addr v5, v6

    #@44
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@47
    move-result v6

    #@48
    sub-int v0, v5, v6

    #@4a
    .restart local v0       #boxht:I
    goto :goto_2e

    #@4b
    .line 4637
    .restart local v3       #textht:I
    :cond_4b
    sub-int v5, v0, v3

    #@4d
    shr-int/lit8 v4, v5, 0x1

    #@4f
    goto :goto_3a
.end method

.method private getDesiredHeight()I
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 6589
    iget-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@3
    invoke-direct {p0, v1, v0}, Landroid/widget/TextView;->getDesiredHeight(Landroid/text/Layout;Z)I

    #@6
    move-result v1

    #@7
    iget-object v2, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@9
    iget-object v3, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@b
    if-eqz v3, :cond_16

    #@d
    :goto_d
    invoke-direct {p0, v2, v0}, Landroid/widget/TextView;->getDesiredHeight(Landroid/text/Layout;Z)I

    #@10
    move-result v0

    #@11
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    #@14
    move-result v0

    #@15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_d
.end method

.method private getDesiredHeight(Landroid/text/Layout;Z)I
    .registers 10
    .parameter "layout"
    .parameter "cap"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 6595
    if-nez p1, :cond_5

    #@3
    .line 6596
    const/4 v0, 0x0

    #@4
    .line 6644
    :goto_4
    return v0

    #@5
    .line 6599
    :cond_5
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    #@8
    move-result v2

    #@9
    .line 6600
    .local v2, linecount:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@c
    move-result v4

    #@d
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@10
    move-result v5

    #@11
    add-int v3, v4, v5

    #@13
    .line 6601
    .local v3, pad:I
    invoke-virtual {p1, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@16
    move-result v0

    #@17
    .line 6603
    .local v0, desired:I
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@19
    .line 6604
    .local v1, dr:Landroid/widget/TextView$Drawables;
    if-eqz v1, :cond_27

    #@1b
    .line 6605
    iget v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@1d
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@20
    move-result v0

    #@21
    .line 6606
    iget v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@23
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@26
    move-result v0

    #@27
    .line 6609
    :cond_27
    add-int/2addr v0, v3

    #@28
    .line 6611
    iget v4, p0, Landroid/widget/TextView;->mMaxMode:I

    #@2a
    if-ne v4, v6, :cond_63

    #@2c
    .line 6616
    if-eqz p2, :cond_49

    #@2e
    .line 6617
    iget v4, p0, Landroid/widget/TextView;->mMaximum:I

    #@30
    if-le v2, v4, :cond_49

    #@32
    .line 6618
    iget v4, p0, Landroid/widget/TextView;->mMaximum:I

    #@34
    invoke-virtual {p1, v4}, Landroid/text/Layout;->getLineTop(I)I

    #@37
    move-result v0

    #@38
    .line 6620
    if-eqz v1, :cond_46

    #@3a
    .line 6621
    iget v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@3c
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@3f
    move-result v0

    #@40
    .line 6622
    iget v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@42
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@45
    move-result v0

    #@46
    .line 6625
    :cond_46
    add-int/2addr v0, v3

    #@47
    .line 6626
    iget v2, p0, Landroid/widget/TextView;->mMaximum:I

    #@49
    .line 6633
    :cond_49
    :goto_49
    iget v4, p0, Landroid/widget/TextView;->mMinMode:I

    #@4b
    if-ne v4, v6, :cond_6a

    #@4d
    .line 6634
    iget v4, p0, Landroid/widget/TextView;->mMinimum:I

    #@4f
    if-ge v2, v4, :cond_5a

    #@51
    .line 6635
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    #@54
    move-result v4

    #@55
    iget v5, p0, Landroid/widget/TextView;->mMinimum:I

    #@57
    sub-int/2addr v5, v2

    #@58
    mul-int/2addr v4, v5

    #@59
    add-int/2addr v0, v4

    #@5a
    .line 6642
    :cond_5a
    :goto_5a
    invoke-virtual {p0}, Landroid/widget/TextView;->getSuggestedMinimumHeight()I

    #@5d
    move-result v4

    #@5e
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@61
    move-result v0

    #@62
    .line 6644
    goto :goto_4

    #@63
    .line 6630
    :cond_63
    iget v4, p0, Landroid/widget/TextView;->mMaximum:I

    #@65
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@68
    move-result v0

    #@69
    goto :goto_49

    #@6a
    .line 6638
    :cond_6a
    iget v4, p0, Landroid/widget/TextView;->mMinimum:I

    #@6c
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    #@6f
    move-result v0

    #@70
    goto :goto_5a
.end method

.method private getInterestingRect(Landroid/graphics/Rect;I)V
    .registers 5
    .parameter "r"
    .parameter "line"

    #@0
    .prologue
    .line 7098
    invoke-direct {p0, p1}, Landroid/widget/TextView;->convertFromViewportToContentCoordinates(Landroid/graphics/Rect;)V

    #@3
    .line 7103
    if-nez p2, :cond_e

    #@5
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@7
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@a
    move-result v1

    #@b
    sub-int/2addr v0, v1

    #@c
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@e
    .line 7104
    :cond_e
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@10
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    #@13
    move-result v0

    #@14
    add-int/lit8 v0, v0, -0x1

    #@16
    if-ne p2, v0, :cond_21

    #@18
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@1a
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@1d
    move-result v1

    #@1e
    add-int/2addr v0, v1

    #@1f
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@21
    .line 7105
    :cond_21
    return-void
.end method

.method private getLayoutAlignment()Landroid/text/Layout$Alignment;
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 6059
    iget-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@3
    if-nez v0, :cond_14

    #@5
    .line 6060
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextAlignment()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/widget/TextView;->mResolvedTextAlignment:I

    #@b
    .line 6061
    iget v0, p0, Landroid/widget/TextView;->mResolvedTextAlignment:I

    #@d
    packed-switch v0, :pswitch_data_6a

    #@10
    .line 6105
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@12
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@14
    .line 6109
    :cond_14
    :goto_14
    iget-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@16
    return-object v0

    #@17
    .line 6063
    :pswitch_17
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@19
    const v1, 0x800007

    #@1c
    and-int/2addr v0, v1

    #@1d
    sparse-switch v0, :sswitch_data_7a

    #@20
    .line 6080
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@22
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@24
    goto :goto_14

    #@25
    .line 6065
    :sswitch_25
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@27
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@29
    goto :goto_14

    #@2a
    .line 6068
    :sswitch_2a
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@2c
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@2e
    goto :goto_14

    #@2f
    .line 6071
    :sswitch_2f
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@31
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@33
    goto :goto_14

    #@34
    .line 6074
    :sswitch_34
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@36
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@38
    goto :goto_14

    #@39
    .line 6077
    :sswitch_39
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    #@3b
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@3d
    goto :goto_14

    #@3e
    .line 6085
    :pswitch_3e
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@40
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@42
    goto :goto_14

    #@43
    .line 6088
    :pswitch_43
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    #@45
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@47
    goto :goto_14

    #@48
    .line 6091
    :pswitch_48
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    #@4a
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@4c
    goto :goto_14

    #@4d
    .line 6094
    :pswitch_4d
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@50
    move-result v0

    #@51
    if-ne v0, v1, :cond_58

    #@53
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@55
    :goto_55
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@57
    goto :goto_14

    #@58
    :cond_58
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@5a
    goto :goto_55

    #@5b
    .line 6098
    :pswitch_5b
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@5e
    move-result v0

    #@5f
    if-ne v0, v1, :cond_66

    #@61
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    #@63
    :goto_63
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@65
    goto :goto_14

    #@66
    :cond_66
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    #@68
    goto :goto_63

    #@69
    .line 6061
    nop

    #@6a
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_17
        :pswitch_3e
        :pswitch_43
        :pswitch_48
        :pswitch_4d
        :pswitch_5b
    .end packed-switch

    #@7a
    .line 6063
    :sswitch_data_7a
    .sparse-switch
        0x1 -> :sswitch_39
        0x3 -> :sswitch_2f
        0x5 -> :sswitch_34
        0x800003 -> :sswitch_25
        0x800005 -> :sswitch_2a
    .end sparse-switch
.end method

.method private getOffsetAtCoordinate(IF)I
    .registers 4
    .parameter "line"
    .parameter "x"

    #@0
    .prologue
    .line 8824
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->convertToLocalHorizontalCoordinate(F)F

    #@3
    move-result p2

    #@4
    .line 8825
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1, p2}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public static getTextColor(Landroid/content/Context;Landroid/content/res/TypedArray;I)I
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "def"

    #@0
    .prologue
    .line 8104
    invoke-static {p0, p1}, Landroid/widget/TextView;->getTextColors(Landroid/content/Context;Landroid/content/res/TypedArray;)Landroid/content/res/ColorStateList;

    #@3
    move-result-object v0

    #@4
    .line 8106
    .local v0, colors:Landroid/content/res/ColorStateList;
    if-nez v0, :cond_7

    #@6
    .line 8109
    .end local p2
    :goto_6
    return p2

    #@7
    .restart local p2
    :cond_7
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    #@a
    move-result p2

    #@b
    goto :goto_6
.end method

.method public static getTextColors(Landroid/content/Context;Landroid/content/res/TypedArray;)Landroid/content/res/ColorStateList;
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 8076
    const/4 v3, 0x5

    #@2
    invoke-virtual {p1, v3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@5
    move-result-object v2

    #@6
    .line 8079
    .local v2, colors:Landroid/content/res/ColorStateList;
    if-nez v2, :cond_1d

    #@8
    .line 8080
    const/4 v3, 0x1

    #@9
    invoke-virtual {p1, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@c
    move-result v0

    #@d
    .line 8082
    .local v0, ap:I
    if-eq v0, v4, :cond_1d

    #@f
    .line 8084
    sget-object v3, Lcom/android/internal/R$styleable;->TextAppearance:[I

    #@11
    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@14
    move-result-object v1

    #@15
    .line 8086
    .local v1, appearance:Landroid/content/res/TypedArray;
    const/4 v3, 0x3

    #@16
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@19
    move-result-object v2

    #@1a
    .line 8088
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@1d
    .line 8092
    .end local v0           #ap:I
    .end local v1           #appearance:Landroid/content/res/TypedArray;
    :cond_1d
    return-object v2
.end method

.method private getUpdatedHighlightPath()Landroid/graphics/Path;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 5063
    const/4 v0, 0x0

    #@2
    .line 5064
    .local v0, highlight:Landroid/graphics/Path;
    iget-object v1, p0, Landroid/widget/TextView;->mHighlightPaint:Landroid/graphics/Paint;

    #@4
    .line 5066
    .local v1, highlightPaint:Landroid/graphics/Paint;
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@7
    move-result v3

    #@8
    .line 5067
    .local v3, selStart:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@b
    move-result v2

    #@c
    .line 5068
    .local v2, selEnd:I
    iget-object v4, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@e
    if-eqz v4, :cond_87

    #@10
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_1c

    #@16
    invoke-virtual {p0}, Landroid/widget/TextView;->isPressed()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_87

    #@1c
    :cond_1c
    if-ltz v3, :cond_87

    #@1e
    .line 5069
    if-ne v3, v2, :cond_88

    #@20
    .line 5070
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@22
    if-eqz v4, :cond_6e

    #@24
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@26
    invoke-virtual {v4}, Landroid/widget/Editor;->isCursorVisible()Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_6e

    #@2c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2f
    move-result-wide v4

    #@30
    iget-object v6, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@32
    iget-wide v6, v6, Landroid/widget/Editor;->mShowCursor:J

    #@34
    sub-long/2addr v4, v6

    #@35
    const-wide/16 v6, 0x3e8

    #@37
    rem-long/2addr v4, v6

    #@38
    const-wide/16 v6, 0x1f4

    #@3a
    cmp-long v4, v4, v6

    #@3c
    if-gez v4, :cond_6e

    #@3e
    .line 5073
    iget-boolean v4, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@40
    if-eqz v4, :cond_62

    #@42
    .line 5074
    iget-object v4, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@44
    if-nez v4, :cond_4d

    #@46
    new-instance v4, Landroid/graphics/Path;

    #@48
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    #@4b
    iput-object v4, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@4d
    .line 5075
    :cond_4d
    iget-object v4, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@4f
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    #@52
    .line 5076
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@54
    iget-object v5, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@56
    iget-object v6, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@58
    invoke-virtual {v4, v3, v5, v6}, Landroid/text/Layout;->getCursorPath(ILandroid/graphics/Path;Ljava/lang/CharSequence;)V

    #@5b
    .line 5077
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5d
    invoke-virtual {v4}, Landroid/widget/Editor;->updateCursorsPositions()V

    #@60
    .line 5078
    iput-boolean v8, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@62
    .line 5082
    :cond_62
    iget v4, p0, Landroid/widget/TextView;->mCurTextColor:I

    #@64
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@67
    .line 5083
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@69
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@6c
    .line 5084
    iget-object v0, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@6e
    .line 5087
    :cond_6e
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@70
    if-eqz v4, :cond_87

    #@72
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@74
    if-eqz v4, :cond_87

    #@76
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@78
    iget-boolean v4, v4, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@7a
    if-eqz v4, :cond_87

    #@7c
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7e
    invoke-virtual {v4}, Landroid/widget/Editor;->isSelectionDragging()Z

    #@81
    move-result v4

    #@82
    if-nez v4, :cond_87

    #@84
    .line 5089
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@87
    .line 5108
    :cond_87
    :goto_87
    return-object v0

    #@88
    .line 5091
    :cond_88
    iget-boolean v4, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@8a
    if-eqz v4, :cond_a5

    #@8c
    .line 5092
    iget-object v4, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@8e
    if-nez v4, :cond_97

    #@90
    new-instance v4, Landroid/graphics/Path;

    #@92
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    #@95
    iput-object v4, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@97
    .line 5093
    :cond_97
    iget-object v4, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@99
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    #@9c
    .line 5094
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@9e
    iget-object v5, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@a0
    invoke-virtual {v4, v3, v2, v5}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    #@a3
    .line 5095
    iput-boolean v8, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@a5
    .line 5099
    :cond_a5
    iget v4, p0, Landroid/widget/TextView;->mHighlightColor:I

    #@a7
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@aa
    .line 5100
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@ac
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@af
    .line 5102
    iget-object v0, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@b1
    goto :goto_87
.end method

.method private hasPasswordTransformationMethod()Z
    .registers 2

    #@0
    .prologue
    .line 4129
    iget-object v0, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@2
    instance-of v0, v0, Landroid/text/method/PasswordTransformationMethod;

    #@4
    return v0
.end method

.method private invalidateCursor(III)V
    .registers 7
    .parameter "a"
    .parameter "b"
    .parameter "c"

    #@0
    .prologue
    .line 4693
    if-gez p1, :cond_6

    #@2
    if-gez p2, :cond_6

    #@4
    if-ltz p3, :cond_1a

    #@6
    .line 4694
    :cond_6
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    #@9
    move-result v2

    #@a
    invoke-static {v2, p3}, Ljava/lang/Math;->min(II)I

    #@d
    move-result v1

    #@e
    .line 4695
    .local v1, start:I
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v2

    #@12
    invoke-static {v2, p3}, Ljava/lang/Math;->max(II)I

    #@15
    move-result v0

    #@16
    .line 4696
    .local v0, end:I
    const/4 v2, 0x1

    #@17
    invoke-virtual {p0, v1, v0, v2}, Landroid/widget/TextView;->invalidateRegion(IIZ)V

    #@1a
    .line 4698
    .end local v0           #end:I
    .end local v1           #start:I
    :cond_1a
    return-void
.end method

.method private static isMultilineInputType(I)Z
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 4028
    const v0, 0x2000f

    #@3
    and-int/2addr v0, p0

    #@4
    const v1, 0x20001

    #@7
    if-ne v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method private static isPasswordInputType(I)Z
    .registers 3
    .parameter "inputType"

    #@0
    .prologue
    .line 4133
    and-int/lit16 v0, p0, 0xfff

    #@2
    .line 4135
    .local v0, variation:I
    const/16 v1, 0x81

    #@4
    if-eq v0, v1, :cond_e

    #@6
    const/16 v1, 0xe1

    #@8
    if-eq v0, v1, :cond_e

    #@a
    const/16 v1, 0x12

    #@c
    if-ne v0, v1, :cond_10

    #@e
    :cond_e
    const/4 v1, 0x1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method private isShowingHint()Z
    .registers 2

    #@0
    .prologue
    .line 6773
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    iget-object v0, p0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private static isVisiblePasswordInputType(I)Z
    .registers 3
    .parameter "inputType"

    #@0
    .prologue
    .line 4144
    and-int/lit16 v0, p0, 0xfff

    #@2
    .line 4146
    .local v0, variation:I
    const/16 v1, 0x91

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v1, 0x1

    #@7
    :goto_7
    return v1

    #@8
    :cond_8
    const/4 v1, 0x0

    #@9
    goto :goto_7
.end method

.method private makeSingleLayout(ILandroid/text/BoringLayout$Metrics;ILandroid/text/Layout$Alignment;ZLandroid/text/TextUtils$TruncateAt;Z)Landroid/text/Layout;
    .registers 23
    .parameter "wantWidth"
    .parameter "boring"
    .parameter "ellipsisWidth"
    .parameter "alignment"
    .parameter "shouldEllipsize"
    .parameter "effectiveEllipsize"
    .parameter "useSaved"

    #@0
    .prologue
    .line 6265
    const/4 v1, 0x0

    #@1
    .line 6266
    .local v1, result:Landroid/text/Layout;
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3
    instance-of v2, v2, Landroid/text/Spannable;

    #@5
    if-eqz v2, :cond_2b

    #@7
    .line 6267
    new-instance v1, Landroid/text/DynamicLayout;

    #@9
    .end local v1           #result:Landroid/text/Layout;
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@b
    iget-object v3, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@d
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@f
    iget-object v7, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@11
    iget v8, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@13
    iget v9, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@15
    iget-boolean v10, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@17
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@1a
    move-result-object v5

    #@1b
    if-nez v5, :cond_29

    #@1d
    move-object/from16 v11, p6

    #@1f
    :goto_1f
    move/from16 v5, p1

    #@21
    move-object/from16 v6, p4

    #@23
    move/from16 v12, p3

    #@25
    invoke-direct/range {v1 .. v12}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;I)V

    #@28
    .line 6330
    .restart local v1       #result:Landroid/text/Layout;
    :cond_28
    :goto_28
    return-object v1

    #@29
    .line 6267
    .end local v1           #result:Landroid/text/Layout;
    :cond_29
    const/4 v11, 0x0

    #@2a
    goto :goto_1f

    #@2b
    .line 6272
    .restart local v1       #result:Landroid/text/Layout;
    :cond_2b
    sget-object v2, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@2d
    move-object/from16 v0, p2

    #@2f
    if-ne v0, v2, :cond_43

    #@31
    .line 6273
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@33
    iget-object v3, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@35
    iget-object v4, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@37
    iget-object v5, p0, Landroid/widget/TextView;->mBoring:Landroid/text/BoringLayout$Metrics;

    #@39
    invoke-static {v2, v3, v4, v5}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@3c
    move-result-object p2

    #@3d
    .line 6274
    if-eqz p2, :cond_43

    #@3f
    .line 6275
    move-object/from16 v0, p2

    #@41
    iput-object v0, p0, Landroid/widget/TextView;->mBoring:Landroid/text/BoringLayout$Metrics;

    #@43
    .line 6279
    :cond_43
    if-eqz p2, :cond_11c

    #@45
    .line 6280
    move-object/from16 v0, p2

    #@47
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@49
    move/from16 v0, p1

    #@4b
    if-gt v2, v0, :cond_90

    #@4d
    if-eqz p6, :cond_57

    #@4f
    move-object/from16 v0, p2

    #@51
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@53
    move/from16 v0, p3

    #@55
    if-gt v2, v0, :cond_90

    #@57
    .line 6282
    :cond_57
    if-eqz p7, :cond_7b

    #@59
    iget-object v2, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@5b
    if-eqz v2, :cond_7b

    #@5d
    .line 6283
    iget-object v2, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@5f
    iget-object v3, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@61
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@63
    iget v7, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@65
    iget v8, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@67
    iget-boolean v10, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@69
    move/from16 v5, p1

    #@6b
    move-object/from16 v6, p4

    #@6d
    move-object/from16 v9, p2

    #@6f
    invoke-virtual/range {v2 .. v10}, Landroid/text/BoringLayout;->replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    #@72
    move-result-object v1

    #@73
    .line 6292
    :goto_73
    if-eqz p7, :cond_28

    #@75
    move-object v2, v1

    #@76
    .line 6293
    check-cast v2, Landroid/text/BoringLayout;

    #@78
    iput-object v2, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@7a
    goto :goto_28

    #@7b
    .line 6287
    :cond_7b
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@7d
    iget-object v3, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@7f
    iget v6, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@81
    iget v7, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@83
    iget-boolean v9, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@85
    move/from16 v4, p1

    #@87
    move-object/from16 v5, p4

    #@89
    move-object/from16 v8, p2

    #@8b
    invoke-static/range {v2 .. v9}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    #@8e
    move-result-object v1

    #@8f
    goto :goto_73

    #@90
    .line 6295
    :cond_90
    if-eqz p5, :cond_d6

    #@92
    move-object/from16 v0, p2

    #@94
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@96
    move/from16 v0, p1

    #@98
    if-gt v2, v0, :cond_d6

    #@9a
    .line 6296
    if-eqz p7, :cond_bc

    #@9c
    iget-object v2, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@9e
    if-eqz v2, :cond_bc

    #@a0
    .line 6297
    iget-object v2, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@a2
    iget-object v3, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@a4
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@a6
    iget v7, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@a8
    iget v8, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@aa
    iget-boolean v10, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@ac
    move/from16 v5, p1

    #@ae
    move-object/from16 v6, p4

    #@b0
    move-object/from16 v9, p2

    #@b2
    move-object/from16 v11, p6

    #@b4
    move/from16 v12, p3

    #@b6
    invoke-virtual/range {v2 .. v12}, Landroid/text/BoringLayout;->replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;

    #@b9
    move-result-object v1

    #@ba
    goto/16 :goto_28

    #@bc
    .line 6302
    :cond_bc
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@be
    iget-object v3, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@c0
    iget v6, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@c2
    iget v7, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@c4
    iget-boolean v9, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@c6
    move/from16 v4, p1

    #@c8
    move-object/from16 v5, p4

    #@ca
    move-object/from16 v8, p2

    #@cc
    move-object/from16 v10, p6

    #@ce
    move/from16 v11, p3

    #@d0
    invoke-static/range {v2 .. v11}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;

    #@d3
    move-result-object v1

    #@d4
    goto/16 :goto_28

    #@d6
    .line 6307
    :cond_d6
    if-eqz p5, :cond_105

    #@d8
    .line 6308
    new-instance v1, Landroid/text/StaticLayout;

    #@da
    .end local v1           #result:Landroid/text/Layout;
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@dc
    const/4 v3, 0x0

    #@dd
    iget-object v4, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@df
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@e2
    move-result v4

    #@e3
    iget-object v5, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@e5
    iget-object v8, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@e7
    iget v9, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@e9
    iget v10, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@eb
    iget-boolean v11, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@ed
    iget v6, p0, Landroid/widget/TextView;->mMaxMode:I

    #@ef
    const/4 v7, 0x1

    #@f0
    if-ne v6, v7, :cond_101

    #@f2
    iget v14, p0, Landroid/widget/TextView;->mMaximum:I

    #@f4
    :goto_f4
    move/from16 v6, p1

    #@f6
    move-object/from16 v7, p4

    #@f8
    move-object/from16 v12, p6

    #@fa
    move/from16 v13, p3

    #@fc
    invoke-direct/range {v1 .. v14}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    #@ff
    .restart local v1       #result:Landroid/text/Layout;
    goto/16 :goto_28

    #@101
    .end local v1           #result:Landroid/text/Layout;
    :cond_101
    const v14, 0x7fffffff

    #@104
    goto :goto_f4

    #@105
    .line 6314
    .restart local v1       #result:Landroid/text/Layout;
    :cond_105
    new-instance v1, Landroid/text/StaticLayout;

    #@107
    .end local v1           #result:Landroid/text/Layout;
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@109
    iget-object v3, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@10b
    iget-object v6, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@10d
    iget v7, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@10f
    iget v8, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@111
    iget-boolean v9, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@113
    move/from16 v4, p1

    #@115
    move-object/from16 v5, p4

    #@117
    invoke-direct/range {v1 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V

    #@11a
    .restart local v1       #result:Landroid/text/Layout;
    goto/16 :goto_28

    #@11c
    .line 6318
    :cond_11c
    if-eqz p5, :cond_14b

    #@11e
    .line 6319
    new-instance v1, Landroid/text/StaticLayout;

    #@120
    .end local v1           #result:Landroid/text/Layout;
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@122
    const/4 v3, 0x0

    #@123
    iget-object v4, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@125
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@128
    move-result v4

    #@129
    iget-object v5, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@12b
    iget-object v8, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@12d
    iget v9, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@12f
    iget v10, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@131
    iget-boolean v11, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@133
    iget v6, p0, Landroid/widget/TextView;->mMaxMode:I

    #@135
    const/4 v7, 0x1

    #@136
    if-ne v6, v7, :cond_147

    #@138
    iget v14, p0, Landroid/widget/TextView;->mMaximum:I

    #@13a
    :goto_13a
    move/from16 v6, p1

    #@13c
    move-object/from16 v7, p4

    #@13e
    move-object/from16 v12, p6

    #@140
    move/from16 v13, p3

    #@142
    invoke-direct/range {v1 .. v14}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    #@145
    .restart local v1       #result:Landroid/text/Layout;
    goto/16 :goto_28

    #@147
    .end local v1           #result:Landroid/text/Layout;
    :cond_147
    const v14, 0x7fffffff

    #@14a
    goto :goto_13a

    #@14b
    .line 6325
    .restart local v1       #result:Landroid/text/Layout;
    :cond_14b
    new-instance v1, Landroid/text/StaticLayout;

    #@14d
    .end local v1           #result:Landroid/text/Layout;
    iget-object v2, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@14f
    iget-object v3, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@151
    iget-object v6, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@153
    iget v7, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@155
    iget v8, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@157
    iget-boolean v9, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@159
    move/from16 v4, p1

    #@15b
    move-object/from16 v5, p4

    #@15d
    invoke-direct/range {v1 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V

    #@160
    .restart local v1       #result:Landroid/text/Layout;
    goto/16 :goto_28
.end method

.method private nullLayouts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 6010
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@3
    instance-of v0, v0, Landroid/text/BoringLayout;

    #@5
    if-eqz v0, :cond_11

    #@7
    iget-object v0, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@9
    if-nez v0, :cond_11

    #@b
    .line 6011
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@d
    check-cast v0, Landroid/text/BoringLayout;

    #@f
    iput-object v0, p0, Landroid/widget/TextView;->mSavedLayout:Landroid/text/BoringLayout;

    #@11
    .line 6013
    :cond_11
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@13
    instance-of v0, v0, Landroid/text/BoringLayout;

    #@15
    if-eqz v0, :cond_21

    #@17
    iget-object v0, p0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@19
    if-nez v0, :cond_21

    #@1b
    .line 6014
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@1d
    check-cast v0, Landroid/text/BoringLayout;

    #@1f
    iput-object v0, p0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@21
    .line 6017
    :cond_21
    iput-object v1, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@23
    iput-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@25
    iput-object v1, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@27
    .line 6019
    iput-object v1, p0, Landroid/widget/TextView;->mHintBoring:Landroid/text/BoringLayout$Metrics;

    #@29
    iput-object v1, p0, Landroid/widget/TextView;->mBoring:Landroid/text/BoringLayout$Metrics;

    #@2b
    .line 6022
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2d
    if-eqz v0, :cond_34

    #@2f
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@31
    invoke-virtual {v0}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@34
    .line 6023
    :cond_34
    return-void
.end method

.method private paste(II)V
    .registers 28
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 8666
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v22

    #@4
    const-string v23, "clipboard"

    #@6
    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v6

    #@a
    check-cast v6, Landroid/content/ClipboardManager;

    #@c
    .line 8668
    .local v6, clipboard:Landroid/content/ClipboardManager;
    invoke-virtual {v6}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    #@f
    move-result-object v4

    #@10
    .line 8669
    .local v4, clip:Landroid/content/ClipData;
    if-eqz v4, :cond_1f1

    #@12
    .line 8670
    const/4 v9, 0x0

    #@13
    .line 8671
    .local v9, didFirst:Z
    const/4 v12, 0x0

    #@14
    .local v12, i:I
    :goto_14
    invoke-virtual {v4}, Landroid/content/ClipData;->getItemCount()I

    #@17
    move-result v22

    #@18
    move/from16 v0, v22

    #@1a
    if-ge v12, v0, :cond_1ea

    #@1c
    .line 8672
    const/16 v18, 0x0

    #@1e
    .line 8673
    .local v18, paste:Ljava/lang/CharSequence;
    sget-boolean v22, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@20
    if-eqz v22, :cond_90

    #@22
    .line 8674
    invoke-virtual {v4, v12}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@25
    move-result-object v13

    #@26
    .line 8675
    .local v13, item:Landroid/content/ClipData$Item;
    invoke-virtual {v13}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    #@29
    move-result-object v5

    #@2a
    .line 8676
    .local v5, clipUri:Landroid/net/Uri;
    if-eqz v5, :cond_85

    #@2c
    const-string v22, "file"

    #@2e
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@31
    move-result-object v23

    #@32
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v22

    #@36
    if-eqz v22, :cond_85

    #@38
    .line 8684
    .end local v5           #clipUri:Landroid/net/Uri;
    .end local v13           #item:Landroid/content/ClipData$Item;
    :goto_38
    if-eqz v18, :cond_126

    #@3a
    .line 8685
    sget-boolean v22, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@3c
    if-eqz v22, :cond_12d

    #@3e
    sget-boolean v22, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@40
    if-nez v22, :cond_12d

    #@42
    .line 8686
    move-object/from16 v0, p0

    #@44
    iget-boolean v0, v0, Landroid/widget/TextView;->mNoEmojiEditMode:Z

    #@46
    move/from16 v22, v0

    #@48
    if-eqz v22, :cond_ed

    #@4a
    invoke-static/range {v18 .. v18}, Landroid/text/Layout;->hasEmoji(Ljava/lang/CharSequence;)Z

    #@4d
    move-result v22

    #@4e
    if-eqz v22, :cond_ed

    #@50
    .line 8687
    new-instance v20, Ljava/lang/String;

    #@52
    const-string v22, ""

    #@54
    move-object/from16 v0, v20

    #@56
    move-object/from16 v1, v22

    #@58
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@5b
    .line 8688
    .local v20, pasteNoEmoji:Ljava/lang/String;
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->length()I

    #@5e
    move-result v19

    #@5f
    .line 8690
    .local v19, pasteLen:I
    const/4 v14, 0x0

    #@60
    .local v14, j:I
    :goto_60
    move/from16 v0, v19

    #@62
    if-ge v14, v0, :cond_d8

    #@64
    .line 8691
    move-object/from16 v0, v18

    #@66
    invoke-static {v0, v14}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    #@69
    move-result v7

    #@6a
    .line 8692
    .local v7, code:I
    add-int/lit8 v22, v14, 0x1

    #@6c
    move/from16 v0, v22

    #@6e
    move/from16 v1, v19

    #@70
    if-ge v0, v1, :cond_9d

    #@72
    invoke-static {v7}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@75
    move-result v22

    #@76
    if-nez v22, :cond_80

    #@78
    move-object/from16 v0, v18

    #@7a
    invoke-static {v0, v14}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@7d
    move-result v22

    #@7e
    if-eqz v22, :cond_9d

    #@80
    .line 8693
    :cond_80
    add-int/lit8 v14, v14, 0x1

    #@82
    .line 8690
    :cond_82
    :goto_82
    add-int/lit8 v14, v14, 0x1

    #@84
    goto :goto_60

    #@85
    .line 8679
    .end local v7           #code:I
    .end local v14           #j:I
    .end local v19           #pasteLen:I
    .end local v20           #pasteNoEmoji:Ljava/lang/String;
    .restart local v5       #clipUri:Landroid/net/Uri;
    .restart local v13       #item:Landroid/content/ClipData$Item;
    :cond_85
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@88
    move-result-object v22

    #@89
    move-object/from16 v0, v22

    #@8b
    invoke-virtual {v13, v0}, Landroid/content/ClipData$Item;->coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@8e
    move-result-object v18

    #@8f
    goto :goto_38

    #@90
    .line 8682
    .end local v5           #clipUri:Landroid/net/Uri;
    .end local v13           #item:Landroid/content/ClipData$Item;
    :cond_90
    invoke-virtual {v4, v12}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@93
    move-result-object v22

    #@94
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@97
    move-result-object v23

    #@98
    invoke-virtual/range {v22 .. v23}, Landroid/content/ClipData$Item;->coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@9b
    move-result-object v18

    #@9c
    goto :goto_38

    #@9d
    .line 8694
    .restart local v7       #code:I
    .restart local v14       #j:I
    .restart local v19       #pasteLen:I
    .restart local v20       #pasteNoEmoji:Ljava/lang/String;
    :cond_9d
    add-int/lit8 v22, v14, 0x3

    #@9f
    move/from16 v0, v22

    #@a1
    move/from16 v1, v19

    #@a3
    if-ge v0, v1, :cond_b0

    #@a5
    move-object/from16 v0, v18

    #@a7
    invoke-static {v0, v14}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@aa
    move-result v22

    #@ab
    if-eqz v22, :cond_b0

    #@ad
    .line 8695
    add-int/lit8 v14, v14, 0x3

    #@af
    goto :goto_82

    #@b0
    .line 8696
    :cond_b0
    move-object/from16 v0, v18

    #@b2
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    #@b5
    move-result v22

    #@b6
    invoke-static/range {v22 .. v22}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@b9
    move-result v22

    #@ba
    if-nez v22, :cond_82

    #@bc
    .line 8697
    new-instance v22, Ljava/lang/StringBuilder;

    #@be
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    move-object/from16 v0, v22

    #@c3
    move-object/from16 v1, v20

    #@c5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v22

    #@c9
    move-object/from16 v0, v18

    #@cb
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    #@ce
    move-result v23

    #@cf
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v22

    #@d3
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v20

    #@d7
    goto :goto_82

    #@d8
    .line 8701
    .end local v7           #code:I
    :cond_d8
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@db
    move-result-object v22

    #@dc
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    #@df
    move-result v22

    #@e0
    if-nez v22, :cond_12a

    #@e2
    .line 8702
    new-instance v18, Ljava/lang/String;

    #@e4
    .end local v18           #paste:Ljava/lang/CharSequence;
    const-string v22, ""

    #@e6
    move-object/from16 v0, v18

    #@e8
    move-object/from16 v1, v22

    #@ea
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@ed
    .line 8732
    .end local v14           #j:I
    .end local v19           #pasteLen:I
    .end local v20           #pasteNoEmoji:Ljava/lang/String;
    .restart local v18       #paste:Ljava/lang/CharSequence;
    :cond_ed
    :goto_ed
    if-nez v9, :cond_1c2

    #@ef
    .line 8733
    move-object/from16 v0, p0

    #@f1
    move/from16 v1, p1

    #@f3
    move/from16 v2, p2

    #@f5
    move-object/from16 v3, v18

    #@f7
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->prepareSpacesAroundPaste(IILjava/lang/CharSequence;)J

    #@fa
    move-result-wide v16

    #@fb
    .line 8734
    .local v16, minMax:J
    invoke-static/range {v16 .. v17}, Landroid/text/TextUtils;->unpackRangeStartFromLong(J)I

    #@fe
    move-result p1

    #@ff
    .line 8735
    invoke-static/range {v16 .. v17}, Landroid/text/TextUtils;->unpackRangeEndFromLong(J)I

    #@102
    move-result p2

    #@103
    .line 8736
    move-object/from16 v0, p0

    #@105
    iget-object v0, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@107
    move-object/from16 v22, v0

    #@109
    check-cast v22, Landroid/text/Spannable;

    #@10b
    move-object/from16 v0, v22

    #@10d
    move/from16 v1, p2

    #@10f
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@112
    .line 8737
    move-object/from16 v0, p0

    #@114
    iget-object v0, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@116
    move-object/from16 v22, v0

    #@118
    check-cast v22, Landroid/text/Editable;

    #@11a
    move-object/from16 v0, v22

    #@11c
    move/from16 v1, p1

    #@11e
    move/from16 v2, p2

    #@120
    move-object/from16 v3, v18

    #@122
    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@125
    .line 8738
    const/4 v9, 0x1

    #@126
    .line 8671
    .end local v16           #minMax:J
    :cond_126
    :goto_126
    add-int/lit8 v12, v12, 0x1

    #@128
    goto/16 :goto_14

    #@12a
    .line 8704
    .restart local v14       #j:I
    .restart local v19       #pasteLen:I
    .restart local v20       #pasteNoEmoji:Ljava/lang/String;
    :cond_12a
    move-object/from16 v18, v20

    #@12c
    goto :goto_ed

    #@12d
    .line 8708
    .end local v14           #j:I
    .end local v19           #pasteLen:I
    .end local v20           #pasteNoEmoji:Ljava/lang/String;
    :cond_12d
    sget-boolean v22, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@12f
    if-eqz v22, :cond_ed

    #@131
    .line 8709
    move-object/from16 v0, p0

    #@133
    iget-boolean v0, v0, Landroid/widget/TextView;->mNoEmojiEditMode:Z

    #@135
    move/from16 v22, v0

    #@137
    if-eqz v22, :cond_ed

    #@139
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13c
    move-result-object v22

    #@13d
    const-string v23, "^[\u0000-\uffff]*$"

    #@13f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@142
    move-result v22

    #@143
    if-nez v22, :cond_ed

    #@145
    .line 8710
    new-instance v21, Landroid/text/SpannableStringBuilder;

    #@147
    invoke-direct/range {v21 .. v21}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@14a
    .line 8711
    .local v21, ssb:Landroid/text/SpannableStringBuilder;
    const/4 v10, 0x0

    #@14b
    .line 8712
    .local v10, done:I
    const/4 v14, 0x0

    #@14c
    .restart local v14       #j:I
    :goto_14c
    add-int/lit8 v22, v14, 0x1

    #@14e
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->length()I

    #@151
    move-result v23

    #@152
    move/from16 v0, v22

    #@154
    move/from16 v1, v23

    #@156
    if-ge v0, v1, :cond_1af

    #@158
    .line 8713
    move-object/from16 v0, v18

    #@15a
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    #@15d
    move-result v11

    #@15e
    .line 8714
    .local v11, high:C
    add-int/lit8 v22, v14, 0x1

    #@160
    move-object/from16 v0, v18

    #@162
    move/from16 v1, v22

    #@164
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@167
    move-result v15

    #@168
    .line 8715
    .local v15, low:C
    invoke-static {v11, v15}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@16b
    move-result v22

    #@16c
    if-eqz v22, :cond_1ac

    #@16e
    .line 8716
    invoke-static {v11, v15}, Ljava/lang/Character;->toCodePoint(CC)I

    #@171
    move-result v8

    #@172
    .line 8717
    .local v8, codePoint:I
    invoke-static {}, Landroid/text/Layout;->getEmojiFactory()Landroid/emoji/EmojiFactory;

    #@175
    move-result-object v22

    #@176
    invoke-virtual/range {v22 .. v22}, Landroid/emoji/EmojiFactory;->getMinimumAndroidPua()I

    #@179
    move-result v22

    #@17a
    move/from16 v0, v22

    #@17c
    if-lt v8, v0, :cond_18a

    #@17e
    invoke-static {}, Landroid/text/Layout;->getEmojiFactory()Landroid/emoji/EmojiFactory;

    #@181
    move-result-object v22

    #@182
    invoke-virtual/range {v22 .. v22}, Landroid/emoji/EmojiFactory;->getMaximumAndroidPua()I

    #@185
    move-result v22

    #@186
    move/from16 v0, v22

    #@188
    if-le v8, v0, :cond_1ac

    #@18a
    :cond_18a
    const/high16 v22, 0x2

    #@18c
    move/from16 v0, v22

    #@18e
    if-lt v8, v0, :cond_197

    #@190
    const v22, 0x2ffff

    #@193
    move/from16 v0, v22

    #@195
    if-le v8, v0, :cond_1ac

    #@197
    .line 8720
    :cond_197
    move-object/from16 v0, v18

    #@199
    invoke-interface {v0, v10, v14}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@19c
    move-result-object v22

    #@19d
    invoke-virtual/range {v21 .. v22}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@1a0
    .line 8721
    add-int/lit8 v22, v14, 0x2

    #@1a2
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->length()I

    #@1a5
    move-result v23

    #@1a6
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    #@1a9
    move-result v10

    #@1aa
    .line 8722
    add-int/lit8 v14, v14, 0x1

    #@1ac
    .line 8712
    .end local v8           #codePoint:I
    :cond_1ac
    add-int/lit8 v14, v14, 0x1

    #@1ae
    goto :goto_14c

    #@1af
    .line 8726
    .end local v11           #high:C
    .end local v15           #low:C
    :cond_1af
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->length()I

    #@1b2
    move-result v22

    #@1b3
    move-object/from16 v0, v18

    #@1b5
    move/from16 v1, v22

    #@1b7
    invoke-interface {v0, v10, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@1ba
    move-result-object v22

    #@1bb
    invoke-virtual/range {v21 .. v22}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@1be
    .line 8727
    move-object/from16 v18, v21

    #@1c0
    goto/16 :goto_ed

    #@1c2
    .line 8740
    .end local v10           #done:I
    .end local v14           #j:I
    .end local v21           #ssb:Landroid/text/SpannableStringBuilder;
    :cond_1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v0, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1c6
    move-object/from16 v22, v0

    #@1c8
    check-cast v22, Landroid/text/Editable;

    #@1ca
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@1cd
    move-result v23

    #@1ce
    const-string v24, "\n"

    #@1d0
    invoke-interface/range {v22 .. v24}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    #@1d3
    .line 8741
    move-object/from16 v0, p0

    #@1d5
    iget-object v0, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1d7
    move-object/from16 v22, v0

    #@1d9
    check-cast v22, Landroid/text/Editable;

    #@1db
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@1de
    move-result v23

    #@1df
    move-object/from16 v0, v22

    #@1e1
    move/from16 v1, v23

    #@1e3
    move-object/from16 v2, v18

    #@1e5
    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    #@1e8
    goto/16 :goto_126

    #@1ea
    .line 8745
    .end local v18           #paste:Ljava/lang/CharSequence;
    :cond_1ea
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@1ed
    .line 8746
    const-wide/16 v22, 0x0

    #@1ef
    sput-wide v22, Landroid/widget/TextView;->LAST_CUT_OR_COPY_TIME:J

    #@1f1
    .line 8748
    .end local v9           #didFirst:Z
    .end local v12           #i:I
    :cond_1f1
    return-void
.end method

.method private registerForPreDraw()V
    .registers 2

    #@0
    .prologue
    .line 4759
    iget-boolean v0, p0, Landroid/widget/TextView;->mPreDrawRegistered:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 4760
    invoke-virtual {p0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@b
    .line 4761
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/widget/TextView;->mPreDrawRegistered:Z

    #@e
    .line 4763
    :cond_e
    return-void
.end method

.method private removeIntersectingSpans(IILjava/lang/Class;)V
    .registers 11
    .parameter "start"
    .parameter "end"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 7513
    .local p3, type:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    iget-object v6, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    instance-of v6, v6, Landroid/text/Editable;

    #@4
    if-nez v6, :cond_7

    #@6
    .line 7526
    :cond_6
    return-void

    #@7
    .line 7514
    :cond_7
    iget-object v5, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@9
    check-cast v5, Landroid/text/Editable;

    #@b
    .line 7516
    .local v5, text:Landroid/text/Editable;
    invoke-interface {v5, p1, p2, p3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    .line 7517
    .local v4, spans:[Ljava/lang/Object;,"[TT;"
    array-length v2, v4

    #@10
    .line 7518
    .local v2, length:I
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v2, :cond_6

    #@13
    .line 7519
    aget-object v6, v4, v1

    #@15
    invoke-interface {v5, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@18
    move-result v3

    #@19
    .line 7520
    .local v3, s:I
    aget-object v6, v4, v1

    #@1b
    invoke-interface {v5, v6}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@1e
    move-result v0

    #@1f
    .line 7523
    .local v0, e:I
    if-eq v0, p1, :cond_6

    #@21
    if-eq v3, p2, :cond_6

    #@23
    .line 7524
    aget-object v6, v4, v1

    #@25
    invoke-interface {v5, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@28
    .line 7518
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_11
.end method

.method static removeParcelableSpans(Landroid/text/Spannable;II)V
    .registers 6
    .parameter "spannable"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 5876
    const-class v2, Landroid/text/ParcelableSpan;

    #@2
    invoke-interface {p0, p1, p2, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    .line 5877
    .local v1, spans:[Ljava/lang/Object;
    array-length v0, v1

    #@7
    .line 5878
    .local v0, i:I
    :goto_7
    if-lez v0, :cond_11

    #@9
    .line 5879
    add-int/lit8 v0, v0, -0x1

    #@b
    .line 5880
    aget-object v2, v1, v0

    #@d
    invoke-interface {p0, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@10
    goto :goto_7

    #@11
    .line 5882
    :cond_11
    return-void
.end method

.method private restartMarqueeIfNeeded()V
    .registers 3

    #@0
    .prologue
    .line 4529
    iget-boolean v0, p0, Landroid/widget/TextView;->mRestartMarquee:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@6
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@8
    if-ne v0, v1, :cond_10

    #@a
    .line 4530
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/widget/TextView;->mRestartMarquee:Z

    #@d
    .line 4531
    invoke-direct {p0}, Landroid/widget/TextView;->startMarquee()V

    #@10
    .line 4533
    :cond_10
    return-void
.end method

.method private sendBeforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 10
    .parameter "text"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 7498
    iget-object v3, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v3, :cond_19

    #@4
    .line 7499
    iget-object v2, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@6
    .line 7500
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/TextWatcher;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 7501
    .local v0, count:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_19

    #@d
    .line 7502
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/text/TextWatcher;

    #@13
    invoke-interface {v3, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    #@16
    .line 7501
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_b

    #@19
    .line 7507
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/TextWatcher;>;"
    :cond_19
    add-int v3, p2, p3

    #@1b
    const-class v4, Landroid/text/style/SpellCheckSpan;

    #@1d
    invoke-direct {p0, p2, v3, v4}, Landroid/widget/TextView;->removeIntersectingSpans(IILjava/lang/Class;)V

    #@20
    .line 7508
    add-int v3, p2, p3

    #@22
    const-class v4, Landroid/text/style/SuggestionSpan;

    #@24
    invoke-direct {p0, p2, v3, v4}, Landroid/widget/TextView;->removeIntersectingSpans(IILjava/lang/Class;)V

    #@27
    .line 7509
    return-void
.end method

.method private setFilters(Landroid/text/Editable;[Landroid/text/InputFilter;)V
    .registers 6
    .parameter "e"
    .parameter "filters"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4558
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3
    if-eqz v1, :cond_23

    #@5
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7
    iget-object v1, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@9
    instance-of v1, v1, Landroid/text/InputFilter;

    #@b
    if-eqz v1, :cond_23

    #@d
    .line 4559
    array-length v1, p2

    #@e
    add-int/lit8 v1, v1, 0x1

    #@10
    new-array v0, v1, [Landroid/text/InputFilter;

    #@12
    .line 4561
    .local v0, nf:[Landroid/text/InputFilter;
    array-length v1, p2

    #@13
    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@16
    .line 4562
    array-length v2, p2

    #@17
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@19
    iget-object v1, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@1b
    check-cast v1, Landroid/text/InputFilter;

    #@1d
    aput-object v1, v0, v2

    #@1f
    .line 4564
    invoke-interface {p1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    #@22
    .line 4568
    .end local v0           #nf:[Landroid/text/InputFilter;
    :goto_22
    return-void

    #@23
    .line 4566
    :cond_23
    invoke-interface {p1, p2}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    #@26
    goto :goto_22
.end method

.method private setForceTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .parameter "tf"

    #@0
    .prologue
    .line 1372
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@5
    .line 1373
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 1374
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@c
    .line 1375
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@f
    .line 1376
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@12
    .line 1378
    :cond_12
    return-void
.end method

.method private setForceTypeface(Landroid/graphics/Typeface;I)V
    .registers 9
    .parameter "tf"
    .parameter "style"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1381
    if-lez p2, :cond_36

    #@4
    .line 1382
    if-nez p1, :cond_2d

    #@6
    .line 1383
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    #@9
    move-result-object p1

    #@a
    .line 1387
    :goto_a
    invoke-direct {p0, p1}, Landroid/widget/TextView;->setForceTypeface(Landroid/graphics/Typeface;)V

    #@d
    .line 1388
    if-eqz p1, :cond_32

    #@f
    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    #@12
    move-result v1

    #@13
    .line 1389
    .local v1, typefaceStyle:I
    :goto_13
    xor-int/lit8 v4, v1, -0x1

    #@15
    and-int v0, p2, v4

    #@17
    .line 1390
    .local v0, need:I
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@19
    and-int/lit8 v5, v0, 0x1

    #@1b
    if-eqz v5, :cond_1e

    #@1d
    const/4 v2, 0x1

    #@1e
    :cond_1e
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@21
    .line 1391
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@23
    and-int/lit8 v2, v0, 0x2

    #@25
    if-eqz v2, :cond_34

    #@27
    const/high16 v2, -0x4180

    #@29
    :goto_29
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@2c
    .line 1397
    .end local v0           #need:I
    .end local v1           #typefaceStyle:I
    :goto_2c
    return-void

    #@2d
    .line 1385
    :cond_2d
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@30
    move-result-object p1

    #@31
    goto :goto_a

    #@32
    :cond_32
    move v1, v2

    #@33
    .line 1388
    goto :goto_13

    #@34
    .restart local v0       #need:I
    .restart local v1       #typefaceStyle:I
    :cond_34
    move v2, v3

    #@35
    .line 1391
    goto :goto_29

    #@36
    .line 1393
    .end local v0           #need:I
    .end local v1           #typefaceStyle:I
    :cond_36
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@38
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@3b
    .line 1394
    iget-object v2, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@3d
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@40
    .line 1395
    invoke-direct {p0, p1}, Landroid/widget/TextView;->setForceTypeface(Landroid/graphics/Typeface;)V

    #@43
    goto :goto_2c
.end method

.method private setForceTypefaceFromAttrs(Ljava/lang/String;II)V
    .registers 5
    .parameter "familyName"
    .parameter "typefaceIndex"
    .parameter "styleIndex"

    #@0
    .prologue
    .line 1400
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;->setTextViewTypeface(Ljava/lang/String;II)V

    #@3
    .line 1401
    const/4 v0, 0x0

    #@4
    .line 1402
    .local v0, tf:Landroid/graphics/Typeface;
    if-eqz p1, :cond_10

    #@6
    .line 1403
    invoke-static {p1, p3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@9
    move-result-object v0

    #@a
    .line 1404
    if-eqz v0, :cond_10

    #@c
    .line 1405
    invoke-direct {p0, v0}, Landroid/widget/TextView;->setForceTypeface(Landroid/graphics/Typeface;)V

    #@f
    .line 1423
    :goto_f
    return-void

    #@10
    .line 1409
    :cond_10
    packed-switch p2, :pswitch_data_20

    #@13
    .line 1422
    :goto_13
    invoke-direct {p0, v0, p3}, Landroid/widget/TextView;->setForceTypeface(Landroid/graphics/Typeface;I)V

    #@16
    goto :goto_f

    #@17
    .line 1411
    :pswitch_17
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@19
    .line 1412
    goto :goto_13

    #@1a
    .line 1415
    :pswitch_1a
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    #@1c
    .line 1416
    goto :goto_13

    #@1d
    .line 1419
    :pswitch_1d
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    #@1f
    goto :goto_13

    #@20
    .line 1409
    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
    .end packed-switch
.end method

.method private setInputType(IZ)V
    .registers 11
    .parameter "type"
    .parameter "direct"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 4164
    and-int/lit8 v2, p1, 0xf

    #@4
    .line 4166
    .local v2, cls:I
    if-ne v2, v4, :cond_37

    #@6
    .line 4167
    const v6, 0x8000

    #@9
    and-int/2addr v6, p1

    #@a
    if-eqz v6, :cond_24

    #@c
    move v0, v4

    #@d
    .line 4169
    .local v0, autotext:Z
    :goto_d
    and-int/lit16 v4, p1, 0x1000

    #@f
    if-eqz v4, :cond_26

    #@11
    .line 4170
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->CHARACTERS:Landroid/text/method/TextKeyListener$Capitalize;

    #@13
    .line 4178
    .local v1, cap:Landroid/text/method/TextKeyListener$Capitalize;
    :goto_13
    invoke-static {v0, v1}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    #@16
    move-result-object v3

    #@17
    .line 4200
    .end local v0           #autotext:Z
    .end local v1           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    .local v3, input:Landroid/text/method/KeyListener;
    :goto_17
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setRawInputType(I)V

    #@1a
    .line 4201
    if-eqz p2, :cond_70

    #@1c
    .line 4202
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@1f
    .line 4203
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@21
    iput-object v3, v4, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@23
    .line 4207
    :goto_23
    return-void

    #@24
    .end local v3           #input:Landroid/text/method/KeyListener;
    :cond_24
    move v0, v5

    #@25
    .line 4167
    goto :goto_d

    #@26
    .line 4171
    .restart local v0       #autotext:Z
    :cond_26
    and-int/lit16 v4, p1, 0x2000

    #@28
    if-eqz v4, :cond_2d

    #@2a
    .line 4172
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->WORDS:Landroid/text/method/TextKeyListener$Capitalize;

    #@2c
    .restart local v1       #cap:Landroid/text/method/TextKeyListener$Capitalize;
    goto :goto_13

    #@2d
    .line 4173
    .end local v1           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :cond_2d
    and-int/lit16 v4, p1, 0x4000

    #@2f
    if-eqz v4, :cond_34

    #@31
    .line 4174
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    #@33
    .restart local v1       #cap:Landroid/text/method/TextKeyListener$Capitalize;
    goto :goto_13

    #@34
    .line 4176
    .end local v1           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :cond_34
    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    #@36
    .restart local v1       #cap:Landroid/text/method/TextKeyListener$Capitalize;
    goto :goto_13

    #@37
    .line 4179
    .end local v0           #autotext:Z
    .end local v1           #cap:Landroid/text/method/TextKeyListener$Capitalize;
    :cond_37
    const/4 v6, 0x2

    #@38
    if-ne v2, v6, :cond_4c

    #@3a
    .line 4180
    and-int/lit16 v6, p1, 0x1000

    #@3c
    if-eqz v6, :cond_48

    #@3e
    move v6, v4

    #@3f
    :goto_3f
    and-int/lit16 v7, p1, 0x2000

    #@41
    if-eqz v7, :cond_4a

    #@43
    :goto_43
    invoke-static {v6, v4}, Landroid/text/method/DigitsKeyListener;->getInstance(ZZ)Landroid/text/method/DigitsKeyListener;

    #@46
    move-result-object v3

    #@47
    .restart local v3       #input:Landroid/text/method/KeyListener;
    goto :goto_17

    #@48
    .end local v3           #input:Landroid/text/method/KeyListener;
    :cond_48
    move v6, v5

    #@49
    goto :goto_3f

    #@4a
    :cond_4a
    move v4, v5

    #@4b
    goto :goto_43

    #@4c
    .line 4183
    :cond_4c
    const/4 v4, 0x4

    #@4d
    if-ne v2, v4, :cond_63

    #@4f
    .line 4184
    and-int/lit16 v4, p1, 0xff0

    #@51
    sparse-switch v4, :sswitch_data_74

    #@54
    .line 4192
    invoke-static {}, Landroid/text/method/DateTimeKeyListener;->getInstance()Landroid/text/method/DateTimeKeyListener;

    #@57
    move-result-object v3

    #@58
    .line 4193
    .restart local v3       #input:Landroid/text/method/KeyListener;
    goto :goto_17

    #@59
    .line 4186
    .end local v3           #input:Landroid/text/method/KeyListener;
    :sswitch_59
    invoke-static {}, Landroid/text/method/DateKeyListener;->getInstance()Landroid/text/method/DateKeyListener;

    #@5c
    move-result-object v3

    #@5d
    .line 4187
    .restart local v3       #input:Landroid/text/method/KeyListener;
    goto :goto_17

    #@5e
    .line 4189
    .end local v3           #input:Landroid/text/method/KeyListener;
    :sswitch_5e
    invoke-static {}, Landroid/text/method/TimeKeyListener;->getInstance()Landroid/text/method/TimeKeyListener;

    #@61
    move-result-object v3

    #@62
    .line 4190
    .restart local v3       #input:Landroid/text/method/KeyListener;
    goto :goto_17

    #@63
    .line 4195
    .end local v3           #input:Landroid/text/method/KeyListener;
    :cond_63
    const/4 v4, 0x3

    #@64
    if-ne v2, v4, :cond_6b

    #@66
    .line 4196
    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    #@69
    move-result-object v3

    #@6a
    .restart local v3       #input:Landroid/text/method/KeyListener;
    goto :goto_17

    #@6b
    .line 4198
    .end local v3           #input:Landroid/text/method/KeyListener;
    :cond_6b
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@6e
    move-result-object v3

    #@6f
    .restart local v3       #input:Landroid/text/method/KeyListener;
    goto :goto_17

    #@70
    .line 4205
    :cond_70
    invoke-direct {p0, v3}, Landroid/widget/TextView;->setKeyListenerOnly(Landroid/text/method/KeyListener;)V

    #@73
    goto :goto_23

    #@74
    .line 4184
    :sswitch_data_74
    .sparse-switch
        0x10 -> :sswitch_59
        0x20 -> :sswitch_5e
    .end sparse-switch
.end method

.method private setInputTypeSingleLine(Z)V
    .registers 5
    .parameter "singleLine"

    #@0
    .prologue
    .line 7230
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_19

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget v0, v0, Landroid/widget/Editor;->mInputType:I

    #@8
    and-int/lit8 v0, v0, 0xf

    #@a
    const/4 v1, 0x1

    #@b
    if-ne v0, v1, :cond_19

    #@d
    .line 7232
    if-eqz p1, :cond_1a

    #@f
    .line 7233
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@11
    iget v1, v0, Landroid/widget/Editor;->mInputType:I

    #@13
    const v2, -0x20001

    #@16
    and-int/2addr v1, v2

    #@17
    iput v1, v0, Landroid/widget/Editor;->mInputType:I

    #@19
    .line 7238
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 7235
    :cond_1a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1c
    iget v1, v0, Landroid/widget/Editor;->mInputType:I

    #@1e
    const/high16 v2, 0x2

    #@20
    or-int/2addr v1, v2

    #@21
    iput v1, v0, Landroid/widget/Editor;->mInputType:I

    #@23
    goto :goto_19
.end method

.method private setKeyListenerOnly(Landroid/text/method/KeyListener;)V
    .registers 4
    .parameter "input"

    #@0
    .prologue
    .line 1688
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_7

    #@4
    if-nez p1, :cond_7

    #@6
    .line 1699
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1690
    :cond_7
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@a
    .line 1691
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@e
    if-eq v0, p1, :cond_6

    #@10
    .line 1692
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@12
    iput-object p1, v0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@14
    .line 1693
    if-eqz p1, :cond_21

    #@16
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@18
    instance-of v0, v0, Landroid/text/Editable;

    #@1a
    if-nez v0, :cond_21

    #@1c
    .line 1694
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1e
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@21
    .line 1697
    :cond_21
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@23
    check-cast v0, Landroid/text/Editable;

    #@25
    iget-object v1, p0, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@27
    invoke-direct {p0, v0, v1}, Landroid/widget/TextView;->setFilters(Landroid/text/Editable;[Landroid/text/InputFilter;)V

    #@2a
    goto :goto_6
.end method

.method private setPrimaryClip(Landroid/content/ClipData;)V
    .registers 5
    .parameter "clip"

    #@0
    .prologue
    .line 8751
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    const-string v2, "clipboard"

    #@6
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/content/ClipboardManager;

    #@c
    .line 8753
    .local v0, clipboard:Landroid/content/ClipboardManager;
    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    #@f
    .line 8754
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@12
    move-result-wide v1

    #@13
    sput-wide v1, Landroid/widget/TextView;->LAST_CUT_OR_COPY_TIME:J

    #@15
    .line 8755
    return-void
.end method

.method private setRawTextSize(F)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 2577
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    #@5
    move-result v0

    #@6
    cmpl-float v0, p1, v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    .line 2578
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@c
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    #@f
    .line 2580
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@11
    if-eqz v0, :cond_1c

    #@13
    .line 2581
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@16
    .line 2582
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@19
    .line 2583
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1c
    .line 2586
    :cond_1c
    return-void
.end method

.method private setRelativeDrawablesIfNeeded(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 10
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1458
    if-nez p1, :cond_5

    #@3
    if-eqz p2, :cond_62

    #@5
    :cond_5
    const/4 v2, 0x1

    #@6
    .line 1459
    .local v2, hasRelativeDrawables:Z
    :goto_6
    if-eqz v2, :cond_61

    #@8
    .line 1460
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@a
    .line 1461
    .local v1, dr:Landroid/widget/TextView$Drawables;
    if-nez v1, :cond_13

    #@c
    .line 1462
    new-instance v1, Landroid/widget/TextView$Drawables;

    #@e
    .end local v1           #dr:Landroid/widget/TextView$Drawables;
    invoke-direct {v1}, Landroid/widget/TextView$Drawables;-><init>()V

    #@11
    .restart local v1       #dr:Landroid/widget/TextView$Drawables;
    iput-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@13
    .line 1464
    :cond_13
    iget-object v0, v1, Landroid/widget/TextView$Drawables;->mCompoundRect:Landroid/graphics/Rect;

    #@15
    .line 1465
    .local v0, compoundRect:Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@18
    move-result-object v3

    #@19
    .line 1466
    .local v3, state:[I
    if-eqz p1, :cond_64

    #@1b
    .line 1467
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1e
    move-result v5

    #@1f
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@22
    move-result v6

    #@23
    invoke-virtual {p1, v4, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@26
    .line 1468
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@29
    .line 1469
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@2c
    .line 1470
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@2f
    .line 1472
    iput-object p1, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@31
    .line 1473
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@34
    move-result v5

    #@35
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@37
    .line 1474
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@3a
    move-result v5

    #@3b
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@3d
    .line 1478
    :goto_3d
    if-eqz p2, :cond_69

    #@3f
    .line 1479
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@42
    move-result v5

    #@43
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@46
    move-result v6

    #@47
    invoke-virtual {p2, v4, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@4a
    .line 1480
    invoke-virtual {p2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@4d
    .line 1481
    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@50
    .line 1482
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@53
    .line 1484
    iput-object p2, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@55
    .line 1485
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@58
    move-result v4

    #@59
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@5b
    .line 1486
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@5e
    move-result v4

    #@5f
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@61
    .line 1491
    .end local v0           #compoundRect:Landroid/graphics/Rect;
    .end local v1           #dr:Landroid/widget/TextView$Drawables;
    .end local v3           #state:[I
    :cond_61
    :goto_61
    return-void

    #@62
    .end local v2           #hasRelativeDrawables:Z
    :cond_62
    move v2, v4

    #@63
    .line 1458
    goto :goto_6

    #@64
    .line 1476
    .restart local v0       #compoundRect:Landroid/graphics/Rect;
    .restart local v1       #dr:Landroid/widget/TextView$Drawables;
    .restart local v2       #hasRelativeDrawables:Z
    .restart local v3       #state:[I
    :cond_64
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@66
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@68
    goto :goto_3d

    #@69
    .line 1488
    :cond_69
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@6b
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@6d
    goto :goto_61
.end method

.method private setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;ZI)V
    .registers 27
    .parameter "text"
    .parameter "type"
    .parameter "notifyBefore"
    .parameter "oldlen"

    #@0
    .prologue
    .line 3743
    if-nez p1, :cond_4

    #@2
    .line 3744
    const-string p1, ""

    #@4
    .line 3748
    :cond_4
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isSuggestionsEnabled()Z

    #@7
    move-result v4

    #@8
    if-nez v4, :cond_e

    #@a
    .line 3749
    invoke-virtual/range {p0 .. p1}, Landroid/widget/TextView;->removeSuggestionSpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@d
    move-result-object p1

    #@e
    .line 3752
    :cond_e
    move-object/from16 v0, p0

    #@10
    iget-boolean v4, v0, Landroid/widget/TextView;->mUserSetTextScaleX:Z

    #@12
    if-nez v4, :cond_1d

    #@14
    move-object/from16 v0, p0

    #@16
    iget-object v4, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@18
    const/high16 v5, 0x3f80

    #@1a
    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextScaleX(F)V

    #@1d
    .line 3754
    :cond_1d
    move-object/from16 v0, p1

    #@1f
    instance-of v4, v0, Landroid/text/Spanned;

    #@21
    if-eqz v4, :cond_4f

    #@23
    move-object/from16 v4, p1

    #@25
    check-cast v4, Landroid/text/Spanned;

    #@27
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@29
    invoke-interface {v4, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@2c
    move-result v4

    #@2d
    if-ltz v4, :cond_4f

    #@2f
    .line 3756
    move-object/from16 v0, p0

    #@31
    iget-object v4, v0, Landroid/widget/TextView;->mContext:Landroid/content/Context;

    #@33
    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->isFadingMarqueeEnabled()Z

    #@3a
    move-result v4

    #@3b
    if-eqz v4, :cond_73

    #@3d
    .line 3757
    const/4 v4, 0x1

    #@3e
    move-object/from16 v0, p0

    #@40
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    #@43
    .line 3758
    const/4 v4, 0x0

    #@44
    move-object/from16 v0, p0

    #@46
    iput v4, v0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@48
    .line 3763
    :goto_48
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@4a
    move-object/from16 v0, p0

    #@4c
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    #@4f
    .line 3766
    :cond_4f
    move-object/from16 v0, p0

    #@51
    iget-object v4, v0, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@53
    array-length v14, v4

    #@54
    .line 3767
    .local v14, n:I
    const/4 v12, 0x0

    #@55
    .local v12, i:I
    :goto_55
    if-ge v12, v14, :cond_7f

    #@57
    .line 3768
    move-object/from16 v0, p0

    #@59
    iget-object v4, v0, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@5b
    aget-object v4, v4, v12

    #@5d
    const/4 v6, 0x0

    #@5e
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@61
    move-result v7

    #@62
    sget-object v8, Landroid/widget/TextView;->EMPTY_SPANNED:Landroid/text/Spanned;

    #@64
    const/4 v9, 0x0

    #@65
    const/4 v10, 0x0

    #@66
    move-object/from16 v5, p1

    #@68
    invoke-interface/range {v4 .. v10}, Landroid/text/InputFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    #@6b
    move-result-object v16

    #@6c
    .line 3769
    .local v16, out:Ljava/lang/CharSequence;
    if-eqz v16, :cond_70

    #@6e
    .line 3770
    move-object/from16 p1, v16

    #@70
    .line 3767
    :cond_70
    add-int/lit8 v12, v12, 0x1

    #@72
    goto :goto_55

    #@73
    .line 3760
    .end local v12           #i:I
    .end local v14           #n:I
    .end local v16           #out:Ljava/lang/CharSequence;
    :cond_73
    const/4 v4, 0x0

    #@74
    move-object/from16 v0, p0

    #@76
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    #@79
    .line 3761
    const/4 v4, 0x1

    #@7a
    move-object/from16 v0, p0

    #@7c
    iput v4, v0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@7e
    goto :goto_48

    #@7f
    .line 3774
    .restart local v12       #i:I
    .restart local v14       #n:I
    :cond_7f
    if-eqz p3, :cond_9f

    #@81
    .line 3775
    move-object/from16 v0, p0

    #@83
    iget-object v4, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@85
    if-eqz v4, :cond_176

    #@87
    .line 3776
    move-object/from16 v0, p0

    #@89
    iget-object v4, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@8b
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@8e
    move-result p4

    #@8f
    .line 3777
    move-object/from16 v0, p0

    #@91
    iget-object v4, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@93
    const/4 v5, 0x0

    #@94
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@97
    move-result v6

    #@98
    move-object/from16 v0, p0

    #@9a
    move/from16 v1, p4

    #@9c
    invoke-direct {v0, v4, v5, v1, v6}, Landroid/widget/TextView;->sendBeforeTextChanged(Ljava/lang/CharSequence;III)V

    #@9f
    .line 3783
    :cond_9f
    :goto_9f
    const/4 v15, 0x0

    #@a0
    .line 3785
    .local v15, needEditableForNotification:Z
    move-object/from16 v0, p0

    #@a2
    iget-object v4, v0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@a4
    if-eqz v4, :cond_b1

    #@a6
    move-object/from16 v0, p0

    #@a8
    iget-object v4, v0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@aa
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@ad
    move-result v4

    #@ae
    if-eqz v4, :cond_b1

    #@b0
    .line 3786
    const/4 v15, 0x1

    #@b1
    .line 3789
    :cond_b1
    sget-object v4, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@b3
    move-object/from16 v0, p2

    #@b5
    if-eq v0, v4, :cond_bf

    #@b7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@ba
    move-result-object v4

    #@bb
    if-nez v4, :cond_bf

    #@bd
    if-eqz v15, :cond_185

    #@bf
    .line 3791
    :cond_bf
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@c2
    .line 3792
    move-object/from16 v0, p0

    #@c4
    iget-object v4, v0, Landroid/widget/TextView;->mEditableFactory:Landroid/text/Editable$Factory;

    #@c6
    move-object/from16 v0, p1

    #@c8
    invoke-virtual {v4, v0}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@cb
    move-result-object v19

    #@cc
    .line 3793
    .local v19, t:Landroid/text/Editable;
    move-object/from16 p1, v19

    #@ce
    .line 3794
    move-object/from16 v0, p0

    #@d0
    iget-object v4, v0, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@d2
    move-object/from16 v0, p0

    #@d4
    move-object/from16 v1, v19

    #@d6
    invoke-direct {v0, v1, v4}, Landroid/widget/TextView;->setFilters(Landroid/text/Editable;[Landroid/text/InputFilter;)V

    #@d9
    .line 3795
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@dc
    move-result-object v13

    #@dd
    .line 3796
    .local v13, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v13, :cond_e4

    #@df
    move-object/from16 v0, p0

    #@e1
    invoke-virtual {v13, v0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@e4
    .line 3803
    .end local v13           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v19           #t:Landroid/text/Editable;
    :cond_e4
    :goto_e4
    move-object/from16 v0, p0

    #@e6
    iget v4, v0, Landroid/widget/TextView;->mAutoLinkMask:I

    #@e8
    if-eqz v4, :cond_12b

    #@ea
    .line 3806
    sget-object v4, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@ec
    move-object/from16 v0, p2

    #@ee
    if-eq v0, v4, :cond_f6

    #@f0
    move-object/from16 v0, p1

    #@f2
    instance-of v4, v0, Landroid/text/Spannable;

    #@f4
    if-eqz v4, :cond_1a9

    #@f6
    :cond_f6
    move-object/from16 v17, p1

    #@f8
    .line 3807
    check-cast v17, Landroid/text/Spannable;

    #@fa
    .line 3812
    .local v17, s2:Landroid/text/Spannable;
    :goto_fa
    move-object/from16 v0, p0

    #@fc
    iget v4, v0, Landroid/widget/TextView;->mAutoLinkMask:I

    #@fe
    move-object/from16 v0, v17

    #@100
    invoke-static {v0, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    #@103
    move-result v4

    #@104
    if-eqz v4, :cond_12b

    #@106
    .line 3813
    move-object/from16 p1, v17

    #@108
    .line 3814
    sget-object v4, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@10a
    move-object/from16 v0, p2

    #@10c
    if-ne v0, v4, :cond_1b5

    #@10e
    sget-object p2, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@110
    .line 3821
    :goto_110
    move-object/from16 v0, p1

    #@112
    move-object/from16 v1, p0

    #@114
    iput-object v0, v1, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@116
    .line 3825
    move-object/from16 v0, p0

    #@118
    iget-boolean v4, v0, Landroid/widget/TextView;->mLinksClickable:Z

    #@11a
    if-eqz v4, :cond_12b

    #@11c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->textCanBeSelected()Z

    #@11f
    move-result v4

    #@120
    if-nez v4, :cond_12b

    #@122
    .line 3826
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    #@125
    move-result-object v4

    #@126
    move-object/from16 v0, p0

    #@128
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    #@12b
    .line 3831
    .end local v17           #s2:Landroid/text/Spannable;
    :cond_12b
    move-object/from16 v0, p2

    #@12d
    move-object/from16 v1, p0

    #@12f
    iput-object v0, v1, Landroid/widget/TextView;->mBufferType:Landroid/widget/TextView$BufferType;

    #@131
    .line 3832
    move-object/from16 v0, p1

    #@133
    move-object/from16 v1, p0

    #@135
    iput-object v0, v1, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@137
    .line 3834
    move-object/from16 v0, p0

    #@139
    iget-object v4, v0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@13b
    if-nez v4, :cond_1b9

    #@13d
    .line 3835
    move-object/from16 v0, p1

    #@13f
    move-object/from16 v1, p0

    #@141
    iput-object v0, v1, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@143
    .line 3840
    :goto_143
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@146
    move-result v20

    #@147
    .line 3842
    .local v20, textLength:I
    move-object/from16 v0, p1

    #@149
    instance-of v4, v0, Landroid/text/Spannable;

    #@14b
    if-eqz v4, :cond_22f

    #@14d
    move-object/from16 v0, p0

    #@14f
    iget-boolean v4, v0, Landroid/widget/TextView;->mAllowTransformationLengthChange:Z

    #@151
    if-nez v4, :cond_22f

    #@153
    move-object/from16 v18, p1

    #@155
    .line 3843
    check-cast v18, Landroid/text/Spannable;

    #@157
    .line 3846
    .local v18, sp:Landroid/text/Spannable;
    const/4 v4, 0x0

    #@158
    invoke-interface/range {v18 .. v18}, Landroid/text/Spannable;->length()I

    #@15b
    move-result v5

    #@15c
    const-class v6, Landroid/widget/TextView$ChangeWatcher;

    #@15e
    move-object/from16 v0, v18

    #@160
    invoke-interface {v0, v4, v5, v6}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@163
    move-result-object v21

    #@164
    check-cast v21, [Landroid/widget/TextView$ChangeWatcher;

    #@166
    .line 3847
    .local v21, watchers:[Landroid/widget/TextView$ChangeWatcher;
    move-object/from16 v0, v21

    #@168
    array-length v11, v0

    #@169
    .line 3848
    .local v11, count:I
    const/4 v12, 0x0

    #@16a
    :goto_16a
    if-ge v12, v11, :cond_1cb

    #@16c
    .line 3849
    aget-object v4, v21, v12

    #@16e
    move-object/from16 v0, v18

    #@170
    invoke-interface {v0, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@173
    .line 3848
    add-int/lit8 v12, v12, 0x1

    #@175
    goto :goto_16a

    #@176
    .line 3779
    .end local v11           #count:I
    .end local v15           #needEditableForNotification:Z
    .end local v18           #sp:Landroid/text/Spannable;
    .end local v20           #textLength:I
    .end local v21           #watchers:[Landroid/widget/TextView$ChangeWatcher;
    :cond_176
    const-string v4, ""

    #@178
    const/4 v5, 0x0

    #@179
    const/4 v6, 0x0

    #@17a
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@17d
    move-result v7

    #@17e
    move-object/from16 v0, p0

    #@180
    invoke-direct {v0, v4, v5, v6, v7}, Landroid/widget/TextView;->sendBeforeTextChanged(Ljava/lang/CharSequence;III)V

    #@183
    goto/16 :goto_9f

    #@185
    .line 3797
    .restart local v15       #needEditableForNotification:Z
    :cond_185
    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@187
    move-object/from16 v0, p2

    #@189
    if-eq v0, v4, :cond_191

    #@18b
    move-object/from16 v0, p0

    #@18d
    iget-object v4, v0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@18f
    if-eqz v4, :cond_19d

    #@191
    .line 3798
    :cond_191
    move-object/from16 v0, p0

    #@193
    iget-object v4, v0, Landroid/widget/TextView;->mSpannableFactory:Landroid/text/Spannable$Factory;

    #@195
    move-object/from16 v0, p1

    #@197
    invoke-virtual {v4, v0}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    #@19a
    move-result-object p1

    #@19b
    goto/16 :goto_e4

    #@19d
    .line 3799
    :cond_19d
    move-object/from16 v0, p1

    #@19f
    instance-of v4, v0, Landroid/widget/TextView$CharWrapper;

    #@1a1
    if-nez v4, :cond_e4

    #@1a3
    .line 3800
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->stringOrSpannedString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@1a6
    move-result-object p1

    #@1a7
    goto/16 :goto_e4

    #@1a9
    .line 3809
    :cond_1a9
    move-object/from16 v0, p0

    #@1ab
    iget-object v4, v0, Landroid/widget/TextView;->mSpannableFactory:Landroid/text/Spannable$Factory;

    #@1ad
    move-object/from16 v0, p1

    #@1af
    invoke-virtual {v4, v0}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    #@1b2
    move-result-object v17

    #@1b3
    .restart local v17       #s2:Landroid/text/Spannable;
    goto/16 :goto_fa

    #@1b5
    .line 3814
    :cond_1b5
    sget-object p2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@1b7
    goto/16 :goto_110

    #@1b9
    .line 3837
    .end local v17           #s2:Landroid/text/Spannable;
    :cond_1b9
    move-object/from16 v0, p0

    #@1bb
    iget-object v4, v0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@1bd
    move-object/from16 v0, p1

    #@1bf
    move-object/from16 v1, p0

    #@1c1
    invoke-interface {v4, v0, v1}, Landroid/text/method/TransformationMethod;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    #@1c4
    move-result-object v4

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    iput-object v4, v0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@1c9
    goto/16 :goto_143

    #@1cb
    .line 3852
    .restart local v11       #count:I
    .restart local v18       #sp:Landroid/text/Spannable;
    .restart local v20       #textLength:I
    .restart local v21       #watchers:[Landroid/widget/TextView$ChangeWatcher;
    :cond_1cb
    move-object/from16 v0, p0

    #@1cd
    iget-object v4, v0, Landroid/widget/TextView;->mChangeWatcher:Landroid/widget/TextView$ChangeWatcher;

    #@1cf
    if-nez v4, :cond_1dd

    #@1d1
    new-instance v4, Landroid/widget/TextView$ChangeWatcher;

    #@1d3
    const/4 v5, 0x0

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    invoke-direct {v4, v0, v5}, Landroid/widget/TextView$ChangeWatcher;-><init>(Landroid/widget/TextView;Landroid/widget/TextView$1;)V

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iput-object v4, v0, Landroid/widget/TextView;->mChangeWatcher:Landroid/widget/TextView$ChangeWatcher;

    #@1dd
    .line 3854
    :cond_1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v4, v0, Landroid/widget/TextView;->mChangeWatcher:Landroid/widget/TextView$ChangeWatcher;

    #@1e1
    const/4 v5, 0x0

    #@1e2
    const v6, 0x640012

    #@1e5
    move-object/from16 v0, v18

    #@1e7
    move/from16 v1, v20

    #@1e9
    invoke-interface {v0, v4, v5, v1, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@1ec
    .line 3857
    move-object/from16 v0, p0

    #@1ee
    iget-object v4, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1f0
    if-eqz v4, :cond_1fb

    #@1f2
    move-object/from16 v0, p0

    #@1f4
    iget-object v4, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1f6
    move-object/from16 v0, v18

    #@1f8
    invoke-virtual {v4, v0}, Landroid/widget/Editor;->addSpanWatchers(Landroid/text/Spannable;)V

    #@1fb
    .line 3859
    :cond_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v4, v0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@1ff
    if-eqz v4, :cond_20f

    #@201
    .line 3860
    move-object/from16 v0, p0

    #@203
    iget-object v4, v0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@205
    const/4 v5, 0x0

    #@206
    const/16 v6, 0x12

    #@208
    move-object/from16 v0, v18

    #@20a
    move/from16 v1, v20

    #@20c
    invoke-interface {v0, v4, v5, v1, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@20f
    .line 3863
    :cond_20f
    move-object/from16 v0, p0

    #@211
    iget-object v4, v0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@213
    if-eqz v4, :cond_22f

    #@215
    .line 3864
    move-object/from16 v0, p0

    #@217
    iget-object v5, v0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@219
    move-object/from16 v4, p1

    #@21b
    check-cast v4, Landroid/text/Spannable;

    #@21d
    move-object/from16 v0, p0

    #@21f
    invoke-interface {v5, v0, v4}, Landroid/text/method/MovementMethod;->initialize(Landroid/widget/TextView;Landroid/text/Spannable;)V

    #@222
    .line 3871
    move-object/from16 v0, p0

    #@224
    iget-object v4, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@226
    if-eqz v4, :cond_22f

    #@228
    move-object/from16 v0, p0

    #@22a
    iget-object v4, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@22c
    const/4 v5, 0x0

    #@22d
    iput-boolean v5, v4, Landroid/widget/Editor;->mSelectionMoved:Z

    #@22f
    .line 3875
    .end local v11           #count:I
    .end local v18           #sp:Landroid/text/Spannable;
    .end local v21           #watchers:[Landroid/widget/TextView$ChangeWatcher;
    :cond_22f
    move-object/from16 v0, p0

    #@231
    iget-object v4, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@233
    if-eqz v4, :cond_238

    #@235
    .line 3876
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->checkForRelayout()V

    #@238
    .line 3879
    :cond_238
    const/4 v4, 0x0

    #@239
    move-object/from16 v0, p0

    #@23b
    move-object/from16 v1, p1

    #@23d
    move/from16 v2, p4

    #@23f
    move/from16 v3, v20

    #@241
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/TextView;->sendOnTextChanged(Ljava/lang/CharSequence;III)V

    #@244
    .line 3880
    const/4 v4, 0x0

    #@245
    move-object/from16 v0, p0

    #@247
    move-object/from16 v1, p1

    #@249
    move/from16 v2, p4

    #@24b
    move/from16 v3, v20

    #@24d
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    #@250
    .line 3882
    if-eqz v15, :cond_257

    #@252
    .line 3883
    check-cast p1, Landroid/text/Editable;

    #@254
    .end local p1
    invoke-virtual/range {p0 .. p1}, Landroid/widget/TextView;->sendAfterTextChanged(Landroid/text/Editable;)V

    #@257
    .line 3887
    :cond_257
    move-object/from16 v0, p0

    #@259
    iget-object v4, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@25b
    if-eqz v4, :cond_264

    #@25d
    move-object/from16 v0, p0

    #@25f
    iget-object v4, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@261
    invoke-virtual {v4}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@264
    .line 3888
    :cond_264
    return-void
.end method

.method private setTextViewTypeface(Ljava/lang/String;II)V
    .registers 7
    .parameter "familyName"
    .parameter "typefaceIndex"
    .parameter "styleIndex"

    #@0
    .prologue
    .line 1357
    iput-object p1, p0, Landroid/widget/TextView;->mFontFamily:Ljava/lang/String;

    #@2
    .line 1358
    iput p2, p0, Landroid/widget/TextView;->mTypefaceIndex:I

    #@4
    .line 1359
    iput p3, p0, Landroid/widget/TextView;->mStyleIndex:I

    #@6
    .line 1361
    :try_start_6
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_19

    #@c
    .line 1362
    iget-object v1, p0, Landroid/widget/TextView;->mCurConfig:Landroid/content/res/Configuration;

    #@e
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@11
    move-result-object v2

    #@12
    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_19} :catch_1a

    #@19
    .line 1367
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 1364
    :catch_1a
    move-exception v0

    #@1b
    .line 1365
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@1e
    goto :goto_19
.end method

.method private setTypefaceFromAttrs(Ljava/lang/String;II)V
    .registers 6
    .parameter "familyName"
    .parameter "typefaceIndex"
    .parameter "styleIndex"

    #@0
    .prologue
    .line 1428
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 1429
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;->setTextViewTypeface(Ljava/lang/String;II)V

    #@7
    .line 1432
    :cond_7
    const/4 v0, 0x0

    #@8
    .line 1433
    .local v0, tf:Landroid/graphics/Typeface;
    if-eqz p1, :cond_14

    #@a
    .line 1434
    invoke-static {p1, p3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@d
    move-result-object v0

    #@e
    .line 1435
    if-eqz v0, :cond_14

    #@10
    .line 1436
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@13
    .line 1455
    :goto_13
    return-void

    #@14
    .line 1440
    :cond_14
    packed-switch p2, :pswitch_data_24

    #@17
    .line 1454
    :goto_17
    invoke-virtual {p0, v0, p3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    #@1a
    goto :goto_13

    #@1b
    .line 1442
    :pswitch_1b
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@1d
    .line 1443
    goto :goto_17

    #@1e
    .line 1446
    :pswitch_1e
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    #@20
    .line 1447
    goto :goto_17

    #@21
    .line 1450
    :pswitch_21
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    #@23
    goto :goto_17

    #@24
    .line 1440
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
    .end packed-switch
.end method

.method private shouldAdvanceFocusOnEnter()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 5519
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@5
    move-result-object v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 5536
    :cond_8
    :goto_8
    return v1

    #@9
    .line 5523
    :cond_9
    iget-boolean v3, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@b
    if-eqz v3, :cond_f

    #@d
    move v1, v2

    #@e
    .line 5524
    goto :goto_8

    #@f
    .line 5527
    :cond_f
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@11
    if-eqz v3, :cond_8

    #@13
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@15
    iget v3, v3, Landroid/widget/Editor;->mInputType:I

    #@17
    and-int/lit8 v3, v3, 0xf

    #@19
    if-ne v3, v2, :cond_8

    #@1b
    .line 5529
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1d
    iget v3, v3, Landroid/widget/Editor;->mInputType:I

    #@1f
    and-int/lit16 v0, v3, 0xff0

    #@21
    .line 5530
    .local v0, variation:I
    const/16 v3, 0x20

    #@23
    if-eq v0, v3, :cond_29

    #@25
    const/16 v3, 0x30

    #@27
    if-ne v0, v3, :cond_8

    #@29
    :cond_29
    move v1, v2

    #@2a
    .line 5532
    goto :goto_8
.end method

.method private shouldAdvanceFocusOnTab()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 5544
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@4
    move-result-object v2

    #@5
    if-eqz v2, :cond_26

    #@7
    iget-boolean v2, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@9
    if-nez v2, :cond_26

    #@b
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@d
    if-eqz v2, :cond_26

    #@f
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@11
    iget v2, v2, Landroid/widget/Editor;->mInputType:I

    #@13
    and-int/lit8 v2, v2, 0xf

    #@15
    if-ne v2, v1, :cond_26

    #@17
    .line 5546
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@19
    iget v2, v2, Landroid/widget/Editor;->mInputType:I

    #@1b
    and-int/lit16 v0, v2, 0xff0

    #@1d
    .line 5547
    .local v0, variation:I
    const/high16 v2, 0x4

    #@1f
    if-eq v0, v2, :cond_25

    #@21
    const/high16 v2, 0x2

    #@23
    if-ne v0, v2, :cond_26

    #@25
    .line 5549
    :cond_25
    const/4 v1, 0x0

    #@26
    .line 5552
    .end local v0           #variation:I
    :cond_26
    return v1
.end method

.method private shouldSpeakPasswordsForAccessibility()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 8253
    iget-object v2, p0, Landroid/widget/TextView;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string/jumbo v3, "speak_password"

    #@b
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e
    move-result v2

    #@f
    if-ne v2, v0, :cond_12

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    move v0, v1

    #@13
    goto :goto_11
.end method

.method private startMarquee()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 7383
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@4
    move-result-object v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 7407
    :cond_7
    :goto_7
    return-void

    #@8
    .line 7385
    :cond_8
    invoke-virtual {p0}, Landroid/widget/TextView;->getWidth()I

    #@b
    move-result v1

    #@c
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@f
    move-result v2

    #@10
    sub-int/2addr v1, v2

    #@11
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@14
    move-result v2

    #@15
    sub-int/2addr v1, v2

    #@16
    int-to-float v1, v1

    #@17
    invoke-direct {p0, v1}, Landroid/widget/TextView;->compressText(F)Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_7

    #@1d
    .line 7390
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@1f
    if-eqz v1, :cond_29

    #@21
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@23
    invoke-virtual {v1}, Landroid/widget/TextView$Marquee;->isStopped()Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_7

    #@29
    :cond_29
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_3b

    #@2f
    invoke-virtual {p0}, Landroid/widget/TextView;->isSelected()Z

    #@32
    move-result v1

    #@33
    if-nez v1, :cond_3b

    #@35
    invoke-virtual {p0}, Landroid/widget/TextView;->isMarqueeAlwaysEnable()Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_7

    #@3b
    :cond_3b
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    #@3e
    move-result v1

    #@3f
    if-ne v1, v3, :cond_7

    #@41
    invoke-direct {p0}, Landroid/widget/TextView;->canMarquee()Z

    #@44
    move-result v1

    #@45
    if-eqz v1, :cond_7

    #@47
    .line 7393
    iget v1, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@49
    if-ne v1, v3, :cond_5f

    #@4b
    .line 7394
    const/4 v1, 0x2

    #@4c
    iput v1, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@4e
    .line 7395
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@50
    .line 7396
    .local v0, tmp:Landroid/text/Layout;
    iget-object v1, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@52
    iput-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@54
    .line 7397
    iput-object v0, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@56
    .line 7398
    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    #@59
    .line 7399
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@5c
    .line 7400
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@5f
    .line 7403
    .end local v0           #tmp:Landroid/text/Layout;
    :cond_5f
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@61
    if-nez v1, :cond_6a

    #@63
    new-instance v1, Landroid/widget/TextView$Marquee;

    #@65
    invoke-direct {v1, p0}, Landroid/widget/TextView$Marquee;-><init>(Landroid/widget/TextView;)V

    #@68
    iput-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@6a
    .line 7404
    :cond_6a
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@6c
    iget v2, p0, Landroid/widget/TextView;->mMarqueeRepeatLimit:I

    #@6e
    invoke-virtual {v1, v2}, Landroid/widget/TextView$Marquee;->start(I)V

    #@71
    goto :goto_7
.end method

.method private startStopMarquee(Z)V
    .registers 4
    .parameter "start"

    #@0
    .prologue
    .line 7426
    iget-object v0, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@2
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@4
    if-ne v0, v1, :cond_b

    #@6
    .line 7427
    if-eqz p1, :cond_c

    #@8
    .line 7428
    invoke-direct {p0}, Landroid/widget/TextView;->startMarquee()V

    #@b
    .line 7433
    :cond_b
    :goto_b
    return-void

    #@c
    .line 7430
    :cond_c
    invoke-direct {p0}, Landroid/widget/TextView;->stopMarquee()V

    #@f
    goto :goto_b
.end method

.method private stopMarquee()V
    .registers 4

    #@0
    .prologue
    .line 7410
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@2
    if-eqz v1, :cond_11

    #@4
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@6
    invoke-virtual {v1}, Landroid/widget/TextView$Marquee;->isStopped()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_11

    #@c
    .line 7411
    iget-object v1, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@e
    invoke-virtual {v1}, Landroid/widget/TextView$Marquee;->stop()V

    #@11
    .line 7414
    :cond_11
    iget v1, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@13
    const/4 v2, 0x2

    #@14
    if-ne v1, v2, :cond_2b

    #@16
    .line 7415
    const/4 v1, 0x1

    #@17
    iput v1, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@19
    .line 7416
    iget-object v0, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@1b
    .line 7417
    .local v0, tmp:Landroid/text/Layout;
    iget-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@1d
    iput-object v1, p0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@1f
    .line 7418
    iput-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@21
    .line 7419
    const/4 v1, 0x0

    #@22
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    #@25
    .line 7420
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@28
    .line 7421
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@2b
    .line 7423
    .end local v0           #tmp:Landroid/text/Layout;
    :cond_2b
    return-void
.end method

.method private updateActionModeTitle()V
    .registers 6

    #@0
    .prologue
    .line 8771
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 8786
    :cond_4
    :goto_4
    return-void

    #@5
    .line 8774
    :cond_5
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7
    iget-object v2, v2, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@9
    if-eqz v2, :cond_4

    #@b
    .line 8777
    const/4 v1, 0x0

    #@c
    .line 8778
    .local v1, showTitle:Z
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@f
    move-result-object v0

    #@10
    .line 8780
    .local v0, context:Landroid/content/Context;
    if-eqz v0, :cond_4

    #@12
    .line 8781
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15
    move-result-object v2

    #@16
    const v3, 0x111003e

    #@19
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1c
    move-result v1

    #@1d
    .line 8782
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1f
    iget-object v3, v2, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@21
    if-eqz v1, :cond_30

    #@23
    iget-object v2, p0, Landroid/widget/TextView;->mContext:Landroid/content/Context;

    #@25
    const v4, 0x10403eb

    #@28
    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    :goto_2c
    invoke-virtual {v3, v2}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    #@2f
    goto :goto_4

    #@30
    :cond_30
    const/4 v2, 0x0

    #@31
    goto :goto_2c
.end method

.method private updateTextColors()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3459
    const/4 v1, 0x0

    #@2
    .line 3460
    .local v1, inval:Z
    iget-object v2, p0, Landroid/widget/TextView;->mTextColor:Landroid/content/res/ColorStateList;

    #@4
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v2, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@b
    move-result v0

    #@c
    .line 3461
    .local v0, color:I
    iget v2, p0, Landroid/widget/TextView;->mCurTextColor:I

    #@e
    if-eq v0, v2, :cond_13

    #@10
    .line 3462
    iput v0, p0, Landroid/widget/TextView;->mCurTextColor:I

    #@12
    .line 3463
    const/4 v1, 0x1

    #@13
    .line 3465
    :cond_13
    iget-object v2, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@15
    if-eqz v2, :cond_2c

    #@17
    .line 3466
    iget-object v2, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@19
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v2, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@20
    move-result v0

    #@21
    .line 3467
    iget-object v2, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@23
    iget v2, v2, Landroid/text/TextPaint;->linkColor:I

    #@25
    if-eq v0, v2, :cond_2c

    #@27
    .line 3468
    iget-object v2, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@29
    iput v0, v2, Landroid/text/TextPaint;->linkColor:I

    #@2b
    .line 3469
    const/4 v1, 0x1

    #@2c
    .line 3472
    :cond_2c
    iget-object v2, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@2e
    if-eqz v2, :cond_49

    #@30
    .line 3473
    iget-object v2, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@32
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v2, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@39
    move-result v0

    #@3a
    .line 3474
    iget v2, p0, Landroid/widget/TextView;->mCurHintTextColor:I

    #@3c
    if-eq v0, v2, :cond_49

    #@3e
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@40
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@43
    move-result v2

    #@44
    if-nez v2, :cond_49

    #@46
    .line 3475
    iput v0, p0, Landroid/widget/TextView;->mCurHintTextColor:I

    #@48
    .line 3476
    const/4 v1, 0x1

    #@49
    .line 3479
    :cond_49
    if-eqz v1, :cond_57

    #@4b
    .line 3481
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4d
    if-eqz v2, :cond_54

    #@4f
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@51
    invoke-virtual {v2}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@54
    .line 3482
    :cond_54
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@57
    .line 3484
    :cond_57
    return-void
.end method

.method private updateTextServicesLocaleAsync()V
    .registers 2

    #@0
    .prologue
    .line 8192
    new-instance v0, Landroid/widget/TextView$3;

    #@2
    invoke-direct {v0, p0}, Landroid/widget/TextView$3;-><init>(Landroid/widget/TextView;)V

    #@5
    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    #@8
    .line 8204
    return-void
.end method

.method private updateTextServicesLocaleLocked()V
    .registers 6

    #@0
    .prologue
    .line 8207
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    .line 8208
    .local v0, locale:Ljava/util/Locale;
    iget-object v3, p0, Landroid/widget/TextView;->mContext:Landroid/content/Context;

    #@6
    const-string/jumbo v4, "textservices"

    #@9
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Landroid/view/textservice/TextServicesManager;

    #@f
    .line 8210
    .local v2, textServicesManager:Landroid/view/textservice/TextServicesManager;
    const/4 v3, 0x1

    #@10
    invoke-virtual {v2, v3}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellCheckerSubtype(Z)Landroid/view/textservice/SpellCheckerSubtype;

    #@13
    move-result-object v1

    #@14
    .line 8211
    .local v1, subtype:Landroid/view/textservice/SpellCheckerSubtype;
    if-eqz v1, :cond_1e

    #@16
    .line 8212
    invoke-virtual {v1}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v3}, Landroid/view/textservice/SpellCheckerSubtype;->constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;

    #@1d
    move-result-object v0

    #@1e
    .line 8214
    :cond_1e
    iput-object v0, p0, Landroid/widget/TextView;->mCurrentTextServicesLocaleCache:Ljava/util/Locale;

    #@20
    .line 8215
    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .registers 3
    .parameter "watcher"

    #@0
    .prologue
    .line 7475
    iget-object v0, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 7476
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@b
    .line 7479
    :cond_b
    iget-object v0, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 7480
    return-void
.end method

.method public final append(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 3442
    const/4 v0, 0x0

    #@1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, p1, v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;II)V

    #@8
    .line 3443
    return-void
.end method

.method public append(Ljava/lang/CharSequence;II)V
    .registers 6
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 3451
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    instance-of v0, v0, Landroid/text/Editable;

    #@4
    if-nez v0, :cond_d

    #@6
    .line 3452
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@8
    sget-object v1, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@a
    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@d
    .line 3455
    :cond_d
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@f
    check-cast v0, Landroid/text/Editable;

    #@11
    invoke-interface {v0, p1, p2, p3}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;II)Landroid/text/Editable;

    #@14
    .line 3456
    return-void
.end method

.method public beginBatchEdit()V
    .registers 2

    #@0
    .prologue
    .line 5972
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor;->beginBatchEdit()V

    #@9
    .line 5973
    :cond_9
    return-void
.end method

.method public bringPointIntoView(I)Z
    .registers 30
    .parameter "offset"

    #@0
    .prologue
    .line 6851
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isLayoutRequested()Z

    #@3
    move-result v24

    #@4
    if-eqz v24, :cond_e

    #@6
    .line 6852
    move/from16 v0, p1

    #@8
    move-object/from16 v1, p0

    #@a
    iput v0, v1, Landroid/widget/TextView;->mDeferScroll:I

    #@c
    .line 6853
    const/4 v5, 0x0

    #@d
    .line 7020
    :cond_d
    :goto_d
    return v5

    #@e
    .line 6855
    :cond_e
    const/4 v5, 0x0

    #@f
    .line 6857
    .local v5, changed:Z
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->isShowingHint()Z

    #@12
    move-result v24

    #@13
    if-eqz v24, :cond_19f

    #@15
    move-object/from16 v0, p0

    #@17
    iget-object v15, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@19
    .line 6859
    .local v15, layout:Landroid/text/Layout;
    :goto_19
    if-eqz v15, :cond_d

    #@1b
    .line 6861
    move/from16 v0, p1

    #@1d
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@20
    move-result v17

    #@21
    .line 6864
    .local v17, line:I
    move/from16 v0, p1

    #@23
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@26
    move-result v24

    #@27
    move/from16 v0, v24

    #@29
    float-to-int v0, v0

    #@2a
    move/from16 v23, v0

    #@2c
    .line 6865
    .local v23, x:I
    move/from16 v0, v17

    #@2e
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@31
    move-result v19

    #@32
    .line 6866
    .local v19, top:I
    add-int/lit8 v24, v17, 0x1

    #@34
    move/from16 v0, v24

    #@36
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@39
    move-result v4

    #@3a
    .line 6868
    .local v4, bottom:I
    move/from16 v0, v17

    #@3c
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineLeft(I)F

    #@3f
    move-result v24

    #@40
    invoke-static/range {v24 .. v24}, Landroid/util/FloatMath;->floor(F)F

    #@43
    move-result v24

    #@44
    move/from16 v0, v24

    #@46
    float-to-int v0, v0

    #@47
    move/from16 v16, v0

    #@49
    .line 6869
    .local v16, left:I
    move/from16 v0, v17

    #@4b
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineRight(I)F

    #@4e
    move-result v24

    #@4f
    invoke-static/range {v24 .. v24}, Landroid/util/FloatMath;->ceil(F)F

    #@52
    move-result v24

    #@53
    move/from16 v0, v24

    #@55
    float-to-int v0, v0

    #@56
    move/from16 v18, v0

    #@58
    .line 6870
    .local v18, right:I
    invoke-virtual {v15}, Landroid/text/Layout;->getHeight()I

    #@5b
    move-result v14

    #@5c
    .line 6874
    .local v14, ht:I
    sget-object v24, Landroid/widget/TextView$5;->$SwitchMap$android$text$Layout$Alignment:[I

    #@5e
    move/from16 v0, v17

    #@60
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getParagraphAlignment(I)Landroid/text/Layout$Alignment;

    #@63
    move-result-object v25

    #@64
    invoke-virtual/range {v25 .. v25}, Landroid/text/Layout$Alignment;->ordinal()I

    #@67
    move-result v25

    #@68
    aget v24, v24, v25

    #@6a
    packed-switch v24, :pswitch_data_29c

    #@6d
    .line 6889
    const/4 v10, 0x0

    #@6e
    .line 6893
    .local v10, grav:I
    :goto_6e
    move-object/from16 v0, p0

    #@70
    iget v0, v0, Landroid/widget/TextView;->mRight:I

    #@72
    move/from16 v24, v0

    #@74
    move-object/from16 v0, p0

    #@76
    iget v0, v0, Landroid/widget/TextView;->mLeft:I

    #@78
    move/from16 v25, v0

    #@7a
    sub-int v24, v24, v25

    #@7c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@7f
    move-result v25

    #@80
    sub-int v24, v24, v25

    #@82
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@85
    move-result v25

    #@86
    sub-int v13, v24, v25

    #@88
    .line 6894
    .local v13, hspace:I
    move-object/from16 v0, p0

    #@8a
    iget v0, v0, Landroid/widget/TextView;->mBottom:I

    #@8c
    move/from16 v24, v0

    #@8e
    move-object/from16 v0, p0

    #@90
    iget v0, v0, Landroid/widget/TextView;->mTop:I

    #@92
    move/from16 v25, v0

    #@94
    sub-int v24, v24, v25

    #@96
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@99
    move-result v25

    #@9a
    sub-int v24, v24, v25

    #@9c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@9f
    move-result v25

    #@a0
    sub-int v22, v24, v25

    #@a2
    .line 6896
    .local v22, vspace:I
    sub-int v24, v4, v19

    #@a4
    div-int/lit8 v12, v24, 0x2

    #@a6
    .line 6897
    .local v12, hslack:I
    move/from16 v21, v12

    #@a8
    .line 6899
    .local v21, vslack:I
    div-int/lit8 v24, v22, 0x4

    #@aa
    move/from16 v0, v21

    #@ac
    move/from16 v1, v24

    #@ae
    if-le v0, v1, :cond_b2

    #@b0
    .line 6900
    div-int/lit8 v21, v22, 0x4

    #@b2
    .line 6901
    :cond_b2
    div-int/lit8 v24, v13, 0x4

    #@b4
    move/from16 v0, v24

    #@b6
    if-le v12, v0, :cond_ba

    #@b8
    .line 6902
    div-int/lit8 v12, v13, 0x4

    #@ba
    .line 6904
    :cond_ba
    move-object/from16 v0, p0

    #@bc
    iget v11, v0, Landroid/widget/TextView;->mScrollX:I

    #@be
    .line 6905
    .local v11, hs:I
    move-object/from16 v0, p0

    #@c0
    iget v0, v0, Landroid/widget/TextView;->mScrollY:I

    #@c2
    move/from16 v20, v0

    #@c4
    .line 6907
    .local v20, vs:I
    sub-int v24, v19, v20

    #@c6
    move/from16 v0, v24

    #@c8
    move/from16 v1, v21

    #@ca
    if-ge v0, v1, :cond_ce

    #@cc
    .line 6908
    sub-int v20, v19, v21

    #@ce
    .line 6909
    :cond_ce
    sub-int v24, v4, v20

    #@d0
    sub-int v25, v22, v21

    #@d2
    move/from16 v0, v24

    #@d4
    move/from16 v1, v25

    #@d6
    if-le v0, v1, :cond_dc

    #@d8
    .line 6910
    sub-int v24, v22, v21

    #@da
    sub-int v20, v4, v24

    #@dc
    .line 6911
    :cond_dc
    sub-int v24, v14, v20

    #@de
    move/from16 v0, v24

    #@e0
    move/from16 v1, v22

    #@e2
    if-ge v0, v1, :cond_e6

    #@e4
    .line 6912
    sub-int v20, v14, v22

    #@e6
    .line 6913
    :cond_e6
    rsub-int/lit8 v24, v20, 0x0

    #@e8
    if-lez v24, :cond_ec

    #@ea
    .line 6914
    const/16 v20, 0x0

    #@ec
    .line 6916
    :cond_ec
    if-eqz v10, :cond_104

    #@ee
    .line 6917
    sub-int v24, v23, v11

    #@f0
    move/from16 v0, v24

    #@f2
    if-ge v0, v12, :cond_f6

    #@f4
    .line 6918
    sub-int v11, v23, v12

    #@f6
    .line 6920
    :cond_f6
    sub-int v24, v23, v11

    #@f8
    sub-int v25, v13, v12

    #@fa
    move/from16 v0, v24

    #@fc
    move/from16 v1, v25

    #@fe
    if-le v0, v1, :cond_104

    #@100
    .line 6921
    sub-int v24, v13, v12

    #@102
    sub-int v11, v23, v24

    #@104
    .line 6925
    :cond_104
    if-gez v10, :cond_1be

    #@106
    .line 6926
    sub-int v24, v16, v11

    #@108
    if-lez v24, :cond_10c

    #@10a
    .line 6927
    move/from16 v11, v16

    #@10c
    .line 6928
    :cond_10c
    sub-int v24, v18, v11

    #@10e
    move/from16 v0, v24

    #@110
    if-ge v0, v13, :cond_114

    #@112
    .line 6929
    sub-int v11, v18, v13

    #@114
    .line 6976
    :cond_114
    :goto_114
    move-object/from16 v0, p0

    #@116
    iget v0, v0, Landroid/widget/TextView;->mScrollX:I

    #@118
    move/from16 v24, v0

    #@11a
    move/from16 v0, v24

    #@11c
    if-ne v11, v0, :cond_12a

    #@11e
    move-object/from16 v0, p0

    #@120
    iget v0, v0, Landroid/widget/TextView;->mScrollY:I

    #@122
    move/from16 v24, v0

    #@124
    move/from16 v0, v20

    #@126
    move/from16 v1, v24

    #@128
    if-eq v0, v1, :cond_13a

    #@12a
    .line 6977
    :cond_12a
    move-object/from16 v0, p0

    #@12c
    iget-object v0, v0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@12e
    move-object/from16 v24, v0

    #@130
    if-nez v24, :cond_224

    #@132
    .line 6978
    move-object/from16 v0, p0

    #@134
    move/from16 v1, v20

    #@136
    invoke-virtual {v0, v11, v1}, Landroid/widget/TextView;->scrollTo(II)V

    #@139
    .line 6999
    :goto_139
    const/4 v5, 0x1

    #@13a
    .line 7002
    :cond_13a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isFocused()Z

    #@13d
    move-result v24

    #@13e
    if-eqz v24, :cond_d

    #@140
    .line 7010
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Landroid/widget/TextView;->mTempRect:Landroid/graphics/Rect;

    #@144
    move-object/from16 v24, v0

    #@146
    if-nez v24, :cond_153

    #@148
    new-instance v24, Landroid/graphics/Rect;

    #@14a
    invoke-direct/range {v24 .. v24}, Landroid/graphics/Rect;-><init>()V

    #@14d
    move-object/from16 v0, v24

    #@14f
    move-object/from16 v1, p0

    #@151
    iput-object v0, v1, Landroid/widget/TextView;->mTempRect:Landroid/graphics/Rect;

    #@153
    .line 7011
    :cond_153
    move-object/from16 v0, p0

    #@155
    iget-object v0, v0, Landroid/widget/TextView;->mTempRect:Landroid/graphics/Rect;

    #@157
    move-object/from16 v24, v0

    #@159
    add-int/lit8 v25, v23, -0x2

    #@15b
    add-int/lit8 v26, v23, 0x2

    #@15d
    move-object/from16 v0, v24

    #@15f
    move/from16 v1, v25

    #@161
    move/from16 v2, v19

    #@163
    move/from16 v3, v26

    #@165
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@168
    .line 7012
    move-object/from16 v0, p0

    #@16a
    iget-object v0, v0, Landroid/widget/TextView;->mTempRect:Landroid/graphics/Rect;

    #@16c
    move-object/from16 v24, v0

    #@16e
    move-object/from16 v0, p0

    #@170
    move-object/from16 v1, v24

    #@172
    move/from16 v2, v17

    #@174
    invoke-direct {v0, v1, v2}, Landroid/widget/TextView;->getInterestingRect(Landroid/graphics/Rect;I)V

    #@177
    .line 7013
    move-object/from16 v0, p0

    #@179
    iget-object v0, v0, Landroid/widget/TextView;->mTempRect:Landroid/graphics/Rect;

    #@17b
    move-object/from16 v24, v0

    #@17d
    move-object/from16 v0, p0

    #@17f
    iget v0, v0, Landroid/widget/TextView;->mScrollX:I

    #@181
    move/from16 v25, v0

    #@183
    move-object/from16 v0, p0

    #@185
    iget v0, v0, Landroid/widget/TextView;->mScrollY:I

    #@187
    move/from16 v26, v0

    #@189
    invoke-virtual/range {v24 .. v26}, Landroid/graphics/Rect;->offset(II)V

    #@18c
    .line 7015
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Landroid/widget/TextView;->mTempRect:Landroid/graphics/Rect;

    #@190
    move-object/from16 v24, v0

    #@192
    move-object/from16 v0, p0

    #@194
    move-object/from16 v1, v24

    #@196
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    #@199
    move-result v24

    #@19a
    if-eqz v24, :cond_d

    #@19c
    .line 7016
    const/4 v5, 0x1

    #@19d
    goto/16 :goto_d

    #@19f
    .line 6857
    .end local v4           #bottom:I
    .end local v10           #grav:I
    .end local v11           #hs:I
    .end local v12           #hslack:I
    .end local v13           #hspace:I
    .end local v14           #ht:I
    .end local v15           #layout:Landroid/text/Layout;
    .end local v16           #left:I
    .end local v17           #line:I
    .end local v18           #right:I
    .end local v19           #top:I
    .end local v20           #vs:I
    .end local v21           #vslack:I
    .end local v22           #vspace:I
    .end local v23           #x:I
    :cond_19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v15, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@1a3
    goto/16 :goto_19

    #@1a5
    .line 6876
    .restart local v4       #bottom:I
    .restart local v14       #ht:I
    .restart local v15       #layout:Landroid/text/Layout;
    .restart local v16       #left:I
    .restart local v17       #line:I
    .restart local v18       #right:I
    .restart local v19       #top:I
    .restart local v23       #x:I
    :pswitch_1a5
    const/4 v10, 0x1

    #@1a6
    .line 6877
    .restart local v10       #grav:I
    goto/16 :goto_6e

    #@1a8
    .line 6879
    .end local v10           #grav:I
    :pswitch_1a8
    const/4 v10, -0x1

    #@1a9
    .line 6880
    .restart local v10       #grav:I
    goto/16 :goto_6e

    #@1ab
    .line 6882
    .end local v10           #grav:I
    :pswitch_1ab
    move/from16 v0, v17

    #@1ad
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@1b0
    move-result v10

    #@1b1
    .line 6883
    .restart local v10       #grav:I
    goto/16 :goto_6e

    #@1b3
    .line 6885
    .end local v10           #grav:I
    :pswitch_1b3
    move/from16 v0, v17

    #@1b5
    invoke-virtual {v15, v0}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@1b8
    move-result v24

    #@1b9
    move/from16 v0, v24

    #@1bb
    neg-int v10, v0

    #@1bc
    .line 6886
    .restart local v10       #grav:I
    goto/16 :goto_6e

    #@1be
    .line 6930
    .restart local v11       #hs:I
    .restart local v12       #hslack:I
    .restart local v13       #hspace:I
    .restart local v20       #vs:I
    .restart local v21       #vslack:I
    .restart local v22       #vspace:I
    :cond_1be
    if-lez v10, :cond_1d0

    #@1c0
    .line 6931
    sub-int v24, v18, v11

    #@1c2
    move/from16 v0, v24

    #@1c4
    if-ge v0, v13, :cond_1c8

    #@1c6
    .line 6932
    sub-int v11, v18, v13

    #@1c8
    .line 6933
    :cond_1c8
    sub-int v24, v16, v11

    #@1ca
    if-lez v24, :cond_114

    #@1cc
    .line 6934
    move/from16 v11, v16

    #@1ce
    goto/16 :goto_114

    #@1d0
    .line 6936
    :cond_1d0
    sub-int v24, v18, v16

    #@1d2
    move/from16 v0, v24

    #@1d4
    if-gt v0, v13, :cond_1e0

    #@1d6
    .line 6940
    sub-int v24, v18, v16

    #@1d8
    sub-int v24, v13, v24

    #@1da
    div-int/lit8 v24, v24, 0x2

    #@1dc
    sub-int v11, v16, v24

    #@1de
    goto/16 :goto_114

    #@1e0
    .line 6941
    :cond_1e0
    sub-int v24, v18, v12

    #@1e2
    move/from16 v0, v23

    #@1e4
    move/from16 v1, v24

    #@1e6
    if-le v0, v1, :cond_1ec

    #@1e8
    .line 6946
    sub-int v11, v18, v13

    #@1ea
    goto/16 :goto_114

    #@1ec
    .line 6947
    :cond_1ec
    add-int v24, v16, v12

    #@1ee
    move/from16 v0, v23

    #@1f0
    move/from16 v1, v24

    #@1f2
    if-ge v0, v1, :cond_1f8

    #@1f4
    .line 6952
    move/from16 v11, v16

    #@1f6
    goto/16 :goto_114

    #@1f8
    .line 6953
    :cond_1f8
    move/from16 v0, v16

    #@1fa
    if-le v0, v11, :cond_200

    #@1fc
    .line 6957
    move/from16 v11, v16

    #@1fe
    goto/16 :goto_114

    #@200
    .line 6958
    :cond_200
    add-int v24, v11, v13

    #@202
    move/from16 v0, v18

    #@204
    move/from16 v1, v24

    #@206
    if-ge v0, v1, :cond_20c

    #@208
    .line 6962
    sub-int v11, v18, v13

    #@20a
    goto/16 :goto_114

    #@20c
    .line 6967
    :cond_20c
    sub-int v24, v23, v11

    #@20e
    move/from16 v0, v24

    #@210
    if-ge v0, v12, :cond_214

    #@212
    .line 6968
    sub-int v11, v23, v12

    #@214
    .line 6970
    :cond_214
    sub-int v24, v23, v11

    #@216
    sub-int v25, v13, v12

    #@218
    move/from16 v0, v24

    #@21a
    move/from16 v1, v25

    #@21c
    if-le v0, v1, :cond_114

    #@21e
    .line 6971
    sub-int v24, v13, v12

    #@220
    sub-int v11, v23, v24

    #@222
    goto/16 :goto_114

    #@224
    .line 6980
    :cond_224
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@227
    move-result-wide v24

    #@228
    move-object/from16 v0, p0

    #@22a
    iget-wide v0, v0, Landroid/widget/TextView;->mLastScroll:J

    #@22c
    move-wide/from16 v26, v0

    #@22e
    sub-long v6, v24, v26

    #@230
    .line 6981
    .local v6, duration:J
    move-object/from16 v0, p0

    #@232
    iget v0, v0, Landroid/widget/TextView;->mScrollX:I

    #@234
    move/from16 v24, v0

    #@236
    sub-int v8, v11, v24

    #@238
    .line 6982
    .local v8, dx:I
    move-object/from16 v0, p0

    #@23a
    iget v0, v0, Landroid/widget/TextView;->mScrollY:I

    #@23c
    move/from16 v24, v0

    #@23e
    sub-int v9, v20, v24

    #@240
    .line 6984
    .local v9, dy:I
    const-wide/16 v24, 0xfa

    #@242
    cmp-long v24, v6, v24

    #@244
    if-lez v24, :cond_281

    #@246
    .line 6985
    move-object/from16 v0, p0

    #@248
    iget-object v0, v0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@24a
    move-object/from16 v24, v0

    #@24c
    move-object/from16 v0, p0

    #@24e
    iget v0, v0, Landroid/widget/TextView;->mScrollX:I

    #@250
    move/from16 v25, v0

    #@252
    move-object/from16 v0, p0

    #@254
    iget v0, v0, Landroid/widget/TextView;->mScrollY:I

    #@256
    move/from16 v26, v0

    #@258
    move-object/from16 v0, v24

    #@25a
    move/from16 v1, v25

    #@25c
    move/from16 v2, v26

    #@25e
    invoke-virtual {v0, v1, v2, v8, v9}, Landroid/widget/Scroller;->startScroll(IIII)V

    #@261
    .line 6986
    move-object/from16 v0, p0

    #@263
    iget-object v0, v0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@265
    move-object/from16 v24, v0

    #@267
    invoke-virtual/range {v24 .. v24}, Landroid/widget/Scroller;->getDuration()I

    #@26a
    move-result v24

    #@26b
    move-object/from16 v0, p0

    #@26d
    move/from16 v1, v24

    #@26f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->awakenScrollBars(I)Z

    #@272
    .line 6987
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->invalidate()V

    #@275
    .line 6996
    :goto_275
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@278
    move-result-wide v24

    #@279
    move-wide/from16 v0, v24

    #@27b
    move-object/from16 v2, p0

    #@27d
    iput-wide v0, v2, Landroid/widget/TextView;->mLastScroll:J

    #@27f
    goto/16 :goto_139

    #@281
    .line 6989
    :cond_281
    move-object/from16 v0, p0

    #@283
    iget-object v0, v0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@285
    move-object/from16 v24, v0

    #@287
    invoke-virtual/range {v24 .. v24}, Landroid/widget/Scroller;->isFinished()Z

    #@28a
    move-result v24

    #@28b
    if-nez v24, :cond_296

    #@28d
    .line 6990
    move-object/from16 v0, p0

    #@28f
    iget-object v0, v0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@291
    move-object/from16 v24, v0

    #@293
    invoke-virtual/range {v24 .. v24}, Landroid/widget/Scroller;->abortAnimation()V

    #@296
    .line 6993
    :cond_296
    move-object/from16 v0, p0

    #@298
    invoke-virtual {v0, v8, v9}, Landroid/widget/TextView;->scrollBy(II)V

    #@29b
    goto :goto_275

    #@29c
    .line 6874
    :pswitch_data_29c
    .packed-switch 0x1
        :pswitch_1a5
        :pswitch_1a8
        :pswitch_1ab
        :pswitch_1b3
    .end packed-switch
.end method

.method canCopy()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 8584
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 8592
    :cond_7
    :goto_7
    return v0

    #@8
    .line 8588
    :cond_8
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@a
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v1

    #@e
    if-lez v1, :cond_7

    #@10
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_7

    #@16
    .line 8589
    const/4 v0, 0x1

    #@17
    goto :goto_7
.end method

.method canCut()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 8570
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 8580
    :cond_7
    :goto_7
    return v0

    #@8
    .line 8574
    :cond_8
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@a
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v1

    #@e
    if-lez v1, :cond_7

    #@10
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_7

    #@16
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@18
    instance-of v1, v1, Landroid/text/Editable;

    #@1a
    if-eqz v1, :cond_7

    #@1c
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1e
    if-eqz v1, :cond_7

    #@20
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@22
    if-eqz v1, :cond_28

    #@24
    iget-boolean v1, p0, Landroid/widget/TextView;->mCanCutBubblePopup:Z

    #@26
    if-eqz v1, :cond_7

    #@28
    :cond_28
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2a
    iget-object v1, v1, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@2c
    if-eqz v1, :cond_7

    #@2e
    .line 8577
    const/4 v0, 0x1

    #@2f
    goto :goto_7
.end method

.method canPaste()Z
    .registers 3

    #@0
    .prologue
    .line 8596
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    instance-of v0, v0, Landroid/text/Editable;

    #@4
    if-eqz v0, :cond_38

    #@6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    if-eqz v0, :cond_38

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@e
    if-eqz v0, :cond_38

    #@10
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@13
    move-result v0

    #@14
    if-ltz v0, :cond_38

    #@16
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@19
    move-result v0

    #@1a
    if-ltz v0, :cond_38

    #@1c
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@1e
    if-eqz v0, :cond_24

    #@20
    iget-boolean v0, p0, Landroid/widget/TextView;->mCanPasteBubblePopup:Z

    #@22
    if-eqz v0, :cond_38

    #@24
    :cond_24
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@27
    move-result-object v0

    #@28
    const-string v1, "clipboard"

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    move-result-object v0

    #@2e
    check-cast v0, Landroid/content/ClipboardManager;

    #@30
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_38

    #@36
    const/4 v0, 0x1

    #@37
    :goto_37
    return v0

    #@38
    :cond_38
    const/4 v0, 0x0

    #@39
    goto :goto_37
.end method

.method public cancelLongPress()V
    .registers 3

    #@0
    .prologue
    .line 7930
    invoke-super {p0}, Landroid/view/View;->cancelLongPress()V

    #@3
    .line 7931
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9
    const/4 v1, 0x1

    #@a
    iput-boolean v1, v0, Landroid/widget/Editor;->mIgnoreActionUpEvent:Z

    #@c
    .line 7932
    :cond_c
    return-void
.end method

.method public clearComposingText()V
    .registers 2

    #@0
    .prologue
    .line 7803
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    instance-of v0, v0, Landroid/text/Spannable;

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 7804
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@8
    check-cast v0, Landroid/text/Spannable;

    #@a
    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    #@d
    .line 7806
    :cond_d
    return-void
.end method

.method protected computeHorizontalScrollRange()I
    .registers 3

    #@0
    .prologue
    .line 8030
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 8031
    iget-boolean v0, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@6
    if-eqz v0, :cond_18

    #@8
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@a
    and-int/lit8 v0, v0, 0x7

    #@c
    const/4 v1, 0x3

    #@d
    if-ne v0, v1, :cond_18

    #@f
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@11
    const/4 v1, 0x0

    #@12
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineWidth(I)F

    #@15
    move-result v0

    #@16
    float-to-int v0, v0

    #@17
    .line 8035
    :goto_17
    return v0

    #@18
    .line 8031
    :cond_18
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@1a
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    #@1d
    move-result v0

    #@1e
    goto :goto_17

    #@1f
    .line 8035
    :cond_1f
    invoke-super {p0}, Landroid/view/View;->computeHorizontalScrollRange()I

    #@22
    move-result v0

    #@23
    goto :goto_17
.end method

.method public computeScroll()V
    .registers 2

    #@0
    .prologue
    .line 7087
    iget-object v0, p0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@2
    if-eqz v0, :cond_22

    #@4
    .line 7088
    iget-object v0, p0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@6
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_22

    #@c
    .line 7089
    iget-object v0, p0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@e
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/widget/TextView;->mScrollX:I

    #@14
    .line 7090
    iget-object v0, p0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@16
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/widget/TextView;->mScrollY:I

    #@1c
    .line 7091
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidateParentCaches()V

    #@1f
    .line 7092
    invoke-virtual {p0}, Landroid/widget/TextView;->postInvalidate()V

    #@22
    .line 7095
    :cond_22
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .registers 3

    #@0
    .prologue
    .line 8048
    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@7
    move-result v1

    #@8
    sub-int/2addr v0, v1

    #@9
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@c
    move-result v1

    #@d
    sub-int/2addr v0, v1

    #@e
    return v0
.end method

.method protected computeVerticalScrollRange()I
    .registers 2

    #@0
    .prologue
    .line 8040
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 8041
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@6
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    #@9
    move-result v0

    #@a
    .line 8043
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-super {p0}, Landroid/view/View;->computeVerticalScrollRange()I

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method convertToLocalHorizontalCoordinate(F)F
    .registers 4
    .parameter "x"

    #@0
    .prologue
    .line 8806
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    #@3
    move-result v0

    #@4
    int-to-float v0, v0

    #@5
    sub-float/2addr p1, v0

    #@6
    .line 8808
    const/4 v0, 0x0

    #@7
    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    #@a
    move-result p1

    #@b
    .line 8809
    invoke-virtual {p0}, Landroid/widget/TextView;->getWidth()I

    #@e
    move-result v0

    #@f
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingRight()I

    #@12
    move-result v1

    #@13
    sub-int/2addr v0, v1

    #@14
    add-int/lit8 v0, v0, -0x1

    #@16
    int-to-float v0, v0

    #@17
    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    #@1a
    move-result p1

    #@1b
    .line 8810
    invoke-virtual {p0}, Landroid/widget/TextView;->getScrollX()I

    #@1e
    move-result v0

    #@1f
    int-to-float v0, v0

    #@20
    add-float/2addr p1, v0

    #@21
    .line 8811
    return p1
.end method

.method public debug(I)V
    .registers 5
    .parameter "depth"

    #@0
    .prologue
    .line 7131
    invoke-super {p0, p1}, Landroid/view/View;->debug(I)V

    #@3
    .line 7133
    invoke-static {p1}, Landroid/widget/TextView;->debugIndent(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 7134
    .local v0, output:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, "frame={"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget v2, p0, Landroid/widget/TextView;->mLeft:I

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ", "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget v2, p0, Landroid/widget/TextView;->mTop:I

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, ", "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    iget v2, p0, Landroid/widget/TextView;->mRight:I

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string v2, ", "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget v2, p0, Landroid/widget/TextView;->mBottom:I

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    const-string/jumbo v2, "} scroll={"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    iget v2, p0, Landroid/widget/TextView;->mScrollX:I

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, ", "

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget v2, p0, Landroid/widget/TextView;->mScrollY:I

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    const-string/jumbo v2, "} "

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    .line 7138
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@66
    if-eqz v1, :cond_c0

    #@68
    .line 7140
    new-instance v1, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    const-string/jumbo v2, "mText=\""

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@7a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v1

    #@7e
    const-string v2, "\" "

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v0

    #@88
    .line 7141
    iget-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@8a
    if-eqz v1, :cond_ba

    #@8c
    .line 7142
    new-instance v1, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    const-string/jumbo v2, "mLayout width="

    #@98
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v1

    #@9c
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@9e
    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    #@a1
    move-result v2

    #@a2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v1

    #@a6
    const-string v2, " height="

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@ae
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    #@b1
    move-result v2

    #@b2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v0

    #@ba
    .line 7148
    :cond_ba
    :goto_ba
    const-string v1, "View"

    #@bc
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 7149
    return-void

    #@c0
    .line 7146
    :cond_c0
    new-instance v1, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v1

    #@c9
    const-string/jumbo v2, "mText=NULL"

    #@cc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v1

    #@d0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v0

    #@d4
    goto :goto_ba
.end method

.method protected deleteText_internal(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 8956
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    check-cast v0, Landroid/text/Editable;

    #@4
    invoke-interface {v0, p1, p2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@7
    .line 8957
    return-void
.end method

.method public didTouchFocusSelect()Z
    .registers 2

    #@0
    .prologue
    .line 7925
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget-boolean v0, v0, Landroid/widget/Editor;->mTouchFocusSelected:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public dispatchFinishTemporaryDetach()V
    .registers 2

    #@0
    .prologue
    .line 7721
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/TextView;->mDispatchTemporaryDetach:Z

    #@3
    .line 7722
    invoke-super {p0}, Landroid/view/View;->dispatchFinishTemporaryDetach()V

    #@6
    .line 7723
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/widget/TextView;->mDispatchTemporaryDetach:Z

    #@9
    .line 7724
    return-void
.end method

.method protected drawableStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 3488
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    #@3
    .line 3489
    iget-object v2, p0, Landroid/widget/TextView;->mTextColor:Landroid/content/res/ColorStateList;

    #@5
    if-eqz v2, :cond_f

    #@7
    iget-object v2, p0, Landroid/widget/TextView;->mTextColor:Landroid/content/res/ColorStateList;

    #@9
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_27

    #@f
    :cond_f
    iget-object v2, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@11
    if-eqz v2, :cond_1b

    #@13
    iget-object v2, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@15
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_27

    #@1b
    :cond_1b
    iget-object v2, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@1d
    if-eqz v2, :cond_2a

    #@1f
    iget-object v2, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@21
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_2a

    #@27
    .line 3492
    :cond_27
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@2a
    .line 3495
    :cond_2a
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2c
    .line 3496
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_98

    #@2e
    .line 3497
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@31
    move-result-object v1

    #@32
    .line 3498
    .local v1, state:[I
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@34
    if-eqz v2, :cond_43

    #@36
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@38
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_43

    #@3e
    .line 3499
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@40
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@43
    .line 3501
    :cond_43
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@45
    if-eqz v2, :cond_54

    #@47
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@49
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_54

    #@4f
    .line 3502
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@51
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@54
    .line 3504
    :cond_54
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@56
    if-eqz v2, :cond_65

    #@58
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@5a
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@5d
    move-result v2

    #@5e
    if-eqz v2, :cond_65

    #@60
    .line 3505
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@62
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@65
    .line 3507
    :cond_65
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@67
    if-eqz v2, :cond_76

    #@69
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@6b
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@6e
    move-result v2

    #@6f
    if-eqz v2, :cond_76

    #@71
    .line 3508
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@73
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@76
    .line 3510
    :cond_76
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@78
    if-eqz v2, :cond_87

    #@7a
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@7c
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_87

    #@82
    .line 3511
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@84
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@87
    .line 3513
    :cond_87
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@89
    if-eqz v2, :cond_98

    #@8b
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@8d
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@90
    move-result v2

    #@91
    if-eqz v2, :cond_98

    #@93
    .line 3514
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@95
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@98
    .line 3517
    .end local v1           #state:[I
    :cond_98
    return-void
.end method

.method public endBatchEdit()V
    .registers 2

    #@0
    .prologue
    .line 5976
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor;->endBatchEdit()V

    #@9
    .line 5977
    :cond_9
    return-void
.end method

.method public extractText(Landroid/view/inputmethod/ExtractedTextRequest;Landroid/view/inputmethod/ExtractedText;)Z
    .registers 4
    .parameter "request"
    .parameter "outText"

    #@0
    .prologue
    .line 5866
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 5867
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    invoke-virtual {v0, p1, p2}, Landroid/widget/Editor;->extractText(Landroid/view/inputmethod/ExtractedTextRequest;Landroid/view/inputmethod/ExtractedText;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V
    .registers 7
    .parameter
    .parameter "searched"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 8053
    .local p1, outViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V

    #@3
    .line 8054
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_36

    #@9
    and-int/lit8 v2, p3, 0x1

    #@b
    if-eqz v2, :cond_36

    #@d
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_36

    #@13
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@15
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_36

    #@1b
    .line 8056
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 8057
    .local v0, searchedLowerCase:Ljava/lang/String;
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@25
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 8058
    .local v1, textLowerCase:Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_36

    #@33
    .line 8059
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    .line 8062
    .end local v0           #searchedLowerCase:Ljava/lang/String;
    .end local v1           #textLowerCase:Ljava/lang/String;
    :cond_36
    return-void
.end method

.method public getAccessibilityCursorPosition()I
    .registers 3

    #@0
    .prologue
    .line 9066
    invoke-virtual {p0}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 9067
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@d
    move-result v0

    #@e
    .line 9068
    .local v0, selectionEnd:I
    if-ltz v0, :cond_11

    #@10
    .line 9072
    .end local v0           #selectionEnd:I
    :goto_10
    return v0

    #@11
    :cond_11
    invoke-super {p0}, Landroid/view/View;->getAccessibilityCursorPosition()I

    #@14
    move-result v0

    #@15
    goto :goto_10
.end method

.method public final getAutoLinkMask()I
    .registers 2

    #@0
    .prologue
    .line 2445
    iget v0, p0, Landroid/widget/TextView;->mAutoLinkMask:I

    #@2
    return v0
.end method

.method public getBaseline()I
    .registers 5

    #@0
    .prologue
    .line 5383
    iget-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    if-nez v1, :cond_9

    #@4
    .line 5384
    invoke-super {p0}, Landroid/view/View;->getBaseline()I

    #@7
    move-result v1

    #@8
    .line 5392
    :goto_8
    return v1

    #@9
    .line 5387
    :cond_9
    const/4 v0, 0x0

    #@a
    .line 5388
    .local v0, voffset:I
    iget v1, p0, Landroid/widget/TextView;->mGravity:I

    #@c
    and-int/lit8 v1, v1, 0x70

    #@e
    const/16 v2, 0x30

    #@10
    if-eq v1, v2, :cond_17

    #@12
    .line 5389
    const/4 v1, 0x1

    #@13
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@16
    move-result v0

    #@17
    .line 5392
    :cond_17
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@1a
    move-result v1

    #@1b
    add-int/2addr v1, v0

    #@1c
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@1e
    const/4 v3, 0x0

    #@1f
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineBaseline(I)I

    #@22
    move-result v2

    #@23
    add-int/2addr v1, v2

    #@24
    goto :goto_8
.end method

.method protected getBottomPaddingOffset()I
    .registers 4

    #@0
    .prologue
    .line 4880
    const/4 v0, 0x0

    #@1
    iget v1, p0, Landroid/widget/TextView;->mShadowDy:F

    #@3
    iget v2, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@5
    add-float/2addr v1, v2

    #@6
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@9
    move-result v0

    #@a
    float-to-int v0, v0

    #@b
    return v0
.end method

.method public getCompoundDrawablePadding()I
    .registers 3

    #@0
    .prologue
    .line 2405
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2
    .line 2406
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_7

    #@4
    iget v1, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@6
    :goto_6
    return v1

    #@7
    :cond_7
    const/4 v1, 0x0

    #@8
    goto :goto_6
.end method

.method public getCompoundDrawables()[Landroid/graphics/drawable/Drawable;
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    .line 2346
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@7
    .line 2347
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_1d

    #@9
    .line 2348
    const/4 v1, 0x4

    #@a
    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    #@c
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@e
    aput-object v2, v1, v3

    #@10
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@12
    aput-object v2, v1, v4

    #@14
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@16
    aput-object v2, v1, v5

    #@18
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@1a
    aput-object v2, v1, v6

    #@1c
    .line 2352
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x4

    #@1e
    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    #@20
    aput-object v2, v1, v3

    #@22
    aput-object v2, v1, v4

    #@24
    aput-object v2, v1, v5

    #@26
    aput-object v2, v1, v6

    #@28
    goto :goto_1c
.end method

.method public getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    .line 2365
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@7
    .line 2366
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_1d

    #@9
    .line 2367
    const/4 v1, 0x4

    #@a
    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    #@c
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@e
    aput-object v2, v1, v3

    #@10
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@12
    aput-object v2, v1, v4

    #@14
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@16
    aput-object v2, v1, v5

    #@18
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@1a
    aput-object v2, v1, v6

    #@1c
    .line 2371
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x4

    #@1e
    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    #@20
    aput-object v2, v1, v3

    #@22
    aput-object v2, v1, v4

    #@24
    aput-object v2, v1, v5

    #@26
    aput-object v2, v1, v6

    #@28
    goto :goto_1c
.end method

.method public getCompoundPaddingBottom()I
    .registers 4

    #@0
    .prologue
    .line 1814
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2
    .line 1815
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_8

    #@4
    iget-object v1, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@6
    if-nez v1, :cond_b

    #@8
    .line 1816
    :cond_8
    iget v1, p0, Landroid/widget/TextView;->mPaddingBottom:I

    #@a
    .line 1818
    :goto_a
    return v1

    #@b
    :cond_b
    iget v1, p0, Landroid/widget/TextView;->mPaddingBottom:I

    #@d
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@f
    add-int/2addr v1, v2

    #@10
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@12
    add-int/2addr v1, v2

    #@13
    goto :goto_a
.end method

.method public getCompoundPaddingEnd()I
    .registers 2

    #@0
    .prologue
    .line 1868
    invoke-virtual {p0}, Landroid/widget/TextView;->resolveDrawables()V

    #@3
    .line 1869
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@6
    move-result v0

    #@7
    packed-switch v0, :pswitch_data_14

    #@a
    .line 1872
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@d
    move-result v0

    #@e
    .line 1874
    :goto_e
    return v0

    #@f
    :pswitch_f
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@12
    move-result v0

    #@13
    goto :goto_e

    #@14
    .line 1869
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_f
    .end packed-switch
.end method

.method public getCompoundPaddingLeft()I
    .registers 4

    #@0
    .prologue
    .line 1827
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2
    .line 1828
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_8

    #@4
    iget-object v1, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@6
    if-nez v1, :cond_b

    #@8
    .line 1829
    :cond_8
    iget v1, p0, Landroid/widget/TextView;->mPaddingLeft:I

    #@a
    .line 1831
    :goto_a
    return v1

    #@b
    :cond_b
    iget v1, p0, Landroid/widget/TextView;->mPaddingLeft:I

    #@d
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@f
    add-int/2addr v1, v2

    #@10
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@12
    add-int/2addr v1, v2

    #@13
    goto :goto_a
.end method

.method public getCompoundPaddingRight()I
    .registers 4

    #@0
    .prologue
    .line 1840
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2
    .line 1841
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_8

    #@4
    iget-object v1, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@6
    if-nez v1, :cond_b

    #@8
    .line 1842
    :cond_8
    iget v1, p0, Landroid/widget/TextView;->mPaddingRight:I

    #@a
    .line 1844
    :goto_a
    return v1

    #@b
    :cond_b
    iget v1, p0, Landroid/widget/TextView;->mPaddingRight:I

    #@d
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@f
    add-int/2addr v1, v2

    #@10
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@12
    add-int/2addr v1, v2

    #@13
    goto :goto_a
.end method

.method public getCompoundPaddingStart()I
    .registers 2

    #@0
    .prologue
    .line 1853
    invoke-virtual {p0}, Landroid/widget/TextView;->resolveDrawables()V

    #@3
    .line 1854
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@6
    move-result v0

    #@7
    packed-switch v0, :pswitch_data_14

    #@a
    .line 1857
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@d
    move-result v0

    #@e
    .line 1859
    :goto_e
    return v0

    #@f
    :pswitch_f
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@12
    move-result v0

    #@13
    goto :goto_e

    #@14
    .line 1854
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_f
    .end packed-switch
.end method

.method public getCompoundPaddingTop()I
    .registers 4

    #@0
    .prologue
    .line 1801
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2
    .line 1802
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-eqz v0, :cond_8

    #@4
    iget-object v1, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@6
    if-nez v1, :cond_b

    #@8
    .line 1803
    :cond_8
    iget v1, p0, Landroid/widget/TextView;->mPaddingTop:I

    #@a
    .line 1805
    :goto_a
    return v1

    #@b
    :cond_b
    iget v1, p0, Landroid/widget/TextView;->mPaddingTop:I

    #@d
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@f
    add-int/2addr v1, v2

    #@10
    iget v2, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@12
    add-int/2addr v1, v2

    #@13
    goto :goto_a
.end method

.method public final getCurrentHintTextColor()I
    .registers 2

    #@0
    .prologue
    .line 2932
    iget-object v0, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget v0, p0, Landroid/widget/TextView;->mCurHintTextColor:I

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    iget v0, p0, Landroid/widget/TextView;->mCurTextColor:I

    #@9
    goto :goto_6
.end method

.method public final getCurrentTextColor()I
    .registers 2

    #@0
    .prologue
    .line 2706
    iget v0, p0, Landroid/widget/TextView;->mCurTextColor:I

    #@2
    return v0
.end method

.method public getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;
    .registers 2

    #@0
    .prologue
    .line 8554
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-object v0, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@a
    goto :goto_5
.end method

.method protected getDefaultEditable()Z
    .registers 2

    #@0
    .prologue
    .line 1560
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected getDefaultMovementMethod()Landroid/text/method/MovementMethod;
    .registers 2

    #@0
    .prologue
    .line 1567
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getEditableText()Landroid/text/Editable;
    .registers 2

    #@0
    .prologue
    .line 1599
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    instance-of v0, v0, Landroid/text/Editable;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@8
    check-cast v0, Landroid/text/Editable;

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getEllipsize()Landroid/text/TextUtils$TruncateAt;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 7320
    iget-object v0, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@2
    return-object v0
.end method

.method public getError()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 4479
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-object v0, v0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@a
    goto :goto_5
.end method

.method public getExtendedPaddingBottom()I
    .registers 8

    #@0
    .prologue
    .line 1917
    iget v5, p0, Landroid/widget/TextView;->mMaxMode:I

    #@2
    const/4 v6, 0x1

    #@3
    if-eq v5, v6, :cond_a

    #@5
    .line 1918
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@8
    move-result v0

    #@9
    .line 1940
    :cond_9
    :goto_9
    return v0

    #@a
    .line 1921
    :cond_a
    iget-object v5, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@c
    invoke-virtual {v5}, Landroid/text/Layout;->getLineCount()I

    #@f
    move-result v5

    #@10
    iget v6, p0, Landroid/widget/TextView;->mMaximum:I

    #@12
    if-gt v5, v6, :cond_19

    #@14
    .line 1922
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@17
    move-result v0

    #@18
    goto :goto_9

    #@19
    .line 1925
    :cond_19
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@1c
    move-result v3

    #@1d
    .line 1926
    .local v3, top:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@20
    move-result v0

    #@21
    .line 1927
    .local v0, bottom:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    #@24
    move-result v5

    #@25
    sub-int/2addr v5, v3

    #@26
    sub-int v4, v5, v0

    #@28
    .line 1928
    .local v4, viewht:I
    iget-object v5, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2a
    iget v6, p0, Landroid/widget/TextView;->mMaximum:I

    #@2c
    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineTop(I)I

    #@2f
    move-result v2

    #@30
    .line 1930
    .local v2, layoutht:I
    if-ge v2, v4, :cond_9

    #@32
    .line 1934
    iget v5, p0, Landroid/widget/TextView;->mGravity:I

    #@34
    and-int/lit8 v1, v5, 0x70

    #@36
    .line 1935
    .local v1, gravity:I
    const/16 v5, 0x30

    #@38
    if-ne v1, v5, :cond_3f

    #@3a
    .line 1936
    add-int v5, v0, v4

    #@3c
    sub-int v0, v5, v2

    #@3e
    goto :goto_9

    #@3f
    .line 1937
    :cond_3f
    const/16 v5, 0x50

    #@41
    if-eq v1, v5, :cond_9

    #@43
    .line 1940
    sub-int v5, v4, v2

    #@45
    div-int/lit8 v5, v5, 0x2

    #@47
    add-int/2addr v0, v5

    #@48
    goto :goto_9
.end method

.method public getExtendedPaddingTop()I
    .registers 8

    #@0
    .prologue
    .line 1884
    iget v5, p0, Landroid/widget/TextView;->mMaxMode:I

    #@2
    const/4 v6, 0x1

    #@3
    if-eq v5, v6, :cond_a

    #@5
    .line 1885
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@8
    move-result v3

    #@9
    .line 1907
    :cond_9
    :goto_9
    return v3

    #@a
    .line 1888
    :cond_a
    iget-object v5, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@c
    invoke-virtual {v5}, Landroid/text/Layout;->getLineCount()I

    #@f
    move-result v5

    #@10
    iget v6, p0, Landroid/widget/TextView;->mMaximum:I

    #@12
    if-gt v5, v6, :cond_19

    #@14
    .line 1889
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@17
    move-result v3

    #@18
    goto :goto_9

    #@19
    .line 1892
    :cond_19
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@1c
    move-result v3

    #@1d
    .line 1893
    .local v3, top:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@20
    move-result v0

    #@21
    .line 1894
    .local v0, bottom:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    #@24
    move-result v5

    #@25
    sub-int/2addr v5, v3

    #@26
    sub-int v4, v5, v0

    #@28
    .line 1895
    .local v4, viewht:I
    iget-object v5, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2a
    iget v6, p0, Landroid/widget/TextView;->mMaximum:I

    #@2c
    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineTop(I)I

    #@2f
    move-result v2

    #@30
    .line 1897
    .local v2, layoutht:I
    if-ge v2, v4, :cond_9

    #@32
    .line 1901
    iget v5, p0, Landroid/widget/TextView;->mGravity:I

    #@34
    and-int/lit8 v1, v5, 0x70

    #@36
    .line 1902
    .local v1, gravity:I
    const/16 v5, 0x30

    #@38
    if-eq v1, v5, :cond_9

    #@3a
    .line 1904
    const/16 v5, 0x50

    #@3c
    if-ne v1, v5, :cond_43

    #@3e
    .line 1905
    add-int v5, v3, v4

    #@40
    sub-int v3, v5, v2

    #@42
    goto :goto_9

    #@43
    .line 1907
    :cond_43
    sub-int v5, v4, v2

    #@45
    div-int/lit8 v5, v5, 0x2

    #@47
    add-int/2addr v3, v5

    #@48
    goto :goto_9
.end method

.method protected getFadeHeight(Z)I
    .registers 3
    .parameter "offsetRequired"

    #@0
    .prologue
    .line 5417
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@6
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method protected getFadeTop(Z)I
    .registers 5
    .parameter "offsetRequired"

    #@0
    .prologue
    .line 5400
    iget-object v1, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    if-nez v1, :cond_6

    #@4
    const/4 v1, 0x0

    #@5
    .line 5409
    :goto_5
    return v1

    #@6
    .line 5402
    :cond_6
    const/4 v0, 0x0

    #@7
    .line 5403
    .local v0, voffset:I
    iget v1, p0, Landroid/widget/TextView;->mGravity:I

    #@9
    and-int/lit8 v1, v1, 0x70

    #@b
    const/16 v2, 0x30

    #@d
    if-eq v1, v2, :cond_14

    #@f
    .line 5404
    const/4 v1, 0x1

    #@10
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@13
    move-result v0

    #@14
    .line 5407
    :cond_14
    if-eqz p1, :cond_1b

    #@16
    invoke-virtual {p0}, Landroid/widget/TextView;->getTopPaddingOffset()I

    #@19
    move-result v1

    #@1a
    add-int/2addr v0, v1

    #@1b
    .line 5409
    :cond_1b
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@1e
    move-result v1

    #@1f
    add-int/2addr v1, v0

    #@20
    goto :goto_5
.end method

.method public getFilters()[Landroid/text/InputFilter;
    .registers 2

    #@0
    .prologue
    .line 4576
    iget-object v0, p0, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@2
    return-object v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .registers 15
    .parameter "r"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 5289
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@3
    if-nez v8, :cond_9

    #@5
    .line 5290
    invoke-super {p0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@8
    .line 5341
    :goto_8
    return-void

    #@9
    .line 5294
    :cond_9
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@c
    move-result v6

    #@d
    .line 5295
    .local v6, selEnd:I
    if-gez v6, :cond_13

    #@f
    .line 5296
    invoke-super {p0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@12
    goto :goto_8

    #@13
    .line 5300
    :cond_13
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@16
    move-result v7

    #@17
    .line 5301
    .local v7, selStart:I
    if-ltz v7, :cond_1b

    #@19
    if-lt v7, v6, :cond_64

    #@1b
    .line 5302
    :cond_1b
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@1d
    invoke-virtual {v8, v6}, Landroid/text/Layout;->getLineForOffset(I)I

    #@20
    move-result v0

    #@21
    .line 5303
    .local v0, line:I
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@23
    invoke-virtual {v8, v0}, Landroid/text/Layout;->getLineTop(I)I

    #@26
    move-result v8

    #@27
    iput v8, p1, Landroid/graphics/Rect;->top:I

    #@29
    .line 5304
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2b
    invoke-virtual {v8, v0}, Landroid/text/Layout;->getLineBottom(I)I

    #@2e
    move-result v8

    #@2f
    iput v8, p1, Landroid/graphics/Rect;->bottom:I

    #@31
    .line 5305
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@33
    invoke-virtual {v8, v6}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@36
    move-result v8

    #@37
    float-to-int v8, v8

    #@38
    add-int/lit8 v8, v8, -0x2

    #@3a
    iput v8, p1, Landroid/graphics/Rect;->left:I

    #@3c
    .line 5306
    iget v8, p1, Landroid/graphics/Rect;->left:I

    #@3e
    add-int/lit8 v8, v8, 0x4

    #@40
    iput v8, p1, Landroid/graphics/Rect;->right:I

    #@42
    .line 5333
    .end local v0           #line:I
    :goto_42
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@45
    move-result v4

    #@46
    .line 5334
    .local v4, paddingLeft:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@49
    move-result v5

    #@4a
    .line 5335
    .local v5, paddingTop:I
    iget v8, p0, Landroid/widget/TextView;->mGravity:I

    #@4c
    and-int/lit8 v8, v8, 0x70

    #@4e
    const/16 v9, 0x30

    #@50
    if-eq v8, v9, :cond_57

    #@52
    .line 5336
    invoke-virtual {p0, v12}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@55
    move-result v8

    #@56
    add-int/2addr v5, v8

    #@57
    .line 5338
    :cond_57
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    #@5a
    .line 5339
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@5d
    move-result v3

    #@5e
    .line 5340
    .local v3, paddingBottom:I
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    #@60
    add-int/2addr v8, v3

    #@61
    iput v8, p1, Landroid/graphics/Rect;->bottom:I

    #@63
    goto :goto_8

    #@64
    .line 5308
    .end local v3           #paddingBottom:I
    .end local v4           #paddingLeft:I
    .end local v5           #paddingTop:I
    :cond_64
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@66
    invoke-virtual {v8, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    #@69
    move-result v2

    #@6a
    .line 5309
    .local v2, lineStart:I
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@6c
    invoke-virtual {v8, v6}, Landroid/text/Layout;->getLineForOffset(I)I

    #@6f
    move-result v1

    #@70
    .line 5310
    .local v1, lineEnd:I
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@72
    invoke-virtual {v8, v2}, Landroid/text/Layout;->getLineTop(I)I

    #@75
    move-result v8

    #@76
    iput v8, p1, Landroid/graphics/Rect;->top:I

    #@78
    .line 5311
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@7a
    invoke-virtual {v8, v1}, Landroid/text/Layout;->getLineBottom(I)I

    #@7d
    move-result v8

    #@7e
    iput v8, p1, Landroid/graphics/Rect;->bottom:I

    #@80
    .line 5312
    if-ne v2, v1, :cond_95

    #@82
    .line 5313
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@84
    invoke-virtual {v8, v7}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@87
    move-result v8

    #@88
    float-to-int v8, v8

    #@89
    iput v8, p1, Landroid/graphics/Rect;->left:I

    #@8b
    .line 5314
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@8d
    invoke-virtual {v8, v6}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@90
    move-result v8

    #@91
    float-to-int v8, v8

    #@92
    iput v8, p1, Landroid/graphics/Rect;->right:I

    #@94
    goto :goto_42

    #@95
    .line 5318
    :cond_95
    iget-boolean v8, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@97
    if-eqz v8, :cond_b2

    #@99
    .line 5319
    iget-object v8, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@9b
    if-nez v8, :cond_a4

    #@9d
    new-instance v8, Landroid/graphics/Path;

    #@9f
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    #@a2
    iput-object v8, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@a4
    .line 5320
    :cond_a4
    iget-object v8, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@a6
    invoke-virtual {v8}, Landroid/graphics/Path;->reset()V

    #@a9
    .line 5321
    iget-object v8, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@ab
    iget-object v9, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@ad
    invoke-virtual {v8, v7, v6, v9}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    #@b0
    .line 5322
    iput-boolean v12, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@b2
    .line 5324
    :cond_b2
    sget-object v9, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@b4
    monitor-enter v9

    #@b5
    .line 5325
    :try_start_b5
    iget-object v8, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@b7
    sget-object v10, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@b9
    const/4 v11, 0x1

    #@ba
    invoke-virtual {v8, v10, v11}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@bd
    .line 5326
    sget-object v8, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@bf
    iget v8, v8, Landroid/graphics/RectF;->left:F

    #@c1
    float-to-int v8, v8

    #@c2
    add-int/lit8 v8, v8, -0x1

    #@c4
    iput v8, p1, Landroid/graphics/Rect;->left:I

    #@c6
    .line 5327
    sget-object v8, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@c8
    iget v8, v8, Landroid/graphics/RectF;->right:F

    #@ca
    float-to-int v8, v8

    #@cb
    add-int/lit8 v8, v8, 0x1

    #@cd
    iput v8, p1, Landroid/graphics/Rect;->right:I

    #@cf
    .line 5328
    monitor-exit v9

    #@d0
    goto/16 :goto_42

    #@d2
    :catchall_d2
    move-exception v8

    #@d3
    monitor-exit v9
    :try_end_d4
    .catchall {:try_start_b5 .. :try_end_d4} :catchall_d2

    #@d4
    throw v8
.end method

.method public getFreezesText()Z
    .registers 2

    #@0
    .prologue
    .line 3675
    iget-boolean v0, p0, Landroid/widget/TextView;->mFreezesText:Z

    #@2
    return v0
.end method

.method public getGravity()I
    .registers 2

    #@0
    .prologue
    .line 3030
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@2
    return v0
.end method

.method public getHighlightColor()I
    .registers 2

    #@0
    .prologue
    .line 2730
    iget v0, p0, Landroid/widget/TextView;->mHighlightColor:I

    #@2
    return v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 4020
    iget-object v0, p0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method final getHintLayout()Landroid/text/Layout;
    .registers 2

    #@0
    .prologue
    .line 1625
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@2
    return-object v0
.end method

.method public final getHintTextColors()Landroid/content/res/ColorStateList;
    .registers 2

    #@0
    .prologue
    .line 2923
    iget-object v0, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@2
    return-object v0
.end method

.method public getHorizontalOffsetForDrawables()I
    .registers 2

    #@0
    .prologue
    .line 5115
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getHorizontallyScrolling()Z
    .registers 2

    #@0
    .prologue
    .line 3085
    iget-boolean v0, p0, Landroid/widget/TextView;->mHorizontallyScrolling:Z

    #@2
    return v0
.end method

.method public getImeActionId()I
    .registers 2

    #@0
    .prologue
    .line 4279
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@8
    if-eqz v0, :cond_11

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@e
    iget v0, v0, Landroid/widget/Editor$InputContentType;->imeActionId:I

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public getImeActionLabel()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 4268
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@8
    if-eqz v0, :cond_11

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@e
    iget-object v0, v0, Landroid/widget/Editor$InputContentType;->imeActionLabel:Ljava/lang/CharSequence;

    #@10
    :goto_10
    return-object v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public getImeOptions()I
    .registers 2

    #@0
    .prologue
    .line 4240
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@8
    if-eqz v0, :cond_11

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@e
    iget v0, v0, Landroid/widget/Editor$InputContentType;->imeOptions:I

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public getIncludeFontPadding()Z
    .registers 2

    #@0
    .prologue
    .line 6405
    iget-boolean v0, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@2
    return v0
.end method

.method public getInputExtras(Z)Landroid/os/Bundle;
    .registers 4
    .parameter "create"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 4460
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3
    if-nez v1, :cond_8

    #@5
    if-nez p1, :cond_8

    #@7
    .line 4470
    :cond_7
    :goto_7
    return-object v0

    #@8
    .line 4461
    :cond_8
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@b
    .line 4462
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@d
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@f
    if-nez v1, :cond_18

    #@11
    .line 4463
    if-eqz p1, :cond_7

    #@13
    .line 4464
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@15
    invoke-virtual {v1}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@18
    .line 4466
    :cond_18
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1a
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@1c
    iget-object v1, v1, Landroid/widget/Editor$InputContentType;->extras:Landroid/os/Bundle;

    #@1e
    if-nez v1, :cond_2d

    #@20
    .line 4467
    if-eqz p1, :cond_7

    #@22
    .line 4468
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@24
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@26
    new-instance v1, Landroid/os/Bundle;

    #@28
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@2b
    iput-object v1, v0, Landroid/widget/Editor$InputContentType;->extras:Landroid/os/Bundle;

    #@2d
    .line 4470
    :cond_2d
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2f
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@31
    iget-object v0, v0, Landroid/widget/Editor$InputContentType;->extras:Landroid/os/Bundle;

    #@33
    goto :goto_7
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 4216
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget v0, v0, Landroid/widget/Editor;->mInputType:I

    #@a
    goto :goto_5
.end method

.method public getIterableTextForAccessibility()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 9024
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_18

    #@8
    .line 9025
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@a
    instance-of v0, v0, Landroid/text/Spannable;

    #@c
    if-nez v0, :cond_15

    #@e
    .line 9026
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@10
    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@12
    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@15
    .line 9028
    :cond_15
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@17
    .line 9030
    :goto_17
    return-object v0

    #@18
    :cond_18
    invoke-super {p0}, Landroid/view/View;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@1b
    move-result-object v0

    #@1c
    goto :goto_17
.end method

.method public getIteratorForGranularity(I)Landroid/view/AccessibilityIterators$TextSegmentIterator;
    .registers 5
    .parameter "granularity"

    #@0
    .prologue
    .line 9038
    sparse-switch p1, :sswitch_data_40

    #@3
    .line 9058
    :cond_3
    invoke-super {p0, p1}, Landroid/view/View;->getIteratorForGranularity(I)Landroid/view/AccessibilityIterators$TextSegmentIterator;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 9040
    :sswitch_8
    invoke-virtual {p0}, Landroid/widget/TextView;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/text/Spannable;

    #@e
    .line 9041
    .local v1, text:Landroid/text/Spannable;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_3

    #@14
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@17
    move-result-object v2

    #@18
    if-eqz v2, :cond_3

    #@1a
    .line 9042
    invoke-static {}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getInstance()Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;

    #@1d
    move-result-object v0

    #@1e
    .line 9044
    .local v0, iterator:Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v0, v1, v2}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->initialize(Landroid/text/Spannable;Landroid/text/Layout;)V

    #@25
    goto :goto_7

    #@26
    .line 9049
    .end local v0           #iterator:Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;
    .end local v1           #text:Landroid/text/Spannable;
    :sswitch_26
    invoke-virtual {p0}, Landroid/widget/TextView;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@29
    move-result-object v1

    #@2a
    check-cast v1, Landroid/text/Spannable;

    #@2c
    .line 9050
    .restart local v1       #text:Landroid/text/Spannable;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2f
    move-result v2

    #@30
    if-nez v2, :cond_3

    #@32
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@35
    move-result-object v2

    #@36
    if-eqz v2, :cond_3

    #@38
    .line 9051
    invoke-static {}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->getInstance()Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;

    #@3b
    move-result-object v0

    #@3c
    .line 9053
    .local v0, iterator:Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;
    invoke-virtual {v0, p0}, Landroid/widget/AccessibilityIterators$PageTextSegmentIterator;->initialize(Landroid/widget/TextView;)V

    #@3f
    goto :goto_7

    #@40
    .line 9038
    :sswitch_data_40
    .sparse-switch
        0x4 -> :sswitch_8
        0x10 -> :sswitch_26
    .end sparse-switch
.end method

.method public final getKeyListener()Landroid/text/method/KeyListener;
    .registers 2

    #@0
    .prologue
    .line 1640
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-object v0, v0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@a
    goto :goto_5
.end method

.method public final getLayout()Landroid/text/Layout;
    .registers 2

    #@0
    .prologue
    .line 1617
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    return-object v0
.end method

.method protected getLeftFadingEdgeStrength()F
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 7951
    iget-object v5, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@5
    sget-object v6, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@7
    if-ne v5, v6, :cond_42

    #@9
    iget v5, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@b
    if-eq v5, v8, :cond_42

    #@d
    .line 7953
    iget-object v5, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@f
    if-eqz v5, :cond_2d

    #@11
    iget-object v5, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@13
    invoke-virtual {v5}, Landroid/widget/TextView$Marquee;->isStopped()Z

    #@16
    move-result v5

    #@17
    if-nez v5, :cond_2d

    #@19
    .line 7954
    iget-object v2, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@1b
    .line 7955
    .local v2, marquee:Landroid/widget/TextView$Marquee;
    invoke-virtual {v2}, Landroid/widget/TextView$Marquee;->shouldDrawLeftFade()Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_2c

    #@21
    .line 7956
    invoke-virtual {v2}, Landroid/widget/TextView$Marquee;->getScroll()F

    #@24
    move-result v3

    #@25
    .line 7957
    .local v3, scroll:F
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontalFadingEdgeLength()I

    #@28
    move-result v4

    #@29
    int-to-float v4, v4

    #@2a
    div-float v4, v3, v4

    #@2c
    .line 7987
    .end local v2           #marquee:Landroid/widget/TextView$Marquee;
    .end local v3           #scroll:F
    :cond_2c
    :goto_2c
    :pswitch_2c
    return v4

    #@2d
    .line 7961
    :cond_2d
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    #@30
    move-result v5

    #@31
    if-ne v5, v8, :cond_42

    #@33
    .line 7962
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@36
    move-result v1

    #@37
    .line 7963
    .local v1, layoutDirection:I
    iget v5, p0, Landroid/widget/TextView;->mGravity:I

    #@39
    invoke-static {v5, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@3c
    move-result v0

    #@3d
    .line 7964
    .local v0, absoluteGravity:I
    and-int/lit8 v5, v0, 0x7

    #@3f
    packed-switch v5, :pswitch_data_92

    #@42
    .line 7987
    .end local v0           #absoluteGravity:I
    .end local v1           #layoutDirection:I
    :cond_42
    :pswitch_42
    invoke-super {p0}, Landroid/view/View;->getLeftFadingEdgeStrength()F

    #@45
    move-result v4

    #@46
    goto :goto_2c

    #@47
    .line 7968
    .restart local v0       #absoluteGravity:I
    .restart local v1       #layoutDirection:I
    :pswitch_47
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@49
    invoke-virtual {v4, v7}, Landroid/text/Layout;->getLineRight(I)F

    #@4c
    move-result v4

    #@4d
    iget v5, p0, Landroid/widget/TextView;->mRight:I

    #@4f
    iget v6, p0, Landroid/widget/TextView;->mLeft:I

    #@51
    sub-int/2addr v5, v6

    #@52
    int-to-float v5, v5

    #@53
    sub-float/2addr v4, v5

    #@54
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@57
    move-result v5

    #@58
    int-to-float v5, v5

    #@59
    sub-float/2addr v4, v5

    #@5a
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@5d
    move-result v5

    #@5e
    int-to-float v5, v5

    #@5f
    sub-float/2addr v4, v5

    #@60
    iget-object v5, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@62
    invoke-virtual {v5, v7}, Landroid/text/Layout;->getLineLeft(I)F

    #@65
    move-result v5

    #@66
    sub-float/2addr v4, v5

    #@67
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontalFadingEdgeLength()I

    #@6a
    move-result v5

    #@6b
    int-to-float v5, v5

    #@6c
    div-float/2addr v4, v5

    #@6d
    goto :goto_2c

    #@6e
    .line 7974
    :pswitch_6e
    invoke-virtual {p0}, Landroid/widget/TextView;->isLayoutRtl()Z

    #@71
    move-result v5

    #@72
    if-eqz v5, :cond_2c

    #@74
    .line 7976
    iget-object v4, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@76
    invoke-virtual {v4, v7}, Landroid/text/Layout;->getLineWidth(I)F

    #@79
    move-result v4

    #@7a
    iget v5, p0, Landroid/widget/TextView;->mRight:I

    #@7c
    iget v6, p0, Landroid/widget/TextView;->mLeft:I

    #@7e
    sub-int/2addr v5, v6

    #@7f
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@82
    move-result v6

    #@83
    sub-int/2addr v5, v6

    #@84
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@87
    move-result v6

    #@88
    sub-int/2addr v5, v6

    #@89
    int-to-float v5, v5

    #@8a
    sub-float/2addr v4, v5

    #@8b
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontalFadingEdgeLength()I

    #@8e
    move-result v5

    #@8f
    int-to-float v5, v5

    #@90
    div-float/2addr v4, v5

    #@91
    goto :goto_2c

    #@92
    .line 7964
    :pswitch_data_92
    .packed-switch 0x1
        :pswitch_6e
        :pswitch_42
        :pswitch_2c
        :pswitch_42
        :pswitch_47
    .end packed-switch
.end method

.method protected getLeftPaddingOffset()I
    .registers 5

    #@0
    .prologue
    .line 4869
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/widget/TextView;->mPaddingLeft:I

    #@6
    sub-int/2addr v0, v1

    #@7
    const/4 v1, 0x0

    #@8
    iget v2, p0, Landroid/widget/TextView;->mShadowDx:F

    #@a
    iget v3, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@c
    sub-float/2addr v2, v3

    #@d
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    #@10
    move-result v1

    #@11
    float-to-int v1, v1

    #@12
    add-int/2addr v0, v1

    #@13
    return v0
.end method

.method getLineAtCoordinate(F)I
    .registers 4
    .parameter "y"

    #@0
    .prologue
    .line 8815
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingTop()I

    #@3
    move-result v0

    #@4
    int-to-float v0, v0

    #@5
    sub-float/2addr p1, v0

    #@6
    .line 8817
    const/4 v0, 0x0

    #@7
    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    #@a
    move-result p1

    #@b
    .line 8818
    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    #@e
    move-result v0

    #@f
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingBottom()I

    #@12
    move-result v1

    #@13
    sub-int/2addr v0, v1

    #@14
    add-int/lit8 v0, v0, -0x1

    #@16
    int-to-float v0, v0

    #@17
    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    #@1a
    move-result p1

    #@1b
    .line 8819
    invoke-virtual {p0}, Landroid/widget/TextView;->getScrollY()I

    #@1e
    move-result v0

    #@1f
    int-to-float v0, v0

    #@20
    add-float/2addr p1, v0

    #@21
    .line 8820
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@24
    move-result-object v0

    #@25
    float-to-int v1, p1

    #@26
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    #@29
    move-result v0

    #@2a
    return v0
.end method

.method public getLineBounds(ILandroid/graphics/Rect;)I
    .registers 7
    .parameter "line"
    .parameter "bounds"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5361
    iget-object v3, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@3
    if-nez v3, :cond_b

    #@5
    .line 5362
    if-eqz p2, :cond_a

    #@7
    .line 5363
    invoke-virtual {p2, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@a
    .line 5377
    :cond_a
    :goto_a
    return v2

    #@b
    .line 5368
    :cond_b
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@d
    invoke-virtual {v2, p1, p2}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    #@10
    move-result v0

    #@11
    .line 5370
    .local v0, baseline:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@14
    move-result v1

    #@15
    .line 5371
    .local v1, voffset:I
    iget v2, p0, Landroid/widget/TextView;->mGravity:I

    #@17
    and-int/lit8 v2, v2, 0x70

    #@19
    const/16 v3, 0x30

    #@1b
    if-eq v2, v3, :cond_23

    #@1d
    .line 5372
    const/4 v2, 0x1

    #@1e
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@21
    move-result v2

    #@22
    add-int/2addr v1, v2

    #@23
    .line 5374
    :cond_23
    if-eqz p2, :cond_2c

    #@25
    .line 5375
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@28
    move-result v2

    #@29
    invoke-virtual {p2, v2, v1}, Landroid/graphics/Rect;->offset(II)V

    #@2c
    .line 5377
    :cond_2c
    add-int v2, v0, v1

    #@2e
    goto :goto_a
.end method

.method public getLineCount()I
    .registers 2

    #@0
    .prologue
    .line 5348
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@6
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getLineHeight()I
    .registers 3

    #@0
    .prologue
    .line 1609
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@6
    move-result v0

    #@7
    int-to-float v0, v0

    #@8
    iget v1, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@a
    mul-float/2addr v0, v1

    #@b
    iget v1, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@d
    add-float/2addr v0, v1

    #@e
    invoke-static {v0}, Lcom/android/internal/util/FastMath;->round(F)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public getLineSpacingExtra()F
    .registers 2

    #@0
    .prologue
    .line 3433
    iget v0, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@2
    return v0
.end method

.method public getLineSpacingMultiplier()F
    .registers 2

    #@0
    .prologue
    .line 3419
    iget v0, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@2
    return v0
.end method

.method public final getLinkTextColors()Landroid/content/res/ColorStateList;
    .registers 2

    #@0
    .prologue
    .line 2974
    iget-object v0, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@2
    return-object v0
.end method

.method public final getLinksClickable()Z
    .registers 2

    #@0
    .prologue
    .line 2863
    iget-boolean v0, p0, Landroid/widget/TextView;->mLinksClickable:Z

    #@2
    return v0
.end method

.method public getMarqueeRepeatLimit()I
    .registers 2

    #@0
    .prologue
    .line 7311
    iget v0, p0, Landroid/widget/TextView;->mMarqueeRepeatLimit:I

    #@2
    return v0
.end method

.method public getMaxEms()I
    .registers 3

    #@0
    .prologue
    .line 3318
    iget v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMaxHeight()I
    .registers 3

    #@0
    .prologue
    .line 3201
    iget v0, p0, Landroid/widget/TextView;->mMaxMode:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMaximum:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMaxLines()I
    .registers 3

    #@0
    .prologue
    .line 3172
    iget v0, p0, Landroid/widget/TextView;->mMaxMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMaximum:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMaxWidth()I
    .registers 3

    #@0
    .prologue
    .line 3345
    iget v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMinEms()I
    .registers 3

    #@0
    .prologue
    .line 3264
    iget v0, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMinWidth:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMinHeight()I
    .registers 3

    #@0
    .prologue
    .line 3144
    iget v0, p0, Landroid/widget/TextView;->mMinMode:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMinimum:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMinLines()I
    .registers 3

    #@0
    .prologue
    .line 3116
    iget v0, p0, Landroid/widget/TextView;->mMinMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMinimum:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public getMinWidth()I
    .registers 3

    #@0
    .prologue
    .line 3291
    iget v0, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/TextView;->mMinWidth:I

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, -0x1

    #@9
    goto :goto_7
.end method

.method public final getMovementMethod()Landroid/text/method/MovementMethod;
    .registers 2

    #@0
    .prologue
    .line 1706
    iget-object v0, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@2
    return-object v0
.end method

.method public getOffsetForPosition(FF)I
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 8799
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@3
    move-result-object v2

    #@4
    if-nez v2, :cond_8

    #@6
    const/4 v1, -0x1

    #@7
    .line 8802
    :goto_7
    return v1

    #@8
    .line 8800
    :cond_8
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->getLineAtCoordinate(F)I

    #@b
    move-result v0

    #@c
    .line 8801
    .local v0, line:I
    invoke-direct {p0, v0, p1}, Landroid/widget/TextView;->getOffsetAtCoordinate(IF)I

    #@f
    move-result v1

    #@10
    .line 8802
    .local v1, offset:I
    goto :goto_7
.end method

.method public getPaint()Landroid/text/TextPaint;
    .registers 2

    #@0
    .prologue
    .line 2826
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    return-object v0
.end method

.method public getPaintFlags()I
    .registers 2

    #@0
    .prologue
    .line 3038
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getFlags()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getPrivateImeOptions()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4426
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@8
    if-eqz v0, :cond_11

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@e
    iget-object v0, v0, Landroid/widget/Editor$InputContentType;->privateImeOptions:Ljava/lang/String;

    #@10
    :goto_10
    return-object v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method protected getRightFadingEdgeStrength()F
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    const/4 v7, 0x0

    #@3
    .line 7992
    iget-object v8, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@5
    sget-object v9, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@7
    if-ne v8, v9, :cond_41

    #@9
    iget v8, p0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@b
    if-eq v8, v11, :cond_41

    #@d
    .line 7994
    iget-object v8, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@f
    if-eqz v8, :cond_2c

    #@11
    iget-object v8, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@13
    invoke-virtual {v8}, Landroid/widget/TextView$Marquee;->isStopped()Z

    #@16
    move-result v8

    #@17
    if-nez v8, :cond_2c

    #@19
    .line 7995
    iget-object v3, p0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@1b
    .line 7996
    .local v3, marquee:Landroid/widget/TextView$Marquee;
    invoke-virtual {v3}, Landroid/widget/TextView$Marquee;->getMaxFadeScroll()F

    #@1e
    move-result v4

    #@1f
    .line 7997
    .local v4, maxFadeScroll:F
    invoke-virtual {v3}, Landroid/widget/TextView$Marquee;->getScroll()F

    #@22
    move-result v5

    #@23
    .line 7998
    .local v5, scroll:F
    sub-float v7, v4, v5

    #@25
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontalFadingEdgeLength()I

    #@28
    move-result v8

    #@29
    int-to-float v8, v8

    #@2a
    div-float/2addr v7, v8

    #@2b
    .line 8025
    .end local v3           #marquee:Landroid/widget/TextView$Marquee;
    .end local v4           #maxFadeScroll:F
    .end local v5           #scroll:F
    :cond_2b
    :goto_2b
    :pswitch_2b
    return v7

    #@2c
    .line 7999
    :cond_2c
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    #@2f
    move-result v8

    #@30
    if-ne v8, v11, :cond_41

    #@32
    .line 8000
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@35
    move-result v1

    #@36
    .line 8001
    .local v1, layoutDirection:I
    iget v8, p0, Landroid/widget/TextView;->mGravity:I

    #@38
    invoke-static {v8, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@3b
    move-result v0

    #@3c
    .line 8002
    .local v0, absoluteGravity:I
    and-int/lit8 v8, v0, 0x7

    #@3e
    packed-switch v8, :pswitch_data_8a

    #@41
    .line 8025
    .end local v0           #absoluteGravity:I
    .end local v1           #layoutDirection:I
    :cond_41
    :pswitch_41
    invoke-super {p0}, Landroid/view/View;->getRightFadingEdgeStrength()F

    #@44
    move-result v7

    #@45
    goto :goto_2b

    #@46
    .line 8004
    .restart local v0       #absoluteGravity:I
    .restart local v1       #layoutDirection:I
    :pswitch_46
    iget v7, p0, Landroid/widget/TextView;->mRight:I

    #@48
    iget v8, p0, Landroid/widget/TextView;->mLeft:I

    #@4a
    sub-int/2addr v7, v8

    #@4b
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@4e
    move-result v8

    #@4f
    sub-int/2addr v7, v8

    #@50
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@53
    move-result v8

    #@54
    sub-int v6, v7, v8

    #@56
    .line 8006
    .local v6, textWidth:I
    iget-object v7, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@58
    invoke-virtual {v7, v10}, Landroid/text/Layout;->getLineWidth(I)F

    #@5b
    move-result v2

    #@5c
    .line 8007
    .local v2, lineWidth:F
    int-to-float v7, v6

    #@5d
    sub-float v7, v2, v7

    #@5f
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontalFadingEdgeLength()I

    #@62
    move-result v8

    #@63
    int-to-float v8, v8

    #@64
    div-float/2addr v7, v8

    #@65
    goto :goto_2b

    #@66
    .line 8014
    .end local v2           #lineWidth:F
    .end local v6           #textWidth:I
    :pswitch_66
    invoke-virtual {p0}, Landroid/widget/TextView;->isLayoutRtl()Z

    #@69
    move-result v8

    #@6a
    if-nez v8, :cond_2b

    #@6c
    .line 8019
    iget-object v7, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@6e
    invoke-virtual {v7, v10}, Landroid/text/Layout;->getLineWidth(I)F

    #@71
    move-result v7

    #@72
    iget v8, p0, Landroid/widget/TextView;->mRight:I

    #@74
    iget v9, p0, Landroid/widget/TextView;->mLeft:I

    #@76
    sub-int/2addr v8, v9

    #@77
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@7a
    move-result v9

    #@7b
    sub-int/2addr v8, v9

    #@7c
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@7f
    move-result v9

    #@80
    sub-int/2addr v8, v9

    #@81
    int-to-float v8, v8

    #@82
    sub-float/2addr v7, v8

    #@83
    invoke-virtual {p0}, Landroid/widget/TextView;->getHorizontalFadingEdgeLength()I

    #@86
    move-result v8

    #@87
    int-to-float v8, v8

    #@88
    div-float/2addr v7, v8

    #@89
    goto :goto_2b

    #@8a
    .line 8002
    :pswitch_data_8a
    .packed-switch 0x1
        :pswitch_66
        :pswitch_41
        :pswitch_46
        :pswitch_41
        :pswitch_2b
        :pswitch_41
        :pswitch_66
    .end packed-switch
.end method

.method protected getRightPaddingOffset()I
    .registers 5

    #@0
    .prologue
    .line 4885
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/widget/TextView;->mPaddingRight:I

    #@6
    sub-int/2addr v0, v1

    #@7
    neg-int v0, v0

    #@8
    const/4 v1, 0x0

    #@9
    iget v2, p0, Landroid/widget/TextView;->mShadowDx:F

    #@b
    iget v3, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@d
    add-float/2addr v2, v3

    #@e
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    #@11
    move-result v1

    #@12
    float-to-int v1, v1

    #@13
    add-int/2addr v0, v1

    #@14
    return v0
.end method

.method public getSelectionEnd()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
    .end annotation

    #@0
    .prologue
    .line 7164
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getSelectionStart()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
    .end annotation

    #@0
    .prologue
    .line 7156
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getShadowColor()I
    .registers 2

    #@0
    .prologue
    .line 2818
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    iget v0, v0, Landroid/text/TextPaint;->shadowColor:I

    #@4
    return v0
.end method

.method public getShadowDx()F
    .registers 2

    #@0
    .prologue
    .line 2796
    iget v0, p0, Landroid/widget/TextView;->mShadowDx:F

    #@2
    return v0
.end method

.method public getShadowDy()F
    .registers 2

    #@0
    .prologue
    .line 2807
    iget v0, p0, Landroid/widget/TextView;->mShadowDy:F

    #@2
    return v0
.end method

.method public getShadowRadius()F
    .registers 2

    #@0
    .prologue
    .line 2785
    iget v0, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@2
    return v0
.end method

.method public final getShowSoftInputOnFocus()Z
    .registers 2

    #@0
    .prologue
    .line 2751
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget-boolean v0, v0, Landroid/widget/Editor;->mShowSoftInputOnFocus:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public getText()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 1582
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public final getTextColors()Landroid/content/res/ColorStateList;
    .registers 2

    #@0
    .prologue
    .line 2697
    iget-object v0, p0, Landroid/widget/TextView;->mTextColor:Landroid/content/res/ColorStateList;

    #@2
    return-object v0
.end method

.method getTextDirectionHeuristic()Landroid/text/TextDirectionHeuristic;
    .registers 2

    #@0
    .prologue
    .line 8871
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 8874
    sget-object v0, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    #@8
    iput-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@a
    .line 8875
    sget-object v0, Landroid/text/TextDirectionHeuristics;->LOCALE:Landroid/text/TextDirectionHeuristic;

    #@c
    iput-object v0, p0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@e
    .line 8876
    iget-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@10
    .line 8913
    :goto_10
    return-object v0

    #@11
    .line 8887
    :cond_11
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextDirection()I

    #@14
    move-result v0

    #@15
    packed-switch v0, :pswitch_data_38

    #@18
    .line 8894
    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@1a
    iput-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@1c
    .line 8912
    :goto_1c
    iget-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@1e
    iput-object v0, p0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@20
    .line 8913
    iget-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@22
    goto :goto_10

    #@23
    .line 8900
    :pswitch_23
    sget-object v0, Landroid/text/TextDirectionHeuristics;->ANYRTL_LTR:Landroid/text/TextDirectionHeuristic;

    #@25
    iput-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@27
    goto :goto_1c

    #@28
    .line 8903
    :pswitch_28
    sget-object v0, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    #@2a
    iput-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@2c
    goto :goto_1c

    #@2d
    .line 8906
    :pswitch_2d
    sget-object v0, Landroid/text/TextDirectionHeuristics;->RTL:Landroid/text/TextDirectionHeuristic;

    #@2f
    iput-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@31
    goto :goto_1c

    #@32
    .line 8909
    :pswitch_32
    sget-object v0, Landroid/text/TextDirectionHeuristics;->LOCALE:Landroid/text/TextDirectionHeuristic;

    #@34
    iput-object v0, p0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@36
    goto :goto_1c

    #@37
    .line 8887
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x2
        :pswitch_23
        :pswitch_28
        :pswitch_2d
        :pswitch_32
    .end packed-switch
.end method

.method public getTextForAccessibility()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 8314
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    .line 8315
    .local v0, text:Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_e

    #@a
    .line 8316
    invoke-virtual {p0}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    #@d
    move-result-object v0

    #@e
    .line 8318
    :cond_e
    return-object v0
.end method

.method public getTextLocale()Ljava/util/Locale;
    .registers 2

    #@0
    .prologue
    .line 2516
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextLocale()Ljava/util/Locale;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTextScaleX()F
    .registers 2

    #@0
    .prologue
    .line 2593
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextScaleX()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getTextServicesLocale()Ljava/util/Locale;
    .registers 2

    #@0
    .prologue
    .line 8182
    iget-object v0, p0, Landroid/widget/TextView;->mCurrentTextServicesLocaleCache:Ljava/util/Locale;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 8184
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/TextView;->mCurrentTextServicesLocaleCache:Ljava/util/Locale;

    #@a
    .line 8187
    :cond_a
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextServicesLocaleAsync()V

    #@d
    .line 8188
    iget-object v0, p0, Landroid/widget/TextView;->mCurrentTextServicesLocaleCache:Ljava/util/Locale;

    #@f
    return-object v0
.end method

.method public getTextSize()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
    .end annotation

    #@0
    .prologue
    .line 2537
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected getTopPaddingOffset()I
    .registers 4

    #@0
    .prologue
    .line 4875
    const/4 v0, 0x0

    #@1
    iget v1, p0, Landroid/widget/TextView;->mShadowDy:F

    #@3
    iget v2, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@5
    sub-float/2addr v1, v2

    #@6
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    #@9
    move-result v0

    #@a
    float-to-int v0, v0

    #@b
    return v0
.end method

.method public getTotalPaddingBottom()I
    .registers 3

    #@0
    .prologue
    .line 1991
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-direct {p0, v1}, Landroid/widget/TextView;->getBottomVerticalOffset(Z)I

    #@8
    move-result v1

    #@9
    add-int/2addr v0, v1

    #@a
    return v0
.end method

.method public getTotalPaddingEnd()I
    .registers 2

    #@0
    .prologue
    .line 1973
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingEnd()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getTotalPaddingLeft()I
    .registers 2

    #@0
    .prologue
    .line 1949
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getTotalPaddingRight()I
    .registers 2

    #@0
    .prologue
    .line 1957
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getTotalPaddingStart()I
    .registers 2

    #@0
    .prologue
    .line 1965
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingStart()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getTotalPaddingTop()I
    .registers 3

    #@0
    .prologue
    .line 1982
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@8
    move-result v1

    #@9
    add-int/2addr v0, v1

    #@a
    return v0
.end method

.method public final getTransformationMethod()Landroid/text/method/TransformationMethod;
    .registers 2

    #@0
    .prologue
    .line 1757
    iget-object v0, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@2
    return-object v0
.end method

.method getTransformedText(II)Ljava/lang/CharSequence;
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 8445
    iget-object v0, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@2
    invoke-interface {v0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->removeSuggestionSpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .registers 2

    #@0
    .prologue
    .line 2651
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getUrls()[Landroid/text/style/URLSpan;
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2874
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3
    instance-of v0, v0, Landroid/text/Spanned;

    #@5
    if-eqz v0, :cond_1a

    #@7
    .line 2875
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@9
    check-cast v0, Landroid/text/Spanned;

    #@b
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@d
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@10
    move-result v1

    #@11
    const-class v2, Landroid/text/style/URLSpan;

    #@13
    invoke-interface {v0, v3, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, [Landroid/text/style/URLSpan;

    #@19
    .line 2877
    :goto_19
    return-object v0

    #@1a
    :cond_1a
    new-array v0, v3, [Landroid/text/style/URLSpan;

    #@1c
    goto :goto_19
.end method

.method getVerticalOffset(Z)I
    .registers 9
    .parameter "forceNormal"

    #@0
    .prologue
    .line 4582
    const/4 v4, 0x0

    #@1
    .line 4583
    .local v4, voffset:I
    iget v5, p0, Landroid/widget/TextView;->mGravity:I

    #@3
    and-int/lit8 v1, v5, 0x70

    #@5
    .line 4585
    .local v1, gravity:I
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@7
    .line 4586
    .local v2, l:Landroid/text/Layout;
    if-nez p1, :cond_17

    #@9
    iget-object v5, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@b
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@e
    move-result v5

    #@f
    if-nez v5, :cond_17

    #@11
    iget-object v5, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@13
    if-eqz v5, :cond_17

    #@15
    .line 4587
    iget-object v2, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@17
    .line 4590
    :cond_17
    const/16 v5, 0x30

    #@19
    if-eq v1, v5, :cond_3a

    #@1b
    .line 4593
    iget-object v5, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@1d
    if-ne v2, v5, :cond_3b

    #@1f
    .line 4594
    invoke-virtual {p0}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@22
    move-result v5

    #@23
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@26
    move-result v6

    #@27
    sub-int/2addr v5, v6

    #@28
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@2b
    move-result v6

    #@2c
    sub-int v0, v5, v6

    #@2e
    .line 4600
    .local v0, boxht:I
    :goto_2e
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    #@31
    move-result v3

    #@32
    .line 4602
    .local v3, textht:I
    if-ge v3, v0, :cond_3a

    #@34
    .line 4603
    const/16 v5, 0x50

    #@36
    if-ne v1, v5, :cond_4b

    #@38
    .line 4604
    sub-int v4, v0, v3

    #@3a
    .line 4609
    .end local v0           #boxht:I
    .end local v3           #textht:I
    :cond_3a
    :goto_3a
    return v4

    #@3b
    .line 4597
    :cond_3b
    invoke-virtual {p0}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@3e
    move-result v5

    #@3f
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@42
    move-result v6

    #@43
    sub-int/2addr v5, v6

    #@44
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@47
    move-result v6

    #@48
    sub-int v0, v5, v6

    #@4a
    .restart local v0       #boxht:I
    goto :goto_2e

    #@4b
    .line 4606
    .restart local v3       #textht:I
    :cond_4b
    sub-int v5, v0, v3

    #@4d
    shr-int/lit8 v4, v5, 0x1

    #@4f
    goto :goto_3a
.end method

.method public getWordIterator()Landroid/text/method/WordIterator;
    .registers 2

    #@0
    .prologue
    .line 8228
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 8229
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor;->getWordIterator()Landroid/text/method/WordIterator;

    #@9
    move-result-object v0

    #@a
    .line 8231
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method handleTextChanged(Ljava/lang/CharSequence;III)V
    .registers 9
    .parameter "buffer"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 7580
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v1, :cond_2b

    #@4
    const/4 v0, 0x0

    #@5
    .line 7581
    .local v0, ims:Landroid/widget/Editor$InputMethodState;
    :goto_5
    if-eqz v0, :cond_b

    #@7
    iget v1, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@9
    if-nez v1, :cond_e

    #@b
    .line 7582
    :cond_b
    invoke-virtual {p0}, Landroid/widget/TextView;->updateAfterEdit()V

    #@e
    .line 7584
    :cond_e
    if-eqz v0, :cond_24

    #@10
    .line 7585
    const/4 v1, 0x1

    #@11
    iput-boolean v1, v0, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@13
    .line 7586
    iget v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@15
    if-gez v1, :cond_30

    #@17
    .line 7587
    iput p2, v0, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@19
    .line 7588
    add-int v1, p2, p3

    #@1b
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@1d
    .line 7593
    :goto_1d
    iget v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedDelta:I

    #@1f
    sub-int v2, p4, p3

    #@21
    add-int/2addr v1, v2

    #@22
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedDelta:I

    #@24
    .line 7596
    :cond_24
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->sendOnTextChanged(Ljava/lang/CharSequence;III)V

    #@27
    .line 7597
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    #@2a
    .line 7598
    return-void

    #@2b
    .line 7580
    .end local v0           #ims:Landroid/widget/Editor$InputMethodState;
    :cond_2b
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2d
    iget-object v0, v1, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@2f
    goto :goto_5

    #@30
    .line 7590
    .restart local v0       #ims:Landroid/widget/Editor$InputMethodState;
    :cond_30
    iget v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@32
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    #@35
    move-result v1

    #@36
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@38
    .line 7591
    iget v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@3a
    add-int v2, p2, p3

    #@3c
    iget v3, v0, Landroid/widget/Editor$InputMethodState;->mChangedDelta:I

    #@3e
    sub-int/2addr v2, v3

    #@3f
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@42
    move-result v1

    #@43
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@45
    goto :goto_1d
.end method

.method public hasOverlappingRendering()Z
    .registers 2

    #@0
    .prologue
    .line 4975
    invoke-virtual {p0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    if-nez v0, :cond_12

    #@6
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@8
    instance-of v0, v0, Landroid/text/Spannable;

    #@a
    if-nez v0, :cond_12

    #@c
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    :cond_12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public hasSelection()Z
    .registers 4

    #@0
    .prologue
    .line 7171
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@3
    move-result v1

    #@4
    .line 7172
    .local v1, selectionStart:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@7
    move-result v0

    #@8
    .line 7174
    .local v0, selectionEnd:I
    if-ltz v1, :cond_e

    #@a
    if-eq v1, v0, :cond_e

    #@c
    const/4 v2, 0x1

    #@d
    :goto_d
    return v2

    #@e
    :cond_e
    const/4 v2, 0x0

    #@f
    goto :goto_d
.end method

.method public hideErrorIfUnchanged()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5695
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3
    if-eqz v0, :cond_14

    #@5
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7
    iget-object v0, v0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@9
    if-eqz v0, :cond_14

    #@b
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@d
    iget-boolean v0, v0, Landroid/widget/Editor;->mErrorWasChanged:Z

    #@f
    if-nez v0, :cond_14

    #@11
    .line 5696
    invoke-virtual {p0, v1, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    #@14
    .line 5698
    :cond_14
    return-void
.end method

.method invalidateCursor()V
    .registers 2

    #@0
    .prologue
    .line 4687
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@3
    move-result v0

    #@4
    .line 4689
    .local v0, where:I
    invoke-direct {p0, v0, v0, v0}, Landroid/widget/TextView;->invalidateCursor(III)V

    #@7
    .line 4690
    return-void
.end method

.method invalidateCursorPath()V
    .registers 12

    #@0
    .prologue
    .line 4644
    iget-boolean v5, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@2
    if-eqz v5, :cond_8

    #@4
    .line 4645
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidateCursor()V

    #@7
    .line 4684
    :cond_7
    :goto_7
    return-void

    #@8
    .line 4647
    :cond_8
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@b
    move-result v1

    #@c
    .line 4648
    .local v1, horizontalPadding:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@f
    move-result v5

    #@10
    const/4 v6, 0x1

    #@11
    invoke-virtual {p0, v6}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@14
    move-result v6

    #@15
    add-int v4, v5, v6

    #@17
    .line 4650
    .local v4, verticalPadding:I
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@19
    iget v5, v5, Landroid/widget/Editor;->mCursorCount:I

    #@1b
    if-nez v5, :cond_75

    #@1d
    .line 4651
    sget-object v6, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@1f
    monitor-enter v6

    #@20
    .line 4661
    :try_start_20
    iget-object v5, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@22
    invoke-virtual {v5}, Landroid/text/TextPaint;->getStrokeWidth()F

    #@25
    move-result v5

    #@26
    invoke-static {v5}, Landroid/util/FloatMath;->ceil(F)F

    #@29
    move-result v3

    #@2a
    .line 4662
    .local v3, thick:F
    const/high16 v5, 0x3f80

    #@2c
    cmpg-float v5, v3, v5

    #@2e
    if-gez v5, :cond_32

    #@30
    .line 4663
    const/high16 v3, 0x3f80

    #@32
    .line 4666
    :cond_32
    const/high16 v5, 0x4000

    #@34
    div-float/2addr v3, v5

    #@35
    .line 4669
    iget-object v5, p0, Landroid/widget/TextView;->mHighlightPath:Landroid/graphics/Path;

    #@37
    sget-object v7, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@39
    const/4 v8, 0x0

    #@3a
    invoke-virtual {v5, v7, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@3d
    .line 4671
    int-to-float v5, v1

    #@3e
    sget-object v7, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@40
    iget v7, v7, Landroid/graphics/RectF;->left:F

    #@42
    add-float/2addr v5, v7

    #@43
    sub-float/2addr v5, v3

    #@44
    invoke-static {v5}, Landroid/util/FloatMath;->floor(F)F

    #@47
    move-result v5

    #@48
    float-to-int v5, v5

    #@49
    int-to-float v7, v4

    #@4a
    sget-object v8, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@4c
    iget v8, v8, Landroid/graphics/RectF;->top:F

    #@4e
    add-float/2addr v7, v8

    #@4f
    sub-float/2addr v7, v3

    #@50
    invoke-static {v7}, Landroid/util/FloatMath;->floor(F)F

    #@53
    move-result v7

    #@54
    float-to-int v7, v7

    #@55
    int-to-float v8, v1

    #@56
    sget-object v9, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@58
    iget v9, v9, Landroid/graphics/RectF;->right:F

    #@5a
    add-float/2addr v8, v9

    #@5b
    add-float/2addr v8, v3

    #@5c
    invoke-static {v8}, Landroid/util/FloatMath;->ceil(F)F

    #@5f
    move-result v8

    #@60
    float-to-int v8, v8

    #@61
    int-to-float v9, v4

    #@62
    sget-object v10, Landroid/widget/TextView;->TEMP_RECTF:Landroid/graphics/RectF;

    #@64
    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    #@66
    add-float/2addr v9, v10

    #@67
    add-float/2addr v9, v3

    #@68
    invoke-static {v9}, Landroid/util/FloatMath;->ceil(F)F

    #@6b
    move-result v9

    #@6c
    float-to-int v9, v9

    #@6d
    invoke-virtual {p0, v5, v7, v8, v9}, Landroid/widget/TextView;->invalidate(IIII)V

    #@70
    .line 4675
    monitor-exit v6

    #@71
    goto :goto_7

    #@72
    .end local v3           #thick:F
    :catchall_72
    move-exception v5

    #@73
    monitor-exit v6
    :try_end_74
    .catchall {:try_start_20 .. :try_end_74} :catchall_72

    #@74
    throw v5

    #@75
    .line 4677
    :cond_75
    const/4 v2, 0x0

    #@76
    .local v2, i:I
    :goto_76
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@78
    iget v5, v5, Landroid/widget/Editor;->mCursorCount:I

    #@7a
    if-ge v2, v5, :cond_7

    #@7c
    .line 4678
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7e
    iget-object v5, v5, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@80
    aget-object v5, v5, v2

    #@82
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@85
    move-result-object v0

    #@86
    .line 4679
    .local v0, bounds:Landroid/graphics/Rect;
    iget v5, v0, Landroid/graphics/Rect;->left:I

    #@88
    add-int/2addr v5, v1

    #@89
    iget v6, v0, Landroid/graphics/Rect;->top:I

    #@8b
    add-int/2addr v6, v4

    #@8c
    iget v7, v0, Landroid/graphics/Rect;->right:I

    #@8e
    add-int/2addr v7, v1

    #@8f
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    #@91
    add-int/2addr v8, v4

    #@92
    invoke-virtual {p0, v5, v6, v7, v8}, Landroid/widget/TextView;->invalidate(IIII)V

    #@95
    .line 4677
    add-int/lit8 v2, v2, 0x1

    #@97
    goto :goto_76
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 16
    .parameter "drawable"

    #@0
    .prologue
    .line 4927
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v10

    #@4
    if-eqz v10, :cond_40

    #@6
    .line 4928
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@9
    move-result-object v4

    #@a
    .line 4929
    .local v4, dirty:Landroid/graphics/Rect;
    iget v7, p0, Landroid/widget/TextView;->mScrollX:I

    #@c
    .line 4930
    .local v7, scrollX:I
    iget v8, p0, Landroid/widget/TextView;->mScrollY:I

    #@e
    .line 4935
    .local v8, scrollY:I
    iget-object v5, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@10
    .line 4936
    .local v5, drawables:Landroid/widget/TextView$Drawables;
    if-eqz v5, :cond_31

    #@12
    .line 4937
    iget-object v10, v5, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@14
    if-ne p1, v10, :cond_41

    #@16
    .line 4938
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@19
    move-result v3

    #@1a
    .line 4939
    .local v3, compoundPaddingTop:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@1d
    move-result v0

    #@1e
    .line 4940
    .local v0, compoundPaddingBottom:I
    iget v10, p0, Landroid/widget/TextView;->mBottom:I

    #@20
    iget v11, p0, Landroid/widget/TextView;->mTop:I

    #@22
    sub-int/2addr v10, v11

    #@23
    sub-int/2addr v10, v0

    #@24
    sub-int v9, v10, v3

    #@26
    .line 4942
    .local v9, vspace:I
    iget v10, p0, Landroid/widget/TextView;->mPaddingLeft:I

    #@28
    add-int/2addr v7, v10

    #@29
    .line 4943
    iget v10, v5, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@2b
    sub-int v10, v9, v10

    #@2d
    div-int/lit8 v10, v10, 0x2

    #@2f
    add-int/2addr v10, v3

    #@30
    add-int/2addr v8, v10

    #@31
    .line 4968
    .end local v0           #compoundPaddingBottom:I
    .end local v3           #compoundPaddingTop:I
    .end local v9           #vspace:I
    :cond_31
    :goto_31
    iget v10, v4, Landroid/graphics/Rect;->left:I

    #@33
    add-int/2addr v10, v7

    #@34
    iget v11, v4, Landroid/graphics/Rect;->top:I

    #@36
    add-int/2addr v11, v8

    #@37
    iget v12, v4, Landroid/graphics/Rect;->right:I

    #@39
    add-int/2addr v12, v7

    #@3a
    iget v13, v4, Landroid/graphics/Rect;->bottom:I

    #@3c
    add-int/2addr v13, v8

    #@3d
    invoke-virtual {p0, v10, v11, v12, v13}, Landroid/widget/TextView;->invalidate(IIII)V

    #@40
    .line 4971
    .end local v4           #dirty:Landroid/graphics/Rect;
    .end local v5           #drawables:Landroid/widget/TextView$Drawables;
    .end local v7           #scrollX:I
    .end local v8           #scrollY:I
    :cond_40
    return-void

    #@41
    .line 4944
    .restart local v4       #dirty:Landroid/graphics/Rect;
    .restart local v5       #drawables:Landroid/widget/TextView$Drawables;
    .restart local v7       #scrollX:I
    .restart local v8       #scrollY:I
    :cond_41
    iget-object v10, v5, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@43
    if-ne p1, v10, :cond_6a

    #@45
    .line 4945
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@48
    move-result v3

    #@49
    .line 4946
    .restart local v3       #compoundPaddingTop:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@4c
    move-result v0

    #@4d
    .line 4947
    .restart local v0       #compoundPaddingBottom:I
    iget v10, p0, Landroid/widget/TextView;->mBottom:I

    #@4f
    iget v11, p0, Landroid/widget/TextView;->mTop:I

    #@51
    sub-int/2addr v10, v11

    #@52
    sub-int/2addr v10, v0

    #@53
    sub-int v9, v10, v3

    #@55
    .line 4949
    .restart local v9       #vspace:I
    iget v10, p0, Landroid/widget/TextView;->mRight:I

    #@57
    iget v11, p0, Landroid/widget/TextView;->mLeft:I

    #@59
    sub-int/2addr v10, v11

    #@5a
    iget v11, p0, Landroid/widget/TextView;->mPaddingRight:I

    #@5c
    sub-int/2addr v10, v11

    #@5d
    iget v11, v5, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@5f
    sub-int/2addr v10, v11

    #@60
    add-int/2addr v7, v10

    #@61
    .line 4950
    iget v10, v5, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@63
    sub-int v10, v9, v10

    #@65
    div-int/lit8 v10, v10, 0x2

    #@67
    add-int/2addr v10, v3

    #@68
    add-int/2addr v8, v10

    #@69
    .line 4951
    goto :goto_31

    #@6a
    .end local v0           #compoundPaddingBottom:I
    .end local v3           #compoundPaddingTop:I
    .end local v9           #vspace:I
    :cond_6a
    iget-object v10, v5, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@6c
    if-ne p1, v10, :cond_8a

    #@6e
    .line 4952
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@71
    move-result v1

    #@72
    .line 4953
    .local v1, compoundPaddingLeft:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@75
    move-result v2

    #@76
    .line 4954
    .local v2, compoundPaddingRight:I
    iget v10, p0, Landroid/widget/TextView;->mRight:I

    #@78
    iget v11, p0, Landroid/widget/TextView;->mLeft:I

    #@7a
    sub-int/2addr v10, v11

    #@7b
    sub-int/2addr v10, v2

    #@7c
    sub-int v6, v10, v1

    #@7e
    .line 4956
    .local v6, hspace:I
    iget v10, v5, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@80
    sub-int v10, v6, v10

    #@82
    div-int/lit8 v10, v10, 0x2

    #@84
    add-int/2addr v10, v1

    #@85
    add-int/2addr v7, v10

    #@86
    .line 4957
    iget v10, p0, Landroid/widget/TextView;->mPaddingTop:I

    #@88
    add-int/2addr v8, v10

    #@89
    .line 4958
    goto :goto_31

    #@8a
    .end local v1           #compoundPaddingLeft:I
    .end local v2           #compoundPaddingRight:I
    .end local v6           #hspace:I
    :cond_8a
    iget-object v10, v5, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@8c
    if-ne p1, v10, :cond_31

    #@8e
    .line 4959
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@91
    move-result v1

    #@92
    .line 4960
    .restart local v1       #compoundPaddingLeft:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@95
    move-result v2

    #@96
    .line 4961
    .restart local v2       #compoundPaddingRight:I
    iget v10, p0, Landroid/widget/TextView;->mRight:I

    #@98
    iget v11, p0, Landroid/widget/TextView;->mLeft:I

    #@9a
    sub-int/2addr v10, v11

    #@9b
    sub-int/2addr v10, v2

    #@9c
    sub-int v6, v10, v1

    #@9e
    .line 4963
    .restart local v6       #hspace:I
    iget v10, v5, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@a0
    sub-int v10, v6, v10

    #@a2
    div-int/lit8 v10, v10, 0x2

    #@a4
    add-int/2addr v10, v1

    #@a5
    add-int/2addr v7, v10

    #@a6
    .line 4964
    iget v10, p0, Landroid/widget/TextView;->mBottom:I

    #@a8
    iget v11, p0, Landroid/widget/TextView;->mTop:I

    #@aa
    sub-int/2addr v10, v11

    #@ab
    iget v11, p0, Landroid/widget/TextView;->mPaddingBottom:I

    #@ad
    sub-int/2addr v10, v11

    #@ae
    iget v11, v5, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@b0
    sub-int/2addr v10, v11

    #@b1
    add-int/2addr v8, v10

    #@b2
    goto/16 :goto_31
.end method

.method invalidateRegion(IIZ)V
    .registers 20
    .parameter "start"
    .parameter "end"
    .parameter "invalidateCursor"

    #@0
    .prologue
    .line 4704
    move-object/from16 v0, p0

    #@2
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@4
    if-nez v12, :cond_a

    #@6
    .line 4705
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->invalidate()V

    #@9
    .line 4756
    :goto_9
    return-void

    #@a
    .line 4707
    :cond_a
    move-object/from16 v0, p0

    #@c
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@e
    move/from16 v0, p1

    #@10
    invoke-virtual {v12, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@13
    move-result v8

    #@14
    .line 4708
    .local v8, lineStart:I
    move-object/from16 v0, p0

    #@16
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@18
    invoke-virtual {v12, v8}, Landroid/text/Layout;->getLineTop(I)I

    #@1b
    move-result v10

    #@1c
    .line 4716
    .local v10, top:I
    if-lez v8, :cond_29

    #@1e
    .line 4717
    move-object/from16 v0, p0

    #@20
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@22
    add-int/lit8 v13, v8, -0x1

    #@24
    invoke-virtual {v12, v13}, Landroid/text/Layout;->getLineDescent(I)I

    #@27
    move-result v12

    #@28
    sub-int/2addr v10, v12

    #@29
    .line 4722
    :cond_29
    move/from16 v0, p1

    #@2b
    move/from16 v1, p2

    #@2d
    if-ne v0, v1, :cond_64

    #@2f
    .line 4723
    move v7, v8

    #@30
    .line 4727
    .local v7, lineEnd:I
    :goto_30
    move-object/from16 v0, p0

    #@32
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@34
    invoke-virtual {v12, v7}, Landroid/text/Layout;->getLineBottom(I)I

    #@37
    move-result v2

    #@38
    .line 4730
    .local v2, bottom:I
    if-eqz p3, :cond_6f

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget-object v12, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3e
    if-eqz v12, :cond_6f

    #@40
    .line 4731
    const/4 v5, 0x0

    #@41
    .local v5, i:I
    :goto_41
    move-object/from16 v0, p0

    #@43
    iget-object v12, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@45
    iget v12, v12, Landroid/widget/Editor;->mCursorCount:I

    #@47
    if-ge v5, v12, :cond_6f

    #@49
    .line 4732
    move-object/from16 v0, p0

    #@4b
    iget-object v12, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4d
    iget-object v12, v12, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@4f
    aget-object v12, v12, v5

    #@51
    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@54
    move-result-object v3

    #@55
    .line 4733
    .local v3, bounds:Landroid/graphics/Rect;
    iget v12, v3, Landroid/graphics/Rect;->top:I

    #@57
    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    #@5a
    move-result v10

    #@5b
    .line 4734
    iget v12, v3, Landroid/graphics/Rect;->bottom:I

    #@5d
    invoke-static {v2, v12}, Ljava/lang/Math;->max(II)I

    #@60
    move-result v2

    #@61
    .line 4731
    add-int/lit8 v5, v5, 0x1

    #@63
    goto :goto_41

    #@64
    .line 4725
    .end local v2           #bottom:I
    .end local v3           #bounds:Landroid/graphics/Rect;
    .end local v5           #i:I
    .end local v7           #lineEnd:I
    :cond_64
    move-object/from16 v0, p0

    #@66
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@68
    move/from16 v0, p2

    #@6a
    invoke-virtual {v12, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@6d
    move-result v7

    #@6e
    .restart local v7       #lineEnd:I
    goto :goto_30

    #@6f
    .line 4738
    .restart local v2       #bottom:I
    :cond_6f
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@72
    move-result v4

    #@73
    .line 4739
    .local v4, compoundPaddingLeft:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@76
    move-result v12

    #@77
    const/4 v13, 0x1

    #@78
    move-object/from16 v0, p0

    #@7a
    invoke-virtual {v0, v13}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@7d
    move-result v13

    #@7e
    add-int v11, v12, v13

    #@80
    .line 4742
    .local v11, verticalPadding:I
    if-ne v8, v7, :cond_b5

    #@82
    if-nez p3, :cond_b5

    #@84
    .line 4743
    move-object/from16 v0, p0

    #@86
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@88
    move/from16 v0, p1

    #@8a
    invoke-virtual {v12, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@8d
    move-result v12

    #@8e
    float-to-int v6, v12

    #@8f
    .line 4744
    .local v6, left:I
    move-object/from16 v0, p0

    #@91
    iget-object v12, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@93
    move/from16 v0, p2

    #@95
    invoke-virtual {v12, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@98
    move-result v12

    #@99
    float-to-double v12, v12

    #@9a
    const-wide/high16 v14, 0x3ff0

    #@9c
    add-double/2addr v12, v14

    #@9d
    double-to-int v9, v12

    #@9e
    .line 4745
    .local v9, right:I
    add-int/2addr v6, v4

    #@9f
    .line 4746
    add-int/2addr v9, v4

    #@a0
    .line 4753
    :goto_a0
    move-object/from16 v0, p0

    #@a2
    iget v12, v0, Landroid/widget/TextView;->mScrollX:I

    #@a4
    add-int/2addr v12, v6

    #@a5
    add-int v13, v11, v10

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget v14, v0, Landroid/widget/TextView;->mScrollX:I

    #@ab
    add-int/2addr v14, v9

    #@ac
    add-int v15, v11, v2

    #@ae
    move-object/from16 v0, p0

    #@b0
    invoke-virtual {v0, v12, v13, v14, v15}, Landroid/widget/TextView;->invalidate(IIII)V

    #@b3
    goto/16 :goto_9

    #@b5
    .line 4749
    .end local v6           #left:I
    .end local v9           #right:I
    :cond_b5
    move v6, v4

    #@b6
    .line 4750
    .restart local v6       #left:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getWidth()I

    #@b9
    move-result v12

    #@ba
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@bd
    move-result v13

    #@be
    sub-int v9, v12, v13

    #@c0
    .restart local v9       #right:I
    goto :goto_a0
.end method

.method public isCursorVisible()Z
    .registers 2

    #@0
    .prologue
    .line 7371
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-boolean v0, v0, Landroid/widget/Editor;->mCursorVisible:Z

    #@a
    goto :goto_5
.end method

.method isInBatchEditMode()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 8862
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 8867
    :cond_5
    :goto_5
    return v1

    #@6
    .line 8863
    :cond_6
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-object v0, v2, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@a
    .line 8864
    .local v0, ims:Landroid/widget/Editor$InputMethodState;
    if-eqz v0, :cond_12

    #@c
    .line 8865
    iget v2, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@e
    if-lez v2, :cond_5

    #@10
    const/4 v1, 0x1

    #@11
    goto :goto_5

    #@12
    .line 8867
    :cond_12
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@14
    iget-boolean v1, v1, Landroid/widget/Editor;->mInBatchEditControllers:Z

    #@16
    goto :goto_5
.end method

.method public isInputMethodTarget()Z
    .registers 3

    #@0
    .prologue
    .line 8337
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3
    move-result-object v0

    #@4
    .line 8338
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_e

    #@6
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    const/4 v1, 0x1

    #@d
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method public final isMarqueeAlwaysEnable()Z
    .registers 2

    #@0
    .prologue
    .line 3963
    iget-boolean v0, p0, Landroid/widget/TextView;->mMarqueeAlwaysEnable:Z

    #@2
    return v0
.end method

.method public isNoEmojiEditMode()Z
    .registers 2

    #@0
    .prologue
    .line 9512
    iget-boolean v0, p0, Landroid/widget/TextView;->mNoEmojiEditMode:Z

    #@2
    return v0
.end method

.method protected isPaddingOffsetRequired()Z
    .registers 3

    #@0
    .prologue
    .line 4864
    iget v0, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@2
    const/4 v1, 0x0

    #@3
    cmpl-float v0, v0, v1

    #@5
    if-nez v0, :cond_b

    #@7
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@9
    if-eqz v0, :cond_d

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method isSingleLine()Z
    .registers 2

    #@0
    .prologue
    .line 4024
    iget-boolean v0, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@2
    return v0
.end method

.method public isSuggestionsEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 8505
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 8512
    :cond_6
    :goto_6
    return v1

    #@7
    .line 8506
    :cond_7
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9
    iget v3, v3, Landroid/widget/Editor;->mInputType:I

    #@b
    and-int/lit8 v3, v3, 0xf

    #@d
    if-ne v3, v2, :cond_6

    #@f
    .line 8509
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@11
    iget v3, v3, Landroid/widget/Editor;->mInputType:I

    #@13
    const/high16 v4, 0x8

    #@15
    and-int/2addr v3, v4

    #@16
    if-gtz v3, :cond_6

    #@18
    .line 8511
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1a
    iget v3, v3, Landroid/widget/Editor;->mInputType:I

    #@1c
    and-int/lit16 v0, v3, 0xff0

    #@1e
    .line 8512
    .local v0, variation:I
    if-eqz v0, :cond_30

    #@20
    const/16 v3, 0x30

    #@22
    if-eq v0, v3, :cond_30

    #@24
    const/16 v3, 0x50

    #@26
    if-eq v0, v3, :cond_30

    #@28
    const/16 v3, 0x40

    #@2a
    if-eq v0, v3, :cond_30

    #@2c
    const/16 v3, 0xa0

    #@2e
    if-ne v0, v3, :cond_6

    #@30
    :cond_30
    move v1, v2

    #@31
    goto :goto_6
.end method

.method isTextEditable()Z
    .registers 2

    #@0
    .prologue
    .line 7915
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    instance-of v0, v0, Landroid/text/Editable;

    #@4
    if-eqz v0, :cond_14

    #@6
    invoke-virtual {p0}, Landroid/widget/TextView;->onCheckIsTextEditor()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public isTextSelectable()Z
    .registers 2

    #@0
    .prologue
    .line 4996
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-boolean v0, v0, Landroid/widget/Editor;->mTextIsSelectable:Z

    #@a
    goto :goto_5
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 4902
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@3
    .line 4903
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@5
    if-eqz v0, :cond_55

    #@7
    .line 4904
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@9
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@b
    if-eqz v0, :cond_14

    #@d
    .line 4905
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@f
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@14
    .line 4907
    :cond_14
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@16
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@18
    if-eqz v0, :cond_21

    #@1a
    .line 4908
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@1c
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@21
    .line 4910
    :cond_21
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@23
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@25
    if-eqz v0, :cond_2e

    #@27
    .line 4911
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@29
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@2b
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@2e
    .line 4913
    :cond_2e
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@30
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@32
    if-eqz v0, :cond_3b

    #@34
    .line 4914
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@36
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@38
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@3b
    .line 4916
    :cond_3b
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@3d
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@3f
    if-eqz v0, :cond_48

    #@41
    .line 4917
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@43
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@45
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@48
    .line 4919
    :cond_48
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@4a
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@4c
    if-eqz v0, :cond_55

    #@4e
    .line 4920
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@50
    iget-object v0, v0, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@52
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@55
    .line 4923
    :cond_55
    return-void
.end method

.method public length()I
    .registers 2

    #@0
    .prologue
    .line 1589
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V
    .registers 39
    .parameter "wantWidth"
    .parameter "hintWidth"
    .parameter "boring"
    .parameter "hintBoring"
    .parameter "ellipsisWidth"
    .parameter "bringIntoView"

    #@0
    .prologue
    .line 6121
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->stopMarquee()V

    #@3
    .line 6124
    move-object/from16 v0, p0

    #@5
    iget v2, v0, Landroid/widget/TextView;->mMaximum:I

    #@7
    move-object/from16 v0, p0

    #@9
    iput v2, v0, Landroid/widget/TextView;->mOldMaximum:I

    #@b
    .line 6125
    move-object/from16 v0, p0

    #@d
    iget v2, v0, Landroid/widget/TextView;->mMaxMode:I

    #@f
    move-object/from16 v0, p0

    #@11
    iput v2, v0, Landroid/widget/TextView;->mOldMaxMode:I

    #@13
    .line 6127
    const/4 v2, 0x1

    #@14
    move-object/from16 v0, p0

    #@16
    iput-boolean v2, v0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@18
    .line 6130
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isLayoutRtl()Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_24

    #@1e
    move-object/from16 v0, p0

    #@20
    instance-of v2, v0, Landroid/widget/CheckedTextView;

    #@22
    if-nez v2, :cond_2a

    #@24
    :cond_24
    move-object/from16 v0, p0

    #@26
    instance-of v2, v0, Landroid/widget/CheckBox;

    #@28
    if-eqz v2, :cond_38

    #@2a
    :cond_2a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isHardwareAccelerated()Z

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_194

    #@30
    const/high16 v2, 0x10

    #@32
    :goto_32
    move/from16 v0, p1

    #@34
    if-ne v0, v2, :cond_38

    #@36
    .line 6132
    move/from16 p1, p5

    #@38
    .line 6135
    :cond_38
    if-gez p1, :cond_3c

    #@3a
    .line 6136
    const/16 p1, 0x0

    #@3c
    .line 6138
    :cond_3c
    if-gez p2, :cond_40

    #@3e
    .line 6139
    const/16 p2, 0x0

    #@40
    .line 6142
    :cond_40
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->getLayoutAlignment()Landroid/text/Layout$Alignment;

    #@43
    move-result-object v6

    #@44
    .line 6143
    .local v6, alignment:Landroid/text/Layout$Alignment;
    move-object/from16 v0, p0

    #@46
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@48
    if-eqz v2, :cond_198

    #@4a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@4d
    move-result-object v2

    #@4e
    if-nez v2, :cond_198

    #@50
    const/4 v7, 0x1

    #@51
    .line 6144
    .local v7, shouldEllipsize:Z
    :goto_51
    move-object/from16 v0, p0

    #@53
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@55
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@57
    if-ne v2, v3, :cond_19b

    #@59
    move-object/from16 v0, p0

    #@5b
    iget v2, v0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@5d
    if-eqz v2, :cond_19b

    #@5f
    const/16 v31, 0x1

    #@61
    .line 6146
    .local v31, switchEllipsize:Z
    :goto_61
    move-object/from16 v0, p0

    #@63
    iget-object v8, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@65
    .line 6147
    .local v8, effectiveEllipsize:Landroid/text/TextUtils$TruncateAt;
    move-object/from16 v0, p0

    #@67
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@69
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@6b
    if-ne v2, v3, :cond_76

    #@6d
    move-object/from16 v0, p0

    #@6f
    iget v2, v0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@71
    const/4 v3, 0x1

    #@72
    if-ne v2, v3, :cond_76

    #@74
    .line 6149
    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    #@76
    .line 6152
    :cond_76
    move-object/from16 v0, p0

    #@78
    iget-object v2, v0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@7a
    if-nez v2, :cond_84

    #@7c
    .line 6153
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getTextDirectionHeuristic()Landroid/text/TextDirectionHeuristic;

    #@7f
    move-result-object v2

    #@80
    move-object/from16 v0, p0

    #@82
    iput-object v2, v0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@84
    .line 6156
    :cond_84
    move-object/from16 v0, p0

    #@86
    iget-object v2, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@88
    if-nez v2, :cond_92

    #@8a
    .line 6157
    move-object/from16 v0, p0

    #@8c
    iget-object v2, v0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@8e
    move-object/from16 v0, p0

    #@90
    iput-object v2, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@92
    .line 6161
    :cond_92
    move-object/from16 v0, p0

    #@94
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@96
    if-ne v8, v2, :cond_19f

    #@98
    const/4 v9, 0x1

    #@99
    :goto_99
    move-object/from16 v2, p0

    #@9b
    move/from16 v3, p1

    #@9d
    move-object/from16 v4, p3

    #@9f
    move/from16 v5, p5

    #@a1
    invoke-direct/range {v2 .. v9}, Landroid/widget/TextView;->makeSingleLayout(ILandroid/text/BoringLayout$Metrics;ILandroid/text/Layout$Alignment;ZLandroid/text/TextUtils$TruncateAt;Z)Landroid/text/Layout;

    #@a4
    move-result-object v2

    #@a5
    move-object/from16 v0, p0

    #@a7
    iput-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@a9
    .line 6163
    if-eqz v31, :cond_cb

    #@ab
    .line 6164
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@ad
    if-ne v8, v2, :cond_1a2

    #@af
    sget-object v15, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    #@b1
    .line 6166
    .local v15, oppositeEllipsize:Landroid/text/TextUtils$TruncateAt;
    :goto_b1
    move-object/from16 v0, p0

    #@b3
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@b5
    if-eq v8, v2, :cond_1a6

    #@b7
    const/16 v16, 0x1

    #@b9
    :goto_b9
    move-object/from16 v9, p0

    #@bb
    move/from16 v10, p1

    #@bd
    move-object/from16 v11, p3

    #@bf
    move/from16 v12, p5

    #@c1
    move-object v13, v6

    #@c2
    move v14, v7

    #@c3
    invoke-direct/range {v9 .. v16}, Landroid/widget/TextView;->makeSingleLayout(ILandroid/text/BoringLayout$Metrics;ILandroid/text/Layout$Alignment;ZLandroid/text/TextUtils$TruncateAt;Z)Landroid/text/Layout;

    #@c6
    move-result-object v2

    #@c7
    move-object/from16 v0, p0

    #@c9
    iput-object v2, v0, Landroid/widget/TextView;->mSavedMarqueeModeLayout:Landroid/text/Layout;

    #@cb
    .line 6170
    .end local v15           #oppositeEllipsize:Landroid/text/TextUtils$TruncateAt;
    :cond_cb
    move-object/from16 v0, p0

    #@cd
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@cf
    if-eqz v2, :cond_1aa

    #@d1
    const/4 v7, 0x1

    #@d2
    .line 6171
    :goto_d2
    const/4 v2, 0x0

    #@d3
    move-object/from16 v0, p0

    #@d5
    iput-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@d7
    .line 6173
    move-object/from16 v0, p0

    #@d9
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@db
    if-eqz v2, :cond_159

    #@dd
    .line 6174
    if-eqz v7, :cond_e1

    #@df
    move/from16 p2, p1

    #@e1
    .line 6176
    :cond_e1
    sget-object v2, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@e3
    move-object/from16 v0, p4

    #@e5
    if-ne v0, v2, :cond_103

    #@e7
    .line 6178
    move-object/from16 v0, p0

    #@e9
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@eb
    move-object/from16 v0, p0

    #@ed
    iget-object v3, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@ef
    move-object/from16 v0, p0

    #@f1
    iget-object v4, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@f3
    move-object/from16 v0, p0

    #@f5
    iget-object v5, v0, Landroid/widget/TextView;->mHintBoring:Landroid/text/BoringLayout$Metrics;

    #@f7
    invoke-static {v2, v3, v4, v5}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@fa
    move-result-object p4

    #@fb
    .line 6181
    if-eqz p4, :cond_103

    #@fd
    .line 6182
    move-object/from16 v0, p4

    #@ff
    move-object/from16 v1, p0

    #@101
    iput-object v0, v1, Landroid/widget/TextView;->mHintBoring:Landroid/text/BoringLayout$Metrics;

    #@103
    .line 6186
    :cond_103
    if-eqz p4, :cond_2ec

    #@105
    .line 6187
    move-object/from16 v0, p4

    #@107
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@109
    move/from16 v0, p2

    #@10b
    if-gt v2, v0, :cond_1db

    #@10d
    if-eqz v7, :cond_117

    #@10f
    move-object/from16 v0, p4

    #@111
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@113
    move/from16 v0, p5

    #@115
    if-gt v2, v0, :cond_1db

    #@117
    .line 6189
    :cond_117
    move-object/from16 v0, p0

    #@119
    iget-object v2, v0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@11b
    if-eqz v2, :cond_1ad

    #@11d
    .line 6190
    move-object/from16 v0, p0

    #@11f
    iget-object v0, v0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@121
    move-object/from16 v16, v0

    #@123
    move-object/from16 v0, p0

    #@125
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@127
    move-object/from16 v17, v0

    #@129
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@12d
    move-object/from16 v18, v0

    #@12f
    move-object/from16 v0, p0

    #@131
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@133
    move/from16 v21, v0

    #@135
    move-object/from16 v0, p0

    #@137
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@139
    move/from16 v22, v0

    #@13b
    move-object/from16 v0, p0

    #@13d
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@13f
    move/from16 v24, v0

    #@141
    move/from16 v19, p2

    #@143
    move-object/from16 v20, v6

    #@145
    move-object/from16 v23, p4

    #@147
    invoke-virtual/range {v16 .. v24}, Landroid/text/BoringLayout;->replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    #@14a
    move-result-object v2

    #@14b
    move-object/from16 v0, p0

    #@14d
    iput-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@14f
    .line 6200
    :goto_14f
    move-object/from16 v0, p0

    #@151
    iget-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@153
    check-cast v2, Landroid/text/BoringLayout;

    #@155
    move-object/from16 v0, p0

    #@157
    iput-object v2, v0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@159
    .line 6240
    :cond_159
    :goto_159
    if-eqz p6, :cond_15e

    #@15b
    .line 6241
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->registerForPreDraw()V

    #@15e
    .line 6244
    :cond_15e
    move-object/from16 v0, p0

    #@160
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@162
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@164
    if-ne v2, v3, :cond_186

    #@166
    .line 6245
    move/from16 v0, p5

    #@168
    int-to-float v2, v0

    #@169
    move-object/from16 v0, p0

    #@16b
    invoke-direct {v0, v2}, Landroid/widget/TextView;->compressText(F)Z

    #@16e
    move-result v2

    #@16f
    if-nez v2, :cond_186

    #@171
    .line 6246
    move-object/from16 v0, p0

    #@173
    iget-object v2, v0, Landroid/widget/TextView;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@175
    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@177
    move/from16 v30, v0

    #@179
    .line 6249
    .local v30, height:I
    const/4 v2, -0x2

    #@17a
    move/from16 v0, v30

    #@17c
    if-eq v0, v2, :cond_37b

    #@17e
    const/4 v2, -0x1

    #@17f
    move/from16 v0, v30

    #@181
    if-eq v0, v2, :cond_37b

    #@183
    .line 6250
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->startMarquee()V

    #@186
    .line 6259
    .end local v30           #height:I
    :cond_186
    :goto_186
    move-object/from16 v0, p0

    #@188
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@18a
    if-eqz v2, :cond_193

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@190
    invoke-virtual {v2}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@193
    .line 6260
    :cond_193
    return-void

    #@194
    .line 6130
    .end local v6           #alignment:Landroid/text/Layout$Alignment;
    .end local v7           #shouldEllipsize:Z
    .end local v8           #effectiveEllipsize:Landroid/text/TextUtils$TruncateAt;
    .end local v31           #switchEllipsize:Z
    :cond_194
    const/high16 v2, 0x1

    #@196
    goto/16 :goto_32

    #@198
    .line 6143
    .restart local v6       #alignment:Landroid/text/Layout$Alignment;
    :cond_198
    const/4 v7, 0x0

    #@199
    goto/16 :goto_51

    #@19b
    .line 6144
    .restart local v7       #shouldEllipsize:Z
    :cond_19b
    const/16 v31, 0x0

    #@19d
    goto/16 :goto_61

    #@19f
    .line 6161
    .restart local v8       #effectiveEllipsize:Landroid/text/TextUtils$TruncateAt;
    .restart local v31       #switchEllipsize:Z
    :cond_19f
    const/4 v9, 0x0

    #@1a0
    goto/16 :goto_99

    #@1a2
    .line 6164
    :cond_1a2
    sget-object v15, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@1a4
    goto/16 :goto_b1

    #@1a6
    .line 6166
    .restart local v15       #oppositeEllipsize:Landroid/text/TextUtils$TruncateAt;
    :cond_1a6
    const/16 v16, 0x0

    #@1a8
    goto/16 :goto_b9

    #@1aa
    .line 6170
    .end local v15           #oppositeEllipsize:Landroid/text/TextUtils$TruncateAt;
    :cond_1aa
    const/4 v7, 0x0

    #@1ab
    goto/16 :goto_d2

    #@1ad
    .line 6195
    :cond_1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@1b1
    move-object/from16 v16, v0

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@1b7
    move-object/from16 v17, v0

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@1bd
    move/from16 v20, v0

    #@1bf
    move-object/from16 v0, p0

    #@1c1
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@1c3
    move/from16 v21, v0

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@1c9
    move/from16 v23, v0

    #@1cb
    move/from16 v18, p2

    #@1cd
    move-object/from16 v19, v6

    #@1cf
    move-object/from16 v22, p4

    #@1d1
    invoke-static/range {v16 .. v23}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    #@1d4
    move-result-object v2

    #@1d5
    move-object/from16 v0, p0

    #@1d7
    iput-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@1d9
    goto/16 :goto_14f

    #@1db
    .line 6201
    :cond_1db
    if-eqz v7, :cond_25d

    #@1dd
    move-object/from16 v0, p4

    #@1df
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@1e1
    move/from16 v0, p2

    #@1e3
    if-gt v2, v0, :cond_25d

    #@1e5
    .line 6202
    move-object/from16 v0, p0

    #@1e7
    iget-object v2, v0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@1e9
    if-eqz v2, :cond_227

    #@1eb
    .line 6203
    move-object/from16 v0, p0

    #@1ed
    iget-object v0, v0, Landroid/widget/TextView;->mSavedHintLayout:Landroid/text/BoringLayout;

    #@1ef
    move-object/from16 v16, v0

    #@1f1
    move-object/from16 v0, p0

    #@1f3
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@1f5
    move-object/from16 v17, v0

    #@1f7
    move-object/from16 v0, p0

    #@1f9
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@1fb
    move-object/from16 v18, v0

    #@1fd
    move-object/from16 v0, p0

    #@1ff
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@201
    move/from16 v21, v0

    #@203
    move-object/from16 v0, p0

    #@205
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@207
    move/from16 v22, v0

    #@209
    move-object/from16 v0, p0

    #@20b
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@20d
    move/from16 v24, v0

    #@20f
    move-object/from16 v0, p0

    #@211
    iget-object v0, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@213
    move-object/from16 v25, v0

    #@215
    move/from16 v19, p2

    #@217
    move-object/from16 v20, v6

    #@219
    move-object/from16 v23, p4

    #@21b
    move/from16 v26, p5

    #@21d
    invoke-virtual/range {v16 .. v26}, Landroid/text/BoringLayout;->replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;

    #@220
    move-result-object v2

    #@221
    move-object/from16 v0, p0

    #@223
    iput-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@225
    goto/16 :goto_159

    #@227
    .line 6209
    :cond_227
    move-object/from16 v0, p0

    #@229
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@22b
    move-object/from16 v16, v0

    #@22d
    move-object/from16 v0, p0

    #@22f
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@231
    move-object/from16 v17, v0

    #@233
    move-object/from16 v0, p0

    #@235
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@237
    move/from16 v20, v0

    #@239
    move-object/from16 v0, p0

    #@23b
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@23d
    move/from16 v21, v0

    #@23f
    move-object/from16 v0, p0

    #@241
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@243
    move/from16 v23, v0

    #@245
    move-object/from16 v0, p0

    #@247
    iget-object v0, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@249
    move-object/from16 v24, v0

    #@24b
    move/from16 v18, p2

    #@24d
    move-object/from16 v19, v6

    #@24f
    move-object/from16 v22, p4

    #@251
    move/from16 v25, p5

    #@253
    invoke-static/range {v16 .. v25}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;

    #@256
    move-result-object v2

    #@257
    move-object/from16 v0, p0

    #@259
    iput-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@25b
    goto/16 :goto_159

    #@25d
    .line 6214
    :cond_25d
    if-eqz v7, :cond_2b7

    #@25f
    .line 6216
    new-instance v16, Landroid/text/StaticLayout;

    #@261
    move-object/from16 v0, p0

    #@263
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@265
    move-object/from16 v17, v0

    #@267
    const/16 v18, 0x0

    #@269
    move-object/from16 v0, p0

    #@26b
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@26d
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@270
    move-result v19

    #@271
    move-object/from16 v0, p0

    #@273
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@275
    move-object/from16 v20, v0

    #@277
    move-object/from16 v0, p0

    #@279
    iget-object v0, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@27b
    move-object/from16 v23, v0

    #@27d
    move-object/from16 v0, p0

    #@27f
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@281
    move/from16 v24, v0

    #@283
    move-object/from16 v0, p0

    #@285
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@287
    move/from16 v25, v0

    #@289
    move-object/from16 v0, p0

    #@28b
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@28d
    move/from16 v26, v0

    #@28f
    move-object/from16 v0, p0

    #@291
    iget-object v0, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@293
    move-object/from16 v27, v0

    #@295
    move-object/from16 v0, p0

    #@297
    iget v2, v0, Landroid/widget/TextView;->mMaxMode:I

    #@299
    const/4 v3, 0x1

    #@29a
    if-ne v2, v3, :cond_2b3

    #@29c
    move-object/from16 v0, p0

    #@29e
    iget v0, v0, Landroid/widget/TextView;->mMaximum:I

    #@2a0
    move/from16 v29, v0

    #@2a2
    :goto_2a2
    move/from16 v21, p2

    #@2a4
    move-object/from16 v22, v6

    #@2a6
    move/from16 v28, p5

    #@2a8
    invoke-direct/range {v16 .. v29}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    #@2ab
    move-object/from16 v0, v16

    #@2ad
    move-object/from16 v1, p0

    #@2af
    iput-object v0, v1, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@2b1
    goto/16 :goto_159

    #@2b3
    :cond_2b3
    const v29, 0x7fffffff

    #@2b6
    goto :goto_2a2

    #@2b7
    .line 6222
    :cond_2b7
    new-instance v16, Landroid/text/StaticLayout;

    #@2b9
    move-object/from16 v0, p0

    #@2bb
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@2bd
    move-object/from16 v17, v0

    #@2bf
    move-object/from16 v0, p0

    #@2c1
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2c3
    move-object/from16 v18, v0

    #@2c5
    move-object/from16 v0, p0

    #@2c7
    iget-object v0, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@2c9
    move-object/from16 v21, v0

    #@2cb
    move-object/from16 v0, p0

    #@2cd
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@2cf
    move/from16 v22, v0

    #@2d1
    move-object/from16 v0, p0

    #@2d3
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@2d5
    move/from16 v23, v0

    #@2d7
    move-object/from16 v0, p0

    #@2d9
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@2db
    move/from16 v24, v0

    #@2dd
    move/from16 v19, p2

    #@2df
    move-object/from16 v20, v6

    #@2e1
    invoke-direct/range {v16 .. v24}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V

    #@2e4
    move-object/from16 v0, v16

    #@2e6
    move-object/from16 v1, p0

    #@2e8
    iput-object v0, v1, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@2ea
    goto/16 :goto_159

    #@2ec
    .line 6226
    :cond_2ec
    if-eqz v7, :cond_346

    #@2ee
    .line 6227
    new-instance v16, Landroid/text/StaticLayout;

    #@2f0
    move-object/from16 v0, p0

    #@2f2
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@2f4
    move-object/from16 v17, v0

    #@2f6
    const/16 v18, 0x0

    #@2f8
    move-object/from16 v0, p0

    #@2fa
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@2fc
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@2ff
    move-result v19

    #@300
    move-object/from16 v0, p0

    #@302
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@304
    move-object/from16 v20, v0

    #@306
    move-object/from16 v0, p0

    #@308
    iget-object v0, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@30a
    move-object/from16 v23, v0

    #@30c
    move-object/from16 v0, p0

    #@30e
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@310
    move/from16 v24, v0

    #@312
    move-object/from16 v0, p0

    #@314
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@316
    move/from16 v25, v0

    #@318
    move-object/from16 v0, p0

    #@31a
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@31c
    move/from16 v26, v0

    #@31e
    move-object/from16 v0, p0

    #@320
    iget-object v0, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@322
    move-object/from16 v27, v0

    #@324
    move-object/from16 v0, p0

    #@326
    iget v2, v0, Landroid/widget/TextView;->mMaxMode:I

    #@328
    const/4 v3, 0x1

    #@329
    if-ne v2, v3, :cond_342

    #@32b
    move-object/from16 v0, p0

    #@32d
    iget v0, v0, Landroid/widget/TextView;->mMaximum:I

    #@32f
    move/from16 v29, v0

    #@331
    :goto_331
    move/from16 v21, p2

    #@333
    move-object/from16 v22, v6

    #@335
    move/from16 v28, p5

    #@337
    invoke-direct/range {v16 .. v29}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    #@33a
    move-object/from16 v0, v16

    #@33c
    move-object/from16 v1, p0

    #@33e
    iput-object v0, v1, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@340
    goto/16 :goto_159

    #@342
    :cond_342
    const v29, 0x7fffffff

    #@345
    goto :goto_331

    #@346
    .line 6233
    :cond_346
    new-instance v16, Landroid/text/StaticLayout;

    #@348
    move-object/from16 v0, p0

    #@34a
    iget-object v0, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@34c
    move-object/from16 v17, v0

    #@34e
    move-object/from16 v0, p0

    #@350
    iget-object v0, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@352
    move-object/from16 v18, v0

    #@354
    move-object/from16 v0, p0

    #@356
    iget-object v0, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@358
    move-object/from16 v21, v0

    #@35a
    move-object/from16 v0, p0

    #@35c
    iget v0, v0, Landroid/widget/TextView;->mSpacingMult:F

    #@35e
    move/from16 v22, v0

    #@360
    move-object/from16 v0, p0

    #@362
    iget v0, v0, Landroid/widget/TextView;->mSpacingAdd:F

    #@364
    move/from16 v23, v0

    #@366
    move-object/from16 v0, p0

    #@368
    iget-boolean v0, v0, Landroid/widget/TextView;->mIncludePad:Z

    #@36a
    move/from16 v24, v0

    #@36c
    move/from16 v19, p2

    #@36e
    move-object/from16 v20, v6

    #@370
    invoke-direct/range {v16 .. v24}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V

    #@373
    move-object/from16 v0, v16

    #@375
    move-object/from16 v1, p0

    #@377
    iput-object v0, v1, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@379
    goto/16 :goto_159

    #@37b
    .line 6253
    .restart local v30       #height:I
    :cond_37b
    const/4 v2, 0x1

    #@37c
    move-object/from16 v0, p0

    #@37e
    iput-boolean v2, v0, Landroid/widget/TextView;->mRestartMarquee:Z

    #@380
    goto/16 :goto_186
.end method

.method public moveCursorToVisibleOffset()Z
    .registers 21

    #@0
    .prologue
    .line 7032
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@4
    move-object/from16 v17, v0

    #@6
    move-object/from16 v0, v17

    #@8
    instance-of v0, v0, Landroid/text/Spannable;

    #@a
    move/from16 v17, v0

    #@c
    if-nez v17, :cond_11

    #@e
    .line 7033
    const/16 v17, 0x0

    #@10
    .line 7082
    :goto_10
    return v17

    #@11
    .line 7035
    :cond_11
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@14
    move-result v12

    #@15
    .line 7036
    .local v12, start:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@18
    move-result v3

    #@19
    .line 7037
    .local v3, end:I
    if-eq v12, v3, :cond_1e

    #@1b
    .line 7038
    const/16 v17, 0x0

    #@1d
    goto :goto_10

    #@1e
    .line 7043
    :cond_1e
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@22
    move-object/from16 v17, v0

    #@24
    move-object/from16 v0, v17

    #@26
    invoke-virtual {v0, v12}, Landroid/text/Layout;->getLineForOffset(I)I

    #@29
    move-result v8

    #@2a
    .line 7045
    .local v8, line:I
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@2e
    move-object/from16 v17, v0

    #@30
    move-object/from16 v0, v17

    #@32
    invoke-virtual {v0, v8}, Landroid/text/Layout;->getLineTop(I)I

    #@35
    move-result v13

    #@36
    .line 7046
    .local v13, top:I
    move-object/from16 v0, p0

    #@38
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@3a
    move-object/from16 v17, v0

    #@3c
    add-int/lit8 v18, v8, 0x1

    #@3e
    invoke-virtual/range {v17 .. v18}, Landroid/text/Layout;->getLineTop(I)I

    #@41
    move-result v2

    #@42
    .line 7047
    .local v2, bottom:I
    move-object/from16 v0, p0

    #@44
    iget v0, v0, Landroid/widget/TextView;->mBottom:I

    #@46
    move/from16 v17, v0

    #@48
    move-object/from16 v0, p0

    #@4a
    iget v0, v0, Landroid/widget/TextView;->mTop:I

    #@4c
    move/from16 v18, v0

    #@4e
    sub-int v17, v17, v18

    #@50
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@53
    move-result v18

    #@54
    sub-int v17, v17, v18

    #@56
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@59
    move-result v18

    #@5a
    sub-int v16, v17, v18

    #@5c
    .line 7048
    .local v16, vspace:I
    sub-int v17, v2, v13

    #@5e
    div-int/lit8 v15, v17, 0x2

    #@60
    .line 7049
    .local v15, vslack:I
    div-int/lit8 v17, v16, 0x4

    #@62
    move/from16 v0, v17

    #@64
    if-le v15, v0, :cond_68

    #@66
    .line 7050
    div-int/lit8 v15, v16, 0x4

    #@68
    .line 7051
    :cond_68
    move-object/from16 v0, p0

    #@6a
    iget v14, v0, Landroid/widget/TextView;->mScrollY:I

    #@6c
    .line 7053
    .local v14, vs:I
    add-int v17, v14, v15

    #@6e
    move/from16 v0, v17

    #@70
    if-ge v13, v0, :cond_e3

    #@72
    .line 7054
    move-object/from16 v0, p0

    #@74
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@76
    move-object/from16 v17, v0

    #@78
    add-int v18, v14, v15

    #@7a
    sub-int v19, v2, v13

    #@7c
    add-int v18, v18, v19

    #@7e
    invoke-virtual/range {v17 .. v18}, Landroid/text/Layout;->getLineForVertical(I)I

    #@81
    move-result v8

    #@82
    .line 7061
    :cond_82
    :goto_82
    move-object/from16 v0, p0

    #@84
    iget v0, v0, Landroid/widget/TextView;->mRight:I

    #@86
    move/from16 v17, v0

    #@88
    move-object/from16 v0, p0

    #@8a
    iget v0, v0, Landroid/widget/TextView;->mLeft:I

    #@8c
    move/from16 v18, v0

    #@8e
    sub-int v17, v17, v18

    #@90
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@93
    move-result v18

    #@94
    sub-int v17, v17, v18

    #@96
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@99
    move-result v18

    #@9a
    sub-int v6, v17, v18

    #@9c
    .line 7062
    .local v6, hspace:I
    move-object/from16 v0, p0

    #@9e
    iget v5, v0, Landroid/widget/TextView;->mScrollX:I

    #@a0
    .line 7063
    .local v5, hs:I
    move-object/from16 v0, p0

    #@a2
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@a4
    move-object/from16 v17, v0

    #@a6
    int-to-float v0, v5

    #@a7
    move/from16 v18, v0

    #@a9
    move-object/from16 v0, v17

    #@ab
    move/from16 v1, v18

    #@ad
    invoke-virtual {v0, v8, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@b0
    move-result v7

    #@b1
    .line 7064
    .local v7, leftChar:I
    move-object/from16 v0, p0

    #@b3
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@b5
    move-object/from16 v17, v0

    #@b7
    add-int v18, v6, v5

    #@b9
    move/from16 v0, v18

    #@bb
    int-to-float v0, v0

    #@bc
    move/from16 v18, v0

    #@be
    move-object/from16 v0, v17

    #@c0
    move/from16 v1, v18

    #@c2
    invoke-virtual {v0, v8, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@c5
    move-result v11

    #@c6
    .line 7067
    .local v11, rightChar:I
    if-ge v7, v11, :cond_fe

    #@c8
    move v9, v7

    #@c9
    .line 7068
    .local v9, lowChar:I
    :goto_c9
    if-le v7, v11, :cond_100

    #@cb
    move v4, v7

    #@cc
    .line 7070
    .local v4, highChar:I
    :goto_cc
    move v10, v12

    #@cd
    .line 7071
    .local v10, newStart:I
    if-ge v10, v9, :cond_102

    #@cf
    .line 7072
    move v10, v9

    #@d0
    .line 7077
    :cond_d0
    :goto_d0
    if-eq v10, v12, :cond_106

    #@d2
    .line 7078
    move-object/from16 v0, p0

    #@d4
    iget-object v0, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@d6
    move-object/from16 v17, v0

    #@d8
    check-cast v17, Landroid/text/Spannable;

    #@da
    move-object/from16 v0, v17

    #@dc
    invoke-static {v0, v10}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@df
    .line 7079
    const/16 v17, 0x1

    #@e1
    goto/16 :goto_10

    #@e3
    .line 7055
    .end local v4           #highChar:I
    .end local v5           #hs:I
    .end local v6           #hspace:I
    .end local v7           #leftChar:I
    .end local v9           #lowChar:I
    .end local v10           #newStart:I
    .end local v11           #rightChar:I
    :cond_e3
    add-int v17, v16, v14

    #@e5
    sub-int v17, v17, v15

    #@e7
    move/from16 v0, v17

    #@e9
    if-le v2, v0, :cond_82

    #@eb
    .line 7056
    move-object/from16 v0, p0

    #@ed
    iget-object v0, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@ef
    move-object/from16 v17, v0

    #@f1
    add-int v18, v16, v14

    #@f3
    sub-int v18, v18, v15

    #@f5
    sub-int v19, v2, v13

    #@f7
    sub-int v18, v18, v19

    #@f9
    invoke-virtual/range {v17 .. v18}, Landroid/text/Layout;->getLineForVertical(I)I

    #@fc
    move-result v8

    #@fd
    goto :goto_82

    #@fe
    .restart local v5       #hs:I
    .restart local v6       #hspace:I
    .restart local v7       #leftChar:I
    .restart local v11       #rightChar:I
    :cond_fe
    move v9, v11

    #@ff
    .line 7067
    goto :goto_c9

    #@100
    .restart local v9       #lowChar:I
    :cond_100
    move v4, v11

    #@101
    .line 7068
    goto :goto_cc

    #@102
    .line 7073
    .restart local v4       #highChar:I
    .restart local v10       #newStart:I
    :cond_102
    if-le v10, v4, :cond_d0

    #@104
    .line 7074
    move v10, v4

    #@105
    goto :goto_d0

    #@106
    .line 7082
    :cond_106
    const/16 v17, 0x0

    #@108
    goto/16 :goto_10
.end method

.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 4835
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@3
    .line 4837
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/TextView;->mTemporaryDetach:Z

    #@6
    .line 4839
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    if-eqz v0, :cond_f

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    invoke-virtual {v0}, Landroid/widget/Editor;->onAttachedToWindow()V

    #@f
    .line 4840
    :cond_f
    return-void
.end method

.method public onBeginBatchEdit()V
    .registers 1

    #@0
    .prologue
    .line 5985
    return-void
.end method

.method public onCheckIsTextEditor()Z
    .registers 2

    #@0
    .prologue
    .line 5801
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    iget v0, v0, Landroid/widget/Editor;->mInputType:I

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 5957
    return-void
.end method

.method public onCommitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 5968
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/Editor;->onCommitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V

    #@9
    .line 5969
    :cond_9
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "newConfig"

    #@0
    .prologue
    .line 8759
    invoke-direct {p0}, Landroid/widget/TextView;->updateActionModeTitle()V

    #@3
    .line 8762
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@5
    if-eqz v0, :cond_1b

    #@7
    .line 8763
    iget-object v0, p0, Landroid/widget/TextView;->mCurConfig:Landroid/content/res/Configuration;

    #@9
    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@c
    move-result v0

    #@d
    const/high16 v1, 0x2000

    #@f
    and-int/2addr v0, v1

    #@10
    if-eqz v0, :cond_1b

    #@12
    .line 8764
    iget-object v0, p0, Landroid/widget/TextView;->mFontFamily:Ljava/lang/String;

    #@14
    iget v1, p0, Landroid/widget/TextView;->mTypefaceIndex:I

    #@16
    iget v2, p0, Landroid/widget/TextView;->mStyleIndex:I

    #@18
    invoke-direct {p0, v0, v1, v2}, Landroid/widget/TextView;->setForceTypefaceFromAttrs(Ljava/lang/String;II)V

    #@1b
    .line 8768
    :cond_1b
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 9
    .parameter "extraSpace"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 5036
    iget-boolean v4, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@3
    if-eqz v4, :cond_2b

    #@5
    .line 5037
    invoke-super {p0, p1}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@8
    move-result-object v0

    #@9
    .line 5043
    .local v0, drawableState:[I
    :goto_9
    invoke-virtual {p0}, Landroid/widget/TextView;->isTextSelectable()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_3a

    #@f
    .line 5048
    array-length v2, v0

    #@10
    .line 5049
    .local v2, length:I
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v2, :cond_3a

    #@13
    .line 5050
    aget v4, v0, v1

    #@15
    const v5, 0x10100a7

    #@18
    if-ne v4, v5, :cond_37

    #@1a
    .line 5051
    add-int/lit8 v4, v2, -0x1

    #@1c
    new-array v3, v4, [I

    #@1e
    .line 5052
    .local v3, nonPressedState:[I
    invoke-static {v0, v6, v3, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@21
    .line 5053
    add-int/lit8 v4, v1, 0x1

    #@23
    sub-int v5, v2, v1

    #@25
    add-int/lit8 v5, v5, -0x1

    #@27
    invoke-static {v0, v4, v3, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2a
    .line 5059
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #nonPressedState:[I
    :goto_2a
    return-object v3

    #@2b
    .line 5039
    .end local v0           #drawableState:[I
    :cond_2b
    add-int/lit8 v4, p1, 0x1

    #@2d
    invoke-super {p0, v4}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@30
    move-result-object v0

    #@31
    .line 5040
    .restart local v0       #drawableState:[I
    sget-object v4, Landroid/widget/TextView;->MULTILINE_STATE_SET:[I

    #@33
    invoke-static {v0, v4}, Landroid/widget/TextView;->mergeDrawableStates([I[I)[I

    #@36
    goto :goto_9

    #@37
    .line 5049
    .restart local v1       #i:I
    .restart local v2       #length:I
    :cond_37
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_11

    #@3a
    .end local v1           #i:I
    .end local v2           #length:I
    :cond_3a
    move-object v3, v0

    #@3b
    .line 5059
    goto :goto_2a
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 7
    .parameter "outAttrs"

    #@0
    .prologue
    const/high16 v4, 0x4000

    #@2
    const/high16 v3, 0x800

    #@4
    .line 5806
    invoke-virtual {p0}, Landroid/widget/TextView;->onCheckIsTextEditor()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_c9

    #@a
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_c9

    #@10
    .line 5807
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@12
    invoke-virtual {v1}, Landroid/widget/Editor;->createInputMethodStateIfNeeded()V

    #@15
    .line 5808
    invoke-virtual {p0}, Landroid/widget/TextView;->getInputType()I

    #@18
    move-result v1

    #@19
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@1b
    .line 5809
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1d
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@1f
    if-eqz v1, :cond_be

    #@21
    .line 5810
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@23
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@25
    iget v1, v1, Landroid/widget/Editor$InputContentType;->imeOptions:I

    #@27
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@29
    .line 5811
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2b
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@2d
    iget-object v1, v1, Landroid/widget/Editor$InputContentType;->privateImeOptions:Ljava/lang/String;

    #@2f
    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    #@31
    .line 5812
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@33
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@35
    iget-object v1, v1, Landroid/widget/Editor$InputContentType;->imeActionLabel:Ljava/lang/CharSequence;

    #@37
    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    #@39
    .line 5813
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3b
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@3d
    iget v1, v1, Landroid/widget/Editor$InputContentType;->imeActionId:I

    #@3f
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->actionId:I

    #@41
    .line 5814
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@43
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@45
    iget-object v1, v1, Landroid/widget/Editor$InputContentType;->extras:Landroid/os/Bundle;

    #@47
    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->extras:Landroid/os/Bundle;

    #@49
    .line 5818
    :goto_49
    const/16 v1, 0x82

    #@4b
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->focusSearch(I)Landroid/view/View;

    #@4e
    move-result-object v1

    #@4f
    if-eqz v1, :cond_56

    #@51
    .line 5819
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@53
    or-int/2addr v1, v3

    #@54
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@56
    .line 5821
    :cond_56
    const/16 v1, 0x21

    #@58
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->focusSearch(I)Landroid/view/View;

    #@5b
    move-result-object v1

    #@5c
    if-eqz v1, :cond_65

    #@5e
    .line 5822
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@60
    const/high16 v2, 0x400

    #@62
    or-int/2addr v1, v2

    #@63
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@65
    .line 5824
    :cond_65
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@67
    and-int/lit16 v1, v1, 0xff

    #@69
    if-nez v1, :cond_81

    #@6b
    .line 5826
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@6d
    and-int/2addr v1, v3

    #@6e
    if-eqz v1, :cond_c2

    #@70
    .line 5829
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@72
    or-int/lit8 v1, v1, 0x5

    #@74
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@76
    .line 5835
    :goto_76
    invoke-direct {p0}, Landroid/widget/TextView;->shouldAdvanceFocusOnEnter()Z

    #@79
    move-result v1

    #@7a
    if-nez v1, :cond_81

    #@7c
    .line 5836
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@7e
    or-int/2addr v1, v4

    #@7f
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@81
    .line 5839
    :cond_81
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@83
    invoke-static {v1}, Landroid/widget/TextView;->isMultilineInputType(I)Z

    #@86
    move-result v1

    #@87
    if-eqz v1, :cond_8e

    #@89
    .line 5841
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@8b
    or-int/2addr v1, v4

    #@8c
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@8e
    .line 5843
    :cond_8e
    iget-object v1, p0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@90
    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->hintText:Ljava/lang/CharSequence;

    #@92
    .line 5845
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@94
    if-eqz v1, :cond_9c

    #@96
    .line 5846
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@98
    iget v1, v1, Landroid/widget/Editor;->mClipDataType:I

    #@9a
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->clipDataType:I

    #@9c
    .line 5849
    :cond_9c
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@9e
    instance-of v1, v1, Landroid/text/Editable;

    #@a0
    if-eqz v1, :cond_c9

    #@a2
    .line 5850
    new-instance v0, Lcom/android/internal/widget/EditableInputConnection;

    #@a4
    invoke-direct {v0, p0}, Lcom/android/internal/widget/EditableInputConnection;-><init>(Landroid/widget/TextView;)V

    #@a7
    .line 5851
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@aa
    move-result v1

    #@ab
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    #@ad
    .line 5852
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@b0
    move-result v1

    #@b1
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    #@b3
    .line 5853
    invoke-virtual {p0}, Landroid/widget/TextView;->getInputType()I

    #@b6
    move-result v1

    #@b7
    invoke-interface {v0, v1}, Landroid/view/inputmethod/InputConnection;->getCursorCapsMode(I)I

    #@ba
    move-result v1

    #@bb
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialCapsMode:I

    #@bd
    .line 5857
    .end local v0           #ic:Landroid/view/inputmethod/InputConnection;
    :goto_bd
    return-object v0

    #@be
    .line 5816
    :cond_be
    const/4 v1, 0x0

    #@bf
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@c1
    goto :goto_49

    #@c2
    .line 5833
    :cond_c2
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@c4
    or-int/lit8 v1, v1, 0x6

    #@c6
    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@c8
    goto :goto_76

    #@c9
    .line 5857
    :cond_c9
    const/4 v0, 0x0

    #@ca
    goto :goto_bd
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 4844
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@3
    .line 4846
    iget-boolean v0, p0, Landroid/widget/TextView;->mPreDrawRegistered:Z

    #@5
    if-eqz v0, :cond_11

    #@7
    .line 4847
    invoke-virtual {p0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@e
    .line 4848
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/widget/TextView;->mPreDrawRegistered:Z

    #@11
    .line 4851
    :cond_11
    invoke-virtual {p0}, Landroid/widget/TextView;->resetResolvedDrawables()V

    #@14
    .line 4853
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@16
    if-eqz v0, :cond_1d

    #@18
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1a
    invoke-virtual {v0}, Landroid/widget/Editor;->onDetachedFromWindow()V

    #@1d
    .line 4854
    :cond_1d
    return-void
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 8830
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    #@4
    move-result v1

    #@5
    packed-switch v1, :pswitch_data_5e

    #@8
    .line 8857
    :cond_8
    :goto_8
    :pswitch_8
    return v2

    #@9
    .line 8832
    :pswitch_9
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@b
    if-eqz v1, :cond_18

    #@d
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@f
    invoke-virtual {v1}, Landroid/widget/Editor;->hasInsertionController()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_18

    #@15
    move v1, v2

    #@16
    :goto_16
    move v2, v1

    #@17
    goto :goto_8

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_16

    #@1a
    .line 8835
    :pswitch_1a
    invoke-virtual {p0}, Landroid/widget/TextView;->requestFocus()Z

    #@1d
    goto :goto_8

    #@1e
    .line 8839
    :pswitch_1e
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    #@21
    move-result v1

    #@22
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    #@25
    move-result v3

    #@26
    invoke-virtual {p0, v1, v3}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@29
    move-result v0

    #@2a
    .line 8842
    .local v0, offset:I
    if-ltz v0, :cond_34

    #@2c
    .line 8843
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2e
    check-cast v1, Landroid/text/Spannable;

    #@30
    invoke-static {v1, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@33
    goto :goto_8

    #@34
    .line 8845
    :cond_34
    const-string v1, "TextView"

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "Skipping selection due to negative offset ("

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    const-string v4, ")"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_8

    #@53
    .line 8851
    .end local v0           #offset:I
    :pswitch_53
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@55
    if-eqz v1, :cond_8

    #@57
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@59
    invoke-virtual {v1, p1}, Landroid/widget/Editor;->onDrop(Landroid/view/DragEvent;)V

    #@5c
    goto :goto_8

    #@5d
    .line 8830
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_9
        :pswitch_1e
        :pswitch_53
        :pswitch_8
        :pswitch_1a
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 42
    .parameter "canvas"

    #@0
    .prologue
    .line 5120
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->restartMarqueeIfNeeded()V

    #@3
    .line 5123
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@6
    .line 5125
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@9
    move-result v16

    #@a
    .line 5126
    .local v16, compoundPaddingLeft:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@d
    move-result v18

    #@e
    .line 5127
    .local v18, compoundPaddingTop:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@11
    move-result v17

    #@12
    .line 5128
    .local v17, compoundPaddingRight:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@15
    move-result v15

    #@16
    .line 5129
    .local v15, compoundPaddingBottom:I
    move-object/from16 v0, p0

    #@18
    iget v0, v0, Landroid/widget/TextView;->mScrollX:I

    #@1a
    move/from16 v33, v0

    #@1c
    .line 5130
    .local v33, scrollX:I
    move-object/from16 v0, p0

    #@1e
    iget v0, v0, Landroid/widget/TextView;->mScrollY:I

    #@20
    move/from16 v34, v0

    #@22
    .line 5131
    .local v34, scrollY:I
    move-object/from16 v0, p0

    #@24
    iget v0, v0, Landroid/widget/TextView;->mRight:I

    #@26
    move/from16 v31, v0

    #@28
    .line 5132
    .local v31, right:I
    move-object/from16 v0, p0

    #@2a
    iget v0, v0, Landroid/widget/TextView;->mLeft:I

    #@2c
    move/from16 v26, v0

    #@2e
    .line 5133
    .local v26, left:I
    move-object/from16 v0, p0

    #@30
    iget v9, v0, Landroid/widget/TextView;->mBottom:I

    #@32
    .line 5134
    .local v9, bottom:I
    move-object/from16 v0, p0

    #@34
    iget v0, v0, Landroid/widget/TextView;->mTop:I

    #@36
    move/from16 v35, v0

    #@38
    .line 5135
    .local v35, top:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isLayoutRtl()Z

    #@3b
    move-result v24

    #@3c
    .line 5136
    .local v24, isLayoutRtl:Z
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getHorizontalOffsetForDrawables()I

    #@3f
    move-result v29

    #@40
    .line 5137
    .local v29, offset:I
    if-eqz v24, :cond_2ee

    #@42
    const/16 v27, 0x0

    #@44
    .line 5138
    .local v27, leftOffset:I
    :goto_44
    if-eqz v24, :cond_2f2

    #@46
    move/from16 v32, v29

    #@48
    .line 5140
    .local v32, rightOffset:I
    :goto_48
    move-object/from16 v0, p0

    #@4a
    iget-object v0, v0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@4c
    move-object/from16 v19, v0

    #@4e
    .line 5141
    .local v19, dr:Landroid/widget/TextView$Drawables;
    if-eqz v19, :cond_123

    #@50
    .line 5147
    sub-int v2, v9, v35

    #@52
    sub-int/2addr v2, v15

    #@53
    sub-int v38, v2, v18

    #@55
    .line 5148
    .local v38, vspace:I
    sub-int v2, v31, v26

    #@57
    sub-int v2, v2, v17

    #@59
    sub-int v23, v2, v16

    #@5b
    .line 5152
    .local v23, hspace:I
    move-object/from16 v0, v19

    #@5d
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@5f
    if-eqz v2, :cond_8a

    #@61
    .line 5153
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@64
    .line 5154
    move-object/from16 v0, p0

    #@66
    iget v2, v0, Landroid/widget/TextView;->mPaddingLeft:I

    #@68
    add-int v2, v2, v33

    #@6a
    add-int v2, v2, v27

    #@6c
    int-to-float v2, v2

    #@6d
    add-int v3, v34, v18

    #@6f
    move-object/from16 v0, v19

    #@71
    iget v6, v0, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@73
    sub-int v6, v38, v6

    #@75
    div-int/lit8 v6, v6, 0x2

    #@77
    add-int/2addr v3, v6

    #@78
    int-to-float v3, v3

    #@79
    move-object/from16 v0, p1

    #@7b
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@7e
    .line 5157
    move-object/from16 v0, v19

    #@80
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@82
    move-object/from16 v0, p1

    #@84
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@87
    .line 5158
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@8a
    .line 5163
    :cond_8a
    move-object/from16 v0, v19

    #@8c
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@8e
    if-eqz v2, :cond_c1

    #@90
    .line 5164
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@93
    .line 5165
    add-int v2, v33, v31

    #@95
    sub-int v2, v2, v26

    #@97
    move-object/from16 v0, p0

    #@99
    iget v3, v0, Landroid/widget/TextView;->mPaddingRight:I

    #@9b
    sub-int/2addr v2, v3

    #@9c
    move-object/from16 v0, v19

    #@9e
    iget v3, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@a0
    sub-int/2addr v2, v3

    #@a1
    sub-int v2, v2, v32

    #@a3
    int-to-float v2, v2

    #@a4
    add-int v3, v34, v18

    #@a6
    move-object/from16 v0, v19

    #@a8
    iget v6, v0, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@aa
    sub-int v6, v38, v6

    #@ac
    div-int/lit8 v6, v6, 0x2

    #@ae
    add-int/2addr v3, v6

    #@af
    int-to-float v3, v3

    #@b0
    move-object/from16 v0, p1

    #@b2
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@b5
    .line 5168
    move-object/from16 v0, v19

    #@b7
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@b9
    move-object/from16 v0, p1

    #@bb
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@be
    .line 5169
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@c1
    .line 5174
    :cond_c1
    move-object/from16 v0, v19

    #@c3
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@c5
    if-eqz v2, :cond_ee

    #@c7
    .line 5175
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@ca
    .line 5176
    add-int v2, v33, v16

    #@cc
    move-object/from16 v0, v19

    #@ce
    iget v3, v0, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@d0
    sub-int v3, v23, v3

    #@d2
    div-int/lit8 v3, v3, 0x2

    #@d4
    add-int/2addr v2, v3

    #@d5
    int-to-float v2, v2

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget v3, v0, Landroid/widget/TextView;->mPaddingTop:I

    #@da
    add-int v3, v3, v34

    #@dc
    int-to-float v3, v3

    #@dd
    move-object/from16 v0, p1

    #@df
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@e2
    .line 5178
    move-object/from16 v0, v19

    #@e4
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@e6
    move-object/from16 v0, p1

    #@e8
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@eb
    .line 5179
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@ee
    .line 5184
    :cond_ee
    move-object/from16 v0, v19

    #@f0
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@f2
    if-eqz v2, :cond_123

    #@f4
    .line 5185
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@f7
    .line 5186
    add-int v2, v33, v16

    #@f9
    move-object/from16 v0, v19

    #@fb
    iget v3, v0, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@fd
    sub-int v3, v23, v3

    #@ff
    div-int/lit8 v3, v3, 0x2

    #@101
    add-int/2addr v2, v3

    #@102
    int-to-float v2, v2

    #@103
    add-int v3, v34, v9

    #@105
    sub-int v3, v3, v35

    #@107
    move-object/from16 v0, p0

    #@109
    iget v6, v0, Landroid/widget/TextView;->mPaddingBottom:I

    #@10b
    sub-int/2addr v3, v6

    #@10c
    move-object/from16 v0, v19

    #@10e
    iget v6, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@110
    sub-int/2addr v3, v6

    #@111
    int-to-float v3, v3

    #@112
    move-object/from16 v0, p1

    #@114
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@117
    .line 5189
    move-object/from16 v0, v19

    #@119
    iget-object v2, v0, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@11b
    move-object/from16 v0, p1

    #@11d
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@120
    .line 5190
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@123
    .line 5194
    .end local v23           #hspace:I
    .end local v38           #vspace:I
    :cond_123
    move-object/from16 v0, p0

    #@125
    iget v14, v0, Landroid/widget/TextView;->mCurTextColor:I

    #@127
    .line 5196
    .local v14, color:I
    move-object/from16 v0, p0

    #@129
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@12b
    if-nez v2, :cond_130

    #@12d
    .line 5197
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->assumeLayout()V

    #@130
    .line 5200
    :cond_130
    move-object/from16 v0, p0

    #@132
    iget-object v4, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@134
    .line 5202
    .local v4, layout:Landroid/text/Layout;
    move-object/from16 v0, p0

    #@136
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@138
    if-eqz v2, :cond_152

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget-object v2, v0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@13e
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@141
    move-result v2

    #@142
    if-nez v2, :cond_152

    #@144
    .line 5203
    move-object/from16 v0, p0

    #@146
    iget-object v2, v0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@148
    if-eqz v2, :cond_14e

    #@14a
    .line 5204
    move-object/from16 v0, p0

    #@14c
    iget v14, v0, Landroid/widget/TextView;->mCurHintTextColor:I

    #@14e
    .line 5207
    :cond_14e
    move-object/from16 v0, p0

    #@150
    iget-object v4, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@152
    .line 5210
    :cond_152
    move-object/from16 v0, p0

    #@154
    iget-object v2, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@156
    invoke-virtual {v2, v14}, Landroid/text/TextPaint;->setColor(I)V

    #@159
    .line 5211
    move-object/from16 v0, p0

    #@15b
    iget-object v2, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@15d
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@160
    move-result-object v3

    #@161
    iput-object v3, v2, Landroid/text/TextPaint;->drawableState:[I

    #@163
    .line 5213
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@166
    .line 5218
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@169
    move-result v22

    #@16a
    .line 5219
    .local v22, extendedPaddingTop:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getExtendedPaddingBottom()I

    #@16d
    move-result v21

    #@16e
    .line 5221
    .local v21, extendedPaddingBottom:I
    move-object/from16 v0, p0

    #@170
    iget v2, v0, Landroid/widget/TextView;->mBottom:I

    #@172
    move-object/from16 v0, p0

    #@174
    iget v3, v0, Landroid/widget/TextView;->mTop:I

    #@176
    sub-int/2addr v2, v3

    #@177
    sub-int/2addr v2, v15

    #@178
    sub-int v38, v2, v18

    #@17a
    .line 5222
    .restart local v38       #vspace:I
    move-object/from16 v0, p0

    #@17c
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@17e
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    #@181
    move-result v2

    #@182
    sub-int v28, v2, v38

    #@184
    .line 5224
    .local v28, maxScrollY:I
    add-int v2, v16, v33

    #@186
    int-to-float v11, v2

    #@187
    .line 5225
    .local v11, clipLeft:F
    if-nez v34, :cond_2f6

    #@189
    const/4 v13, 0x0

    #@18a
    .line 5226
    .local v13, clipTop:F
    :goto_18a
    sub-int v2, v31, v26

    #@18c
    sub-int v2, v2, v17

    #@18e
    add-int v2, v2, v33

    #@190
    int-to-float v12, v2

    #@191
    .line 5227
    .local v12, clipRight:F
    sub-int v2, v9, v35

    #@193
    add-int v2, v2, v34

    #@195
    move/from16 v0, v34

    #@197
    move/from16 v1, v28

    #@199
    if-ne v0, v1, :cond_19d

    #@19b
    const/16 v21, 0x0

    #@19d
    .end local v21           #extendedPaddingBottom:I
    :cond_19d
    sub-int v2, v2, v21

    #@19f
    int-to-float v10, v2

    #@1a0
    .line 5230
    .local v10, clipBottom:F
    move-object/from16 v0, p0

    #@1a2
    iget v2, v0, Landroid/widget/TextView;->mShadowRadius:F

    #@1a4
    const/4 v3, 0x0

    #@1a5
    cmpl-float v2, v2, v3

    #@1a7
    if-eqz v2, :cond_1e5

    #@1a9
    .line 5231
    const/4 v2, 0x0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    iget v3, v0, Landroid/widget/TextView;->mShadowDx:F

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iget v6, v0, Landroid/widget/TextView;->mShadowRadius:F

    #@1b2
    sub-float/2addr v3, v6

    #@1b3
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    #@1b6
    move-result v2

    #@1b7
    add-float/2addr v11, v2

    #@1b8
    .line 5232
    const/4 v2, 0x0

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget v3, v0, Landroid/widget/TextView;->mShadowDx:F

    #@1bd
    move-object/from16 v0, p0

    #@1bf
    iget v6, v0, Landroid/widget/TextView;->mShadowRadius:F

    #@1c1
    add-float/2addr v3, v6

    #@1c2
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    #@1c5
    move-result v2

    #@1c6
    add-float/2addr v12, v2

    #@1c7
    .line 5234
    const/4 v2, 0x0

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iget v3, v0, Landroid/widget/TextView;->mShadowDy:F

    #@1cc
    move-object/from16 v0, p0

    #@1ce
    iget v6, v0, Landroid/widget/TextView;->mShadowRadius:F

    #@1d0
    sub-float/2addr v3, v6

    #@1d1
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    #@1d4
    move-result v2

    #@1d5
    add-float/2addr v13, v2

    #@1d6
    .line 5235
    const/4 v2, 0x0

    #@1d7
    move-object/from16 v0, p0

    #@1d9
    iget v3, v0, Landroid/widget/TextView;->mShadowDy:F

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iget v6, v0, Landroid/widget/TextView;->mShadowRadius:F

    #@1df
    add-float/2addr v3, v6

    #@1e0
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    #@1e3
    move-result v2

    #@1e4
    add-float/2addr v10, v2

    #@1e5
    .line 5238
    :cond_1e5
    move-object/from16 v0, p1

    #@1e7
    invoke-virtual {v0, v11, v13, v12, v10}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    #@1ea
    .line 5240
    const/16 v37, 0x0

    #@1ec
    .line 5241
    .local v37, voffsetText:I
    const/16 v36, 0x0

    #@1ee
    .line 5245
    .local v36, voffsetCursor:I
    move-object/from16 v0, p0

    #@1f0
    iget v2, v0, Landroid/widget/TextView;->mGravity:I

    #@1f2
    and-int/lit8 v2, v2, 0x70

    #@1f4
    const/16 v3, 0x30

    #@1f6
    if-eq v2, v3, :cond_206

    #@1f8
    .line 5246
    const/4 v2, 0x0

    #@1f9
    move-object/from16 v0, p0

    #@1fb
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@1fe
    move-result v37

    #@1ff
    .line 5247
    const/4 v2, 0x1

    #@200
    move-object/from16 v0, p0

    #@202
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@205
    move-result v36

    #@206
    .line 5249
    :cond_206
    move/from16 v0, v16

    #@208
    int-to-float v2, v0

    #@209
    add-int v3, v22, v37

    #@20b
    int-to-float v3, v3

    #@20c
    move-object/from16 v0, p1

    #@20e
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@211
    .line 5251
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLayoutDirection()I

    #@214
    move-result v25

    #@215
    .line 5252
    .local v25, layoutDirection:I
    move-object/from16 v0, p0

    #@217
    iget v2, v0, Landroid/widget/TextView;->mGravity:I

    #@219
    move/from16 v0, v25

    #@21b
    invoke-static {v2, v0}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@21e
    move-result v8

    #@21f
    .line 5253
    .local v8, absoluteGravity:I
    move-object/from16 v0, p0

    #@221
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@223
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@225
    if-ne v2, v3, :cond_2a1

    #@227
    move-object/from16 v0, p0

    #@229
    iget v2, v0, Landroid/widget/TextView;->mMarqueeFadeMode:I

    #@22b
    const/4 v3, 0x1

    #@22c
    if-eq v2, v3, :cond_2a1

    #@22e
    .line 5255
    move-object/from16 v0, p0

    #@230
    iget-boolean v2, v0, Landroid/widget/TextView;->mSingleLine:Z

    #@232
    if-nez v2, :cond_277

    #@234
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLineCount()I

    #@237
    move-result v2

    #@238
    const/4 v3, 0x1

    #@239
    if-ne v2, v3, :cond_277

    #@23b
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->canMarquee()Z

    #@23e
    move-result v2

    #@23f
    if-eqz v2, :cond_277

    #@241
    and-int/lit8 v2, v8, 0x7

    #@243
    const/4 v3, 0x3

    #@244
    if-eq v2, v3, :cond_277

    #@246
    .line 5257
    move-object/from16 v0, p0

    #@248
    iget v2, v0, Landroid/widget/TextView;->mRight:I

    #@24a
    move-object/from16 v0, p0

    #@24c
    iget v3, v0, Landroid/widget/TextView;->mLeft:I

    #@24e
    sub-int v39, v2, v3

    #@250
    .line 5258
    .local v39, width:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@253
    move-result v2

    #@254
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@257
    move-result v3

    #@258
    add-int v30, v2, v3

    #@25a
    .line 5259
    .local v30, padding:I
    move-object/from16 v0, p0

    #@25c
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@25e
    const/4 v3, 0x0

    #@25f
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineRight(I)F

    #@262
    move-result v2

    #@263
    sub-int v3, v39, v30

    #@265
    int-to-float v3, v3

    #@266
    sub-float v20, v2, v3

    #@268
    .line 5260
    .local v20, dx:F
    if-eqz v24, :cond_26f

    #@26a
    move/from16 v0, v20

    #@26c
    neg-float v0, v0

    #@26d
    move/from16 v20, v0

    #@26f
    .end local v20           #dx:F
    :cond_26f
    const/4 v2, 0x0

    #@270
    move-object/from16 v0, p1

    #@272
    move/from16 v1, v20

    #@274
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@277
    .line 5263
    .end local v30           #padding:I
    .end local v39           #width:I
    :cond_277
    move-object/from16 v0, p0

    #@279
    iget-object v2, v0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@27b
    if-eqz v2, :cond_2a1

    #@27d
    move-object/from16 v0, p0

    #@27f
    iget-object v2, v0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@281
    invoke-virtual {v2}, Landroid/widget/TextView$Marquee;->isRunning()Z

    #@284
    move-result v2

    #@285
    if-eqz v2, :cond_2a1

    #@287
    .line 5264
    move-object/from16 v0, p0

    #@289
    iget-object v2, v0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@28b
    invoke-virtual {v2}, Landroid/widget/TextView$Marquee;->getScroll()F

    #@28e
    move-result v2

    #@28f
    neg-float v0, v2

    #@290
    move/from16 v20, v0

    #@292
    .line 5265
    .restart local v20       #dx:F
    if-eqz v24, :cond_299

    #@294
    move/from16 v0, v20

    #@296
    neg-float v0, v0

    #@297
    move/from16 v20, v0

    #@299
    .end local v20           #dx:F
    :cond_299
    const/4 v2, 0x0

    #@29a
    move-object/from16 v0, p1

    #@29c
    move/from16 v1, v20

    #@29e
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@2a1
    .line 5269
    :cond_2a1
    sub-int v7, v36, v37

    #@2a3
    .line 5271
    .local v7, cursorOffsetVertical:I
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->getUpdatedHighlightPath()Landroid/graphics/Path;

    #@2a6
    move-result-object v5

    #@2a7
    .line 5272
    .local v5, highlight:Landroid/graphics/Path;
    move-object/from16 v0, p0

    #@2a9
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2ab
    if-eqz v2, :cond_2fb

    #@2ad
    .line 5273
    move-object/from16 v0, p0

    #@2af
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2b1
    move-object/from16 v0, p0

    #@2b3
    iget-object v6, v0, Landroid/widget/TextView;->mHighlightPaint:Landroid/graphics/Paint;

    #@2b5
    move-object/from16 v3, p1

    #@2b7
    invoke-virtual/range {v2 .. v7}, Landroid/widget/Editor;->onDraw(Landroid/graphics/Canvas;Landroid/text/Layout;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@2ba
    .line 5278
    :goto_2ba
    move-object/from16 v0, p0

    #@2bc
    iget-object v2, v0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@2be
    if-eqz v2, :cond_2ea

    #@2c0
    move-object/from16 v0, p0

    #@2c2
    iget-object v2, v0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@2c4
    invoke-virtual {v2}, Landroid/widget/TextView$Marquee;->shouldDrawGhost()Z

    #@2c7
    move-result v2

    #@2c8
    if-eqz v2, :cond_2ea

    #@2ca
    .line 5279
    move-object/from16 v0, p0

    #@2cc
    iget-object v2, v0, Landroid/widget/TextView;->mMarquee:Landroid/widget/TextView$Marquee;

    #@2ce
    invoke-virtual {v2}, Landroid/widget/TextView$Marquee;->getGhostOffset()F

    #@2d1
    move-result v2

    #@2d2
    float-to-int v0, v2

    #@2d3
    move/from16 v20, v0

    #@2d5
    .line 5280
    .local v20, dx:I
    if-eqz v24, :cond_305

    #@2d7
    move/from16 v0, v20

    #@2d9
    neg-int v2, v0

    #@2da
    int-to-float v2, v2

    #@2db
    :goto_2db
    const/4 v3, 0x0

    #@2dc
    move-object/from16 v0, p1

    #@2de
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@2e1
    .line 5281
    move-object/from16 v0, p0

    #@2e3
    iget-object v2, v0, Landroid/widget/TextView;->mHighlightPaint:Landroid/graphics/Paint;

    #@2e5
    move-object/from16 v0, p1

    #@2e7
    invoke-virtual {v4, v0, v5, v2, v7}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@2ea
    .line 5284
    .end local v20           #dx:I
    :cond_2ea
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@2ed
    .line 5285
    return-void

    #@2ee
    .end local v4           #layout:Landroid/text/Layout;
    .end local v5           #highlight:Landroid/graphics/Path;
    .end local v7           #cursorOffsetVertical:I
    .end local v8           #absoluteGravity:I
    .end local v10           #clipBottom:F
    .end local v11           #clipLeft:F
    .end local v12           #clipRight:F
    .end local v13           #clipTop:F
    .end local v14           #color:I
    .end local v19           #dr:Landroid/widget/TextView$Drawables;
    .end local v22           #extendedPaddingTop:I
    .end local v25           #layoutDirection:I
    .end local v27           #leftOffset:I
    .end local v28           #maxScrollY:I
    .end local v32           #rightOffset:I
    .end local v36           #voffsetCursor:I
    .end local v37           #voffsetText:I
    .end local v38           #vspace:I
    :cond_2ee
    move/from16 v27, v29

    #@2f0
    .line 5137
    goto/16 :goto_44

    #@2f2
    .line 5138
    .restart local v27       #leftOffset:I
    :cond_2f2
    const/16 v32, 0x0

    #@2f4
    goto/16 :goto_48

    #@2f6
    .line 5225
    .restart local v4       #layout:Landroid/text/Layout;
    .restart local v11       #clipLeft:F
    .restart local v14       #color:I
    .restart local v19       #dr:Landroid/widget/TextView$Drawables;
    .restart local v21       #extendedPaddingBottom:I
    .restart local v22       #extendedPaddingTop:I
    .restart local v28       #maxScrollY:I
    .restart local v32       #rightOffset:I
    .restart local v38       #vspace:I
    :cond_2f6
    add-int v2, v22, v34

    #@2f8
    int-to-float v13, v2

    #@2f9
    goto/16 :goto_18a

    #@2fb
    .line 5275
    .end local v21           #extendedPaddingBottom:I
    .restart local v5       #highlight:Landroid/graphics/Path;
    .restart local v7       #cursorOffsetVertical:I
    .restart local v8       #absoluteGravity:I
    .restart local v10       #clipBottom:F
    .restart local v12       #clipRight:F
    .restart local v13       #clipTop:F
    .restart local v25       #layoutDirection:I
    .restart local v36       #voffsetCursor:I
    .restart local v37       #voffsetText:I
    :cond_2fb
    move-object/from16 v0, p0

    #@2fd
    iget-object v2, v0, Landroid/widget/TextView;->mHighlightPaint:Landroid/graphics/Paint;

    #@2ff
    move-object/from16 v0, p1

    #@301
    invoke-virtual {v4, v0, v5, v2, v7}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@304
    goto :goto_2ba

    #@305
    .line 5280
    .restart local v20       #dx:I
    :cond_305
    move/from16 v0, v20

    #@307
    int-to-float v2, v0

    #@308
    goto :goto_2db
.end method

.method public onEditorAction(I)V
    .registers 23
    .parameter "actionCode"

    #@0
    .prologue
    .line 4318
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-eqz v2, :cond_1c

    #@4
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    if-eqz v2, :cond_1c

    #@a
    .line 4319
    const/4 v2, 0x5

    #@b
    move/from16 v0, p1

    #@d
    if-eq v0, v2, :cond_19

    #@f
    const/4 v2, 0x7

    #@10
    move/from16 v0, p1

    #@12
    if-eq v0, v2, :cond_19

    #@14
    const/4 v2, 0x6

    #@15
    move/from16 v0, p1

    #@17
    if-ne v0, v2, :cond_1c

    #@19
    .line 4321
    :cond_19
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@1c
    .line 4325
    :cond_1c
    move-object/from16 v0, p0

    #@1e
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@20
    if-nez v2, :cond_3c

    #@22
    const/16 v17, 0x0

    #@24
    .line 4326
    .local v17, ict:Landroid/widget/Editor$InputContentType;
    :goto_24
    if-eqz v17, :cond_a3

    #@26
    .line 4327
    move-object/from16 v0, v17

    #@28
    iget-object v2, v0, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@2a
    if-eqz v2, :cond_45

    #@2c
    .line 4328
    move-object/from16 v0, v17

    #@2e
    iget-object v2, v0, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@30
    const/4 v5, 0x0

    #@31
    move-object/from16 v0, p0

    #@33
    move/from16 v1, p1

    #@35
    invoke-interface {v2, v0, v1, v5}, Landroid/widget/TextView$OnEditorActionListener;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_45

    #@3b
    .line 4384
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 4325
    .end local v17           #ict:Landroid/widget/Editor$InputContentType;
    :cond_3c
    move-object/from16 v0, p0

    #@3e
    iget-object v2, v0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@40
    iget-object v0, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@42
    move-object/from16 v17, v0

    #@44
    goto :goto_24

    #@45
    .line 4339
    .restart local v17       #ict:Landroid/widget/Editor$InputContentType;
    :cond_45
    const/4 v2, 0x5

    #@46
    move/from16 v0, p1

    #@48
    if-ne v0, v2, :cond_64

    #@4a
    .line 4340
    const/4 v2, 0x2

    #@4b
    move-object/from16 v0, p0

    #@4d
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->focusSearch(I)Landroid/view/View;

    #@50
    move-result-object v19

    #@51
    .line 4341
    .local v19, v:Landroid/view/View;
    if-eqz v19, :cond_3b

    #@53
    .line 4342
    const/4 v2, 0x2

    #@54
    move-object/from16 v0, v19

    #@56
    invoke-virtual {v0, v2}, Landroid/view/View;->requestFocus(I)Z

    #@59
    move-result v2

    #@5a
    if-nez v2, :cond_3b

    #@5c
    .line 4343
    new-instance v2, Ljava/lang/IllegalStateException;

    #@5e
    const-string v5, "focus search returned a view that wasn\'t able to take focus!"

    #@60
    invoke-direct {v2, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@63
    throw v2

    #@64
    .line 4349
    .end local v19           #v:Landroid/view/View;
    :cond_64
    const/4 v2, 0x7

    #@65
    move/from16 v0, p1

    #@67
    if-ne v0, v2, :cond_83

    #@69
    .line 4350
    const/4 v2, 0x1

    #@6a
    move-object/from16 v0, p0

    #@6c
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->focusSearch(I)Landroid/view/View;

    #@6f
    move-result-object v19

    #@70
    .line 4351
    .restart local v19       #v:Landroid/view/View;
    if-eqz v19, :cond_3b

    #@72
    .line 4352
    const/4 v2, 0x1

    #@73
    move-object/from16 v0, v19

    #@75
    invoke-virtual {v0, v2}, Landroid/view/View;->requestFocus(I)Z

    #@78
    move-result v2

    #@79
    if-nez v2, :cond_3b

    #@7b
    .line 4353
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7d
    const-string v5, "focus search returned a view that wasn\'t able to take focus!"

    #@7f
    invoke-direct {v2, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@82
    throw v2

    #@83
    .line 4359
    .end local v19           #v:Landroid/view/View;
    :cond_83
    const/4 v2, 0x6

    #@84
    move/from16 v0, p1

    #@86
    if-ne v0, v2, :cond_a3

    #@88
    .line 4360
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@8b
    move-result-object v18

    #@8c
    .line 4361
    .local v18, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v18, :cond_3b

    #@8e
    move-object/from16 v0, v18

    #@90
    move-object/from16 v1, p0

    #@92
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@95
    move-result v2

    #@96
    if-eqz v2, :cond_3b

    #@98
    .line 4362
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    #@9b
    move-result-object v2

    #@9c
    const/4 v5, 0x0

    #@9d
    move-object/from16 v0, v18

    #@9f
    invoke-virtual {v0, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@a2
    goto :goto_3b

    #@a3
    .line 4368
    .end local v18           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_a3
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@a6
    move-result-object v20

    #@a7
    .line 4369
    .local v20, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v20, :cond_3b

    #@a9
    .line 4370
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@ac
    move-result-wide v3

    #@ad
    .line 4371
    .local v3, eventTime:J
    new-instance v2, Landroid/view/KeyEvent;

    #@af
    const/4 v7, 0x0

    #@b0
    const/16 v8, 0x42

    #@b2
    const/4 v9, 0x0

    #@b3
    const/4 v10, 0x0

    #@b4
    const/4 v11, -0x1

    #@b5
    const/4 v12, 0x0

    #@b6
    const/16 v13, 0x16

    #@b8
    move-wide v5, v3

    #@b9
    invoke-direct/range {v2 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@bc
    move-object/from16 v0, v20

    #@be
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl;->dispatchKeyFromIme(Landroid/view/KeyEvent;)V

    #@c1
    .line 4377
    new-instance v5, Landroid/view/KeyEvent;

    #@c3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c6
    move-result-wide v6

    #@c7
    const/4 v10, 0x1

    #@c8
    const/16 v11, 0x42

    #@ca
    const/4 v12, 0x0

    #@cb
    const/4 v13, 0x0

    #@cc
    const/4 v14, -0x1

    #@cd
    const/4 v15, 0x0

    #@ce
    const/16 v16, 0x16

    #@d0
    move-wide v8, v3

    #@d1
    invoke-direct/range {v5 .. v16}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@d4
    move-object/from16 v0, v20

    #@d6
    invoke-virtual {v0, v5}, Landroid/view/ViewRootImpl;->dispatchKeyFromIme(Landroid/view/KeyEvent;)V

    #@d9
    goto/16 :goto_3b
.end method

.method public onEndBatchEdit()V
    .registers 1

    #@0
    .prologue
    .line 5993
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 7742
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    #@4
    .line 7745
    iget-boolean v0, p0, Landroid/widget/TextView;->mDispatchTemporaryDetach:Z

    #@6
    if-nez v0, :cond_a

    #@8
    iput-boolean v1, p0, Landroid/widget/TextView;->mTemporaryDetach:Z

    #@a
    .line 7746
    :cond_a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    if-eqz v0, :cond_12

    #@e
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@10
    iput-boolean v1, v0, Landroid/widget/Editor;->mTemporaryDetach:Z

    #@12
    .line 7747
    :cond_12
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 12
    .parameter "focused"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 7751
    iget-boolean v0, p0, Landroid/widget/TextView;->mTemporaryDetach:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 7753
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@7
    .line 7777
    :goto_7
    return-void

    #@8
    .line 7757
    :cond_8
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a
    if-eqz v0, :cond_11

    #@c
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@e
    invoke-virtual {v0, p1, p2}, Landroid/widget/Editor;->onFocusChanged(ZI)V

    #@11
    .line 7759
    :cond_11
    if-eqz p1, :cond_29

    #@13
    .line 7760
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@15
    instance-of v0, v0, Landroid/text/Spannable;

    #@17
    if-eqz v0, :cond_29

    #@19
    .line 7761
    iget-object v7, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1b
    check-cast v7, Landroid/text/Spannable;

    #@1d
    .line 7762
    .local v7, sp:Landroid/text/Spannable;
    invoke-static {v7}, Landroid/text/method/MetaKeyKeyListener;->resetMetaState(Landroid/text/Spannable;)V

    #@20
    .line 7764
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@23
    move-result-object v6

    #@24
    .line 7765
    .local v6, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v6, :cond_29

    #@26
    invoke-virtual {v6, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@29
    .line 7770
    .end local v6           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v7           #sp:Landroid/text/Spannable;
    :cond_29
    invoke-direct {p0, p1}, Landroid/widget/TextView;->startStopMarquee(Z)V

    #@2c
    .line 7772
    iget-object v0, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@2e
    if-eqz v0, :cond_3b

    #@30
    .line 7773
    iget-object v0, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@32
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@34
    move-object v1, p0

    #@35
    move v3, p1

    #@36
    move v4, p2

    #@37
    move-object v5, p3

    #@38
    invoke-interface/range {v0 .. v5}, Landroid/text/method/TransformationMethod;->onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V

    #@3b
    .line 7776
    :cond_3b
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3e
    goto :goto_7
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 7896
    iget-object v0, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@2
    if-eqz v0, :cond_1d

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@6
    instance-of v0, v0, Landroid/text/Spannable;

    #@8
    if-eqz v0, :cond_1d

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@c
    if-eqz v0, :cond_1d

    #@e
    .line 7898
    :try_start_e
    iget-object v1, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@10
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@12
    check-cast v0, Landroid/text/Spannable;

    #@14
    invoke-interface {v1, p0, v0, p1}, Landroid/text/method/MovementMethod;->onGenericMotionEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    :try_end_17
    .catch Ljava/lang/AbstractMethodError; {:try_start_e .. :try_end_17} :catch_1c

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1d

    #@1a
    .line 7899
    const/4 v0, 0x1

    #@1b
    .line 7907
    :goto_1b
    return v0

    #@1c
    .line 7901
    :catch_1c
    move-exception v0

    #@1d
    .line 7907
    :cond_1d
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@20
    move-result v0

    #@21
    goto :goto_1b
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 8259
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 8261
    const-class v1, Landroid/widget/TextView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 8262
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@f
    move-result v0

    #@10
    .line 8263
    .local v0, isPassword:Z
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPassword(Z)V

    #@13
    .line 8265
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@16
    move-result v1

    #@17
    const/16 v2, 0x2000

    #@19
    if-ne v1, v2, :cond_36

    #@1b
    .line 8266
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1d
    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@20
    move-result v1

    #@21
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    #@24
    .line 8267
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@26
    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@29
    move-result v1

    #@2a
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    #@2d
    .line 8268
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2f
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@32
    move-result v1

    #@33
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    #@36
    .line 8270
    :cond_36
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 8274
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 8276
    const-class v1, Landroid/widget/TextView;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 8277
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@f
    move-result v0

    #@10
    .line 8278
    .local v0, isPassword:Z
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPassword(Z)V

    #@13
    .line 8280
    if-nez v0, :cond_1c

    #@15
    .line 8281
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextForAccessibility()Ljava/lang/CharSequence;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    #@1c
    .line 8284
    :cond_1c
    invoke-virtual {p0}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_3d

    #@26
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@28
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2b
    move-result v1

    #@2c
    if-nez v1, :cond_3d

    #@2e
    .line 8285
    const/16 v1, 0x100

    #@30
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@33
    .line 8286
    const/16 v1, 0x200

    #@35
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@38
    .line 8287
    const/16 v1, 0x1f

    #@3a
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setMovementGranularities(I)V

    #@3d
    .line 8293
    :cond_3d
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 5460
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, p2, v1}, Landroid/widget/TextView;->doKeyDown(ILandroid/view/KeyEvent;Landroid/view/KeyEvent;)I

    #@4
    move-result v0

    #@5
    .line 5461
    .local v0, which:I
    if-nez v0, :cond_c

    #@7
    .line 5463
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@a
    move-result v1

    #@b
    .line 5466
    :goto_b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x1

    #@d
    goto :goto_b
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 10
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 5471
    const/4 v3, 0x0

    #@2
    invoke-static {p3, v3}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@5
    move-result-object v0

    #@6
    .line 5473
    .local v0, down:Landroid/view/KeyEvent;
    invoke-direct {p0, p1, v0, p3}, Landroid/widget/TextView;->doKeyDown(ILandroid/view/KeyEvent;Landroid/view/KeyEvent;)I

    #@9
    move-result v2

    #@a
    .line 5474
    .local v2, which:I
    if-nez v2, :cond_11

    #@c
    .line 5476
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    #@f
    move-result v3

    #@10
    .line 5509
    :goto_10
    return v3

    #@11
    .line 5478
    :cond_11
    const/4 v3, -0x1

    #@12
    if-ne v2, v3, :cond_16

    #@14
    move v3, v4

    #@15
    .line 5480
    goto :goto_10

    #@16
    .line 5483
    :cond_16
    add-int/lit8 p2, p2, -0x1

    #@18
    .line 5490
    invoke-static {p3, v4}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@1b
    move-result-object v1

    #@1c
    .line 5491
    .local v1, up:Landroid/view/KeyEvent;
    if-ne v2, v4, :cond_49

    #@1e
    .line 5493
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@20
    iget-object v5, v3, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@22
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@24
    check-cast v3, Landroid/text/Editable;

    #@26
    invoke-interface {v5, p0, v3, p1, v1}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@29
    .line 5494
    :goto_29
    add-int/lit8 p2, p2, -0x1

    #@2b
    if-lez p2, :cond_44

    #@2d
    .line 5495
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2f
    iget-object v5, v3, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@31
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@33
    check-cast v3, Landroid/text/Editable;

    #@35
    invoke-interface {v5, p0, v3, p1, v0}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@38
    .line 5496
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3a
    iget-object v5, v3, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@3c
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3e
    check-cast v3, Landroid/text/Editable;

    #@40
    invoke-interface {v5, p0, v3, p1, v1}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@43
    goto :goto_29

    #@44
    .line 5498
    :cond_44
    invoke-virtual {p0}, Landroid/widget/TextView;->hideErrorIfUnchanged()V

    #@47
    :cond_47
    move v3, v4

    #@48
    .line 5509
    goto :goto_10

    #@49
    .line 5500
    :cond_49
    const/4 v3, 0x2

    #@4a
    if-ne v2, v3, :cond_47

    #@4c
    .line 5502
    iget-object v5, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@4e
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@50
    check-cast v3, Landroid/text/Spannable;

    #@52
    invoke-interface {v5, p0, v3, p1, v1}, Landroid/text/method/MovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@55
    .line 5503
    :goto_55
    add-int/lit8 p2, p2, -0x1

    #@57
    if-lez p2, :cond_47

    #@59
    .line 5504
    iget-object v5, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@5b
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@5d
    check-cast v3, Landroid/text/Spannable;

    #@5f
    invoke-interface {v5, p0, v3, p1, v0}, Landroid/text/method/MovementMethod;->onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@62
    .line 5505
    iget-object v5, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@64
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@66
    check-cast v3, Landroid/text/Spannable;

    #@68
    invoke-interface {v5, p0, v3, p1, v1}, Landroid/text/method/MovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@6b
    goto :goto_55
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 5422
    const/4 v3, 0x4

    #@3
    if-ne p1, v3, :cond_8c

    #@5
    .line 5423
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@7
    if-eqz v3, :cond_18

    #@9
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_18

    #@f
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@11
    if-eqz v3, :cond_18

    #@13
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@15
    invoke-virtual {v3, v0}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@18
    .line 5424
    :cond_18
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1a
    if-eqz v3, :cond_2f

    #@1c
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1e
    iget-object v3, v3, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@20
    if-nez v3, :cond_2e

    #@22
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@24
    if-eqz v3, :cond_2f

    #@26
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@28
    invoke-virtual {v3}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_2f

    #@2e
    :cond_2e
    move v0, v2

    #@2f
    .line 5427
    .local v0, isInSelectionMode:Z
    :cond_2f
    if-eqz v0, :cond_66

    #@31
    .line 5428
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@34
    move-result v3

    #@35
    if-nez v3, :cond_47

    #@37
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@3a
    move-result v3

    #@3b
    if-nez v3, :cond_47

    #@3d
    .line 5429
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@40
    move-result-object v1

    #@41
    .line 5430
    .local v1, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v1, :cond_46

    #@43
    .line 5431
    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@46
    .line 5455
    .end local v0           #isInSelectionMode:Z
    .end local v1           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_46
    :goto_46
    return v2

    #@47
    .line 5434
    .restart local v0       #isInSelectionMode:Z
    :cond_47
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@4a
    move-result v3

    #@4b
    if-ne v3, v2, :cond_8c

    #@4d
    .line 5435
    invoke-virtual {p0}, Landroid/widget/TextView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@50
    move-result-object v1

    #@51
    .line 5436
    .restart local v1       #state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v1, :cond_56

    #@53
    .line 5437
    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@56
    .line 5439
    :cond_56
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@59
    move-result v3

    #@5a
    if-eqz v3, :cond_8c

    #@5c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@5f
    move-result v3

    #@60
    if-nez v3, :cond_8c

    #@62
    .line 5440
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@65
    goto :goto_46

    #@66
    .line 5445
    .end local v1           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_66
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    #@69
    move-result v3

    #@6a
    if-ne v3, v2, :cond_8c

    #@6c
    .line 5446
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@6e
    if-eqz v3, :cond_8c

    #@70
    .line 5447
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@72
    if-eqz v3, :cond_8c

    #@74
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@76
    iget-object v3, v3, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@78
    if-eqz v3, :cond_8c

    #@7a
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7c
    iget-object v3, v3, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@7e
    invoke-interface {v3}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@81
    move-result v3

    #@82
    if-nez v3, :cond_8c

    #@84
    .line 5448
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@86
    iget-object v3, v3, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@88
    invoke-interface {v3}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptray()V

    #@8b
    goto :goto_46

    #@8c
    .line 5455
    .end local v0           #isInSelectionMode:Z
    :cond_8c
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    #@8f
    move-result v2

    #@90
    goto :goto_46
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 8115
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    #@3
    move-result v1

    #@4
    and-int/lit16 v0, v1, -0x7001

    #@6
    .line 8116
    .local v0, filteredMetaState:I
    invoke-static {v0}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_f

    #@c
    .line 8117
    sparse-switch p1, :sswitch_data_4c

    #@f
    .line 8140
    :cond_f
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    #@12
    move-result v1

    #@13
    :goto_13
    return v1

    #@14
    .line 8119
    :sswitch_14
    invoke-direct {p0}, Landroid/widget/TextView;->canSelectText()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_f

    #@1a
    .line 8120
    const v1, 0x102001f

    #@1d
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@20
    move-result v1

    #@21
    goto :goto_13

    #@22
    .line 8124
    :sswitch_22
    invoke-virtual {p0}, Landroid/widget/TextView;->canCut()Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_f

    #@28
    .line 8125
    const v1, 0x1020020

    #@2b
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@2e
    move-result v1

    #@2f
    goto :goto_13

    #@30
    .line 8129
    :sswitch_30
    invoke-virtual {p0}, Landroid/widget/TextView;->canCopy()Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_f

    #@36
    .line 8130
    const v1, 0x1020021

    #@39
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@3c
    move-result v1

    #@3d
    goto :goto_13

    #@3e
    .line 8134
    :sswitch_3e
    invoke-virtual {p0}, Landroid/widget/TextView;->canPaste()Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_f

    #@44
    .line 8135
    const v1, 0x1020022

    #@47
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@4a
    move-result v1

    #@4b
    goto :goto_13

    #@4c
    .line 8117
    :sswitch_data_4c
    .sparse-switch
        0x1d -> :sswitch_14
        0x1f -> :sswitch_30
        0x32 -> :sswitch_3e
        0x34 -> :sswitch_22
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/16 v5, 0x82

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 5702
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_f

    #@a
    .line 5703
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@d
    move-result v2

    #@e
    .line 5796
    :goto_e
    return v2

    #@f
    .line 5706
    :cond_f
    sparse-switch p1, :sswitch_data_104

    #@12
    .line 5788
    :cond_12
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@14
    if-eqz v2, :cond_e7

    #@16
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@18
    iget-object v2, v2, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@1a
    if-eqz v2, :cond_e7

    #@1c
    .line 5789
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1e
    iget-object v4, v2, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@20
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@22
    check-cast v2, Landroid/text/Editable;

    #@24
    invoke-interface {v4, p0, v2, p1, p2}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_e7

    #@2a
    move v2, v3

    #@2b
    .line 5790
    goto :goto_e

    #@2c
    .line 5708
    :sswitch_2c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_5e

    #@32
    .line 5718
    invoke-virtual {p0}, Landroid/widget/TextView;->hasOnClickListeners()Z

    #@35
    move-result v2

    #@36
    if-nez v2, :cond_5e

    #@38
    .line 5719
    iget-object v2, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@3a
    if-eqz v2, :cond_5e

    #@3c
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3e
    instance-of v2, v2, Landroid/text/Editable;

    #@40
    if-eqz v2, :cond_5e

    #@42
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@44
    if-eqz v2, :cond_5e

    #@46
    invoke-virtual {p0}, Landroid/widget/TextView;->onCheckIsTextEditor()Z

    #@49
    move-result v2

    #@4a
    if-eqz v2, :cond_5e

    #@4c
    .line 5721
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@4f
    move-result-object v0

    #@50
    .line 5722
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->viewClicked(Landroid/view/inputmethod/InputMethodManager;)V

    #@53
    .line 5723
    if-eqz v0, :cond_5e

    #@55
    invoke-virtual {p0}, Landroid/widget/TextView;->getShowSoftInputOnFocus()Z

    #@58
    move-result v2

    #@59
    if-eqz v2, :cond_5e

    #@5b
    .line 5724
    invoke-virtual {v0, p0, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@5e
    .line 5729
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_5e
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@61
    move-result v2

    #@62
    goto :goto_e

    #@63
    .line 5732
    :sswitch_63
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@66
    move-result v2

    #@67
    if-eqz v2, :cond_12

    #@69
    .line 5733
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6b
    if-eqz v2, :cond_98

    #@6d
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6f
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@71
    if-eqz v2, :cond_98

    #@73
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@75
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@77
    iget-object v2, v2, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@79
    if-eqz v2, :cond_98

    #@7b
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7d
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@7f
    iget-boolean v2, v2, Landroid/widget/Editor$InputContentType;->enterDown:Z

    #@81
    if-eqz v2, :cond_98

    #@83
    .line 5736
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@85
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@87
    iput-boolean v4, v2, Landroid/widget/Editor$InputContentType;->enterDown:Z

    #@89
    .line 5737
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8b
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@8d
    iget-object v2, v2, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@8f
    invoke-interface {v2, p0, v4, p2}, Landroid/widget/TextView$OnEditorActionListener;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    #@92
    move-result v2

    #@93
    if-eqz v2, :cond_98

    #@95
    move v2, v3

    #@96
    .line 5739
    goto/16 :goto_e

    #@98
    .line 5743
    :cond_98
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    #@9b
    move-result v2

    #@9c
    and-int/lit8 v2, v2, 0x10

    #@9e
    if-nez v2, :cond_a6

    #@a0
    invoke-direct {p0}, Landroid/widget/TextView;->shouldAdvanceFocusOnEnter()Z

    #@a3
    move-result v2

    #@a4
    if-eqz v2, :cond_e1

    #@a6
    .line 5755
    :cond_a6
    invoke-virtual {p0}, Landroid/widget/TextView;->hasOnClickListeners()Z

    #@a9
    move-result v2

    #@aa
    if-nez v2, :cond_e1

    #@ac
    .line 5756
    invoke-virtual {p0, v5}, Landroid/widget/TextView;->focusSearch(I)Landroid/view/View;

    #@af
    move-result-object v1

    #@b0
    .line 5758
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_c6

    #@b2
    .line 5759
    invoke-virtual {v1, v5}, Landroid/view/View;->requestFocus(I)Z

    #@b5
    move-result v2

    #@b6
    if-nez v2, :cond_c0

    #@b8
    .line 5760
    new-instance v2, Ljava/lang/IllegalStateException;

    #@ba
    const-string v3, "focus search returned a view that wasn\'t able to take focus!"

    #@bc
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@bf
    throw v2

    #@c0
    .line 5770
    :cond_c0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@c3
    move v2, v3

    #@c4
    .line 5771
    goto/16 :goto_e

    #@c6
    .line 5772
    :cond_c6
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    #@c9
    move-result v2

    #@ca
    and-int/lit8 v2, v2, 0x10

    #@cc
    if-eqz v2, :cond_e1

    #@ce
    .line 5776
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@d1
    move-result-object v0

    #@d2
    .line 5777
    .restart local v0       #imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_e1

    #@d4
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@d7
    move-result v2

    #@d8
    if-eqz v2, :cond_e1

    #@da
    .line 5778
    invoke-virtual {p0}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    #@dd
    move-result-object v2

    #@de
    invoke-virtual {v0, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@e1
    .line 5783
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v1           #v:Landroid/view/View;
    :cond_e1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@e4
    move-result v2

    #@e5
    goto/16 :goto_e

    #@e7
    .line 5792
    :cond_e7
    iget-object v2, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@e9
    if-eqz v2, :cond_fe

    #@eb
    iget-object v2, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@ed
    if-eqz v2, :cond_fe

    #@ef
    .line 5793
    iget-object v4, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@f1
    iget-object v2, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@f3
    check-cast v2, Landroid/text/Spannable;

    #@f5
    invoke-interface {v4, p0, v2, p1, p2}, Landroid/text/method/MovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@f8
    move-result v2

    #@f9
    if-eqz v2, :cond_fe

    #@fb
    move v2, v3

    #@fc
    .line 5794
    goto/16 :goto_e

    #@fe
    .line 5796
    :cond_fe
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@101
    move-result v2

    #@102
    goto/16 :goto_e

    #@104
    .line 5706
    :sswitch_data_104
    .sparse-switch
        0x17 -> :sswitch_2c
        0x42 -> :sswitch_63
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 14
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const v7, 0x800003

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 6747
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    #@8
    .line 6748
    iget v3, p0, Landroid/widget/TextView;->mDeferScroll:I

    #@a
    if-ltz v3, :cond_1e

    #@c
    .line 6749
    iget v0, p0, Landroid/widget/TextView;->mDeferScroll:I

    #@e
    .line 6750
    .local v0, curs:I
    const/4 v3, -0x1

    #@f
    iput v3, p0, Landroid/widget/TextView;->mDeferScroll:I

    #@11
    .line 6751
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@13
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    #@16
    move-result v3

    #@17
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    #@1a
    move-result v3

    #@1b
    invoke-virtual {p0, v3}, Landroid/widget/TextView;->bringPointIntoView(I)Z

    #@1e
    .line 6753
    .end local v0           #curs:I
    :cond_1e
    if-eqz p1, :cond_29

    #@20
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@22
    if-eqz v3, :cond_29

    #@24
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@26
    invoke-virtual {v3}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@29
    .line 6755
    :cond_29
    invoke-virtual {p0}, Landroid/widget/TextView;->isLayoutRtl()Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_90

    #@2f
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@31
    instance-of v3, v3, Landroid/text/Editable;

    #@33
    if-nez v3, :cond_90

    #@35
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@37
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    const-string v4, ""

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v3

    #@41
    if-nez v3, :cond_90

    #@43
    .line 6756
    iget-object v3, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@45
    if-eqz v3, :cond_90

    #@47
    iget-object v3, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@49
    invoke-virtual {v3, v5}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@4c
    move-result v3

    #@4d
    if-ne v6, v3, :cond_90

    #@4f
    .line 6757
    iget v3, p0, Landroid/widget/TextView;->mRight:I

    #@51
    iget v4, p0, Landroid/widget/TextView;->mLeft:I

    #@53
    sub-int/2addr v3, v4

    #@54
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@57
    move-result v4

    #@58
    sub-int/2addr v3, v4

    #@59
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@5c
    move-result v4

    #@5d
    sub-int v2, v3, v4

    #@5f
    .line 6758
    .local v2, mWidth:I
    if-eqz v2, :cond_90

    #@61
    iget-object v3, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@63
    invoke-virtual {v3, v5}, Landroid/text/Layout;->getLineWidth(I)F

    #@66
    move-result v3

    #@67
    const/4 v4, 0x0

    #@68
    cmpl-float v3, v3, v4

    #@6a
    if-eqz v3, :cond_90

    #@6c
    .line 6759
    iget v1, p0, Landroid/widget/TextView;->mGravity:I

    #@6e
    .line 6760
    .local v1, gravity:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    #@71
    move-result v3

    #@72
    if-ne v3, v6, :cond_90

    #@74
    iget-object v3, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@76
    invoke-virtual {v3, v5}, Landroid/text/Layout;->getLineWidth(I)F

    #@79
    move-result v3

    #@7a
    int-to-float v4, v2

    #@7b
    cmpg-float v3, v3, v4

    #@7d
    if-gez v3, :cond_90

    #@7f
    and-int v3, v1, v7

    #@81
    if-ne v3, v7, :cond_90

    #@83
    .line 6761
    const v3, -0x800004

    #@86
    and-int/2addr v1, v3

    #@87
    .line 6762
    or-int/lit8 v1, v1, 0x5

    #@89
    .line 6763
    iput-boolean v6, p0, Landroid/widget/TextView;->mIsShortLtrText:Z

    #@8b
    .line 6764
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setGravity(I)V

    #@8e
    .line 6765
    iput-boolean v5, p0, Landroid/widget/TextView;->mIsShortLtrText:Z

    #@90
    .line 6770
    .end local v1           #gravity:I
    .end local v2           #mWidth:I
    :cond_90
    return-void
.end method

.method onLocaleChanged()V
    .registers 3

    #@0
    .prologue
    .line 8219
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    const/4 v1, 0x0

    #@3
    iput-object v1, v0, Landroid/widget/Editor;->mWordIterator:Landroid/text/method/WordIterator;

    #@5
    .line 8220
    return-void
.end method

.method protected onMeasure(II)V
    .registers 30
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 6412
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v24

    #@4
    .line 6413
    .local v24, widthMode:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v14

    #@8
    .line 6414
    .local v14, heightMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@b
    move-result v25

    #@c
    .line 6415
    .local v25, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@f
    move-result v15

    #@10
    .line 6420
    .local v15, heightSize:I
    sget-object v5, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@12
    .line 6421
    .local v5, boring:Landroid/text/BoringLayout$Metrics;
    sget-object v6, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@14
    .line 6423
    .local v6, hintBoring:Landroid/text/BoringLayout$Metrics;
    move-object/from16 v0, p0

    #@16
    iget-object v2, v0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@18
    if-nez v2, :cond_1d

    #@1a
    .line 6424
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getTextDirectionHeuristic()Landroid/text/TextDirectionHeuristic;

    #@1d
    .line 6427
    :cond_1d
    const/4 v9, -0x1

    #@1e
    .line 6428
    .local v9, des:I
    const/4 v12, 0x0

    #@1f
    .line 6430
    .local v12, fromexisting:Z
    const/high16 v2, 0x4000

    #@21
    move/from16 v0, v24

    #@23
    if-ne v0, v2, :cond_ca

    #@25
    .line 6432
    move/from16 v22, v25

    #@27
    .line 6515
    .local v22, width:I
    :cond_27
    :goto_27
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@2a
    move-result v2

    #@2b
    sub-int v2, v22, v2

    #@2d
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@30
    move-result v7

    #@31
    sub-int v3, v2, v7

    #@33
    .line 6516
    .local v3, want:I
    move/from16 v21, v3

    #@35
    .line 6519
    .local v21, unpaddedWidth:I
    move-object/from16 v0, p0

    #@37
    iget-boolean v2, v0, Landroid/widget/TextView;->mHorizontallyScrolling:Z

    #@39
    if-eqz v2, :cond_43

    #@3b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->isHardwareAccelerated()Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_205

    #@41
    const/high16 v3, 0x10

    #@43
    .line 6522
    :cond_43
    :goto_43
    move v4, v3

    #@44
    .line 6523
    .local v4, hintWant:I
    move-object/from16 v0, p0

    #@46
    iget-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@48
    if-nez v2, :cond_209

    #@4a
    move/from16 v17, v4

    #@4c
    .line 6525
    .local v17, hintWidth:I
    :goto_4c
    move-object/from16 v0, p0

    #@4e
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@50
    if-nez v2, :cond_213

    #@52
    .line 6526
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@55
    move-result v2

    #@56
    sub-int v2, v22, v2

    #@58
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@5b
    move-result v7

    #@5c
    sub-int v7, v2, v7

    #@5e
    const/4 v8, 0x0

    #@5f
    move-object/from16 v2, p0

    #@61
    invoke-virtual/range {v2 .. v8}, Landroid/widget/TextView;->makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    #@64
    .line 6553
    :cond_64
    :goto_64
    const/high16 v2, 0x4000

    #@66
    if-ne v14, v2, :cond_2a2

    #@68
    .line 6555
    move v13, v15

    #@69
    .line 6556
    .local v13, height:I
    const/4 v2, -0x1

    #@6a
    move-object/from16 v0, p0

    #@6c
    iput v2, v0, Landroid/widget/TextView;->mDesiredHeightAtMeasure:I

    #@6e
    .line 6568
    :cond_6e
    :goto_6e
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@71
    move-result v2

    #@72
    sub-int v2, v13, v2

    #@74
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@77
    move-result v7

    #@78
    sub-int v20, v2, v7

    #@7a
    .line 6569
    .local v20, unpaddedHeight:I
    move-object/from16 v0, p0

    #@7c
    iget v2, v0, Landroid/widget/TextView;->mMaxMode:I

    #@7e
    const/4 v7, 0x1

    #@7f
    if-ne v2, v7, :cond_a1

    #@81
    move-object/from16 v0, p0

    #@83
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@85
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    #@88
    move-result v2

    #@89
    move-object/from16 v0, p0

    #@8b
    iget v7, v0, Landroid/widget/TextView;->mMaximum:I

    #@8d
    if-le v2, v7, :cond_a1

    #@8f
    .line 6570
    move-object/from16 v0, p0

    #@91
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@93
    move-object/from16 v0, p0

    #@95
    iget v7, v0, Landroid/widget/TextView;->mMaximum:I

    #@97
    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineTop(I)I

    #@9a
    move-result v2

    #@9b
    move/from16 v0, v20

    #@9d
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    #@a0
    move-result v20

    #@a1
    .line 6577
    :cond_a1
    move-object/from16 v0, p0

    #@a3
    iget-object v2, v0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@a5
    if-nez v2, :cond_bf

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@ab
    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    #@ae
    move-result v2

    #@af
    move/from16 v0, v21

    #@b1
    if-gt v2, v0, :cond_bf

    #@b3
    move-object/from16 v0, p0

    #@b5
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@b7
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    #@ba
    move-result v2

    #@bb
    move/from16 v0, v20

    #@bd
    if-le v2, v0, :cond_2b5

    #@bf
    .line 6580
    :cond_bf
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->registerForPreDraw()V

    #@c2
    .line 6585
    :goto_c2
    move-object/from16 v0, p0

    #@c4
    move/from16 v1, v22

    #@c6
    invoke-virtual {v0, v1, v13}, Landroid/widget/TextView;->setMeasuredDimension(II)V

    #@c9
    .line 6586
    return-void

    #@ca
    .line 6434
    .end local v3           #want:I
    .end local v4           #hintWant:I
    .end local v13           #height:I
    .end local v17           #hintWidth:I
    .end local v20           #unpaddedHeight:I
    .end local v21           #unpaddedWidth:I
    .end local v22           #width:I
    :cond_ca
    move-object/from16 v0, p0

    #@cc
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@ce
    if-eqz v2, :cond_de

    #@d0
    move-object/from16 v0, p0

    #@d2
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@d4
    if-nez v2, :cond_de

    #@d6
    .line 6435
    move-object/from16 v0, p0

    #@d8
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@da
    invoke-static {v2}, Landroid/widget/TextView;->desired(Landroid/text/Layout;)I

    #@dd
    move-result v9

    #@de
    .line 6438
    :cond_de
    if-gez v9, :cond_1e1

    #@e0
    .line 6439
    move-object/from16 v0, p0

    #@e2
    iget-object v2, v0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@e4
    move-object/from16 v0, p0

    #@e6
    iget-object v7, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@e8
    move-object/from16 v0, p0

    #@ea
    iget-object v8, v0, Landroid/widget/TextView;->mTextDir:Landroid/text/TextDirectionHeuristic;

    #@ec
    move-object/from16 v0, p0

    #@ee
    iget-object v0, v0, Landroid/widget/TextView;->mBoring:Landroid/text/BoringLayout$Metrics;

    #@f0
    move-object/from16 v26, v0

    #@f2
    move-object/from16 v0, v26

    #@f4
    invoke-static {v2, v7, v8, v0}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@f7
    move-result-object v5

    #@f8
    .line 6440
    if-eqz v5, :cond_fe

    #@fa
    .line 6441
    move-object/from16 v0, p0

    #@fc
    iput-object v5, v0, Landroid/widget/TextView;->mBoring:Landroid/text/BoringLayout$Metrics;

    #@fe
    .line 6447
    :cond_fe
    :goto_fe
    if-eqz v5, :cond_104

    #@100
    sget-object v2, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@102
    if-ne v5, v2, :cond_1e4

    #@104
    .line 6448
    :cond_104
    if-gez v9, :cond_117

    #@106
    .line 6449
    move-object/from16 v0, p0

    #@108
    iget-object v2, v0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget-object v7, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@10e
    invoke-static {v2, v7}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    #@111
    move-result v2

    #@112
    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    #@115
    move-result v2

    #@116
    float-to-int v9, v2

    #@117
    .line 6451
    :cond_117
    move/from16 v22, v9

    #@119
    .line 6456
    .restart local v22       #width:I
    :goto_119
    move-object/from16 v0, p0

    #@11b
    iget-object v11, v0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@11d
    .line 6457
    .local v11, dr:Landroid/widget/TextView$Drawables;
    if-eqz v11, :cond_12f

    #@11f
    .line 6458
    iget v2, v11, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@121
    move/from16 v0, v22

    #@123
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    #@126
    move-result v22

    #@127
    .line 6459
    iget v2, v11, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@129
    move/from16 v0, v22

    #@12b
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    #@12e
    move-result v22

    #@12f
    .line 6462
    :cond_12f
    move-object/from16 v0, p0

    #@131
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@133
    if-eqz v2, :cond_190

    #@135
    .line 6463
    const/16 v16, -0x1

    #@137
    .line 6466
    .local v16, hintDes:I
    move-object/from16 v0, p0

    #@139
    iget-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@13b
    if-eqz v2, :cond_14b

    #@13d
    move-object/from16 v0, p0

    #@13f
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@141
    if-nez v2, :cond_14b

    #@143
    .line 6467
    move-object/from16 v0, p0

    #@145
    iget-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@147
    invoke-static {v2}, Landroid/widget/TextView;->desired(Landroid/text/Layout;)I

    #@14a
    move-result v16

    #@14b
    .line 6470
    :cond_14b
    if-gez v16, :cond_16b

    #@14d
    .line 6472
    move-object/from16 v0, p0

    #@14f
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@151
    move-object/from16 v0, p0

    #@153
    iget-object v7, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@155
    move-object/from16 v0, p0

    #@157
    iget-object v8, v0, Landroid/widget/TextView;->mHintTextDir:Landroid/text/TextDirectionHeuristic;

    #@159
    move-object/from16 v0, p0

    #@15b
    iget-object v0, v0, Landroid/widget/TextView;->mHintBoring:Landroid/text/BoringLayout$Metrics;

    #@15d
    move-object/from16 v26, v0

    #@15f
    move-object/from16 v0, v26

    #@161
    invoke-static {v2, v7, v8, v0}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@164
    move-result-object v6

    #@165
    .line 6474
    if-eqz v6, :cond_16b

    #@167
    .line 6475
    move-object/from16 v0, p0

    #@169
    iput-object v6, v0, Landroid/widget/TextView;->mHintBoring:Landroid/text/BoringLayout$Metrics;

    #@16b
    .line 6479
    :cond_16b
    if-eqz v6, :cond_171

    #@16d
    sget-object v2, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@16f
    if-ne v6, v2, :cond_1ea

    #@171
    .line 6480
    :cond_171
    if-gez v16, :cond_186

    #@173
    .line 6481
    move-object/from16 v0, p0

    #@175
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@177
    move-object/from16 v0, p0

    #@179
    iget-object v7, v0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@17b
    invoke-static {v2, v7}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    #@17e
    move-result v2

    #@17f
    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    #@182
    move-result v2

    #@183
    float-to-int v0, v2

    #@184
    move/from16 v16, v0

    #@186
    .line 6483
    :cond_186
    move/from16 v17, v16

    #@188
    .line 6488
    .restart local v17       #hintWidth:I
    :goto_188
    move/from16 v0, v17

    #@18a
    move/from16 v1, v22

    #@18c
    if-le v0, v1, :cond_190

    #@18e
    .line 6489
    move/from16 v22, v17

    #@190
    .line 6493
    .end local v16           #hintDes:I
    .end local v17           #hintWidth:I
    :cond_190
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@193
    move-result v2

    #@194
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@197
    move-result v7

    #@198
    add-int/2addr v2, v7

    #@199
    add-int v22, v22, v2

    #@19b
    .line 6495
    move-object/from16 v0, p0

    #@19d
    iget v2, v0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@19f
    const/4 v7, 0x1

    #@1a0
    if-ne v2, v7, :cond_1ef

    #@1a2
    .line 6496
    move-object/from16 v0, p0

    #@1a4
    iget v2, v0, Landroid/widget/TextView;->mMaxWidth:I

    #@1a6
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLineHeight()I

    #@1a9
    move-result v7

    #@1aa
    mul-int/2addr v2, v7

    #@1ab
    move/from16 v0, v22

    #@1ad
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    #@1b0
    move-result v22

    #@1b1
    .line 6501
    :goto_1b1
    move-object/from16 v0, p0

    #@1b3
    iget v2, v0, Landroid/widget/TextView;->mMinWidthMode:I

    #@1b5
    const/4 v7, 0x1

    #@1b6
    if-ne v2, v7, :cond_1fa

    #@1b8
    .line 6502
    move-object/from16 v0, p0

    #@1ba
    iget v2, v0, Landroid/widget/TextView;->mMinWidth:I

    #@1bc
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getLineHeight()I

    #@1bf
    move-result v7

    #@1c0
    mul-int/2addr v2, v7

    #@1c1
    move/from16 v0, v22

    #@1c3
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    #@1c6
    move-result v22

    #@1c7
    .line 6508
    :goto_1c7
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getSuggestedMinimumWidth()I

    #@1ca
    move-result v2

    #@1cb
    move/from16 v0, v22

    #@1cd
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    #@1d0
    move-result v22

    #@1d1
    .line 6510
    const/high16 v2, -0x8000

    #@1d3
    move/from16 v0, v24

    #@1d5
    if-ne v0, v2, :cond_27

    #@1d7
    .line 6511
    move/from16 v0, v25

    #@1d9
    move/from16 v1, v22

    #@1db
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@1de
    move-result v22

    #@1df
    goto/16 :goto_27

    #@1e1
    .line 6444
    .end local v11           #dr:Landroid/widget/TextView$Drawables;
    .end local v22           #width:I
    :cond_1e1
    const/4 v12, 0x1

    #@1e2
    goto/16 :goto_fe

    #@1e4
    .line 6453
    :cond_1e4
    iget v0, v5, Landroid/text/BoringLayout$Metrics;->width:I

    #@1e6
    move/from16 v22, v0

    #@1e8
    .restart local v22       #width:I
    goto/16 :goto_119

    #@1ea
    .line 6485
    .restart local v11       #dr:Landroid/widget/TextView$Drawables;
    .restart local v16       #hintDes:I
    :cond_1ea
    iget v0, v6, Landroid/text/BoringLayout$Metrics;->width:I

    #@1ec
    move/from16 v17, v0

    #@1ee
    .restart local v17       #hintWidth:I
    goto :goto_188

    #@1ef
    .line 6498
    .end local v16           #hintDes:I
    .end local v17           #hintWidth:I
    :cond_1ef
    move-object/from16 v0, p0

    #@1f1
    iget v2, v0, Landroid/widget/TextView;->mMaxWidth:I

    #@1f3
    move/from16 v0, v22

    #@1f5
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    #@1f8
    move-result v22

    #@1f9
    goto :goto_1b1

    #@1fa
    .line 6504
    :cond_1fa
    move-object/from16 v0, p0

    #@1fc
    iget v2, v0, Landroid/widget/TextView;->mMinWidth:I

    #@1fe
    move/from16 v0, v22

    #@200
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    #@203
    move-result v22

    #@204
    goto :goto_1c7

    #@205
    .line 6519
    .end local v11           #dr:Landroid/widget/TextView$Drawables;
    .restart local v3       #want:I
    .restart local v21       #unpaddedWidth:I
    :cond_205
    const/high16 v3, 0x1

    #@207
    goto/16 :goto_43

    #@209
    .line 6523
    .restart local v4       #hintWant:I
    :cond_209
    move-object/from16 v0, p0

    #@20b
    iget-object v2, v0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@20d
    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    #@210
    move-result v17

    #@211
    goto/16 :goto_4c

    #@213
    .line 6529
    .restart local v17       #hintWidth:I
    :cond_213
    move-object/from16 v0, p0

    #@215
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@217
    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    #@21a
    move-result v2

    #@21b
    if-ne v2, v3, :cond_236

    #@21d
    move/from16 v0, v17

    #@21f
    if-ne v0, v4, :cond_236

    #@221
    move-object/from16 v0, p0

    #@223
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@225
    invoke-virtual {v2}, Landroid/text/Layout;->getEllipsizedWidth()I

    #@228
    move-result v2

    #@229
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@22c
    move-result v7

    #@22d
    sub-int v7, v22, v7

    #@22f
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@232
    move-result v8

    #@233
    sub-int/2addr v7, v8

    #@234
    if-eq v2, v7, :cond_285

    #@236
    :cond_236
    const/16 v18, 0x1

    #@238
    .line 6534
    .local v18, layoutChanged:Z
    :goto_238
    move-object/from16 v0, p0

    #@23a
    iget-object v2, v0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@23c
    if-nez v2, :cond_288

    #@23e
    move-object/from16 v0, p0

    #@240
    iget-object v2, v0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@242
    if-nez v2, :cond_288

    #@244
    move-object/from16 v0, p0

    #@246
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@248
    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    #@24b
    move-result v2

    #@24c
    if-le v3, v2, :cond_288

    #@24e
    move-object/from16 v0, p0

    #@250
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@252
    instance-of v2, v2, Landroid/text/BoringLayout;

    #@254
    if-nez v2, :cond_25c

    #@256
    if-eqz v12, :cond_288

    #@258
    if-ltz v9, :cond_288

    #@25a
    if-gt v9, v3, :cond_288

    #@25c
    :cond_25c
    const/16 v23, 0x1

    #@25e
    .line 6539
    .local v23, widthChanged:Z
    :goto_25e
    move-object/from16 v0, p0

    #@260
    iget v2, v0, Landroid/widget/TextView;->mMaxMode:I

    #@262
    move-object/from16 v0, p0

    #@264
    iget v7, v0, Landroid/widget/TextView;->mOldMaxMode:I

    #@266
    if-ne v2, v7, :cond_272

    #@268
    move-object/from16 v0, p0

    #@26a
    iget v2, v0, Landroid/widget/TextView;->mMaximum:I

    #@26c
    move-object/from16 v0, p0

    #@26e
    iget v7, v0, Landroid/widget/TextView;->mOldMaximum:I

    #@270
    if-eq v2, v7, :cond_28b

    #@272
    :cond_272
    const/16 v19, 0x1

    #@274
    .line 6541
    .local v19, maximumChanged:Z
    :goto_274
    if-nez v18, :cond_278

    #@276
    if-eqz v19, :cond_64

    #@278
    .line 6542
    :cond_278
    if-nez v19, :cond_28e

    #@27a
    if-eqz v23, :cond_28e

    #@27c
    .line 6543
    move-object/from16 v0, p0

    #@27e
    iget-object v2, v0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@280
    invoke-virtual {v2, v3}, Landroid/text/Layout;->increaseWidthTo(I)V

    #@283
    goto/16 :goto_64

    #@285
    .line 6529
    .end local v18           #layoutChanged:Z
    .end local v19           #maximumChanged:Z
    .end local v23           #widthChanged:Z
    :cond_285
    const/16 v18, 0x0

    #@287
    goto :goto_238

    #@288
    .line 6534
    .restart local v18       #layoutChanged:Z
    :cond_288
    const/16 v23, 0x0

    #@28a
    goto :goto_25e

    #@28b
    .line 6539
    .restart local v23       #widthChanged:Z
    :cond_28b
    const/16 v19, 0x0

    #@28d
    goto :goto_274

    #@28e
    .line 6545
    .restart local v19       #maximumChanged:Z
    :cond_28e
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@291
    move-result v2

    #@292
    sub-int v2, v22, v2

    #@294
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@297
    move-result v7

    #@298
    sub-int v7, v2, v7

    #@29a
    const/4 v8, 0x0

    #@29b
    move-object/from16 v2, p0

    #@29d
    invoke-virtual/range {v2 .. v8}, Landroid/widget/TextView;->makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    #@2a0
    goto/16 :goto_64

    #@2a2
    .line 6558
    .end local v18           #layoutChanged:Z
    .end local v19           #maximumChanged:Z
    .end local v23           #widthChanged:Z
    :cond_2a2
    invoke-direct/range {p0 .. p0}, Landroid/widget/TextView;->getDesiredHeight()I

    #@2a5
    move-result v10

    #@2a6
    .line 6560
    .local v10, desired:I
    move v13, v10

    #@2a7
    .line 6561
    .restart local v13       #height:I
    move-object/from16 v0, p0

    #@2a9
    iput v10, v0, Landroid/widget/TextView;->mDesiredHeightAtMeasure:I

    #@2ab
    .line 6563
    const/high16 v2, -0x8000

    #@2ad
    if-ne v14, v2, :cond_6e

    #@2af
    .line 6564
    invoke-static {v10, v15}, Ljava/lang/Math;->min(II)I

    #@2b2
    move-result v13

    #@2b3
    goto/16 :goto_6e

    #@2b5
    .line 6582
    .end local v10           #desired:I
    .restart local v20       #unpaddedHeight:I
    :cond_2b5
    const/4 v2, 0x0

    #@2b6
    const/4 v7, 0x0

    #@2b7
    move-object/from16 v0, p0

    #@2b9
    invoke-virtual {v0, v2, v7}, Landroid/widget/TextView;->scrollTo(II)V

    #@2bc
    goto/16 :goto_c2
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 8237
    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 8239
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@6
    move-result v0

    #@7
    .line 8240
    .local v0, isPassword:Z
    if-eqz v0, :cond_f

    #@9
    invoke-direct {p0}, Landroid/widget/TextView;->shouldSpeakPasswordsForAccessibility()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_20

    #@f
    .line 8241
    :cond_f
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextForAccessibility()Ljava/lang/CharSequence;

    #@12
    move-result-object v1

    #@13
    .line 8242
    .local v1, text:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_20

    #@19
    .line 8243
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@1c
    move-result-object v2

    #@1d
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@20
    .line 8246
    .end local v1           #text:Ljava/lang/CharSequence;
    :cond_20
    return-void
.end method

.method public onPreDraw()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4769
    iget-object v3, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@3
    if-nez v3, :cond_8

    #@5
    .line 4770
    invoke-direct {p0}, Landroid/widget/TextView;->assumeLayout()V

    #@8
    .line 4773
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 4775
    .local v0, changed:Z
    iget-object v3, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@b
    if-eqz v3, :cond_ba

    #@d
    .line 4780
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@10
    move-result v1

    #@11
    .line 4782
    .local v1, curs:I
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@13
    if-eqz v3, :cond_29

    #@15
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@17
    iget-object v3, v3, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@19
    if-eqz v3, :cond_29

    #@1b
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1d
    iget-object v3, v3, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@1f
    invoke-virtual {v3}, Landroid/widget/Editor$SelectionModifierCursorController;->isSelectionStartDragged()Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_29

    #@25
    .line 4784
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@28
    move-result v1

    #@29
    .line 4792
    :cond_29
    if-gez v1, :cond_39

    #@2b
    iget v3, p0, Landroid/widget/TextView;->mGravity:I

    #@2d
    and-int/lit8 v3, v3, 0x70

    #@2f
    const/16 v4, 0x50

    #@31
    if-ne v3, v4, :cond_39

    #@33
    .line 4793
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@35
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    #@38
    move-result v1

    #@39
    .line 4796
    :cond_39
    if-ltz v1, :cond_3f

    #@3b
    .line 4797
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->bringPointIntoView(I)Z

    #@3e
    move-result v0

    #@3f
    .line 4806
    .end local v1           #curs:I
    :cond_3f
    :goto_3f
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@41
    if-eqz v3, :cond_58

    #@43
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@45
    iget-boolean v3, v3, Landroid/widget/Editor;->mCreatedWithASelection:Z

    #@47
    if-eqz v3, :cond_58

    #@49
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@4c
    move-result v3

    #@4d
    if-eqz v3, :cond_58

    #@4f
    .line 4807
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@51
    invoke-virtual {v3}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@54
    .line 4808
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@56
    iput-boolean v2, v3, Landroid/widget/Editor;->mCreatedWithASelection:Z

    #@58
    .line 4814
    :cond_58
    instance-of v3, p0, Landroid/inputmethodservice/ExtractEditText;

    #@5a
    if-eqz v3, :cond_6b

    #@5c
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@5f
    move-result v3

    #@60
    if-eqz v3, :cond_6b

    #@62
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@64
    if-eqz v3, :cond_6b

    #@66
    .line 4815
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@68
    invoke-virtual {v3}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@6b
    .line 4818
    :cond_6b
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@6d
    if-eqz v3, :cond_92

    #@6f
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@71
    if-eqz v3, :cond_92

    #@73
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@75
    invoke-virtual {v3}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@78
    move-result v3

    #@79
    if-eqz v3, :cond_92

    #@7b
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7d
    invoke-virtual {v3}, Landroid/widget/Editor;->isSelectionDragging()Z

    #@80
    move-result v3

    #@81
    if-nez v3, :cond_92

    #@83
    .line 4819
    invoke-virtual {p0}, Landroid/widget/TextView;->hasSelection()Z

    #@86
    move-result v3

    #@87
    if-nez v3, :cond_92

    #@89
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@8c
    move-result v3

    #@8d
    if-eqz v3, :cond_92

    #@8f
    .line 4820
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@92
    .line 4824
    :cond_92
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@94
    if-eqz v3, :cond_ad

    #@96
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@98
    if-eqz v3, :cond_ad

    #@9a
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9c
    iget-boolean v3, v3, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@9e
    if-nez v3, :cond_ad

    #@a0
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a2
    invoke-virtual {v3}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@a5
    move-result v3

    #@a6
    if-eqz v3, :cond_ad

    #@a8
    .line 4825
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@aa
    invoke-virtual {v3}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@ad
    .line 4827
    :cond_ad
    invoke-virtual {p0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@b0
    move-result-object v3

    #@b1
    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@b4
    .line 4828
    iput-boolean v2, p0, Landroid/widget/TextView;->mPreDrawRegistered:Z

    #@b6
    .line 4830
    if-nez v0, :cond_b9

    #@b8
    const/4 v2, 0x1

    #@b9
    :cond_b9
    return v2

    #@ba
    .line 4800
    :cond_ba
    invoke-direct {p0}, Landroid/widget/TextView;->bringTextIntoView()Z

    #@bd
    move-result v0

    #@be
    goto :goto_3f
.end method

.method public onPrivateIMECommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 6006
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onResolveDrawables(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 8923
    iget v0, p0, Landroid/widget/TextView;->mLastLayoutDirection:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 8932
    :cond_4
    :goto_4
    return-void

    #@5
    .line 8926
    :cond_5
    iput p1, p0, Landroid/widget/TextView;->mLastLayoutDirection:I

    #@7
    .line 8929
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@9
    if-eqz v0, :cond_4

    #@b
    .line 8930
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@d
    invoke-virtual {v0, p1}, Landroid/widget/TextView$Drawables;->resolveWithLayoutDirection(I)V

    #@10
    goto :goto_4
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 9
    .parameter "state"

    #@0
    .prologue
    .line 3596
    instance-of v4, p1, Landroid/widget/TextView$SavedState;

    #@2
    if-nez v4, :cond_8

    #@4
    .line 3597
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@7
    .line 3646
    :cond_7
    :goto_7
    return-void

    #@8
    :cond_8
    move-object v3, p1

    #@9
    .line 3601
    check-cast v3, Landroid/widget/TextView$SavedState;

    #@b
    .line 3602
    .local v3, ss:Landroid/widget/TextView$SavedState;
    invoke-virtual {v3}, Landroid/widget/TextView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@e
    move-result-object v4

    #@f
    invoke-super {p0, v4}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@12
    .line 3605
    iget-object v4, v3, Landroid/widget/TextView$SavedState;->text:Ljava/lang/CharSequence;

    #@14
    if-eqz v4, :cond_1b

    #@16
    .line 3606
    iget-object v4, v3, Landroid/widget/TextView$SavedState;->text:Ljava/lang/CharSequence;

    #@18
    invoke-virtual {p0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1b
    .line 3609
    :cond_1b
    iget v4, v3, Landroid/widget/TextView$SavedState;->selStart:I

    #@1d
    if-ltz v4, :cond_7c

    #@1f
    iget v4, v3, Landroid/widget/TextView$SavedState;->selEnd:I

    #@21
    if-ltz v4, :cond_7c

    #@23
    .line 3610
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@25
    instance-of v4, v4, Landroid/text/Spannable;

    #@27
    if-eqz v4, :cond_7c

    #@29
    .line 3611
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2b
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@2e
    move-result v1

    #@2f
    .line 3613
    .local v1, len:I
    iget v4, v3, Landroid/widget/TextView$SavedState;->selStart:I

    #@31
    if-gt v4, v1, :cond_37

    #@33
    iget v4, v3, Landroid/widget/TextView$SavedState;->selEnd:I

    #@35
    if-le v4, v1, :cond_9b

    #@37
    .line 3614
    :cond_37
    const-string v2, ""

    #@39
    .line 3616
    .local v2, restored:Ljava/lang/String;
    iget-object v4, v3, Landroid/widget/TextView$SavedState;->text:Ljava/lang/CharSequence;

    #@3b
    if-eqz v4, :cond_3f

    #@3d
    .line 3617
    const-string v2, "(restored) "

    #@3f
    .line 3620
    :cond_3f
    const-string v4, "TextView"

    #@41
    new-instance v5, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v6, "Saved cursor position "

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    iget v6, v3, Landroid/widget/TextView$SavedState;->selStart:I

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    const-string v6, "/"

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    iget v6, v3, Landroid/widget/TextView$SavedState;->selEnd:I

    #@5a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    const-string v6, " out of range for "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    const-string/jumbo v6, "text "

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    iget-object v6, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 3634
    .end local v1           #len:I
    .end local v2           #restored:Ljava/lang/String;
    :cond_7c
    :goto_7c
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@7e
    if-eqz v4, :cond_8b

    #@80
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@82
    if-eqz v4, :cond_8b

    #@84
    .line 3635
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@86
    iget-boolean v5, v3, Landroid/widget/TextView$SavedState;->isShowingBubblePopup:Z

    #@88
    invoke-virtual {v4, v5}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@8b
    .line 3637
    :cond_8b
    iget-object v4, v3, Landroid/widget/TextView$SavedState;->error:Ljava/lang/CharSequence;

    #@8d
    if-eqz v4, :cond_7

    #@8f
    .line 3638
    iget-object v0, v3, Landroid/widget/TextView$SavedState;->error:Ljava/lang/CharSequence;

    #@91
    .line 3640
    .local v0, error:Ljava/lang/CharSequence;
    new-instance v4, Landroid/widget/TextView$1;

    #@93
    invoke-direct {v4, p0, v0}, Landroid/widget/TextView$1;-><init>(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    #@96
    invoke-virtual {p0, v4}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    #@99
    goto/16 :goto_7

    #@9b
    .line 3624
    .end local v0           #error:Ljava/lang/CharSequence;
    .restart local v1       #len:I
    :cond_9b
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@9d
    check-cast v4, Landroid/text/Spannable;

    #@9f
    iget v5, v3, Landroid/widget/TextView$SavedState;->selStart:I

    #@a1
    iget v6, v3, Landroid/widget/TextView$SavedState;->selEnd:I

    #@a3
    invoke-static {v4, v5, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@a6
    .line 3626
    iget-boolean v4, v3, Landroid/widget/TextView$SavedState;->frozenWithFocus:Z

    #@a8
    if-eqz v4, :cond_7c

    #@aa
    .line 3627
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@ad
    .line 3628
    iget-object v4, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@af
    const/4 v5, 0x1

    #@b0
    iput-boolean v5, v4, Landroid/widget/Editor;->mFrozenWithFocus:Z

    #@b2
    goto :goto_7c
.end method

.method public onRtlPropertiesChanged(I)V
    .registers 4
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 6050
    iget-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 6051
    iget v0, p0, Landroid/widget/TextView;->mResolvedTextAlignment:I

    #@6
    const/4 v1, 0x5

    #@7
    if-eq v0, v1, :cond_e

    #@9
    iget v0, p0, Landroid/widget/TextView;->mResolvedTextAlignment:I

    #@b
    const/4 v1, 0x6

    #@c
    if-ne v0, v1, :cond_11

    #@e
    .line 6053
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@11
    .line 6056
    :cond_11
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 14

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 3521
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    #@4
    move-result-object v9

    #@5
    .line 3524
    .local v9, superState:Landroid/os/Parcelable;
    iget-boolean v5, p0, Landroid/widget/TextView;->mFreezesText:Z

    #@7
    .line 3525
    .local v5, save:Z
    const/4 v8, 0x0

    #@8
    .line 3526
    .local v8, start:I
    const/4 v2, 0x0

    #@9
    .line 3528
    .local v2, end:I
    iget-object v10, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@b
    if-eqz v10, :cond_1a

    #@d
    .line 3529
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@10
    move-result v8

    #@11
    .line 3530
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@14
    move-result v2

    #@15
    .line 3531
    if-gez v8, :cond_19

    #@17
    if-ltz v2, :cond_1a

    #@19
    .line 3533
    :cond_19
    const/4 v5, 0x1

    #@1a
    .line 3537
    :cond_1a
    if-eqz v5, :cond_89

    #@1c
    .line 3538
    new-instance v7, Landroid/widget/TextView$SavedState;

    #@1e
    invoke-direct {v7, v9}, Landroid/widget/TextView$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@21
    .line 3539
    .local v7, ss:Landroid/widget/TextView$SavedState;
    sget-boolean v10, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@23
    if-eqz v10, :cond_31

    #@25
    .line 3540
    iget-object v10, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@27
    if-eqz v10, :cond_5a

    #@29
    iget-object v10, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2b
    invoke-virtual {v10}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@2e
    move-result v10

    #@2f
    :goto_2f
    iput-boolean v10, v7, Landroid/widget/TextView$SavedState;->isShowingBubblePopup:Z

    #@31
    .line 3542
    :cond_31
    iput v8, v7, Landroid/widget/TextView$SavedState;->selStart:I

    #@33
    .line 3543
    iput v2, v7, Landroid/widget/TextView$SavedState;->selEnd:I

    #@35
    .line 3545
    iget-object v10, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@37
    instance-of v10, v10, Landroid/text/Spanned;

    #@39
    if-eqz v10, :cond_80

    #@3b
    .line 3554
    new-instance v6, Landroid/text/SpannableString;

    #@3d
    iget-object v10, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3f
    invoke-direct {v6, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@42
    .line 3556
    .local v6, sp:Landroid/text/Spannable;
    invoke-interface {v6}, Landroid/text/Spannable;->length()I

    #@45
    move-result v10

    #@46
    const-class v12, Landroid/widget/TextView$ChangeWatcher;

    #@48
    invoke-interface {v6, v11, v10, v12}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, [Landroid/widget/TextView$ChangeWatcher;

    #@4e
    .local v0, arr$:[Landroid/widget/TextView$ChangeWatcher;
    array-length v4, v0

    #@4f
    .local v4, len$:I
    const/4 v3, 0x0

    #@50
    .local v3, i$:I
    :goto_50
    if-ge v3, v4, :cond_5c

    #@52
    aget-object v1, v0, v3

    #@54
    .line 3557
    .local v1, cw:Landroid/widget/TextView$ChangeWatcher;
    invoke-interface {v6, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@57
    .line 3556
    add-int/lit8 v3, v3, 0x1

    #@59
    goto :goto_50

    #@5a
    .end local v0           #arr$:[Landroid/widget/TextView$ChangeWatcher;
    .end local v1           #cw:Landroid/widget/TextView$ChangeWatcher;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v6           #sp:Landroid/text/Spannable;
    :cond_5a
    move v10, v11

    #@5b
    .line 3540
    goto :goto_2f

    #@5c
    .line 3560
    .restart local v0       #arr$:[Landroid/widget/TextView$ChangeWatcher;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    .restart local v6       #sp:Landroid/text/Spannable;
    :cond_5c
    iget-object v10, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5e
    if-eqz v10, :cond_6a

    #@60
    .line 3561
    invoke-virtual {p0, v6}, Landroid/widget/TextView;->removeMisspelledSpans(Landroid/text/Spannable;)V

    #@63
    .line 3562
    iget-object v10, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@65
    iget-object v10, v10, Landroid/widget/Editor;->mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

    #@67
    invoke-interface {v6, v10}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@6a
    .line 3565
    :cond_6a
    iput-object v6, v7, Landroid/widget/TextView$SavedState;->text:Ljava/lang/CharSequence;

    #@6c
    .line 3570
    .end local v0           #arr$:[Landroid/widget/TextView$ChangeWatcher;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v6           #sp:Landroid/text/Spannable;
    :goto_6c
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@6f
    move-result v10

    #@70
    if-eqz v10, :cond_79

    #@72
    if-ltz v8, :cond_79

    #@74
    if-ltz v2, :cond_79

    #@76
    .line 3571
    const/4 v10, 0x1

    #@77
    iput-boolean v10, v7, Landroid/widget/TextView$SavedState;->frozenWithFocus:Z

    #@79
    .line 3574
    :cond_79
    invoke-virtual {p0}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    #@7c
    move-result-object v10

    #@7d
    iput-object v10, v7, Landroid/widget/TextView$SavedState;->error:Ljava/lang/CharSequence;

    #@7f
    .line 3579
    .end local v7           #ss:Landroid/widget/TextView$SavedState;
    :goto_7f
    return-object v7

    #@80
    .line 3567
    .restart local v7       #ss:Landroid/widget/TextView$SavedState;
    :cond_80
    iget-object v10, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@82
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@85
    move-result-object v10

    #@86
    iput-object v10, v7, Landroid/widget/TextView$SavedState;->text:Ljava/lang/CharSequence;

    #@88
    goto :goto_6c

    #@89
    .end local v7           #ss:Landroid/widget/TextView$SavedState;
    :cond_89
    move-object v7, v9

    #@8a
    .line 3579
    goto :goto_7f
.end method

.method public onScreenStateChanged(I)V
    .registers 3
    .parameter "screenState"

    #@0
    .prologue
    .line 4858
    invoke-super {p0, p1}, Landroid/view/View;->onScreenStateChanged(I)V

    #@3
    .line 4859
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9
    invoke-virtual {v0, p1}, Landroid/widget/Editor;->onScreenStateChanged(I)V

    #@c
    .line 4860
    :cond_c
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .registers 6
    .parameter "horiz"
    .parameter "vert"
    .parameter "oldHoriz"
    .parameter "oldVert"

    #@0
    .prologue
    .line 8476
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onScrollChanged(IIII)V

    #@3
    .line 8477
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 8478
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9
    invoke-virtual {v0}, Landroid/widget/Editor;->onScrollChanged()V

    #@c
    .line 8480
    :cond_c
    return-void
.end method

.method protected onSelectionChanged(II)V
    .registers 4
    .parameter "selStart"
    .parameter "selEnd"

    #@0
    .prologue
    .line 7462
    const/16 v0, 0x2000

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    #@5
    .line 7463
    return-void
.end method

.method public onStartTemporaryDetach()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 7728
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    #@4
    .line 7731
    iget-boolean v0, p0, Landroid/widget/TextView;->mDispatchTemporaryDetach:Z

    #@6
    if-nez v0, :cond_a

    #@8
    iput-boolean v1, p0, Landroid/widget/TextView;->mTemporaryDetach:Z

    #@a
    .line 7734
    :cond_a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    if-eqz v0, :cond_13

    #@e
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@10
    invoke-virtual {v0}, Landroid/widget/Editor;->suggestionPopupWindowWhenDetached()V

    #@13
    .line 7737
    :cond_13
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@15
    if-eqz v0, :cond_1b

    #@17
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@19
    iput-boolean v1, v0, Landroid/widget/Editor;->mTemporaryDetach:Z

    #@1b
    .line 7738
    :cond_1b
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "text"
    .parameter "start"
    .parameter "lengthBefore"
    .parameter "lengthAfter"

    #@0
    .prologue
    .line 7452
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .registers 13
    .parameter "id"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v10, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 8358
    const/4 v4, 0x0

    #@4
    .line 8359
    .local v4, min:I
    iget-object v9, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@6
    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v3

    #@a
    .line 8361
    .local v3, max:I
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_28

    #@10
    .line 8362
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@13
    move-result v6

    #@14
    .line 8363
    .local v6, selStart:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@17
    move-result v5

    #@18
    .line 8365
    .local v5, selEnd:I
    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    #@1b
    move-result v9

    #@1c
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@1f
    move-result v4

    #@20
    .line 8366
    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    #@23
    move-result v9

    #@24
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@27
    move-result v3

    #@28
    .line 8369
    .end local v5           #selEnd:I
    .end local v6           #selStart:I
    :cond_28
    sparse-switch p1, :sswitch_data_110

    #@2b
    move v7, v8

    #@2c
    .line 8441
    :cond_2c
    :goto_2c
    return v7

    #@2d
    .line 8373
    :sswitch_2d
    invoke-virtual {p0}, Landroid/widget/TextView;->selectAllText()Z

    #@30
    goto :goto_2c

    #@31
    .line 8377
    :sswitch_31
    invoke-direct {p0, v4, v3}, Landroid/widget/TextView;->paste(II)V

    #@34
    goto :goto_2c

    #@35
    .line 8382
    :sswitch_35
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@38
    move-result-object v8

    #@39
    if-eqz v8, :cond_45

    #@3b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3e
    move-result-object v8

    #@3f
    invoke-interface {v8, v7}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@42
    move-result v8

    #@43
    if-nez v8, :cond_2c

    #@45
    .line 8388
    :cond_45
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@47
    if-eqz v8, :cond_77

    #@49
    .line 8389
    iget-object v8, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4b
    iget-object v8, v8, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@4d
    if-eqz v8, :cond_6b

    #@4f
    .line 8390
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@52
    move-result-wide v8

    #@53
    sput-wide v8, Landroid/widget/TextView;->LAST_CUT_OR_COPY_TIME:J

    #@55
    .line 8391
    iget-object v8, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@57
    iget-object v8, v8, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@59
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5c
    move-result-object v9

    #@5d
    invoke-interface {v9, v4, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@60
    move-result-object v9

    #@61
    invoke-interface {v8, v9}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->copyToCliptray(Ljava/lang/CharSequence;)V

    #@64
    .line 8398
    :goto_64
    invoke-virtual {p0, v4, v3}, Landroid/widget/TextView;->deleteText_internal(II)V

    #@67
    .line 8399
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@6a
    goto :goto_2c

    #@6b
    .line 8393
    :cond_6b
    invoke-virtual {p0, v4, v3}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@6e
    move-result-object v8

    #@6f
    invoke-static {v10, v8}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@72
    move-result-object v8

    #@73
    invoke-direct {p0, v8}, Landroid/widget/TextView;->setPrimaryClip(Landroid/content/ClipData;)V

    #@76
    goto :goto_64

    #@77
    .line 8396
    :cond_77
    invoke-virtual {p0, v4, v3}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@7a
    move-result-object v8

    #@7b
    invoke-static {v10, v8}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@7e
    move-result-object v8

    #@7f
    invoke-direct {p0, v8}, Landroid/widget/TextView;->setPrimaryClip(Landroid/content/ClipData;)V

    #@82
    goto :goto_64

    #@83
    .line 8404
    :sswitch_83
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@86
    move-result-object v8

    #@87
    if-eqz v8, :cond_93

    #@89
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@8c
    move-result-object v8

    #@8d
    invoke-interface {v8, v7}, Lcom/lge/cappuccino/IMdm;->checkDisabledClipboard(Z)Z

    #@90
    move-result v8

    #@91
    if-nez v8, :cond_2c

    #@93
    .line 8410
    :cond_93
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@95
    if-eqz v8, :cond_c3

    #@97
    .line 8411
    iget-object v8, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@99
    iget-object v8, v8, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@9b
    if-eqz v8, :cond_b7

    #@9d
    .line 8412
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a0
    move-result-wide v8

    #@a1
    sput-wide v8, Landroid/widget/TextView;->LAST_CUT_OR_COPY_TIME:J

    #@a3
    .line 8413
    iget-object v8, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a5
    iget-object v8, v8, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@a7
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@aa
    move-result-object v9

    #@ab
    invoke-interface {v9, v4, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@ae
    move-result-object v9

    #@af
    invoke-interface {v8, v9}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->copyToCliptray(Ljava/lang/CharSequence;)V

    #@b2
    .line 8420
    :goto_b2
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@b5
    goto/16 :goto_2c

    #@b7
    .line 8415
    :cond_b7
    invoke-virtual {p0, v4, v3}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@ba
    move-result-object v8

    #@bb
    invoke-static {v10, v8}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@be
    move-result-object v8

    #@bf
    invoke-direct {p0, v8}, Landroid/widget/TextView;->setPrimaryClip(Landroid/content/ClipData;)V

    #@c2
    goto :goto_b2

    #@c3
    .line 8418
    :cond_c3
    invoke-virtual {p0, v4, v3}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@c6
    move-result-object v8

    #@c7
    invoke-static {v10, v8}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@ca
    move-result-object v8

    #@cb
    invoke-direct {p0, v8}, Landroid/widget/TextView;->setPrimaryClip(Landroid/content/ClipData;)V

    #@ce
    goto :goto_b2

    #@cf
    .line 8426
    :sswitch_cf
    :try_start_cf
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@d2
    move-result-object v0

    #@d3
    .line 8427
    .local v0, context:Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    #@d5
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@d8
    .line 8428
    .local v2, intent:Landroid/content/Intent;
    const-string v8, "com.lge.smarttext.action.SEND"

    #@da
    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@dd
    .line 8429
    const-string/jumbo v8, "text/plain"

    #@e0
    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@e3
    .line 8430
    const-string v8, "calling_package"

    #@e5
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@e8
    move-result-object v9

    #@e9
    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ec
    .line 8431
    const-string v8, "android.intent.extra.TEXT"

    #@ee
    invoke-virtual {p0, v4, v3}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@f1
    move-result-object v9

    #@f2
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@f5
    move-result-object v9

    #@f6
    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@f9
    .line 8432
    const/high16 v8, 0x1000

    #@fb
    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@fe
    .line 8433
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_101
    .catch Landroid/content/ActivityNotFoundException; {:try_start_cf .. :try_end_101} :catch_106

    #@101
    .line 8437
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #intent:Landroid/content/Intent;
    :goto_101
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@104
    goto/16 :goto_2c

    #@106
    .line 8434
    :catch_106
    move-exception v1

    #@107
    .line 8435
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v8, "com.lge.smarttext"

    #@109
    const-string v9, "Text link application is not installed"

    #@10b
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    goto :goto_101

    #@10f
    .line 8369
    nop

    #@110
    :sswitch_data_110
    .sparse-switch
        0x102001f -> :sswitch_2d
        0x1020020 -> :sswitch_35
        0x1020021 -> :sswitch_83
        0x1020022 -> :sswitch_31
        0x20d003e -> :sswitch_cf
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "event"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 7825
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@5
    move-result v0

    #@6
    .line 7827
    .local v0, action:I
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    if-eqz v7, :cond_f

    #@a
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    invoke-virtual {v7, p1}, Landroid/widget/Editor;->onTouchEvent(Landroid/view/MotionEvent;)V

    #@f
    .line 7829
    :cond_f
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v4

    #@13
    .line 7836
    .local v4, superResult:Z
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@15
    if-eqz v7, :cond_24

    #@17
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@19
    iget-boolean v7, v7, Landroid/widget/Editor;->mDiscardNextActionUp:Z

    #@1b
    if-eqz v7, :cond_24

    #@1d
    if-ne v0, v8, :cond_24

    #@1f
    .line 7837
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@21
    iput-boolean v9, v7, Landroid/widget/Editor;->mDiscardNextActionUp:Z

    #@23
    .line 7891
    .end local v4           #superResult:Z
    :cond_23
    :goto_23
    return v4

    #@24
    .line 7842
    .restart local v4       #superResult:Z
    :cond_24
    iget-boolean v7, p0, Landroid/widget/TextView;->mDiscardNextActionUp:Z

    #@26
    if-eqz v7, :cond_2d

    #@28
    if-ne v0, v8, :cond_2d

    #@2a
    .line 7843
    iput-boolean v9, p0, Landroid/widget/TextView;->mDiscardNextActionUp:Z

    #@2c
    goto :goto_23

    #@2d
    .line 7847
    :cond_2d
    if-ne v0, v8, :cond_c5

    #@2f
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@31
    if-eqz v7, :cond_39

    #@33
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@35
    iget-boolean v7, v7, Landroid/widget/Editor;->mIgnoreActionUpEvent:Z

    #@37
    if-nez v7, :cond_c5

    #@39
    :cond_39
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@3c
    move-result v7

    #@3d
    if-eqz v7, :cond_c5

    #@3f
    move v6, v8

    #@40
    .line 7850
    .local v6, touchIsFinished:Z
    :goto_40
    iget-object v7, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@42
    if-nez v7, :cond_4a

    #@44
    invoke-virtual {p0}, Landroid/widget/TextView;->onCheckIsTextEditor()Z

    #@47
    move-result v7

    #@48
    if-eqz v7, :cond_23

    #@4a
    :cond_4a
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@4d
    move-result v7

    #@4e
    if-eqz v7, :cond_23

    #@50
    iget-object v7, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@52
    instance-of v7, v7, Landroid/text/Spannable;

    #@54
    if-eqz v7, :cond_23

    #@56
    iget-object v7, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@58
    if-eqz v7, :cond_23

    #@5a
    .line 7852
    const/4 v1, 0x0

    #@5b
    .line 7854
    .local v1, handled:Z
    iget-object v7, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@5d
    if-eqz v7, :cond_6a

    #@5f
    .line 7855
    iget-object v10, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@61
    iget-object v7, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@63
    check-cast v7, Landroid/text/Spannable;

    #@65
    invoke-interface {v10, p0, v7, p1}, Landroid/text/method/MovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    #@68
    move-result v7

    #@69
    or-int/2addr v1, v7

    #@6a
    .line 7858
    :cond_6a
    invoke-virtual {p0}, Landroid/widget/TextView;->isTextSelectable()Z

    #@6d
    move-result v5

    #@6e
    .line 7859
    .local v5, textIsSelectable:Z
    if-eqz v6, :cond_97

    #@70
    iget-boolean v7, p0, Landroid/widget/TextView;->mLinksClickable:Z

    #@72
    if-eqz v7, :cond_97

    #@74
    iget v7, p0, Landroid/widget/TextView;->mAutoLinkMask:I

    #@76
    if-eqz v7, :cond_97

    #@78
    if-eqz v5, :cond_97

    #@7a
    .line 7863
    iget-object v7, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@7c
    check-cast v7, Landroid/text/Spannable;

    #@7e
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@81
    move-result v10

    #@82
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@85
    move-result v11

    #@86
    const-class v12, Landroid/text/style/ClickableSpan;

    #@88
    invoke-interface {v7, v10, v11, v12}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@8b
    move-result-object v3

    #@8c
    check-cast v3, [Landroid/text/style/ClickableSpan;

    #@8e
    .line 7866
    .local v3, links:[Landroid/text/style/ClickableSpan;
    array-length v7, v3

    #@8f
    if-lez v7, :cond_97

    #@91
    .line 7867
    aget-object v7, v3, v9

    #@93
    invoke-virtual {v7, p0}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    #@96
    .line 7868
    const/4 v1, 0x1

    #@97
    .line 7872
    .end local v3           #links:[Landroid/text/style/ClickableSpan;
    :cond_97
    if-eqz v6, :cond_c0

    #@99
    invoke-virtual {p0}, Landroid/widget/TextView;->isTextEditable()Z

    #@9c
    move-result v7

    #@9d
    if-nez v7, :cond_a1

    #@9f
    if-eqz v5, :cond_c0

    #@a1
    .line 7874
    :cond_a1
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@a4
    move-result-object v2

    #@a5
    .line 7875
    .local v2, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->viewClicked(Landroid/view/inputmethod/InputMethodManager;)V

    #@a8
    .line 7876
    if-nez v5, :cond_ba

    #@aa
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@ac
    iget-boolean v7, v7, Landroid/widget/Editor;->mShowSoftInputOnFocus:Z

    #@ae
    if-eqz v7, :cond_ba

    #@b0
    .line 7877
    if-eqz v2, :cond_b9

    #@b2
    invoke-virtual {v2, p0, v9}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@b5
    move-result v7

    #@b6
    if-eqz v7, :cond_b9

    #@b8
    move v9, v8

    #@b9
    :cond_b9
    or-int/2addr v1, v9

    #@ba
    .line 7881
    :cond_ba
    iget-object v7, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@bc
    invoke-virtual {v7, p1}, Landroid/widget/Editor;->onTouchUpEvent(Landroid/view/MotionEvent;)V

    #@bf
    .line 7883
    const/4 v1, 0x1

    #@c0
    .line 7886
    .end local v2           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_c0
    if-eqz v1, :cond_23

    #@c2
    move v4, v8

    #@c3
    .line 7887
    goto/16 :goto_23

    #@c5
    .end local v1           #handled:Z
    .end local v5           #textIsSelectable:Z
    .end local v6           #touchIsFinished:Z
    :cond_c5
    move v6, v9

    #@c6
    .line 7847
    goto/16 :goto_40
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 7936
    iget-object v0, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@2
    if-eqz v0, :cond_1c

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@6
    instance-of v0, v0, Landroid/text/Spannable;

    #@8
    if-eqz v0, :cond_1c

    #@a
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@c
    if-eqz v0, :cond_1c

    #@e
    .line 7937
    iget-object v1, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@10
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@12
    check-cast v0, Landroid/text/Spannable;

    #@14
    invoke-interface {v1, p0, v0, p1}, Landroid/text/method/MovementMethod;->onTrackballEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    .line 7938
    const/4 v0, 0x1

    #@1b
    .line 7942
    :goto_1b
    return v0

    #@1c
    :cond_1c
    invoke-super {p0, p1}, Landroid/view/View;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    #@1f
    move-result v0

    #@20
    goto :goto_1b
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 7790
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 7791
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    if-eqz v0, :cond_1d

    #@7
    if-eqz p2, :cond_1d

    #@9
    .line 7792
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@b
    invoke-virtual {v0}, Landroid/widget/Editor;->hideControllers()V

    #@e
    .line 7793
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@10
    if-eqz v0, :cond_1d

    #@12
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@14
    invoke-virtual {v0}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1d

    #@1a
    invoke-virtual {p0}, Landroid/widget/TextView;->stopSelectionActionMode()V

    #@1d
    .line 7795
    :cond_1d
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 3
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 7781
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    #@3
    .line 7783
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9
    invoke-virtual {v0, p1}, Landroid/widget/Editor;->onWindowFocusChanged(Z)V

    #@c
    .line 7785
    :cond_c
    invoke-direct {p0, p1}, Landroid/widget/TextView;->startStopMarquee(Z)V

    #@f
    .line 7786
    return-void
.end method

.method public performLongClick()Z
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 8450
    const/4 v0, 0x0

    #@2
    .line 8452
    .local v0, handled:Z
    invoke-super {p0}, Landroid/view/View;->performLongClick()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_9

    #@8
    .line 8453
    const/4 v0, 0x1

    #@9
    .line 8456
    :cond_9
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 8457
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@f
    invoke-virtual {v1, v0}, Landroid/widget/Editor;->performLongClick(Z)Z

    #@12
    move-result v1

    #@13
    or-int/2addr v0, v1

    #@14
    .line 8460
    :cond_14
    if-eqz v0, :cond_22

    #@16
    .line 8461
    const/4 v1, 0x0

    #@17
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->performHapticFeedback(I)Z

    #@1a
    .line 8464
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1c
    if-eqz v1, :cond_23

    #@1e
    .line 8465
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@20
    iput-boolean v2, v1, Landroid/widget/Editor;->mDiscardNextActionUp:Z

    #@22
    .line 8471
    :cond_22
    :goto_22
    return v0

    #@23
    .line 8467
    :cond_23
    iput-boolean v2, p0, Landroid/widget/TextView;->mDiscardNextActionUp:Z

    #@25
    goto :goto_22
.end method

.method prepareSpacesAroundPaste(IILjava/lang/CharSequence;)J
    .registers 11
    .parameter "min"
    .parameter "max"
    .parameter "paste"

    #@0
    .prologue
    const/16 v6, 0xa

    #@2
    .line 8618
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    #@5
    move-result v4

    #@6
    if-lez v4, :cond_61

    #@8
    .line 8619
    if-lez p1, :cond_38

    #@a
    .line 8620
    iget-object v4, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@c
    add-int/lit8 v5, p1, -0x1

    #@e
    invoke-interface {v4, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@11
    move-result v1

    #@12
    .line 8621
    .local v1, charBefore:C
    const/4 v4, 0x0

    #@13
    invoke-interface {p3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@16
    move-result v0

    #@17
    .line 8623
    .local v0, charAfter:C
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_66

    #@1d
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_66

    #@23
    .line 8625
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@25
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@28
    move-result v3

    #@29
    .line 8626
    .local v3, originalLength:I
    add-int/lit8 v4, p1, -0x1

    #@2b
    invoke-virtual {p0, v4, p1}, Landroid/widget/TextView;->deleteText_internal(II)V

    #@2e
    .line 8629
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@30
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@33
    move-result v4

    #@34
    sub-int v2, v4, v3

    #@36
    .line 8630
    .local v2, delta:I
    add-int/2addr p1, v2

    #@37
    .line 8631
    add-int/2addr p2, v2

    #@38
    .line 8644
    .end local v0           #charAfter:C
    .end local v1           #charBefore:C
    .end local v2           #delta:I
    .end local v3           #originalLength:I
    :cond_38
    :goto_38
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3a
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@3d
    move-result v4

    #@3e
    if-ge p2, v4, :cond_61

    #@40
    .line 8645
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    #@43
    move-result v4

    #@44
    add-int/lit8 v4, v4, -0x1

    #@46
    invoke-interface {p3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@49
    move-result v1

    #@4a
    .line 8646
    .restart local v1       #charBefore:C
    iget-object v4, p0, Landroid/widget/TextView;->mTransformed:Ljava/lang/CharSequence;

    #@4c
    invoke-interface {v4, p2}, Ljava/lang/CharSequence;->charAt(I)C

    #@4f
    move-result v0

    #@50
    .line 8648
    .restart local v0       #charAfter:C
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_8c

    #@56
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_8c

    #@5c
    .line 8650
    add-int/lit8 v4, p2, 0x1

    #@5e
    invoke-virtual {p0, p2, v4}, Landroid/widget/TextView;->deleteText_internal(II)V

    #@61
    .line 8659
    .end local v0           #charAfter:C
    .end local v1           #charBefore:C
    :cond_61
    :goto_61
    invoke-static {p1, p2}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@64
    move-result-wide v4

    #@65
    return-wide v4

    #@66
    .line 8632
    .restart local v0       #charAfter:C
    .restart local v1       #charBefore:C
    :cond_66
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@69
    move-result v4

    #@6a
    if-nez v4, :cond_38

    #@6c
    if-eq v1, v6, :cond_38

    #@6e
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@71
    move-result v4

    #@72
    if-nez v4, :cond_38

    #@74
    if-eq v0, v6, :cond_38

    #@76
    .line 8635
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@78
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@7b
    move-result v3

    #@7c
    .line 8636
    .restart local v3       #originalLength:I
    const-string v4, " "

    #@7e
    invoke-virtual {p0, p1, p1, v4}, Landroid/widget/TextView;->replaceText_internal(IILjava/lang/CharSequence;)V

    #@81
    .line 8638
    iget-object v4, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@83
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@86
    move-result v4

    #@87
    sub-int v2, v4, v3

    #@89
    .line 8639
    .restart local v2       #delta:I
    add-int/2addr p1, v2

    #@8a
    .line 8640
    add-int/2addr p2, v2

    #@8b
    goto :goto_38

    #@8c
    .line 8651
    .end local v2           #delta:I
    .end local v3           #originalLength:I
    :cond_8c
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@8f
    move-result v4

    #@90
    if-nez v4, :cond_61

    #@92
    if-eq v1, v6, :cond_61

    #@94
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@97
    move-result v4

    #@98
    if-nez v4, :cond_61

    #@9a
    if-eq v0, v6, :cond_61

    #@9c
    .line 8654
    const-string v4, " "

    #@9e
    invoke-virtual {p0, p2, p2, v4}, Landroid/widget/TextView;->replaceText_internal(IILjava/lang/CharSequence;)V

    #@a1
    goto :goto_61
.end method

.method removeMisspelledSpans(Landroid/text/Spannable;)V
    .registers 8
    .parameter "spannable"

    #@0
    .prologue
    .line 3583
    const/4 v3, 0x0

    #@1
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    #@4
    move-result v4

    #@5
    const-class v5, Landroid/text/style/SuggestionSpan;

    #@7
    invoke-interface {p1, v3, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, [Landroid/text/style/SuggestionSpan;

    #@d
    .line 3585
    .local v2, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    array-length v3, v2

    #@f
    if-ge v1, v3, :cond_27

    #@11
    .line 3586
    aget-object v3, v2, v1

    #@13
    invoke-virtual {v3}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@16
    move-result v0

    #@17
    .line 3587
    .local v0, flags:I
    and-int/lit8 v3, v0, 0x1

    #@19
    if-eqz v3, :cond_24

    #@1b
    and-int/lit8 v3, v0, 0x2

    #@1d
    if-eqz v3, :cond_24

    #@1f
    .line 3589
    aget-object v3, v2, v1

    #@21
    invoke-interface {p1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@24
    .line 3585
    :cond_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_e

    #@27
    .line 3592
    .end local v0           #flags:I
    :cond_27
    return-void
.end method

.method removeSuggestionSpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 8
    .parameter "text"

    #@0
    .prologue
    .line 4036
    instance-of v3, p1, Landroid/text/Spanned;

    #@2
    if-eqz v3, :cond_2b

    #@4
    .line 4038
    instance-of v3, p1, Landroid/text/Spannable;

    #@6
    if-eqz v3, :cond_24

    #@8
    move-object v1, p1

    #@9
    .line 4039
    check-cast v1, Landroid/text/Spannable;

    #@b
    .line 4045
    .local v1, spannable:Landroid/text/Spannable;
    :goto_b
    const/4 v3, 0x0

    #@c
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@f
    move-result v4

    #@10
    const-class v5, Landroid/text/style/SuggestionSpan;

    #@12
    invoke-interface {v1, v3, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, [Landroid/text/style/SuggestionSpan;

    #@18
    .line 4046
    .local v2, spans:[Landroid/text/style/SuggestionSpan;
    const/4 v0, 0x0

    #@19
    .local v0, i:I
    :goto_19
    array-length v3, v2

    #@1a
    if-ge v0, v3, :cond_2b

    #@1c
    .line 4047
    aget-object v3, v2, v0

    #@1e
    invoke-interface {v1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@21
    .line 4046
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_19

    #@24
    .line 4041
    .end local v0           #i:I
    .end local v1           #spannable:Landroid/text/Spannable;
    .end local v2           #spans:[Landroid/text/style/SuggestionSpan;
    :cond_24
    new-instance v1, Landroid/text/SpannableString;

    #@26
    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@29
    .line 4042
    .restart local v1       #spannable:Landroid/text/Spannable;
    move-object p1, v1

    #@2a
    goto :goto_b

    #@2b
    .line 4050
    .end local v1           #spannable:Landroid/text/Spannable;
    :cond_2b
    return-object p1
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .registers 4
    .parameter "watcher"

    #@0
    .prologue
    .line 7488
    iget-object v1, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v1, :cond_11

    #@4
    .line 7489
    iget-object v1, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@9
    move-result v0

    #@a
    .line 7491
    .local v0, i:I
    if-ltz v0, :cond_11

    #@c
    .line 7492
    iget-object v1, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@11
    .line 7495
    .end local v0           #i:I
    :cond_11
    return-void
.end method

.method protected replaceText_internal(IILjava/lang/CharSequence;)V
    .registers 5
    .parameter "start"
    .parameter "end"
    .parameter "text"

    #@0
    .prologue
    .line 8964
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    check-cast v0, Landroid/text/Editable;

    #@4
    invoke-interface {v0, p1, p2, p3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@7
    .line 8965
    return-void
.end method

.method public resetErrorChangedFlag()V
    .registers 3

    #@0
    .prologue
    .line 5688
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    const/4 v1, 0x0

    #@7
    iput-boolean v1, v0, Landroid/widget/Editor;->mErrorWasChanged:Z

    #@9
    .line 5689
    :cond_9
    return-void
.end method

.method protected resetResolvedDrawables()V
    .registers 2

    #@0
    .prologue
    .line 8938
    invoke-super {p0}, Landroid/view/View;->resetResolvedDrawables()V

    #@3
    .line 8939
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/widget/TextView;->mLastLayoutDirection:I

    #@6
    .line 8940
    return-void
.end method

.method selectAllText()Z
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 8606
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@3
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@6
    move-result v0

    #@7
    .line 8607
    .local v0, length:I
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@9
    check-cast v1, Landroid/text/Spannable;

    #@b
    invoke-static {v1, v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@e
    .line 8608
    if-lez v0, :cond_12

    #@10
    const/4 v1, 0x1

    #@11
    :goto_11
    return v1

    #@12
    :cond_12
    move v1, v2

    #@13
    goto :goto_11
.end method

.method public sendAccessibilityEvent(I)V
    .registers 3
    .parameter "eventType"

    #@0
    .prologue
    .line 8300
    const/16 v0, 0x1000

    #@2
    if-ne p1, v0, :cond_5

    #@4
    .line 8304
    :goto_4
    return-void

    #@5
    .line 8303
    :cond_5
    invoke-super {p0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@8
    goto :goto_4
.end method

.method sendAccessibilityEventTypeViewTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter "beforeText"
    .parameter "fromIndex"
    .parameter "removedCount"
    .parameter "addedCount"

    #@0
    .prologue
    .line 8323
    const/16 v1, 0x10

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@5
    move-result-object v0

    #@6
    .line 8325
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    #@9
    .line 8326
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityEvent;->setRemovedCount(I)V

    #@c
    .line 8327
    invoke-virtual {v0, p4}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    #@f
    .line 8328
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityEvent;->setBeforeText(Ljava/lang/CharSequence;)V

    #@12
    .line 8329
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    #@15
    .line 8330
    return-void
.end method

.method sendAfterTextChanged(Landroid/text/Editable;)V
    .registers 6
    .parameter "text"

    #@0
    .prologue
    .line 7549
    iget-object v3, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v3, :cond_19

    #@4
    .line 7550
    iget-object v2, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@6
    .line 7551
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/TextWatcher;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 7552
    .local v0, count:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_19

    #@d
    .line 7553
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/text/TextWatcher;

    #@13
    invoke-interface {v3, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    #@16
    .line 7552
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_b

    #@19
    .line 7556
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/TextWatcher;>;"
    :cond_19
    return-void
.end method

.method sendOnTextChanged(Ljava/lang/CharSequence;III)V
    .registers 9
    .parameter "text"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 7533
    iget-object v3, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v3, :cond_19

    #@4
    .line 7534
    iget-object v2, p0, Landroid/widget/TextView;->mListeners:Ljava/util/ArrayList;

    #@6
    .line 7535
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/TextWatcher;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 7536
    .local v0, count:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_19

    #@d
    .line 7537
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/text/TextWatcher;

    #@13
    invoke-interface {v3, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    #@16
    .line 7536
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_b

    #@19
    .line 7541
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/text/TextWatcher;>;"
    :cond_19
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1b
    if-eqz v3, :cond_22

    #@1d
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1f
    invoke-virtual {v3, p2, p4}, Landroid/widget/Editor;->sendOnTextChanged(II)V

    #@22
    .line 7542
    :cond_22
    return-void
.end method

.method public setAccessibilityCursorPosition(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 9080
    invoke-virtual {p0}, Landroid/widget/TextView;->getAccessibilityCursorPosition()I

    #@3
    move-result v0

    #@4
    if-ne v0, p1, :cond_7

    #@6
    .line 9092
    :goto_6
    return-void

    #@7
    .line 9083
    :cond_7
    invoke-virtual {p0}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_2b

    #@11
    .line 9084
    if-ltz p1, :cond_23

    #@13
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@15
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@18
    move-result v0

    #@19
    if-gt p1, v0, :cond_23

    #@1b
    .line 9085
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1d
    check-cast v0, Landroid/text/Spannable;

    #@1f
    invoke-static {v0, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@22
    goto :goto_6

    #@23
    .line 9087
    :cond_23
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@25
    check-cast v0, Landroid/text/Spannable;

    #@27
    invoke-static {v0}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    #@2a
    goto :goto_6

    #@2b
    .line 9090
    :cond_2b
    invoke-super {p0, p1}, Landroid/view/View;->setAccessibilityCursorPosition(I)V

    #@2e
    goto :goto_6
.end method

.method public setAllCaps(Z)V
    .registers 4
    .parameter "allCaps"

    #@0
    .prologue
    .line 7200
    if-eqz p1, :cond_f

    #@2
    .line 7201
    new-instance v0, Landroid/text/method/AllCapsTransformationMethod;

    #@4
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1}, Landroid/text/method/AllCapsTransformationMethod;-><init>(Landroid/content/Context;)V

    #@b
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@e
    .line 7205
    :goto_e
    return-void

    #@f
    .line 7203
    :cond_f
    const/4 v0, 0x0

    #@10
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@13
    goto :goto_e
.end method

.method public final setAutoLinkMask(I)V
    .registers 2
    .parameter "mask"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2838
    iput p1, p0, Landroid/widget/TextView;->mAutoLinkMask:I

    #@2
    .line 2839
    return-void
.end method

.method public setCompoundDrawablePadding(I)V
    .registers 3
    .parameter "pad"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2383
    iget-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2
    .line 2384
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-nez p1, :cond_f

    #@4
    .line 2385
    if-eqz v0, :cond_8

    #@6
    .line 2386
    iput p1, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@8
    .line 2395
    :cond_8
    :goto_8
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 2396
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@e
    .line 2397
    return-void

    #@f
    .line 2389
    :cond_f
    if-nez v0, :cond_18

    #@11
    .line 2390
    new-instance v0, Landroid/widget/TextView$Drawables;

    #@13
    .end local v0           #dr:Landroid/widget/TextView$Drawables;
    invoke-direct {v0}, Landroid/widget/TextView$Drawables;-><init>()V

    #@16
    .restart local v0       #dr:Landroid/widget/TextView$Drawables;
    iput-object v0, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@18
    .line 2392
    :cond_18
    iput p1, v0, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@1a
    goto :goto_8
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 2007
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@4
    .line 2009
    .local v1, dr:Landroid/widget/TextView$Drawables;
    if-nez p1, :cond_c

    #@6
    if-nez p2, :cond_c

    #@8
    if-nez p3, :cond_c

    #@a
    if-eqz p4, :cond_1e

    #@c
    :cond_c
    const/4 v2, 0x1

    #@d
    .line 2012
    .local v2, drawables:Z
    :goto_d
    if-nez v2, :cond_5d

    #@f
    .line 2014
    if-eqz v1, :cond_17

    #@11
    .line 2015
    iget v5, v1, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@13
    if-nez v5, :cond_20

    #@15
    .line 2016
    iput-object v6, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@17
    .line 2105
    :cond_17
    :goto_17
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1a
    .line 2106
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@1d
    .line 2107
    return-void

    #@1e
    .end local v2           #drawables:Z
    :cond_1e
    move v2, v4

    #@1f
    .line 2009
    goto :goto_d

    #@20
    .line 2020
    .restart local v2       #drawables:Z
    :cond_20
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@22
    if-eqz v5, :cond_29

    #@24
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@26
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@29
    .line 2021
    :cond_29
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@2b
    .line 2022
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@2d
    if-eqz v5, :cond_34

    #@2f
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@31
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@34
    .line 2023
    :cond_34
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@36
    .line 2024
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@38
    if-eqz v5, :cond_3f

    #@3a
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@3c
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@3f
    .line 2025
    :cond_3f
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@41
    .line 2026
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@43
    if-eqz v5, :cond_4a

    #@45
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@47
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@4a
    .line 2027
    :cond_4a
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@4c
    .line 2028
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@4e
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@50
    .line 2029
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@52
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@54
    .line 2030
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@56
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@58
    .line 2031
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@5a
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@5c
    goto :goto_17

    #@5d
    .line 2035
    :cond_5d
    if-nez v1, :cond_66

    #@5f
    .line 2036
    new-instance v1, Landroid/widget/TextView$Drawables;

    #@61
    .end local v1           #dr:Landroid/widget/TextView$Drawables;
    invoke-direct {v1}, Landroid/widget/TextView$Drawables;-><init>()V

    #@64
    .restart local v1       #dr:Landroid/widget/TextView$Drawables;
    iput-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@66
    .line 2039
    :cond_66
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@68
    if-eq v5, p1, :cond_73

    #@6a
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@6c
    if-eqz v5, :cond_73

    #@6e
    .line 2040
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@70
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@73
    .line 2042
    :cond_73
    iput-object p1, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@75
    .line 2044
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@77
    if-eq v5, p2, :cond_82

    #@79
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@7b
    if-eqz v5, :cond_82

    #@7d
    .line 2045
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@7f
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@82
    .line 2047
    :cond_82
    iput-object p2, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@84
    .line 2049
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@86
    if-eq v5, p3, :cond_91

    #@88
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@8a
    if-eqz v5, :cond_91

    #@8c
    .line 2050
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@8e
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@91
    .line 2052
    :cond_91
    iput-object p3, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@93
    .line 2054
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@95
    if-eq v5, p4, :cond_a0

    #@97
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@99
    if-eqz v5, :cond_a0

    #@9b
    .line 2055
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@9d
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@a0
    .line 2057
    :cond_a0
    iput-object p4, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@a2
    .line 2059
    iget-object v0, v1, Landroid/widget/TextView$Drawables;->mCompoundRect:Landroid/graphics/Rect;

    #@a4
    .line 2062
    .local v0, compoundRect:Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@a7
    move-result-object v3

    #@a8
    .line 2064
    .local v3, state:[I
    if-eqz p1, :cond_106

    #@aa
    .line 2065
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@ad
    .line 2066
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@b0
    .line 2067
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@b3
    .line 2068
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@b6
    move-result v5

    #@b7
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@b9
    .line 2069
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@bc
    move-result v5

    #@bd
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@bf
    .line 2074
    :goto_bf
    if-eqz p3, :cond_10b

    #@c1
    .line 2075
    invoke-virtual {p3, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@c4
    .line 2076
    invoke-virtual {p3, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@c7
    .line 2077
    invoke-virtual {p3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@ca
    .line 2078
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@cd
    move-result v5

    #@ce
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@d0
    .line 2079
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@d3
    move-result v5

    #@d4
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@d6
    .line 2084
    :goto_d6
    if-eqz p2, :cond_110

    #@d8
    .line 2085
    invoke-virtual {p2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@db
    .line 2086
    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@de
    .line 2087
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@e1
    .line 2088
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@e4
    move-result v5

    #@e5
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@e7
    .line 2089
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@ea
    move-result v5

    #@eb
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@ed
    .line 2094
    :goto_ed
    if-eqz p4, :cond_115

    #@ef
    .line 2095
    invoke-virtual {p4, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@f2
    .line 2096
    invoke-virtual {p4, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@f5
    .line 2097
    invoke-virtual {p4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@f8
    .line 2098
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@fb
    move-result v4

    #@fc
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@fe
    .line 2099
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@101
    move-result v4

    #@102
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@104
    goto/16 :goto_17

    #@106
    .line 2071
    :cond_106
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@108
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@10a
    goto :goto_bf

    #@10b
    .line 2081
    :cond_10b
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@10d
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@10f
    goto :goto_d6

    #@110
    .line 2091
    :cond_110
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@112
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@114
    goto :goto_ed

    #@115
    .line 2101
    :cond_115
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@117
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@119
    goto/16 :goto_17
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 12
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 2176
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@4
    .line 2178
    .local v1, dr:Landroid/widget/TextView$Drawables;
    if-nez p1, :cond_c

    #@6
    if-nez p2, :cond_c

    #@8
    if-nez p3, :cond_c

    #@a
    if-eqz p4, :cond_21

    #@c
    :cond_c
    const/4 v2, 0x1

    #@d
    .line 2181
    .local v2, drawables:Z
    :goto_d
    if-nez v2, :cond_60

    #@f
    .line 2183
    if-eqz v1, :cond_17

    #@11
    .line 2184
    iget v5, v1, Landroid/widget/TextView$Drawables;->mDrawablePadding:I

    #@13
    if-nez v5, :cond_23

    #@15
    .line 2185
    iput-object v6, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@17
    .line 2274
    :cond_17
    :goto_17
    invoke-virtual {p0}, Landroid/widget/TextView;->resolveDrawables()V

    #@1a
    .line 2275
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1d
    .line 2276
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@20
    .line 2277
    return-void

    #@21
    .end local v2           #drawables:Z
    :cond_21
    move v2, v4

    #@22
    .line 2178
    goto :goto_d

    #@23
    .line 2189
    .restart local v2       #drawables:Z
    :cond_23
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@25
    if-eqz v5, :cond_2c

    #@27
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@29
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@2c
    .line 2190
    :cond_2c
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@2e
    .line 2191
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@30
    if-eqz v5, :cond_37

    #@32
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@34
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@37
    .line 2192
    :cond_37
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@39
    .line 2193
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@3b
    if-eqz v5, :cond_42

    #@3d
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@3f
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@42
    .line 2194
    :cond_42
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@44
    .line 2195
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@46
    if-eqz v5, :cond_4d

    #@48
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@4a
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@4d
    .line 2196
    :cond_4d
    iput-object v6, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@4f
    .line 2197
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@51
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@53
    .line 2198
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@55
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@57
    .line 2199
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@59
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@5b
    .line 2200
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@5d
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@5f
    goto :goto_17

    #@60
    .line 2204
    :cond_60
    if-nez v1, :cond_69

    #@62
    .line 2205
    new-instance v1, Landroid/widget/TextView$Drawables;

    #@64
    .end local v1           #dr:Landroid/widget/TextView$Drawables;
    invoke-direct {v1}, Landroid/widget/TextView$Drawables;-><init>()V

    #@67
    .restart local v1       #dr:Landroid/widget/TextView$Drawables;
    iput-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@69
    .line 2208
    :cond_69
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@6b
    if-eq v5, p1, :cond_76

    #@6d
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@6f
    if-eqz v5, :cond_76

    #@71
    .line 2209
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@73
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@76
    .line 2211
    :cond_76
    iput-object p1, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@78
    .line 2213
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@7a
    if-eq v5, p2, :cond_85

    #@7c
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@7e
    if-eqz v5, :cond_85

    #@80
    .line 2214
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@82
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@85
    .line 2216
    :cond_85
    iput-object p2, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@87
    .line 2218
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@89
    if-eq v5, p3, :cond_94

    #@8b
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@8d
    if-eqz v5, :cond_94

    #@8f
    .line 2219
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@91
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@94
    .line 2221
    :cond_94
    iput-object p3, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@96
    .line 2223
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@98
    if-eq v5, p4, :cond_a3

    #@9a
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@9c
    if-eqz v5, :cond_a3

    #@9e
    .line 2224
    iget-object v5, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@a0
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@a3
    .line 2226
    :cond_a3
    iput-object p4, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@a5
    .line 2228
    iget-object v0, v1, Landroid/widget/TextView$Drawables;->mCompoundRect:Landroid/graphics/Rect;

    #@a7
    .line 2231
    .local v0, compoundRect:Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/widget/TextView;->getDrawableState()[I

    #@aa
    move-result-object v3

    #@ab
    .line 2233
    .local v3, state:[I
    if-eqz p1, :cond_109

    #@ad
    .line 2234
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@b0
    .line 2235
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@b3
    .line 2236
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@b6
    .line 2237
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@b9
    move-result v5

    #@ba
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@bc
    .line 2238
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@bf
    move-result v5

    #@c0
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@c2
    .line 2243
    :goto_c2
    if-eqz p3, :cond_10e

    #@c4
    .line 2244
    invoke-virtual {p3, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@c7
    .line 2245
    invoke-virtual {p3, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@ca
    .line 2246
    invoke-virtual {p3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@cd
    .line 2247
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@d0
    move-result v5

    #@d1
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@d3
    .line 2248
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@d6
    move-result v5

    #@d7
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@d9
    .line 2253
    :goto_d9
    if-eqz p2, :cond_113

    #@db
    .line 2254
    invoke-virtual {p2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@de
    .line 2255
    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@e1
    .line 2256
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@e4
    .line 2257
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@e7
    move-result v5

    #@e8
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@ea
    .line 2258
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@ed
    move-result v5

    #@ee
    iput v5, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@f0
    .line 2263
    :goto_f0
    if-eqz p4, :cond_118

    #@f2
    .line 2264
    invoke-virtual {p4, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@f5
    .line 2265
    invoke-virtual {p4, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    #@f8
    .line 2266
    invoke-virtual {p4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@fb
    .line 2267
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@fe
    move-result v4

    #@ff
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@101
    .line 2268
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@104
    move-result v4

    #@105
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@107
    goto/16 :goto_17

    #@109
    .line 2240
    :cond_109
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightStart:I

    #@10b
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeStart:I

    #@10d
    goto :goto_c2

    #@10e
    .line 2250
    :cond_10e
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightEnd:I

    #@110
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeEnd:I

    #@112
    goto :goto_d9

    #@113
    .line 2260
    :cond_113
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthTop:I

    #@115
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeTop:I

    #@117
    goto :goto_f0

    #@118
    .line 2270
    :cond_118
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableWidthBottom:I

    #@11a
    iput v4, v1, Landroid/widget/TextView$Drawables;->mDrawableSizeBottom:I

    #@11c
    goto/16 :goto_17
.end method

.method public setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V
    .registers 10
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2298
    invoke-virtual {p0}, Landroid/widget/TextView;->resetResolvedDrawables()V

    #@4
    .line 2299
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    .line 2300
    .local v0, resources:Landroid/content/res/Resources;
    if-eqz p1, :cond_2a

    #@e
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@11
    move-result-object v2

    #@12
    move-object v4, v2

    #@13
    :goto_13
    if-eqz p2, :cond_2c

    #@15
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@18
    move-result-object v2

    #@19
    move-object v3, v2

    #@1a
    :goto_1a
    if-eqz p3, :cond_2e

    #@1c
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1f
    move-result-object v2

    #@20
    :goto_20
    if-eqz p4, :cond_26

    #@22
    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@25
    move-result-object v1

    #@26
    :cond_26
    invoke-virtual {p0, v4, v3, v2, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@29
    .line 2305
    return-void

    #@2a
    :cond_2a
    move-object v4, v1

    #@2b
    .line 2300
    goto :goto_13

    #@2c
    :cond_2c
    move-object v3, v1

    #@2d
    goto :goto_1a

    #@2e
    :cond_2e
    move-object v2, v1

    #@2f
    goto :goto_20
.end method

.method public setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 8
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2321
    invoke-virtual {p0}, Landroid/widget/TextView;->resetResolvedDrawables()V

    #@4
    .line 2322
    if-eqz p1, :cond_11

    #@6
    .line 2323
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@9
    move-result v0

    #@a
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@d
    move-result v1

    #@e
    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@11
    .line 2325
    :cond_11
    if-eqz p3, :cond_1e

    #@13
    .line 2326
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@16
    move-result v0

    #@17
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {p3, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1e
    .line 2328
    :cond_1e
    if-eqz p2, :cond_2b

    #@20
    .line 2329
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@23
    move-result v0

    #@24
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@27
    move-result v1

    #@28
    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2b
    .line 2331
    :cond_2b
    if-eqz p4, :cond_38

    #@2d
    .line 2332
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@30
    move-result v0

    #@31
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@34
    move-result v1

    #@35
    invoke-virtual {p4, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@38
    .line 2334
    :cond_38
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@3b
    .line 2335
    return-void
.end method

.method public setCompoundDrawablesWithIntrinsicBounds(IIII)V
    .registers 10
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2127
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v0

    #@9
    .line 2128
    .local v0, resources:Landroid/content/res/Resources;
    if-eqz p1, :cond_27

    #@b
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v2

    #@f
    move-object v4, v2

    #@10
    :goto_10
    if-eqz p2, :cond_29

    #@12
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@15
    move-result-object v2

    #@16
    move-object v3, v2

    #@17
    :goto_17
    if-eqz p3, :cond_2b

    #@19
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1c
    move-result-object v2

    #@1d
    :goto_1d
    if-eqz p4, :cond_23

    #@1f
    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@22
    move-result-object v1

    #@23
    :cond_23
    invoke-virtual {p0, v4, v3, v2, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@26
    .line 2132
    return-void

    #@27
    :cond_27
    move-object v4, v1

    #@28
    .line 2128
    goto :goto_10

    #@29
    :cond_29
    move-object v3, v1

    #@2a
    goto :goto_17

    #@2b
    :cond_2b
    move-object v2, v1

    #@2c
    goto :goto_1d
.end method

.method public setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 8
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2148
    if-eqz p1, :cond_e

    #@3
    .line 2149
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@6
    move-result v0

    #@7
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@a
    move-result v1

    #@b
    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@e
    .line 2151
    :cond_e
    if-eqz p3, :cond_1b

    #@10
    .line 2152
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@13
    move-result v0

    #@14
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@17
    move-result v1

    #@18
    invoke-virtual {p3, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1b
    .line 2154
    :cond_1b
    if-eqz p2, :cond_28

    #@1d
    .line 2155
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@20
    move-result v0

    #@21
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@24
    move-result v1

    #@25
    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@28
    .line 2157
    :cond_28
    if-eqz p4, :cond_35

    #@2a
    .line 2158
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@2d
    move-result v0

    #@2e
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@31
    move-result v1

    #@32
    invoke-virtual {p4, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@35
    .line 2160
    :cond_35
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@38
    .line 2161
    return-void
.end method

.method protected setCursorPosition_internal(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 8980
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    check-cast v0, Landroid/text/Editable;

    #@4
    invoke-static {v0, p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@7
    .line 8981
    return-void
.end method

.method public setCursorVisible(Z)V
    .registers 3
    .parameter "visible"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 7349
    if-eqz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4
    if-nez v0, :cond_7

    #@6
    .line 7360
    :cond_6
    :goto_6
    return-void

    #@7
    .line 7350
    :cond_7
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@a
    .line 7351
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-boolean v0, v0, Landroid/widget/Editor;->mCursorVisible:Z

    #@e
    if-eq v0, p1, :cond_6

    #@10
    .line 7352
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@12
    iput-boolean p1, v0, Landroid/widget/Editor;->mCursorVisible:Z

    #@14
    .line 7353
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@17
    .line 7355
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@19
    invoke-virtual {v0}, Landroid/widget/Editor;->makeBlink()V

    #@1c
    .line 7358
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1e
    invoke-virtual {v0}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@21
    goto :goto_6
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .registers 3
    .parameter "actionModeCallback"

    #@0
    .prologue
    .line 8544
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 8545
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    iput-object p1, v0, Landroid/widget/Editor;->mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

    #@7
    .line 8546
    return-void
.end method

.method public final setEditableFactory(Landroid/text/Editable$Factory;)V
    .registers 3
    .parameter "factory"

    #@0
    .prologue
    .line 3684
    iput-object p1, p0, Landroid/widget/TextView;->mEditableFactory:Landroid/text/Editable$Factory;

    #@2
    .line 3685
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@7
    .line 3686
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .registers 3
    .parameter "where"

    #@0
    .prologue
    .line 7276
    iget-object v0, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@2
    if-eq v0, p1, :cond_13

    #@4
    .line 7277
    iput-object p1, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@6
    .line 7279
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 7280
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@d
    .line 7281
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@10
    .line 7282
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@13
    .line 7285
    :cond_13
    return-void
.end method

.method public setEms(I)V
    .registers 3
    .parameter "ems"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3360
    iput p1, p0, Landroid/widget/TextView;->mMinWidth:I

    #@2
    iput p1, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@4
    .line 3361
    const/4 v0, 0x1

    #@5
    iput v0, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@7
    iput v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@9
    .line 3363
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@c
    .line 3364
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@f
    .line 3365
    return-void
.end method

.method public setEnabled(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 1495
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@3
    move-result v1

    #@4
    if-ne p1, v1, :cond_7

    #@6
    .line 1523
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1499
    :cond_7
    if-nez p1, :cond_1d

    #@9
    .line 1501
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@c
    move-result-object v0

    #@d
    .line 1502
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1d

    #@f
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1d

    #@15
    .line 1503
    invoke-virtual {p0}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    #@18
    move-result-object v1

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1d
    .line 1507
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_1d
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    #@20
    .line 1509
    if-eqz p1, :cond_2b

    #@22
    .line 1511
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@25
    move-result-object v0

    #@26
    .line 1512
    .restart local v0       #imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_2b

    #@28
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@2b
    .line 1516
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_2b
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2d
    if-eqz v1, :cond_6

    #@2f
    .line 1517
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@31
    invoke-virtual {v1}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@34
    .line 1518
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@36
    invoke-virtual {v1}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@39
    .line 1521
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@3b
    invoke-virtual {v1}, Landroid/widget/Editor;->makeBlink()V

    #@3e
    goto :goto_6
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .registers 6
    .parameter "error"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 4492
    if-nez p1, :cond_8

    #@4
    .line 4493
    invoke-virtual {p0, v1, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    #@7
    .line 4501
    :goto_7
    return-void

    #@8
    .line 4495
    :cond_8
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v1

    #@10
    const v2, 0x108037d

    #@13
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@16
    move-result-object v0

    #@17
    .line 4498
    .local v0, dr:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@22
    .line 4499
    invoke-virtual {p0, p1, v0}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    #@25
    goto :goto_7
.end method

.method public setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "error"
    .parameter "icon"

    #@0
    .prologue
    .line 4513
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 4514
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    invoke-virtual {v0, p1, p2}, Landroid/widget/Editor;->setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    #@8
    .line 4515
    return-void
.end method

.method public setExtractedText(Landroid/view/inputmethod/ExtractedText;)V
    .registers 10
    .parameter "text"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 5889
    invoke-virtual {p0}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    #@4
    move-result-object v1

    #@5
    .line 5890
    .local v1, content:Landroid/text/Editable;
    iget-object v5, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@7
    if-eqz v5, :cond_12

    #@9
    .line 5891
    if-nez v1, :cond_43

    #@b
    .line 5892
    iget-object v5, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@d
    sget-object v6, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    #@f
    invoke-virtual {p0, v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@12
    .line 5911
    :cond_12
    :goto_12
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@15
    move-result-object v3

    #@16
    check-cast v3, Landroid/text/Spannable;

    #@18
    .line 5912
    .local v3, sp:Landroid/text/Spannable;
    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    #@1b
    move-result v0

    #@1c
    .line 5913
    .local v0, N:I
    iget v4, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    #@1e
    .line 5914
    .local v4, start:I
    if-gez v4, :cond_6f

    #@20
    const/4 v4, 0x0

    #@21
    .line 5916
    :cond_21
    :goto_21
    iget v2, p1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    #@23
    .line 5917
    .local v2, end:I
    if-gez v2, :cond_73

    #@25
    const/4 v2, 0x0

    #@26
    .line 5919
    :cond_26
    :goto_26
    invoke-static {v3, v4, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@29
    .line 5922
    iget v5, p1, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@2b
    and-int/lit8 v5, v5, 0x2

    #@2d
    if-eqz v5, :cond_77

    #@2f
    .line 5923
    invoke-static {p0, v3}, Landroid/text/method/MetaKeyKeyListener;->startSelecting(Landroid/view/View;Landroid/text/Spannable;)V

    #@32
    .line 5928
    :goto_32
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@34
    if-eqz v5, :cond_42

    #@36
    iget v5, p1, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@38
    and-int/lit8 v5, v5, 0x20

    #@3a
    const/16 v6, 0x20

    #@3c
    if-ne v5, v6, :cond_42

    #@3e
    .line 5929
    const/4 v5, 0x1

    #@3f
    invoke-static {v5}, Landroid/widget/BubblePopupHelper;->setShowingAnyBubblePopup(Z)V

    #@42
    .line 5930
    :cond_42
    return-void

    #@43
    .line 5893
    .end local v0           #N:I
    .end local v2           #end:I
    .end local v3           #sp:Landroid/text/Spannable;
    .end local v4           #start:I
    :cond_43
    iget v5, p1, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    #@45
    if-gez v5, :cond_58

    #@47
    .line 5894
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    #@4a
    move-result v5

    #@4b
    invoke-static {v1, v7, v5}, Landroid/widget/TextView;->removeParcelableSpans(Landroid/text/Spannable;II)V

    #@4e
    .line 5895
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    #@51
    move-result v5

    #@52
    iget-object v6, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@54
    invoke-interface {v1, v7, v5, v6}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@57
    goto :goto_12

    #@58
    .line 5897
    :cond_58
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    #@5b
    move-result v0

    #@5c
    .line 5898
    .restart local v0       #N:I
    iget v4, p1, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    #@5e
    .line 5899
    .restart local v4       #start:I
    if-le v4, v0, :cond_61

    #@60
    move v4, v0

    #@61
    .line 5900
    :cond_61
    iget v2, p1, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    #@63
    .line 5901
    .restart local v2       #end:I
    if-le v2, v0, :cond_66

    #@65
    move v2, v0

    #@66
    .line 5902
    :cond_66
    invoke-static {v1, v4, v2}, Landroid/widget/TextView;->removeParcelableSpans(Landroid/text/Spannable;II)V

    #@69
    .line 5903
    iget-object v5, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@6b
    invoke-interface {v1, v4, v2, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@6e
    goto :goto_12

    #@6f
    .line 5915
    .end local v2           #end:I
    .restart local v3       #sp:Landroid/text/Spannable;
    :cond_6f
    if-le v4, v0, :cond_21

    #@71
    move v4, v0

    #@72
    goto :goto_21

    #@73
    .line 5918
    .restart local v2       #end:I
    :cond_73
    if-le v2, v0, :cond_26

    #@75
    move v2, v0

    #@76
    goto :goto_26

    #@77
    .line 5925
    :cond_77
    invoke-static {p0, v3}, Landroid/text/method/MetaKeyKeyListener;->stopSelecting(Landroid/view/View;Landroid/text/Spannable;)V

    #@7a
    goto :goto_32
.end method

.method public setExtracting(Landroid/view/inputmethod/ExtractedTextRequest;)V
    .registers 3
    .parameter "req"

    #@0
    .prologue
    .line 5936
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2
    iget-object v0, v0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 5937
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8
    iget-object v0, v0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@a
    iput-object p1, v0, Landroid/widget/Editor$InputMethodState;->mExtractedTextRequest:Landroid/view/inputmethod/ExtractedTextRequest;

    #@c
    .line 5942
    :cond_c
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@e
    invoke-virtual {v0}, Landroid/widget/Editor;->hideControllers()V

    #@11
    .line 5943
    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .registers 3
    .parameter "filters"

    #@0
    .prologue
    .line 4542
    if-nez p1, :cond_8

    #@2
    .line 4543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@7
    throw v0

    #@8
    .line 4546
    :cond_8
    iput-object p1, p0, Landroid/widget/TextView;->mFilters:[Landroid/text/InputFilter;

    #@a
    .line 4548
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@c
    instance-of v0, v0, Landroid/text/Editable;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 4549
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@12
    check-cast v0, Landroid/text/Editable;

    #@14
    invoke-direct {p0, v0, p1}, Landroid/widget/TextView;->setFilters(Landroid/text/Editable;[Landroid/text/InputFilter;)V

    #@17
    .line 4551
    :cond_17
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 7
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 4519
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setFrame(IIII)Z

    #@3
    move-result v0

    #@4
    .line 4521
    .local v0, result:Z
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    if-eqz v1, :cond_d

    #@8
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a
    invoke-virtual {v1}, Landroid/widget/Editor;->setFrame()V

    #@d
    .line 4523
    :cond_d
    invoke-direct {p0}, Landroid/widget/TextView;->restartMarqueeIfNeeded()V

    #@10
    .line 4525
    return v0
.end method

.method public setFreezesText(Z)V
    .registers 2
    .parameter "freezesText"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3663
    iput-boolean p1, p0, Landroid/widget/TextView;->mFreezesText:Z

    #@2
    .line 3664
    return-void
.end method

.method public setGravity(I)V
    .registers 10
    .parameter "gravity"

    #@0
    .prologue
    const v4, 0x800007

    #@3
    .line 2986
    and-int v0, p1, v4

    #@5
    if-nez v0, :cond_b

    #@7
    .line 2987
    const v0, 0x800003

    #@a
    or-int/2addr p1, v0

    #@b
    .line 2989
    :cond_b
    and-int/lit8 v0, p1, 0x70

    #@d
    if-nez v0, :cond_11

    #@f
    .line 2990
    or-int/lit8 p1, p1, 0x30

    #@11
    .line 2993
    :cond_11
    const/4 v7, 0x0

    #@12
    .line 2995
    .local v7, newLayout:Z
    and-int v0, p1, v4

    #@14
    iget v3, p0, Landroid/widget/TextView;->mGravity:I

    #@16
    and-int/2addr v3, v4

    #@17
    if-eq v0, v3, :cond_1a

    #@19
    .line 2997
    const/4 v7, 0x1

    #@1a
    .line 3000
    :cond_1a
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@1c
    if-eq p1, v0, :cond_24

    #@1e
    .line 3001
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@21
    .line 3002
    const/4 v0, 0x0

    #@22
    iput-object v0, p0, Landroid/widget/TextView;->mLayoutAlignment:Landroid/text/Layout$Alignment;

    #@24
    .line 3005
    :cond_24
    iput p1, p0, Landroid/widget/TextView;->mGravity:I

    #@26
    .line 3006
    sget-boolean v0, Landroid/widget/TextView;->mRtlLangAndLocaleMetaDataExist:Z

    #@28
    if-eqz v0, :cond_43

    #@2a
    iget-boolean v0, p0, Landroid/widget/TextView;->mIsShortLtrText:Z

    #@2c
    if-nez v0, :cond_43

    #@2e
    .line 3007
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@30
    and-int/lit8 v0, v0, 0x3

    #@32
    const/4 v3, 0x3

    #@33
    if-eq v0, v3, :cond_3c

    #@35
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@37
    and-int/lit8 v0, v0, 0x5

    #@39
    const/4 v3, 0x5

    #@3a
    if-ne v0, v3, :cond_43

    #@3c
    .line 3008
    :cond_3c
    iget v0, p0, Landroid/widget/TextView;->mGravity:I

    #@3e
    const/high16 v3, 0x80

    #@40
    or-int/2addr v0, v3

    #@41
    iput v0, p0, Landroid/widget/TextView;->mGravity:I

    #@43
    .line 3012
    :cond_43
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@45
    if-eqz v0, :cond_6d

    #@47
    if-eqz v7, :cond_6d

    #@49
    .line 3014
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@4b
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    #@4e
    move-result v1

    #@4f
    .line 3015
    .local v1, want:I
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@51
    if-nez v0, :cond_6e

    #@53
    const/4 v2, 0x0

    #@54
    .line 3017
    .local v2, hintWant:I
    :goto_54
    sget-object v3, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@56
    sget-object v4, Landroid/widget/TextView;->UNKNOWN_BORING:Landroid/text/BoringLayout$Metrics;

    #@58
    iget v0, p0, Landroid/widget/TextView;->mRight:I

    #@5a
    iget v5, p0, Landroid/widget/TextView;->mLeft:I

    #@5c
    sub-int/2addr v0, v5

    #@5d
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@60
    move-result v5

    #@61
    sub-int/2addr v0, v5

    #@62
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    #@65
    move-result v5

    #@66
    sub-int v5, v0, v5

    #@68
    const/4 v6, 0x1

    #@69
    move-object v0, p0

    #@6a
    invoke-virtual/range {v0 .. v6}, Landroid/widget/TextView;->makeNewLayout(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    #@6d
    .line 3021
    .end local v1           #want:I
    .end local v2           #hintWant:I
    :cond_6d
    return-void

    #@6e
    .line 3015
    .restart local v1       #want:I
    :cond_6e
    iget-object v0, p0, Landroid/widget/TextView;->mHintLayout:Landroid/text/Layout;

    #@70
    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    #@73
    move-result v2

    #@74
    goto :goto_54
.end method

.method public setHeight(I)V
    .registers 3
    .parameter "pixels"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3233
    iput p1, p0, Landroid/widget/TextView;->mMinimum:I

    #@2
    iput p1, p0, Landroid/widget/TextView;->mMaximum:I

    #@4
    .line 3234
    const/4 v0, 0x2

    #@5
    iput v0, p0, Landroid/widget/TextView;->mMinMode:I

    #@7
    iput v0, p0, Landroid/widget/TextView;->mMaxMode:I

    #@9
    .line 3236
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@c
    .line 3237
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@f
    .line 3238
    return-void
.end method

.method public setHighlightColor(I)V
    .registers 3
    .parameter "color"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2716
    iget v0, p0, Landroid/widget/TextView;->mHighlightColor:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 2717
    iput p1, p0, Landroid/widget/TextView;->mHighlightColor:I

    #@6
    .line 2718
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@9
    .line 2720
    :cond_9
    return-void
.end method

.method public final setHint(I)V
    .registers 3
    .parameter "resid"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 4009
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    #@f
    .line 4010
    return-void
.end method

.method public final setHint(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "hint"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3985
    invoke-static {p1}, Landroid/text/TextUtils;->stringOrSpannedString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@6
    .line 3987
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 3988
    invoke-direct {p0}, Landroid/widget/TextView;->checkForRelayout()V

    #@d
    .line 3991
    :cond_d
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@f
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_18

    #@15
    .line 3992
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@18
    .line 3996
    :cond_18
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1a
    if-eqz v0, :cond_2d

    #@1c
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1e
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_2d

    #@24
    iget-object v0, p0, Landroid/widget/TextView;->mHint:Ljava/lang/CharSequence;

    #@26
    if-eqz v0, :cond_2d

    #@28
    .line 3997
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2a
    invoke-virtual {v0}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@2d
    .line 3999
    :cond_2d
    return-void
.end method

.method public final setHintTextColor(I)V
    .registers 3
    .parameter "color"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2893
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@6
    .line 2894
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@9
    .line 2895
    return-void
.end method

.method public final setHintTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .parameter "colors"

    #@0
    .prologue
    .line 2908
    iput-object p1, p0, Landroid/widget/TextView;->mHintTextColor:Landroid/content/res/ColorStateList;

    #@2
    .line 2909
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@5
    .line 2910
    return-void
.end method

.method public setHorizontallyScrolling(Z)V
    .registers 3
    .parameter "whether"

    #@0
    .prologue
    .line 3066
    iget-boolean v0, p0, Landroid/widget/TextView;->mHorizontallyScrolling:Z

    #@2
    if-eq v0, p1, :cond_13

    #@4
    .line 3067
    iput-boolean p1, p0, Landroid/widget/TextView;->mHorizontallyScrolling:Z

    #@6
    .line 3069
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 3070
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@d
    .line 3071
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@10
    .line 3072
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@13
    .line 3075
    :cond_13
    return-void
.end method

.method public setImeActionLabel(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "label"
    .parameter "actionId"

    #@0
    .prologue
    .line 4255
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 4256
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    invoke-virtual {v0}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@8
    .line 4257
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@c
    iput-object p1, v0, Landroid/widget/Editor$InputContentType;->imeActionLabel:Ljava/lang/CharSequence;

    #@e
    .line 4258
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@10
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@12
    iput p2, v0, Landroid/widget/Editor$InputContentType;->imeActionId:I

    #@14
    .line 4259
    return-void
.end method

.method public setImeOptions(I)V
    .registers 3
    .parameter "imeOptions"

    #@0
    .prologue
    .line 4228
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 4229
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    invoke-virtual {v0}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@8
    .line 4230
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@c
    iput p1, v0, Landroid/widget/Editor$InputContentType;->imeOptions:I

    #@e
    .line 4231
    return-void
.end method

.method public setIncludeFontPadding(Z)V
    .registers 3
    .parameter "includepad"

    #@0
    .prologue
    .line 6385
    iget-boolean v0, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@2
    if-eq v0, p1, :cond_13

    #@4
    .line 6386
    iput-boolean p1, p0, Landroid/widget/TextView;->mIncludePad:Z

    #@6
    .line 6388
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 6389
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@d
    .line 6390
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@10
    .line 6391
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@13
    .line 6394
    :cond_13
    return-void
.end method

.method public setInputExtras(I)V
    .registers 5
    .parameter "xmlResId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 4442
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 4443
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@a
    move-result-object v0

    #@b
    .line 4444
    .local v0, parser:Landroid/content/res/XmlResourceParser;
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@d
    invoke-virtual {v1}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@10
    .line 4445
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@12
    iget-object v1, v1, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@14
    new-instance v2, Landroid/os/Bundle;

    #@16
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@19
    iput-object v2, v1, Landroid/widget/Editor$InputContentType;->extras:Landroid/os/Bundle;

    #@1b
    .line 4446
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@21
    iget-object v2, v2, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@23
    iget-object v2, v2, Landroid/widget/Editor$InputContentType;->extras:Landroid/os/Bundle;

    #@25
    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->parseBundleExtras(Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;)V

    #@28
    .line 4447
    return-void
.end method

.method public setInputType(I)V
    .registers 13
    .parameter "type"

    #@0
    .prologue
    const/4 v10, -0x1

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 4069
    invoke-virtual {p0}, Landroid/widget/TextView;->getInputType()I

    #@6
    move-result v9

    #@7
    invoke-static {v9}, Landroid/widget/TextView;->isPasswordInputType(I)Z

    #@a
    move-result v5

    #@b
    .line 4070
    .local v5, wasPassword:Z
    invoke-virtual {p0}, Landroid/widget/TextView;->getInputType()I

    #@e
    move-result v9

    #@f
    invoke-static {v9}, Landroid/widget/TextView;->isVisiblePasswordInputType(I)Z

    #@12
    move-result v6

    #@13
    .line 4071
    .local v6, wasVisiblePassword:Z
    invoke-direct {p0, p1, v8}, Landroid/widget/TextView;->setInputType(IZ)V

    #@16
    .line 4072
    invoke-static {p1}, Landroid/widget/TextView;->isPasswordInputType(I)Z

    #@19
    move-result v2

    #@1a
    .line 4073
    .local v2, isPassword:Z
    invoke-static {p1}, Landroid/widget/TextView;->isVisiblePasswordInputType(I)Z

    #@1d
    move-result v3

    #@1e
    .line 4074
    .local v3, isVisiblePassword:Z
    const/4 v0, 0x0

    #@1f
    .line 4075
    .local v0, forceUpdate:Z
    if-eqz v2, :cond_53

    #@21
    .line 4076
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    #@24
    move-result-object v9

    #@25
    invoke-virtual {p0, v9}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@28
    .line 4095
    :cond_28
    :goto_28
    invoke-static {p1}, Landroid/widget/TextView;->isMultilineInputType(I)Z

    #@2b
    move-result v9

    #@2c
    if-nez v9, :cond_71

    #@2e
    move v4, v7

    #@2f
    .line 4099
    .local v4, singleLine:Z
    :goto_2f
    iget-boolean v9, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@31
    if-ne v9, v4, :cond_35

    #@33
    if-eqz v0, :cond_3b

    #@35
    .line 4102
    :cond_35
    if-nez v2, :cond_38

    #@37
    move v8, v7

    #@38
    :cond_38
    invoke-direct {p0, v4, v8, v7}, Landroid/widget/TextView;->applySingleLine(ZZZ)V

    #@3b
    .line 4105
    :cond_3b
    invoke-virtual {p0}, Landroid/widget/TextView;->isSuggestionsEnabled()Z

    #@3e
    move-result v7

    #@3f
    if-nez v7, :cond_49

    #@41
    .line 4106
    iget-object v7, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@43
    invoke-virtual {p0, v7}, Landroid/widget/TextView;->removeSuggestionSpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@46
    move-result-object v7

    #@47
    iput-object v7, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@49
    .line 4109
    :cond_49
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@4c
    move-result-object v1

    #@4d
    .line 4110
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_52

    #@4f
    invoke-virtual {v1, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@52
    .line 4111
    :cond_52
    return-void

    #@53
    .line 4078
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v4           #singleLine:Z
    :cond_53
    if-eqz v3, :cond_5f

    #@55
    .line 4079
    iget-object v9, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@57
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    #@5a
    move-result-object v10

    #@5b
    if-ne v9, v10, :cond_28

    #@5d
    .line 4080
    const/4 v0, 0x1

    #@5e
    goto :goto_28

    #@5f
    .line 4087
    :cond_5f
    if-nez v5, :cond_63

    #@61
    if-eqz v6, :cond_28

    #@63
    .line 4089
    :cond_63
    const/4 v9, 0x0

    #@64
    invoke-direct {p0, v9, v10, v10}, Landroid/widget/TextView;->setTypefaceFromAttrs(Ljava/lang/String;II)V

    #@67
    .line 4090
    iget-object v9, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@69
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    #@6c
    move-result-object v10

    #@6d
    if-ne v9, v10, :cond_28

    #@6f
    .line 4091
    const/4 v0, 0x1

    #@70
    goto :goto_28

    #@71
    :cond_71
    move v4, v8

    #@72
    .line 4095
    goto :goto_2f
.end method

.method public setKeyListener(Landroid/text/method/KeyListener;)V
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 1666
    invoke-direct {p0, p1}, Landroid/widget/TextView;->setKeyListenerOnly(Landroid/text/method/KeyListener;)V

    #@3
    .line 1667
    invoke-direct {p0}, Landroid/widget/TextView;->fixFocusableAndClickableSettings()V

    #@6
    .line 1669
    if-eqz p1, :cond_2d

    #@8
    .line 1670
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@b
    .line 1672
    :try_start_b
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@d
    iget-object v3, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@f
    iget-object v3, v3, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@11
    invoke-interface {v3}, Landroid/text/method/KeyListener;->getInputType()I

    #@14
    move-result v3

    #@15
    iput v3, v2, Landroid/widget/Editor;->mInputType:I
    :try_end_17
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_b .. :try_end_17} :catch_26

    #@17
    .line 1678
    :goto_17
    iget-boolean v2, p0, Landroid/widget/TextView;->mSingleLine:Z

    #@19
    invoke-direct {p0, v2}, Landroid/widget/TextView;->setInputTypeSingleLine(Z)V

    #@1c
    .line 1683
    :cond_1c
    :goto_1c
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@1f
    move-result-object v1

    #@20
    .line 1684
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_25

    #@22
    invoke-virtual {v1, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@25
    .line 1685
    :cond_25
    return-void

    #@26
    .line 1673
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :catch_26
    move-exception v0

    #@27
    .line 1674
    .local v0, e:Ljava/lang/IncompatibleClassChangeError;
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@29
    const/4 v3, 0x1

    #@2a
    iput v3, v2, Landroid/widget/Editor;->mInputType:I

    #@2c
    goto :goto_17

    #@2d
    .line 1680
    .end local v0           #e:Ljava/lang/IncompatibleClassChangeError;
    :cond_2d
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@2f
    if-eqz v2, :cond_1c

    #@31
    iget-object v2, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@33
    const/4 v3, 0x0

    #@34
    iput v3, v2, Landroid/widget/Editor;->mInputType:I

    #@36
    goto :goto_1c
.end method

.method public setLineSpacing(FF)V
    .registers 4
    .parameter "add"
    .parameter "mult"

    #@0
    .prologue
    .line 3396
    iget v0, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@2
    cmpl-float v0, v0, p1

    #@4
    if-nez v0, :cond_c

    #@6
    iget v0, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@8
    cmpl-float v0, v0, p2

    #@a
    if-eqz v0, :cond_1d

    #@c
    .line 3397
    :cond_c
    iput p1, p0, Landroid/widget/TextView;->mSpacingAdd:F

    #@e
    .line 3398
    iput p2, p0, Landroid/widget/TextView;->mSpacingMult:F

    #@10
    .line 3400
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@12
    if-eqz v0, :cond_1d

    #@14
    .line 3401
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@17
    .line 3402
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@1a
    .line 3403
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1d
    .line 3406
    :cond_1d
    return-void
.end method

.method public setLines(I)V
    .registers 3
    .parameter "lines"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3214
    iput p1, p0, Landroid/widget/TextView;->mMinimum:I

    #@2
    iput p1, p0, Landroid/widget/TextView;->mMaximum:I

    #@4
    .line 3215
    const/4 v0, 0x1

    #@5
    iput v0, p0, Landroid/widget/TextView;->mMinMode:I

    #@7
    iput v0, p0, Landroid/widget/TextView;->mMaxMode:I

    #@9
    .line 3217
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@c
    .line 3218
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@f
    .line 3219
    return-void
.end method

.method public final setLinkTextColor(I)V
    .registers 3
    .parameter "color"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2945
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@6
    .line 2946
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@9
    .line 2947
    return-void
.end method

.method public final setLinkTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .parameter "colors"

    #@0
    .prologue
    .line 2960
    iput-object p1, p0, Landroid/widget/TextView;->mLinkTextColor:Landroid/content/res/ColorStateList;

    #@2
    .line 2961
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@5
    .line 2962
    return-void
.end method

.method public final setLinksClickable(Z)V
    .registers 2
    .parameter "whether"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2851
    iput-boolean p1, p0, Landroid/widget/TextView;->mLinksClickable:Z

    #@2
    .line 2852
    return-void
.end method

.method public final setMarqueeAlwaysEnable(Z)V
    .registers 2
    .parameter "value"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3955
    iput-boolean p1, p0, Landroid/widget/TextView;->mMarqueeAlwaysEnable:Z

    #@2
    .line 3956
    return-void
.end method

.method public setMarqueeRepeatLimit(I)V
    .registers 2
    .parameter "marqueeLimit"

    #@0
    .prologue
    .line 7296
    iput p1, p0, Landroid/widget/TextView;->mMarqueeRepeatLimit:I

    #@2
    .line 7297
    return-void
.end method

.method public setMaxEms(I)V
    .registers 3
    .parameter "maxems"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3301
    iput p1, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@2
    .line 3302
    const/4 v0, 0x1

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@5
    .line 3304
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3305
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3306
    return-void
.end method

.method public setMaxHeight(I)V
    .registers 3
    .parameter "maxHeight"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3185
    iput p1, p0, Landroid/widget/TextView;->mMaximum:I

    #@2
    .line 3186
    const/4 v0, 0x2

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMaxMode:I

    #@5
    .line 3188
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3189
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3190
    return-void
.end method

.method public setMaxLines(I)V
    .registers 3
    .parameter "maxlines"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3156
    iput p1, p0, Landroid/widget/TextView;->mMaximum:I

    #@2
    .line 3157
    const/4 v0, 0x1

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMaxMode:I

    #@5
    .line 3159
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3160
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3161
    return-void
.end method

.method public setMaxWidth(I)V
    .registers 3
    .parameter "maxpixels"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3328
    iput p1, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@2
    .line 3329
    const/4 v0, 0x2

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@5
    .line 3331
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3332
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3333
    return-void
.end method

.method public setMinEms(I)V
    .registers 3
    .parameter "minems"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3247
    iput p1, p0, Landroid/widget/TextView;->mMinWidth:I

    #@2
    .line 3248
    const/4 v0, 0x1

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@5
    .line 3250
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3251
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3252
    return-void
.end method

.method public setMinHeight(I)V
    .registers 3
    .parameter "minHeight"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3128
    iput p1, p0, Landroid/widget/TextView;->mMinimum:I

    #@2
    .line 3129
    const/4 v0, 0x2

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMinMode:I

    #@5
    .line 3131
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3132
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3133
    return-void
.end method

.method public setMinLines(I)V
    .registers 3
    .parameter "minlines"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3100
    iput p1, p0, Landroid/widget/TextView;->mMinimum:I

    #@2
    .line 3101
    const/4 v0, 0x1

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMinMode:I

    #@5
    .line 3103
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3104
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3105
    return-void
.end method

.method public setMinWidth(I)V
    .registers 3
    .parameter "minpixels"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3274
    iput p1, p0, Landroid/widget/TextView;->mMinWidth:I

    #@2
    .line 3275
    const/4 v0, 0x2

    #@3
    iput v0, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@5
    .line 3277
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@8
    .line 3278
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@b
    .line 3279
    return-void
.end method

.method public final setMovementMethod(Landroid/text/method/MovementMethod;)V
    .registers 3
    .parameter "movement"

    #@0
    .prologue
    .line 1721
    iget-object v0, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@2
    if-eq v0, p1, :cond_1f

    #@4
    .line 1722
    iput-object p1, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@6
    .line 1724
    if-eqz p1, :cond_13

    #@8
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@a
    instance-of v0, v0, Landroid/text/Spannable;

    #@c
    if-nez v0, :cond_13

    #@e
    .line 1725
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@10
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@13
    .line 1728
    :cond_13
    invoke-direct {p0}, Landroid/widget/TextView;->fixFocusableAndClickableSettings()V

    #@16
    .line 1732
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1c
    invoke-virtual {v0}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@1f
    .line 1734
    :cond_1f
    return-void
.end method

.method public setNoEmojiEditMode()V
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 9491
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@3
    if-eqz v4, :cond_34

    #@5
    .line 9492
    invoke-virtual {p0}, Landroid/widget/TextView;->getRootView()Landroid/view/View;

    #@8
    move-result-object v2

    #@9
    .line 9493
    .local v2, rootView:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@c
    move-result-object v1

    #@d
    .line 9494
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    const/4 v0, 0x0

    #@e
    .line 9496
    .local v0, isFloatingWindow:Z
    instance-of v4, v1, Landroid/view/WindowManager$LayoutParams;

    #@10
    if-eqz v4, :cond_2b

    #@12
    .line 9497
    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    #@14
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@16
    const/16 v5, 0x7d2

    #@18
    if-ne v4, v5, :cond_35

    #@1a
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    const-string v5, "FrameView"

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_35

    #@2a
    move v0, v3

    #@2b
    .line 9501
    :cond_2b
    :goto_2b
    if-nez v0, :cond_34

    #@2d
    .line 9502
    iput-boolean v3, p0, Landroid/widget/TextView;->mNoEmojiEditMode:Z

    #@2f
    .line 9503
    const-string v3, "com.lge.android.editmode.noEmoji"

    #@31
    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    #@34
    .line 9506
    .end local v0           #isFloatingWindow:Z
    .end local v2           #rootView:Landroid/view/View;
    :cond_34
    return-void

    #@35
    .line 9497
    .restart local v0       #isFloatingWindow:Z
    .restart local v2       #rootView:Landroid/view/View;
    :cond_35
    const/4 v0, 0x0

    #@36
    goto :goto_2b
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4292
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 4293
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    invoke-virtual {v0}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@8
    .line 4294
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@a
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@c
    iput-object p1, v0, Landroid/widget/Editor$InputContentType;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@e
    .line 4295
    return-void
.end method

.method public setPadding(IIII)V
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2411
    iget v0, p0, Landroid/widget/TextView;->mPaddingLeft:I

    #@2
    if-ne p1, v0, :cond_10

    #@4
    iget v0, p0, Landroid/widget/TextView;->mPaddingRight:I

    #@6
    if-ne p3, v0, :cond_10

    #@8
    iget v0, p0, Landroid/widget/TextView;->mPaddingTop:I

    #@a
    if-ne p2, v0, :cond_10

    #@c
    iget v0, p0, Landroid/widget/TextView;->mPaddingBottom:I

    #@e
    if-eq p4, v0, :cond_13

    #@10
    .line 2415
    :cond_10
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@13
    .line 2419
    :cond_13
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setPadding(IIII)V

    #@16
    .line 2420
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@19
    .line 2421
    return-void
.end method

.method public setPaddingRelative(IIII)V
    .registers 6
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"

    #@0
    .prologue
    .line 2425
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaddingStart()I

    #@3
    move-result v0

    #@4
    if-ne p1, v0, :cond_14

    #@6
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaddingEnd()I

    #@9
    move-result v0

    #@a
    if-ne p3, v0, :cond_14

    #@c
    iget v0, p0, Landroid/widget/TextView;->mPaddingTop:I

    #@e
    if-ne p2, v0, :cond_14

    #@10
    iget v0, p0, Landroid/widget/TextView;->mPaddingBottom:I

    #@12
    if-eq p4, v0, :cond_17

    #@14
    .line 2429
    :cond_14
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@17
    .line 2433
    :cond_17
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setPaddingRelative(IIII)V

    #@1a
    .line 2434
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1d
    .line 2435
    return-void
.end method

.method public setPaintFlags(I)V
    .registers 3
    .parameter "flags"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3048
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getFlags()I

    #@5
    move-result v0

    #@6
    if-eq v0, p1, :cond_1a

    #@8
    .line 3049
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@a
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setFlags(I)V

    #@d
    .line 3051
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 3052
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@14
    .line 3053
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@17
    .line 3054
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1a
    .line 3057
    :cond_1a
    return-void
.end method

.method public setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;I)V
    .registers 5
    .parameter "listener"
    .parameter "type"

    #@0
    .prologue
    .line 9474
    if-eqz p1, :cond_6

    #@2
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4
    if-nez v1, :cond_7

    #@6
    .line 9481
    :cond_6
    :goto_6
    return-void

    #@7
    .line 9477
    :cond_7
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@9
    invoke-virtual {v1, p1, p2}, Landroid/widget/Editor;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;I)V

    #@c
    .line 9479
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@f
    move-result-object v0

    #@10
    .line 9480
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_6

    #@12
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    #@15
    goto :goto_6
.end method

.method public setPrivateImeOptions(Ljava/lang/String;)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4396
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@4
    .line 4397
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor;->createInputContentTypeIfNeeded()V

    #@9
    .line 4398
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@b
    iget-object v0, v0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@d
    iput-object p1, v0, Landroid/widget/Editor$InputContentType;->privateImeOptions:Ljava/lang/String;

    #@f
    .line 4400
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@11
    if-eqz v0, :cond_31

    #@13
    .line 4401
    const-string v0, "DISABLE_BUBBLE_POPUP"

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_1d

    #@1b
    .line 4402
    iput-boolean v1, p0, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@1d
    .line 4404
    :cond_1d
    const-string v0, "DISABLE_BUBBLE_POPUP_PASTE"

    #@1f
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_27

    #@25
    .line 4405
    iput-boolean v1, p0, Landroid/widget/TextView;->mCanPasteBubblePopup:Z

    #@27
    .line 4407
    :cond_27
    const-string v0, "DISABLE_BUBBLE_POPUP_CUT"

    #@29
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_31

    #@2f
    .line 4408
    iput-boolean v1, p0, Landroid/widget/TextView;->mCanCutBubblePopup:Z

    #@31
    .line 4412
    :cond_31
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@33
    if-eqz v0, :cond_42

    #@35
    .line 4413
    if-eqz p1, :cond_42

    #@37
    const-string v0, "com.lge.android.editmode.noEmoji"

    #@39
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@3c
    move-result v0

    #@3d
    if-eqz v0, :cond_42

    #@3f
    .line 4414
    const/4 v0, 0x1

    #@40
    iput-boolean v0, p0, Landroid/widget/TextView;->mNoEmojiEditMode:Z

    #@42
    .line 4417
    :cond_42
    return-void
.end method

.method public setRawInputType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 4158
    if-nez p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4
    if-nez v0, :cond_7

    #@6
    .line 4161
    :goto_6
    return-void

    #@7
    .line 4159
    :cond_7
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@a
    .line 4160
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iput p1, v0, Landroid/widget/Editor;->mInputType:I

    #@e
    goto :goto_6
.end method

.method public setScroller(Landroid/widget/Scroller;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 7946
    iput-object p1, p0, Landroid/widget/TextView;->mScroller:Landroid/widget/Scroller;

    #@2
    .line 7947
    return-void
.end method

.method public setSelectAllOnFocus(Z)V
    .registers 4
    .parameter "selectAllOnFocus"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 7331
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 7332
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    iput-boolean p1, v0, Landroid/widget/Editor;->mSelectAllOnFocus:Z

    #@7
    .line 7334
    if-eqz p1, :cond_16

    #@9
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@b
    instance-of v0, v0, Landroid/text/Spannable;

    #@d
    if-nez v0, :cond_16

    #@f
    .line 7335
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@11
    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@16
    .line 7337
    :cond_16
    return-void
.end method

.method public setSelected(Z)V
    .registers 5
    .parameter "selected"

    #@0
    .prologue
    .line 7810
    invoke-virtual {p0}, Landroid/widget/TextView;->isSelected()Z

    #@3
    move-result v0

    #@4
    .line 7812
    .local v0, wasSelected:Z
    invoke-super {p0, p1}, Landroid/view/View;->setSelected(Z)V

    #@7
    .line 7814
    if-eq p1, v0, :cond_14

    #@9
    iget-object v1, p0, Landroid/widget/TextView;->mEllipsize:Landroid/text/TextUtils$TruncateAt;

    #@b
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@d
    if-ne v1, v2, :cond_14

    #@f
    .line 7815
    if-eqz p1, :cond_15

    #@11
    .line 7816
    invoke-direct {p0}, Landroid/widget/TextView;->startMarquee()V

    #@14
    .line 7821
    :cond_14
    :goto_14
    return-void

    #@15
    .line 7818
    :cond_15
    invoke-direct {p0}, Landroid/widget/TextView;->stopMarquee()V

    #@18
    goto :goto_14
.end method

.method public setShadowLayer(FFFI)V
    .registers 6
    .parameter "radius"
    .parameter "dx"
    .parameter "dy"
    .parameter "color"

    #@0
    .prologue
    .line 2764
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    #@5
    .line 2766
    iput p1, p0, Landroid/widget/TextView;->mShadowRadius:F

    #@7
    .line 2767
    iput p2, p0, Landroid/widget/TextView;->mShadowDx:F

    #@9
    .line 2768
    iput p3, p0, Landroid/widget/TextView;->mShadowDy:F

    #@b
    .line 2771
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@d
    if-eqz v0, :cond_14

    #@f
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@11
    invoke-virtual {v0}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@14
    .line 2772
    :cond_14
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@17
    .line 2773
    return-void
.end method

.method public final setShowSoftInputOnFocus(Z)V
    .registers 3
    .parameter "show"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2740
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@3
    .line 2741
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@5
    iput-boolean p1, v0, Landroid/widget/Editor;->mShowSoftInputOnFocus:Z

    #@7
    .line 2742
    return-void
.end method

.method public setSingleLine()V
    .registers 2

    #@0
    .prologue
    .line 7184
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    #@4
    .line 7185
    return-void
.end method

.method public setSingleLine(Z)V
    .registers 3
    .parameter "singleLine"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 7221
    invoke-direct {p0, p1}, Landroid/widget/TextView;->setInputTypeSingleLine(Z)V

    #@4
    .line 7222
    invoke-direct {p0, p1, v0, v0}, Landroid/widget/TextView;->applySingleLine(ZZZ)V

    #@7
    .line 7223
    return-void
.end method

.method protected setSpan_internal(Ljava/lang/Object;III)V
    .registers 6
    .parameter "span"
    .parameter "start"
    .parameter "end"
    .parameter "flags"

    #@0
    .prologue
    .line 8972
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2
    check-cast v0, Landroid/text/Editable;

    #@4
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@7
    .line 8973
    return-void
.end method

.method public final setSpannableFactory(Landroid/text/Spannable$Factory;)V
    .registers 3
    .parameter "factory"

    #@0
    .prologue
    .line 3692
    iput-object p1, p0, Landroid/widget/TextView;->mSpannableFactory:Landroid/text/Spannable$Factory;

    #@2
    .line 3693
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@7
    .line 3694
    return-void
.end method

.method public final setText(I)V
    .registers 3
    .parameter "resid"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3969
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f
    .line 3970
    return-void
.end method

.method public final setText(ILandroid/widget/TextView$BufferType;)V
    .registers 4
    .parameter "resid"
    .parameter "type"

    #@0
    .prologue
    .line 3973
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@f
    .line 3974
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3709
    iget-object v0, p0, Landroid/widget/TextView;->mBufferType:Landroid/widget/TextView$BufferType;

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@5
    .line 3710
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .registers 5
    .parameter "text"
    .parameter "type"

    #@0
    .prologue
    .line 3734
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;ZI)V

    #@5
    .line 3736
    iget-object v0, p0, Landroid/widget/TextView;->mCharWrapper:Landroid/widget/TextView$CharWrapper;

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 3737
    iget-object v0, p0, Landroid/widget/TextView;->mCharWrapper:Landroid/widget/TextView$CharWrapper;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-static {v0, v1}, Landroid/widget/TextView$CharWrapper;->access$102(Landroid/widget/TextView$CharWrapper;[C)[C

    #@f
    .line 3739
    :cond_f
    return-void
.end method

.method public final setText([CII)V
    .registers 8
    .parameter "text"
    .parameter "start"
    .parameter "len"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3898
    const/4 v0, 0x0

    #@2
    .line 3900
    .local v0, oldlen:I
    if-ltz p2, :cond_b

    #@4
    if-ltz p3, :cond_b

    #@6
    add-int v1, p2, p3

    #@8
    array-length v2, p1

    #@9
    if-le v1, v2, :cond_28

    #@b
    .line 3901
    :cond_b
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 3909
    :cond_28
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2a
    if-eqz v1, :cond_4a

    #@2c
    .line 3910
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2e
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@31
    move-result v0

    #@32
    .line 3911
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@34
    invoke-direct {p0, v1, v3, v0, p3}, Landroid/widget/TextView;->sendBeforeTextChanged(Ljava/lang/CharSequence;III)V

    #@37
    .line 3916
    :goto_37
    iget-object v1, p0, Landroid/widget/TextView;->mCharWrapper:Landroid/widget/TextView$CharWrapper;

    #@39
    if-nez v1, :cond_50

    #@3b
    .line 3917
    new-instance v1, Landroid/widget/TextView$CharWrapper;

    #@3d
    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView$CharWrapper;-><init>([CII)V

    #@40
    iput-object v1, p0, Landroid/widget/TextView;->mCharWrapper:Landroid/widget/TextView$CharWrapper;

    #@42
    .line 3922
    :goto_42
    iget-object v1, p0, Landroid/widget/TextView;->mCharWrapper:Landroid/widget/TextView$CharWrapper;

    #@44
    iget-object v2, p0, Landroid/widget/TextView;->mBufferType:Landroid/widget/TextView$BufferType;

    #@46
    invoke-direct {p0, v1, v2, v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;ZI)V

    #@49
    .line 3923
    return-void

    #@4a
    .line 3913
    :cond_4a
    const-string v1, ""

    #@4c
    invoke-direct {p0, v1, v3, v3, p3}, Landroid/widget/TextView;->sendBeforeTextChanged(Ljava/lang/CharSequence;III)V

    #@4f
    goto :goto_37

    #@50
    .line 3919
    :cond_50
    iget-object v1, p0, Landroid/widget/TextView;->mCharWrapper:Landroid/widget/TextView$CharWrapper;

    #@52
    invoke-virtual {v1, p1, p2, p3}, Landroid/widget/TextView$CharWrapper;->set([CII)V

    #@55
    goto :goto_42
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .registers 13
    .parameter "context"
    .parameter "resid"

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 2453
    sget-object v7, Lcom/android/internal/R$styleable;->TextAppearance:[I

    #@4
    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@7
    move-result-object v0

    #@8
    .line 2461
    .local v0, appearance:Landroid/content/res/TypedArray;
    const/4 v7, 0x4

    #@9
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    #@c
    move-result v1

    #@d
    .line 2463
    .local v1, color:I
    if-eqz v1, :cond_12

    #@f
    .line 2464
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setHighlightColor(I)V

    #@12
    .line 2467
    :cond_12
    const/4 v7, 0x3

    #@13
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@16
    move-result-object v2

    #@17
    .line 2469
    .local v2, colors:Landroid/content/res/ColorStateList;
    if-eqz v2, :cond_1c

    #@19
    .line 2470
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    #@1c
    .line 2473
    :cond_1c
    invoke-virtual {v0, v8, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@1f
    move-result v5

    #@20
    .line 2475
    .local v5, ts:I
    if-eqz v5, :cond_26

    #@22
    .line 2476
    int-to-float v7, v5

    #@23
    invoke-direct {p0, v7}, Landroid/widget/TextView;->setRawTextSize(F)V

    #@26
    .line 2479
    :cond_26
    const/4 v7, 0x5

    #@27
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@2a
    move-result-object v2

    #@2b
    .line 2481
    if-eqz v2, :cond_30

    #@2d
    .line 2482
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    #@30
    .line 2485
    :cond_30
    const/4 v7, 0x6

    #@31
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@34
    move-result-object v2

    #@35
    .line 2487
    if-eqz v2, :cond_3a

    #@37
    .line 2488
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    #@3a
    .line 2494
    :cond_3a
    const/16 v7, 0x8

    #@3c
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    .line 2496
    .local v3, familyName:Ljava/lang/String;
    const/4 v7, 0x1

    #@41
    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@44
    move-result v6

    #@45
    .line 2498
    .local v6, typefaceIndex:I
    const/4 v7, 0x2

    #@46
    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@49
    move-result v4

    #@4a
    .line 2501
    .local v4, styleIndex:I
    invoke-direct {p0, v3, v6, v4}, Landroid/widget/TextView;->setTypefaceFromAttrs(Ljava/lang/String;II)V

    #@4d
    .line 2503
    const/4 v7, 0x7

    #@4e
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@51
    move-result v7

    #@52
    if-eqz v7, :cond_60

    #@54
    .line 2505
    new-instance v7, Landroid/text/method/AllCapsTransformationMethod;

    #@56
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@59
    move-result-object v8

    #@5a
    invoke-direct {v7, v8}, Landroid/text/method/AllCapsTransformationMethod;-><init>(Landroid/content/Context;)V

    #@5d
    invoke-virtual {p0, v7}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    #@60
    .line 2508
    :cond_60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@63
    .line 2509
    return-void
.end method

.method public setTextColor(I)V
    .registers 3
    .parameter "color"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2665
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/widget/TextView;->mTextColor:Landroid/content/res/ColorStateList;

    #@6
    .line 2666
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@9
    .line 2667
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .registers 3
    .parameter "colors"

    #@0
    .prologue
    .line 2680
    if-nez p1, :cond_8

    #@2
    .line 2681
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 2684
    :cond_8
    iput-object p1, p0, Landroid/widget/TextView;->mTextColor:Landroid/content/res/ColorStateList;

    #@a
    .line 2685
    invoke-direct {p0}, Landroid/widget/TextView;->updateTextColors()V

    #@d
    .line 2686
    return-void
.end method

.method public setTextIsSelectable(Z)V
    .registers 4
    .parameter "selectable"

    #@0
    .prologue
    .line 5012
    if-nez p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4
    if-nez v0, :cond_7

    #@6
    .line 5030
    :cond_6
    :goto_6
    return-void

    #@7
    .line 5014
    :cond_7
    invoke-direct {p0}, Landroid/widget/TextView;->createEditorIfNeeded()V

    #@a
    .line 5015
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@c
    iget-boolean v0, v0, Landroid/widget/Editor;->mTextIsSelectable:Z

    #@e
    if-eq v0, p1, :cond_6

    #@10
    .line 5017
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@12
    iput-boolean p1, v0, Landroid/widget/Editor;->mTextIsSelectable:Z

    #@14
    .line 5018
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    #@17
    .line 5019
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setFocusable(Z)V

    #@1a
    .line 5020
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setClickable(Z)V

    #@1d
    .line 5021
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setLongClickable(Z)V

    #@20
    .line 5025
    if-eqz p1, :cond_38

    #@22
    invoke-static {}, Landroid/text/method/ArrowKeyMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    #@25
    move-result-object v0

    #@26
    :goto_26
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    #@29
    .line 5026
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2b
    if-eqz p1, :cond_3a

    #@2d
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@2f
    :goto_2f
    invoke-virtual {p0, v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@32
    .line 5029
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@34
    invoke-virtual {v0}, Landroid/widget/Editor;->prepareCursorControllers()V

    #@37
    goto :goto_6

    #@38
    .line 5025
    :cond_38
    const/4 v0, 0x0

    #@39
    goto :goto_26

    #@3a
    .line 5026
    :cond_3a
    sget-object v0, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    #@3c
    goto :goto_2f
.end method

.method public final setTextKeepState(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "text"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3722
    iget-object v0, p0, Landroid/widget/TextView;->mBufferType:Landroid/widget/TextView$BufferType;

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/widget/TextView;->setTextKeepState(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@5
    .line 3723
    return-void
.end method

.method public final setTextKeepState(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .registers 10
    .parameter "text"
    .parameter "type"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 3932
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@4
    move-result v2

    #@5
    .line 3933
    .local v2, start:I
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@8
    move-result v0

    #@9
    .line 3934
    .local v0, end:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@c
    move-result v1

    #@d
    .line 3936
    .local v1, len:I
    invoke-virtual {p0, p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@10
    .line 3938
    if-gez v2, :cond_14

    #@12
    if-ltz v0, :cond_31

    #@14
    .line 3939
    :cond_14
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@16
    instance-of v3, v3, Landroid/text/Spannable;

    #@18
    if-eqz v3, :cond_31

    #@1a
    .line 3940
    iget-object v3, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1c
    check-cast v3, Landroid/text/Spannable;

    #@1e
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    #@21
    move-result v4

    #@22
    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    #@25
    move-result v4

    #@26
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@29
    move-result v5

    #@2a
    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    #@2d
    move-result v5

    #@2e
    invoke-static {v3, v4, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@31
    .line 3945
    :cond_31
    return-void
.end method

.method public setTextLocale(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 2529
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextLocale(Ljava/util/Locale;)V

    #@5
    .line 2530
    return-void
.end method

.method public setTextScaleX(F)V
    .registers 3
    .parameter "size"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2603
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextScaleX()F

    #@5
    move-result v0

    #@6
    cmpl-float v0, p1, v0

    #@8
    if-eqz v0, :cond_1f

    #@a
    .line 2604
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/widget/TextView;->mUserSetTextScaleX:Z

    #@d
    .line 2605
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@f
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextScaleX(F)V

    #@12
    .line 2607
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@14
    if-eqz v0, :cond_1f

    #@16
    .line 2608
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@19
    .line 2609
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@1c
    .line 2610
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1f
    .line 2613
    :cond_1f
    return-void
.end method

.method public setTextSize(F)V
    .registers 3
    .parameter "size"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 2551
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    #@4
    .line 2552
    return-void
.end method

.method public setTextSize(IF)V
    .registers 6
    .parameter "unit"
    .parameter "size"

    #@0
    .prologue
    .line 2564
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 2567
    .local v0, c:Landroid/content/Context;
    if-nez v0, :cond_16

    #@6
    .line 2568
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@9
    move-result-object v1

    #@a
    .line 2572
    .local v1, r:Landroid/content/res/Resources;
    :goto_a
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@d
    move-result-object v2

    #@e
    invoke-static {p1, p2, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@11
    move-result v2

    #@12
    invoke-direct {p0, v2}, Landroid/widget/TextView;->setRawTextSize(F)V

    #@15
    .line 2574
    return-void

    #@16
    .line 2570
    .end local v1           #r:Landroid/content/res/Resources;
    :cond_16
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v1

    #@1a
    .restart local v1       #r:Landroid/content/res/Resources;
    goto :goto_a
.end method

.method public final setTransformationMethod(Landroid/text/method/TransformationMethod;)V
    .registers 6
    .parameter "method"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1768
    iget-object v1, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@3
    if-ne p1, v1, :cond_6

    #@5
    .line 1794
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1773
    :cond_6
    iget-object v1, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@8
    if-eqz v1, :cond_19

    #@a
    .line 1774
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@c
    instance-of v1, v1, Landroid/text/Spannable;

    #@e
    if-eqz v1, :cond_19

    #@10
    .line 1775
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@12
    check-cast v1, Landroid/text/Spannable;

    #@14
    iget-object v3, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@16
    invoke-interface {v1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@19
    .line 1779
    :cond_19
    iput-object p1, p0, Landroid/widget/TextView;->mTransformation:Landroid/text/method/TransformationMethod;

    #@1b
    .line 1781
    instance-of v1, p1, Landroid/text/method/TransformationMethod2;

    #@1d
    if-eqz v1, :cond_47

    #@1f
    move-object v0, p1

    #@20
    .line 1782
    check-cast v0, Landroid/text/method/TransformationMethod2;

    #@22
    .line 1783
    .local v0, method2:Landroid/text/method/TransformationMethod2;
    invoke-virtual {p0}, Landroid/widget/TextView;->isTextSelectable()Z

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_45

    #@28
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@2a
    instance-of v1, v1, Landroid/text/Editable;

    #@2c
    if-nez v1, :cond_45

    #@2e
    const/4 v1, 0x1

    #@2f
    :goto_2f
    iput-boolean v1, p0, Landroid/widget/TextView;->mAllowTransformationLengthChange:Z

    #@31
    .line 1784
    iget-boolean v1, p0, Landroid/widget/TextView;->mAllowTransformationLengthChange:Z

    #@33
    invoke-interface {v0, v1}, Landroid/text/method/TransformationMethod2;->setLengthChangesAllowed(Z)V

    #@36
    .line 1789
    .end local v0           #method2:Landroid/text/method/TransformationMethod2;
    :goto_36
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@38
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@3b
    .line 1791
    invoke-direct {p0}, Landroid/widget/TextView;->hasPasswordTransformationMethod()Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_5

    #@41
    .line 1792
    invoke-virtual {p0}, Landroid/widget/TextView;->notifyAccessibilityStateChanged()V

    #@44
    goto :goto_5

    #@45
    .restart local v0       #method2:Landroid/text/method/TransformationMethod2;
    :cond_45
    move v1, v2

    #@46
    .line 1783
    goto :goto_2f

    #@47
    .line 1786
    .end local v0           #method2:Landroid/text/method/TransformationMethod2;
    :cond_47
    iput-boolean v2, p0, Landroid/widget/TextView;->mAllowTransformationLengthChange:Z

    #@49
    goto :goto_36
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .parameter "tf"

    #@0
    .prologue
    .line 2629
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    #@5
    move-result-object v0

    #@6
    if-eq v0, p1, :cond_1a

    #@8
    .line 2630
    iget-object v0, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@a
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@d
    .line 2632
    iget-object v0, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 2633
    invoke-direct {p0}, Landroid/widget/TextView;->nullLayouts()V

    #@14
    .line 2634
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@17
    .line 2635
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@1a
    .line 2638
    :cond_1a
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;I)V
    .registers 9
    .parameter "tf"
    .parameter "style"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1535
    if-lez p2, :cond_36

    #@4
    .line 1536
    if-nez p1, :cond_2d

    #@6
    .line 1537
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    #@9
    move-result-object p1

    #@a
    .line 1542
    :goto_a
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@d
    .line 1544
    if-eqz p1, :cond_32

    #@f
    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    #@12
    move-result v1

    #@13
    .line 1545
    .local v1, typefaceStyle:I
    :goto_13
    xor-int/lit8 v4, v1, -0x1

    #@15
    and-int v0, p2, v4

    #@17
    .line 1546
    .local v0, need:I
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@19
    and-int/lit8 v5, v0, 0x1

    #@1b
    if-eqz v5, :cond_1e

    #@1d
    const/4 v2, 0x1

    #@1e
    :cond_1e
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@21
    .line 1547
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@23
    and-int/lit8 v2, v0, 0x2

    #@25
    if-eqz v2, :cond_34

    #@27
    const/high16 v2, -0x4180

    #@29
    :goto_29
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@2c
    .line 1553
    .end local v0           #need:I
    .end local v1           #typefaceStyle:I
    :goto_2c
    return-void

    #@2d
    .line 1539
    :cond_2d
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@30
    move-result-object p1

    #@31
    goto :goto_a

    #@32
    :cond_32
    move v1, v2

    #@33
    .line 1544
    goto :goto_13

    #@34
    .restart local v0       #need:I
    .restart local v1       #typefaceStyle:I
    :cond_34
    move v2, v3

    #@35
    .line 1547
    goto :goto_29

    #@36
    .line 1549
    .end local v0           #need:I
    .end local v1           #typefaceStyle:I
    :cond_36
    iget-object v4, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@38
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@3b
    .line 1550
    iget-object v2, p0, Landroid/widget/TextView;->mTextPaint:Landroid/text/TextPaint;

    #@3d
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@40
    .line 1551
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@43
    goto :goto_2c
.end method

.method public setWidth(I)V
    .registers 3
    .parameter "pixels"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 3381
    iput p1, p0, Landroid/widget/TextView;->mMinWidth:I

    #@2
    iput p1, p0, Landroid/widget/TextView;->mMaxWidth:I

    #@4
    .line 3382
    const/4 v0, 0x2

    #@5
    iput v0, p0, Landroid/widget/TextView;->mMinWidthMode:I

    #@7
    iput v0, p0, Landroid/widget/TextView;->mMaxWidthMode:I

    #@9
    .line 3384
    invoke-virtual {p0}, Landroid/widget/TextView;->requestLayout()V

    #@c
    .line 3385
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@f
    .line 3386
    return-void
.end method

.method spanChange(Landroid/text/Spanned;Ljava/lang/Object;IIII)V
    .registers 15
    .parameter "buf"
    .parameter "what"
    .parameter "oldStart"
    .parameter "newStart"
    .parameter "oldEnd"
    .parameter "newEnd"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 7608
    const/4 v4, 0x0

    #@2
    .line 7609
    .local v4, selChanged:Z
    const/4 v3, -0x1

    #@3
    .local v3, newSelStart:I
    const/4 v2, -0x1

    #@4
    .line 7611
    .local v2, newSelEnd:I
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@6
    if-nez v5, :cond_f8

    #@8
    const/4 v1, 0x0

    #@9
    .line 7613
    .local v1, ims:Landroid/widget/Editor$InputMethodState;
    :goto_9
    sget-object v5, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@b
    if-ne p2, v5, :cond_29

    #@d
    .line 7614
    const/4 v4, 0x1

    #@e
    .line 7615
    move v2, p4

    #@f
    .line 7617
    if-gez p3, :cond_13

    #@11
    if-ltz p4, :cond_29

    #@13
    .line 7618
    :cond_13
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@16
    move-result v5

    #@17
    invoke-direct {p0, v5, p3, p4}, Landroid/widget/TextView;->invalidateCursor(III)V

    #@1a
    .line 7619
    invoke-direct {p0}, Landroid/widget/TextView;->checkForResize()V

    #@1d
    .line 7620
    invoke-direct {p0}, Landroid/widget/TextView;->registerForPreDraw()V

    #@20
    .line 7621
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@22
    if-eqz v5, :cond_29

    #@24
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@26
    invoke-virtual {v5}, Landroid/widget/Editor;->makeBlink()V

    #@29
    .line 7625
    :cond_29
    sget-object v5, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    #@2b
    if-ne p2, v5, :cond_3a

    #@2d
    .line 7626
    const/4 v4, 0x1

    #@2e
    .line 7627
    move v3, p4

    #@2f
    .line 7629
    if-gez p3, :cond_33

    #@31
    if-ltz p4, :cond_3a

    #@33
    .line 7630
    :cond_33
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@36
    move-result v0

    #@37
    .line 7631
    .local v0, end:I
    invoke-direct {p0, v0, p3, p4}, Landroid/widget/TextView;->invalidateCursor(III)V

    #@3a
    .line 7635
    .end local v0           #end:I
    :cond_3a
    if-eqz v4, :cond_63

    #@3c
    .line 7636
    iput-boolean v7, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@3e
    .line 7637
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@40
    if-eqz v5, :cond_4c

    #@42
    invoke-virtual {p0}, Landroid/widget/TextView;->isFocused()Z

    #@45
    move-result v5

    #@46
    if-nez v5, :cond_4c

    #@48
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@4a
    iput-boolean v7, v5, Landroid/widget/Editor;->mSelectionMoved:Z

    #@4c
    .line 7639
    :cond_4c
    invoke-interface {p1, p2}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@4f
    move-result v5

    #@50
    and-int/lit16 v5, v5, 0x200

    #@52
    if-nez v5, :cond_63

    #@54
    .line 7640
    if-gez v3, :cond_5a

    #@56
    .line 7641
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@59
    move-result v3

    #@5a
    .line 7643
    :cond_5a
    if-gez v2, :cond_60

    #@5c
    .line 7644
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@5f
    move-result v2

    #@60
    .line 7646
    :cond_60
    invoke-virtual {p0, v3, v2}, Landroid/widget/TextView;->onSelectionChanged(II)V

    #@63
    .line 7650
    :cond_63
    instance-of v5, p2, Landroid/text/style/UpdateAppearance;

    #@65
    if-nez v5, :cond_6f

    #@67
    instance-of v5, p2, Landroid/text/style/ParagraphStyle;

    #@69
    if-nez v5, :cond_6f

    #@6b
    instance-of v5, p2, Landroid/text/style/CharacterStyle;

    #@6d
    if-eqz v5, :cond_93

    #@6f
    .line 7652
    :cond_6f
    if-eqz v1, :cond_75

    #@71
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@73
    if-nez v5, :cond_fe

    #@75
    .line 7653
    :cond_75
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@78
    .line 7654
    iput-boolean v7, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@7a
    .line 7655
    invoke-direct {p0}, Landroid/widget/TextView;->checkForResize()V

    #@7d
    .line 7659
    :goto_7d
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@7f
    if-eqz v5, :cond_93

    #@81
    .line 7660
    if-ltz p3, :cond_8a

    #@83
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@85
    iget-object v6, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@87
    invoke-virtual {v5, v6, p3, p5}, Landroid/widget/Editor;->invalidateTextDisplayList(Landroid/text/Layout;II)V

    #@8a
    .line 7661
    :cond_8a
    if-ltz p4, :cond_93

    #@8c
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@8e
    iget-object v6, p0, Landroid/widget/TextView;->mLayout:Landroid/text/Layout;

    #@90
    invoke-virtual {v5, v6, p4, p6}, Landroid/widget/Editor;->invalidateTextDisplayList(Landroid/text/Layout;II)V

    #@93
    .line 7665
    :cond_93
    invoke-static {p1, p2}, Landroid/text/method/MetaKeyKeyListener;->isMetaTracker(Ljava/lang/CharSequence;Ljava/lang/Object;)Z

    #@96
    move-result v5

    #@97
    if-eqz v5, :cond_b4

    #@99
    .line 7666
    iput-boolean v7, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@9b
    .line 7667
    if-eqz v1, :cond_a5

    #@9d
    invoke-static {p1, p2}, Landroid/text/method/MetaKeyKeyListener;->isSelectingMetaTracker(Ljava/lang/CharSequence;Ljava/lang/Object;)Z

    #@a0
    move-result v5

    #@a1
    if-eqz v5, :cond_a5

    #@a3
    .line 7668
    iput-boolean v7, v1, Landroid/widget/Editor$InputMethodState;->mSelectionModeChanged:Z

    #@a5
    .line 7671
    :cond_a5
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@a8
    move-result v5

    #@a9
    if-ltz v5, :cond_b4

    #@ab
    .line 7672
    if-eqz v1, :cond_b1

    #@ad
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@af
    if-nez v5, :cond_102

    #@b1
    .line 7673
    :cond_b1
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidateCursor()V

    #@b4
    .line 7680
    :cond_b4
    :goto_b4
    instance-of v5, p2, Landroid/text/ParcelableSpan;

    #@b6
    if-eqz v5, :cond_de

    #@b8
    .line 7683
    if-eqz v1, :cond_de

    #@ba
    iget-object v5, v1, Landroid/widget/Editor$InputMethodState;->mExtractedTextRequest:Landroid/view/inputmethod/ExtractedTextRequest;

    #@bc
    if-eqz v5, :cond_de

    #@be
    .line 7684
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@c0
    if-eqz v5, :cond_105

    #@c2
    .line 7685
    if-ltz p3, :cond_d0

    #@c4
    .line 7686
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@c6
    if-le v5, p3, :cond_ca

    #@c8
    .line 7687
    iput p3, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@ca
    .line 7689
    :cond_ca
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@cc
    if-le v5, p5, :cond_d0

    #@ce
    .line 7690
    iput p5, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@d0
    .line 7693
    :cond_d0
    if-ltz p4, :cond_de

    #@d2
    .line 7694
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@d4
    if-le v5, p4, :cond_d8

    #@d6
    .line 7695
    iput p4, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@d8
    .line 7697
    :cond_d8
    iget v5, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@da
    if-le v5, p6, :cond_de

    #@dc
    .line 7698
    iput p6, v1, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@de
    .line 7710
    :cond_de
    :goto_de
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@e0
    if-eqz v5, :cond_f7

    #@e2
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@e4
    iget-object v5, v5, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@e6
    if-eqz v5, :cond_f7

    #@e8
    if-gez p4, :cond_f7

    #@ea
    instance-of v5, p2, Landroid/text/style/SpellCheckSpan;

    #@ec
    if-eqz v5, :cond_f7

    #@ee
    .line 7712
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@f0
    iget-object v5, v5, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@f2
    check-cast p2, Landroid/text/style/SpellCheckSpan;

    #@f4
    .end local p2
    invoke-virtual {v5, p2}, Landroid/widget/SpellChecker;->onSpellCheckSpanRemoved(Landroid/text/style/SpellCheckSpan;)V

    #@f7
    .line 7714
    :cond_f7
    return-void

    #@f8
    .line 7611
    .end local v1           #ims:Landroid/widget/Editor$InputMethodState;
    .restart local p2
    :cond_f8
    iget-object v5, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@fa
    iget-object v1, v5, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@fc
    goto/16 :goto_9

    #@fe
    .line 7657
    .restart local v1       #ims:Landroid/widget/Editor$InputMethodState;
    :cond_fe
    iput-boolean v7, v1, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@100
    goto/16 :goto_7d

    #@102
    .line 7675
    :cond_102
    iput-boolean v7, v1, Landroid/widget/Editor$InputMethodState;->mCursorChanged:Z

    #@104
    goto :goto_b4

    #@105
    .line 7705
    :cond_105
    iput-boolean v7, v1, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@107
    goto :goto_de
.end method

.method protected stopSelectionActionMode()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 8561
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@3
    if-eqz v0, :cond_20

    #@5
    iget-object v0, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@7
    instance-of v0, v0, Landroid/text/Spannable;

    #@9
    if-eqz v0, :cond_20

    #@b
    .line 8562
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/text/Spannable;

    #@11
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    #@14
    move-result v1

    #@15
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@18
    .line 8563
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setHasTransientState(Z)V

    #@1b
    .line 8564
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1d
    invoke-virtual {v0, v2}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@20
    .line 8566
    :cond_20
    iget-object v0, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@22
    invoke-virtual {v0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@25
    .line 8567
    return-void
.end method

.method textCanBeSelected()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 8163
    iget-object v1, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@3
    if-eqz v1, :cond_d

    #@5
    iget-object v1, p0, Landroid/widget/TextView;->mMovement:Landroid/text/method/MovementMethod;

    #@7
    invoke-interface {v1}, Landroid/text/method/MovementMethod;->canSelectArbitrarily()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_e

    #@d
    .line 8164
    :cond_d
    :goto_d
    return v0

    #@e
    :cond_e
    invoke-virtual {p0}, Landroid/widget/TextView;->isTextEditable()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_26

    #@14
    invoke-virtual {p0}, Landroid/widget/TextView;->isTextSelectable()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_d

    #@1a
    iget-object v1, p0, Landroid/widget/TextView;->mText:Ljava/lang/CharSequence;

    #@1c
    instance-of v1, v1, Landroid/text/Spannable;

    #@1e
    if-eqz v1, :cond_d

    #@20
    invoke-virtual {p0}, Landroid/widget/TextView;->isEnabled()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_d

    #@26
    :cond_26
    const/4 v0, 0x1

    #@27
    goto :goto_d
.end method

.method updateAfterEdit()V
    .registers 4

    #@0
    .prologue
    .line 7559
    invoke-virtual {p0}, Landroid/widget/TextView;->invalidate()V

    #@3
    .line 7560
    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionStart()I

    #@6
    move-result v0

    #@7
    .line 7562
    .local v0, curs:I
    if-gez v0, :cond_11

    #@9
    iget v1, p0, Landroid/widget/TextView;->mGravity:I

    #@b
    and-int/lit8 v1, v1, 0x70

    #@d
    const/16 v2, 0x50

    #@f
    if-ne v1, v2, :cond_14

    #@11
    .line 7563
    :cond_11
    invoke-direct {p0}, Landroid/widget/TextView;->registerForPreDraw()V

    #@14
    .line 7566
    :cond_14
    invoke-direct {p0}, Landroid/widget/TextView;->checkForResize()V

    #@17
    .line 7568
    if-ltz v0, :cond_28

    #@19
    .line 7569
    const/4 v1, 0x1

    #@1a
    iput-boolean v1, p0, Landroid/widget/TextView;->mHighlightPathBogus:Z

    #@1c
    .line 7570
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@1e
    if-eqz v1, :cond_25

    #@20
    iget-object v1, p0, Landroid/widget/TextView;->mEditor:Landroid/widget/Editor;

    #@22
    invoke-virtual {v1}, Landroid/widget/Editor;->makeBlink()V

    #@25
    .line 7571
    :cond_25
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->bringPointIntoView(I)Z

    #@28
    .line 7573
    :cond_28
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 4
    .parameter "who"

    #@0
    .prologue
    .line 4891
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    .line 4892
    .local v0, verified:Z
    if-nez v0, :cond_32

    #@6
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@8
    if-eqz v1, :cond_32

    #@a
    .line 4893
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@c
    iget-object v1, v1, Landroid/widget/TextView$Drawables;->mDrawableLeft:Landroid/graphics/drawable/Drawable;

    #@e
    if-eq p1, v1, :cond_2e

    #@10
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@12
    iget-object v1, v1, Landroid/widget/TextView$Drawables;->mDrawableTop:Landroid/graphics/drawable/Drawable;

    #@14
    if-eq p1, v1, :cond_2e

    #@16
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@18
    iget-object v1, v1, Landroid/widget/TextView$Drawables;->mDrawableRight:Landroid/graphics/drawable/Drawable;

    #@1a
    if-eq p1, v1, :cond_2e

    #@1c
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@1e
    iget-object v1, v1, Landroid/widget/TextView$Drawables;->mDrawableBottom:Landroid/graphics/drawable/Drawable;

    #@20
    if-eq p1, v1, :cond_2e

    #@22
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@24
    iget-object v1, v1, Landroid/widget/TextView$Drawables;->mDrawableStart:Landroid/graphics/drawable/Drawable;

    #@26
    if-eq p1, v1, :cond_2e

    #@28
    iget-object v1, p0, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@2a
    iget-object v1, v1, Landroid/widget/TextView$Drawables;->mDrawableEnd:Landroid/graphics/drawable/Drawable;

    #@2c
    if-ne p1, v1, :cond_30

    #@2e
    :cond_2e
    const/4 v1, 0x1

    #@2f
    .line 4897
    :goto_2f
    return v1

    #@30
    .line 4893
    :cond_30
    const/4 v1, 0x0

    #@31
    goto :goto_2f

    #@32
    :cond_32
    move v1, v0

    #@33
    .line 4897
    goto :goto_2f
.end method

.method protected viewClicked(Landroid/view/inputmethod/InputMethodManager;)V
    .registers 2
    .parameter "imm"

    #@0
    .prologue
    .line 8946
    if-eqz p1, :cond_5

    #@2
    .line 8947
    invoke-virtual {p1, p0}, Landroid/view/inputmethod/InputMethodManager;->viewClicked(Landroid/view/View;)V

    #@5
    .line 8949
    :cond_5
    return-void
.end method

.method viewportToContentHorizontalOffset()I
    .registers 3

    #@0
    .prologue
    .line 7118
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/widget/TextView;->mScrollX:I

    #@6
    sub-int/2addr v0, v1

    #@7
    return v0
.end method

.method viewportToContentVerticalOffset()I
    .registers 4

    #@0
    .prologue
    .line 7122
    invoke-virtual {p0}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@3
    move-result v1

    #@4
    iget v2, p0, Landroid/widget/TextView;->mScrollY:I

    #@6
    sub-int v0, v1, v2

    #@8
    .line 7123
    .local v0, offset:I
    iget v1, p0, Landroid/widget/TextView;->mGravity:I

    #@a
    and-int/lit8 v1, v1, 0x70

    #@c
    const/16 v2, 0x30

    #@e
    if-eq v1, v2, :cond_16

    #@10
    .line 7124
    const/4 v1, 0x0

    #@11
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@14
    move-result v1

    #@15
    add-int/2addr v0, v1

    #@16
    .line 7126
    :cond_16
    return v0
.end method
