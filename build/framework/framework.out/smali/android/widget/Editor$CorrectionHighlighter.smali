.class Landroid/widget/Editor$CorrectionHighlighter;
.super Ljava/lang/Object;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CorrectionHighlighter"
.end annotation


# static fields
.field private static final FADE_OUT_DURATION:I = 0x190


# instance fields
.field private mEnd:I

.field private mFadingStartTime:J

.field private final mPaint:Landroid/graphics/Paint;

.field private final mPath:Landroid/graphics/Path;

.field private mStart:I

.field private mTempRectF:Landroid/graphics/RectF;

.field final synthetic this$0:Landroid/widget/Editor;


# direct methods
.method public constructor <init>(Landroid/widget/Editor;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 4815
    iput-object p1, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4808
    new-instance v0, Landroid/graphics/Path;

    #@7
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    #@a
    iput-object v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPath:Landroid/graphics/Path;

    #@c
    .line 4809
    new-instance v0, Landroid/graphics/Paint;

    #@e
    const/4 v1, 0x1

    #@f
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    #@12
    iput-object v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPaint:Landroid/graphics/Paint;

    #@14
    .line 4816
    iget-object v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPaint:Landroid/graphics/Paint;

    #@16
    invoke-static {p1}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@21
    move-result-object v1

    #@22
    iget v1, v1, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@24
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setCompatibilityScaling(F)V

    #@27
    .line 4818
    iget-object v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPaint:Landroid/graphics/Paint;

    #@29
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@2b
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@2e
    .line 4819
    return-void
.end method

.method static synthetic access$500(Landroid/widget/Editor$CorrectionHighlighter;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 4807
    invoke-direct {p0, p1}, Landroid/widget/Editor$CorrectionHighlighter;->invalidate(Z)V

    #@3
    return-void
.end method

.method private invalidate(Z)V
    .registers 9
    .parameter "delayed"

    #@0
    .prologue
    .line 4876
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v2

    #@a
    if-nez v2, :cond_d

    #@c
    .line 4892
    :goto_c
    return-void

    #@d
    .line 4878
    :cond_d
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@f
    if-nez v2, :cond_18

    #@11
    new-instance v2, Landroid/graphics/RectF;

    #@13
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    #@16
    iput-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@18
    .line 4879
    :cond_18
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPath:Landroid/graphics/Path;

    #@1a
    iget-object v3, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@20
    .line 4881
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@22
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    #@29
    move-result v0

    #@2a
    .line 4882
    .local v0, left:I
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@2c
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    #@33
    move-result v2

    #@34
    iget-object v3, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@36
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@39
    move-result-object v3

    #@3a
    const/4 v4, 0x1

    #@3b
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->getVerticalOffset(Z)I

    #@3e
    move-result v3

    #@3f
    add-int v1, v2, v3

    #@41
    .line 4884
    .local v1, top:I
    if-eqz p1, :cond_65

    #@43
    .line 4885
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@45
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@48
    move-result-object v2

    #@49
    iget-object v3, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@4b
    iget v3, v3, Landroid/graphics/RectF;->left:F

    #@4d
    float-to-int v3, v3

    #@4e
    add-int/2addr v3, v0

    #@4f
    iget-object v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@51
    iget v4, v4, Landroid/graphics/RectF;->top:F

    #@53
    float-to-int v4, v4

    #@54
    add-int/2addr v4, v1

    #@55
    iget-object v5, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@57
    iget v5, v5, Landroid/graphics/RectF;->right:F

    #@59
    float-to-int v5, v5

    #@5a
    add-int/2addr v5, v0

    #@5b
    iget-object v6, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@5d
    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    #@5f
    float-to-int v6, v6

    #@60
    add-int/2addr v6, v1

    #@61
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/TextView;->postInvalidateOnAnimation(IIII)V

    #@64
    goto :goto_c

    #@65
    .line 4889
    :cond_65
    iget-object v2, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@67
    invoke-static {v2}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@6a
    move-result-object v2

    #@6b
    iget-object v3, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@6d
    iget v3, v3, Landroid/graphics/RectF;->left:F

    #@6f
    float-to-int v3, v3

    #@70
    iget-object v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@72
    iget v4, v4, Landroid/graphics/RectF;->top:F

    #@74
    float-to-int v4, v4

    #@75
    iget-object v5, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@77
    iget v5, v5, Landroid/graphics/RectF;->right:F

    #@79
    float-to-int v5, v5

    #@7a
    iget-object v6, p0, Landroid/widget/Editor$CorrectionHighlighter;->mTempRectF:Landroid/graphics/RectF;

    #@7c
    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    #@7e
    float-to-int v6, v6

    #@7f
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/TextView;->postInvalidate(IIII)V

    #@82
    goto :goto_c
.end method

.method private stopAnimation()V
    .registers 3

    #@0
    .prologue
    .line 4895
    iget-object v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@2
    const/4 v1, 0x0

    #@3
    iput-object v1, v0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@5
    .line 4896
    return-void
.end method

.method private updatePaint()Z
    .registers 10

    #@0
    .prologue
    .line 4850
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v5

    #@4
    iget-wide v7, p0, Landroid/widget/Editor$CorrectionHighlighter;->mFadingStartTime:J

    #@6
    sub-long v2, v5, v7

    #@8
    .line 4851
    .local v2, duration:J
    const-wide/16 v5, 0x190

    #@a
    cmp-long v5, v2, v5

    #@c
    if-lez v5, :cond_10

    #@e
    const/4 v5, 0x0

    #@f
    .line 4858
    :goto_f
    return v5

    #@10
    .line 4853
    :cond_10
    const/high16 v5, 0x3f80

    #@12
    long-to-float v6, v2

    #@13
    const/high16 v7, 0x43c8

    #@15
    div-float/2addr v6, v7

    #@16
    sub-float v0, v5, v6

    #@18
    .line 4854
    .local v0, coef:F
    iget-object v5, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@1a
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@1d
    move-result-object v5

    #@1e
    iget v5, v5, Landroid/widget/TextView;->mHighlightColor:I

    #@20
    invoke-static {v5}, Landroid/graphics/Color;->alpha(I)I

    #@23
    move-result v4

    #@24
    .line 4855
    .local v4, highlightColorAlpha:I
    iget-object v5, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@26
    invoke-static {v5}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@29
    move-result-object v5

    #@2a
    iget v5, v5, Landroid/widget/TextView;->mHighlightColor:I

    #@2c
    const v6, 0xffffff

    #@2f
    and-int/2addr v5, v6

    #@30
    int-to-float v6, v4

    #@31
    mul-float/2addr v6, v0

    #@32
    float-to-int v6, v6

    #@33
    shl-int/lit8 v6, v6, 0x18

    #@35
    add-int v1, v5, v6

    #@37
    .line 4857
    .local v1, color:I
    iget-object v5, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPaint:Landroid/graphics/Paint;

    #@39
    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@3c
    .line 4858
    const/4 v5, 0x1

    #@3d
    goto :goto_f
.end method

.method private updatePath()Z
    .registers 6

    #@0
    .prologue
    .line 4862
    iget-object v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@2
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@9
    move-result-object v1

    #@a
    .line 4863
    .local v1, layout:Landroid/text/Layout;
    if-nez v1, :cond_e

    #@c
    const/4 v4, 0x0

    #@d
    .line 4872
    :goto_d
    return v4

    #@e
    .line 4866
    :cond_e
    iget-object v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->this$0:Landroid/widget/Editor;

    #@10
    invoke-static {v4}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@17
    move-result-object v4

    #@18
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@1b
    move-result v2

    #@1c
    .line 4867
    .local v2, length:I
    iget v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->mStart:I

    #@1e
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    #@21
    move-result v3

    #@22
    .line 4868
    .local v3, start:I
    iget v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->mEnd:I

    #@24
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    #@27
    move-result v0

    #@28
    .line 4870
    .local v0, end:I
    iget-object v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPath:Landroid/graphics/Path;

    #@2a
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    #@2d
    .line 4871
    iget-object v4, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPath:Landroid/graphics/Path;

    #@2f
    invoke-virtual {v1, v3, v0, v4}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    #@32
    .line 4872
    const/4 v4, 0x1

    #@33
    goto :goto_d
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;I)V
    .registers 6
    .parameter "canvas"
    .parameter "cursorOffsetVertical"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4832
    invoke-direct {p0}, Landroid/widget/Editor$CorrectionHighlighter;->updatePath()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_26

    #@7
    invoke-direct {p0}, Landroid/widget/Editor$CorrectionHighlighter;->updatePaint()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_26

    #@d
    .line 4833
    if-eqz p2, :cond_13

    #@f
    .line 4834
    int-to-float v0, p2

    #@10
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    #@13
    .line 4837
    :cond_13
    iget-object v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPath:Landroid/graphics/Path;

    #@15
    iget-object v1, p0, Landroid/widget/Editor$CorrectionHighlighter;->mPaint:Landroid/graphics/Paint;

    #@17
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@1a
    .line 4839
    if-eqz p2, :cond_21

    #@1c
    .line 4840
    neg-int v0, p2

    #@1d
    int-to-float v0, v0

    #@1e
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    #@21
    .line 4842
    :cond_21
    const/4 v0, 0x1

    #@22
    invoke-direct {p0, v0}, Landroid/widget/Editor$CorrectionHighlighter;->invalidate(Z)V

    #@25
    .line 4847
    :goto_25
    return-void

    #@26
    .line 4844
    :cond_26
    invoke-direct {p0}, Landroid/widget/Editor$CorrectionHighlighter;->stopAnimation()V

    #@29
    .line 4845
    const/4 v0, 0x0

    #@2a
    invoke-direct {p0, v0}, Landroid/widget/Editor$CorrectionHighlighter;->invalidate(Z)V

    #@2d
    goto :goto_25
.end method

.method public highlight(Landroid/view/inputmethod/CorrectionInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 4822
    invoke-virtual {p1}, Landroid/view/inputmethod/CorrectionInfo;->getOffset()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mStart:I

    #@6
    .line 4823
    iget v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mStart:I

    #@8
    invoke-virtual {p1}, Landroid/view/inputmethod/CorrectionInfo;->getNewText()Ljava/lang/CharSequence;

    #@b
    move-result-object v1

    #@c
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@f
    move-result v1

    #@10
    add-int/2addr v0, v1

    #@11
    iput v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mEnd:I

    #@13
    .line 4824
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@16
    move-result-wide v0

    #@17
    iput-wide v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mFadingStartTime:J

    #@19
    .line 4826
    iget v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mStart:I

    #@1b
    if-ltz v0, :cond_21

    #@1d
    iget v0, p0, Landroid/widget/Editor$CorrectionHighlighter;->mEnd:I

    #@1f
    if-gez v0, :cond_24

    #@21
    .line 4827
    :cond_21
    invoke-direct {p0}, Landroid/widget/Editor$CorrectionHighlighter;->stopAnimation()V

    #@24
    .line 4829
    :cond_24
    return-void
.end method
