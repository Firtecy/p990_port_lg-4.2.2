.class Landroid/widget/StackView$StackSlider;
.super Ljava/lang/Object;
.source "StackView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/StackView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StackSlider"
.end annotation


# static fields
.field static final BEGINNING_OF_STACK_MODE:I = 0x1

.field static final END_OF_STACK_MODE:I = 0x2

.field static final NORMAL_MODE:I


# instance fields
.field mMode:I

.field mView:Landroid/view/View;

.field mXProgress:F

.field mYProgress:F

.field final synthetic this$0:Landroid/widget/StackView;


# direct methods
.method public constructor <init>(Landroid/widget/StackView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 900
    iput-object p1, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 898
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/widget/StackView$StackSlider;->mMode:I

    #@8
    .line 901
    return-void
.end method

.method public constructor <init>(Landroid/widget/StackView;Landroid/widget/StackView$StackSlider;)V
    .registers 4
    .parameter
    .parameter "copy"

    #@0
    .prologue
    .line 903
    iput-object p1, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 898
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/widget/StackView$StackSlider;->mMode:I

    #@8
    .line 904
    iget-object v0, p2, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@a
    iput-object v0, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@c
    .line 905
    iget v0, p2, Landroid/widget/StackView$StackSlider;->mYProgress:F

    #@e
    iput v0, p0, Landroid/widget/StackView$StackSlider;->mYProgress:F

    #@10
    .line 906
    iget v0, p2, Landroid/widget/StackView$StackSlider;->mXProgress:F

    #@12
    iput v0, p0, Landroid/widget/StackView$StackSlider;->mXProgress:F

    #@14
    .line 907
    iget v0, p2, Landroid/widget/StackView$StackSlider;->mMode:I

    #@16
    iput v0, p0, Landroid/widget/StackView$StackSlider;->mMode:I

    #@18
    .line 908
    return-void
.end method

.method private cubic(F)F
    .registers 7
    .parameter "r"

    #@0
    .prologue
    const/high16 v4, 0x4000

    #@2
    .line 911
    mul-float v0, v4, p1

    #@4
    const/high16 v1, 0x3f80

    #@6
    sub-float/2addr v0, v1

    #@7
    float-to-double v0, v0

    #@8
    const-wide/high16 v2, 0x4008

    #@a
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    #@d
    move-result-wide v0

    #@e
    const-wide/high16 v2, 0x3ff0

    #@10
    add-double/2addr v0, v2

    #@11
    double-to-float v0, v0

    #@12
    div-float/2addr v0, v4

    #@13
    return v0
.end method

.method private getDuration(ZF)F
    .registers 15
    .parameter "invert"
    .parameter "velocity"

    #@0
    .prologue
    const/high16 v11, 0x43c8

    #@2
    const/4 v4, 0x0

    #@3
    const-wide/high16 v9, 0x4000

    #@5
    .line 1043
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@7
    if-eqz v5, :cond_77

    #@9
    .line 1044
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@b
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Landroid/widget/StackView$LayoutParams;

    #@11
    .line 1046
    .local v3, viewLp:Landroid/widget/StackView$LayoutParams;
    iget v5, v3, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@13
    int-to-double v5, v5

    #@14
    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->pow(DD)D

    #@17
    move-result-wide v5

    #@18
    iget v7, v3, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@1a
    int-to-double v7, v7

    #@1b
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    #@1e
    move-result-wide v7

    #@1f
    add-double/2addr v5, v7

    #@20
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    #@23
    move-result-wide v5

    #@24
    double-to-float v0, v5

    #@25
    .line 1048
    .local v0, d:F
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@27
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@2a
    move-result v5

    #@2b
    int-to-double v5, v5

    #@2c
    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->pow(DD)D

    #@2f
    move-result-wide v5

    #@30
    const v7, 0x3ecccccd

    #@33
    iget-object v8, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@35
    invoke-static {v8}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@38
    move-result v8

    #@39
    int-to-float v8, v8

    #@3a
    mul-float/2addr v7, v8

    #@3b
    float-to-double v7, v7

    #@3c
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    #@3f
    move-result-wide v7

    #@40
    add-double/2addr v5, v7

    #@41
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    #@44
    move-result-wide v5

    #@45
    double-to-float v2, v5

    #@46
    .line 1051
    .local v2, maxd:F
    cmpl-float v5, p2, v4

    #@48
    if-nez v5, :cond_57

    #@4a
    .line 1052
    if-eqz p1, :cond_54

    #@4c
    const/high16 v4, 0x3f80

    #@4e
    div-float v5, v0, v2

    #@50
    sub-float/2addr v4, v5

    #@51
    :goto_51
    mul-float v1, v4, v11

    #@53
    .line 1064
    .end local v0           #d:F
    .end local v2           #maxd:F
    .end local v3           #viewLp:Landroid/widget/StackView$LayoutParams;
    :cond_53
    :goto_53
    return v1

    #@54
    .line 1052
    .restart local v0       #d:F
    .restart local v2       #maxd:F
    .restart local v3       #viewLp:Landroid/widget/StackView$LayoutParams;
    :cond_54
    div-float v4, v0, v2

    #@56
    goto :goto_51

    #@57
    .line 1054
    :cond_57
    if-eqz p1, :cond_6e

    #@59
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    #@5c
    move-result v5

    #@5d
    div-float v1, v0, v5

    #@5f
    .line 1056
    .local v1, duration:F
    :goto_5f
    const/high16 v5, 0x4248

    #@61
    cmpg-float v5, v1, v5

    #@63
    if-ltz v5, :cond_69

    #@65
    cmpl-float v5, v1, v11

    #@67
    if-lez v5, :cond_53

    #@69
    .line 1058
    :cond_69
    invoke-direct {p0, p1, v4}, Landroid/widget/StackView$StackSlider;->getDuration(ZF)F

    #@6c
    move-result v1

    #@6d
    goto :goto_53

    #@6e
    .line 1054
    .end local v1           #duration:F
    :cond_6e
    sub-float v5, v2, v0

    #@70
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    #@73
    move-result v6

    #@74
    div-float v1, v5, v6

    #@76
    goto :goto_5f

    #@77
    .end local v0           #d:F
    .end local v2           #maxd:F
    .end local v3           #viewLp:Landroid/widget/StackView$LayoutParams;
    :cond_77
    move v1, v4

    #@78
    .line 1064
    goto :goto_53
.end method

.method private highlightAlphaInterpolator(F)F
    .registers 7
    .parameter "r"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    const v3, 0x3f59999a

    #@5
    .line 915
    const v0, 0x3ecccccd

    #@8
    .line 916
    .local v0, pivot:F
    cmpg-float v1, p1, v0

    #@a
    if-gez v1, :cond_14

    #@c
    .line 917
    div-float v1, p1, v0

    #@e
    invoke-direct {p0, v1}, Landroid/widget/StackView$StackSlider;->cubic(F)F

    #@11
    move-result v1

    #@12
    mul-float/2addr v1, v3

    #@13
    .line 919
    :goto_13
    return v1

    #@14
    :cond_14
    sub-float v1, p1, v0

    #@16
    sub-float v2, v4, v0

    #@18
    div-float/2addr v1, v2

    #@19
    sub-float v1, v4, v1

    #@1b
    invoke-direct {p0, v1}, Landroid/widget/StackView$StackSlider;->cubic(F)F

    #@1e
    move-result v1

    #@1f
    mul-float/2addr v1, v3

    #@20
    goto :goto_13
.end method

.method private rotationInterpolator(F)F
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 933
    const v0, 0x3e4ccccd

    #@3
    .line 934
    .local v0, pivot:F
    cmpg-float v1, p1, v0

    #@5
    if-gez v1, :cond_9

    #@7
    .line 935
    const/4 v1, 0x0

    #@8
    .line 937
    :goto_8
    return v1

    #@9
    :cond_9
    sub-float v1, p1, v0

    #@b
    const/high16 v2, 0x3f80

    #@d
    sub-float/2addr v2, v0

    #@e
    div-float/2addr v1, v2

    #@f
    goto :goto_8
.end method

.method private viewAlphaInterpolator(F)F
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 924
    const v0, 0x3e99999a

    #@3
    .line 925
    .local v0, pivot:F
    cmpl-float v1, p1, v0

    #@5
    if-lez v1, :cond_e

    #@7
    .line 926
    sub-float v1, p1, v0

    #@9
    const/high16 v2, 0x3f80

    #@b
    sub-float/2addr v2, v0

    #@c
    div-float/2addr v1, v2

    #@d
    .line 928
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method


# virtual methods
.method getDurationForNeutralPosition()F
    .registers 3

    #@0
    .prologue
    .line 1027
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, v0, v1}, Landroid/widget/StackView$StackSlider;->getDuration(ZF)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getDurationForNeutralPosition(F)F
    .registers 3
    .parameter "velocity"

    #@0
    .prologue
    .line 1035
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1}, Landroid/widget/StackView$StackSlider;->getDuration(ZF)F

    #@4
    move-result v0

    #@5
    return v0
.end method

.method getDurationForOffscreenPosition()F
    .registers 3

    #@0
    .prologue
    .line 1031
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, v0, v1}, Landroid/widget/StackView$StackSlider;->getDuration(ZF)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getDurationForOffscreenPosition(F)F
    .registers 3
    .parameter "velocity"

    #@0
    .prologue
    .line 1039
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0, p1}, Landroid/widget/StackView$StackSlider;->getDuration(ZF)F

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getXProgress()F
    .registers 2

    #@0
    .prologue
    .line 1076
    iget v0, p0, Landroid/widget/StackView$StackSlider;->mXProgress:F

    #@2
    return v0
.end method

.method public getYProgress()F
    .registers 2

    #@0
    .prologue
    .line 1070
    iget v0, p0, Landroid/widget/StackView$StackSlider;->mYProgress:F

    #@2
    return v0
.end method

.method setMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 1023
    iput p1, p0, Landroid/widget/StackView$StackSlider;->mMode:I

    #@2
    .line 1024
    return-void
.end method

.method setView(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 942
    iput-object p1, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@2
    .line 943
    return-void
.end method

.method public setXProgress(F)V
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 1008
    const/high16 v2, 0x4000

    #@2
    invoke-static {v2, p1}, Ljava/lang/Math;->min(FF)F

    #@5
    move-result p1

    #@6
    .line 1009
    const/high16 v2, -0x4000

    #@8
    invoke-static {v2, p1}, Ljava/lang/Math;->max(FF)F

    #@b
    move-result p1

    #@c
    .line 1011
    iput p1, p0, Landroid/widget/StackView$StackSlider;->mXProgress:F

    #@e
    .line 1013
    iget-object v2, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@10
    if-nez v2, :cond_13

    #@12
    .line 1020
    :goto_12
    return-void

    #@13
    .line 1014
    :cond_13
    iget-object v2, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@15
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/widget/StackView$LayoutParams;

    #@1b
    .line 1015
    .local v1, viewLp:Landroid/widget/StackView$LayoutParams;
    iget-object v2, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@1d
    invoke-static {v2}, Landroid/widget/StackView;->access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/widget/StackView$LayoutParams;

    #@27
    .line 1017
    .local v0, highlightLp:Landroid/widget/StackView$LayoutParams;
    const v2, 0x3e4ccccd

    #@2a
    mul-float/2addr p1, v2

    #@2b
    .line 1018
    iget-object v2, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@2d
    invoke-static {v2}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@30
    move-result v2

    #@31
    int-to-float v2, v2

    #@32
    mul-float/2addr v2, p1

    #@33
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    #@36
    move-result v2

    #@37
    invoke-virtual {v1, v2}, Landroid/widget/StackView$LayoutParams;->setHorizontalOffset(I)V

    #@3a
    .line 1019
    iget-object v2, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@3c
    invoke-static {v2}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@3f
    move-result v2

    #@40
    int-to-float v2, v2

    #@41
    mul-float/2addr v2, p1

    #@42
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    #@45
    move-result v2

    #@46
    invoke-virtual {v0, v2}, Landroid/widget/StackView$LayoutParams;->setHorizontalOffset(I)V

    #@49
    goto :goto_12
.end method

.method public setYProgress(F)V
    .registers 14
    .parameter "r"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/high16 v10, 0x42b4

    #@3
    const v9, 0x3e4ccccd

    #@6
    const/high16 v8, 0x3f80

    #@8
    const/4 v7, 0x0

    #@9
    .line 947
    invoke-static {v8, p1}, Ljava/lang/Math;->min(FF)F

    #@c
    move-result p1

    #@d
    .line 948
    invoke-static {v7, p1}, Ljava/lang/Math;->max(FF)F

    #@10
    move-result p1

    #@11
    .line 950
    iput p1, p0, Landroid/widget/StackView$StackSlider;->mYProgress:F

    #@13
    .line 951
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@15
    if-nez v4, :cond_18

    #@17
    .line 1004
    :goto_17
    return-void

    #@18
    .line 953
    :cond_18
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@1a
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Landroid/widget/StackView$LayoutParams;

    #@20
    .line 954
    .local v3, viewLp:Landroid/widget/StackView$LayoutParams;
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@22
    invoke-static {v4}, Landroid/widget/StackView;->access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@29
    move-result-object v1

    #@2a
    check-cast v1, Landroid/widget/StackView$LayoutParams;

    #@2c
    .line 956
    .local v1, highlightLp:Landroid/widget/StackView$LayoutParams;
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@2e
    invoke-static {v4}, Landroid/widget/StackView;->access$100(Landroid/widget/StackView;)I

    #@31
    move-result v4

    #@32
    if-nez v4, :cond_cf

    #@34
    const/4 v2, 0x1

    #@35
    .line 960
    .local v2, stackDirection:I
    :goto_35
    iget v4, p0, Landroid/widget/StackView$StackSlider;->mYProgress:F

    #@37
    invoke-static {v7, v4}, Ljava/lang/Float;->compare(FF)I

    #@3a
    move-result v4

    #@3b
    if-eqz v4, :cond_d2

    #@3d
    iget v4, p0, Landroid/widget/StackView$StackSlider;->mYProgress:F

    #@3f
    invoke-static {v8, v4}, Ljava/lang/Float;->compare(FF)I

    #@42
    move-result v4

    #@43
    if-eqz v4, :cond_d2

    #@45
    .line 961
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@47
    invoke-virtual {v4}, Landroid/view/View;->getLayerType()I

    #@4a
    move-result v4

    #@4b
    if-nez v4, :cond_54

    #@4d
    .line 962
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@4f
    const/4 v5, 0x2

    #@50
    const/4 v6, 0x0

    #@51
    invoke-virtual {v4, v5, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    #@54
    .line 970
    :cond_54
    :goto_54
    iget v4, p0, Landroid/widget/StackView$StackSlider;->mMode:I

    #@56
    packed-switch v4, :pswitch_data_168

    #@59
    goto :goto_17

    #@5a
    .line 972
    :pswitch_5a
    neg-float v4, p1

    #@5b
    int-to-float v5, v2

    #@5c
    mul-float/2addr v4, v5

    #@5d
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@5f
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@62
    move-result v5

    #@63
    int-to-float v5, v5

    #@64
    mul-float/2addr v4, v5

    #@65
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@68
    move-result v4

    #@69
    invoke-virtual {v3, v4}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@6c
    .line 973
    neg-float v4, p1

    #@6d
    int-to-float v5, v2

    #@6e
    mul-float/2addr v4, v5

    #@6f
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@71
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@74
    move-result v5

    #@75
    int-to-float v5, v5

    #@76
    mul-float/2addr v4, v5

    #@77
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@7a
    move-result v4

    #@7b
    invoke-virtual {v1, v4}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@7e
    .line 974
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@80
    invoke-static {v4}, Landroid/widget/StackView;->access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;

    #@83
    move-result-object v4

    #@84
    invoke-direct {p0, p1}, Landroid/widget/StackView$StackSlider;->highlightAlphaInterpolator(F)F

    #@87
    move-result v5

    #@88
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    #@8b
    .line 976
    sub-float v4, v8, p1

    #@8d
    invoke-direct {p0, v4}, Landroid/widget/StackView$StackSlider;->viewAlphaInterpolator(F)F

    #@90
    move-result v0

    #@91
    .line 980
    .local v0, alpha:F
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@93
    invoke-virtual {v4}, Landroid/view/View;->getAlpha()F

    #@96
    move-result v4

    #@97
    cmpl-float v4, v4, v7

    #@99
    if-nez v4, :cond_e2

    #@9b
    cmpl-float v4, v0, v7

    #@9d
    if-eqz v4, :cond_e2

    #@9f
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@a1
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@a4
    move-result v4

    #@a5
    if-eqz v4, :cond_e2

    #@a7
    .line 981
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@a9
    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    #@ac
    .line 987
    :cond_ac
    :goto_ac
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@ae
    invoke-virtual {v4, v0}, Landroid/view/View;->setAlpha(F)V

    #@b1
    .line 988
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@b3
    int-to-float v5, v2

    #@b4
    mul-float/2addr v5, v10

    #@b5
    invoke-direct {p0, p1}, Landroid/widget/StackView$StackSlider;->rotationInterpolator(F)F

    #@b8
    move-result v6

    #@b9
    mul-float/2addr v5, v6

    #@ba
    invoke-virtual {v4, v5}, Landroid/view/View;->setRotationX(F)V

    #@bd
    .line 989
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@bf
    invoke-static {v4}, Landroid/widget/StackView;->access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;

    #@c2
    move-result-object v4

    #@c3
    int-to-float v5, v2

    #@c4
    mul-float/2addr v5, v10

    #@c5
    invoke-direct {p0, p1}, Landroid/widget/StackView$StackSlider;->rotationInterpolator(F)F

    #@c8
    move-result v6

    #@c9
    mul-float/2addr v5, v6

    #@ca
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setRotationX(F)V

    #@cd
    goto/16 :goto_17

    #@cf
    .line 956
    .end local v0           #alpha:F
    .end local v2           #stackDirection:I
    :cond_cf
    const/4 v2, -0x1

    #@d0
    goto/16 :goto_35

    #@d2
    .line 965
    .restart local v2       #stackDirection:I
    :cond_d2
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@d4
    invoke-virtual {v4}, Landroid/view/View;->getLayerType()I

    #@d7
    move-result v4

    #@d8
    if-eqz v4, :cond_54

    #@da
    .line 966
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@dc
    const/4 v5, 0x0

    #@dd
    invoke-virtual {v4, v11, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    #@e0
    goto/16 :goto_54

    #@e2
    .line 982
    .restart local v0       #alpha:F
    :cond_e2
    cmpl-float v4, v0, v7

    #@e4
    if-nez v4, :cond_ac

    #@e6
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@e8
    invoke-virtual {v4}, Landroid/view/View;->getAlpha()F

    #@eb
    move-result v4

    #@ec
    cmpl-float v4, v4, v7

    #@ee
    if-eqz v4, :cond_ac

    #@f0
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@f2
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@f5
    move-result v4

    #@f6
    if-nez v4, :cond_ac

    #@f8
    .line 984
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->mView:Landroid/view/View;

    #@fa
    const/4 v5, 0x4

    #@fb
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    #@fe
    goto :goto_ac

    #@ff
    .line 992
    .end local v0           #alpha:F
    :pswitch_ff
    mul-float/2addr p1, v9

    #@100
    .line 993
    neg-int v4, v2

    #@101
    int-to-float v4, v4

    #@102
    mul-float/2addr v4, p1

    #@103
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@105
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@108
    move-result v5

    #@109
    int-to-float v5, v5

    #@10a
    mul-float/2addr v4, v5

    #@10b
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@10e
    move-result v4

    #@10f
    invoke-virtual {v3, v4}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@112
    .line 994
    neg-int v4, v2

    #@113
    int-to-float v4, v4

    #@114
    mul-float/2addr v4, p1

    #@115
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@117
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@11a
    move-result v5

    #@11b
    int-to-float v5, v5

    #@11c
    mul-float/2addr v4, v5

    #@11d
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@120
    move-result v4

    #@121
    invoke-virtual {v1, v4}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@124
    .line 995
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@126
    invoke-static {v4}, Landroid/widget/StackView;->access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;

    #@129
    move-result-object v4

    #@12a
    invoke-direct {p0, p1}, Landroid/widget/StackView$StackSlider;->highlightAlphaInterpolator(F)F

    #@12d
    move-result v5

    #@12e
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    #@131
    goto/16 :goto_17

    #@133
    .line 998
    :pswitch_133
    sub-float v4, v8, p1

    #@135
    mul-float p1, v4, v9

    #@137
    .line 999
    int-to-float v4, v2

    #@138
    mul-float/2addr v4, p1

    #@139
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@13b
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@13e
    move-result v5

    #@13f
    int-to-float v5, v5

    #@140
    mul-float/2addr v4, v5

    #@141
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@144
    move-result v4

    #@145
    invoke-virtual {v3, v4}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@148
    .line 1000
    int-to-float v4, v2

    #@149
    mul-float/2addr v4, p1

    #@14a
    iget-object v5, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@14c
    invoke-static {v5}, Landroid/widget/StackView;->access$200(Landroid/widget/StackView;)I

    #@14f
    move-result v5

    #@150
    int-to-float v5, v5

    #@151
    mul-float/2addr v4, v5

    #@152
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@155
    move-result v4

    #@156
    invoke-virtual {v1, v4}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@159
    .line 1001
    iget-object v4, p0, Landroid/widget/StackView$StackSlider;->this$0:Landroid/widget/StackView;

    #@15b
    invoke-static {v4}, Landroid/widget/StackView;->access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;

    #@15e
    move-result-object v4

    #@15f
    invoke-direct {p0, p1}, Landroid/widget/StackView$StackSlider;->highlightAlphaInterpolator(F)F

    #@162
    move-result v5

    #@163
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    #@166
    goto/16 :goto_17

    #@168
    .line 970
    :pswitch_data_168
    .packed-switch 0x0
        :pswitch_5a
        :pswitch_133
        :pswitch_ff
    .end packed-switch
.end method
