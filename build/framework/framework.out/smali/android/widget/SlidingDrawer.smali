.class public Landroid/widget/SlidingDrawer;
.super Landroid/view/ViewGroup;
.source "SlidingDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/SlidingDrawer$1;,
        Landroid/widget/SlidingDrawer$SlidingHandler;,
        Landroid/widget/SlidingDrawer$DrawerToggler;,
        Landroid/widget/SlidingDrawer$OnDrawerScrollListener;,
        Landroid/widget/SlidingDrawer$OnDrawerCloseListener;,
        Landroid/widget/SlidingDrawer$OnDrawerOpenListener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final ANIMATION_FRAME_DURATION:I = 0x10

.field private static final COLLAPSED_FULL_CLOSED:I = -0x2712

.field private static final EXPANDED_FULL_OPEN:I = -0x2711

.field private static final MAXIMUM_ACCELERATION:F = 2000.0f

.field private static final MAXIMUM_MAJOR_VELOCITY:F = 200.0f

.field private static final MAXIMUM_MINOR_VELOCITY:F = 150.0f

.field private static final MAXIMUM_TAP_VELOCITY:F = 100.0f

.field private static final MSG_ANIMATE:I = 0x3e8

.field public static final ORIENTATION_HORIZONTAL:I = 0x0

.field public static final ORIENTATION_VERTICAL:I = 0x1

.field private static final TAP_THRESHOLD:I = 0x6

.field private static final VELOCITY_UNITS:I = 0x3e8


# instance fields
.field private mAllowSingleTap:Z

.field private mAnimateOnClick:Z

.field private mAnimatedAcceleration:F

.field private mAnimatedVelocity:F

.field private mAnimating:Z

.field private mAnimationLastTime:J

.field private mAnimationPosition:F

.field private mBottomOffset:I

.field private mContent:Landroid/view/View;

.field private final mContentId:I

.field private mCurrentAnimationTime:J

.field private mExpanded:Z

.field private final mFrame:Landroid/graphics/Rect;

.field private mHandle:Landroid/view/View;

.field private mHandleHeight:I

.field private final mHandleId:I

.field private mHandleWidth:I

.field private final mHandler:Landroid/os/Handler;

.field private final mInvalidate:Landroid/graphics/Rect;

.field private mLocked:Z

.field private final mMaximumAcceleration:I

.field private final mMaximumMajorVelocity:I

.field private final mMaximumMinorVelocity:I

.field private final mMaximumTapVelocity:I

.field private mOnDrawerCloseListener:Landroid/widget/SlidingDrawer$OnDrawerCloseListener;

.field private mOnDrawerOpenListener:Landroid/widget/SlidingDrawer$OnDrawerOpenListener;

.field private mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

.field private final mTapThreshold:I

.field private mTopOffset:I

.field private mTouchDelta:I

.field private mTracking:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private final mVelocityUnits:I

.field private mVertical:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 187
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/SlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 188
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 15
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    const/high16 v9, 0x3f00

    #@5
    .line 198
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@8
    .line 109
    new-instance v5, Landroid/graphics/Rect;

    #@a
    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    #@d
    iput-object v5, p0, Landroid/widget/SlidingDrawer;->mFrame:Landroid/graphics/Rect;

    #@f
    .line 110
    new-instance v5, Landroid/graphics/Rect;

    #@11
    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    #@14
    iput-object v5, p0, Landroid/widget/SlidingDrawer;->mInvalidate:Landroid/graphics/Rect;

    #@16
    .line 127
    new-instance v5, Landroid/widget/SlidingDrawer$SlidingHandler;

    #@18
    const/4 v8, 0x0

    #@19
    invoke-direct {v5, p0, v8}, Landroid/widget/SlidingDrawer$SlidingHandler;-><init>(Landroid/widget/SlidingDrawer;Landroid/widget/SlidingDrawer$1;)V

    #@1c
    iput-object v5, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@1e
    .line 199
    sget-object v5, Landroid/R$styleable;->SlidingDrawer:[I

    #@20
    invoke-virtual {p1, p2, v5, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@23
    move-result-object v0

    #@24
    .line 201
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v7, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@27
    move-result v4

    #@28
    .line 202
    .local v4, orientation:I
    if-ne v4, v6, :cond_59

    #@2a
    move v5, v6

    #@2b
    :goto_2b
    iput-boolean v5, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@2d
    .line 203
    invoke-virtual {v0, v6, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@30
    move-result v5

    #@31
    float-to-int v5, v5

    #@32
    iput v5, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@34
    .line 204
    const/4 v5, 0x2

    #@35
    invoke-virtual {v0, v5, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@38
    move-result v5

    #@39
    float-to-int v5, v5

    #@3a
    iput v5, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@3c
    .line 205
    const/4 v5, 0x3

    #@3d
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@40
    move-result v5

    #@41
    iput-boolean v5, p0, Landroid/widget/SlidingDrawer;->mAllowSingleTap:Z

    #@43
    .line 206
    const/4 v5, 0x6

    #@44
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@47
    move-result v5

    #@48
    iput-boolean v5, p0, Landroid/widget/SlidingDrawer;->mAnimateOnClick:Z

    #@4a
    .line 208
    const/4 v5, 0x4

    #@4b
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@4e
    move-result v3

    #@4f
    .line 209
    .local v3, handleId:I
    if-nez v3, :cond_5b

    #@51
    .line 210
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@53
    const-string v6, "The handle attribute is required and must refer to a valid child."

    #@55
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@58
    throw v5

    #@59
    .end local v3           #handleId:I
    :cond_59
    move v5, v7

    #@5a
    .line 202
    goto :goto_2b

    #@5b
    .line 214
    .restart local v3       #handleId:I
    :cond_5b
    const/4 v5, 0x5

    #@5c
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5f
    move-result v1

    #@60
    .line 215
    .local v1, contentId:I
    if-nez v1, :cond_6a

    #@62
    .line 216
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@64
    const-string v6, "The content attribute is required and must refer to a valid child."

    #@66
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@69
    throw v5

    #@6a
    .line 220
    :cond_6a
    if-ne v3, v1, :cond_74

    #@6c
    .line 221
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@6e
    const-string v6, "The content and handle attributes must refer to different children."

    #@70
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@73
    throw v5

    #@74
    .line 225
    :cond_74
    iput v3, p0, Landroid/widget/SlidingDrawer;->mHandleId:I

    #@76
    .line 226
    iput v1, p0, Landroid/widget/SlidingDrawer;->mContentId:I

    #@78
    .line 228
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getResources()Landroid/content/res/Resources;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@7f
    move-result-object v5

    #@80
    iget v2, v5, Landroid/util/DisplayMetrics;->density:F

    #@82
    .line 229
    .local v2, density:F
    const/high16 v5, 0x40c0

    #@84
    mul-float/2addr v5, v2

    #@85
    add-float/2addr v5, v9

    #@86
    float-to-int v5, v5

    #@87
    iput v5, p0, Landroid/widget/SlidingDrawer;->mTapThreshold:I

    #@89
    .line 230
    const/high16 v5, 0x42c8

    #@8b
    mul-float/2addr v5, v2

    #@8c
    add-float/2addr v5, v9

    #@8d
    float-to-int v5, v5

    #@8e
    iput v5, p0, Landroid/widget/SlidingDrawer;->mMaximumTapVelocity:I

    #@90
    .line 231
    const/high16 v5, 0x4316

    #@92
    mul-float/2addr v5, v2

    #@93
    add-float/2addr v5, v9

    #@94
    float-to-int v5, v5

    #@95
    iput v5, p0, Landroid/widget/SlidingDrawer;->mMaximumMinorVelocity:I

    #@97
    .line 232
    const/high16 v5, 0x4348

    #@99
    mul-float/2addr v5, v2

    #@9a
    add-float/2addr v5, v9

    #@9b
    float-to-int v5, v5

    #@9c
    iput v5, p0, Landroid/widget/SlidingDrawer;->mMaximumMajorVelocity:I

    #@9e
    .line 233
    const/high16 v5, 0x44fa

    #@a0
    mul-float/2addr v5, v2

    #@a1
    add-float/2addr v5, v9

    #@a2
    float-to-int v5, v5

    #@a3
    iput v5, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@a5
    .line 234
    const/high16 v5, 0x447a

    #@a7
    mul-float/2addr v5, v2

    #@a8
    add-float/2addr v5, v9

    #@a9
    float-to-int v5, v5

    #@aa
    iput v5, p0, Landroid/widget/SlidingDrawer;->mVelocityUnits:I

    #@ac
    .line 236
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@af
    .line 238
    invoke-virtual {p0, v7}, Landroid/widget/SlidingDrawer;->setAlwaysDrawnWithCacheEnabled(Z)V

    #@b2
    .line 239
    return-void
.end method

.method static synthetic access$200(Landroid/widget/SlidingDrawer;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 87
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mLocked:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/widget/SlidingDrawer;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 87
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mAnimateOnClick:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/widget/SlidingDrawer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 87
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->doAnimation()V

    #@3
    return-void
.end method

.method private animateClose(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 483
    invoke-direct {p0, p1}, Landroid/widget/SlidingDrawer;->prepareTracking(I)V

    #@3
    .line 484
    iget v0, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@5
    int-to-float v0, v0

    #@6
    const/4 v1, 0x1

    #@7
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/SlidingDrawer;->performFling(IFZ)V

    #@a
    .line 485
    return-void
.end method

.method private animateOpen(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 488
    invoke-direct {p0, p1}, Landroid/widget/SlidingDrawer;->prepareTracking(I)V

    #@3
    .line 489
    iget v0, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@5
    neg-int v0, v0

    #@6
    int-to-float v0, v0

    #@7
    const/4 v1, 0x1

    #@8
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/SlidingDrawer;->performFling(IFZ)V

    #@b
    .line 490
    return-void
.end method

.method private closeDrawer()V
    .registers 3

    #@0
    .prologue
    .line 832
    const/16 v0, -0x2712

    #@2
    invoke-direct {p0, v0}, Landroid/widget/SlidingDrawer;->moveHandle(I)V

    #@5
    .line 833
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@7
    const/16 v1, 0x8

    #@9
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@c
    .line 834
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@e
    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    #@11
    .line 836
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@13
    if-nez v0, :cond_16

    #@15
    .line 844
    :cond_15
    :goto_15
    return-void

    #@16
    .line 840
    :cond_16
    const/4 v0, 0x0

    #@17
    iput-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@19
    .line 841
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerCloseListener:Landroid/widget/SlidingDrawer$OnDrawerCloseListener;

    #@1b
    if-eqz v0, :cond_15

    #@1d
    .line 842
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerCloseListener:Landroid/widget/SlidingDrawer$OnDrawerCloseListener;

    #@1f
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerCloseListener;->onDrawerClosed()V

    #@22
    goto :goto_15
.end method

.method private doAnimation()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 681
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@3
    if-eqz v0, :cond_21

    #@5
    .line 682
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->incrementAnimation()V

    #@8
    .line 683
    iget v1, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@a
    iget v2, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@c
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@e
    if-eqz v0, :cond_22

    #@10
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getHeight()I

    #@13
    move-result v0

    #@14
    :goto_14
    add-int/2addr v0, v2

    #@15
    add-int/lit8 v0, v0, -0x1

    #@17
    int-to-float v0, v0

    #@18
    cmpl-float v0, v1, v0

    #@1a
    if-ltz v0, :cond_27

    #@1c
    .line 684
    iput-boolean v3, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@1e
    .line 685
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->closeDrawer()V

    #@21
    .line 696
    :cond_21
    :goto_21
    return-void

    #@22
    .line 683
    :cond_22
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getWidth()I

    #@25
    move-result v0

    #@26
    goto :goto_14

    #@27
    .line 686
    :cond_27
    iget v0, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@29
    iget v1, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@2b
    int-to-float v1, v1

    #@2c
    cmpg-float v0, v0, v1

    #@2e
    if-gez v0, :cond_36

    #@30
    .line 687
    iput-boolean v3, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@32
    .line 688
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->openDrawer()V

    #@35
    goto :goto_21

    #@36
    .line 690
    :cond_36
    iget v0, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@38
    float-to-int v0, v0

    #@39
    invoke-direct {p0, v0}, Landroid/widget/SlidingDrawer;->moveHandle(I)V

    #@3c
    .line 691
    iget-wide v0, p0, Landroid/widget/SlidingDrawer;->mCurrentAnimationTime:J

    #@3e
    const-wide/16 v2, 0x10

    #@40
    add-long/2addr v0, v2

    #@41
    iput-wide v0, p0, Landroid/widget/SlidingDrawer;->mCurrentAnimationTime:J

    #@43
    .line 692
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@45
    iget-object v1, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@47
    const/16 v2, 0x3e8

    #@49
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@4c
    move-result-object v1

    #@4d
    iget-wide v2, p0, Landroid/widget/SlidingDrawer;->mCurrentAnimationTime:J

    #@4f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@52
    goto :goto_21
.end method

.method private incrementAnimation()V
    .registers 9

    #@0
    .prologue
    .line 699
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v1

    #@4
    .line 700
    .local v1, now:J
    iget-wide v6, p0, Landroid/widget/SlidingDrawer;->mAnimationLastTime:J

    #@6
    sub-long v6, v1, v6

    #@8
    long-to-float v6, v6

    #@9
    const/high16 v7, 0x447a

    #@b
    div-float v4, v6, v7

    #@d
    .line 701
    .local v4, t:F
    iget v3, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@f
    .line 702
    .local v3, position:F
    iget v5, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@11
    .line 703
    .local v5, v:F
    iget v0, p0, Landroid/widget/SlidingDrawer;->mAnimatedAcceleration:F

    #@13
    .line 704
    .local v0, a:F
    mul-float v6, v5, v4

    #@15
    add-float/2addr v6, v3

    #@16
    const/high16 v7, 0x3f00

    #@18
    mul-float/2addr v7, v0

    #@19
    mul-float/2addr v7, v4

    #@1a
    mul-float/2addr v7, v4

    #@1b
    add-float/2addr v6, v7

    #@1c
    iput v6, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@1e
    .line 705
    mul-float v6, v0, v4

    #@20
    add-float/2addr v6, v5

    #@21
    iput v6, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@23
    .line 706
    iput-wide v1, p0, Landroid/widget/SlidingDrawer;->mAnimationLastTime:J

    #@25
    .line 707
    return-void
.end method

.method private moveHandle(I)V
    .registers 14
    .parameter "position"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/16 v9, -0x2711

    #@3
    const/16 v8, -0x2712

    #@5
    .line 567
    iget-object v3, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@7
    .line 569
    .local v3, handle:Landroid/view/View;
    iget-boolean v7, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@9
    if-eqz v7, :cond_90

    #@b
    .line 570
    if-ne p1, v9, :cond_1b

    #@d
    .line 571
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@f
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@12
    move-result v8

    #@13
    sub-int/2addr v7, v8

    #@14
    invoke-virtual {v3, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@17
    .line 572
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@1a
    .line 630
    :goto_1a
    return-void

    #@1b
    .line 573
    :cond_1b
    if-ne p1, v8, :cond_34

    #@1d
    .line 574
    iget v7, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@1f
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@21
    add-int/2addr v7, v8

    #@22
    iget v8, p0, Landroid/view/View;->mTop:I

    #@24
    sub-int/2addr v7, v8

    #@25
    iget v8, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@27
    sub-int/2addr v7, v8

    #@28
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@2b
    move-result v8

    #@2c
    sub-int/2addr v7, v8

    #@2d
    invoke-virtual {v3, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@30
    .line 576
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@33
    goto :goto_1a

    #@34
    .line 578
    :cond_34
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@37
    move-result v6

    #@38
    .line 579
    .local v6, top:I
    sub-int v1, p1, v6

    #@3a
    .line 580
    .local v1, deltaY:I
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@3c
    if-ge p1, v7, :cond_74

    #@3e
    .line 581
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@40
    sub-int v1, v7, v6

    #@42
    .line 585
    :cond_42
    :goto_42
    invoke-virtual {v3, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@45
    .line 587
    iget-object v2, p0, Landroid/widget/SlidingDrawer;->mFrame:Landroid/graphics/Rect;

    #@47
    .line 588
    .local v2, frame:Landroid/graphics/Rect;
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mInvalidate:Landroid/graphics/Rect;

    #@49
    .line 590
    .local v5, region:Landroid/graphics/Rect;
    invoke-virtual {v3, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@4c
    .line 591
    invoke-virtual {v5, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@4f
    .line 593
    iget v7, v2, Landroid/graphics/Rect;->left:I

    #@51
    iget v8, v2, Landroid/graphics/Rect;->top:I

    #@53
    sub-int/2addr v8, v1

    #@54
    iget v9, v2, Landroid/graphics/Rect;->right:I

    #@56
    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    #@58
    sub-int/2addr v10, v1

    #@59
    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/graphics/Rect;->union(IIII)V

    #@5c
    .line 594
    iget v7, v2, Landroid/graphics/Rect;->bottom:I

    #@5e
    sub-int/2addr v7, v1

    #@5f
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getWidth()I

    #@62
    move-result v8

    #@63
    iget v9, v2, Landroid/graphics/Rect;->bottom:I

    #@65
    sub-int/2addr v9, v1

    #@66
    iget-object v10, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@68
    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    #@6b
    move-result v10

    #@6c
    add-int/2addr v9, v10

    #@6d
    invoke-virtual {v5, v11, v7, v8, v9}, Landroid/graphics/Rect;->union(IIII)V

    #@70
    .line 597
    invoke-virtual {p0, v5}, Landroid/widget/SlidingDrawer;->invalidate(Landroid/graphics/Rect;)V

    #@73
    goto :goto_1a

    #@74
    .line 582
    .end local v2           #frame:Landroid/graphics/Rect;
    .end local v5           #region:Landroid/graphics/Rect;
    :cond_74
    iget v7, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@76
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@78
    add-int/2addr v7, v8

    #@79
    iget v8, p0, Landroid/view/View;->mTop:I

    #@7b
    sub-int/2addr v7, v8

    #@7c
    iget v8, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@7e
    sub-int/2addr v7, v8

    #@7f
    sub-int/2addr v7, v6

    #@80
    if-le v1, v7, :cond_42

    #@82
    .line 583
    iget v7, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@84
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@86
    add-int/2addr v7, v8

    #@87
    iget v8, p0, Landroid/view/View;->mTop:I

    #@89
    sub-int/2addr v7, v8

    #@8a
    iget v8, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@8c
    sub-int/2addr v7, v8

    #@8d
    sub-int v1, v7, v6

    #@8f
    goto :goto_42

    #@90
    .line 600
    .end local v1           #deltaY:I
    .end local v6           #top:I
    :cond_90
    if-ne p1, v9, :cond_a1

    #@92
    .line 601
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@94
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    #@97
    move-result v8

    #@98
    sub-int/2addr v7, v8

    #@99
    invoke-virtual {v3, v7}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@9c
    .line 602
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@9f
    goto/16 :goto_1a

    #@a1
    .line 603
    :cond_a1
    if-ne p1, v8, :cond_bb

    #@a3
    .line 604
    iget v7, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@a5
    iget v8, p0, Landroid/view/View;->mRight:I

    #@a7
    add-int/2addr v7, v8

    #@a8
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@aa
    sub-int/2addr v7, v8

    #@ab
    iget v8, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@ad
    sub-int/2addr v7, v8

    #@ae
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    #@b1
    move-result v8

    #@b2
    sub-int/2addr v7, v8

    #@b3
    invoke-virtual {v3, v7}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@b6
    .line 606
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@b9
    goto/16 :goto_1a

    #@bb
    .line 608
    :cond_bb
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    #@be
    move-result v4

    #@bf
    .line 609
    .local v4, left:I
    sub-int v0, p1, v4

    #@c1
    .line 610
    .local v0, deltaX:I
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@c3
    if-ge p1, v7, :cond_fc

    #@c5
    .line 611
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@c7
    sub-int v0, v7, v4

    #@c9
    .line 615
    :cond_c9
    :goto_c9
    invoke-virtual {v3, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    #@cc
    .line 617
    iget-object v2, p0, Landroid/widget/SlidingDrawer;->mFrame:Landroid/graphics/Rect;

    #@ce
    .line 618
    .restart local v2       #frame:Landroid/graphics/Rect;
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mInvalidate:Landroid/graphics/Rect;

    #@d0
    .line 620
    .restart local v5       #region:Landroid/graphics/Rect;
    invoke-virtual {v3, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@d3
    .line 621
    invoke-virtual {v5, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@d6
    .line 623
    iget v7, v2, Landroid/graphics/Rect;->left:I

    #@d8
    sub-int/2addr v7, v0

    #@d9
    iget v8, v2, Landroid/graphics/Rect;->top:I

    #@db
    iget v9, v2, Landroid/graphics/Rect;->right:I

    #@dd
    sub-int/2addr v9, v0

    #@de
    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    #@e0
    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/graphics/Rect;->union(IIII)V

    #@e3
    .line 624
    iget v7, v2, Landroid/graphics/Rect;->right:I

    #@e5
    sub-int/2addr v7, v0

    #@e6
    iget v8, v2, Landroid/graphics/Rect;->right:I

    #@e8
    sub-int/2addr v8, v0

    #@e9
    iget-object v9, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@eb
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    #@ee
    move-result v9

    #@ef
    add-int/2addr v8, v9

    #@f0
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getHeight()I

    #@f3
    move-result v9

    #@f4
    invoke-virtual {v5, v7, v11, v8, v9}, Landroid/graphics/Rect;->union(IIII)V

    #@f7
    .line 627
    invoke-virtual {p0, v5}, Landroid/widget/SlidingDrawer;->invalidate(Landroid/graphics/Rect;)V

    #@fa
    goto/16 :goto_1a

    #@fc
    .line 612
    .end local v2           #frame:Landroid/graphics/Rect;
    .end local v5           #region:Landroid/graphics/Rect;
    :cond_fc
    iget v7, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@fe
    iget v8, p0, Landroid/view/View;->mRight:I

    #@100
    add-int/2addr v7, v8

    #@101
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@103
    sub-int/2addr v7, v8

    #@104
    iget v8, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@106
    sub-int/2addr v7, v8

    #@107
    sub-int/2addr v7, v4

    #@108
    if-le v0, v7, :cond_c9

    #@10a
    .line 613
    iget v7, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@10c
    iget v8, p0, Landroid/view/View;->mRight:I

    #@10e
    add-int/2addr v7, v8

    #@10f
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@111
    sub-int/2addr v7, v8

    #@112
    iget v8, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@114
    sub-int/2addr v7, v8

    #@115
    sub-int v0, v7, v4

    #@117
    goto :goto_c9
.end method

.method private openDrawer()V
    .registers 3

    #@0
    .prologue
    .line 847
    const/16 v0, -0x2711

    #@2
    invoke-direct {p0, v0}, Landroid/widget/SlidingDrawer;->moveHandle(I)V

    #@5
    .line 848
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@b
    .line 850
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@d
    if-eqz v0, :cond_10

    #@f
    .line 859
    :cond_f
    :goto_f
    return-void

    #@10
    .line 854
    :cond_10
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@13
    .line 856
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerOpenListener:Landroid/widget/SlidingDrawer$OnDrawerOpenListener;

    #@15
    if-eqz v0, :cond_f

    #@17
    .line 857
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerOpenListener:Landroid/widget/SlidingDrawer$OnDrawerOpenListener;

    #@19
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerOpenListener;->onDrawerOpened()V

    #@1c
    goto :goto_f
.end method

.method private performFling(IFZ)V
    .registers 10
    .parameter "position"
    .parameter "velocity"
    .parameter "always"

    #@0
    .prologue
    const/16 v5, 0x3e8

    #@2
    const/4 v4, 0x0

    #@3
    .line 493
    int-to-float v2, p1

    #@4
    iput v2, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@6
    .line 494
    iput p2, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@8
    .line 496
    iget-boolean v2, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@a
    if-eqz v2, :cond_67

    #@c
    .line 497
    if-nez p3, :cond_28

    #@e
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumMajorVelocity:I

    #@10
    int-to-float v2, v2

    #@11
    cmpl-float v2, p2, v2

    #@13
    if-gtz v2, :cond_28

    #@15
    iget v3, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@17
    iget-boolean v2, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@19
    if-eqz v2, :cond_57

    #@1b
    iget v2, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@1d
    :goto_1d
    add-int/2addr v2, v3

    #@1e
    if-le p1, v2, :cond_5a

    #@20
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumMajorVelocity:I

    #@22
    neg-int v2, v2

    #@23
    int-to-float v2, v2

    #@24
    cmpl-float v2, p2, v2

    #@26
    if-lez v2, :cond_5a

    #@28
    .line 502
    :cond_28
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@2a
    int-to-float v2, v2

    #@2b
    iput v2, p0, Landroid/widget/SlidingDrawer;->mAnimatedAcceleration:F

    #@2d
    .line 503
    cmpg-float v2, p2, v4

    #@2f
    if-gez v2, :cond_33

    #@31
    .line 504
    iput v4, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@33
    .line 532
    :cond_33
    :goto_33
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@36
    move-result-wide v0

    #@37
    .line 533
    .local v0, now:J
    iput-wide v0, p0, Landroid/widget/SlidingDrawer;->mAnimationLastTime:J

    #@39
    .line 534
    const-wide/16 v2, 0x10

    #@3b
    add-long/2addr v2, v0

    #@3c
    iput-wide v2, p0, Landroid/widget/SlidingDrawer;->mCurrentAnimationTime:J

    #@3e
    .line 535
    const/4 v2, 0x1

    #@3f
    iput-boolean v2, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@41
    .line 536
    iget-object v2, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@43
    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@46
    .line 537
    iget-object v2, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@48
    iget-object v3, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@4a
    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@4d
    move-result-object v3

    #@4e
    iget-wide v4, p0, Landroid/widget/SlidingDrawer;->mCurrentAnimationTime:J

    #@50
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@53
    .line 538
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->stopTracking()V

    #@56
    .line 539
    return-void

    #@57
    .line 497
    .end local v0           #now:J
    :cond_57
    iget v2, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@59
    goto :goto_1d

    #@5a
    .line 508
    :cond_5a
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@5c
    neg-int v2, v2

    #@5d
    int-to-float v2, v2

    #@5e
    iput v2, p0, Landroid/widget/SlidingDrawer;->mAnimatedAcceleration:F

    #@60
    .line 509
    cmpl-float v2, p2, v4

    #@62
    if-lez v2, :cond_33

    #@64
    .line 510
    iput v4, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@66
    goto :goto_33

    #@67
    .line 514
    :cond_67
    if-nez p3, :cond_95

    #@69
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumMajorVelocity:I

    #@6b
    int-to-float v2, v2

    #@6c
    cmpl-float v2, p2, v2

    #@6e
    if-gtz v2, :cond_84

    #@70
    iget-boolean v2, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@72
    if-eqz v2, :cond_90

    #@74
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getHeight()I

    #@77
    move-result v2

    #@78
    :goto_78
    div-int/lit8 v2, v2, 0x2

    #@7a
    if-le p1, v2, :cond_95

    #@7c
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumMajorVelocity:I

    #@7e
    neg-int v2, v2

    #@7f
    int-to-float v2, v2

    #@80
    cmpl-float v2, p2, v2

    #@82
    if-lez v2, :cond_95

    #@84
    .line 518
    :cond_84
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@86
    int-to-float v2, v2

    #@87
    iput v2, p0, Landroid/widget/SlidingDrawer;->mAnimatedAcceleration:F

    #@89
    .line 519
    cmpg-float v2, p2, v4

    #@8b
    if-gez v2, :cond_33

    #@8d
    .line 520
    iput v4, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@8f
    goto :goto_33

    #@90
    .line 514
    :cond_90
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getWidth()I

    #@93
    move-result v2

    #@94
    goto :goto_78

    #@95
    .line 525
    :cond_95
    iget v2, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@97
    neg-int v2, v2

    #@98
    int-to-float v2, v2

    #@99
    iput v2, p0, Landroid/widget/SlidingDrawer;->mAnimatedAcceleration:F

    #@9b
    .line 526
    cmpl-float v2, p2, v4

    #@9d
    if-lez v2, :cond_33

    #@9f
    .line 527
    iput v4, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@a1
    goto :goto_33
.end method

.method private prepareContent()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/high16 v8, 0x4000

    #@3
    .line 633
    iget-boolean v5, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@5
    if-eqz v5, :cond_8

    #@7
    .line 664
    :goto_7
    return-void

    #@8
    .line 639
    :cond_8
    iget-object v2, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@a
    .line 640
    .local v2, content:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->isLayoutRequested()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_42

    #@10
    .line 641
    iget-boolean v5, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@12
    if-eqz v5, :cond_58

    #@14
    .line 642
    iget v0, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@16
    .line 643
    .local v0, childHeight:I
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@18
    iget v6, p0, Landroid/view/View;->mTop:I

    #@1a
    sub-int/2addr v5, v6

    #@1b
    sub-int/2addr v5, v0

    #@1c
    iget v6, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@1e
    sub-int v3, v5, v6

    #@20
    .line 644
    .local v3, height:I
    iget v5, p0, Landroid/view/View;->mRight:I

    #@22
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@24
    sub-int/2addr v5, v6

    #@25
    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@28
    move-result v5

    #@29
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@2c
    move-result v6

    #@2d
    invoke-virtual {v2, v5, v6}, Landroid/view/View;->measure(II)V

    #@30
    .line 646
    iget v5, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@32
    add-int/2addr v5, v0

    #@33
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    #@36
    move-result v6

    #@37
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@39
    add-int/2addr v7, v0

    #@3a
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    #@3d
    move-result v8

    #@3e
    add-int/2addr v7, v8

    #@3f
    invoke-virtual {v2, v9, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    #@42
    .line 660
    .end local v0           #childHeight:I
    .end local v3           #height:I
    :cond_42
    :goto_42
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5}, Landroid/view/ViewTreeObserver;->dispatchOnPreDraw()Z

    #@49
    .line 661
    invoke-virtual {v2}, Landroid/view/View;->isHardwareAccelerated()Z

    #@4c
    move-result v5

    #@4d
    if-nez v5, :cond_52

    #@4f
    invoke-virtual {v2}, Landroid/view/View;->buildDrawingCache()V

    #@52
    .line 663
    :cond_52
    const/16 v5, 0x8

    #@54
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    #@57
    goto :goto_7

    #@58
    .line 649
    :cond_58
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@5a
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    #@5d
    move-result v1

    #@5e
    .line 650
    .local v1, childWidth:I
    iget v5, p0, Landroid/view/View;->mRight:I

    #@60
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@62
    sub-int/2addr v5, v6

    #@63
    sub-int/2addr v5, v1

    #@64
    iget v6, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@66
    sub-int v4, v5, v6

    #@68
    .line 651
    .local v4, width:I
    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@6b
    move-result v5

    #@6c
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@6e
    iget v7, p0, Landroid/view/View;->mTop:I

    #@70
    sub-int/2addr v6, v7

    #@71
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@74
    move-result v6

    #@75
    invoke-virtual {v2, v5, v6}, Landroid/view/View;->measure(II)V

    #@78
    .line 653
    iget v5, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@7a
    add-int/2addr v5, v1

    #@7b
    iget v6, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@7d
    add-int/2addr v6, v1

    #@7e
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    #@81
    move-result v7

    #@82
    add-int/2addr v6, v7

    #@83
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    #@86
    move-result v7

    #@87
    invoke-virtual {v2, v5, v9, v6, v7}, Landroid/view/View;->layout(IIII)V

    #@8a
    goto :goto_42
.end method

.method private prepareTracking(I)V
    .registers 10
    .parameter "position"

    #@0
    .prologue
    const/16 v7, 0x3e8

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 542
    iput-boolean v4, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@6
    .line 543
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@9
    move-result-object v5

    #@a
    iput-object v5, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    .line 544
    iget-boolean v5, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@e
    if-nez v5, :cond_49

    #@10
    move v2, v4

    #@11
    .line 545
    .local v2, opening:Z
    :goto_11
    if-eqz v2, :cond_53

    #@13
    .line 546
    iget v3, p0, Landroid/widget/SlidingDrawer;->mMaximumAcceleration:I

    #@15
    int-to-float v3, v3

    #@16
    iput v3, p0, Landroid/widget/SlidingDrawer;->mAnimatedAcceleration:F

    #@18
    .line 547
    iget v3, p0, Landroid/widget/SlidingDrawer;->mMaximumMajorVelocity:I

    #@1a
    int-to-float v3, v3

    #@1b
    iput v3, p0, Landroid/widget/SlidingDrawer;->mAnimatedVelocity:F

    #@1d
    .line 548
    iget v5, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@1f
    iget-boolean v3, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@21
    if-eqz v3, :cond_4b

    #@23
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getHeight()I

    #@26
    move-result v3

    #@27
    iget v6, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@29
    sub-int/2addr v3, v6

    #@2a
    :goto_2a
    add-int/2addr v3, v5

    #@2b
    int-to-float v3, v3

    #@2c
    iput v3, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@2e
    .line 550
    iget v3, p0, Landroid/widget/SlidingDrawer;->mAnimationPosition:F

    #@30
    float-to-int v3, v3

    #@31
    invoke-direct {p0, v3}, Landroid/widget/SlidingDrawer;->moveHandle(I)V

    #@34
    .line 551
    iput-boolean v4, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@36
    .line 552
    iget-object v3, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@38
    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    #@3b
    .line 553
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3e
    move-result-wide v0

    #@3f
    .line 554
    .local v0, now:J
    iput-wide v0, p0, Landroid/widget/SlidingDrawer;->mAnimationLastTime:J

    #@41
    .line 555
    const-wide/16 v5, 0x10

    #@43
    add-long/2addr v5, v0

    #@44
    iput-wide v5, p0, Landroid/widget/SlidingDrawer;->mCurrentAnimationTime:J

    #@46
    .line 556
    iput-boolean v4, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@48
    .line 564
    .end local v0           #now:J
    :goto_48
    return-void

    #@49
    .end local v2           #opening:Z
    :cond_49
    move v2, v3

    #@4a
    .line 544
    goto :goto_11

    #@4b
    .line 548
    .restart local v2       #opening:Z
    :cond_4b
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getWidth()I

    #@4e
    move-result v3

    #@4f
    iget v6, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@51
    sub-int/2addr v3, v6

    #@52
    goto :goto_2a

    #@53
    .line 558
    :cond_53
    iget-boolean v4, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@55
    if-eqz v4, :cond_5e

    #@57
    .line 559
    iput-boolean v3, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@59
    .line 560
    iget-object v3, p0, Landroid/widget/SlidingDrawer;->mHandler:Landroid/os/Handler;

    #@5b
    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    #@5e
    .line 562
    :cond_5e
    invoke-direct {p0, p1}, Landroid/widget/SlidingDrawer;->moveHandle(I)V

    #@61
    goto :goto_48
.end method

.method private stopTracking()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 667
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@3
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    #@6
    .line 668
    iput-boolean v1, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@8
    .line 670
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 671
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@e
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerScrollListener;->onScrollEnded()V

    #@11
    .line 674
    :cond_11
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@13
    if-eqz v0, :cond_1d

    #@15
    .line 675
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@17
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@1a
    .line 676
    const/4 v0, 0x0

    #@1b
    iput-object v0, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1d
    .line 678
    :cond_1d
    return-void
.end method


# virtual methods
.method public animateClose()V
    .registers 3

    #@0
    .prologue
    .line 783
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->prepareContent()V

    #@3
    .line 784
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@5
    .line 785
    .local v0, scrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;
    if-eqz v0, :cond_a

    #@7
    .line 786
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerScrollListener;->onScrollStarted()V

    #@a
    .line 788
    :cond_a
    iget-boolean v1, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@c
    if-eqz v1, :cond_1d

    #@e
    iget-object v1, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@10
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@13
    move-result v1

    #@14
    :goto_14
    invoke-direct {p0, v1}, Landroid/widget/SlidingDrawer;->animateClose(I)V

    #@17
    .line 790
    if-eqz v0, :cond_1c

    #@19
    .line 791
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerScrollListener;->onScrollEnded()V

    #@1c
    .line 793
    :cond_1c
    return-void

    #@1d
    .line 788
    :cond_1d
    iget-object v1, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@1f
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    #@22
    move-result v1

    #@23
    goto :goto_14
.end method

.method public animateOpen()V
    .registers 3

    #@0
    .prologue
    .line 805
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->prepareContent()V

    #@3
    .line 806
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@5
    .line 807
    .local v0, scrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;
    if-eqz v0, :cond_a

    #@7
    .line 808
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerScrollListener;->onScrollStarted()V

    #@a
    .line 810
    :cond_a
    iget-boolean v1, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@c
    if-eqz v1, :cond_22

    #@e
    iget-object v1, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@10
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    #@13
    move-result v1

    #@14
    :goto_14
    invoke-direct {p0, v1}, Landroid/widget/SlidingDrawer;->animateOpen(I)V

    #@17
    .line 812
    const/16 v1, 0x20

    #@19
    invoke-virtual {p0, v1}, Landroid/widget/SlidingDrawer;->sendAccessibilityEvent(I)V

    #@1c
    .line 814
    if-eqz v0, :cond_21

    #@1e
    .line 815
    invoke-interface {v0}, Landroid/widget/SlidingDrawer$OnDrawerScrollListener;->onScrollEnded()V

    #@21
    .line 817
    :cond_21
    return-void

    #@22
    .line 810
    :cond_22
    iget-object v1, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@24
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    #@27
    move-result v1

    #@28
    goto :goto_14
.end method

.method public animateToggle()V
    .registers 2

    #@0
    .prologue
    .line 738
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@2
    if-nez v0, :cond_8

    #@4
    .line 739
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->animateOpen()V

    #@7
    .line 743
    :goto_7
    return-void

    #@8
    .line 741
    :cond_8
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->animateClose()V

    #@b
    goto :goto_7
.end method

.method public close()V
    .registers 1

    #@0
    .prologue
    .line 768
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->closeDrawer()V

    #@3
    .line 769
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@6
    .line 770
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->requestLayout()V

    #@9
    .line 771
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 288
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->getDrawingTime()J

    #@5
    move-result-wide v1

    #@6
    .line 289
    .local v1, drawingTime:J
    iget-object v3, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@8
    .line 290
    .local v3, handle:Landroid/view/View;
    iget-boolean v4, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@a
    .line 292
    .local v4, isVertical:Z
    invoke-virtual {p0, p1, v3, v1, v2}, Landroid/widget/SlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@d
    .line 294
    iget-boolean v6, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@f
    if-nez v6, :cond_15

    #@11
    iget-boolean v6, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@13
    if-eqz v6, :cond_56

    #@15
    .line 295
    :cond_15
    iget-object v6, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@17
    invoke-virtual {v6}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    #@1a
    move-result-object v0

    #@1b
    .line 296
    .local v0, cache:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_31

    #@1d
    .line 297
    if-eqz v4, :cond_28

    #@1f
    .line 298
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@22
    move-result v6

    #@23
    int-to-float v6, v6

    #@24
    invoke-virtual {p1, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@27
    .line 312
    .end local v0           #cache:Landroid/graphics/Bitmap;
    :cond_27
    :goto_27
    return-void

    #@28
    .line 300
    .restart local v0       #cache:Landroid/graphics/Bitmap;
    :cond_28
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    #@2b
    move-result v6

    #@2c
    int-to-float v6, v6

    #@2d
    invoke-virtual {p1, v0, v6, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@30
    goto :goto_27

    #@31
    .line 303
    :cond_31
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@34
    .line 304
    if-eqz v4, :cond_4d

    #@36
    move v6, v5

    #@37
    :goto_37
    if-eqz v4, :cond_41

    #@39
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@3c
    move-result v5

    #@3d
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@3f
    sub-int/2addr v5, v7

    #@40
    int-to-float v5, v5

    #@41
    :cond_41
    invoke-virtual {p1, v6, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@44
    .line 306
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@46
    invoke-virtual {p0, p1, v5, v1, v2}, Landroid/widget/SlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@49
    .line 307
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@4c
    goto :goto_27

    #@4d
    .line 304
    :cond_4d
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    #@50
    move-result v6

    #@51
    iget v7, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@53
    sub-int/2addr v6, v7

    #@54
    int-to-float v6, v6

    #@55
    goto :goto_37

    #@56
    .line 309
    .end local v0           #cache:Landroid/graphics/Bitmap;
    :cond_56
    iget-boolean v5, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@58
    if-eqz v5, :cond_27

    #@5a
    .line 310
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@5c
    invoke-virtual {p0, p1, v5, v1, v2}, Landroid/widget/SlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@5f
    goto :goto_27
.end method

.method public getContent()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 908
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getHandle()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 898
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public isMoving()Z
    .registers 2

    #@0
    .prologue
    .line 944
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@2
    if-nez v0, :cond_8

    #@4
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isOpened()Z
    .registers 2

    #@0
    .prologue
    .line 935
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@2
    return v0
.end method

.method public lock()V
    .registers 2

    #@0
    .prologue
    .line 926
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/SlidingDrawer;->mLocked:Z

    #@3
    .line 927
    return-void
.end method

.method protected onFinishInflate()V
    .registers 4

    #@0
    .prologue
    .line 243
    iget v0, p0, Landroid/widget/SlidingDrawer;->mHandleId:I

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/SlidingDrawer;->findViewById(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@8
    .line 244
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@a
    if-nez v0, :cond_14

    #@c
    .line 245
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v1, "The handle attribute is must refer to an existing child."

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 248
    :cond_14
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@16
    new-instance v1, Landroid/widget/SlidingDrawer$DrawerToggler;

    #@18
    const/4 v2, 0x0

    #@19
    invoke-direct {v1, p0, v2}, Landroid/widget/SlidingDrawer$DrawerToggler;-><init>(Landroid/widget/SlidingDrawer;Landroid/widget/SlidingDrawer$1;)V

    #@1c
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@1f
    .line 250
    iget v0, p0, Landroid/widget/SlidingDrawer;->mContentId:I

    #@21
    invoke-virtual {p0, v0}, Landroid/widget/SlidingDrawer;->findViewById(I)Landroid/view/View;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@27
    .line 251
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@29
    if-nez v0, :cond_33

    #@2b
    .line 252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2d
    const-string v1, "The content attribute is must refer to an existing child."

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v0

    #@33
    .line 255
    :cond_33
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@35
    const/16 v1, 0x8

    #@37
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@3a
    .line 256
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 821
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 822
    const-class v0, Landroid/widget/SlidingDrawer;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 823
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 827
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 828
    const-class v0, Landroid/widget/SlidingDrawer;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 829
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 355
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mLocked:Z

    #@4
    if-eqz v9, :cond_7

    #@6
    .line 396
    :cond_6
    :goto_6
    return v7

    #@7
    .line 359
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@a
    move-result v0

    #@b
    .line 361
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@e
    move-result v5

    #@f
    .line 362
    .local v5, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@12
    move-result v6

    #@13
    .line 364
    .local v6, y:F
    iget-object v1, p0, Landroid/widget/SlidingDrawer;->mFrame:Landroid/graphics/Rect;

    #@15
    .line 365
    .local v1, frame:Landroid/graphics/Rect;
    iget-object v2, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@17
    .line 367
    .local v2, handle:Landroid/view/View;
    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@1a
    .line 368
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@1c
    if-nez v9, :cond_26

    #@1e
    float-to-int v9, v5

    #@1f
    float-to-int v10, v6

    #@20
    invoke-virtual {v1, v9, v10}, Landroid/graphics/Rect;->contains(II)Z

    #@23
    move-result v9

    #@24
    if-eqz v9, :cond_6

    #@26
    .line 372
    :cond_26
    if-nez v0, :cond_4f

    #@28
    .line 373
    iput-boolean v8, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@2a
    .line 375
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    #@2d
    .line 377
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->prepareContent()V

    #@30
    .line 380
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@32
    if-eqz v7, :cond_39

    #@34
    .line 381
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@36
    invoke-interface {v7}, Landroid/widget/SlidingDrawer$OnDrawerScrollListener;->onScrollStarted()V

    #@39
    .line 384
    :cond_39
    iget-boolean v7, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@3b
    if-eqz v7, :cond_51

    #@3d
    .line 385
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@3f
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@42
    move-result v4

    #@43
    .line 386
    .local v4, top:I
    float-to-int v7, v6

    #@44
    sub-int/2addr v7, v4

    #@45
    iput v7, p0, Landroid/widget/SlidingDrawer;->mTouchDelta:I

    #@47
    .line 387
    invoke-direct {p0, v4}, Landroid/widget/SlidingDrawer;->prepareTracking(I)V

    #@4a
    .line 393
    .end local v4           #top:I
    :goto_4a
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@4c
    invoke-virtual {v7, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@4f
    :cond_4f
    move v7, v8

    #@50
    .line 396
    goto :goto_6

    #@51
    .line 389
    :cond_51
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@53
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    #@56
    move-result v3

    #@57
    .line 390
    .local v3, left:I
    float-to-int v7, v5

    #@58
    sub-int/2addr v7, v3

    #@59
    iput v7, p0, Landroid/widget/SlidingDrawer;->mTouchDelta:I

    #@5b
    .line 391
    invoke-direct {p0, v3}, Landroid/widget/SlidingDrawer;->prepareTracking(I)V

    #@5e
    goto :goto_4a
.end method

.method protected onLayout(ZIIII)V
    .registers 19
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 316
    iget-boolean v8, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@2
    if-eqz v8, :cond_5

    #@4
    .line 351
    :goto_4
    return-void

    #@5
    .line 320
    :cond_5
    sub-int v7, p4, p2

    #@7
    .line 321
    .local v7, width:I
    sub-int v6, p5, p3

    #@9
    .line 323
    .local v6, height:I
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@b
    .line 325
    .local v5, handle:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    #@e
    move-result v3

    #@f
    .line 326
    .local v3, childWidth:I
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    #@12
    move-result v0

    #@13
    .line 331
    .local v0, childHeight:I
    iget-object v4, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@15
    .line 333
    .local v4, content:Landroid/view/View;
    iget-boolean v8, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@17
    if-eqz v8, :cond_51

    #@19
    .line 334
    sub-int v8, v7, v3

    #@1b
    div-int/lit8 v1, v8, 0x2

    #@1d
    .line 335
    .local v1, childLeft:I
    iget-boolean v8, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@1f
    if-eqz v8, :cond_4a

    #@21
    iget v2, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@23
    .line 337
    .local v2, childTop:I
    :goto_23
    const/4 v8, 0x0

    #@24
    iget v9, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@26
    add-int/2addr v9, v0

    #@27
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@2a
    move-result v10

    #@2b
    iget v11, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@2d
    add-int/2addr v11, v0

    #@2e
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@31
    move-result v12

    #@32
    add-int/2addr v11, v12

    #@33
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    #@36
    .line 348
    :goto_36
    add-int v8, v1, v3

    #@38
    add-int v9, v2, v0

    #@3a
    invoke-virtual {v5, v1, v2, v8, v9}, Landroid/view/View;->layout(IIII)V

    #@3d
    .line 349
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    #@40
    move-result v8

    #@41
    iput v8, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@43
    .line 350
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    #@46
    move-result v8

    #@47
    iput v8, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@49
    goto :goto_4

    #@4a
    .line 335
    .end local v2           #childTop:I
    :cond_4a
    sub-int v8, v6, v0

    #@4c
    iget v9, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@4e
    add-int v2, v8, v9

    #@50
    goto :goto_23

    #@51
    .line 340
    .end local v1           #childLeft:I
    :cond_51
    iget-boolean v8, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@53
    if-eqz v8, :cond_6f

    #@55
    iget v1, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@57
    .line 341
    .restart local v1       #childLeft:I
    :goto_57
    sub-int v8, v6, v0

    #@59
    div-int/lit8 v2, v8, 0x2

    #@5b
    .line 343
    .restart local v2       #childTop:I
    iget v8, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@5d
    add-int/2addr v8, v3

    #@5e
    const/4 v9, 0x0

    #@5f
    iget v10, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@61
    add-int/2addr v10, v3

    #@62
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@65
    move-result v11

    #@66
    add-int/2addr v10, v11

    #@67
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@6a
    move-result v11

    #@6b
    invoke-virtual {v4, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    #@6e
    goto :goto_36

    #@6f
    .line 340
    .end local v1           #childLeft:I
    .end local v2           #childTop:I
    :cond_6f
    sub-int v8, v7, v3

    #@71
    iget v9, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@73
    add-int v1, v8, v9

    #@75
    goto :goto_57
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v9, 0x4000

    #@2
    .line 260
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@5
    move-result v5

    #@6
    .line 261
    .local v5, widthSpecMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9
    move-result v6

    #@a
    .line 263
    .local v6, widthSpecSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@d
    move-result v2

    #@e
    .line 264
    .local v2, heightSpecMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@11
    move-result v3

    #@12
    .line 266
    .local v3, heightSpecSize:I
    if-eqz v5, :cond_16

    #@14
    if-nez v2, :cond_1e

    #@16
    .line 267
    :cond_16
    new-instance v7, Ljava/lang/RuntimeException;

    #@18
    const-string v8, "SlidingDrawer cannot have UNSPECIFIED dimensions"

    #@1a
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v7

    #@1e
    .line 270
    :cond_1e
    iget-object v0, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@20
    .line 271
    .local v0, handle:Landroid/view/View;
    invoke-virtual {p0, v0, p1, p2}, Landroid/widget/SlidingDrawer;->measureChild(Landroid/view/View;II)V

    #@23
    .line 273
    iget-boolean v7, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@25
    if-eqz v7, :cond_42

    #@27
    .line 274
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@2a
    move-result v7

    #@2b
    sub-int v7, v3, v7

    #@2d
    iget v8, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@2f
    sub-int v1, v7, v8

    #@31
    .line 275
    .local v1, height:I
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@33
    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@36
    move-result v8

    #@37
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3a
    move-result v9

    #@3b
    invoke-virtual {v7, v8, v9}, Landroid/view/View;->measure(II)V

    #@3e
    .line 283
    .end local v1           #height:I
    :goto_3e
    invoke-virtual {p0, v6, v3}, Landroid/widget/SlidingDrawer;->setMeasuredDimension(II)V

    #@41
    .line 284
    return-void

    #@42
    .line 278
    :cond_42
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@45
    move-result v7

    #@46
    sub-int v7, v6, v7

    #@48
    iget v8, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@4a
    sub-int v4, v7, v8

    #@4c
    .line 279
    .local v4, width:I
    iget-object v7, p0, Landroid/widget/SlidingDrawer;->mContent:Landroid/view/View;

    #@4e
    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@51
    move-result v8

    #@52
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@55
    move-result v9

    #@56
    invoke-virtual {v7, v8, v9}, Landroid/view/View;->measure(II)V

    #@59
    goto :goto_3e
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "event"

    #@0
    .prologue
    .line 401
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mLocked:Z

    #@2
    if-eqz v9, :cond_6

    #@4
    .line 402
    const/4 v9, 0x1

    #@5
    .line 479
    :goto_5
    return v9

    #@6
    .line 405
    :cond_6
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@8
    if-eqz v9, :cond_16

    #@a
    .line 406
    iget-object v9, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    invoke-virtual {v9, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@f
    .line 407
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@12
    move-result v0

    #@13
    .line 408
    .local v0, action:I
    packed-switch v0, :pswitch_data_120

    #@16
    .line 479
    .end local v0           #action:I
    :cond_16
    :goto_16
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mTracking:Z

    #@18
    if-nez v9, :cond_24

    #@1a
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mAnimating:Z

    #@1c
    if-nez v9, :cond_24

    #@1e
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@21
    move-result v9

    #@22
    if-eqz v9, :cond_11c

    #@24
    :cond_24
    const/4 v9, 0x1

    #@25
    goto :goto_5

    #@26
    .line 410
    .restart local v0       #action:I
    :pswitch_26
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@28
    if-eqz v9, :cond_36

    #@2a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@2d
    move-result v9

    #@2e
    :goto_2e
    float-to-int v9, v9

    #@2f
    iget v10, p0, Landroid/widget/SlidingDrawer;->mTouchDelta:I

    #@31
    sub-int/2addr v9, v10

    #@32
    invoke-direct {p0, v9}, Landroid/widget/SlidingDrawer;->moveHandle(I)V

    #@35
    goto :goto_16

    #@36
    :cond_36
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@39
    move-result v9

    #@3a
    goto :goto_2e

    #@3b
    .line 414
    :pswitch_3b
    iget-object v5, p0, Landroid/widget/SlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@3d
    .line 415
    .local v5, velocityTracker:Landroid/view/VelocityTracker;
    iget v9, p0, Landroid/widget/SlidingDrawer;->mVelocityUnits:I

    #@3f
    invoke-virtual {v5, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@42
    .line 417
    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@45
    move-result v8

    #@46
    .line 418
    .local v8, yVelocity:F
    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@49
    move-result v7

    #@4a
    .line 421
    .local v7, xVelocity:F
    iget-boolean v6, p0, Landroid/widget/SlidingDrawer;->mVertical:Z

    #@4c
    .line 422
    .local v6, vertical:Z
    if-eqz v6, :cond_bb

    #@4e
    .line 423
    const/4 v9, 0x0

    #@4f
    cmpg-float v9, v8, v9

    #@51
    if-gez v9, :cond_b9

    #@53
    const/4 v2, 0x1

    #@54
    .line 424
    .local v2, negative:Z
    :goto_54
    const/4 v9, 0x0

    #@55
    cmpg-float v9, v7, v9

    #@57
    if-gez v9, :cond_5a

    #@59
    .line 425
    neg-float v7, v7

    #@5a
    .line 427
    :cond_5a
    iget v9, p0, Landroid/widget/SlidingDrawer;->mMaximumMinorVelocity:I

    #@5c
    int-to-float v9, v9

    #@5d
    cmpl-float v9, v7, v9

    #@5f
    if-lez v9, :cond_64

    #@61
    .line 428
    iget v9, p0, Landroid/widget/SlidingDrawer;->mMaximumMinorVelocity:I

    #@63
    int-to-float v7, v9

    #@64
    .line 440
    :cond_64
    :goto_64
    float-to-double v9, v7

    #@65
    float-to-double v11, v8

    #@66
    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->hypot(DD)D

    #@69
    move-result-wide v9

    #@6a
    double-to-float v4, v9

    #@6b
    .line 441
    .local v4, velocity:F
    if-eqz v2, :cond_6e

    #@6d
    .line 442
    neg-float v4, v4

    #@6e
    .line 445
    :cond_6e
    iget-object v9, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@70
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    #@73
    move-result v3

    #@74
    .line 446
    .local v3, top:I
    iget-object v9, p0, Landroid/widget/SlidingDrawer;->mHandle:Landroid/view/View;

    #@76
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    #@79
    move-result v1

    #@7a
    .line 448
    .local v1, left:I
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    #@7d
    move-result v9

    #@7e
    iget v10, p0, Landroid/widget/SlidingDrawer;->mMaximumTapVelocity:I

    #@80
    int-to-float v10, v10

    #@81
    cmpg-float v9, v9, v10

    #@83
    if-gez v9, :cond_112

    #@85
    .line 449
    if-eqz v6, :cond_d4

    #@87
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@89
    if-eqz v9, :cond_92

    #@8b
    iget v9, p0, Landroid/widget/SlidingDrawer;->mTapThreshold:I

    #@8d
    iget v10, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@8f
    add-int/2addr v9, v10

    #@90
    if-lt v3, v9, :cond_a6

    #@92
    :cond_92
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@94
    if-nez v9, :cond_f3

    #@96
    iget v9, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@98
    iget v10, p0, Landroid/view/View;->mBottom:I

    #@9a
    add-int/2addr v9, v10

    #@9b
    iget v10, p0, Landroid/view/View;->mTop:I

    #@9d
    sub-int/2addr v9, v10

    #@9e
    iget v10, p0, Landroid/widget/SlidingDrawer;->mHandleHeight:I

    #@a0
    sub-int/2addr v9, v10

    #@a1
    iget v10, p0, Landroid/widget/SlidingDrawer;->mTapThreshold:I

    #@a3
    sub-int/2addr v9, v10

    #@a4
    if-le v3, v9, :cond_f3

    #@a6
    .line 456
    :cond_a6
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mAllowSingleTap:Z

    #@a8
    if-eqz v9, :cond_106

    #@aa
    .line 457
    const/4 v9, 0x0

    #@ab
    invoke-virtual {p0, v9}, Landroid/widget/SlidingDrawer;->playSoundEffect(I)V

    #@ae
    .line 459
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@b0
    if-eqz v9, :cond_fd

    #@b2
    .line 460
    if-eqz v6, :cond_fb

    #@b4
    .end local v3           #top:I
    :goto_b4
    invoke-direct {p0, v3}, Landroid/widget/SlidingDrawer;->animateClose(I)V

    #@b7
    goto/16 :goto_16

    #@b9
    .line 423
    .end local v1           #left:I
    .end local v2           #negative:Z
    .end local v4           #velocity:F
    :cond_b9
    const/4 v2, 0x0

    #@ba
    goto :goto_54

    #@bb
    .line 431
    :cond_bb
    const/4 v9, 0x0

    #@bc
    cmpg-float v9, v7, v9

    #@be
    if-gez v9, :cond_d2

    #@c0
    const/4 v2, 0x1

    #@c1
    .line 432
    .restart local v2       #negative:Z
    :goto_c1
    const/4 v9, 0x0

    #@c2
    cmpg-float v9, v8, v9

    #@c4
    if-gez v9, :cond_c7

    #@c6
    .line 433
    neg-float v8, v8

    #@c7
    .line 435
    :cond_c7
    iget v9, p0, Landroid/widget/SlidingDrawer;->mMaximumMinorVelocity:I

    #@c9
    int-to-float v9, v9

    #@ca
    cmpl-float v9, v8, v9

    #@cc
    if-lez v9, :cond_64

    #@ce
    .line 436
    iget v9, p0, Landroid/widget/SlidingDrawer;->mMaximumMinorVelocity:I

    #@d0
    int-to-float v8, v9

    #@d1
    goto :goto_64

    #@d2
    .line 431
    .end local v2           #negative:Z
    :cond_d2
    const/4 v2, 0x0

    #@d3
    goto :goto_c1

    #@d4
    .line 449
    .restart local v1       #left:I
    .restart local v2       #negative:Z
    .restart local v3       #top:I
    .restart local v4       #velocity:F
    :cond_d4
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@d6
    if-eqz v9, :cond_df

    #@d8
    iget v9, p0, Landroid/widget/SlidingDrawer;->mTapThreshold:I

    #@da
    iget v10, p0, Landroid/widget/SlidingDrawer;->mTopOffset:I

    #@dc
    add-int/2addr v9, v10

    #@dd
    if-lt v1, v9, :cond_a6

    #@df
    :cond_df
    iget-boolean v9, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@e1
    if-nez v9, :cond_f3

    #@e3
    iget v9, p0, Landroid/widget/SlidingDrawer;->mBottomOffset:I

    #@e5
    iget v10, p0, Landroid/view/View;->mRight:I

    #@e7
    add-int/2addr v9, v10

    #@e8
    iget v10, p0, Landroid/view/View;->mLeft:I

    #@ea
    sub-int/2addr v9, v10

    #@eb
    iget v10, p0, Landroid/widget/SlidingDrawer;->mHandleWidth:I

    #@ed
    sub-int/2addr v9, v10

    #@ee
    iget v10, p0, Landroid/widget/SlidingDrawer;->mTapThreshold:I

    #@f0
    sub-int/2addr v9, v10

    #@f1
    if-gt v1, v9, :cond_a6

    #@f3
    .line 469
    :cond_f3
    if-eqz v6, :cond_110

    #@f5
    .end local v3           #top:I
    :goto_f5
    const/4 v9, 0x0

    #@f6
    invoke-direct {p0, v3, v4, v9}, Landroid/widget/SlidingDrawer;->performFling(IFZ)V

    #@f9
    goto/16 :goto_16

    #@fb
    .restart local v3       #top:I
    :cond_fb
    move v3, v1

    #@fc
    .line 460
    goto :goto_b4

    #@fd
    .line 462
    :cond_fd
    if-eqz v6, :cond_104

    #@ff
    .end local v3           #top:I
    :goto_ff
    invoke-direct {p0, v3}, Landroid/widget/SlidingDrawer;->animateOpen(I)V

    #@102
    goto/16 :goto_16

    #@104
    .restart local v3       #top:I
    :cond_104
    move v3, v1

    #@105
    goto :goto_ff

    #@106
    .line 465
    :cond_106
    if-eqz v6, :cond_10e

    #@108
    .end local v3           #top:I
    :goto_108
    const/4 v9, 0x0

    #@109
    invoke-direct {p0, v3, v4, v9}, Landroid/widget/SlidingDrawer;->performFling(IFZ)V

    #@10c
    goto/16 :goto_16

    #@10e
    .restart local v3       #top:I
    :cond_10e
    move v3, v1

    #@10f
    goto :goto_108

    #@110
    :cond_110
    move v3, v1

    #@111
    .line 469
    goto :goto_f5

    #@112
    .line 472
    :cond_112
    if-eqz v6, :cond_11a

    #@114
    .end local v3           #top:I
    :goto_114
    const/4 v9, 0x0

    #@115
    invoke-direct {p0, v3, v4, v9}, Landroid/widget/SlidingDrawer;->performFling(IFZ)V

    #@118
    goto/16 :goto_16

    #@11a
    .restart local v3       #top:I
    :cond_11a
    move v3, v1

    #@11b
    goto :goto_114

    #@11c
    .line 479
    .end local v0           #action:I
    .end local v1           #left:I
    .end local v2           #negative:Z
    .end local v3           #top:I
    .end local v4           #velocity:F
    .end local v5           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v6           #vertical:Z
    .end local v7           #xVelocity:F
    .end local v8           #yVelocity:F
    :cond_11c
    const/4 v9, 0x0

    #@11d
    goto/16 :goto_5

    #@11f
    .line 408
    nop

    #@120
    :pswitch_data_120
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_26
        :pswitch_3b
    .end packed-switch
.end method

.method public open()V
    .registers 2

    #@0
    .prologue
    .line 753
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->openDrawer()V

    #@3
    .line 754
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@6
    .line 755
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->requestLayout()V

    #@9
    .line 757
    const/16 v0, 0x20

    #@b
    invoke-virtual {p0, v0}, Landroid/widget/SlidingDrawer;->sendAccessibilityEvent(I)V

    #@e
    .line 758
    return-void
.end method

.method public setOnDrawerCloseListener(Landroid/widget/SlidingDrawer$OnDrawerCloseListener;)V
    .registers 2
    .parameter "onDrawerCloseListener"

    #@0
    .prologue
    .line 876
    iput-object p1, p0, Landroid/widget/SlidingDrawer;->mOnDrawerCloseListener:Landroid/widget/SlidingDrawer$OnDrawerCloseListener;

    #@2
    .line 877
    return-void
.end method

.method public setOnDrawerOpenListener(Landroid/widget/SlidingDrawer$OnDrawerOpenListener;)V
    .registers 2
    .parameter "onDrawerOpenListener"

    #@0
    .prologue
    .line 867
    iput-object p1, p0, Landroid/widget/SlidingDrawer;->mOnDrawerOpenListener:Landroid/widget/SlidingDrawer$OnDrawerOpenListener;

    #@2
    .line 868
    return-void
.end method

.method public setOnDrawerScrollListener(Landroid/widget/SlidingDrawer$OnDrawerScrollListener;)V
    .registers 2
    .parameter "onDrawerScrollListener"

    #@0
    .prologue
    .line 888
    iput-object p1, p0, Landroid/widget/SlidingDrawer;->mOnDrawerScrollListener:Landroid/widget/SlidingDrawer$OnDrawerScrollListener;

    #@2
    .line 889
    return-void
.end method

.method public toggle()V
    .registers 2

    #@0
    .prologue
    .line 719
    iget-boolean v0, p0, Landroid/widget/SlidingDrawer;->mExpanded:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 720
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->openDrawer()V

    #@7
    .line 724
    :goto_7
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->invalidate()V

    #@a
    .line 725
    invoke-virtual {p0}, Landroid/widget/SlidingDrawer;->requestLayout()V

    #@d
    .line 726
    return-void

    #@e
    .line 722
    :cond_e
    invoke-direct {p0}, Landroid/widget/SlidingDrawer;->closeDrawer()V

    #@11
    goto :goto_7
.end method

.method public unlock()V
    .registers 2

    #@0
    .prologue
    .line 917
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/SlidingDrawer;->mLocked:Z

    #@3
    .line 918
    return-void
.end method
