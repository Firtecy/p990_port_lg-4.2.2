.class Landroid/widget/Toast$TN;
.super Landroid/app/ITransientNotification$Stub;
.source "Toast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Toast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TN"
.end annotation


# instance fields
.field mGravity:I

.field final mHandler:Landroid/os/Handler;

.field final mHide:Ljava/lang/Runnable;

.field mHorizontalMargin:F

.field mNextView:Landroid/view/View;

.field private final mParams:Landroid/view/WindowManager$LayoutParams;

.field final mShow:Ljava/lang/Runnable;

.field mVerticalMargin:F

.field mView:Landroid/view/View;

.field mWM:Landroid/view/WindowManager;

.field mX:I

.field mY:I


# direct methods
.method constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x2

    #@1
    .line 351
    invoke-direct {p0}, Landroid/app/ITransientNotification$Stub;-><init>()V

    #@4
    .line 321
    new-instance v1, Landroid/widget/Toast$TN$1;

    #@6
    invoke-direct {v1, p0}, Landroid/widget/Toast$TN$1;-><init>(Landroid/widget/Toast$TN;)V

    #@9
    iput-object v1, p0, Landroid/widget/Toast$TN;->mShow:Ljava/lang/Runnable;

    #@b
    .line 328
    new-instance v1, Landroid/widget/Toast$TN$2;

    #@d
    invoke-direct {v1, p0}, Landroid/widget/Toast$TN$2;-><init>(Landroid/widget/Toast$TN;)V

    #@10
    iput-object v1, p0, Landroid/widget/Toast$TN;->mHide:Ljava/lang/Runnable;

    #@12
    .line 337
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    #@14
    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@17
    iput-object v1, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@19
    .line 338
    new-instance v1, Landroid/os/Handler;

    #@1b
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@1e
    iput-object v1, p0, Landroid/widget/Toast$TN;->mHandler:Landroid/os/Handler;

    #@20
    .line 340
    const/16 v1, 0x51

    #@22
    iput v1, p0, Landroid/widget/Toast$TN;->mGravity:I

    #@24
    .line 354
    iget-object v0, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@26
    .line 355
    .local v0, params:Landroid/view/WindowManager$LayoutParams;
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@28
    .line 356
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2a
    .line 357
    const/16 v1, 0x98

    #@2c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2e
    .line 360
    const/4 v1, -0x3

    #@2f
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@31
    .line 361
    const v1, 0x1030004

    #@34
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@36
    .line 362
    const/16 v1, 0x7d5

    #@38
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3a
    .line 363
    const-string v1, "Toast"

    #@3c
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@3f
    .line 364
    return-void
.end method

.method private trySendAccessibilityEvent()V
    .registers 4

    #@0
    .prologue
    .line 445
    iget-object v2, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@2
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@9
    move-result-object v0

    #@a
    .line 447
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_11

    #@10
    .line 458
    :goto_10
    return-void

    #@11
    .line 452
    :cond_11
    const/16 v2, 0x40

    #@13
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@16
    move-result-object v1

    #@17
    .line 454
    .local v1, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@22
    .line 455
    iget-object v2, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@24
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@2f
    .line 456
    iget-object v2, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@31
    invoke-virtual {v2, v1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@34
    .line 457
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@37
    goto :goto_10
.end method


# virtual methods
.method public handleHide()V
    .registers 3

    #@0
    .prologue
    .line 462
    iget-object v0, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 466
    iget-object v0, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@9
    move-result-object v0

    #@a
    if-eqz v0, :cond_13

    #@c
    .line 468
    iget-object v0, p0, Landroid/widget/Toast$TN;->mWM:Landroid/view/WindowManager;

    #@e
    iget-object v1, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@10
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@13
    .line 471
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@16
    .line 473
    :cond_16
    return-void
.end method

.method public handleShow()V
    .registers 13

    #@0
    .prologue
    const/high16 v10, 0x3f80

    #@2
    const/4 v11, 0x0

    #@3
    .line 387
    iget-object v8, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@5
    iget-object v9, p0, Landroid/widget/Toast$TN;->mNextView:Landroid/view/View;

    #@7
    if-eq v8, v9, :cond_ea

    #@9
    .line 390
    invoke-virtual {p0}, Landroid/widget/Toast$TN;->handleHide()V

    #@c
    .line 391
    iget-object v8, p0, Landroid/widget/Toast$TN;->mNextView:Landroid/view/View;

    #@e
    iput-object v8, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@10
    .line 392
    iget-object v8, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@12
    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@15
    move-result-object v8

    #@16
    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@19
    move-result-object v1

    #@1a
    .line 393
    .local v1, context:Landroid/content/Context;
    if-nez v1, :cond_22

    #@1c
    .line 394
    iget-object v8, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@1e
    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@21
    move-result-object v1

    #@22
    .line 396
    :cond_22
    const-string/jumbo v8, "window"

    #@25
    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@28
    move-result-object v8

    #@29
    check-cast v8, Landroid/view/WindowManager;

    #@2b
    iput-object v8, p0, Landroid/widget/Toast$TN;->mWM:Landroid/view/WindowManager;

    #@2d
    .line 399
    iget-object v8, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@2f
    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@3a
    move-result-object v0

    #@3b
    .line 400
    .local v0, config:Landroid/content/res/Configuration;
    iget v8, p0, Landroid/widget/Toast$TN;->mGravity:I

    #@3d
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    #@40
    move-result v9

    #@41
    invoke-static {v8, v9}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@44
    move-result v5

    #@45
    .line 401
    .local v5, gravity:I
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@47
    iput v5, v8, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@49
    .line 402
    and-int/lit8 v8, v5, 0x7

    #@4b
    const/4 v9, 0x7

    #@4c
    if-ne v8, v9, :cond_52

    #@4e
    .line 403
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@50
    iput v10, v8, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@52
    .line 405
    :cond_52
    and-int/lit8 v8, v5, 0x70

    #@54
    const/16 v9, 0x70

    #@56
    if-ne v8, v9, :cond_5c

    #@58
    .line 406
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@5a
    iput v10, v8, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@5c
    .line 408
    :cond_5c
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@5e
    iget v9, p0, Landroid/widget/Toast$TN;->mX:I

    #@60
    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->x:I

    #@62
    .line 409
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@64
    iget v9, p0, Landroid/widget/Toast$TN;->mY:I

    #@66
    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->y:I

    #@68
    .line 410
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@6a
    iget v9, p0, Landroid/widget/Toast$TN;->mVerticalMargin:F

    #@6c
    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@6e
    .line 411
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@70
    iget v9, p0, Landroid/widget/Toast$TN;->mHorizontalMargin:F

    #@72
    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@74
    .line 412
    iget-object v8, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@76
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@79
    move-result-object v8

    #@7a
    if-eqz v8, :cond_83

    #@7c
    .line 414
    iget-object v8, p0, Landroid/widget/Toast$TN;->mWM:Landroid/view/WindowManager;

    #@7e
    iget-object v9, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@80
    invoke-interface {v8, v9}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@83
    .line 420
    :cond_83
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@85
    if-eqz v8, :cond_de

    #@87
    const/16 v8, 0x51

    #@89
    iget-object v9, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@8b
    iget v9, v9, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@8d
    if-ne v8, v9, :cond_de

    #@8f
    .line 421
    invoke-static {}, Landroid/widget/Toast;->access$000()Ljava/util/HashMap;

    #@92
    move-result-object v8

    #@93
    invoke-virtual {v8, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    move-result-object v2

    #@97
    check-cast v2, Landroid/content/Context;

    #@99
    .line 422
    .local v2, ctx:Landroid/content/Context;
    if-eqz v2, :cond_de

    #@9b
    instance-of v8, v2, Landroid/app/Activity;

    #@9d
    if-eqz v8, :cond_de

    #@9f
    move-object v8, v2

    #@a0
    .line 423
    check-cast v8, Landroid/app/Activity;

    #@a2
    invoke-virtual {v8}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@a5
    move-result-object v8

    #@a6
    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@a9
    move-result-object v3

    #@aa
    .line 424
    .local v3, decor:Landroid/view/View;
    new-instance v7, Landroid/graphics/Rect;

    #@ac
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@af
    .line 425
    .local v7, rect:Landroid/graphics/Rect;
    invoke-virtual {v3, v7}, Landroid/view/View;->isWindowSplit(Landroid/graphics/Rect;)Z

    #@b2
    move-result v8

    #@b3
    if-eqz v8, :cond_de

    #@b5
    .line 426
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b8
    move-result-object v8

    #@b9
    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@bc
    move-result-object v8

    #@bd
    iget v4, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    #@bf
    .line 427
    .local v4, displayHeight:I
    iget v8, v7, Landroid/graphics/Rect;->top:I

    #@c1
    iget v9, v7, Landroid/graphics/Rect;->bottom:I

    #@c3
    iget v10, v7, Landroid/graphics/Rect;->top:I

    #@c5
    sub-int/2addr v9, v10

    #@c6
    div-int/lit8 v9, v9, 0x2

    #@c8
    add-int v6, v8, v9

    #@ca
    .line 429
    .local v6, mid:I
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@cc
    const/16 v9, 0x31

    #@ce
    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@d0
    .line 430
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@d2
    add-int/lit8 v9, v6, -0x32

    #@d4
    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->y:I

    #@d6
    .line 431
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@d8
    iput v11, v8, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@da
    .line 432
    iget-object v8, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@dc
    iput v11, v8, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@de
    .line 439
    .end local v2           #ctx:Landroid/content/Context;
    .end local v3           #decor:Landroid/view/View;
    .end local v4           #displayHeight:I
    .end local v6           #mid:I
    .end local v7           #rect:Landroid/graphics/Rect;
    :cond_de
    iget-object v8, p0, Landroid/widget/Toast$TN;->mWM:Landroid/view/WindowManager;

    #@e0
    iget-object v9, p0, Landroid/widget/Toast$TN;->mView:Landroid/view/View;

    #@e2
    iget-object v10, p0, Landroid/widget/Toast$TN;->mParams:Landroid/view/WindowManager$LayoutParams;

    #@e4
    invoke-interface {v8, v9, v10}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@e7
    .line 440
    invoke-direct {p0}, Landroid/widget/Toast$TN;->trySendAccessibilityEvent()V

    #@ea
    .line 442
    .end local v0           #config:Landroid/content/res/Configuration;
    .end local v1           #context:Landroid/content/Context;
    .end local v5           #gravity:I
    :cond_ea
    return-void
.end method

.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 381
    iget-object v0, p0, Landroid/widget/Toast$TN;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Landroid/widget/Toast$TN;->mHide:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 382
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Landroid/widget/Toast$TN;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Landroid/widget/Toast$TN;->mShow:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 373
    return-void
.end method
