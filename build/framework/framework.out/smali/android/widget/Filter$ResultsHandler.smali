.class Landroid/widget/Filter$ResultsHandler;
.super Landroid/os/Handler;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResultsHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/Filter;


# direct methods
.method private constructor <init>(Landroid/widget/Filter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 267
    iput-object p1, p0, Landroid/widget/Filter$ResultsHandler;->this$0:Landroid/widget/Filter;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/Filter;Landroid/widget/Filter$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 267
    invoke-direct {p0, p1}, Landroid/widget/Filter$ResultsHandler;-><init>(Landroid/widget/Filter;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 280
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/widget/Filter$RequestArguments;

    #@4
    .line 282
    .local v0, args:Landroid/widget/Filter$RequestArguments;
    iget-object v2, p0, Landroid/widget/Filter$ResultsHandler;->this$0:Landroid/widget/Filter;

    #@6
    iget-object v3, v0, Landroid/widget/Filter$RequestArguments;->constraint:Ljava/lang/CharSequence;

    #@8
    iget-object v4, v0, Landroid/widget/Filter$RequestArguments;->results:Landroid/widget/Filter$FilterResults;

    #@a
    invoke-virtual {v2, v3, v4}, Landroid/widget/Filter;->publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V

    #@d
    .line 283
    iget-object v2, v0, Landroid/widget/Filter$RequestArguments;->listener:Landroid/widget/Filter$FilterListener;

    #@f
    if-eqz v2, :cond_1e

    #@11
    .line 284
    iget-object v2, v0, Landroid/widget/Filter$RequestArguments;->results:Landroid/widget/Filter$FilterResults;

    #@13
    if-eqz v2, :cond_1f

    #@15
    iget-object v2, v0, Landroid/widget/Filter$RequestArguments;->results:Landroid/widget/Filter$FilterResults;

    #@17
    iget v1, v2, Landroid/widget/Filter$FilterResults;->count:I

    #@19
    .line 285
    .local v1, count:I
    :goto_19
    iget-object v2, v0, Landroid/widget/Filter$RequestArguments;->listener:Landroid/widget/Filter$FilterListener;

    #@1b
    invoke-interface {v2, v1}, Landroid/widget/Filter$FilterListener;->onFilterComplete(I)V

    #@1e
    .line 287
    .end local v1           #count:I
    :cond_1e
    return-void

    #@1f
    .line 284
    :cond_1f
    const/4 v1, -0x1

    #@20
    goto :goto_19
.end method
