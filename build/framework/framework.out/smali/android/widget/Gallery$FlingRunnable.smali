.class Landroid/widget/Gallery$FlingRunnable;
.super Ljava/lang/Object;
.source "Gallery.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Gallery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlingRunnable"
.end annotation


# instance fields
.field private mLastFlingX:I

.field private mScroller:Landroid/widget/Scroller;

.field final synthetic this$0:Landroid/widget/Gallery;


# direct methods
.method public constructor <init>(Landroid/widget/Gallery;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 1438
    iput-object p1, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1439
    new-instance v0, Landroid/widget/Scroller;

    #@7
    invoke-virtual {p1}, Landroid/widget/Gallery;->getContext()Landroid/content/Context;

    #@a
    move-result-object v1

    #@b
    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    #@e
    iput-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    #@10
    .line 1440
    return-void
.end method

.method static synthetic access$100(Landroid/widget/Gallery$FlingRunnable;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1427
    invoke-direct {p0, p1}, Landroid/widget/Gallery$FlingRunnable;->endFling(Z)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/Gallery$FlingRunnable;)Landroid/widget/Scroller;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1427
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    #@2
    return-object v0
.end method

.method private endFling(Z)V
    .registers 4
    .parameter "scrollIntoSlots"

    #@0
    .prologue
    .line 1479
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    #@6
    .line 1481
    if-eqz p1, :cond_d

    #@8
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@a
    invoke-static {v0}, Landroid/widget/Gallery;->access$500(Landroid/widget/Gallery;)V

    #@d
    .line 1482
    :cond_d
    return-void
.end method

.method private startCommon()V
    .registers 2

    #@0
    .prologue
    .line 1444
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@2
    invoke-virtual {v0, p0}, Landroid/widget/Gallery;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@5
    .line 1445
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 1487
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@3
    iget v5, v5, Landroid/widget/AdapterView;->mItemCount:I

    #@5
    if-nez v5, :cond_b

    #@7
    .line 1488
    invoke-direct {p0, v8}, Landroid/widget/Gallery$FlingRunnable;->endFling(Z)V

    #@a
    .line 1528
    :goto_a
    return-void

    #@b
    .line 1492
    :cond_b
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@d
    const/4 v6, 0x0

    #@e
    invoke-static {v5, v6}, Landroid/widget/Gallery;->access$602(Landroid/widget/Gallery;Z)Z

    #@11
    .line 1494
    iget-object v3, p0, Landroid/widget/Gallery$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    #@13
    .line 1495
    .local v3, scroller:Landroid/widget/Scroller;
    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@16
    move-result v1

    #@17
    .line 1496
    .local v1, more:Z
    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    #@1a
    move-result v4

    #@1b
    .line 1500
    .local v4, x:I
    iget v5, p0, Landroid/widget/Gallery$FlingRunnable;->mLastFlingX:I

    #@1d
    sub-int v0, v5, v4

    #@1f
    .line 1503
    .local v0, delta:I
    if-lez v0, :cond_71

    #@21
    .line 1505
    iget-object v6, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@23
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@25
    invoke-static {v5}, Landroid/widget/Gallery;->access$800(Landroid/widget/Gallery;)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_6c

    #@2b
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@2d
    iget v5, v5, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2f
    iget-object v7, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@31
    invoke-virtual {v7}, Landroid/widget/Gallery;->getChildCount()I

    #@34
    move-result v7

    #@35
    add-int/2addr v5, v7

    #@36
    add-int/lit8 v5, v5, -0x1

    #@38
    :goto_38
    invoke-static {v6, v5}, Landroid/widget/Gallery;->access$702(Landroid/widget/Gallery;I)I

    #@3b
    .line 1509
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@3d
    invoke-virtual {v5}, Landroid/widget/Gallery;->getWidth()I

    #@40
    move-result v5

    #@41
    iget-object v6, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@43
    invoke-static {v6}, Landroid/widget/Gallery;->access$900(Landroid/widget/Gallery;)I

    #@46
    move-result v6

    #@47
    sub-int/2addr v5, v6

    #@48
    iget-object v6, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@4a
    invoke-static {v6}, Landroid/widget/Gallery;->access$1000(Landroid/widget/Gallery;)I

    #@4d
    move-result v6

    #@4e
    sub-int/2addr v5, v6

    #@4f
    add-int/lit8 v5, v5, -0x1

    #@51
    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    #@54
    move-result v0

    #@55
    .line 1520
    :goto_55
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@57
    invoke-virtual {v5, v0}, Landroid/widget/Gallery;->trackMotionScroll(I)V

    #@5a
    .line 1522
    if-eqz v1, :cond_b4

    #@5c
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@5e
    invoke-static {v5}, Landroid/widget/Gallery;->access$600(Landroid/widget/Gallery;)Z

    #@61
    move-result v5

    #@62
    if-nez v5, :cond_b4

    #@64
    .line 1523
    iput v4, p0, Landroid/widget/Gallery$FlingRunnable;->mLastFlingX:I

    #@66
    .line 1524
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@68
    invoke-virtual {v5, p0}, Landroid/widget/Gallery;->post(Ljava/lang/Runnable;)Z

    #@6b
    goto :goto_a

    #@6c
    .line 1505
    :cond_6c
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@6e
    iget v5, v5, Landroid/widget/AdapterView;->mFirstPosition:I

    #@70
    goto :goto_38

    #@71
    .line 1512
    :cond_71
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@73
    invoke-virtual {v5}, Landroid/widget/Gallery;->getChildCount()I

    #@76
    move-result v5

    #@77
    add-int/lit8 v2, v5, -0x1

    #@79
    .line 1513
    .local v2, offsetToLast:I
    iget-object v6, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@7b
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@7d
    invoke-static {v5}, Landroid/widget/Gallery;->access$800(Landroid/widget/Gallery;)Z

    #@80
    move-result v5

    #@81
    if-eqz v5, :cond_a6

    #@83
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@85
    iget v5, v5, Landroid/widget/AdapterView;->mFirstPosition:I

    #@87
    :goto_87
    invoke-static {v6, v5}, Landroid/widget/Gallery;->access$702(Landroid/widget/Gallery;I)I

    #@8a
    .line 1517
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@8c
    invoke-virtual {v5}, Landroid/widget/Gallery;->getWidth()I

    #@8f
    move-result v5

    #@90
    iget-object v6, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@92
    invoke-static {v6}, Landroid/widget/Gallery;->access$1100(Landroid/widget/Gallery;)I

    #@95
    move-result v6

    #@96
    sub-int/2addr v5, v6

    #@97
    iget-object v6, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@99
    invoke-static {v6}, Landroid/widget/Gallery;->access$1200(Landroid/widget/Gallery;)I

    #@9c
    move-result v6

    #@9d
    sub-int/2addr v5, v6

    #@9e
    add-int/lit8 v5, v5, -0x1

    #@a0
    neg-int v5, v5

    #@a1
    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    #@a4
    move-result v0

    #@a5
    goto :goto_55

    #@a6
    .line 1513
    :cond_a6
    iget-object v5, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@a8
    iget v5, v5, Landroid/widget/AdapterView;->mFirstPosition:I

    #@aa
    iget-object v7, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@ac
    invoke-virtual {v7}, Landroid/widget/Gallery;->getChildCount()I

    #@af
    move-result v7

    #@b0
    add-int/2addr v5, v7

    #@b1
    add-int/lit8 v5, v5, -0x1

    #@b3
    goto :goto_87

    #@b4
    .line 1526
    .end local v2           #offsetToLast:I
    :cond_b4
    invoke-direct {p0, v8}, Landroid/widget/Gallery$FlingRunnable;->endFling(Z)V

    #@b7
    goto/16 :goto_a
.end method

.method public startUsingDistance(I)V
    .registers 8
    .parameter "distance"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1460
    if-nez p1, :cond_4

    #@3
    .line 1467
    :goto_3
    return-void

    #@4
    .line 1462
    :cond_4
    invoke-direct {p0}, Landroid/widget/Gallery$FlingRunnable;->startCommon()V

    #@7
    .line 1464
    iput v1, p0, Landroid/widget/Gallery$FlingRunnable;->mLastFlingX:I

    #@9
    .line 1465
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    #@b
    neg-int v3, p1

    #@c
    iget-object v2, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@e
    invoke-static {v2}, Landroid/widget/Gallery;->access$400(Landroid/widget/Gallery;)I

    #@11
    move-result v5

    #@12
    move v2, v1

    #@13
    move v4, v1

    #@14
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@17
    .line 1466
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@19
    invoke-virtual {v0, p0}, Landroid/widget/Gallery;->post(Ljava/lang/Runnable;)Z

    #@1c
    goto :goto_3
.end method

.method public startUsingVelocity(I)V
    .registers 11
    .parameter "initialVelocity"

    #@0
    .prologue
    const v6, 0x7fffffff

    #@3
    const/4 v2, 0x0

    #@4
    .line 1448
    if-nez p1, :cond_7

    #@6
    .line 1457
    :goto_6
    return-void

    #@7
    .line 1450
    :cond_7
    invoke-direct {p0}, Landroid/widget/Gallery$FlingRunnable;->startCommon()V

    #@a
    .line 1452
    if-gez p1, :cond_1f

    #@c
    move v1, v6

    #@d
    .line 1453
    .local v1, initialX:I
    :goto_d
    iput v1, p0, Landroid/widget/Gallery$FlingRunnable;->mLastFlingX:I

    #@f
    .line 1454
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    #@11
    move v3, p1

    #@12
    move v4, v2

    #@13
    move v5, v2

    #@14
    move v7, v2

    #@15
    move v8, v6

    #@16
    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    #@19
    .line 1456
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@1b
    invoke-virtual {v0, p0}, Landroid/widget/Gallery;->post(Ljava/lang/Runnable;)Z

    #@1e
    goto :goto_6

    #@1f
    .end local v1           #initialX:I
    :cond_1f
    move v1, v2

    #@20
    .line 1452
    goto :goto_d
.end method

.method public stop(Z)V
    .registers 3
    .parameter "scrollIntoSlots"

    #@0
    .prologue
    .line 1470
    iget-object v0, p0, Landroid/widget/Gallery$FlingRunnable;->this$0:Landroid/widget/Gallery;

    #@2
    invoke-virtual {v0, p0}, Landroid/widget/Gallery;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@5
    .line 1471
    invoke-direct {p0, p1}, Landroid/widget/Gallery$FlingRunnable;->endFling(Z)V

    #@8
    .line 1472
    return-void
.end method
