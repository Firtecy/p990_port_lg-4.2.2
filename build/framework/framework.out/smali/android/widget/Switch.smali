.class public Landroid/widget/Switch;
.super Landroid/widget/CompoundButton;
.source "Switch.java"


# static fields
.field private static final CHECKED_STATE_SET:[I = null

.field private static final MONOSPACE:I = 0x3

.field private static final SANS:I = 0x1

.field private static final SERIF:I = 0x2

.field private static final TOUCH_MODE_DOWN:I = 0x1

.field private static final TOUCH_MODE_DRAGGING:I = 0x2

.field private static final TOUCH_MODE_IDLE:I


# instance fields
.field private mMinFlingVelocity:I

.field private mOffLayout:Landroid/text/Layout;

.field private mOnLayout:Landroid/text/Layout;

.field private mSwitchBottom:I

.field private mSwitchHeight:I

.field private mSwitchLeft:I

.field private mSwitchMinWidth:I

.field private mSwitchPadding:I

.field private mSwitchRight:I

.field private mSwitchTop:I

.field private mSwitchTransformationMethod:Landroid/text/method/TransformationMethod2;

.field private mSwitchWidth:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTextColors:Landroid/content/res/ColorStateList;

.field private mTextOff:Ljava/lang/CharSequence;

.field private mTextOn:Ljava/lang/CharSequence;

.field private mTextPaint:Landroid/text/TextPaint;

.field private mThumbDrawable:Landroid/graphics/drawable/Drawable;

.field private mThumbPosition:F

.field private mThumbTextPadding:I

.field private mThumbWidth:I

.field private mTouchMode:I

.field private mTouchSlop:I

.field private mTouchX:F

.field private mTouchY:F

.field private mTrackDrawable:Landroid/graphics/drawable/Drawable;

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 115
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const v2, 0x10100a0

    #@7
    aput v2, v0, v1

    #@9
    sput-object v0, Landroid/widget/Switch;->CHECKED_STATE_SET:[I

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 125
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 136
    const v0, 0x10103f6

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 149
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 93
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@8
    move-result-object v4

    #@9
    iput-object v4, p0, Landroid/widget/Switch;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@b
    .line 112
    new-instance v4, Landroid/graphics/Rect;

    #@d
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@10
    iput-object v4, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@12
    .line 151
    new-instance v4, Landroid/text/TextPaint;

    #@14
    invoke-direct {v4, v7}, Landroid/text/TextPaint;-><init>(I)V

    #@17
    iput-object v4, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@19
    .line 152
    invoke-virtual {p0}, Landroid/widget/Switch;->getResources()Landroid/content/res/Resources;

    #@1c
    move-result-object v3

    #@1d
    .line 153
    .local v3, res:Landroid/content/res/Resources;
    iget-object v4, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@1f
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@22
    move-result-object v5

    #@23
    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    #@25
    iput v5, v4, Landroid/text/TextPaint;->density:F

    #@27
    .line 154
    iget-object v4, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@29
    invoke-virtual {v3}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@2c
    move-result-object v5

    #@2d
    iget v5, v5, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@2f
    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setCompatibilityScaling(F)V

    #@32
    .line 156
    sget-object v4, Lcom/android/internal/R$styleable;->Switch:[I

    #@34
    invoke-virtual {p1, p2, v4, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@37
    move-result-object v0

    #@38
    .line 159
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x2

    #@39
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3c
    move-result-object v4

    #@3d
    iput-object v4, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@3f
    .line 160
    const/4 v4, 0x4

    #@40
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@43
    move-result-object v4

    #@44
    iput-object v4, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@46
    .line 161
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@49
    move-result-object v4

    #@4a
    iput-object v4, p0, Landroid/widget/Switch;->mTextOn:Ljava/lang/CharSequence;

    #@4c
    .line 162
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@4f
    move-result-object v4

    #@50
    iput-object v4, p0, Landroid/widget/Switch;->mTextOff:Ljava/lang/CharSequence;

    #@52
    .line 163
    const/4 v4, 0x7

    #@53
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@56
    move-result v4

    #@57
    iput v4, p0, Landroid/widget/Switch;->mThumbTextPadding:I

    #@59
    .line 165
    const/4 v4, 0x5

    #@5a
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@5d
    move-result v4

    #@5e
    iput v4, p0, Landroid/widget/Switch;->mSwitchMinWidth:I

    #@60
    .line 167
    const/4 v4, 0x6

    #@61
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@64
    move-result v4

    #@65
    iput v4, p0, Landroid/widget/Switch;->mSwitchPadding:I

    #@67
    .line 170
    const/4 v4, 0x3

    #@68
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@6b
    move-result v1

    #@6c
    .line 172
    .local v1, appearance:I
    if-eqz v1, :cond_71

    #@6e
    .line 173
    invoke-virtual {p0, p1, v1}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    #@71
    .line 175
    :cond_71
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@74
    .line 177
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@77
    move-result-object v2

    #@78
    .line 178
    .local v2, config:Landroid/view/ViewConfiguration;
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@7b
    move-result v4

    #@7c
    iput v4, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@7e
    .line 179
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@81
    move-result v4

    #@82
    iput v4, p0, Landroid/widget/Switch;->mMinFlingVelocity:I

    #@84
    .line 182
    invoke-virtual {p0}, Landroid/widget/Switch;->refreshDrawableState()V

    #@87
    .line 183
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@8a
    move-result v4

    #@8b
    invoke-virtual {p0, v4}, Landroid/widget/Switch;->setChecked(Z)V

    #@8e
    .line 184
    return-void
.end method

.method private animateThumbToCheckedState(Z)V
    .registers 2
    .parameter "newCheckedState"

    #@0
    .prologue
    .line 648
    invoke-virtual {p0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    #@3
    .line 649
    return-void
.end method

.method private cancelSuperTouch(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter "ev"

    #@0
    .prologue
    .line 611
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@3
    move-result-object v0

    #@4
    .line 612
    .local v0, cancel:Landroid/view/MotionEvent;
    const/4 v1, 0x3

    #@5
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@8
    .line 613
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@b
    .line 614
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    #@e
    .line 615
    return-void
.end method

.method private getTargetCheckedState()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 652
    invoke-virtual {p0}, Landroid/widget/Switch;->isLayoutRtl()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_18

    #@8
    .line 653
    iget v2, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@a
    invoke-direct {p0}, Landroid/widget/Switch;->getThumbScrollRange()I

    #@d
    move-result v3

    #@e
    div-int/lit8 v3, v3, 0x2

    #@10
    int-to-float v3, v3

    #@11
    cmpg-float v2, v2, v3

    #@13
    if-gtz v2, :cond_16

    #@15
    .line 655
    :cond_15
    :goto_15
    return v0

    #@16
    :cond_16
    move v0, v1

    #@17
    .line 653
    goto :goto_15

    #@18
    .line 655
    :cond_18
    iget v2, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@1a
    invoke-direct {p0}, Landroid/widget/Switch;->getThumbScrollRange()I

    #@1d
    move-result v3

    #@1e
    div-int/lit8 v3, v3, 0x2

    #@20
    int-to-float v3, v3

    #@21
    cmpl-float v2, v2, v3

    #@23
    if-gez v2, :cond_15

    #@25
    move v0, v1

    #@26
    goto :goto_15
.end method

.method private getThumbScrollRange()I
    .registers 3

    #@0
    .prologue
    .line 790
    iget-object v0, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 791
    const/4 v0, 0x0

    #@5
    .line 794
    :goto_5
    return v0

    #@6
    .line 793
    :cond_6
    iget-object v0, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    iget-object v1, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@a
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@d
    .line 794
    iget v0, p0, Landroid/widget/Switch;->mSwitchWidth:I

    #@f
    iget v1, p0, Landroid/widget/Switch;->mThumbWidth:I

    #@11
    sub-int/2addr v0, v1

    #@12
    iget-object v1, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@14
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@16
    sub-int/2addr v0, v1

    #@17
    iget-object v1, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@19
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@1b
    sub-int/2addr v0, v1

    #@1c
    goto :goto_5
.end method

.method private hitThumb(FF)Z
    .registers 10
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 534
    iget-object v4, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    iget-object v5, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@4
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@7
    .line 535
    iget v4, p0, Landroid/widget/Switch;->mSwitchTop:I

    #@9
    iget v5, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@b
    sub-int v3, v4, v5

    #@d
    .line 536
    .local v3, thumbTop:I
    iget v4, p0, Landroid/widget/Switch;->mSwitchLeft:I

    #@f
    iget v5, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@11
    const/high16 v6, 0x3f00

    #@13
    add-float/2addr v5, v6

    #@14
    float-to-int v5, v5

    #@15
    add-int/2addr v4, v5

    #@16
    iget v5, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@18
    sub-int v1, v4, v5

    #@1a
    .line 537
    .local v1, thumbLeft:I
    iget v4, p0, Landroid/widget/Switch;->mThumbWidth:I

    #@1c
    add-int/2addr v4, v1

    #@1d
    iget-object v5, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@1f
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@21
    add-int/2addr v4, v5

    #@22
    iget-object v5, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@24
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@26
    add-int/2addr v4, v5

    #@27
    iget v5, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@29
    add-int v2, v4, v5

    #@2b
    .line 539
    .local v2, thumbRight:I
    iget v4, p0, Landroid/widget/Switch;->mSwitchBottom:I

    #@2d
    iget v5, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@2f
    add-int v0, v4, v5

    #@31
    .line 540
    .local v0, thumbBottom:I
    int-to-float v4, v1

    #@32
    cmpl-float v4, p1, v4

    #@34
    if-lez v4, :cond_47

    #@36
    int-to-float v4, v2

    #@37
    cmpg-float v4, p1, v4

    #@39
    if-gez v4, :cond_47

    #@3b
    int-to-float v4, v3

    #@3c
    cmpl-float v4, p2, v4

    #@3e
    if-lez v4, :cond_47

    #@40
    int-to-float v4, v0

    #@41
    cmpg-float v4, p2, v4

    #@43
    if-gez v4, :cond_47

    #@45
    const/4 v4, 0x1

    #@46
    :goto_46
    return v4

    #@47
    :cond_47
    const/4 v4, 0x0

    #@48
    goto :goto_46
.end method

.method private makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .registers 10
    .parameter "text"

    #@0
    .prologue
    .line 521
    iget-object v0, p0, Landroid/widget/Switch;->mSwitchTransformationMethod:Landroid/text/method/TransformationMethod2;

    #@2
    if-eqz v0, :cond_24

    #@4
    iget-object v0, p0, Landroid/widget/Switch;->mSwitchTransformationMethod:Landroid/text/method/TransformationMethod2;

    #@6
    invoke-interface {v0, p1, p0}, Landroid/text/method/TransformationMethod2;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    #@9
    move-result-object v1

    #@a
    .line 525
    .local v1, transformed:Ljava/lang/CharSequence;
    :goto_a
    new-instance v0, Landroid/text/StaticLayout;

    #@c
    iget-object v2, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@e
    iget-object v3, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@10
    invoke-static {v1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    #@13
    move-result v3

    #@14
    float-to-double v3, v3

    #@15
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    #@18
    move-result-wide v3

    #@19
    double-to-int v3, v3

    #@1a
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@1c
    const/high16 v5, 0x3f80

    #@1e
    const/4 v6, 0x0

    #@1f
    const/4 v7, 0x1

    #@20
    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    #@23
    return-object v0

    #@24
    .end local v1           #transformed:Ljava/lang/CharSequence;
    :cond_24
    move-object v1, p1

    #@25
    .line 521
    goto :goto_a
.end method

.method private setSwitchTypefaceByIndex(II)V
    .registers 4
    .parameter "typefaceIndex"
    .parameter "styleIndex"

    #@0
    .prologue
    .line 240
    const/4 v0, 0x0

    #@1
    .line 241
    .local v0, tf:Landroid/graphics/Typeface;
    packed-switch p1, :pswitch_data_12

    #@4
    .line 255
    :goto_4
    invoke-virtual {p0, v0, p2}, Landroid/widget/Switch;->setSwitchTypeface(Landroid/graphics/Typeface;I)V

    #@7
    .line 256
    return-void

    #@8
    .line 243
    :pswitch_8
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@a
    .line 244
    goto :goto_4

    #@b
    .line 247
    :pswitch_b
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    #@d
    .line 248
    goto :goto_4

    #@e
    .line 251
    :pswitch_e
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    #@10
    goto :goto_4

    #@11
    .line 241
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_8
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method

.method private setThumbPosition(Z)V
    .registers 4
    .parameter "checked"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 660
    invoke-virtual {p0}, Landroid/widget/Switch;->isLayoutRtl()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_12

    #@7
    .line 661
    if-eqz p1, :cond_c

    #@9
    :goto_9
    iput v0, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@b
    .line 665
    :goto_b
    return-void

    #@c
    .line 661
    :cond_c
    invoke-direct {p0}, Landroid/widget/Switch;->getThumbScrollRange()I

    #@f
    move-result v0

    #@10
    int-to-float v0, v0

    #@11
    goto :goto_9

    #@12
    .line 663
    :cond_12
    if-eqz p1, :cond_19

    #@14
    invoke-direct {p0}, Landroid/widget/Switch;->getThumbScrollRange()I

    #@17
    move-result v0

    #@18
    int-to-float v0, v0

    #@19
    :cond_19
    iput v0, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@1b
    goto :goto_b
.end method

.method private stopDrag(Landroid/view/MotionEvent;)V
    .registers 10
    .parameter "ev"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 623
    iput v4, p0, Landroid/widget/Switch;->mTouchMode:I

    #@5
    .line 625
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@8
    move-result v5

    #@9
    if-ne v5, v3, :cond_3e

    #@b
    invoke-virtual {p0}, Landroid/widget/Switch;->isEnabled()Z

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_3e

    #@11
    move v0, v3

    #@12
    .line 627
    .local v0, commitChange:Z
    :goto_12
    invoke-direct {p0, p1}, Landroid/widget/Switch;->cancelSuperTouch(Landroid/view/MotionEvent;)V

    #@15
    .line 629
    if-eqz v0, :cond_4f

    #@17
    .line 631
    iget-object v5, p0, Landroid/widget/Switch;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@19
    const/16 v6, 0x3e8

    #@1b
    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@1e
    .line 632
    iget-object v5, p0, Landroid/widget/Switch;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@20
    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@23
    move-result v2

    #@24
    .line 633
    .local v2, xvel:F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    #@27
    move-result v5

    #@28
    iget v6, p0, Landroid/widget/Switch;->mMinFlingVelocity:I

    #@2a
    int-to-float v6, v6

    #@2b
    cmpl-float v5, v5, v6

    #@2d
    if-lez v5, :cond_4a

    #@2f
    .line 634
    invoke-virtual {p0}, Landroid/widget/Switch;->isLayoutRtl()Z

    #@32
    move-result v5

    #@33
    if-eqz v5, :cond_42

    #@35
    cmpg-float v5, v2, v7

    #@37
    if-gez v5, :cond_40

    #@39
    move v1, v3

    #@3a
    .line 638
    .local v1, newState:Z
    :goto_3a
    invoke-direct {p0, v1}, Landroid/widget/Switch;->animateThumbToCheckedState(Z)V

    #@3d
    .line 642
    .end local v1           #newState:Z
    .end local v2           #xvel:F
    :goto_3d
    return-void

    #@3e
    .end local v0           #commitChange:Z
    :cond_3e
    move v0, v4

    #@3f
    .line 625
    goto :goto_12

    #@40
    .restart local v0       #commitChange:Z
    .restart local v2       #xvel:F
    :cond_40
    move v1, v4

    #@41
    .line 634
    goto :goto_3a

    #@42
    :cond_42
    cmpl-float v5, v2, v7

    #@44
    if-lez v5, :cond_48

    #@46
    move v1, v3

    #@47
    goto :goto_3a

    #@48
    :cond_48
    move v1, v4

    #@49
    goto :goto_3a

    #@4a
    .line 636
    :cond_4a
    invoke-direct {p0}, Landroid/widget/Switch;->getTargetCheckedState()Z

    #@4d
    move-result v1

    #@4e
    .restart local v1       #newState:Z
    goto :goto_3a

    #@4f
    .line 640
    .end local v1           #newState:Z
    .end local v2           #xvel:F
    :cond_4f
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@52
    move-result v3

    #@53
    invoke-direct {p0, v3}, Landroid/widget/Switch;->animateThumbToCheckedState(Z)V

    #@56
    goto :goto_3d
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 808
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    #@3
    .line 810
    invoke-virtual {p0}, Landroid/widget/Switch;->getDrawableState()[I

    #@6
    move-result-object v0

    #@7
    .line 814
    .local v0, myDrawableState:[I
    iget-object v1, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    if-eqz v1, :cond_10

    #@b
    iget-object v1, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@10
    .line 815
    :cond_10
    iget-object v1, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    if-eqz v1, :cond_19

    #@14
    iget-object v1, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@19
    .line 817
    :cond_19
    invoke-virtual {p0}, Landroid/widget/Switch;->invalidate()V

    #@1c
    .line 818
    return-void
.end method

.method public getCompoundPaddingLeft()I
    .registers 4

    #@0
    .prologue
    .line 767
    invoke-virtual {p0}, Landroid/widget/Switch;->isLayoutRtl()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_b

    #@6
    .line 768
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingLeft()I

    #@9
    move-result v0

    #@a
    .line 774
    :cond_a
    :goto_a
    return v0

    #@b
    .line 770
    :cond_b
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingLeft()I

    #@e
    move-result v1

    #@f
    iget v2, p0, Landroid/widget/Switch;->mSwitchWidth:I

    #@11
    add-int v0, v1, v2

    #@13
    .line 771
    .local v0, padding:I
    invoke-virtual {p0}, Landroid/widget/Switch;->getText()Ljava/lang/CharSequence;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_a

    #@1d
    .line 772
    iget v1, p0, Landroid/widget/Switch;->mSwitchPadding:I

    #@1f
    add-int/2addr v0, v1

    #@20
    goto :goto_a
.end method

.method public getCompoundPaddingRight()I
    .registers 4

    #@0
    .prologue
    .line 779
    invoke-virtual {p0}, Landroid/widget/Switch;->isLayoutRtl()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 780
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    #@9
    move-result v0

    #@a
    .line 786
    :cond_a
    :goto_a
    return v0

    #@b
    .line 782
    :cond_b
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    #@e
    move-result v1

    #@f
    iget v2, p0, Landroid/widget/Switch;->mSwitchWidth:I

    #@11
    add-int v0, v1, v2

    #@13
    .line 783
    .local v0, padding:I
    invoke-virtual {p0}, Landroid/widget/Switch;->getText()Ljava/lang/CharSequence;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_a

    #@1d
    .line 784
    iget v1, p0, Landroid/widget/Switch;->mSwitchPadding:I

    #@1f
    add-int/2addr v0, v1

    #@20
    goto :goto_a
.end method

.method public getSwitchMinWidth()I
    .registers 2

    #@0
    .prologue
    .line 349
    iget v0, p0, Landroid/widget/Switch;->mSwitchMinWidth:I

    #@2
    return v0
.end method

.method public getSwitchPadding()I
    .registers 2

    #@0
    .prologue
    .line 324
    iget v0, p0, Landroid/widget/Switch;->mSwitchPadding:I

    #@2
    return v0
.end method

.method public getTextOff()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 471
    iget-object v0, p0, Landroid/widget/Switch;->mTextOff:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTextOn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Landroid/widget/Switch;->mTextOn:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getThumbDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 443
    iget-object v0, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getThumbTextPadding()I
    .registers 2

    #@0
    .prologue
    .line 372
    iget v0, p0, Landroid/widget/Switch;->mThumbTextPadding:I

    #@2
    return v0
.end method

.method public getTrackDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 406
    iget-object v0, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 827
    invoke-super {p0}, Landroid/widget/CompoundButton;->jumpDrawablesToCurrentState()V

    #@3
    .line 828
    iget-object v0, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@8
    .line 829
    iget-object v0, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@d
    .line 830
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 4
    .parameter "extraSpace"

    #@0
    .prologue
    .line 799
    add-int/lit8 v1, p1, 0x1

    #@2
    invoke-super {p0, v1}, Landroid/widget/CompoundButton;->onCreateDrawableState(I)[I

    #@5
    move-result-object v0

    #@6
    .line 800
    .local v0, drawableState:[I
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 801
    sget-object v1, Landroid/widget/Switch;->CHECKED_STATE_SET:[I

    #@e
    invoke-static {v0, v1}, Landroid/widget/Switch;->mergeDrawableStates([I[I)[I

    #@11
    .line 803
    :cond_11
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 19
    .parameter "canvas"

    #@0
    .prologue
    .line 720
    invoke-super/range {p0 .. p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 723
    move-object/from16 v0, p0

    #@5
    iget v6, v0, Landroid/widget/Switch;->mSwitchLeft:I

    #@7
    .line 724
    .local v6, switchLeft:I
    move-object/from16 v0, p0

    #@9
    iget v9, v0, Landroid/widget/Switch;->mSwitchTop:I

    #@b
    .line 725
    .local v9, switchTop:I
    move-object/from16 v0, p0

    #@d
    iget v7, v0, Landroid/widget/Switch;->mSwitchRight:I

    #@f
    .line 726
    .local v7, switchRight:I
    move-object/from16 v0, p0

    #@11
    iget v1, v0, Landroid/widget/Switch;->mSwitchBottom:I

    #@13
    .line 728
    .local v1, switchBottom:I
    move-object/from16 v0, p0

    #@15
    iget-object v13, v0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@17
    invoke-virtual {v13, v6, v9, v7, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@1a
    .line 729
    move-object/from16 v0, p0

    #@1c
    iget-object v13, v0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    move-object/from16 v0, p1

    #@20
    invoke-virtual {v13, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@23
    .line 731
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@26
    .line 733
    move-object/from16 v0, p0

    #@28
    iget-object v13, v0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@2a
    move-object/from16 v0, p0

    #@2c
    iget-object v14, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@2e
    invoke-virtual {v13, v14}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@31
    .line 734
    move-object/from16 v0, p0

    #@33
    iget-object v13, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@35
    iget v13, v13, Landroid/graphics/Rect;->left:I

    #@37
    add-int v3, v6, v13

    #@39
    .line 735
    .local v3, switchInnerLeft:I
    move-object/from16 v0, p0

    #@3b
    iget-object v13, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@3d
    iget v13, v13, Landroid/graphics/Rect;->top:I

    #@3f
    add-int v5, v9, v13

    #@41
    .line 736
    .local v5, switchInnerTop:I
    move-object/from16 v0, p0

    #@43
    iget-object v13, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@45
    iget v13, v13, Landroid/graphics/Rect;->right:I

    #@47
    sub-int v4, v7, v13

    #@49
    .line 737
    .local v4, switchInnerRight:I
    move-object/from16 v0, p0

    #@4b
    iget-object v13, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@4d
    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    #@4f
    sub-int v2, v1, v13

    #@51
    .line 738
    .local v2, switchInnerBottom:I
    move-object/from16 v0, p1

    #@53
    invoke-virtual {v0, v3, v9, v4, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@56
    .line 740
    move-object/from16 v0, p0

    #@58
    iget-object v13, v0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@5a
    move-object/from16 v0, p0

    #@5c
    iget-object v14, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@5e
    invoke-virtual {v13, v14}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@61
    .line 741
    move-object/from16 v0, p0

    #@63
    iget v13, v0, Landroid/widget/Switch;->mThumbPosition:F

    #@65
    const/high16 v14, 0x3f00

    #@67
    add-float/2addr v13, v14

    #@68
    float-to-int v11, v13

    #@69
    .line 742
    .local v11, thumbPos:I
    move-object/from16 v0, p0

    #@6b
    iget-object v13, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@6d
    iget v13, v13, Landroid/graphics/Rect;->left:I

    #@6f
    sub-int v13, v3, v13

    #@71
    add-int v10, v13, v11

    #@73
    .line 743
    .local v10, thumbLeft:I
    add-int v13, v3, v11

    #@75
    move-object/from16 v0, p0

    #@77
    iget v14, v0, Landroid/widget/Switch;->mThumbWidth:I

    #@79
    add-int/2addr v13, v14

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget-object v14, v0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@7e
    iget v14, v14, Landroid/graphics/Rect;->right:I

    #@80
    add-int v12, v13, v14

    #@82
    .line 745
    .local v12, thumbRight:I
    move-object/from16 v0, p0

    #@84
    iget-object v13, v0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@86
    invoke-virtual {v13, v10, v9, v12, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@89
    .line 746
    move-object/from16 v0, p0

    #@8b
    iget-object v13, v0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@8d
    move-object/from16 v0, p1

    #@8f
    invoke-virtual {v13, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@92
    .line 749
    move-object/from16 v0, p0

    #@94
    iget-object v13, v0, Landroid/widget/Switch;->mTextColors:Landroid/content/res/ColorStateList;

    #@96
    if-eqz v13, :cond_b5

    #@98
    .line 750
    move-object/from16 v0, p0

    #@9a
    iget-object v13, v0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v14, v0, Landroid/widget/Switch;->mTextColors:Landroid/content/res/ColorStateList;

    #@a0
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Switch;->getDrawableState()[I

    #@a3
    move-result-object v15

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Landroid/widget/Switch;->mTextColors:Landroid/content/res/ColorStateList;

    #@a8
    move-object/from16 v16, v0

    #@aa
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    #@ad
    move-result v16

    #@ae
    invoke-virtual/range {v14 .. v16}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@b1
    move-result v14

    #@b2
    invoke-virtual {v13, v14}, Landroid/text/TextPaint;->setColor(I)V

    #@b5
    .line 753
    :cond_b5
    move-object/from16 v0, p0

    #@b7
    iget-object v13, v0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@b9
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Switch;->getDrawableState()[I

    #@bc
    move-result-object v14

    #@bd
    iput-object v14, v13, Landroid/text/TextPaint;->drawableState:[I

    #@bf
    .line 755
    invoke-direct/range {p0 .. p0}, Landroid/widget/Switch;->getTargetCheckedState()Z

    #@c2
    move-result v13

    #@c3
    if-eqz v13, :cond_f1

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget-object v8, v0, Landroid/widget/Switch;->mOnLayout:Landroid/text/Layout;

    #@c9
    .line 756
    .local v8, switchText:Landroid/text/Layout;
    :goto_c9
    if-eqz v8, :cond_ed

    #@cb
    .line 757
    add-int v13, v10, v12

    #@cd
    div-int/lit8 v13, v13, 0x2

    #@cf
    invoke-virtual {v8}, Landroid/text/Layout;->getWidth()I

    #@d2
    move-result v14

    #@d3
    div-int/lit8 v14, v14, 0x2

    #@d5
    sub-int/2addr v13, v14

    #@d6
    int-to-float v13, v13

    #@d7
    add-int v14, v5, v2

    #@d9
    div-int/lit8 v14, v14, 0x2

    #@db
    invoke-virtual {v8}, Landroid/text/Layout;->getHeight()I

    #@de
    move-result v15

    #@df
    div-int/lit8 v15, v15, 0x2

    #@e1
    sub-int/2addr v14, v15

    #@e2
    int-to-float v14, v14

    #@e3
    move-object/from16 v0, p1

    #@e5
    invoke-virtual {v0, v13, v14}, Landroid/graphics/Canvas;->translate(FF)V

    #@e8
    .line 759
    move-object/from16 v0, p1

    #@ea
    invoke-virtual {v8, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    #@ed
    .line 762
    :cond_ed
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@f0
    .line 763
    return-void

    #@f1
    .line 755
    .end local v8           #switchText:Landroid/text/Layout;
    :cond_f1
    move-object/from16 v0, p0

    #@f3
    iget-object v8, v0, Landroid/widget/Switch;->mOffLayout:Landroid/text/Layout;

    #@f5
    goto :goto_c9
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 834
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 835
    const-class v0, Landroid/widget/Switch;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 836
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 7
    .parameter "info"

    #@0
    .prologue
    .line 840
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 841
    const-class v3, Landroid/widget/Switch;

    #@5
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 842
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_28

    #@12
    iget-object v2, p0, Landroid/widget/Switch;->mTextOn:Ljava/lang/CharSequence;

    #@14
    .line 843
    .local v2, switchText:Ljava/lang/CharSequence;
    :goto_14
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_27

    #@1a
    .line 844
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    #@1d
    move-result-object v1

    #@1e
    .line 845
    .local v1, oldText:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_2b

    #@24
    .line 846
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    #@27
    .line 853
    .end local v1           #oldText:Ljava/lang/CharSequence;
    :cond_27
    :goto_27
    return-void

    #@28
    .line 842
    .end local v2           #switchText:Ljava/lang/CharSequence;
    :cond_28
    iget-object v2, p0, Landroid/widget/Switch;->mTextOff:Ljava/lang/CharSequence;

    #@2a
    goto :goto_14

    #@2b
    .line 848
    .restart local v1       #oldText:Ljava/lang/CharSequence;
    .restart local v2       #switchText:Ljava/lang/CharSequence;
    :cond_2b
    new-instance v0, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    .line 849
    .local v0, newText:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const/16 v4, 0x20

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@3d
    .line 850
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    #@40
    goto :goto_27
.end method

.method protected onLayout(ZIIII)V
    .registers 12
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 676
    invoke-super/range {p0 .. p5}, Landroid/widget/CompoundButton;->onLayout(ZIIII)V

    #@3
    .line 678
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@6
    move-result v4

    #@7
    invoke-direct {p0, v4}, Landroid/widget/Switch;->setThumbPosition(Z)V

    #@a
    .line 683
    invoke-virtual {p0}, Landroid/widget/Switch;->isLayoutRtl()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_34

    #@10
    .line 684
    invoke-virtual {p0}, Landroid/widget/Switch;->getPaddingLeft()I

    #@13
    move-result v1

    #@14
    .line 685
    .local v1, switchLeft:I
    iget v4, p0, Landroid/widget/Switch;->mSwitchWidth:I

    #@16
    add-int v2, v1, v4

    #@18
    .line 691
    .local v2, switchRight:I
    :goto_18
    const/4 v3, 0x0

    #@19
    .line 692
    .local v3, switchTop:I
    const/4 v0, 0x0

    #@1a
    .line 693
    .local v0, switchBottom:I
    invoke-virtual {p0}, Landroid/widget/Switch;->getGravity()I

    #@1d
    move-result v4

    #@1e
    and-int/lit8 v4, v4, 0x70

    #@20
    sparse-switch v4, :sswitch_data_6e

    #@23
    .line 696
    invoke-virtual {p0}, Landroid/widget/Switch;->getPaddingTop()I

    #@26
    move-result v3

    #@27
    .line 697
    iget v4, p0, Landroid/widget/Switch;->mSwitchHeight:I

    #@29
    add-int v0, v3, v4

    #@2b
    .line 712
    :goto_2b
    iput v1, p0, Landroid/widget/Switch;->mSwitchLeft:I

    #@2d
    .line 713
    iput v3, p0, Landroid/widget/Switch;->mSwitchTop:I

    #@2f
    .line 714
    iput v0, p0, Landroid/widget/Switch;->mSwitchBottom:I

    #@31
    .line 715
    iput v2, p0, Landroid/widget/Switch;->mSwitchRight:I

    #@33
    .line 716
    return-void

    #@34
    .line 687
    .end local v0           #switchBottom:I
    .end local v1           #switchLeft:I
    .end local v2           #switchRight:I
    .end local v3           #switchTop:I
    :cond_34
    invoke-virtual {p0}, Landroid/widget/Switch;->getWidth()I

    #@37
    move-result v4

    #@38
    invoke-virtual {p0}, Landroid/widget/Switch;->getPaddingRight()I

    #@3b
    move-result v5

    #@3c
    sub-int v2, v4, v5

    #@3e
    .line 688
    .restart local v2       #switchRight:I
    iget v4, p0, Landroid/widget/Switch;->mSwitchWidth:I

    #@40
    sub-int v1, v2, v4

    #@42
    .restart local v1       #switchLeft:I
    goto :goto_18

    #@43
    .line 701
    .restart local v0       #switchBottom:I
    .restart local v3       #switchTop:I
    :sswitch_43
    invoke-virtual {p0}, Landroid/widget/Switch;->getPaddingTop()I

    #@46
    move-result v4

    #@47
    invoke-virtual {p0}, Landroid/widget/Switch;->getHeight()I

    #@4a
    move-result v5

    #@4b
    add-int/2addr v4, v5

    #@4c
    invoke-virtual {p0}, Landroid/widget/Switch;->getPaddingBottom()I

    #@4f
    move-result v5

    #@50
    sub-int/2addr v4, v5

    #@51
    div-int/lit8 v4, v4, 0x2

    #@53
    iget v5, p0, Landroid/widget/Switch;->mSwitchHeight:I

    #@55
    div-int/lit8 v5, v5, 0x2

    #@57
    sub-int v3, v4, v5

    #@59
    .line 703
    iget v4, p0, Landroid/widget/Switch;->mSwitchHeight:I

    #@5b
    add-int v0, v3, v4

    #@5d
    .line 704
    goto :goto_2b

    #@5e
    .line 707
    :sswitch_5e
    invoke-virtual {p0}, Landroid/widget/Switch;->getHeight()I

    #@61
    move-result v4

    #@62
    invoke-virtual {p0}, Landroid/widget/Switch;->getPaddingBottom()I

    #@65
    move-result v5

    #@66
    sub-int v0, v4, v5

    #@68
    .line 708
    iget v4, p0, Landroid/widget/Switch;->mSwitchHeight:I

    #@6a
    sub-int v3, v0, v4

    #@6c
    goto :goto_2b

    #@6d
    .line 693
    nop

    #@6e
    :sswitch_data_6e
    .sparse-switch
        0x10 -> :sswitch_43
        0x50 -> :sswitch_5e
    .end sparse-switch
.end method

.method public onMeasure(II)V
    .registers 10
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 486
    iget-object v4, p0, Landroid/widget/Switch;->mOnLayout:Landroid/text/Layout;

    #@2
    if-nez v4, :cond_c

    #@4
    .line 487
    iget-object v4, p0, Landroid/widget/Switch;->mTextOn:Ljava/lang/CharSequence;

    #@6
    invoke-direct {p0, v4}, Landroid/widget/Switch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    #@9
    move-result-object v4

    #@a
    iput-object v4, p0, Landroid/widget/Switch;->mOnLayout:Landroid/text/Layout;

    #@c
    .line 489
    :cond_c
    iget-object v4, p0, Landroid/widget/Switch;->mOffLayout:Landroid/text/Layout;

    #@e
    if-nez v4, :cond_18

    #@10
    .line 490
    iget-object v4, p0, Landroid/widget/Switch;->mTextOff:Ljava/lang/CharSequence;

    #@12
    invoke-direct {p0, v4}, Landroid/widget/Switch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    #@15
    move-result-object v4

    #@16
    iput-object v4, p0, Landroid/widget/Switch;->mOffLayout:Landroid/text/Layout;

    #@18
    .line 493
    :cond_18
    iget-object v4, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@1a
    iget-object v5, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@1c
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@1f
    .line 494
    iget-object v4, p0, Landroid/widget/Switch;->mOnLayout:Landroid/text/Layout;

    #@21
    invoke-virtual {v4}, Landroid/text/Layout;->getWidth()I

    #@24
    move-result v4

    #@25
    iget-object v5, p0, Landroid/widget/Switch;->mOffLayout:Landroid/text/Layout;

    #@27
    invoke-virtual {v5}, Landroid/text/Layout;->getWidth()I

    #@2a
    move-result v5

    #@2b
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@2e
    move-result v0

    #@2f
    .line 495
    .local v0, maxTextWidth:I
    iget v4, p0, Landroid/widget/Switch;->mSwitchMinWidth:I

    #@31
    mul-int/lit8 v5, v0, 0x2

    #@33
    iget v6, p0, Landroid/widget/Switch;->mThumbTextPadding:I

    #@35
    mul-int/lit8 v6, v6, 0x4

    #@37
    add-int/2addr v5, v6

    #@38
    iget-object v6, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@3a
    iget v6, v6, Landroid/graphics/Rect;->left:I

    #@3c
    add-int/2addr v5, v6

    #@3d
    iget-object v6, p0, Landroid/widget/Switch;->mTempRect:Landroid/graphics/Rect;

    #@3f
    iget v6, v6, Landroid/graphics/Rect;->right:I

    #@41
    add-int/2addr v5, v6

    #@42
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@45
    move-result v3

    #@46
    .line 497
    .local v3, switchWidth:I
    iget-object v4, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@48
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@4b
    move-result v2

    #@4c
    .line 499
    .local v2, switchHeight:I
    iget v4, p0, Landroid/widget/Switch;->mThumbTextPadding:I

    #@4e
    mul-int/lit8 v4, v4, 0x2

    #@50
    add-int/2addr v4, v0

    #@51
    iput v4, p0, Landroid/widget/Switch;->mThumbWidth:I

    #@53
    .line 501
    iput v3, p0, Landroid/widget/Switch;->mSwitchWidth:I

    #@55
    .line 502
    iput v2, p0, Landroid/widget/Switch;->mSwitchHeight:I

    #@57
    .line 504
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    #@5a
    .line 505
    invoke-virtual {p0}, Landroid/widget/Switch;->getMeasuredHeight()I

    #@5d
    move-result v1

    #@5e
    .line 506
    .local v1, measuredHeight:I
    if-ge v1, v2, :cond_67

    #@60
    .line 507
    invoke-virtual {p0}, Landroid/widget/Switch;->getMeasuredWidthAndState()I

    #@63
    move-result v4

    #@64
    invoke-virtual {p0, v4, v2}, Landroid/widget/Switch;->setMeasuredDimension(II)V

    #@67
    .line 509
    :cond_67
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 513
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 514
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_23

    #@9
    iget-object v0, p0, Landroid/widget/Switch;->mOnLayout:Landroid/text/Layout;

    #@b
    .line 515
    .local v0, layout:Landroid/text/Layout;
    :goto_b
    if-eqz v0, :cond_22

    #@d
    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    #@10
    move-result-object v1

    #@11
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_22

    #@17
    .line 516
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    #@1e
    move-result-object v2

    #@1f
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@22
    .line 518
    :cond_22
    return-void

    #@23
    .line 514
    .end local v0           #layout:Landroid/text/Layout;
    :cond_23
    iget-object v0, p0, Landroid/widget/Switch;->mOffLayout:Landroid/text/Layout;

    #@25
    goto :goto_b
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "ev"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    .line 545
    iget-object v6, p0, Landroid/widget/Switch;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@4
    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@7
    .line 546
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@a
    move-result v0

    #@b
    .line 547
    .local v0, action:I
    packed-switch v0, :pswitch_data_a2

    #@e
    .line 607
    :cond_e
    :goto_e
    :pswitch_e
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@11
    move-result v5

    #@12
    :cond_12
    :goto_12
    return v5

    #@13
    .line 549
    :pswitch_13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@16
    move-result v3

    #@17
    .line 550
    .local v3, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1a
    move-result v4

    #@1b
    .line 551
    .local v4, y:F
    invoke-virtual {p0}, Landroid/widget/Switch;->isEnabled()Z

    #@1e
    move-result v6

    #@1f
    if-eqz v6, :cond_e

    #@21
    invoke-direct {p0, v3, v4}, Landroid/widget/Switch;->hitThumb(FF)Z

    #@24
    move-result v6

    #@25
    if-eqz v6, :cond_e

    #@27
    .line 552
    iput v5, p0, Landroid/widget/Switch;->mTouchMode:I

    #@29
    .line 553
    iput v3, p0, Landroid/widget/Switch;->mTouchX:F

    #@2b
    .line 554
    iput v4, p0, Landroid/widget/Switch;->mTouchY:F

    #@2d
    goto :goto_e

    #@2e
    .line 560
    .end local v3           #x:F
    .end local v4           #y:F
    :pswitch_2e
    iget v6, p0, Landroid/widget/Switch;->mTouchMode:I

    #@30
    packed-switch v6, :pswitch_data_ae

    #@33
    goto :goto_e

    #@34
    .line 566
    :pswitch_34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@37
    move-result v3

    #@38
    .line 567
    .restart local v3       #x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@3b
    move-result v4

    #@3c
    .line 568
    .restart local v4       #y:F
    iget v6, p0, Landroid/widget/Switch;->mTouchX:F

    #@3e
    sub-float v6, v3, v6

    #@40
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@43
    move-result v6

    #@44
    iget v7, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@46
    int-to-float v7, v7

    #@47
    cmpl-float v6, v6, v7

    #@49
    if-gtz v6, :cond_5a

    #@4b
    iget v6, p0, Landroid/widget/Switch;->mTouchY:F

    #@4d
    sub-float v6, v4, v6

    #@4f
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@52
    move-result v6

    #@53
    iget v7, p0, Landroid/widget/Switch;->mTouchSlop:I

    #@55
    int-to-float v7, v7

    #@56
    cmpl-float v6, v6, v7

    #@58
    if-lez v6, :cond_e

    #@5a
    .line 570
    :cond_5a
    iput v8, p0, Landroid/widget/Switch;->mTouchMode:I

    #@5c
    .line 571
    invoke-virtual {p0}, Landroid/widget/Switch;->getParent()Landroid/view/ViewParent;

    #@5f
    move-result-object v6

    #@60
    invoke-interface {v6, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@63
    .line 572
    iput v3, p0, Landroid/widget/Switch;->mTouchX:F

    #@65
    .line 573
    iput v4, p0, Landroid/widget/Switch;->mTouchY:F

    #@67
    goto :goto_12

    #@68
    .line 580
    .end local v3           #x:F
    .end local v4           #y:F
    :pswitch_68
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@6b
    move-result v3

    #@6c
    .line 581
    .restart local v3       #x:F
    iget v6, p0, Landroid/widget/Switch;->mTouchX:F

    #@6e
    sub-float v1, v3, v6

    #@70
    .line 582
    .local v1, dx:F
    const/4 v6, 0x0

    #@71
    iget v7, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@73
    add-float/2addr v7, v1

    #@74
    invoke-direct {p0}, Landroid/widget/Switch;->getThumbScrollRange()I

    #@77
    move-result v8

    #@78
    int-to-float v8, v8

    #@79
    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    #@7c
    move-result v7

    #@7d
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    #@80
    move-result v2

    #@81
    .line 584
    .local v2, newPos:F
    iget v6, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@83
    cmpl-float v6, v2, v6

    #@85
    if-eqz v6, :cond_12

    #@87
    .line 585
    iput v2, p0, Landroid/widget/Switch;->mThumbPosition:F

    #@89
    .line 586
    iput v3, p0, Landroid/widget/Switch;->mTouchX:F

    #@8b
    .line 587
    invoke-virtual {p0}, Landroid/widget/Switch;->invalidate()V

    #@8e
    goto :goto_12

    #@8f
    .line 597
    .end local v1           #dx:F
    .end local v2           #newPos:F
    .end local v3           #x:F
    :pswitch_8f
    iget v6, p0, Landroid/widget/Switch;->mTouchMode:I

    #@91
    if-ne v6, v8, :cond_98

    #@93
    .line 598
    invoke-direct {p0, p1}, Landroid/widget/Switch;->stopDrag(Landroid/view/MotionEvent;)V

    #@96
    goto/16 :goto_12

    #@98
    .line 601
    :cond_98
    const/4 v5, 0x0

    #@99
    iput v5, p0, Landroid/widget/Switch;->mTouchMode:I

    #@9b
    .line 602
    iget-object v5, p0, Landroid/widget/Switch;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@9d
    invoke-virtual {v5}, Landroid/view/VelocityTracker;->clear()V

    #@a0
    goto/16 :goto_e

    #@a2
    .line 547
    :pswitch_data_a2
    .packed-switch 0x0
        :pswitch_13
        :pswitch_8f
        :pswitch_2e
        :pswitch_8f
    .end packed-switch

    #@ae
    .line 560
    :pswitch_data_ae
    .packed-switch 0x0
        :pswitch_e
        :pswitch_34
        :pswitch_68
    .end packed-switch
.end method

.method public playSoundEffect(I)V
    .registers 9
    .parameter "soundConstant"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 857
    invoke-virtual {p0}, Landroid/widget/Switch;->getContext()Landroid/content/Context;

    #@5
    move-result-object v5

    #@6
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v5

    #@a
    const-string/jumbo v6, "sound_effects_enabled"

    #@d
    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_36

    #@13
    move v1, v3

    #@14
    .line 860
    .local v1, mIsSettingsSoundEffectEnabled:Z
    :goto_14
    invoke-virtual {p0}, Landroid/widget/Switch;->isSoundEffectsEnabled()Z

    #@17
    move-result v2

    #@18
    .line 862
    .local v2, mIsViewSoundEffectEnabled:Z
    if-eqz v1, :cond_4a

    #@1a
    if-eqz v2, :cond_4a

    #@1c
    if-nez p1, :cond_4a

    #@1e
    .line 864
    invoke-virtual {p0, v4}, Landroid/widget/Switch;->setSoundEffectsEnabled(Z)V

    #@21
    .line 865
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    #@24
    .line 866
    invoke-virtual {p0, v3}, Landroid/widget/Switch;->setSoundEffectsEnabled(Z)V

    #@27
    .line 868
    invoke-virtual {p0}, Landroid/widget/Switch;->getContext()Landroid/content/Context;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "audio"

    #@2d
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Landroid/media/AudioManager;

    #@33
    .line 871
    .local v0, audioManager:Landroid/media/AudioManager;
    if-nez v0, :cond_38

    #@35
    .line 881
    .end local v0           #audioManager:Landroid/media/AudioManager;
    :goto_35
    return-void

    #@36
    .end local v1           #mIsSettingsSoundEffectEnabled:Z
    .end local v2           #mIsViewSoundEffectEnabled:Z
    :cond_36
    move v1, v4

    #@37
    .line 857
    goto :goto_14

    #@38
    .line 874
    .restart local v0       #audioManager:Landroid/media/AudioManager;
    .restart local v1       #mIsSettingsSoundEffectEnabled:Z
    .restart local v2       #mIsViewSoundEffectEnabled:Z
    :cond_38
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@3b
    move-result v4

    #@3c
    if-ne v4, v3, :cond_44

    #@3e
    .line 875
    const/16 v3, 0x9

    #@40
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@43
    goto :goto_35

    #@44
    .line 877
    :cond_44
    const/16 v3, 0xa

    #@46
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@49
    goto :goto_35

    #@4a
    .line 879
    .end local v0           #audioManager:Landroid/media/AudioManager;
    :cond_4a
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    #@4d
    goto :goto_35
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "checked"

    #@0
    .prologue
    .line 669
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@3
    .line 670
    invoke-virtual {p0}, Landroid/widget/Switch;->isChecked()Z

    #@6
    move-result v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/Switch;->setThumbPosition(Z)V

    #@a
    .line 671
    invoke-virtual {p0}, Landroid/widget/Switch;->invalidate()V

    #@d
    .line 672
    return-void
.end method

.method public setSwitchMinWidth(I)V
    .registers 2
    .parameter "pixels"

    #@0
    .prologue
    .line 336
    iput p1, p0, Landroid/widget/Switch;->mSwitchMinWidth:I

    #@2
    .line 337
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 338
    return-void
.end method

.method public setSwitchPadding(I)V
    .registers 2
    .parameter "pixels"

    #@0
    .prologue
    .line 312
    iput p1, p0, Landroid/widget/Switch;->mSwitchPadding:I

    #@2
    .line 313
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 314
    return-void
.end method

.method public setSwitchTextAppearance(Landroid/content/Context;I)V
    .registers 14
    .parameter "context"
    .parameter "resid"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v9, -0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 193
    sget-object v6, Lcom/android/internal/R$styleable;->TextAppearance:[I

    #@5
    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v1

    #@9
    .line 200
    .local v1, appearance:Landroid/content/res/TypedArray;
    const/4 v6, 0x3

    #@a
    invoke-virtual {v1, v6}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@d
    move-result-object v2

    #@e
    .line 202
    .local v2, colors:Landroid/content/res/ColorStateList;
    if-eqz v2, :cond_53

    #@10
    .line 203
    iput-object v2, p0, Landroid/widget/Switch;->mTextColors:Landroid/content/res/ColorStateList;

    #@12
    .line 209
    :goto_12
    invoke-virtual {v1, v8, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@15
    move-result v4

    #@16
    .line 211
    .local v4, ts:I
    if-eqz v4, :cond_2c

    #@18
    .line 212
    int-to-float v6, v4

    #@19
    iget-object v7, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@1b
    invoke-virtual {v7}, Landroid/text/TextPaint;->getTextSize()F

    #@1e
    move-result v7

    #@1f
    cmpl-float v6, v6, v7

    #@21
    if-eqz v6, :cond_2c

    #@23
    .line 213
    iget-object v6, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@25
    int-to-float v7, v4

    #@26
    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTextSize(F)V

    #@29
    .line 214
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@2c
    .line 220
    :cond_2c
    invoke-virtual {v1, v10, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2f
    move-result v5

    #@30
    .line 222
    .local v5, typefaceIndex:I
    const/4 v6, 0x2

    #@31
    invoke-virtual {v1, v6, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@34
    move-result v3

    #@35
    .line 225
    .local v3, styleIndex:I
    invoke-direct {p0, v5, v3}, Landroid/widget/Switch;->setSwitchTypefaceByIndex(II)V

    #@38
    .line 227
    const/4 v6, 0x7

    #@39
    invoke-virtual {v1, v6, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3c
    move-result v0

    #@3d
    .line 229
    .local v0, allCaps:Z
    if-eqz v0, :cond_5a

    #@3f
    .line 230
    new-instance v6, Landroid/text/method/AllCapsTransformationMethod;

    #@41
    invoke-virtual {p0}, Landroid/widget/Switch;->getContext()Landroid/content/Context;

    #@44
    move-result-object v7

    #@45
    invoke-direct {v6, v7}, Landroid/text/method/AllCapsTransformationMethod;-><init>(Landroid/content/Context;)V

    #@48
    iput-object v6, p0, Landroid/widget/Switch;->mSwitchTransformationMethod:Landroid/text/method/TransformationMethod2;

    #@4a
    .line 231
    iget-object v6, p0, Landroid/widget/Switch;->mSwitchTransformationMethod:Landroid/text/method/TransformationMethod2;

    #@4c
    invoke-interface {v6, v10}, Landroid/text/method/TransformationMethod2;->setLengthChangesAllowed(Z)V

    #@4f
    .line 236
    :goto_4f
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@52
    .line 237
    return-void

    #@53
    .line 206
    .end local v0           #allCaps:Z
    .end local v3           #styleIndex:I
    .end local v4           #ts:I
    .end local v5           #typefaceIndex:I
    :cond_53
    invoke-virtual {p0}, Landroid/widget/Switch;->getTextColors()Landroid/content/res/ColorStateList;

    #@56
    move-result-object v6

    #@57
    iput-object v6, p0, Landroid/widget/Switch;->mTextColors:Landroid/content/res/ColorStateList;

    #@59
    goto :goto_12

    #@5a
    .line 233
    .restart local v0       #allCaps:Z
    .restart local v3       #styleIndex:I
    .restart local v4       #ts:I
    .restart local v5       #typefaceIndex:I
    :cond_5a
    const/4 v6, 0x0

    #@5b
    iput-object v6, p0, Landroid/widget/Switch;->mSwitchTransformationMethod:Landroid/text/method/TransformationMethod2;

    #@5d
    goto :goto_4f
.end method

.method public setSwitchTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .parameter "tf"

    #@0
    .prologue
    .line 296
    iget-object v0, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@2
    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    #@5
    move-result-object v0

    #@6
    if-eq v0, p1, :cond_13

    #@8
    .line 297
    iget-object v0, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@a
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@d
    .line 299
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@10
    .line 300
    invoke-virtual {p0}, Landroid/widget/Switch;->invalidate()V

    #@13
    .line 302
    :cond_13
    return-void
.end method

.method public setSwitchTypeface(Landroid/graphics/Typeface;I)V
    .registers 9
    .parameter "tf"
    .parameter "style"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 265
    if-lez p2, :cond_36

    #@4
    .line 266
    if-nez p1, :cond_2d

    #@6
    .line 267
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    #@9
    move-result-object p1

    #@a
    .line 272
    :goto_a
    invoke-virtual {p0, p1}, Landroid/widget/Switch;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    #@d
    .line 274
    if-eqz p1, :cond_32

    #@f
    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    #@12
    move-result v1

    #@13
    .line 275
    .local v1, typefaceStyle:I
    :goto_13
    xor-int/lit8 v4, v1, -0x1

    #@15
    and-int v0, p2, v4

    #@17
    .line 276
    .local v0, need:I
    iget-object v4, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@19
    and-int/lit8 v5, v0, 0x1

    #@1b
    if-eqz v5, :cond_1e

    #@1d
    const/4 v2, 0x1

    #@1e
    :cond_1e
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@21
    .line 277
    iget-object v4, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@23
    and-int/lit8 v2, v0, 0x2

    #@25
    if-eqz v2, :cond_34

    #@27
    const/high16 v2, -0x4180

    #@29
    :goto_29
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@2c
    .line 283
    .end local v0           #need:I
    .end local v1           #typefaceStyle:I
    :goto_2c
    return-void

    #@2d
    .line 269
    :cond_2d
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@30
    move-result-object p1

    #@31
    goto :goto_a

    #@32
    :cond_32
    move v1, v2

    #@33
    .line 274
    goto :goto_13

    #@34
    .restart local v0       #need:I
    .restart local v1       #typefaceStyle:I
    :cond_34
    move v2, v3

    #@35
    .line 277
    goto :goto_29

    #@36
    .line 279
    .end local v0           #need:I
    .end local v1           #typefaceStyle:I
    :cond_36
    iget-object v4, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@38
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@3b
    .line 280
    iget-object v2, p0, Landroid/widget/Switch;->mTextPaint:Landroid/text/TextPaint;

    #@3d
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@40
    .line 281
    invoke-virtual {p0, p1}, Landroid/widget/Switch;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    #@43
    goto :goto_2c
.end method

.method public setTextOff(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "textOff"

    #@0
    .prologue
    .line 480
    iput-object p1, p0, Landroid/widget/Switch;->mTextOff:Ljava/lang/CharSequence;

    #@2
    .line 481
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 482
    return-void
.end method

.method public setTextOn(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "textOn"

    #@0
    .prologue
    .line 461
    iput-object p1, p0, Landroid/widget/Switch;->mTextOn:Ljava/lang/CharSequence;

    #@2
    .line 462
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 463
    return-void
.end method

.method public setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "thumb"

    #@0
    .prologue
    .line 418
    iput-object p1, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    .line 419
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 420
    return-void
.end method

.method public setThumbResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 431
    invoke-virtual {p0}, Landroid/widget/Switch;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/Switch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 432
    return-void
.end method

.method public setThumbTextPadding(I)V
    .registers 2
    .parameter "pixels"

    #@0
    .prologue
    .line 360
    iput p1, p0, Landroid/widget/Switch;->mThumbTextPadding:I

    #@2
    .line 361
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 362
    return-void
.end method

.method public setTrackDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "track"

    #@0
    .prologue
    .line 383
    iput-object p1, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    .line 384
    invoke-virtual {p0}, Landroid/widget/Switch;->requestLayout()V

    #@5
    .line 385
    return-void
.end method

.method public setTrackResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 395
    invoke-virtual {p0}, Landroid/widget/Switch;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/widget/Switch;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 396
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 822
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    iget-object v0, p0, Landroid/widget/Switch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    if-eq p1, v0, :cond_e

    #@a
    iget-object v0, p0, Landroid/widget/Switch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    if-ne p1, v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method
