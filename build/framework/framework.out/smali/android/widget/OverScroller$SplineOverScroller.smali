.class Landroid/widget/OverScroller$SplineOverScroller;
.super Ljava/lang/Object;
.source "OverScroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/OverScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SplineOverScroller"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/OverScroller$SplineOverScroller$FrameRate;
    }
.end annotation


# static fields
.field private static final ABNORMAL_PERIOD:I = 0x5

.field private static final BALLISTIC:I = 0x2

.field private static final CUBIC:I = 0x1

.field private static DECELERATION_RATE:F = 0.0f

.field private static final END_TENSION:F = 1.0f

.field private static final FLING_COMPUTE_MODE_USING_CONSTANT_TIME_DIFF:I = 0x2

.field private static final FLING_COMPUTE_MODE_USING_NOISE_SURPRESS:I = 0x1

.field private static final FLING_COMPUTE_ORIGINAL_MODE:I = 0x0

.field private static final GRAVITY:F = 2000.0f

.field private static final INFLEXION:F = 0.35f

.field private static final MAX_COMPUTE_OFFSET_PERIOD:I = 0x28

.field private static final MAX_DURATION_FOR_LIST:I = 0xa06

.field private static final MIN_DURATION_FOR_LIST:I = 0x4b0

.field private static final MULTIPLE_VELOCITY_ACTIONUP:I = 0x7

.field private static final NB_SAMPLES:I = 0x64

.field private static final P1:F = 0.175f

.field private static final P2:F = 0.35000002f

.field private static final SPLINE:I = 0x0

.field private static final SPLINE_POSITION:[F = null

.field private static final SPLINE_TIME:[F = null

.field private static final START_TENSION:F = 0.5f

.field private static flyWheelOfSpline:Z

.field private static maxFlickVelocity:I

.field private static minFlickVelocity:I


# instance fields
.field private countAbnormalScene:I

.field private flingComputeMode:I

.field private frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

.field private frameTotalTime:D

.field private lastCurrentTime:J

.field private mCurrVelocity:F

.field private mCurrentPosition:I

.field private mDeceleration:F

.field private mDuration:I

.field private mFinal:I

.field private mFinished:Z

.field private mFlingFriction:F

.field private mOver:I

.field private mPhysicalCoeff:F

.field private mSplineDistance:I

.field private mSplineDuration:I

.field private mStart:I

.field private mStartTime:J

.field private mState:I

.field private mVelocity:I

.field private timePassedDiffConstant:F


# direct methods
.method static constructor <clinit>()V
    .registers 16

    #@0
    .prologue
    .line 657
    const-wide v11, 0x3fe8f5c28f5c28f6L

    #@5
    invoke-static {v11, v12}, Ljava/lang/Math;->log(D)D

    #@8
    move-result-wide v11

    #@9
    const-wide v13, 0x3feccccccccccccdL

    #@e
    invoke-static {v13, v14}, Ljava/lang/Math;->log(D)D

    #@11
    move-result-wide v13

    #@12
    div-double/2addr v11, v13

    #@13
    double-to-float v11, v11

    #@14
    sput v11, Landroid/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    #@16
    .line 665
    const/16 v11, 0x65

    #@18
    new-array v11, v11, [F

    #@1a
    sput-object v11, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    #@1c
    .line 666
    const/16 v11, 0x65

    #@1e
    new-array v11, v11, [F

    #@20
    sput-object v11, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    #@22
    .line 729
    const/4 v7, 0x0

    #@23
    .line 730
    .local v7, x_min:F
    const/4 v10, 0x0

    #@24
    .line 731
    .local v10, y_min:F
    const/4 v3, 0x0

    #@25
    .local v3, i:I
    :goto_25
    const/16 v11, 0x64

    #@27
    if-ge v3, v11, :cond_ca

    #@29
    .line 732
    int-to-float v11, v3

    #@2a
    const/high16 v12, 0x42c8

    #@2c
    div-float v0, v11, v12

    #@2e
    .line 734
    .local v0, alpha:F
    const/high16 v6, 0x3f80

    #@30
    .line 737
    .local v6, x_max:F
    :goto_30
    sub-float v11, v6, v7

    #@32
    const/high16 v12, 0x4000

    #@34
    div-float/2addr v11, v12

    #@35
    add-float v5, v7, v11

    #@37
    .line 738
    .local v5, x:F
    const/high16 v11, 0x4040

    #@39
    mul-float/2addr v11, v5

    #@3a
    const/high16 v12, 0x3f80

    #@3c
    sub-float/2addr v12, v5

    #@3d
    mul-float v1, v11, v12

    #@3f
    .line 739
    .local v1, coef:F
    const/high16 v11, 0x3f80

    #@41
    sub-float/2addr v11, v5

    #@42
    const v12, 0x3e333333

    #@45
    mul-float/2addr v11, v12

    #@46
    const v12, 0x3eb33334

    #@49
    mul-float/2addr v12, v5

    #@4a
    add-float/2addr v11, v12

    #@4b
    mul-float/2addr v11, v1

    #@4c
    mul-float v12, v5, v5

    #@4e
    mul-float/2addr v12, v5

    #@4f
    add-float v4, v11, v12

    #@51
    .line 740
    .local v4, tx:F
    sub-float v11, v4, v0

    #@53
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    #@56
    move-result v11

    #@57
    float-to-double v11, v11

    #@58
    const-wide v13, 0x3ee4f8b588e368f1L

    #@5d
    cmpg-double v11, v11, v13

    #@5f
    if-gez v11, :cond_b8

    #@61
    .line 744
    sget-object v11, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    #@63
    const/high16 v12, 0x3f80

    #@65
    sub-float/2addr v12, v5

    #@66
    const/high16 v13, 0x3f00

    #@68
    mul-float/2addr v12, v13

    #@69
    add-float/2addr v12, v5

    #@6a
    mul-float/2addr v12, v1

    #@6b
    mul-float v13, v5, v5

    #@6d
    mul-float/2addr v13, v5

    #@6e
    add-float/2addr v12, v13

    #@6f
    aput v12, v11, v3

    #@71
    .line 746
    const/high16 v9, 0x3f80

    #@73
    .line 749
    .local v9, y_max:F
    :goto_73
    sub-float v11, v9, v10

    #@75
    const/high16 v12, 0x4000

    #@77
    div-float/2addr v11, v12

    #@78
    add-float v8, v10, v11

    #@7a
    .line 750
    .local v8, y:F
    const/high16 v11, 0x4040

    #@7c
    mul-float/2addr v11, v8

    #@7d
    const/high16 v12, 0x3f80

    #@7f
    sub-float/2addr v12, v8

    #@80
    mul-float v1, v11, v12

    #@82
    .line 751
    const/high16 v11, 0x3f80

    #@84
    sub-float/2addr v11, v8

    #@85
    const/high16 v12, 0x3f00

    #@87
    mul-float/2addr v11, v12

    #@88
    add-float/2addr v11, v8

    #@89
    mul-float/2addr v11, v1

    #@8a
    mul-float v12, v8, v8

    #@8c
    mul-float/2addr v12, v8

    #@8d
    add-float v2, v11, v12

    #@8f
    .line 752
    .local v2, dy:F
    sub-float v11, v2, v0

    #@91
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    #@94
    move-result v11

    #@95
    float-to-double v11, v11

    #@96
    const-wide v13, 0x3ee4f8b588e368f1L

    #@9b
    cmpg-double v11, v11, v13

    #@9d
    if-gez v11, :cond_c2

    #@9f
    .line 756
    sget-object v11, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    #@a1
    const/high16 v12, 0x3f80

    #@a3
    sub-float/2addr v12, v8

    #@a4
    const v13, 0x3e333333

    #@a7
    mul-float/2addr v12, v13

    #@a8
    const v13, 0x3eb33334

    #@ab
    mul-float/2addr v13, v8

    #@ac
    add-float/2addr v12, v13

    #@ad
    mul-float/2addr v12, v1

    #@ae
    mul-float v13, v8, v8

    #@b0
    mul-float/2addr v13, v8

    #@b1
    add-float/2addr v12, v13

    #@b2
    aput v12, v11, v3

    #@b4
    .line 731
    add-int/lit8 v3, v3, 0x1

    #@b6
    goto/16 :goto_25

    #@b8
    .line 741
    .end local v2           #dy:F
    .end local v8           #y:F
    .end local v9           #y_max:F
    :cond_b8
    cmpl-float v11, v4, v0

    #@ba
    if-lez v11, :cond_bf

    #@bc
    move v6, v5

    #@bd
    goto/16 :goto_30

    #@bf
    .line 742
    :cond_bf
    move v7, v5

    #@c0
    goto/16 :goto_30

    #@c2
    .line 753
    .restart local v2       #dy:F
    .restart local v8       #y:F
    .restart local v9       #y_max:F
    :cond_c2
    cmpl-float v11, v2, v0

    #@c4
    if-lez v11, :cond_c8

    #@c6
    move v9, v8

    #@c7
    goto :goto_73

    #@c8
    .line 754
    :cond_c8
    move v10, v8

    #@c9
    goto :goto_73

    #@ca
    .line 758
    .end local v0           #alpha:F
    .end local v1           #coef:F
    .end local v2           #dy:F
    .end local v4           #tx:F
    .end local v5           #x:F
    .end local v6           #x_max:F
    .end local v8           #y:F
    .end local v9           #y_max:F
    :cond_ca
    sget-object v11, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    #@cc
    const/16 v12, 0x64

    #@ce
    sget-object v13, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    #@d0
    const/16 v14, 0x64

    #@d2
    const/high16 v15, 0x3f80

    #@d4
    aput v15, v13, v14

    #@d6
    aput v15, v11, v12

    #@d8
    .line 759
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 773
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 646
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    #@7
    move-result v2

    #@8
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    #@a
    .line 649
    const/4 v2, 0x0

    #@b
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@d
    .line 774
    iput-boolean v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@f
    .line 775
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@16
    move-result-object v2

    #@17
    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    #@19
    const/high16 v3, 0x4320

    #@1b
    mul-float v1, v2, v3

    #@1d
    .line 776
    .local v1, ppi:F
    const v2, 0x43c10b3d

    #@20
    mul-float/2addr v2, v1

    #@21
    const v3, 0x3f570a3d

    #@24
    mul-float/2addr v2, v3

    #@25
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    #@27
    .line 781
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@29
    if-eqz v2, :cond_44

    #@2b
    .line 782
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@2e
    move-result-object v0

    #@2f
    .line 783
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@32
    move-result v2

    #@33
    sput v2, Landroid/widget/OverScroller$SplineOverScroller;->minFlickVelocity:I

    #@35
    .line 784
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@38
    move-result v2

    #@39
    sput v2, Landroid/widget/OverScroller$SplineOverScroller;->maxFlickVelocity:I

    #@3b
    .line 785
    sput-boolean v4, Landroid/widget/OverScroller$SplineOverScroller;->flyWheelOfSpline:Z

    #@3d
    .line 786
    new-instance v2, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@3f
    invoke-direct {v2, p0}, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;-><init>(Landroid/widget/OverScroller$SplineOverScroller;)V

    #@42
    iput-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@44
    .line 788
    .end local v0           #configuration:Landroid/view/ViewConfiguration;
    :cond_44
    return-void
.end method

.method static synthetic access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget-boolean v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/widget/OverScroller$SplineOverScroller;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 608
    iput-boolean p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/widget/OverScroller$SplineOverScroller;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrentPosition:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/widget/OverScroller$SplineOverScroller;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/widget/OverScroller$SplineOverScroller;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/widget/OverScroller$SplineOverScroller;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/widget/OverScroller$SplineOverScroller;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/widget/OverScroller$SplineOverScroller;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@2
    return-wide v0
.end method

.method static synthetic access$700(Landroid/widget/OverScroller$SplineOverScroller;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 608
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@2
    return v0
.end method

.method private adjustDuration(III)V
    .registers 16
    .parameter "start"
    .parameter "oldFinal"
    .parameter "newFinal"

    #@0
    .prologue
    const/high16 v11, 0x42c8

    #@2
    .line 806
    sub-int v2, p2, p1

    #@4
    .line 807
    .local v2, oldDistance:I
    sub-int v1, p3, p1

    #@6
    .line 808
    .local v1, newDistance:I
    int-to-float v9, v1

    #@7
    int-to-float v10, v2

    #@8
    div-float/2addr v9, v10

    #@9
    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    #@c
    move-result v6

    #@d
    .line 809
    .local v6, x:F
    mul-float v9, v11, v6

    #@f
    float-to-int v0, v9

    #@10
    .line 810
    .local v0, index:I
    const/16 v9, 0x64

    #@12
    if-ge v0, v9, :cond_37

    #@14
    .line 811
    int-to-float v9, v0

    #@15
    div-float v7, v9, v11

    #@17
    .line 812
    .local v7, x_inf:F
    add-int/lit8 v9, v0, 0x1

    #@19
    int-to-float v9, v9

    #@1a
    div-float v8, v9, v11

    #@1c
    .line 813
    .local v8, x_sup:F
    sget-object v9, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    #@1e
    aget v3, v9, v0

    #@20
    .line 814
    .local v3, t_inf:F
    sget-object v9, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    #@22
    add-int/lit8 v10, v0, 0x1

    #@24
    aget v4, v9, v10

    #@26
    .line 815
    .local v4, t_sup:F
    sub-float v9, v6, v7

    #@28
    sub-float v10, v8, v7

    #@2a
    div-float/2addr v9, v10

    #@2b
    sub-float v10, v4, v3

    #@2d
    mul-float/2addr v9, v10

    #@2e
    add-float v5, v3, v9

    #@30
    .line 816
    .local v5, timeCoef:F
    iget v9, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@32
    int-to-float v9, v9

    #@33
    mul-float/2addr v9, v5

    #@34
    float-to-int v9, v9

    #@35
    iput v9, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@37
    .line 818
    .end local v3           #t_inf:F
    .end local v4           #t_sup:F
    .end local v5           #timeCoef:F
    .end local v7           #x_inf:F
    .end local v8           #x_sup:F
    :cond_37
    return-void
.end method

.method private fitOnBounceCurve(III)V
    .registers 12
    .parameter "start"
    .parameter "end"
    .parameter "velocity"

    #@0
    .prologue
    .line 986
    neg-int v4, p3

    #@1
    int-to-float v4, v4

    #@2
    iget v5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@4
    div-float v2, v4, v5

    #@6
    .line 987
    .local v2, durationToApex:F
    mul-int v4, p3, p3

    #@8
    int-to-float v4, v4

    #@9
    const/high16 v5, 0x4000

    #@b
    div-float/2addr v4, v5

    #@c
    iget v5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@e
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    #@11
    move-result v5

    #@12
    div-float v0, v4, v5

    #@14
    .line 988
    .local v0, distanceToApex:F
    sub-int v4, p2, p1

    #@16
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    #@19
    move-result v4

    #@1a
    int-to-float v1, v4

    #@1b
    .line 989
    .local v1, distanceToEdge:F
    const-wide/high16 v4, 0x4000

    #@1d
    add-float v6, v0, v1

    #@1f
    float-to-double v6, v6

    #@20
    mul-double/2addr v4, v6

    #@21
    iget v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@23
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@26
    move-result v6

    #@27
    float-to-double v6, v6

    #@28
    div-double/2addr v4, v6

    #@29
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    #@2c
    move-result-wide v4

    #@2d
    double-to-float v3, v4

    #@2e
    .line 991
    .local v3, totalDuration:F
    iget-wide v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@30
    const/high16 v6, 0x447a

    #@32
    sub-float v7, v3, v2

    #@34
    mul-float/2addr v6, v7

    #@35
    float-to-int v6, v6

    #@36
    int-to-long v6, v6

    #@37
    sub-long/2addr v4, v6

    #@38
    iput-wide v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@3a
    .line 992
    iput p2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@3c
    .line 993
    iget v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@3e
    neg-float v4, v4

    #@3f
    mul-float/2addr v4, v3

    #@40
    float-to-int v4, v4

    #@41
    iput v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@43
    .line 994
    return-void
.end method

.method private static getDeceleration(I)F
    .registers 2
    .parameter "velocity"

    #@0
    .prologue
    .line 798
    if-lez p0, :cond_5

    #@2
    const/high16 v0, -0x3b06

    #@4
    :goto_4
    return v0

    #@5
    :cond_5
    const/high16 v0, 0x44fa

    #@7
    goto :goto_4
.end method

.method private getSplineDeceleration(I)D
    .registers 5
    .parameter "velocity"

    #@0
    .prologue
    .line 968
    const v0, 0x3eb33333

    #@3
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@6
    move-result v1

    #@7
    int-to-float v1, v1

    #@8
    mul-float/2addr v0, v1

    #@9
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    #@b
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    #@d
    mul-float/2addr v1, v2

    #@e
    div-float/2addr v0, v1

    #@f
    float-to-double v0, v0

    #@10
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    #@13
    move-result-wide v0

    #@14
    return-wide v0
.end method

.method private getSplineFlingDistance(I)D
    .registers 10
    .parameter "velocity"

    #@0
    .prologue
    .line 972
    invoke-direct {p0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineDeceleration(I)D

    #@3
    move-result-wide v2

    #@4
    .line 973
    .local v2, l:D
    sget v4, Landroid/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    #@6
    float-to-double v4, v4

    #@7
    const-wide/high16 v6, 0x3ff0

    #@9
    sub-double v0, v4, v6

    #@b
    .line 974
    .local v0, decelMinusOne:D
    iget v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    #@d
    iget v5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    #@f
    mul-float/2addr v4, v5

    #@10
    float-to-double v4, v4

    #@11
    sget v6, Landroid/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    #@13
    float-to-double v6, v6

    #@14
    div-double/2addr v6, v0

    #@15
    mul-double/2addr v6, v2

    #@16
    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    #@19
    move-result-wide v6

    #@1a
    mul-double/2addr v4, v6

    #@1b
    return-wide v4
.end method

.method private getSplineFlingDuration(I)I
    .registers 10
    .parameter "velocity"

    #@0
    .prologue
    .line 979
    invoke-direct {p0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineDeceleration(I)D

    #@3
    move-result-wide v2

    #@4
    .line 980
    .local v2, l:D
    sget v4, Landroid/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    #@6
    float-to-double v4, v4

    #@7
    const-wide/high16 v6, 0x3ff0

    #@9
    sub-double v0, v4, v6

    #@b
    .line 981
    .local v0, decelMinusOne:D
    const-wide v4, 0x408f400000000000L

    #@10
    div-double v6, v2, v0

    #@12
    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    #@15
    move-result-wide v6

    #@16
    mul-double/2addr v4, v6

    #@17
    double-to-int v4, v4

    #@18
    return v4
.end method

.method private onEdgeReached()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, 0x4000

    #@2
    .line 1038
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@4
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@6
    mul-int/2addr v2, v3

    #@7
    int-to-float v2, v2

    #@8
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@a
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    #@d
    move-result v3

    #@e
    mul-float/2addr v3, v4

    #@f
    div-float v0, v2, v3

    #@11
    .line 1039
    .local v0, distance:F
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@13
    int-to-float v2, v2

    #@14
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    #@17
    move-result v1

    #@18
    .line 1041
    .local v1, sign:F
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@1a
    int-to-float v2, v2

    #@1b
    cmpl-float v2, v0, v2

    #@1d
    if-lez v2, :cond_32

    #@1f
    .line 1043
    neg-float v2, v1

    #@20
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@22
    int-to-float v3, v3

    #@23
    mul-float/2addr v2, v3

    #@24
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@26
    int-to-float v3, v3

    #@27
    mul-float/2addr v2, v3

    #@28
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@2a
    int-to-float v3, v3

    #@2b
    mul-float/2addr v3, v4

    #@2c
    div-float/2addr v2, v3

    #@2d
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@2f
    .line 1044
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@31
    int-to-float v0, v2

    #@32
    .line 1047
    :cond_32
    float-to-int v2, v0

    #@33
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@35
    .line 1048
    const/4 v2, 0x2

    #@36
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@38
    .line 1049
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@3a
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@3c
    if-lez v3, :cond_50

    #@3e
    .end local v0           #distance:F
    :goto_3e
    float-to-int v3, v0

    #@3f
    add-int/2addr v2, v3

    #@40
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@42
    .line 1050
    const/high16 v2, 0x447a

    #@44
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@46
    int-to-float v3, v3

    #@47
    mul-float/2addr v2, v3

    #@48
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@4a
    div-float/2addr v2, v3

    #@4b
    float-to-int v2, v2

    #@4c
    neg-int v2, v2

    #@4d
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@4f
    .line 1051
    return-void

    #@50
    .line 1049
    .restart local v0       #distance:F
    :cond_50
    neg-float v0, v0

    #@51
    goto :goto_3e
.end method

.method static setFlyWheelStatus(Z)V
    .registers 2
    .parameter "flyWheel"

    #@0
    .prologue
    .line 763
    sget-boolean v0, Landroid/widget/OverScroller$SplineOverScroller;->flyWheelOfSpline:Z

    #@2
    if-eq v0, p0, :cond_6

    #@4
    .line 764
    sput-boolean p0, Landroid/widget/OverScroller$SplineOverScroller;->flyWheelOfSpline:Z

    #@6
    .line 766
    :cond_6
    return-void
.end method

.method private startAfterEdge(IIII)V
    .registers 18
    .parameter "start"
    .parameter "min"
    .parameter "max"
    .parameter "velocity"

    #@0
    .prologue
    .line 1003
    if-le p1, p2, :cond_12

    #@2
    move/from16 v0, p3

    #@4
    if-ge p1, v0, :cond_12

    #@6
    .line 1004
    const-string v1, "OverScroller"

    #@8
    const-string/jumbo v2, "startAfterEdge called from a valid position"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1005
    const/4 v1, 0x1

    #@f
    iput-boolean v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@11
    .line 1023
    :goto_11
    return-void

    #@12
    .line 1008
    :cond_12
    move/from16 v0, p3

    #@14
    if-le p1, v0, :cond_2a

    #@16
    const/4 v10, 0x1

    #@17
    .line 1009
    .local v10, positive:Z
    :goto_17
    if-eqz v10, :cond_2c

    #@19
    move/from16 v7, p3

    #@1b
    .line 1010
    .local v7, edge:I
    :goto_1b
    sub-int v9, p1, v7

    #@1d
    .line 1011
    .local v9, overDistance:I
    mul-int v1, v9, p4

    #@1f
    if-ltz v1, :cond_2e

    #@21
    const/4 v8, 0x1

    #@22
    .line 1012
    .local v8, keepIncreasing:Z
    :goto_22
    if-eqz v8, :cond_30

    #@24
    .line 1014
    move/from16 v0, p4

    #@26
    invoke-direct {p0, p1, v7, v0}, Landroid/widget/OverScroller$SplineOverScroller;->startBounceAfterEdge(III)V

    #@29
    goto :goto_11

    #@2a
    .line 1008
    .end local v7           #edge:I
    .end local v8           #keepIncreasing:Z
    .end local v9           #overDistance:I
    .end local v10           #positive:Z
    :cond_2a
    const/4 v10, 0x0

    #@2b
    goto :goto_17

    #@2c
    .restart local v10       #positive:Z
    :cond_2c
    move v7, p2

    #@2d
    .line 1009
    goto :goto_1b

    #@2e
    .line 1011
    .restart local v7       #edge:I
    .restart local v9       #overDistance:I
    :cond_2e
    const/4 v8, 0x0

    #@2f
    goto :goto_22

    #@30
    .line 1016
    .restart local v8       #keepIncreasing:Z
    :cond_30
    move/from16 v0, p4

    #@32
    invoke-direct {p0, v0}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    #@35
    move-result-wide v11

    #@36
    .line 1017
    .local v11, totalDistance:D
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@39
    move-result v1

    #@3a
    int-to-double v1, v1

    #@3b
    cmpl-double v1, v11, v1

    #@3d
    if-lez v1, :cond_54

    #@3f
    .line 1018
    if-eqz v10, :cond_4f

    #@41
    move v4, p2

    #@42
    :goto_42
    if-eqz v10, :cond_51

    #@44
    move v5, p1

    #@45
    :goto_45
    iget v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@47
    move-object v1, p0

    #@48
    move v2, p1

    #@49
    move/from16 v3, p4

    #@4b
    invoke-virtual/range {v1 .. v6}, Landroid/widget/OverScroller$SplineOverScroller;->fling(IIIII)V

    #@4e
    goto :goto_11

    #@4f
    :cond_4f
    move v4, p1

    #@50
    goto :goto_42

    #@51
    :cond_51
    move/from16 v5, p3

    #@53
    goto :goto_45

    #@54
    .line 1020
    :cond_54
    move/from16 v0, p4

    #@56
    invoke-direct {p0, p1, v7, v0}, Landroid/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    #@59
    goto :goto_11
.end method

.method private startBounceAfterEdge(III)V
    .registers 5
    .parameter "start"
    .parameter "end"
    .parameter "velocity"

    #@0
    .prologue
    .line 997
    if-nez p3, :cond_11

    #@2
    sub-int v0, p1, p2

    #@4
    :goto_4
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->getDeceleration(I)F

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@a
    .line 998
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/OverScroller$SplineOverScroller;->fitOnBounceCurve(III)V

    #@d
    .line 999
    invoke-direct {p0}, Landroid/widget/OverScroller$SplineOverScroller;->onEdgeReached()V

    #@10
    .line 1000
    return-void

    #@11
    :cond_11
    move v0, p3

    #@12
    .line 997
    goto :goto_4
.end method

.method private startSpringback(III)V
    .registers 11
    .parameter "start"
    .parameter "end"
    .parameter "velocity"

    #@0
    .prologue
    .line 874
    const/4 v1, 0x0

    #@1
    iput-boolean v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@3
    .line 875
    const/4 v1, 0x1

    #@4
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@6
    .line 876
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@8
    .line 877
    iput p2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@a
    .line 878
    sub-int v0, p1, p2

    #@c
    .line 879
    .local v0, delta:I
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->getDeceleration(I)F

    #@f
    move-result v1

    #@10
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@12
    .line 881
    neg-int v1, v0

    #@13
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@15
    .line 882
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@18
    move-result v1

    #@19
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@1b
    .line 883
    const-wide v1, 0x408f400000000000L

    #@20
    const-wide/high16 v3, -0x4000

    #@22
    int-to-double v5, v0

    #@23
    mul-double/2addr v3, v5

    #@24
    iget v5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@26
    float-to-double v5, v5

    #@27
    div-double/2addr v3, v5

    #@28
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    #@2b
    move-result-wide v3

    #@2c
    mul-double/2addr v1, v3

    #@2d
    double-to-int v1, v1

    #@2e
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@30
    .line 884
    return-void
.end method


# virtual methods
.method continueWhenFinished()Z
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1054
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@3
    packed-switch v1, :pswitch_data_3e

    #@6
    .line 1078
    :goto_6
    invoke-virtual {p0}, Landroid/widget/OverScroller$SplineOverScroller;->update()Z

    #@9
    .line 1079
    const/4 v0, 0x1

    #@a
    :cond_a
    :pswitch_a
    return v0

    #@b
    .line 1057
    :pswitch_b
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@d
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@f
    if-ge v1, v2, :cond_a

    #@11
    .line 1059
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@13
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@15
    .line 1061
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@17
    float-to-int v0, v0

    #@18
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@1a
    .line 1062
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@1c
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->getDeceleration(I)F

    #@1f
    move-result v0

    #@20
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@22
    .line 1063
    iget-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@24
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@26
    int-to-long v2, v2

    #@27
    add-long/2addr v0, v2

    #@28
    iput-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@2a
    .line 1064
    invoke-direct {p0}, Landroid/widget/OverScroller$SplineOverScroller;->onEdgeReached()V

    #@2d
    goto :goto_6

    #@2e
    .line 1071
    :pswitch_2e
    iget-wide v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@30
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@32
    int-to-long v3, v3

    #@33
    add-long/2addr v1, v3

    #@34
    iput-wide v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@36
    .line 1072
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@38
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@3a
    invoke-direct {p0, v1, v2, v0}, Landroid/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    #@3d
    goto :goto_6

    #@3e
    .line 1054
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_2e
    .end packed-switch
.end method

.method extendDuration(I)V
    .registers 7
    .parameter "extend"

    #@0
    .prologue
    .line 848
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v1

    #@4
    .line 849
    .local v1, time:J
    iget-wide v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@6
    sub-long v3, v1, v3

    #@8
    long-to-int v0, v3

    #@9
    .line 850
    .local v0, elapsedTime:I
    add-int v3, v0, p1

    #@b
    iput v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@d
    .line 851
    const/4 v3, 0x0

    #@e
    iput-boolean v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@10
    .line 852
    return-void
.end method

.method finish()V
    .registers 2

    #@0
    .prologue
    .line 835
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@2
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrentPosition:I

    #@4
    .line 839
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@7
    .line 840
    return-void
.end method

.method fling(IIIII)V
    .registers 16
    .parameter "start"
    .parameter "velocity"
    .parameter "min"
    .parameter "max"
    .parameter "over"

    #@0
    .prologue
    const/16 v5, 0xa06

    #@2
    const/16 v4, 0x4b0

    #@4
    const/4 v9, 0x2

    #@5
    const-wide/16 v7, 0x0

    #@7
    const/4 v6, 0x0

    #@8
    .line 887
    iput p5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@a
    .line 888
    iput-boolean v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@c
    .line 889
    iput p2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@e
    int-to-float v2, p2

    #@f
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@11
    .line 890
    iput v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@13
    iput v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@15
    .line 891
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@18
    move-result-wide v2

    #@19
    iput-wide v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@1b
    .line 892
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@1d
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrentPosition:I

    #@1f
    .line 894
    if-gt p1, p4, :cond_23

    #@21
    if-ge p1, p3, :cond_27

    #@23
    .line 895
    :cond_23
    invoke-direct {p0, p1, p3, p4, p2}, Landroid/widget/OverScroller$SplineOverScroller;->startAfterEdge(IIII)V

    #@26
    .line 965
    :cond_26
    :goto_26
    return-void

    #@27
    .line 899
    :cond_27
    iput v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@29
    .line 900
    const-wide/16 v0, 0x0

    #@2b
    .line 902
    .local v0, totalDistance:D
    if-eqz p2, :cond_47

    #@2d
    .line 903
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@2f
    if-eqz v2, :cond_103

    #@31
    .line 904
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@34
    move-result v2

    #@35
    sget v3, Landroid/widget/OverScroller$SplineOverScroller;->minFlickVelocity:I

    #@37
    mul-int/lit8 v3, v3, 0x7

    #@39
    if-gt v2, v3, :cond_ad

    #@3b
    .line 905
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    #@3e
    move-result v2

    #@3f
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@41
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@43
    .line 906
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    #@46
    move-result-wide v0

    #@47
    .line 932
    :cond_47
    :goto_47
    int-to-float v2, p2

    #@48
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    #@4b
    move-result v2

    #@4c
    float-to-double v2, v2

    #@4d
    mul-double/2addr v2, v0

    #@4e
    double-to-int v2, v2

    #@4f
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    #@51
    .line 933
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    #@53
    add-int/2addr v2, p1

    #@54
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@56
    .line 936
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@58
    if-ge v2, p3, :cond_63

    #@5a
    .line 937
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@5c
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@5e
    invoke-direct {p0, v2, v3, p3}, Landroid/widget/OverScroller$SplineOverScroller;->adjustDuration(III)V

    #@61
    .line 938
    iput p3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@63
    .line 941
    :cond_63
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@65
    if-le v2, p4, :cond_70

    #@67
    .line 942
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@69
    iget v3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@6b
    invoke-direct {p0, v2, v3, p4}, Landroid/widget/OverScroller$SplineOverScroller;->adjustDuration(III)V

    #@6e
    .line 943
    iput p4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@70
    .line 946
    :cond_70
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@72
    if-eqz v2, :cond_26

    #@74
    .line 947
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@76
    iget-wide v2, v2, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timePassed:J

    #@78
    cmp-long v2, v2, v7

    #@7a
    if-eqz v2, :cond_84

    #@7c
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@7e
    iget v2, v2, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@80
    const/16 v3, 0xf

    #@82
    if-ge v2, v3, :cond_115

    #@84
    .line 948
    :cond_84
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@86
    iput v6, v2, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@88
    .line 949
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@8a
    iput-wide v7, v2, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timePassed:J

    #@8c
    .line 950
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@8e
    iget v2, v2, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameTotalCount:I

    #@90
    const/16 v3, 0x32

    #@92
    if-le v2, v3, :cond_111

    #@94
    .line 951
    iput v9, p0, Landroid/widget/OverScroller$SplineOverScroller;->flingComputeMode:I

    #@96
    .line 952
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@98
    invoke-virtual {v2}, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->getTimeDiff()F

    #@9b
    move-result v2

    #@9c
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->timePassedDiffConstant:F

    #@9e
    .line 960
    :goto_9e
    const-wide/16 v2, 0x0

    #@a0
    iput-wide v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameTotalTime:D

    #@a2
    .line 961
    iput v6, p0, Landroid/widget/OverScroller$SplineOverScroller;->countAbnormalScene:I

    #@a4
    .line 962
    iput-wide v7, p0, Landroid/widget/OverScroller$SplineOverScroller;->lastCurrentTime:J

    #@a6
    .line 963
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@a8
    invoke-virtual {v2}, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->reset()V

    #@ab
    goto/16 :goto_26

    #@ad
    .line 907
    :cond_ad
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    #@b0
    move-result v2

    #@b1
    sget v3, Landroid/widget/OverScroller$SplineOverScroller;->minFlickVelocity:I

    #@b3
    mul-int/lit8 v3, v3, 0x7

    #@b5
    if-le v2, v3, :cond_47

    #@b7
    .line 909
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    #@ba
    move-result v2

    #@bb
    if-gt v2, v4, :cond_e7

    #@bd
    .line 910
    iput v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@bf
    iput v4, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@c1
    .line 911
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    #@c4
    move-result-wide v2

    #@c5
    const-wide v4, 0x4092c00000000000L

    #@ca
    mul-double/2addr v2, v4

    #@cb
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    #@ce
    move-result v4

    #@cf
    int-to-double v4, v4

    #@d0
    div-double v0, v2, v4

    #@d2
    .line 920
    :goto_d2
    sget-boolean v2, Landroid/widget/OverScroller$SplineOverScroller;->flyWheelOfSpline:Z

    #@d4
    if-nez v2, :cond_47

    #@d6
    .line 921
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@d8
    div-int/lit8 v2, v2, 0x2

    #@da
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@dc
    .line 922
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@de
    div-int/lit8 v2, v2, 0x2

    #@e0
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@e2
    .line 923
    const-wide/high16 v2, 0x4000

    #@e4
    div-double/2addr v0, v2

    #@e5
    goto/16 :goto_47

    #@e7
    .line 912
    :cond_e7
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    #@ea
    move-result v2

    #@eb
    if-le v2, v5, :cond_f6

    #@ed
    .line 913
    iput v5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@ef
    iput v5, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@f1
    .line 914
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    #@f4
    move-result-wide v0

    #@f5
    goto :goto_d2

    #@f6
    .line 916
    :cond_f6
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    #@f9
    move-result v2

    #@fa
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@fc
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@fe
    .line 917
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    #@101
    move-result-wide v0

    #@102
    goto :goto_d2

    #@103
    .line 927
    :cond_103
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    #@106
    move-result v2

    #@107
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@109
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@10b
    .line 928
    invoke-direct {p0, p2}, Landroid/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    #@10e
    move-result-wide v0

    #@10f
    goto/16 :goto_47

    #@111
    .line 954
    :cond_111
    const/4 v2, 0x1

    #@112
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->flingComputeMode:I

    #@114
    goto :goto_9e

    #@115
    .line 957
    :cond_115
    iget-object v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@117
    invoke-virtual {v2}, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->getTimeDiff()F

    #@11a
    move-result v2

    #@11b
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->timePassedDiffConstant:F

    #@11d
    .line 958
    iput v9, p0, Landroid/widget/OverScroller$SplineOverScroller;->flingComputeMode:I

    #@11f
    goto/16 :goto_9e
.end method

.method notifyEdgeReached(III)V
    .registers 6
    .parameter "start"
    .parameter "end"
    .parameter "over"

    #@0
    .prologue
    .line 1027
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@2
    if-nez v0, :cond_12

    #@4
    .line 1028
    iput p3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@6
    .line 1029
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@9
    move-result-wide v0

    #@a
    iput-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@c
    .line 1032
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@e
    float-to-int v0, v0

    #@f
    invoke-direct {p0, p1, p2, p2, v0}, Landroid/widget/OverScroller$SplineOverScroller;->startAfterEdge(IIII)V

    #@12
    .line 1034
    :cond_12
    return-void
.end method

.method setFinalPosition(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 843
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@2
    .line 844
    const/4 v0, 0x0

    #@3
    iput-boolean v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@5
    .line 845
    return-void
.end method

.method setFriction(F)V
    .registers 2
    .parameter "friction"

    #@0
    .prologue
    .line 770
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    #@2
    .line 771
    return-void
.end method

.method springback(III)Z
    .registers 8
    .parameter "start"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 855
    iput-boolean v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@4
    .line 857
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@6
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@8
    .line 858
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@a
    .line 860
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@d
    move-result-wide v2

    #@e
    iput-wide v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@10
    .line 861
    iput v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@12
    .line 863
    if-ge p1, p2, :cond_1c

    #@14
    .line 864
    invoke-direct {p0, p1, p2, v1}, Landroid/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    #@17
    .line 869
    :cond_17
    :goto_17
    iget-boolean v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@19
    if-nez v2, :cond_22

    #@1b
    :goto_1b
    return v0

    #@1c
    .line 865
    :cond_1c
    if-le p1, p3, :cond_17

    #@1e
    .line 866
    invoke-direct {p0, p1, p3, v1}, Landroid/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    #@21
    goto :goto_17

    #@22
    :cond_22
    move v0, v1

    #@23
    .line 869
    goto :goto_1b
.end method

.method startScroll(III)V
    .registers 7
    .parameter "start"
    .parameter "distance"
    .parameter "duration"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 821
    iput-boolean v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinished:Z

    #@3
    .line 823
    iput p1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@5
    .line 824
    add-int v0, p1, p2

    #@7
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@9
    .line 826
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@c
    move-result-wide v0

    #@d
    iput-wide v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@f
    .line 827
    iput p3, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@11
    .line 830
    const/4 v0, 0x0

    #@12
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@14
    .line 831
    iput v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@16
    .line 832
    return-void
.end method

.method update()Z
    .registers 25

    #@0
    .prologue
    .line 1088
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v17

    #@4
    .line 1092
    .local v17, time:J
    move-object/from16 v0, p0

    #@6
    iget-wide v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mStartTime:J

    #@8
    move-wide/from16 v20, v0

    #@a
    sub-long v2, v17, v20

    #@c
    .line 1093
    .local v2, currentTime:J
    move-wide v10, v2

    #@d
    .line 1095
    .local v10, origCurrentTime:J
    sget-boolean v20, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@f
    if-eqz v20, :cond_118

    #@11
    .line 1096
    move-object/from16 v0, p0

    #@13
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@15
    move/from16 v20, v0

    #@17
    if-nez v20, :cond_118

    #@19
    .line 1097
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@1d
    move-object/from16 v20, v0

    #@1f
    move-object/from16 v0, v20

    #@21
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@23
    move/from16 v21, v0

    #@25
    add-int/lit8 v21, v21, 0x1

    #@27
    move/from16 v0, v21

    #@29
    move-object/from16 v1, v20

    #@2b
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@2d
    .line 1098
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@31
    move-object/from16 v20, v0

    #@33
    move-object/from16 v0, v20

    #@35
    iput-wide v2, v0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->timePassed:J

    #@37
    .line 1100
    move-object/from16 v0, p0

    #@39
    iget-wide v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->lastCurrentTime:J

    #@3b
    move-wide/from16 v20, v0

    #@3d
    sub-long v20, v2, v20

    #@3f
    const-wide/16 v22, 0x5

    #@41
    cmp-long v20, v20, v22

    #@43
    if-gtz v20, :cond_71

    #@45
    .line 1101
    move-object/from16 v0, p0

    #@47
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->countAbnormalScene:I

    #@49
    move/from16 v20, v0

    #@4b
    add-int/lit8 v20, v20, 0x1

    #@4d
    move/from16 v0, v20

    #@4f
    move-object/from16 v1, p0

    #@51
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller;->countAbnormalScene:I

    #@53
    .line 1102
    move-object/from16 v0, p0

    #@55
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->countAbnormalScene:I

    #@57
    move/from16 v20, v0

    #@59
    const/16 v21, 0x8

    #@5b
    move/from16 v0, v20

    #@5d
    move/from16 v1, v21

    #@5f
    if-ne v0, v1, :cond_79

    #@61
    sget-boolean v20, Landroid/widget/OverScroller$SplineOverScroller;->flyWheelOfSpline:Z

    #@63
    if-eqz v20, :cond_79

    #@65
    .line 1103
    move-object/from16 v0, p0

    #@67
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@69
    move-object/from16 v20, v0

    #@6b
    invoke-virtual/range {v20 .. v20}, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->reset()V

    #@6e
    .line 1104
    const/16 v20, 0x0

    #@70
    .line 1166
    :goto_70
    return v20

    #@71
    .line 1107
    :cond_71
    const/16 v20, 0x0

    #@73
    move/from16 v0, v20

    #@75
    move-object/from16 v1, p0

    #@77
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller;->countAbnormalScene:I

    #@79
    .line 1110
    :cond_79
    move-object/from16 v0, p0

    #@7b
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->flingComputeMode:I

    #@7d
    move/from16 v20, v0

    #@7f
    const/16 v21, 0x2

    #@81
    move/from16 v0, v20

    #@83
    move/from16 v1, v21

    #@85
    if-ne v0, v1, :cond_d7

    #@87
    move-object/from16 v0, p0

    #@89
    iget-wide v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->lastCurrentTime:J

    #@8b
    move-wide/from16 v20, v0

    #@8d
    sub-long v20, v2, v20

    #@8f
    const-wide/16 v22, 0x28

    #@91
    cmp-long v20, v20, v22

    #@93
    if-lez v20, :cond_d7

    #@95
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@99
    move-object/from16 v20, v0

    #@9b
    move-object/from16 v0, v20

    #@9d
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@9f
    move/from16 v20, v0

    #@a1
    const/16 v21, 0x1

    #@a3
    move/from16 v0, v20

    #@a5
    move/from16 v1, v21

    #@a7
    if-le v0, v1, :cond_d7

    #@a9
    .line 1112
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@ad
    move-object/from16 v20, v0

    #@af
    move-object/from16 v0, v20

    #@b1
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->extraTime:I

    #@b3
    move/from16 v21, v0

    #@b5
    move-object/from16 v0, p0

    #@b7
    iget-wide v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->lastCurrentTime:J

    #@b9
    move-wide/from16 v22, v0

    #@bb
    sub-long v22, v2, v22

    #@bd
    move-wide/from16 v0, v22

    #@bf
    long-to-float v0, v0

    #@c0
    move/from16 v22, v0

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->timePassedDiffConstant:F

    #@c6
    move/from16 v23, v0

    #@c8
    sub-float v22, v22, v23

    #@ca
    move/from16 v0, v22

    #@cc
    float-to-int v0, v0

    #@cd
    move/from16 v22, v0

    #@cf
    add-int v21, v21, v22

    #@d1
    move/from16 v0, v21

    #@d3
    move-object/from16 v1, v20

    #@d5
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->extraTime:I

    #@d7
    .line 1114
    :cond_d7
    move-object/from16 v0, p0

    #@d9
    iput-wide v2, v0, Landroid/widget/OverScroller$SplineOverScroller;->lastCurrentTime:J

    #@db
    .line 1116
    move-object/from16 v0, p0

    #@dd
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->flingComputeMode:I

    #@df
    move/from16 v20, v0

    #@e1
    const/16 v21, 0x2

    #@e3
    move/from16 v0, v20

    #@e5
    move/from16 v1, v21

    #@e7
    if-ne v0, v1, :cond_118

    #@e9
    .line 1117
    move-object/from16 v0, p0

    #@eb
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->timePassedDiffConstant:F

    #@ed
    move/from16 v20, v0

    #@ef
    move-object/from16 v0, p0

    #@f1
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@f3
    move-object/from16 v21, v0

    #@f5
    move-object/from16 v0, v21

    #@f7
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->frameCount:I

    #@f9
    move/from16 v21, v0

    #@fb
    move/from16 v0, v21

    #@fd
    int-to-float v0, v0

    #@fe
    move/from16 v21, v0

    #@100
    mul-float v20, v20, v21

    #@102
    move/from16 v0, v20

    #@104
    float-to-int v0, v0

    #@105
    move/from16 v20, v0

    #@107
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->frameRate:Landroid/widget/OverScroller$SplineOverScroller$FrameRate;

    #@10b
    move-object/from16 v21, v0

    #@10d
    move-object/from16 v0, v21

    #@10f
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller$FrameRate;->extraTime:I

    #@111
    move/from16 v21, v0

    #@113
    add-int v20, v20, v21

    #@115
    move/from16 v0, v20

    #@117
    int-to-long v2, v0

    #@118
    .line 1122
    :cond_118
    move-object/from16 v0, p0

    #@11a
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@11c
    move/from16 v20, v0

    #@11e
    move/from16 v0, v20

    #@120
    int-to-long v0, v0

    #@121
    move-wide/from16 v20, v0

    #@123
    cmp-long v20, v2, v20

    #@125
    if-lez v20, :cond_12b

    #@127
    .line 1123
    const/16 v20, 0x0

    #@129
    goto/16 :goto_70

    #@12b
    .line 1126
    :cond_12b
    const-wide/16 v6, 0x0

    #@12d
    .line 1127
    .local v6, distance:D
    move-object/from16 v0, p0

    #@12f
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mState:I

    #@131
    move/from16 v20, v0

    #@133
    packed-switch v20, :pswitch_data_274

    #@136
    .line 1164
    :goto_136
    move-object/from16 v0, p0

    #@138
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@13a
    move/from16 v20, v0

    #@13c
    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    #@13f
    move-result-wide v21

    #@140
    move-wide/from16 v0, v21

    #@142
    long-to-int v0, v0

    #@143
    move/from16 v21, v0

    #@145
    add-int v20, v20, v21

    #@147
    move/from16 v0, v20

    #@149
    move-object/from16 v1, p0

    #@14b
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller;->mCurrentPosition:I

    #@14d
    .line 1166
    const/16 v20, 0x1

    #@14f
    goto/16 :goto_70

    #@151
    .line 1129
    :pswitch_151
    long-to-float v0, v2

    #@152
    move/from16 v20, v0

    #@154
    move-object/from16 v0, p0

    #@156
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@158
    move/from16 v21, v0

    #@15a
    move/from16 v0, v21

    #@15c
    int-to-float v0, v0

    #@15d
    move/from16 v21, v0

    #@15f
    div-float v13, v20, v21

    #@161
    .line 1130
    .local v13, t:F
    const/high16 v20, 0x42c8

    #@163
    mul-float v20, v20, v13

    #@165
    move/from16 v0, v20

    #@167
    float-to-int v9, v0

    #@168
    .line 1131
    .local v9, index:I
    const/high16 v8, 0x3f80

    #@16a
    .line 1132
    .local v8, distanceCoef:F
    const/16 v19, 0x0

    #@16c
    .line 1133
    .local v19, velocityCoef:F
    const/16 v20, 0x64

    #@16e
    move/from16 v0, v20

    #@170
    if-ge v9, v0, :cond_19a

    #@172
    .line 1134
    int-to-float v0, v9

    #@173
    move/from16 v20, v0

    #@175
    const/high16 v21, 0x42c8

    #@177
    div-float v15, v20, v21

    #@179
    .line 1135
    .local v15, t_inf:F
    add-int/lit8 v20, v9, 0x1

    #@17b
    move/from16 v0, v20

    #@17d
    int-to-float v0, v0

    #@17e
    move/from16 v20, v0

    #@180
    const/high16 v21, 0x42c8

    #@182
    div-float v16, v20, v21

    #@184
    .line 1136
    .local v16, t_sup:F
    sget-object v20, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    #@186
    aget v4, v20, v9

    #@188
    .line 1137
    .local v4, d_inf:F
    sget-object v20, Landroid/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    #@18a
    add-int/lit8 v21, v9, 0x1

    #@18c
    aget v5, v20, v21

    #@18e
    .line 1138
    .local v5, d_sup:F
    sub-float v20, v5, v4

    #@190
    sub-float v21, v16, v15

    #@192
    div-float v19, v20, v21

    #@194
    .line 1139
    sub-float v20, v13, v15

    #@196
    mul-float v20, v20, v19

    #@198
    add-float v8, v4, v20

    #@19a
    .line 1142
    .end local v4           #d_inf:F
    .end local v5           #d_sup:F
    .end local v15           #t_inf:F
    .end local v16           #t_sup:F
    :cond_19a
    move-object/from16 v0, p0

    #@19c
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    #@19e
    move/from16 v20, v0

    #@1a0
    move/from16 v0, v20

    #@1a2
    int-to-float v0, v0

    #@1a3
    move/from16 v20, v0

    #@1a5
    mul-float v20, v20, v8

    #@1a7
    move/from16 v0, v20

    #@1a9
    float-to-double v6, v0

    #@1aa
    .line 1143
    move-object/from16 v0, p0

    #@1ac
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    #@1ae
    move/from16 v20, v0

    #@1b0
    move/from16 v0, v20

    #@1b2
    int-to-float v0, v0

    #@1b3
    move/from16 v20, v0

    #@1b5
    mul-float v20, v20, v19

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    #@1bb
    move/from16 v21, v0

    #@1bd
    move/from16 v0, v21

    #@1bf
    int-to-float v0, v0

    #@1c0
    move/from16 v21, v0

    #@1c2
    div-float v20, v20, v21

    #@1c4
    const/high16 v21, 0x447a

    #@1c6
    mul-float v20, v20, v21

    #@1c8
    move/from16 v0, v20

    #@1ca
    move-object/from16 v1, p0

    #@1cc
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@1ce
    goto/16 :goto_136

    #@1d0
    .line 1148
    .end local v8           #distanceCoef:F
    .end local v9           #index:I
    .end local v13           #t:F
    .end local v19           #velocityCoef:F
    :pswitch_1d0
    long-to-float v0, v2

    #@1d1
    move/from16 v20, v0

    #@1d3
    const/high16 v21, 0x447a

    #@1d5
    div-float v13, v20, v21

    #@1d7
    .line 1149
    .restart local v13       #t:F
    move-object/from16 v0, p0

    #@1d9
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@1db
    move/from16 v20, v0

    #@1dd
    move/from16 v0, v20

    #@1df
    int-to-float v0, v0

    #@1e0
    move/from16 v20, v0

    #@1e2
    move-object/from16 v0, p0

    #@1e4
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@1e6
    move/from16 v21, v0

    #@1e8
    mul-float v21, v21, v13

    #@1ea
    add-float v20, v20, v21

    #@1ec
    move/from16 v0, v20

    #@1ee
    move-object/from16 v1, p0

    #@1f0
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@1f2
    .line 1150
    move-object/from16 v0, p0

    #@1f4
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@1f6
    move/from16 v20, v0

    #@1f8
    move/from16 v0, v20

    #@1fa
    int-to-float v0, v0

    #@1fb
    move/from16 v20, v0

    #@1fd
    mul-float v20, v20, v13

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    #@203
    move/from16 v21, v0

    #@205
    mul-float v21, v21, v13

    #@207
    mul-float v21, v21, v13

    #@209
    const/high16 v22, 0x4000

    #@20b
    div-float v21, v21, v22

    #@20d
    add-float v20, v20, v21

    #@20f
    move/from16 v0, v20

    #@211
    float-to-double v6, v0

    #@212
    .line 1151
    goto/16 :goto_136

    #@214
    .line 1155
    .end local v13           #t:F
    :pswitch_214
    long-to-float v0, v2

    #@215
    move/from16 v20, v0

    #@217
    move-object/from16 v0, p0

    #@219
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mDuration:I

    #@21b
    move/from16 v21, v0

    #@21d
    move/from16 v0, v21

    #@21f
    int-to-float v0, v0

    #@220
    move/from16 v21, v0

    #@222
    div-float v13, v20, v21

    #@224
    .line 1156
    .restart local v13       #t:F
    mul-float v14, v13, v13

    #@226
    .line 1157
    .local v14, t2:F
    move-object/from16 v0, p0

    #@228
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mVelocity:I

    #@22a
    move/from16 v20, v0

    #@22c
    move/from16 v0, v20

    #@22e
    int-to-float v0, v0

    #@22f
    move/from16 v20, v0

    #@231
    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    #@234
    move-result v12

    #@235
    .line 1158
    .local v12, sign:F
    move-object/from16 v0, p0

    #@237
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@239
    move/from16 v20, v0

    #@23b
    move/from16 v0, v20

    #@23d
    int-to-float v0, v0

    #@23e
    move/from16 v20, v0

    #@240
    mul-float v20, v20, v12

    #@242
    const/high16 v21, 0x4040

    #@244
    mul-float v21, v21, v14

    #@246
    const/high16 v22, 0x4000

    #@248
    mul-float v22, v22, v13

    #@24a
    mul-float v22, v22, v14

    #@24c
    sub-float v21, v21, v22

    #@24e
    mul-float v20, v20, v21

    #@250
    move/from16 v0, v20

    #@252
    float-to-double v6, v0

    #@253
    .line 1159
    move-object/from16 v0, p0

    #@255
    iget v0, v0, Landroid/widget/OverScroller$SplineOverScroller;->mOver:I

    #@257
    move/from16 v20, v0

    #@259
    move/from16 v0, v20

    #@25b
    int-to-float v0, v0

    #@25c
    move/from16 v20, v0

    #@25e
    mul-float v20, v20, v12

    #@260
    const/high16 v21, 0x40c0

    #@262
    mul-float v20, v20, v21

    #@264
    neg-float v0, v13

    #@265
    move/from16 v21, v0

    #@267
    add-float v21, v21, v14

    #@269
    mul-float v20, v20, v21

    #@26b
    move/from16 v0, v20

    #@26d
    move-object/from16 v1, p0

    #@26f
    iput v0, v1, Landroid/widget/OverScroller$SplineOverScroller;->mCurrVelocity:F

    #@271
    goto/16 :goto_136

    #@273
    .line 1127
    nop

    #@274
    :pswitch_data_274
    .packed-switch 0x0
        :pswitch_151
        :pswitch_214
        :pswitch_1d0
    .end packed-switch
.end method

.method updateScroll(F)V
    .registers 5
    .parameter "q"

    #@0
    .prologue
    .line 791
    iget v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@2
    iget v1, p0, Landroid/widget/OverScroller$SplineOverScroller;->mFinal:I

    #@4
    iget v2, p0, Landroid/widget/OverScroller$SplineOverScroller;->mStart:I

    #@6
    sub-int/2addr v1, v2

    #@7
    int-to-float v1, v1

    #@8
    mul-float/2addr v1, p1

    #@9
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@c
    move-result v1

    #@d
    add-int/2addr v0, v1

    #@e
    iput v0, p0, Landroid/widget/OverScroller$SplineOverScroller;->mCurrentPosition:I

    #@10
    .line 792
    return-void
.end method
