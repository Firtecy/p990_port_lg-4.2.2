.class public Landroid/widget/TableRow$LayoutParams;
.super Landroid/widget/LinearLayout$LayoutParams;
.source "TableRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/TableRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# static fields
.field private static final LOCATION:I = 0x0

.field private static final LOCATION_NEXT:I = 0x1


# instance fields
.field public column:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mOffset:[I

.field public span:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 470
    const/4 v0, -0x2

    #@2
    invoke-direct {p0, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@5
    .line 418
    const/4 v0, 0x2

    #@6
    new-array v0, v0, [I

    #@8
    iput-object v0, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@a
    .line 471
    iput v1, p0, Landroid/widget/TableRow$LayoutParams;->column:I

    #@c
    .line 472
    const/4 v0, 0x1

    #@d
    iput v0, p0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@f
    .line 473
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "column"

    #@0
    .prologue
    .line 485
    invoke-direct {p0}, Landroid/widget/TableRow$LayoutParams;-><init>()V

    #@3
    .line 486
    iput p1, p0, Landroid/widget/TableRow$LayoutParams;->column:I

    #@5
    .line 487
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 446
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@3
    .line 418
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [I

    #@6
    iput-object v0, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@8
    .line 447
    const/4 v0, -0x1

    #@9
    iput v0, p0, Landroid/widget/TableRow$LayoutParams;->column:I

    #@b
    .line 448
    const/4 v0, 0x1

    #@c
    iput v0, p0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@e
    .line 449
    return-void
.end method

.method public constructor <init>(IIF)V
    .registers 5
    .parameter "w"
    .parameter "h"
    .parameter "initWeight"

    #@0
    .prologue
    .line 459
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@3
    .line 418
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [I

    #@6
    iput-object v0, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@8
    .line 460
    const/4 v0, -0x1

    #@9
    iput v0, p0, Landroid/widget/TableRow$LayoutParams;->column:I

    #@b
    .line 461
    const/4 v0, 0x1

    #@c
    iput v0, p0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@e
    .line 462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 424
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 418
    const/4 v1, 0x2

    #@5
    new-array v1, v1, [I

    #@7
    iput-object v1, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@9
    .line 426
    sget-object v1, Lcom/android/internal/R$styleable;->TableRow_Cell:[I

    #@b
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@e
    move-result-object v0

    #@f
    .line 430
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@10
    const/4 v2, -0x1

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@14
    move-result v1

    #@15
    iput v1, p0, Landroid/widget/TableRow$LayoutParams;->column:I

    #@17
    .line 431
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1a
    move-result v1

    #@1b
    iput v1, p0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@1d
    .line 432
    iget v1, p0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@1f
    if-gt v1, v3, :cond_23

    #@21
    .line 433
    iput v3, p0, Landroid/widget/TableRow$LayoutParams;->span:I

    #@23
    .line 436
    :cond_23
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@26
    .line 437
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 493
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 418
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [I

    #@6
    iput-object v0, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@8
    .line 494
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 500
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    #@3
    .line 418
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [I

    #@6
    iput-object v0, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@8
    .line 501
    return-void
.end method

.method static synthetic access$200(Landroid/widget/TableRow$LayoutParams;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 402
    iget-object v0, p0, Landroid/widget/TableRow$LayoutParams;->mOffset:[I

    #@2
    return-object v0
.end method


# virtual methods
.method protected setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .registers 5
    .parameter "a"
    .parameter "widthAttr"
    .parameter "heightAttr"

    #@0
    .prologue
    .line 506
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1f

    #@6
    .line 507
    const-string/jumbo v0, "layout_width"

    #@9
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@f
    .line 513
    :goto_f
    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_23

    #@15
    .line 514
    const-string/jumbo v0, "layout_height"

    #@18
    invoke-virtual {p1, p3, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1e
    .line 518
    :goto_1e
    return-void

    #@1f
    .line 509
    :cond_1f
    const/4 v0, -0x1

    #@20
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@22
    goto :goto_f

    #@23
    .line 516
    :cond_23
    const/4 v0, -0x2

    #@24
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@26
    goto :goto_1e
.end method
