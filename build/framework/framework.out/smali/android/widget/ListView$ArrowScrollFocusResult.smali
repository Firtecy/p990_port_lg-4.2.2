.class Landroid/widget/ListView$ArrowScrollFocusResult;
.super Ljava/lang/Object;
.source "ListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArrowScrollFocusResult"
.end annotation


# instance fields
.field private mAmountToScroll:I

.field private mSelectedPosition:I


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2715
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ListView$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2715
    invoke-direct {p0}, Landroid/widget/ListView$ArrowScrollFocusResult;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getAmountToScroll()I
    .registers 2

    #@0
    .prologue
    .line 2732
    iget v0, p0, Landroid/widget/ListView$ArrowScrollFocusResult;->mAmountToScroll:I

    #@2
    return v0
.end method

.method public getSelectedPosition()I
    .registers 2

    #@0
    .prologue
    .line 2728
    iget v0, p0, Landroid/widget/ListView$ArrowScrollFocusResult;->mSelectedPosition:I

    #@2
    return v0
.end method

.method populate(II)V
    .registers 3
    .parameter "selectedPosition"
    .parameter "amountToScroll"

    #@0
    .prologue
    .line 2723
    iput p1, p0, Landroid/widget/ListView$ArrowScrollFocusResult;->mSelectedPosition:I

    #@2
    .line 2724
    iput p2, p0, Landroid/widget/ListView$ArrowScrollFocusResult;->mAmountToScroll:I

    #@4
    .line 2725
    return-void
.end method
