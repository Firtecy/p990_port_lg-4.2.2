.class public Landroid/widget/DigitalClock;
.super Landroid/widget/TextView;
.source "DigitalClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/DigitalClock$FormatChangeObserver;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final m12:Ljava/lang/String; = "h:mm:ss aa"

.field private static final m24:Ljava/lang/String; = "k:mm:ss"


# instance fields
.field mCalendar:Ljava/util/Calendar;

.field mFormat:Ljava/lang/String;

.field private mFormatChangeObserver:Landroid/widget/DigitalClock$FormatChangeObserver;

.field private mHandler:Landroid/os/Handler;

.field private mTicker:Ljava/lang/Runnable;

.field private mTickerStopped:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@3
    .line 50
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/DigitalClock;->mTickerStopped:Z

    #@6
    .line 56
    invoke-direct {p0}, Landroid/widget/DigitalClock;->initClock()V

    #@9
    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 50
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/widget/DigitalClock;->mTickerStopped:Z

    #@6
    .line 61
    invoke-direct {p0}, Landroid/widget/DigitalClock;->initClock()V

    #@9
    .line 62
    return-void
.end method

.method static synthetic access$000(Landroid/widget/DigitalClock;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-boolean v0, p0, Landroid/widget/DigitalClock;->mTickerStopped:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/widget/DigitalClock;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Landroid/widget/DigitalClock;->mTicker:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/DigitalClock;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Landroid/widget/DigitalClock;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/widget/DigitalClock;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Landroid/widget/DigitalClock;->setFormat()V

    #@3
    return-void
.end method

.method private get24HourMode()Z
    .registers 2

    #@0
    .prologue
    .line 109
    invoke-virtual {p0}, Landroid/widget/DigitalClock;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private initClock()V
    .registers 5

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 66
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@a
    .line 69
    :cond_a
    new-instance v0, Landroid/widget/DigitalClock$FormatChangeObserver;

    #@c
    invoke-direct {v0, p0}, Landroid/widget/DigitalClock$FormatChangeObserver;-><init>(Landroid/widget/DigitalClock;)V

    #@f
    iput-object v0, p0, Landroid/widget/DigitalClock;->mFormatChangeObserver:Landroid/widget/DigitalClock$FormatChangeObserver;

    #@11
    .line 70
    invoke-virtual {p0}, Landroid/widget/DigitalClock;->getContext()Landroid/content/Context;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18
    move-result-object v0

    #@19
    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@1b
    const/4 v2, 0x1

    #@1c
    iget-object v3, p0, Landroid/widget/DigitalClock;->mFormatChangeObserver:Landroid/widget/DigitalClock$FormatChangeObserver;

    #@1e
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@21
    .line 73
    invoke-direct {p0}, Landroid/widget/DigitalClock;->setFormat()V

    #@24
    .line 74
    return-void
.end method

.method private setFormat()V
    .registers 2

    #@0
    .prologue
    .line 113
    invoke-direct {p0}, Landroid/widget/DigitalClock;->get24HourMode()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 114
    const-string/jumbo v0, "k:mm:ss"

    #@9
    iput-object v0, p0, Landroid/widget/DigitalClock;->mFormat:Ljava/lang/String;

    #@b
    .line 118
    :goto_b
    return-void

    #@c
    .line 116
    :cond_c
    const-string v0, "h:mm:ss aa"

    #@e
    iput-object v0, p0, Landroid/widget/DigitalClock;->mFormat:Ljava/lang/String;

    #@10
    goto :goto_b
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 78
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/DigitalClock;->mTickerStopped:Z

    #@3
    .line 79
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    #@6
    .line 80
    new-instance v0, Landroid/os/Handler;

    #@8
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@b
    iput-object v0, p0, Landroid/widget/DigitalClock;->mHandler:Landroid/os/Handler;

    #@d
    .line 85
    new-instance v0, Landroid/widget/DigitalClock$1;

    #@f
    invoke-direct {v0, p0}, Landroid/widget/DigitalClock$1;-><init>(Landroid/widget/DigitalClock;)V

    #@12
    iput-object v0, p0, Landroid/widget/DigitalClock;->mTicker:Ljava/lang/Runnable;

    #@14
    .line 96
    iget-object v0, p0, Landroid/widget/DigitalClock;->mTicker:Ljava/lang/Runnable;

    #@16
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@19
    .line 97
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 101
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    #@3
    .line 102
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/widget/DigitalClock;->mTickerStopped:Z

    #@6
    .line 103
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 135
    const-class v0, Landroid/widget/DigitalClock;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 136
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 142
    const-class v0, Landroid/widget/DigitalClock;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 143
    return-void
.end method
