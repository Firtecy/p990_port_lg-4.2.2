.class public Landroid/widget/ZoomButton;
.super Landroid/widget/ImageButton;
.source "ZoomButton.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mIsInLongpress:Z

.field private final mRunnable:Ljava/lang/Runnable;

.field private mZoomSpeed:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 45
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/ZoomButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 49
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ZoomButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 32
    new-instance v0, Landroid/widget/ZoomButton$1;

    #@5
    invoke-direct {v0, p0}, Landroid/widget/ZoomButton$1;-><init>(Landroid/widget/ZoomButton;)V

    #@8
    iput-object v0, p0, Landroid/widget/ZoomButton;->mRunnable:Ljava/lang/Runnable;

    #@a
    .line 41
    const-wide/16 v0, 0x3e8

    #@c
    iput-wide v0, p0, Landroid/widget/ZoomButton;->mZoomSpeed:J

    #@e
    .line 54
    new-instance v0, Landroid/os/Handler;

    #@10
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@13
    iput-object v0, p0, Landroid/widget/ZoomButton;->mHandler:Landroid/os/Handler;

    #@15
    .line 55
    invoke-virtual {p0, p0}, Landroid/widget/ZoomButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@18
    .line 56
    return-void
.end method

.method static synthetic access$000(Landroid/widget/ZoomButton;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 29
    iget-boolean v0, p0, Landroid/widget/ZoomButton;->mIsInLongpress:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/widget/ZoomButton;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 29
    iget-wide v0, p0, Landroid/widget/ZoomButton;->mZoomSpeed:J

    #@2
    return-wide v0
.end method

.method static synthetic access$200(Landroid/widget/ZoomButton;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 29
    iget-object v0, p0, Landroid/widget/ZoomButton;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method


# virtual methods
.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 4
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/widget/ZoomButton;->clearFocus()V

    #@3
    .line 99
    invoke-super {p0, p1, p2}, Landroid/widget/ImageButton;->dispatchUnhandledMove(Landroid/view/View;I)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 105
    const-class v0, Landroid/widget/ZoomButton;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 106
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 110
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 111
    const-class v0, Landroid/widget/ZoomButton;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 112
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/widget/ZoomButton;->mIsInLongpress:Z

    #@3
    .line 80
    invoke-super {p0, p1, p2}, Landroid/widget/ImageButton;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 5
    .parameter "v"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 72
    iput-boolean v2, p0, Landroid/widget/ZoomButton;->mIsInLongpress:Z

    #@3
    .line 73
    iget-object v0, p0, Landroid/widget/ZoomButton;->mHandler:Landroid/os/Handler;

    #@5
    iget-object v1, p0, Landroid/widget/ZoomButton;->mRunnable:Ljava/lang/Runnable;

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 74
    return v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 60
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x3

    #@5
    if-eq v0, v1, :cond_e

    #@7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@a
    move-result v0

    #@b
    const/4 v1, 0x1

    #@c
    if-ne v0, v1, :cond_11

    #@e
    .line 62
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/widget/ZoomButton;->mIsInLongpress:Z

    #@11
    .line 64
    :cond_11
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@14
    move-result v0

    #@15
    return v0
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 85
    if-nez p1, :cond_6

    #@2
    .line 91
    const/4 v0, 0x0

    #@3
    invoke-virtual {p0, v0}, Landroid/widget/ZoomButton;->setPressed(Z)V

    #@6
    .line 93
    :cond_6
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    #@9
    .line 94
    return-void
.end method

.method public setZoomSpeed(J)V
    .registers 3
    .parameter "speed"

    #@0
    .prologue
    .line 68
    iput-wide p1, p0, Landroid/widget/ZoomButton;->mZoomSpeed:J

    #@2
    .line 69
    return-void
.end method
