.class public Landroid/widget/FrameLayout;
.super Landroid/view/ViewGroup;
.source "FrameLayout.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/FrameLayout$LayoutParams;
    }
.end annotation


# static fields
.field private static final DEFAULT_CHILD_GRAVITY:I = 0x800033


# instance fields
.field private mForeground:Landroid/graphics/drawable/Drawable;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation
.end field

.field mForegroundBoundsChanged:Z

.field private mForegroundGravity:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation
.end field

.field protected mForegroundInPadding:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation
.end field

.field private mForegroundPaddingBottom:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field private mForegroundPaddingLeft:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field private mForegroundPaddingRight:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field private mForegroundPaddingTop:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field private final mMatchParentChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mMeasureAllChildren:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private final mOverlayBounds:Landroid/graphics/Rect;

.field private final mSelfBounds:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 92
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@5
    .line 59
    iput-boolean v1, p0, Landroid/widget/FrameLayout;->mMeasureAllChildren:Z

    #@7
    .line 65
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@9
    .line 68
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@b
    .line 71
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@d
    .line 74
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@f
    .line 77
    new-instance v0, Landroid/graphics/Rect;

    #@11
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@14
    iput-object v0, p0, Landroid/widget/FrameLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@16
    .line 78
    new-instance v0, Landroid/graphics/Rect;

    #@18
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/widget/FrameLayout;->mOverlayBounds:Landroid/graphics/Rect;

    #@1d
    .line 80
    const/16 v0, 0x77

    #@1f
    iput v0, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@21
    .line 84
    iput-boolean v2, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@23
    .line 87
    iput-boolean v1, p0, Landroid/widget/FrameLayout;->mForegroundBoundsChanged:Z

    #@25
    .line 89
    new-instance v0, Ljava/util/ArrayList;

    #@27
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@2a
    iput-object v0, p0, Landroid/widget/FrameLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    #@2c
    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 96
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 100
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 59
    iput-boolean v4, p0, Landroid/widget/FrameLayout;->mMeasureAllChildren:Z

    #@7
    .line 65
    iput v4, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@9
    .line 68
    iput v4, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@b
    .line 71
    iput v4, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@d
    .line 74
    iput v4, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@f
    .line 77
    new-instance v2, Landroid/graphics/Rect;

    #@11
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@14
    iput-object v2, p0, Landroid/widget/FrameLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@16
    .line 78
    new-instance v2, Landroid/graphics/Rect;

    #@18
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@1b
    iput-object v2, p0, Landroid/widget/FrameLayout;->mOverlayBounds:Landroid/graphics/Rect;

    #@1d
    .line 80
    const/16 v2, 0x77

    #@1f
    iput v2, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@21
    .line 84
    iput-boolean v5, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@23
    .line 87
    iput-boolean v4, p0, Landroid/widget/FrameLayout;->mForegroundBoundsChanged:Z

    #@25
    .line 89
    new-instance v2, Ljava/util/ArrayList;

    #@27
    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    #@2a
    iput-object v2, p0, Landroid/widget/FrameLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    #@2c
    .line 102
    sget-object v2, Lcom/android/internal/R$styleable;->FrameLayout:[I

    #@2e
    invoke-virtual {p1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@31
    move-result-object v0

    #@32
    .line 105
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x2

    #@33
    iget v3, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@35
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@38
    move-result v2

    #@39
    iput v2, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@3b
    .line 108
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v1

    #@3f
    .line 109
    .local v1, d:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_44

    #@41
    .line 110
    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    #@44
    .line 113
    :cond_44
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@47
    move-result v2

    #@48
    if-eqz v2, :cond_4d

    #@4a
    .line 114
    invoke-virtual {p0, v5}, Landroid/widget/FrameLayout;->setMeasureAllChildren(Z)V

    #@4d
    .line 117
    :cond_4d
    const/4 v2, 0x3

    #@4e
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@51
    move-result v2

    #@52
    iput-boolean v2, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@54
    .line 120
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@57
    .line 121
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 56
    sget-boolean v0, Landroid/widget/FrameLayout;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    return v0
.end method

.method private getPaddingBottomWithForeground()I
    .registers 3

    #@0
    .prologue
    .line 285
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/view/View;->mPaddingBottom:I

    #@6
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@b
    move-result v0

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    iget v0, p0, Landroid/view/View;->mPaddingBottom:I

    #@f
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@11
    add-int/2addr v0, v1

    #@12
    goto :goto_c
.end method

.method private getPaddingLeftWithForeground()I
    .registers 3

    #@0
    .prologue
    .line 270
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@6
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@b
    move-result v0

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@f
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@11
    add-int/2addr v0, v1

    #@12
    goto :goto_c
.end method

.method private getPaddingRightWithForeground()I
    .registers 3

    #@0
    .prologue
    .line 275
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@6
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@b
    move-result v0

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@f
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@11
    add-int/2addr v0, v1

    #@12
    goto :goto_c
.end method

.method private getPaddingTopWithForeground()I
    .registers 3

    #@0
    .prologue
    .line 280
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@6
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@b
    move-result v0

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@f
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@11
    add-int/2addr v0, v1

    #@12
    goto :goto_c
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 565
    instance-of v0, p1, Landroid/widget/FrameLayout$LayoutParams;

    #@2
    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 465
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    #@4
    .line 467
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_3e

    #@8
    .line 468
    iget-object v6, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@a
    .line 470
    .local v6, foreground:Landroid/graphics/drawable/Drawable;
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundBoundsChanged:Z

    #@c
    if-eqz v0, :cond_3b

    #@e
    .line 471
    iput-boolean v2, p0, Landroid/widget/FrameLayout;->mForegroundBoundsChanged:Z

    #@10
    .line 472
    iget-object v3, p0, Landroid/widget/FrameLayout;->mSelfBounds:Landroid/graphics/Rect;

    #@12
    .line 473
    .local v3, selfBounds:Landroid/graphics/Rect;
    iget-object v4, p0, Landroid/widget/FrameLayout;->mOverlayBounds:Landroid/graphics/Rect;

    #@14
    .line 475
    .local v4, overlayBounds:Landroid/graphics/Rect;
    iget v0, p0, Landroid/view/View;->mRight:I

    #@16
    iget v1, p0, Landroid/view/View;->mLeft:I

    #@18
    sub-int v8, v0, v1

    #@1a
    .line 476
    .local v8, w:I
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@1c
    iget v1, p0, Landroid/view/View;->mTop:I

    #@1e
    sub-int v7, v0, v1

    #@20
    .line 478
    .local v7, h:I
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundInPadding:Z

    #@22
    if-eqz v0, :cond_3f

    #@24
    .line 479
    invoke-virtual {v3, v2, v2, v8, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@27
    .line 484
    :goto_27
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getLayoutDirection()I

    #@2a
    move-result v5

    #@2b
    .line 485
    .local v5, layoutDirection:I
    iget v0, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@2d
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@30
    move-result v1

    #@31
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@34
    move-result v2

    #@35
    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    #@38
    .line 488
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@3b
    .line 491
    .end local v3           #selfBounds:Landroid/graphics/Rect;
    .end local v4           #overlayBounds:Landroid/graphics/Rect;
    .end local v5           #layoutDirection:I
    .end local v7           #h:I
    .end local v8           #w:I
    :cond_3b
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@3e
    .line 493
    .end local v6           #foreground:Landroid/graphics/drawable/Drawable;
    :cond_3e
    return-void

    #@3f
    .line 481
    .restart local v3       #selfBounds:Landroid/graphics/Rect;
    .restart local v4       #overlayBounds:Landroid/graphics/Rect;
    .restart local v6       #foreground:Landroid/graphics/drawable/Drawable;
    .restart local v7       #h:I
    .restart local v8       #w:I
    :cond_3f
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@41
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@43
    iget v2, p0, Landroid/view/View;->mPaddingRight:I

    #@45
    sub-int v2, v8, v2

    #@47
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@49
    sub-int v9, v7, v9

    #@4b
    invoke-virtual {v3, v0, v1, v2, v9}, Landroid/graphics/Rect;->set(IIII)V

    #@4e
    goto :goto_27
.end method

.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 197
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    #@3
    .line 198
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_18

    #@7
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_18

    #@f
    .line 199
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getDrawableState()[I

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@18
    .line 201
    :cond_18
    return-void
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .registers 4
    .parameter "region"

    #@0
    .prologue
    .line 500
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    #@3
    move-result v0

    #@4
    .line 501
    .local v0, opaque:Z
    if-eqz p1, :cond_f

    #@6
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 502
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {p0, v1, p1}, Landroid/widget/FrameLayout;->applyDrawableToTransparentRegion(Landroid/graphics/drawable/Drawable;Landroid/graphics/Region;)V

    #@f
    .line 504
    :cond_f
    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 55
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 210
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@6
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 570
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 552
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public getConsiderGoneChildrenWhenMeasuring()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 534
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getMeasureAllChildren()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getForeground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getForegroundGravity()I
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@2
    return v0
.end method

.method public getMeasureAllChildren()Z
    .registers 2

    #@0
    .prologue
    .line 544
    iget-boolean v0, p0, Landroid/widget/FrameLayout;->mMeasureAllChildren:Z

    #@2
    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 188
    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    #@3
    .line 189
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 190
    :cond_c
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 576
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 577
    const-class v0, Landroid/widget/FrameLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 578
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 582
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 583
    const-class v0, Landroid/widget/FrameLayout;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 584
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 26
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 387
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getChildCount()I

    #@3
    move-result v6

    #@4
    .line 389
    .local v6, count:I
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingLeftWithForeground()I

    #@7
    move-result v13

    #@8
    .line 390
    .local v13, parentLeft:I
    sub-int v18, p4, p2

    #@a
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingRightWithForeground()I

    #@d
    move-result v19

    #@e
    sub-int v14, v18, v19

    #@10
    .line 392
    .local v14, parentRight:I
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingTopWithForeground()I

    #@13
    move-result v15

    #@14
    .line 393
    .local v15, parentTop:I
    sub-int v18, p5, p3

    #@16
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingBottomWithForeground()I

    #@19
    move-result v19

    #@1a
    sub-int v12, v18, v19

    #@1c
    .line 395
    .local v12, parentBottom:I
    const/16 v18, 0x1

    #@1e
    move/from16 v0, v18

    #@20
    move-object/from16 v1, p0

    #@22
    iput-boolean v0, v1, Landroid/widget/FrameLayout;->mForegroundBoundsChanged:Z

    #@24
    .line 396
    const/4 v9, 0x0

    #@25
    .local v9, i:I
    :goto_25
    if-ge v9, v6, :cond_ca

    #@27
    .line 397
    move-object/from16 v0, p0

    #@29
    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    #@2c
    move-result-object v3

    #@2d
    .line 398
    .local v3, child:Landroid/view/View;
    if-eqz v3, :cond_7d

    #@2f
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@32
    move-result v18

    #@33
    const/16 v19, 0x8

    #@35
    move/from16 v0, v18

    #@37
    move/from16 v1, v19

    #@39
    if-eq v0, v1, :cond_7d

    #@3b
    .line 399
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3e
    move-result-object v11

    #@3f
    check-cast v11, Landroid/widget/FrameLayout$LayoutParams;

    #@41
    .line 401
    .local v11, lp:Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@44
    move-result v17

    #@45
    .line 402
    .local v17, width:I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@48
    move-result v8

    #@49
    .line 407
    .local v8, height:I
    iget v7, v11, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@4b
    .line 408
    .local v7, gravity:I
    const/16 v18, -0x1

    #@4d
    move/from16 v0, v18

    #@4f
    if-ne v7, v0, :cond_54

    #@51
    .line 409
    const v7, 0x800033

    #@54
    .line 412
    :cond_54
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getLayoutDirection()I

    #@57
    move-result v10

    #@58
    .line 413
    .local v10, layoutDirection:I
    invoke-static {v7, v10}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@5b
    move-result v2

    #@5c
    .line 414
    .local v2, absoluteGravity:I
    and-int/lit8 v16, v7, 0x70

    #@5e
    .line 416
    .local v16, verticalGravity:I
    and-int/lit8 v18, v2, 0x7

    #@60
    packed-switch v18, :pswitch_data_cc

    #@63
    .line 428
    :pswitch_63
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@65
    move/from16 v18, v0

    #@67
    add-int v4, v13, v18

    #@69
    .line 431
    .local v4, childLeft:I
    :goto_69
    sparse-switch v16, :sswitch_data_da

    #@6c
    .line 443
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@6e
    move/from16 v18, v0

    #@70
    add-int v5, v15, v18

    #@72
    .line 446
    .local v5, childTop:I
    :goto_72
    add-int v18, v4, v17

    #@74
    add-int v19, v5, v8

    #@76
    move/from16 v0, v18

    #@78
    move/from16 v1, v19

    #@7a
    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/view/View;->layout(IIII)V

    #@7d
    .line 396
    .end local v2           #absoluteGravity:I
    .end local v4           #childLeft:I
    .end local v5           #childTop:I
    .end local v7           #gravity:I
    .end local v8           #height:I
    .end local v10           #layoutDirection:I
    .end local v11           #lp:Landroid/widget/FrameLayout$LayoutParams;
    .end local v16           #verticalGravity:I
    .end local v17           #width:I
    :cond_7d
    add-int/lit8 v9, v9, 0x1

    #@7f
    goto :goto_25

    #@80
    .line 418
    .restart local v2       #absoluteGravity:I
    .restart local v7       #gravity:I
    .restart local v8       #height:I
    .restart local v10       #layoutDirection:I
    .restart local v11       #lp:Landroid/widget/FrameLayout$LayoutParams;
    .restart local v16       #verticalGravity:I
    .restart local v17       #width:I
    :pswitch_80
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@82
    move/from16 v18, v0

    #@84
    add-int v4, v13, v18

    #@86
    .line 419
    .restart local v4       #childLeft:I
    goto :goto_69

    #@87
    .line 421
    .end local v4           #childLeft:I
    :pswitch_87
    sub-int v18, v14, v13

    #@89
    sub-int v18, v18, v17

    #@8b
    div-int/lit8 v18, v18, 0x2

    #@8d
    add-int v18, v18, v13

    #@8f
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@91
    move/from16 v19, v0

    #@93
    add-int v18, v18, v19

    #@95
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@97
    move/from16 v19, v0

    #@99
    sub-int v4, v18, v19

    #@9b
    .line 423
    .restart local v4       #childLeft:I
    goto :goto_69

    #@9c
    .line 425
    .end local v4           #childLeft:I
    :pswitch_9c
    sub-int v18, v14, v17

    #@9e
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@a0
    move/from16 v19, v0

    #@a2
    sub-int v4, v18, v19

    #@a4
    .line 426
    .restart local v4       #childLeft:I
    goto :goto_69

    #@a5
    .line 433
    :sswitch_a5
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@a7
    move/from16 v18, v0

    #@a9
    add-int v5, v15, v18

    #@ab
    .line 434
    .restart local v5       #childTop:I
    goto :goto_72

    #@ac
    .line 436
    .end local v5           #childTop:I
    :sswitch_ac
    sub-int v18, v12, v15

    #@ae
    sub-int v18, v18, v8

    #@b0
    div-int/lit8 v18, v18, 0x2

    #@b2
    add-int v18, v18, v15

    #@b4
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@b6
    move/from16 v19, v0

    #@b8
    add-int v18, v18, v19

    #@ba
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@bc
    move/from16 v19, v0

    #@be
    sub-int v5, v18, v19

    #@c0
    .line 438
    .restart local v5       #childTop:I
    goto :goto_72

    #@c1
    .line 440
    .end local v5           #childTop:I
    :sswitch_c1
    sub-int v18, v12, v8

    #@c3
    iget v0, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@c5
    move/from16 v19, v0

    #@c7
    sub-int v5, v18, v19

    #@c9
    .line 441
    .restart local v5       #childTop:I
    goto :goto_72

    #@ca
    .line 449
    .end local v2           #absoluteGravity:I
    .end local v3           #child:Landroid/view/View;
    .end local v4           #childLeft:I
    .end local v5           #childTop:I
    .end local v7           #gravity:I
    .end local v8           #height:I
    .end local v10           #layoutDirection:I
    .end local v11           #lp:Landroid/widget/FrameLayout$LayoutParams;
    .end local v16           #verticalGravity:I
    .end local v17           #width:I
    :cond_ca
    return-void

    #@cb
    .line 416
    nop

    #@cc
    :pswitch_data_cc
    .packed-switch 0x1
        :pswitch_87
        :pswitch_63
        :pswitch_80
        :pswitch_63
        :pswitch_9c
    .end packed-switch

    #@da
    .line 431
    :sswitch_data_da
    .sparse-switch
        0x10 -> :sswitch_ac
        0x30 -> :sswitch_a5
        0x50 -> :sswitch_c1
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 20
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 295
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getChildCount()I

    #@3
    move-result v10

    #@4
    .line 297
    .local v10, count:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v1

    #@8
    const/high16 v3, 0x4000

    #@a
    if-ne v1, v3, :cond_14

    #@c
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@f
    move-result v1

    #@10
    const/high16 v3, 0x4000

    #@12
    if-eq v1, v3, :cond_82

    #@14
    :cond_14
    const/16 v16, 0x1

    #@16
    .line 300
    .local v16, measureMatchParentChildren:Z
    :goto_16
    move-object/from16 v0, p0

    #@18
    iget-object v1, v0, Landroid/widget/FrameLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@1d
    .line 302
    const/4 v14, 0x0

    #@1e
    .line 303
    .local v14, maxHeight:I
    const/4 v15, 0x0

    #@1f
    .line 304
    .local v15, maxWidth:I
    const/4 v8, 0x0

    #@20
    .line 306
    .local v8, childState:I
    const/4 v12, 0x0

    #@21
    .local v12, i:I
    :goto_21
    if-ge v12, v10, :cond_85

    #@23
    .line 307
    move-object/from16 v0, p0

    #@25
    invoke-virtual {v0, v12}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    #@28
    move-result-object v2

    #@29
    .line 308
    .local v2, child:Landroid/view/View;
    move-object/from16 v0, p0

    #@2b
    iget-boolean v1, v0, Landroid/widget/FrameLayout;->mMeasureAllChildren:Z

    #@2d
    if-nez v1, :cond_37

    #@2f
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@32
    move-result v1

    #@33
    const/16 v3, 0x8

    #@35
    if-eq v1, v3, :cond_7f

    #@37
    .line 309
    :cond_37
    const/4 v4, 0x0

    #@38
    const/4 v6, 0x0

    #@39
    move-object/from16 v1, p0

    #@3b
    move/from16 v3, p1

    #@3d
    move/from16 v5, p2

    #@3f
    invoke-virtual/range {v1 .. v6}, Landroid/widget/FrameLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    #@42
    .line 310
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@45
    move-result-object v13

    #@46
    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    #@48
    .line 311
    .local v13, lp:Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    #@4b
    move-result v1

    #@4c
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@4e
    add-int/2addr v1, v3

    #@4f
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@51
    add-int/2addr v1, v3

    #@52
    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    #@55
    move-result v15

    #@56
    .line 313
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    #@59
    move-result v1

    #@5a
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@5c
    add-int/2addr v1, v3

    #@5d
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@5f
    add-int/2addr v1, v3

    #@60
    invoke-static {v14, v1}, Ljava/lang/Math;->max(II)I

    #@63
    move-result v14

    #@64
    .line 315
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredState()I

    #@67
    move-result v1

    #@68
    invoke-static {v8, v1}, Landroid/widget/FrameLayout;->combineMeasuredStates(II)I

    #@6b
    move-result v8

    #@6c
    .line 316
    if-eqz v16, :cond_7f

    #@6e
    .line 317
    iget v1, v13, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@70
    const/4 v3, -0x1

    #@71
    if-eq v1, v3, :cond_78

    #@73
    iget v1, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@75
    const/4 v3, -0x1

    #@76
    if-ne v1, v3, :cond_7f

    #@78
    .line 319
    :cond_78
    move-object/from16 v0, p0

    #@7a
    iget-object v1, v0, Landroid/widget/FrameLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    #@7c
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7f
    .line 306
    .end local v13           #lp:Landroid/widget/FrameLayout$LayoutParams;
    :cond_7f
    add-int/lit8 v12, v12, 0x1

    #@81
    goto :goto_21

    #@82
    .line 297
    .end local v2           #child:Landroid/view/View;
    .end local v8           #childState:I
    .end local v12           #i:I
    .end local v14           #maxHeight:I
    .end local v15           #maxWidth:I
    .end local v16           #measureMatchParentChildren:Z
    :cond_82
    const/16 v16, 0x0

    #@84
    goto :goto_16

    #@85
    .line 326
    .restart local v8       #childState:I
    .restart local v12       #i:I
    .restart local v14       #maxHeight:I
    .restart local v15       #maxWidth:I
    .restart local v16       #measureMatchParentChildren:Z
    :cond_85
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingLeftWithForeground()I

    #@88
    move-result v1

    #@89
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingRightWithForeground()I

    #@8c
    move-result v3

    #@8d
    add-int/2addr v1, v3

    #@8e
    add-int/2addr v15, v1

    #@8f
    .line 327
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingTopWithForeground()I

    #@92
    move-result v1

    #@93
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingBottomWithForeground()I

    #@96
    move-result v3

    #@97
    add-int/2addr v1, v3

    #@98
    add-int/2addr v14, v1

    #@99
    .line 330
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getSuggestedMinimumHeight()I

    #@9c
    move-result v1

    #@9d
    invoke-static {v14, v1}, Ljava/lang/Math;->max(II)I

    #@a0
    move-result v14

    #@a1
    .line 331
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getSuggestedMinimumWidth()I

    #@a4
    move-result v1

    #@a5
    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    #@a8
    move-result v15

    #@a9
    .line 334
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    #@ac
    move-result-object v11

    #@ad
    .line 335
    .local v11, drawable:Landroid/graphics/drawable/Drawable;
    if-eqz v11, :cond_bf

    #@af
    .line 336
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@b2
    move-result v1

    #@b3
    invoke-static {v14, v1}, Ljava/lang/Math;->max(II)I

    #@b6
    move-result v14

    #@b7
    .line 337
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@ba
    move-result v1

    #@bb
    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    #@be
    move-result v15

    #@bf
    .line 340
    :cond_bf
    move/from16 v0, p1

    #@c1
    invoke-static {v15, v0, v8}, Landroid/widget/FrameLayout;->resolveSizeAndState(III)I

    #@c4
    move-result v1

    #@c5
    shl-int/lit8 v3, v8, 0x10

    #@c7
    move/from16 v0, p2

    #@c9
    invoke-static {v14, v0, v3}, Landroid/widget/FrameLayout;->resolveSizeAndState(III)I

    #@cc
    move-result v3

    #@cd
    move-object/from16 v0, p0

    #@cf
    invoke-virtual {v0, v1, v3}, Landroid/widget/FrameLayout;->setMeasuredDimension(II)V

    #@d2
    .line 344
    move-object/from16 v0, p0

    #@d4
    iget-object v1, v0, Landroid/widget/FrameLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    #@d6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@d9
    move-result v10

    #@da
    .line 345
    const/4 v1, 0x1

    #@db
    if-le v10, v1, :cond_164

    #@dd
    .line 346
    const/4 v12, 0x0

    #@de
    :goto_de
    if-ge v12, v10, :cond_164

    #@e0
    .line 347
    move-object/from16 v0, p0

    #@e2
    iget-object v1, v0, Landroid/widget/FrameLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    #@e4
    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e7
    move-result-object v2

    #@e8
    check-cast v2, Landroid/view/View;

    #@ea
    .line 349
    .restart local v2       #child:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@ed
    move-result-object v13

    #@ee
    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    #@f0
    .line 353
    .local v13, lp:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, v13, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@f2
    const/4 v3, -0x1

    #@f3
    if-ne v1, v3, :cond_134

    #@f5
    .line 354
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    #@f8
    move-result v1

    #@f9
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingLeftWithForeground()I

    #@fc
    move-result v3

    #@fd
    sub-int/2addr v1, v3

    #@fe
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingRightWithForeground()I

    #@101
    move-result v3

    #@102
    sub-int/2addr v1, v3

    #@103
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@105
    sub-int/2addr v1, v3

    #@106
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@108
    sub-int/2addr v1, v3

    #@109
    const/high16 v3, 0x4000

    #@10b
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@10e
    move-result v9

    #@10f
    .line 365
    .local v9, childWidthMeasureSpec:I
    :goto_10f
    iget v1, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@111
    const/4 v3, -0x1

    #@112
    if-ne v1, v3, :cond_14c

    #@114
    .line 366
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    #@117
    move-result v1

    #@118
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingTopWithForeground()I

    #@11b
    move-result v3

    #@11c
    sub-int/2addr v1, v3

    #@11d
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingBottomWithForeground()I

    #@120
    move-result v3

    #@121
    sub-int/2addr v1, v3

    #@122
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@124
    sub-int/2addr v1, v3

    #@125
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@127
    sub-int/2addr v1, v3

    #@128
    const/high16 v3, 0x4000

    #@12a
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@12d
    move-result v7

    #@12e
    .line 377
    .local v7, childHeightMeasureSpec:I
    :goto_12e
    invoke-virtual {v2, v9, v7}, Landroid/view/View;->measure(II)V

    #@131
    .line 346
    add-int/lit8 v12, v12, 0x1

    #@133
    goto :goto_de

    #@134
    .line 359
    .end local v7           #childHeightMeasureSpec:I
    .end local v9           #childWidthMeasureSpec:I
    :cond_134
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingLeftWithForeground()I

    #@137
    move-result v1

    #@138
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingRightWithForeground()I

    #@13b
    move-result v3

    #@13c
    add-int/2addr v1, v3

    #@13d
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@13f
    add-int/2addr v1, v3

    #@140
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@142
    add-int/2addr v1, v3

    #@143
    iget v3, v13, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@145
    move/from16 v0, p1

    #@147
    invoke-static {v0, v1, v3}, Landroid/widget/FrameLayout;->getChildMeasureSpec(III)I

    #@14a
    move-result v9

    #@14b
    .restart local v9       #childWidthMeasureSpec:I
    goto :goto_10f

    #@14c
    .line 371
    :cond_14c
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingTopWithForeground()I

    #@14f
    move-result v1

    #@150
    invoke-direct/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingBottomWithForeground()I

    #@153
    move-result v3

    #@154
    add-int/2addr v1, v3

    #@155
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@157
    add-int/2addr v1, v3

    #@158
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@15a
    add-int/2addr v1, v3

    #@15b
    iget v3, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@15d
    move/from16 v0, p2

    #@15f
    invoke-static {v0, v1, v3}, Landroid/widget/FrameLayout;->getChildMeasureSpec(III)I

    #@162
    move-result v7

    #@163
    .restart local v7       #childHeightMeasureSpec:I
    goto :goto_12e

    #@164
    .line 380
    .end local v2           #child:Landroid/view/View;
    .end local v7           #childHeightMeasureSpec:I
    .end local v9           #childWidthMeasureSpec:I
    .end local v13           #lp:Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_164
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 456
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    #@3
    .line 457
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/widget/FrameLayout;->mForegroundBoundsChanged:Z

    #@6
    .line 458
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "drawable"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 224
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@3
    if-eq v1, p1, :cond_5a

    #@5
    .line 225
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@7
    if-eqz v1, :cond_14

    #@9
    .line 226
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@f
    .line 227
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@14
    .line 230
    :cond_14
    iput-object p1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@16
    .line 231
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@18
    .line 232
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@1a
    .line 233
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@1c
    .line 234
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@1e
    .line 236
    if-eqz p1, :cond_5b

    #@20
    .line 237
    invoke-virtual {p0, v3}, Landroid/widget/FrameLayout;->setWillNotDraw(Z)V

    #@23
    .line 238
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@26
    .line 239
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_33

    #@2c
    .line 240
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getDrawableState()[I

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@33
    .line 242
    :cond_33
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@35
    const/16 v2, 0x77

    #@37
    if-ne v1, v2, :cond_54

    #@39
    .line 243
    new-instance v0, Landroid/graphics/Rect;

    #@3b
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@3e
    .line 244
    .local v0, padding:Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_54

    #@44
    .line 245
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@46
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@48
    .line 246
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@4a
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@4c
    .line 247
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@4e
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@50
    .line 248
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@52
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@54
    .line 254
    .end local v0           #padding:Landroid/graphics/Rect;
    :cond_54
    :goto_54
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    #@57
    .line 255
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->invalidate()V

    #@5a
    .line 257
    :cond_5a
    return-void

    #@5b
    .line 252
    :cond_5b
    const/4 v1, 0x1

    #@5c
    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->setWillNotDraw(Z)V

    #@5f
    goto :goto_54
.end method

.method public setForegroundGravity(I)V
    .registers 6
    .parameter "foregroundGravity"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 147
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@3
    if-eq v1, p1, :cond_41

    #@5
    .line 148
    const v1, 0x800007

    #@8
    and-int/2addr v1, p1

    #@9
    if-nez v1, :cond_f

    #@b
    .line 149
    const v1, 0x800003

    #@e
    or-int/2addr p1, v1

    #@f
    .line 152
    :cond_f
    and-int/lit8 v1, p1, 0x70

    #@11
    if-nez v1, :cond_15

    #@13
    .line 153
    or-int/lit8 p1, p1, 0x30

    #@15
    .line 156
    :cond_15
    iput p1, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@17
    .line 159
    iget v1, p0, Landroid/widget/FrameLayout;->mForegroundGravity:I

    #@19
    const/16 v2, 0x77

    #@1b
    if-ne v1, v2, :cond_42

    #@1d
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@1f
    if-eqz v1, :cond_42

    #@21
    .line 160
    new-instance v0, Landroid/graphics/Rect;

    #@23
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@26
    .line 161
    .local v0, padding:Landroid/graphics/Rect;
    iget-object v1, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@28
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_3e

    #@2e
    .line 162
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@30
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@32
    .line 163
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@34
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@36
    .line 164
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@38
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@3a
    .line 165
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@3c
    iput v1, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@3e
    .line 174
    .end local v0           #padding:Landroid/graphics/Rect;
    :cond_3e
    :goto_3e
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    #@41
    .line 176
    :cond_41
    return-void

    #@42
    .line 168
    :cond_42
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingLeft:I

    #@44
    .line 169
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingTop:I

    #@46
    .line 170
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingRight:I

    #@48
    .line 171
    iput v3, p0, Landroid/widget/FrameLayout;->mForegroundPaddingBottom:I

    #@4a
    goto :goto_3e
.end method

.method public setMeasureAllChildren(Z)V
    .registers 2
    .parameter "measureAll"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 518
    iput-boolean p1, p0, Landroid/widget/FrameLayout;->mMeasureAllChildren:Z

    #@2
    .line 519
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 557
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    iget-object v0, p0, Landroid/widget/FrameLayout;->mForeground:Landroid/graphics/drawable/Drawable;

    #@8
    if-ne p1, v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
