.class Landroid/widget/Editor$HandleView$1;
.super Ljava/lang/Object;
.source "Editor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/Editor$HandleView;->showActionPopupWindow(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/widget/Editor$HandleView;


# direct methods
.method constructor <init>(Landroid/widget/Editor$HandleView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3969
    iput-object p1, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3971
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@3
    if-eqz v3, :cond_5f

    #@5
    iget-object v3, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@7
    iget-object v3, v3, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@9
    invoke-static {v3}, Landroid/widget/Editor;->access$000(Landroid/widget/Editor;)Landroid/widget/TextView;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3}, Landroid/widget/TextView;->hasSelection()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_5f

    #@13
    .line 3973
    iget-object v3, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@15
    iget-object v3, v3, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@17
    invoke-virtual {v3}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@1a
    move-result-object v0

    #@1b
    .line 3974
    .local v0, controller:Landroid/widget/Editor$SelectionModifierCursorController;
    if-eqz v0, :cond_5f

    #@1d
    .line 3975
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionModifierCursorController;->getStartHandle()Landroid/widget/Editor$SelectionStartHandleView;

    #@20
    move-result-object v2

    #@21
    .line 3976
    .local v2, start:Landroid/widget/Editor$HandleView;
    if-eqz v2, :cond_3e

    #@23
    .line 3977
    iget-object v3, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@25
    iget-object v3, v3, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@27
    invoke-static {v3}, Landroid/widget/Editor;->access$3200(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Landroid/widget/Editor$PositionListener;->getPositionX()I

    #@2e
    move-result v3

    #@2f
    iget-object v4, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@31
    iget-object v4, v4, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@33
    invoke-static {v4}, Landroid/widget/Editor;->access$3200(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Landroid/widget/Editor$PositionListener;->getPositionY()I

    #@3a
    move-result v4

    #@3b
    invoke-virtual {v2, v3, v4, v5, v5}, Landroid/widget/Editor$HandleView;->updatePosition(IIZZ)V

    #@3e
    .line 3978
    :cond_3e
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionModifierCursorController;->getEndHandle()Landroid/widget/Editor$SelectionEndHandleView;

    #@41
    move-result-object v1

    #@42
    .line 3979
    .local v1, end:Landroid/widget/Editor$HandleView;
    if-eqz v1, :cond_5f

    #@44
    .line 3980
    iget-object v3, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@46
    iget-object v3, v3, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@48
    invoke-static {v3}, Landroid/widget/Editor;->access$3200(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Landroid/widget/Editor$PositionListener;->getPositionX()I

    #@4f
    move-result v3

    #@50
    iget-object v4, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@52
    iget-object v4, v4, Landroid/widget/Editor$HandleView;->this$0:Landroid/widget/Editor;

    #@54
    invoke-static {v4}, Landroid/widget/Editor;->access$3200(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4}, Landroid/widget/Editor$PositionListener;->getPositionY()I

    #@5b
    move-result v4

    #@5c
    invoke-virtual {v1, v3, v4, v5, v5}, Landroid/widget/Editor$HandleView;->updatePosition(IIZZ)V

    #@5f
    .line 3983
    .end local v0           #controller:Landroid/widget/Editor$SelectionModifierCursorController;
    .end local v1           #end:Landroid/widget/Editor$HandleView;
    .end local v2           #start:Landroid/widget/Editor$HandleView;
    :cond_5f
    iget-object v3, p0, Landroid/widget/Editor$HandleView$1;->this$1:Landroid/widget/Editor$HandleView;

    #@61
    iget-object v3, v3, Landroid/widget/Editor$HandleView;->mActionPopupWindow:Landroid/widget/Editor$ActionPopupWindow;

    #@63
    invoke-virtual {v3}, Landroid/widget/Editor$ActionPopupWindow;->show()V

    #@66
    .line 3984
    return-void
.end method
