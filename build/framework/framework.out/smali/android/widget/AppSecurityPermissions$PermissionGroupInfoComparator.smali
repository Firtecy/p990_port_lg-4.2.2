.class Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;
.super Ljava/lang/Object;
.source "AppSecurityPermissions.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AppSecurityPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PermissionGroupInfoComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final sCollator:Ljava/text/Collator;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 579
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 578
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;->sCollator:Ljava/text/Collator;

    #@9
    .line 580
    return-void
.end method


# virtual methods
.method public final compare(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;)I
    .registers 7
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, -0x1

    #@2
    .line 582
    iget v2, p1, Landroid/content/pm/PermissionGroupInfo;->flags:I

    #@4
    iget v3, p2, Landroid/content/pm/PermissionGroupInfo;->flags:I

    #@6
    xor-int/2addr v2, v3

    #@7
    and-int/lit8 v2, v2, 0x1

    #@9
    if-eqz v2, :cond_14

    #@b
    .line 583
    iget v2, p1, Landroid/content/pm/PermissionGroupInfo;->flags:I

    #@d
    and-int/lit8 v2, v2, 0x1

    #@f
    if-eqz v2, :cond_12

    #@11
    .line 588
    :cond_11
    :goto_11
    return v0

    #@12
    :cond_12
    move v0, v1

    #@13
    .line 583
    goto :goto_11

    #@14
    .line 585
    :cond_14
    iget v2, p1, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@16
    iget v3, p2, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@18
    if-eq v2, v3, :cond_22

    #@1a
    .line 586
    iget v2, p1, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@1c
    iget v3, p2, Landroid/content/pm/PermissionGroupInfo;->priority:I

    #@1e
    if-gt v2, v3, :cond_11

    #@20
    move v0, v1

    #@21
    goto :goto_11

    #@22
    .line 588
    :cond_22
    iget-object v0, p0, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;->sCollator:Ljava/text/Collator;

    #@24
    iget-object v1, p1, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    #@26
    iget-object v2, p2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;->mLabel:Ljava/lang/CharSequence;

    #@28
    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    #@2b
    move-result v0

    #@2c
    goto :goto_11
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 577
    check-cast p1, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/widget/AppSecurityPermissions$PermissionGroupInfoComparator;->compare(Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;Landroid/widget/AppSecurityPermissions$MyPermissionGroupInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
