.class public Landroid/widget/ViewSwitcher;
.super Landroid/widget/ViewAnimator;
.source "ViewSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ViewSwitcher$ViewFactory;
    }
.end annotation


# instance fields
.field mFactory:Landroid/widget/ViewSwitcher$ViewFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;)V

    #@3
    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 56
    return-void
.end method

.method private obtainView()Landroid/view/View;
    .registers 5

    #@0
    .prologue
    .line 94
    iget-object v2, p0, Landroid/widget/ViewSwitcher;->mFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    #@2
    invoke-interface {v2}, Landroid/widget/ViewSwitcher$ViewFactory;->makeView()Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    .line 95
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    #@c
    .line 96
    .local v1, lp:Landroid/widget/FrameLayout$LayoutParams;
    if-nez v1, :cond_15

    #@e
    .line 97
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@10
    .end local v1           #lp:Landroid/widget/FrameLayout$LayoutParams;
    const/4 v2, -0x1

    #@11
    const/4 v3, -0x2

    #@12
    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@15
    .line 99
    .restart local v1       #lp:Landroid/widget/FrameLayout$LayoutParams;
    :cond_15
    invoke-virtual {p0, v0, v1}, Landroid/widget/ViewSwitcher;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@18
    .line 100
    return-object v0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 65
    invoke-virtual {p0}, Landroid/widget/ViewSwitcher;->getChildCount()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x2

    #@5
    if-lt v0, v1, :cond_f

    #@7
    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Can\'t add more than 2 views to a ViewSwitcher"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 68
    :cond_f
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ViewAnimator;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@12
    .line 69
    return-void
.end method

.method public getNextView()Landroid/view/View;
    .registers 3

    #@0
    .prologue
    .line 89
    iget v1, p0, Landroid/widget/ViewAnimator;->mWhichChild:I

    #@2
    if-nez v1, :cond_a

    #@4
    const/4 v0, 0x1

    #@5
    .line 90
    .local v0, which:I
    :goto_5
    invoke-virtual {p0, v0}, Landroid/widget/ViewSwitcher;->getChildAt(I)Landroid/view/View;

    #@8
    move-result-object v1

    #@9
    return-object v1

    #@a
    .line 89
    .end local v0           #which:I
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_5
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/widget/ViewAnimator;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 74
    const-class v0, Landroid/widget/ViewSwitcher;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 75
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/widget/ViewAnimator;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 80
    const-class v0, Landroid/widget/ViewSwitcher;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 81
    return-void
.end method

.method public reset()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x1

    #@3
    .line 122
    iput-boolean v2, p0, Landroid/widget/ViewAnimator;->mFirstTime:Z

    #@5
    .line 124
    const/4 v1, 0x0

    #@6
    invoke-virtual {p0, v1}, Landroid/widget/ViewSwitcher;->getChildAt(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 125
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_f

    #@c
    .line 126
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@f
    .line 128
    :cond_f
    invoke-virtual {p0, v2}, Landroid/widget/ViewSwitcher;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v0

    #@13
    .line 129
    if-eqz v0, :cond_18

    #@15
    .line 130
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@18
    .line 132
    :cond_18
    return-void
.end method

.method public setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V
    .registers 2
    .parameter "factory"

    #@0
    .prologue
    .line 112
    iput-object p1, p0, Landroid/widget/ViewSwitcher;->mFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    #@2
    .line 113
    invoke-direct {p0}, Landroid/widget/ViewSwitcher;->obtainView()Landroid/view/View;

    #@5
    .line 114
    invoke-direct {p0}, Landroid/widget/ViewSwitcher;->obtainView()Landroid/view/View;

    #@8
    .line 115
    return-void
.end method
