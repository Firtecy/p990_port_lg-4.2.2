.class Landroid/widget/AbsListView$CheckForKeyLongPress;
.super Landroid/widget/AbsListView$WindowRunnnable;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForKeyLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method private constructor <init>(Landroid/widget/AbsListView;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2811
    iput-object p1, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Landroid/widget/AbsListView$WindowRunnnable;-><init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V

    #@6
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2811
    invoke-direct {p0, p1}, Landroid/widget/AbsListView$CheckForKeyLongPress;-><init>(Landroid/widget/AbsListView;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2813
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@3
    invoke-virtual {v3}, Landroid/widget/AbsListView;->isPressed()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_44

    #@9
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@b
    iget v3, v3, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@d
    if-ltz v3, :cond_44

    #@f
    .line 2814
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@11
    iget v3, v3, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@13
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@15
    iget v4, v4, Landroid/widget/AdapterView;->mFirstPosition:I

    #@17
    sub-int v1, v3, v4

    #@19
    .line 2815
    .local v1, index:I
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@1b
    invoke-virtual {v3, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1e
    move-result-object v2

    #@1f
    .line 2817
    .local v2, v:Landroid/view/View;
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@21
    iget-boolean v3, v3, Landroid/widget/AdapterView;->mDataChanged:Z

    #@23
    if-nez v3, :cond_45

    #@25
    .line 2818
    const/4 v0, 0x0

    #@26
    .line 2819
    .local v0, handled:Z
    invoke-virtual {p0}, Landroid/widget/AbsListView$CheckForKeyLongPress;->sameWindow()Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_3a

    #@2c
    .line 2820
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@2e
    iget-object v4, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@30
    iget v4, v4, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@32
    iget-object v5, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@34
    iget-wide v5, v5, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@36
    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/widget/AbsListView;->performLongPress(Landroid/view/View;IJ)Z

    #@39
    move-result v0

    #@3a
    .line 2822
    :cond_3a
    if-eqz v0, :cond_44

    #@3c
    .line 2823
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@3e
    invoke-virtual {v3, v7}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@41
    .line 2824
    invoke-virtual {v2, v7}, Landroid/view/View;->setPressed(Z)V

    #@44
    .line 2831
    .end local v0           #handled:Z
    .end local v1           #index:I
    .end local v2           #v:Landroid/view/View;
    :cond_44
    :goto_44
    return-void

    #@45
    .line 2827
    .restart local v1       #index:I
    .restart local v2       #v:Landroid/view/View;
    :cond_45
    iget-object v3, p0, Landroid/widget/AbsListView$CheckForKeyLongPress;->this$0:Landroid/widget/AbsListView;

    #@47
    invoke-virtual {v3, v7}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@4a
    .line 2828
    if-eqz v2, :cond_44

    #@4c
    invoke-virtual {v2, v7}, Landroid/view/View;->setPressed(Z)V

    #@4f
    goto :goto_44
.end method
