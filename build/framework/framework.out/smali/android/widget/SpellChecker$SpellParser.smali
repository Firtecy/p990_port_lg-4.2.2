.class Landroid/widget/SpellChecker$SpellParser;
.super Ljava/lang/Object;
.source "SpellChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/SpellChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpellParser"
.end annotation


# instance fields
.field private mRange:Ljava/lang/Object;

.field final synthetic this$0:Landroid/widget/SpellChecker;


# direct methods
.method private constructor <init>(Landroid/widget/SpellChecker;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 503
    iput-object p1, p0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 504
    new-instance v0, Ljava/lang/Object;

    #@7
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v0, p0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/SpellChecker;Landroid/widget/SpellChecker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 503
    invoke-direct {p0, p1}, Landroid/widget/SpellChecker$SpellParser;-><init>(Landroid/widget/SpellChecker;)V

    #@3
    return-void
.end method

.method private removeRangeSpan(Landroid/text/Editable;)V
    .registers 3
    .parameter "editable"

    #@0
    .prologue
    .line 541
    iget-object v0, p0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@2
    invoke-interface {p1, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@5
    .line 542
    return-void
.end method

.method private removeSpansAt(Landroid/text/Editable;I[Ljava/lang/Object;)V
    .registers 9
    .parameter "editable"
    .parameter "offset"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/text/Editable;",
            "I[TT;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 745
    .local p3, spans:[Ljava/lang/Object;,"[TT;"
    array-length v2, p3

    #@1
    .line 746
    .local v2, length:I
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    if-ge v1, v2, :cond_19

    #@4
    .line 747
    aget-object v3, p3, v1

    #@6
    .line 748
    .local v3, span:Ljava/lang/Object;,"TT;"
    invoke-interface {p1, v3}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@9
    move-result v4

    #@a
    .line 749
    .local v4, start:I
    if-le v4, p2, :cond_f

    #@c
    .line 746
    :cond_c
    :goto_c
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_2

    #@f
    .line 750
    :cond_f
    invoke-interface {p1, v3}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@12
    move-result v0

    #@13
    .line 751
    .local v0, end:I
    if-lt v0, p2, :cond_c

    #@15
    .line 752
    invoke-interface {p1, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@18
    goto :goto_c

    #@19
    .line 754
    .end local v0           #end:I
    .end local v3           #span:Ljava/lang/Object;,"TT;"
    .end local v4           #start:I
    :cond_19
    return-void
.end method

.method private setRangeSpan(Landroid/text/Editable;II)V
    .registers 6
    .parameter "editable"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@2
    const/16 v1, 0x21

    #@4
    invoke-interface {p1, v0, p2, p3, v1}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@7
    .line 534
    return-void
.end method


# virtual methods
.method public isFinished()Z
    .registers 3

    #@0
    .prologue
    .line 522
    iget-object v0, p0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2
    invoke-static {v0}, Landroid/widget/SpellChecker;->access$200(Landroid/widget/SpellChecker;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/text/Editable;

    #@c
    iget-object v1, p0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@e
    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@11
    move-result v0

    #@12
    if-gez v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public parse()V
    .registers 26

    #@0
    .prologue
    .line 545
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@4
    move-object/from16 v22, v0

    #@6
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$200(Landroid/widget/SpellChecker;)Landroid/widget/TextView;

    #@9
    move-result-object v22

    #@a
    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@d
    move-result-object v5

    #@e
    check-cast v5, Landroid/text/Editable;

    #@10
    .line 548
    .local v5, editable:Landroid/text/Editable;
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@14
    move-object/from16 v22, v0

    #@16
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$400(Landroid/widget/SpellChecker;)Z

    #@19
    move-result v22

    #@1a
    if-eqz v22, :cond_af

    #@1c
    .line 551
    const/16 v22, 0x0

    #@1e
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@22
    move-object/from16 v23, v0

    #@24
    move-object/from16 v0, v23

    #@26
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@29
    move-result v23

    #@2a
    add-int/lit8 v23, v23, -0x32

    #@2c
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    #@2f
    move-result v16

    #@30
    .line 557
    .local v16, start:I
    :goto_30
    move-object/from16 v0, p0

    #@32
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@34
    move-object/from16 v22, v0

    #@36
    move-object/from16 v0, v22

    #@38
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@3b
    move-result v6

    #@3c
    .line 559
    .local v6, end:I
    move/from16 v0, v16

    #@3e
    add-int/lit16 v0, v0, 0x15e

    #@40
    move/from16 v22, v0

    #@42
    move/from16 v0, v22

    #@44
    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    #@47
    move-result v20

    #@48
    .line 560
    .local v20, wordIteratorWindowEnd:I
    move-object/from16 v0, p0

    #@4a
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@4c
    move-object/from16 v22, v0

    #@4e
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@51
    move-result-object v22

    #@52
    move-object/from16 v0, v22

    #@54
    move/from16 v1, v16

    #@56
    move/from16 v2, v20

    #@58
    invoke-virtual {v0, v5, v1, v2}, Landroid/text/method/WordIterator;->setCharSequence(Ljava/lang/CharSequence;II)V

    #@5b
    .line 563
    move-object/from16 v0, p0

    #@5d
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@5f
    move-object/from16 v22, v0

    #@61
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@64
    move-result-object v22

    #@65
    move-object/from16 v0, v22

    #@67
    move/from16 v1, v16

    #@69
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->preceding(I)I

    #@6c
    move-result v21

    #@6d
    .line 565
    .local v21, wordStart:I
    const/16 v22, -0x1

    #@6f
    move/from16 v0, v21

    #@71
    move/from16 v1, v22

    #@73
    if-ne v0, v1, :cond_bd

    #@75
    .line 566
    move-object/from16 v0, p0

    #@77
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@79
    move-object/from16 v22, v0

    #@7b
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@7e
    move-result-object v22

    #@7f
    move-object/from16 v0, v22

    #@81
    move/from16 v1, v16

    #@83
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->following(I)I

    #@86
    move-result v19

    #@87
    .line 567
    .local v19, wordEnd:I
    const/16 v22, -0x1

    #@89
    move/from16 v0, v19

    #@8b
    move/from16 v1, v22

    #@8d
    if-eq v0, v1, :cond_a1

    #@8f
    .line 568
    move-object/from16 v0, p0

    #@91
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@93
    move-object/from16 v22, v0

    #@95
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@98
    move-result-object v22

    #@99
    move-object/from16 v0, v22

    #@9b
    move/from16 v1, v19

    #@9d
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->getBeginning(I)I

    #@a0
    move-result v21

    #@a1
    .line 573
    :cond_a1
    :goto_a1
    const/16 v22, -0x1

    #@a3
    move/from16 v0, v19

    #@a5
    move/from16 v1, v22

    #@a7
    if-ne v0, v1, :cond_d0

    #@a9
    .line 577
    move-object/from16 v0, p0

    #@ab
    invoke-direct {v0, v5}, Landroid/widget/SpellChecker$SpellParser;->removeRangeSpan(Landroid/text/Editable;)V

    #@ae
    .line 742
    :goto_ae
    return-void

    #@af
    .line 554
    .end local v6           #end:I
    .end local v16           #start:I
    .end local v19           #wordEnd:I
    .end local v20           #wordIteratorWindowEnd:I
    .end local v21           #wordStart:I
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->mRange:Ljava/lang/Object;

    #@b3
    move-object/from16 v22, v0

    #@b5
    move-object/from16 v0, v22

    #@b7
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@ba
    move-result v16

    #@bb
    .restart local v16       #start:I
    goto/16 :goto_30

    #@bd
    .line 571
    .restart local v6       #end:I
    .restart local v20       #wordIteratorWindowEnd:I
    .restart local v21       #wordStart:I
    :cond_bd
    move-object/from16 v0, p0

    #@bf
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@c1
    move-object/from16 v22, v0

    #@c3
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@c6
    move-result-object v22

    #@c7
    move-object/from16 v0, v22

    #@c9
    move/from16 v1, v21

    #@cb
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->getEnd(I)I

    #@ce
    move-result v19

    #@cf
    .restart local v19       #wordEnd:I
    goto :goto_a1

    #@d0
    .line 583
    :cond_d0
    add-int/lit8 v22, v16, -0x1

    #@d2
    add-int/lit8 v23, v6, 0x1

    #@d4
    const-class v24, Landroid/text/style/SpellCheckSpan;

    #@d6
    move/from16 v0, v22

    #@d8
    move/from16 v1, v23

    #@da
    move-object/from16 v2, v24

    #@dc
    invoke-interface {v5, v0, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@df
    move-result-object v14

    #@e0
    check-cast v14, [Landroid/text/style/SpellCheckSpan;

    #@e2
    .line 585
    .local v14, spellCheckSpans:[Landroid/text/style/SpellCheckSpan;
    add-int/lit8 v22, v16, -0x1

    #@e4
    add-int/lit8 v23, v6, 0x1

    #@e6
    const-class v24, Landroid/text/style/SuggestionSpan;

    #@e8
    move/from16 v0, v22

    #@ea
    move/from16 v1, v23

    #@ec
    move-object/from16 v2, v24

    #@ee
    invoke-interface {v5, v0, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@f1
    move-result-object v17

    #@f2
    check-cast v17, [Landroid/text/style/SuggestionSpan;

    #@f4
    .line 588
    .local v17, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    const/16 v18, 0x0

    #@f6
    .line 589
    .local v18, wordCount:I
    const/4 v9, 0x0

    #@f7
    .line 591
    .local v9, scheduleOtherSpellCheck:Z
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@fb
    move-object/from16 v22, v0

    #@fd
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$400(Landroid/widget/SpellChecker;)Z

    #@100
    move-result v22

    #@101
    if-eqz v22, :cond_1f0

    #@103
    .line 592
    move/from16 v0, v20

    #@105
    if-ge v0, v6, :cond_108

    #@107
    .line 597
    const/4 v9, 0x1

    #@108
    .line 599
    :cond_108
    move-object/from16 v0, p0

    #@10a
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@10c
    move-object/from16 v22, v0

    #@10e
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@111
    move-result-object v22

    #@112
    move-object/from16 v0, v22

    #@114
    move/from16 v1, v20

    #@116
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->preceding(I)I

    #@119
    move-result v12

    #@11a
    .line 600
    .local v12, spellCheckEnd:I
    const/16 v22, -0x1

    #@11c
    move/from16 v0, v22

    #@11e
    if-eq v12, v0, :cond_143

    #@120
    const/4 v3, 0x1

    #@121
    .line 601
    .local v3, correct:Z
    :goto_121
    if-eqz v3, :cond_13a

    #@123
    .line 602
    move-object/from16 v0, p0

    #@125
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@127
    move-object/from16 v22, v0

    #@129
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@12c
    move-result-object v22

    #@12d
    move-object/from16 v0, v22

    #@12f
    invoke-virtual {v0, v12}, Landroid/text/method/WordIterator;->getEnd(I)I

    #@132
    move-result v12

    #@133
    .line 603
    const/16 v22, -0x1

    #@135
    move/from16 v0, v22

    #@137
    if-eq v12, v0, :cond_145

    #@139
    const/4 v3, 0x1

    #@13a
    .line 605
    :cond_13a
    :goto_13a
    if-nez v3, :cond_147

    #@13c
    .line 609
    move-object/from16 v0, p0

    #@13e
    invoke-direct {v0, v5}, Landroid/widget/SpellChecker$SpellParser;->removeRangeSpan(Landroid/text/Editable;)V

    #@141
    goto/16 :goto_ae

    #@143
    .line 600
    .end local v3           #correct:Z
    :cond_143
    const/4 v3, 0x0

    #@144
    goto :goto_121

    #@145
    .line 603
    .restart local v3       #correct:Z
    :cond_145
    const/4 v3, 0x0

    #@146
    goto :goto_13a

    #@147
    .line 614
    :cond_147
    move/from16 v15, v21

    #@149
    .line 615
    .local v15, spellCheckStart:I
    const/4 v4, 0x1

    #@14a
    .line 617
    .local v4, createSpellCheckSpan:Z
    const/4 v7, 0x0

    #@14b
    .local v7, i:I
    :goto_14b
    move-object/from16 v0, p0

    #@14d
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@14f
    move-object/from16 v22, v0

    #@151
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$600(Landroid/widget/SpellChecker;)I

    #@154
    move-result v22

    #@155
    move/from16 v0, v22

    #@157
    if-ge v7, v0, :cond_18d

    #@159
    .line 618
    move-object/from16 v0, p0

    #@15b
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@15d
    move-object/from16 v22, v0

    #@15f
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$700(Landroid/widget/SpellChecker;)[Landroid/text/style/SpellCheckSpan;

    #@162
    move-result-object v22

    #@163
    aget-object v13, v22, v7

    #@165
    .line 619
    .local v13, spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@169
    move-object/from16 v22, v0

    #@16b
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$800(Landroid/widget/SpellChecker;)[I

    #@16e
    move-result-object v22

    #@16f
    aget v22, v22, v7

    #@171
    if-ltz v22, :cond_179

    #@173
    invoke-virtual {v13}, Landroid/text/style/SpellCheckSpan;->isSpellCheckInProgress()Z

    #@176
    move-result v22

    #@177
    if-eqz v22, :cond_17c

    #@179
    .line 617
    :cond_179
    :goto_179
    add-int/lit8 v7, v7, 0x1

    #@17b
    goto :goto_14b

    #@17c
    .line 622
    :cond_17c
    invoke-interface {v5, v13}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@17f
    move-result v11

    #@180
    .line 623
    .local v11, spanStart:I
    invoke-interface {v5, v13}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@183
    move-result v10

    #@184
    .line 624
    .local v10, spanEnd:I
    if-lt v10, v15, :cond_179

    #@186
    if-lt v12, v11, :cond_179

    #@188
    .line 628
    if-gt v11, v15, :cond_1a9

    #@18a
    if-gt v12, v10, :cond_1a9

    #@18c
    .line 631
    const/4 v4, 0x0

    #@18d
    .line 651
    .end local v10           #spanEnd:I
    .end local v11           #spanStart:I
    .end local v13           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    :cond_18d
    move/from16 v0, v16

    #@18f
    if-ge v12, v0, :cond_1b5

    #@191
    .line 665
    :cond_191
    :goto_191
    invoke-static {v12, v6}, Ljava/lang/Math;->min(II)I

    #@194
    move-result v21

    #@195
    .line 734
    .end local v3           #correct:Z
    .end local v4           #createSpellCheckSpan:Z
    .end local v7           #i:I
    .end local v12           #spellCheckEnd:I
    .end local v15           #spellCheckStart:I
    :cond_195
    :goto_195
    if-eqz v9, :cond_2fd

    #@197
    .line 736
    move-object/from16 v0, p0

    #@199
    move/from16 v1, v21

    #@19b
    invoke-direct {v0, v5, v1, v6}, Landroid/widget/SpellChecker$SpellParser;->setRangeSpan(Landroid/text/Editable;II)V

    #@19e
    .line 741
    :goto_19e
    move-object/from16 v0, p0

    #@1a0
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@1a2
    move-object/from16 v22, v0

    #@1a4
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$1000(Landroid/widget/SpellChecker;)V

    #@1a7
    goto/16 :goto_ae

    #@1a9
    .line 638
    .restart local v3       #correct:Z
    .restart local v4       #createSpellCheckSpan:Z
    .restart local v7       #i:I
    .restart local v10       #spanEnd:I
    .restart local v11       #spanStart:I
    .restart local v12       #spellCheckEnd:I
    .restart local v13       #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    .restart local v15       #spellCheckStart:I
    :cond_1a9
    invoke-interface {v5, v13}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@1ac
    .line 639
    invoke-static {v11, v15}, Ljava/lang/Math;->min(II)I

    #@1af
    move-result v15

    #@1b0
    .line 640
    invoke-static {v10, v12}, Ljava/lang/Math;->max(II)I

    #@1b3
    move-result v12

    #@1b4
    goto :goto_179

    #@1b5
    .line 654
    .end local v10           #spanEnd:I
    .end local v11           #spanStart:I
    .end local v13           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    :cond_1b5
    if-gt v12, v15, :cond_1e2

    #@1b7
    .line 655
    invoke-static {}, Landroid/widget/SpellChecker;->access$300()Ljava/lang/String;

    #@1ba
    move-result-object v22

    #@1bb
    new-instance v23, Ljava/lang/StringBuilder;

    #@1bd
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@1c0
    const-string v24, "Trying to spellcheck invalid region, from "

    #@1c2
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v23

    #@1c6
    move-object/from16 v0, v23

    #@1c8
    move/from16 v1, v16

    #@1ca
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v23

    #@1ce
    const-string v24, " to "

    #@1d0
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v23

    #@1d4
    move-object/from16 v0, v23

    #@1d6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v23

    #@1da
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dd
    move-result-object v23

    #@1de
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e1
    goto :goto_191

    #@1e2
    .line 659
    :cond_1e2
    if-eqz v4, :cond_191

    #@1e4
    .line 660
    move-object/from16 v0, p0

    #@1e6
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@1e8
    move-object/from16 v22, v0

    #@1ea
    move-object/from16 v0, v22

    #@1ec
    invoke-static {v0, v5, v15, v12}, Landroid/widget/SpellChecker;->access$900(Landroid/widget/SpellChecker;Landroid/text/Editable;II)V

    #@1ef
    goto :goto_191

    #@1f0
    .line 668
    .end local v3           #correct:Z
    .end local v4           #createSpellCheckSpan:Z
    .end local v7           #i:I
    .end local v12           #spellCheckEnd:I
    .end local v15           #spellCheckStart:I
    :cond_1f0
    move/from16 v0, v21

    #@1f2
    if-gt v0, v6, :cond_195

    #@1f4
    .line 669
    move/from16 v0, v19

    #@1f6
    move/from16 v1, v16

    #@1f8
    if-lt v0, v1, :cond_280

    #@1fa
    move/from16 v0, v19

    #@1fc
    move/from16 v1, v21

    #@1fe
    if-le v0, v1, :cond_280

    #@200
    .line 670
    const/16 v22, 0x32

    #@202
    move/from16 v0, v18

    #@204
    move/from16 v1, v22

    #@206
    if-lt v0, v1, :cond_20a

    #@208
    .line 671
    const/4 v9, 0x1

    #@209
    .line 672
    goto :goto_195

    #@20a
    .line 677
    :cond_20a
    move/from16 v0, v21

    #@20c
    move/from16 v1, v16

    #@20e
    if-ge v0, v1, :cond_226

    #@210
    move/from16 v0, v19

    #@212
    move/from16 v1, v16

    #@214
    if-le v0, v1, :cond_226

    #@216
    .line 678
    move-object/from16 v0, p0

    #@218
    move/from16 v1, v16

    #@21a
    invoke-direct {v0, v5, v1, v14}, Landroid/widget/SpellChecker$SpellParser;->removeSpansAt(Landroid/text/Editable;I[Ljava/lang/Object;)V

    #@21d
    .line 679
    move-object/from16 v0, p0

    #@21f
    move/from16 v1, v16

    #@221
    move-object/from16 v2, v17

    #@223
    invoke-direct {v0, v5, v1, v2}, Landroid/widget/SpellChecker$SpellParser;->removeSpansAt(Landroid/text/Editable;I[Ljava/lang/Object;)V

    #@226
    .line 682
    :cond_226
    move/from16 v0, v21

    #@228
    if-ge v0, v6, :cond_23a

    #@22a
    move/from16 v0, v19

    #@22c
    if-le v0, v6, :cond_23a

    #@22e
    .line 683
    move-object/from16 v0, p0

    #@230
    invoke-direct {v0, v5, v6, v14}, Landroid/widget/SpellChecker$SpellParser;->removeSpansAt(Landroid/text/Editable;I[Ljava/lang/Object;)V

    #@233
    .line 684
    move-object/from16 v0, p0

    #@235
    move-object/from16 v1, v17

    #@237
    invoke-direct {v0, v5, v6, v1}, Landroid/widget/SpellChecker$SpellParser;->removeSpansAt(Landroid/text/Editable;I[Ljava/lang/Object;)V

    #@23a
    .line 688
    :cond_23a
    const/4 v4, 0x1

    #@23b
    .line 689
    .restart local v4       #createSpellCheckSpan:Z
    move/from16 v0, v19

    #@23d
    move/from16 v1, v16

    #@23f
    if-ne v0, v1, :cond_256

    #@241
    .line 690
    const/4 v7, 0x0

    #@242
    .restart local v7       #i:I
    :goto_242
    array-length v0, v14

    #@243
    move/from16 v22, v0

    #@245
    move/from16 v0, v22

    #@247
    if-ge v7, v0, :cond_256

    #@249
    .line 691
    aget-object v22, v14, v7

    #@24b
    move-object/from16 v0, v22

    #@24d
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@250
    move-result v10

    #@251
    .line 692
    .restart local v10       #spanEnd:I
    move/from16 v0, v16

    #@253
    if-ne v10, v0, :cond_2f5

    #@255
    .line 693
    const/4 v4, 0x0

    #@256
    .line 699
    .end local v7           #i:I
    .end local v10           #spanEnd:I
    :cond_256
    move/from16 v0, v21

    #@258
    if-ne v0, v6, :cond_26d

    #@25a
    .line 700
    const/4 v7, 0x0

    #@25b
    .restart local v7       #i:I
    :goto_25b
    array-length v0, v14

    #@25c
    move/from16 v22, v0

    #@25e
    move/from16 v0, v22

    #@260
    if-ge v7, v0, :cond_26d

    #@262
    .line 701
    aget-object v22, v14, v7

    #@264
    move-object/from16 v0, v22

    #@266
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@269
    move-result v11

    #@26a
    .line 702
    .restart local v11       #spanStart:I
    if-ne v11, v6, :cond_2f9

    #@26c
    .line 703
    const/4 v4, 0x0

    #@26d
    .line 709
    .end local v7           #i:I
    .end local v11           #spanStart:I
    :cond_26d
    if-eqz v4, :cond_27e

    #@26f
    .line 710
    move-object/from16 v0, p0

    #@271
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@273
    move-object/from16 v22, v0

    #@275
    move-object/from16 v0, v22

    #@277
    move/from16 v1, v21

    #@279
    move/from16 v2, v19

    #@27b
    invoke-static {v0, v5, v1, v2}, Landroid/widget/SpellChecker;->access$900(Landroid/widget/SpellChecker;Landroid/text/Editable;II)V

    #@27e
    .line 712
    :cond_27e
    add-int/lit8 v18, v18, 0x1

    #@280
    .line 716
    .end local v4           #createSpellCheckSpan:Z
    :cond_280
    move/from16 v8, v19

    #@282
    .line 717
    .local v8, originalWordEnd:I
    move-object/from16 v0, p0

    #@284
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@286
    move-object/from16 v22, v0

    #@288
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@28b
    move-result-object v22

    #@28c
    move-object/from16 v0, v22

    #@28e
    move/from16 v1, v19

    #@290
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->following(I)I

    #@293
    move-result v19

    #@294
    .line 718
    move/from16 v0, v20

    #@296
    if-ge v0, v6, :cond_2d1

    #@298
    const/16 v22, -0x1

    #@29a
    move/from16 v0, v19

    #@29c
    move/from16 v1, v22

    #@29e
    if-eq v0, v1, :cond_2a6

    #@2a0
    move/from16 v0, v19

    #@2a2
    move/from16 v1, v20

    #@2a4
    if-lt v0, v1, :cond_2d1

    #@2a6
    .line 720
    :cond_2a6
    add-int/lit16 v0, v8, 0x15e

    #@2a8
    move/from16 v22, v0

    #@2aa
    move/from16 v0, v22

    #@2ac
    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    #@2af
    move-result v20

    #@2b0
    .line 722
    move-object/from16 v0, p0

    #@2b2
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2b4
    move-object/from16 v22, v0

    #@2b6
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@2b9
    move-result-object v22

    #@2ba
    move-object/from16 v0, v22

    #@2bc
    move/from16 v1, v20

    #@2be
    invoke-virtual {v0, v5, v8, v1}, Landroid/text/method/WordIterator;->setCharSequence(Ljava/lang/CharSequence;II)V

    #@2c1
    .line 724
    move-object/from16 v0, p0

    #@2c3
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2c5
    move-object/from16 v22, v0

    #@2c7
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@2ca
    move-result-object v22

    #@2cb
    move-object/from16 v0, v22

    #@2cd
    invoke-virtual {v0, v8}, Landroid/text/method/WordIterator;->following(I)I

    #@2d0
    move-result v19

    #@2d1
    .line 726
    :cond_2d1
    const/16 v22, -0x1

    #@2d3
    move/from16 v0, v19

    #@2d5
    move/from16 v1, v22

    #@2d7
    if-eq v0, v1, :cond_195

    #@2d9
    .line 727
    move-object/from16 v0, p0

    #@2db
    iget-object v0, v0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2dd
    move-object/from16 v22, v0

    #@2df
    invoke-static/range {v22 .. v22}, Landroid/widget/SpellChecker;->access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;

    #@2e2
    move-result-object v22

    #@2e3
    move-object/from16 v0, v22

    #@2e5
    move/from16 v1, v19

    #@2e7
    invoke-virtual {v0, v1}, Landroid/text/method/WordIterator;->getBeginning(I)I

    #@2ea
    move-result v21

    #@2eb
    .line 728
    const/16 v22, -0x1

    #@2ed
    move/from16 v0, v21

    #@2ef
    move/from16 v1, v22

    #@2f1
    if-ne v0, v1, :cond_1f0

    #@2f3
    goto/16 :goto_195

    #@2f5
    .line 690
    .end local v8           #originalWordEnd:I
    .restart local v4       #createSpellCheckSpan:Z
    .restart local v7       #i:I
    .restart local v10       #spanEnd:I
    :cond_2f5
    add-int/lit8 v7, v7, 0x1

    #@2f7
    goto/16 :goto_242

    #@2f9
    .line 700
    .end local v10           #spanEnd:I
    .restart local v11       #spanStart:I
    :cond_2f9
    add-int/lit8 v7, v7, 0x1

    #@2fb
    goto/16 :goto_25b

    #@2fd
    .line 738
    .end local v4           #createSpellCheckSpan:Z
    .end local v7           #i:I
    .end local v11           #spanStart:I
    :cond_2fd
    move-object/from16 v0, p0

    #@2ff
    invoke-direct {v0, v5}, Landroid/widget/SpellChecker$SpellParser;->removeRangeSpan(Landroid/text/Editable;)V

    #@302
    goto/16 :goto_19e
.end method

.method public parse(II)V
    .registers 8
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 507
    iget-object v2, p0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2
    invoke-static {v2}, Landroid/widget/SpellChecker;->access$200(Landroid/widget/SpellChecker;)Landroid/widget/TextView;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Landroid/widget/TextView;->length()I

    #@9
    move-result v0

    #@a
    .line 509
    .local v0, max:I
    if-le p2, v0, :cond_46

    #@c
    .line 510
    invoke-static {}, Landroid/widget/SpellChecker;->access$300()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "Parse invalid region, from "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, " to "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 511
    move v1, v0

    #@31
    .line 515
    .local v1, parseEnd:I
    :goto_31
    if-le v1, p1, :cond_45

    #@33
    .line 516
    iget-object v2, p0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@35
    invoke-static {v2}, Landroid/widget/SpellChecker;->access$200(Landroid/widget/SpellChecker;)Landroid/widget/TextView;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Landroid/text/Editable;

    #@3f
    invoke-direct {p0, v2, p1, v1}, Landroid/widget/SpellChecker$SpellParser;->setRangeSpan(Landroid/text/Editable;II)V

    #@42
    .line 517
    invoke-virtual {p0}, Landroid/widget/SpellChecker$SpellParser;->parse()V

    #@45
    .line 519
    :cond_45
    return-void

    #@46
    .line 513
    .end local v1           #parseEnd:I
    :cond_46
    move v1, p2

    #@47
    .restart local v1       #parseEnd:I
    goto :goto_31
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 526
    iget-object v0, p0, Landroid/widget/SpellChecker$SpellParser;->this$0:Landroid/widget/SpellChecker;

    #@2
    invoke-static {v0}, Landroid/widget/SpellChecker;->access$200(Landroid/widget/SpellChecker;)Landroid/widget/TextView;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/text/Editable;

    #@c
    invoke-direct {p0, v0}, Landroid/widget/SpellChecker$SpellParser;->removeRangeSpan(Landroid/text/Editable;)V

    #@f
    .line 527
    return-void
.end method
