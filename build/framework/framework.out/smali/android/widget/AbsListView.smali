.class public abstract Landroid/widget/AbsListView;
.super Landroid/widget/AdapterView;
.source "AbsListView.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/Filter$FilterListener;
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;
.implements Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/AbsListView$RecycleBin;,
        Landroid/widget/AbsListView$RecyclerListener;,
        Landroid/widget/AbsListView$LayoutParams;,
        Landroid/widget/AbsListView$MultiChoiceModeWrapper;,
        Landroid/widget/AbsListView$MultiChoiceModeListener;,
        Landroid/widget/AbsListView$AdapterDataSetObserver;,
        Landroid/widget/AbsListView$PositionScroller;,
        Landroid/widget/AbsListView$FlingRunnable;,
        Landroid/widget/AbsListView$CheckForTap;,
        Landroid/widget/AbsListView$CheckForKeyLongPress;,
        Landroid/widget/AbsListView$CheckForLongPress;,
        Landroid/widget/AbsListView$PerformClick;,
        Landroid/widget/AbsListView$WindowRunnnable;,
        Landroid/widget/AbsListView$ListItemAccessibilityDelegate;,
        Landroid/widget/AbsListView$SavedState;,
        Landroid/widget/AbsListView$SelectionBoundsAdjuster;,
        Landroid/widget/AbsListView$OnScrollListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/text/TextWatcher;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Landroid/widget/Filter$FilterListener;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;",
        "Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;"
    }
.end annotation


# static fields
.field private static final CHECK_POSITION_SEARCH_DISTANCE:I = 0x14

.field public static final CHOICE_MODE_MULTIPLE:I = 0x2

.field public static final CHOICE_MODE_MULTIPLE_MODAL:I = 0x3

.field public static final CHOICE_MODE_NONE:I = 0x0

.field public static final CHOICE_MODE_SINGLE:I = 0x1

.field private static COORDINATE_DIRECTION:I = 0x0

.field private static final FLING_TIMEPASSED_CRITERIA:J = 0x64L

.field private static final FLING_VELOCITY_CRITERIA:F = 150.0f

.field private static final INVALID_POINTER:I = -0x1

.field static final LAYOUT_FORCE_BOTTOM:I = 0x3

.field static final LAYOUT_FORCE_TOP:I = 0x1

.field static final LAYOUT_MOVE_SELECTION:I = 0x6

.field static final LAYOUT_NORMAL:I = 0x0

.field static final LAYOUT_SET_SELECTION:I = 0x2

.field static final LAYOUT_SPECIFIC:I = 0x4

.field static final LAYOUT_SYNC:I = 0x5

.field static final OVERSCROLL_LIMIT_DIVISOR:I = 0x3

.field private static final PROFILE_FLINGING:Z = false

.field private static final PROFILE_SCROLLING:Z = false

.field private static final TAG:Ljava/lang/String; = "AbsListView"

.field static final TOUCH_MODE_DONE_WAITING:I = 0x2

.field static final TOUCH_MODE_DOWN:I = 0x0

.field static final TOUCH_MODE_FLING:I = 0x4

.field private static final TOUCH_MODE_OFF:I = 0x1

.field private static final TOUCH_MODE_ON:I = 0x0

.field static final TOUCH_MODE_OVERFLING:I = 0x6

.field static final TOUCH_MODE_OVERSCROLL:I = 0x5

.field static final TOUCH_MODE_REST:I = -0x1

.field static final TOUCH_MODE_SCROLL:I = 0x3

.field static final TOUCH_MODE_TAP:I = 0x1

.field private static final TOUCH_MODE_UNKNOWN:I = -0x1

.field public static final TRANSCRIPT_MODE_ALWAYS_SCROLL:I = 0x2

.field public static final TRANSCRIPT_MODE_DISABLED:I = 0x0

.field public static final TRANSCRIPT_MODE_NORMAL:I = 0x1

.field private static eventMonitor:I

.field private static mCapptouchFlickNoti:Z

.field private static moveCounter:I

.field static final sLinearInterpolator:Landroid/view/animation/Interpolator;

.field private static toCompareY:I


# instance fields
.field private final FLICKMAXVELOCITY:I

.field private final GAP_BETEEN_LAST_BEFORE:I

.field private final LIMIT_Y_GAP:I

.field private final MARK_INVERSION:I

.field private final WEIGHTED_VELOCITY:I

.field private final X_Y_VELOCITY_GAP:I

.field private final coorDirection:[I

.field private mAccessibilityDelegate:Landroid/widget/AbsListView$ListItemAccessibilityDelegate;

.field private mActivePointerId:I

.field mAdapter:Landroid/widget/ListAdapter;

.field mAdapterHasStableIds:Z

.field private mCacheColorHint:I

.field mCachingActive:Z

.field mCachingStarted:Z

.field mCheckStates:Landroid/util/SparseBooleanArray;

.field mCheckedIdStates:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mCheckedItemCount:I

.field mChoiceActionMode:Landroid/view/ActionMode;

.field mChoiceMode:I

.field private mClearScrollingCache:Ljava/lang/Runnable;

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

.field private mDefInputConnection:Landroid/view/inputmethod/InputConnection;

.field private mDeferNotifyDataSetChanged:Z

.field private mDensityScale:F

.field private mDirection:I

.field mDrawSelectorOnTop:Z

.field private mEdgeGlowBottom:Landroid/widget/EdgeEffect;

.field private mEdgeGlowTop:Landroid/widget/EdgeEffect;

.field mFastScrollEnabled:Z

.field private mFastScroller:Landroid/widget/FastScroller;

.field private mFiltered:Z

.field private mFirstPositionDistanceGuess:I

.field private mFlingProfilingStarted:Z

.field private mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

.field private mFlingStrictSpan:Landroid/os/StrictMode$Span;

.field private mForceTranscriptScroll:Z

.field private mGlobalLayoutListenerAddedFilter:Z

.field private mGlowPaddingLeft:I

.field private mGlowPaddingRight:I

.field mIsAttached:Z

.field private mIsChildViewEnabled:Z

.field final mIsScrap:[Z

.field private mLastAccessibilityScrollEventFromIndex:I

.field private mLastAccessibilityScrollEventToIndex:I

.field private mLastHandledItemCount:I

.field private mLastPositionDistanceGuess:I

.field private mLastScrollState:I

.field private mLastTouchMode:I

.field mLastY:I

.field mLayoutMode:I

.field mListPadding:Landroid/graphics/Rect;

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field mMotionCorrection:I

.field mMotionPosition:I

.field mMotionViewNewTop:I

.field mMotionViewOriginalTop:I

.field mMotionX:I

.field mMotionY:I

.field mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

.field private mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field mOverflingDistance:I

.field mOverscrollDistance:I

.field mOverscrollMax:I

.field private mPendingCheckForKeyLongPress:Landroid/widget/AbsListView$CheckForKeyLongPress;

.field private mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

.field private mPendingCheckForTap:Ljava/lang/Runnable;

.field private mPendingSync:Landroid/widget/AbsListView$SavedState;

.field private mPerformClick:Landroid/widget/AbsListView$PerformClick;

.field mPopup:Landroid/widget/PopupWindow;

.field private mPopupHidden:Z

.field mPositionScrollAfterLayout:Ljava/lang/Runnable;

.field mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

.field private mPublicInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

.field final mRecycler:Landroid/widget/AbsListView$RecycleBin;

.field private mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

.field mResurrectToPosition:I

.field mScrollDown:Landroid/view/View;

.field private mScrollProfilingStarted:Z

.field private mScrollStrictSpan:Landroid/os/StrictMode$Span;

.field mScrollUp:Landroid/view/View;

.field mScrollingCacheEnabled:Z

.field mSelectedTop:I

.field mSelectionBottomPadding:I

.field mSelectionLeftPadding:I

.field mSelectionRightPadding:I

.field mSelectionTopPadding:I

.field mSelector:Landroid/graphics/drawable/Drawable;

.field mSelectorPosition:I

.field mSelectorRect:Landroid/graphics/Rect;

.field private mSmoothScrollbarEnabled:Z

.field mStackFromBottom:Z

.field mTextFilter:Landroid/widget/EditText;

.field private mTextFilterEnabled:Z

.field private mTouchFrame:Landroid/graphics/Rect;

.field mTouchMode:I

.field private mTouchModeReset:Ljava/lang/Runnable;

.field private mTouchSlop:I

.field private mTranscriptMode:I

.field private mVelocityScale:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field mWidthMeasureSpec:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 652
    sput-boolean v1, Landroid/widget/AbsListView;->mCapptouchFlickNoti:Z

    #@3
    .line 653
    const/16 v0, 0x8

    #@5
    sput v0, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@7
    .line 661
    sput v1, Landroid/widget/AbsListView;->toCompareY:I

    #@9
    .line 662
    sput v1, Landroid/widget/AbsListView;->eventMonitor:I

    #@b
    .line 663
    sput v1, Landroid/widget/AbsListView;->moveCounter:I

    #@d
    .line 695
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    #@f
    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@12
    sput-object v0, Landroid/widget/AbsListView;->sLinearInterpolator:Landroid/view/animation/Interpolator;

    #@14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, -0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 776
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    #@7
    .line 226
    iput v2, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@9
    .line 259
    iput v2, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@b
    .line 284
    iput-boolean v2, p0, Landroid/widget/AbsListView;->mDeferNotifyDataSetChanged:Z

    #@d
    .line 289
    iput-boolean v2, p0, Landroid/widget/AbsListView;->mDrawSelectorOnTop:Z

    #@f
    .line 299
    iput v3, p0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@11
    .line 304
    new-instance v1, Landroid/graphics/Rect;

    #@13
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@16
    iput-object v1, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@18
    .line 310
    new-instance v1, Landroid/widget/AbsListView$RecycleBin;

    #@1a
    invoke-direct {v1, p0}, Landroid/widget/AbsListView$RecycleBin;-><init>(Landroid/widget/AbsListView;)V

    #@1d
    iput-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@1f
    .line 315
    iput v2, p0, Landroid/widget/AbsListView;->mSelectionLeftPadding:I

    #@21
    .line 320
    iput v2, p0, Landroid/widget/AbsListView;->mSelectionTopPadding:I

    #@23
    .line 325
    iput v2, p0, Landroid/widget/AbsListView;->mSelectionRightPadding:I

    #@25
    .line 330
    iput v2, p0, Landroid/widget/AbsListView;->mSelectionBottomPadding:I

    #@27
    .line 335
    new-instance v1, Landroid/graphics/Rect;

    #@29
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@2c
    iput-object v1, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2e
    .line 340
    iput v2, p0, Landroid/widget/AbsListView;->mWidthMeasureSpec:I

    #@30
    .line 388
    iput v3, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@32
    .line 419
    iput v2, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@34
    .line 457
    iput-boolean v4, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@36
    .line 477
    iput v3, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@38
    .line 479
    iput-object v5, p0, Landroid/widget/AbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@3a
    .line 505
    iput v3, p0, Landroid/widget/AbsListView;->mLastTouchMode:I

    #@3c
    .line 508
    iput-boolean v2, p0, Landroid/widget/AbsListView;->mScrollProfilingStarted:Z

    #@3e
    .line 511
    iput-boolean v2, p0, Landroid/widget/AbsListView;->mFlingProfilingStarted:Z

    #@40
    .line 519
    iput-object v5, p0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@42
    .line 520
    iput-object v5, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@44
    .line 567
    iput v2, p0, Landroid/widget/AbsListView;->mLastScrollState:I

    #@46
    .line 586
    const/high16 v1, 0x3f80

    #@48
    iput v1, p0, Landroid/widget/AbsListView;->mVelocityScale:F

    #@4a
    .line 588
    new-array v1, v4, [Z

    #@4c
    iput-object v1, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@4e
    .line 598
    iput v3, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@50
    .line 646
    iput v2, p0, Landroid/widget/AbsListView;->mDirection:I

    #@52
    .line 654
    const/16 v1, 0x320

    #@54
    iput v1, p0, Landroid/widget/AbsListView;->X_Y_VELOCITY_GAP:I

    #@56
    .line 655
    const/16 v1, 0x258

    #@58
    iput v1, p0, Landroid/widget/AbsListView;->WEIGHTED_VELOCITY:I

    #@5a
    .line 656
    iput v3, p0, Landroid/widget/AbsListView;->MARK_INVERSION:I

    #@5c
    .line 657
    sget v1, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@5e
    new-array v1, v1, [I

    #@60
    iput-object v1, p0, Landroid/widget/AbsListView;->coorDirection:[I

    #@62
    .line 658
    const/16 v1, 0xbb8

    #@64
    iput v1, p0, Landroid/widget/AbsListView;->FLICKMAXVELOCITY:I

    #@66
    .line 659
    const/16 v1, 0x19

    #@68
    iput v1, p0, Landroid/widget/AbsListView;->GAP_BETEEN_LAST_BEFORE:I

    #@6a
    .line 660
    const/4 v1, 0x3

    #@6b
    iput v1, p0, Landroid/widget/AbsListView;->LIMIT_Y_GAP:I

    #@6d
    .line 777
    invoke-direct {p0}, Landroid/widget/AbsListView;->initAbsListView()V

    #@70
    .line 779
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->setVerticalScrollBarEnabled(Z)V

    #@73
    .line 780
    sget-object v1, Lcom/android/internal/R$styleable;->View:[I

    #@75
    invoke-virtual {p1, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@78
    move-result-object v0

    #@79
    .line 781
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->initializeScrollbars(Landroid/content/res/TypedArray;)V

    #@7c
    .line 782
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@7f
    .line 783
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 786
    const v0, 0x101006a

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 15
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 790
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 226
    const/4 v9, 0x0

    #@4
    iput v9, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@6
    .line 259
    const/4 v9, 0x0

    #@7
    iput v9, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@9
    .line 284
    const/4 v9, 0x0

    #@a
    iput-boolean v9, p0, Landroid/widget/AbsListView;->mDeferNotifyDataSetChanged:Z

    #@c
    .line 289
    const/4 v9, 0x0

    #@d
    iput-boolean v9, p0, Landroid/widget/AbsListView;->mDrawSelectorOnTop:Z

    #@f
    .line 299
    const/4 v9, -0x1

    #@10
    iput v9, p0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@12
    .line 304
    new-instance v9, Landroid/graphics/Rect;

    #@14
    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    #@17
    iput-object v9, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@19
    .line 310
    new-instance v9, Landroid/widget/AbsListView$RecycleBin;

    #@1b
    invoke-direct {v9, p0}, Landroid/widget/AbsListView$RecycleBin;-><init>(Landroid/widget/AbsListView;)V

    #@1e
    iput-object v9, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@20
    .line 315
    const/4 v9, 0x0

    #@21
    iput v9, p0, Landroid/widget/AbsListView;->mSelectionLeftPadding:I

    #@23
    .line 320
    const/4 v9, 0x0

    #@24
    iput v9, p0, Landroid/widget/AbsListView;->mSelectionTopPadding:I

    #@26
    .line 325
    const/4 v9, 0x0

    #@27
    iput v9, p0, Landroid/widget/AbsListView;->mSelectionRightPadding:I

    #@29
    .line 330
    const/4 v9, 0x0

    #@2a
    iput v9, p0, Landroid/widget/AbsListView;->mSelectionBottomPadding:I

    #@2c
    .line 335
    new-instance v9, Landroid/graphics/Rect;

    #@2e
    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    #@31
    iput-object v9, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@33
    .line 340
    const/4 v9, 0x0

    #@34
    iput v9, p0, Landroid/widget/AbsListView;->mWidthMeasureSpec:I

    #@36
    .line 388
    const/4 v9, -0x1

    #@37
    iput v9, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@39
    .line 419
    const/4 v9, 0x0

    #@3a
    iput v9, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@3c
    .line 457
    const/4 v9, 0x1

    #@3d
    iput-boolean v9, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@3f
    .line 477
    const/4 v9, -0x1

    #@40
    iput v9, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@42
    .line 479
    const/4 v9, 0x0

    #@43
    iput-object v9, p0, Landroid/widget/AbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@45
    .line 505
    const/4 v9, -0x1

    #@46
    iput v9, p0, Landroid/widget/AbsListView;->mLastTouchMode:I

    #@48
    .line 508
    const/4 v9, 0x0

    #@49
    iput-boolean v9, p0, Landroid/widget/AbsListView;->mScrollProfilingStarted:Z

    #@4b
    .line 511
    const/4 v9, 0x0

    #@4c
    iput-boolean v9, p0, Landroid/widget/AbsListView;->mFlingProfilingStarted:Z

    #@4e
    .line 519
    const/4 v9, 0x0

    #@4f
    iput-object v9, p0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@51
    .line 520
    const/4 v9, 0x0

    #@52
    iput-object v9, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@54
    .line 567
    const/4 v9, 0x0

    #@55
    iput v9, p0, Landroid/widget/AbsListView;->mLastScrollState:I

    #@57
    .line 586
    const/high16 v9, 0x3f80

    #@59
    iput v9, p0, Landroid/widget/AbsListView;->mVelocityScale:F

    #@5b
    .line 588
    const/4 v9, 0x1

    #@5c
    new-array v9, v9, [Z

    #@5e
    iput-object v9, p0, Landroid/widget/AbsListView;->mIsScrap:[Z

    #@60
    .line 598
    const/4 v9, -0x1

    #@61
    iput v9, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@63
    .line 646
    const/4 v9, 0x0

    #@64
    iput v9, p0, Landroid/widget/AbsListView;->mDirection:I

    #@66
    .line 654
    const/16 v9, 0x320

    #@68
    iput v9, p0, Landroid/widget/AbsListView;->X_Y_VELOCITY_GAP:I

    #@6a
    .line 655
    const/16 v9, 0x258

    #@6c
    iput v9, p0, Landroid/widget/AbsListView;->WEIGHTED_VELOCITY:I

    #@6e
    .line 656
    const/4 v9, -0x1

    #@6f
    iput v9, p0, Landroid/widget/AbsListView;->MARK_INVERSION:I

    #@71
    .line 657
    sget v9, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@73
    new-array v9, v9, [I

    #@75
    iput-object v9, p0, Landroid/widget/AbsListView;->coorDirection:[I

    #@77
    .line 658
    const/16 v9, 0xbb8

    #@79
    iput v9, p0, Landroid/widget/AbsListView;->FLICKMAXVELOCITY:I

    #@7b
    .line 659
    const/16 v9, 0x19

    #@7d
    iput v9, p0, Landroid/widget/AbsListView;->GAP_BETEEN_LAST_BEFORE:I

    #@7f
    .line 660
    const/4 v9, 0x3

    #@80
    iput v9, p0, Landroid/widget/AbsListView;->LIMIT_Y_GAP:I

    #@82
    .line 791
    invoke-direct {p0}, Landroid/widget/AbsListView;->initAbsListView()V

    #@85
    .line 793
    sget-object v9, Lcom/android/internal/R$styleable;->AbsListView:[I

    #@87
    const/4 v10, 0x0

    #@88
    invoke-virtual {p1, p2, v9, p3, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@8b
    move-result-object v0

    #@8c
    .line 796
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v9, 0x0

    #@8d
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@90
    move-result-object v2

    #@91
    .line 797
    .local v2, d:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_96

    #@93
    .line 798
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    #@96
    .line 801
    :cond_96
    const/4 v9, 0x1

    #@97
    const/4 v10, 0x0

    #@98
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@9b
    move-result v9

    #@9c
    iput-boolean v9, p0, Landroid/widget/AbsListView;->mDrawSelectorOnTop:Z

    #@9e
    .line 804
    const/4 v9, 0x2

    #@9f
    const/4 v10, 0x0

    #@a0
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a3
    move-result v6

    #@a4
    .line 805
    .local v6, stackFromBottom:Z
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->setStackFromBottom(Z)V

    #@a7
    .line 807
    const/4 v9, 0x3

    #@a8
    const/4 v10, 0x1

    #@a9
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ac
    move-result v4

    #@ad
    .line 808
    .local v4, scrollingCacheEnabled:Z
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->setScrollingCacheEnabled(Z)V

    #@b0
    .line 810
    const/4 v9, 0x4

    #@b1
    const/4 v10, 0x0

    #@b2
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@b5
    move-result v8

    #@b6
    .line 811
    .local v8, useTextFilter:Z
    invoke-virtual {p0, v8}, Landroid/widget/AbsListView;->setTextFilterEnabled(Z)V

    #@b9
    .line 813
    const/4 v9, 0x5

    #@ba
    const/4 v10, 0x0

    #@bb
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    #@be
    move-result v7

    #@bf
    .line 815
    .local v7, transcriptMode:I
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    #@c2
    .line 817
    const/4 v9, 0x6

    #@c3
    const/4 v10, 0x0

    #@c4
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    #@c7
    move-result v1

    #@c8
    .line 818
    .local v1, color:I
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setCacheColorHint(I)V

    #@cb
    .line 820
    const/16 v9, 0x8

    #@cd
    const/4 v10, 0x0

    #@ce
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@d1
    move-result v3

    #@d2
    .line 821
    .local v3, enableFastScroll:Z
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    #@d5
    .line 823
    const/16 v9, 0x9

    #@d7
    const/4 v10, 0x1

    #@d8
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@db
    move-result v5

    #@dc
    .line 824
    .local v5, smoothScrollbar:Z
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->setSmoothScrollbarEnabled(Z)V

    #@df
    .line 826
    const/4 v9, 0x7

    #@e0
    const/4 v10, 0x0

    #@e1
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    #@e4
    move-result v9

    #@e5
    invoke-virtual {p0, v9}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    #@e8
    .line 827
    const/16 v9, 0xa

    #@ea
    const/4 v10, 0x0

    #@eb
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ee
    move-result v9

    #@ef
    invoke-virtual {p0, v9}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    #@f2
    .line 830
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f5
    move-result-object v9

    #@f6
    const v10, 0x2060022

    #@f9
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@fc
    move-result v9

    #@fd
    sput-boolean v9, Landroid/widget/AbsListView;->mCapptouchFlickNoti:Z

    #@ff
    .line 833
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@102
    .line 834
    return-void
.end method

.method private acceptFilter()Z
    .registers 2

    #@0
    .prologue
    .line 1787
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@2
    if-eqz v0, :cond_1a

    #@4
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@7
    move-result-object v0

    #@8
    instance-of v0, v0, Landroid/widget/Filterable;

    #@a
    if-eqz v0, :cond_1a

    #@c
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/widget/Filterable;

    #@12
    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@15
    move-result-object v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method static synthetic access$1000(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@2
    return v0
.end method

.method static synthetic access$1100(Landroid/widget/AbsListView;)Landroid/view/VelocityTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/widget/AbsListView;->mMaximumVelocity:I

    #@2
    return v0
.end method

.method static synthetic access$1300(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/widget/AbsListView;->mMinimumVelocity:I

    #@2
    return v0
.end method

.method static synthetic access$1400(Landroid/widget/AbsListView;)Landroid/os/StrictMode$Span;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Landroid/widget/AbsListView;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$1600(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$1800(Landroid/widget/AbsListView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-direct {p0}, Landroid/widget/AbsListView;->contentFits()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1900(Landroid/widget/AbsListView;)Landroid/widget/EdgeEffect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getWindowAttachCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2000(Landroid/widget/AbsListView;)Landroid/widget/EdgeEffect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Landroid/widget/AbsListView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-direct {p0}, Landroid/widget/AbsListView;->clearScrollingCache()V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mPaddingBottom:I

    #@2
    return v0
.end method

.method static synthetic access$2300(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@2
    return v0
.end method

.method static synthetic access$2400(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mPaddingBottom:I

    #@2
    return v0
.end method

.method static synthetic access$2500(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@2
    return v0
.end method

.method static synthetic access$2600(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$2700(Landroid/widget/AbsListView;IIIIIIIIZ)Z
    .registers 11
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"
    .parameter "x9"

    #@0
    .prologue
    .line 93
    invoke-virtual/range {p0 .. p9}, Landroid/widget/AbsListView;->overScrollBy(IIIIIIIIZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2800(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method static synthetic access$2900(Landroid/widget/AbsListView;IIIIIIIIZ)Z
    .registers 11
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"
    .parameter "x9"

    #@0
    .prologue
    .line 93
    invoke-virtual/range {p0 .. p9}, Landroid/widget/AbsListView;->overScrollBy(IIIIIIIIZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getWindowAttachCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3000(Landroid/widget/AbsListView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$3100(Landroid/widget/AbsListView;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->setChildrenDrawnWithCacheEnabled(Z)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Landroid/widget/AbsListView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@2
    return v0
.end method

.method static synthetic access$3300(Landroid/widget/AbsListView;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->setChildrenDrawingCacheEnabled(Z)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Landroid/widget/AbsListView;)Landroid/view/inputmethod/InputConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mDefInputConnection:Landroid/view/inputmethod/InputConnection;

    #@2
    return-object v0
.end method

.method static synthetic access$3600(Landroid/widget/AbsListView;)Landroid/widget/FastScroller;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@2
    return-object v0
.end method

.method static synthetic access$3700(Landroid/widget/AbsListView;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsListView;->removeDetachedView(Landroid/view/View;Z)V

    #@3
    return-void
.end method

.method static synthetic access$3800(Landroid/widget/AbsListView;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsListView;->removeDetachedView(Landroid/view/View;Z)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Landroid/widget/AbsListView;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsListView;->removeDetachedView(Landroid/view/View;Z)V

    #@3
    return-void
.end method

.method static synthetic access$4000(Landroid/widget/AbsListView;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsListView;->removeDetachedView(Landroid/view/View;Z)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Landroid/widget/AbsListView;Landroid/view/View;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsListView;->removeDetachedView(Landroid/view/View;Z)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/widget/AbsListView;)Landroid/widget/AbsListView$CheckForLongPress;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/widget/AbsListView;->mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Landroid/widget/AbsListView;Landroid/widget/AbsListView$CheckForLongPress;)Landroid/widget/AbsListView$CheckForLongPress;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Landroid/widget/AbsListView;->mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

    #@2
    return-object p1
.end method

.method static synthetic access$902(Landroid/widget/AbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@2
    return-object p1
.end method

.method private clearScrollingCache()V
    .registers 2

    #@0
    .prologue
    .line 5001
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isHardwareAccelerated()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_16

    #@6
    .line 5002
    iget-object v0, p0, Landroid/widget/AbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    #@8
    if-nez v0, :cond_11

    #@a
    .line 5003
    new-instance v0, Landroid/widget/AbsListView$2;

    #@c
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$2;-><init>(Landroid/widget/AbsListView;)V

    #@f
    iput-object v0, p0, Landroid/widget/AbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    #@11
    .line 5018
    :cond_11
    iget-object v0, p0, Landroid/widget/AbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    #@13
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    #@16
    .line 5020
    :cond_16
    return-void
.end method

.method private contentFits()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1216
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 1217
    .local v0, childCount:I
    if-nez v0, :cond_9

    #@8
    .line 1220
    :cond_8
    :goto_8
    return v1

    #@9
    .line 1218
    :cond_9
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@b
    if-eq v0, v3, :cond_f

    #@d
    move v1, v2

    #@e
    goto :goto_8

    #@f
    .line 1220
    :cond_f
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@16
    move-result v3

    #@17
    iget-object v4, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@19
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@1b
    if-lt v3, v4, :cond_32

    #@1d
    add-int/lit8 v3, v0, -0x1

    #@1f
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@26
    move-result v3

    #@27
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@2a
    move-result v4

    #@2b
    iget-object v5, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2d
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@2f
    sub-int/2addr v4, v5

    #@30
    if-le v3, v4, :cond_8

    #@32
    :cond_32
    move v1, v2

    #@33
    goto :goto_8
.end method

.method private createScrollingCache()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 4993
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mScrollingCacheEnabled:Z

    #@3
    if-eqz v0, :cond_19

    #@5
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@7
    if-nez v0, :cond_19

    #@9
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isHardwareAccelerated()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_19

    #@f
    .line 4994
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setChildrenDrawnWithCacheEnabled(Z)V

    #@12
    .line 4995
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setChildrenDrawingCacheEnabled(Z)V

    #@15
    .line 4996
    iput-boolean v1, p0, Landroid/widget/AbsListView;->mCachingActive:Z

    #@17
    iput-boolean v1, p0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@19
    .line 4998
    :cond_19
    return-void
.end method

.method private createTextFilter(Z)V
    .registers 10
    .parameter "animateEntrance"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, -0x2

    #@3
    .line 5859
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@5
    if-nez v3, :cond_5b

    #@7
    .line 5860
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    .line 5861
    .local v0, c:Landroid/content/Context;
    new-instance v2, Landroid/widget/PopupWindow;

    #@d
    invoke-direct {v2, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    #@10
    .line 5862
    .local v2, p:Landroid/widget/PopupWindow;
    const-string/jumbo v3, "layout_inflater"

    #@13
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Landroid/view/LayoutInflater;

    #@19
    .line 5864
    .local v1, layoutInflater:Landroid/view/LayoutInflater;
    const v3, 0x10900e4

    #@1c
    invoke-virtual {v1, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Landroid/widget/EditText;

    #@22
    iput-object v3, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@24
    .line 5869
    iget-object v3, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@26
    const/16 v4, 0xb1

    #@28
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setRawInputType(I)V

    #@2b
    .line 5871
    iget-object v3, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@2d
    const/high16 v4, 0x1000

    #@2f
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    #@32
    .line 5872
    iget-object v3, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@34
    invoke-virtual {v3, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@37
    .line 5873
    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    #@3a
    .line 5874
    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    #@3d
    .line 5875
    const/4 v3, 0x2

    #@3e
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    #@41
    .line 5876
    iget-object v3, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@43
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@46
    .line 5877
    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@49
    .line 5878
    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@4c
    .line 5879
    invoke-virtual {v2, v7}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@4f
    .line 5880
    iput-object v2, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@51
    .line 5881
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@58
    .line 5882
    const/4 v3, 0x1

    #@59
    iput-boolean v3, p0, Landroid/widget/AbsListView;->mGlobalLayoutListenerAddedFilter:Z

    #@5b
    .line 5884
    .end local v0           #c:Landroid/content/Context;
    .end local v1           #layoutInflater:Landroid/view/LayoutInflater;
    .end local v2           #p:Landroid/widget/PopupWindow;
    :cond_5b
    if-eqz p1, :cond_66

    #@5d
    .line 5885
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@5f
    const v4, 0x10301e5

    #@62
    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    #@65
    .line 5889
    :goto_65
    return-void

    #@66
    .line 5887
    :cond_66
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@68
    const v4, 0x10301e6

    #@6b
    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    #@6e
    goto :goto_65
.end method

.method private dismissPopup()V
    .registers 2

    #@0
    .prologue
    .line 5621
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 5622
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@9
    .line 5624
    :cond_9
    return-void
.end method

.method private drawSelector(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 2436
    iget-object v1, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@2
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_12

    #@8
    .line 2437
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@a
    .line 2438
    .local v0, selector:Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@c
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@f
    .line 2439
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@12
    .line 2441
    .end local v0           #selector:Landroid/graphics/drawable/Drawable;
    :cond_12
    return-void
.end method

.method private finishGlows()V
    .registers 2

    #@0
    .prologue
    .line 6090
    iget-object v0, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 6091
    iget-object v0, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@6
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    #@9
    .line 6092
    iget-object v0, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@b
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    #@e
    .line 6094
    :cond_e
    return-void
.end method

.method static getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .registers 11
    .parameter "source"
    .parameter "dest"
    .parameter "direction"

    #@0
    .prologue
    .line 5667
    sparse-switch p2, :sswitch_data_a2

    #@3
    .line 5700
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v7, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    #@7
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v6

    #@b
    .line 5669
    :sswitch_b
    iget v4, p0, Landroid/graphics/Rect;->right:I

    #@d
    .line 5670
    .local v4, sX:I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    #@f
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    #@12
    move-result v7

    #@13
    div-int/lit8 v7, v7, 0x2

    #@15
    add-int v5, v6, v7

    #@17
    .line 5671
    .local v5, sY:I
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@19
    .line 5672
    .local v0, dX:I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@1b
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@1e
    move-result v7

    #@1f
    div-int/lit8 v7, v7, 0x2

    #@21
    add-int v1, v6, v7

    #@23
    .line 5704
    .local v1, dY:I
    :goto_23
    sub-int v2, v0, v4

    #@25
    .line 5705
    .local v2, deltaX:I
    sub-int v3, v1, v5

    #@27
    .line 5706
    .local v3, deltaY:I
    mul-int v6, v3, v3

    #@29
    mul-int v7, v2, v2

    #@2b
    add-int/2addr v6, v7

    #@2c
    return v6

    #@2d
    .line 5675
    .end local v0           #dX:I
    .end local v1           #dY:I
    .end local v2           #deltaX:I
    .end local v3           #deltaY:I
    .end local v4           #sX:I
    .end local v5           #sY:I
    :sswitch_2d
    iget v6, p0, Landroid/graphics/Rect;->left:I

    #@2f
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    #@32
    move-result v7

    #@33
    div-int/lit8 v7, v7, 0x2

    #@35
    add-int v4, v6, v7

    #@37
    .line 5676
    .restart local v4       #sX:I
    iget v5, p0, Landroid/graphics/Rect;->bottom:I

    #@39
    .line 5677
    .restart local v5       #sY:I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    #@3b
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@3e
    move-result v7

    #@3f
    div-int/lit8 v7, v7, 0x2

    #@41
    add-int v0, v6, v7

    #@43
    .line 5678
    .restart local v0       #dX:I
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@45
    .line 5679
    .restart local v1       #dY:I
    goto :goto_23

    #@46
    .line 5681
    .end local v0           #dX:I
    .end local v1           #dY:I
    .end local v4           #sX:I
    .end local v5           #sY:I
    :sswitch_46
    iget v4, p0, Landroid/graphics/Rect;->left:I

    #@48
    .line 5682
    .restart local v4       #sX:I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    #@4a
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    #@4d
    move-result v7

    #@4e
    div-int/lit8 v7, v7, 0x2

    #@50
    add-int v5, v6, v7

    #@52
    .line 5683
    .restart local v5       #sY:I
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@54
    .line 5684
    .restart local v0       #dX:I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@56
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@59
    move-result v7

    #@5a
    div-int/lit8 v7, v7, 0x2

    #@5c
    add-int v1, v6, v7

    #@5e
    .line 5685
    .restart local v1       #dY:I
    goto :goto_23

    #@5f
    .line 5687
    .end local v0           #dX:I
    .end local v1           #dY:I
    .end local v4           #sX:I
    .end local v5           #sY:I
    :sswitch_5f
    iget v6, p0, Landroid/graphics/Rect;->left:I

    #@61
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    #@64
    move-result v7

    #@65
    div-int/lit8 v7, v7, 0x2

    #@67
    add-int v4, v6, v7

    #@69
    .line 5688
    .restart local v4       #sX:I
    iget v5, p0, Landroid/graphics/Rect;->top:I

    #@6b
    .line 5689
    .restart local v5       #sY:I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    #@6d
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@70
    move-result v7

    #@71
    div-int/lit8 v7, v7, 0x2

    #@73
    add-int v0, v6, v7

    #@75
    .line 5690
    .restart local v0       #dX:I
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    #@77
    .line 5691
    .restart local v1       #dY:I
    goto :goto_23

    #@78
    .line 5694
    .end local v0           #dX:I
    .end local v1           #dY:I
    .end local v4           #sX:I
    .end local v5           #sY:I
    :sswitch_78
    iget v6, p0, Landroid/graphics/Rect;->right:I

    #@7a
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    #@7d
    move-result v7

    #@7e
    div-int/lit8 v7, v7, 0x2

    #@80
    add-int v4, v6, v7

    #@82
    .line 5695
    .restart local v4       #sX:I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    #@84
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    #@87
    move-result v7

    #@88
    div-int/lit8 v7, v7, 0x2

    #@8a
    add-int v5, v6, v7

    #@8c
    .line 5696
    .restart local v5       #sY:I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    #@8e
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@91
    move-result v7

    #@92
    div-int/lit8 v7, v7, 0x2

    #@94
    add-int v0, v6, v7

    #@96
    .line 5697
    .restart local v0       #dX:I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@98
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@9b
    move-result v7

    #@9c
    div-int/lit8 v7, v7, 0x2

    #@9e
    add-int v1, v6, v7

    #@a0
    .line 5698
    .restart local v1       #dY:I
    goto :goto_23

    #@a1
    .line 5667
    nop

    #@a2
    :sswitch_data_a2
    .sparse-switch
        0x1 -> :sswitch_78
        0x2 -> :sswitch_78
        0x11 -> :sswitch_46
        0x21 -> :sswitch_5f
        0x42 -> :sswitch_b
        0x82 -> :sswitch_2d
    .end sparse-switch
.end method

.method private getFlickValue(II)I
    .registers 23
    .parameter "vectorVelocity"
    .parameter "initialXVelocity"

    #@0
    .prologue
    .line 3273
    sget v17, Landroid/widget/AbsListView;->eventMonitor:I

    #@2
    add-int/lit8 v17, v17, -0x1

    #@4
    sput v17, Landroid/widget/AbsListView;->eventMonitor:I

    #@6
    .line 3274
    move/from16 v0, p1

    #@8
    int-to-float v0, v0

    #@9
    move/from16 v17, v0

    #@b
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@e
    move-result v17

    #@f
    move/from16 v0, v17

    #@11
    float-to-int v14, v0

    #@12
    .line 3277
    .local v14, signal:I
    const/4 v6, 0x0

    #@13
    .line 3278
    .local v6, directionUPCount:I
    const/4 v5, 0x0

    #@14
    .line 3279
    .local v5, directionDownCount:I
    const/16 v16, 0x0

    #@16
    .line 3280
    .local v16, up:Z
    const/4 v7, 0x0

    #@17
    .line 3284
    .local v7, down:Z
    sget v17, Landroid/widget/AbsListView;->moveCounter:I

    #@19
    sget v18, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@1b
    move/from16 v0, v17

    #@1d
    move/from16 v1, v18

    #@1f
    if-lt v0, v1, :cond_7b

    #@21
    sget v2, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@23
    .line 3286
    .local v2, arrayCount:I
    :goto_23
    sget v17, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@25
    add-int/lit8 v8, v17, -0x1

    #@27
    .line 3287
    .local v8, forBitOperation:I
    const/4 v13, 0x0

    #@28
    .line 3288
    .local v13, rememberFirstSignal:I
    const/4 v4, 0x0

    #@29
    .line 3289
    .local v4, calcFactor:I
    sget v17, Landroid/widget/AbsListView;->moveCounter:I

    #@2b
    sget v18, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@2d
    move/from16 v0, v17

    #@2f
    move/from16 v1, v18

    #@31
    if-le v0, v1, :cond_37

    #@33
    .line 3292
    sget v17, Landroid/widget/AbsListView;->eventMonitor:I

    #@35
    add-int v4, v17, v2

    #@37
    .line 3295
    :cond_37
    const/4 v12, 0x0

    #@38
    .line 3296
    .local v12, keepSignal:Z
    const/4 v9, 0x0

    #@39
    .line 3297
    .local v9, gotSignal:I
    const/4 v15, 0x0

    #@3a
    .line 3298
    .local v15, sumDeltaY:I
    const/4 v11, 0x0

    #@3b
    .line 3308
    .local v11, ifFindFirstSignal:Z
    const/4 v10, 0x1

    #@3c
    .local v10, i:I
    :goto_3c
    if-ge v10, v2, :cond_f2

    #@3e
    .line 3310
    if-nez v4, :cond_7e

    #@40
    .line 3311
    add-int/lit8 v3, v10, -0x1

    #@42
    .line 3317
    .local v3, arrayIndex:I
    :goto_42
    move-object/from16 v0, p0

    #@44
    iget-object v0, v0, Landroid/widget/AbsListView;->coorDirection:[I

    #@46
    move-object/from16 v17, v0

    #@48
    and-int v18, v3, v8

    #@4a
    aget v17, v17, v18

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Landroid/widget/AbsListView;->coorDirection:[I

    #@50
    move-object/from16 v18, v0

    #@52
    add-int/lit8 v19, v3, 0x1

    #@54
    and-int v19, v19, v8

    #@56
    aget v18, v18, v19

    #@58
    sub-int v9, v17, v18

    #@5a
    .line 3318
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@5d
    move-result v17

    #@5e
    add-int v15, v15, v17

    #@60
    .line 3320
    int-to-float v0, v9

    #@61
    move/from16 v17, v0

    #@63
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@66
    move-result v17

    #@67
    const/16 v18, 0x0

    #@69
    cmpg-float v17, v17, v18

    #@6b
    if-gez v17, :cond_81

    #@6d
    .line 3321
    add-int/lit8 v5, v5, 0x1

    #@6f
    .line 3326
    :cond_6f
    :goto_6f
    if-eqz v9, :cond_96

    #@71
    if-nez v11, :cond_96

    #@73
    .line 3327
    const/4 v11, 0x1

    #@74
    .line 3328
    move v13, v9

    #@75
    .line 3330
    if-gez v13, :cond_91

    #@77
    .line 3331
    const/4 v7, 0x1

    #@78
    .line 3308
    :cond_78
    :goto_78
    add-int/lit8 v10, v10, 0x1

    #@7a
    goto :goto_3c

    #@7b
    .line 3284
    .end local v2           #arrayCount:I
    .end local v3           #arrayIndex:I
    .end local v4           #calcFactor:I
    .end local v8           #forBitOperation:I
    .end local v9           #gotSignal:I
    .end local v10           #i:I
    .end local v11           #ifFindFirstSignal:Z
    .end local v12           #keepSignal:Z
    .end local v13           #rememberFirstSignal:I
    .end local v15           #sumDeltaY:I
    :cond_7b
    sget v2, Landroid/widget/AbsListView;->moveCounter:I

    #@7d
    goto :goto_23

    #@7e
    .line 3313
    .restart local v2       #arrayCount:I
    .restart local v4       #calcFactor:I
    .restart local v8       #forBitOperation:I
    .restart local v9       #gotSignal:I
    .restart local v10       #i:I
    .restart local v11       #ifFindFirstSignal:Z
    .restart local v12       #keepSignal:Z
    .restart local v13       #rememberFirstSignal:I
    .restart local v15       #sumDeltaY:I
    :cond_7e
    add-int v3, v4, v10

    #@80
    .restart local v3       #arrayIndex:I
    goto :goto_42

    #@81
    .line 3322
    :cond_81
    int-to-float v0, v9

    #@82
    move/from16 v17, v0

    #@84
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@87
    move-result v17

    #@88
    const/16 v18, 0x0

    #@8a
    cmpl-float v17, v17, v18

    #@8c
    if-lez v17, :cond_6f

    #@8e
    .line 3323
    add-int/lit8 v6, v6, 0x1

    #@90
    goto :goto_6f

    #@91
    .line 3332
    :cond_91
    if-lez v13, :cond_78

    #@93
    .line 3333
    const/16 v16, 0x1

    #@95
    goto :goto_78

    #@96
    .line 3336
    :cond_96
    if-eqz v9, :cond_78

    #@98
    int-to-float v0, v9

    #@99
    move/from16 v17, v0

    #@9b
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@9e
    move-result v17

    #@9f
    move/from16 v0, v17

    #@a1
    float-to-int v0, v0

    #@a2
    move/from16 v17, v0

    #@a4
    int-to-float v0, v13

    #@a5
    move/from16 v18, v0

    #@a7
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    #@aa
    move-result v18

    #@ab
    move/from16 v0, v18

    #@ad
    float-to-int v0, v0

    #@ae
    move/from16 v18, v0

    #@b0
    move/from16 v0, v17

    #@b2
    move/from16 v1, v18

    #@b4
    if-eq v0, v1, :cond_78

    #@b6
    .line 3337
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    #@b9
    move-result v17

    #@ba
    const/high16 v18, 0x41c8

    #@bc
    move-object/from16 v0, p0

    #@be
    iget v0, v0, Landroid/widget/AbsListView;->mDensityScale:F

    #@c0
    move/from16 v19, v0

    #@c2
    mul-float v18, v18, v19

    #@c4
    move/from16 v0, v18

    #@c6
    float-to-int v0, v0

    #@c7
    move/from16 v18, v0

    #@c9
    move/from16 v0, v17

    #@cb
    move/from16 v1, v18

    #@cd
    if-lt v0, v1, :cond_d0

    #@cf
    .line 3338
    const/4 v12, 0x1

    #@d0
    .line 3340
    :cond_d0
    add-int/lit8 v17, v2, -0x1

    #@d2
    move/from16 v0, v17

    #@d4
    if-ne v10, v0, :cond_78

    #@d6
    .line 3341
    add-int/lit8 v17, v2, -0x1

    #@d8
    div-int v17, v15, v17

    #@da
    const/high16 v18, 0x4040

    #@dc
    move-object/from16 v0, p0

    #@de
    iget v0, v0, Landroid/widget/AbsListView;->mDensityScale:F

    #@e0
    move/from16 v19, v0

    #@e2
    mul-float v18, v18, v19

    #@e4
    move/from16 v0, v18

    #@e6
    float-to-int v0, v0

    #@e7
    move/from16 v18, v0

    #@e9
    move/from16 v0, v17

    #@eb
    move/from16 v1, v18

    #@ed
    if-gt v0, v1, :cond_78

    #@ef
    .line 3342
    move/from16 p2, p1

    #@f1
    goto :goto_78

    #@f2
    .line 3355
    .end local v3           #arrayIndex:I
    :cond_f2
    if-nez v12, :cond_fc

    #@f4
    .line 3356
    if-le v5, v6, :cond_191

    #@f6
    .line 3357
    if-eqz p1, :cond_fc

    #@f8
    if-gez v14, :cond_fc

    #@fa
    .line 3358
    mul-int/lit8 p1, p1, -0x1

    #@fc
    .line 3376
    :cond_fc
    :goto_fc
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    #@ff
    move-result v17

    #@100
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    #@103
    move-result v18

    #@104
    move/from16 v0, v17

    #@106
    move/from16 v1, v18

    #@108
    if-ge v0, v1, :cond_175

    #@10a
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    #@10d
    move-result v17

    #@10e
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    #@111
    move-result v18

    #@112
    sub-int v17, v17, v18

    #@114
    const/16 v18, 0x320

    #@116
    move/from16 v0, v17

    #@118
    move/from16 v1, v18

    #@11a
    if-le v0, v1, :cond_175

    #@11c
    .line 3377
    move/from16 v0, p1

    #@11e
    int-to-float v0, v0

    #@11f
    move/from16 v17, v0

    #@121
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@124
    move-result v17

    #@125
    move/from16 v0, v17

    #@127
    float-to-int v14, v0

    #@128
    .line 3378
    mul-int v17, p1, p1

    #@12a
    mul-int v18, p2, p2

    #@12c
    add-int v17, v17, v18

    #@12e
    move/from16 v0, v17

    #@130
    int-to-double v0, v0

    #@131
    move-wide/from16 v17, v0

    #@133
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sqrt(D)D

    #@136
    move-result-wide v17

    #@137
    move-wide/from16 v0, v17

    #@139
    double-to-int v0, v0

    #@13a
    move/from16 p1, v0

    #@13c
    .line 3379
    const/high16 v17, 0x4416

    #@13e
    move-object/from16 v0, p0

    #@140
    iget v0, v0, Landroid/widget/AbsListView;->mDensityScale:F

    #@142
    move/from16 v18, v0

    #@144
    div-float v17, v17, v18

    #@146
    move/from16 v0, v17

    #@148
    float-to-int v0, v0

    #@149
    move/from16 v17, v0

    #@14b
    add-int p1, p1, v17

    #@14d
    .line 3380
    const v17, 0x453b8000

    #@150
    move-object/from16 v0, p0

    #@152
    iget v0, v0, Landroid/widget/AbsListView;->mDensityScale:F

    #@154
    move/from16 v18, v0

    #@156
    mul-float v17, v17, v18

    #@158
    move/from16 v0, v17

    #@15a
    float-to-int v0, v0

    #@15b
    move/from16 v17, v0

    #@15d
    move/from16 v0, p1

    #@15f
    move/from16 v1, v17

    #@161
    if-lt v0, v1, :cond_173

    #@163
    .line 3381
    const v17, 0x453b8000

    #@166
    move-object/from16 v0, p0

    #@168
    iget v0, v0, Landroid/widget/AbsListView;->mDensityScale:F

    #@16a
    move/from16 v18, v0

    #@16c
    mul-float v17, v17, v18

    #@16e
    move/from16 v0, v17

    #@170
    float-to-int v0, v0

    #@171
    move/from16 p1, v0

    #@173
    .line 3383
    :cond_173
    mul-int p1, p1, v14

    #@175
    .line 3385
    :cond_175
    const/16 v17, 0x0

    #@177
    sput v17, Landroid/widget/AbsListView;->eventMonitor:I

    #@179
    .line 3386
    const/16 v17, 0x0

    #@17b
    sput v17, Landroid/widget/AbsListView;->moveCounter:I

    #@17d
    .line 3387
    const/4 v10, 0x0

    #@17e
    :goto_17e
    sget v17, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@180
    move/from16 v0, v17

    #@182
    if-ge v10, v0, :cond_1ab

    #@184
    .line 3388
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Landroid/widget/AbsListView;->coorDirection:[I

    #@188
    move-object/from16 v17, v0

    #@18a
    const/16 v18, 0x0

    #@18c
    aput v18, v17, v10

    #@18e
    .line 3387
    add-int/lit8 v10, v10, 0x1

    #@190
    goto :goto_17e

    #@191
    .line 3362
    :cond_191
    if-ge v5, v6, :cond_19b

    #@193
    .line 3363
    if-eqz p1, :cond_fc

    #@195
    if-lez v14, :cond_fc

    #@197
    .line 3364
    mul-int/lit8 p1, p1, -0x1

    #@199
    goto/16 :goto_fc

    #@19b
    .line 3369
    :cond_19b
    if-eqz v7, :cond_1a3

    #@19d
    if-gez v14, :cond_1a3

    #@19f
    .line 3370
    mul-int/lit8 p1, p1, -0x1

    #@1a1
    goto/16 :goto_fc

    #@1a3
    .line 3371
    :cond_1a3
    if-eqz v16, :cond_fc

    #@1a5
    if-lez v14, :cond_fc

    #@1a7
    .line 3372
    mul-int/lit8 p1, p1, -0x1

    #@1a9
    goto/16 :goto_fc

    #@1ab
    .line 3390
    :cond_1ab
    return p1
.end method

.method private initAbsListView()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 838
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setClickable(Z)V

    #@5
    .line 839
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setFocusableInTouchMode(Z)V

    #@8
    .line 840
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setWillNotDraw(Z)V

    #@b
    .line 841
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setAlwaysDrawnWithCacheEnabled(Z)V

    #@e
    .line 842
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setScrollingCacheEnabled(Z)V

    #@11
    .line 844
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@13
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@16
    move-result-object v0

    #@17
    .line 845
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@1a
    move-result v1

    #@1b
    iput v1, p0, Landroid/widget/AbsListView;->mTouchSlop:I

    #@1d
    .line 846
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@20
    move-result v1

    #@21
    iput v1, p0, Landroid/widget/AbsListView;->mMinimumVelocity:I

    #@23
    .line 847
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@26
    move-result v1

    #@27
    iput v1, p0, Landroid/widget/AbsListView;->mMaximumVelocity:I

    #@29
    .line 848
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Landroid/widget/AbsListView;->mOverscrollDistance:I

    #@2f
    .line 849
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    #@32
    move-result v1

    #@33
    iput v1, p0, Landroid/widget/AbsListView;->mOverflingDistance:I

    #@35
    .line 851
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@40
    move-result-object v1

    #@41
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    #@43
    iput v1, p0, Landroid/widget/AbsListView;->mDensityScale:F

    #@45
    .line 852
    return-void
.end method

.method private initOrResetVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 3924
    iget-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 3925
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 3929
    :goto_a
    return-void

    #@b
    .line 3927
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@d
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    #@10
    goto :goto_a
.end method

.method private initVelocityTrackerIfNotExists()V
    .registers 2

    #@0
    .prologue
    .line 3932
    iget-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 3933
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 3935
    :cond_a
    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 8
    .parameter "ev"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4051
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@4
    move-result v4

    #@5
    const v5, 0xff00

    #@8
    and-int/2addr v4, v5

    #@9
    shr-int/lit8 v2, v4, 0x8

    #@b
    .line 4053
    .local v2, pointerIndex:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@e
    move-result v1

    #@f
    .line 4054
    .local v1, pointerId:I
    iget v4, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@11
    if-ne v1, v4, :cond_2c

    #@13
    .line 4058
    if-nez v2, :cond_2d

    #@15
    const/4 v0, 0x1

    #@16
    .line 4059
    .local v0, newPointerIndex:I
    :goto_16
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@19
    move-result v4

    #@1a
    float-to-int v4, v4

    #@1b
    iput v4, p0, Landroid/widget/AbsListView;->mMotionX:I

    #@1d
    .line 4060
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@20
    move-result v4

    #@21
    float-to-int v4, v4

    #@22
    iput v4, p0, Landroid/widget/AbsListView;->mMotionY:I

    #@24
    .line 4061
    iput v3, p0, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@26
    .line 4062
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@29
    move-result v3

    #@2a
    iput v3, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@2c
    .line 4064
    .end local v0           #newPointerIndex:I
    :cond_2c
    return-void

    #@2d
    :cond_2d
    move v0, v3

    #@2e
    .line 4058
    goto :goto_16
.end method

.method private positionPopup()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, -0x1

    #@2
    .line 5640
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@9
    move-result-object v3

    #@a
    iget v1, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@c
    .line 5641
    .local v1, screenHeight:I
    const/4 v3, 0x2

    #@d
    new-array v2, v3, [I

    #@f
    .line 5642
    .local v2, xy:[I
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->getLocationOnScreen([I)V

    #@12
    .line 5645
    const/4 v3, 0x1

    #@13
    aget v3, v2, v3

    #@15
    sub-int v3, v1, v3

    #@17
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@1a
    move-result v4

    #@1b
    sub-int/2addr v3, v4

    #@1c
    iget v4, p0, Landroid/widget/AbsListView;->mDensityScale:F

    #@1e
    const/high16 v5, 0x41a0

    #@20
    mul-float/2addr v4, v5

    #@21
    float-to-int v4, v4

    #@22
    add-int v0, v3, v4

    #@24
    .line 5646
    .local v0, bottomGap:I
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@26
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    #@29
    move-result v3

    #@2a
    if-nez v3, :cond_36

    #@2c
    .line 5647
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@2e
    const/16 v4, 0x51

    #@30
    aget v5, v2, v7

    #@32
    invoke-virtual {v3, p0, v4, v5, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@35
    .line 5652
    :goto_35
    return-void

    #@36
    .line 5650
    :cond_36
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@38
    aget v4, v2, v7

    #@3a
    invoke-virtual {v3, v4, v0, v6, v6}, Landroid/widget/PopupWindow;->update(IIII)V

    #@3d
    goto :goto_35
.end method

.method private positionSelector(IIII)V
    .registers 10
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 2337
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@2
    iget v1, p0, Landroid/widget/AbsListView;->mSelectionLeftPadding:I

    #@4
    sub-int v1, p1, v1

    #@6
    iget v2, p0, Landroid/widget/AbsListView;->mSelectionTopPadding:I

    #@8
    sub-int v2, p2, v2

    #@a
    iget v3, p0, Landroid/widget/AbsListView;->mSelectionRightPadding:I

    #@c
    add-int/2addr v3, p3

    #@d
    iget v4, p0, Landroid/widget/AbsListView;->mSelectionBottomPadding:I

    #@f
    add-int/2addr v4, p4

    #@10
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@13
    .line 2339
    return-void
.end method

.method private recycleVelocityTracker()V
    .registers 2

    #@0
    .prologue
    .line 3938
    iget-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 3939
    iget-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@9
    .line 3940
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c
    .line 3942
    :cond_c
    return-void
.end method

.method static retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;
    .registers 6
    .parameter
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 6752
    .local p0, scrapViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v1

    #@4
    .line 6753
    .local v1, size:I
    if-lez v1, :cond_2a

    #@6
    .line 6755
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_20

    #@9
    .line 6756
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Landroid/view/View;

    #@f
    .line 6757
    .local v2, view:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Landroid/widget/AbsListView$LayoutParams;

    #@15
    iget v3, v3, Landroid/widget/AbsListView$LayoutParams;->scrappedFromPosition:I

    #@17
    if-ne v3, p1, :cond_1d

    #@19
    .line 6759
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1c
    .line 6765
    .end local v0           #i:I
    .end local v2           #view:Landroid/view/View;
    :goto_1c
    return-object v2

    #@1d
    .line 6755
    .restart local v0       #i:I
    .restart local v2       #view:Landroid/view/View;
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 6763
    .end local v2           #view:Landroid/view/View;
    :cond_20
    add-int/lit8 v3, v1, -0x1

    #@22
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Landroid/view/View;

    #@28
    move-object v2, v3

    #@29
    goto :goto_1c

    #@2a
    .line 6765
    .end local v0           #i:I
    :cond_2a
    const/4 v2, 0x0

    #@2b
    goto :goto_1c
.end method

.method private scrollIfNeeded(I)V
    .registers 32
    .parameter "y"

    #@0
    .prologue
    .line 3073
    move-object/from16 v0, p0

    #@2
    iget v3, v0, Landroid/widget/AbsListView;->mMotionY:I

    #@4
    sub-int v29, p1, v3

    #@6
    .line 3074
    .local v29, rawDeltaY:I
    move-object/from16 v0, p0

    #@8
    iget v3, v0, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@a
    sub-int v17, v29, v3

    #@c
    .line 3075
    .local v17, deltaY:I
    move-object/from16 v0, p0

    #@e
    iget v3, v0, Landroid/widget/AbsListView;->mLastY:I

    #@10
    const/high16 v4, -0x8000

    #@12
    if-eq v3, v4, :cond_126

    #@14
    move-object/from16 v0, p0

    #@16
    iget v3, v0, Landroid/widget/AbsListView;->mLastY:I

    #@18
    sub-int v18, p1, v3

    #@1a
    .line 3077
    .local v18, incrementalDeltaY:I
    :goto_1a
    move-object/from16 v0, p0

    #@1c
    iget v3, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@1e
    const/4 v4, 0x3

    #@1f
    if-ne v3, v4, :cond_162

    #@21
    .line 3085
    move-object/from16 v0, p0

    #@23
    iget-object v3, v0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@25
    if-nez v3, :cond_31

    #@27
    .line 3087
    const-string v3, "AbsListView-scroll"

    #@29
    invoke-static {v3}, Landroid/os/StrictMode;->enterCriticalSpan(Ljava/lang/String;)Landroid/os/StrictMode$Span;

    #@2c
    move-result-object v3

    #@2d
    move-object/from16 v0, p0

    #@2f
    iput-object v3, v0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@31
    .line 3090
    :cond_31
    move-object/from16 v0, p0

    #@33
    iget v3, v0, Landroid/widget/AbsListView;->mLastY:I

    #@35
    move/from16 v0, p1

    #@37
    if-eq v0, v3, :cond_125

    #@39
    .line 3094
    move-object/from16 v0, p0

    #@3b
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@3d
    const/high16 v4, 0x8

    #@3f
    and-int/2addr v3, v4

    #@40
    if-nez v3, :cond_58

    #@42
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    #@45
    move-result v3

    #@46
    move-object/from16 v0, p0

    #@48
    iget v4, v0, Landroid/widget/AbsListView;->mTouchSlop:I

    #@4a
    if-le v3, v4, :cond_58

    #@4c
    .line 3096
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getParent()Landroid/view/ViewParent;

    #@4f
    move-result-object v28

    #@50
    .line 3097
    .local v28, parent:Landroid/view/ViewParent;
    if-eqz v28, :cond_58

    #@52
    .line 3098
    const/4 v3, 0x1

    #@53
    move-object/from16 v0, v28

    #@55
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@58
    .line 3103
    .end local v28           #parent:Landroid/view/ViewParent;
    :cond_58
    move-object/from16 v0, p0

    #@5a
    iget v3, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@5c
    if-ltz v3, :cond_12a

    #@5e
    .line 3104
    move-object/from16 v0, p0

    #@60
    iget v3, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@62
    move-object/from16 v0, p0

    #@64
    iget v4, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@66
    sub-int v19, v3, v4

    #@68
    .line 3111
    .local v19, motionIndex:I
    :goto_68
    const/16 v22, 0x0

    #@6a
    .line 3112
    .local v22, motionViewPrevTop:I
    move-object/from16 v0, p0

    #@6c
    move/from16 v1, v19

    #@6e
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@71
    move-result-object v21

    #@72
    .line 3113
    .local v21, motionView:Landroid/view/View;
    if-eqz v21, :cond_78

    #@74
    .line 3114
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getTop()I

    #@77
    move-result v22

    #@78
    .line 3118
    :cond_78
    const/16 v16, 0x0

    #@7a
    .line 3119
    .local v16, atEdge:Z
    if-eqz v18, :cond_86

    #@7c
    .line 3120
    move-object/from16 v0, p0

    #@7e
    move/from16 v1, v17

    #@80
    move/from16 v2, v18

    #@82
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->trackMotionScroll(II)Z

    #@85
    move-result v16

    #@86
    .line 3124
    :cond_86
    move-object/from16 v0, p0

    #@88
    move/from16 v1, v19

    #@8a
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@8d
    move-result-object v21

    #@8e
    .line 3125
    if-eqz v21, :cond_11f

    #@90
    .line 3128
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getTop()I

    #@93
    move-result v23

    #@94
    .line 3129
    .local v23, motionViewRealTop:I
    if-eqz v16, :cond_119

    #@96
    .line 3132
    move/from16 v0, v18

    #@98
    neg-int v3, v0

    #@99
    sub-int v4, v23, v22

    #@9b
    sub-int v5, v3, v4

    #@9d
    .line 3134
    .local v5, overscroll:I
    const/4 v4, 0x0

    #@9e
    const/4 v6, 0x0

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget v7, v0, Landroid/view/View;->mScrollY:I

    #@a3
    const/4 v8, 0x0

    #@a4
    const/4 v9, 0x0

    #@a5
    const/4 v10, 0x0

    #@a6
    move-object/from16 v0, p0

    #@a8
    iget v11, v0, Landroid/widget/AbsListView;->mOverscrollDistance:I

    #@aa
    const/4 v12, 0x1

    #@ab
    move-object/from16 v3, p0

    #@ad
    invoke-virtual/range {v3 .. v12}, Landroid/widget/AbsListView;->overScrollBy(IIIIIIIIZ)Z

    #@b0
    .line 3136
    move-object/from16 v0, p0

    #@b2
    iget v3, v0, Landroid/widget/AbsListView;->mOverscrollDistance:I

    #@b4
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    #@b7
    move-result v3

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget v4, v0, Landroid/view/View;->mScrollY:I

    #@bc
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    #@bf
    move-result v4

    #@c0
    if-ne v3, v4, :cond_cf

    #@c2
    .line 3138
    move-object/from16 v0, p0

    #@c4
    iget-object v3, v0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@c6
    if-eqz v3, :cond_cf

    #@c8
    .line 3139
    move-object/from16 v0, p0

    #@ca
    iget-object v3, v0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@cc
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    #@cf
    .line 3143
    :cond_cf
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getOverScrollMode()I

    #@d2
    move-result v27

    #@d3
    .line 3144
    .local v27, overscrollMode:I
    if-eqz v27, :cond_e0

    #@d5
    const/4 v3, 0x1

    #@d6
    move/from16 v0, v27

    #@d8
    if-ne v0, v3, :cond_119

    #@da
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->contentFits()Z

    #@dd
    move-result v3

    #@de
    if-nez v3, :cond_119

    #@e0
    .line 3147
    :cond_e0
    const/4 v3, 0x0

    #@e1
    move-object/from16 v0, p0

    #@e3
    iput v3, v0, Landroid/widget/AbsListView;->mDirection:I

    #@e5
    .line 3148
    const/4 v3, 0x5

    #@e6
    move-object/from16 v0, p0

    #@e8
    iput v3, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@ea
    .line 3149
    if-lez v29, :cond_132

    #@ec
    .line 3150
    move-object/from16 v0, p0

    #@ee
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@f0
    int-to-float v4, v5

    #@f1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@f4
    move-result v6

    #@f5
    int-to-float v6, v6

    #@f6
    div-float/2addr v4, v6

    #@f7
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@fa
    .line 3151
    move-object/from16 v0, p0

    #@fc
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@fe
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@101
    move-result v3

    #@102
    if-nez v3, :cond_10b

    #@104
    .line 3152
    move-object/from16 v0, p0

    #@106
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@108
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@10b
    .line 3154
    :cond_10b
    move-object/from16 v0, p0

    #@10d
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@10f
    const/4 v4, 0x0

    #@110
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    #@113
    move-result-object v3

    #@114
    move-object/from16 v0, p0

    #@116
    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->invalidate(Landroid/graphics/Rect;)V

    #@119
    .line 3164
    .end local v5           #overscroll:I
    .end local v27           #overscrollMode:I
    :cond_119
    :goto_119
    move/from16 v0, p1

    #@11b
    move-object/from16 v1, p0

    #@11d
    iput v0, v1, Landroid/widget/AbsListView;->mMotionY:I

    #@11f
    .line 3166
    .end local v23           #motionViewRealTop:I
    :cond_11f
    move/from16 v0, p1

    #@121
    move-object/from16 v1, p0

    #@123
    iput v0, v1, Landroid/widget/AbsListView;->mLastY:I

    #@125
    .line 3234
    .end local v16           #atEdge:Z
    .end local v19           #motionIndex:I
    .end local v21           #motionView:Landroid/view/View;
    .end local v22           #motionViewPrevTop:I
    :cond_125
    :goto_125
    return-void

    #@126
    .end local v18           #incrementalDeltaY:I
    :cond_126
    move/from16 v18, v17

    #@128
    .line 3075
    goto/16 :goto_1a

    #@12a
    .line 3108
    .restart local v18       #incrementalDeltaY:I
    :cond_12a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@12d
    move-result v3

    #@12e
    div-int/lit8 v19, v3, 0x2

    #@130
    .restart local v19       #motionIndex:I
    goto/16 :goto_68

    #@132
    .line 3155
    .restart local v5       #overscroll:I
    .restart local v16       #atEdge:Z
    .restart local v21       #motionView:Landroid/view/View;
    .restart local v22       #motionViewPrevTop:I
    .restart local v23       #motionViewRealTop:I
    .restart local v27       #overscrollMode:I
    :cond_132
    if-gez v29, :cond_119

    #@134
    .line 3156
    move-object/from16 v0, p0

    #@136
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@138
    int-to-float v4, v5

    #@139
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@13c
    move-result v6

    #@13d
    int-to-float v6, v6

    #@13e
    div-float/2addr v4, v6

    #@13f
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@142
    .line 3157
    move-object/from16 v0, p0

    #@144
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@146
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@149
    move-result v3

    #@14a
    if-nez v3, :cond_153

    #@14c
    .line 3158
    move-object/from16 v0, p0

    #@14e
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@150
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@153
    .line 3160
    :cond_153
    move-object/from16 v0, p0

    #@155
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@157
    const/4 v4, 0x1

    #@158
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    #@15b
    move-result-object v3

    #@15c
    move-object/from16 v0, p0

    #@15e
    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->invalidate(Landroid/graphics/Rect;)V

    #@161
    goto :goto_119

    #@162
    .line 3168
    .end local v5           #overscroll:I
    .end local v16           #atEdge:Z
    .end local v19           #motionIndex:I
    .end local v21           #motionView:Landroid/view/View;
    .end local v22           #motionViewPrevTop:I
    .end local v23           #motionViewRealTop:I
    .end local v27           #overscrollMode:I
    :cond_162
    move-object/from16 v0, p0

    #@164
    iget v3, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@166
    const/4 v4, 0x5

    #@167
    if-ne v3, v4, :cond_125

    #@169
    .line 3169
    move-object/from16 v0, p0

    #@16b
    iget v3, v0, Landroid/widget/AbsListView;->mLastY:I

    #@16d
    move/from16 v0, p1

    #@16f
    if-eq v0, v3, :cond_125

    #@171
    .line 3170
    move-object/from16 v0, p0

    #@173
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@175
    move/from16 v26, v0

    #@177
    .line 3171
    .local v26, oldScroll:I
    sub-int v25, v26, v18

    #@179
    .line 3172
    .local v25, newScroll:I
    move-object/from16 v0, p0

    #@17b
    iget v3, v0, Landroid/widget/AbsListView;->mLastY:I

    #@17d
    move/from16 v0, p1

    #@17f
    if-le v0, v3, :cond_24b

    #@181
    const/16 v24, 0x1

    #@183
    .line 3174
    .local v24, newDirection:I
    :goto_183
    move-object/from16 v0, p0

    #@185
    iget v3, v0, Landroid/widget/AbsListView;->mDirection:I

    #@187
    if-nez v3, :cond_18f

    #@189
    .line 3175
    move/from16 v0, v24

    #@18b
    move-object/from16 v1, p0

    #@18d
    iput v0, v1, Landroid/widget/AbsListView;->mDirection:I

    #@18f
    .line 3178
    :cond_18f
    move/from16 v0, v18

    #@191
    neg-int v8, v0

    #@192
    .line 3179
    .local v8, overScrollDistance:I
    if-gez v25, :cond_196

    #@194
    if-gez v26, :cond_19a

    #@196
    :cond_196
    if-lez v25, :cond_24f

    #@198
    if-gtz v26, :cond_24f

    #@19a
    .line 3180
    :cond_19a
    move/from16 v0, v26

    #@19c
    neg-int v8, v0

    #@19d
    .line 3181
    add-int v18, v18, v8

    #@19f
    .line 3186
    :goto_19f
    if-eqz v8, :cond_1f4

    #@1a1
    .line 3187
    const/4 v7, 0x0

    #@1a2
    const/4 v9, 0x0

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iget v10, v0, Landroid/view/View;->mScrollY:I

    #@1a7
    const/4 v11, 0x0

    #@1a8
    const/4 v12, 0x0

    #@1a9
    const/4 v13, 0x0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    iget v14, v0, Landroid/widget/AbsListView;->mOverscrollDistance:I

    #@1ae
    const/4 v15, 0x1

    #@1af
    move-object/from16 v6, p0

    #@1b1
    invoke-virtual/range {v6 .. v15}, Landroid/widget/AbsListView;->overScrollBy(IIIIIIIIZ)Z

    #@1b4
    .line 3189
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getOverScrollMode()I

    #@1b7
    move-result v27

    #@1b8
    .line 3190
    .restart local v27       #overscrollMode:I
    if-eqz v27, :cond_1c5

    #@1ba
    const/4 v3, 0x1

    #@1bb
    move/from16 v0, v27

    #@1bd
    if-ne v0, v3, :cond_1f4

    #@1bf
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->contentFits()Z

    #@1c2
    move-result v3

    #@1c3
    if-nez v3, :cond_1f4

    #@1c5
    .line 3193
    :cond_1c5
    if-lez v29, :cond_253

    #@1c7
    .line 3194
    move-object/from16 v0, p0

    #@1c9
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1cb
    int-to-float v4, v8

    #@1cc
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@1cf
    move-result v6

    #@1d0
    int-to-float v6, v6

    #@1d1
    div-float/2addr v4, v6

    #@1d2
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@1d5
    .line 3195
    move-object/from16 v0, p0

    #@1d7
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1d9
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@1dc
    move-result v3

    #@1dd
    if-nez v3, :cond_1e6

    #@1df
    .line 3196
    move-object/from16 v0, p0

    #@1e1
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1e3
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@1e6
    .line 3198
    :cond_1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@1ea
    const/4 v4, 0x0

    #@1eb
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    #@1ee
    move-result-object v3

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->invalidate(Landroid/graphics/Rect;)V

    #@1f4
    .line 3209
    .end local v27           #overscrollMode:I
    :cond_1f4
    :goto_1f4
    if-eqz v18, :cond_23d

    #@1f6
    .line 3211
    move-object/from16 v0, p0

    #@1f8
    iget v3, v0, Landroid/view/View;->mScrollY:I

    #@1fa
    if-eqz v3, :cond_204

    #@1fc
    .line 3212
    const/4 v3, 0x0

    #@1fd
    move-object/from16 v0, p0

    #@1ff
    iput v3, v0, Landroid/view/View;->mScrollY:I

    #@201
    .line 3213
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->invalidateParentIfNeeded()V

    #@204
    .line 3216
    :cond_204
    move-object/from16 v0, p0

    #@206
    move/from16 v1, v18

    #@208
    move/from16 v2, v18

    #@20a
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->trackMotionScroll(II)Z

    #@20d
    .line 3218
    const/4 v3, 0x3

    #@20e
    move-object/from16 v0, p0

    #@210
    iput v3, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@212
    .line 3222
    invoke-virtual/range {p0 .. p1}, Landroid/widget/AbsListView;->findClosestMotionRow(I)I

    #@215
    move-result v20

    #@216
    .line 3224
    .local v20, motionPosition:I
    const/4 v3, 0x0

    #@217
    move-object/from16 v0, p0

    #@219
    iput v3, v0, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@21b
    .line 3225
    move-object/from16 v0, p0

    #@21d
    iget v3, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@21f
    sub-int v3, v20, v3

    #@221
    move-object/from16 v0, p0

    #@223
    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@226
    move-result-object v21

    #@227
    .line 3226
    .restart local v21       #motionView:Landroid/view/View;
    if-eqz v21, :cond_284

    #@229
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getTop()I

    #@22c
    move-result v3

    #@22d
    :goto_22d
    move-object/from16 v0, p0

    #@22f
    iput v3, v0, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@231
    .line 3227
    move/from16 v0, p1

    #@233
    move-object/from16 v1, p0

    #@235
    iput v0, v1, Landroid/widget/AbsListView;->mMotionY:I

    #@237
    .line 3228
    move/from16 v0, v20

    #@239
    move-object/from16 v1, p0

    #@23b
    iput v0, v1, Landroid/widget/AbsListView;->mMotionPosition:I

    #@23d
    .line 3230
    .end local v20           #motionPosition:I
    .end local v21           #motionView:Landroid/view/View;
    :cond_23d
    move/from16 v0, p1

    #@23f
    move-object/from16 v1, p0

    #@241
    iput v0, v1, Landroid/widget/AbsListView;->mLastY:I

    #@243
    .line 3231
    move/from16 v0, v24

    #@245
    move-object/from16 v1, p0

    #@247
    iput v0, v1, Landroid/widget/AbsListView;->mDirection:I

    #@249
    goto/16 :goto_125

    #@24b
    .line 3172
    .end local v8           #overScrollDistance:I
    .end local v24           #newDirection:I
    :cond_24b
    const/16 v24, -0x1

    #@24d
    goto/16 :goto_183

    #@24f
    .line 3183
    .restart local v8       #overScrollDistance:I
    .restart local v24       #newDirection:I
    :cond_24f
    const/16 v18, 0x0

    #@251
    goto/16 :goto_19f

    #@253
    .line 3199
    .restart local v27       #overscrollMode:I
    :cond_253
    if-gez v29, :cond_1f4

    #@255
    .line 3200
    move-object/from16 v0, p0

    #@257
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@259
    int-to-float v4, v8

    #@25a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@25d
    move-result v6

    #@25e
    int-to-float v6, v6

    #@25f
    div-float/2addr v4, v6

    #@260
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    #@263
    .line 3201
    move-object/from16 v0, p0

    #@265
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@267
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@26a
    move-result v3

    #@26b
    if-nez v3, :cond_274

    #@26d
    .line 3202
    move-object/from16 v0, p0

    #@26f
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@271
    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    #@274
    .line 3204
    :cond_274
    move-object/from16 v0, p0

    #@276
    iget-object v3, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@278
    const/4 v4, 0x1

    #@279
    invoke-virtual {v3, v4}, Landroid/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    #@27c
    move-result-object v3

    #@27d
    move-object/from16 v0, p0

    #@27f
    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->invalidate(Landroid/graphics/Rect;)V

    #@282
    goto/16 :goto_1f4

    #@284
    .line 3226
    .end local v27           #overscrollMode:I
    .restart local v20       #motionPosition:I
    .restart local v21       #motionView:Landroid/view/View;
    :cond_284
    const/4 v3, 0x0

    #@285
    goto :goto_22d
.end method

.method private showPopup()V
    .registers 2

    #@0
    .prologue
    .line 5631
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getWindowVisibility()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_10

    #@6
    .line 5632
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Landroid/widget/AbsListView;->createTextFilter(Z)V

    #@a
    .line 5633
    invoke-direct {p0}, Landroid/widget/AbsListView;->positionPopup()V

    #@d
    .line 5635
    invoke-virtual {p0}, Landroid/widget/AbsListView;->checkFocus()V

    #@10
    .line 5637
    :cond_10
    return-void
.end method

.method private startScrollIfNeeded(I)Z
    .registers 12
    .parameter "y"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 3034
    iget v6, p0, Landroid/widget/AbsListView;->mMotionY:I

    #@4
    sub-int v0, p1, v6

    #@6
    .line 3035
    .local v0, deltaY:I
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@9
    move-result v1

    #@a
    .line 3036
    .local v1, distance:I
    iget v6, p0, Landroid/view/View;->mScrollY:I

    #@c
    if-eqz v6, :cond_4c

    #@e
    move v4, v8

    #@f
    .line 3037
    .local v4, overscroll:Z
    :goto_f
    if-nez v4, :cond_15

    #@11
    iget v6, p0, Landroid/widget/AbsListView;->mTouchSlop:I

    #@13
    if-le v1, v6, :cond_5c

    #@15
    .line 3038
    :cond_15
    invoke-direct {p0}, Landroid/widget/AbsListView;->createScrollingCache()V

    #@18
    .line 3039
    if-eqz v4, :cond_4e

    #@1a
    .line 3040
    const/4 v6, 0x5

    #@1b
    iput v6, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@1d
    .line 3041
    iput v7, p0, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@1f
    .line 3046
    :goto_1f
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHandler()Landroid/os/Handler;

    #@22
    move-result-object v2

    #@23
    .line 3050
    .local v2, handler:Landroid/os/Handler;
    if-eqz v2, :cond_2a

    #@25
    .line 3051
    iget-object v6, p0, Landroid/widget/AbsListView;->mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

    #@27
    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@2a
    .line 3053
    :cond_2a
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@2d
    .line 3054
    iget v6, p0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@2f
    iget v9, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@31
    sub-int/2addr v6, v9

    #@32
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@35
    move-result-object v3

    #@36
    .line 3055
    .local v3, motionView:Landroid/view/View;
    if-eqz v3, :cond_3b

    #@38
    .line 3056
    invoke-virtual {v3, v7}, Landroid/view/View;->setPressed(Z)V

    #@3b
    .line 3058
    :cond_3b
    invoke-virtual {p0, v8}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@3e
    .line 3061
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getParent()Landroid/view/ViewParent;

    #@41
    move-result-object v5

    #@42
    .line 3062
    .local v5, parent:Landroid/view/ViewParent;
    if-eqz v5, :cond_47

    #@44
    .line 3063
    invoke-interface {v5, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@47
    .line 3065
    :cond_47
    invoke-direct {p0, p1}, Landroid/widget/AbsListView;->scrollIfNeeded(I)V

    #@4a
    move v6, v8

    #@4b
    .line 3069
    .end local v2           #handler:Landroid/os/Handler;
    .end local v3           #motionView:Landroid/view/View;
    .end local v5           #parent:Landroid/view/ViewParent;
    :goto_4b
    return v6

    #@4c
    .end local v4           #overscroll:Z
    :cond_4c
    move v4, v7

    #@4d
    .line 3036
    goto :goto_f

    #@4e
    .line 3043
    .restart local v4       #overscroll:Z
    :cond_4e
    const/4 v6, 0x3

    #@4f
    iput v6, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@51
    .line 3044
    if-lez v0, :cond_58

    #@53
    iget v6, p0, Landroid/widget/AbsListView;->mTouchSlop:I

    #@55
    :goto_55
    iput v6, p0, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@57
    goto :goto_1f

    #@58
    :cond_58
    iget v6, p0, Landroid/widget/AbsListView;->mTouchSlop:I

    #@5a
    neg-int v6, v6

    #@5b
    goto :goto_55

    #@5c
    :cond_5c
    move v6, v7

    #@5d
    .line 3069
    goto :goto_4b
.end method

.method private updateOnScreenCheckedViews()V
    .registers 9

    #@0
    .prologue
    .line 1141
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2
    .line 1142
    .local v2, firstPos:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@5
    move-result v1

    #@6
    .line 1143
    .local v1, count:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v6}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@d
    move-result-object v6

    #@e
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@10
    const/16 v7, 0xb

    #@12
    if-lt v6, v7, :cond_30

    #@14
    const/4 v5, 0x1

    #@15
    .line 1145
    .local v5, useActivated:Z
    :goto_15
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v1, :cond_3e

    #@18
    .line 1146
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1b
    move-result-object v0

    #@1c
    .line 1147
    .local v0, child:Landroid/view/View;
    add-int v4, v2, v3

    #@1e
    .line 1149
    .local v4, position:I
    instance-of v6, v0, Landroid/widget/Checkable;

    #@20
    if-eqz v6, :cond_32

    #@22
    .line 1150
    check-cast v0, Landroid/widget/Checkable;

    #@24
    .end local v0           #child:Landroid/view/View;
    iget-object v6, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@26
    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@29
    move-result v6

    #@2a
    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    #@2d
    .line 1145
    :cond_2d
    :goto_2d
    add-int/lit8 v3, v3, 0x1

    #@2f
    goto :goto_16

    #@30
    .line 1143
    .end local v3           #i:I
    .end local v4           #position:I
    .end local v5           #useActivated:Z
    :cond_30
    const/4 v5, 0x0

    #@31
    goto :goto_15

    #@32
    .line 1151
    .restart local v0       #child:Landroid/view/View;
    .restart local v3       #i:I
    .restart local v4       #position:I
    .restart local v5       #useActivated:Z
    :cond_32
    if-eqz v5, :cond_2d

    #@34
    .line 1152
    iget-object v6, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@36
    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@39
    move-result v6

    #@3a
    invoke-virtual {v0, v6}, Landroid/view/View;->setActivated(Z)V

    #@3d
    goto :goto_2d

    #@3e
    .line 1155
    .end local v0           #child:Landroid/view/View;
    .end local v4           #position:I
    :cond_3e
    return-void
.end method

.method private useDefaultSelector()V
    .registers 3

    #@0
    .prologue
    .line 1513
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    const v1, 0x1080062

    #@7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    #@e
    .line 1515
    return-void
.end method


# virtual methods
.method public addTouchables(Ljava/util/ArrayList;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 4071
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v2

    #@4
    .line 4072
    .local v2, count:I
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@6
    .line 4073
    .local v3, firstPosition:I
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@8
    .line 4075
    .local v0, adapter:Landroid/widget/ListAdapter;
    if-nez v0, :cond_b

    #@a
    .line 4086
    :cond_a
    return-void

    #@b
    .line 4079
    :cond_b
    const/4 v4, 0x0

    #@c
    .local v4, i:I
    :goto_c
    if-ge v4, v2, :cond_a

    #@e
    .line 4080
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v1

    #@12
    .line 4081
    .local v1, child:Landroid/view/View;
    add-int v5, v3, v4

    #@14
    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_1d

    #@1a
    .line 4082
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 4084
    :cond_1d
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    #@20
    .line 4079
    add-int/lit8 v4, v4, 0x1

    #@22
    goto :goto_c
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 5969
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 5931
    return-void
.end method

.method public checkInputConnectionProxy(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 5850
    iget-object v0, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@2
    if-ne p1, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 5996
    instance-of v0, p1, Landroid/widget/AbsListView$LayoutParams;

    #@2
    return v0
.end method

.method public clearChoices()V
    .registers 2

    #@0
    .prologue
    .line 988
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 989
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@6
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    #@9
    .line 991
    :cond_9
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 992
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@f
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    #@12
    .line 994
    :cond_12
    const/4 v0, 0x0

    #@13
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@15
    .line 995
    return-void
.end method

.method public clearTextFilter()V
    .registers 3

    #@0
    .prologue
    .line 5895
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 5896
    iget-object v0, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@6
    const-string v1, ""

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@b
    .line 5897
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@e
    .line 5898
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@10
    if-eqz v0, :cond_1d

    #@12
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@14
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1d

    #@1a
    .line 5899
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@1d
    .line 5902
    :cond_1d
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1874
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@4
    move-result v1

    #@5
    .line 1875
    .local v1, count:I
    if-lez v1, :cond_3c

    #@7
    .line 1876
    iget-boolean v7, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@9
    if-eqz v7, :cond_3a

    #@b
    .line 1877
    mul-int/lit8 v2, v1, 0x64

    #@d
    .line 1879
    .local v2, extent:I
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v5

    #@11
    .line 1880
    .local v5, view:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@14
    move-result v4

    #@15
    .line 1881
    .local v4, top:I
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    #@18
    move-result v3

    #@19
    .line 1882
    .local v3, height:I
    if-lez v3, :cond_1f

    #@1b
    .line 1883
    mul-int/lit8 v6, v4, 0x64

    #@1d
    div-int/2addr v6, v3

    #@1e
    add-int/2addr v2, v6

    #@1f
    .line 1886
    :cond_1f
    add-int/lit8 v6, v1, -0x1

    #@21
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@24
    move-result-object v5

    #@25
    .line 1887
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@28
    move-result v0

    #@29
    .line 1888
    .local v0, bottom:I
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    #@2c
    move-result v3

    #@2d
    .line 1889
    if-lez v3, :cond_39

    #@2f
    .line 1890
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@32
    move-result v6

    #@33
    sub-int v6, v0, v6

    #@35
    mul-int/lit8 v6, v6, 0x64

    #@37
    div-int/2addr v6, v3

    #@38
    sub-int/2addr v2, v6

    #@39
    .line 1898
    .end local v0           #bottom:I
    .end local v2           #extent:I
    .end local v3           #height:I
    .end local v4           #top:I
    .end local v5           #view:Landroid/view/View;
    :cond_39
    :goto_39
    return v2

    #@3a
    .line 1895
    :cond_3a
    const/4 v2, 0x1

    #@3b
    goto :goto_39

    #@3c
    :cond_3c
    move v2, v6

    #@3d
    .line 1898
    goto :goto_39
.end method

.method protected computeVerticalScrollOffset()I
    .registers 12

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1903
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3
    .line 1904
    .local v2, firstPosition:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@6
    move-result v0

    #@7
    .line 1905
    .local v0, childCount:I
    if-ltz v2, :cond_39

    #@9
    if-lez v0, :cond_39

    #@b
    .line 1906
    iget-boolean v8, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@d
    if-eqz v8, :cond_3a

    #@f
    .line 1907
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v6

    #@13
    .line 1908
    .local v6, view:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@16
    move-result v5

    #@17
    .line 1909
    .local v5, top:I
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    #@1a
    move-result v3

    #@1b
    .line 1910
    .local v3, height:I
    if-lez v3, :cond_39

    #@1d
    .line 1911
    mul-int/lit8 v8, v2, 0x64

    #@1f
    mul-int/lit8 v9, v5, 0x64

    #@21
    div-int/2addr v9, v3

    #@22
    sub-int/2addr v8, v9

    #@23
    iget v9, p0, Landroid/view/View;->mScrollY:I

    #@25
    int-to-float v9, v9

    #@26
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@29
    move-result v10

    #@2a
    int-to-float v10, v10

    #@2b
    div-float/2addr v9, v10

    #@2c
    iget v10, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2e
    int-to-float v10, v10

    #@2f
    mul-float/2addr v9, v10

    #@30
    const/high16 v10, 0x42c8

    #@32
    mul-float/2addr v9, v10

    #@33
    float-to-int v9, v9

    #@34
    add-int/2addr v8, v9

    #@35
    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    #@38
    move-result v7

    #@39
    .line 1927
    .end local v3           #height:I
    .end local v5           #top:I
    .end local v6           #view:Landroid/view/View;
    :cond_39
    :goto_39
    return v7

    #@3a
    .line 1916
    :cond_3a
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3c
    .line 1917
    .local v1, count:I
    if-nez v2, :cond_48

    #@3e
    .line 1918
    const/4 v4, 0x0

    #@3f
    .line 1924
    .local v4, index:I
    :goto_3f
    int-to-float v7, v2

    #@40
    int-to-float v8, v0

    #@41
    int-to-float v9, v4

    #@42
    int-to-float v10, v1

    #@43
    div-float/2addr v9, v10

    #@44
    mul-float/2addr v8, v9

    #@45
    add-float/2addr v7, v8

    #@46
    float-to-int v7, v7

    #@47
    goto :goto_39

    #@48
    .line 1919
    .end local v4           #index:I
    :cond_48
    add-int v7, v2, v0

    #@4a
    if-ne v7, v1, :cond_4e

    #@4c
    .line 1920
    move v4, v1

    #@4d
    .restart local v4       #index:I
    goto :goto_3f

    #@4e
    .line 1922
    .end local v4           #index:I
    :cond_4e
    div-int/lit8 v7, v0, 0x2

    #@50
    add-int v4, v2, v7

    #@52
    .restart local v4       #index:I
    goto :goto_3f
.end method

.method protected computeVerticalScrollRange()I
    .registers 4

    #@0
    .prologue
    .line 1933
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@2
    if-eqz v1, :cond_28

    #@4
    .line 1934
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@6
    mul-int/lit8 v1, v1, 0x64

    #@8
    const/4 v2, 0x0

    #@9
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@c
    move-result v0

    #@d
    .line 1935
    .local v0, result:I
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@f
    if-eqz v1, :cond_27

    #@11
    .line 1937
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@13
    int-to-float v1, v1

    #@14
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@17
    move-result v2

    #@18
    int-to-float v2, v2

    #@19
    div-float/2addr v1, v2

    #@1a
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1c
    int-to-float v2, v2

    #@1d
    mul-float/2addr v1, v2

    #@1e
    const/high16 v2, 0x42c8

    #@20
    mul-float/2addr v1, v2

    #@21
    float-to-int v1, v1

    #@22
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@25
    move-result v1

    #@26
    add-int/2addr v0, v1

    #@27
    .line 1942
    :cond_27
    :goto_27
    return v0

    #@28
    .line 1940
    .end local v0           #result:I
    :cond_28
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2a
    .restart local v0       #result:I
    goto :goto_27
.end method

.method confirmCheckedPositionsById()V
    .registers 18

    #@0
    .prologue
    .line 5414
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@4
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    #@7
    .line 5416
    const/4 v7, 0x0

    #@8
    .line 5417
    .local v7, checkedCountChanged:Z
    const/4 v8, 0x0

    #@9
    .local v8, checkedIndex:I
    :goto_9
    move-object/from16 v0, p0

    #@b
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@d
    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    #@10
    move-result v1

    #@11
    if-ge v8, v1, :cond_a9

    #@13
    .line 5418
    move-object/from16 v0, p0

    #@15
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@17
    invoke-virtual {v1, v8}, Landroid/util/LongSparseArray;->keyAt(I)J

    #@1a
    move-result-wide v4

    #@1b
    .line 5419
    .local v4, id:J
    move-object/from16 v0, p0

    #@1d
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@1f
    invoke-virtual {v1, v8}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Ljava/lang/Integer;

    #@25
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@28
    move-result v3

    #@29
    .line 5421
    .local v3, lastPos:I
    move-object/from16 v0, p0

    #@2b
    iget-object v1, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2d
    invoke-interface {v1, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@30
    move-result-wide v11

    #@31
    .line 5422
    .local v11, lastPosId:J
    cmp-long v1, v4, v11

    #@33
    if-eqz v1, :cond_a0

    #@35
    .line 5424
    const/4 v1, 0x0

    #@36
    add-int/lit8 v2, v3, -0x14

    #@38
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@3b
    move-result v16

    #@3c
    .line 5425
    .local v16, start:I
    add-int/lit8 v1, v3, 0x14

    #@3e
    move-object/from16 v0, p0

    #@40
    iget v2, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@42
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@45
    move-result v9

    #@46
    .line 5426
    .local v9, end:I
    const/4 v10, 0x0

    #@47
    .line 5427
    .local v10, found:Z
    move/from16 v15, v16

    #@49
    .local v15, searchPos:I
    :goto_49
    if-ge v15, v9, :cond_6b

    #@4b
    .line 5428
    move-object/from16 v0, p0

    #@4d
    iget-object v1, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@4f
    invoke-interface {v1, v15}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@52
    move-result-wide v13

    #@53
    .line 5429
    .local v13, searchId:J
    cmp-long v1, v4, v13

    #@55
    if-nez v1, :cond_9d

    #@57
    .line 5430
    const/4 v10, 0x1

    #@58
    .line 5431
    move-object/from16 v0, p0

    #@5a
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@5c
    const/4 v2, 0x1

    #@5d
    invoke-virtual {v1, v15, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@60
    .line 5432
    move-object/from16 v0, p0

    #@62
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@64
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {v1, v8, v2}, Landroid/util/LongSparseArray;->setValueAt(ILjava/lang/Object;)V

    #@6b
    .line 5437
    .end local v13           #searchId:J
    :cond_6b
    if-nez v10, :cond_99

    #@6d
    .line 5438
    move-object/from16 v0, p0

    #@6f
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@71
    invoke-virtual {v1, v4, v5}, Landroid/util/LongSparseArray;->delete(J)V

    #@74
    .line 5439
    add-int/lit8 v8, v8, -0x1

    #@76
    .line 5440
    move-object/from16 v0, p0

    #@78
    iget v1, v0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@7a
    add-int/lit8 v1, v1, -0x1

    #@7c
    move-object/from16 v0, p0

    #@7e
    iput v1, v0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@80
    .line 5441
    const/4 v7, 0x1

    #@81
    .line 5442
    move-object/from16 v0, p0

    #@83
    iget-object v1, v0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@85
    if-eqz v1, :cond_99

    #@87
    move-object/from16 v0, p0

    #@89
    iget-object v1, v0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@8b
    if-eqz v1, :cond_99

    #@8d
    .line 5443
    move-object/from16 v0, p0

    #@8f
    iget-object v1, v0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@91
    move-object/from16 v0, p0

    #@93
    iget-object v2, v0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@95
    const/4 v6, 0x0

    #@96
    invoke-virtual/range {v1 .. v6}, Landroid/widget/AbsListView$MultiChoiceModeWrapper;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    #@99
    .line 5417
    .end local v9           #end:I
    .end local v10           #found:Z
    .end local v15           #searchPos:I
    .end local v16           #start:I
    :cond_99
    :goto_99
    add-int/lit8 v8, v8, 0x1

    #@9b
    goto/16 :goto_9

    #@9d
    .line 5427
    .restart local v9       #end:I
    .restart local v10       #found:Z
    .restart local v13       #searchId:J
    .restart local v15       #searchPos:I
    .restart local v16       #start:I
    :cond_9d
    add-int/lit8 v15, v15, 0x1

    #@9f
    goto :goto_49

    #@a0
    .line 5448
    .end local v9           #end:I
    .end local v10           #found:Z
    .end local v13           #searchId:J
    .end local v15           #searchPos:I
    .end local v16           #start:I
    :cond_a0
    move-object/from16 v0, p0

    #@a2
    iget-object v1, v0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@a4
    const/4 v2, 0x1

    #@a5
    invoke-virtual {v1, v3, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@a8
    goto :goto_99

    #@a9
    .line 5452
    .end local v3           #lastPos:I
    .end local v4           #id:J
    .end local v11           #lastPosId:J
    :cond_a9
    if-eqz v7, :cond_b8

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v1, v0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@af
    if-eqz v1, :cond_b8

    #@b1
    .line 5453
    move-object/from16 v0, p0

    #@b3
    iget-object v1, v0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@b5
    invoke-virtual {v1}, Landroid/view/ActionMode;->invalidate()V

    #@b8
    .line 5455
    :cond_b8
    return-void
.end method

.method createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 6
    .parameter "view"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 2745
    new-instance v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    #@2
    invoke-direct {v0, p1, p2, p3, p4}, Landroid/widget/AdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    #@5
    return-object v0
.end method

.method public deferNotifyDataSetChanged()V
    .registers 2

    #@0
    .prologue
    .line 6140
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/AbsListView;->mDeferNotifyDataSetChanged:Z

    #@3
    .line 6141
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    #@0
    .prologue
    .line 2343
    const/4 v2, 0x0

    #@1
    .line 2344
    .local v2, saveCount:I
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@3
    and-int/lit8 v5, v5, 0x22

    #@5
    const/16 v6, 0x22

    #@7
    if-ne v5, v6, :cond_50

    #@9
    const/4 v0, 0x1

    #@a
    .line 2345
    .local v0, clipToPadding:Z
    :goto_a
    if-eqz v0, :cond_35

    #@c
    .line 2346
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@f
    move-result v2

    #@10
    .line 2347
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@12
    .line 2348
    .local v3, scrollX:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@14
    .line 2349
    .local v4, scrollY:I
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@16
    add-int/2addr v5, v3

    #@17
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@19
    add-int/2addr v6, v4

    #@1a
    iget v7, p0, Landroid/view/View;->mRight:I

    #@1c
    add-int/2addr v7, v3

    #@1d
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@1f
    sub-int/2addr v7, v8

    #@20
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    #@22
    sub-int/2addr v7, v8

    #@23
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@25
    add-int/2addr v8, v4

    #@26
    iget v9, p0, Landroid/view/View;->mTop:I

    #@28
    sub-int/2addr v8, v9

    #@29
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@2b
    sub-int/2addr v8, v9

    #@2c
    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@2f
    .line 2352
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@31
    and-int/lit8 v5, v5, -0x23

    #@33
    iput v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@35
    .line 2355
    .end local v3           #scrollX:I
    .end local v4           #scrollY:I
    :cond_35
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mDrawSelectorOnTop:Z

    #@37
    .line 2356
    .local v1, drawSelectorOnTop:Z
    if-nez v1, :cond_3c

    #@39
    .line 2357
    invoke-direct {p0, p1}, Landroid/widget/AbsListView;->drawSelector(Landroid/graphics/Canvas;)V

    #@3c
    .line 2360
    :cond_3c
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@3f
    .line 2362
    if-eqz v1, :cond_44

    #@41
    .line 2363
    invoke-direct {p0, p1}, Landroid/widget/AbsListView;->drawSelector(Landroid/graphics/Canvas;)V

    #@44
    .line 2366
    :cond_44
    if-eqz v0, :cond_4f

    #@46
    .line 2367
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@49
    .line 2368
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4b
    or-int/lit8 v5, v5, 0x22

    #@4d
    iput v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4f
    .line 2370
    :cond_4f
    return-void

    #@50
    .line 2344
    .end local v0           #clipToPadding:Z
    .end local v1           #drawSelectorOnTop:Z
    :cond_50
    const/4 v0, 0x0

    #@51
    goto :goto_a
.end method

.method protected dispatchSetPressed(Z)V
    .registers 2
    .parameter "pressed"

    #@0
    .prologue
    .line 2938
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 14
    .parameter "canvas"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x0

    #@2
    .line 3863
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->draw(Landroid/graphics/Canvas;)V

    #@5
    .line 3864
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@7
    if-eqz v8, :cond_b9

    #@9
    .line 3865
    iget v6, p0, Landroid/view/View;->mScrollY:I

    #@b
    .line 3866
    .local v6, scrollY:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@d
    invoke-virtual {v8}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@10
    move-result v8

    #@11
    if-nez v8, :cond_5c

    #@13
    .line 3867
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@16
    move-result v4

    #@17
    .line 3868
    .local v4, restoreCount:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@19
    iget v8, v8, Landroid/graphics/Rect;->left:I

    #@1b
    iget v9, p0, Landroid/widget/AbsListView;->mGlowPaddingLeft:I

    #@1d
    add-int v3, v8, v9

    #@1f
    .line 3869
    .local v3, leftPadding:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@21
    iget v8, v8, Landroid/graphics/Rect;->right:I

    #@23
    iget v9, p0, Landroid/widget/AbsListView;->mGlowPaddingRight:I

    #@25
    add-int v5, v8, v9

    #@27
    .line 3870
    .local v5, rightPadding:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getWidth()I

    #@2a
    move-result v8

    #@2b
    sub-int/2addr v8, v3

    #@2c
    sub-int v7, v8, v5

    #@2e
    .line 3872
    .local v7, width:I
    iget v8, p0, Landroid/widget/AbsListView;->mFirstPositionDistanceGuess:I

    #@30
    add-int/2addr v8, v6

    #@31
    invoke-static {v11, v8}, Ljava/lang/Math;->min(II)I

    #@34
    move-result v1

    #@35
    .line 3873
    .local v1, edgeY:I
    int-to-float v8, v3

    #@36
    int-to-float v9, v1

    #@37
    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    #@3a
    .line 3874
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@3c
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@3f
    move-result v9

    #@40
    invoke-virtual {v8, v7, v9}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@43
    .line 3875
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@45
    invoke-virtual {v8, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@48
    move-result v8

    #@49
    if-eqz v8, :cond_59

    #@4b
    .line 3876
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@4d
    invoke-virtual {v8, v3, v1}, Landroid/widget/EdgeEffect;->setPosition(II)V

    #@50
    .line 3877
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@52
    invoke-virtual {v8, v11}, Landroid/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {p0, v8}, Landroid/widget/AbsListView;->invalidate(Landroid/graphics/Rect;)V

    #@59
    .line 3879
    :cond_59
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@5c
    .line 3881
    .end local v1           #edgeY:I
    .end local v3           #leftPadding:I
    .end local v4           #restoreCount:I
    .end local v5           #rightPadding:I
    .end local v7           #width:I
    :cond_5c
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@5e
    invoke-virtual {v8}, Landroid/widget/EdgeEffect;->isFinished()Z

    #@61
    move-result v8

    #@62
    if-nez v8, :cond_b9

    #@64
    .line 3882
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@67
    move-result v4

    #@68
    .line 3883
    .restart local v4       #restoreCount:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@6a
    iget v8, v8, Landroid/graphics/Rect;->left:I

    #@6c
    iget v9, p0, Landroid/widget/AbsListView;->mGlowPaddingLeft:I

    #@6e
    add-int v3, v8, v9

    #@70
    .line 3884
    .restart local v3       #leftPadding:I
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@72
    iget v8, v8, Landroid/graphics/Rect;->right:I

    #@74
    iget v9, p0, Landroid/widget/AbsListView;->mGlowPaddingRight:I

    #@76
    add-int v5, v8, v9

    #@78
    .line 3885
    .restart local v5       #rightPadding:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getWidth()I

    #@7b
    move-result v8

    #@7c
    sub-int/2addr v8, v3

    #@7d
    sub-int v7, v8, v5

    #@7f
    .line 3886
    .restart local v7       #width:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@82
    move-result v2

    #@83
    .line 3888
    .local v2, height:I
    neg-int v8, v7

    #@84
    add-int v0, v8, v3

    #@86
    .line 3889
    .local v0, edgeX:I
    iget v8, p0, Landroid/widget/AbsListView;->mLastPositionDistanceGuess:I

    #@88
    add-int/2addr v8, v6

    #@89
    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    #@8c
    move-result v1

    #@8d
    .line 3890
    .restart local v1       #edgeY:I
    int-to-float v8, v0

    #@8e
    int-to-float v9, v1

    #@8f
    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    #@92
    .line 3891
    const/high16 v8, 0x4334

    #@94
    int-to-float v9, v7

    #@95
    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@98
    .line 3892
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@9a
    invoke-virtual {v8, v7, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    #@9d
    .line 3893
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@9f
    invoke-virtual {v8, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    #@a2
    move-result v8

    #@a3
    if-eqz v8, :cond_b6

    #@a5
    .line 3895
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@a7
    add-int v9, v0, v7

    #@a9
    invoke-virtual {v8, v9, v1}, Landroid/widget/EdgeEffect;->setPosition(II)V

    #@ac
    .line 3896
    iget-object v8, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@ae
    const/4 v9, 0x1

    #@af
    invoke-virtual {v8, v9}, Landroid/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    #@b2
    move-result-object v8

    #@b3
    invoke-virtual {p0, v8}, Landroid/widget/AbsListView;->invalidate(Landroid/graphics/Rect;)V

    #@b6
    .line 3898
    :cond_b6
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@b9
    .line 3901
    .end local v0           #edgeX:I
    .end local v1           #edgeY:I
    .end local v2           #height:I
    .end local v3           #leftPadding:I
    .end local v4           #restoreCount:I
    .end local v5           #rightPadding:I
    .end local v6           #scrollY:I
    .end local v7           #width:I
    :cond_b9
    iget-object v8, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@bb
    if-eqz v8, :cond_d1

    #@bd
    .line 3902
    iget v6, p0, Landroid/view/View;->mScrollY:I

    #@bf
    .line 3903
    .restart local v6       #scrollY:I
    if-eqz v6, :cond_d2

    #@c1
    .line 3905
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@c4
    move-result v4

    #@c5
    .line 3906
    .restart local v4       #restoreCount:I
    int-to-float v8, v6

    #@c6
    invoke-virtual {p1, v10, v8}, Landroid/graphics/Canvas;->translate(FF)V

    #@c9
    .line 3907
    iget-object v8, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@cb
    invoke-virtual {v8, p1}, Landroid/widget/FastScroller;->draw(Landroid/graphics/Canvas;)V

    #@ce
    .line 3908
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@d1
    .line 3913
    .end local v4           #restoreCount:I
    .end local v6           #scrollY:I
    :cond_d1
    :goto_d1
    return-void

    #@d2
    .line 3910
    .restart local v6       #scrollY:I
    :cond_d2
    iget-object v8, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@d4
    invoke-virtual {v8, p1}, Landroid/widget/FastScroller;->draw(Landroid/graphics/Canvas;)V

    #@d7
    goto :goto_d1
.end method

.method protected drawableStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 2552
    invoke-super {p0}, Landroid/widget/AdapterView;->drawableStateChanged()V

    #@3
    .line 2553
    invoke-virtual {p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@6
    .line 2554
    return-void
.end method

.method abstract fillGap(Z)V
.end method

.method findClosestMotionRow(I)I
    .registers 5
    .parameter "y"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 5263
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@4
    move-result v0

    #@5
    .line 5264
    .local v0, childCount:I
    if-nez v0, :cond_9

    #@7
    move v1, v2

    #@8
    .line 5269
    :cond_8
    :goto_8
    return v1

    #@9
    .line 5268
    :cond_9
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->findMotionRow(I)I

    #@c
    move-result v1

    #@d
    .line 5269
    .local v1, motionRow:I
    if-ne v1, v2, :cond_8

    #@f
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@11
    add-int/2addr v2, v0

    #@12
    add-int/lit8 v1, v2, -0x1

    #@14
    goto :goto_8
.end method

.method abstract findMotionRow(I)I
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 5

    #@0
    .prologue
    .line 5980
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    #@2
    const/4 v1, -0x1

    #@3
    const/4 v2, -0x2

    #@4
    const/4 v3, 0x0

    #@5
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    #@8
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/AbsListView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 5986
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/AbsListView$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 5991
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected getBottomFadingEdgeStrength()F
    .registers 8

    #@0
    .prologue
    .line 1964
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 1965
    .local v1, count:I
    invoke-super {p0}, Landroid/widget/AdapterView;->getBottomFadingEdgeStrength()F

    #@7
    move-result v2

    #@8
    .line 1966
    .local v2, fadeEdge:F
    if-nez v1, :cond_b

    #@a
    .line 1976
    .end local v2           #fadeEdge:F
    :cond_a
    :goto_a
    return v2

    #@b
    .line 1969
    .restart local v2       #fadeEdge:F
    :cond_b
    iget v5, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@d
    add-int/2addr v5, v1

    #@e
    add-int/lit8 v5, v5, -0x1

    #@10
    iget v6, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@12
    add-int/lit8 v6, v6, -0x1

    #@14
    if-ge v5, v6, :cond_19

    #@16
    .line 1970
    const/high16 v2, 0x3f80

    #@18
    goto :goto_a

    #@19
    .line 1973
    :cond_19
    add-int/lit8 v5, v1, -0x1

    #@1b
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@22
    move-result v0

    #@23
    .line 1974
    .local v0, bottom:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@26
    move-result v4

    #@27
    .line 1975
    .local v4, height:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getVerticalFadingEdgeLength()I

    #@2a
    move-result v5

    #@2b
    int-to-float v3, v5

    #@2c
    .line 1976
    .local v3, fadeLength:F
    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    #@2e
    sub-int v5, v4, v5

    #@30
    if-le v0, v5, :cond_a

    #@32
    sub-int v5, v0, v4

    #@34
    iget v6, p0, Landroid/view/View;->mPaddingBottom:I

    #@36
    add-int/2addr v5, v6

    #@37
    int-to-float v5, v5

    #@38
    div-float v2, v5, v3

    #@3a
    goto :goto_a
.end method

.method protected getBottomPaddingOffset()I
    .registers 3

    #@0
    .prologue
    .line 2394
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit8 v0, v0, 0x22

    #@4
    const/16 v1, 0x22

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    iget v0, p0, Landroid/view/View;->mPaddingBottom:I

    #@c
    goto :goto_9
.end method

.method public getCacheColorHint()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 6057
    iget v0, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@2
    return v0
.end method

.method public getCheckedItemCount()I
    .registers 2

    #@0
    .prologue
    .line 905
    iget v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@2
    return v0
.end method

.method public getCheckedItemIds()[J
    .registers 7

    #@0
    .prologue
    .line 969
    iget v4, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@2
    if-eqz v4, :cond_c

    #@4
    iget-object v4, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@6
    if-eqz v4, :cond_c

    #@8
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@a
    if-nez v4, :cond_10

    #@c
    .line 970
    :cond_c
    const/4 v4, 0x0

    #@d
    new-array v3, v4, [J

    #@f
    .line 981
    :cond_f
    return-object v3

    #@10
    .line 973
    :cond_10
    iget-object v2, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@12
    .line 974
    .local v2, idStates:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    #@15
    move-result v0

    #@16
    .line 975
    .local v0, count:I
    new-array v3, v0, [J

    #@18
    .line 977
    .local v3, ids:[J
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    if-ge v1, v0, :cond_f

    #@1b
    .line 978
    invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    #@1e
    move-result-wide v4

    #@1f
    aput-wide v4, v3, v1

    #@21
    .line 977
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_19
.end method

.method public getCheckedItemPosition()I
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 937
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@3
    if-ne v0, v1, :cond_19

    #@5
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@7
    if-eqz v0, :cond_19

    #@9
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@b
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    #@e
    move-result v0

    #@f
    if-ne v0, v1, :cond_19

    #@11
    .line 938
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@17
    move-result v0

    #@18
    .line 941
    :goto_18
    return v0

    #@19
    :cond_19
    const/4 v0, -0x1

    #@1a
    goto :goto_18
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .registers 2

    #@0
    .prologue
    .line 954
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 955
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@6
    .line 957
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getChoiceMode()I
    .registers 2

    #@0
    .prologue
    .line 1163
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@2
    return v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    #@0
    .prologue
    .line 2863
    iget-object v0, p0, Landroid/widget/AbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@2
    return-object v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 1500
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getSelectedView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 1501
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_13

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@9
    move-result-object v1

    #@a
    if-ne v1, p0, :cond_13

    #@c
    .line 1504
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@f
    .line 1505
    invoke-virtual {p0, v0, p1}, Landroid/widget/AbsListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@12
    .line 1510
    :goto_12
    return-void

    #@13
    .line 1508
    :cond_13
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->getFocusedRect(Landroid/graphics/Rect;)V

    #@16
    goto :goto_12
.end method

.method getFooterViewsCount()I
    .registers 2

    #@0
    .prologue
    .line 5206
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method getHeaderViewsCount()I
    .registers 2

    #@0
    .prologue
    .line 5196
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected getLeftPaddingOffset()I
    .registers 3

    #@0
    .prologue
    .line 2379
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit8 v0, v0, 0x22

    #@4
    const/16 v1, 0x22

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@c
    neg-int v0, v0

    #@d
    goto :goto_9
.end method

.method public getListPaddingBottom()I
    .registers 2

    #@0
    .prologue
    .line 2120
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@4
    return v0
.end method

.method public getListPaddingLeft()I
    .registers 2

    #@0
    .prologue
    .line 2132
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4
    return v0
.end method

.method public getListPaddingRight()I
    .registers 2

    #@0
    .prologue
    .line 2144
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@4
    return v0
.end method

.method public getListPaddingTop()I
    .registers 2

    #@0
    .prologue
    .line 2108
    iget-object v0, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@4
    return v0
.end method

.method protected getRightPaddingOffset()I
    .registers 3

    #@0
    .prologue
    .line 2389
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit8 v0, v0, 0x22

    #@4
    const/16 v1, 0x22

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@c
    goto :goto_9
.end method

.method public getSelectedView()Landroid/view/View;
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 2092
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2
    if-lez v0, :cond_12

    #@4
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@6
    if-ltz v0, :cond_12

    #@8
    .line 2093
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@a
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@c
    sub-int/2addr v0, v1

    #@d
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 2095
    :goto_11
    return-object v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 2490
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getSolidColor()I
    .registers 2

    #@0
    .prologue
    .line 6025
    iget v0, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@2
    return v0
.end method

.method public getTextFilter()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1824
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 1825
    iget-object v0, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@a
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@d
    move-result-object v0

    #@e
    .line 1827
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method protected getTopFadingEdgeStrength()F
    .registers 6

    #@0
    .prologue
    .line 1947
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 1948
    .local v0, count:I
    invoke-super {p0}, Landroid/widget/AdapterView;->getTopFadingEdgeStrength()F

    #@7
    move-result v1

    #@8
    .line 1949
    .local v1, fadeEdge:F
    if-nez v0, :cond_b

    #@a
    .line 1958
    .end local v1           #fadeEdge:F
    :cond_a
    :goto_a
    return v1

    #@b
    .line 1952
    .restart local v1       #fadeEdge:F
    :cond_b
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@d
    if-lez v4, :cond_12

    #@f
    .line 1953
    const/high16 v1, 0x3f80

    #@11
    goto :goto_a

    #@12
    .line 1956
    :cond_12
    const/4 v4, 0x0

    #@13
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    #@1a
    move-result v3

    #@1b
    .line 1957
    .local v3, top:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getVerticalFadingEdgeLength()I

    #@1e
    move-result v4

    #@1f
    int-to-float v2, v4

    #@20
    .line 1958
    .local v2, fadeLength:F
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@22
    if-ge v3, v4, :cond_a

    #@24
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@26
    sub-int v4, v3, v4

    #@28
    neg-int v4, v4

    #@29
    int-to-float v4, v4

    #@2a
    div-float v1, v4, v2

    #@2c
    goto :goto_a
.end method

.method protected getTopPaddingOffset()I
    .registers 3

    #@0
    .prologue
    .line 2384
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit8 v0, v0, 0x22

    #@4
    const/16 v1, 0x22

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@c
    neg-int v0, v0

    #@d
    goto :goto_9
.end method

.method public getTranscriptMode()I
    .registers 2

    #@0
    .prologue
    .line 6020
    iget v0, p0, Landroid/widget/AbsListView;->mTranscriptMode:I

    #@2
    return v0
.end method

.method public getVerticalScrollbarWidth()I
    .registers 3

    #@0
    .prologue
    .line 1284
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isFastScrollAlwaysVisible()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_15

    #@6
    .line 1285
    invoke-super {p0}, Landroid/widget/AdapterView;->getVerticalScrollbarWidth()I

    #@9
    move-result v0

    #@a
    iget-object v1, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@c
    invoke-virtual {v1}, Landroid/widget/FastScroller;->getWidth()I

    #@f
    move-result v1

    #@10
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v0

    #@14
    .line 1287
    :goto_14
    return v0

    #@15
    :cond_15
    invoke-super {p0}, Landroid/widget/AdapterView;->getVerticalScrollbarWidth()I

    #@18
    move-result v0

    #@19
    goto :goto_14
.end method

.method protected handleDataChanged()V
    .registers 16

    #@0
    .prologue
    const/4 v14, 0x5

    #@1
    const/4 v13, -0x1

    #@2
    const/4 v8, 0x3

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v12, 0x0

    #@5
    .line 5459
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@7
    .line 5460
    .local v1, count:I
    iget v4, p0, Landroid/widget/AbsListView;->mLastHandledItemCount:I

    #@9
    .line 5461
    .local v4, lastHandledItemCount:I
    iget v10, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@b
    iput v10, p0, Landroid/widget/AbsListView;->mLastHandledItemCount:I

    #@d
    .line 5463
    iget v10, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@f
    if-eqz v10, :cond_20

    #@11
    iget-object v10, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@13
    if-eqz v10, :cond_20

    #@15
    iget-object v10, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@17
    invoke-interface {v10}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@1a
    move-result v10

    #@1b
    if-eqz v10, :cond_20

    #@1d
    .line 5464
    invoke-virtual {p0}, Landroid/widget/AbsListView;->confirmCheckedPositionsById()V

    #@20
    .line 5468
    :cond_20
    iget-object v10, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@22
    invoke-virtual {v10}, Landroid/widget/AbsListView$RecycleBin;->clearTransientStateViews()V

    #@25
    .line 5470
    if-lez v1, :cond_ea

    #@27
    .line 5475
    iget-boolean v10, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@29
    if-eqz v10, :cond_73

    #@2b
    .line 5477
    iput-boolean v12, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@2d
    .line 5478
    const/4 v10, 0x0

    #@2e
    iput-object v10, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@30
    .line 5480
    iget v10, p0, Landroid/widget/AbsListView;->mTranscriptMode:I

    #@32
    const/4 v11, 0x2

    #@33
    if-ne v10, v11, :cond_38

    #@35
    .line 5481
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@37
    .line 5597
    :cond_37
    :goto_37
    return-void

    #@38
    .line 5483
    :cond_38
    iget v10, p0, Landroid/widget/AbsListView;->mTranscriptMode:I

    #@3a
    if-ne v10, v9, :cond_6e

    #@3c
    .line 5484
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mForceTranscriptScroll:Z

    #@3e
    if-eqz v10, :cond_45

    #@40
    .line 5485
    iput-boolean v12, p0, Landroid/widget/AbsListView;->mForceTranscriptScroll:Z

    #@42
    .line 5486
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@44
    goto :goto_37

    #@45
    .line 5489
    :cond_45
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@48
    move-result v0

    #@49
    .line 5490
    .local v0, childCount:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@4c
    move-result v10

    #@4d
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getPaddingBottom()I

    #@50
    move-result v11

    #@51
    sub-int v5, v10, v11

    #@53
    .line 5491
    .local v5, listBottom:I
    add-int/lit8 v10, v0, -0x1

    #@55
    invoke-virtual {p0, v10}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@58
    move-result-object v3

    #@59
    .line 5492
    .local v3, lastChild:Landroid/view/View;
    if-eqz v3, :cond_69

    #@5b
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    #@5e
    move-result v2

    #@5f
    .line 5493
    .local v2, lastBottom:I
    :goto_5f
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@61
    add-int/2addr v10, v0

    #@62
    if-lt v10, v4, :cond_6b

    #@64
    if-gt v2, v5, :cond_6b

    #@66
    .line 5495
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@68
    goto :goto_37

    #@69
    .end local v2           #lastBottom:I
    :cond_69
    move v2, v5

    #@6a
    .line 5492
    goto :goto_5f

    #@6b
    .line 5500
    .restart local v2       #lastBottom:I
    :cond_6b
    invoke-virtual {p0}, Landroid/widget/AbsListView;->awakenScrollBars()Z

    #@6e
    .line 5503
    .end local v0           #childCount:I
    .end local v2           #lastBottom:I
    .end local v3           #lastChild:Landroid/view/View;
    .end local v5           #listBottom:I
    :cond_6e
    iget v10, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@70
    packed-switch v10, :pswitch_data_10a

    #@73
    .line 5551
    :cond_73
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isInTouchMode()Z

    #@76
    move-result v10

    #@77
    if-nez v10, :cond_e6

    #@79
    .line 5553
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    #@7c
    move-result v6

    #@7d
    .line 5556
    .local v6, newPos:I
    if-lt v6, v1, :cond_81

    #@7f
    .line 5557
    add-int/lit8 v6, v1, -0x1

    #@81
    .line 5559
    :cond_81
    if-gez v6, :cond_84

    #@83
    .line 5560
    const/4 v6, 0x0

    #@84
    .line 5564
    :cond_84
    invoke-virtual {p0, v6, v9}, Landroid/widget/AbsListView;->lookForSelectablePosition(IZ)I

    #@87
    move-result v7

    #@88
    .line 5566
    .local v7, selectablePos:I
    if-ltz v7, :cond_db

    #@8a
    .line 5567
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->setNextSelectedPositionInt(I)V

    #@8d
    goto :goto_37

    #@8e
    .line 5505
    .end local v6           #newPos:I
    .end local v7           #selectablePos:I
    :pswitch_8e
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isInTouchMode()Z

    #@91
    move-result v10

    #@92
    if-eqz v10, :cond_a5

    #@94
    .line 5510
    iput v14, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@96
    .line 5511
    iget v8, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@98
    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    #@9b
    move-result v8

    #@9c
    add-int/lit8 v9, v1, -0x1

    #@9e
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@a1
    move-result v8

    #@a2
    iput v8, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@a4
    goto :goto_37

    #@a5
    .line 5517
    :cond_a5
    invoke-virtual {p0}, Landroid/widget/AbsListView;->findSyncPosition()I

    #@a8
    move-result v6

    #@a9
    .line 5518
    .restart local v6       #newPos:I
    if-ltz v6, :cond_73

    #@ab
    .line 5520
    invoke-virtual {p0, v6, v9}, Landroid/widget/AbsListView;->lookForSelectablePosition(IZ)I

    #@ae
    move-result v7

    #@af
    .line 5521
    .restart local v7       #selectablePos:I
    if-ne v7, v6, :cond_73

    #@b1
    .line 5523
    iput v6, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@b3
    .line 5525
    iget-wide v8, p0, Landroid/widget/AdapterView;->mSyncHeight:J

    #@b5
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@b8
    move-result v10

    #@b9
    int-to-long v10, v10

    #@ba
    cmp-long v8, v8, v10

    #@bc
    if-nez v8, :cond_c5

    #@be
    .line 5528
    iput v14, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@c0
    .line 5536
    :goto_c0
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->setNextSelectedPositionInt(I)V

    #@c3
    goto/16 :goto_37

    #@c5
    .line 5532
    :cond_c5
    const/4 v8, 0x2

    #@c6
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@c8
    goto :goto_c0

    #@c9
    .line 5544
    .end local v6           #newPos:I
    .end local v7           #selectablePos:I
    :pswitch_c9
    iput v14, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@cb
    .line 5545
    iget v8, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@cd
    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    #@d0
    move-result v8

    #@d1
    add-int/lit8 v9, v1, -0x1

    #@d3
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@d6
    move-result v8

    #@d7
    iput v8, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@d9
    goto/16 :goto_37

    #@db
    .line 5571
    .restart local v6       #newPos:I
    .restart local v7       #selectablePos:I
    :cond_db
    invoke-virtual {p0, v6, v12}, Landroid/widget/AbsListView;->lookForSelectablePosition(IZ)I

    #@de
    move-result v7

    #@df
    .line 5572
    if-ltz v7, :cond_ea

    #@e1
    .line 5573
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->setNextSelectedPositionInt(I)V

    #@e4
    goto/16 :goto_37

    #@e6
    .line 5580
    .end local v6           #newPos:I
    .end local v7           #selectablePos:I
    :cond_e6
    iget v10, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@e8
    if-gez v10, :cond_37

    #@ea
    .line 5588
    :cond_ea
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@ec
    if-eqz v10, :cond_108

    #@ee
    :goto_ee
    iput v8, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@f0
    .line 5589
    iput v13, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@f2
    .line 5590
    const-wide/high16 v8, -0x8000

    #@f4
    iput-wide v8, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@f6
    .line 5591
    iput v13, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@f8
    .line 5592
    const-wide/high16 v8, -0x8000

    #@fa
    iput-wide v8, p0, Landroid/widget/AdapterView;->mNextSelectedRowId:J

    #@fc
    .line 5593
    iput-boolean v12, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@fe
    .line 5594
    const/4 v8, 0x0

    #@ff
    iput-object v8, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@101
    .line 5595
    iput v13, p0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@103
    .line 5596
    invoke-virtual {p0}, Landroid/widget/AbsListView;->checkSelectionChanged()V

    #@106
    goto/16 :goto_37

    #@108
    :cond_108
    move v8, v9

    #@109
    .line 5588
    goto :goto_ee

    #@10a
    .line 5503
    :pswitch_data_10a
    .packed-switch 0x0
        :pswitch_8e
        :pswitch_c9
    .end packed-switch
.end method

.method public hasTextFilter()Z
    .registers 2

    #@0
    .prologue
    .line 5908
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@2
    return v0
.end method

.method hideSelector()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 5220
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3
    if-eq v0, v2, :cond_25

    #@5
    .line 5221
    iget v0, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@7
    const/4 v1, 0x4

    #@8
    if-eq v0, v1, :cond_e

    #@a
    .line 5222
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@c
    iput v0, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@e
    .line 5224
    :cond_e
    iget v0, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@10
    if-ltz v0, :cond_1c

    #@12
    iget v0, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@14
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@16
    if-eq v0, v1, :cond_1c

    #@18
    .line 5225
    iget v0, p0, Landroid/widget/AdapterView;->mNextSelectedPosition:I

    #@1a
    iput v0, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@1c
    .line 5227
    :cond_1c
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setSelectedPositionInt(I)V

    #@1f
    .line 5228
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setNextSelectedPositionInt(I)V

    #@22
    .line 5229
    const/4 v0, 0x0

    #@23
    iput v0, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@25
    .line 5231
    :cond_25
    return-void
.end method

.method public invalidateViews()V
    .registers 2

    #@0
    .prologue
    .line 5276
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@3
    .line 5277
    invoke-virtual {p0}, Landroid/widget/AbsListView;->rememberSyncState()V

    #@6
    .line 5278
    invoke-virtual {p0}, Landroid/widget/AbsListView;->requestLayout()V

    #@9
    .line 5279
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidate()V

    #@c
    .line 5280
    return-void
.end method

.method invokeOnItemScrollListener()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1365
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@3
    if-eqz v0, :cond_12

    #@5
    .line 1366
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@7
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@9
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@c
    move-result v2

    #@d
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@f
    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/widget/FastScroller;->onScroll(Landroid/widget/AbsListView;III)V

    #@12
    .line 1368
    :cond_12
    iget-object v0, p0, Landroid/widget/AbsListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    #@14
    if-eqz v0, :cond_23

    #@16
    .line 1369
    iget-object v0, p0, Landroid/widget/AbsListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    #@18
    iget v1, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1a
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@1d
    move-result v2

    #@1e
    iget v3, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@20
    invoke-interface {v0, p0, v1, v2, v3}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    #@23
    .line 1371
    :cond_23
    invoke-virtual {p0, v4, v4, v4, v4}, Landroid/widget/AbsListView;->onScrollChanged(IIII)V

    #@26
    .line 1372
    return-void
.end method

.method public isFastScrollAlwaysVisible()Z
    .registers 2

    #@0
    .prologue
    .line 1279
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFastScrollEnabled:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@6
    invoke-virtual {v0}, Landroid/widget/FastScroller;->isAlwaysShowEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isFastScrollEnabled()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 1297
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFastScrollEnabled:Z

    #@2
    return v0
.end method

.method protected isInFilterMode()Z
    .registers 2

    #@0
    .prologue
    .line 5711
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@2
    return v0
.end method

.method public isItemChecked(I)Z
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 920
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 921
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@a
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@d
    move-result v0

    #@e
    .line 924
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method protected isPaddingOffsetRequired()Z
    .registers 3

    #@0
    .prologue
    .line 2374
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit8 v0, v0, 0x22

    #@4
    const/16 v1, 0x22

    #@6
    if-eq v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isScrollingCacheEnabled()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 1448
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mScrollingCacheEnabled:Z

    #@2
    return v0
.end method

.method public isSmoothScrollbarEnabled()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 1348
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@2
    return v0
.end method

.method public isStackFromBottom()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 1525
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@2
    return v0
.end method

.method public isTextFilterEnabled()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 1495
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@2
    return v0
.end method

.method protected isVerticalScrollBarHidden()Z
    .registers 2

    #@0
    .prologue
    .line 1314
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@6
    invoke-virtual {v0}, Landroid/widget/FastScroller;->isVisible()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 2597
    invoke-super {p0}, Landroid/widget/AdapterView;->jumpDrawablesToCurrentState()V

    #@3
    .line 2598
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 2599
    :cond_c
    return-void
.end method

.method keyPressed()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 2498
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isEnabled()Z

    #@4
    move-result v5

    #@5
    if-eqz v5, :cond_d

    #@7
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isClickable()Z

    #@a
    move-result v5

    #@b
    if-nez v5, :cond_e

    #@d
    .line 2533
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2502
    :cond_e
    iget-object v2, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@10
    .line 2503
    .local v2, selector:Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@12
    .line 2504
    .local v3, selectorRect:Landroid/graphics/Rect;
    if-eqz v2, :cond_d

    #@14
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isFocused()Z

    #@17
    move-result v5

    #@18
    if-nez v5, :cond_20

    #@1a
    invoke-virtual {p0}, Landroid/widget/AbsListView;->touchModeDrawsInPressedState()Z

    #@1d
    move-result v5

    #@1e
    if-eqz v5, :cond_d

    #@20
    :cond_20
    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    #@23
    move-result v5

    #@24
    if-nez v5, :cond_d

    #@26
    .line 2507
    iget v5, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@28
    iget v6, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2a
    sub-int/2addr v5, v6

    #@2b
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@2e
    move-result-object v4

    #@2f
    .line 2509
    .local v4, v:Landroid/view/View;
    if-eqz v4, :cond_3a

    #@31
    .line 2510
    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    #@34
    move-result v5

    #@35
    if-nez v5, :cond_d

    #@37
    .line 2511
    invoke-virtual {v4, v7}, Landroid/view/View;->setPressed(Z)V

    #@3a
    .line 2513
    :cond_3a
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@3d
    .line 2515
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isLongClickable()Z

    #@40
    move-result v1

    #@41
    .line 2516
    .local v1, longClickable:Z
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    #@44
    move-result-object v0

    #@45
    .line 2517
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_56

    #@47
    instance-of v5, v0, Landroid/graphics/drawable/TransitionDrawable;

    #@49
    if-eqz v5, :cond_56

    #@4b
    .line 2518
    if-eqz v1, :cond_78

    #@4d
    .line 2519
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    #@4f
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@52
    move-result v5

    #@53
    invoke-virtual {v0, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    #@56
    .line 2525
    :cond_56
    :goto_56
    if-eqz v1, :cond_d

    #@58
    iget-boolean v5, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@5a
    if-nez v5, :cond_d

    #@5c
    .line 2526
    iget-object v5, p0, Landroid/widget/AbsListView;->mPendingCheckForKeyLongPress:Landroid/widget/AbsListView$CheckForKeyLongPress;

    #@5e
    if-nez v5, :cond_68

    #@60
    .line 2527
    new-instance v5, Landroid/widget/AbsListView$CheckForKeyLongPress;

    #@62
    const/4 v6, 0x0

    #@63
    invoke-direct {v5, p0, v6}, Landroid/widget/AbsListView$CheckForKeyLongPress;-><init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V

    #@66
    iput-object v5, p0, Landroid/widget/AbsListView;->mPendingCheckForKeyLongPress:Landroid/widget/AbsListView$CheckForKeyLongPress;

    #@68
    .line 2529
    :cond_68
    iget-object v5, p0, Landroid/widget/AbsListView;->mPendingCheckForKeyLongPress:Landroid/widget/AbsListView$CheckForKeyLongPress;

    #@6a
    invoke-virtual {v5}, Landroid/widget/AbsListView$CheckForKeyLongPress;->rememberWindowAttachCount()V

    #@6d
    .line 2530
    iget-object v5, p0, Landroid/widget/AbsListView;->mPendingCheckForKeyLongPress:Landroid/widget/AbsListView$CheckForKeyLongPress;

    #@6f
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@72
    move-result v6

    #@73
    int-to-long v6, v6

    #@74
    invoke-virtual {p0, v5, v6, v7}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@77
    goto :goto_d

    #@78
    .line 2522
    .restart local v0       #d:Landroid/graphics/drawable/Drawable;
    :cond_78
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    #@7a
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    #@7d
    goto :goto_56
.end method

.method protected layoutChildren()V
    .registers 1

    #@0
    .prologue
    .line 2053
    return-void
.end method

.method obtainView(I[Z)Landroid/view/View;
    .registers 10
    .parameter "position"
    .parameter "isScrap"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2160
    aput-boolean v5, p2, v5

    #@4
    .line 2163
    iget-object v4, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@6
    invoke-virtual {v4, p1}, Landroid/widget/AbsListView$RecycleBin;->getTransientStateView(I)Landroid/view/View;

    #@9
    move-result-object v2

    #@a
    .line 2164
    .local v2, scrapView:Landroid/view/View;
    if-eqz v2, :cond_e

    #@c
    move-object v0, v2

    #@d
    .line 2222
    :cond_d
    :goto_d
    return-object v0

    #@e
    .line 2168
    :cond_e
    iget-object v4, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@10
    invoke-virtual {v4, p1}, Landroid/widget/AbsListView$RecycleBin;->getScrapView(I)Landroid/view/View;

    #@13
    move-result-object v2

    #@14
    .line 2171
    if-eqz v2, :cond_79

    #@16
    .line 2172
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@18
    invoke-interface {v4, p1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@1b
    move-result-object v0

    #@1c
    .line 2174
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_25

    #@22
    .line 2175
    invoke-virtual {v0, v6}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@25
    .line 2178
    :cond_25
    if-eq v0, v2, :cond_73

    #@27
    .line 2179
    iget-object v4, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@29
    invoke-virtual {v4, v2, p1}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@2c
    .line 2180
    iget v4, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@2e
    if-eqz v4, :cond_35

    #@30
    .line 2181
    iget v4, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@32
    invoke-virtual {v0, v4}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    #@35
    .line 2199
    :cond_35
    :goto_35
    iget-boolean v4, p0, Landroid/widget/AbsListView;->mAdapterHasStableIds:Z

    #@37
    if-eqz v4, :cond_50

    #@39
    .line 2200
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3c
    move-result-object v3

    #@3d
    .line 2202
    .local v3, vlp:Landroid/view/ViewGroup$LayoutParams;
    if-nez v3, :cond_93

    #@3f
    .line 2203
    invoke-virtual {p0}, Landroid/widget/AbsListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@42
    move-result-object v1

    #@43
    check-cast v1, Landroid/widget/AbsListView$LayoutParams;

    #@45
    .line 2209
    .local v1, lp:Landroid/widget/AbsListView$LayoutParams;
    :goto_45
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@47
    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@4a
    move-result-wide v4

    #@4b
    iput-wide v4, v1, Landroid/widget/AbsListView$LayoutParams;->itemId:J

    #@4d
    .line 2210
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@50
    .line 2213
    .end local v1           #lp:Landroid/widget/AbsListView$LayoutParams;
    .end local v3           #vlp:Landroid/view/ViewGroup$LayoutParams;
    :cond_50
    iget-object v4, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@52
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_d

    #@5c
    .line 2214
    iget-object v4, p0, Landroid/widget/AbsListView;->mAccessibilityDelegate:Landroid/widget/AbsListView$ListItemAccessibilityDelegate;

    #@5e
    if-nez v4, :cond_67

    #@60
    .line 2215
    new-instance v4, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;

    #@62
    invoke-direct {v4, p0}, Landroid/widget/AbsListView$ListItemAccessibilityDelegate;-><init>(Landroid/widget/AbsListView;)V

    #@65
    iput-object v4, p0, Landroid/widget/AbsListView;->mAccessibilityDelegate:Landroid/widget/AbsListView$ListItemAccessibilityDelegate;

    #@67
    .line 2217
    :cond_67
    invoke-virtual {v0}, Landroid/view/View;->getAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    #@6a
    move-result-object v4

    #@6b
    if-nez v4, :cond_d

    #@6d
    .line 2218
    iget-object v4, p0, Landroid/widget/AbsListView;->mAccessibilityDelegate:Landroid/widget/AbsListView$ListItemAccessibilityDelegate;

    #@6f
    invoke-virtual {v0, v4}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    #@72
    goto :goto_d

    #@73
    .line 2184
    :cond_73
    aput-boolean v6, p2, v5

    #@75
    .line 2185
    invoke-virtual {v0}, Landroid/view/View;->dispatchFinishTemporaryDetach()V

    #@78
    goto :goto_35

    #@79
    .line 2188
    .end local v0           #child:Landroid/view/View;
    :cond_79
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@7b
    const/4 v5, 0x0

    #@7c
    invoke-interface {v4, p1, v5, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@7f
    move-result-object v0

    #@80
    .line 2190
    .restart local v0       #child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    #@83
    move-result v4

    #@84
    if-nez v4, :cond_89

    #@86
    .line 2191
    invoke-virtual {v0, v6}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@89
    .line 2194
    :cond_89
    iget v4, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@8b
    if-eqz v4, :cond_35

    #@8d
    .line 2195
    iget v4, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@8f
    invoke-virtual {v0, v4}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    #@92
    goto :goto_35

    #@93
    .line 2204
    .restart local v3       #vlp:Landroid/view/ViewGroup$LayoutParams;
    :cond_93
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    #@96
    move-result v4

    #@97
    if-nez v4, :cond_a0

    #@99
    .line 2205
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    #@9c
    move-result-object v1

    #@9d
    check-cast v1, Landroid/widget/AbsListView$LayoutParams;

    #@9f
    .restart local v1       #lp:Landroid/widget/AbsListView$LayoutParams;
    goto :goto_45

    #@a0
    .end local v1           #lp:Landroid/widget/AbsListView$LayoutParams;
    :cond_a0
    move-object v1, v3

    #@a1
    .line 2207
    check-cast v1, Landroid/widget/AbsListView$LayoutParams;

    #@a3
    .restart local v1       #lp:Landroid/widget/AbsListView$LayoutParams;
    goto :goto_45
.end method

.method protected onAttachedToWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2603
    invoke-super {p0}, Landroid/widget/AdapterView;->onAttachedToWindow()V

    #@4
    .line 2605
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@7
    move-result-object v0

    #@8
    .line 2606
    .local v0, treeObserver:Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@b
    .line 2607
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@d
    if-eqz v1, :cond_1a

    #@f
    iget-object v1, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@11
    if-eqz v1, :cond_1a

    #@13
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mGlobalLayoutListenerAddedFilter:Z

    #@15
    if-nez v1, :cond_1a

    #@17
    .line 2608
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@1a
    .line 2611
    :cond_1a
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@1c
    if-eqz v1, :cond_3e

    #@1e
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@20
    if-nez v1, :cond_3e

    #@22
    .line 2612
    new-instance v1, Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@24
    invoke-direct {v1, p0}, Landroid/widget/AbsListView$AdapterDataSetObserver;-><init>(Landroid/widget/AbsListView;)V

    #@27
    iput-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@29
    .line 2613
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2b
    iget-object v2, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@2d
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@30
    .line 2616
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@32
    .line 2617
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@34
    iput v1, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@36
    .line 2618
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@38
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    #@3b
    move-result v1

    #@3c
    iput v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3e
    .line 2620
    :cond_3e
    iput-boolean v3, p0, Landroid/widget/AbsListView;->mIsAttached:Z

    #@40
    .line 2621
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 8
    .parameter "extraSpace"

    #@0
    .prologue
    .line 2559
    iget-boolean v4, p0, Landroid/widget/AbsListView;->mIsChildViewEnabled:Z

    #@2
    if-eqz v4, :cond_9

    #@4
    .line 2561
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    #@7
    move-result-object v3

    #@8
    .line 2587
    :cond_8
    :goto_8
    return-object v3

    #@9
    .line 2567
    :cond_9
    sget-object v4, Landroid/widget/AbsListView;->ENABLED_STATE_SET:[I

    #@b
    const/4 v5, 0x0

    #@c
    aget v1, v4, v5

    #@e
    .line 2572
    .local v1, enabledState:I
    add-int/lit8 v4, p1, 0x1

    #@10
    invoke-super {p0, v4}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    #@13
    move-result-object v3

    #@14
    .line 2573
    .local v3, state:[I
    const/4 v0, -0x1

    #@15
    .line 2574
    .local v0, enabledPos:I
    array-length v4, v3

    #@16
    add-int/lit8 v2, v4, -0x1

    #@18
    .local v2, i:I
    :goto_18
    if-ltz v2, :cond_1f

    #@1a
    .line 2575
    aget v4, v3, v2

    #@1c
    if-ne v4, v1, :cond_2b

    #@1e
    .line 2576
    move v0, v2

    #@1f
    .line 2582
    :cond_1f
    if-ltz v0, :cond_8

    #@21
    .line 2583
    add-int/lit8 v4, v0, 0x1

    #@23
    array-length v5, v3

    #@24
    sub-int/2addr v5, v0

    #@25
    add-int/lit8 v5, v5, -0x1

    #@27
    invoke-static {v3, v4, v3, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2a
    goto :goto_8

    #@2b
    .line 2574
    :cond_2b
    add-int/lit8 v2, v2, -0x1

    #@2d
    goto :goto_18
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 5
    .parameter "outAttrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5795
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isTextFilterEnabled()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_2d

    #@7
    .line 5799
    invoke-direct {p0, v1}, Landroid/widget/AbsListView;->createTextFilter(Z)V

    #@a
    .line 5800
    iget-object v0, p0, Landroid/widget/AbsListView;->mPublicInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    #@c
    if-nez v0, :cond_23

    #@e
    .line 5801
    new-instance v0, Landroid/view/inputmethod/BaseInputConnection;

    #@10
    invoke-direct {v0, p0, v1}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    #@13
    iput-object v0, p0, Landroid/widget/AbsListView;->mDefInputConnection:Landroid/view/inputmethod/InputConnection;

    #@15
    .line 5802
    new-instance v0, Landroid/widget/AbsListView$3;

    #@17
    iget-object v1, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@19
    invoke-virtual {v1, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    #@1c
    move-result-object v1

    #@1d
    const/4 v2, 0x1

    #@1e
    invoke-direct {v0, p0, v1, v2}, Landroid/widget/AbsListView$3;-><init>(Landroid/widget/AbsListView;Landroid/view/inputmethod/InputConnection;Z)V

    #@21
    iput-object v0, p0, Landroid/widget/AbsListView;->mPublicInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    #@23
    .line 5836
    :cond_23
    const/16 v0, 0xb1

    #@25
    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@27
    .line 5838
    const/4 v0, 0x6

    #@28
    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@2a
    .line 5839
    iget-object v0, p0, Landroid/widget/AbsListView;->mPublicInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    #@2c
    .line 5841
    :goto_2c
    return-object v0

    #@2d
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_2c
.end method

.method protected onDetachedFromWindow()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 2625
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    #@5
    .line 2628
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@8
    .line 2631
    iget-object v1, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@a
    invoke-virtual {v1}, Landroid/widget/AbsListView$RecycleBin;->clear()V

    #@d
    .line 2633
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@10
    move-result-object v0

    #@11
    .line 2634
    .local v0, treeObserver:Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@14
    .line 2635
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@16
    if-eqz v1, :cond_21

    #@18
    iget-object v1, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@1a
    if-eqz v1, :cond_21

    #@1c
    .line 2636
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@1f
    .line 2637
    iput-boolean v4, p0, Landroid/widget/AbsListView;->mGlobalLayoutListenerAddedFilter:Z

    #@21
    .line 2640
    :cond_21
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@23
    if-eqz v1, :cond_2e

    #@25
    .line 2641
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@27
    iget-object v2, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@29
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@2c
    .line 2642
    iput-object v3, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@2e
    .line 2645
    :cond_2e
    iget-object v1, p0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@30
    if-eqz v1, :cond_39

    #@32
    .line 2646
    iget-object v1, p0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@34
    invoke-virtual {v1}, Landroid/os/StrictMode$Span;->finish()V

    #@37
    .line 2647
    iput-object v3, p0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@39
    .line 2650
    :cond_39
    iget-object v1, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@3b
    if-eqz v1, :cond_44

    #@3d
    .line 2651
    iget-object v1, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@3f
    invoke-virtual {v1}, Landroid/os/StrictMode$Span;->finish()V

    #@42
    .line 2652
    iput-object v3, p0, Landroid/widget/AbsListView;->mFlingStrictSpan:Landroid/os/StrictMode$Span;

    #@44
    .line 2655
    :cond_44
    iget-object v1, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@46
    if-eqz v1, :cond_4d

    #@48
    .line 2656
    iget-object v1, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@4a
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@4d
    .line 2659
    :cond_4d
    iget-object v1, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@4f
    if-eqz v1, :cond_56

    #@51
    .line 2660
    iget-object v1, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@53
    invoke-virtual {v1}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@56
    .line 2663
    :cond_56
    iget-object v1, p0, Landroid/widget/AbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    #@58
    if-eqz v1, :cond_5f

    #@5a
    .line 2664
    iget-object v1, p0, Landroid/widget/AbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    #@5c
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@5f
    .line 2667
    :cond_5f
    iget-object v1, p0, Landroid/widget/AbsListView;->mPerformClick:Landroid/widget/AbsListView$PerformClick;

    #@61
    if-eqz v1, :cond_68

    #@63
    .line 2668
    iget-object v1, p0, Landroid/widget/AbsListView;->mPerformClick:Landroid/widget/AbsListView$PerformClick;

    #@65
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@68
    .line 2671
    :cond_68
    iget-object v1, p0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@6a
    if-eqz v1, :cond_76

    #@6c
    .line 2672
    iget-object v1, p0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@6e
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@71
    .line 2673
    iget-object v1, p0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@73
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    #@76
    .line 2675
    :cond_76
    iput-boolean v4, p0, Landroid/widget/AbsListView;->mIsAttached:Z

    #@78
    .line 2676
    return-void
.end method

.method protected onDisplayHint(I)V
    .registers 3
    .parameter "hint"

    #@0
    .prologue
    .line 5601
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onDisplayHint(I)V

    #@3
    .line 5602
    sparse-switch p1, :sswitch_data_34

    #@6
    .line 5614
    :cond_6
    :goto_6
    const/4 v0, 0x4

    #@7
    if-ne p1, v0, :cond_31

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/widget/AbsListView;->mPopupHidden:Z

    #@c
    .line 5615
    return-void

    #@d
    .line 5604
    :sswitch_d
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@f
    if-eqz v0, :cond_6

    #@11
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@13
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_6

    #@19
    .line 5605
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@1c
    goto :goto_6

    #@1d
    .line 5609
    :sswitch_1d
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@1f
    if-eqz v0, :cond_6

    #@21
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@23
    if-eqz v0, :cond_6

    #@25
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@27
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@2a
    move-result v0

    #@2b
    if-nez v0, :cond_6

    #@2d
    .line 5610
    invoke-direct {p0}, Landroid/widget/AbsListView;->showPopup()V

    #@30
    goto :goto_6

    #@31
    .line 5614
    :cond_31
    const/4 v0, 0x0

    #@32
    goto :goto_a

    #@33
    .line 5602
    nop

    #@34
    :sswitch_data_34
    .sparse-switch
        0x0 -> :sswitch_1d
        0x4 -> :sswitch_d
    .end sparse-switch
.end method

.method public onFilterComplete(I)V
    .registers 3
    .parameter "count"

    #@0
    .prologue
    .line 5972
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    if-gez v0, :cond_c

    #@4
    if-lez p1, :cond_c

    #@6
    .line 5973
    const/4 v0, -0x1

    #@7
    iput v0, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@9
    .line 5974
    invoke-virtual {p0}, Landroid/widget/AbsListView;->resurrectSelection()Z

    #@c
    .line 5976
    :cond_c
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 5
    .parameter "gainFocus"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 1832
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AdapterView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@3
    .line 1833
    if-eqz p1, :cond_29

    #@5
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@7
    if-gez v0, :cond_29

    #@9
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isInTouchMode()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_29

    #@f
    .line 1834
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mIsAttached:Z

    #@11
    if-nez v0, :cond_26

    #@13
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@15
    if-eqz v0, :cond_26

    #@17
    .line 1837
    const/4 v0, 0x1

    #@18
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@1a
    .line 1838
    iget v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@1c
    iput v0, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@1e
    .line 1839
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@20
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    #@23
    move-result v0

    #@24
    iput v0, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@26
    .line 1841
    :cond_26
    invoke-virtual {p0}, Landroid/widget/AbsListView;->resurrectSelection()Z

    #@29
    .line 1843
    :cond_29
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 3843
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@3
    move-result v2

    #@4
    and-int/lit8 v2, v2, 0x2

    #@6
    if-eqz v2, :cond_f

    #@8
    .line 3844
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@b
    move-result v2

    #@c
    packed-switch v2, :pswitch_data_32

    #@f
    .line 3858
    :cond_f
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v2

    #@13
    :goto_13
    return v2

    #@14
    .line 3846
    :pswitch_14
    iget v2, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@16
    const/4 v3, -0x1

    #@17
    if-ne v2, v3, :cond_f

    #@19
    .line 3847
    const/16 v2, 0x9

    #@1b
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@1e
    move-result v1

    #@1f
    .line 3848
    .local v1, vscroll:F
    const/4 v2, 0x0

    #@20
    cmpl-float v2, v1, v2

    #@22
    if-eqz v2, :cond_f

    #@24
    .line 3849
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getVerticalScrollFactor()F

    #@27
    move-result v2

    #@28
    mul-float/2addr v2, v1

    #@29
    float-to-int v0, v2

    #@2a
    .line 3850
    .local v0, delta:I
    invoke-virtual {p0, v0, v0}, Landroid/widget/AbsListView;->trackMotionScroll(II)Z

    #@2d
    move-result v2

    #@2e
    if-nez v2, :cond_f

    #@30
    .line 3851
    const/4 v2, 0x1

    #@31
    goto :goto_13

    #@32
    .line 3844
    :pswitch_data_32
    .packed-switch 0x8
        :pswitch_14
    .end packed-switch
.end method

.method public onGlobalLayout()V
    .registers 2

    #@0
    .prologue
    .line 5912
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isShown()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1e

    #@6
    .line 5914
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@8
    if-eqz v0, :cond_1d

    #@a
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@c
    if-eqz v0, :cond_1d

    #@e
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@10
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1d

    #@16
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mPopupHidden:Z

    #@18
    if-nez v0, :cond_1d

    #@1a
    .line 5915
    invoke-direct {p0}, Landroid/widget/AbsListView;->showPopup()V

    #@1d
    .line 5924
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 5919
    :cond_1e
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@20
    if-eqz v0, :cond_1d

    #@22
    iget-object v0, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@24
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_1d

    #@2a
    .line 5920
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@2d
    goto :goto_1d
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1395
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1396
    const-class v0, Landroid/widget/AbsListView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1397
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 1401
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1402
    const-class v0, Landroid/widget/AbsListView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1403
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isEnabled()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_2e

    #@12
    .line 1404
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    #@15
    move-result v0

    #@16
    if-lez v0, :cond_1d

    #@18
    .line 1405
    const/16 v0, 0x2000

    #@1a
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@1d
    .line 1407
    :cond_1d
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    #@20
    move-result v0

    #@21
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getCount()I

    #@24
    move-result v1

    #@25
    add-int/lit8 v1, v1, -0x1

    #@27
    if-ge v0, v1, :cond_2e

    #@29
    .line 1408
    const/16 v0, 0x1000

    #@2b
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@2e
    .line 1411
    :cond_2e
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    #@0
    .prologue
    const/4 v12, 0x4

    #@1
    const/4 v11, -0x1

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 3954
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@7
    move-result v0

    #@8
    .line 3957
    .local v0, action:I
    iget-object v10, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@a
    if-eqz v10, :cond_11

    #@c
    .line 3958
    iget-object v10, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@e
    invoke-virtual {v10}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@11
    .line 3961
    :cond_11
    iget-boolean v10, p0, Landroid/widget/AbsListView;->mIsAttached:Z

    #@13
    if-nez v10, :cond_16

    #@15
    .line 4047
    :cond_15
    :goto_15
    return v8

    #@16
    .line 3969
    :cond_16
    iget-object v10, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@18
    if-eqz v10, :cond_24

    #@1a
    .line 3970
    iget-object v10, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@1c
    invoke-virtual {v10, p1}, Landroid/widget/FastScroller;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@1f
    move-result v1

    #@20
    .line 3971
    .local v1, intercepted:Z
    if-eqz v1, :cond_24

    #@22
    move v8, v9

    #@23
    .line 3972
    goto :goto_15

    #@24
    .line 3976
    .end local v1           #intercepted:Z
    :cond_24
    and-int/lit16 v10, v0, 0xff

    #@26
    packed-switch v10, :pswitch_data_b6

    #@29
    :pswitch_29
    goto :goto_15

    #@2a
    .line 3978
    :pswitch_2a
    iget v4, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@2c
    .line 3979
    .local v4, touchMode:I
    const/4 v10, 0x6

    #@2d
    if-eq v4, v10, :cond_32

    #@2f
    const/4 v10, 0x5

    #@30
    if-ne v4, v10, :cond_36

    #@32
    .line 3980
    :cond_32
    iput v8, p0, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@34
    move v8, v9

    #@35
    .line 3981
    goto :goto_15

    #@36
    .line 3984
    :cond_36
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@39
    move-result v10

    #@3a
    float-to-int v6, v10

    #@3b
    .line 3985
    .local v6, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@3e
    move-result v10

    #@3f
    float-to-int v7, v10

    #@40
    .line 3986
    .local v7, y:I
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@43
    move-result v10

    #@44
    iput v10, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@46
    .line 3988
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->findMotionRow(I)I

    #@49
    move-result v2

    #@4a
    .line 3989
    .local v2, motionPosition:I
    if-eq v4, v12, :cond_69

    #@4c
    if-ltz v2, :cond_69

    #@4e
    .line 3992
    iget v10, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@50
    sub-int v10, v2, v10

    #@52
    invoke-virtual {p0, v10}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@55
    move-result-object v5

    #@56
    .line 3994
    .local v5, v:Landroid/view/View;
    if-eqz v5, :cond_64

    #@58
    .line 3995
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@5b
    move-result v10

    #@5c
    iput v10, p0, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@5e
    .line 3996
    iput v6, p0, Landroid/widget/AbsListView;->mMotionX:I

    #@60
    .line 3997
    iput v7, p0, Landroid/widget/AbsListView;->mMotionY:I

    #@62
    .line 3998
    iput v2, p0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@64
    .line 4001
    :cond_64
    iput v8, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@66
    .line 4002
    invoke-direct {p0}, Landroid/widget/AbsListView;->clearScrollingCache()V

    #@69
    .line 4004
    .end local v5           #v:Landroid/view/View;
    :cond_69
    const/high16 v10, -0x8000

    #@6b
    iput v10, p0, Landroid/widget/AbsListView;->mLastY:I

    #@6d
    .line 4005
    invoke-direct {p0}, Landroid/widget/AbsListView;->initOrResetVelocityTracker()V

    #@70
    .line 4006
    iget-object v10, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@72
    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@75
    .line 4007
    if-ne v4, v12, :cond_15

    #@77
    move v8, v9

    #@78
    .line 4008
    goto :goto_15

    #@79
    .line 4014
    .end local v2           #motionPosition:I
    .end local v4           #touchMode:I
    .end local v6           #x:I
    .end local v7           #y:I
    :pswitch_79
    iget v10, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@7b
    packed-switch v10, :pswitch_data_c8

    #@7e
    goto :goto_15

    #@7f
    .line 4016
    :pswitch_7f
    iget v10, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@81
    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@84
    move-result v3

    #@85
    .line 4017
    .local v3, pointerIndex:I
    if-ne v3, v11, :cond_8e

    #@87
    .line 4018
    const/4 v3, 0x0

    #@88
    .line 4019
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@8b
    move-result v10

    #@8c
    iput v10, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@8e
    .line 4021
    :cond_8e
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    #@91
    move-result v10

    #@92
    float-to-int v7, v10

    #@93
    .line 4022
    .restart local v7       #y:I
    invoke-direct {p0}, Landroid/widget/AbsListView;->initVelocityTrackerIfNotExists()V

    #@96
    .line 4023
    iget-object v10, p0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@98
    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@9b
    .line 4024
    invoke-direct {p0, v7}, Landroid/widget/AbsListView;->startScrollIfNeeded(I)Z

    #@9e
    move-result v10

    #@9f
    if-eqz v10, :cond_15

    #@a1
    move v8, v9

    #@a2
    .line 4025
    goto/16 :goto_15

    #@a4
    .line 4034
    .end local v3           #pointerIndex:I
    .end local v7           #y:I
    :pswitch_a4
    iput v11, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@a6
    .line 4035
    iput v11, p0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@a8
    .line 4036
    invoke-direct {p0}, Landroid/widget/AbsListView;->recycleVelocityTracker()V

    #@ab
    .line 4037
    invoke-virtual {p0, v8}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@ae
    goto/16 :goto_15

    #@b0
    .line 4042
    :pswitch_b0
    invoke-direct {p0, p1}, Landroid/widget/AbsListView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@b3
    goto/16 :goto_15

    #@b5
    .line 3976
    nop

    #@b6
    :pswitch_data_b6
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_a4
        :pswitch_79
        :pswitch_a4
        :pswitch_29
        :pswitch_29
        :pswitch_b0
    .end packed-switch

    #@c8
    .line 4014
    :pswitch_data_c8
    .packed-switch 0x0
        :pswitch_7f
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2906
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 9
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2911
    sparse-switch p1, :sswitch_data_48

    #@5
    .line 2931
    :cond_5
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@8
    move-result v1

    #@9
    :cond_9
    :goto_9
    return v1

    #@a
    .line 2914
    :sswitch_a
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isEnabled()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_9

    #@10
    .line 2917
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isClickable()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_5

    #@16
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isPressed()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_5

    #@1c
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1e
    if-ltz v2, :cond_5

    #@20
    iget-object v2, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@22
    if-eqz v2, :cond_5

    #@24
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@26
    iget-object v3, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@28
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    #@2b
    move-result v3

    #@2c
    if-ge v2, v3, :cond_5

    #@2e
    .line 2921
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@30
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@32
    sub-int/2addr v2, v3

    #@33
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@36
    move-result-object v0

    #@37
    .line 2922
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_43

    #@39
    .line 2923
    iget v2, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3b
    iget-wide v3, p0, Landroid/widget/AdapterView;->mSelectedRowId:J

    #@3d
    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/widget/AbsListView;->performItemClick(Landroid/view/View;IJ)Z

    #@40
    .line 2924
    invoke-virtual {v0, v5}, Landroid/view/View;->setPressed(Z)V

    #@43
    .line 2926
    :cond_43
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@46
    goto :goto_9

    #@47
    .line 2911
    nop

    #@48
    :sswitch_data_48
    .sparse-switch
        0x17 -> :sswitch_a
        0x42 -> :sswitch_a
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 11
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 2009
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    #@3
    .line 2010
    const/4 v2, 0x1

    #@4
    iput-boolean v2, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@6
    .line 2011
    if-eqz p1, :cond_1e

    #@8
    .line 2012
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 2013
    .local v0, childCount:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_19

    #@f
    .line 2014
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    #@16
    .line 2013
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_d

    #@19
    .line 2016
    :cond_19
    iget-object v2, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@1b
    invoke-virtual {v2}, Landroid/widget/AbsListView$RecycleBin;->markChildrenDirty()V

    #@1e
    .line 2019
    .end local v0           #childCount:I
    .end local v1           #i:I
    :cond_1e
    iget-object v2, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@20
    if-eqz v2, :cond_31

    #@22
    iget v2, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@24
    iget v3, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@26
    if-eq v2, v3, :cond_31

    #@28
    .line 2020
    iget-object v2, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@2a
    iget v3, p0, Landroid/widget/AdapterView;->mOldItemCount:I

    #@2c
    iget v4, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@2e
    invoke-virtual {v2, v3, v4}, Landroid/widget/FastScroller;->onItemCountChanged(II)V

    #@31
    .line 2023
    :cond_31
    invoke-virtual {p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@34
    .line 2024
    const/4 v2, 0x0

    #@35
    iput-boolean v2, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@37
    .line 2026
    sub-int v2, p5, p3

    #@39
    div-int/lit8 v2, v2, 0x3

    #@3b
    iput v2, p0, Landroid/widget/AbsListView;->mOverscrollMax:I

    #@3d
    .line 2027
    return-void
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1983
    iget-object v6, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@3
    if-nez v6, :cond_8

    #@5
    .line 1984
    invoke-direct {p0}, Landroid/widget/AbsListView;->useDefaultSelector()V

    #@8
    .line 1986
    :cond_8
    iget-object v4, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@a
    .line 1987
    .local v4, listPadding:Landroid/graphics/Rect;
    iget v6, p0, Landroid/widget/AbsListView;->mSelectionLeftPadding:I

    #@c
    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    #@e
    add-int/2addr v6, v7

    #@f
    iput v6, v4, Landroid/graphics/Rect;->left:I

    #@11
    .line 1988
    iget v6, p0, Landroid/widget/AbsListView;->mSelectionTopPadding:I

    #@13
    iget v7, p0, Landroid/view/View;->mPaddingTop:I

    #@15
    add-int/2addr v6, v7

    #@16
    iput v6, v4, Landroid/graphics/Rect;->top:I

    #@18
    .line 1989
    iget v6, p0, Landroid/widget/AbsListView;->mSelectionRightPadding:I

    #@1a
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@1c
    add-int/2addr v6, v7

    #@1d
    iput v6, v4, Landroid/graphics/Rect;->right:I

    #@1f
    .line 1990
    iget v6, p0, Landroid/widget/AbsListView;->mSelectionBottomPadding:I

    #@21
    iget v7, p0, Landroid/view/View;->mPaddingBottom:I

    #@23
    add-int/2addr v6, v7

    #@24
    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    #@26
    .line 1993
    iget v6, p0, Landroid/widget/AbsListView;->mTranscriptMode:I

    #@28
    if-ne v6, v5, :cond_4f

    #@2a
    .line 1994
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@2d
    move-result v0

    #@2e
    .line 1995
    .local v0, childCount:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@31
    move-result v6

    #@32
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getPaddingBottom()I

    #@35
    move-result v7

    #@36
    sub-int v3, v6, v7

    #@38
    .line 1996
    .local v3, listBottom:I
    add-int/lit8 v6, v0, -0x1

    #@3a
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@3d
    move-result-object v2

    #@3e
    .line 1997
    .local v2, lastChild:Landroid/view/View;
    if-eqz v2, :cond_50

    #@40
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    #@43
    move-result v1

    #@44
    .line 1998
    .local v1, lastBottom:I
    :goto_44
    iget v6, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@46
    add-int/2addr v6, v0

    #@47
    iget v7, p0, Landroid/widget/AbsListView;->mLastHandledItemCount:I

    #@49
    if-lt v6, v7, :cond_52

    #@4b
    if-gt v1, v3, :cond_52

    #@4d
    :goto_4d
    iput-boolean v5, p0, Landroid/widget/AbsListView;->mForceTranscriptScroll:Z

    #@4f
    .line 2001
    .end local v0           #childCount:I
    .end local v1           #lastBottom:I
    .end local v2           #lastChild:Landroid/view/View;
    .end local v3           #listBottom:I
    :cond_4f
    return-void

    #@50
    .restart local v0       #childCount:I
    .restart local v2       #lastChild:Landroid/view/View;
    .restart local v3       #listBottom:I
    :cond_50
    move v1, v3

    #@51
    .line 1997
    goto :goto_44

    #@52
    .line 1998
    .restart local v1       #lastBottom:I
    :cond_52
    const/4 v5, 0x0

    #@53
    goto :goto_4d
.end method

.method protected onOverScrolled(IIZZ)V
    .registers 8
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "clampedX"
    .parameter "clampedY"

    #@0
    .prologue
    .line 3832
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    if-eq v0, p2, :cond_15

    #@4
    .line 3833
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@6
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@8
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@a
    invoke-virtual {p0, v0, p2, v1, v2}, Landroid/widget/AbsListView;->onScrollChanged(IIII)V

    #@d
    .line 3834
    iput p2, p0, Landroid/view/View;->mScrollY:I

    #@f
    .line 3835
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidateParentIfNeeded()V

    #@12
    .line 3837
    invoke-virtual {p0}, Landroid/widget/AbsListView;->awakenScrollBars()Z

    #@15
    .line 3839
    :cond_15
    return-void
.end method

.method public onRemoteAdapterConnected()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6147
    iget-object v1, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@3
    iget-object v2, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@5
    if-eq v1, v2, :cond_18

    #@7
    .line 6148
    iget-object v1, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@9
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@c
    .line 6149
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mDeferNotifyDataSetChanged:Z

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 6150
    iget-object v1, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@12
    invoke-virtual {v1}, Landroid/widget/RemoteViewsAdapter;->notifyDataSetChanged()V

    #@15
    .line 6151
    iput-boolean v0, p0, Landroid/widget/AbsListView;->mDeferNotifyDataSetChanged:Z

    #@17
    .line 6158
    :cond_17
    :goto_17
    return v0

    #@18
    .line 6154
    :cond_18
    iget-object v1, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@1a
    if-eqz v1, :cond_17

    #@1c
    .line 6155
    iget-object v0, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@1e
    invoke-virtual {v0}, Landroid/widget/RemoteViewsAdapter;->superNotifyDataSetChanged()V

    #@21
    .line 6156
    const/4 v0, 0x1

    #@22
    goto :goto_17
.end method

.method public onRemoteAdapterDisconnected()V
    .registers 1

    #@0
    .prologue
    .line 6170
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 9
    .parameter "state"

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    const/4 v4, -0x1

    #@3
    const/4 v3, 0x1

    #@4
    .line 1739
    move-object v0, p1

    #@5
    check-cast v0, Landroid/widget/AbsListView$SavedState;

    #@7
    .line 1741
    .local v0, ss:Landroid/widget/AbsListView$SavedState;
    invoke-virtual {v0}, Landroid/widget/AbsListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@a
    move-result-object v1

    #@b
    invoke-super {p0, v1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@e
    .line 1742
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@10
    .line 1744
    iget v1, v0, Landroid/widget/AbsListView$SavedState;->height:I

    #@12
    int-to-long v1, v1

    #@13
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncHeight:J

    #@15
    .line 1746
    iget-wide v1, v0, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@17
    cmp-long v1, v1, v5

    #@19
    if-ltz v1, :cond_60

    #@1b
    .line 1747
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@1d
    .line 1748
    iput-object v0, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@1f
    .line 1749
    iget-wide v1, v0, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@21
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@23
    .line 1750
    iget v1, v0, Landroid/widget/AbsListView$SavedState;->position:I

    #@25
    iput v1, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@27
    .line 1751
    iget v1, v0, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@29
    iput v1, p0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@2b
    .line 1752
    const/4 v1, 0x0

    #@2c
    iput v1, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@2e
    .line 1766
    :cond_2e
    :goto_2e
    iget-object v1, v0, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@30
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setFilterText(Ljava/lang/String;)V

    #@33
    .line 1768
    iget-object v1, v0, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@35
    if-eqz v1, :cond_3b

    #@37
    .line 1769
    iget-object v1, v0, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@39
    iput-object v1, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@3b
    .line 1772
    :cond_3b
    iget-object v1, v0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@3d
    if-eqz v1, :cond_43

    #@3f
    .line 1773
    iget-object v1, v0, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@41
    iput-object v1, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@43
    .line 1776
    :cond_43
    iget v1, v0, Landroid/widget/AbsListView$SavedState;->checkedItemCount:I

    #@45
    iput v1, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@47
    .line 1778
    iget-boolean v1, v0, Landroid/widget/AbsListView$SavedState;->inActionMode:Z

    #@49
    if-eqz v1, :cond_5c

    #@4b
    iget v1, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@4d
    const/4 v2, 0x3

    #@4e
    if-ne v1, v2, :cond_5c

    #@50
    iget-object v1, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@52
    if-eqz v1, :cond_5c

    #@54
    .line 1780
    iget-object v1, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@56
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@59
    move-result-object v1

    #@5a
    iput-object v1, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@5c
    .line 1783
    :cond_5c
    invoke-virtual {p0}, Landroid/widget/AbsListView;->requestLayout()V

    #@5f
    .line 1784
    return-void

    #@60
    .line 1753
    :cond_60
    iget-wide v1, v0, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@62
    cmp-long v1, v1, v5

    #@64
    if-ltz v1, :cond_2e

    #@66
    .line 1754
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->setSelectedPositionInt(I)V

    #@69
    .line 1756
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->setNextSelectedPositionInt(I)V

    #@6c
    .line 1757
    iput v4, p0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@6e
    .line 1758
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@70
    .line 1759
    iput-object v0, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@72
    .line 1760
    iget-wide v1, v0, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@74
    iput-wide v1, p0, Landroid/widget/AdapterView;->mSyncRowId:J

    #@76
    .line 1761
    iget v1, v0, Landroid/widget/AbsListView$SavedState;->position:I

    #@78
    iput v1, p0, Landroid/widget/AdapterView;->mSyncPosition:I

    #@7a
    .line 1762
    iget v1, v0, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@7c
    iput v1, p0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@7e
    .line 1763
    iput v3, p0, Landroid/widget/AdapterView;->mSyncMode:I

    #@80
    goto :goto_2e
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 16

    #@0
    .prologue
    .line 1647
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@3
    .line 1649
    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@6
    move-result-object v9

    #@7
    .line 1651
    .local v9, superState:Landroid/os/Parcelable;
    new-instance v8, Landroid/widget/AbsListView$SavedState;

    #@9
    invoke-direct {v8, v9}, Landroid/widget/AbsListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@c
    .line 1653
    .local v8, ss:Landroid/widget/AbsListView$SavedState;
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@e
    if-eqz v12, :cond_4d

    #@10
    .line 1655
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@12
    iget-wide v12, v12, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@14
    iput-wide v12, v8, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@16
    .line 1656
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@18
    iget-wide v12, v12, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@1a
    iput-wide v12, v8, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@1c
    .line 1657
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@1e
    iget v12, v12, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@20
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@22
    .line 1658
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@24
    iget v12, v12, Landroid/widget/AbsListView$SavedState;->position:I

    #@26
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->position:I

    #@28
    .line 1659
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@2a
    iget v12, v12, Landroid/widget/AbsListView$SavedState;->height:I

    #@2c
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->height:I

    #@2e
    .line 1660
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@30
    iget-object v12, v12, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@32
    iput-object v12, v8, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@34
    .line 1661
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@36
    iget-boolean v12, v12, Landroid/widget/AbsListView$SavedState;->inActionMode:Z

    #@38
    iput-boolean v12, v8, Landroid/widget/AbsListView$SavedState;->inActionMode:Z

    #@3a
    .line 1662
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@3c
    iget v12, v12, Landroid/widget/AbsListView$SavedState;->checkedItemCount:I

    #@3e
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->checkedItemCount:I

    #@40
    .line 1663
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@42
    iget-object v12, v12, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@44
    iput-object v12, v8, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@46
    .line 1664
    iget-object v12, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@48
    iget-object v12, v12, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@4a
    iput-object v12, v8, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@4c
    .line 1734
    :cond_4c
    :goto_4c
    return-object v8

    #@4d
    .line 1668
    :cond_4d
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@50
    move-result v12

    #@51
    if-lez v12, :cond_cb

    #@53
    iget v12, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@55
    if-lez v12, :cond_cb

    #@57
    const/4 v3, 0x1

    #@58
    .line 1669
    .local v3, haveChildren:Z
    :goto_58
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getSelectedItemId()J

    #@5b
    move-result-wide v6

    #@5c
    .line 1670
    .local v6, selectedId:J
    iput-wide v6, v8, Landroid/widget/AbsListView$SavedState;->selectedId:J

    #@5e
    .line 1671
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@61
    move-result v12

    #@62
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->height:I

    #@64
    .line 1673
    const-wide/16 v12, 0x0

    #@66
    cmp-long v12, v6, v12

    #@68
    if-ltz v12, :cond_cd

    #@6a
    .line 1675
    iget v12, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@6c
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@6e
    .line 1676
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    #@71
    move-result v12

    #@72
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->position:I

    #@74
    .line 1677
    const-wide/16 v12, -0x1

    #@76
    iput-wide v12, v8, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@78
    .line 1704
    :goto_78
    const/4 v12, 0x0

    #@79
    iput-object v12, v8, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@7b
    .line 1705
    iget-boolean v12, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@7d
    if-eqz v12, :cond_8f

    #@7f
    .line 1706
    iget-object v10, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@81
    .line 1707
    .local v10, textFilter:Landroid/widget/EditText;
    if-eqz v10, :cond_8f

    #@83
    .line 1708
    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@86
    move-result-object v1

    #@87
    .line 1709
    .local v1, filterText:Landroid/text/Editable;
    if-eqz v1, :cond_8f

    #@89
    .line 1710
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8c
    move-result-object v12

    #@8d
    iput-object v12, v8, Landroid/widget/AbsListView$SavedState;->filter:Ljava/lang/String;

    #@8f
    .line 1715
    .end local v1           #filterText:Landroid/text/Editable;
    .end local v10           #textFilter:Landroid/widget/EditText;
    :cond_8f
    iget v12, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@91
    const/4 v13, 0x3

    #@92
    if-ne v12, v13, :cond_ff

    #@94
    iget-object v12, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@96
    if-eqz v12, :cond_ff

    #@98
    const/4 v12, 0x1

    #@99
    :goto_99
    iput-boolean v12, v8, Landroid/widget/AbsListView$SavedState;->inActionMode:Z

    #@9b
    .line 1717
    iget-object v12, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@9d
    if-eqz v12, :cond_a7

    #@9f
    .line 1718
    iget-object v12, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@a1
    invoke-virtual {v12}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    #@a4
    move-result-object v12

    #@a5
    iput-object v12, v8, Landroid/widget/AbsListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    #@a7
    .line 1720
    :cond_a7
    iget-object v12, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@a9
    if-eqz v12, :cond_103

    #@ab
    .line 1721
    new-instance v5, Landroid/util/LongSparseArray;

    #@ad
    invoke-direct {v5}, Landroid/util/LongSparseArray;-><init>()V

    #@b0
    .line 1722
    .local v5, idState:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/Integer;>;"
    iget-object v12, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@b2
    invoke-virtual {v12}, Landroid/util/LongSparseArray;->size()I

    #@b5
    move-result v0

    #@b6
    .line 1723
    .local v0, count:I
    const/4 v4, 0x0

    #@b7
    .local v4, i:I
    :goto_b7
    if-ge v4, v0, :cond_101

    #@b9
    .line 1724
    iget-object v12, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@bb
    invoke-virtual {v12, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    #@be
    move-result-wide v12

    #@bf
    iget-object v14, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@c1
    invoke-virtual {v14, v4}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@c4
    move-result-object v14

    #@c5
    invoke-virtual {v5, v12, v13, v14}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@c8
    .line 1723
    add-int/lit8 v4, v4, 0x1

    #@ca
    goto :goto_b7

    #@cb
    .line 1668
    .end local v0           #count:I
    .end local v3           #haveChildren:Z
    .end local v4           #i:I
    .end local v5           #idState:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/Integer;>;"
    .end local v6           #selectedId:J
    :cond_cb
    const/4 v3, 0x0

    #@cc
    goto :goto_58

    #@cd
    .line 1679
    .restart local v3       #haveChildren:Z
    .restart local v6       #selectedId:J
    :cond_cd
    if-eqz v3, :cond_f3

    #@cf
    iget v12, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@d1
    if-lez v12, :cond_f3

    #@d3
    .line 1689
    const/4 v12, 0x0

    #@d4
    invoke-virtual {p0, v12}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@d7
    move-result-object v11

    #@d8
    .line 1690
    .local v11, v:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    #@db
    move-result v12

    #@dc
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@de
    .line 1691
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@e0
    .line 1692
    .local v2, firstPos:I
    iget v12, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@e2
    if-lt v2, v12, :cond_e8

    #@e4
    .line 1693
    iget v12, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@e6
    add-int/lit8 v2, v12, -0x1

    #@e8
    .line 1695
    :cond_e8
    iput v2, v8, Landroid/widget/AbsListView$SavedState;->position:I

    #@ea
    .line 1696
    iget-object v12, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@ec
    invoke-interface {v12, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@ef
    move-result-wide v12

    #@f0
    iput-wide v12, v8, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@f2
    goto :goto_78

    #@f3
    .line 1698
    .end local v2           #firstPos:I
    .end local v11           #v:Landroid/view/View;
    :cond_f3
    const/4 v12, 0x0

    #@f4
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->viewTop:I

    #@f6
    .line 1699
    const-wide/16 v12, -0x1

    #@f8
    iput-wide v12, v8, Landroid/widget/AbsListView$SavedState;->firstId:J

    #@fa
    .line 1700
    const/4 v12, 0x0

    #@fb
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->position:I

    #@fd
    goto/16 :goto_78

    #@ff
    .line 1715
    :cond_ff
    const/4 v12, 0x0

    #@100
    goto :goto_99

    #@101
    .line 1726
    .restart local v0       #count:I
    .restart local v4       #i:I
    .restart local v5       #idState:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_101
    iput-object v5, v8, Landroid/widget/AbsListView$SavedState;->checkIdState:Landroid/util/LongSparseArray;

    #@103
    .line 1728
    .end local v0           #count:I
    .end local v4           #i:I
    .end local v5           #idState:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_103
    iget v12, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@105
    iput v12, v8, Landroid/widget/AbsListView$SavedState;->checkedItemCount:I

    #@107
    .line 1730
    iget-object v12, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@109
    if-eqz v12, :cond_4c

    #@10b
    .line 1731
    iget-object v12, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@10d
    invoke-virtual {v12}, Landroid/widget/RemoteViewsAdapter;->saveRemoteViewsCache()V

    #@110
    goto/16 :goto_4c
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 2399
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_c

    #@6
    .line 2400
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@9
    .line 2401
    invoke-virtual {p0}, Landroid/widget/AbsListView;->rememberSyncState()V

    #@c
    .line 2404
    :cond_c
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 2405
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@12
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/FastScroller;->onSizeChanged(IIII)V

    #@15
    .line 2407
    :cond_15
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 10
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 5939
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@2
    if-eqz v3, :cond_31

    #@4
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isTextFilterEnabled()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_31

    #@a
    .line 5940
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v1

    #@e
    .line 5941
    .local v1, length:I
    iget-object v3, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@10
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    #@13
    move-result v2

    #@14
    .line 5942
    .local v2, showing:Z
    if-nez v2, :cond_32

    #@16
    if-lez v1, :cond_32

    #@18
    .line 5944
    invoke-direct {p0}, Landroid/widget/AbsListView;->showPopup()V

    #@1b
    .line 5945
    const/4 v3, 0x1

    #@1c
    iput-boolean v3, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@1e
    .line 5951
    :cond_1e
    :goto_1e
    iget-object v3, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@20
    instance-of v3, v3, Landroid/widget/Filterable;

    #@22
    if-eqz v3, :cond_31

    #@24
    .line 5952
    iget-object v3, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@26
    check-cast v3, Landroid/widget/Filterable;

    #@28
    invoke-interface {v3}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@2b
    move-result-object v0

    #@2c
    .line 5954
    .local v0, f:Landroid/widget/Filter;
    if-eqz v0, :cond_3d

    #@2e
    .line 5955
    invoke-virtual {v0, p1, p0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    #@31
    .line 5962
    .end local v0           #f:Landroid/widget/Filter;
    .end local v1           #length:I
    .end local v2           #showing:Z
    :cond_31
    return-void

    #@32
    .line 5946
    .restart local v1       #length:I
    .restart local v2       #showing:Z
    :cond_32
    if-eqz v2, :cond_1e

    #@34
    if-nez v1, :cond_1e

    #@36
    .line 5948
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@39
    .line 5949
    const/4 v3, 0x0

    #@3a
    iput-boolean v3, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@3c
    goto :goto_1e

    #@3d
    .line 5957
    .restart local v0       #f:Landroid/widget/Filter;
    :cond_3d
    new-instance v3, Ljava/lang/IllegalStateException;

    #@3f
    const-string v4, "You cannot call onTextChanged with a non filterable adapter"

    #@41
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@44
    throw v3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 36
    .parameter "ev"

    #@0
    .prologue
    .line 3395
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->isEnabled()Z

    #@3
    move-result v31

    #@4
    if-nez v31, :cond_18

    #@6
    .line 3398
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->isClickable()Z

    #@9
    move-result v31

    #@a
    if-nez v31, :cond_12

    #@c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->isLongClickable()Z

    #@f
    move-result v31

    #@10
    if-eqz v31, :cond_15

    #@12
    :cond_12
    const/16 v31, 0x1

    #@14
    .line 3827
    :goto_14
    return v31

    #@15
    .line 3398
    :cond_15
    const/16 v31, 0x0

    #@17
    goto :goto_14

    #@18
    .line 3401
    :cond_18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@1c
    move-object/from16 v31, v0

    #@1e
    if-eqz v31, :cond_29

    #@20
    .line 3402
    move-object/from16 v0, p0

    #@22
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@24
    move-object/from16 v31, v0

    #@26
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@29
    .line 3405
    :cond_29
    move-object/from16 v0, p0

    #@2b
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mIsAttached:Z

    #@2d
    move/from16 v31, v0

    #@2f
    if-nez v31, :cond_34

    #@31
    .line 3410
    const/16 v31, 0x0

    #@33
    goto :goto_14

    #@34
    .line 3413
    :cond_34
    move-object/from16 v0, p0

    #@36
    iget-object v0, v0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@38
    move-object/from16 v31, v0

    #@3a
    if-eqz v31, :cond_4f

    #@3c
    .line 3414
    move-object/from16 v0, p0

    #@3e
    iget-object v0, v0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@40
    move-object/from16 v31, v0

    #@42
    move-object/from16 v0, v31

    #@44
    move-object/from16 v1, p1

    #@46
    invoke-virtual {v0, v1}, Landroid/widget/FastScroller;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@49
    move-result v18

    #@4a
    .line 3415
    .local v18, intercepted:Z
    if-eqz v18, :cond_4f

    #@4c
    .line 3416
    const/16 v31, 0x1

    #@4e
    goto :goto_14

    #@4f
    .line 3420
    .end local v18           #intercepted:Z
    :cond_4f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@52
    move-result v4

    #@53
    .line 3424
    .local v4, action:I
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->initVelocityTrackerIfNotExists()V

    #@56
    .line 3425
    move-object/from16 v0, p0

    #@58
    iget-object v0, v0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@5a
    move-object/from16 v31, v0

    #@5c
    move-object/from16 v0, v31

    #@5e
    move-object/from16 v1, p1

    #@60
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@63
    .line 3426
    sget-boolean v31, Landroid/widget/AbsListView;->mCapptouchFlickNoti:Z

    #@65
    if-eqz v31, :cond_94

    #@67
    .line 3427
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@6a
    move-result v31

    #@6b
    move/from16 v0, v31

    #@6d
    float-to-int v0, v0

    #@6e
    move/from16 v30, v0

    #@70
    .line 3428
    .local v30, y:I
    sget v31, Landroid/widget/AbsListView;->eventMonitor:I

    #@72
    sget v32, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@74
    add-int/lit8 v32, v32, -0x1

    #@76
    and-int v31, v31, v32

    #@78
    if-nez v31, :cond_7e

    #@7a
    .line 3429
    const/16 v31, 0x0

    #@7c
    sput v31, Landroid/widget/AbsListView;->eventMonitor:I

    #@7e
    .line 3431
    :cond_7e
    move-object/from16 v0, p0

    #@80
    iget-object v0, v0, Landroid/widget/AbsListView;->coorDirection:[I

    #@82
    move-object/from16 v31, v0

    #@84
    sget v32, Landroid/widget/AbsListView;->toCompareY:I

    #@86
    sget v33, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@88
    add-int/lit8 v33, v33, -0x1

    #@8a
    and-int v32, v32, v33

    #@8c
    aget v31, v31, v32

    #@8e
    move/from16 v0, v31

    #@90
    move/from16 v1, v30

    #@92
    if-ne v0, v1, :cond_9f

    #@94
    .line 3441
    .end local v30           #y:I
    :cond_94
    :goto_94
    and-int/lit16 v0, v4, 0xff

    #@96
    move/from16 v31, v0

    #@98
    packed-switch v31, :pswitch_data_8c6

    #@9b
    .line 3827
    :cond_9b
    :goto_9b
    :pswitch_9b
    const/16 v31, 0x1

    #@9d
    goto/16 :goto_14

    #@9f
    .line 3434
    .restart local v30       #y:I
    :cond_9f
    move-object/from16 v0, p0

    #@a1
    iget-object v0, v0, Landroid/widget/AbsListView;->coorDirection:[I

    #@a3
    move-object/from16 v31, v0

    #@a5
    sget v32, Landroid/widget/AbsListView;->eventMonitor:I

    #@a7
    sget v33, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@a9
    add-int/lit8 v33, v33, -0x1

    #@ab
    and-int v32, v32, v33

    #@ad
    aput v30, v31, v32

    #@af
    .line 3435
    sget v31, Landroid/widget/AbsListView;->eventMonitor:I

    #@b1
    sput v31, Landroid/widget/AbsListView;->toCompareY:I

    #@b3
    .line 3436
    sget v31, Landroid/widget/AbsListView;->eventMonitor:I

    #@b5
    add-int/lit8 v31, v31, 0x1

    #@b7
    sput v31, Landroid/widget/AbsListView;->eventMonitor:I

    #@b9
    .line 3437
    sget v31, Landroid/widget/AbsListView;->moveCounter:I

    #@bb
    add-int/lit8 v31, v31, 0x1

    #@bd
    sput v31, Landroid/widget/AbsListView;->moveCounter:I

    #@bf
    goto :goto_94

    #@c0
    .line 3443
    .end local v30           #y:I
    :pswitch_c0
    move-object/from16 v0, p0

    #@c2
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@c4
    move/from16 v31, v0

    #@c6
    packed-switch v31, :pswitch_data_8d8

    #@c9
    .line 3459
    const/16 v31, 0x0

    #@cb
    move-object/from16 v0, p1

    #@cd
    move/from16 v1, v31

    #@cf
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@d2
    move-result v31

    #@d3
    move/from16 v0, v31

    #@d5
    move-object/from16 v1, p0

    #@d7
    iput v0, v1, Landroid/widget/AbsListView;->mActivePointerId:I

    #@d9
    .line 3460
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@dc
    move-result v31

    #@dd
    move/from16 v0, v31

    #@df
    float-to-int v0, v0

    #@e0
    move/from16 v29, v0

    #@e2
    .line 3461
    .local v29, x:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@e5
    move-result v31

    #@e6
    move/from16 v0, v31

    #@e8
    float-to-int v0, v0

    #@e9
    move/from16 v30, v0

    #@eb
    .line 3462
    .restart local v30       #y:I
    move-object/from16 v0, p0

    #@ed
    move/from16 v1, v29

    #@ef
    move/from16 v2, v30

    #@f1
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->pointToPosition(II)I

    #@f4
    move-result v20

    #@f5
    .line 3463
    .local v20, motionPosition:I
    move-object/from16 v0, p0

    #@f7
    iget-boolean v0, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@f9
    move/from16 v31, v0

    #@fb
    if-nez v31, :cond_1d6

    #@fd
    .line 3464
    sget-boolean v31, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@ff
    if-eqz v31, :cond_17f

    #@101
    .line 3465
    move-object/from16 v0, p0

    #@103
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@105
    move/from16 v31, v0

    #@107
    const/16 v32, 0x4

    #@109
    move/from16 v0, v31

    #@10b
    move/from16 v1, v32

    #@10d
    if-ne v0, v1, :cond_17f

    #@10f
    move-object/from16 v0, p0

    #@111
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@113
    move-object/from16 v31, v0

    #@115
    if-eqz v31, :cond_17f

    #@117
    if-ltz v20, :cond_17f

    #@119
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@11c
    move-result-object v31

    #@11d
    check-cast v31, Landroid/widget/ListAdapter;

    #@11f
    move-object/from16 v0, v31

    #@121
    move/from16 v1, v20

    #@123
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@126
    move-result v31

    #@127
    if-eqz v31, :cond_17f

    #@129
    .line 3467
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@12d
    move-object/from16 v31, v0

    #@12f
    invoke-static/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->access$700(Landroid/widget/AbsListView$FlingRunnable;)Landroid/widget/OverScroller;

    #@132
    move-result-object v31

    #@133
    invoke-virtual/range {v31 .. v31}, Landroid/widget/OverScroller;->getCurrVelocity()F

    #@136
    move-result v31

    #@137
    invoke-static/range {v31 .. v31}, Ljava/lang/Math;->abs(F)F

    #@13a
    move-result v27

    #@13b
    .line 3468
    .local v27, velocity:F
    move-object/from16 v0, p0

    #@13d
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@13f
    move-object/from16 v31, v0

    #@141
    invoke-static/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->access$700(Landroid/widget/AbsListView$FlingRunnable;)Landroid/widget/OverScroller;

    #@144
    move-result-object v31

    #@145
    invoke-virtual/range {v31 .. v31}, Landroid/widget/OverScroller;->timePassed()I

    #@148
    move-result v31

    #@149
    move/from16 v0, v31

    #@14b
    int-to-long v0, v0

    #@14c
    move-wide/from16 v24, v0

    #@14e
    .line 3469
    .local v24, timepassed:J
    move/from16 v0, v27

    #@150
    float-to-int v0, v0

    #@151
    move/from16 v31, v0

    #@153
    if-eqz v31, :cond_17f

    #@155
    const/high16 v31, 0x4316

    #@157
    cmpg-float v31, v27, v31

    #@159
    if-gez v31, :cond_17f

    #@15b
    const-wide/16 v31, 0x64

    #@15d
    cmp-long v31, v24, v31

    #@15f
    if-lez v31, :cond_17f

    #@161
    .line 3470
    move-object/from16 v0, p0

    #@163
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@165
    move-object/from16 v31, v0

    #@167
    move-object/from16 v0, p0

    #@169
    move-object/from16 v1, v31

    #@16b
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@16e
    .line 3471
    move-object/from16 v0, p0

    #@170
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@172
    move-object/from16 v31, v0

    #@174
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@177
    .line 3472
    const/16 v31, -0x1

    #@179
    move/from16 v0, v31

    #@17b
    move-object/from16 v1, p0

    #@17d
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@17f
    .line 3477
    .end local v24           #timepassed:J
    .end local v27           #velocity:F
    :cond_17f
    move-object/from16 v0, p0

    #@181
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@183
    move/from16 v31, v0

    #@185
    const/16 v32, 0x4

    #@187
    move/from16 v0, v31

    #@189
    move/from16 v1, v32

    #@18b
    if-eq v0, v1, :cond_293

    #@18d
    if-ltz v20, :cond_293

    #@18f
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    #@192
    move-result-object v31

    #@193
    check-cast v31, Landroid/widget/ListAdapter;

    #@195
    move-object/from16 v0, v31

    #@197
    move/from16 v1, v20

    #@199
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@19c
    move-result v31

    #@19d
    if-eqz v31, :cond_293

    #@19f
    .line 3482
    const/16 v31, 0x0

    #@1a1
    move/from16 v0, v31

    #@1a3
    move-object/from16 v1, p0

    #@1a5
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@1a7
    .line 3484
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@1ab
    move-object/from16 v31, v0

    #@1ad
    if-nez v31, :cond_1be

    #@1af
    .line 3485
    new-instance v31, Landroid/widget/AbsListView$CheckForTap;

    #@1b1
    move-object/from16 v0, v31

    #@1b3
    move-object/from16 v1, p0

    #@1b5
    invoke-direct {v0, v1}, Landroid/widget/AbsListView$CheckForTap;-><init>(Landroid/widget/AbsListView;)V

    #@1b8
    move-object/from16 v0, v31

    #@1ba
    move-object/from16 v1, p0

    #@1bc
    iput-object v0, v1, Landroid/widget/AbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@1be
    .line 3487
    :cond_1be
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@1c2
    move-object/from16 v31, v0

    #@1c4
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@1c7
    move-result v32

    #@1c8
    move/from16 v0, v32

    #@1ca
    int-to-long v0, v0

    #@1cb
    move-wide/from16 v32, v0

    #@1cd
    move-object/from16 v0, p0

    #@1cf
    move-object/from16 v1, v31

    #@1d1
    move-wide/from16 v2, v32

    #@1d3
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1d6
    .line 3502
    :cond_1d6
    :goto_1d6
    if-ltz v20, :cond_1f4

    #@1d8
    .line 3504
    move-object/from16 v0, p0

    #@1da
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1dc
    move/from16 v31, v0

    #@1de
    sub-int v31, v20, v31

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    move/from16 v1, v31

    #@1e4
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@1e7
    move-result-object v26

    #@1e8
    .line 3506
    .local v26, v:Landroid/view/View;
    if-eqz v26, :cond_1f4

    #@1ea
    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getTop()I

    #@1ed
    move-result v31

    #@1ee
    move/from16 v0, v31

    #@1f0
    move-object/from16 v1, p0

    #@1f2
    iput v0, v1, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@1f4
    .line 3509
    .end local v26           #v:Landroid/view/View;
    :cond_1f4
    move/from16 v0, v29

    #@1f6
    move-object/from16 v1, p0

    #@1f8
    iput v0, v1, Landroid/widget/AbsListView;->mMotionX:I

    #@1fa
    .line 3510
    move/from16 v0, v30

    #@1fc
    move-object/from16 v1, p0

    #@1fe
    iput v0, v1, Landroid/widget/AbsListView;->mMotionY:I

    #@200
    .line 3511
    move/from16 v0, v20

    #@202
    move-object/from16 v1, p0

    #@204
    iput v0, v1, Landroid/widget/AbsListView;->mMotionPosition:I

    #@206
    .line 3512
    const/high16 v31, -0x8000

    #@208
    move/from16 v0, v31

    #@20a
    move-object/from16 v1, p0

    #@20c
    iput v0, v1, Landroid/widget/AbsListView;->mLastY:I

    #@20e
    .line 3517
    .end local v20           #motionPosition:I
    .end local v29           #x:I
    .end local v30           #y:I
    :goto_20e
    invoke-virtual/range {p0 .. p1}, Landroid/widget/AbsListView;->performButtonActionOnTouchDown(Landroid/view/MotionEvent;)Z

    #@211
    move-result v31

    #@212
    if-eqz v31, :cond_9b

    #@214
    .line 3518
    move-object/from16 v0, p0

    #@216
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@218
    move/from16 v31, v0

    #@21a
    if-nez v31, :cond_9b

    #@21c
    .line 3519
    move-object/from16 v0, p0

    #@21e
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@220
    move-object/from16 v31, v0

    #@222
    move-object/from16 v0, p0

    #@224
    move-object/from16 v1, v31

    #@226
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@229
    goto/16 :goto_9b

    #@22b
    .line 3445
    :pswitch_22b
    move-object/from16 v0, p0

    #@22d
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@22f
    move-object/from16 v31, v0

    #@231
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@234
    .line 3446
    move-object/from16 v0, p0

    #@236
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@238
    move-object/from16 v31, v0

    #@23a
    if-eqz v31, :cond_245

    #@23c
    .line 3447
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@240
    move-object/from16 v31, v0

    #@242
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@245
    .line 3449
    :cond_245
    const/16 v31, 0x5

    #@247
    move/from16 v0, v31

    #@249
    move-object/from16 v1, p0

    #@24b
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@24d
    .line 3450
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@250
    move-result v31

    #@251
    move/from16 v0, v31

    #@253
    float-to-int v0, v0

    #@254
    move/from16 v31, v0

    #@256
    move/from16 v0, v31

    #@258
    move-object/from16 v1, p0

    #@25a
    iput v0, v1, Landroid/widget/AbsListView;->mMotionX:I

    #@25c
    .line 3451
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@25f
    move-result v31

    #@260
    move/from16 v0, v31

    #@262
    float-to-int v0, v0

    #@263
    move/from16 v31, v0

    #@265
    move/from16 v0, v31

    #@267
    move-object/from16 v1, p0

    #@269
    iput v0, v1, Landroid/widget/AbsListView;->mLastY:I

    #@26b
    move/from16 v0, v31

    #@26d
    move-object/from16 v1, p0

    #@26f
    iput v0, v1, Landroid/widget/AbsListView;->mMotionY:I

    #@271
    .line 3452
    const/16 v31, 0x0

    #@273
    move/from16 v0, v31

    #@275
    move-object/from16 v1, p0

    #@277
    iput v0, v1, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@279
    .line 3453
    const/16 v31, 0x0

    #@27b
    move-object/from16 v0, p1

    #@27d
    move/from16 v1, v31

    #@27f
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@282
    move-result v31

    #@283
    move/from16 v0, v31

    #@285
    move-object/from16 v1, p0

    #@287
    iput v0, v1, Landroid/widget/AbsListView;->mActivePointerId:I

    #@289
    .line 3454
    const/16 v31, 0x0

    #@28b
    move/from16 v0, v31

    #@28d
    move-object/from16 v1, p0

    #@28f
    iput v0, v1, Landroid/widget/AbsListView;->mDirection:I

    #@291
    goto/16 :goto_20e

    #@293
    .line 3489
    .restart local v20       #motionPosition:I
    .restart local v29       #x:I
    .restart local v30       #y:I
    :cond_293
    move-object/from16 v0, p0

    #@295
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@297
    move/from16 v31, v0

    #@299
    const/16 v32, 0x4

    #@29b
    move/from16 v0, v31

    #@29d
    move/from16 v1, v32

    #@29f
    if-ne v0, v1, :cond_1d6

    #@2a1
    .line 3491
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->createScrollingCache()V

    #@2a4
    .line 3492
    const/16 v31, 0x3

    #@2a6
    move/from16 v0, v31

    #@2a8
    move-object/from16 v1, p0

    #@2aa
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@2ac
    .line 3493
    const/16 v31, 0x0

    #@2ae
    move/from16 v0, v31

    #@2b0
    move-object/from16 v1, p0

    #@2b2
    iput v0, v1, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@2b4
    .line 3494
    move-object/from16 v0, p0

    #@2b6
    move/from16 v1, v30

    #@2b8
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->findMotionRow(I)I

    #@2bb
    move-result v20

    #@2bc
    .line 3495
    move-object/from16 v0, p0

    #@2be
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@2c0
    move-object/from16 v31, v0

    #@2c2
    if-eqz v31, :cond_1d6

    #@2c4
    .line 3496
    move-object/from16 v0, p0

    #@2c6
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@2c8
    move-object/from16 v31, v0

    #@2ca
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->flywheelTouch()V

    #@2cd
    goto/16 :goto_1d6

    #@2cf
    .line 3526
    .end local v20           #motionPosition:I
    .end local v29           #x:I
    .end local v30           #y:I
    :pswitch_2cf
    move-object/from16 v0, p0

    #@2d1
    iget v0, v0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@2d3
    move/from16 v31, v0

    #@2d5
    move-object/from16 v0, p1

    #@2d7
    move/from16 v1, v31

    #@2d9
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@2dc
    move-result v23

    #@2dd
    .line 3527
    .local v23, pointerIndex:I
    const/16 v31, -0x1

    #@2df
    move/from16 v0, v23

    #@2e1
    move/from16 v1, v31

    #@2e3
    if-ne v0, v1, :cond_2f5

    #@2e5
    .line 3528
    const/16 v23, 0x0

    #@2e7
    .line 3529
    move-object/from16 v0, p1

    #@2e9
    move/from16 v1, v23

    #@2eb
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@2ee
    move-result v31

    #@2ef
    move/from16 v0, v31

    #@2f1
    move-object/from16 v1, p0

    #@2f3
    iput v0, v1, Landroid/widget/AbsListView;->mActivePointerId:I

    #@2f5
    .line 3531
    :cond_2f5
    move-object/from16 v0, p1

    #@2f7
    move/from16 v1, v23

    #@2f9
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@2fc
    move-result v31

    #@2fd
    move/from16 v0, v31

    #@2ff
    float-to-int v0, v0

    #@300
    move/from16 v30, v0

    #@302
    .line 3533
    .restart local v30       #y:I
    move-object/from16 v0, p0

    #@304
    iget-boolean v0, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@306
    move/from16 v31, v0

    #@308
    if-eqz v31, :cond_30d

    #@30a
    .line 3536
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@30d
    .line 3539
    :cond_30d
    move-object/from16 v0, p0

    #@30f
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@311
    move/from16 v31, v0

    #@313
    packed-switch v31, :pswitch_data_8de

    #@316
    :pswitch_316
    goto/16 :goto_9b

    #@318
    .line 3545
    :pswitch_318
    move-object/from16 v0, p0

    #@31a
    move/from16 v1, v30

    #@31c
    invoke-direct {v0, v1}, Landroid/widget/AbsListView;->startScrollIfNeeded(I)Z

    #@31f
    goto/16 :goto_9b

    #@321
    .line 3549
    :pswitch_321
    move-object/from16 v0, p0

    #@323
    move/from16 v1, v30

    #@325
    invoke-direct {v0, v1}, Landroid/widget/AbsListView;->scrollIfNeeded(I)V

    #@328
    goto/16 :goto_9b

    #@32a
    .line 3556
    .end local v23           #pointerIndex:I
    .end local v30           #y:I
    :pswitch_32a
    move-object/from16 v0, p0

    #@32c
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@32e
    move/from16 v31, v0

    #@330
    packed-switch v31, :pswitch_data_8ee

    #@333
    .line 3704
    :cond_333
    :goto_333
    :pswitch_333
    const/16 v31, 0x0

    #@335
    move-object/from16 v0, p0

    #@337
    move/from16 v1, v31

    #@339
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@33c
    .line 3706
    move-object/from16 v0, p0

    #@33e
    iget-object v0, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@340
    move-object/from16 v31, v0

    #@342
    if-eqz v31, :cond_356

    #@344
    .line 3707
    move-object/from16 v0, p0

    #@346
    iget-object v0, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@348
    move-object/from16 v31, v0

    #@34a
    invoke-virtual/range {v31 .. v31}, Landroid/widget/EdgeEffect;->onRelease()V

    #@34d
    .line 3708
    move-object/from16 v0, p0

    #@34f
    iget-object v0, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@351
    move-object/from16 v31, v0

    #@353
    invoke-virtual/range {v31 .. v31}, Landroid/widget/EdgeEffect;->onRelease()V

    #@356
    .line 3712
    :cond_356
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->invalidate()V

    #@359
    .line 3714
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHandler()Landroid/os/Handler;

    #@35c
    move-result-object v11

    #@35d
    .line 3715
    .local v11, handler:Landroid/os/Handler;
    if-eqz v11, :cond_36a

    #@35f
    .line 3716
    move-object/from16 v0, p0

    #@361
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

    #@363
    move-object/from16 v31, v0

    #@365
    move-object/from16 v0, v31

    #@367
    invoke-virtual {v11, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@36a
    .line 3719
    :cond_36a
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->recycleVelocityTracker()V

    #@36d
    .line 3721
    const/16 v31, -0x1

    #@36f
    move/from16 v0, v31

    #@371
    move-object/from16 v1, p0

    #@373
    iput v0, v1, Landroid/widget/AbsListView;->mActivePointerId:I

    #@375
    .line 3730
    move-object/from16 v0, p0

    #@377
    iget-object v0, v0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@379
    move-object/from16 v31, v0

    #@37b
    if-eqz v31, :cond_9b

    #@37d
    .line 3731
    move-object/from16 v0, p0

    #@37f
    iget-object v0, v0, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@381
    move-object/from16 v31, v0

    #@383
    invoke-virtual/range {v31 .. v31}, Landroid/os/StrictMode$Span;->finish()V

    #@386
    .line 3732
    const/16 v31, 0x0

    #@388
    move-object/from16 v0, v31

    #@38a
    move-object/from16 v1, p0

    #@38c
    iput-object v0, v1, Landroid/widget/AbsListView;->mScrollStrictSpan:Landroid/os/StrictMode$Span;

    #@38e
    goto/16 :goto_9b

    #@390
    .line 3560
    .end local v11           #handler:Landroid/os/Handler;
    :pswitch_390
    move-object/from16 v0, p0

    #@392
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@394
    move/from16 v20, v0

    #@396
    .line 3561
    .restart local v20       #motionPosition:I
    move-object/from16 v0, p0

    #@398
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@39a
    move/from16 v31, v0

    #@39c
    sub-int v31, v20, v31

    #@39e
    move-object/from16 v0, p0

    #@3a0
    move/from16 v1, v31

    #@3a2
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@3a5
    move-result-object v5

    #@3a6
    .line 3563
    .local v5, child:Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@3a9
    move-result v29

    #@3aa
    .line 3564
    .local v29, x:F
    move-object/from16 v0, p0

    #@3ac
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3ae
    move-object/from16 v31, v0

    #@3b0
    move-object/from16 v0, v31

    #@3b2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@3b4
    move/from16 v31, v0

    #@3b6
    move/from16 v0, v31

    #@3b8
    int-to-float v0, v0

    #@3b9
    move/from16 v31, v0

    #@3bb
    cmpl-float v31, v29, v31

    #@3bd
    if-lez v31, :cond_509

    #@3bf
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getWidth()I

    #@3c2
    move-result v31

    #@3c3
    move-object/from16 v0, p0

    #@3c5
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@3c7
    move-object/from16 v32, v0

    #@3c9
    move-object/from16 v0, v32

    #@3cb
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@3cd
    move/from16 v32, v0

    #@3cf
    sub-int v31, v31, v32

    #@3d1
    move/from16 v0, v31

    #@3d3
    int-to-float v0, v0

    #@3d4
    move/from16 v31, v0

    #@3d6
    cmpg-float v31, v29, v31

    #@3d8
    if-gez v31, :cond_509

    #@3da
    const/4 v14, 0x1

    #@3db
    .line 3566
    .local v14, inList:Z
    :goto_3db
    if-eqz v5, :cond_53b

    #@3dd
    invoke-virtual {v5}, Landroid/view/View;->hasFocusable()Z

    #@3e0
    move-result v31

    #@3e1
    if-nez v31, :cond_53b

    #@3e3
    if-eqz v14, :cond_53b

    #@3e5
    .line 3567
    move-object/from16 v0, p0

    #@3e7
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@3e9
    move/from16 v31, v0

    #@3eb
    if-eqz v31, :cond_3f4

    #@3ed
    .line 3568
    const/16 v31, 0x0

    #@3ef
    move/from16 v0, v31

    #@3f1
    invoke-virtual {v5, v0}, Landroid/view/View;->setPressed(Z)V

    #@3f4
    .line 3571
    :cond_3f4
    move-object/from16 v0, p0

    #@3f6
    iget-object v0, v0, Landroid/widget/AbsListView;->mPerformClick:Landroid/widget/AbsListView$PerformClick;

    #@3f8
    move-object/from16 v31, v0

    #@3fa
    if-nez v31, :cond_40f

    #@3fc
    .line 3572
    new-instance v31, Landroid/widget/AbsListView$PerformClick;

    #@3fe
    const/16 v32, 0x0

    #@400
    move-object/from16 v0, v31

    #@402
    move-object/from16 v1, p0

    #@404
    move-object/from16 v2, v32

    #@406
    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$PerformClick;-><init>(Landroid/widget/AbsListView;Landroid/widget/AbsListView$1;)V

    #@409
    move-object/from16 v0, v31

    #@40b
    move-object/from16 v1, p0

    #@40d
    iput-object v0, v1, Landroid/widget/AbsListView;->mPerformClick:Landroid/widget/AbsListView$PerformClick;

    #@40f
    .line 3575
    :cond_40f
    move-object/from16 v0, p0

    #@411
    iget-object v0, v0, Landroid/widget/AbsListView;->mPerformClick:Landroid/widget/AbsListView$PerformClick;

    #@413
    move-object/from16 v22, v0

    #@415
    .line 3576
    .local v22, performClick:Landroid/widget/AbsListView$PerformClick;
    move/from16 v0, v20

    #@417
    move-object/from16 v1, v22

    #@419
    iput v0, v1, Landroid/widget/AbsListView$PerformClick;->mClickMotionPosition:I

    #@41b
    .line 3577
    invoke-virtual/range {v22 .. v22}, Landroid/widget/AbsListView$PerformClick;->rememberWindowAttachCount()V

    #@41e
    .line 3579
    move/from16 v0, v20

    #@420
    move-object/from16 v1, p0

    #@422
    iput v0, v1, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@424
    .line 3581
    move-object/from16 v0, p0

    #@426
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@428
    move/from16 v31, v0

    #@42a
    if-eqz v31, :cond_43a

    #@42c
    move-object/from16 v0, p0

    #@42e
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@430
    move/from16 v31, v0

    #@432
    const/16 v32, 0x1

    #@434
    move/from16 v0, v31

    #@436
    move/from16 v1, v32

    #@438
    if-ne v0, v1, :cond_520

    #@43a
    .line 3582
    :cond_43a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHandler()Landroid/os/Handler;

    #@43d
    move-result-object v11

    #@43e
    .line 3583
    .restart local v11       #handler:Landroid/os/Handler;
    if-eqz v11, :cond_453

    #@440
    .line 3584
    move-object/from16 v0, p0

    #@442
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@444
    move/from16 v31, v0

    #@446
    if-nez v31, :cond_50c

    #@448
    move-object/from16 v0, p0

    #@44a
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    #@44c
    move-object/from16 v31, v0

    #@44e
    :goto_44e
    move-object/from16 v0, v31

    #@450
    invoke-virtual {v11, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@453
    .line 3587
    :cond_453
    const/16 v31, 0x0

    #@455
    move/from16 v0, v31

    #@457
    move-object/from16 v1, p0

    #@459
    iput v0, v1, Landroid/widget/AbsListView;->mLayoutMode:I

    #@45b
    .line 3588
    move-object/from16 v0, p0

    #@45d
    iget-boolean v0, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@45f
    move/from16 v31, v0

    #@461
    if-nez v31, :cond_514

    #@463
    move-object/from16 v0, p0

    #@465
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@467
    move-object/from16 v31, v0

    #@469
    move-object/from16 v0, v31

    #@46b
    move/from16 v1, v20

    #@46d
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@470
    move-result v31

    #@471
    if-eqz v31, :cond_514

    #@473
    .line 3589
    const/16 v31, 0x1

    #@475
    move/from16 v0, v31

    #@477
    move-object/from16 v1, p0

    #@479
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@47b
    .line 3590
    move-object/from16 v0, p0

    #@47d
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@47f
    move/from16 v31, v0

    #@481
    move-object/from16 v0, p0

    #@483
    move/from16 v1, v31

    #@485
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelectedPositionInt(I)V

    #@488
    .line 3591
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@48b
    .line 3592
    const/16 v31, 0x1

    #@48d
    move/from16 v0, v31

    #@48f
    invoke-virtual {v5, v0}, Landroid/view/View;->setPressed(Z)V

    #@492
    .line 3593
    move-object/from16 v0, p0

    #@494
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@496
    move/from16 v31, v0

    #@498
    move-object/from16 v0, p0

    #@49a
    move/from16 v1, v31

    #@49c
    invoke-virtual {v0, v1, v5}, Landroid/widget/AbsListView;->positionSelector(ILandroid/view/View;)V

    #@49f
    .line 3594
    const/16 v31, 0x1

    #@4a1
    move-object/from16 v0, p0

    #@4a3
    move/from16 v1, v31

    #@4a5
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@4a8
    .line 3595
    move-object/from16 v0, p0

    #@4aa
    iget-object v0, v0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@4ac
    move-object/from16 v31, v0

    #@4ae
    if-eqz v31, :cond_4c7

    #@4b0
    .line 3596
    move-object/from16 v0, p0

    #@4b2
    iget-object v0, v0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@4b4
    move-object/from16 v31, v0

    #@4b6
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    #@4b9
    move-result-object v9

    #@4ba
    .line 3597
    .local v9, d:Landroid/graphics/drawable/Drawable;
    if-eqz v9, :cond_4c7

    #@4bc
    instance-of v0, v9, Landroid/graphics/drawable/TransitionDrawable;

    #@4be
    move/from16 v31, v0

    #@4c0
    if-eqz v31, :cond_4c7

    #@4c2
    .line 3598
    check-cast v9, Landroid/graphics/drawable/TransitionDrawable;

    #@4c4
    .end local v9           #d:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v9}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    #@4c7
    .line 3601
    :cond_4c7
    move-object/from16 v0, p0

    #@4c9
    iget-object v0, v0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@4cb
    move-object/from16 v31, v0

    #@4cd
    if-eqz v31, :cond_4dc

    #@4cf
    .line 3602
    move-object/from16 v0, p0

    #@4d1
    iget-object v0, v0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@4d3
    move-object/from16 v31, v0

    #@4d5
    move-object/from16 v0, p0

    #@4d7
    move-object/from16 v1, v31

    #@4d9
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@4dc
    .line 3604
    :cond_4dc
    new-instance v31, Landroid/widget/AbsListView$1;

    #@4de
    move-object/from16 v0, v31

    #@4e0
    move-object/from16 v1, p0

    #@4e2
    move-object/from16 v2, v22

    #@4e4
    invoke-direct {v0, v1, v5, v2}, Landroid/widget/AbsListView$1;-><init>(Landroid/widget/AbsListView;Landroid/view/View;Landroid/widget/AbsListView$PerformClick;)V

    #@4e7
    move-object/from16 v0, v31

    #@4e9
    move-object/from16 v1, p0

    #@4eb
    iput-object v0, v1, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@4ed
    .line 3617
    move-object/from16 v0, p0

    #@4ef
    iget-object v0, v0, Landroid/widget/AbsListView;->mTouchModeReset:Ljava/lang/Runnable;

    #@4f1
    move-object/from16 v31, v0

    #@4f3
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    #@4f6
    move-result v32

    #@4f7
    move/from16 v0, v32

    #@4f9
    int-to-long v0, v0

    #@4fa
    move-wide/from16 v32, v0

    #@4fc
    move-object/from16 v0, p0

    #@4fe
    move-object/from16 v1, v31

    #@500
    move-wide/from16 v2, v32

    #@502
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@505
    .line 3623
    :goto_505
    const/16 v31, 0x1

    #@507
    goto/16 :goto_14

    #@509
    .line 3564
    .end local v11           #handler:Landroid/os/Handler;
    .end local v14           #inList:Z
    .end local v22           #performClick:Landroid/widget/AbsListView$PerformClick;
    :cond_509
    const/4 v14, 0x0

    #@50a
    goto/16 :goto_3db

    #@50c
    .line 3584
    .restart local v11       #handler:Landroid/os/Handler;
    .restart local v14       #inList:Z
    .restart local v22       #performClick:Landroid/widget/AbsListView$PerformClick;
    :cond_50c
    move-object/from16 v0, p0

    #@50e
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

    #@510
    move-object/from16 v31, v0

    #@512
    goto/16 :goto_44e

    #@514
    .line 3620
    :cond_514
    const/16 v31, -0x1

    #@516
    move/from16 v0, v31

    #@518
    move-object/from16 v1, p0

    #@51a
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@51c
    .line 3621
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@51f
    goto :goto_505

    #@520
    .line 3624
    .end local v11           #handler:Landroid/os/Handler;
    :cond_520
    move-object/from16 v0, p0

    #@522
    iget-boolean v0, v0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@524
    move/from16 v31, v0

    #@526
    if-nez v31, :cond_53b

    #@528
    move-object/from16 v0, p0

    #@52a
    iget-object v0, v0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@52c
    move-object/from16 v31, v0

    #@52e
    move-object/from16 v0, v31

    #@530
    move/from16 v1, v20

    #@532
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    #@535
    move-result v31

    #@536
    if-eqz v31, :cond_53b

    #@538
    .line 3625
    invoke-virtual/range {v22 .. v22}, Landroid/widget/AbsListView$PerformClick;->run()V

    #@53b
    .line 3628
    .end local v22           #performClick:Landroid/widget/AbsListView$PerformClick;
    :cond_53b
    const/16 v31, -0x1

    #@53d
    move/from16 v0, v31

    #@53f
    move-object/from16 v1, p0

    #@541
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@543
    .line 3629
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@546
    goto/16 :goto_333

    #@548
    .line 3632
    .end local v5           #child:Landroid/view/View;
    .end local v14           #inList:Z
    .end local v20           #motionPosition:I
    .end local v29           #x:F
    :pswitch_548
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@54b
    move-result v6

    #@54c
    .line 3633
    .local v6, childCount:I
    if-lez v6, :cond_6ce

    #@54e
    .line 3634
    const/16 v31, 0x0

    #@550
    move-object/from16 v0, p0

    #@552
    move/from16 v1, v31

    #@554
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@557
    move-result-object v31

    #@558
    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getTop()I

    #@55b
    move-result v10

    #@55c
    .line 3635
    .local v10, firstChildTop:I
    add-int/lit8 v31, v6, -0x1

    #@55e
    move-object/from16 v0, p0

    #@560
    move/from16 v1, v31

    #@562
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@565
    move-result-object v31

    #@566
    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getBottom()I

    #@569
    move-result v19

    #@56a
    .line 3636
    .local v19, lastChildBottom:I
    move-object/from16 v0, p0

    #@56c
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@56e
    move-object/from16 v31, v0

    #@570
    move-object/from16 v0, v31

    #@572
    iget v8, v0, Landroid/graphics/Rect;->top:I

    #@574
    .line 3637
    .local v8, contentTop:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@577
    move-result v31

    #@578
    move-object/from16 v0, p0

    #@57a
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@57c
    move-object/from16 v32, v0

    #@57e
    move-object/from16 v0, v32

    #@580
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@582
    move/from16 v32, v0

    #@584
    sub-int v7, v31, v32

    #@586
    .line 3638
    .local v7, contentBottom:I
    move-object/from16 v0, p0

    #@588
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@58a
    move/from16 v31, v0

    #@58c
    if-nez v31, :cond_5c3

    #@58e
    if-lt v10, v8, :cond_5c3

    #@590
    move-object/from16 v0, p0

    #@592
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@594
    move/from16 v31, v0

    #@596
    add-int v31, v31, v6

    #@598
    move-object/from16 v0, p0

    #@59a
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@59c
    move/from16 v32, v0

    #@59e
    move/from16 v0, v31

    #@5a0
    move/from16 v1, v32

    #@5a2
    if-ge v0, v1, :cond_5c3

    #@5a4
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@5a7
    move-result v31

    #@5a8
    sub-int v31, v31, v7

    #@5aa
    move/from16 v0, v19

    #@5ac
    move/from16 v1, v31

    #@5ae
    if-gt v0, v1, :cond_5c3

    #@5b0
    .line 3641
    const/16 v31, -0x1

    #@5b2
    move/from16 v0, v31

    #@5b4
    move-object/from16 v1, p0

    #@5b6
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@5b8
    .line 3642
    const/16 v31, 0x0

    #@5ba
    move-object/from16 v0, p0

    #@5bc
    move/from16 v1, v31

    #@5be
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@5c1
    goto/16 :goto_333

    #@5c3
    .line 3644
    :cond_5c3
    move-object/from16 v0, p0

    #@5c5
    iget-object v0, v0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@5c7
    move-object/from16 v28, v0

    #@5c9
    .line 3645
    .local v28, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v31, 0x3e8

    #@5cb
    move-object/from16 v0, p0

    #@5cd
    iget v0, v0, Landroid/widget/AbsListView;->mMaximumVelocity:I

    #@5cf
    move/from16 v32, v0

    #@5d1
    move/from16 v0, v32

    #@5d3
    int-to-float v0, v0

    #@5d4
    move/from16 v32, v0

    #@5d6
    move-object/from16 v0, v28

    #@5d8
    move/from16 v1, v31

    #@5da
    move/from16 v2, v32

    #@5dc
    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@5df
    .line 3648
    move-object/from16 v0, p0

    #@5e1
    iget v0, v0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@5e3
    move/from16 v31, v0

    #@5e5
    move-object/from16 v0, v28

    #@5e7
    move/from16 v1, v31

    #@5e9
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@5ec
    move-result v31

    #@5ed
    move-object/from16 v0, p0

    #@5ef
    iget v0, v0, Landroid/widget/AbsListView;->mVelocityScale:F

    #@5f1
    move/from16 v32, v0

    #@5f3
    mul-float v31, v31, v32

    #@5f5
    move/from16 v0, v31

    #@5f7
    float-to-int v0, v0

    #@5f8
    move/from16 v16, v0

    #@5fa
    .line 3650
    .local v16, initialVelocity:I
    sget-boolean v31, Landroid/widget/AbsListView;->mCapptouchFlickNoti:Z

    #@5fc
    if-eqz v31, :cond_623

    #@5fe
    .line 3651
    move-object/from16 v0, p0

    #@600
    iget v0, v0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@602
    move/from16 v31, v0

    #@604
    move-object/from16 v0, v28

    #@606
    move/from16 v1, v31

    #@608
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@60b
    move-result v31

    #@60c
    move-object/from16 v0, p0

    #@60e
    iget v0, v0, Landroid/widget/AbsListView;->mVelocityScale:F

    #@610
    move/from16 v32, v0

    #@612
    mul-float v31, v31, v32

    #@614
    move/from16 v0, v31

    #@616
    float-to-int v0, v0

    #@617
    move/from16 v17, v0

    #@619
    .line 3652
    .local v17, initialXVelocity:I
    move-object/from16 v0, p0

    #@61b
    move/from16 v1, v16

    #@61d
    move/from16 v2, v17

    #@61f
    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView;->getFlickValue(II)I

    #@622
    move-result v16

    #@623
    .line 3658
    .end local v17           #initialXVelocity:I
    :cond_623
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(I)I

    #@626
    move-result v31

    #@627
    move-object/from16 v0, p0

    #@629
    iget v0, v0, Landroid/widget/AbsListView;->mMinimumVelocity:I

    #@62b
    move/from16 v32, v0

    #@62d
    move/from16 v0, v31

    #@62f
    move/from16 v1, v32

    #@631
    if-le v0, v1, :cond_699

    #@633
    move-object/from16 v0, p0

    #@635
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@637
    move/from16 v31, v0

    #@639
    if-nez v31, :cond_647

    #@63b
    move-object/from16 v0, p0

    #@63d
    iget v0, v0, Landroid/widget/AbsListView;->mOverscrollDistance:I

    #@63f
    move/from16 v31, v0

    #@641
    sub-int v31, v8, v31

    #@643
    move/from16 v0, v31

    #@645
    if-eq v10, v0, :cond_699

    #@647
    :cond_647
    move-object/from16 v0, p0

    #@649
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@64b
    move/from16 v31, v0

    #@64d
    add-int v31, v31, v6

    #@64f
    move-object/from16 v0, p0

    #@651
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@653
    move/from16 v32, v0

    #@655
    move/from16 v0, v31

    #@657
    move/from16 v1, v32

    #@659
    if-ne v0, v1, :cond_669

    #@65b
    move-object/from16 v0, p0

    #@65d
    iget v0, v0, Landroid/widget/AbsListView;->mOverscrollDistance:I

    #@65f
    move/from16 v31, v0

    #@661
    add-int v31, v31, v7

    #@663
    move/from16 v0, v19

    #@665
    move/from16 v1, v31

    #@667
    if-eq v0, v1, :cond_699

    #@669
    .line 3663
    :cond_669
    move-object/from16 v0, p0

    #@66b
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@66d
    move-object/from16 v31, v0

    #@66f
    if-nez v31, :cond_680

    #@671
    .line 3664
    new-instance v31, Landroid/widget/AbsListView$FlingRunnable;

    #@673
    move-object/from16 v0, v31

    #@675
    move-object/from16 v1, p0

    #@677
    invoke-direct {v0, v1}, Landroid/widget/AbsListView$FlingRunnable;-><init>(Landroid/widget/AbsListView;)V

    #@67a
    move-object/from16 v0, v31

    #@67c
    move-object/from16 v1, p0

    #@67e
    iput-object v0, v1, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@680
    .line 3666
    :cond_680
    const/16 v31, 0x2

    #@682
    move-object/from16 v0, p0

    #@684
    move/from16 v1, v31

    #@686
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@689
    .line 3668
    move-object/from16 v0, p0

    #@68b
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@68d
    move-object/from16 v31, v0

    #@68f
    move/from16 v0, v16

    #@691
    neg-int v0, v0

    #@692
    move/from16 v32, v0

    #@694
    invoke-virtual/range {v31 .. v32}, Landroid/widget/AbsListView$FlingRunnable;->start(I)V

    #@697
    goto/16 :goto_333

    #@699
    .line 3670
    :cond_699
    const/16 v31, -0x1

    #@69b
    move/from16 v0, v31

    #@69d
    move-object/from16 v1, p0

    #@69f
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@6a1
    .line 3671
    const/16 v31, 0x0

    #@6a3
    move-object/from16 v0, p0

    #@6a5
    move/from16 v1, v31

    #@6a7
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@6aa
    .line 3672
    move-object/from16 v0, p0

    #@6ac
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@6ae
    move-object/from16 v31, v0

    #@6b0
    if-eqz v31, :cond_6bb

    #@6b2
    .line 3673
    move-object/from16 v0, p0

    #@6b4
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@6b6
    move-object/from16 v31, v0

    #@6b8
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@6bb
    .line 3675
    :cond_6bb
    move-object/from16 v0, p0

    #@6bd
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@6bf
    move-object/from16 v31, v0

    #@6c1
    if-eqz v31, :cond_333

    #@6c3
    .line 3676
    move-object/from16 v0, p0

    #@6c5
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@6c7
    move-object/from16 v31, v0

    #@6c9
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@6cc
    goto/16 :goto_333

    #@6ce
    .line 3681
    .end local v7           #contentBottom:I
    .end local v8           #contentTop:I
    .end local v10           #firstChildTop:I
    .end local v16           #initialVelocity:I
    .end local v19           #lastChildBottom:I
    .end local v28           #velocityTracker:Landroid/view/VelocityTracker;
    :cond_6ce
    const/16 v31, -0x1

    #@6d0
    move/from16 v0, v31

    #@6d2
    move-object/from16 v1, p0

    #@6d4
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@6d6
    .line 3682
    const/16 v31, 0x0

    #@6d8
    move-object/from16 v0, p0

    #@6da
    move/from16 v1, v31

    #@6dc
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@6df
    goto/16 :goto_333

    #@6e1
    .line 3687
    .end local v6           #childCount:I
    :pswitch_6e1
    move-object/from16 v0, p0

    #@6e3
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@6e5
    move-object/from16 v31, v0

    #@6e7
    if-nez v31, :cond_6f8

    #@6e9
    .line 3688
    new-instance v31, Landroid/widget/AbsListView$FlingRunnable;

    #@6eb
    move-object/from16 v0, v31

    #@6ed
    move-object/from16 v1, p0

    #@6ef
    invoke-direct {v0, v1}, Landroid/widget/AbsListView$FlingRunnable;-><init>(Landroid/widget/AbsListView;)V

    #@6f2
    move-object/from16 v0, v31

    #@6f4
    move-object/from16 v1, p0

    #@6f6
    iput-object v0, v1, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@6f8
    .line 3690
    :cond_6f8
    move-object/from16 v0, p0

    #@6fa
    iget-object v0, v0, Landroid/widget/AbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6fc
    move-object/from16 v28, v0

    #@6fe
    .line 3691
    .restart local v28       #velocityTracker:Landroid/view/VelocityTracker;
    const/16 v31, 0x3e8

    #@700
    move-object/from16 v0, p0

    #@702
    iget v0, v0, Landroid/widget/AbsListView;->mMaximumVelocity:I

    #@704
    move/from16 v32, v0

    #@706
    move/from16 v0, v32

    #@708
    int-to-float v0, v0

    #@709
    move/from16 v32, v0

    #@70b
    move-object/from16 v0, v28

    #@70d
    move/from16 v1, v31

    #@70f
    move/from16 v2, v32

    #@711
    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@714
    .line 3692
    move-object/from16 v0, p0

    #@716
    iget v0, v0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@718
    move/from16 v31, v0

    #@71a
    move-object/from16 v0, v28

    #@71c
    move/from16 v1, v31

    #@71e
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@721
    move-result v31

    #@722
    move/from16 v0, v31

    #@724
    float-to-int v0, v0

    #@725
    move/from16 v16, v0

    #@727
    .line 3694
    .restart local v16       #initialVelocity:I
    const/16 v31, 0x2

    #@729
    move-object/from16 v0, p0

    #@72b
    move/from16 v1, v31

    #@72d
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@730
    .line 3695
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(I)I

    #@733
    move-result v31

    #@734
    move-object/from16 v0, p0

    #@736
    iget v0, v0, Landroid/widget/AbsListView;->mMinimumVelocity:I

    #@738
    move/from16 v32, v0

    #@73a
    move/from16 v0, v31

    #@73c
    move/from16 v1, v32

    #@73e
    if-le v0, v1, :cond_750

    #@740
    .line 3696
    move-object/from16 v0, p0

    #@742
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@744
    move-object/from16 v31, v0

    #@746
    move/from16 v0, v16

    #@748
    neg-int v0, v0

    #@749
    move/from16 v32, v0

    #@74b
    invoke-virtual/range {v31 .. v32}, Landroid/widget/AbsListView$FlingRunnable;->startOverfling(I)V

    #@74e
    goto/16 :goto_333

    #@750
    .line 3698
    :cond_750
    move-object/from16 v0, p0

    #@752
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@754
    move-object/from16 v31, v0

    #@756
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->startSpringback()V

    #@759
    goto/16 :goto_333

    #@75b
    .line 3738
    .end local v16           #initialVelocity:I
    .end local v28           #velocityTracker:Landroid/view/VelocityTracker;
    :pswitch_75b
    sget-boolean v31, Landroid/widget/AbsListView;->mCapptouchFlickNoti:Z

    #@75d
    if-eqz v31, :cond_77b

    #@75f
    .line 3739
    const/16 v31, 0x0

    #@761
    sput v31, Landroid/widget/AbsListView;->eventMonitor:I

    #@763
    .line 3740
    const/16 v31, 0x0

    #@765
    sput v31, Landroid/widget/AbsListView;->moveCounter:I

    #@767
    .line 3741
    const/4 v12, 0x0

    #@768
    .local v12, i:I
    :goto_768
    sget v31, Landroid/widget/AbsListView;->COORDINATE_DIRECTION:I

    #@76a
    move/from16 v0, v31

    #@76c
    if-ge v12, v0, :cond_77b

    #@76e
    .line 3742
    move-object/from16 v0, p0

    #@770
    iget-object v0, v0, Landroid/widget/AbsListView;->coorDirection:[I

    #@772
    move-object/from16 v31, v0

    #@774
    const/16 v32, 0x0

    #@776
    aput v32, v31, v12

    #@778
    .line 3741
    add-int/lit8 v12, v12, 0x1

    #@77a
    goto :goto_768

    #@77b
    .line 3745
    .end local v12           #i:I
    :cond_77b
    move-object/from16 v0, p0

    #@77d
    iget v0, v0, Landroid/widget/AbsListView;->mTouchMode:I

    #@77f
    move/from16 v31, v0

    #@781
    packed-switch v31, :pswitch_data_8fe

    #@784
    .line 3758
    const/16 v31, -0x1

    #@786
    move/from16 v0, v31

    #@788
    move-object/from16 v1, p0

    #@78a
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@78c
    .line 3759
    const/16 v31, 0x0

    #@78e
    move-object/from16 v0, p0

    #@790
    move/from16 v1, v31

    #@792
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setPressed(Z)V

    #@795
    .line 3760
    move-object/from16 v0, p0

    #@797
    iget v0, v0, Landroid/widget/AbsListView;->mMotionPosition:I

    #@799
    move/from16 v31, v0

    #@79b
    move-object/from16 v0, p0

    #@79d
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@79f
    move/from16 v32, v0

    #@7a1
    sub-int v31, v31, v32

    #@7a3
    move-object/from16 v0, p0

    #@7a5
    move/from16 v1, v31

    #@7a7
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@7aa
    move-result-object v21

    #@7ab
    .line 3761
    .local v21, motionView:Landroid/view/View;
    if-eqz v21, :cond_7b6

    #@7ad
    .line 3762
    const/16 v31, 0x0

    #@7af
    move-object/from16 v0, v21

    #@7b1
    move/from16 v1, v31

    #@7b3
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    #@7b6
    .line 3764
    :cond_7b6
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->clearScrollingCache()V

    #@7b9
    .line 3766
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHandler()Landroid/os/Handler;

    #@7bc
    move-result-object v11

    #@7bd
    .line 3767
    .restart local v11       #handler:Landroid/os/Handler;
    if-eqz v11, :cond_7ca

    #@7bf
    .line 3768
    move-object/from16 v0, p0

    #@7c1
    iget-object v0, v0, Landroid/widget/AbsListView;->mPendingCheckForLongPress:Landroid/widget/AbsListView$CheckForLongPress;

    #@7c3
    move-object/from16 v31, v0

    #@7c5
    move-object/from16 v0, v31

    #@7c7
    invoke-virtual {v11, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7ca
    .line 3771
    :cond_7ca
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->recycleVelocityTracker()V

    #@7cd
    .line 3774
    .end local v11           #handler:Landroid/os/Handler;
    .end local v21           #motionView:Landroid/view/View;
    :goto_7cd
    :pswitch_7cd
    move-object/from16 v0, p0

    #@7cf
    iget-object v0, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@7d1
    move-object/from16 v31, v0

    #@7d3
    if-eqz v31, :cond_7e7

    #@7d5
    .line 3775
    move-object/from16 v0, p0

    #@7d7
    iget-object v0, v0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@7d9
    move-object/from16 v31, v0

    #@7db
    invoke-virtual/range {v31 .. v31}, Landroid/widget/EdgeEffect;->onRelease()V

    #@7de
    .line 3776
    move-object/from16 v0, p0

    #@7e0
    iget-object v0, v0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@7e2
    move-object/from16 v31, v0

    #@7e4
    invoke-virtual/range {v31 .. v31}, Landroid/widget/EdgeEffect;->onRelease()V

    #@7e7
    .line 3778
    :cond_7e7
    const/16 v31, -0x1

    #@7e9
    move/from16 v0, v31

    #@7eb
    move-object/from16 v1, p0

    #@7ed
    iput v0, v1, Landroid/widget/AbsListView;->mActivePointerId:I

    #@7ef
    goto/16 :goto_9b

    #@7f1
    .line 3747
    :pswitch_7f1
    move-object/from16 v0, p0

    #@7f3
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@7f5
    move-object/from16 v31, v0

    #@7f7
    if-nez v31, :cond_808

    #@7f9
    .line 3748
    new-instance v31, Landroid/widget/AbsListView$FlingRunnable;

    #@7fb
    move-object/from16 v0, v31

    #@7fd
    move-object/from16 v1, p0

    #@7ff
    invoke-direct {v0, v1}, Landroid/widget/AbsListView$FlingRunnable;-><init>(Landroid/widget/AbsListView;)V

    #@802
    move-object/from16 v0, v31

    #@804
    move-object/from16 v1, p0

    #@806
    iput-object v0, v1, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@808
    .line 3750
    :cond_808
    move-object/from16 v0, p0

    #@80a
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@80c
    move-object/from16 v31, v0

    #@80e
    invoke-virtual/range {v31 .. v31}, Landroid/widget/AbsListView$FlingRunnable;->startSpringback()V

    #@811
    goto :goto_7cd

    #@812
    .line 3783
    :pswitch_812
    invoke-direct/range {p0 .. p1}, Landroid/widget/AbsListView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@815
    .line 3784
    move-object/from16 v0, p0

    #@817
    iget v0, v0, Landroid/widget/AbsListView;->mMotionX:I

    #@819
    move/from16 v29, v0

    #@81b
    .line 3785
    .local v29, x:I
    move-object/from16 v0, p0

    #@81d
    iget v0, v0, Landroid/widget/AbsListView;->mMotionY:I

    #@81f
    move/from16 v30, v0

    #@821
    .line 3786
    .restart local v30       #y:I
    move-object/from16 v0, p0

    #@823
    move/from16 v1, v29

    #@825
    move/from16 v2, v30

    #@827
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->pointToPosition(II)I

    #@82a
    move-result v20

    #@82b
    .line 3787
    .restart local v20       #motionPosition:I
    if-ltz v20, :cond_84f

    #@82d
    .line 3789
    move-object/from16 v0, p0

    #@82f
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@831
    move/from16 v31, v0

    #@833
    sub-int v31, v20, v31

    #@835
    move-object/from16 v0, p0

    #@837
    move/from16 v1, v31

    #@839
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@83c
    move-result-object v26

    #@83d
    .line 3791
    .restart local v26       #v:Landroid/view/View;
    if-eqz v26, :cond_84f

    #@83f
    .line 3792
    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getTop()I

    #@842
    move-result v31

    #@843
    move/from16 v0, v31

    #@845
    move-object/from16 v1, p0

    #@847
    iput v0, v1, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@849
    .line 3793
    move/from16 v0, v20

    #@84b
    move-object/from16 v1, p0

    #@84d
    iput v0, v1, Landroid/widget/AbsListView;->mMotionPosition:I

    #@84f
    .line 3797
    .end local v26           #v:Landroid/view/View;
    :cond_84f
    move/from16 v0, v30

    #@851
    move-object/from16 v1, p0

    #@853
    iput v0, v1, Landroid/widget/AbsListView;->mLastY:I

    #@855
    goto/16 :goto_9b

    #@857
    .line 3803
    .end local v20           #motionPosition:I
    .end local v29           #x:I
    .end local v30           #y:I
    :pswitch_857
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@85a
    move-result v15

    #@85b
    .line 3804
    .local v15, index:I
    move-object/from16 v0, p1

    #@85d
    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@860
    move-result v13

    #@861
    .line 3805
    .local v13, id:I
    move-object/from16 v0, p1

    #@863
    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getX(I)F

    #@866
    move-result v31

    #@867
    move/from16 v0, v31

    #@869
    float-to-int v0, v0

    #@86a
    move/from16 v29, v0

    #@86c
    .line 3806
    .restart local v29       #x:I
    move-object/from16 v0, p1

    #@86e
    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getY(I)F

    #@871
    move-result v31

    #@872
    move/from16 v0, v31

    #@874
    float-to-int v0, v0

    #@875
    move/from16 v30, v0

    #@877
    .line 3807
    .restart local v30       #y:I
    const/16 v31, 0x0

    #@879
    move/from16 v0, v31

    #@87b
    move-object/from16 v1, p0

    #@87d
    iput v0, v1, Landroid/widget/AbsListView;->mMotionCorrection:I

    #@87f
    .line 3808
    move-object/from16 v0, p0

    #@881
    iput v13, v0, Landroid/widget/AbsListView;->mActivePointerId:I

    #@883
    .line 3809
    move/from16 v0, v29

    #@885
    move-object/from16 v1, p0

    #@887
    iput v0, v1, Landroid/widget/AbsListView;->mMotionX:I

    #@889
    .line 3810
    move/from16 v0, v30

    #@88b
    move-object/from16 v1, p0

    #@88d
    iput v0, v1, Landroid/widget/AbsListView;->mMotionY:I

    #@88f
    .line 3811
    move-object/from16 v0, p0

    #@891
    move/from16 v1, v29

    #@893
    move/from16 v2, v30

    #@895
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->pointToPosition(II)I

    #@898
    move-result v20

    #@899
    .line 3812
    .restart local v20       #motionPosition:I
    if-ltz v20, :cond_8bd

    #@89b
    .line 3814
    move-object/from16 v0, p0

    #@89d
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@89f
    move/from16 v31, v0

    #@8a1
    sub-int v31, v20, v31

    #@8a3
    move-object/from16 v0, p0

    #@8a5
    move/from16 v1, v31

    #@8a7
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@8aa
    move-result-object v26

    #@8ab
    .line 3816
    .restart local v26       #v:Landroid/view/View;
    if-eqz v26, :cond_8bd

    #@8ad
    .line 3817
    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getTop()I

    #@8b0
    move-result v31

    #@8b1
    move/from16 v0, v31

    #@8b3
    move-object/from16 v1, p0

    #@8b5
    iput v0, v1, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@8b7
    .line 3818
    move/from16 v0, v20

    #@8b9
    move-object/from16 v1, p0

    #@8bb
    iput v0, v1, Landroid/widget/AbsListView;->mMotionPosition:I

    #@8bd
    .line 3822
    .end local v26           #v:Landroid/view/View;
    :cond_8bd
    move/from16 v0, v30

    #@8bf
    move-object/from16 v1, p0

    #@8c1
    iput v0, v1, Landroid/widget/AbsListView;->mLastY:I

    #@8c3
    goto/16 :goto_9b

    #@8c5
    .line 3441
    nop

    #@8c6
    :pswitch_data_8c6
    .packed-switch 0x0
        :pswitch_c0
        :pswitch_32a
        :pswitch_2cf
        :pswitch_75b
        :pswitch_9b
        :pswitch_857
        :pswitch_812
    .end packed-switch

    #@8d8
    .line 3443
    :pswitch_data_8d8
    .packed-switch 0x6
        :pswitch_22b
    .end packed-switch

    #@8de
    .line 3539
    :pswitch_data_8de
    .packed-switch 0x0
        :pswitch_318
        :pswitch_318
        :pswitch_318
        :pswitch_321
        :pswitch_316
        :pswitch_321
    .end packed-switch

    #@8ee
    .line 3556
    :pswitch_data_8ee
    .packed-switch 0x0
        :pswitch_390
        :pswitch_390
        :pswitch_390
        :pswitch_548
        :pswitch_333
        :pswitch_6e1
    .end packed-switch

    #@8fe
    .line 3745
    :pswitch_data_8fe
    .packed-switch 0x5
        :pswitch_7f1
        :pswitch_7cd
    .end packed-switch
.end method

.method public onTouchModeChanged(Z)V
    .registers 4
    .parameter "isInTouchMode"

    #@0
    .prologue
    .line 3237
    if-eqz p1, :cond_18

    #@2
    .line 3239
    invoke-virtual {p0}, Landroid/widget/AbsListView;->hideSelector()V

    #@5
    .line 3243
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@8
    move-result v1

    #@9
    if-lez v1, :cond_14

    #@b
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@e
    move-result v1

    #@f
    if-lez v1, :cond_14

    #@11
    .line 3246
    invoke-virtual {p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@14
    .line 3248
    :cond_14
    invoke-virtual {p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@17
    .line 3267
    :cond_17
    :goto_17
    return-void

    #@18
    .line 3250
    :cond_18
    iget v0, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@1a
    .line 3251
    .local v0, touchMode:I
    const/4 v1, 0x5

    #@1b
    if-eq v0, v1, :cond_20

    #@1d
    const/4 v1, 0x6

    #@1e
    if-ne v0, v1, :cond_17

    #@20
    .line 3252
    :cond_20
    iget-object v1, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 3253
    iget-object v1, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@26
    invoke-virtual {v1}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@29
    .line 3255
    :cond_29
    iget-object v1, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@2b
    if-eqz v1, :cond_32

    #@2d
    .line 3256
    iget-object v1, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@2f
    invoke-virtual {v1}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@32
    .line 3259
    :cond_32
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@34
    if-eqz v1, :cond_17

    #@36
    .line 3260
    const/4 v1, 0x0

    #@37
    iput v1, p0, Landroid/view/View;->mScrollY:I

    #@39
    .line 3261
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidateParentCaches()V

    #@3c
    .line 3262
    invoke-direct {p0}, Landroid/widget/AbsListView;->finishGlows()V

    #@3f
    .line 3263
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidate()V

    #@42
    goto :goto_17
.end method

.method public onWindowFocusChanged(Z)V
    .registers 7
    .parameter "hasWindowFocus"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 2680
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onWindowFocusChanged(Z)V

    #@5
    .line 2682
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isInTouchMode()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_43

    #@b
    move v0, v1

    #@c
    .line 2684
    .local v0, touchMode:I
    :goto_c
    if-nez p1, :cond_45

    #@e
    .line 2685
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->setChildrenDrawingCacheEnabled(Z)V

    #@11
    .line 2686
    iget-object v3, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@13
    if-eqz v3, :cond_37

    #@15
    .line 2687
    iget-object v3, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@17
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1a
    .line 2690
    iget-object v3, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@1c
    invoke-virtual {v3}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@1f
    .line 2691
    iget-object v3, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@21
    if-eqz v3, :cond_28

    #@23
    .line 2692
    iget-object v3, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@25
    invoke-virtual {v3}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@28
    .line 2694
    :cond_28
    iget v3, p0, Landroid/view/View;->mScrollY:I

    #@2a
    if-eqz v3, :cond_37

    #@2c
    .line 2695
    iput v1, p0, Landroid/view/View;->mScrollY:I

    #@2e
    .line 2696
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidateParentCaches()V

    #@31
    .line 2697
    invoke-direct {p0}, Landroid/widget/AbsListView;->finishGlows()V

    #@34
    .line 2698
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidate()V

    #@37
    .line 2702
    :cond_37
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@3a
    .line 2704
    if-ne v0, v2, :cond_40

    #@3c
    .line 2706
    iget v1, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@3e
    iput v1, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@40
    .line 2730
    :cond_40
    :goto_40
    iput v0, p0, Landroid/widget/AbsListView;->mLastTouchMode:I

    #@42
    .line 2731
    return-void

    #@43
    .end local v0           #touchMode:I
    :cond_43
    move v0, v2

    #@44
    .line 2682
    goto :goto_c

    #@45
    .line 2709
    .restart local v0       #touchMode:I
    :cond_45
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@47
    if-eqz v3, :cond_50

    #@49
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mPopupHidden:Z

    #@4b
    if-nez v3, :cond_50

    #@4d
    .line 2711
    invoke-direct {p0}, Landroid/widget/AbsListView;->showPopup()V

    #@50
    .line 2715
    :cond_50
    iget v3, p0, Landroid/widget/AbsListView;->mLastTouchMode:I

    #@52
    if-eq v0, v3, :cond_40

    #@54
    iget v3, p0, Landroid/widget/AbsListView;->mLastTouchMode:I

    #@56
    const/4 v4, -0x1

    #@57
    if-eq v3, v4, :cond_40

    #@59
    .line 2717
    if-ne v0, v2, :cond_5f

    #@5b
    .line 2719
    invoke-virtual {p0}, Landroid/widget/AbsListView;->resurrectSelection()Z

    #@5e
    goto :goto_40

    #@5f
    .line 2723
    :cond_5f
    invoke-virtual {p0}, Landroid/widget/AbsListView;->hideSelector()V

    #@62
    .line 2724
    iput v1, p0, Landroid/widget/AbsListView;->mLayoutMode:I

    #@64
    .line 2725
    invoke-virtual {p0}, Landroid/widget/AbsListView;->layoutChildren()V

    #@67
    goto :goto_40
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 9
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/16 v5, 0xc8

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 1415
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_b

    #@a
    .line 1434
    :goto_a
    return v1

    #@b
    .line 1418
    :cond_b
    sparse-switch p1, :sswitch_data_58

    #@e
    move v1, v2

    #@f
    .line 1434
    goto :goto_a

    #@10
    .line 1420
    :sswitch_10
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isEnabled()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_35

    #@16
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getCount()I

    #@1d
    move-result v4

    #@1e
    add-int/lit8 v4, v4, -0x1

    #@20
    if-ge v3, v4, :cond_35

    #@22
    .line 1421
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@25
    move-result v2

    #@26
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@28
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@2a
    sub-int/2addr v2, v3

    #@2b
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@2d
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@2f
    sub-int v0, v2, v3

    #@31
    .line 1422
    .local v0, viewportHeight:I
    invoke-virtual {p0, v0, v5}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    #@34
    goto :goto_a

    #@35
    .end local v0           #viewportHeight:I
    :cond_35
    move v1, v2

    #@36
    .line 1425
    goto :goto_a

    #@37
    .line 1427
    :sswitch_37
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isEnabled()Z

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_55

    #@3d
    iget v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@3f
    if-lez v3, :cond_55

    #@41
    .line 1428
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@44
    move-result v2

    #@45
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@47
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@49
    sub-int/2addr v2, v3

    #@4a
    iget-object v3, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@4c
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@4e
    sub-int v0, v2, v3

    #@50
    .line 1429
    .restart local v0       #viewportHeight:I
    neg-int v2, v0

    #@51
    invoke-virtual {p0, v2, v5}, Landroid/widget/AbsListView;->smoothScrollBy(II)V

    #@54
    goto :goto_a

    #@55
    .end local v0           #viewportHeight:I
    :cond_55
    move v1, v2

    #@56
    .line 1432
    goto :goto_a

    #@57
    .line 1418
    nop

    #@58
    :sswitch_data_58
    .sparse-switch
        0x1000 -> :sswitch_10
        0x2000 -> :sswitch_37
    .end sparse-switch
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .registers 14
    .parameter "view"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1078
    const/4 v8, 0x0

    #@3
    .line 1079
    .local v8, handled:Z
    const/4 v7, 0x1

    #@4
    .line 1081
    .local v7, dispatchItemClick:Z
    iget v2, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@6
    if-eqz v2, :cond_5f

    #@8
    .line 1082
    const/4 v8, 0x1

    #@9
    .line 1083
    const/4 v6, 0x0

    #@a
    .line 1085
    .local v6, checkedStateChanged:Z
    iget v2, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@c
    const/4 v3, 0x2

    #@d
    if-eq v2, v3, :cond_18

    #@f
    iget v2, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@11
    const/4 v3, 0x3

    #@12
    if-ne v2, v3, :cond_7c

    #@14
    iget-object v2, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@16
    if-eqz v2, :cond_7c

    #@18
    .line 1087
    :cond_18
    iget-object v2, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@1a
    invoke-virtual {v2, p2, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_67

    #@20
    move v5, v0

    #@21
    .line 1088
    .local v5, checked:Z
    :goto_21
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@23
    invoke-virtual {v0, p2, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@26
    .line 1089
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@28
    if-eqz v0, :cond_43

    #@2a
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@2c
    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_43

    #@32
    .line 1090
    if-eqz v5, :cond_69

    #@34
    .line 1091
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@36
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@38
    invoke-interface {v1, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@3b
    move-result-wide v1

    #@3c
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@43
    .line 1096
    :cond_43
    :goto_43
    if-eqz v5, :cond_75

    #@45
    .line 1097
    iget v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@47
    add-int/lit8 v0, v0, 0x1

    #@49
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@4b
    .line 1101
    :goto_4b
    iget-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@4d
    if-eqz v0, :cond_59

    #@4f
    .line 1102
    iget-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@51
    iget-object v1, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@53
    move v2, p2

    #@54
    move-wide v3, p3

    #@55
    invoke-virtual/range {v0 .. v5}, Landroid/widget/AbsListView$MultiChoiceModeWrapper;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    #@58
    .line 1104
    const/4 v7, 0x0

    #@59
    .line 1106
    :cond_59
    const/4 v6, 0x1

    #@5a
    .line 1123
    .end local v5           #checked:Z
    :cond_5a
    :goto_5a
    if-eqz v6, :cond_5f

    #@5c
    .line 1124
    invoke-direct {p0}, Landroid/widget/AbsListView;->updateOnScreenCheckedViews()V

    #@5f
    .line 1128
    .end local v6           #checkedStateChanged:Z
    :cond_5f
    if-eqz v7, :cond_66

    #@61
    .line 1129
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->performItemClick(Landroid/view/View;IJ)Z

    #@64
    move-result v0

    #@65
    or-int/2addr v8, v0

    #@66
    .line 1132
    :cond_66
    return v8

    #@67
    .restart local v6       #checkedStateChanged:Z
    :cond_67
    move v5, v1

    #@68
    .line 1087
    goto :goto_21

    #@69
    .line 1093
    .restart local v5       #checked:Z
    :cond_69
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@6b
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@6d
    invoke-interface {v1, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@70
    move-result-wide v1

    #@71
    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->delete(J)V

    #@74
    goto :goto_43

    #@75
    .line 1099
    :cond_75
    iget v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@77
    add-int/lit8 v0, v0, -0x1

    #@79
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@7b
    goto :goto_4b

    #@7c
    .line 1107
    .end local v5           #checked:Z
    :cond_7c
    iget v2, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@7e
    if-ne v2, v0, :cond_5a

    #@80
    .line 1108
    iget-object v2, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@82
    invoke-virtual {v2, p2, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    #@85
    move-result v2

    #@86
    if-nez v2, :cond_b9

    #@88
    move v5, v0

    #@89
    .line 1109
    .restart local v5       #checked:Z
    :goto_89
    if-eqz v5, :cond_bb

    #@8b
    .line 1110
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@8d
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    #@90
    .line 1111
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@92
    invoke-virtual {v1, p2, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@95
    .line 1112
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@97
    if-eqz v1, :cond_b5

    #@99
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@9b
    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@9e
    move-result v1

    #@9f
    if-eqz v1, :cond_b5

    #@a1
    .line 1113
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@a3
    invoke-virtual {v1}, Landroid/util/LongSparseArray;->clear()V

    #@a6
    .line 1114
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@a8
    iget-object v2, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@aa
    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@ad
    move-result-wide v2

    #@ae
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b1
    move-result-object v4

    #@b2
    invoke-virtual {v1, v2, v3, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@b5
    .line 1116
    :cond_b5
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@b7
    .line 1120
    :cond_b7
    :goto_b7
    const/4 v6, 0x1

    #@b8
    goto :goto_5a

    #@b9
    .end local v5           #checked:Z
    :cond_b9
    move v5, v1

    #@ba
    .line 1108
    goto :goto_89

    #@bb
    .line 1117
    .restart local v5       #checked:Z
    :cond_bb
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@bd
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    #@c0
    move-result v0

    #@c1
    if-eqz v0, :cond_cb

    #@c3
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@c5
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@c8
    move-result v0

    #@c9
    if-nez v0, :cond_b7

    #@cb
    .line 1118
    :cond_cb
    iput v1, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@cd
    goto :goto_b7
.end method

.method performLongPress(Landroid/view/View;IJ)Z
    .registers 13
    .parameter "child"
    .parameter "longPressPosition"
    .parameter "longPressId"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 2837
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@4
    const/4 v1, 0x3

    #@5
    if-ne v0, v1, :cond_1c

    #@7
    .line 2838
    iget-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@9
    if-nez v0, :cond_1b

    #@b
    iget-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@d
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@13
    if-eqz v0, :cond_1b

    #@15
    .line 2840
    invoke-virtual {p0, p2, v6}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    #@18
    .line 2841
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->performHapticFeedback(I)Z

    #@1b
    .line 2858
    :cond_1b
    :goto_1b
    return v6

    #@1c
    .line 2846
    :cond_1c
    const/4 v6, 0x0

    #@1d
    .line 2847
    .local v6, handled:Z
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@1f
    if-eqz v0, :cond_2b

    #@21
    .line 2848
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@23
    move-object v1, p0

    #@24
    move-object v2, p1

    #@25
    move v3, p2

    #@26
    move-wide v4, p3

    #@27
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    #@2a
    move-result v6

    #@2b
    .line 2851
    :cond_2b
    if-nez v6, :cond_37

    #@2d
    .line 2852
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/widget/AbsListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/widget/AbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@33
    .line 2853
    invoke-super {p0, p0}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    #@36
    move-result v6

    #@37
    .line 2855
    :cond_37
    if-eqz v6, :cond_1b

    #@39
    .line 2856
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->performHapticFeedback(I)Z

    #@3c
    goto :goto_1b
.end method

.method public pointToPosition(II)I
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 2949
    iget-object v2, p0, Landroid/widget/AbsListView;->mTouchFrame:Landroid/graphics/Rect;

    #@2
    .line 2950
    .local v2, frame:Landroid/graphics/Rect;
    if-nez v2, :cond_d

    #@4
    .line 2951
    new-instance v4, Landroid/graphics/Rect;

    #@6
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v4, p0, Landroid/widget/AbsListView;->mTouchFrame:Landroid/graphics/Rect;

    #@b
    .line 2952
    iget-object v2, p0, Landroid/widget/AbsListView;->mTouchFrame:Landroid/graphics/Rect;

    #@d
    .line 2955
    :cond_d
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@10
    move-result v1

    #@11
    .line 2956
    .local v1, count:I
    add-int/lit8 v3, v1, -0x1

    #@13
    .local v3, i:I
    :goto_13
    if-ltz v3, :cond_2f

    #@15
    .line 2957
    invoke-virtual {p0, v3}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    .line 2958
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@1c
    move-result v4

    #@1d
    if-nez v4, :cond_2c

    #@1f
    .line 2959
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@22
    .line 2960
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_2c

    #@28
    .line 2961
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2a
    add-int/2addr v4, v3

    #@2b
    .line 2965
    .end local v0           #child:Landroid/view/View;
    :goto_2b
    return v4

    #@2c
    .line 2956
    .restart local v0       #child:Landroid/view/View;
    :cond_2c
    add-int/lit8 v3, v3, -0x1

    #@2e
    goto :goto_13

    #@2f
    .line 2965
    .end local v0           #child:Landroid/view/View;
    :cond_2f
    const/4 v4, -0x1

    #@30
    goto :goto_2b
.end method

.method public pointToRowId(II)J
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 2978
    invoke-virtual {p0, p1, p2}, Landroid/widget/AbsListView;->pointToPosition(II)I

    #@3
    move-result v0

    #@4
    .line 2979
    .local v0, position:I
    if-ltz v0, :cond_d

    #@6
    .line 2980
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@8
    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@b
    move-result-wide v1

    #@c
    .line 2982
    :goto_c
    return-wide v1

    #@d
    :cond_d
    const-wide/high16 v1, -0x8000

    #@f
    goto :goto_c
.end method

.method positionSelector(ILandroid/view/View;)V
    .registers 10
    .parameter "position"
    .parameter "sel"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 2315
    if-eq p1, v6, :cond_5

    #@3
    .line 2316
    iput p1, p0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@5
    .line 2319
    :cond_5
    iget-object v1, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@7
    .line 2320
    .local v1, selectorRect:Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    #@a
    move-result v2

    #@b
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    #@e
    move-result v3

    #@f
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    #@12
    move-result v4

    #@13
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    #@16
    move-result v5

    #@17
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@1a
    .line 2321
    instance-of v2, p2, Landroid/widget/AbsListView$SelectionBoundsAdjuster;

    #@1c
    if-eqz v2, :cond_24

    #@1e
    move-object v2, p2

    #@1f
    .line 2322
    check-cast v2, Landroid/widget/AbsListView$SelectionBoundsAdjuster;

    #@21
    invoke-interface {v2, v1}, Landroid/widget/AbsListView$SelectionBoundsAdjuster;->adjustListItemSelectionBounds(Landroid/graphics/Rect;)V

    #@24
    .line 2324
    :cond_24
    iget v2, v1, Landroid/graphics/Rect;->left:I

    #@26
    iget v3, v1, Landroid/graphics/Rect;->top:I

    #@28
    iget v4, v1, Landroid/graphics/Rect;->right:I

    #@2a
    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    #@2c
    invoke-direct {p0, v2, v3, v4, v5}, Landroid/widget/AbsListView;->positionSelector(IIII)V

    #@2f
    .line 2327
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mIsChildViewEnabled:Z

    #@31
    .line 2328
    .local v0, isChildViewEnabled:Z
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    #@34
    move-result v2

    #@35
    if-eq v2, v0, :cond_45

    #@37
    .line 2329
    if-nez v0, :cond_46

    #@39
    const/4 v2, 0x1

    #@3a
    :goto_3a
    iput-boolean v2, p0, Landroid/widget/AbsListView;->mIsChildViewEnabled:Z

    #@3c
    .line 2330
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    #@3f
    move-result v2

    #@40
    if-eq v2, v6, :cond_45

    #@42
    .line 2331
    invoke-virtual {p0}, Landroid/widget/AbsListView;->refreshDrawableState()V

    #@45
    .line 2334
    :cond_45
    return-void

    #@46
    .line 2329
    :cond_46
    const/4 v2, 0x0

    #@47
    goto :goto_3a
.end method

.method public reclaimViews(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 6068
    .local p1, views:Ljava/util/List;,"Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 6069
    .local v1, childCount:I
    iget-object v5, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@6
    invoke-static {v5}, Landroid/widget/AbsListView$RecycleBin;->access$3500(Landroid/widget/AbsListView$RecycleBin;)Landroid/widget/AbsListView$RecyclerListener;

    #@9
    move-result-object v3

    #@a
    .line 6072
    .local v3, listener:Landroid/widget/AbsListView$RecyclerListener;
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v1, :cond_32

    #@d
    .line 6073
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 6074
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Landroid/widget/AbsListView$LayoutParams;

    #@17
    .line 6076
    .local v4, lp:Landroid/widget/AbsListView$LayoutParams;
    if-eqz v4, :cond_2f

    #@19
    iget-object v5, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@1b
    iget v6, v4, Landroid/widget/AbsListView$LayoutParams;->viewType:I

    #@1d
    invoke-virtual {v5, v6}, Landroid/widget/AbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_2f

    #@23
    .line 6077
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@26
    .line 6078
    const/4 v5, 0x0

    #@27
    invoke-virtual {v0, v5}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    #@2a
    .line 6079
    if-eqz v3, :cond_2f

    #@2c
    .line 6081
    invoke-interface {v3, v0}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    #@2f
    .line 6072
    :cond_2f
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_b

    #@32
    .line 6085
    .end local v0           #child:Landroid/view/View;
    .end local v4           #lp:Landroid/widget/AbsListView$LayoutParams;
    :cond_32
    iget-object v5, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@34
    invoke-virtual {v5, p1}, Landroid/widget/AbsListView$RecycleBin;->reclaimScrapViews(Ljava/util/List;)V

    #@37
    .line 6086
    invoke-virtual {p0}, Landroid/widget/AbsListView;->removeAllViewsInLayout()V

    #@3a
    .line 6087
    return-void
.end method

.method reconcileSelectedPosition()I
    .registers 3

    #@0
    .prologue
    .line 5239
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    .line 5240
    .local v0, position:I
    if-gez v0, :cond_6

    #@4
    .line 5241
    iget v0, p0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@6
    .line 5243
    :cond_6
    const/4 v1, 0x0

    #@7
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    #@a
    move-result v0

    #@b
    .line 5244
    iget v1, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@d
    add-int/lit8 v1, v1, -0x1

    #@f
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@12
    move-result v0

    #@13
    .line 5245
    return v0
.end method

.method reportScrollStateChange(I)V
    .registers 3
    .parameter "newState"

    #@0
    .prologue
    .line 4096
    iget v0, p0, Landroid/widget/AbsListView;->mLastScrollState:I

    #@2
    if-eq p1, v0, :cond_f

    #@4
    .line 4097
    iget-object v0, p0, Landroid/widget/AbsListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 4098
    iput p1, p0, Landroid/widget/AbsListView;->mLastScrollState:I

    #@a
    .line 4099
    iget-object v0, p0, Landroid/widget/AbsListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    #@c
    invoke-interface {v0, p0, p1}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    #@f
    .line 4102
    :cond_f
    return-void
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2
    .parameter "disallowIntercept"

    #@0
    .prologue
    .line 3946
    if-eqz p1, :cond_5

    #@2
    .line 3947
    invoke-direct {p0}, Landroid/widget/AbsListView;->recycleVelocityTracker()V

    #@5
    .line 3949
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->requestDisallowInterceptTouchEvent(Z)V

    #@8
    .line 3950
    return-void
.end method

.method public requestLayout()V
    .registers 2

    #@0
    .prologue
    .line 1847
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@2
    if-nez v0, :cond_b

    #@4
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@6
    if-nez v0, :cond_b

    #@8
    .line 1848
    invoke-super {p0}, Landroid/widget/AdapterView;->requestLayout()V

    #@b
    .line 1850
    :cond_b
    return-void
.end method

.method requestLayoutIfNecessary()V
    .registers 2

    #@0
    .prologue
    .line 1543
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_f

    #@6
    .line 1544
    invoke-virtual {p0}, Landroid/widget/AbsListView;->resetList()V

    #@9
    .line 1545
    invoke-virtual {p0}, Landroid/widget/AbsListView;->requestLayout()V

    #@c
    .line 1546
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidate()V

    #@f
    .line 1548
    :cond_f
    return-void
.end method

.method resetList()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, -0x1

    #@3
    .line 1856
    invoke-virtual {p0}, Landroid/widget/AbsListView;->removeAllViewsInLayout()V

    #@6
    .line 1857
    iput v3, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@8
    .line 1858
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@a
    .line 1859
    iput-object v0, p0, Landroid/widget/AbsListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    #@c
    .line 1860
    iput-boolean v3, p0, Landroid/widget/AdapterView;->mNeedSync:Z

    #@e
    .line 1861
    iput-object v0, p0, Landroid/widget/AbsListView;->mPendingSync:Landroid/widget/AbsListView$SavedState;

    #@10
    .line 1862
    iput v2, p0, Landroid/widget/AdapterView;->mOldSelectedPosition:I

    #@12
    .line 1863
    const-wide/high16 v0, -0x8000

    #@14
    iput-wide v0, p0, Landroid/widget/AdapterView;->mOldSelectedRowId:J

    #@16
    .line 1864
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setSelectedPositionInt(I)V

    #@19
    .line 1865
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setNextSelectedPositionInt(I)V

    #@1c
    .line 1866
    iput v3, p0, Landroid/widget/AbsListView;->mSelectedTop:I

    #@1e
    .line 1867
    iput v2, p0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@20
    .line 1868
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    #@25
    .line 1869
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invalidate()V

    #@28
    .line 1870
    return-void
.end method

.method resurrectSelection()Z
    .registers 20

    #@0
    .prologue
    .line 5307
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v3

    #@4
    .line 5309
    .local v3, childCount:I
    if-gtz v3, :cond_9

    #@6
    .line 5310
    const/16 v17, 0x0

    #@8
    .line 5409
    :goto_8
    return v17

    #@9
    .line 5313
    :cond_9
    const/4 v13, 0x0

    #@a
    .line 5315
    .local v13, selectedTop:I
    move-object/from16 v0, p0

    #@c
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@e
    move-object/from16 v17, v0

    #@10
    move-object/from16 v0, v17

    #@12
    iget v5, v0, Landroid/graphics/Rect;->top:I

    #@14
    .line 5316
    .local v5, childrenTop:I
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@18
    move/from16 v17, v0

    #@1a
    move-object/from16 v0, p0

    #@1c
    iget v0, v0, Landroid/view/View;->mTop:I

    #@1e
    move/from16 v18, v0

    #@20
    sub-int v17, v17, v18

    #@22
    move-object/from16 v0, p0

    #@24
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@26
    move-object/from16 v18, v0

    #@28
    move-object/from16 v0, v18

    #@2a
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@2c
    move/from16 v18, v0

    #@2e
    sub-int v4, v17, v18

    #@30
    .line 5317
    .local v4, childrenBottom:I
    move-object/from16 v0, p0

    #@32
    iget v7, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@34
    .line 5318
    .local v7, firstPosition:I
    move-object/from16 v0, p0

    #@36
    iget v14, v0, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@38
    .line 5319
    .local v14, toPosition:I
    const/4 v6, 0x1

    #@39
    .line 5321
    .local v6, down:Z
    if-lt v14, v7, :cond_da

    #@3b
    add-int v17, v7, v3

    #@3d
    move/from16 v0, v17

    #@3f
    if-ge v14, v0, :cond_da

    #@41
    .line 5322
    move v12, v14

    #@42
    .line 5324
    .local v12, selectedPos:I
    move-object/from16 v0, p0

    #@44
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@46
    move/from16 v17, v0

    #@48
    sub-int v17, v12, v17

    #@4a
    move-object/from16 v0, p0

    #@4c
    move/from16 v1, v17

    #@4e
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@51
    move-result-object v10

    #@52
    .line 5326
    .local v10, selected:Landroid/view/View;
    if-eqz v10, :cond_64

    #@54
    .line 5327
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@57
    move-result v13

    #@58
    .line 5328
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    #@5b
    move-result v11

    #@5c
    .line 5331
    .local v11, selectedBottom:I
    if-ge v13, v5, :cond_cb

    #@5e
    .line 5332
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getVerticalFadingEdgeLength()I

    #@61
    move-result v17

    #@62
    add-int v13, v5, v17

    #@64
    .line 5390
    .end local v10           #selected:Landroid/view/View;
    .end local v11           #selectedBottom:I
    :cond_64
    :goto_64
    const/16 v17, -0x1

    #@66
    move/from16 v0, v17

    #@68
    move-object/from16 v1, p0

    #@6a
    iput v0, v1, Landroid/widget/AbsListView;->mResurrectToPosition:I

    #@6c
    .line 5391
    move-object/from16 v0, p0

    #@6e
    iget-object v0, v0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@70
    move-object/from16 v17, v0

    #@72
    move-object/from16 v0, p0

    #@74
    move-object/from16 v1, v17

    #@76
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@79
    .line 5392
    move-object/from16 v0, p0

    #@7b
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@7d
    move-object/from16 v17, v0

    #@7f
    if-eqz v17, :cond_8a

    #@81
    .line 5393
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@85
    move-object/from16 v17, v0

    #@87
    invoke-virtual/range {v17 .. v17}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@8a
    .line 5395
    :cond_8a
    const/16 v17, -0x1

    #@8c
    move/from16 v0, v17

    #@8e
    move-object/from16 v1, p0

    #@90
    iput v0, v1, Landroid/widget/AbsListView;->mTouchMode:I

    #@92
    .line 5396
    invoke-direct/range {p0 .. p0}, Landroid/widget/AbsListView;->clearScrollingCache()V

    #@95
    .line 5397
    move-object/from16 v0, p0

    #@97
    iput v13, v0, Landroid/widget/AdapterView;->mSpecificTop:I

    #@99
    .line 5398
    move-object/from16 v0, p0

    #@9b
    invoke-virtual {v0, v12, v6}, Landroid/widget/AbsListView;->lookForSelectablePosition(IZ)I

    #@9e
    move-result v12

    #@9f
    .line 5399
    if-lt v12, v7, :cond_13b

    #@a1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    #@a4
    move-result v17

    #@a5
    move/from16 v0, v17

    #@a7
    if-gt v12, v0, :cond_13b

    #@a9
    .line 5400
    const/16 v17, 0x4

    #@ab
    move/from16 v0, v17

    #@ad
    move-object/from16 v1, p0

    #@af
    iput v0, v1, Landroid/widget/AbsListView;->mLayoutMode:I

    #@b1
    .line 5401
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@b4
    .line 5402
    move-object/from16 v0, p0

    #@b6
    invoke-virtual {v0, v12}, Landroid/widget/AbsListView;->setSelectionInt(I)V

    #@b9
    .line 5403
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->invokeOnItemScrollListener()V

    #@bc
    .line 5407
    :goto_bc
    const/16 v17, 0x0

    #@be
    move-object/from16 v0, p0

    #@c0
    move/from16 v1, v17

    #@c2
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@c5
    .line 5409
    if-ltz v12, :cond_13d

    #@c7
    const/16 v17, 0x1

    #@c9
    goto/16 :goto_8

    #@cb
    .line 5333
    .restart local v10       #selected:Landroid/view/View;
    .restart local v11       #selectedBottom:I
    :cond_cb
    if-le v11, v4, :cond_64

    #@cd
    .line 5334
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    #@d0
    move-result v17

    #@d1
    sub-int v17, v4, v17

    #@d3
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getVerticalFadingEdgeLength()I

    #@d6
    move-result v18

    #@d7
    sub-int v13, v17, v18

    #@d9
    goto :goto_64

    #@da
    .line 5340
    .end local v10           #selected:Landroid/view/View;
    .end local v11           #selectedBottom:I
    .end local v12           #selectedPos:I
    :cond_da
    if-ge v14, v7, :cond_101

    #@dc
    .line 5342
    move v12, v7

    #@dd
    .line 5343
    .restart local v12       #selectedPos:I
    const/4 v8, 0x0

    #@de
    .local v8, i:I
    :goto_de
    if-ge v8, v3, :cond_64

    #@e0
    .line 5344
    move-object/from16 v0, p0

    #@e2
    invoke-virtual {v0, v8}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@e5
    move-result-object v16

    #@e6
    .line 5345
    .local v16, v:Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getTop()I

    #@e9
    move-result v15

    #@ea
    .line 5347
    .local v15, top:I
    if-nez v8, :cond_f7

    #@ec
    .line 5349
    move v13, v15

    #@ed
    .line 5351
    if-gtz v7, :cond_f1

    #@ef
    if-ge v15, v5, :cond_f7

    #@f1
    .line 5354
    :cond_f1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getVerticalFadingEdgeLength()I

    #@f4
    move-result v17

    #@f5
    add-int v5, v5, v17

    #@f7
    .line 5357
    :cond_f7
    if-lt v15, v5, :cond_fe

    #@f9
    .line 5359
    add-int v12, v7, v8

    #@fb
    .line 5360
    move v13, v15

    #@fc
    .line 5361
    goto/16 :goto_64

    #@fe
    .line 5343
    :cond_fe
    add-int/lit8 v8, v8, 0x1

    #@100
    goto :goto_de

    #@101
    .line 5365
    .end local v8           #i:I
    .end local v12           #selectedPos:I
    .end local v15           #top:I
    .end local v16           #v:Landroid/view/View;
    :cond_101
    move-object/from16 v0, p0

    #@103
    iget v9, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@105
    .line 5366
    .local v9, itemCount:I
    const/4 v6, 0x0

    #@106
    .line 5367
    add-int v17, v7, v3

    #@108
    add-int/lit8 v12, v17, -0x1

    #@10a
    .line 5369
    .restart local v12       #selectedPos:I
    add-int/lit8 v8, v3, -0x1

    #@10c
    .restart local v8       #i:I
    :goto_10c
    if-ltz v8, :cond_64

    #@10e
    .line 5370
    move-object/from16 v0, p0

    #@110
    invoke-virtual {v0, v8}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@113
    move-result-object v16

    #@114
    .line 5371
    .restart local v16       #v:Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getTop()I

    #@117
    move-result v15

    #@118
    .line 5372
    .restart local v15       #top:I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getBottom()I

    #@11b
    move-result v2

    #@11c
    .line 5374
    .local v2, bottom:I
    add-int/lit8 v17, v3, -0x1

    #@11e
    move/from16 v0, v17

    #@120
    if-ne v8, v0, :cond_131

    #@122
    .line 5375
    move v13, v15

    #@123
    .line 5376
    add-int v17, v7, v3

    #@125
    move/from16 v0, v17

    #@127
    if-lt v0, v9, :cond_12b

    #@129
    if-le v2, v4, :cond_131

    #@12b
    .line 5377
    :cond_12b
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getVerticalFadingEdgeLength()I

    #@12e
    move-result v17

    #@12f
    sub-int v4, v4, v17

    #@131
    .line 5381
    :cond_131
    if-gt v2, v4, :cond_138

    #@133
    .line 5382
    add-int v12, v7, v8

    #@135
    .line 5383
    move v13, v15

    #@136
    .line 5384
    goto/16 :goto_64

    #@138
    .line 5369
    :cond_138
    add-int/lit8 v8, v8, -0x1

    #@13a
    goto :goto_10c

    #@13b
    .line 5405
    .end local v2           #bottom:I
    .end local v8           #i:I
    .end local v9           #itemCount:I
    .end local v15           #top:I
    .end local v16           #v:Landroid/view/View;
    :cond_13b
    const/4 v12, -0x1

    #@13c
    goto :goto_bc

    #@13d
    .line 5409
    :cond_13d
    const/16 v17, 0x0

    #@13f
    goto/16 :goto_8
.end method

.method resurrectSelectionIfNeeded()Z
    .registers 2

    #@0
    .prologue
    .line 5287
    iget v0, p0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@2
    if-gez v0, :cond_f

    #@4
    invoke-virtual {p0}, Landroid/widget/AbsListView;->resurrectSelection()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 5288
    invoke-virtual {p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@d
    .line 5289
    const/4 v0, 0x1

    #@e
    .line 5291
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public sendAccessibilityEvent(I)V
    .registers 5
    .parameter "eventType"

    #@0
    .prologue
    .line 1379
    const/16 v2, 0x1000

    #@2
    if-ne p1, v2, :cond_19

    #@4
    .line 1380
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    #@7
    move-result v0

    #@8
    .line 1381
    .local v0, firstVisiblePosition:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    #@b
    move-result v1

    #@c
    .line 1382
    .local v1, lastVisiblePosition:I
    iget v2, p0, Landroid/widget/AbsListView;->mLastAccessibilityScrollEventFromIndex:I

    #@e
    if-ne v2, v0, :cond_15

    #@10
    iget v2, p0, Landroid/widget/AbsListView;->mLastAccessibilityScrollEventToIndex:I

    #@12
    if-ne v2, v1, :cond_15

    #@14
    .line 1391
    .end local v0           #firstVisiblePosition:I
    .end local v1           #lastVisiblePosition:I
    :goto_14
    return-void

    #@15
    .line 1386
    .restart local v0       #firstVisiblePosition:I
    .restart local v1       #lastVisiblePosition:I
    :cond_15
    iput v0, p0, Landroid/widget/AbsListView;->mLastAccessibilityScrollEventFromIndex:I

    #@17
    .line 1387
    iput v1, p0, Landroid/widget/AbsListView;->mLastAccessibilityScrollEventToIndex:I

    #@19
    .line 1390
    .end local v0           #firstVisiblePosition:I
    .end local v1           #lastVisiblePosition:I
    :cond_19
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->sendAccessibilityEvent(I)V

    #@1c
    goto :goto_14
.end method

.method sendToTextFilter(IILandroid/view/KeyEvent;)Z
    .registers 12
    .parameter "keyCode"
    .parameter "count"
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 5723
    invoke-direct {p0}, Landroid/widget/AbsListView;->acceptFilter()Z

    #@5
    move-result v6

    #@6
    if-nez v6, :cond_a

    #@8
    move v2, v5

    #@9
    .line 5787
    :cond_9
    :goto_9
    return v2

    #@a
    .line 5727
    :cond_a
    const/4 v2, 0x0

    #@b
    .line 5728
    .local v2, handled:Z
    const/4 v3, 0x1

    #@c
    .line 5729
    .local v3, okToSend:Z
    sparse-switch p1, :sswitch_data_86

    #@f
    .line 5764
    :goto_f
    if-eqz v3, :cond_9

    #@11
    .line 5765
    invoke-direct {p0, v7}, Landroid/widget/AbsListView;->createTextFilter(Z)V

    #@14
    .line 5767
    move-object v1, p3

    #@15
    .line 5768
    .local v1, forwardEvent:Landroid/view/KeyEvent;
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@18
    move-result v6

    #@19
    if-lez v6, :cond_23

    #@1b
    .line 5769
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    #@1e
    move-result-wide v6

    #@1f
    invoke-static {p3, v6, v7, v5}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JI)Landroid/view/KeyEvent;

    #@22
    move-result-object v1

    #@23
    .line 5772
    :cond_23
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@26
    move-result v0

    #@27
    .line 5773
    .local v0, action:I
    packed-switch v0, :pswitch_data_a8

    #@2a
    goto :goto_9

    #@2b
    .line 5775
    :pswitch_2b
    iget-object v5, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@2d
    invoke-virtual {v5, p1, v1}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@30
    move-result v2

    #@31
    .line 5776
    goto :goto_9

    #@32
    .line 5736
    .end local v0           #action:I
    .end local v1           #forwardEvent:Landroid/view/KeyEvent;
    :sswitch_32
    const/4 v3, 0x0

    #@33
    .line 5737
    goto :goto_f

    #@34
    .line 5739
    :sswitch_34
    iget-boolean v6, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@36
    if-eqz v6, :cond_5a

    #@38
    iget-object v6, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@3a
    if-eqz v6, :cond_5a

    #@3c
    iget-object v6, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@3e
    invoke-virtual {v6}, Landroid/widget/PopupWindow;->isShowing()Z

    #@41
    move-result v6

    #@42
    if-eqz v6, :cond_5a

    #@44
    .line 5740
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@47
    move-result v6

    #@48
    if-nez v6, :cond_5c

    #@4a
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@4d
    move-result v6

    #@4e
    if-nez v6, :cond_5c

    #@50
    .line 5742
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@53
    move-result-object v4

    #@54
    .line 5743
    .local v4, state:Landroid/view/KeyEvent$DispatcherState;
    if-eqz v4, :cond_59

    #@56
    .line 5744
    invoke-virtual {v4, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@59
    .line 5746
    :cond_59
    const/4 v2, 0x1

    #@5a
    .line 5756
    .end local v4           #state:Landroid/view/KeyEvent$DispatcherState;
    :cond_5a
    :goto_5a
    const/4 v3, 0x0

    #@5b
    .line 5757
    goto :goto_f

    #@5c
    .line 5747
    :cond_5c
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@5f
    move-result v6

    #@60
    if-ne v6, v7, :cond_5a

    #@62
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isTracking()Z

    #@65
    move-result v6

    #@66
    if-eqz v6, :cond_5a

    #@68
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    #@6b
    move-result v6

    #@6c
    if-nez v6, :cond_5a

    #@6e
    .line 5749
    const/4 v2, 0x1

    #@6f
    .line 5751
    invoke-direct {p0}, Landroid/widget/AbsListView;->dismissPopup()V

    #@72
    .line 5752
    iput-boolean v5, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@74
    goto :goto_5a

    #@75
    .line 5760
    :sswitch_75
    iget-boolean v3, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@77
    goto :goto_f

    #@78
    .line 5779
    .restart local v0       #action:I
    .restart local v1       #forwardEvent:Landroid/view/KeyEvent;
    :pswitch_78
    iget-object v5, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@7a
    invoke-virtual {v5, p1, v1}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@7d
    move-result v2

    #@7e
    .line 5780
    goto :goto_9

    #@7f
    .line 5783
    :pswitch_7f
    iget-object v5, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@81
    invoke-virtual {v5, p1, p2, p3}, Landroid/widget/EditText;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    #@84
    move-result v2

    #@85
    goto :goto_9

    #@86
    .line 5729
    :sswitch_data_86
    .sparse-switch
        0x4 -> :sswitch_34
        0x13 -> :sswitch_32
        0x14 -> :sswitch_32
        0x15 -> :sswitch_32
        0x16 -> :sswitch_32
        0x17 -> :sswitch_32
        0x3e -> :sswitch_75
        0x42 -> :sswitch_32
    .end sparse-switch

    #@a8
    .line 5773
    :pswitch_data_a8
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_78
        :pswitch_7f
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 93
    check-cast p1, Landroid/widget/ListAdapter;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@5
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 3
    .parameter "adapter"

    #@0
    .prologue
    .line 874
    if-eqz p1, :cond_1d

    #@2
    .line 875
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@4
    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@7
    move-result v0

    #@8
    iput-boolean v0, p0, Landroid/widget/AbsListView;->mAdapterHasStableIds:Z

    #@a
    .line 876
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@c
    if-eqz v0, :cond_1d

    #@e
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mAdapterHasStableIds:Z

    #@10
    if-eqz v0, :cond_1d

    #@12
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@14
    if-nez v0, :cond_1d

    #@16
    .line 878
    new-instance v0, Landroid/util/LongSparseArray;

    #@18
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@1d
    .line 882
    :cond_1d
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@1f
    if-eqz v0, :cond_26

    #@21
    .line 883
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@23
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    #@26
    .line 886
    :cond_26
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@28
    if-eqz v0, :cond_2f

    #@2a
    .line 887
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@2c
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    #@2f
    .line 889
    :cond_2f
    return-void
.end method

.method public setCacheColorHint(I)V
    .registers 5
    .parameter "color"

    #@0
    .prologue
    .line 6039
    iget v2, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@2
    if-eq p1, v2, :cond_1c

    #@4
    .line 6040
    iput p1, p0, Landroid/widget/AbsListView;->mCacheColorHint:I

    #@6
    .line 6041
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@9
    move-result v0

    #@a
    .line 6042
    .local v0, count:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_17

    #@d
    .line 6043
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    #@14
    .line 6042
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_b

    #@17
    .line 6045
    :cond_17
    iget-object v2, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@19
    invoke-virtual {v2, p1}, Landroid/widget/AbsListView$RecycleBin;->setCacheColorHint(I)V

    #@1c
    .line 6047
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_1c
    return-void
.end method

.method public setChoiceMode(I)V
    .registers 4
    .parameter "choiceMode"

    #@0
    .prologue
    .line 1176
    iput p1, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@2
    .line 1177
    iget-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 1178
    iget-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@8
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    #@b
    .line 1179
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@e
    .line 1181
    :cond_e
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@10
    if-eqz v0, :cond_40

    #@12
    .line 1182
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@14
    if-nez v0, :cond_1d

    #@16
    .line 1183
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@18
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@1d
    .line 1185
    :cond_1d
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@1f
    if-nez v0, :cond_34

    #@21
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@23
    if-eqz v0, :cond_34

    #@25
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@27
    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_34

    #@2d
    .line 1186
    new-instance v0, Landroid/util/LongSparseArray;

    #@2f
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@32
    iput-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@34
    .line 1189
    :cond_34
    iget v0, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@36
    const/4 v1, 0x3

    #@37
    if-ne v0, v1, :cond_40

    #@39
    .line 1190
    invoke-virtual {p0}, Landroid/widget/AbsListView;->clearChoices()V

    #@3c
    .line 1191
    const/4 v0, 0x1

    #@3d
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->setLongClickable(Z)V

    #@40
    .line 1194
    :cond_40
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .registers 2
    .parameter "onTop"

    #@0
    .prologue
    .line 2453
    iput-boolean p1, p0, Landroid/widget/AbsListView;->mDrawSelectorOnTop:Z

    #@2
    .line 2454
    return-void
.end method

.method public setFastScrollAlwaysVisible(Z)V
    .registers 3
    .parameter "alwaysShow"

    #@0
    .prologue
    .line 1259
    if-eqz p1, :cond_a

    #@2
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mFastScrollEnabled:Z

    #@4
    if-nez v0, :cond_a

    #@6
    .line 1260
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    #@a
    .line 1263
    :cond_a
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 1264
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@10
    invoke-virtual {v0, p1}, Landroid/widget/FastScroller;->setAlwaysShow(Z)V

    #@13
    .line 1267
    :cond_13
    invoke-virtual {p0}, Landroid/widget/AbsListView;->computeOpaqueFlags()V

    #@16
    .line 1268
    invoke-virtual {p0}, Landroid/widget/AbsListView;->recomputePadding()V

    #@19
    .line 1269
    return-void
.end method

.method public setFastScrollEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 1234
    iput-boolean p1, p0, Landroid/widget/AbsListView;->mFastScrollEnabled:Z

    #@2
    .line 1235
    if-eqz p1, :cond_14

    #@4
    .line 1236
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@6
    if-nez v0, :cond_13

    #@8
    .line 1237
    new-instance v0, Landroid/widget/FastScroller;

    #@a
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@d
    move-result-object v1

    #@e
    invoke-direct {v0, v1, p0}, Landroid/widget/FastScroller;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;)V

    #@11
    iput-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@13
    .line 1245
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1240
    :cond_14
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@16
    if-eqz v0, :cond_13

    #@18
    .line 1241
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@1a
    invoke-virtual {v0}, Landroid/widget/FastScroller;->stop()V

    #@1d
    .line 1242
    const/4 v0, 0x0

    #@1e
    iput-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@20
    goto :goto_13
.end method

.method public setFilterText(Ljava/lang/String;)V
    .registers 5
    .parameter "filterText"

    #@0
    .prologue
    .line 1799
    iget-boolean v1, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@2
    if-eqz v1, :cond_39

    #@4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_39

    #@a
    .line 1800
    const/4 v1, 0x0

    #@b
    invoke-direct {p0, v1}, Landroid/widget/AbsListView;->createTextFilter(Z)V

    #@e
    .line 1803
    iget-object v1, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@10
    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@13
    .line 1804
    iget-object v1, p0, Landroid/widget/AbsListView;->mTextFilter:Landroid/widget/EditText;

    #@15
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    #@1c
    .line 1805
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@1e
    instance-of v1, v1, Landroid/widget/Filterable;

    #@20
    if-eqz v1, :cond_39

    #@22
    .line 1807
    iget-object v1, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@24
    if-nez v1, :cond_31

    #@26
    .line 1808
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@28
    check-cast v1, Landroid/widget/Filterable;

    #@2a
    invoke-interface {v1}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    #@2d
    move-result-object v0

    #@2e
    .line 1809
    .local v0, f:Landroid/widget/Filter;
    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    #@31
    .line 1813
    .end local v0           #f:Landroid/widget/Filter;
    :cond_31
    const/4 v1, 0x1

    #@32
    iput-boolean v1, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@34
    .line 1814
    iget-object v1, p0, Landroid/widget/AbsListView;->mDataSetObserver:Landroid/widget/AbsListView$AdapterDataSetObserver;

    #@36
    invoke-virtual {v1}, Landroid/widget/AbsListView$AdapterDataSetObserver;->clearSavedState()V

    #@39
    .line 1817
    :cond_39
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 8
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2034
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->setFrame(IIII)Z

    #@3
    move-result v0

    #@4
    .line 2036
    .local v0, changed:Z
    if-eqz v0, :cond_22

    #@6
    .line 2040
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getWindowVisibility()I

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_23

    #@c
    const/4 v1, 0x1

    #@d
    .line 2041
    .local v1, visible:Z
    :goto_d
    iget-boolean v2, p0, Landroid/widget/AbsListView;->mFiltered:Z

    #@f
    if-eqz v2, :cond_22

    #@11
    if-eqz v1, :cond_22

    #@13
    iget-object v2, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@15
    if-eqz v2, :cond_22

    #@17
    iget-object v2, p0, Landroid/widget/AbsListView;->mPopup:Landroid/widget/PopupWindow;

    #@19
    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_22

    #@1f
    .line 2042
    invoke-direct {p0}, Landroid/widget/AbsListView;->positionPopup()V

    #@22
    .line 2046
    .end local v1           #visible:Z
    :cond_22
    return v0

    #@23
    .line 2040
    :cond_23
    const/4 v1, 0x0

    #@24
    goto :goto_d
.end method

.method public setFriction(F)V
    .registers 3
    .parameter "friction"

    #@0
    .prologue
    .line 4840
    iget-object v0, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 4841
    new-instance v0, Landroid/widget/AbsListView$FlingRunnable;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$FlingRunnable;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v0, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@b
    .line 4843
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@d
    invoke-static {v0}, Landroid/widget/AbsListView$FlingRunnable;->access$700(Landroid/widget/AbsListView$FlingRunnable;)Landroid/widget/OverScroller;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller;->setFriction(F)V

    #@14
    .line 4844
    return-void
.end method

.method public setItemChecked(IZ)V
    .registers 12
    .parameter "position"
    .parameter "value"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 1006
    iget v1, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@5
    if-nez v1, :cond_8

    #@7
    .line 1074
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1011
    :cond_8
    if-eqz p2, :cond_2e

    #@a
    iget v1, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@c
    if-ne v1, v5, :cond_2e

    #@e
    iget-object v1, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@10
    if-nez v1, :cond_2e

    #@12
    .line 1012
    iget-object v1, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@14
    if-eqz v1, :cond_1e

    #@16
    iget-object v1, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@18
    invoke-virtual {v1}, Landroid/widget/AbsListView$MultiChoiceModeWrapper;->hasWrappedCallback()Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_26

    #@1e
    .line 1014
    :cond_1e
    new-instance v0, Ljava/lang/IllegalStateException;

    #@20
    const-string v1, "AbsListView: attempted to start selection mode for CHOICE_MODE_MULTIPLE_MODAL but no choice mode callback was supplied. Call setMultiChoiceModeListener to set a callback."

    #@22
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 1018
    :cond_26
    iget-object v1, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@28
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@2b
    move-result-object v1

    #@2c
    iput-object v1, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@2e
    .line 1021
    :cond_2e
    iget v1, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@30
    const/4 v2, 0x2

    #@31
    if-eq v1, v2, :cond_37

    #@33
    iget v1, p0, Landroid/widget/AbsListView;->mChoiceMode:I

    #@35
    if-ne v1, v5, :cond_a5

    #@37
    .line 1022
    :cond_37
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@39
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@3c
    move-result v6

    #@3d
    .line 1023
    .local v6, oldValue:Z
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@3f
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@42
    .line 1024
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@44
    if-eqz v0, :cond_5f

    #@46
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@48
    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_5f

    #@4e
    .line 1025
    if-eqz p2, :cond_92

    #@50
    .line 1026
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@52
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@54
    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@57
    move-result-wide v1

    #@58
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v0, v1, v2, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@5f
    .line 1031
    :cond_5f
    :goto_5f
    if-eq v6, p2, :cond_69

    #@61
    .line 1032
    if-eqz p2, :cond_9e

    #@63
    .line 1033
    iget v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@65
    add-int/lit8 v0, v0, 0x1

    #@67
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@69
    .line 1039
    :cond_69
    :goto_69
    iget-object v0, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@6b
    if-eqz v0, :cond_80

    #@6d
    iget-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@6f
    if-eqz v0, :cond_80

    #@71
    .line 1041
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@73
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@76
    move-result-wide v3

    #@77
    .line 1042
    .local v3, id:J
    iget-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@79
    iget-object v1, p0, Landroid/widget/AbsListView;->mChoiceActionMode:Landroid/view/ActionMode;

    #@7b
    move v2, p1

    #@7c
    move v5, p2

    #@7d
    invoke-virtual/range {v0 .. v5}, Landroid/widget/AbsListView$MultiChoiceModeWrapper;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    #@80
    .line 1069
    .end local v3           #id:J
    .end local v6           #oldValue:Z
    :cond_80
    :goto_80
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mInLayout:Z

    #@82
    if-nez v0, :cond_7

    #@84
    iget-boolean v0, p0, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@86
    if-nez v0, :cond_7

    #@88
    .line 1070
    iput-boolean v8, p0, Landroid/widget/AdapterView;->mDataChanged:Z

    #@8a
    .line 1071
    invoke-virtual {p0}, Landroid/widget/AbsListView;->rememberSyncState()V

    #@8d
    .line 1072
    invoke-virtual {p0}, Landroid/widget/AbsListView;->requestLayout()V

    #@90
    goto/16 :goto_7

    #@92
    .line 1028
    .restart local v6       #oldValue:Z
    :cond_92
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@94
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@96
    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@99
    move-result-wide v1

    #@9a
    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->delete(J)V

    #@9d
    goto :goto_5f

    #@9e
    .line 1035
    :cond_9e
    iget v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@a0
    add-int/lit8 v0, v0, -0x1

    #@a2
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@a4
    goto :goto_69

    #@a5
    .line 1046
    .end local v6           #oldValue:Z
    :cond_a5
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@a7
    if-eqz v1, :cond_e1

    #@a9
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@ab
    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    #@ae
    move-result v1

    #@af
    if-eqz v1, :cond_e1

    #@b1
    move v7, v8

    #@b2
    .line 1049
    .local v7, updateIds:Z
    :goto_b2
    if-nez p2, :cond_ba

    #@b4
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    #@b7
    move-result v1

    #@b8
    if-eqz v1, :cond_c6

    #@ba
    .line 1050
    :cond_ba
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@bc
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    #@bf
    .line 1051
    if-eqz v7, :cond_c6

    #@c1
    .line 1052
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@c3
    invoke-virtual {v1}, Landroid/util/LongSparseArray;->clear()V

    #@c6
    .line 1057
    :cond_c6
    if-eqz p2, :cond_e3

    #@c8
    .line 1058
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@ca
    invoke-virtual {v0, p1, v8}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@cd
    .line 1059
    if-eqz v7, :cond_de

    #@cf
    .line 1060
    iget-object v0, p0, Landroid/widget/AbsListView;->mCheckedIdStates:Landroid/util/LongSparseArray;

    #@d1
    iget-object v1, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@d3
    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@d6
    move-result-wide v1

    #@d7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@da
    move-result-object v5

    #@db
    invoke-virtual {v0, v1, v2, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@de
    .line 1062
    :cond_de
    iput v8, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@e0
    goto :goto_80

    #@e1
    .end local v7           #updateIds:Z
    :cond_e1
    move v7, v0

    #@e2
    .line 1046
    goto :goto_b2

    #@e3
    .line 1063
    .restart local v7       #updateIds:Z
    :cond_e3
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@e5
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    #@e8
    move-result v1

    #@e9
    if-eqz v1, :cond_f3

    #@eb
    iget-object v1, p0, Landroid/widget/AbsListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    #@ed
    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@f0
    move-result v1

    #@f1
    if-nez v1, :cond_80

    #@f3
    .line 1064
    :cond_f3
    iput v0, p0, Landroid/widget/AbsListView;->mCheckedItemCount:I

    #@f5
    goto :goto_80
.end method

.method public setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 1206
    iget-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1207
    new-instance v0, Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$MultiChoiceModeWrapper;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@b
    .line 1209
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mMultiChoiceModeCallback:Landroid/widget/AbsListView$MultiChoiceModeWrapper;

    #@d
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView$MultiChoiceModeWrapper;->setWrapped(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    #@10
    .line 1210
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 1357
    iput-object p1, p0, Landroid/widget/AbsListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    #@2
    .line 1358
    invoke-virtual {p0}, Landroid/widget/AbsListView;->invokeOnItemScrollListener()V

    #@5
    .line 1359
    return-void
.end method

.method public setOverScrollEffectPadding(II)V
    .registers 3
    .parameter "leftPadding"
    .parameter "rightPadding"

    #@0
    .prologue
    .line 3919
    iput p1, p0, Landroid/widget/AbsListView;->mGlowPaddingLeft:I

    #@2
    .line 3920
    iput p2, p0, Landroid/widget/AbsListView;->mGlowPaddingRight:I

    #@4
    .line 3921
    return-void
.end method

.method public setOverScrollMode(I)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 856
    const/4 v1, 0x2

    #@2
    if-eq p1, v1, :cond_1e

    #@4
    .line 857
    iget-object v1, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@6
    if-nez v1, :cond_1a

    #@8
    .line 858
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v0

    #@c
    .line 859
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/widget/EdgeEffect;

    #@e
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@11
    iput-object v1, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@13
    .line 860
    new-instance v1, Landroid/widget/EdgeEffect;

    #@15
    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    #@18
    iput-object v1, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@1a
    .line 866
    .end local v0           #context:Landroid/content/Context;
    :cond_1a
    :goto_1a
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setOverScrollMode(I)V

    #@1d
    .line 867
    return-void

    #@1e
    .line 863
    :cond_1e
    iput-object v2, p0, Landroid/widget/AbsListView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    #@20
    .line 864
    iput-object v2, p0, Landroid/widget/AbsListView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    #@22
    goto :goto_1a
.end method

.method public setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 6194
    iget-object v0, p0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@2
    invoke-static {v0, p1}, Landroid/widget/AbsListView$RecycleBin;->access$3502(Landroid/widget/AbsListView$RecycleBin;Landroid/widget/AbsListView$RecyclerListener;)Landroid/widget/AbsListView$RecyclerListener;

    #@5
    .line 6195
    return-void
.end method

.method public setRemoteViewsAdapter(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 6104
    iget-object v2, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2
    if-eqz v2, :cond_1b

    #@4
    .line 6105
    new-instance v0, Landroid/content/Intent$FilterComparison;

    #@6
    invoke-direct {v0, p1}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@9
    .line 6106
    .local v0, fcNew:Landroid/content/Intent$FilterComparison;
    new-instance v1, Landroid/content/Intent$FilterComparison;

    #@b
    iget-object v2, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@d
    invoke-virtual {v2}, Landroid/widget/RemoteViewsAdapter;->getRemoteViewsServiceIntent()Landroid/content/Intent;

    #@10
    move-result-object v2

    #@11
    invoke-direct {v1, v2}, Landroid/content/Intent$FilterComparison;-><init>(Landroid/content/Intent;)V

    #@14
    .line 6108
    .local v1, fcOld:Landroid/content/Intent$FilterComparison;
    invoke-virtual {v0, v1}, Landroid/content/Intent$FilterComparison;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_1b

    #@1a
    .line 6118
    .end local v0           #fcNew:Landroid/content/Intent$FilterComparison;
    .end local v1           #fcOld:Landroid/content/Intent$FilterComparison;
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 6112
    :cond_1b
    const/4 v2, 0x0

    #@1c
    iput-boolean v2, p0, Landroid/widget/AbsListView;->mDeferNotifyDataSetChanged:Z

    #@1e
    .line 6114
    new-instance v2, Landroid/widget/RemoteViewsAdapter;

    #@20
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    #@23
    move-result-object v3

    #@24
    invoke-direct {v2, v3, p1, p0}, Landroid/widget/RemoteViewsAdapter;-><init>(Landroid/content/Context;Landroid/content/Intent;Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;)V

    #@27
    iput-object v2, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@29
    .line 6115
    iget-object v2, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2b
    invoke-virtual {v2}, Landroid/widget/RemoteViewsAdapter;->isDataReady()Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_1a

    #@31
    .line 6116
    iget-object v2, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@33
    invoke-virtual {p0, v2}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@36
    goto :goto_1a
.end method

.method public setRemoteViewsOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 6130
    iget-object v0, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 6131
    iget-object v0, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViewsAdapter;->setRemoteViewsOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V

    #@9
    .line 6133
    :cond_9
    return-void
.end method

.method public setScrollIndicators(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter "up"
    .parameter "down"

    #@0
    .prologue
    .line 2536
    iput-object p1, p0, Landroid/widget/AbsListView;->mScrollUp:Landroid/view/View;

    #@2
    .line 2537
    iput-object p2, p0, Landroid/widget/AbsListView;->mScrollDown:Landroid/view/View;

    #@4
    .line 2538
    return-void
.end method

.method public setScrollingCacheEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1465
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mScrollingCacheEnabled:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    if-nez p1, :cond_9

    #@6
    .line 1466
    invoke-direct {p0}, Landroid/widget/AbsListView;->clearScrollingCache()V

    #@9
    .line 1468
    :cond_9
    iput-boolean p1, p0, Landroid/widget/AbsListView;->mScrollingCacheEnabled:Z

    #@b
    .line 1469
    return-void
.end method

.method abstract setSelectionInt(I)V
.end method

.method public setSelector(I)V
    .registers 3
    .parameter "resID"

    #@0
    .prologue
    .line 2464
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 2465
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "sel"

    #@0
    .prologue
    .line 2468
    iget-object v1, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 2469
    iget-object v1, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@a
    .line 2470
    iget-object v1, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 2472
    :cond_f
    iput-object p1, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@11
    .line 2473
    new-instance v0, Landroid/graphics/Rect;

    #@13
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@16
    .line 2474
    .local v0, padding:Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@19
    .line 2475
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@1b
    iput v1, p0, Landroid/widget/AbsListView;->mSelectionLeftPadding:I

    #@1d
    .line 2476
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@1f
    iput v1, p0, Landroid/widget/AbsListView;->mSelectionTopPadding:I

    #@21
    .line 2477
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@23
    iput v1, p0, Landroid/widget/AbsListView;->mSelectionRightPadding:I

    #@25
    .line 2478
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@27
    iput v1, p0, Landroid/widget/AbsListView;->mSelectionBottomPadding:I

    #@29
    .line 2479
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@2c
    .line 2480
    invoke-virtual {p0}, Landroid/widget/AbsListView;->updateSelectorState()V

    #@2f
    .line 2481
    return-void
.end method

.method public setSmoothScrollbarEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 1336
    iput-boolean p1, p0, Landroid/widget/AbsListView;->mSmoothScrollbarEnabled:Z

    #@2
    .line 1337
    return-void
.end method

.method public setStackFromBottom(Z)V
    .registers 3
    .parameter "stackFromBottom"

    #@0
    .prologue
    .line 1536
    iget-boolean v0, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1537
    iput-boolean p1, p0, Landroid/widget/AbsListView;->mStackFromBottom:Z

    #@6
    .line 1538
    invoke-virtual {p0}, Landroid/widget/AbsListView;->requestLayoutIfNecessary()V

    #@9
    .line 1540
    :cond_9
    return-void
.end method

.method public setTextFilterEnabled(Z)V
    .registers 2
    .parameter "textFilterEnabled"

    #@0
    .prologue
    .line 1482
    iput-boolean p1, p0, Landroid/widget/AbsListView;->mTextFilterEnabled:Z

    #@2
    .line 1483
    return-void
.end method

.method public setTranscriptMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 6010
    iput p1, p0, Landroid/widget/AbsListView;->mTranscriptMode:I

    #@2
    .line 6011
    return-void
.end method

.method public setVelocityScale(F)V
    .registers 2
    .parameter "scale"

    #@0
    .prologue
    .line 4853
    iput p1, p0, Landroid/widget/AbsListView;->mVelocityScale:F

    #@2
    .line 4854
    return-void
.end method

.method public setVerticalScrollbarPosition(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 1302
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setVerticalScrollbarPosition(I)V

    #@3
    .line 1303
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 1304
    iget-object v0, p0, Landroid/widget/AbsListView;->mFastScroller:Landroid/widget/FastScroller;

    #@9
    invoke-virtual {v0, p1}, Landroid/widget/FastScroller;->setScrollbarPosition(I)V

    #@c
    .line 1306
    :cond_c
    return-void
.end method

.method setVisibleRangeHint(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 6177
    iget-object v0, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 6178
    iget-object v0, p0, Landroid/widget/AbsListView;->mRemoteAdapter:Landroid/widget/RemoteViewsAdapter;

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/widget/RemoteViewsAdapter;->setVisibleRangeHint(II)V

    #@9
    .line 6180
    :cond_9
    return-void
.end method

.method shouldShowSelector()Z
    .registers 2

    #@0
    .prologue
    .line 2432
    invoke-virtual {p0}, Landroid/widget/AbsListView;->hasFocus()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    invoke-virtual {p0}, Landroid/widget/AbsListView;->isInTouchMode()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_12

    #@c
    :cond_c
    invoke-virtual {p0}, Landroid/widget/AbsListView;->touchModeDrawsInPressedState()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    :cond_12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public showContextMenu(FFI)Z
    .registers 10
    .parameter "x"
    .parameter "y"
    .parameter "metaState"

    #@0
    .prologue
    .line 2869
    float-to-int v4, p1

    #@1
    float-to-int v5, p2

    #@2
    invoke-virtual {p0, v4, v5}, Landroid/widget/AbsListView;->pointToPosition(II)I

    #@5
    move-result v3

    #@6
    .line 2870
    .local v3, position:I
    const/4 v4, -0x1

    #@7
    if-eq v3, v4, :cond_24

    #@9
    .line 2871
    iget-object v4, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@b
    invoke-interface {v4, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@e
    move-result-wide v1

    #@f
    .line 2872
    .local v1, id:J
    iget v4, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@11
    sub-int v4, v3, v4

    #@13
    invoke-virtual {p0, v4}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    .line 2873
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_24

    #@19
    .line 2874
    invoke-virtual {p0, v0, v3, v1, v2}, Landroid/widget/AbsListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    #@1c
    move-result-object v4

    #@1d
    iput-object v4, p0, Landroid/widget/AbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@1f
    .line 2875
    invoke-super {p0, p0}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    #@22
    move-result v4

    #@23
    .line 2878
    .end local v0           #child:Landroid/view/View;
    .end local v1           #id:J
    :goto_23
    return v4

    #@24
    :cond_24
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AdapterView;->showContextMenu(FFI)Z

    #@27
    move-result v4

    #@28
    goto :goto_23
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .registers 9
    .parameter "originalView"

    #@0
    .prologue
    .line 2883
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->getPositionForView(Landroid/view/View;)I

    #@3
    move-result v3

    #@4
    .line 2884
    .local v3, longPressPosition:I
    if-ltz v3, :cond_2e

    #@6
    .line 2885
    iget-object v0, p0, Landroid/widget/AbsListView;->mAdapter:Landroid/widget/ListAdapter;

    #@8
    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    #@b
    move-result-wide v4

    #@c
    .line 2886
    .local v4, longPressId:J
    const/4 v6, 0x0

    #@d
    .line 2888
    .local v6, handled:Z
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@f
    if-eqz v0, :cond_19

    #@11
    .line 2889
    iget-object v0, p0, Landroid/widget/AdapterView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    #@13
    move-object v1, p0

    #@14
    move-object v2, p1

    #@15
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    #@18
    move-result v6

    #@19
    .line 2892
    :cond_19
    if-nez v6, :cond_2d

    #@1b
    .line 2893
    iget v0, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1d
    sub-int v0, v3, v0

    #@1f
    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {p0, v0, v3, v4, v5}, Landroid/widget/AbsListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    #@26
    move-result-object v0

    #@27
    iput-object v0, p0, Landroid/widget/AbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@29
    .line 2896
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    #@2c
    move-result v6

    #@2d
    .line 2901
    .end local v4           #longPressId:J
    .end local v6           #handled:Z
    :cond_2d
    :goto_2d
    return v6

    #@2e
    :cond_2e
    const/4 v6, 0x0

    #@2f
    goto :goto_2d
.end method

.method public smoothScrollBy(II)V
    .registers 4
    .parameter "distance"
    .parameter "duration"

    #@0
    .prologue
    .line 4927
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/widget/AbsListView;->smoothScrollBy(IIZ)V

    #@4
    .line 4928
    return-void
.end method

.method smoothScrollBy(IIZ)V
    .registers 11
    .parameter "distance"
    .parameter "duration"
    .parameter "linear"

    #@0
    .prologue
    .line 4931
    iget-object v5, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@2
    if-nez v5, :cond_b

    #@4
    .line 4932
    new-instance v5, Landroid/widget/AbsListView$FlingRunnable;

    #@6
    invoke-direct {v5, p0}, Landroid/widget/AbsListView$FlingRunnable;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v5, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@b
    .line 4936
    :cond_b
    iget v2, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@d
    .line 4937
    .local v2, firstPos:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@10
    move-result v1

    #@11
    .line 4938
    .local v1, childCount:I
    add-int v3, v2, v1

    #@13
    .line 4939
    .local v3, lastPos:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getPaddingTop()I

    #@16
    move-result v4

    #@17
    .line 4940
    .local v4, topLimit:I
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getHeight()I

    #@1a
    move-result v5

    #@1b
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getPaddingBottom()I

    #@1e
    move-result v6

    #@1f
    sub-int v0, v5, v6

    #@21
    .line 4942
    .local v0, bottomLimit:I
    if-eqz p1, :cond_4a

    #@23
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@25
    if-eqz v5, :cond_4a

    #@27
    if-eqz v1, :cond_4a

    #@29
    if-nez v2, :cond_38

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    #@33
    move-result v5

    #@34
    if-ne v5, v4, :cond_38

    #@36
    if-ltz p1, :cond_4a

    #@38
    :cond_38
    iget v5, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@3a
    if-ne v3, v5, :cond_59

    #@3c
    add-int/lit8 v5, v1, -0x1

    #@3e
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    #@45
    move-result v5

    #@46
    if-ne v5, v0, :cond_59

    #@48
    if-lez p1, :cond_59

    #@4a
    .line 4946
    :cond_4a
    iget-object v5, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@4c
    invoke-virtual {v5}, Landroid/widget/AbsListView$FlingRunnable;->endFling()V

    #@4f
    .line 4947
    iget-object v5, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@51
    if-eqz v5, :cond_58

    #@53
    .line 4948
    iget-object v5, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@55
    invoke-virtual {v5}, Landroid/widget/AbsListView$PositionScroller;->stop()V

    #@58
    .line 4954
    :cond_58
    :goto_58
    return-void

    #@59
    .line 4951
    :cond_59
    const/4 v5, 0x2

    #@5a
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->reportScrollStateChange(I)V

    #@5d
    .line 4952
    iget-object v5, p0, Landroid/widget/AbsListView;->mFlingRunnable:Landroid/widget/AbsListView$FlingRunnable;

    #@5f
    invoke-virtual {v5, p1, p2, p3}, Landroid/widget/AbsListView$FlingRunnable;->startScroll(IIZ)V

    #@62
    goto :goto_58
.end method

.method smoothScrollByOffset(I)V
    .registers 12
    .parameter "position"

    #@0
    .prologue
    const/high16 v9, 0x3f40

    #@2
    .line 4960
    const/4 v2, -0x1

    #@3
    .line 4961
    .local v2, index:I
    if-gez p1, :cond_58

    #@5
    .line 4962
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    #@8
    move-result v2

    #@9
    .line 4967
    :cond_9
    :goto_9
    const/4 v7, -0x1

    #@a
    if-le v2, v7, :cond_57

    #@c
    .line 4968
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    #@f
    move-result v7

    #@10
    sub-int v7, v2, v7

    #@12
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 4969
    .local v0, child:Landroid/view/View;
    if-eqz v0, :cond_57

    #@18
    .line 4970
    new-instance v4, Landroid/graphics/Rect;

    #@1a
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@1d
    .line 4971
    .local v4, visibleRect:Landroid/graphics/Rect;
    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    #@20
    move-result v7

    #@21
    if-eqz v7, :cond_45

    #@23
    .line 4973
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@26
    move-result v7

    #@27
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@2a
    move-result v8

    #@2b
    mul-int v1, v7, v8

    #@2d
    .line 4974
    .local v1, childRectArea:I
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    #@30
    move-result v7

    #@31
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    #@34
    move-result v8

    #@35
    mul-int v5, v7, v8

    #@37
    .line 4975
    .local v5, visibleRectArea:I
    int-to-float v7, v5

    #@38
    int-to-float v8, v1

    #@39
    div-float v3, v7, v8

    #@3b
    .line 4976
    .local v3, visibleArea:F
    const/high16 v6, 0x3f40

    #@3d
    .line 4977
    .local v6, visibleThreshold:F
    if-gez p1, :cond_5f

    #@3f
    cmpg-float v7, v3, v9

    #@41
    if-gez v7, :cond_5f

    #@43
    .line 4980
    add-int/lit8 v2, v2, 0x1

    #@45
    .line 4987
    .end local v1           #childRectArea:I
    .end local v3           #visibleArea:F
    .end local v5           #visibleRectArea:I
    .end local v6           #visibleThreshold:F
    :cond_45
    :goto_45
    const/4 v7, 0x0

    #@46
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getCount()I

    #@49
    move-result v8

    #@4a
    add-int v9, v2, p1

    #@4c
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@4f
    move-result v8

    #@50
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@53
    move-result v7

    #@54
    invoke-virtual {p0, v7}, Landroid/widget/AbsListView;->smoothScrollToPosition(I)V

    #@57
    .line 4990
    .end local v0           #child:Landroid/view/View;
    .end local v4           #visibleRect:Landroid/graphics/Rect;
    :cond_57
    return-void

    #@58
    .line 4963
    :cond_58
    if-lez p1, :cond_9

    #@5a
    .line 4964
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    #@5d
    move-result v2

    #@5e
    goto :goto_9

    #@5f
    .line 4981
    .restart local v0       #child:Landroid/view/View;
    .restart local v1       #childRectArea:I
    .restart local v3       #visibleArea:F
    .restart local v4       #visibleRect:Landroid/graphics/Rect;
    .restart local v5       #visibleRectArea:I
    .restart local v6       #visibleThreshold:F
    :cond_5f
    if-lez p1, :cond_45

    #@61
    cmpg-float v7, v3, v9

    #@63
    if-gez v7, :cond_45

    #@65
    .line 4984
    add-int/lit8 v2, v2, -0x1

    #@67
    goto :goto_45
.end method

.method public smoothScrollToPosition(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 4862
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 4863
    new-instance v0, Landroid/widget/AbsListView$PositionScroller;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$PositionScroller;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@b
    .line 4865
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@d
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView$PositionScroller;->start(I)V

    #@10
    .line 4866
    return-void
.end method

.method public smoothScrollToPosition(II)V
    .registers 4
    .parameter "position"
    .parameter "boundPosition"

    #@0
    .prologue
    .line 4915
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 4916
    new-instance v0, Landroid/widget/AbsListView$PositionScroller;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$PositionScroller;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@b
    .line 4918
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/widget/AbsListView$PositionScroller;->start(II)V

    #@10
    .line 4919
    return-void
.end method

.method public smoothScrollToPositionFromTop(II)V
    .registers 4
    .parameter "position"
    .parameter "offset"

    #@0
    .prologue
    .line 4899
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 4900
    new-instance v0, Landroid/widget/AbsListView$PositionScroller;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$PositionScroller;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@b
    .line 4902
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/widget/AbsListView$PositionScroller;->startWithOffset(II)V

    #@10
    .line 4903
    return-void
.end method

.method public smoothScrollToPositionFromTop(III)V
    .registers 5
    .parameter "position"
    .parameter "offset"
    .parameter "duration"

    #@0
    .prologue
    .line 4881
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 4882
    new-instance v0, Landroid/widget/AbsListView$PositionScroller;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/AbsListView$PositionScroller;-><init>(Landroid/widget/AbsListView;)V

    #@9
    iput-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@b
    .line 4884
    :cond_b
    iget-object v0, p0, Landroid/widget/AbsListView;->mPositionScroller:Landroid/widget/AbsListView$PositionScroller;

    #@d
    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/AbsListView$PositionScroller;->startWithOffset(III)V

    #@10
    .line 4885
    return-void
.end method

.method touchModeDrawsInPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 2415
    iget v0, p0, Landroid/widget/AbsListView;->mTouchMode:I

    #@2
    packed-switch v0, :pswitch_data_a

    #@5
    .line 2420
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 2418
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 2415
    nop

    #@a
    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method trackMotionScroll(II)Z
    .registers 34
    .parameter "deltaY"
    .parameter "incrementalDeltaY"

    #@0
    .prologue
    .line 5031
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@3
    move-result v8

    #@4
    .line 5032
    .local v8, childCount:I
    if-nez v8, :cond_9

    #@6
    .line 5033
    const/16 v29, 0x1

    #@8
    .line 5186
    :goto_8
    return v29

    #@9
    .line 5036
    :cond_9
    const/16 v29, 0x0

    #@b
    move-object/from16 v0, p0

    #@d
    move/from16 v1, v29

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v29

    #@13
    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getTop()I

    #@16
    move-result v16

    #@17
    .line 5037
    .local v16, firstTop:I
    add-int/lit8 v29, v8, -0x1

    #@19
    move-object/from16 v0, p0

    #@1b
    move/from16 v1, v29

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v29

    #@21
    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getBottom()I

    #@24
    move-result v22

    #@25
    .line 5039
    .local v22, lastBottom:I
    move-object/from16 v0, p0

    #@27
    iget-object v0, v0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@29
    move-object/from16 v23, v0

    #@2b
    .line 5044
    .local v23, listPadding:Landroid/graphics/Rect;
    const/4 v13, 0x0

    #@2c
    .line 5045
    .local v13, effectivePaddingTop:I
    const/4 v12, 0x0

    #@2d
    .line 5046
    .local v12, effectivePaddingBottom:I
    move-object/from16 v0, p0

    #@2f
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@31
    move/from16 v29, v0

    #@33
    and-int/lit8 v29, v29, 0x22

    #@35
    const/16 v30, 0x22

    #@37
    move/from16 v0, v29

    #@39
    move/from16 v1, v30

    #@3b
    if-ne v0, v1, :cond_45

    #@3d
    .line 5047
    move-object/from16 v0, v23

    #@3f
    iget v13, v0, Landroid/graphics/Rect;->top:I

    #@41
    .line 5048
    move-object/from16 v0, v23

    #@43
    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    #@45
    .line 5052
    :cond_45
    sub-int v25, v13, v16

    #@47
    .line 5053
    .local v25, spaceAbove:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@4a
    move-result v29

    #@4b
    sub-int v14, v29, v12

    #@4d
    .line 5054
    .local v14, end:I
    sub-int v26, v22, v14

    #@4f
    .line 5056
    .local v26, spaceBelow:I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@52
    move-result v29

    #@53
    move-object/from16 v0, p0

    #@55
    iget v0, v0, Landroid/view/View;->mPaddingBottom:I

    #@57
    move/from16 v30, v0

    #@59
    sub-int v29, v29, v30

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    #@5f
    move/from16 v30, v0

    #@61
    sub-int v19, v29, v30

    #@63
    .line 5057
    .local v19, height:I
    if-gez p1, :cond_f3

    #@65
    .line 5058
    add-int/lit8 v29, v19, -0x1

    #@67
    move/from16 v0, v29

    #@69
    neg-int v0, v0

    #@6a
    move/from16 v29, v0

    #@6c
    move/from16 v0, v29

    #@6e
    move/from16 v1, p1

    #@70
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@73
    move-result p1

    #@74
    .line 5063
    :goto_74
    if-gez p2, :cond_ff

    #@76
    .line 5064
    add-int/lit8 v29, v19, -0x1

    #@78
    move/from16 v0, v29

    #@7a
    neg-int v0, v0

    #@7b
    move/from16 v29, v0

    #@7d
    move/from16 v0, v29

    #@7f
    move/from16 v1, p2

    #@81
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@84
    move-result p2

    #@85
    .line 5069
    :goto_85
    move-object/from16 v0, p0

    #@87
    iget v15, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@89
    .line 5072
    .local v15, firstPosition:I
    if-nez v15, :cond_10b

    #@8b
    .line 5073
    move-object/from16 v0, v23

    #@8d
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@8f
    move/from16 v29, v0

    #@91
    sub-int v29, v16, v29

    #@93
    move/from16 v0, v29

    #@95
    move-object/from16 v1, p0

    #@97
    iput v0, v1, Landroid/widget/AbsListView;->mFirstPositionDistanceGuess:I

    #@99
    .line 5077
    :goto_99
    add-int v29, v15, v8

    #@9b
    move-object/from16 v0, p0

    #@9d
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@9f
    move/from16 v30, v0

    #@a1
    move/from16 v0, v29

    #@a3
    move/from16 v1, v30

    #@a5
    if-ne v0, v1, :cond_11a

    #@a7
    .line 5078
    move-object/from16 v0, v23

    #@a9
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@ab
    move/from16 v29, v0

    #@ad
    add-int v29, v29, v22

    #@af
    move/from16 v0, v29

    #@b1
    move-object/from16 v1, p0

    #@b3
    iput v0, v1, Landroid/widget/AbsListView;->mLastPositionDistanceGuess:I

    #@b5
    .line 5083
    :goto_b5
    if-nez v15, :cond_129

    #@b7
    move-object/from16 v0, v23

    #@b9
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@bb
    move/from16 v29, v0

    #@bd
    move/from16 v0, v16

    #@bf
    move/from16 v1, v29

    #@c1
    if-lt v0, v1, :cond_129

    #@c3
    if-ltz p2, :cond_129

    #@c5
    const/4 v5, 0x1

    #@c6
    .line 5085
    .local v5, cannotScrollDown:Z
    :goto_c6
    add-int v29, v15, v8

    #@c8
    move-object/from16 v0, p0

    #@ca
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@cc
    move/from16 v30, v0

    #@ce
    move/from16 v0, v29

    #@d0
    move/from16 v1, v30

    #@d2
    if-ne v0, v1, :cond_12b

    #@d4
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@d7
    move-result v29

    #@d8
    move-object/from16 v0, v23

    #@da
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@dc
    move/from16 v30, v0

    #@de
    sub-int v29, v29, v30

    #@e0
    move/from16 v0, v22

    #@e2
    move/from16 v1, v29

    #@e4
    if-gt v0, v1, :cond_12b

    #@e6
    if-gtz p2, :cond_12b

    #@e8
    const/4 v6, 0x1

    #@e9
    .line 5088
    .local v6, cannotScrollUp:Z
    :goto_e9
    if-nez v5, :cond_ed

    #@eb
    if-eqz v6, :cond_131

    #@ed
    .line 5089
    :cond_ed
    if-eqz p2, :cond_12d

    #@ef
    const/16 v29, 0x1

    #@f1
    goto/16 :goto_8

    #@f3
    .line 5060
    .end local v5           #cannotScrollDown:Z
    .end local v6           #cannotScrollUp:Z
    .end local v15           #firstPosition:I
    :cond_f3
    add-int/lit8 v29, v19, -0x1

    #@f5
    move/from16 v0, v29

    #@f7
    move/from16 v1, p1

    #@f9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@fc
    move-result p1

    #@fd
    goto/16 :goto_74

    #@ff
    .line 5066
    :cond_ff
    add-int/lit8 v29, v19, -0x1

    #@101
    move/from16 v0, v29

    #@103
    move/from16 v1, p2

    #@105
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@108
    move-result p2

    #@109
    goto/16 :goto_85

    #@10b
    .line 5075
    .restart local v15       #firstPosition:I
    :cond_10b
    move-object/from16 v0, p0

    #@10d
    iget v0, v0, Landroid/widget/AbsListView;->mFirstPositionDistanceGuess:I

    #@10f
    move/from16 v29, v0

    #@111
    add-int v29, v29, p2

    #@113
    move/from16 v0, v29

    #@115
    move-object/from16 v1, p0

    #@117
    iput v0, v1, Landroid/widget/AbsListView;->mFirstPositionDistanceGuess:I

    #@119
    goto :goto_99

    #@11a
    .line 5080
    :cond_11a
    move-object/from16 v0, p0

    #@11c
    iget v0, v0, Landroid/widget/AbsListView;->mLastPositionDistanceGuess:I

    #@11e
    move/from16 v29, v0

    #@120
    add-int v29, v29, p2

    #@122
    move/from16 v0, v29

    #@124
    move-object/from16 v1, p0

    #@126
    iput v0, v1, Landroid/widget/AbsListView;->mLastPositionDistanceGuess:I

    #@128
    goto :goto_b5

    #@129
    .line 5083
    :cond_129
    const/4 v5, 0x0

    #@12a
    goto :goto_c6

    #@12b
    .line 5085
    .restart local v5       #cannotScrollDown:Z
    :cond_12b
    const/4 v6, 0x0

    #@12c
    goto :goto_e9

    #@12d
    .line 5089
    .restart local v6       #cannotScrollUp:Z
    :cond_12d
    const/16 v29, 0x0

    #@12f
    goto/16 :goto_8

    #@131
    .line 5092
    :cond_131
    if-gez p2, :cond_22c

    #@133
    const/4 v11, 0x1

    #@134
    .line 5094
    .local v11, down:Z
    :goto_134
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->isInTouchMode()Z

    #@137
    move-result v21

    #@138
    .line 5095
    .local v21, inTouchMode:Z
    if-eqz v21, :cond_13d

    #@13a
    .line 5096
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->hideSelector()V

    #@13d
    .line 5099
    :cond_13d
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeaderViewsCount()I

    #@140
    move-result v18

    #@141
    .line 5100
    .local v18, headerViewsCount:I
    move-object/from16 v0, p0

    #@143
    iget v0, v0, Landroid/widget/AdapterView;->mItemCount:I

    #@145
    move/from16 v29, v0

    #@147
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getFooterViewsCount()I

    #@14a
    move-result v30

    #@14b
    sub-int v17, v29, v30

    #@14d
    .line 5102
    .local v17, footerViewsStart:I
    const/16 v27, 0x0

    #@14f
    .line 5103
    .local v27, start:I
    const/4 v10, 0x0

    #@150
    .line 5105
    .local v10, count:I
    if-eqz v11, :cond_250

    #@152
    .line 5106
    move/from16 v0, p2

    #@154
    neg-int v0, v0

    #@155
    move/from16 v28, v0

    #@157
    .line 5107
    .local v28, top:I
    move-object/from16 v0, p0

    #@159
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@15b
    move/from16 v29, v0

    #@15d
    and-int/lit8 v29, v29, 0x22

    #@15f
    const/16 v30, 0x22

    #@161
    move/from16 v0, v29

    #@163
    move/from16 v1, v30

    #@165
    if-ne v0, v1, :cond_16f

    #@167
    .line 5108
    move-object/from16 v0, v23

    #@169
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@16b
    move/from16 v29, v0

    #@16d
    add-int v28, v28, v29

    #@16f
    .line 5110
    :cond_16f
    const/16 v20, 0x0

    #@171
    .local v20, i:I
    :goto_171
    move/from16 v0, v20

    #@173
    if-ge v0, v8, :cond_187

    #@175
    .line 5111
    move-object/from16 v0, p0

    #@177
    move/from16 v1, v20

    #@179
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@17c
    move-result-object v7

    #@17d
    .line 5112
    .local v7, child:Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    #@180
    move-result v29

    #@181
    move/from16 v0, v29

    #@183
    move/from16 v1, v28

    #@185
    if-lt v0, v1, :cond_22f

    #@187
    .line 5142
    .end local v7           #child:Landroid/view/View;
    .end local v28           #top:I
    :cond_187
    move-object/from16 v0, p0

    #@189
    iget v0, v0, Landroid/widget/AbsListView;->mMotionViewOriginalTop:I

    #@18b
    move/from16 v29, v0

    #@18d
    add-int v29, v29, p1

    #@18f
    move/from16 v0, v29

    #@191
    move-object/from16 v1, p0

    #@193
    iput v0, v1, Landroid/widget/AbsListView;->mMotionViewNewTop:I

    #@195
    .line 5144
    const/16 v29, 0x1

    #@197
    move/from16 v0, v29

    #@199
    move-object/from16 v1, p0

    #@19b
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@19d
    .line 5146
    if-lez v10, :cond_1af

    #@19f
    .line 5147
    move-object/from16 v0, p0

    #@1a1
    move/from16 v1, v27

    #@1a3
    invoke-virtual {v0, v1, v10}, Landroid/widget/AbsListView;->detachViewsFromParent(II)V

    #@1a6
    .line 5148
    move-object/from16 v0, p0

    #@1a8
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@1aa
    move-object/from16 v29, v0

    #@1ac
    invoke-virtual/range {v29 .. v29}, Landroid/widget/AbsListView$RecycleBin;->removeSkippedScrap()V

    #@1af
    .line 5153
    :cond_1af
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->awakenScrollBars()Z

    #@1b2
    move-result v29

    #@1b3
    if-nez v29, :cond_1b8

    #@1b5
    .line 5154
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->invalidate()V

    #@1b8
    .line 5157
    :cond_1b8
    move-object/from16 v0, p0

    #@1ba
    move/from16 v1, p2

    #@1bc
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->offsetChildrenTopAndBottom(I)V

    #@1bf
    .line 5159
    if-eqz v11, :cond_1cf

    #@1c1
    .line 5160
    move-object/from16 v0, p0

    #@1c3
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1c5
    move/from16 v29, v0

    #@1c7
    add-int v29, v29, v10

    #@1c9
    move/from16 v0, v29

    #@1cb
    move-object/from16 v1, p0

    #@1cd
    iput v0, v1, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1cf
    .line 5163
    :cond_1cf
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    #@1d2
    move-result v3

    #@1d3
    .line 5164
    .local v3, absIncrementalDeltaY:I
    move/from16 v0, v25

    #@1d5
    if-lt v0, v3, :cond_1db

    #@1d7
    move/from16 v0, v26

    #@1d9
    if-ge v0, v3, :cond_1e0

    #@1db
    .line 5165
    :cond_1db
    move-object/from16 v0, p0

    #@1dd
    invoke-virtual {v0, v11}, Landroid/widget/AbsListView;->fillGap(Z)V

    #@1e0
    .line 5168
    :cond_1e0
    if-nez v21, :cond_2a4

    #@1e2
    move-object/from16 v0, p0

    #@1e4
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1e6
    move/from16 v29, v0

    #@1e8
    const/16 v30, -0x1

    #@1ea
    move/from16 v0, v29

    #@1ec
    move/from16 v1, v30

    #@1ee
    if-eq v0, v1, :cond_2a4

    #@1f0
    .line 5169
    move-object/from16 v0, p0

    #@1f2
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@1f4
    move/from16 v29, v0

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@1fa
    move/from16 v30, v0

    #@1fc
    sub-int v9, v29, v30

    #@1fe
    .line 5170
    .local v9, childIndex:I
    if-ltz v9, :cond_21d

    #@200
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@203
    move-result v29

    #@204
    move/from16 v0, v29

    #@206
    if-ge v9, v0, :cond_21d

    #@208
    .line 5171
    move-object/from16 v0, p0

    #@20a
    iget v0, v0, Landroid/widget/AdapterView;->mSelectedPosition:I

    #@20c
    move/from16 v29, v0

    #@20e
    move-object/from16 v0, p0

    #@210
    invoke-virtual {v0, v9}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@213
    move-result-object v30

    #@214
    move-object/from16 v0, p0

    #@216
    move/from16 v1, v29

    #@218
    move-object/from16 v2, v30

    #@21a
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->positionSelector(ILandroid/view/View;)V

    #@21d
    .line 5182
    .end local v9           #childIndex:I
    :cond_21d
    :goto_21d
    const/16 v29, 0x0

    #@21f
    move/from16 v0, v29

    #@221
    move-object/from16 v1, p0

    #@223
    iput-boolean v0, v1, Landroid/widget/AdapterView;->mBlockLayoutRequests:Z

    #@225
    .line 5184
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->invokeOnItemScrollListener()V

    #@228
    .line 5186
    const/16 v29, 0x0

    #@22a
    goto/16 :goto_8

    #@22c
    .line 5092
    .end local v3           #absIncrementalDeltaY:I
    .end local v10           #count:I
    .end local v11           #down:Z
    .end local v17           #footerViewsStart:I
    .end local v18           #headerViewsCount:I
    .end local v20           #i:I
    .end local v21           #inTouchMode:Z
    .end local v27           #start:I
    :cond_22c
    const/4 v11, 0x0

    #@22d
    goto/16 :goto_134

    #@22f
    .line 5115
    .restart local v7       #child:Landroid/view/View;
    .restart local v10       #count:I
    .restart local v11       #down:Z
    .restart local v17       #footerViewsStart:I
    .restart local v18       #headerViewsCount:I
    .restart local v20       #i:I
    .restart local v21       #inTouchMode:Z
    .restart local v27       #start:I
    .restart local v28       #top:I
    :cond_22f
    add-int/lit8 v10, v10, 0x1

    #@231
    .line 5116
    add-int v24, v15, v20

    #@233
    .line 5117
    .local v24, position:I
    move/from16 v0, v24

    #@235
    move/from16 v1, v18

    #@237
    if-lt v0, v1, :cond_24c

    #@239
    move/from16 v0, v24

    #@23b
    move/from16 v1, v17

    #@23d
    if-ge v0, v1, :cond_24c

    #@23f
    .line 5118
    move-object/from16 v0, p0

    #@241
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@243
    move-object/from16 v29, v0

    #@245
    move-object/from16 v0, v29

    #@247
    move/from16 v1, v24

    #@249
    invoke-virtual {v0, v7, v1}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@24c
    .line 5110
    :cond_24c
    add-int/lit8 v20, v20, 0x1

    #@24e
    goto/16 :goto_171

    #@250
    .line 5123
    .end local v7           #child:Landroid/view/View;
    .end local v20           #i:I
    .end local v24           #position:I
    .end local v28           #top:I
    :cond_250
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getHeight()I

    #@253
    move-result v29

    #@254
    sub-int v4, v29, p2

    #@256
    .line 5124
    .local v4, bottom:I
    move-object/from16 v0, p0

    #@258
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@25a
    move/from16 v29, v0

    #@25c
    and-int/lit8 v29, v29, 0x22

    #@25e
    const/16 v30, 0x22

    #@260
    move/from16 v0, v29

    #@262
    move/from16 v1, v30

    #@264
    if-ne v0, v1, :cond_26e

    #@266
    .line 5125
    move-object/from16 v0, v23

    #@268
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@26a
    move/from16 v29, v0

    #@26c
    sub-int v4, v4, v29

    #@26e
    .line 5127
    :cond_26e
    add-int/lit8 v20, v8, -0x1

    #@270
    .restart local v20       #i:I
    :goto_270
    if-ltz v20, :cond_187

    #@272
    .line 5128
    move-object/from16 v0, p0

    #@274
    move/from16 v1, v20

    #@276
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@279
    move-result-object v7

    #@27a
    .line 5129
    .restart local v7       #child:Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    #@27d
    move-result v29

    #@27e
    move/from16 v0, v29

    #@280
    if-le v0, v4, :cond_187

    #@282
    .line 5132
    move/from16 v27, v20

    #@284
    .line 5133
    add-int/lit8 v10, v10, 0x1

    #@286
    .line 5134
    add-int v24, v15, v20

    #@288
    .line 5135
    .restart local v24       #position:I
    move/from16 v0, v24

    #@28a
    move/from16 v1, v18

    #@28c
    if-lt v0, v1, :cond_2a1

    #@28e
    move/from16 v0, v24

    #@290
    move/from16 v1, v17

    #@292
    if-ge v0, v1, :cond_2a1

    #@294
    .line 5136
    move-object/from16 v0, p0

    #@296
    iget-object v0, v0, Landroid/widget/AbsListView;->mRecycler:Landroid/widget/AbsListView$RecycleBin;

    #@298
    move-object/from16 v29, v0

    #@29a
    move-object/from16 v0, v29

    #@29c
    move/from16 v1, v24

    #@29e
    invoke-virtual {v0, v7, v1}, Landroid/widget/AbsListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    #@2a1
    .line 5127
    :cond_2a1
    add-int/lit8 v20, v20, -0x1

    #@2a3
    goto :goto_270

    #@2a4
    .line 5173
    .end local v4           #bottom:I
    .end local v7           #child:Landroid/view/View;
    .end local v24           #position:I
    .restart local v3       #absIncrementalDeltaY:I
    :cond_2a4
    move-object/from16 v0, p0

    #@2a6
    iget v0, v0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@2a8
    move/from16 v29, v0

    #@2aa
    const/16 v30, -0x1

    #@2ac
    move/from16 v0, v29

    #@2ae
    move/from16 v1, v30

    #@2b0
    if-eq v0, v1, :cond_2dd

    #@2b2
    .line 5174
    move-object/from16 v0, p0

    #@2b4
    iget v0, v0, Landroid/widget/AbsListView;->mSelectorPosition:I

    #@2b6
    move/from16 v29, v0

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    iget v0, v0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@2bc
    move/from16 v30, v0

    #@2be
    sub-int v9, v29, v30

    #@2c0
    .line 5175
    .restart local v9       #childIndex:I
    if-ltz v9, :cond_21d

    #@2c2
    invoke-virtual/range {p0 .. p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@2c5
    move-result v29

    #@2c6
    move/from16 v0, v29

    #@2c8
    if-ge v9, v0, :cond_21d

    #@2ca
    .line 5176
    const/16 v29, -0x1

    #@2cc
    move-object/from16 v0, p0

    #@2ce
    invoke-virtual {v0, v9}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@2d1
    move-result-object v30

    #@2d2
    move-object/from16 v0, p0

    #@2d4
    move/from16 v1, v29

    #@2d6
    move-object/from16 v2, v30

    #@2d8
    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->positionSelector(ILandroid/view/View;)V

    #@2db
    goto/16 :goto_21d

    #@2dd
    .line 5179
    .end local v9           #childIndex:I
    :cond_2dd
    move-object/from16 v0, p0

    #@2df
    iget-object v0, v0, Landroid/widget/AbsListView;->mSelectorRect:Landroid/graphics/Rect;

    #@2e1
    move-object/from16 v29, v0

    #@2e3
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Rect;->setEmpty()V

    #@2e6
    goto/16 :goto_21d
.end method

.method updateScrollIndicators()V
    .registers 11

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 2056
    iget-object v6, p0, Landroid/widget/AbsListView;->mScrollUp:Landroid/view/View;

    #@5
    if-eqz v6, :cond_2b

    #@7
    .line 2059
    iget v6, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@9
    if-lez v6, :cond_5b

    #@b
    move v1, v4

    #@c
    .line 2062
    .local v1, canScrollUp:Z
    :goto_c
    if-nez v1, :cond_23

    #@e
    .line 2063
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@11
    move-result v6

    #@12
    if-lez v6, :cond_23

    #@14
    .line 2064
    invoke-virtual {p0, v5}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@17
    move-result-object v2

    #@18
    .line 2065
    .local v2, child:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    #@1b
    move-result v6

    #@1c
    iget-object v8, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@1e
    iget v8, v8, Landroid/graphics/Rect;->top:I

    #@20
    if-ge v6, v8, :cond_5d

    #@22
    move v1, v4

    #@23
    .line 2069
    .end local v2           #child:Landroid/view/View;
    :cond_23
    :goto_23
    iget-object v8, p0, Landroid/widget/AbsListView;->mScrollUp:Landroid/view/View;

    #@25
    if-eqz v1, :cond_5f

    #@27
    move v6, v5

    #@28
    :goto_28
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    #@2b
    .line 2072
    .end local v1           #canScrollUp:Z
    :cond_2b
    iget-object v6, p0, Landroid/widget/AbsListView;->mScrollDown:Landroid/view/View;

    #@2d
    if-eqz v6, :cond_5a

    #@2f
    .line 2074
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    #@32
    move-result v3

    #@33
    .line 2077
    .local v3, count:I
    iget v6, p0, Landroid/widget/AdapterView;->mFirstPosition:I

    #@35
    add-int/2addr v6, v3

    #@36
    iget v8, p0, Landroid/widget/AdapterView;->mItemCount:I

    #@38
    if-ge v6, v8, :cond_61

    #@3a
    move v0, v4

    #@3b
    .line 2080
    .local v0, canScrollDown:Z
    :goto_3b
    if-nez v0, :cond_53

    #@3d
    if-lez v3, :cond_53

    #@3f
    .line 2081
    add-int/lit8 v6, v3, -0x1

    #@41
    invoke-virtual {p0, v6}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@44
    move-result-object v2

    #@45
    .line 2082
    .restart local v2       #child:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    #@48
    move-result v6

    #@49
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@4b
    iget-object v9, p0, Landroid/widget/AbsListView;->mListPadding:Landroid/graphics/Rect;

    #@4d
    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    #@4f
    sub-int/2addr v8, v9

    #@50
    if-le v6, v8, :cond_63

    #@52
    move v0, v4

    #@53
    .line 2085
    .end local v2           #child:Landroid/view/View;
    :cond_53
    :goto_53
    iget-object v4, p0, Landroid/widget/AbsListView;->mScrollDown:Landroid/view/View;

    #@55
    if-eqz v0, :cond_65

    #@57
    :goto_57
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    #@5a
    .line 2087
    .end local v0           #canScrollDown:Z
    .end local v3           #count:I
    :cond_5a
    return-void

    #@5b
    :cond_5b
    move v1, v5

    #@5c
    .line 2059
    goto :goto_c

    #@5d
    .restart local v1       #canScrollUp:Z
    .restart local v2       #child:Landroid/view/View;
    :cond_5d
    move v1, v5

    #@5e
    .line 2065
    goto :goto_23

    #@5f
    .end local v2           #child:Landroid/view/View;
    :cond_5f
    move v6, v7

    #@60
    .line 2069
    goto :goto_28

    #@61
    .end local v1           #canScrollUp:Z
    .restart local v3       #count:I
    :cond_61
    move v0, v5

    #@62
    .line 2077
    goto :goto_3b

    #@63
    .restart local v0       #canScrollDown:Z
    .restart local v2       #child:Landroid/view/View;
    :cond_63
    move v0, v5

    #@64
    .line 2082
    goto :goto_53

    #@65
    .end local v2           #child:Landroid/view/View;
    :cond_65
    move v5, v7

    #@66
    .line 2085
    goto :goto_57
.end method

.method updateSelectorState()V
    .registers 3

    #@0
    .prologue
    .line 2541
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 2542
    invoke-virtual {p0}, Landroid/widget/AbsListView;->shouldShowSelector()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_14

    #@a
    .line 2543
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getDrawableState()[I

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@13
    .line 2548
    :cond_13
    :goto_13
    return-void

    #@14
    .line 2545
    :cond_14
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@16
    sget-object v1, Landroid/util/StateSet;->NOTHING:[I

    #@18
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@1b
    goto :goto_13
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "dr"

    #@0
    .prologue
    .line 2592
    iget-object v0, p0, Landroid/widget/AbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq v0, p1, :cond_a

    #@4
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
