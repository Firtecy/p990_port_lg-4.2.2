.class Landroid/widget/Filter$RequestHandler;
.super Landroid/os/Handler;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/Filter;


# direct methods
.method public constructor <init>(Landroid/widget/Filter;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 216
    iput-object p1, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@2
    .line 217
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 218
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    .line 228
    iget v4, p1, Landroid/os/Message;->what:I

    #@2
    .line 230
    .local v4, what:I
    sparse-switch v4, :sswitch_data_a4

    #@5
    .line 260
    :goto_5
    return-void

    #@6
    .line 232
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/widget/Filter$RequestArguments;

    #@a
    .line 234
    .local v0, args:Landroid/widget/Filter$RequestArguments;
    :try_start_a
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@c
    iget-object v6, v0, Landroid/widget/Filter$RequestArguments;->constraint:Ljava/lang/CharSequence;

    #@e
    invoke-virtual {v5, v6}, Landroid/widget/Filter;->performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;

    #@11
    move-result-object v5

    #@12
    iput-object v5, v0, Landroid/widget/Filter$RequestArguments;->results:Landroid/widget/Filter$FilterResults;
    :try_end_14
    .catchall {:try_start_a .. :try_end_14} :catchall_6b
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_14} :catch_4f

    #@14
    .line 239
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@16
    invoke-static {v5}, Landroid/widget/Filter;->access$200(Landroid/widget/Filter;)Landroid/os/Handler;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1d
    move-result-object v3

    #@1e
    .line 240
    .local v3, message:Landroid/os/Message;
    iput-object v0, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    .line 241
    :goto_20
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@23
    .line 244
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@25
    invoke-static {v5}, Landroid/widget/Filter;->access$300(Landroid/widget/Filter;)Ljava/lang/Object;

    #@28
    move-result-object v6

    #@29
    monitor-enter v6

    #@2a
    .line 245
    :try_start_2a
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@2c
    invoke-static {v5}, Landroid/widget/Filter;->access$400(Landroid/widget/Filter;)Landroid/os/Handler;

    #@2f
    move-result-object v5

    #@30
    if-eqz v5, :cond_4a

    #@32
    .line 246
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@34
    invoke-static {v5}, Landroid/widget/Filter;->access$400(Landroid/widget/Filter;)Landroid/os/Handler;

    #@37
    move-result-object v5

    #@38
    const v7, -0x21524111

    #@3b
    invoke-virtual {v5, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3e
    move-result-object v2

    #@3f
    .line 247
    .local v2, finishMessage:Landroid/os/Message;
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@41
    invoke-static {v5}, Landroid/widget/Filter;->access$400(Landroid/widget/Filter;)Landroid/os/Handler;

    #@44
    move-result-object v5

    #@45
    const-wide/16 v7, 0xbb8

    #@47
    invoke-virtual {v5, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@4a
    .line 249
    .end local v2           #finishMessage:Landroid/os/Message;
    :cond_4a
    monitor-exit v6

    #@4b
    goto :goto_5

    #@4c
    :catchall_4c
    move-exception v5

    #@4d
    monitor-exit v6
    :try_end_4e
    .catchall {:try_start_2a .. :try_end_4e} :catchall_4c

    #@4e
    throw v5

    #@4f
    .line 235
    .end local v3           #message:Landroid/os/Message;
    :catch_4f
    move-exception v1

    #@50
    .line 236
    .local v1, e:Ljava/lang/Exception;
    :try_start_50
    new-instance v5, Landroid/widget/Filter$FilterResults;

    #@52
    invoke-direct {v5}, Landroid/widget/Filter$FilterResults;-><init>()V

    #@55
    iput-object v5, v0, Landroid/widget/Filter$RequestArguments;->results:Landroid/widget/Filter$FilterResults;

    #@57
    .line 237
    const-string v5, "Filter"

    #@59
    const-string v6, "An exception occured during performFiltering()!"

    #@5b
    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5e
    .catchall {:try_start_50 .. :try_end_5e} :catchall_6b

    #@5e
    .line 239
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@60
    invoke-static {v5}, Landroid/widget/Filter;->access$200(Landroid/widget/Filter;)Landroid/os/Handler;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@67
    move-result-object v3

    #@68
    .line 240
    .restart local v3       #message:Landroid/os/Message;
    iput-object v0, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6a
    goto :goto_20

    #@6b
    .line 239
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #message:Landroid/os/Message;
    :catchall_6b
    move-exception v5

    #@6c
    iget-object v6, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@6e
    invoke-static {v6}, Landroid/widget/Filter;->access$200(Landroid/widget/Filter;)Landroid/os/Handler;

    #@71
    move-result-object v6

    #@72
    invoke-virtual {v6, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@75
    move-result-object v3

    #@76
    .line 240
    .restart local v3       #message:Landroid/os/Message;
    iput-object v0, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@78
    .line 241
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@7b
    .line 239
    throw v5

    #@7c
    .line 252
    .end local v0           #args:Landroid/widget/Filter$RequestArguments;
    .end local v3           #message:Landroid/os/Message;
    :sswitch_7c
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@7e
    invoke-static {v5}, Landroid/widget/Filter;->access$300(Landroid/widget/Filter;)Ljava/lang/Object;

    #@81
    move-result-object v6

    #@82
    monitor-enter v6

    #@83
    .line 253
    :try_start_83
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@85
    invoke-static {v5}, Landroid/widget/Filter;->access$400(Landroid/widget/Filter;)Landroid/os/Handler;

    #@88
    move-result-object v5

    #@89
    if-eqz v5, :cond_9e

    #@8b
    .line 254
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@8d
    invoke-static {v5}, Landroid/widget/Filter;->access$400(Landroid/widget/Filter;)Landroid/os/Handler;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5}, Landroid/os/Looper;->quit()V

    #@98
    .line 255
    iget-object v5, p0, Landroid/widget/Filter$RequestHandler;->this$0:Landroid/widget/Filter;

    #@9a
    const/4 v7, 0x0

    #@9b
    invoke-static {v5, v7}, Landroid/widget/Filter;->access$402(Landroid/widget/Filter;Landroid/os/Handler;)Landroid/os/Handler;

    #@9e
    .line 257
    :cond_9e
    monitor-exit v6

    #@9f
    goto/16 :goto_5

    #@a1
    :catchall_a1
    move-exception v5

    #@a2
    monitor-exit v6
    :try_end_a3
    .catchall {:try_start_83 .. :try_end_a3} :catchall_a1

    #@a3
    throw v5

    #@a4
    .line 230
    :sswitch_data_a4
    .sparse-switch
        -0x2f2f0ff3 -> :sswitch_6
        -0x21524111 -> :sswitch_7c
    .end sparse-switch
.end method
