.class Landroid/widget/DatePicker$1;
.super Ljava/lang/Object;
.source "DatePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/DatePicker;


# direct methods
.method constructor <init>(Landroid/widget/DatePicker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 186
    iput-object p1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .registers 12
    .parameter "picker"
    .parameter "oldVal"
    .parameter "newVal"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x5

    #@3
    const/4 v4, 0x1

    #@4
    .line 188
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@6
    invoke-static {v1}, Landroid/widget/DatePicker;->access$000(Landroid/widget/DatePicker;)V

    #@9
    .line 189
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@b
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@e
    move-result-object v1

    #@f
    iget-object v2, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@11
    invoke-static {v2}, Landroid/widget/DatePicker;->access$100(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    #@18
    move-result-wide v2

    #@19
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@1c
    .line 191
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@1e
    invoke-static {v1}, Landroid/widget/DatePicker;->access$300(Landroid/widget/DatePicker;)Landroid/widget/NumberPicker;

    #@21
    move-result-object v1

    #@22
    if-ne p1, v1, :cond_88

    #@24
    .line 192
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@26
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    #@2d
    move-result v0

    #@2e
    .line 193
    .local v0, maxDayOfMonth:I
    if-ne p2, v0, :cond_6e

    #@30
    if-ne p3, v4, :cond_6e

    #@32
    .line 194
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@34
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, v5, v4}, Ljava/util/Calendar;->add(II)V

    #@3b
    .line 216
    .end local v0           #maxDayOfMonth:I
    :goto_3b
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@3d
    iget-object v2, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@3f
    invoke-static {v2}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    #@46
    move-result v2

    #@47
    iget-object v3, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@49
    invoke-static {v3}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    #@50
    move-result v3

    #@51
    iget-object v4, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@53
    invoke-static {v4}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    #@5a
    move-result v4

    #@5b
    invoke-static {v1, v2, v3, v4}, Landroid/widget/DatePicker;->access$700(Landroid/widget/DatePicker;III)V

    #@5e
    .line 218
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@60
    invoke-static {v1}, Landroid/widget/DatePicker;->access$800(Landroid/widget/DatePicker;)V

    #@63
    .line 219
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@65
    invoke-static {v1}, Landroid/widget/DatePicker;->access$900(Landroid/widget/DatePicker;)V

    #@68
    .line 220
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@6a
    invoke-static {v1}, Landroid/widget/DatePicker;->access$1000(Landroid/widget/DatePicker;)V

    #@6d
    .line 221
    return-void

    #@6e
    .line 195
    .restart local v0       #maxDayOfMonth:I
    :cond_6e
    if-ne p2, v4, :cond_7c

    #@70
    if-ne p3, v0, :cond_7c

    #@72
    .line 196
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@74
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1, v5, v7}, Ljava/util/Calendar;->add(II)V

    #@7b
    goto :goto_3b

    #@7c
    .line 198
    :cond_7c
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@7e
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@81
    move-result-object v1

    #@82
    sub-int v2, p3, p2

    #@84
    invoke-virtual {v1, v5, v2}, Ljava/util/Calendar;->add(II)V

    #@87
    goto :goto_3b

    #@88
    .line 200
    .end local v0           #maxDayOfMonth:I
    :cond_88
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@8a
    invoke-static {v1}, Landroid/widget/DatePicker;->access$400(Landroid/widget/DatePicker;)Landroid/widget/NumberPicker;

    #@8d
    move-result-object v1

    #@8e
    if-ne p1, v1, :cond_da

    #@90
    .line 202
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@92
    invoke-static {v1}, Landroid/widget/DatePicker;->access$500(Landroid/widget/DatePicker;)I

    #@95
    move-result v1

    #@96
    add-int/lit8 v1, v1, 0xb

    #@98
    if-ne p2, v1, :cond_ae

    #@9a
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@9c
    invoke-static {v1}, Landroid/widget/DatePicker;->access$500(Landroid/widget/DatePicker;)I

    #@9f
    move-result v1

    #@a0
    add-int/lit8 v1, v1, 0x0

    #@a2
    if-ne p3, v1, :cond_ae

    #@a4
    .line 203
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@a6
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@a9
    move-result-object v1

    #@aa
    invoke-virtual {v1, v6, v4}, Ljava/util/Calendar;->add(II)V

    #@ad
    goto :goto_3b

    #@ae
    .line 204
    :cond_ae
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@b0
    invoke-static {v1}, Landroid/widget/DatePicker;->access$500(Landroid/widget/DatePicker;)I

    #@b3
    move-result v1

    #@b4
    add-int/lit8 v1, v1, 0x0

    #@b6
    if-ne p2, v1, :cond_cd

    #@b8
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@ba
    invoke-static {v1}, Landroid/widget/DatePicker;->access$500(Landroid/widget/DatePicker;)I

    #@bd
    move-result v1

    #@be
    add-int/lit8 v1, v1, 0xb

    #@c0
    if-ne p3, v1, :cond_cd

    #@c2
    .line 205
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@c4
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@c7
    move-result-object v1

    #@c8
    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    #@cb
    goto/16 :goto_3b

    #@cd
    .line 207
    :cond_cd
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@cf
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@d2
    move-result-object v1

    #@d3
    sub-int v2, p3, p2

    #@d5
    invoke-virtual {v1, v6, v2}, Ljava/util/Calendar;->add(II)V

    #@d8
    goto/16 :goto_3b

    #@da
    .line 210
    :cond_da
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@dc
    invoke-static {v1}, Landroid/widget/DatePicker;->access$600(Landroid/widget/DatePicker;)Landroid/widget/NumberPicker;

    #@df
    move-result-object v1

    #@e0
    if-ne p1, v1, :cond_ed

    #@e2
    .line 211
    iget-object v1, p0, Landroid/widget/DatePicker$1;->this$0:Landroid/widget/DatePicker;

    #@e4
    invoke-static {v1}, Landroid/widget/DatePicker;->access$200(Landroid/widget/DatePicker;)Ljava/util/Calendar;

    #@e7
    move-result-object v1

    #@e8
    invoke-virtual {v1, v4, p3}, Ljava/util/Calendar;->set(II)V

    #@eb
    goto/16 :goto_3b

    #@ed
    .line 213
    :cond_ed
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@ef
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@f2
    throw v1
.end method
