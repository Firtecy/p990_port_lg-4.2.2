.class Landroid/widget/TimePicker$3;
.super Ljava/lang/Object;
.source "TimePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Landroid/widget/TimePicker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 176
    iput-object p1, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .registers 11
    .parameter "spinner"
    .parameter "oldVal"
    .parameter "newVal"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 178
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@4
    invoke-static {v5}, Landroid/widget/TimePicker;->access$000(Landroid/widget/TimePicker;)V

    #@7
    .line 179
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@9
    invoke-static {v5}, Landroid/widget/TimePicker;->access$400(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;

    #@c
    move-result-object v5

    #@d
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getMinValue()I

    #@10
    move-result v1

    #@11
    .line 180
    .local v1, minValue:I
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@13
    invoke-static {v5}, Landroid/widget/TimePicker;->access$400(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getMaxValue()I

    #@1a
    move-result v0

    #@1b
    .line 181
    .local v0, maxValue:I
    if-ne p2, v0, :cond_5a

    #@1d
    if-ne p3, v1, :cond_5a

    #@1f
    .line 182
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@21
    invoke-static {v5}, Landroid/widget/TimePicker;->access$500(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getValue()I

    #@28
    move-result v5

    #@29
    add-int/lit8 v2, v5, 0x1

    #@2b
    .line 183
    .local v2, newHour:I
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@2d
    invoke-virtual {v5}, Landroid/widget/TimePicker;->is24HourView()Z

    #@30
    move-result v5

    #@31
    if-nez v5, :cond_49

    #@33
    const/16 v5, 0xc

    #@35
    if-ne v2, v5, :cond_49

    #@37
    .line 184
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@39
    iget-object v6, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@3b
    invoke-static {v6}, Landroid/widget/TimePicker;->access$100(Landroid/widget/TimePicker;)Z

    #@3e
    move-result v6

    #@3f
    if-nez v6, :cond_58

    #@41
    :goto_41
    invoke-static {v5, v3}, Landroid/widget/TimePicker;->access$102(Landroid/widget/TimePicker;Z)Z

    #@44
    .line 185
    iget-object v3, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@46
    invoke-static {v3}, Landroid/widget/TimePicker;->access$200(Landroid/widget/TimePicker;)V

    #@49
    .line 187
    :cond_49
    iget-object v3, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@4b
    invoke-static {v3}, Landroid/widget/TimePicker;->access$500(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3, v2}, Landroid/widget/NumberPicker;->setValue(I)V

    #@52
    .line 196
    .end local v2           #newHour:I
    :cond_52
    :goto_52
    iget-object v3, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@54
    invoke-static {v3}, Landroid/widget/TimePicker;->access$300(Landroid/widget/TimePicker;)V

    #@57
    .line 197
    return-void

    #@58
    .restart local v2       #newHour:I
    :cond_58
    move v3, v4

    #@59
    .line 184
    goto :goto_41

    #@5a
    .line 188
    .end local v2           #newHour:I
    :cond_5a
    if-ne p2, v1, :cond_52

    #@5c
    if-ne p3, v0, :cond_52

    #@5e
    .line 189
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@60
    invoke-static {v5}, Landroid/widget/TimePicker;->access$500(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5}, Landroid/widget/NumberPicker;->getValue()I

    #@67
    move-result v5

    #@68
    add-int/lit8 v2, v5, -0x1

    #@6a
    .line 190
    .restart local v2       #newHour:I
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@6c
    invoke-virtual {v5}, Landroid/widget/TimePicker;->is24HourView()Z

    #@6f
    move-result v5

    #@70
    if-nez v5, :cond_88

    #@72
    const/16 v5, 0xb

    #@74
    if-ne v2, v5, :cond_88

    #@76
    .line 191
    iget-object v5, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@78
    iget-object v6, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@7a
    invoke-static {v6}, Landroid/widget/TimePicker;->access$100(Landroid/widget/TimePicker;)Z

    #@7d
    move-result v6

    #@7e
    if-nez v6, :cond_92

    #@80
    :goto_80
    invoke-static {v5, v3}, Landroid/widget/TimePicker;->access$102(Landroid/widget/TimePicker;Z)Z

    #@83
    .line 192
    iget-object v3, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@85
    invoke-static {v3}, Landroid/widget/TimePicker;->access$200(Landroid/widget/TimePicker;)V

    #@88
    .line 194
    :cond_88
    iget-object v3, p0, Landroid/widget/TimePicker$3;->this$0:Landroid/widget/TimePicker;

    #@8a
    invoke-static {v3}, Landroid/widget/TimePicker;->access$500(Landroid/widget/TimePicker;)Landroid/widget/NumberPicker;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v3, v2}, Landroid/widget/NumberPicker;->setValue(I)V

    #@91
    goto :goto_52

    #@92
    :cond_92
    move v3, v4

    #@93
    .line 191
    goto :goto_80
.end method
