.class final Landroid/widget/GridLayout$Axis;
.super Ljava/lang/Object;
.source "GridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/GridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Axis"
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final COMPLETE:I = 0x2

.field private static final NEW:I = 0x0

.field private static final PENDING:I = 0x1


# instance fields
.field public arcs:[Landroid/widget/GridLayout$Arc;

.field public arcsValid:Z

.field backwardLinks:Landroid/widget/GridLayout$PackedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;"
        }
    .end annotation
.end field

.field public backwardLinksValid:Z

.field public definedCount:I

.field forwardLinks:Landroid/widget/GridLayout$PackedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;"
        }
    .end annotation
.end field

.field public forwardLinksValid:Z

.field groupBounds:Landroid/widget/GridLayout$PackedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Spec;",
            "Landroid/widget/GridLayout$Bounds;",
            ">;"
        }
    .end annotation
.end field

.field public groupBoundsValid:Z

.field public final horizontal:Z

.field public leadingMargins:[I

.field public leadingMarginsValid:Z

.field public locations:[I

.field public locationsValid:Z

.field private maxIndex:I

.field orderPreserved:Z

.field private parentMax:Landroid/widget/GridLayout$MutableInt;

.field private parentMin:Landroid/widget/GridLayout$MutableInt;

.field final synthetic this$0:Landroid/widget/GridLayout;

.field public trailingMargins:[I

.field public trailingMarginsValid:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1162
    const-class v0, Landroid/widget/GridLayout;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/widget/GridLayout$Axis;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method private constructor <init>(Landroid/widget/GridLayout;Z)V
    .registers 5
    .parameter
    .parameter "horizontal"

    #@0
    .prologue
    const/high16 v0, -0x8000

    #@2
    const/4 v1, 0x0

    #@3
    .line 1198
    iput-object p1, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 1169
    iput v0, p0, Landroid/widget/GridLayout$Axis;->definedCount:I

    #@a
    .line 1170
    iput v0, p0, Landroid/widget/GridLayout$Axis;->maxIndex:I

    #@c
    .line 1173
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->groupBoundsValid:Z

    #@e
    .line 1176
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->forwardLinksValid:Z

    #@10
    .line 1179
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->backwardLinksValid:Z

    #@12
    .line 1182
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->leadingMarginsValid:Z

    #@14
    .line 1185
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->trailingMarginsValid:Z

    #@16
    .line 1188
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->arcsValid:Z

    #@18
    .line 1191
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->locationsValid:Z

    #@1a
    .line 1193
    const/4 v0, 0x1

    #@1b
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->orderPreserved:Z

    #@1d
    .line 1195
    new-instance v0, Landroid/widget/GridLayout$MutableInt;

    #@1f
    invoke-direct {v0, v1}, Landroid/widget/GridLayout$MutableInt;-><init>(I)V

    #@22
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->parentMin:Landroid/widget/GridLayout$MutableInt;

    #@24
    .line 1196
    new-instance v0, Landroid/widget/GridLayout$MutableInt;

    #@26
    const v1, -0x186a0

    #@29
    invoke-direct {v0, v1}, Landroid/widget/GridLayout$MutableInt;-><init>(I)V

    #@2c
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->parentMax:Landroid/widget/GridLayout$MutableInt;

    #@2e
    .line 1199
    iput-boolean p2, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@30
    .line 1200
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/GridLayout;ZLandroid/widget/GridLayout$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1162
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout$Axis;-><init>(Landroid/widget/GridLayout;Z)V

    #@3
    return-void
.end method

.method private addComponentSizes(Ljava/util/List;Landroid/widget/GridLayout$PackedMap;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/GridLayout$Arc;",
            ">;",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1425
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    .local p2, links:Landroid/widget/GridLayout$PackedMap;,"Landroid/widget/GridLayout$PackedMap<Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p2, Landroid/widget/GridLayout$PackedMap;->keys:[Ljava/lang/Object;

    #@3
    check-cast v2, [Landroid/widget/GridLayout$Interval;

    #@5
    array-length v2, v2

    #@6
    if-ge v0, v2, :cond_1b

    #@8
    .line 1426
    iget-object v2, p2, Landroid/widget/GridLayout$PackedMap;->keys:[Ljava/lang/Object;

    #@a
    check-cast v2, [Landroid/widget/GridLayout$Interval;

    #@c
    aget-object v1, v2, v0

    #@e
    .line 1427
    .local v1, key:Landroid/widget/GridLayout$Interval;
    iget-object v2, p2, Landroid/widget/GridLayout$PackedMap;->values:[Ljava/lang/Object;

    #@10
    check-cast v2, [Landroid/widget/GridLayout$MutableInt;

    #@12
    aget-object v2, v2, v0

    #@14
    const/4 v3, 0x0

    #@15
    invoke-direct {p0, p1, v1, v2, v3}, Landroid/widget/GridLayout$Axis;->include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;Z)V

    #@18
    .line 1425
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 1429
    .end local v1           #key:Landroid/widget/GridLayout$Interval;
    :cond_1b
    return-void
.end method

.method private arcsToString(Ljava/util/List;)Ljava/lang/String;
    .registers 12
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/GridLayout$Arc;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 1499
    .local p1, arcs:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    iget-boolean v8, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@2
    if-eqz v8, :cond_5b

    #@4
    const-string/jumbo v7, "x"

    #@7
    .line 1500
    .local v7, var:Ljava/lang/String;
    :goto_7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    .line 1501
    .local v4, result:Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    #@d
    .line 1502
    .local v2, first:Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v3

    #@11
    .local v3, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v8

    #@15
    if-eqz v8, :cond_91

    #@17
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/widget/GridLayout$Arc;

    #@1d
    .line 1503
    .local v0, arc:Landroid/widget/GridLayout$Arc;
    if-eqz v2, :cond_5f

    #@1f
    .line 1504
    const/4 v2, 0x0

    #@20
    .line 1508
    :goto_20
    iget-object v8, v0, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@22
    iget v5, v8, Landroid/widget/GridLayout$Interval;->min:I

    #@24
    .line 1509
    .local v5, src:I
    iget-object v8, v0, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@26
    iget v1, v8, Landroid/widget/GridLayout$Interval;->max:I

    #@28
    .line 1510
    .local v1, dst:I
    iget-object v8, v0, Landroid/widget/GridLayout$Arc;->value:Landroid/widget/GridLayout$MutableInt;

    #@2a
    iget v6, v8, Landroid/widget/GridLayout$MutableInt;->value:I

    #@2c
    .line 1511
    .local v6, value:I
    if-ge v5, v1, :cond_66

    #@2e
    new-instance v8, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    const-string v9, "-"

    #@3d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    const-string v9, ">="

    #@4b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    :goto_57
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    goto :goto_11

    #@5b
    .line 1499
    .end local v0           #arc:Landroid/widget/GridLayout$Arc;
    .end local v1           #dst:I
    .end local v2           #first:Z
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #result:Ljava/lang/StringBuilder;
    .end local v5           #src:I
    .end local v6           #value:I
    .end local v7           #var:Ljava/lang/String;
    :cond_5b
    const-string/jumbo v7, "y"

    #@5e
    goto :goto_7

    #@5f
    .line 1506
    .restart local v0       #arc:Landroid/widget/GridLayout$Arc;
    .restart local v2       #first:Z
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #result:Ljava/lang/StringBuilder;
    .restart local v7       #var:Ljava/lang/String;
    :cond_5f
    const-string v8, ", "

    #@61
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    goto :goto_20

    #@66
    .line 1511
    .restart local v1       #dst:I
    .restart local v5       #src:I
    .restart local v6       #value:I
    :cond_66
    new-instance v8, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v8

    #@6f
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v8

    #@73
    const-string v9, "-"

    #@75
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v8

    #@81
    const-string v9, "<="

    #@83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    neg-int v9, v6

    #@88
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v8

    #@8c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v8

    #@90
    goto :goto_57

    #@91
    .line 1516
    .end local v0           #arc:Landroid/widget/GridLayout$Arc;
    .end local v1           #dst:I
    .end local v5           #src:I
    .end local v6           #value:I
    :cond_91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v8

    #@95
    return-object v8
.end method

.method private calculateMaxIndex()I
    .registers 9

    #@0
    .prologue
    .line 1204
    const/4 v4, -0x1

    #@1
    .line 1205
    .local v4, result:I
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@4
    invoke-virtual {v7}, Landroid/widget/GridLayout;->getChildCount()I

    #@7
    move-result v0

    #@8
    .local v0, N:I
    :goto_8
    if-ge v2, v0, :cond_38

    #@a
    .line 1206
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@c
    invoke-virtual {v7, v2}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@f
    move-result-object v1

    #@10
    .line 1207
    .local v1, c:Landroid/view/View;
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@12
    invoke-virtual {v7, v1}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@15
    move-result-object v3

    #@16
    .line 1208
    .local v3, params:Landroid/widget/GridLayout$LayoutParams;
    iget-boolean v7, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@18
    if-eqz v7, :cond_35

    #@1a
    iget-object v6, v3, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@1c
    .line 1209
    .local v6, spec:Landroid/widget/GridLayout$Spec;
    :goto_1c
    iget-object v5, v6, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@1e
    .line 1210
    .local v5, span:Landroid/widget/GridLayout$Interval;
    iget v7, v5, Landroid/widget/GridLayout$Interval;->min:I

    #@20
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    #@23
    move-result v4

    #@24
    .line 1211
    iget v7, v5, Landroid/widget/GridLayout$Interval;->max:I

    #@26
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    #@29
    move-result v4

    #@2a
    .line 1212
    invoke-virtual {v5}, Landroid/widget/GridLayout$Interval;->size()I

    #@2d
    move-result v7

    #@2e
    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    #@31
    move-result v4

    #@32
    .line 1205
    add-int/lit8 v2, v2, 0x1

    #@34
    goto :goto_8

    #@35
    .line 1208
    .end local v5           #span:Landroid/widget/GridLayout$Interval;
    .end local v6           #spec:Landroid/widget/GridLayout$Spec;
    :cond_35
    iget-object v6, v3, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@37
    goto :goto_1c

    #@38
    .line 1214
    .end local v1           #c:Landroid/view/View;
    .end local v3           #params:Landroid/widget/GridLayout$LayoutParams;
    :cond_38
    const/4 v7, -0x1

    #@39
    if-ne v4, v7, :cond_3d

    #@3b
    const/high16 v4, -0x8000

    #@3d
    .end local v4           #result:I
    :cond_3d
    return v4
.end method

.method private computeArcs()V
    .registers 1

    #@0
    .prologue
    .line 1463
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->getForwardLinks()Landroid/widget/GridLayout$PackedMap;

    #@3
    .line 1464
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->getBackwardLinks()Landroid/widget/GridLayout$PackedMap;

    #@6
    .line 1465
    return-void
.end method

.method private computeGroupBounds()V
    .registers 9

    #@0
    .prologue
    .line 1259
    iget-object v6, p0, Landroid/widget/GridLayout$Axis;->groupBounds:Landroid/widget/GridLayout$PackedMap;

    #@2
    iget-object v5, v6, Landroid/widget/GridLayout$PackedMap;->values:[Ljava/lang/Object;

    #@4
    check-cast v5, [Landroid/widget/GridLayout$Bounds;

    #@6
    .line 1260
    .local v5, values:[Landroid/widget/GridLayout$Bounds;
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    array-length v6, v5

    #@8
    if-ge v2, v6, :cond_12

    #@a
    .line 1261
    aget-object v6, v5, v2

    #@c
    invoke-virtual {v6}, Landroid/widget/GridLayout$Bounds;->reset()V

    #@f
    .line 1260
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_7

    #@12
    .line 1263
    :cond_12
    const/4 v2, 0x0

    #@13
    iget-object v6, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@15
    invoke-virtual {v6}, Landroid/widget/GridLayout;->getChildCount()I

    #@18
    move-result v0

    #@19
    .local v0, N:I
    :goto_19
    if-ge v2, v0, :cond_40

    #@1b
    .line 1264
    iget-object v6, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@1d
    invoke-virtual {v6, v2}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@20
    move-result-object v1

    #@21
    .line 1265
    .local v1, c:Landroid/view/View;
    iget-object v6, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@23
    invoke-virtual {v6, v1}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@26
    move-result-object v3

    #@27
    .line 1266
    .local v3, lp:Landroid/widget/GridLayout$LayoutParams;
    iget-boolean v6, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@29
    if-eqz v6, :cond_3d

    #@2b
    iget-object v4, v3, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@2d
    .line 1267
    .local v4, spec:Landroid/widget/GridLayout$Spec;
    :goto_2d
    iget-object v6, p0, Landroid/widget/GridLayout$Axis;->groupBounds:Landroid/widget/GridLayout$PackedMap;

    #@2f
    invoke-virtual {v6, v2}, Landroid/widget/GridLayout$PackedMap;->getValue(I)Ljava/lang/Object;

    #@32
    move-result-object v6

    #@33
    check-cast v6, Landroid/widget/GridLayout$Bounds;

    #@35
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@37
    invoke-virtual {v6, v7, v1, v4, p0}, Landroid/widget/GridLayout$Bounds;->include(Landroid/widget/GridLayout;Landroid/view/View;Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Axis;)V

    #@3a
    .line 1263
    add-int/lit8 v2, v2, 0x1

    #@3c
    goto :goto_19

    #@3d
    .line 1266
    .end local v4           #spec:Landroid/widget/GridLayout$Spec;
    :cond_3d
    iget-object v4, v3, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@3f
    goto :goto_2d

    #@40
    .line 1269
    .end local v1           #c:Landroid/view/View;
    .end local v3           #lp:Landroid/widget/GridLayout$LayoutParams;
    :cond_40
    return-void
.end method

.method private computeLinks(Landroid/widget/GridLayout$PackedMap;Z)V
    .registers 9
    .parameter
    .parameter "min"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1294
    .local p1, links:Landroid/widget/GridLayout$PackedMap;,"Landroid/widget/GridLayout$PackedMap<Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;>;"
    iget-object v3, p1, Landroid/widget/GridLayout$PackedMap;->values:[Ljava/lang/Object;

    #@2
    check-cast v3, [Landroid/widget/GridLayout$MutableInt;

    #@4
    .line 1295
    .local v3, spans:[Landroid/widget/GridLayout$MutableInt;
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    array-length v5, v3

    #@6
    if-ge v1, v5, :cond_10

    #@8
    .line 1296
    aget-object v5, v3, v1

    #@a
    invoke-virtual {v5}, Landroid/widget/GridLayout$MutableInt;->reset()V

    #@d
    .line 1295
    add-int/lit8 v1, v1, 0x1

    #@f
    goto :goto_5

    #@10
    .line 1300
    :cond_10
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getGroupBounds()Landroid/widget/GridLayout$PackedMap;

    #@13
    move-result-object v5

    #@14
    iget-object v0, v5, Landroid/widget/GridLayout$PackedMap;->values:[Ljava/lang/Object;

    #@16
    check-cast v0, [Landroid/widget/GridLayout$Bounds;

    #@18
    .line 1301
    .local v0, bounds:[Landroid/widget/GridLayout$Bounds;
    const/4 v1, 0x0

    #@19
    :goto_19
    array-length v5, v0

    #@1a
    if-ge v1, v5, :cond_37

    #@1c
    .line 1302
    aget-object v5, v0, v1

    #@1e
    invoke-virtual {v5, p2}, Landroid/widget/GridLayout$Bounds;->size(Z)I

    #@21
    move-result v2

    #@22
    .line 1303
    .local v2, size:I
    invoke-virtual {p1, v1}, Landroid/widget/GridLayout$PackedMap;->getValue(I)Ljava/lang/Object;

    #@25
    move-result-object v4

    #@26
    check-cast v4, Landroid/widget/GridLayout$MutableInt;

    #@28
    .line 1305
    .local v4, valueHolder:Landroid/widget/GridLayout$MutableInt;
    iget v5, v4, Landroid/widget/GridLayout$MutableInt;->value:I

    #@2a
    if-eqz p2, :cond_35

    #@2c
    .end local v2           #size:I
    :goto_2c
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    #@2f
    move-result v5

    #@30
    iput v5, v4, Landroid/widget/GridLayout$MutableInt;->value:I

    #@32
    .line 1301
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_19

    #@35
    .line 1305
    .restart local v2       #size:I
    :cond_35
    neg-int v2, v2

    #@36
    goto :goto_2c

    #@37
    .line 1307
    .end local v2           #size:I
    .end local v4           #valueHolder:Landroid/widget/GridLayout$MutableInt;
    :cond_37
    return-void
.end method

.method private computeLocations([I)V
    .registers 6
    .parameter "a"

    #@0
    .prologue
    .line 1640
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getArcs()[Landroid/widget/GridLayout$Arc;

    #@3
    move-result-object v3

    #@4
    invoke-direct {p0, v3, p1}, Landroid/widget/GridLayout$Axis;->solve([Landroid/widget/GridLayout$Arc;[I)V

    #@7
    .line 1641
    iget-boolean v3, p0, Landroid/widget/GridLayout$Axis;->orderPreserved:Z

    #@9
    if-nez v3, :cond_1a

    #@b
    .line 1648
    const/4 v3, 0x0

    #@c
    aget v1, p1, v3

    #@e
    .line 1649
    .local v1, a0:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    array-length v0, p1

    #@10
    .local v0, N:I
    :goto_10
    if-ge v2, v0, :cond_1a

    #@12
    .line 1650
    aget v3, p1, v2

    #@14
    sub-int/2addr v3, v1

    #@15
    aput v3, p1, v2

    #@17
    .line 1649
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_10

    #@1a
    .line 1653
    .end local v0           #N:I
    .end local v1           #a0:I
    .end local v2           #i:I
    :cond_1a
    return-void
.end method

.method private computeMargins(Z)V
    .registers 13
    .parameter "leading"

    #@0
    .prologue
    .line 1603
    if-eqz p1, :cond_1e

    #@2
    iget-object v5, p0, Landroid/widget/GridLayout$Axis;->leadingMargins:[I

    #@4
    .line 1604
    .local v5, margins:[I
    :goto_4
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    iget-object v8, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@7
    invoke-virtual {v8}, Landroid/widget/GridLayout;->getChildCount()I

    #@a
    move-result v0

    #@b
    .local v0, N:I
    :goto_b
    if-ge v2, v0, :cond_4a

    #@d
    .line 1605
    iget-object v8, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@f
    invoke-virtual {v8, v2}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v1

    #@13
    .line 1606
    .local v1, c:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@16
    move-result v8

    #@17
    const/16 v9, 0x8

    #@19
    if-ne v8, v9, :cond_21

    #@1b
    .line 1604
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_b

    #@1e
    .line 1603
    .end local v0           #N:I
    .end local v1           #c:Landroid/view/View;
    .end local v2           #i:I
    .end local v5           #margins:[I
    :cond_1e
    iget-object v5, p0, Landroid/widget/GridLayout$Axis;->trailingMargins:[I

    #@20
    goto :goto_4

    #@21
    .line 1607
    .restart local v0       #N:I
    .restart local v1       #c:Landroid/view/View;
    .restart local v2       #i:I
    .restart local v5       #margins:[I
    :cond_21
    iget-object v8, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@23
    invoke-virtual {v8, v1}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@26
    move-result-object v4

    #@27
    .line 1608
    .local v4, lp:Landroid/widget/GridLayout$LayoutParams;
    iget-boolean v8, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@29
    if-eqz v8, :cond_44

    #@2b
    iget-object v7, v4, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@2d
    .line 1609
    .local v7, spec:Landroid/widget/GridLayout$Spec;
    :goto_2d
    iget-object v6, v7, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@2f
    .line 1610
    .local v6, span:Landroid/widget/GridLayout$Interval;
    if-eqz p1, :cond_47

    #@31
    iget v3, v6, Landroid/widget/GridLayout$Interval;->min:I

    #@33
    .line 1611
    .local v3, index:I
    :goto_33
    aget v8, v5, v3

    #@35
    iget-object v9, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@37
    iget-boolean v10, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@39
    invoke-virtual {v9, v1, v10, p1}, Landroid/widget/GridLayout;->getMargin1(Landroid/view/View;ZZ)I

    #@3c
    move-result v9

    #@3d
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@40
    move-result v8

    #@41
    aput v8, v5, v3

    #@43
    goto :goto_1b

    #@44
    .line 1608
    .end local v3           #index:I
    .end local v6           #span:Landroid/widget/GridLayout$Interval;
    .end local v7           #spec:Landroid/widget/GridLayout$Spec;
    :cond_44
    iget-object v7, v4, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@46
    goto :goto_2d

    #@47
    .line 1610
    .restart local v6       #span:Landroid/widget/GridLayout$Interval;
    .restart local v7       #spec:Landroid/widget/GridLayout$Spec;
    :cond_47
    iget v3, v6, Landroid/widget/GridLayout$Interval;->max:I

    #@49
    goto :goto_33

    #@4a
    .line 1613
    .end local v1           #c:Landroid/view/View;
    .end local v4           #lp:Landroid/widget/GridLayout$LayoutParams;
    .end local v6           #span:Landroid/widget/GridLayout$Interval;
    .end local v7           #spec:Landroid/widget/GridLayout$Spec;
    :cond_4a
    return-void
.end method

.method private createArcs()[Landroid/widget/GridLayout$Arc;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1432
    new-instance v3, Ljava/util/ArrayList;

    #@3
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 1433
    .local v3, mins:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    new-instance v2, Ljava/util/ArrayList;

    #@8
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 1436
    .local v2, maxs:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->getForwardLinks()Landroid/widget/GridLayout$PackedMap;

    #@e
    move-result-object v6

    #@f
    invoke-direct {p0, v3, v6}, Landroid/widget/GridLayout$Axis;->addComponentSizes(Ljava/util/List;Landroid/widget/GridLayout$PackedMap;)V

    #@12
    .line 1438
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->getBackwardLinks()Landroid/widget/GridLayout$PackedMap;

    #@15
    move-result-object v6

    #@16
    invoke-direct {p0, v2, v6}, Landroid/widget/GridLayout$Axis;->addComponentSizes(Ljava/util/List;Landroid/widget/GridLayout$PackedMap;)V

    #@19
    .line 1441
    iget-boolean v6, p0, Landroid/widget/GridLayout$Axis;->orderPreserved:Z

    #@1b
    if-eqz v6, :cond_36

    #@1d
    .line 1443
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@21
    move-result v6

    #@22
    if-ge v1, v6, :cond_36

    #@24
    .line 1444
    new-instance v6, Landroid/widget/GridLayout$Interval;

    #@26
    add-int/lit8 v7, v1, 0x1

    #@28
    invoke-direct {v6, v1, v7}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@2b
    new-instance v7, Landroid/widget/GridLayout$MutableInt;

    #@2d
    invoke-direct {v7, v8}, Landroid/widget/GridLayout$MutableInt;-><init>(I)V

    #@30
    invoke-direct {p0, v3, v6, v7}, Landroid/widget/GridLayout$Axis;->include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;)V

    #@33
    .line 1443
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_1e

    #@36
    .line 1450
    .end local v1           #i:I
    :cond_36
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@39
    move-result v0

    #@3a
    .line 1451
    .local v0, N:I
    new-instance v6, Landroid/widget/GridLayout$Interval;

    #@3c
    invoke-direct {v6, v8, v0}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@3f
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->parentMin:Landroid/widget/GridLayout$MutableInt;

    #@41
    invoke-direct {p0, v3, v6, v7, v8}, Landroid/widget/GridLayout$Axis;->include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;Z)V

    #@44
    .line 1452
    new-instance v6, Landroid/widget/GridLayout$Interval;

    #@46
    invoke-direct {v6, v0, v8}, Landroid/widget/GridLayout$Interval;-><init>(II)V

    #@49
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->parentMax:Landroid/widget/GridLayout$MutableInt;

    #@4b
    invoke-direct {p0, v2, v6, v7, v8}, Landroid/widget/GridLayout$Axis;->include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;Z)V

    #@4e
    .line 1455
    invoke-direct {p0, v3}, Landroid/widget/GridLayout$Axis;->topologicalSort(Ljava/util/List;)[Landroid/widget/GridLayout$Arc;

    #@51
    move-result-object v5

    #@52
    .line 1456
    .local v5, sMins:[Landroid/widget/GridLayout$Arc;
    invoke-direct {p0, v2}, Landroid/widget/GridLayout$Axis;->topologicalSort(Ljava/util/List;)[Landroid/widget/GridLayout$Arc;

    #@55
    move-result-object v4

    #@56
    .line 1458
    .local v4, sMaxs:[Landroid/widget/GridLayout$Arc;
    invoke-static {v5, v4}, Landroid/widget/GridLayout;->append([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    #@59
    move-result-object v6

    #@5a
    check-cast v6, [Landroid/widget/GridLayout$Arc;

    #@5c
    return-object v6
.end method

.method private createGroupBounds()Landroid/widget/GridLayout$PackedMap;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Spec;",
            "Landroid/widget/GridLayout$Bounds;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1247
    const-class v7, Landroid/widget/GridLayout$Spec;

    #@2
    const-class v8, Landroid/widget/GridLayout$Bounds;

    #@4
    invoke-static {v7, v8}, Landroid/widget/GridLayout$Assoc;->of(Ljava/lang/Class;Ljava/lang/Class;)Landroid/widget/GridLayout$Assoc;

    #@7
    move-result-object v1

    #@8
    .line 1248
    .local v1, assoc:Landroid/widget/GridLayout$Assoc;,"Landroid/widget/GridLayout$Assoc<Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Bounds;>;"
    const/4 v4, 0x0

    #@9
    .local v4, i:I
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@b
    invoke-virtual {v7}, Landroid/widget/GridLayout;->getChildCount()I

    #@e
    move-result v0

    #@f
    .local v0, N:I
    :goto_f
    if-ge v4, v0, :cond_3a

    #@11
    .line 1249
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@13
    invoke-virtual {v7, v4}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    #@16
    move-result-object v3

    #@17
    .line 1250
    .local v3, c:Landroid/view/View;
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@19
    invoke-virtual {v7, v3}, Landroid/widget/GridLayout;->getLayoutParams(Landroid/view/View;)Landroid/widget/GridLayout$LayoutParams;

    #@1c
    move-result-object v5

    #@1d
    .line 1251
    .local v5, lp:Landroid/widget/GridLayout$LayoutParams;
    iget-boolean v7, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@1f
    if-eqz v7, :cond_37

    #@21
    iget-object v6, v5, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    #@23
    .line 1252
    .local v6, spec:Landroid/widget/GridLayout$Spec;
    :goto_23
    iget-object v7, p0, Landroid/widget/GridLayout$Axis;->this$0:Landroid/widget/GridLayout;

    #@25
    iget-object v8, v6, Landroid/widget/GridLayout$Spec;->alignment:Landroid/widget/GridLayout$Alignment;

    #@27
    iget-boolean v9, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@29
    invoke-virtual {v7, v8, v9}, Landroid/widget/GridLayout;->getAlignment(Landroid/widget/GridLayout$Alignment;Z)Landroid/widget/GridLayout$Alignment;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7}, Landroid/widget/GridLayout$Alignment;->getBounds()Landroid/widget/GridLayout$Bounds;

    #@30
    move-result-object v2

    #@31
    .line 1253
    .local v2, bounds:Landroid/widget/GridLayout$Bounds;
    invoke-virtual {v1, v6, v2}, Landroid/widget/GridLayout$Assoc;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    #@34
    .line 1248
    add-int/lit8 v4, v4, 0x1

    #@36
    goto :goto_f

    #@37
    .line 1251
    .end local v2           #bounds:Landroid/widget/GridLayout$Bounds;
    .end local v6           #spec:Landroid/widget/GridLayout$Spec;
    :cond_37
    iget-object v6, v5, Landroid/widget/GridLayout$LayoutParams;->rowSpec:Landroid/widget/GridLayout$Spec;

    #@39
    goto :goto_23

    #@3a
    .line 1255
    .end local v3           #c:Landroid/view/View;
    .end local v5           #lp:Landroid/widget/GridLayout$LayoutParams;
    :cond_3a
    invoke-virtual {v1}, Landroid/widget/GridLayout$Assoc;->pack()Landroid/widget/GridLayout$PackedMap;

    #@3d
    move-result-object v7

    #@3e
    return-object v7
.end method

.method private createLinks(Z)Landroid/widget/GridLayout$PackedMap;
    .registers 9
    .parameter "min"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1284
    const-class v5, Landroid/widget/GridLayout$Interval;

    #@2
    const-class v6, Landroid/widget/GridLayout$MutableInt;

    #@4
    invoke-static {v5, v6}, Landroid/widget/GridLayout$Assoc;->of(Ljava/lang/Class;Ljava/lang/Class;)Landroid/widget/GridLayout$Assoc;

    #@7
    move-result-object v3

    #@8
    .line 1285
    .local v3, result:Landroid/widget/GridLayout$Assoc;,"Landroid/widget/GridLayout$Assoc<Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;>;"
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getGroupBounds()Landroid/widget/GridLayout$PackedMap;

    #@b
    move-result-object v5

    #@c
    iget-object v2, v5, Landroid/widget/GridLayout$PackedMap;->keys:[Ljava/lang/Object;

    #@e
    check-cast v2, [Landroid/widget/GridLayout$Spec;

    #@10
    .line 1286
    .local v2, keys:[Landroid/widget/GridLayout$Spec;
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    array-length v0, v2

    #@12
    .local v0, N:I
    :goto_12
    if-ge v1, v0, :cond_2e

    #@14
    .line 1287
    if-eqz p1, :cond_25

    #@16
    aget-object v5, v2, v1

    #@18
    iget-object v4, v5, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@1a
    .line 1288
    .local v4, span:Landroid/widget/GridLayout$Interval;
    :goto_1a
    new-instance v5, Landroid/widget/GridLayout$MutableInt;

    #@1c
    invoke-direct {v5}, Landroid/widget/GridLayout$MutableInt;-><init>()V

    #@1f
    invoke-virtual {v3, v4, v5}, Landroid/widget/GridLayout$Assoc;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    #@22
    .line 1286
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_12

    #@25
    .line 1287
    .end local v4           #span:Landroid/widget/GridLayout$Interval;
    :cond_25
    aget-object v5, v2, v1

    #@27
    iget-object v5, v5, Landroid/widget/GridLayout$Spec;->span:Landroid/widget/GridLayout$Interval;

    #@29
    invoke-virtual {v5}, Landroid/widget/GridLayout$Interval;->inverse()Landroid/widget/GridLayout$Interval;

    #@2c
    move-result-object v4

    #@2d
    goto :goto_1a

    #@2e
    .line 1290
    :cond_2e
    invoke-virtual {v3}, Landroid/widget/GridLayout$Assoc;->pack()Landroid/widget/GridLayout$PackedMap;

    #@31
    move-result-object v5

    #@32
    return-object v5
.end method

.method private getBackwardLinks()Landroid/widget/GridLayout$PackedMap;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1321
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@3
    if-nez v0, :cond_b

    #@5
    .line 1322
    invoke-direct {p0, v1}, Landroid/widget/GridLayout$Axis;->createLinks(Z)Landroid/widget/GridLayout$PackedMap;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@b
    .line 1324
    :cond_b
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinksValid:Z

    #@d
    if-nez v0, :cond_17

    #@f
    .line 1325
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@11
    invoke-direct {p0, v0, v1}, Landroid/widget/GridLayout$Axis;->computeLinks(Landroid/widget/GridLayout$PackedMap;Z)V

    #@14
    .line 1326
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinksValid:Z

    #@17
    .line 1328
    :cond_17
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@19
    return-object v0
.end method

.method private getForwardLinks()Landroid/widget/GridLayout$PackedMap;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1310
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->forwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@3
    if-nez v0, :cond_b

    #@5
    .line 1311
    invoke-direct {p0, v1}, Landroid/widget/GridLayout$Axis;->createLinks(Z)Landroid/widget/GridLayout$PackedMap;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->forwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@b
    .line 1313
    :cond_b
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->forwardLinksValid:Z

    #@d
    if-nez v0, :cond_16

    #@f
    .line 1314
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->forwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@11
    invoke-direct {p0, v0, v1}, Landroid/widget/GridLayout$Axis;->computeLinks(Landroid/widget/GridLayout$PackedMap;Z)V

    #@14
    .line 1315
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->forwardLinksValid:Z

    #@16
    .line 1317
    :cond_16
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->forwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@18
    return-object v0
.end method

.method private getMaxIndex()I
    .registers 3

    #@0
    .prologue
    .line 1218
    iget v0, p0, Landroid/widget/GridLayout$Axis;->maxIndex:I

    #@2
    const/high16 v1, -0x8000

    #@4
    if-ne v0, v1, :cond_11

    #@6
    .line 1219
    const/4 v0, 0x0

    #@7
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->calculateMaxIndex()I

    #@a
    move-result v1

    #@b
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/widget/GridLayout$Axis;->maxIndex:I

    #@11
    .line 1221
    :cond_11
    iget v0, p0, Landroid/widget/GridLayout$Axis;->maxIndex:I

    #@13
    return v0
.end method

.method private getMeasure(II)I
    .registers 4
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 1681
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout$Axis;->setParentConstraints(II)V

    #@3
    .line 1682
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getLocations()[I

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/widget/GridLayout$Axis;->size([I)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method private include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;)V
    .registers 5
    .parameter
    .parameter "key"
    .parameter "size"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/GridLayout$Arc;",
            ">;",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 1356
    .local p1, arcs:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/GridLayout$Axis;->include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;Z)V

    #@4
    .line 1357
    return-void
.end method

.method private include(Ljava/util/List;Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;Z)V
    .registers 9
    .parameter
    .parameter "key"
    .parameter "size"
    .parameter "ignoreIfAlreadyPresent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/GridLayout$Arc;",
            ">;",
            "Landroid/widget/GridLayout$Interval;",
            "Landroid/widget/GridLayout$MutableInt;",
            "Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1339
    .local p1, arcs:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    invoke-virtual {p2}, Landroid/widget/GridLayout$Interval;->size()I

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 1353
    :goto_6
    return-void

    #@7
    .line 1344
    :cond_7
    if-eqz p4, :cond_22

    #@9
    .line 1345
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :cond_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_22

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/GridLayout$Arc;

    #@19
    .line 1346
    .local v0, arc:Landroid/widget/GridLayout$Arc;
    iget-object v2, v0, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@1b
    .line 1347
    .local v2, span:Landroid/widget/GridLayout$Interval;
    invoke-virtual {v2, p2}, Landroid/widget/GridLayout$Interval;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_d

    #@21
    goto :goto_6

    #@22
    .line 1352
    .end local v0           #arc:Landroid/widget/GridLayout$Arc;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #span:Landroid/widget/GridLayout$Interval;
    :cond_22
    new-instance v3, Landroid/widget/GridLayout$Arc;

    #@24
    invoke-direct {v3, p2, p3}, Landroid/widget/GridLayout$Arc;-><init>(Landroid/widget/GridLayout$Interval;Landroid/widget/GridLayout$MutableInt;)V

    #@27
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2a
    goto :goto_6
.end method

.method private init([I)V
    .registers 3
    .parameter "locations"

    #@0
    .prologue
    .line 1495
    const/4 v0, 0x0

    #@1
    invoke-static {p1, v0}, Ljava/util/Arrays;->fill([II)V

    #@4
    .line 1496
    return-void
.end method

.method private logError(Ljava/lang/String;[Landroid/widget/GridLayout$Arc;[Z)V
    .registers 11
    .parameter "axisName"
    .parameter "arcs"
    .parameter "culprits0"

    #@0
    .prologue
    .line 1520
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1521
    .local v2, culprits:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@7
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@a
    .line 1522
    .local v3, removed:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    const/4 v1, 0x0

    #@b
    .local v1, c:I
    :goto_b
    array-length v4, p2

    #@c
    if-ge v1, v4, :cond_21

    #@e
    .line 1523
    aget-object v0, p2, v1

    #@10
    .line 1524
    .local v0, arc:Landroid/widget/GridLayout$Arc;
    aget-boolean v4, p3, v1

    #@12
    if-eqz v4, :cond_17

    #@14
    .line 1525
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@17
    .line 1527
    :cond_17
    iget-boolean v4, v0, Landroid/widget/GridLayout$Arc;->valid:Z

    #@19
    if-nez v4, :cond_1e

    #@1b
    .line 1528
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1e
    .line 1522
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_b

    #@21
    .line 1531
    .end local v0           #arc:Landroid/widget/GridLayout$Arc;
    :cond_21
    sget-object v4, Landroid/widget/GridLayout;->TAG:Ljava/lang/String;

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const-string v6, " constraints: "

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-direct {p0, v2}, Landroid/widget/GridLayout$Axis;->arcsToString(Ljava/util/List;)Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    const-string v6, " are inconsistent; "

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    const-string/jumbo v6, "permanently removing: "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-direct {p0, v3}, Landroid/widget/GridLayout$Axis;->arcsToString(Ljava/util/List;)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    const-string v6, ". "

    #@51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 1533
    return-void
.end method

.method private relax([ILandroid/widget/GridLayout$Arc;)Z
    .registers 10
    .parameter "locations"
    .parameter "entry"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1479
    iget-boolean v6, p2, Landroid/widget/GridLayout$Arc;->valid:Z

    #@3
    if-nez v6, :cond_6

    #@5
    .line 1491
    :cond_5
    :goto_5
    return v5

    #@6
    .line 1482
    :cond_6
    iget-object v1, p2, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@8
    .line 1483
    .local v1, span:Landroid/widget/GridLayout$Interval;
    iget v2, v1, Landroid/widget/GridLayout$Interval;->min:I

    #@a
    .line 1484
    .local v2, u:I
    iget v3, v1, Landroid/widget/GridLayout$Interval;->max:I

    #@c
    .line 1485
    .local v3, v:I
    iget-object v6, p2, Landroid/widget/GridLayout$Arc;->value:Landroid/widget/GridLayout$MutableInt;

    #@e
    iget v4, v6, Landroid/widget/GridLayout$MutableInt;->value:I

    #@10
    .line 1486
    .local v4, value:I
    aget v6, p1, v2

    #@12
    add-int v0, v6, v4

    #@14
    .line 1487
    .local v0, candidate:I
    aget v6, p1, v3

    #@16
    if-le v0, v6, :cond_5

    #@18
    .line 1488
    aput v0, p1, v3

    #@1a
    .line 1489
    const/4 v5, 0x1

    #@1b
    goto :goto_5
.end method

.method private setParentConstraints(II)V
    .registers 5
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 1675
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->parentMin:Landroid/widget/GridLayout$MutableInt;

    #@2
    iput p1, v0, Landroid/widget/GridLayout$MutableInt;->value:I

    #@4
    .line 1676
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->parentMax:Landroid/widget/GridLayout$MutableInt;

    #@6
    neg-int v1, p2

    #@7
    iput v1, v0, Landroid/widget/GridLayout$MutableInt;->value:I

    #@9
    .line 1677
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->locationsValid:Z

    #@c
    .line 1678
    return-void
.end method

.method private size([I)I
    .registers 3
    .parameter "locations"

    #@0
    .prologue
    .line 1671
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@3
    move-result v0

    #@4
    aget v0, p1, v0

    #@6
    return v0
.end method

.method private solve([Landroid/widget/GridLayout$Arc;[I)V
    .registers 15
    .parameter "arcs"
    .parameter "locations"

    #@0
    .prologue
    .line 1556
    iget-boolean v10, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@2
    if-eqz v10, :cond_26

    #@4
    const-string v2, "horizontal"

    #@6
    .line 1557
    .local v2, axisName:Ljava/lang/String;
    :goto_6
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@9
    move-result v10

    #@a
    add-int/lit8 v0, v10, 0x1

    #@c
    .line 1558
    .local v0, N:I
    const/4 v8, 0x0

    #@d
    .line 1560
    .local v8, originalCulprits:[Z
    const/4 v9, 0x0

    #@e
    .local v9, p:I
    :goto_e
    array-length v10, p1

    #@f
    if-ge v9, v10, :cond_31

    #@11
    .line 1561
    invoke-direct {p0, p2}, Landroid/widget/GridLayout$Axis;->init([I)V

    #@14
    .line 1564
    const/4 v5, 0x0

    #@15
    .local v5, i:I
    :goto_15
    if-ge v5, v0, :cond_35

    #@17
    .line 1565
    const/4 v3, 0x0

    #@18
    .line 1566
    .local v3, changed:Z
    const/4 v6, 0x0

    #@19
    .local v6, j:I
    array-length v7, p1

    #@1a
    .local v7, length:I
    :goto_1a
    if-ge v6, v7, :cond_2a

    #@1c
    .line 1567
    aget-object v10, p1, v6

    #@1e
    invoke-direct {p0, p2, v10}, Landroid/widget/GridLayout$Axis;->relax([ILandroid/widget/GridLayout$Arc;)Z

    #@21
    move-result v10

    #@22
    or-int/2addr v3, v10

    #@23
    .line 1566
    add-int/lit8 v6, v6, 0x1

    #@25
    goto :goto_1a

    #@26
    .line 1556
    .end local v0           #N:I
    .end local v2           #axisName:Ljava/lang/String;
    .end local v3           #changed:Z
    .end local v5           #i:I
    .end local v6           #j:I
    .end local v7           #length:I
    .end local v8           #originalCulprits:[Z
    .end local v9           #p:I
    :cond_26
    const-string/jumbo v2, "vertical"

    #@29
    goto :goto_6

    #@2a
    .line 1569
    .restart local v0       #N:I
    .restart local v2       #axisName:Ljava/lang/String;
    .restart local v3       #changed:Z
    .restart local v5       #i:I
    .restart local v6       #j:I
    .restart local v7       #length:I
    .restart local v8       #originalCulprits:[Z
    .restart local v9       #p:I
    :cond_2a
    if-nez v3, :cond_32

    #@2c
    .line 1570
    if-eqz v8, :cond_31

    #@2e
    .line 1571
    invoke-direct {p0, v2, p1, v8}, Landroid/widget/GridLayout$Axis;->logError(Ljava/lang/String;[Landroid/widget/GridLayout$Arc;[Z)V

    #@31
    .line 1600
    .end local v3           #changed:Z
    .end local v5           #i:I
    .end local v6           #j:I
    .end local v7           #length:I
    :cond_31
    return-void

    #@32
    .line 1564
    .restart local v3       #changed:Z
    .restart local v5       #i:I
    .restart local v6       #j:I
    .restart local v7       #length:I
    :cond_32
    add-int/lit8 v5, v5, 0x1

    #@34
    goto :goto_15

    #@35
    .line 1577
    .end local v3           #changed:Z
    .end local v6           #j:I
    .end local v7           #length:I
    :cond_35
    array-length v10, p1

    #@36
    new-array v4, v10, [Z

    #@38
    .line 1578
    .local v4, culprits:[Z
    const/4 v5, 0x0

    #@39
    :goto_39
    if-ge v5, v0, :cond_50

    #@3b
    .line 1579
    const/4 v6, 0x0

    #@3c
    .restart local v6       #j:I
    array-length v7, p1

    #@3d
    .restart local v7       #length:I
    :goto_3d
    if-ge v6, v7, :cond_4d

    #@3f
    .line 1580
    aget-boolean v10, v4, v6

    #@41
    aget-object v11, p1, v6

    #@43
    invoke-direct {p0, p2, v11}, Landroid/widget/GridLayout$Axis;->relax([ILandroid/widget/GridLayout$Arc;)Z

    #@46
    move-result v11

    #@47
    or-int/2addr v10, v11

    #@48
    aput-boolean v10, v4, v6

    #@4a
    .line 1579
    add-int/lit8 v6, v6, 0x1

    #@4c
    goto :goto_3d

    #@4d
    .line 1578
    :cond_4d
    add-int/lit8 v5, v5, 0x1

    #@4f
    goto :goto_39

    #@50
    .line 1584
    .end local v6           #j:I
    .end local v7           #length:I
    :cond_50
    if-nez v9, :cond_53

    #@52
    .line 1585
    move-object v8, v4

    #@53
    .line 1588
    :cond_53
    const/4 v5, 0x0

    #@54
    :goto_54
    array-length v10, p1

    #@55
    if-ge v5, v10, :cond_6d

    #@57
    .line 1589
    aget-boolean v10, v4, v5

    #@59
    if-eqz v10, :cond_67

    #@5b
    .line 1590
    aget-object v1, p1, v5

    #@5d
    .line 1592
    .local v1, arc:Landroid/widget/GridLayout$Arc;
    iget-object v10, v1, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@5f
    iget v10, v10, Landroid/widget/GridLayout$Interval;->min:I

    #@61
    iget-object v11, v1, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@63
    iget v11, v11, Landroid/widget/GridLayout$Interval;->max:I

    #@65
    if-ge v10, v11, :cond_6a

    #@67
    .line 1588
    .end local v1           #arc:Landroid/widget/GridLayout$Arc;
    :cond_67
    add-int/lit8 v5, v5, 0x1

    #@69
    goto :goto_54

    #@6a
    .line 1595
    .restart local v1       #arc:Landroid/widget/GridLayout$Arc;
    :cond_6a
    const/4 v10, 0x0

    #@6b
    iput-boolean v10, v1, Landroid/widget/GridLayout$Arc;->valid:Z

    #@6d
    .line 1560
    .end local v1           #arc:Landroid/widget/GridLayout$Arc;
    :cond_6d
    add-int/lit8 v9, v9, 0x1

    #@6f
    goto :goto_e
.end method

.method private topologicalSort(Ljava/util/List;)[Landroid/widget/GridLayout$Arc;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/GridLayout$Arc;",
            ">;)[",
            "Landroid/widget/GridLayout$Arc;"
        }
    .end annotation

    #@0
    .prologue
    .line 1421
    .local p1, arcs:Ljava/util/List;,"Ljava/util/List<Landroid/widget/GridLayout$Arc;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    new-array v0, v0, [Landroid/widget/GridLayout$Arc;

    #@6
    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, [Landroid/widget/GridLayout$Arc;

    #@c
    invoke-direct {p0, v0}, Landroid/widget/GridLayout$Axis;->topologicalSort([Landroid/widget/GridLayout$Arc;)[Landroid/widget/GridLayout$Arc;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method private topologicalSort([Landroid/widget/GridLayout$Arc;)[Landroid/widget/GridLayout$Arc;
    .registers 3
    .parameter "arcs"

    #@0
    .prologue
    .line 1382
    new-instance v0, Landroid/widget/GridLayout$Axis$1;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/widget/GridLayout$Axis$1;-><init>(Landroid/widget/GridLayout$Axis;[Landroid/widget/GridLayout$Arc;)V

    #@5
    invoke-virtual {v0}, Landroid/widget/GridLayout$Axis$1;->sort()[Landroid/widget/GridLayout$Arc;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method


# virtual methods
.method public getArcs()[Landroid/widget/GridLayout$Arc;
    .registers 2

    #@0
    .prologue
    .line 1468
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->arcs:[Landroid/widget/GridLayout$Arc;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 1469
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->createArcs()[Landroid/widget/GridLayout$Arc;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->arcs:[Landroid/widget/GridLayout$Arc;

    #@a
    .line 1471
    :cond_a
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->arcsValid:Z

    #@c
    if-nez v0, :cond_14

    #@e
    .line 1472
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->computeArcs()V

    #@11
    .line 1473
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->arcsValid:Z

    #@14
    .line 1475
    :cond_14
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->arcs:[Landroid/widget/GridLayout$Arc;

    #@16
    return-object v0
.end method

.method public getCount()I
    .registers 3

    #@0
    .prologue
    .line 1225
    iget v0, p0, Landroid/widget/GridLayout$Axis;->definedCount:I

    #@2
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->getMaxIndex()I

    #@5
    move-result v1

    #@6
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getGroupBounds()Landroid/widget/GridLayout$PackedMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/GridLayout$PackedMap",
            "<",
            "Landroid/widget/GridLayout$Spec;",
            "Landroid/widget/GridLayout$Bounds;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1272
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->groupBounds:Landroid/widget/GridLayout$PackedMap;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 1273
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->createGroupBounds()Landroid/widget/GridLayout$PackedMap;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->groupBounds:Landroid/widget/GridLayout$PackedMap;

    #@a
    .line 1275
    :cond_a
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->groupBoundsValid:Z

    #@c
    if-nez v0, :cond_14

    #@e
    .line 1276
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->computeGroupBounds()V

    #@11
    .line 1277
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->groupBoundsValid:Z

    #@14
    .line 1279
    :cond_14
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->groupBounds:Landroid/widget/GridLayout$PackedMap;

    #@16
    return-object v0
.end method

.method public getLeadingMargins()[I
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1618
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->leadingMargins:[I

    #@3
    if-nez v0, :cond_f

    #@5
    .line 1619
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@8
    move-result v0

    #@9
    add-int/lit8 v0, v0, 0x1

    #@b
    new-array v0, v0, [I

    #@d
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->leadingMargins:[I

    #@f
    .line 1621
    :cond_f
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->leadingMarginsValid:Z

    #@11
    if-nez v0, :cond_18

    #@13
    .line 1622
    invoke-direct {p0, v1}, Landroid/widget/GridLayout$Axis;->computeMargins(Z)V

    #@16
    .line 1623
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->leadingMarginsValid:Z

    #@18
    .line 1625
    :cond_18
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->leadingMargins:[I

    #@1a
    return-object v0
.end method

.method public getLocations()[I
    .registers 3

    #@0
    .prologue
    .line 1656
    iget-object v1, p0, Landroid/widget/GridLayout$Axis;->locations:[I

    #@2
    if-nez v1, :cond_e

    #@4
    .line 1657
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v0, v1, 0x1

    #@a
    .line 1658
    .local v0, N:I
    new-array v1, v0, [I

    #@c
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->locations:[I

    #@e
    .line 1660
    .end local v0           #N:I
    :cond_e
    iget-boolean v1, p0, Landroid/widget/GridLayout$Axis;->locationsValid:Z

    #@10
    if-nez v1, :cond_1a

    #@12
    .line 1661
    iget-object v1, p0, Landroid/widget/GridLayout$Axis;->locations:[I

    #@14
    invoke-direct {p0, v1}, Landroid/widget/GridLayout$Axis;->computeLocations([I)V

    #@17
    .line 1662
    const/4 v1, 0x1

    #@18
    iput-boolean v1, p0, Landroid/widget/GridLayout$Axis;->locationsValid:Z

    #@1a
    .line 1664
    :cond_1a
    iget-object v1, p0, Landroid/widget/GridLayout$Axis;->locations:[I

    #@1c
    return-object v1
.end method

.method public getMeasure(I)I
    .registers 6
    .parameter "measureSpec"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1686
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v0

    #@5
    .line 1687
    .local v0, mode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v1

    #@9
    .line 1688
    .local v1, size:I
    sparse-switch v0, :sswitch_data_28

    #@c
    .line 1699
    sget-boolean v3, Landroid/widget/GridLayout$Axis;->$assertionsDisabled:Z

    #@e
    if-nez v3, :cond_1d

    #@10
    new-instance v2, Ljava/lang/AssertionError;

    #@12
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@15
    throw v2

    #@16
    .line 1690
    :sswitch_16
    const v3, 0x186a0

    #@19
    invoke-direct {p0, v2, v3}, Landroid/widget/GridLayout$Axis;->getMeasure(II)I

    #@1c
    move-result v2

    #@1d
    .line 1700
    :cond_1d
    :goto_1d
    return v2

    #@1e
    .line 1693
    :sswitch_1e
    invoke-direct {p0, v1, v1}, Landroid/widget/GridLayout$Axis;->getMeasure(II)I

    #@21
    move-result v2

    #@22
    goto :goto_1d

    #@23
    .line 1696
    :sswitch_23
    invoke-direct {p0, v2, v1}, Landroid/widget/GridLayout$Axis;->getMeasure(II)I

    #@26
    move-result v2

    #@27
    goto :goto_1d

    #@28
    .line 1688
    :sswitch_data_28
    .sparse-switch
        -0x80000000 -> :sswitch_23
        0x0 -> :sswitch_16
        0x40000000 -> :sswitch_1e
    .end sparse-switch
.end method

.method public getTrailingMargins()[I
    .registers 2

    #@0
    .prologue
    .line 1629
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->trailingMargins:[I

    #@2
    if-nez v0, :cond_e

    #@4
    .line 1630
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@7
    move-result v0

    #@8
    add-int/lit8 v0, v0, 0x1

    #@a
    new-array v0, v0, [I

    #@c
    iput-object v0, p0, Landroid/widget/GridLayout$Axis;->trailingMargins:[I

    #@e
    .line 1632
    :cond_e
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->trailingMarginsValid:Z

    #@10
    if-nez v0, :cond_19

    #@12
    .line 1633
    const/4 v0, 0x0

    #@13
    invoke-direct {p0, v0}, Landroid/widget/GridLayout$Axis;->computeMargins(Z)V

    #@16
    .line 1634
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->trailingMarginsValid:Z

    #@19
    .line 1636
    :cond_19
    iget-object v0, p0, Landroid/widget/GridLayout$Axis;->trailingMargins:[I

    #@1b
    return-object v0
.end method

.method groupArcsByFirstVertex([Landroid/widget/GridLayout$Arc;)[[Landroid/widget/GridLayout$Arc;
    .registers 13
    .parameter "arcs"

    #@0
    .prologue
    .line 1362
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getCount()I

    #@3
    move-result v8

    #@4
    add-int/lit8 v0, v8, 0x1

    #@6
    .line 1363
    .local v0, N:I
    new-array v6, v0, [[Landroid/widget/GridLayout$Arc;

    #@8
    .line 1364
    .local v6, result:[[Landroid/widget/GridLayout$Arc;
    new-array v7, v0, [I

    #@a
    .line 1365
    .local v7, sizes:[I
    move-object v2, p1

    #@b
    .local v2, arr$:[Landroid/widget/GridLayout$Arc;
    array-length v5, v2

    #@c
    .local v5, len$:I
    const/4 v4, 0x0

    #@d
    .local v4, i$:I
    :goto_d
    if-ge v4, v5, :cond_1e

    #@f
    aget-object v1, v2, v4

    #@11
    .line 1366
    .local v1, arc:Landroid/widget/GridLayout$Arc;
    iget-object v8, v1, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@13
    iget v8, v8, Landroid/widget/GridLayout$Interval;->min:I

    #@15
    aget v9, v7, v8

    #@17
    add-int/lit8 v9, v9, 0x1

    #@19
    aput v9, v7, v8

    #@1b
    .line 1365
    add-int/lit8 v4, v4, 0x1

    #@1d
    goto :goto_d

    #@1e
    .line 1368
    .end local v1           #arc:Landroid/widget/GridLayout$Arc;
    :cond_1e
    const/4 v3, 0x0

    #@1f
    .local v3, i:I
    :goto_1f
    array-length v8, v7

    #@20
    if-ge v3, v8, :cond_2b

    #@22
    .line 1369
    aget v8, v7, v3

    #@24
    new-array v8, v8, [Landroid/widget/GridLayout$Arc;

    #@26
    aput-object v8, v6, v3

    #@28
    .line 1368
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_1f

    #@2b
    .line 1372
    :cond_2b
    const/4 v8, 0x0

    #@2c
    invoke-static {v7, v8}, Ljava/util/Arrays;->fill([II)V

    #@2f
    .line 1373
    move-object v2, p1

    #@30
    array-length v5, v2

    #@31
    const/4 v4, 0x0

    #@32
    :goto_32
    if-ge v4, v5, :cond_47

    #@34
    aget-object v1, v2, v4

    #@36
    .line 1374
    .restart local v1       #arc:Landroid/widget/GridLayout$Arc;
    iget-object v8, v1, Landroid/widget/GridLayout$Arc;->span:Landroid/widget/GridLayout$Interval;

    #@38
    iget v3, v8, Landroid/widget/GridLayout$Interval;->min:I

    #@3a
    .line 1375
    aget-object v8, v6, v3

    #@3c
    aget v9, v7, v3

    #@3e
    add-int/lit8 v10, v9, 0x1

    #@40
    aput v10, v7, v3

    #@42
    aput-object v1, v8, v9

    #@44
    .line 1373
    add-int/lit8 v4, v4, 0x1

    #@46
    goto :goto_32

    #@47
    .line 1378
    .end local v1           #arc:Landroid/widget/GridLayout$Arc;
    :cond_47
    return-object v6
.end method

.method public invalidateStructure()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1711
    const/high16 v0, -0x8000

    #@3
    iput v0, p0, Landroid/widget/GridLayout$Axis;->maxIndex:I

    #@5
    .line 1713
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->groupBounds:Landroid/widget/GridLayout$PackedMap;

    #@7
    .line 1714
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->forwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@9
    .line 1715
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->backwardLinks:Landroid/widget/GridLayout$PackedMap;

    #@b
    .line 1717
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->leadingMargins:[I

    #@d
    .line 1718
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->trailingMargins:[I

    #@f
    .line 1719
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->arcs:[Landroid/widget/GridLayout$Arc;

    #@11
    .line 1721
    iput-object v1, p0, Landroid/widget/GridLayout$Axis;->locations:[I

    #@13
    .line 1723
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->invalidateValues()V

    #@16
    .line 1724
    return-void
.end method

.method public invalidateValues()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1727
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->groupBoundsValid:Z

    #@3
    .line 1728
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->forwardLinksValid:Z

    #@5
    .line 1729
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->backwardLinksValid:Z

    #@7
    .line 1731
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->leadingMarginsValid:Z

    #@9
    .line 1732
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->trailingMarginsValid:Z

    #@b
    .line 1733
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->arcsValid:Z

    #@d
    .line 1735
    iput-boolean v0, p0, Landroid/widget/GridLayout$Axis;->locationsValid:Z

    #@f
    .line 1736
    return-void
.end method

.method public isOrderPreserved()Z
    .registers 2

    #@0
    .prologue
    .line 1238
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->orderPreserved:Z

    #@2
    return v0
.end method

.method public layout(I)V
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 1706
    invoke-direct {p0, p1, p1}, Landroid/widget/GridLayout$Axis;->setParentConstraints(II)V

    #@3
    .line 1707
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->getLocations()[I

    #@6
    .line 1708
    return-void
.end method

.method public setCount(I)V
    .registers 4
    .parameter "count"

    #@0
    .prologue
    .line 1229
    const/high16 v0, -0x8000

    #@2
    if-eq p1, v0, :cond_2c

    #@4
    invoke-direct {p0}, Landroid/widget/GridLayout$Axis;->getMaxIndex()I

    #@7
    move-result v0

    #@8
    if-ge p1, v0, :cond_2c

    #@a
    .line 1230
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    iget-boolean v0, p0, Landroid/widget/GridLayout$Axis;->horizontal:Z

    #@11
    if-eqz v0, :cond_2f

    #@13
    const-string v0, "column"

    #@15
    :goto_15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, "Count must be greater than or equal to the maximum of all grid indices "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, "(and spans) defined in the LayoutParams of each child"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-static {v0}, Landroid/widget/GridLayout;->access$100(Ljava/lang/String;)V

    #@2c
    .line 1234
    :cond_2c
    iput p1, p0, Landroid/widget/GridLayout$Axis;->definedCount:I

    #@2e
    .line 1235
    return-void

    #@2f
    .line 1230
    :cond_2f
    const-string/jumbo v0, "row"

    #@32
    goto :goto_15
.end method

.method public setOrderPreserved(Z)V
    .registers 2
    .parameter "orderPreserved"

    #@0
    .prologue
    .line 1242
    iput-boolean p1, p0, Landroid/widget/GridLayout$Axis;->orderPreserved:Z

    #@2
    .line 1243
    invoke-virtual {p0}, Landroid/widget/GridLayout$Axis;->invalidateStructure()V

    #@5
    .line 1244
    return-void
.end method
