.class public Landroid/widget/AlphabetIndexer;
.super Landroid/database/DataSetObserver;
.source "AlphabetIndexer.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private mAlphaMap:Landroid/util/SparseIntArray;

.field protected mAlphabet:Ljava/lang/CharSequence;

.field private mAlphabetArray:[Ljava/lang/String;

.field private mAlphabetLength:I

.field private mCollator:Ljava/text/Collator;

.field protected mColumnIndex:I

.field protected mDataCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;ILjava/lang/CharSequence;)V
    .registers 7
    .parameter "cursor"
    .parameter "sortedColumnIndex"
    .parameter "alphabet"

    #@0
    .prologue
    .line 82
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    #@3
    .line 83
    iput-object p1, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@5
    .line 84
    iput p2, p0, Landroid/widget/AlphabetIndexer;->mColumnIndex:I

    #@7
    .line 85
    iput-object p3, p0, Landroid/widget/AlphabetIndexer;->mAlphabet:Ljava/lang/CharSequence;

    #@9
    .line 86
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    #@c
    move-result v1

    #@d
    iput v1, p0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@f
    .line 87
    iget v1, p0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@11
    new-array v1, v1, [Ljava/lang/String;

    #@13
    iput-object v1, p0, Landroid/widget/AlphabetIndexer;->mAlphabetArray:[Ljava/lang/String;

    #@15
    .line 88
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    iget v1, p0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@18
    if-ge v0, v1, :cond_2b

    #@1a
    .line 89
    iget-object v1, p0, Landroid/widget/AlphabetIndexer;->mAlphabetArray:[Ljava/lang/String;

    #@1c
    iget-object v2, p0, Landroid/widget/AlphabetIndexer;->mAlphabet:Ljava/lang/CharSequence;

    #@1e
    invoke-interface {v2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@21
    move-result v2

    #@22
    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    aput-object v2, v1, v0

    #@28
    .line 88
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_16

    #@2b
    .line 91
    :cond_2b
    new-instance v1, Landroid/util/SparseIntArray;

    #@2d
    iget v2, p0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@2f
    invoke-direct {v1, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    #@32
    iput-object v1, p0, Landroid/widget/AlphabetIndexer;->mAlphaMap:Landroid/util/SparseIntArray;

    #@34
    .line 92
    if-eqz p1, :cond_39

    #@36
    .line 93
    invoke-interface {p1, p0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@39
    .line 96
    :cond_39
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    #@3c
    move-result-object v1

    #@3d
    iput-object v1, p0, Landroid/widget/AlphabetIndexer;->mCollator:Ljava/text/Collator;

    #@3f
    .line 97
    iget-object v1, p0, Landroid/widget/AlphabetIndexer;->mCollator:Ljava/text/Collator;

    #@41
    const/4 v2, 0x0

    #@42
    invoke-virtual {v1, v2}, Ljava/text/Collator;->setStrength(I)V

    #@45
    .line 98
    return-void
.end method


# virtual methods
.method protected compare(Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "word"
    .parameter "letter"

    #@0
    .prologue
    .line 128
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_f

    #@6
    .line 129
    const-string v0, " "

    #@8
    .line 134
    .local v0, firstLetter:Ljava/lang/String;
    :goto_8
    iget-object v1, p0, Landroid/widget/AlphabetIndexer;->mCollator:Ljava/text/Collator;

    #@a
    invoke-virtual {v1, v0, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    move-result v1

    #@e
    return v1

    #@f
    .line 131
    .end local v0           #firstLetter:Ljava/lang/String;
    :cond_f
    const/4 v1, 0x0

    #@10
    const/4 v2, 0x1

    #@11
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .restart local v0       #firstLetter:Ljava/lang/String;
    goto :goto_8
.end method

.method public getPositionForSection(I)I
    .registers 19
    .parameter "sectionIndex"

    #@0
    .prologue
    .line 147
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/widget/AlphabetIndexer;->mAlphaMap:Landroid/util/SparseIntArray;

    #@4
    .line 148
    .local v1, alphaMap:Landroid/util/SparseIntArray;
    move-object/from16 v0, p0

    #@6
    iget-object v4, v0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@8
    .line 150
    .local v4, cursor:Landroid/database/Cursor;
    if-eqz v4, :cond_10

    #@a
    move-object/from16 v0, p0

    #@c
    iget-object v15, v0, Landroid/widget/AlphabetIndexer;->mAlphabet:Ljava/lang/CharSequence;

    #@e
    if-nez v15, :cond_12

    #@10
    .line 151
    :cond_10
    const/4 v9, 0x0

    #@11
    .line 248
    :cond_11
    :goto_11
    return v9

    #@12
    .line 155
    :cond_12
    if-gtz p1, :cond_16

    #@14
    .line 156
    const/4 v9, 0x0

    #@15
    goto :goto_11

    #@16
    .line 158
    :cond_16
    move-object/from16 v0, p0

    #@18
    iget v15, v0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@1a
    move/from16 v0, p1

    #@1c
    if-lt v0, v15, :cond_24

    #@1e
    .line 159
    move-object/from16 v0, p0

    #@20
    iget v15, v0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@22
    add-int/lit8 p1, v15, -0x1

    #@24
    .line 162
    :cond_24
    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    #@27
    move-result v12

    #@28
    .line 164
    .local v12, savedCursorPos:I
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    #@2b
    move-result v2

    #@2c
    .line 165
    .local v2, count:I
    const/4 v13, 0x0

    #@2d
    .line 166
    .local v13, start:I
    move v6, v2

    #@2e
    .line 169
    .local v6, end:I
    move-object/from16 v0, p0

    #@30
    iget-object v15, v0, Landroid/widget/AlphabetIndexer;->mAlphabet:Ljava/lang/CharSequence;

    #@32
    move/from16 v0, p1

    #@34
    invoke-interface {v15, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@37
    move-result v8

    #@38
    .line 170
    .local v8, letter:C
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@3b
    move-result-object v14

    #@3c
    .line 171
    .local v14, targetLetter:Ljava/lang/String;
    move v7, v8

    #@3d
    .line 173
    .local v7, key:I
    const/high16 v15, -0x8000

    #@3f
    const/high16 v16, -0x8000

    #@41
    move/from16 v0, v16

    #@43
    invoke-virtual {v1, v7, v0}, Landroid/util/SparseIntArray;->get(II)I

    #@46
    move-result v9

    #@47
    .local v9, pos:I
    if-eq v15, v9, :cond_4d

    #@49
    .line 177
    if-gez v9, :cond_11

    #@4b
    .line 178
    neg-int v9, v9

    #@4c
    .line 179
    move v6, v9

    #@4d
    .line 187
    :cond_4d
    if-lez p1, :cond_67

    #@4f
    .line 188
    move-object/from16 v0, p0

    #@51
    iget-object v15, v0, Landroid/widget/AlphabetIndexer;->mAlphabet:Ljava/lang/CharSequence;

    #@53
    add-int/lit8 v16, p1, -0x1

    #@55
    invoke-interface/range {v15 .. v16}, Ljava/lang/CharSequence;->charAt(I)C

    #@58
    move-result v10

    #@59
    .line 190
    .local v10, prevLetter:I
    const/high16 v15, -0x8000

    #@5b
    invoke-virtual {v1, v10, v15}, Landroid/util/SparseIntArray;->get(II)I

    #@5e
    move-result v11

    #@5f
    .line 191
    .local v11, prevLetterPos:I
    const/high16 v15, -0x8000

    #@61
    if-eq v11, v15, :cond_67

    #@63
    .line 192
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    #@66
    move-result v13

    #@67
    .line 198
    .end local v10           #prevLetter:I
    .end local v11           #prevLetterPos:I
    :cond_67
    add-int v15, v6, v13

    #@69
    div-int/lit8 v9, v15, 0x2

    #@6b
    .line 200
    :goto_6b
    if-ge v9, v6, :cond_7c

    #@6d
    .line 202
    invoke-interface {v4, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@70
    .line 203
    move-object/from16 v0, p0

    #@72
    iget v15, v0, Landroid/widget/AlphabetIndexer;->mColumnIndex:I

    #@74
    invoke-interface {v4, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    .line 204
    .local v3, curName:Ljava/lang/String;
    if-nez v3, :cond_86

    #@7a
    .line 205
    if-nez v9, :cond_83

    #@7c
    .line 246
    .end local v3           #curName:Ljava/lang/String;
    :cond_7c
    :goto_7c
    invoke-virtual {v1, v7, v9}, Landroid/util/SparseIntArray;->put(II)V

    #@7f
    .line 247
    invoke-interface {v4, v12}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@82
    goto :goto_11

    #@83
    .line 208
    .restart local v3       #curName:Ljava/lang/String;
    :cond_83
    add-int/lit8 v9, v9, -0x1

    #@85
    .line 209
    goto :goto_6b

    #@86
    .line 212
    :cond_86
    move-object/from16 v0, p0

    #@88
    invoke-virtual {v0, v3, v14}, Landroid/widget/AlphabetIndexer;->compare(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    move-result v5

    #@8c
    .line 213
    .local v5, diff:I
    if-eqz v5, :cond_9c

    #@8e
    .line 225
    if-gez v5, :cond_96

    #@90
    .line 226
    add-int/lit8 v13, v9, 0x1

    #@92
    .line 227
    if-lt v13, v2, :cond_97

    #@94
    .line 228
    move v9, v2

    #@95
    .line 229
    goto :goto_7c

    #@96
    .line 232
    :cond_96
    move v6, v9

    #@97
    .line 244
    :cond_97
    :goto_97
    add-int v15, v13, v6

    #@99
    div-int/lit8 v9, v15, 0x2

    #@9b
    .line 245
    goto :goto_6b

    #@9c
    .line 236
    :cond_9c
    if-eq v13, v9, :cond_7c

    #@9e
    .line 241
    move v6, v9

    #@9f
    goto :goto_97
.end method

.method public getSectionForPosition(I)I
    .registers 9
    .parameter "position"

    #@0
    .prologue
    .line 256
    iget-object v5, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    #@5
    move-result v3

    #@6
    .line 257
    .local v3, savedCursorPos:I
    iget-object v5, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@8
    invoke-interface {v5, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@b
    .line 258
    iget-object v5, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@d
    iget v6, p0, Landroid/widget/AlphabetIndexer;->mColumnIndex:I

    #@f
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 259
    .local v0, curName:Ljava/lang/String;
    iget-object v5, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@15
    invoke-interface {v5, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@18
    .line 262
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    iget v5, p0, Landroid/widget/AlphabetIndexer;->mAlphabetLength:I

    #@1b
    if-ge v1, v5, :cond_31

    #@1d
    .line 263
    iget-object v5, p0, Landroid/widget/AlphabetIndexer;->mAlphabet:Ljava/lang/CharSequence;

    #@1f
    invoke-interface {v5, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@22
    move-result v2

    #@23
    .line 264
    .local v2, letter:C
    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    .line 265
    .local v4, targetLetter:Ljava/lang/String;
    invoke-virtual {p0, v0, v4}, Landroid/widget/AlphabetIndexer;->compare(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    move-result v5

    #@2b
    if-nez v5, :cond_2e

    #@2d
    .line 269
    .end local v1           #i:I
    .end local v2           #letter:C
    .end local v4           #targetLetter:Ljava/lang/String;
    :goto_2d
    return v1

    #@2e
    .line 262
    .restart local v1       #i:I
    .restart local v2       #letter:C
    .restart local v4       #targetLetter:Ljava/lang/String;
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_19

    #@31
    .line 269
    .end local v2           #letter:C
    .end local v4           #targetLetter:Ljava/lang/String;
    :cond_31
    const/4 v1, 0x0

    #@32
    goto :goto_2d
.end method

.method public getSections()[Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mAlphabetArray:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public onChanged()V
    .registers 2

    #@0
    .prologue
    .line 277
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    #@3
    .line 278
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mAlphaMap:Landroid/util/SparseIntArray;

    #@5
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    #@8
    .line 279
    return-void
.end method

.method public onInvalidated()V
    .registers 2

    #@0
    .prologue
    .line 286
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    #@3
    .line 287
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mAlphaMap:Landroid/util/SparseIntArray;

    #@5
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    #@8
    .line 288
    return-void
.end method

.method public setCursor(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 114
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v0, p0}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@9
    .line 116
    :cond_9
    iput-object p1, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@b
    .line 117
    if-eqz p1, :cond_12

    #@d
    .line 118
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mDataCursor:Landroid/database/Cursor;

    #@f
    invoke-interface {v0, p0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@12
    .line 120
    :cond_12
    iget-object v0, p0, Landroid/widget/AlphabetIndexer;->mAlphaMap:Landroid/util/SparseIntArray;

    #@14
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    #@17
    .line 121
    return-void
.end method
