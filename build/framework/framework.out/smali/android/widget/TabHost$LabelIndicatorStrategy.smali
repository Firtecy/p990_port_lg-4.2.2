.class Landroid/widget/TabHost$LabelIndicatorStrategy;
.super Ljava/lang/Object;
.source "TabHost.java"

# interfaces
.implements Landroid/widget/TabHost$IndicatorStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/TabHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LabelIndicatorStrategy"
.end annotation


# instance fields
.field private final mLabel:Ljava/lang/CharSequence;

.field final synthetic this$0:Landroid/widget/TabHost;


# direct methods
.method private constructor <init>(Landroid/widget/TabHost;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter
    .parameter "label"

    #@0
    .prologue
    .line 590
    iput-object p1, p0, Landroid/widget/TabHost$LabelIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 591
    iput-object p2, p0, Landroid/widget/TabHost$LabelIndicatorStrategy;->mLabel:Ljava/lang/CharSequence;

    #@7
    .line 592
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/TabHost;Ljava/lang/CharSequence;Landroid/widget/TabHost$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 586
    invoke-direct {p0, p1, p2}, Landroid/widget/TabHost$LabelIndicatorStrategy;-><init>(Landroid/widget/TabHost;Ljava/lang/CharSequence;)V

    #@3
    return-void
.end method


# virtual methods
.method public createIndicatorView()Landroid/view/View;
    .registers 8

    #@0
    .prologue
    .line 595
    iget-object v4, p0, Landroid/widget/TabHost$LabelIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@2
    invoke-virtual {v4}, Landroid/widget/TabHost;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    .line 596
    .local v0, context:Landroid/content/Context;
    const-string/jumbo v4, "layout_inflater"

    #@9
    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/view/LayoutInflater;

    #@f
    .line 598
    .local v1, inflater:Landroid/view/LayoutInflater;
    iget-object v4, p0, Landroid/widget/TabHost$LabelIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@11
    invoke-static {v4}, Landroid/widget/TabHost;->access$900(Landroid/widget/TabHost;)I

    #@14
    move-result v4

    #@15
    iget-object v5, p0, Landroid/widget/TabHost$LabelIndicatorStrategy;->this$0:Landroid/widget/TabHost;

    #@17
    invoke-static {v5}, Landroid/widget/TabHost;->access$1000(Landroid/widget/TabHost;)Landroid/widget/TabWidget;

    #@1a
    move-result-object v5

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-virtual {v1, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@1f
    move-result-object v2

    #@20
    .line 602
    .local v2, tabIndicator:Landroid/view/View;
    const v4, 0x1020016

    #@23
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Landroid/widget/TextView;

    #@29
    .line 603
    .local v3, tv:Landroid/widget/TextView;
    iget-object v4, p0, Landroid/widget/TabHost$LabelIndicatorStrategy;->mLabel:Ljava/lang/CharSequence;

    #@2b
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2e
    .line 605
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@31
    move-result-object v4

    #@32
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@34
    const/4 v5, 0x4

    #@35
    if-gt v4, v5, :cond_4b

    #@37
    .line 607
    const v4, 0x10805bb

    #@3a
    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    #@3d
    .line 608
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@40
    move-result-object v4

    #@41
    const v5, 0x106007d

    #@44
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    #@4b
    .line 611
    :cond_4b
    return-object v2
.end method
