.class public Landroid/widget/StackView;
.super Landroid/widget/AdapterViewAnimator;
.source "StackView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/StackView$HolographicHelper;,
        Landroid/widget/StackView$LayoutParams;,
        Landroid/widget/StackView$StackSlider;,
        Landroid/widget/StackView$StackFrame;
    }
.end annotation


# static fields
.field private static final DEFAULT_ANIMATION_DURATION:I = 0x190

.field private static final FRAME_PADDING:I = 0x4

.field private static final GESTURE_NONE:I = 0x0

.field private static final GESTURE_SLIDE_DOWN:I = 0x2

.field private static final GESTURE_SLIDE_UP:I = 0x1

.field private static final INVALID_POINTER:I = -0x1

.field private static final ITEMS_SLIDE_DOWN:I = 0x1

.field private static final ITEMS_SLIDE_UP:I = 0x0

.field private static final MINIMUM_ANIMATION_DURATION:I = 0x32

.field private static final MIN_TIME_BETWEEN_INTERACTION_AND_AUTOADVANCE:I = 0x1388

.field private static final MIN_TIME_BETWEEN_SCROLLS:J = 0x64L

.field private static final NUM_ACTIVE_VIEWS:I = 0x5

.field private static final PERSPECTIVE_SCALE_FACTOR:F = 0.0f

.field private static final PERSPECTIVE_SHIFT_FACTOR_X:F = 0.1f

.field private static final PERSPECTIVE_SHIFT_FACTOR_Y:F = 0.1f

.field private static final SLIDE_UP_RATIO:F = 0.7f

.field private static final STACK_RELAYOUT_DURATION:I = 0x64

.field private static final SWIPE_THRESHOLD_RATIO:F = 0.2f

.field private static sHolographicHelper:Landroid/widget/StackView$HolographicHelper;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mActivePointerId:I

.field private mClickColor:I

.field private mClickFeedback:Landroid/widget/ImageView;

.field private mClickFeedbackIsValid:Z

.field private mFirstLayoutHappened:Z

.field private mFramePadding:I

.field private mHighlight:Landroid/widget/ImageView;

.field private mInitialX:F

.field private mInitialY:F

.field private mLastInteractionTime:J

.field private mLastScrollTime:J

.field private mMaximumVelocity:I

.field private mNewPerspectiveShiftX:F

.field private mNewPerspectiveShiftY:F

.field private mPerspectiveShiftX:F

.field private mPerspectiveShiftY:F

.field private mResOutColor:I

.field private mSlideAmount:I

.field private mStackMode:I

.field private mStackSlider:Landroid/widget/StackView$StackSlider;

.field private mSwipeGestureType:I

.field private mSwipeThreshold:I

.field private final mTouchRect:Landroid/graphics/Rect;

.field private mTouchSlop:I

.field private mTransitionIsSetup:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mYVelocity:I

.field private final stackInvalidateRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 157
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/StackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 164
    const v0, 0x10103dd

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/StackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 171
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 55
    const-string v1, "StackView"

    #@6
    iput-object v1, p0, Landroid/widget/StackView;->TAG:Ljava/lang/String;

    #@8
    .line 117
    new-instance v1, Landroid/graphics/Rect;

    #@a
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@d
    iput-object v1, p0, Landroid/widget/StackView;->mTouchRect:Landroid/graphics/Rect;

    #@f
    .line 130
    iput v3, p0, Landroid/widget/StackView;->mYVelocity:I

    #@11
    .line 131
    iput v3, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@13
    .line 137
    iput-boolean v3, p0, Landroid/widget/StackView;->mTransitionIsSetup:Z

    #@15
    .line 144
    iput-boolean v3, p0, Landroid/widget/StackView;->mClickFeedbackIsValid:Z

    #@17
    .line 146
    iput-boolean v3, p0, Landroid/widget/StackView;->mFirstLayoutHappened:Z

    #@19
    .line 147
    const-wide/16 v1, 0x0

    #@1b
    iput-wide v1, p0, Landroid/widget/StackView;->mLastInteractionTime:J

    #@1d
    .line 151
    new-instance v1, Landroid/graphics/Rect;

    #@1f
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@22
    iput-object v1, p0, Landroid/widget/StackView;->stackInvalidateRect:Landroid/graphics/Rect;

    #@24
    .line 172
    sget-object v1, Lcom/android/internal/R$styleable;->StackView:[I

    #@26
    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@29
    move-result-object v0

    #@2a
    .line 175
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    #@2d
    move-result v1

    #@2e
    iput v1, p0, Landroid/widget/StackView;->mResOutColor:I

    #@30
    .line 177
    const/4 v1, 0x1

    #@31
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    #@34
    move-result v1

    #@35
    iput v1, p0, Landroid/widget/StackView;->mClickColor:I

    #@37
    .line 180
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3a
    .line 181
    invoke-direct {p0}, Landroid/widget/StackView;->initStackView()V

    #@3d
    .line 182
    return-void
.end method

.method static synthetic access$000(Landroid/widget/StackView;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/widget/StackView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Landroid/widget/StackView;->mStackMode:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/widget/StackView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Landroid/widget/StackView;->mSlideAmount:I

    #@2
    return v0
.end method

.method private beginGestureIfNeeded(F)V
    .registers 14
    .parameter "deltaY"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 652
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@6
    move-result v10

    #@7
    float-to-int v10, v10

    #@8
    iget v11, p0, Landroid/widget/StackView;->mTouchSlop:I

    #@a
    if-le v10, v11, :cond_20

    #@c
    iget v10, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@e
    if-nez v10, :cond_20

    #@10
    .line 653
    const/4 v10, 0x0

    #@11
    cmpg-float v10, p1, v10

    #@13
    if-gez v10, :cond_21

    #@15
    move v5, v7

    #@16
    .line 654
    .local v5, swipeGestureType:I
    :goto_16
    invoke-virtual {p0}, Landroid/widget/StackView;->cancelLongPress()V

    #@19
    .line 655
    invoke-virtual {p0, v7}, Landroid/widget/StackView;->requestDisallowInterceptTouchEvent(Z)V

    #@1c
    .line 657
    iget-object v10, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@1e
    if-nez v10, :cond_23

    #@20
    .line 697
    .end local v5           #swipeGestureType:I
    :cond_20
    :goto_20
    return-void

    #@21
    :cond_21
    move v5, v8

    #@22
    .line 653
    goto :goto_16

    #@23
    .line 658
    .restart local v5       #swipeGestureType:I
    :cond_23
    invoke-virtual {p0}, Landroid/widget/StackView;->getCount()I

    #@26
    move-result v1

    #@27
    .line 661
    .local v1, adapterCount:I
    iget v10, p0, Landroid/widget/StackView;->mStackMode:I

    #@29
    if-nez v10, :cond_72

    #@2b
    .line 662
    if-ne v5, v8, :cond_70

    #@2d
    move v0, v9

    #@2e
    .line 667
    .local v0, activeIndex:I
    :goto_2e
    iget-boolean v10, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@30
    if-eqz v10, :cond_78

    #@32
    if-ne v1, v7, :cond_78

    #@34
    iget v10, p0, Landroid/widget/StackView;->mStackMode:I

    #@36
    if-nez v10, :cond_3a

    #@38
    if-eq v5, v7, :cond_40

    #@3a
    :cond_3a
    iget v10, p0, Landroid/widget/StackView;->mStackMode:I

    #@3c
    if-ne v10, v7, :cond_78

    #@3e
    if-ne v5, v8, :cond_78

    #@40
    :cond_40
    move v3, v7

    #@41
    .line 670
    .local v3, endOfStack:Z
    :goto_41
    iget-boolean v10, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@43
    if-eqz v10, :cond_7a

    #@45
    if-ne v1, v7, :cond_7a

    #@47
    iget v10, p0, Landroid/widget/StackView;->mStackMode:I

    #@49
    if-ne v10, v7, :cond_4d

    #@4b
    if-eq v5, v7, :cond_53

    #@4d
    :cond_4d
    iget v10, p0, Landroid/widget/StackView;->mStackMode:I

    #@4f
    if-nez v10, :cond_7a

    #@51
    if-ne v5, v8, :cond_7a

    #@53
    :cond_53
    move v2, v7

    #@54
    .line 675
    .local v2, beginningOfStack:Z
    :goto_54
    iget-boolean v8, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@56
    if-eqz v8, :cond_7c

    #@58
    if-nez v2, :cond_7c

    #@5a
    if-nez v3, :cond_7c

    #@5c
    .line 676
    const/4 v4, 0x0

    #@5d
    .line 686
    .local v4, stackMode:I
    :goto_5d
    if-nez v4, :cond_95

    #@5f
    :goto_5f
    iput-boolean v7, p0, Landroid/widget/StackView;->mTransitionIsSetup:Z

    #@61
    .line 688
    invoke-virtual {p0, v0}, Landroid/widget/StackView;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@64
    move-result-object v6

    #@65
    .line 689
    .local v6, v:Landroid/view/View;
    if-eqz v6, :cond_20

    #@67
    .line 691
    invoke-direct {p0, v6, v4}, Landroid/widget/StackView;->setupStackSlider(Landroid/view/View;I)V

    #@6a
    .line 694
    iput v5, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@6c
    .line 695
    invoke-virtual {p0}, Landroid/widget/StackView;->cancelHandleClick()V

    #@6f
    goto :goto_20

    #@70
    .end local v0           #activeIndex:I
    .end local v2           #beginningOfStack:Z
    .end local v3           #endOfStack:Z
    .end local v4           #stackMode:I
    .end local v6           #v:Landroid/view/View;
    :cond_70
    move v0, v7

    #@71
    .line 662
    goto :goto_2e

    #@72
    .line 664
    :cond_72
    if-ne v5, v8, :cond_76

    #@74
    move v0, v7

    #@75
    .restart local v0       #activeIndex:I
    :goto_75
    goto :goto_2e

    #@76
    .end local v0           #activeIndex:I
    :cond_76
    move v0, v9

    #@77
    goto :goto_75

    #@78
    .restart local v0       #activeIndex:I
    :cond_78
    move v3, v9

    #@79
    .line 667
    goto :goto_41

    #@7a
    .restart local v3       #endOfStack:Z
    :cond_7a
    move v2, v9

    #@7b
    .line 670
    goto :goto_54

    #@7c
    .line 677
    .restart local v2       #beginningOfStack:Z
    :cond_7c
    iget v8, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStartUnbounded:I

    #@7e
    add-int/2addr v8, v0

    #@7f
    const/4 v10, -0x1

    #@80
    if-eq v8, v10, :cond_84

    #@82
    if-eqz v2, :cond_88

    #@84
    .line 678
    :cond_84
    add-int/lit8 v0, v0, 0x1

    #@86
    .line 679
    const/4 v4, 0x1

    #@87
    .restart local v4       #stackMode:I
    goto :goto_5d

    #@88
    .line 680
    .end local v4           #stackMode:I
    :cond_88
    iget v8, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStartUnbounded:I

    #@8a
    add-int/2addr v8, v0

    #@8b
    add-int/lit8 v10, v1, -0x1

    #@8d
    if-eq v8, v10, :cond_91

    #@8f
    if-eqz v3, :cond_93

    #@91
    .line 681
    :cond_91
    const/4 v4, 0x2

    #@92
    .restart local v4       #stackMode:I
    goto :goto_5d

    #@93
    .line 683
    .end local v4           #stackMode:I
    :cond_93
    const/4 v4, 0x0

    #@94
    .restart local v4       #stackMode:I
    goto :goto_5d

    #@95
    :cond_95
    move v7, v9

    #@96
    .line 686
    goto :goto_5f
.end method

.method private handlePointerUp(Landroid/view/MotionEvent;)V
    .registers 15
    .parameter "ev"

    #@0
    .prologue
    .line 805
    iget v9, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@2
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@5
    move-result v6

    #@6
    .line 806
    .local v6, pointerIndex:I
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    #@9
    move-result v4

    #@a
    .line 807
    .local v4, newY:F
    iget v9, p0, Landroid/widget/StackView;->mInitialY:F

    #@c
    sub-float v9, v4, v9

    #@e
    float-to-int v1, v9

    #@f
    .line 808
    .local v1, deltaY:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12
    move-result-wide v9

    #@13
    iput-wide v9, p0, Landroid/widget/StackView;->mLastInteractionTime:J

    #@15
    .line 810
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@17
    if-eqz v9, :cond_2e

    #@19
    .line 811
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1b
    const/16 v10, 0x3e8

    #@1d
    iget v11, p0, Landroid/widget/StackView;->mMaximumVelocity:I

    #@1f
    int-to-float v11, v11

    #@20
    invoke-virtual {v9, v10, v11}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@23
    .line 812
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@25
    iget v10, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@27
    invoke-virtual {v9, v10}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@2a
    move-result v9

    #@2b
    float-to-int v9, v9

    #@2c
    iput v9, p0, Landroid/widget/StackView;->mYVelocity:I

    #@2e
    .line 815
    :cond_2e
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@30
    if-eqz v9, :cond_3a

    #@32
    .line 816
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@34
    invoke-virtual {v9}, Landroid/view/VelocityTracker;->recycle()V

    #@37
    .line 817
    const/4 v9, 0x0

    #@38
    iput-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@3a
    .line 820
    :cond_3a
    iget v9, p0, Landroid/widget/StackView;->mSwipeThreshold:I

    #@3c
    if-le v1, v9, :cond_63

    #@3e
    iget v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@40
    const/4 v10, 0x2

    #@41
    if-ne v9, v10, :cond_63

    #@43
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@45
    iget v9, v9, Landroid/widget/StackView$StackSlider;->mMode:I

    #@47
    if-nez v9, :cond_63

    #@49
    .line 824
    const/4 v9, 0x0

    #@4a
    iput v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@4c
    .line 827
    iget v9, p0, Landroid/widget/StackView;->mStackMode:I

    #@4e
    if-nez v9, :cond_5f

    #@50
    .line 828
    invoke-virtual {p0}, Landroid/widget/StackView;->showPrevious()V

    #@53
    .line 832
    :goto_53
    iget-object v9, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@55
    invoke-virtual {v9}, Landroid/widget/ImageView;->bringToFront()V

    #@58
    .line 885
    :cond_58
    :goto_58
    const/4 v9, -0x1

    #@59
    iput v9, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@5b
    .line 886
    const/4 v9, 0x0

    #@5c
    iput v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@5e
    .line 887
    return-void

    #@5f
    .line 830
    :cond_5f
    invoke-virtual {p0}, Landroid/widget/StackView;->showNext()V

    #@62
    goto :goto_53

    #@63
    .line 833
    :cond_63
    iget v9, p0, Landroid/widget/StackView;->mSwipeThreshold:I

    #@65
    neg-int v9, v9

    #@66
    if-ge v1, v9, :cond_87

    #@68
    iget v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@6a
    const/4 v10, 0x1

    #@6b
    if-ne v9, v10, :cond_87

    #@6d
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@6f
    iget v9, v9, Landroid/widget/StackView$StackSlider;->mMode:I

    #@71
    if-nez v9, :cond_87

    #@73
    .line 837
    const/4 v9, 0x0

    #@74
    iput v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@76
    .line 840
    iget v9, p0, Landroid/widget/StackView;->mStackMode:I

    #@78
    if-nez v9, :cond_83

    #@7a
    .line 841
    invoke-virtual {p0}, Landroid/widget/StackView;->showNext()V

    #@7d
    .line 846
    :goto_7d
    iget-object v9, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@7f
    invoke-virtual {v9}, Landroid/widget/ImageView;->bringToFront()V

    #@82
    goto :goto_58

    #@83
    .line 843
    :cond_83
    invoke-virtual {p0}, Landroid/widget/StackView;->showPrevious()V

    #@86
    goto :goto_7d

    #@87
    .line 847
    :cond_87
    iget v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@89
    const/4 v10, 0x1

    #@8a
    if-ne v9, v10, :cond_f2

    #@8c
    .line 850
    iget v9, p0, Landroid/widget/StackView;->mStackMode:I

    #@8e
    const/4 v10, 0x1

    #@8f
    if-ne v9, v10, :cond_e5

    #@91
    const/high16 v3, 0x3f80

    #@93
    .line 851
    .local v3, finalYProgress:F
    :goto_93
    iget v9, p0, Landroid/widget/StackView;->mStackMode:I

    #@95
    if-eqz v9, :cond_9d

    #@97
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@99
    iget v9, v9, Landroid/widget/StackView$StackSlider;->mMode:I

    #@9b
    if-eqz v9, :cond_e7

    #@9d
    .line 852
    :cond_9d
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@9f
    invoke-virtual {v9}, Landroid/widget/StackView$StackSlider;->getDurationForNeutralPosition()F

    #@a2
    move-result v9

    #@a3
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@a6
    move-result v2

    #@a7
    .line 857
    .local v2, duration:I
    :goto_a7
    new-instance v0, Landroid/widget/StackView$StackSlider;

    #@a9
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@ab
    invoke-direct {v0, p0, v9}, Landroid/widget/StackView$StackSlider;-><init>(Landroid/widget/StackView;Landroid/widget/StackView$StackSlider;)V

    #@ae
    .line 858
    .local v0, animationSlider:Landroid/widget/StackView$StackSlider;
    const-string v9, "YProgress"

    #@b0
    const/4 v10, 0x1

    #@b1
    new-array v10, v10, [F

    #@b3
    const/4 v11, 0x0

    #@b4
    aput v3, v10, v11

    #@b6
    invoke-static {v9, v10}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@b9
    move-result-object v8

    #@ba
    .line 859
    .local v8, snapBackY:Landroid/animation/PropertyValuesHolder;
    const-string v9, "XProgress"

    #@bc
    const/4 v10, 0x1

    #@bd
    new-array v10, v10, [F

    #@bf
    const/4 v11, 0x0

    #@c0
    const/4 v12, 0x0

    #@c1
    aput v12, v10, v11

    #@c3
    invoke-static {v9, v10}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@c6
    move-result-object v7

    #@c7
    .line 860
    .local v7, snapBackX:Landroid/animation/PropertyValuesHolder;
    const/4 v9, 0x2

    #@c8
    new-array v9, v9, [Landroid/animation/PropertyValuesHolder;

    #@ca
    const/4 v10, 0x0

    #@cb
    aput-object v7, v9, v10

    #@cd
    const/4 v10, 0x1

    #@ce
    aput-object v8, v9, v10

    #@d0
    invoke-static {v0, v9}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@d3
    move-result-object v5

    #@d4
    .line 862
    .local v5, pa:Landroid/animation/ObjectAnimator;
    int-to-long v9, v2

    #@d5
    invoke-virtual {v5, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@d8
    .line 863
    new-instance v9, Landroid/view/animation/LinearInterpolator;

    #@da
    invoke-direct {v9}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@dd
    invoke-virtual {v5, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@e0
    .line 864
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    #@e3
    goto/16 :goto_58

    #@e5
    .line 850
    .end local v0           #animationSlider:Landroid/widget/StackView$StackSlider;
    .end local v2           #duration:I
    .end local v3           #finalYProgress:F
    .end local v5           #pa:Landroid/animation/ObjectAnimator;
    .end local v7           #snapBackX:Landroid/animation/PropertyValuesHolder;
    .end local v8           #snapBackY:Landroid/animation/PropertyValuesHolder;
    :cond_e5
    const/4 v3, 0x0

    #@e6
    goto :goto_93

    #@e7
    .line 854
    .restart local v3       #finalYProgress:F
    :cond_e7
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@e9
    invoke-virtual {v9}, Landroid/widget/StackView$StackSlider;->getDurationForOffscreenPosition()F

    #@ec
    move-result v9

    #@ed
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@f0
    move-result v2

    #@f1
    .restart local v2       #duration:I
    goto :goto_a7

    #@f2
    .line 865
    .end local v2           #duration:I
    .end local v3           #finalYProgress:F
    :cond_f2
    iget v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@f4
    const/4 v10, 0x2

    #@f5
    if-ne v9, v10, :cond_58

    #@f7
    .line 867
    iget v9, p0, Landroid/widget/StackView;->mStackMode:I

    #@f9
    const/4 v10, 0x1

    #@fa
    if-ne v9, v10, :cond_148

    #@fc
    const/4 v3, 0x0

    #@fd
    .line 869
    .restart local v3       #finalYProgress:F
    :goto_fd
    iget v9, p0, Landroid/widget/StackView;->mStackMode:I

    #@ff
    const/4 v10, 0x1

    #@100
    if-eq v9, v10, :cond_108

    #@102
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@104
    iget v9, v9, Landroid/widget/StackView$StackSlider;->mMode:I

    #@106
    if-eqz v9, :cond_14b

    #@108
    .line 870
    :cond_108
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@10a
    invoke-virtual {v9}, Landroid/widget/StackView$StackSlider;->getDurationForNeutralPosition()F

    #@10d
    move-result v9

    #@10e
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@111
    move-result v2

    #@112
    .line 875
    .restart local v2       #duration:I
    :goto_112
    new-instance v0, Landroid/widget/StackView$StackSlider;

    #@114
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@116
    invoke-direct {v0, p0, v9}, Landroid/widget/StackView$StackSlider;-><init>(Landroid/widget/StackView;Landroid/widget/StackView$StackSlider;)V

    #@119
    .line 876
    .restart local v0       #animationSlider:Landroid/widget/StackView$StackSlider;
    const-string v9, "YProgress"

    #@11b
    const/4 v10, 0x1

    #@11c
    new-array v10, v10, [F

    #@11e
    const/4 v11, 0x0

    #@11f
    aput v3, v10, v11

    #@121
    invoke-static {v9, v10}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@124
    move-result-object v8

    #@125
    .line 878
    .restart local v8       #snapBackY:Landroid/animation/PropertyValuesHolder;
    const-string v9, "XProgress"

    #@127
    const/4 v10, 0x1

    #@128
    new-array v10, v10, [F

    #@12a
    const/4 v11, 0x0

    #@12b
    const/4 v12, 0x0

    #@12c
    aput v12, v10, v11

    #@12e
    invoke-static {v9, v10}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@131
    move-result-object v7

    #@132
    .line 879
    .restart local v7       #snapBackX:Landroid/animation/PropertyValuesHolder;
    const/4 v9, 0x2

    #@133
    new-array v9, v9, [Landroid/animation/PropertyValuesHolder;

    #@135
    const/4 v10, 0x0

    #@136
    aput-object v7, v9, v10

    #@138
    const/4 v10, 0x1

    #@139
    aput-object v8, v9, v10

    #@13b
    invoke-static {v0, v9}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@13e
    move-result-object v5

    #@13f
    .line 881
    .restart local v5       #pa:Landroid/animation/ObjectAnimator;
    int-to-long v9, v2

    #@140
    invoke-virtual {v5, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@143
    .line 882
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    #@146
    goto/16 :goto_58

    #@148
    .line 867
    .end local v0           #animationSlider:Landroid/widget/StackView$StackSlider;
    .end local v2           #duration:I
    .end local v3           #finalYProgress:F
    .end local v5           #pa:Landroid/animation/ObjectAnimator;
    .end local v7           #snapBackX:Landroid/animation/PropertyValuesHolder;
    .end local v8           #snapBackY:Landroid/animation/PropertyValuesHolder;
    :cond_148
    const/high16 v3, 0x3f80

    #@14a
    goto :goto_fd

    #@14b
    .line 872
    .restart local v3       #finalYProgress:F
    :cond_14b
    iget-object v9, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@14d
    invoke-virtual {v9}, Landroid/widget/StackView$StackSlider;->getDurationForOffscreenPosition()F

    #@150
    move-result v9

    #@151
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@154
    move-result v2

    #@155
    .restart local v2       #duration:I
    goto :goto_112
.end method

.method private initStackView()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, -0x1

    #@3
    .line 185
    const/4 v2, 0x5

    #@4
    invoke-virtual {p0, v2, v6}, Landroid/widget/StackView;->configureViewAnimator(II)V

    #@7
    .line 186
    invoke-virtual {p0, v6}, Landroid/widget/StackView;->setStaticTransformationsEnabled(Z)V

    #@a
    .line 187
    invoke-virtual {p0}, Landroid/widget/StackView;->getContext()Landroid/content/Context;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@11
    move-result-object v0

    #@12
    .line 188
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@15
    move-result v2

    #@16
    iput v2, p0, Landroid/widget/StackView;->mTouchSlop:I

    #@18
    .line 189
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@1b
    move-result v2

    #@1c
    iput v2, p0, Landroid/widget/StackView;->mMaximumVelocity:I

    #@1e
    .line 190
    iput v5, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@20
    .line 192
    new-instance v2, Landroid/widget/ImageView;

    #@22
    invoke-virtual {p0}, Landroid/widget/StackView;->getContext()Landroid/content/Context;

    #@25
    move-result-object v3

    #@26
    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@29
    iput-object v2, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@2b
    .line 193
    iget-object v2, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@2d
    new-instance v3, Landroid/widget/StackView$LayoutParams;

    #@2f
    iget-object v4, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@31
    invoke-direct {v3, p0, v4}, Landroid/widget/StackView$LayoutParams;-><init>(Landroid/widget/StackView;Landroid/view/View;)V

    #@34
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@37
    .line 194
    iget-object v2, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@39
    new-instance v3, Landroid/widget/StackView$LayoutParams;

    #@3b
    iget-object v4, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@3d
    invoke-direct {v3, p0, v4}, Landroid/widget/StackView$LayoutParams;-><init>(Landroid/widget/StackView;Landroid/view/View;)V

    #@40
    invoke-virtual {p0, v2, v5, v3}, Landroid/widget/StackView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    #@43
    .line 196
    new-instance v2, Landroid/widget/ImageView;

    #@45
    invoke-virtual {p0}, Landroid/widget/StackView;->getContext()Landroid/content/Context;

    #@48
    move-result-object v3

    #@49
    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@4c
    iput-object v2, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@4e
    .line 197
    iget-object v2, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@50
    new-instance v3, Landroid/widget/StackView$LayoutParams;

    #@52
    iget-object v4, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@54
    invoke-direct {v3, p0, v4}, Landroid/widget/StackView$LayoutParams;-><init>(Landroid/widget/StackView;Landroid/view/View;)V

    #@57
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@5a
    .line 198
    iget-object v2, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@5c
    new-instance v3, Landroid/widget/StackView$LayoutParams;

    #@5e
    iget-object v4, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@60
    invoke-direct {v3, p0, v4}, Landroid/widget/StackView$LayoutParams;-><init>(Landroid/widget/StackView;Landroid/view/View;)V

    #@63
    invoke-virtual {p0, v2, v5, v3}, Landroid/widget/StackView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    #@66
    .line 199
    iget-object v2, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@68
    const/4 v3, 0x4

    #@69
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6c
    .line 201
    new-instance v2, Landroid/widget/StackView$StackSlider;

    #@6e
    invoke-direct {v2, p0}, Landroid/widget/StackView$StackSlider;-><init>(Landroid/widget/StackView;)V

    #@71
    iput-object v2, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@73
    .line 203
    sget-object v2, Landroid/widget/StackView;->sHolographicHelper:Landroid/widget/StackView$HolographicHelper;

    #@75
    if-nez v2, :cond_80

    #@77
    .line 204
    new-instance v2, Landroid/widget/StackView$HolographicHelper;

    #@79
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@7b
    invoke-direct {v2, v3}, Landroid/widget/StackView$HolographicHelper;-><init>(Landroid/content/Context;)V

    #@7e
    sput-object v2, Landroid/widget/StackView;->sHolographicHelper:Landroid/widget/StackView$HolographicHelper;

    #@80
    .line 206
    :cond_80
    invoke-virtual {p0, v7}, Landroid/widget/StackView;->setClipChildren(Z)V

    #@83
    .line 207
    invoke-virtual {p0, v7}, Landroid/widget/StackView;->setClipToPadding(Z)V

    #@86
    .line 212
    iput v6, p0, Landroid/widget/StackView;->mStackMode:I

    #@88
    .line 215
    iput v5, p0, Landroid/widget/AdapterViewAnimator;->mWhichChild:I

    #@8a
    .line 219
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@8c
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8f
    move-result-object v2

    #@90
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@93
    move-result-object v2

    #@94
    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    #@96
    .line 220
    .local v1, density:F
    const/high16 v2, 0x4080

    #@98
    mul-float/2addr v2, v1

    #@99
    float-to-double v2, v2

    #@9a
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    #@9d
    move-result-wide v2

    #@9e
    double-to-int v2, v2

    #@9f
    iput v2, p0, Landroid/widget/StackView;->mFramePadding:I

    #@a1
    .line 221
    return-void
.end method

.method private measureChildren()V
    .registers 14

    #@0
    .prologue
    .line 1127
    invoke-virtual {p0}, Landroid/widget/StackView;->getChildCount()I

    #@3
    move-result v5

    #@4
    .line 1129
    .local v5, count:I
    invoke-virtual {p0}, Landroid/widget/StackView;->getMeasuredWidth()I

    #@7
    move-result v10

    #@8
    .line 1130
    .local v10, measuredWidth:I
    invoke-virtual {p0}, Landroid/widget/StackView;->getMeasuredHeight()I

    #@b
    move-result v9

    #@c
    .line 1132
    .local v9, measuredHeight:I
    int-to-float v11, v10

    #@d
    const v12, 0x3f666666

    #@10
    mul-float/2addr v11, v12

    #@11
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    #@14
    move-result v11

    #@15
    iget v12, p0, Landroid/view/View;->mPaddingLeft:I

    #@17
    sub-int/2addr v11, v12

    #@18
    iget v12, p0, Landroid/view/View;->mPaddingRight:I

    #@1a
    sub-int v4, v11, v12

    #@1c
    .line 1134
    .local v4, childWidth:I
    int-to-float v11, v9

    #@1d
    const v12, 0x3f666666

    #@20
    mul-float/2addr v11, v12

    #@21
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    #@24
    move-result v11

    #@25
    iget v12, p0, Landroid/view/View;->mPaddingTop:I

    #@27
    sub-int/2addr v11, v12

    #@28
    iget v12, p0, Landroid/view/View;->mPaddingBottom:I

    #@2a
    sub-int v1, v11, v12

    #@2c
    .line 1137
    .local v1, childHeight:I
    const/4 v8, 0x0

    #@2d
    .line 1138
    .local v8, maxWidth:I
    const/4 v7, 0x0

    #@2e
    .line 1140
    .local v7, maxHeight:I
    const/4 v6, 0x0

    #@2f
    .local v6, i:I
    :goto_2f
    if-ge v6, v5, :cond_5d

    #@31
    .line 1141
    invoke-virtual {p0, v6}, Landroid/widget/StackView;->getChildAt(I)Landroid/view/View;

    #@34
    move-result-object v0

    #@35
    .line 1142
    .local v0, child:Landroid/view/View;
    const/high16 v11, -0x8000

    #@37
    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3a
    move-result v11

    #@3b
    const/high16 v12, -0x8000

    #@3d
    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@40
    move-result v12

    #@41
    invoke-virtual {v0, v11, v12}, Landroid/view/View;->measure(II)V

    #@44
    .line 1145
    iget-object v11, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@46
    if-eq v0, v11, :cond_5a

    #@48
    iget-object v11, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@4a
    if-eq v0, v11, :cond_5a

    #@4c
    .line 1146
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@4f
    move-result v3

    #@50
    .line 1147
    .local v3, childMeasuredWidth:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@53
    move-result v2

    #@54
    .line 1148
    .local v2, childMeasuredHeight:I
    if-le v3, v8, :cond_57

    #@56
    .line 1149
    move v8, v3

    #@57
    .line 1151
    :cond_57
    if-le v2, v7, :cond_5a

    #@59
    .line 1152
    move v7, v2

    #@5a
    .line 1140
    .end local v2           #childMeasuredHeight:I
    .end local v3           #childMeasuredWidth:I
    :cond_5a
    add-int/lit8 v6, v6, 0x1

    #@5c
    goto :goto_2f

    #@5d
    .line 1157
    .end local v0           #child:Landroid/view/View;
    :cond_5d
    const v11, 0x3dcccccd

    #@60
    int-to-float v12, v10

    #@61
    mul-float/2addr v11, v12

    #@62
    iput v11, p0, Landroid/widget/StackView;->mNewPerspectiveShiftX:F

    #@64
    .line 1158
    const v11, 0x3dcccccd

    #@67
    int-to-float v12, v9

    #@68
    mul-float/2addr v11, v12

    #@69
    iput v11, p0, Landroid/widget/StackView;->mNewPerspectiveShiftY:F

    #@6b
    .line 1161
    if-lez v8, :cond_76

    #@6d
    if-lez v5, :cond_76

    #@6f
    if-ge v8, v4, :cond_76

    #@71
    .line 1162
    sub-int v11, v10, v8

    #@73
    int-to-float v11, v11

    #@74
    iput v11, p0, Landroid/widget/StackView;->mNewPerspectiveShiftX:F

    #@76
    .line 1165
    :cond_76
    if-lez v7, :cond_81

    #@78
    if-lez v5, :cond_81

    #@7a
    if-ge v7, v1, :cond_81

    #@7c
    .line 1166
    sub-int v11, v9, v7

    #@7e
    int-to-float v11, v11

    #@7f
    iput v11, p0, Landroid/widget/StackView;->mNewPerspectiveShiftY:F

    #@81
    .line 1168
    :cond_81
    return-void
.end method

.method private onLayout()V
    .registers 4

    #@0
    .prologue
    .line 557
    iget-boolean v1, p0, Landroid/widget/StackView;->mFirstLayoutHappened:Z

    #@2
    if-nez v1, :cond_a

    #@4
    .line 558
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Landroid/widget/StackView;->mFirstLayoutHappened:Z

    #@7
    .line 559
    invoke-direct {p0}, Landroid/widget/StackView;->updateChildTransforms()V

    #@a
    .line 562
    :cond_a
    const v1, 0x3f333333

    #@d
    invoke-virtual {p0}, Landroid/widget/StackView;->getMeasuredHeight()I

    #@10
    move-result v2

    #@11
    int-to-float v2, v2

    #@12
    mul-float/2addr v1, v2

    #@13
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@16
    move-result v0

    #@17
    .line 563
    .local v0, newSlideAmount:I
    iget v1, p0, Landroid/widget/StackView;->mSlideAmount:I

    #@19
    if-eq v1, v0, :cond_28

    #@1b
    .line 564
    iput v0, p0, Landroid/widget/StackView;->mSlideAmount:I

    #@1d
    .line 565
    const v1, 0x3e4ccccd

    #@20
    int-to-float v2, v0

    #@21
    mul-float/2addr v1, v2

    #@22
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@25
    move-result v1

    #@26
    iput v1, p0, Landroid/widget/StackView;->mSwipeThreshold:I

    #@28
    .line 568
    :cond_28
    iget v1, p0, Landroid/widget/StackView;->mPerspectiveShiftY:F

    #@2a
    iget v2, p0, Landroid/widget/StackView;->mNewPerspectiveShiftY:F

    #@2c
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    #@2f
    move-result v1

    #@30
    if-nez v1, :cond_3c

    #@32
    iget v1, p0, Landroid/widget/StackView;->mPerspectiveShiftX:F

    #@34
    iget v2, p0, Landroid/widget/StackView;->mNewPerspectiveShiftX:F

    #@36
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_47

    #@3c
    .line 571
    :cond_3c
    iget v1, p0, Landroid/widget/StackView;->mNewPerspectiveShiftY:F

    #@3e
    iput v1, p0, Landroid/widget/StackView;->mPerspectiveShiftY:F

    #@40
    .line 572
    iget v1, p0, Landroid/widget/StackView;->mNewPerspectiveShiftX:F

    #@42
    iput v1, p0, Landroid/widget/StackView;->mPerspectiveShiftX:F

    #@44
    .line 573
    invoke-direct {p0}, Landroid/widget/StackView;->updateChildTransforms()V

    #@47
    .line 575
    :cond_47
    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .registers 16
    .parameter "ev"

    #@0
    .prologue
    .line 761
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@3
    move-result v0

    #@4
    .line 762
    .local v0, activePointerIndex:I
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@7
    move-result v5

    #@8
    .line 763
    .local v5, pointerId:I
    iget v9, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@a
    if-ne v5, v9, :cond_18

    #@c
    .line 765
    iget v9, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@e
    const/4 v10, 0x2

    #@f
    if-ne v9, v10, :cond_19

    #@11
    const/4 v1, 0x0

    #@12
    .line 767
    .local v1, activeViewIndex:I
    :goto_12
    invoke-virtual {p0, v1}, Landroid/widget/StackView;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@15
    move-result-object v6

    #@16
    .line 768
    .local v6, v:Landroid/view/View;
    if-nez v6, :cond_1b

    #@18
    .line 802
    .end local v1           #activeViewIndex:I
    .end local v6           #v:Landroid/view/View;
    :cond_18
    :goto_18
    return-void

    #@19
    .line 765
    :cond_19
    const/4 v1, 0x1

    #@1a
    goto :goto_12

    #@1b
    .line 774
    .restart local v1       #activeViewIndex:I
    .restart local v6       #v:Landroid/view/View;
    :cond_1b
    const/4 v2, 0x0

    #@1c
    .local v2, index:I
    :goto_1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@1f
    move-result v9

    #@20
    if-ge v2, v9, :cond_7a

    #@22
    .line 775
    if-eq v2, v0, :cond_77

    #@24
    .line 777
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    #@27
    move-result v7

    #@28
    .line 778
    .local v7, x:F
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    #@2b
    move-result v8

    #@2c
    .line 780
    .local v8, y:F
    iget-object v9, p0, Landroid/widget/StackView;->mTouchRect:Landroid/graphics/Rect;

    #@2e
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    #@31
    move-result v10

    #@32
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@35
    move-result v11

    #@36
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    #@39
    move-result v12

    #@3a
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@3d
    move-result v13

    #@3e
    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    #@41
    .line 781
    iget-object v9, p0, Landroid/widget/StackView;->mTouchRect:Landroid/graphics/Rect;

    #@43
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    #@46
    move-result v10

    #@47
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    #@4a
    move-result v11

    #@4b
    invoke-virtual {v9, v10, v11}, Landroid/graphics/Rect;->contains(II)Z

    #@4e
    move-result v9

    #@4f
    if-eqz v9, :cond_77

    #@51
    .line 782
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@54
    move-result v3

    #@55
    .line 783
    .local v3, oldX:F
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@58
    move-result v4

    #@59
    .line 786
    .local v4, oldY:F
    iget v9, p0, Landroid/widget/StackView;->mInitialY:F

    #@5b
    sub-float v10, v8, v4

    #@5d
    add-float/2addr v9, v10

    #@5e
    iput v9, p0, Landroid/widget/StackView;->mInitialY:F

    #@60
    .line 787
    iget v9, p0, Landroid/widget/StackView;->mInitialX:F

    #@62
    sub-float v10, v7, v3

    #@64
    add-float/2addr v9, v10

    #@65
    iput v9, p0, Landroid/widget/StackView;->mInitialX:F

    #@67
    .line 789
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@6a
    move-result v9

    #@6b
    iput v9, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@6d
    .line 790
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6f
    if-eqz v9, :cond_18

    #@71
    .line 791
    iget-object v9, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@73
    invoke-virtual {v9}, Landroid/view/VelocityTracker;->clear()V

    #@76
    goto :goto_18

    #@77
    .line 774
    .end local v3           #oldX:F
    .end local v4           #oldY:F
    .end local v7           #x:F
    .end local v8           #y:F
    :cond_77
    add-int/lit8 v2, v2, 0x1

    #@79
    goto :goto_1c

    #@7a
    .line 800
    :cond_7a
    invoke-direct {p0, p1}, Landroid/widget/StackView;->handlePointerUp(Landroid/view/MotionEvent;)V

    #@7d
    goto :goto_18
.end method

.method private pacedScroll(Z)V
    .registers 8
    .parameter "up"

    #@0
    .prologue
    .line 598
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v2

    #@4
    iget-wide v4, p0, Landroid/widget/StackView;->mLastScrollTime:J

    #@6
    sub-long v0, v2, v4

    #@8
    .line 599
    .local v0, timeSinceLastScroll:J
    const-wide/16 v2, 0x64

    #@a
    cmp-long v2, v0, v2

    #@c
    if-lez v2, :cond_19

    #@e
    .line 600
    if-eqz p1, :cond_1a

    #@10
    .line 601
    invoke-virtual {p0}, Landroid/widget/StackView;->showPrevious()V

    #@13
    .line 605
    :goto_13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@16
    move-result-wide v2

    #@17
    iput-wide v2, p0, Landroid/widget/StackView;->mLastScrollTime:J

    #@19
    .line 607
    :cond_19
    return-void

    #@1a
    .line 603
    :cond_1a
    invoke-virtual {p0}, Landroid/widget/StackView;->showNext()V

    #@1d
    goto :goto_13
.end method

.method private setupStackSlider(Landroid/view/View;I)V
    .registers 6
    .parameter "v"
    .parameter "mode"

    #@0
    .prologue
    .line 367
    iget-object v0, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@2
    invoke-virtual {v0, p2}, Landroid/widget/StackView$StackSlider;->setMode(I)V

    #@5
    .line 368
    if-eqz p1, :cond_40

    #@7
    .line 369
    iget-object v0, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@9
    sget-object v1, Landroid/widget/StackView;->sHolographicHelper:Landroid/widget/StackView$HolographicHelper;

    #@b
    iget v2, p0, Landroid/widget/StackView;->mResOutColor:I

    #@d
    invoke-virtual {v1, p1, v2}, Landroid/widget/StackView$HolographicHelper;->createResOutline(Landroid/view/View;I)Landroid/graphics/Bitmap;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@14
    .line 370
    iget-object v0, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@16
    invoke-virtual {p1}, Landroid/view/View;->getRotation()F

    #@19
    move-result v1

    #@1a
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    #@1d
    .line 371
    iget-object v0, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@1f
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    #@22
    move-result v1

    #@23
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    #@26
    .line 372
    iget-object v0, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@28
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    #@2b
    move-result v1

    #@2c
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    #@2f
    .line 373
    iget-object v0, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@31
    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    #@34
    .line 374
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    #@37
    .line 375
    iget-object v0, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@39
    invoke-virtual {v0, p1}, Landroid/widget/StackView$StackSlider;->setView(Landroid/view/View;)V

    #@3c
    .line 377
    const/4 v0, 0x0

    #@3d
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    #@40
    .line 379
    :cond_40
    return-void
.end method

.method private transformViewAtIndex(ILandroid/view/View;Z)V
    .registers 24
    .parameter "index"
    .parameter "view"
    .parameter "animate"

    #@0
    .prologue
    .line 314
    move-object/from16 v0, p0

    #@2
    iget v3, v0, Landroid/widget/StackView;->mPerspectiveShiftY:F

    #@4
    .line 315
    .local v3, maxPerspectiveShiftY:F
    move-object/from16 v0, p0

    #@6
    iget v2, v0, Landroid/widget/StackView;->mPerspectiveShiftX:F

    #@8
    .line 317
    .local v2, maxPerspectiveShiftX:F
    move-object/from16 v0, p0

    #@a
    iget v0, v0, Landroid/widget/StackView;->mStackMode:I

    #@c
    move/from16 v17, v0

    #@e
    const/16 v18, 0x1

    #@10
    move/from16 v0, v17

    #@12
    move/from16 v1, v18

    #@14
    if-ne v0, v1, :cond_127

    #@16
    .line 318
    move-object/from16 v0, p0

    #@18
    iget v0, v0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@1a
    move/from16 v17, v0

    #@1c
    sub-int v17, v17, p1

    #@1e
    add-int/lit8 p1, v17, -0x1

    #@20
    .line 319
    move-object/from16 v0, p0

    #@22
    iget v0, v0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@24
    move/from16 v17, v0

    #@26
    add-int/lit8 v17, v17, -0x1

    #@28
    move/from16 v0, p1

    #@2a
    move/from16 v1, v17

    #@2c
    if-ne v0, v1, :cond_30

    #@2e
    add-int/lit8 p1, p1, -0x1

    #@30
    .line 325
    :cond_30
    :goto_30
    move/from16 v0, p1

    #@32
    int-to-float v0, v0

    #@33
    move/from16 v17, v0

    #@35
    const/high16 v18, 0x3f80

    #@37
    mul-float v17, v17, v18

    #@39
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Landroid/widget/AdapterViewAnimator;->mMaxNumActiveViews:I

    #@3d
    move/from16 v18, v0

    #@3f
    add-int/lit8 v18, v18, -0x2

    #@41
    move/from16 v0, v18

    #@43
    int-to-float v0, v0

    #@44
    move/from16 v18, v0

    #@46
    div-float v7, v17, v18

    #@48
    .line 327
    .local v7, r:F
    const/high16 v17, 0x3f80

    #@4a
    const/16 v18, 0x0

    #@4c
    const/high16 v19, 0x3f80

    #@4e
    sub-float v19, v19, v7

    #@50
    mul-float v18, v18, v19

    #@52
    sub-float v8, v17, v18

    #@54
    .line 329
    .local v8, scale:F
    mul-float v6, v7, v3

    #@56
    .line 330
    .local v6, perspectiveTranslationY:F
    const/high16 v17, 0x3f80

    #@58
    sub-float v17, v8, v17

    #@5a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/StackView;->getMeasuredHeight()I

    #@5d
    move-result v18

    #@5e
    move/from16 v0, v18

    #@60
    int-to-float v0, v0

    #@61
    move/from16 v18, v0

    #@63
    const v19, 0x3f666666

    #@66
    mul-float v18, v18, v19

    #@68
    const/high16 v19, 0x4000

    #@6a
    div-float v18, v18, v19

    #@6c
    mul-float v12, v17, v18

    #@6e
    .line 332
    .local v12, scaleShiftCorrectionY:F
    add-float v14, v6, v12

    #@70
    .line 334
    .local v14, transY:F
    const/high16 v17, 0x3f80

    #@72
    sub-float v17, v17, v7

    #@74
    mul-float v5, v17, v2

    #@76
    .line 335
    .local v5, perspectiveTranslationX:F
    const/high16 v17, 0x3f80

    #@78
    sub-float v17, v17, v8

    #@7a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/StackView;->getMeasuredWidth()I

    #@7d
    move-result v18

    #@7e
    move/from16 v0, v18

    #@80
    int-to-float v0, v0

    #@81
    move/from16 v18, v0

    #@83
    const v19, 0x3f666666

    #@86
    mul-float v18, v18, v19

    #@88
    const/high16 v19, 0x4000

    #@8a
    div-float v18, v18, v19

    #@8c
    mul-float v11, v17, v18

    #@8e
    .line 337
    .local v11, scaleShiftCorrectionX:F
    add-float v13, v5, v11

    #@90
    .line 341
    .local v13, transX:F
    move-object/from16 v0, p2

    #@92
    instance-of v0, v0, Landroid/widget/StackView$StackFrame;

    #@94
    move/from16 v17, v0

    #@96
    if-eqz v17, :cond_9f

    #@98
    move-object/from16 v17, p2

    #@9a
    .line 342
    check-cast v17, Landroid/widget/StackView$StackFrame;

    #@9c
    invoke-virtual/range {v17 .. v17}, Landroid/widget/StackView$StackFrame;->cancelTransformAnimator()Z

    #@9f
    .line 345
    :cond_9f
    if-eqz p3, :cond_12f

    #@a1
    .line 346
    const-string/jumbo v17, "translationX"

    #@a4
    const/16 v18, 0x1

    #@a6
    move/from16 v0, v18

    #@a8
    new-array v0, v0, [F

    #@aa
    move-object/from16 v18, v0

    #@ac
    const/16 v19, 0x0

    #@ae
    aput v13, v18, v19

    #@b0
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@b3
    move-result-object v15

    #@b4
    .line 347
    .local v15, translationX:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v17, "translationY"

    #@b7
    const/16 v18, 0x1

    #@b9
    move/from16 v0, v18

    #@bb
    new-array v0, v0, [F

    #@bd
    move-object/from16 v18, v0

    #@bf
    const/16 v19, 0x0

    #@c1
    aput v14, v18, v19

    #@c3
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@c6
    move-result-object v16

    #@c7
    .line 348
    .local v16, translationY:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v17, "scaleX"

    #@ca
    const/16 v18, 0x1

    #@cc
    move/from16 v0, v18

    #@ce
    new-array v0, v0, [F

    #@d0
    move-object/from16 v18, v0

    #@d2
    const/16 v19, 0x0

    #@d4
    aput v8, v18, v19

    #@d6
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@d9
    move-result-object v9

    #@da
    .line 349
    .local v9, scalePropX:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v17, "scaleY"

    #@dd
    const/16 v18, 0x1

    #@df
    move/from16 v0, v18

    #@e1
    new-array v0, v0, [F

    #@e3
    move-object/from16 v18, v0

    #@e5
    const/16 v19, 0x0

    #@e7
    aput v8, v18, v19

    #@e9
    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@ec
    move-result-object v10

    #@ed
    .line 351
    .local v10, scalePropY:Landroid/animation/PropertyValuesHolder;
    const/16 v17, 0x4

    #@ef
    move/from16 v0, v17

    #@f1
    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    #@f3
    move-object/from16 v17, v0

    #@f5
    const/16 v18, 0x0

    #@f7
    aput-object v9, v17, v18

    #@f9
    const/16 v18, 0x1

    #@fb
    aput-object v10, v17, v18

    #@fd
    const/16 v18, 0x2

    #@ff
    aput-object v16, v17, v18

    #@101
    const/16 v18, 0x3

    #@103
    aput-object v15, v17, v18

    #@105
    move-object/from16 v0, p2

    #@107
    move-object/from16 v1, v17

    #@109
    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@10c
    move-result-object v4

    #@10d
    .line 353
    .local v4, oa:Landroid/animation/ObjectAnimator;
    const-wide/16 v17, 0x64

    #@10f
    move-wide/from16 v0, v17

    #@111
    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@114
    .line 354
    move-object/from16 v0, p2

    #@116
    instance-of v0, v0, Landroid/widget/StackView$StackFrame;

    #@118
    move/from16 v17, v0

    #@11a
    if-eqz v17, :cond_123

    #@11c
    .line 355
    check-cast p2, Landroid/widget/StackView$StackFrame;

    #@11e
    .end local p2
    move-object/from16 v0, p2

    #@120
    invoke-virtual {v0, v4}, Landroid/widget/StackView$StackFrame;->setTransformAnimator(Landroid/animation/ObjectAnimator;)V

    #@123
    .line 357
    :cond_123
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    #@126
    .line 364
    .end local v4           #oa:Landroid/animation/ObjectAnimator;
    .end local v9           #scalePropX:Landroid/animation/PropertyValuesHolder;
    .end local v10           #scalePropY:Landroid/animation/PropertyValuesHolder;
    .end local v15           #translationX:Landroid/animation/PropertyValuesHolder;
    .end local v16           #translationY:Landroid/animation/PropertyValuesHolder;
    :goto_126
    return-void

    #@127
    .line 321
    .end local v5           #perspectiveTranslationX:F
    .end local v6           #perspectiveTranslationY:F
    .end local v7           #r:F
    .end local v8           #scale:F
    .end local v11           #scaleShiftCorrectionX:F
    .end local v12           #scaleShiftCorrectionY:F
    .end local v13           #transX:F
    .end local v14           #transY:F
    .restart local p2
    :cond_127
    add-int/lit8 p1, p1, -0x1

    #@129
    .line 322
    if-gez p1, :cond_30

    #@12b
    add-int/lit8 p1, p1, 0x1

    #@12d
    goto/16 :goto_30

    #@12f
    .line 359
    .restart local v5       #perspectiveTranslationX:F
    .restart local v6       #perspectiveTranslationY:F
    .restart local v7       #r:F
    .restart local v8       #scale:F
    .restart local v11       #scaleShiftCorrectionX:F
    .restart local v12       #scaleShiftCorrectionY:F
    .restart local v13       #transX:F
    .restart local v14       #transY:F
    :cond_12f
    move-object/from16 v0, p2

    #@131
    invoke-virtual {v0, v13}, Landroid/view/View;->setTranslationX(F)V

    #@134
    .line 360
    move-object/from16 v0, p2

    #@136
    invoke-virtual {v0, v14}, Landroid/view/View;->setTranslationY(F)V

    #@139
    .line 361
    move-object/from16 v0, p2

    #@13b
    invoke-virtual {v0, v8}, Landroid/view/View;->setScaleX(F)V

    #@13e
    .line 362
    move-object/from16 v0, p2

    #@140
    invoke-virtual {v0, v8}, Landroid/view/View;->setScaleY(F)V

    #@143
    goto :goto_126
.end method

.method private updateChildTransforms()V
    .registers 4

    #@0
    .prologue
    .line 465
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    invoke-virtual {p0}, Landroid/widget/StackView;->getNumActiveViews()I

    #@4
    move-result v2

    #@5
    if-ge v0, v2, :cond_14

    #@7
    .line 466
    invoke-virtual {p0, v0}, Landroid/widget/StackView;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@a
    move-result-object v1

    #@b
    .line 467
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_11

    #@d
    .line 468
    const/4 v2, 0x0

    #@e
    invoke-direct {p0, v0, v1, v2}, Landroid/widget/StackView;->transformViewAtIndex(ILandroid/view/View;Z)V

    #@11
    .line 465
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_1

    #@14
    .line 471
    .end local v1           #v:Landroid/view/View;
    :cond_14
    return-void
.end method


# virtual methods
.method public advance()V
    .registers 8

    #@0
    .prologue
    .line 1114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v3

    #@4
    iget-wide v5, p0, Landroid/widget/StackView;->mLastInteractionTime:J

    #@6
    sub-long v1, v3, v5

    #@8
    .line 1116
    .local v1, timeSinceLastInteraction:J
    iget-object v3, p0, Landroid/widget/AdapterViewAnimator;->mAdapter:Landroid/widget/Adapter;

    #@a
    if-nez v3, :cond_d

    #@c
    .line 1124
    :cond_c
    :goto_c
    return-void

    #@d
    .line 1117
    :cond_d
    invoke-virtual {p0}, Landroid/widget/StackView;->getCount()I

    #@10
    move-result v0

    #@11
    .line 1118
    .local v0, adapterCount:I
    const/4 v3, 0x1

    #@12
    if-ne v0, v3, :cond_18

    #@14
    iget-boolean v3, p0, Landroid/widget/AdapterViewAnimator;->mLoopViews:Z

    #@16
    if-nez v3, :cond_c

    #@18
    .line 1120
    :cond_18
    iget v3, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@1a
    if-nez v3, :cond_c

    #@1c
    const-wide/16 v3, 0x1388

    #@1e
    cmp-long v3, v1, v3

    #@20
    if-lez v3, :cond_c

    #@22
    .line 1122
    invoke-virtual {p0}, Landroid/widget/StackView;->showNext()V

    #@25
    goto :goto_c
.end method

.method applyTransformForChildAtIndex(Landroid/view/View;I)V
    .registers 3
    .parameter "child"
    .parameter "relativeIndex"

    #@0
    .prologue
    .line 523
    return-void
.end method

.method bridge synthetic createOrReuseLayoutParams(Landroid/view/View;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-virtual {p0, p1}, Landroid/widget/StackView;->createOrReuseLayoutParams(Landroid/view/View;)Landroid/widget/StackView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method createOrReuseLayoutParams(Landroid/view/View;)Landroid/widget/StackView$LayoutParams;
    .registers 6
    .parameter "v"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1081
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v0

    #@5
    .line 1082
    .local v0, currentLp:Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v0, Landroid/widget/StackView$LayoutParams;

    #@7
    if-eqz v2, :cond_17

    #@9
    move-object v1, v0

    #@a
    .line 1083
    check-cast v1, Landroid/widget/StackView$LayoutParams;

    #@c
    .line 1084
    .local v1, lp:Landroid/widget/StackView$LayoutParams;
    invoke-virtual {v1, v3}, Landroid/widget/StackView$LayoutParams;->setHorizontalOffset(I)V

    #@f
    .line 1085
    invoke-virtual {v1, v3}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@12
    .line 1086
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@14
    .line 1087
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@16
    .line 1090
    .end local v1           #lp:Landroid/widget/StackView$LayoutParams;
    :goto_16
    return-object v1

    #@17
    :cond_17
    new-instance v1, Landroid/widget/StackView$LayoutParams;

    #@19
    invoke-direct {v1, p0, p1}, Landroid/widget/StackView$LayoutParams;-><init>(Landroid/widget/StackView;Landroid/view/View;)V

    #@1c
    goto :goto_16
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    #@0
    .prologue
    .line 527
    const/4 v3, 0x0

    #@1
    .line 529
    .local v3, expandClipRegion:Z
    iget-object v6, p0, Landroid/widget/StackView;->stackInvalidateRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    #@6
    .line 530
    invoke-virtual {p0}, Landroid/widget/StackView;->getChildCount()I

    #@9
    move-result v1

    #@a
    .line 531
    .local v1, childCount:I
    const/4 v4, 0x0

    #@b
    .local v4, i:I
    :goto_b
    if-ge v4, v1, :cond_44

    #@d
    .line 532
    invoke-virtual {p0, v4}, Landroid/widget/StackView;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 533
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Landroid/widget/StackView$LayoutParams;

    #@17
    .line 534
    .local v5, lp:Landroid/widget/StackView$LayoutParams;
    iget v6, v5, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@19
    if-nez v6, :cond_1f

    #@1b
    iget v6, v5, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@1d
    if-eqz v6, :cond_2e

    #@1f
    :cond_1f
    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    #@22
    move-result v6

    #@23
    const/4 v7, 0x0

    #@24
    cmpl-float v6, v6, v7

    #@26
    if-eqz v6, :cond_2e

    #@28
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_31

    #@2e
    .line 536
    :cond_2e
    invoke-virtual {v5}, Landroid/widget/StackView$LayoutParams;->resetInvalidateRect()V

    #@31
    .line 538
    :cond_31
    invoke-virtual {v5}, Landroid/widget/StackView$LayoutParams;->getInvalidateRect()Landroid/graphics/Rect;

    #@34
    move-result-object v2

    #@35
    .line 539
    .local v2, childInvalidateRect:Landroid/graphics/Rect;
    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    #@38
    move-result v6

    #@39
    if-nez v6, :cond_41

    #@3b
    .line 540
    const/4 v3, 0x1

    #@3c
    .line 541
    iget-object v6, p0, Landroid/widget/StackView;->stackInvalidateRect:Landroid/graphics/Rect;

    #@3e
    invoke-virtual {v6, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@41
    .line 531
    :cond_41
    add-int/lit8 v4, v4, 0x1

    #@43
    goto :goto_b

    #@44
    .line 546
    .end local v0           #child:Landroid/view/View;
    .end local v2           #childInvalidateRect:Landroid/graphics/Rect;
    .end local v5           #lp:Landroid/widget/StackView$LayoutParams;
    :cond_44
    if-eqz v3, :cond_58

    #@46
    .line 547
    const/4 v6, 0x2

    #@47
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->save(I)I

    #@4a
    .line 548
    iget-object v6, p0, Landroid/widget/StackView;->stackInvalidateRect:Landroid/graphics/Rect;

    #@4c
    sget-object v7, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@4e
    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@51
    .line 549
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@54
    .line 550
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@57
    .line 554
    :goto_57
    return-void

    #@58
    .line 552
    :cond_58
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@5b
    goto :goto_57
.end method

.method getFrameForChild()Landroid/widget/FrameLayout;
    .registers 6

    #@0
    .prologue
    .line 514
    new-instance v0, Landroid/widget/StackView$StackFrame;

    #@2
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Landroid/widget/StackView$StackFrame;-><init>(Landroid/content/Context;)V

    #@7
    .line 515
    .local v0, fl:Landroid/widget/StackView$StackFrame;
    iget v1, p0, Landroid/widget/StackView;->mFramePadding:I

    #@9
    iget v2, p0, Landroid/widget/StackView;->mFramePadding:I

    #@b
    iget v3, p0, Landroid/widget/StackView;->mFramePadding:I

    #@d
    iget v4, p0, Landroid/widget/StackView;->mFramePadding:I

    #@f
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/StackView$StackFrame;->setPadding(IIII)V

    #@12
    .line 516
    return-object v0
.end method

.method hideTapFeedback(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 460
    iget-object v0, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6
    .line 461
    invoke-virtual {p0}, Landroid/widget/StackView;->invalidate()V

    #@9
    .line 462
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 579
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@5
    move-result v2

    #@6
    and-int/lit8 v2, v2, 0x2

    #@8
    if-eqz v2, :cond_11

    #@a
    .line 580
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@d
    move-result v2

    #@e
    packed-switch v2, :pswitch_data_2e

    #@11
    .line 593
    :cond_11
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@14
    move-result v1

    #@15
    :goto_15
    return v1

    #@16
    .line 582
    :pswitch_16
    const/16 v2, 0x9

    #@18
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@1b
    move-result v0

    #@1c
    .line 583
    .local v0, vscroll:F
    cmpg-float v2, v0, v3

    #@1e
    if-gez v2, :cond_25

    #@20
    .line 584
    const/4 v2, 0x0

    #@21
    invoke-direct {p0, v2}, Landroid/widget/StackView;->pacedScroll(Z)V

    #@24
    goto :goto_15

    #@25
    .line 586
    :cond_25
    cmpl-float v2, v0, v3

    #@27
    if-lez v2, :cond_11

    #@29
    .line 587
    invoke-direct {p0, v1}, Landroid/widget/StackView;->pacedScroll(Z)V

    #@2c
    goto :goto_15

    #@2d
    .line 580
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x8
        :pswitch_16
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1224
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1225
    const-class v0, Landroid/widget/StackView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1226
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1230
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@4
    .line 1231
    const-class v1, Landroid/widget/StackView;

    #@6
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@d
    .line 1232
    invoke-virtual {p0}, Landroid/widget/StackView;->getChildCount()I

    #@10
    move-result v1

    #@11
    if-le v1, v0, :cond_39

    #@13
    :goto_13
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    #@16
    .line 1233
    invoke-virtual {p0}, Landroid/widget/StackView;->isEnabled()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_38

    #@1c
    .line 1234
    invoke-virtual {p0}, Landroid/widget/StackView;->getDisplayedChild()I

    #@1f
    move-result v0

    #@20
    invoke-virtual {p0}, Landroid/widget/StackView;->getChildCount()I

    #@23
    move-result v1

    #@24
    add-int/lit8 v1, v1, -0x1

    #@26
    if-ge v0, v1, :cond_2d

    #@28
    .line 1235
    const/16 v0, 0x1000

    #@2a
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@2d
    .line 1237
    :cond_2d
    invoke-virtual {p0}, Landroid/widget/StackView;->getDisplayedChild()I

    #@30
    move-result v0

    #@31
    if-lez v0, :cond_38

    #@33
    .line 1238
    const/16 v0, 0x2000

    #@35
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@38
    .line 1241
    :cond_38
    return-void

    #@39
    .line 1232
    :cond_39
    const/4 v0, 0x0

    #@3a
    goto :goto_13
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter "ev"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 614
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@5
    move-result v0

    #@6
    .line 615
    .local v0, action:I
    and-int/lit16 v5, v0, 0xff

    #@8
    packed-switch v5, :pswitch_data_4e

    #@b
    .line 648
    :cond_b
    :goto_b
    :pswitch_b
    iget v5, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@d
    if-eqz v5, :cond_10

    #@f
    const/4 v4, 0x1

    #@10
    :cond_10
    :goto_10
    return v4

    #@11
    .line 617
    :pswitch_11
    iget v5, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@13
    if-ne v5, v6, :cond_b

    #@15
    .line 618
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@18
    move-result v5

    #@19
    iput v5, p0, Landroid/widget/StackView;->mInitialX:F

    #@1b
    .line 619
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1e
    move-result v5

    #@1f
    iput v5, p0, Landroid/widget/StackView;->mInitialY:F

    #@21
    .line 620
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@24
    move-result v5

    #@25
    iput v5, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@27
    goto :goto_b

    #@28
    .line 625
    :pswitch_28
    iget v5, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@2a
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@2d
    move-result v3

    #@2e
    .line 626
    .local v3, pointerIndex:I
    if-ne v3, v6, :cond_38

    #@30
    .line 628
    const-string v5, "StackView"

    #@32
    const-string v6, "Error: No data for our primary pointer."

    #@34
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_10

    #@38
    .line 631
    :cond_38
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    #@3b
    move-result v2

    #@3c
    .line 632
    .local v2, newY:F
    iget v5, p0, Landroid/widget/StackView;->mInitialY:F

    #@3e
    sub-float v1, v2, v5

    #@40
    .line 634
    .local v1, deltaY:F
    invoke-direct {p0, v1}, Landroid/widget/StackView;->beginGestureIfNeeded(F)V

    #@43
    goto :goto_b

    #@44
    .line 638
    .end local v1           #deltaY:F
    .end local v2           #newY:F
    .end local v3           #pointerIndex:I
    :pswitch_44
    invoke-direct {p0, p1}, Landroid/widget/StackView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@47
    goto :goto_b

    #@48
    .line 643
    :pswitch_48
    iput v6, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@4a
    .line 644
    iput v4, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@4c
    goto :goto_b

    #@4d
    .line 615
    nop

    #@4e
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_11
        :pswitch_48
        :pswitch_28
        :pswitch_48
        :pswitch_b
        :pswitch_b
        :pswitch_44
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 16
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1095
    invoke-virtual {p0}, Landroid/widget/StackView;->checkForAndHandleDataChanged()V

    #@3
    .line 1097
    invoke-virtual {p0}, Landroid/widget/StackView;->getChildCount()I

    #@6
    move-result v2

    #@7
    .line 1098
    .local v2, childCount:I
    const/4 v4, 0x0

    #@8
    .local v4, i:I
    :goto_8
    if-ge v4, v2, :cond_3a

    #@a
    .line 1099
    invoke-virtual {p0, v4}, Landroid/widget/StackView;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 1101
    .local v0, child:Landroid/view/View;
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@10
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@13
    move-result v7

    #@14
    add-int v3, v6, v7

    #@16
    .line 1102
    .local v3, childRight:I
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@18
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@1b
    move-result v7

    #@1c
    add-int v1, v6, v7

    #@1e
    .line 1103
    .local v1, childBottom:I
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Landroid/widget/StackView$LayoutParams;

    #@24
    .line 1105
    .local v5, lp:Landroid/widget/StackView$LayoutParams;
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@26
    iget v7, v5, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@28
    add-int/2addr v6, v7

    #@29
    iget v7, p0, Landroid/view/View;->mPaddingTop:I

    #@2b
    iget v8, v5, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@2d
    add-int/2addr v7, v8

    #@2e
    iget v8, v5, Landroid/widget/StackView$LayoutParams;->horizontalOffset:I

    #@30
    add-int/2addr v8, v3

    #@31
    iget v9, v5, Landroid/widget/StackView$LayoutParams;->verticalOffset:I

    #@33
    add-int/2addr v9, v1

    #@34
    invoke-virtual {v0, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    #@37
    .line 1098
    add-int/lit8 v4, v4, 0x1

    #@39
    goto :goto_8

    #@3a
    .line 1109
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childBottom:I
    .end local v3           #childRight:I
    .end local v5           #lp:Landroid/widget/StackView$LayoutParams;
    :cond_3a
    invoke-direct {p0}, Landroid/widget/StackView;->onLayout()V

    #@3d
    .line 1110
    return-void
.end method

.method protected onMeasure(II)V
    .registers 14
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 1172
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@3
    move-result v8

    #@4
    .line 1173
    .local v8, widthSpecSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@7
    move-result v5

    #@8
    .line 1174
    .local v5, heightSpecSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v7

    #@c
    .line 1175
    .local v7, widthSpecMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@f
    move-result v4

    #@10
    .line 1177
    .local v4, heightSpecMode:I
    iget v9, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@12
    const/4 v10, -0x1

    #@13
    if-eq v9, v10, :cond_54

    #@15
    iget v9, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@17
    const/4 v10, -0x1

    #@18
    if-eq v9, v10, :cond_54

    #@1a
    const/4 v2, 0x1

    #@1b
    .line 1181
    .local v2, haveChildRefSize:Z
    :goto_1b
    const v1, 0x3f8e38e4

    #@1e
    .line 1182
    .local v1, factorY:F
    if-nez v4, :cond_58

    #@20
    .line 1183
    if-eqz v2, :cond_56

    #@22
    iget v9, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@24
    int-to-float v9, v9

    #@25
    const/high16 v10, 0x3f80

    #@27
    add-float/2addr v10, v1

    #@28
    mul-float/2addr v9, v10

    #@29
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@2c
    move-result v9

    #@2d
    iget v10, p0, Landroid/view/View;->mPaddingTop:I

    #@2f
    add-int/2addr v9, v10

    #@30
    iget v10, p0, Landroid/view/View;->mPaddingBottom:I

    #@32
    add-int v5, v9, v10

    #@34
    .line 1201
    :cond_34
    :goto_34
    const v0, 0x3f8e38e4

    #@37
    .line 1202
    .local v0, factorX:F
    if-nez v7, :cond_7c

    #@39
    .line 1203
    if-eqz v2, :cond_7a

    #@3b
    iget v9, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@3d
    int-to-float v9, v9

    #@3e
    const/high16 v10, 0x3f80

    #@40
    add-float/2addr v10, v0

    #@41
    mul-float/2addr v9, v10

    #@42
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@45
    move-result v9

    #@46
    iget v10, p0, Landroid/view/View;->mPaddingLeft:I

    #@48
    add-int/2addr v9, v10

    #@49
    iget v10, p0, Landroid/view/View;->mPaddingRight:I

    #@4b
    add-int v8, v9, v10

    #@4d
    .line 1218
    :cond_4d
    :goto_4d
    invoke-virtual {p0, v8, v5}, Landroid/widget/StackView;->setMeasuredDimension(II)V

    #@50
    .line 1219
    invoke-direct {p0}, Landroid/widget/StackView;->measureChildren()V

    #@53
    .line 1220
    return-void

    #@54
    .line 1177
    .end local v0           #factorX:F
    .end local v1           #factorY:F
    .end local v2           #haveChildRefSize:Z
    :cond_54
    const/4 v2, 0x0

    #@55
    goto :goto_1b

    #@56
    .line 1183
    .restart local v1       #factorY:F
    .restart local v2       #haveChildRefSize:Z
    :cond_56
    const/4 v5, 0x0

    #@57
    goto :goto_34

    #@58
    .line 1186
    :cond_58
    const/high16 v9, -0x8000

    #@5a
    if-ne v4, v9, :cond_34

    #@5c
    .line 1187
    if-eqz v2, :cond_78

    #@5e
    .line 1188
    iget v9, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildHeight:I

    #@60
    int-to-float v9, v9

    #@61
    const/high16 v10, 0x3f80

    #@63
    add-float/2addr v10, v1

    #@64
    mul-float/2addr v9, v10

    #@65
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    #@68
    move-result v9

    #@69
    iget v10, p0, Landroid/view/View;->mPaddingTop:I

    #@6b
    add-int/2addr v9, v10

    #@6c
    iget v10, p0, Landroid/view/View;->mPaddingBottom:I

    #@6e
    add-int v3, v9, v10

    #@70
    .line 1190
    .local v3, height:I
    if-gt v3, v5, :cond_74

    #@72
    .line 1191
    move v5, v3

    #@73
    goto :goto_34

    #@74
    .line 1193
    :cond_74
    const/high16 v9, 0x100

    #@76
    or-int/2addr v5, v9

    #@77
    goto :goto_34

    #@78
    .line 1197
    .end local v3           #height:I
    :cond_78
    const/4 v5, 0x0

    #@79
    goto :goto_34

    #@7a
    .line 1203
    .restart local v0       #factorX:F
    :cond_7a
    const/4 v8, 0x0

    #@7b
    goto :goto_4d

    #@7c
    .line 1206
    :cond_7c
    const/high16 v9, -0x8000

    #@7e
    if-ne v4, v9, :cond_4d

    #@80
    .line 1207
    if-eqz v2, :cond_93

    #@82
    .line 1208
    iget v9, p0, Landroid/widget/AdapterViewAnimator;->mReferenceChildWidth:I

    #@84
    iget v10, p0, Landroid/view/View;->mPaddingLeft:I

    #@86
    add-int/2addr v9, v10

    #@87
    iget v10, p0, Landroid/view/View;->mPaddingRight:I

    #@89
    add-int v6, v9, v10

    #@8b
    .line 1209
    .local v6, width:I
    if-gt v6, v8, :cond_8f

    #@8d
    .line 1210
    move v8, v6

    #@8e
    goto :goto_4d

    #@8f
    .line 1212
    :cond_8f
    const/high16 v9, 0x100

    #@91
    or-int/2addr v8, v9

    #@92
    goto :goto_4d

    #@93
    .line 1215
    .end local v6           #width:I
    :cond_93
    const/4 v8, 0x0

    #@94
    goto :goto_4d
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "ev"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v12, -0x1

    #@2
    const/4 v9, 0x1

    #@3
    const/high16 v11, 0x3f80

    #@5
    .line 704
    invoke-super {p0, p1}, Landroid/widget/AdapterViewAnimator;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@8
    .line 706
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@b
    move-result v0

    #@c
    .line 707
    .local v0, action:I
    iget v10, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@e
    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@11
    move-result v5

    #@12
    .line 708
    .local v5, pointerIndex:I
    if-ne v5, v12, :cond_1c

    #@14
    .line 710
    const-string v9, "StackView"

    #@16
    const-string v10, "Error: No data for our primary pointer."

    #@18
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 757
    :goto_1b
    return v8

    #@1c
    .line 714
    :cond_1c
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    #@1f
    move-result v4

    #@20
    .line 715
    .local v4, newY:F
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    #@23
    move-result v3

    #@24
    .line 716
    .local v3, newX:F
    iget v10, p0, Landroid/widget/StackView;->mInitialY:F

    #@26
    sub-float v2, v4, v10

    #@28
    .line 717
    .local v2, deltaY:F
    iget v10, p0, Landroid/widget/StackView;->mInitialX:F

    #@2a
    sub-float v1, v3, v10

    #@2c
    .line 718
    .local v1, deltaX:F
    iget-object v10, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2e
    if-nez v10, :cond_36

    #@30
    .line 719
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@33
    move-result-object v10

    #@34
    iput-object v10, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@36
    .line 721
    :cond_36
    iget-object v10, p0, Landroid/widget/StackView;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@38
    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@3b
    .line 723
    and-int/lit16 v10, v0, 0xff

    #@3d
    packed-switch v10, :pswitch_data_a0

    #@40
    :cond_40
    :goto_40
    :pswitch_40
    move v8, v9

    #@41
    .line 757
    goto :goto_1b

    #@42
    .line 725
    :pswitch_42
    invoke-direct {p0, v2}, Landroid/widget/StackView;->beginGestureIfNeeded(F)V

    #@45
    .line 727
    iget v8, p0, Landroid/widget/StackView;->mSlideAmount:I

    #@47
    int-to-float v8, v8

    #@48
    mul-float/2addr v8, v11

    #@49
    div-float v7, v1, v8

    #@4b
    .line 728
    .local v7, rx:F
    iget v8, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@4d
    const/4 v10, 0x2

    #@4e
    if-ne v8, v10, :cond_70

    #@50
    .line 729
    iget v8, p0, Landroid/widget/StackView;->mTouchSlop:I

    #@52
    int-to-float v8, v8

    #@53
    mul-float/2addr v8, v11

    #@54
    sub-float v8, v2, v8

    #@56
    iget v10, p0, Landroid/widget/StackView;->mSlideAmount:I

    #@58
    int-to-float v10, v10

    #@59
    div-float/2addr v8, v10

    #@5a
    mul-float v6, v8, v11

    #@5c
    .line 730
    .local v6, r:F
    iget v8, p0, Landroid/widget/StackView;->mStackMode:I

    #@5e
    if-ne v8, v9, :cond_62

    #@60
    sub-float v6, v11, v6

    #@62
    .line 731
    :cond_62
    iget-object v8, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@64
    sub-float v10, v11, v6

    #@66
    invoke-virtual {v8, v10}, Landroid/widget/StackView$StackSlider;->setYProgress(F)V

    #@69
    .line 732
    iget-object v8, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@6b
    invoke-virtual {v8, v7}, Landroid/widget/StackView$StackSlider;->setXProgress(F)V

    #@6e
    move v8, v9

    #@6f
    .line 733
    goto :goto_1b

    #@70
    .line 734
    .end local v6           #r:F
    :cond_70
    iget v8, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@72
    if-ne v8, v9, :cond_40

    #@74
    .line 735
    iget v8, p0, Landroid/widget/StackView;->mTouchSlop:I

    #@76
    int-to-float v8, v8

    #@77
    mul-float/2addr v8, v11

    #@78
    add-float/2addr v8, v2

    #@79
    neg-float v8, v8

    #@7a
    iget v10, p0, Landroid/widget/StackView;->mSlideAmount:I

    #@7c
    int-to-float v10, v10

    #@7d
    div-float/2addr v8, v10

    #@7e
    mul-float v6, v8, v11

    #@80
    .line 736
    .restart local v6       #r:F
    iget v8, p0, Landroid/widget/StackView;->mStackMode:I

    #@82
    if-ne v8, v9, :cond_86

    #@84
    sub-float v6, v11, v6

    #@86
    .line 737
    :cond_86
    iget-object v8, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@88
    invoke-virtual {v8, v6}, Landroid/widget/StackView$StackSlider;->setYProgress(F)V

    #@8b
    .line 738
    iget-object v8, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@8d
    invoke-virtual {v8, v7}, Landroid/widget/StackView$StackSlider;->setXProgress(F)V

    #@90
    move v8, v9

    #@91
    .line 739
    goto :goto_1b

    #@92
    .line 744
    .end local v6           #r:F
    .end local v7           #rx:F
    :pswitch_92
    invoke-direct {p0, p1}, Landroid/widget/StackView;->handlePointerUp(Landroid/view/MotionEvent;)V

    #@95
    goto :goto_40

    #@96
    .line 748
    :pswitch_96
    invoke-direct {p0, p1}, Landroid/widget/StackView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    #@99
    goto :goto_40

    #@9a
    .line 752
    :pswitch_9a
    iput v12, p0, Landroid/widget/StackView;->mActivePointerId:I

    #@9c
    .line 753
    iput v8, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@9e
    goto :goto_40

    #@9f
    .line 723
    nop

    #@a0
    :pswitch_data_a0
    .packed-switch 0x1
        :pswitch_92
        :pswitch_42
        :pswitch_9a
        :pswitch_40
        :pswitch_40
        :pswitch_96
    .end packed-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 7
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1245
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterViewAnimator;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_9

    #@8
    .line 1265
    :goto_8
    return v0

    #@9
    .line 1248
    :cond_9
    invoke-virtual {p0}, Landroid/widget/StackView;->isEnabled()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_11

    #@f
    move v0, v1

    #@10
    .line 1249
    goto :goto_8

    #@11
    .line 1251
    :cond_11
    sparse-switch p1, :sswitch_data_34

    #@14
    move v0, v1

    #@15
    .line 1265
    goto :goto_8

    #@16
    .line 1253
    :sswitch_16
    invoke-virtual {p0}, Landroid/widget/StackView;->getDisplayedChild()I

    #@19
    move-result v2

    #@1a
    invoke-virtual {p0}, Landroid/widget/StackView;->getChildCount()I

    #@1d
    move-result v3

    #@1e
    add-int/lit8 v3, v3, -0x1

    #@20
    if-ge v2, v3, :cond_26

    #@22
    .line 1254
    invoke-virtual {p0}, Landroid/widget/StackView;->showNext()V

    #@25
    goto :goto_8

    #@26
    :cond_26
    move v0, v1

    #@27
    .line 1257
    goto :goto_8

    #@28
    .line 1259
    :sswitch_28
    invoke-virtual {p0}, Landroid/widget/StackView;->getDisplayedChild()I

    #@2b
    move-result v2

    #@2c
    if-lez v2, :cond_32

    #@2e
    .line 1260
    invoke-virtual {p0}, Landroid/widget/StackView;->showPrevious()V

    #@31
    goto :goto_8

    #@32
    :cond_32
    move v0, v1

    #@33
    .line 1263
    goto :goto_8

    #@34
    .line 1251
    :sswitch_data_34
    .sparse-switch
        0x1000 -> :sswitch_16
        0x2000 -> :sswitch_28
    .end sparse-switch
.end method

.method public showNext()V
    .registers 4
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 387
    iget v1, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 397
    :goto_5
    return-void

    #@6
    .line 388
    :cond_6
    iget-boolean v1, p0, Landroid/widget/StackView;->mTransitionIsSetup:Z

    #@8
    if-nez v1, :cond_1f

    #@a
    .line 389
    const/4 v1, 0x1

    #@b
    invoke-virtual {p0, v1}, Landroid/widget/StackView;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    .line 390
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_1f

    #@11
    .line 391
    const/4 v1, 0x0

    #@12
    invoke-direct {p0, v0, v1}, Landroid/widget/StackView;->setupStackSlider(Landroid/view/View;I)V

    #@15
    .line 392
    iget-object v1, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@17
    invoke-virtual {v1, v2}, Landroid/widget/StackView$StackSlider;->setYProgress(F)V

    #@1a
    .line 393
    iget-object v1, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@1c
    invoke-virtual {v1, v2}, Landroid/widget/StackView$StackSlider;->setXProgress(F)V

    #@1f
    .line 396
    .end local v0           #v:Landroid/view/View;
    :cond_1f
    invoke-super {p0}, Landroid/widget/AdapterViewAnimator;->showNext()V

    #@22
    goto :goto_5
.end method

.method showOnly(IZ)V
    .registers 10
    .parameter "childIndex"
    .parameter "animate"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 419
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterViewAnimator;->showOnly(IZ)V

    #@4
    .line 422
    iget v0, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowEnd:I

    #@6
    .local v0, i:I
    :goto_6
    iget v4, p0, Landroid/widget/AdapterViewAnimator;->mCurrentWindowStart:I

    #@8
    if-lt v0, v4, :cond_36

    #@a
    .line 423
    invoke-virtual {p0}, Landroid/widget/StackView;->getWindowSize()I

    #@d
    move-result v4

    #@e
    invoke-virtual {p0, v0, v4}, Landroid/widget/StackView;->modulo(II)I

    #@11
    move-result v1

    #@12
    .line 424
    .local v1, index:I
    iget-object v4, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@14
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    check-cast v3, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@1e
    .line 425
    .local v3, vm:Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    if-eqz v3, :cond_33

    #@20
    .line 426
    iget-object v4, p0, Landroid/widget/AdapterViewAnimator;->mViewsMap:Ljava/util/HashMap;

    #@22
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    move-result-object v4

    #@2a
    check-cast v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;

    #@2c
    iget-object v2, v4, Landroid/widget/AdapterViewAnimator$ViewAndMetaData;->view:Landroid/view/View;

    #@2e
    .line 427
    .local v2, v:Landroid/view/View;
    if-eqz v2, :cond_33

    #@30
    invoke-virtual {v2}, Landroid/view/View;->bringToFront()V

    #@33
    .line 422
    .end local v2           #v:Landroid/view/View;
    :cond_33
    add-int/lit8 v0, v0, -0x1

    #@35
    goto :goto_6

    #@36
    .line 430
    .end local v1           #index:I
    .end local v3           #vm:Landroid/widget/AdapterViewAnimator$ViewAndMetaData;
    :cond_36
    iget-object v4, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@38
    if-eqz v4, :cond_3f

    #@3a
    .line 431
    iget-object v4, p0, Landroid/widget/StackView;->mHighlight:Landroid/widget/ImageView;

    #@3c
    invoke-virtual {v4}, Landroid/widget/ImageView;->bringToFront()V

    #@3f
    .line 433
    :cond_3f
    iput-boolean v6, p0, Landroid/widget/StackView;->mTransitionIsSetup:Z

    #@41
    .line 434
    iput-boolean v6, p0, Landroid/widget/StackView;->mClickFeedbackIsValid:Z

    #@43
    .line 435
    return-void
.end method

.method public showPrevious()V
    .registers 4
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 405
    iget v1, p0, Landroid/widget/StackView;->mSwipeGestureType:I

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 415
    :goto_5
    return-void

    #@6
    .line 406
    :cond_6
    iget-boolean v1, p0, Landroid/widget/StackView;->mTransitionIsSetup:Z

    #@8
    if-nez v1, :cond_20

    #@a
    .line 407
    invoke-virtual {p0, v2}, Landroid/widget/StackView;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 408
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_20

    #@10
    .line 409
    invoke-direct {p0, v0, v2}, Landroid/widget/StackView;->setupStackSlider(Landroid/view/View;I)V

    #@13
    .line 410
    iget-object v1, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@15
    const/high16 v2, 0x3f80

    #@17
    invoke-virtual {v1, v2}, Landroid/widget/StackView$StackSlider;->setYProgress(F)V

    #@1a
    .line 411
    iget-object v1, p0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@1c
    const/4 v2, 0x0

    #@1d
    invoke-virtual {v1, v2}, Landroid/widget/StackView$StackSlider;->setXProgress(F)V

    #@20
    .line 414
    .end local v0           #v:Landroid/view/View;
    :cond_20
    invoke-super {p0}, Landroid/widget/AdapterViewAnimator;->showPrevious()V

    #@23
    goto :goto_5
.end method

.method showTapFeedback(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 452
    invoke-virtual {p0}, Landroid/widget/StackView;->updateClickFeedback()V

    #@3
    .line 453
    iget-object v0, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@9
    .line 454
    iget-object v0, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@b
    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    #@e
    .line 455
    invoke-virtual {p0}, Landroid/widget/StackView;->invalidate()V

    #@11
    .line 456
    return-void
.end method

.method transformViewForTransition(IILandroid/view/View;Z)V
    .registers 22
    .parameter "fromIndex"
    .parameter "toIndex"
    .parameter "view"
    .parameter "animate"

    #@0
    .prologue
    .line 227
    if-nez p4, :cond_1d

    #@2
    move-object/from16 v13, p3

    #@4
    .line 228
    check-cast v13, Landroid/widget/StackView$StackFrame;

    #@6
    invoke-virtual {v13}, Landroid/widget/StackView$StackFrame;->cancelSliderAnimator()Z

    #@9
    .line 229
    const/4 v13, 0x0

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v13}, Landroid/view/View;->setRotationX(F)V

    #@f
    .line 230
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@12
    move-result-object v6

    #@13
    check-cast v6, Landroid/widget/StackView$LayoutParams;

    #@15
    .line 231
    .local v6, lp:Landroid/widget/StackView$LayoutParams;
    const/4 v13, 0x0

    #@16
    invoke-virtual {v6, v13}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@19
    .line 232
    const/4 v13, 0x0

    #@1a
    invoke-virtual {v6, v13}, Landroid/widget/StackView$LayoutParams;->setHorizontalOffset(I)V

    #@1d
    .line 235
    .end local v6           #lp:Landroid/widget/StackView$LayoutParams;
    :cond_1d
    const/4 v13, -0x1

    #@1e
    move/from16 v0, p1

    #@20
    if-ne v0, v13, :cond_54

    #@22
    invoke-virtual/range {p0 .. p0}, Landroid/widget/StackView;->getNumActiveViews()I

    #@25
    move-result v13

    #@26
    add-int/lit8 v13, v13, -0x1

    #@28
    move/from16 v0, p2

    #@2a
    if-ne v0, v13, :cond_54

    #@2c
    .line 236
    const/4 v13, 0x0

    #@2d
    move-object/from16 v0, p0

    #@2f
    move/from16 v1, p2

    #@31
    move-object/from16 v2, p3

    #@33
    invoke-direct {v0, v1, v2, v13}, Landroid/widget/StackView;->transformViewAtIndex(ILandroid/view/View;Z)V

    #@36
    .line 237
    const/4 v13, 0x0

    #@37
    move-object/from16 v0, p3

    #@39
    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    #@3c
    .line 238
    const/high16 v13, 0x3f80

    #@3e
    move-object/from16 v0, p3

    #@40
    invoke-virtual {v0, v13}, Landroid/view/View;->setAlpha(F)V

    #@43
    .line 308
    :cond_43
    :goto_43
    const/4 v13, -0x1

    #@44
    move/from16 v0, p2

    #@46
    if-eq v0, v13, :cond_53

    #@48
    .line 309
    move-object/from16 v0, p0

    #@4a
    move/from16 v1, p2

    #@4c
    move-object/from16 v2, p3

    #@4e
    move/from16 v3, p4

    #@50
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/StackView;->transformViewAtIndex(ILandroid/view/View;Z)V

    #@53
    .line 311
    :cond_53
    return-void

    #@54
    .line 239
    :cond_54
    if-nez p1, :cond_d6

    #@56
    const/4 v13, 0x1

    #@57
    move/from16 v0, p2

    #@59
    if-ne v0, v13, :cond_d6

    #@5b
    move-object/from16 v13, p3

    #@5d
    .line 241
    check-cast v13, Landroid/widget/StackView$StackFrame;

    #@5f
    invoke-virtual {v13}, Landroid/widget/StackView$StackFrame;->cancelSliderAnimator()Z

    #@62
    .line 242
    const/4 v13, 0x0

    #@63
    move-object/from16 v0, p3

    #@65
    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    #@68
    .line 244
    move-object/from16 v0, p0

    #@6a
    iget-object v13, v0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@6c
    move-object/from16 v0, p0

    #@6e
    iget v14, v0, Landroid/widget/StackView;->mYVelocity:I

    #@70
    int-to-float v14, v14

    #@71
    invoke-virtual {v13, v14}, Landroid/widget/StackView$StackSlider;->getDurationForNeutralPosition(F)F

    #@74
    move-result v13

    #@75
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    #@78
    move-result v5

    #@79
    .line 245
    .local v5, duration:I
    new-instance v4, Landroid/widget/StackView$StackSlider;

    #@7b
    move-object/from16 v0, p0

    #@7d
    iget-object v13, v0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@7f
    move-object/from16 v0, p0

    #@81
    invoke-direct {v4, v0, v13}, Landroid/widget/StackView$StackSlider;-><init>(Landroid/widget/StackView;Landroid/widget/StackView$StackSlider;)V

    #@84
    .line 246
    .local v4, animationSlider:Landroid/widget/StackView$StackSlider;
    move-object/from16 v0, p3

    #@86
    invoke-virtual {v4, v0}, Landroid/widget/StackView$StackSlider;->setView(Landroid/view/View;)V

    #@89
    .line 248
    if-eqz p4, :cond_cc

    #@8b
    .line 249
    const-string v13, "YProgress"

    #@8d
    const/4 v14, 0x1

    #@8e
    new-array v14, v14, [F

    #@90
    const/4 v15, 0x0

    #@91
    const/16 v16, 0x0

    #@93
    aput v16, v14, v15

    #@95
    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@98
    move-result-object v9

    #@99
    .line 250
    .local v9, slideInY:Landroid/animation/PropertyValuesHolder;
    const-string v13, "XProgress"

    #@9b
    const/4 v14, 0x1

    #@9c
    new-array v14, v14, [F

    #@9e
    const/4 v15, 0x0

    #@9f
    const/16 v16, 0x0

    #@a1
    aput v16, v14, v15

    #@a3
    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@a6
    move-result-object v8

    #@a7
    .line 251
    .local v8, slideInX:Landroid/animation/PropertyValuesHolder;
    const/4 v13, 0x2

    #@a8
    new-array v13, v13, [Landroid/animation/PropertyValuesHolder;

    #@aa
    const/4 v14, 0x0

    #@ab
    aput-object v8, v13, v14

    #@ad
    const/4 v14, 0x1

    #@ae
    aput-object v9, v13, v14

    #@b0
    invoke-static {v4, v13}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@b3
    move-result-object v7

    #@b4
    .line 253
    .local v7, slideIn:Landroid/animation/ObjectAnimator;
    int-to-long v13, v5

    #@b5
    invoke-virtual {v7, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@b8
    .line 254
    new-instance v13, Landroid/view/animation/LinearInterpolator;

    #@ba
    invoke-direct {v13}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@bd
    invoke-virtual {v7, v13}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@c0
    move-object/from16 v13, p3

    #@c2
    .line 255
    check-cast v13, Landroid/widget/StackView$StackFrame;

    #@c4
    invoke-virtual {v13, v7}, Landroid/widget/StackView$StackFrame;->setSliderAnimator(Landroid/animation/ObjectAnimator;)V

    #@c7
    .line 256
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    #@ca
    goto/16 :goto_43

    #@cc
    .line 258
    .end local v7           #slideIn:Landroid/animation/ObjectAnimator;
    .end local v8           #slideInX:Landroid/animation/PropertyValuesHolder;
    .end local v9           #slideInY:Landroid/animation/PropertyValuesHolder;
    :cond_cc
    const/4 v13, 0x0

    #@cd
    invoke-virtual {v4, v13}, Landroid/widget/StackView$StackSlider;->setYProgress(F)V

    #@d0
    .line 259
    const/4 v13, 0x0

    #@d1
    invoke-virtual {v4, v13}, Landroid/widget/StackView$StackSlider;->setXProgress(F)V

    #@d4
    goto/16 :goto_43

    #@d6
    .line 261
    .end local v4           #animationSlider:Landroid/widget/StackView$StackSlider;
    .end local v5           #duration:I
    :cond_d6
    const/4 v13, 0x1

    #@d7
    move/from16 v0, p1

    #@d9
    if-ne v0, v13, :cond_153

    #@db
    if-nez p2, :cond_153

    #@dd
    move-object/from16 v13, p3

    #@df
    .line 263
    check-cast v13, Landroid/widget/StackView$StackFrame;

    #@e1
    invoke-virtual {v13}, Landroid/widget/StackView$StackFrame;->cancelSliderAnimator()Z

    #@e4
    .line 264
    move-object/from16 v0, p0

    #@e6
    iget-object v13, v0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@e8
    move-object/from16 v0, p0

    #@ea
    iget v14, v0, Landroid/widget/StackView;->mYVelocity:I

    #@ec
    int-to-float v14, v14

    #@ed
    invoke-virtual {v13, v14}, Landroid/widget/StackView$StackSlider;->getDurationForOffscreenPosition(F)F

    #@f0
    move-result v13

    #@f1
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    #@f4
    move-result v5

    #@f5
    .line 266
    .restart local v5       #duration:I
    new-instance v4, Landroid/widget/StackView$StackSlider;

    #@f7
    move-object/from16 v0, p0

    #@f9
    iget-object v13, v0, Landroid/widget/StackView;->mStackSlider:Landroid/widget/StackView$StackSlider;

    #@fb
    move-object/from16 v0, p0

    #@fd
    invoke-direct {v4, v0, v13}, Landroid/widget/StackView$StackSlider;-><init>(Landroid/widget/StackView;Landroid/widget/StackView$StackSlider;)V

    #@100
    .line 267
    .restart local v4       #animationSlider:Landroid/widget/StackView$StackSlider;
    move-object/from16 v0, p3

    #@102
    invoke-virtual {v4, v0}, Landroid/widget/StackView$StackSlider;->setView(Landroid/view/View;)V

    #@105
    .line 268
    if-eqz p4, :cond_148

    #@107
    .line 269
    const-string v13, "YProgress"

    #@109
    const/4 v14, 0x1

    #@10a
    new-array v14, v14, [F

    #@10c
    const/4 v15, 0x0

    #@10d
    const/high16 v16, 0x3f80

    #@10f
    aput v16, v14, v15

    #@111
    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@114
    move-result-object v12

    #@115
    .line 270
    .local v12, slideOutY:Landroid/animation/PropertyValuesHolder;
    const-string v13, "XProgress"

    #@117
    const/4 v14, 0x1

    #@118
    new-array v14, v14, [F

    #@11a
    const/4 v15, 0x0

    #@11b
    const/16 v16, 0x0

    #@11d
    aput v16, v14, v15

    #@11f
    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@122
    move-result-object v11

    #@123
    .line 271
    .local v11, slideOutX:Landroid/animation/PropertyValuesHolder;
    const/4 v13, 0x2

    #@124
    new-array v13, v13, [Landroid/animation/PropertyValuesHolder;

    #@126
    const/4 v14, 0x0

    #@127
    aput-object v11, v13, v14

    #@129
    const/4 v14, 0x1

    #@12a
    aput-object v12, v13, v14

    #@12c
    invoke-static {v4, v13}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@12f
    move-result-object v10

    #@130
    .line 273
    .local v10, slideOut:Landroid/animation/ObjectAnimator;
    int-to-long v13, v5

    #@131
    invoke-virtual {v10, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@134
    .line 274
    new-instance v13, Landroid/view/animation/LinearInterpolator;

    #@136
    invoke-direct {v13}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@139
    invoke-virtual {v10, v13}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@13c
    move-object/from16 v13, p3

    #@13e
    .line 275
    check-cast v13, Landroid/widget/StackView$StackFrame;

    #@140
    invoke-virtual {v13, v10}, Landroid/widget/StackView$StackFrame;->setSliderAnimator(Landroid/animation/ObjectAnimator;)V

    #@143
    .line 276
    invoke-virtual {v10}, Landroid/animation/ObjectAnimator;->start()V

    #@146
    goto/16 :goto_43

    #@148
    .line 278
    .end local v10           #slideOut:Landroid/animation/ObjectAnimator;
    .end local v11           #slideOutX:Landroid/animation/PropertyValuesHolder;
    .end local v12           #slideOutY:Landroid/animation/PropertyValuesHolder;
    :cond_148
    const/high16 v13, 0x3f80

    #@14a
    invoke-virtual {v4, v13}, Landroid/widget/StackView$StackSlider;->setYProgress(F)V

    #@14d
    .line 279
    const/4 v13, 0x0

    #@14e
    invoke-virtual {v4, v13}, Landroid/widget/StackView$StackSlider;->setXProgress(F)V

    #@151
    goto/16 :goto_43

    #@153
    .line 281
    .end local v4           #animationSlider:Landroid/widget/StackView$StackSlider;
    .end local v5           #duration:I
    :cond_153
    if-nez p2, :cond_163

    #@155
    .line 283
    const/4 v13, 0x0

    #@156
    move-object/from16 v0, p3

    #@158
    invoke-virtual {v0, v13}, Landroid/view/View;->setAlpha(F)V

    #@15b
    .line 284
    const/4 v13, 0x4

    #@15c
    move-object/from16 v0, p3

    #@15e
    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    #@161
    goto/16 :goto_43

    #@163
    .line 285
    :cond_163
    if-eqz p1, :cond_16a

    #@165
    const/4 v13, 0x1

    #@166
    move/from16 v0, p1

    #@168
    if-ne v0, v13, :cond_192

    #@16a
    :cond_16a
    const/4 v13, 0x1

    #@16b
    move/from16 v0, p2

    #@16d
    if-le v0, v13, :cond_192

    #@16f
    .line 286
    const/4 v13, 0x0

    #@170
    move-object/from16 v0, p3

    #@172
    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    #@175
    .line 287
    const/high16 v13, 0x3f80

    #@177
    move-object/from16 v0, p3

    #@179
    invoke-virtual {v0, v13}, Landroid/view/View;->setAlpha(F)V

    #@17c
    .line 288
    const/4 v13, 0x0

    #@17d
    move-object/from16 v0, p3

    #@17f
    invoke-virtual {v0, v13}, Landroid/view/View;->setRotationX(F)V

    #@182
    .line 289
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@185
    move-result-object v6

    #@186
    check-cast v6, Landroid/widget/StackView$LayoutParams;

    #@188
    .line 290
    .restart local v6       #lp:Landroid/widget/StackView$LayoutParams;
    const/4 v13, 0x0

    #@189
    invoke-virtual {v6, v13}, Landroid/widget/StackView$LayoutParams;->setVerticalOffset(I)V

    #@18c
    .line 291
    const/4 v13, 0x0

    #@18d
    invoke-virtual {v6, v13}, Landroid/widget/StackView$LayoutParams;->setHorizontalOffset(I)V

    #@190
    goto/16 :goto_43

    #@192
    .line 292
    .end local v6           #lp:Landroid/widget/StackView$LayoutParams;
    :cond_192
    const/4 v13, -0x1

    #@193
    move/from16 v0, p1

    #@195
    if-ne v0, v13, :cond_1a6

    #@197
    .line 293
    const/high16 v13, 0x3f80

    #@199
    move-object/from16 v0, p3

    #@19b
    invoke-virtual {v0, v13}, Landroid/view/View;->setAlpha(F)V

    #@19e
    .line 294
    const/4 v13, 0x0

    #@19f
    move-object/from16 v0, p3

    #@1a1
    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    #@1a4
    goto/16 :goto_43

    #@1a6
    .line 295
    :cond_1a6
    const/4 v13, -0x1

    #@1a7
    move/from16 v0, p2

    #@1a9
    if-ne v0, v13, :cond_43

    #@1ab
    .line 296
    if-eqz p4, :cond_1bf

    #@1ad
    .line 297
    new-instance v13, Landroid/widget/StackView$1;

    #@1af
    move-object/from16 v0, p0

    #@1b1
    move-object/from16 v1, p3

    #@1b3
    invoke-direct {v13, v0, v1}, Landroid/widget/StackView$1;-><init>(Landroid/widget/StackView;Landroid/view/View;)V

    #@1b6
    const-wide/16 v14, 0x64

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    invoke-virtual {v0, v13, v14, v15}, Landroid/widget/StackView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1bd
    goto/16 :goto_43

    #@1bf
    .line 303
    :cond_1bf
    const/4 v13, 0x0

    #@1c0
    move-object/from16 v0, p3

    #@1c2
    invoke-virtual {v0, v13}, Landroid/view/View;->setAlpha(F)V

    #@1c5
    goto/16 :goto_43
.end method

.method updateClickFeedback()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 438
    iget-boolean v1, p0, Landroid/widget/StackView;->mClickFeedbackIsValid:Z

    #@3
    if-nez v1, :cond_2c

    #@5
    .line 439
    invoke-virtual {p0, v4}, Landroid/widget/StackView;->getViewAtRelativeIndex(I)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    .line 440
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_2a

    #@b
    .line 441
    iget-object v1, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@d
    sget-object v2, Landroid/widget/StackView;->sHolographicHelper:Landroid/widget/StackView$HolographicHelper;

    #@f
    iget v3, p0, Landroid/widget/StackView;->mClickColor:I

    #@11
    invoke-virtual {v2, v0, v3}, Landroid/widget/StackView$HolographicHelper;->createClickOutline(Landroid/view/View;I)Landroid/graphics/Bitmap;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@18
    .line 443
    iget-object v1, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@1a
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    #@1d
    move-result v2

    #@1e
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationX(F)V

    #@21
    .line 444
    iget-object v1, p0, Landroid/widget/StackView;->mClickFeedback:Landroid/widget/ImageView;

    #@23
    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    #@26
    move-result v2

    #@27
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    #@2a
    .line 446
    :cond_2a
    iput-boolean v4, p0, Landroid/widget/StackView;->mClickFeedbackIsValid:Z

    #@2c
    .line 448
    .end local v0           #v:Landroid/view/View;
    :cond_2c
    return-void
.end method
