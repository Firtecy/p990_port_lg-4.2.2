.class Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;
.super Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;
.source "AccessibilityIterators.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AccessibilityIterators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LineTextSegmentIterator"
.end annotation


# static fields
.field protected static final DIRECTION_END:I = 0x1

.field protected static final DIRECTION_START:I = -0x1

.field private static sLineInstance:Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;


# instance fields
.field protected mLayout:Landroid/text/Layout;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 30
    invoke-direct {p0}, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;
    .registers 1

    #@0
    .prologue
    .line 39
    sget-object v0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->sLineInstance:Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 40
    new-instance v0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;

    #@6
    invoke-direct {v0}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;-><init>()V

    #@9
    sput-object v0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->sLineInstance:Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;

    #@b
    .line 42
    :cond_b
    sget-object v0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->sLineInstance:Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;

    #@d
    return-object v0
.end method


# virtual methods
.method public following(I)[I
    .registers 11
    .parameter "offset"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 52
    iget-object v6, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@7
    move-result v4

    #@8
    .line 53
    .local v4, textLegth:I
    if-gtz v4, :cond_b

    #@a
    .line 75
    :cond_a
    :goto_a
    return-object v5

    #@b
    .line 56
    :cond_b
    iget-object v6, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@d
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@10
    move-result v6

    #@11
    if-ge p1, v6, :cond_a

    #@13
    .line 60
    if-gez p1, :cond_34

    #@15
    .line 61
    iget-object v6, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@17
    const/4 v7, 0x0

    #@18
    invoke-virtual {v6, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    #@1b
    move-result v2

    #@1c
    .line 70
    .local v2, nextLine:I
    :goto_1c
    iget-object v6, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@1e
    invoke-virtual {v6}, Landroid/text/Layout;->getLineCount()I

    #@21
    move-result v6

    #@22
    if-ge v2, v6, :cond_a

    #@24
    .line 73
    invoke-virtual {p0, v2, v8}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getLineEdgeIndex(II)I

    #@27
    move-result v3

    #@28
    .line 74
    .local v3, start:I
    const/4 v5, 0x1

    #@29
    invoke-virtual {p0, v2, v5}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getLineEdgeIndex(II)I

    #@2c
    move-result v5

    #@2d
    add-int/lit8 v1, v5, 0x1

    #@2f
    .line 75
    .local v1, end:I
    invoke-virtual {p0, v3, v1}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getRange(II)[I

    #@32
    move-result-object v5

    #@33
    goto :goto_a

    #@34
    .line 63
    .end local v1           #end:I
    .end local v2           #nextLine:I
    .end local v3           #start:I
    :cond_34
    iget-object v6, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@36
    invoke-virtual {v6, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@39
    move-result v0

    #@3a
    .line 64
    .local v0, currentLine:I
    invoke-virtual {p0, v0, v8}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getLineEdgeIndex(II)I

    #@3d
    move-result v6

    #@3e
    if-ne v6, p1, :cond_42

    #@40
    .line 65
    move v2, v0

    #@41
    .restart local v2       #nextLine:I
    goto :goto_1c

    #@42
    .line 67
    .end local v2           #nextLine:I
    :cond_42
    add-int/lit8 v2, v0, 0x1

    #@44
    .restart local v2       #nextLine:I
    goto :goto_1c
.end method

.method protected getLineEdgeIndex(II)I
    .registers 5
    .parameter "lineNumber"
    .parameter "direction"

    #@0
    .prologue
    .line 107
    iget-object v1, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@2
    invoke-virtual {v1, p1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@5
    move-result v0

    #@6
    .line 108
    .local v0, paragraphDirection:I
    mul-int v1, p2, v0

    #@8
    if-gez v1, :cond_11

    #@a
    .line 109
    iget-object v1, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@c
    invoke-virtual {v1, p1}, Landroid/text/Layout;->getLineStart(I)I

    #@f
    move-result v1

    #@10
    .line 111
    :goto_10
    return v1

    #@11
    :cond_11
    iget-object v1, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@13
    invoke-virtual {v1, p1}, Landroid/text/Layout;->getLineEnd(I)I

    #@16
    move-result v1

    #@17
    add-int/lit8 v1, v1, -0x1

    #@19
    goto :goto_10
.end method

.method public initialize(Landroid/text/Spannable;Landroid/text/Layout;)V
    .registers 4
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    .line 46
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@6
    .line 47
    iput-object p2, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@8
    .line 48
    return-void
.end method

.method public preceding(I)[I
    .registers 11
    .parameter "offset"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 80
    iget-object v6, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@7
    move-result v4

    #@8
    .line 81
    .local v4, textLegth:I
    if-gtz v4, :cond_b

    #@a
    .line 103
    :cond_a
    :goto_a
    return-object v5

    #@b
    .line 84
    :cond_b
    if-lez p1, :cond_a

    #@d
    .line 88
    iget-object v6, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@f
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@12
    move-result v6

    #@13
    if-le p1, v6, :cond_33

    #@15
    .line 89
    iget-object v6, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@17
    iget-object v7, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@19
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@1c
    move-result v7

    #@1d
    invoke-virtual {v6, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    #@20
    move-result v2

    #@21
    .line 98
    .local v2, previousLine:I
    :goto_21
    if-ltz v2, :cond_a

    #@23
    .line 101
    const/4 v5, -0x1

    #@24
    invoke-virtual {p0, v2, v5}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getLineEdgeIndex(II)I

    #@27
    move-result v3

    #@28
    .line 102
    .local v3, start:I
    invoke-virtual {p0, v2, v8}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getLineEdgeIndex(II)I

    #@2b
    move-result v5

    #@2c
    add-int/lit8 v1, v5, 0x1

    #@2e
    .line 103
    .local v1, end:I
    invoke-virtual {p0, v3, v1}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getRange(II)[I

    #@31
    move-result-object v5

    #@32
    goto :goto_a

    #@33
    .line 91
    .end local v1           #end:I
    .end local v2           #previousLine:I
    .end local v3           #start:I
    :cond_33
    iget-object v6, p0, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->mLayout:Landroid/text/Layout;

    #@35
    invoke-virtual {v6, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@38
    move-result v0

    #@39
    .line 92
    .local v0, currentLine:I
    invoke-virtual {p0, v0, v8}, Landroid/widget/AccessibilityIterators$LineTextSegmentIterator;->getLineEdgeIndex(II)I

    #@3c
    move-result v6

    #@3d
    add-int/lit8 v6, v6, 0x1

    #@3f
    if-ne v6, p1, :cond_43

    #@41
    .line 93
    move v2, v0

    #@42
    .restart local v2       #previousLine:I
    goto :goto_21

    #@43
    .line 95
    .end local v2           #previousLine:I
    :cond_43
    add-int/lit8 v2, v0, -0x1

    #@45
    .restart local v2       #previousLine:I
    goto :goto_21
.end method
