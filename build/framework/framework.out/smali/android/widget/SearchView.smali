.class public Landroid/widget/SearchView;
.super Landroid/widget/LinearLayout;
.source "SearchView.java"

# interfaces
.implements Landroid/view/CollapsibleActionView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/SearchView$SearchAutoComplete;,
        Landroid/widget/SearchView$OnSuggestionListener;,
        Landroid/widget/SearchView$OnCloseListener;,
        Landroid/widget/SearchView$OnQueryTextListener;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final IME_OPTION_NO_MICROPHONE:Ljava/lang/String; = "nm"

.field private static final LOG_TAG:Ljava/lang/String; = "SearchView"


# instance fields
.field private mAppSearchData:Landroid/os/Bundle;

.field private mClearingFocus:Z

.field private mCloseButton:Landroid/widget/ImageView;

.field private mCollapsedImeOptions:I

.field private mDropDownAnchor:Landroid/view/View;

.field private mExpandedInActionView:Z

.field private mIconified:Z

.field private mIconifiedByDefault:Z

.field private mMaxWidth:I

.field private mOldQueryText:Ljava/lang/CharSequence;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

.field private final mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private final mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mOnQueryTextFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mOnSearchClickListener:Landroid/view/View$OnClickListener;

.field private mOnSuggestionListener:Landroid/widget/SearchView$OnSuggestionListener;

.field private final mOutsideDrawablesCache:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field

.field private mQueryHint:Ljava/lang/CharSequence;

.field private mQueryRefinement:Z

.field private mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

.field private mReleaseCursorRunnable:Ljava/lang/Runnable;

.field private mSearchButton:Landroid/view/View;

.field private mSearchEditFrame:Landroid/view/View;

.field private mSearchHintIcon:Landroid/widget/ImageView;

.field private mSearchPlate:Landroid/view/View;

.field private mSearchable:Landroid/app/SearchableInfo;

.field private mShowImeRunnable:Ljava/lang/Runnable;

.field private mSubmitArea:Landroid/view/View;

.field private mSubmitButton:Landroid/view/View;

.field private mSubmitButtonEnabled:Z

.field private mSuggestionsAdapter:Landroid/widget/CursorAdapter;

.field mTextKeyListener:Landroid/view/View$OnKeyListener;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mUpdateDrawableStateRunnable:Ljava/lang/Runnable;

.field private mUserQuery:Ljava/lang/CharSequence;

.field private final mVoiceAppSearchIntent:Landroid/content/Intent;

.field private mVoiceButton:Landroid/view/View;

.field private mVoiceButtonEnabled:Z

.field private final mVoiceWebSearchIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 244
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/SearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 245
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 16
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/high16 v12, 0x1000

    #@2
    const/4 v11, 0x1

    #@3
    const/4 v10, 0x0

    #@4
    const/4 v9, -0x1

    #@5
    .line 248
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@8
    .line 142
    new-instance v7, Landroid/widget/SearchView$1;

    #@a
    invoke-direct {v7, p0}, Landroid/widget/SearchView$1;-><init>(Landroid/widget/SearchView;)V

    #@d
    iput-object v7, p0, Landroid/widget/SearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    #@f
    .line 153
    new-instance v7, Landroid/widget/SearchView$2;

    #@11
    invoke-direct {v7, p0}, Landroid/widget/SearchView$2;-><init>(Landroid/widget/SearchView;)V

    #@14
    iput-object v7, p0, Landroid/widget/SearchView;->mUpdateDrawableStateRunnable:Ljava/lang/Runnable;

    #@16
    .line 159
    new-instance v7, Landroid/widget/SearchView$3;

    #@18
    invoke-direct {v7, p0}, Landroid/widget/SearchView$3;-><init>(Landroid/widget/SearchView;)V

    #@1b
    iput-object v7, p0, Landroid/widget/SearchView;->mReleaseCursorRunnable:Ljava/lang/Runnable;

    #@1d
    .line 173
    new-instance v7, Ljava/util/WeakHashMap;

    #@1f
    invoke-direct {v7}, Ljava/util/WeakHashMap;-><init>()V

    #@22
    iput-object v7, p0, Landroid/widget/SearchView;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@24
    .line 855
    new-instance v7, Landroid/widget/SearchView$6;

    #@26
    invoke-direct {v7, p0}, Landroid/widget/SearchView$6;-><init>(Landroid/widget/SearchView;)V

    #@29
    iput-object v7, p0, Landroid/widget/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@2b
    .line 904
    new-instance v7, Landroid/widget/SearchView$7;

    #@2d
    invoke-direct {v7, p0}, Landroid/widget/SearchView$7;-><init>(Landroid/widget/SearchView;)V

    #@30
    iput-object v7, p0, Landroid/widget/SearchView;->mTextKeyListener:Landroid/view/View$OnKeyListener;

    #@32
    .line 1137
    new-instance v7, Landroid/widget/SearchView$8;

    #@34
    invoke-direct {v7, p0}, Landroid/widget/SearchView$8;-><init>(Landroid/widget/SearchView;)V

    #@37
    iput-object v7, p0, Landroid/widget/SearchView;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@39
    .line 1330
    new-instance v7, Landroid/widget/SearchView$9;

    #@3b
    invoke-direct {v7, p0}, Landroid/widget/SearchView$9;-><init>(Landroid/widget/SearchView;)V

    #@3e
    iput-object v7, p0, Landroid/widget/SearchView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@40
    .line 1341
    new-instance v7, Landroid/widget/SearchView$10;

    #@42
    invoke-direct {v7, p0}, Landroid/widget/SearchView$10;-><init>(Landroid/widget/SearchView;)V

    #@45
    iput-object v7, p0, Landroid/widget/SearchView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@47
    .line 1635
    new-instance v7, Landroid/widget/SearchView$11;

    #@49
    invoke-direct {v7, p0}, Landroid/widget/SearchView$11;-><init>(Landroid/widget/SearchView;)V

    #@4c
    iput-object v7, p0, Landroid/widget/SearchView;->mTextWatcher:Landroid/text/TextWatcher;

    #@4e
    .line 250
    const-string/jumbo v7, "layout_inflater"

    #@51
    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@54
    move-result-object v3

    #@55
    check-cast v3, Landroid/view/LayoutInflater;

    #@57
    .line 252
    .local v3, inflater:Landroid/view/LayoutInflater;
    const v7, 0x10900c4

    #@5a
    invoke-virtual {v3, v7, p0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@5d
    .line 254
    const v7, 0x1020377

    #@60
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@63
    move-result-object v7

    #@64
    iput-object v7, p0, Landroid/widget/SearchView;->mSearchButton:Landroid/view/View;

    #@66
    .line 255
    const v7, 0x102037b

    #@69
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@6c
    move-result-object v7

    #@6d
    check-cast v7, Landroid/widget/SearchView$SearchAutoComplete;

    #@6f
    iput-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@71
    .line 256
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@73
    invoke-virtual {v7, p0}, Landroid/widget/SearchView$SearchAutoComplete;->setSearchView(Landroid/widget/SearchView;)V

    #@76
    .line 258
    const v7, 0x1020378

    #@79
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@7c
    move-result-object v7

    #@7d
    iput-object v7, p0, Landroid/widget/SearchView;->mSearchEditFrame:Landroid/view/View;

    #@7f
    .line 259
    const v7, 0x102037a

    #@82
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@85
    move-result-object v7

    #@86
    iput-object v7, p0, Landroid/widget/SearchView;->mSearchPlate:Landroid/view/View;

    #@88
    .line 260
    const v7, 0x102037d

    #@8b
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@8e
    move-result-object v7

    #@8f
    iput-object v7, p0, Landroid/widget/SearchView;->mSubmitArea:Landroid/view/View;

    #@91
    .line 261
    const v7, 0x102037e

    #@94
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@97
    move-result-object v7

    #@98
    iput-object v7, p0, Landroid/widget/SearchView;->mSubmitButton:Landroid/view/View;

    #@9a
    .line 262
    const v7, 0x102037c

    #@9d
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@a0
    move-result-object v7

    #@a1
    check-cast v7, Landroid/widget/ImageView;

    #@a3
    iput-object v7, p0, Landroid/widget/SearchView;->mCloseButton:Landroid/widget/ImageView;

    #@a5
    .line 263
    const v7, 0x102037f

    #@a8
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@ab
    move-result-object v7

    #@ac
    iput-object v7, p0, Landroid/widget/SearchView;->mVoiceButton:Landroid/view/View;

    #@ae
    .line 264
    const v7, 0x1020379

    #@b1
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@b4
    move-result-object v7

    #@b5
    check-cast v7, Landroid/widget/ImageView;

    #@b7
    iput-object v7, p0, Landroid/widget/SearchView;->mSearchHintIcon:Landroid/widget/ImageView;

    #@b9
    .line 266
    iget-object v7, p0, Landroid/widget/SearchView;->mSearchButton:Landroid/view/View;

    #@bb
    iget-object v8, p0, Landroid/widget/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@bd
    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@c0
    .line 267
    iget-object v7, p0, Landroid/widget/SearchView;->mCloseButton:Landroid/widget/ImageView;

    #@c2
    iget-object v8, p0, Landroid/widget/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@c4
    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@c7
    .line 268
    iget-object v7, p0, Landroid/widget/SearchView;->mSubmitButton:Landroid/view/View;

    #@c9
    iget-object v8, p0, Landroid/widget/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@cb
    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@ce
    .line 269
    iget-object v7, p0, Landroid/widget/SearchView;->mVoiceButton:Landroid/view/View;

    #@d0
    iget-object v8, p0, Landroid/widget/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@d2
    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@d5
    .line 270
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@d7
    iget-object v8, p0, Landroid/widget/SearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@d9
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@dc
    .line 272
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@de
    iget-object v8, p0, Landroid/widget/SearchView;->mTextWatcher:Landroid/text/TextWatcher;

    #@e0
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@e3
    .line 273
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@e5
    iget-object v8, p0, Landroid/widget/SearchView;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    #@e7
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    #@ea
    .line 274
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@ec
    iget-object v8, p0, Landroid/widget/SearchView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@ee
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@f1
    .line 275
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@f3
    iget-object v8, p0, Landroid/widget/SearchView;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@f5
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    #@f8
    .line 276
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@fa
    iget-object v8, p0, Landroid/widget/SearchView;->mTextKeyListener:Landroid/view/View$OnKeyListener;

    #@fc
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@ff
    .line 278
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@101
    new-instance v8, Landroid/widget/SearchView$4;

    #@103
    invoke-direct {v8, p0}, Landroid/widget/SearchView$4;-><init>(Landroid/widget/SearchView;)V

    #@106
    invoke-virtual {v7, v8}, Landroid/widget/SearchView$SearchAutoComplete;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    #@109
    .line 287
    sget-object v7, Lcom/android/internal/R$styleable;->SearchView:[I

    #@10b
    invoke-virtual {p1, p2, v7, v10, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@10e
    move-result-object v0

    #@10f
    .line 288
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v7, 0x3

    #@110
    invoke-virtual {v0, v7, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@113
    move-result v7

    #@114
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    #@117
    .line 289
    invoke-virtual {v0, v10, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@11a
    move-result v5

    #@11b
    .line 290
    .local v5, maxWidth:I
    if-eq v5, v9, :cond_120

    #@11d
    .line 291
    invoke-virtual {p0, v5}, Landroid/widget/SearchView;->setMaxWidth(I)V

    #@120
    .line 293
    :cond_120
    const/4 v7, 0x4

    #@121
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@124
    move-result-object v6

    #@125
    .line 294
    .local v6, queryHint:Ljava/lang/CharSequence;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@128
    move-result v7

    #@129
    if-nez v7, :cond_12e

    #@12b
    .line 295
    invoke-virtual {p0, v6}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    #@12e
    .line 297
    :cond_12e
    const/4 v7, 0x2

    #@12f
    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@132
    move-result v2

    #@133
    .line 298
    .local v2, imeOptions:I
    if-eq v2, v9, :cond_138

    #@135
    .line 299
    invoke-virtual {p0, v2}, Landroid/widget/SearchView;->setImeOptions(I)V

    #@138
    .line 301
    :cond_138
    invoke-virtual {v0, v11, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@13b
    move-result v4

    #@13c
    .line 302
    .local v4, inputType:I
    if-eq v4, v9, :cond_141

    #@13e
    .line 303
    invoke-virtual {p0, v4}, Landroid/widget/SearchView;->setInputType(I)V

    #@141
    .line 306
    :cond_141
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@144
    .line 308
    const/4 v1, 0x1

    #@145
    .line 310
    .local v1, focusable:Z
    sget-object v7, Lcom/android/internal/R$styleable;->View:[I

    #@147
    invoke-virtual {p1, p2, v7, v10, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@14a
    move-result-object v0

    #@14b
    .line 311
    const/16 v7, 0x12

    #@14d
    invoke-virtual {v0, v7, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@150
    move-result v1

    #@151
    .line 312
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@154
    .line 313
    invoke-virtual {p0, v1}, Landroid/widget/SearchView;->setFocusable(Z)V

    #@157
    .line 316
    new-instance v7, Landroid/content/Intent;

    #@159
    const-string v8, "android.speech.action.WEB_SEARCH"

    #@15b
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15e
    iput-object v7, p0, Landroid/widget/SearchView;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@160
    .line 317
    iget-object v7, p0, Landroid/widget/SearchView;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@162
    invoke-virtual {v7, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@165
    .line 318
    iget-object v7, p0, Landroid/widget/SearchView;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@167
    const-string v8, "android.speech.extra.LANGUAGE_MODEL"

    #@169
    const-string/jumbo v9, "web_search"

    #@16c
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@16f
    .line 321
    new-instance v7, Landroid/content/Intent;

    #@171
    const-string v8, "android.speech.action.RECOGNIZE_SPEECH"

    #@173
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@176
    iput-object v7, p0, Landroid/widget/SearchView;->mVoiceAppSearchIntent:Landroid/content/Intent;

    #@178
    .line 322
    iget-object v7, p0, Landroid/widget/SearchView;->mVoiceAppSearchIntent:Landroid/content/Intent;

    #@17a
    invoke-virtual {v7, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@17d
    .line 324
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@17f
    invoke-virtual {v7}, Landroid/widget/SearchView$SearchAutoComplete;->getDropDownAnchor()I

    #@182
    move-result v7

    #@183
    invoke-virtual {p0, v7}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@186
    move-result-object v7

    #@187
    iput-object v7, p0, Landroid/widget/SearchView;->mDropDownAnchor:Landroid/view/View;

    #@189
    .line 325
    iget-object v7, p0, Landroid/widget/SearchView;->mDropDownAnchor:Landroid/view/View;

    #@18b
    if-eqz v7, :cond_197

    #@18d
    .line 326
    iget-object v7, p0, Landroid/widget/SearchView;->mDropDownAnchor:Landroid/view/View;

    #@18f
    new-instance v8, Landroid/widget/SearchView$5;

    #@191
    invoke-direct {v8, p0}, Landroid/widget/SearchView$5;-><init>(Landroid/widget/SearchView;)V

    #@194
    invoke-virtual {v7, v8}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    #@197
    .line 335
    :cond_197
    iget-boolean v7, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@199
    invoke-direct {p0, v7}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@19c
    .line 336
    invoke-direct {p0}, Landroid/widget/SearchView;->updateQueryHint()V

    #@19f
    .line 337
    return-void
.end method

.method static synthetic access$000(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->updateFocusedState()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/widget/SearchView;)Landroid/widget/CursorAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/SearchView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mVoiceButton:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->onVoiceClicked()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/widget/SearchView;)Landroid/widget/SearchView$SearchAutoComplete;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->forceSuggestionQuery()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/widget/SearchView;)Landroid/app/SearchableInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/widget/SearchView;Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SearchView;->onSuggestionsKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Landroid/widget/SearchView;ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SearchView;->launchQuerySearch(ILjava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/widget/SearchView;IILjava/lang/String;)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SearchView;->onItemClicked(IILjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1900(Landroid/widget/SearchView;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1}, Landroid/widget/SearchView;->onItemSelected(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Landroid/widget/SearchView;)Landroid/view/View$OnFocusChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mOnQueryTextFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Landroid/widget/SearchView;Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1}, Landroid/widget/SearchView;->onTextChanged(Ljava/lang/CharSequence;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Landroid/widget/SearchView;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1}, Landroid/widget/SearchView;->setImeVisibility(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->adjustDropDownSizeAndPosition()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/widget/SearchView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mSearchButton:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->onSearchClicked()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/widget/SearchView;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mCloseButton:Landroid/widget/ImageView;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->onCloseClicked()V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/widget/SearchView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/widget/SearchView;->mSubmitButton:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/SearchView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/SearchView;->onSubmitQuery()V

    #@3
    return-void
.end method

.method private adjustDropDownSizeAndPosition()V
    .registers 10

    #@0
    .prologue
    .line 1287
    iget-object v7, p0, Landroid/widget/SearchView;->mDropDownAnchor:Landroid/view/View;

    #@2
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    #@5
    move-result v7

    #@6
    const/4 v8, 0x1

    #@7
    if-le v7, v8, :cond_5b

    #@9
    .line 1288
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@c
    move-result-object v7

    #@d
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v5

    #@11
    .line 1289
    .local v5, res:Landroid/content/res/Resources;
    iget-object v7, p0, Landroid/widget/SearchView;->mSearchPlate:Landroid/view/View;

    #@13
    invoke-virtual {v7}, Landroid/view/View;->getPaddingLeft()I

    #@16
    move-result v0

    #@17
    .line 1290
    .local v0, anchorPadding:I
    new-instance v1, Landroid/graphics/Rect;

    #@19
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@1c
    .line 1291
    .local v1, dropDownPadding:Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/widget/SearchView;->isLayoutRtl()Z

    #@1f
    move-result v3

    #@20
    .line 1292
    .local v3, isLayoutRtl:Z
    iget-boolean v7, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@22
    if-eqz v7, :cond_5c

    #@24
    const v7, 0x105004d

    #@27
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@2a
    move-result v7

    #@2b
    const v8, 0x105004b

    #@2e
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@31
    move-result v8

    #@32
    add-int v2, v7, v8

    #@34
    .line 1296
    .local v2, iconOffset:I
    :goto_34
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@36
    invoke-virtual {v7}, Landroid/widget/SearchView$SearchAutoComplete;->getDropDownBackground()Landroid/graphics/drawable/Drawable;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@3d
    .line 1298
    if-eqz v3, :cond_5e

    #@3f
    .line 1299
    iget v7, v1, Landroid/graphics/Rect;->left:I

    #@41
    neg-int v4, v7

    #@42
    .line 1303
    .local v4, offset:I
    :goto_42
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@44
    invoke-virtual {v7, v4}, Landroid/widget/SearchView$SearchAutoComplete;->setDropDownHorizontalOffset(I)V

    #@47
    .line 1304
    iget-object v7, p0, Landroid/widget/SearchView;->mDropDownAnchor:Landroid/view/View;

    #@49
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    #@4c
    move-result v7

    #@4d
    iget v8, v1, Landroid/graphics/Rect;->left:I

    #@4f
    add-int/2addr v7, v8

    #@50
    iget v8, v1, Landroid/graphics/Rect;->right:I

    #@52
    add-int/2addr v7, v8

    #@53
    add-int/2addr v7, v2

    #@54
    sub-int v6, v7, v0

    #@56
    .line 1306
    .local v6, width:I
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@58
    invoke-virtual {v7, v6}, Landroid/widget/SearchView$SearchAutoComplete;->setDropDownWidth(I)V

    #@5b
    .line 1308
    .end local v0           #anchorPadding:I
    .end local v1           #dropDownPadding:Landroid/graphics/Rect;
    .end local v2           #iconOffset:I
    .end local v3           #isLayoutRtl:Z
    .end local v4           #offset:I
    .end local v5           #res:Landroid/content/res/Resources;
    .end local v6           #width:I
    :cond_5b
    return-void

    #@5c
    .line 1292
    .restart local v0       #anchorPadding:I
    .restart local v1       #dropDownPadding:Landroid/graphics/Rect;
    .restart local v3       #isLayoutRtl:Z
    .restart local v5       #res:Landroid/content/res/Resources;
    :cond_5c
    const/4 v2, 0x0

    #@5d
    goto :goto_34

    #@5e
    .line 1301
    .restart local v2       #iconOffset:I
    :cond_5e
    iget v7, v1, Landroid/graphics/Rect;->left:I

    #@60
    add-int/2addr v7, v2

    #@61
    sub-int v4, v0, v7

    #@63
    .restart local v4       #offset:I
    goto :goto_42
.end method

.method private createIntent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;
    .registers 10
    .parameter "action"
    .parameter "data"
    .parameter "extraData"
    .parameter "query"
    .parameter "actionKey"
    .parameter "actionMsg"

    #@0
    .prologue
    .line 1464
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5
    .line 1465
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@a
    .line 1469
    if-eqz p2, :cond_f

    #@c
    .line 1470
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@f
    .line 1472
    :cond_f
    const-string/jumbo v1, "user_query"

    #@12
    iget-object v2, p0, Landroid/widget/SearchView;->mUserQuery:Ljava/lang/CharSequence;

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    #@17
    .line 1473
    if-eqz p4, :cond_1f

    #@19
    .line 1474
    const-string/jumbo v1, "query"

    #@1c
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 1476
    :cond_1f
    if-eqz p3, :cond_26

    #@21
    .line 1477
    const-string v1, "intent_extra_data_key"

    #@23
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@26
    .line 1479
    :cond_26
    iget-object v1, p0, Landroid/widget/SearchView;->mAppSearchData:Landroid/os/Bundle;

    #@28
    if-eqz v1, :cond_31

    #@2a
    .line 1480
    const-string v1, "app_data"

    #@2c
    iget-object v2, p0, Landroid/widget/SearchView;->mAppSearchData:Landroid/os/Bundle;

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@31
    .line 1482
    :cond_31
    if-eqz p5, :cond_3d

    #@33
    .line 1483
    const-string v1, "action_key"

    #@35
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@38
    .line 1484
    const-string v1, "action_msg"

    #@3a
    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d
    .line 1486
    :cond_3d
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@3f
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@46
    .line 1487
    return-object v0
.end method

.method private createIntentFromSuggestion(Landroid/database/Cursor;ILjava/lang/String;)Landroid/content/Intent;
    .registers 16
    .parameter "c"
    .parameter "actionKey"
    .parameter "actionMsg"

    #@0
    .prologue
    .line 1582
    :try_start_0
    const-string/jumbo v0, "suggest_intent_action"

    #@3
    invoke-static {p1, v0}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 1584
    .local v1, action:Ljava/lang/String;
    if-nez v1, :cond_f

    #@9
    .line 1585
    iget-object v0, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@b
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentAction()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 1587
    :cond_f
    if-nez v1, :cond_13

    #@11
    .line 1588
    const-string v1, "android.intent.action.SEARCH"

    #@13
    .line 1592
    :cond_13
    const-string/jumbo v0, "suggest_intent_data"

    #@16
    invoke-static {p1, v0}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    .line 1593
    .local v7, data:Ljava/lang/String;
    if-nez v7, :cond_22

    #@1c
    .line 1594
    iget-object v0, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@1e
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentData()Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    .line 1597
    :cond_22
    if-eqz v7, :cond_48

    #@24
    .line 1598
    const-string/jumbo v0, "suggest_intent_data_id"

    #@27
    invoke-static {p1, v0}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v10

    #@2b
    .line 1599
    .local v10, id:Ljava/lang/String;
    if-eqz v10, :cond_48

    #@2d
    .line 1600
    new-instance v0, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v5, "/"

    #@38
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-static {v10}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    .line 1603
    .end local v10           #id:Ljava/lang/String;
    :cond_48
    if-nez v7, :cond_61

    #@4a
    const/4 v2, 0x0

    #@4b
    .line 1605
    .local v2, dataUri:Landroid/net/Uri;
    :goto_4b
    const-string/jumbo v0, "suggest_intent_query"

    #@4e
    invoke-static {p1, v0}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    .line 1606
    .local v4, query:Ljava/lang/String;
    const-string/jumbo v0, "suggest_intent_extra_data"

    #@55
    invoke-static {p1, v0}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    .local v3, extraData:Ljava/lang/String;
    move-object v0, p0

    #@5a
    move v5, p2

    #@5b
    move-object v6, p3

    #@5c
    .line 1608
    invoke-direct/range {v0 .. v6}, Landroid/widget/SearchView;->createIntent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    #@5f
    move-result-object v0

    #@60
    .line 1618
    .end local v1           #action:Ljava/lang/String;
    .end local v2           #dataUri:Landroid/net/Uri;
    .end local v3           #extraData:Ljava/lang/String;
    .end local v4           #query:Ljava/lang/String;
    .end local v7           #data:Ljava/lang/String;
    :goto_60
    return-object v0

    #@61
    .line 1603
    .restart local v1       #action:Ljava/lang/String;
    .restart local v7       #data:Ljava/lang/String;
    :cond_61
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_64
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_64} :catch_66

    #@64
    move-result-object v2

    #@65
    goto :goto_4b

    #@66
    .line 1609
    .end local v1           #action:Ljava/lang/String;
    .end local v7           #data:Ljava/lang/String;
    :catch_66
    move-exception v8

    #@67
    .line 1612
    .local v8, e:Ljava/lang/RuntimeException;
    :try_start_67
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I
    :try_end_6a
    .catch Ljava/lang/RuntimeException; {:try_start_67 .. :try_end_6a} :catch_8b

    #@6a
    move-result v11

    #@6b
    .line 1616
    .local v11, rowNum:I
    :goto_6b
    const-string v0, "SearchView"

    #@6d
    new-instance v5, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v6, "Search suggestions cursor at row "

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    const-string v6, " returned exception."

    #@7e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v5

    #@86
    invoke-static {v0, v5, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@89
    .line 1618
    const/4 v0, 0x0

    #@8a
    goto :goto_60

    #@8b
    .line 1613
    .end local v11           #rowNum:I
    :catch_8b
    move-exception v9

    #@8c
    .line 1614
    .local v9, e2:Ljava/lang/RuntimeException;
    const/4 v11, -0x1

    #@8d
    .restart local v11       #rowNum:I
    goto :goto_6b
.end method

.method private createVoiceAppSearchIntent(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;
    .registers 16
    .parameter "baseIntent"
    .parameter "searchable"

    #@0
    .prologue
    .line 1509
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    #@3
    move-result-object v8

    #@4
    .line 1514
    .local v8, searchActivity:Landroid/content/ComponentName;
    new-instance v6, Landroid/content/Intent;

    #@6
    const-string v10, "android.intent.action.SEARCH"

    #@8
    invoke-direct {v6, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 1515
    .local v6, queryIntent:Landroid/content/Intent;
    invoke-virtual {v6, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@e
    .line 1516
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@11
    move-result-object v10

    #@12
    const/4 v11, 0x0

    #@13
    const/high16 v12, 0x4000

    #@15
    invoke-static {v10, v11, v6, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@18
    move-result-object v3

    #@19
    .line 1523
    .local v3, pending:Landroid/app/PendingIntent;
    new-instance v5, Landroid/os/Bundle;

    #@1b
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    #@1e
    .line 1524
    .local v5, queryExtras:Landroid/os/Bundle;
    iget-object v10, p0, Landroid/widget/SearchView;->mAppSearchData:Landroid/os/Bundle;

    #@20
    if-eqz v10, :cond_29

    #@22
    .line 1525
    const-string v10, "app_data"

    #@24
    iget-object v11, p0, Landroid/widget/SearchView;->mAppSearchData:Landroid/os/Bundle;

    #@26
    invoke-virtual {v5, v10, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@29
    .line 1531
    :cond_29
    new-instance v9, Landroid/content/Intent;

    #@2b
    invoke-direct {v9, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@2e
    .line 1534
    .local v9, voiceIntent:Landroid/content/Intent;
    const-string v1, "free_form"

    #@30
    .line 1535
    .local v1, languageModel:Ljava/lang/String;
    const/4 v4, 0x0

    #@31
    .line 1536
    .local v4, prompt:Ljava/lang/String;
    const/4 v0, 0x0

    #@32
    .line 1537
    .local v0, language:Ljava/lang/String;
    const/4 v2, 0x1

    #@33
    .line 1539
    .local v2, maxResults:I
    invoke-virtual {p0}, Landroid/widget/SearchView;->getResources()Landroid/content/res/Resources;

    #@36
    move-result-object v7

    #@37
    .line 1540
    .local v7, resources:Landroid/content/res/Resources;
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    #@3a
    move-result v10

    #@3b
    if-eqz v10, :cond_45

    #@3d
    .line 1541
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageModeId()I

    #@40
    move-result v10

    #@41
    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    .line 1543
    :cond_45
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    #@48
    move-result v10

    #@49
    if-eqz v10, :cond_53

    #@4b
    .line 1544
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoicePromptTextId()I

    #@4e
    move-result v10

    #@4f
    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    .line 1546
    :cond_53
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    #@56
    move-result v10

    #@57
    if-eqz v10, :cond_61

    #@59
    .line 1547
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceLanguageId()I

    #@5c
    move-result v10

    #@5d
    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    .line 1549
    :cond_61
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    #@64
    move-result v10

    #@65
    if-eqz v10, :cond_6b

    #@67
    .line 1550
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getVoiceMaxResults()I

    #@6a
    move-result v2

    #@6b
    .line 1552
    :cond_6b
    const-string v10, "android.speech.extra.LANGUAGE_MODEL"

    #@6d
    invoke-virtual {v9, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@70
    .line 1553
    const-string v10, "android.speech.extra.PROMPT"

    #@72
    invoke-virtual {v9, v10, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@75
    .line 1554
    const-string v10, "android.speech.extra.LANGUAGE"

    #@77
    invoke-virtual {v9, v10, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7a
    .line 1555
    const-string v10, "android.speech.extra.MAX_RESULTS"

    #@7c
    invoke-virtual {v9, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7f
    .line 1556
    const-string v11, "calling_package"

    #@81
    if-nez v8, :cond_92

    #@83
    const/4 v10, 0x0

    #@84
    :goto_84
    invoke-virtual {v9, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@87
    .line 1560
    const-string v10, "android.speech.extra.RESULTS_PENDINGINTENT"

    #@89
    invoke-virtual {v9, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@8c
    .line 1561
    const-string v10, "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"

    #@8e
    invoke-virtual {v9, v10, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@91
    .line 1563
    return-object v9

    #@92
    .line 1556
    :cond_92
    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@95
    move-result-object v10

    #@96
    goto :goto_84
.end method

.method private createVoiceWebSearchIntent(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;
    .registers 7
    .parameter "baseIntent"
    .parameter "searchable"

    #@0
    .prologue
    .line 1494
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@5
    .line 1495
    .local v1, voiceIntent:Landroid/content/Intent;
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    #@8
    move-result-object v0

    #@9
    .line 1496
    .local v0, searchActivity:Landroid/content/ComponentName;
    const-string v3, "calling_package"

    #@b
    if-nez v0, :cond_12

    #@d
    const/4 v2, 0x0

    #@e
    :goto_e
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 1498
    return-object v1

    #@12
    .line 1496
    :cond_12
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    goto :goto_e
.end method

.method private dismissSuggestions()V
    .registers 2

    #@0
    .prologue
    .line 1177
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->dismissDropDown()V

    #@5
    .line 1178
    return-void
.end method

.method private forceSuggestionQuery()V
    .registers 2

    #@0
    .prologue
    .line 1623
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->doBeforeTextChanged()V

    #@5
    .line 1624
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@7
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->doAfterTextChanged()V

    #@a
    .line 1625
    return-void
.end method

.method private static getActionKeyMessage(Landroid/database/Cursor;Landroid/app/SearchableInfo$ActionKeyInfo;)Ljava/lang/String;
    .registers 4
    .parameter "c"
    .parameter "actionKey"

    #@0
    .prologue
    .line 1028
    const/4 v1, 0x0

    #@1
    .line 1030
    .local v1, result:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/app/SearchableInfo$ActionKeyInfo;->getSuggestActionMsgColumn()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 1031
    .local v0, column:Ljava/lang/String;
    if-eqz v0, :cond_b

    #@7
    .line 1032
    invoke-static {p0, v0}, Landroid/widget/SuggestionsAdapter;->getColumnString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 1037
    :cond_b
    if-nez v1, :cond_11

    #@d
    .line 1038
    invoke-virtual {p1}, Landroid/app/SearchableInfo$ActionKeyInfo;->getSuggestActionMsg()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 1040
    :cond_11
    return-object v1
.end method

.method private getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 10
    .parameter "hintText"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1052
    iget-boolean v3, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@3
    if-nez v3, :cond_6

    #@5
    .line 1061
    .end local p1
    :goto_5
    return-object p1

    #@6
    .line 1054
    .restart local p1
    :cond_6
    new-instance v1, Landroid/text/SpannableStringBuilder;

    #@8
    const-string v3, "   "

    #@a
    invoke-direct {v1, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@d
    .line 1055
    .local v1, ssb:Landroid/text/SpannableStringBuilder;
    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@10
    .line 1056
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v3

    #@18
    invoke-direct {p0}, Landroid/widget/SearchView;->getSearchIconId()I

    #@1b
    move-result v4

    #@1c
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1f
    move-result-object v0

    #@20
    .line 1057
    .local v0, searchIcon:Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@22
    invoke-virtual {v3}, Landroid/widget/SearchView$SearchAutoComplete;->getTextSize()F

    #@25
    move-result v3

    #@26
    float-to-double v3, v3

    #@27
    const-wide/high16 v5, 0x3ff4

    #@29
    mul-double/2addr v3, v5

    #@2a
    double-to-int v2, v3

    #@2b
    .line 1058
    .local v2, textSize:I
    invoke-virtual {v0, v7, v7, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2e
    .line 1060
    new-instance v3, Landroid/text/style/ImageSpan;

    #@30
    const/16 v4, 0x63

    #@32
    invoke-direct {v3, v0, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    #@35
    const/4 v4, 0x1

    #@36
    const/4 v5, 0x2

    #@37
    const/16 v6, 0x21

    #@39
    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@3c
    move-object p1, v1

    #@3d
    .line 1061
    goto :goto_5
.end method

.method private getPreferredWidth()I
    .registers 3

    #@0
    .prologue
    .line 746
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    const v1, 0x1050039

    #@b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@e
    move-result v0

    #@f
    return v0
.end method

.method private getSearchIconId()I
    .registers 5

    #@0
    .prologue
    .line 1044
    new-instance v0, Landroid/util/TypedValue;

    #@2
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@5
    .line 1045
    .local v0, outValue:Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@c
    move-result-object v1

    #@d
    const v2, 0x10103ee

    #@10
    const/4 v3, 0x1

    #@11
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@14
    .line 1047
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    #@16
    return v1
.end method

.method private hasVoiceSearch()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 767
    iget-object v3, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@3
    if-eqz v3, :cond_2b

    #@5
    iget-object v3, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@7
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getVoiceSearchEnabled()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_2b

    #@d
    .line 768
    const/4 v1, 0x0

    #@e
    .line 769
    .local v1, testIntent:Landroid/content/Intent;
    iget-object v3, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@10
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchWebSearch()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_2c

    #@16
    .line 770
    iget-object v1, p0, Landroid/widget/SearchView;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@18
    .line 774
    :cond_18
    :goto_18
    if-eqz v1, :cond_2b

    #@1a
    .line 775
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@21
    move-result-object v3

    #@22
    const/high16 v4, 0x1

    #@24
    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@27
    move-result-object v0

    #@28
    .line 777
    .local v0, ri:Landroid/content/pm/ResolveInfo;
    if-eqz v0, :cond_2b

    #@2a
    const/4 v2, 0x1

    #@2b
    .line 780
    .end local v0           #ri:Landroid/content/pm/ResolveInfo;
    .end local v1           #testIntent:Landroid/content/Intent;
    :cond_2b
    return v2

    #@2c
    .line 771
    .restart local v1       #testIntent:Landroid/content/Intent;
    :cond_2c
    iget-object v3, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@2e
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchRecognizer()Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_18

    #@34
    .line 772
    iget-object v1, p0, Landroid/widget/SearchView;->mVoiceAppSearchIntent:Landroid/content/Intent;

    #@36
    goto :goto_18
.end method

.method static isLandscapeMode(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1628
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    #@a
    const/4 v1, 0x2

    #@b
    if-ne v0, v1, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method private isSubmitAreaEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 784
    iget-boolean v0, p0, Landroid/widget/SearchView;->mSubmitButtonEnabled:Z

    #@2
    if-nez v0, :cond_8

    #@4
    iget-boolean v0, p0, Landroid/widget/SearchView;->mVoiceButtonEnabled:Z

    #@6
    if-eqz v0, :cond_10

    #@8
    :cond_8
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method private launchIntent(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 1419
    if-nez p1, :cond_3

    #@2
    .line 1429
    :goto_2
    return-void

    #@3
    .line 1425
    :cond_3
    :try_start_3
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_a} :catch_b

    #@a
    goto :goto_2

    #@b
    .line 1426
    :catch_b
    move-exception v0

    #@c
    .line 1427
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v1, "SearchView"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Failed launch activity: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@24
    goto :goto_2
.end method

.method private launchQuerySearch(ILjava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "actionKey"
    .parameter "actionMsg"
    .parameter "query"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1441
    const-string v1, "android.intent.action.SEARCH"

    #@3
    .local v1, action:Ljava/lang/String;
    move-object v0, p0

    #@4
    move-object v3, v2

    #@5
    move-object v4, p3

    #@6
    move v5, p1

    #@7
    move-object v6, p2

    #@8
    .line 1442
    invoke-direct/range {v0 .. v6}, Landroid/widget/SearchView;->createIntent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    #@b
    move-result-object v7

    #@c
    .line 1443
    .local v7, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@13
    .line 1444
    return-void
.end method

.method private launchSuggestion(IILjava/lang/String;)Z
    .registers 7
    .parameter "position"
    .parameter "actionKey"
    .parameter "actionMsg"

    #@0
    .prologue
    .line 1402
    iget-object v2, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@2
    invoke-virtual {v2}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    #@5
    move-result-object v0

    #@6
    .line 1403
    .local v0, c:Landroid/database/Cursor;
    if-eqz v0, :cond_17

    #@8
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_17

    #@e
    .line 1405
    invoke-direct {p0, v0, p2, p3}, Landroid/widget/SearchView;->createIntentFromSuggestion(Landroid/database/Cursor;ILjava/lang/String;)Landroid/content/Intent;

    #@11
    move-result-object v1

    #@12
    .line 1408
    .local v1, intent:Landroid/content/Intent;
    invoke-direct {p0, v1}, Landroid/widget/SearchView;->launchIntent(Landroid/content/Intent;)V

    #@15
    .line 1410
    const/4 v2, 0x1

    #@16
    .line 1412
    .end local v1           #intent:Landroid/content/Intent;
    :goto_16
    return v2

    #@17
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_16
.end method

.method private onCloseClicked()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1181
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@3
    invoke-virtual {v1}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@6
    move-result-object v0

    #@7
    .line 1182
    .local v0, text:Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_24

    #@d
    .line 1183
    iget-boolean v1, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@f
    if-eqz v1, :cond_23

    #@11
    .line 1185
    iget-object v1, p0, Landroid/widget/SearchView;->mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

    #@13
    if-eqz v1, :cond_1d

    #@15
    iget-object v1, p0, Landroid/widget/SearchView;->mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

    #@17
    invoke-interface {v1}, Landroid/widget/SearchView$OnCloseListener;->onClose()Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_23

    #@1d
    .line 1187
    :cond_1d
    invoke-virtual {p0}, Landroid/widget/SearchView;->clearFocus()V

    #@20
    .line 1189
    invoke-direct {p0, v3}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@23
    .line 1198
    :cond_23
    :goto_23
    return-void

    #@24
    .line 1193
    :cond_24
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@26
    const-string v2, ""

    #@28
    invoke-virtual {v1, v2}, Landroid/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    #@2b
    .line 1194
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2d
    invoke-virtual {v1}, Landroid/widget/SearchView$SearchAutoComplete;->requestFocus()Z

    #@30
    .line 1195
    invoke-direct {p0, v3}, Landroid/widget/SearchView;->setImeVisibility(Z)V

    #@33
    goto :goto_23
.end method

.method private onItemClicked(IILjava/lang/String;)Z
    .registers 6
    .parameter "position"
    .parameter "actionKey"
    .parameter "actionMsg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1311
    iget-object v1, p0, Landroid/widget/SearchView;->mOnSuggestionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@3
    if-eqz v1, :cond_d

    #@5
    iget-object v1, p0, Landroid/widget/SearchView;->mOnSuggestionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@7
    invoke-interface {v1, p1}, Landroid/widget/SearchView$OnSuggestionListener;->onSuggestionClick(I)Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_18

    #@d
    .line 1313
    :cond_d
    const/4 v1, 0x0

    #@e
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/SearchView;->launchSuggestion(IILjava/lang/String;)Z

    #@11
    .line 1314
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->setImeVisibility(Z)V

    #@14
    .line 1315
    invoke-direct {p0}, Landroid/widget/SearchView;->dismissSuggestions()V

    #@17
    .line 1316
    const/4 v0, 0x1

    #@18
    .line 1318
    :cond_18
    return v0
.end method

.method private onItemSelected(I)Z
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 1322
    iget-object v0, p0, Landroid/widget/SearchView;->mOnSuggestionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/SearchView;->mOnSuggestionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@6
    invoke-interface {v0, p1}, Landroid/widget/SearchView$OnSuggestionListener;->onSuggestionSelect(I)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_11

    #@c
    .line 1324
    :cond_c
    invoke-direct {p0, p1}, Landroid/widget/SearchView;->rewriteQueryFromSuggestion(I)V

    #@f
    .line 1325
    const/4 v0, 0x1

    #@10
    .line 1327
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method private onSearchClicked()V
    .registers 2

    #@0
    .prologue
    .line 1201
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@4
    .line 1202
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@6
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->requestFocus()Z

    #@9
    .line 1203
    const/4 v0, 0x1

    #@a
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->setImeVisibility(Z)V

    #@d
    .line 1204
    iget-object v0, p0, Landroid/widget/SearchView;->mOnSearchClickListener:Landroid/view/View$OnClickListener;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 1205
    iget-object v0, p0, Landroid/widget/SearchView;->mOnSearchClickListener:Landroid/view/View$OnClickListener;

    #@13
    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    #@16
    .line 1207
    :cond_16
    return-void
.end method

.method private onSubmitQuery()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1163
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@3
    invoke-virtual {v1}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@6
    move-result-object v0

    #@7
    .line 1164
    .local v0, query:Ljava/lang/CharSequence;
    if-eqz v0, :cond_31

    #@9
    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    #@c
    move-result v1

    #@d
    if-lez v1, :cond_31

    #@f
    .line 1165
    iget-object v1, p0, Landroid/widget/SearchView;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@11
    if-eqz v1, :cond_1f

    #@13
    iget-object v1, p0, Landroid/widget/SearchView;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@15
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-interface {v1, v2}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_31

    #@1f
    .line 1167
    :cond_1f
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@21
    if-eqz v1, :cond_2e

    #@23
    .line 1168
    const/4 v1, 0x0

    #@24
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {p0, v3, v1, v2}, Landroid/widget/SearchView;->launchQuerySearch(ILjava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 1169
    invoke-direct {p0, v3}, Landroid/widget/SearchView;->setImeVisibility(Z)V

    #@2e
    .line 1171
    :cond_2e
    invoke-direct {p0}, Landroid/widget/SearchView;->dismissSuggestions()V

    #@31
    .line 1174
    :cond_31
    return-void
.end method

.method private onSuggestionsKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 13
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/16 v8, 0x15

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 956
    iget-object v7, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@6
    if-nez v7, :cond_9

    #@8
    .line 1013
    :cond_8
    :goto_8
    return v5

    #@9
    .line 959
    :cond_9
    iget-object v7, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@b
    if-eqz v7, :cond_8

    #@d
    .line 962
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@10
    move-result v7

    #@11
    if-nez v7, :cond_8

    #@13
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@16
    move-result v7

    #@17
    if-eqz v7, :cond_8

    #@19
    .line 965
    const/16 v7, 0x42

    #@1b
    if-eq p2, v7, :cond_25

    #@1d
    const/16 v7, 0x54

    #@1f
    if-eq p2, v7, :cond_25

    #@21
    const/16 v7, 0x3d

    #@23
    if-ne p2, v7, :cond_31

    #@25
    .line 967
    :cond_25
    iget-object v6, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@27
    invoke-virtual {v6}, Landroid/widget/SearchView$SearchAutoComplete;->getListSelection()I

    #@2a
    move-result v3

    #@2b
    .line 968
    .local v3, position:I
    const/4 v6, 0x0

    #@2c
    invoke-direct {p0, v3, v5, v6}, Landroid/widget/SearchView;->onItemClicked(IILjava/lang/String;)Z

    #@2f
    move-result v5

    #@30
    goto :goto_8

    #@31
    .line 973
    .end local v3           #position:I
    :cond_31
    if-eq p2, v8, :cond_37

    #@33
    const/16 v7, 0x16

    #@35
    if-ne p2, v7, :cond_57

    #@37
    .line 978
    :cond_37
    if-ne p2, v8, :cond_50

    #@39
    move v4, v5

    #@3a
    .line 980
    .local v4, selPoint:I
    :goto_3a
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@3c
    invoke-virtual {v7, v4}, Landroid/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    #@3f
    .line 981
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@41
    invoke-virtual {v7, v5}, Landroid/widget/SearchView$SearchAutoComplete;->setListSelection(I)V

    #@44
    .line 982
    iget-object v5, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@46
    invoke-virtual {v5}, Landroid/widget/SearchView$SearchAutoComplete;->clearListSelection()V

    #@49
    .line 983
    iget-object v5, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@4b
    invoke-virtual {v5, v6}, Landroid/widget/SearchView$SearchAutoComplete;->ensureImeVisible(Z)V

    #@4e
    move v5, v6

    #@4f
    .line 985
    goto :goto_8

    #@50
    .line 978
    .end local v4           #selPoint:I
    :cond_50
    iget-object v7, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@52
    invoke-virtual {v7}, Landroid/widget/SearchView$SearchAutoComplete;->length()I

    #@55
    move-result v4

    #@56
    goto :goto_3a

    #@57
    .line 989
    :cond_57
    const/16 v6, 0x13

    #@59
    if-ne p2, v6, :cond_63

    #@5b
    iget-object v6, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@5d
    invoke-virtual {v6}, Landroid/widget/SearchView$SearchAutoComplete;->getListSelection()I

    #@60
    move-result v6

    #@61
    if-eqz v6, :cond_8

    #@63
    .line 996
    :cond_63
    iget-object v6, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@65
    invoke-virtual {v6, p2}, Landroid/app/SearchableInfo;->findActionKey(I)Landroid/app/SearchableInfo$ActionKeyInfo;

    #@68
    move-result-object v0

    #@69
    .line 997
    .local v0, actionKey:Landroid/app/SearchableInfo$ActionKeyInfo;
    if-eqz v0, :cond_8

    #@6b
    invoke-virtual {v0}, Landroid/app/SearchableInfo$ActionKeyInfo;->getSuggestActionMsg()Ljava/lang/String;

    #@6e
    move-result-object v6

    #@6f
    if-nez v6, :cond_77

    #@71
    invoke-virtual {v0}, Landroid/app/SearchableInfo$ActionKeyInfo;->getSuggestActionMsgColumn()Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    if-eqz v6, :cond_8

    #@77
    .line 1001
    :cond_77
    iget-object v6, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@79
    invoke-virtual {v6}, Landroid/widget/SearchView$SearchAutoComplete;->getListSelection()I

    #@7c
    move-result v3

    #@7d
    .line 1002
    .restart local v3       #position:I
    const/4 v6, -0x1

    #@7e
    if-eq v3, v6, :cond_8

    #@80
    .line 1003
    iget-object v6, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@82
    invoke-virtual {v6}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    #@85
    move-result-object v2

    #@86
    .line 1004
    .local v2, c:Landroid/database/Cursor;
    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@89
    move-result v6

    #@8a
    if-eqz v6, :cond_8

    #@8c
    .line 1005
    invoke-static {v2, v0}, Landroid/widget/SearchView;->getActionKeyMessage(Landroid/database/Cursor;Landroid/app/SearchableInfo$ActionKeyInfo;)Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    .line 1006
    .local v1, actionMsg:Ljava/lang/String;
    if-eqz v1, :cond_8

    #@92
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@95
    move-result v6

    #@96
    if-lez v6, :cond_8

    #@98
    .line 1007
    invoke-direct {p0, v3, p2, v1}, Landroid/widget/SearchView;->onItemClicked(IILjava/lang/String;)Z

    #@9b
    move-result v5

    #@9c
    goto/16 :goto_8
.end method

.method private onTextChanged(Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "newText"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1149
    iget-object v4, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@4
    invoke-virtual {v4}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@7
    move-result-object v1

    #@8
    .line 1150
    .local v1, text:Ljava/lang/CharSequence;
    iput-object v1, p0, Landroid/widget/SearchView;->mUserQuery:Ljava/lang/CharSequence;

    #@a
    .line 1151
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_3b

    #@10
    move v0, v2

    #@11
    .line 1152
    .local v0, hasText:Z
    :goto_11
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateSubmitButton(Z)V

    #@14
    .line 1153
    if-nez v0, :cond_3d

    #@16
    :goto_16
    invoke-direct {p0, v2}, Landroid/widget/SearchView;->updateVoiceButton(Z)V

    #@19
    .line 1154
    invoke-direct {p0}, Landroid/widget/SearchView;->updateCloseButton()V

    #@1c
    .line 1155
    invoke-direct {p0}, Landroid/widget/SearchView;->updateSubmitArea()V

    #@1f
    .line 1156
    iget-object v2, p0, Landroid/widget/SearchView;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@21
    if-eqz v2, :cond_34

    #@23
    iget-object v2, p0, Landroid/widget/SearchView;->mOldQueryText:Ljava/lang/CharSequence;

    #@25
    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@28
    move-result v2

    #@29
    if-nez v2, :cond_34

    #@2b
    .line 1157
    iget-object v2, p0, Landroid/widget/SearchView;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@2d
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-interface {v2, v3}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    #@34
    .line 1159
    :cond_34
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    iput-object v2, p0, Landroid/widget/SearchView;->mOldQueryText:Ljava/lang/CharSequence;

    #@3a
    .line 1160
    return-void

    #@3b
    .end local v0           #hasText:Z
    :cond_3b
    move v0, v3

    #@3c
    .line 1151
    goto :goto_11

    #@3d
    .restart local v0       #hasText:Z
    :cond_3d
    move v2, v3

    #@3e
    .line 1153
    goto :goto_16
.end method

.method private onVoiceClicked()V
    .registers 7

    #@0
    .prologue
    .line 1211
    iget-object v4, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    if-nez v4, :cond_5

    #@4
    .line 1230
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1214
    :cond_5
    iget-object v2, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@7
    .line 1216
    .local v2, searchable:Landroid/app/SearchableInfo;
    :try_start_7
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchWebSearch()Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_24

    #@d
    .line 1217
    iget-object v4, p0, Landroid/widget/SearchView;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@f
    invoke-direct {p0, v4, v2}, Landroid/widget/SearchView;->createVoiceWebSearchIntent(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;

    #@12
    move-result-object v3

    #@13
    .line 1219
    .local v3, webSearchIntent:Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_7 .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_4

    #@1b
    .line 1225
    .end local v3           #webSearchIntent:Landroid/content/Intent;
    :catch_1b
    move-exception v1

    #@1c
    .line 1228
    .local v1, e:Landroid/content/ActivityNotFoundException;
    const-string v4, "SearchView"

    #@1e
    const-string v5, "Could not find voice search activity"

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_4

    #@24
    .line 1220
    .end local v1           #e:Landroid/content/ActivityNotFoundException;
    :cond_24
    :try_start_24
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getVoiceSearchLaunchRecognizer()Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_4

    #@2a
    .line 1221
    iget-object v4, p0, Landroid/widget/SearchView;->mVoiceAppSearchIntent:Landroid/content/Intent;

    #@2c
    invoke-direct {p0, v4, v2}, Landroid/widget/SearchView;->createVoiceAppSearchIntent(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;

    #@2f
    move-result-object v0

    #@30
    .line 1223
    .local v0, appSearchIntent:Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_37
    .catch Landroid/content/ActivityNotFoundException; {:try_start_24 .. :try_end_37} :catch_1b

    #@37
    goto :goto_4
.end method

.method private postUpdateFocusedState()V
    .registers 2

    #@0
    .prologue
    .line 816
    iget-object v0, p0, Landroid/widget/SearchView;->mUpdateDrawableStateRunnable:Ljava/lang/Runnable;

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    #@5
    .line 817
    return-void
.end method

.method private rewriteQueryFromSuggestion(I)V
    .registers 6
    .parameter "position"

    #@0
    .prologue
    .line 1369
    iget-object v3, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v3}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@5
    move-result-object v2

    #@6
    .line 1370
    .local v2, oldQuery:Ljava/lang/CharSequence;
    iget-object v3, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@8
    invoke-virtual {v3}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    #@b
    move-result-object v0

    #@c
    .line 1371
    .local v0, c:Landroid/database/Cursor;
    if-nez v0, :cond_f

    #@e
    .line 1389
    :goto_e
    return-void

    #@f
    .line 1374
    :cond_f
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_25

    #@15
    .line 1376
    iget-object v3, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@17
    invoke-virtual {v3, v0}, Landroid/widget/CursorAdapter;->convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    #@1a
    move-result-object v1

    #@1b
    .line 1377
    .local v1, newQuery:Ljava/lang/CharSequence;
    if-eqz v1, :cond_21

    #@1d
    .line 1380
    invoke-direct {p0, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    #@20
    goto :goto_e

    #@21
    .line 1383
    :cond_21
    invoke-direct {p0, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    #@24
    goto :goto_e

    #@25
    .line 1387
    .end local v1           #newQuery:Ljava/lang/CharSequence;
    :cond_25
    invoke-direct {p0, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    #@28
    goto :goto_e
.end method

.method private setImeVisibility(Z)V
    .registers 5
    .parameter "visible"

    #@0
    .prologue
    .line 834
    if-eqz p1, :cond_8

    #@2
    .line 835
    iget-object v1, p0, Landroid/widget/SearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {p0, v1}, Landroid/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    #@7
    .line 845
    :cond_7
    :goto_7
    return-void

    #@8
    .line 837
    :cond_8
    iget-object v1, p0, Landroid/widget/SearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    #@a
    invoke-virtual {p0, v1}, Landroid/widget/SearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@d
    .line 838
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@10
    move-result-object v1

    #@11
    const-string v2, "input_method"

    #@13
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@19
    .line 841
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_7

    #@1b
    .line 842
    invoke-virtual {p0}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    #@1e
    move-result-object v1

    #@1f
    const/4 v2, 0x0

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@23
    goto :goto_7
.end method

.method private setQuery(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "query"

    #@0
    .prologue
    .line 1435
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;Z)V

    #@6
    .line 1437
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@8
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_13

    #@e
    const/4 v0, 0x0

    #@f
    :goto_f
    invoke-virtual {v1, v0}, Landroid/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    #@12
    .line 1438
    return-void

    #@13
    .line 1437
    :cond_13
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@16
    move-result v0

    #@17
    goto :goto_f
.end method

.method private updateCloseButton()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 807
    iget-object v3, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@4
    invoke-virtual {v3}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@7
    move-result-object v3

    #@8
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_2e

    #@e
    move v0, v1

    #@f
    .line 810
    .local v0, hasText:Z
    :goto_f
    if-nez v0, :cond_19

    #@11
    iget-boolean v3, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@13
    if-eqz v3, :cond_30

    #@15
    iget-boolean v3, p0, Landroid/widget/SearchView;->mExpandedInActionView:Z

    #@17
    if-nez v3, :cond_30

    #@19
    .line 811
    .local v1, showClose:Z
    :cond_19
    :goto_19
    iget-object v3, p0, Landroid/widget/SearchView;->mCloseButton:Landroid/widget/ImageView;

    #@1b
    if-eqz v1, :cond_32

    #@1d
    :goto_1d
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    #@20
    .line 812
    iget-object v2, p0, Landroid/widget/SearchView;->mCloseButton:Landroid/widget/ImageView;

    #@22
    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@25
    move-result-object v3

    #@26
    if-eqz v0, :cond_35

    #@28
    sget-object v2, Landroid/widget/SearchView;->ENABLED_STATE_SET:[I

    #@2a
    :goto_2a
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@2d
    .line 813
    return-void

    #@2e
    .end local v0           #hasText:Z
    .end local v1           #showClose:Z
    :cond_2e
    move v0, v2

    #@2f
    .line 807
    goto :goto_f

    #@30
    .restart local v0       #hasText:Z
    :cond_30
    move v1, v2

    #@31
    .line 810
    goto :goto_19

    #@32
    .line 811
    .restart local v1       #showClose:Z
    :cond_32
    const/16 v2, 0x8

    #@34
    goto :goto_1d

    #@35
    .line 812
    :cond_35
    sget-object v2, Landroid/widget/SearchView;->EMPTY_STATE_SET:[I

    #@37
    goto :goto_2a
.end method

.method private updateFocusedState()V
    .registers 4

    #@0
    .prologue
    .line 820
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v1}, Landroid/widget/SearchView$SearchAutoComplete;->hasFocus()Z

    #@5
    move-result v0

    #@6
    .line 821
    .local v0, focused:Z
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchPlate:Landroid/view/View;

    #@8
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v2

    #@c
    if-eqz v0, :cond_24

    #@e
    sget-object v1, Landroid/widget/SearchView;->FOCUSED_STATE_SET:[I

    #@10
    :goto_10
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@13
    .line 822
    iget-object v1, p0, Landroid/widget/SearchView;->mSubmitArea:Landroid/view/View;

    #@15
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    #@18
    move-result-object v2

    #@19
    if-eqz v0, :cond_27

    #@1b
    sget-object v1, Landroid/widget/SearchView;->FOCUSED_STATE_SET:[I

    #@1d
    :goto_1d
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@20
    .line 823
    invoke-virtual {p0}, Landroid/widget/SearchView;->invalidate()V

    #@23
    .line 824
    return-void

    #@24
    .line 821
    :cond_24
    sget-object v1, Landroid/widget/SearchView;->EMPTY_STATE_SET:[I

    #@26
    goto :goto_10

    #@27
    .line 822
    :cond_27
    sget-object v1, Landroid/widget/SearchView;->EMPTY_STATE_SET:[I

    #@29
    goto :goto_1d
.end method

.method private updateQueryHint()V
    .registers 5

    #@0
    .prologue
    .line 1065
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryHint:Ljava/lang/CharSequence;

    #@2
    if-eqz v2, :cond_10

    #@4
    .line 1066
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@6
    iget-object v3, p0, Landroid/widget/SearchView;->mQueryHint:Ljava/lang/CharSequence;

    #@8
    invoke-direct {p0, v3}, Landroid/widget/SearchView;->getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v2, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    #@f
    .line 1079
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1067
    :cond_10
    iget-object v2, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@12
    if-eqz v2, :cond_31

    #@14
    .line 1068
    const/4 v0, 0x0

    #@15
    .line 1069
    .local v0, hint:Ljava/lang/CharSequence;
    iget-object v2, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@17
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getHintId()I

    #@1a
    move-result v1

    #@1b
    .line 1070
    .local v1, hintId:I
    if-eqz v1, :cond_25

    #@1d
    .line 1071
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    .line 1073
    :cond_25
    if-eqz v0, :cond_f

    #@27
    .line 1074
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@29
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    #@30
    goto :goto_f

    #@31
    .line 1077
    .end local v0           #hint:Ljava/lang/CharSequence;
    .end local v1           #hintId:I
    :cond_31
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@33
    const-string v3, ""

    #@35
    invoke-direct {p0, v3}, Landroid/widget/SearchView;->getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v2, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    #@3c
    goto :goto_f
.end method

.method private updateSearchAutoComplete()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1085
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@3
    const/4 v3, 0x0

    #@4
    invoke-virtual {v1, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setDropDownAnimationStyle(I)V

    #@7
    .line 1086
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@9
    iget-object v3, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@b
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    #@e
    move-result v3

    #@f
    invoke-virtual {v1, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setThreshold(I)V

    #@12
    .line 1087
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@14
    iget-object v3, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@16
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->getImeOptions()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {v1, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    #@1d
    .line 1088
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@1f
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getInputType()I

    #@22
    move-result v0

    #@23
    .line 1091
    .local v0, inputType:I
    and-int/lit8 v1, v0, 0xf

    #@25
    if-ne v1, v2, :cond_39

    #@27
    .line 1094
    const v1, -0x10001

    #@2a
    and-int/2addr v0, v1

    #@2b
    .line 1095
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@2d
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    if-eqz v1, :cond_39

    #@33
    .line 1096
    const/high16 v1, 0x1

    #@35
    or-int/2addr v0, v1

    #@36
    .line 1103
    const/high16 v1, 0x8

    #@38
    or-int/2addr v0, v1

    #@39
    .line 1106
    :cond_39
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@3b
    invoke-virtual {v1, v0}, Landroid/widget/SearchView$SearchAutoComplete;->setInputType(I)V

    #@3e
    .line 1107
    iget-object v1, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@40
    if-eqz v1, :cond_48

    #@42
    .line 1108
    iget-object v1, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@44
    const/4 v3, 0x0

    #@45
    invoke-virtual {v1, v3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    #@48
    .line 1112
    :cond_48
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@4a
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    if-eqz v1, :cond_72

    #@50
    .line 1113
    new-instance v1, Landroid/widget/SuggestionsAdapter;

    #@52
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@55
    move-result-object v3

    #@56
    iget-object v4, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@58
    iget-object v5, p0, Landroid/widget/SearchView;->mOutsideDrawablesCache:Ljava/util/WeakHashMap;

    #@5a
    invoke-direct {v1, v3, p0, v4, v5}, Landroid/widget/SuggestionsAdapter;-><init>(Landroid/content/Context;Landroid/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V

    #@5d
    iput-object v1, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@5f
    .line 1115
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@61
    iget-object v3, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@63
    invoke-virtual {v1, v3}, Landroid/widget/SearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    #@66
    .line 1116
    iget-object v1, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@68
    check-cast v1, Landroid/widget/SuggestionsAdapter;

    #@6a
    iget-boolean v3, p0, Landroid/widget/SearchView;->mQueryRefinement:Z

    #@6c
    if-eqz v3, :cond_6f

    #@6e
    const/4 v2, 0x2

    #@6f
    :cond_6f
    invoke-virtual {v1, v2}, Landroid/widget/SuggestionsAdapter;->setQueryRefinement(I)V

    #@72
    .line 1120
    :cond_72
    return-void
.end method

.method private updateSubmitArea()V
    .registers 3

    #@0
    .prologue
    .line 797
    const/16 v0, 0x8

    #@2
    .line 798
    .local v0, visibility:I
    invoke-direct {p0}, Landroid/widget/SearchView;->isSubmitAreaEnabled()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_19

    #@8
    iget-object v1, p0, Landroid/widget/SearchView;->mSubmitButton:Landroid/view/View;

    #@a
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_18

    #@10
    iget-object v1, p0, Landroid/widget/SearchView;->mVoiceButton:Landroid/view/View;

    #@12
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_19

    #@18
    .line 801
    :cond_18
    const/4 v0, 0x0

    #@19
    .line 803
    :cond_19
    iget-object v1, p0, Landroid/widget/SearchView;->mSubmitArea:Landroid/view/View;

    #@1b
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    #@1e
    .line 804
    return-void
.end method

.method private updateSubmitButton(Z)V
    .registers 4
    .parameter "hasText"

    #@0
    .prologue
    .line 788
    const/16 v0, 0x8

    #@2
    .line 789
    .local v0, visibility:I
    iget-boolean v1, p0, Landroid/widget/SearchView;->mSubmitButtonEnabled:Z

    #@4
    if-eqz v1, :cond_19

    #@6
    invoke-direct {p0}, Landroid/widget/SearchView;->isSubmitAreaEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_19

    #@c
    invoke-virtual {p0}, Landroid/widget/SearchView;->hasFocus()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_19

    #@12
    if-nez p1, :cond_18

    #@14
    iget-boolean v1, p0, Landroid/widget/SearchView;->mVoiceButtonEnabled:Z

    #@16
    if-nez v1, :cond_19

    #@18
    .line 791
    :cond_18
    const/4 v0, 0x0

    #@19
    .line 793
    :cond_19
    iget-object v1, p0, Landroid/widget/SearchView;->mSubmitButton:Landroid/view/View;

    #@1b
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    #@1e
    .line 794
    return-void
.end method

.method private updateViewsVisibility(Z)V
    .registers 9
    .parameter "collapsed"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/16 v3, 0x8

    #@3
    const/4 v2, 0x0

    #@4
    .line 751
    iput-boolean p1, p0, Landroid/widget/SearchView;->mIconified:Z

    #@6
    .line 753
    if-eqz p1, :cond_3b

    #@8
    move v1, v2

    #@9
    .line 755
    .local v1, visCollapsed:I
    :goto_9
    iget-object v5, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@b
    invoke-virtual {v5}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@e
    move-result-object v5

    #@f
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@12
    move-result v5

    #@13
    if-nez v5, :cond_3d

    #@15
    move v0, v4

    #@16
    .line 757
    .local v0, hasText:Z
    :goto_16
    iget-object v5, p0, Landroid/widget/SearchView;->mSearchButton:Landroid/view/View;

    #@18
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    #@1b
    .line 758
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateSubmitButton(Z)V

    #@1e
    .line 759
    iget-object v6, p0, Landroid/widget/SearchView;->mSearchEditFrame:Landroid/view/View;

    #@20
    if-eqz p1, :cond_3f

    #@22
    move v5, v3

    #@23
    :goto_23
    invoke-virtual {v6, v5}, Landroid/view/View;->setVisibility(I)V

    #@26
    .line 760
    iget-object v5, p0, Landroid/widget/SearchView;->mSearchHintIcon:Landroid/widget/ImageView;

    #@28
    iget-boolean v6, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@2a
    if-eqz v6, :cond_41

    #@2c
    :goto_2c
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@2f
    .line 761
    invoke-direct {p0}, Landroid/widget/SearchView;->updateCloseButton()V

    #@32
    .line 762
    if-nez v0, :cond_43

    #@34
    :goto_34
    invoke-direct {p0, v4}, Landroid/widget/SearchView;->updateVoiceButton(Z)V

    #@37
    .line 763
    invoke-direct {p0}, Landroid/widget/SearchView;->updateSubmitArea()V

    #@3a
    .line 764
    return-void

    #@3b
    .end local v0           #hasText:Z
    .end local v1           #visCollapsed:I
    :cond_3b
    move v1, v3

    #@3c
    .line 753
    goto :goto_9

    #@3d
    .restart local v1       #visCollapsed:I
    :cond_3d
    move v0, v2

    #@3e
    .line 755
    goto :goto_16

    #@3f
    .restart local v0       #hasText:Z
    :cond_3f
    move v5, v2

    #@40
    .line 759
    goto :goto_23

    #@41
    :cond_41
    move v3, v2

    #@42
    .line 760
    goto :goto_2c

    #@43
    :cond_43
    move v4, v2

    #@44
    .line 762
    goto :goto_34
.end method

.method private updateVoiceButton(Z)V
    .registers 5
    .parameter "empty"

    #@0
    .prologue
    .line 1129
    const/16 v0, 0x8

    #@2
    .line 1130
    .local v0, visibility:I
    iget-boolean v1, p0, Landroid/widget/SearchView;->mVoiceButtonEnabled:Z

    #@4
    if-eqz v1, :cond_16

    #@6
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_16

    #@c
    if-eqz p1, :cond_16

    #@e
    .line 1131
    const/4 v0, 0x0

    #@f
    .line 1132
    iget-object v1, p0, Landroid/widget/SearchView;->mSubmitButton:Landroid/view/View;

    #@11
    const/16 v2, 0x8

    #@13
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@16
    .line 1134
    :cond_16
    iget-object v1, p0, Landroid/widget/SearchView;->mVoiceButton:Landroid/view/View;

    #@18
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    #@1b
    .line 1135
    return-void
.end method


# virtual methods
.method public clearFocus()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 440
    const/4 v0, 0x1

    #@2
    iput-boolean v0, p0, Landroid/widget/SearchView;->mClearingFocus:Z

    #@4
    .line 441
    invoke-direct {p0, v1}, Landroid/widget/SearchView;->setImeVisibility(Z)V

    #@7
    .line 442
    invoke-super {p0}, Landroid/widget/LinearLayout;->clearFocus()V

    #@a
    .line 443
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@c
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->clearFocus()V

    #@f
    .line 444
    iput-boolean v1, p0, Landroid/widget/SearchView;->mClearingFocus:Z

    #@11
    .line 445
    return-void
.end method

.method public getImeOptions()I
    .registers 2

    #@0
    .prologue
    .line 393
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->getImeOptions()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 415
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->getInputType()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMaxWidth()I
    .registers 2

    #@0
    .prologue
    .line 707
    iget v0, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@2
    return v0
.end method

.method public getQuery()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 502
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getQueryHint()Ljava/lang/CharSequence;
    .registers 4

    #@0
    .prologue
    .line 546
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryHint:Ljava/lang/CharSequence;

    #@2
    if-eqz v2, :cond_7

    #@4
    .line 547
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryHint:Ljava/lang/CharSequence;

    #@6
    .line 556
    :cond_6
    :goto_6
    return-object v0

    #@7
    .line 548
    :cond_7
    iget-object v2, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@9
    if-eqz v2, :cond_1d

    #@b
    .line 549
    const/4 v0, 0x0

    #@c
    .line 550
    .local v0, hint:Ljava/lang/CharSequence;
    iget-object v2, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@e
    invoke-virtual {v2}, Landroid/app/SearchableInfo;->getHintId()I

    #@11
    move-result v1

    #@12
    .line 551
    .local v1, hintId:I
    if-eqz v1, :cond_6

    #@14
    .line 552
    invoke-virtual {p0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    goto :goto_6

    #@1d
    .line 556
    .end local v0           #hint:Ljava/lang/CharSequence;
    .end local v1           #hintId:I
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_6
.end method

.method public getSuggestionsAdapter()Landroid/widget/CursorAdapter;
    .registers 2

    #@0
    .prologue
    .line 685
    iget-object v0, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@2
    return-object v0
.end method

.method public isIconfiedByDefault()Z
    .registers 2

    #@0
    .prologue
    .line 585
    iget-boolean v0, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@2
    return v0
.end method

.method public isIconified()Z
    .registers 2

    #@0
    .prologue
    .line 613
    iget-boolean v0, p0, Landroid/widget/SearchView;->mIconified:Z

    #@2
    return v0
.end method

.method public isQueryRefinementEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 665
    iget-boolean v0, p0, Landroid/widget/SearchView;->mQueryRefinement:Z

    #@2
    return v0
.end method

.method public isSubmitButtonEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 635
    iget-boolean v0, p0, Landroid/widget/SearchView;->mSubmitButtonEnabled:Z

    #@2
    return v0
.end method

.method public onActionViewCollapsed()V
    .registers 3

    #@0
    .prologue
    .line 1254
    invoke-virtual {p0}, Landroid/widget/SearchView;->clearFocus()V

    #@3
    .line 1255
    const/4 v0, 0x1

    #@4
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@7
    .line 1256
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@9
    iget v1, p0, Landroid/widget/SearchView;->mCollapsedImeOptions:I

    #@b
    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    #@e
    .line 1257
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/widget/SearchView;->mExpandedInActionView:Z

    #@11
    .line 1258
    return-void
.end method

.method public onActionViewExpanded()V
    .registers 4

    #@0
    .prologue
    .line 1265
    iget-boolean v0, p0, Landroid/widget/SearchView;->mExpandedInActionView:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 1272
    :goto_4
    return-void

    #@5
    .line 1267
    :cond_5
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/widget/SearchView;->mExpandedInActionView:Z

    #@8
    .line 1268
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@a
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->getImeOptions()I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/widget/SearchView;->mCollapsedImeOptions:I

    #@10
    .line 1269
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@12
    iget v1, p0, Landroid/widget/SearchView;->mCollapsedImeOptions:I

    #@14
    const/high16 v2, 0x200

    #@16
    or-int/2addr v1, v2

    #@17
    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    #@1a
    .line 1270
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@1c
    const-string v1, ""

    #@1e
    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    #@21
    .line 1271
    const/4 v0, 0x0

    #@22
    invoke-virtual {p0, v0}, Landroid/widget/SearchView;->setIconified(Z)V

    #@25
    goto :goto_4
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 828
    iget-object v0, p0, Landroid/widget/SearchView;->mUpdateDrawableStateRunnable:Ljava/lang/Runnable;

    #@2
    invoke-virtual {p0, v0}, Landroid/widget/SearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@5
    .line 829
    iget-object v0, p0, Landroid/widget/SearchView;->mReleaseCursorRunnable:Ljava/lang/Runnable;

    #@7
    invoke-virtual {p0, v0}, Landroid/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 830
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    #@d
    .line 831
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1276
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 1277
    const-class v0, Landroid/widget/SearchView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1278
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1282
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 1283
    const-class v0, Landroid/widget/SearchView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 1284
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 883
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 884
    const/4 v1, 0x0

    #@5
    .line 896
    :goto_5
    return v1

    #@6
    .line 889
    :cond_6
    iget-object v1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@8
    invoke-virtual {v1, p1}, Landroid/app/SearchableInfo;->findActionKey(I)Landroid/app/SearchableInfo$ActionKeyInfo;

    #@b
    move-result-object v0

    #@c
    .line 890
    .local v0, actionKey:Landroid/app/SearchableInfo$ActionKeyInfo;
    if-eqz v0, :cond_27

    #@e
    invoke-virtual {v0}, Landroid/app/SearchableInfo$ActionKeyInfo;->getQueryActionMsg()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    if-eqz v1, :cond_27

    #@14
    .line 891
    invoke-virtual {v0}, Landroid/app/SearchableInfo$ActionKeyInfo;->getQueryActionMsg()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@1a
    invoke-virtual {v2}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-direct {p0, p1, v1, v2}, Landroid/widget/SearchView;->launchQuerySearch(ILjava/lang/String;Ljava/lang/String;)V

    #@25
    .line 893
    const/4 v1, 0x1

    #@26
    goto :goto_5

    #@27
    .line 896
    :cond_27
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@2a
    move-result v1

    #@2b
    goto :goto_5
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 713
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_a

    #@6
    .line 714
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@9
    .line 743
    :goto_9
    return-void

    #@a
    .line 718
    :cond_a
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@d
    move-result v1

    #@e
    .line 719
    .local v1, widthMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@11
    move-result v0

    #@12
    .line 721
    .local v0, width:I
    sparse-switch v1, :sswitch_data_4a

    #@15
    .line 741
    :cond_15
    :goto_15
    const/high16 v1, 0x4000

    #@17
    .line 742
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1a
    move-result v2

    #@1b
    invoke-super {p0, v2, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@1e
    goto :goto_9

    #@1f
    .line 724
    :sswitch_1f
    iget v2, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@21
    if-lez v2, :cond_2a

    #@23
    .line 725
    iget v2, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@25
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    #@28
    move-result v0

    #@29
    goto :goto_15

    #@2a
    .line 727
    :cond_2a
    invoke-direct {p0}, Landroid/widget/SearchView;->getPreferredWidth()I

    #@2d
    move-result v2

    #@2e
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    #@31
    move-result v0

    #@32
    .line 729
    goto :goto_15

    #@33
    .line 732
    :sswitch_33
    iget v2, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@35
    if-lez v2, :cond_15

    #@37
    .line 733
    iget v2, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@39
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    #@3c
    move-result v0

    #@3d
    goto :goto_15

    #@3e
    .line 738
    :sswitch_3e
    iget v2, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@40
    if-lez v2, :cond_45

    #@42
    iget v0, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@44
    :goto_44
    goto :goto_15

    #@45
    :cond_45
    invoke-direct {p0}, Landroid/widget/SearchView;->getPreferredWidth()I

    #@48
    move-result v0

    #@49
    goto :goto_44

    #@4a
    .line 721
    :sswitch_data_4a
    .sparse-switch
        -0x80000000 -> :sswitch_1f
        0x0 -> :sswitch_3e
        0x40000000 -> :sswitch_33
    .end sparse-switch
.end method

.method onQueryRefine(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "queryText"

    #@0
    .prologue
    .line 852
    invoke-direct {p0, p1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;)V

    #@3
    .line 853
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 1362
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/SearchView$SearchAutoComplete;->setLayoutDirection(I)V

    #@5
    .line 1363
    return-void
.end method

.method onTextFocusChanged()V
    .registers 2

    #@0
    .prologue
    .line 1233
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@7
    .line 1236
    invoke-direct {p0}, Landroid/widget/SearchView;->postUpdateFocusedState()V

    #@a
    .line 1237
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@c
    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->hasFocus()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 1238
    invoke-direct {p0}, Landroid/widget/SearchView;->forceSuggestionQuery()V

    #@15
    .line 1240
    :cond_15
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 1244
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    #@3
    .line 1246
    invoke-direct {p0}, Landroid/widget/SearchView;->postUpdateFocusedState()V

    #@6
    .line 1247
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .registers 6
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 422
    iget-boolean v2, p0, Landroid/widget/SearchView;->mClearingFocus:Z

    #@3
    if-eqz v2, :cond_7

    #@5
    move v0, v1

    #@6
    .line 433
    :cond_6
    :goto_6
    return v0

    #@7
    .line 424
    :cond_7
    invoke-virtual {p0}, Landroid/widget/SearchView;->isFocusable()Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_f

    #@d
    move v0, v1

    #@e
    goto :goto_6

    #@f
    .line 426
    :cond_f
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_21

    #@15
    .line 427
    iget-object v2, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@17
    invoke-virtual {v2, p1, p2}, Landroid/widget/SearchView$SearchAutoComplete;->requestFocus(ILandroid/graphics/Rect;)Z

    #@1a
    move-result v0

    #@1b
    .line 428
    .local v0, result:Z
    if-eqz v0, :cond_6

    #@1d
    .line 429
    invoke-direct {p0, v1}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@20
    goto :goto_6

    #@21
    .line 433
    .end local v0           #result:Z
    :cond_21
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->requestFocus(ILandroid/graphics/Rect;)Z

    #@24
    move-result v0

    #@25
    goto :goto_6
.end method

.method public setAppSearchData(Landroid/os/Bundle;)V
    .registers 2
    .parameter "appSearchData"

    #@0
    .prologue
    .line 370
    iput-object p1, p0, Landroid/widget/SearchView;->mAppSearchData:Landroid/os/Bundle;

    #@2
    .line 371
    return-void
.end method

.method public setIconified(Z)V
    .registers 2
    .parameter "iconify"

    #@0
    .prologue
    .line 599
    if-eqz p1, :cond_6

    #@2
    .line 600
    invoke-direct {p0}, Landroid/widget/SearchView;->onCloseClicked()V

    #@5
    .line 604
    :goto_5
    return-void

    #@6
    .line 602
    :cond_6
    invoke-direct {p0}, Landroid/widget/SearchView;->onSearchClicked()V

    #@9
    goto :goto_5
.end method

.method public setIconifiedByDefault(Z)V
    .registers 3
    .parameter "iconified"

    #@0
    .prologue
    .line 572
    iget-boolean v0, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 576
    :goto_4
    return-void

    #@5
    .line 573
    :cond_5
    iput-boolean p1, p0, Landroid/widget/SearchView;->mIconifiedByDefault:Z

    #@7
    .line 574
    invoke-direct {p0, p1}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@a
    .line 575
    invoke-direct {p0}, Landroid/widget/SearchView;->updateQueryHint()V

    #@d
    goto :goto_4
.end method

.method public setImeOptions(I)V
    .registers 3
    .parameter "imeOptions"

    #@0
    .prologue
    .line 382
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/SearchView$SearchAutoComplete;->setImeOptions(I)V

    #@5
    .line 383
    return-void
.end method

.method public setInputType(I)V
    .registers 3
    .parameter "inputType"

    #@0
    .prologue
    .line 405
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/SearchView$SearchAutoComplete;->setInputType(I)V

    #@5
    .line 406
    return-void
.end method

.method public setMaxWidth(I)V
    .registers 2
    .parameter "maxpixels"

    #@0
    .prologue
    .line 694
    iput p1, p0, Landroid/widget/SearchView;->mMaxWidth:I

    #@2
    .line 696
    invoke-virtual {p0}, Landroid/widget/SearchView;->requestLayout()V

    #@5
    .line 697
    return-void
.end method

.method public setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 463
    iput-object p1, p0, Landroid/widget/SearchView;->mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

    #@2
    .line 464
    return-void
.end method

.method public setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 472
    iput-object p1, p0, Landroid/widget/SearchView;->mOnQueryTextFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    #@2
    .line 473
    return-void
.end method

.method public setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 454
    iput-object p1, p0, Landroid/widget/SearchView;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@2
    .line 455
    return-void
.end method

.method public setOnSearchClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 493
    iput-object p1, p0, Landroid/widget/SearchView;->mOnSearchClickListener:Landroid/view/View$OnClickListener;

    #@2
    .line 494
    return-void
.end method

.method public setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 481
    iput-object p1, p0, Landroid/widget/SearchView;->mOnSuggestionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@2
    .line 482
    return-void
.end method

.method public setQuery(Ljava/lang/CharSequence;Z)V
    .registers 5
    .parameter "query"
    .parameter "submit"

    #@0
    .prologue
    .line 514
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    #@5
    .line 515
    if-eqz p1, :cond_14

    #@7
    .line 516
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@9
    iget-object v1, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@b
    invoke-virtual {v1}, Landroid/widget/SearchView$SearchAutoComplete;->length()I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    #@12
    .line 517
    iput-object p1, p0, Landroid/widget/SearchView;->mUserQuery:Ljava/lang/CharSequence;

    #@14
    .line 521
    :cond_14
    if-eqz p2, :cond_1f

    #@16
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_1f

    #@1c
    .line 522
    invoke-direct {p0}, Landroid/widget/SearchView;->onSubmitQuery()V

    #@1f
    .line 524
    :cond_1f
    return-void
.end method

.method public setQueryHint(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "hint"

    #@0
    .prologue
    .line 535
    iput-object p1, p0, Landroid/widget/SearchView;->mQueryHint:Ljava/lang/CharSequence;

    #@2
    .line 536
    invoke-direct {p0}, Landroid/widget/SearchView;->updateQueryHint()V

    #@5
    .line 537
    return-void
.end method

.method public setQueryRefinementEnabled(Z)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 653
    iput-boolean p1, p0, Landroid/widget/SearchView;->mQueryRefinement:Z

    #@2
    .line 654
    iget-object v0, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@4
    instance-of v0, v0, Landroid/widget/SuggestionsAdapter;

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 655
    iget-object v0, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@a
    check-cast v0, Landroid/widget/SuggestionsAdapter;

    #@c
    if-eqz p1, :cond_13

    #@e
    const/4 v1, 0x2

    #@f
    :goto_f
    invoke-virtual {v0, v1}, Landroid/widget/SuggestionsAdapter;->setQueryRefinement(I)V

    #@12
    .line 658
    :cond_12
    return-void

    #@13
    .line 655
    :cond_13
    const/4 v1, 0x1

    #@14
    goto :goto_f
.end method

.method public setSearchableInfo(Landroid/app/SearchableInfo;)V
    .registers 4
    .parameter "searchable"

    #@0
    .prologue
    .line 348
    iput-object p1, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    .line 349
    iget-object v0, p0, Landroid/widget/SearchView;->mSearchable:Landroid/app/SearchableInfo;

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 350
    invoke-direct {p0}, Landroid/widget/SearchView;->updateSearchAutoComplete()V

    #@9
    .line 351
    invoke-direct {p0}, Landroid/widget/SearchView;->updateQueryHint()V

    #@c
    .line 354
    :cond_c
    invoke-direct {p0}, Landroid/widget/SearchView;->hasVoiceSearch()Z

    #@f
    move-result v0

    #@10
    iput-boolean v0, p0, Landroid/widget/SearchView;->mVoiceButtonEnabled:Z

    #@12
    .line 356
    iget-boolean v0, p0, Landroid/widget/SearchView;->mVoiceButtonEnabled:Z

    #@14
    if-eqz v0, :cond_1e

    #@16
    .line 359
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@18
    const-string/jumbo v1, "nm"

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setPrivateImeOptions(Ljava/lang/String;)V

    #@1e
    .line 361
    :cond_1e
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@21
    move-result v0

    #@22
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@25
    .line 362
    return-void
.end method

.method public setSubmitButtonEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 625
    iput-boolean p1, p0, Landroid/widget/SearchView;->mSubmitButtonEnabled:Z

    #@2
    .line 626
    invoke-virtual {p0}, Landroid/widget/SearchView;->isIconified()Z

    #@5
    move-result v0

    #@6
    invoke-direct {p0, v0}, Landroid/widget/SearchView;->updateViewsVisibility(Z)V

    #@9
    .line 627
    return-void
.end method

.method public setSuggestionsAdapter(Landroid/widget/CursorAdapter;)V
    .registers 4
    .parameter "adapter"

    #@0
    .prologue
    .line 675
    iput-object p1, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@2
    .line 677
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    #@4
    iget-object v1, p0, Landroid/widget/SearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    #@6
    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    #@9
    .line 678
    return-void
.end method
