.class public Landroid/widget/QuickContactBadge;
.super Landroid/widget/ImageView;
.source "QuickContactBadge.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/QuickContactBadge$QueryHandler;
    }
.end annotation


# static fields
.field static final EMAIL_ID_COLUMN_INDEX:I = 0x0

.field static final EMAIL_LOOKUP_PROJECTION:[Ljava/lang/String; = null

.field static final EMAIL_LOOKUP_STRING_COLUMN_INDEX:I = 0x1

.field static final PHONE_ID_COLUMN_INDEX:I = 0x0

.field static final PHONE_LOOKUP_PROJECTION:[Ljava/lang/String; = null

.field static final PHONE_LOOKUP_STRING_COLUMN_INDEX:I = 0x1

.field private static final TOKEN_EMAIL_LOOKUP:I = 0x0

.field private static final TOKEN_EMAIL_LOOKUP_AND_TRIGGER:I = 0x2

.field private static final TOKEN_PHONE_LOOKUP:I = 0x1

.field private static final TOKEN_PHONE_LOOKUP_AND_TRIGGER:I = 0x3


# instance fields
.field private mContactEmail:Ljava/lang/String;

.field private mContactPhone:Ljava/lang/String;

.field private mContactUri:Landroid/net/Uri;

.field private mDefaultAvatar:Landroid/graphics/drawable/Drawable;

.field protected mExcludeMimes:[Ljava/lang/String;

.field private mOverlay:Landroid/graphics/drawable/Drawable;

.field private mQueryHandler:Landroid/widget/QuickContactBadge$QueryHandler;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 61
    new-array v0, v4, [Ljava/lang/String;

    #@5
    const-string v1, "contact_id"

    #@7
    aput-object v1, v0, v2

    #@9
    const-string/jumbo v1, "lookup"

    #@c
    aput-object v1, v0, v3

    #@e
    sput-object v0, Landroid/widget/QuickContactBadge;->EMAIL_LOOKUP_PROJECTION:[Ljava/lang/String;

    #@10
    .line 68
    new-array v0, v4, [Ljava/lang/String;

    #@12
    const-string v1, "_id"

    #@14
    aput-object v1, v0, v2

    #@16
    const-string/jumbo v1, "lookup"

    #@19
    aput-object v1, v0, v3

    #@1b
    sput-object v0, Landroid/widget/QuickContactBadge;->PHONE_LOOKUP_PROJECTION:[Ljava/lang/String;

    #@1d
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 76
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 54
    const/4 v1, 0x0

    #@4
    iput-object v1, p0, Landroid/widget/QuickContactBadge;->mExcludeMimes:[Ljava/lang/String;

    #@6
    .line 86
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@8
    sget-object v2, Lcom/android/internal/R$styleable;->Theme:[I

    #@a
    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@d
    move-result-object v0

    #@e
    .line 87
    .local v0, styledAttributes:Landroid/content/res/TypedArray;
    const/16 v1, 0xe9

    #@10
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@16
    .line 89
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@19
    .line 91
    new-instance v1, Landroid/widget/QuickContactBadge$QueryHandler;

    #@1b
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, p0, v2}, Landroid/widget/QuickContactBadge$QueryHandler;-><init>(Landroid/widget/QuickContactBadge;Landroid/content/ContentResolver;)V

    #@24
    iput-object v1, p0, Landroid/widget/QuickContactBadge;->mQueryHandler:Landroid/widget/QuickContactBadge$QueryHandler;

    #@26
    .line 92
    invoke-virtual {p0, p0}, Landroid/widget/QuickContactBadge;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@29
    .line 93
    return-void
.end method

.method static synthetic access$002(Landroid/widget/QuickContactBadge;Landroid/net/Uri;)Landroid/net/Uri;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Landroid/widget/QuickContactBadge;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/widget/QuickContactBadge;->onContactUriChanged()V

    #@3
    return-void
.end method

.method private isAssigned()Z
    .registers 2

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@2
    if-nez v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@6
    if-nez v0, :cond_c

    #@8
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private onContactUriChanged()V
    .registers 2

    #@0
    .prologue
    .line 217
    invoke-direct {p0}, Landroid/widget/QuickContactBadge;->isAssigned()Z

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/widget/QuickContactBadge;->setEnabled(Z)V

    #@7
    .line 218
    return-void
.end method


# virtual methods
.method public assignContactFromEmail(Ljava/lang/String;Z)V
    .registers 11
    .parameter "emailAddress"
    .parameter "lazyLookup"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 178
    iput-object p1, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@3
    .line 179
    if-nez p2, :cond_1d

    #@5
    .line 180
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mQueryHandler:Landroid/widget/QuickContactBadge$QueryHandler;

    #@7
    const/4 v1, 0x0

    #@8
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    #@a
    iget-object v4, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@c
    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v4

    #@10
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@13
    move-result-object v3

    #@14
    sget-object v4, Landroid/widget/QuickContactBadge;->EMAIL_LOOKUP_PROJECTION:[Ljava/lang/String;

    #@16
    move-object v5, v2

    #@17
    move-object v6, v2

    #@18
    move-object v7, v2

    #@19
    invoke-virtual/range {v0 .. v7}, Landroid/widget/QuickContactBadge$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 190
    :goto_1c
    return-void

    #@1d
    .line 184
    :cond_1d
    iput-object v2, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@1f
    .line 186
    iput-object v2, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@21
    .line 188
    invoke-direct {p0}, Landroid/widget/QuickContactBadge;->onContactUriChanged()V

    #@24
    goto :goto_1c
.end method

.method public assignContactFromPhone(Ljava/lang/String;Z)V
    .registers 11
    .parameter "phoneNumber"
    .parameter "lazyLookup"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 202
    iput-object p1, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@3
    .line 203
    if-nez p2, :cond_19

    #@5
    .line 204
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mQueryHandler:Landroid/widget/QuickContactBadge$QueryHandler;

    #@7
    const/4 v1, 0x1

    #@8
    sget-object v3, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@a
    iget-object v4, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@c
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@f
    move-result-object v3

    #@10
    sget-object v4, Landroid/widget/QuickContactBadge;->PHONE_LOOKUP_PROJECTION:[Ljava/lang/String;

    #@12
    move-object v5, v2

    #@13
    move-object v6, v2

    #@14
    move-object v7, v2

    #@15
    invoke-virtual/range {v0 .. v7}, Landroid/widget/QuickContactBadge$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 214
    :goto_18
    return-void

    #@19
    .line 208
    :cond_19
    iput-object v2, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@1b
    .line 210
    iput-object v2, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@1d
    .line 212
    invoke-direct {p0}, Landroid/widget/QuickContactBadge;->onContactUriChanged()V

    #@20
    goto :goto_18
.end method

.method public assignContactUri(Landroid/net/Uri;)V
    .registers 3
    .parameter "contactUri"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 162
    iput-object p1, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@3
    .line 163
    iput-object v0, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@5
    .line 164
    iput-object v0, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@7
    .line 165
    invoke-direct {p0}, Landroid/widget/QuickContactBadge;->onContactUriChanged()V

    #@a
    .line 166
    return-void
.end method

.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    #@3
    .line 98
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_1b

    #@7
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_1b

    #@f
    .line 99
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->getDrawableState()[I

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@18
    .line 100
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->invalidate()V

    #@1b
    .line 102
    :cond_1b
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 10
    .parameter "v"

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    const/4 v5, 0x0

    #@2
    .line 222
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@4
    if-eqz v0, :cond_12

    #@6
    .line 223
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    iget-object v2, p0, Landroid/widget/QuickContactBadge;->mContactUri:Landroid/net/Uri;

    #@c
    iget-object v3, p0, Landroid/widget/QuickContactBadge;->mExcludeMimes:[Ljava/lang/String;

    #@e
    invoke-static {v0, p0, v2, v1, v3}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    #@11
    .line 237
    :cond_11
    :goto_11
    return-void

    #@12
    .line 225
    :cond_12
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@14
    if-eqz v0, :cond_2f

    #@16
    .line 226
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mQueryHandler:Landroid/widget/QuickContactBadge$QueryHandler;

    #@18
    const/4 v1, 0x2

    #@19
    iget-object v2, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@1b
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    #@1d
    iget-object v4, p0, Landroid/widget/QuickContactBadge;->mContactEmail:Ljava/lang/String;

    #@1f
    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@26
    move-result-object v3

    #@27
    sget-object v4, Landroid/widget/QuickContactBadge;->EMAIL_LOOKUP_PROJECTION:[Ljava/lang/String;

    #@29
    move-object v6, v5

    #@2a
    move-object v7, v5

    #@2b
    invoke-virtual/range {v0 .. v7}, Landroid/widget/QuickContactBadge$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    goto :goto_11

    #@2f
    .line 229
    :cond_2f
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@31
    if-eqz v0, :cond_11

    #@33
    .line 230
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mQueryHandler:Landroid/widget/QuickContactBadge$QueryHandler;

    #@35
    iget-object v2, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@37
    sget-object v3, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@39
    iget-object v4, p0, Landroid/widget/QuickContactBadge;->mContactPhone:Ljava/lang/String;

    #@3b
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@3e
    move-result-object v3

    #@3f
    sget-object v4, Landroid/widget/QuickContactBadge;->PHONE_LOOKUP_PROJECTION:[Ljava/lang/String;

    #@41
    move-object v6, v5

    #@42
    move-object v7, v5

    #@43
    invoke-virtual/range {v0 .. v7}, Landroid/widget/QuickContactBadge$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    #@46
    goto :goto_11
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 111
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    #@4
    .line 113
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 135
    :cond_a
    :goto_a
    return-void

    #@b
    .line 118
    :cond_b
    iget-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@d
    if-eqz v1, :cond_a

    #@f
    iget-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_a

    #@17
    iget-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@19
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_a

    #@1f
    .line 124
    iget-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@21
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->getWidth()I

    #@24
    move-result v2

    #@25
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->getHeight()I

    #@28
    move-result v3

    #@29
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2c
    .line 126
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@2e
    if-nez v1, :cond_3a

    #@30
    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    #@32
    if-nez v1, :cond_3a

    #@34
    .line 127
    iget-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@36
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@39
    goto :goto_a

    #@3a
    .line 129
    :cond_3a
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    #@3d
    move-result v0

    #@3e
    .line 130
    .local v0, saveCount:I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@41
    .line 131
    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    #@43
    int-to-float v1, v1

    #@44
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@46
    int-to-float v2, v2

    #@47
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@4a
    .line 132
    iget-object v1, p0, Landroid/widget/QuickContactBadge;->mOverlay:Landroid/graphics/drawable/Drawable;

    #@4c
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@4f
    .line 133
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@52
    goto :goto_a
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 241
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 242
    const-class v0, Landroid/widget/QuickContactBadge;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 243
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 248
    const-class v0, Landroid/widget/QuickContactBadge;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 249
    return-void
.end method

.method public setExcludeMimes([Ljava/lang/String;)V
    .registers 2
    .parameter "excludeMimes"

    #@0
    .prologue
    .line 257
    iput-object p1, p0, Landroid/widget/QuickContactBadge;->mExcludeMimes:[Ljava/lang/String;

    #@2
    .line 258
    return-void
.end method

.method public setImageToDefault()V
    .registers 3

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mDefaultAvatar:Landroid/graphics/drawable/Drawable;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 147
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    const v1, 0x10802c5

    #@b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/widget/QuickContactBadge;->mDefaultAvatar:Landroid/graphics/drawable/Drawable;

    #@11
    .line 149
    :cond_11
    iget-object v0, p0, Landroid/widget/QuickContactBadge;->mDefaultAvatar:Landroid/graphics/drawable/Drawable;

    #@13
    invoke-virtual {p0, v0}, Landroid/widget/QuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@16
    .line 150
    return-void
.end method

.method public setMode(I)V
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 107
    return-void
.end method
