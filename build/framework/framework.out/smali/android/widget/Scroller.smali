.class public Landroid/widget/Scroller;
.super Ljava/lang/Object;
.source "Scroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Scroller$FrameRate;
    }
.end annotation


# static fields
.field private static final ABNORMAL_FRAMETIME_GAP_LOWER:F = 13.0f

.field private static final ABNORMAL_FRAMETIME_GAP_MAX:I = 0x28

.field private static final ABNORMAL_FRAMETIME_GAP_UPPER:F = 22.0f

.field private static final COEF_OF_ABNORMAL_FRAMETIME_GAP_LOWER:F = 1.3f

.field private static DECELERATION_RATE:F = 0.0f

.field private static final DEFAULT_DURATION:I = 0xfa

.field private static final END_TENSION:F = 1.0f

.field private static final FLING_COMPUTE_MODE_USING_CONSTANT_TIME_DIFF:I = 0x1

.field private static final FLING_COMPUTE_ORIGINAL_MODE:I = 0x0

.field private static final FLING_MODE:I = 0x1

.field private static final INFLEXION:F = 0.35f

.field private static final NB_SAMPLES:I = 0x64

.field private static final P1:F = 0.175f

.field private static final P2:F = 0.35000002f

.field private static final SCROLL_MODE:I = 0x0

.field private static final SPLINE_POSITION:[F = null

.field private static final SPLINE_TIME:[F = null

.field private static final START_TENSION:F = 0.5f

.field private static sViscousFluidNormalize:F

.field private static sViscousFluidScale:F


# instance fields
.field private flingComputeMode:I

.field private frameRate:Landroid/widget/Scroller$FrameRate;

.field private isNormalFlick:Z

.field private lastTimePassed:J

.field private mCurrVelocity:F

.field private mCurrX:I

.field private mCurrY:I

.field private mDeceleration:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDistance:I

.field private mDuration:I

.field private mDurationReciprocal:F

.field private mFinalX:I

.field private mFinalY:I

.field private mFinished:Z

.field private mFlingFriction:F

.field private mFlywheel:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mMaxX:I

.field private mMaxY:I

.field private mMinX:I

.field private mMinY:I

.field private mMode:I

.field private mPhysicalCoeff:F

.field private final mPpi:F

.field private mStartTime:J

.field private mStartX:I

.field private mStartY:I

.field private mVelocity:F

.field private timePassedDiffConstant:F


# direct methods
.method static constructor <clinit>()V
    .registers 16

    #@0
    .prologue
    .line 98
    const-wide v11, 0x3fe8f5c28f5c28f6L

    #@5
    invoke-static {v11, v12}, Ljava/lang/Math;->log(D)D

    #@8
    move-result-wide v11

    #@9
    const-wide v13, 0x3feccccccccccccdL

    #@e
    invoke-static {v13, v14}, Ljava/lang/Math;->log(D)D

    #@11
    move-result-wide v13

    #@12
    div-double/2addr v11, v13

    #@13
    double-to-float v11, v11

    #@14
    sput v11, Landroid/widget/Scroller;->DECELERATION_RATE:F

    #@16
    .line 106
    const/16 v11, 0x65

    #@18
    new-array v11, v11, [F

    #@1a
    sput-object v11, Landroid/widget/Scroller;->SPLINE_POSITION:[F

    #@1c
    .line 107
    const/16 v11, 0x65

    #@1e
    new-array v11, v11, [F

    #@20
    sput-object v11, Landroid/widget/Scroller;->SPLINE_TIME:[F

    #@22
    .line 116
    const/4 v7, 0x0

    #@23
    .line 117
    .local v7, x_min:F
    const/4 v10, 0x0

    #@24
    .line 118
    .local v10, y_min:F
    const/4 v3, 0x0

    #@25
    .local v3, i:I
    :goto_25
    const/16 v11, 0x64

    #@27
    if-ge v3, v11, :cond_ca

    #@29
    .line 119
    int-to-float v11, v3

    #@2a
    const/high16 v12, 0x42c8

    #@2c
    div-float v0, v11, v12

    #@2e
    .line 121
    .local v0, alpha:F
    const/high16 v6, 0x3f80

    #@30
    .line 124
    .local v6, x_max:F
    :goto_30
    sub-float v11, v6, v7

    #@32
    const/high16 v12, 0x4000

    #@34
    div-float/2addr v11, v12

    #@35
    add-float v5, v7, v11

    #@37
    .line 125
    .local v5, x:F
    const/high16 v11, 0x4040

    #@39
    mul-float/2addr v11, v5

    #@3a
    const/high16 v12, 0x3f80

    #@3c
    sub-float/2addr v12, v5

    #@3d
    mul-float v1, v11, v12

    #@3f
    .line 126
    .local v1, coef:F
    const/high16 v11, 0x3f80

    #@41
    sub-float/2addr v11, v5

    #@42
    const v12, 0x3e333333

    #@45
    mul-float/2addr v11, v12

    #@46
    const v12, 0x3eb33334

    #@49
    mul-float/2addr v12, v5

    #@4a
    add-float/2addr v11, v12

    #@4b
    mul-float/2addr v11, v1

    #@4c
    mul-float v12, v5, v5

    #@4e
    mul-float/2addr v12, v5

    #@4f
    add-float v4, v11, v12

    #@51
    .line 127
    .local v4, tx:F
    sub-float v11, v4, v0

    #@53
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    #@56
    move-result v11

    #@57
    float-to-double v11, v11

    #@58
    const-wide v13, 0x3ee4f8b588e368f1L

    #@5d
    cmpg-double v11, v11, v13

    #@5f
    if-gez v11, :cond_b8

    #@61
    .line 131
    sget-object v11, Landroid/widget/Scroller;->SPLINE_POSITION:[F

    #@63
    const/high16 v12, 0x3f80

    #@65
    sub-float/2addr v12, v5

    #@66
    const/high16 v13, 0x3f00

    #@68
    mul-float/2addr v12, v13

    #@69
    add-float/2addr v12, v5

    #@6a
    mul-float/2addr v12, v1

    #@6b
    mul-float v13, v5, v5

    #@6d
    mul-float/2addr v13, v5

    #@6e
    add-float/2addr v12, v13

    #@6f
    aput v12, v11, v3

    #@71
    .line 133
    const/high16 v9, 0x3f80

    #@73
    .line 136
    .local v9, y_max:F
    :goto_73
    sub-float v11, v9, v10

    #@75
    const/high16 v12, 0x4000

    #@77
    div-float/2addr v11, v12

    #@78
    add-float v8, v10, v11

    #@7a
    .line 137
    .local v8, y:F
    const/high16 v11, 0x4040

    #@7c
    mul-float/2addr v11, v8

    #@7d
    const/high16 v12, 0x3f80

    #@7f
    sub-float/2addr v12, v8

    #@80
    mul-float v1, v11, v12

    #@82
    .line 138
    const/high16 v11, 0x3f80

    #@84
    sub-float/2addr v11, v8

    #@85
    const/high16 v12, 0x3f00

    #@87
    mul-float/2addr v11, v12

    #@88
    add-float/2addr v11, v8

    #@89
    mul-float/2addr v11, v1

    #@8a
    mul-float v12, v8, v8

    #@8c
    mul-float/2addr v12, v8

    #@8d
    add-float v2, v11, v12

    #@8f
    .line 139
    .local v2, dy:F
    sub-float v11, v2, v0

    #@91
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    #@94
    move-result v11

    #@95
    float-to-double v11, v11

    #@96
    const-wide v13, 0x3ee4f8b588e368f1L

    #@9b
    cmpg-double v11, v11, v13

    #@9d
    if-gez v11, :cond_c2

    #@9f
    .line 143
    sget-object v11, Landroid/widget/Scroller;->SPLINE_TIME:[F

    #@a1
    const/high16 v12, 0x3f80

    #@a3
    sub-float/2addr v12, v8

    #@a4
    const v13, 0x3e333333

    #@a7
    mul-float/2addr v12, v13

    #@a8
    const v13, 0x3eb33334

    #@ab
    mul-float/2addr v13, v8

    #@ac
    add-float/2addr v12, v13

    #@ad
    mul-float/2addr v12, v1

    #@ae
    mul-float v13, v8, v8

    #@b0
    mul-float/2addr v13, v8

    #@b1
    add-float/2addr v12, v13

    #@b2
    aput v12, v11, v3

    #@b4
    .line 118
    add-int/lit8 v3, v3, 0x1

    #@b6
    goto/16 :goto_25

    #@b8
    .line 128
    .end local v2           #dy:F
    .end local v8           #y:F
    .end local v9           #y_max:F
    :cond_b8
    cmpl-float v11, v4, v0

    #@ba
    if-lez v11, :cond_bf

    #@bc
    move v6, v5

    #@bd
    goto/16 :goto_30

    #@bf
    .line 129
    :cond_bf
    move v7, v5

    #@c0
    goto/16 :goto_30

    #@c2
    .line 140
    .restart local v2       #dy:F
    .restart local v8       #y:F
    .restart local v9       #y_max:F
    :cond_c2
    cmpl-float v11, v2, v0

    #@c4
    if-lez v11, :cond_c8

    #@c6
    move v9, v8

    #@c7
    goto :goto_73

    #@c8
    .line 141
    :cond_c8
    move v10, v8

    #@c9
    goto :goto_73

    #@ca
    .line 145
    .end local v0           #alpha:F
    .end local v1           #coef:F
    .end local v2           #dy:F
    .end local v4           #tx:F
    .end local v5           #x:F
    .end local v6           #x_max:F
    .end local v8           #y:F
    .end local v9           #y_max:F
    :cond_ca
    sget-object v11, Landroid/widget/Scroller;->SPLINE_POSITION:[F

    #@cc
    const/16 v12, 0x64

    #@ce
    sget-object v13, Landroid/widget/Scroller;->SPLINE_TIME:[F

    #@d0
    const/16 v14, 0x64

    #@d2
    const/high16 v15, 0x3f80

    #@d4
    aput v15, v13, v14

    #@d6
    aput v15, v11, v12

    #@d8
    .line 148
    const/high16 v11, 0x4100

    #@da
    sput v11, Landroid/widget/Scroller;->sViscousFluidScale:F

    #@dc
    .line 150
    const/high16 v11, 0x3f80

    #@de
    sput v11, Landroid/widget/Scroller;->sViscousFluidNormalize:F

    #@e0
    .line 151
    const/high16 v11, 0x3f80

    #@e2
    const/high16 v12, 0x3f80

    #@e4
    invoke-static {v12}, Landroid/widget/Scroller;->viscousFluid(F)F

    #@e7
    move-result v12

    #@e8
    div-float/2addr v11, v12

    #@e9
    sput v11, Landroid/widget/Scroller;->sViscousFluidNormalize:F

    #@eb
    .line 153
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 162
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    #@4
    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .registers 5
    .parameter "context"
    .parameter "interpolator"

    #@0
    .prologue
    .line 171
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@3
    move-result-object v0

    #@4
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@6
    const/16 v1, 0xb

    #@8
    if-lt v0, v1, :cond_f

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    #@e
    .line 173
    return-void

    #@f
    .line 171
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_b
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V
    .registers 6
    .parameter "context"
    .parameter "interpolator"
    .parameter "flywheel"

    #@0
    .prologue
    .line 180
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 92
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/widget/Scroller;->mFlingFriction:F

    #@9
    .line 181
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Landroid/widget/Scroller;->mFinished:Z

    #@c
    .line 182
    iput-object p2, p0, Landroid/widget/Scroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@e
    .line 183
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@15
    move-result-object v0

    #@16
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    #@18
    const/high16 v1, 0x4320

    #@1a
    mul-float/2addr v0, v1

    #@1b
    iput v0, p0, Landroid/widget/Scroller;->mPpi:F

    #@1d
    .line 184
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    #@20
    move-result v0

    #@21
    invoke-direct {p0, v0}, Landroid/widget/Scroller;->computeDeceleration(F)F

    #@24
    move-result v0

    #@25
    iput v0, p0, Landroid/widget/Scroller;->mDeceleration:F

    #@27
    .line 185
    iput-boolean p3, p0, Landroid/widget/Scroller;->mFlywheel:Z

    #@29
    .line 187
    const v0, 0x3f570a3d

    #@2c
    invoke-direct {p0, v0}, Landroid/widget/Scroller;->computeDeceleration(F)F

    #@2f
    move-result v0

    #@30
    iput v0, p0, Landroid/widget/Scroller;->mPhysicalCoeff:F

    #@32
    .line 189
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@34
    if-eqz v0, :cond_40

    #@36
    .line 190
    new-instance v0, Landroid/widget/Scroller$FrameRate;

    #@38
    invoke-direct {v0, p0}, Landroid/widget/Scroller$FrameRate;-><init>(Landroid/widget/Scroller;)V

    #@3b
    iput-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@3d
    .line 191
    const/4 v0, 0x0

    #@3e
    iput-boolean v0, p0, Landroid/widget/Scroller;->isNormalFlick:Z

    #@40
    .line 193
    :cond_40
    return-void
.end method

.method private computeDeceleration(F)F
    .registers 4
    .parameter "friction"

    #@0
    .prologue
    .line 208
    const v0, 0x43c10b3d

    #@3
    iget v1, p0, Landroid/widget/Scroller;->mPpi:F

    #@5
    mul-float/2addr v0, v1

    #@6
    mul-float/2addr v0, p1

    #@7
    return v0
.end method

.method private getSplineDeceleration(F)D
    .registers 5
    .parameter "velocity"

    #@0
    .prologue
    .line 610
    const v0, 0x3eb33333

    #@3
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@6
    move-result v1

    #@7
    mul-float/2addr v0, v1

    #@8
    iget v1, p0, Landroid/widget/Scroller;->mFlingFriction:F

    #@a
    iget v2, p0, Landroid/widget/Scroller;->mPhysicalCoeff:F

    #@c
    mul-float/2addr v1, v2

    #@d
    div-float/2addr v0, v1

    #@e
    float-to-double v0, v0

    #@f
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    #@12
    move-result-wide v0

    #@13
    return-wide v0
.end method

.method private getSplineFlingDistance(F)D
    .registers 10
    .parameter "velocity"

    #@0
    .prologue
    .line 620
    invoke-direct {p0, p1}, Landroid/widget/Scroller;->getSplineDeceleration(F)D

    #@3
    move-result-wide v2

    #@4
    .line 621
    .local v2, l:D
    sget v4, Landroid/widget/Scroller;->DECELERATION_RATE:F

    #@6
    float-to-double v4, v4

    #@7
    const-wide/high16 v6, 0x3ff0

    #@9
    sub-double v0, v4, v6

    #@b
    .line 622
    .local v0, decelMinusOne:D
    iget v4, p0, Landroid/widget/Scroller;->mFlingFriction:F

    #@d
    iget v5, p0, Landroid/widget/Scroller;->mPhysicalCoeff:F

    #@f
    mul-float/2addr v4, v5

    #@10
    float-to-double v4, v4

    #@11
    sget v6, Landroid/widget/Scroller;->DECELERATION_RATE:F

    #@13
    float-to-double v6, v6

    #@14
    div-double/2addr v6, v0

    #@15
    mul-double/2addr v6, v2

    #@16
    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    #@19
    move-result-wide v6

    #@1a
    mul-double/2addr v4, v6

    #@1b
    return-wide v4
.end method

.method private getSplineFlingDuration(F)I
    .registers 10
    .parameter "velocity"

    #@0
    .prologue
    .line 614
    invoke-direct {p0, p1}, Landroid/widget/Scroller;->getSplineDeceleration(F)D

    #@3
    move-result-wide v2

    #@4
    .line 615
    .local v2, l:D
    sget v4, Landroid/widget/Scroller;->DECELERATION_RATE:F

    #@6
    float-to-double v4, v4

    #@7
    const-wide/high16 v6, 0x3ff0

    #@9
    sub-double v0, v4, v6

    #@b
    .line 616
    .local v0, decelMinusOne:D
    const-wide v4, 0x408f400000000000L

    #@10
    div-double v6, v2, v0

    #@12
    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    #@15
    move-result-wide v6

    #@16
    mul-double/2addr v4, v6

    #@17
    double-to-int v4, v4

    #@18
    return v4
.end method

.method static viscousFluid(F)F
    .registers 5
    .parameter "x"

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    .line 627
    sget v1, Landroid/widget/Scroller;->sViscousFluidScale:F

    #@4
    mul-float/2addr p0, v1

    #@5
    .line 628
    cmpg-float v1, p0, v3

    #@7
    if-gez v1, :cond_17

    #@9
    .line 629
    neg-float v1, p0

    #@a
    float-to-double v1, v1

    #@b
    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    #@e
    move-result-wide v1

    #@f
    double-to-float v1, v1

    #@10
    sub-float v1, v3, v1

    #@12
    sub-float/2addr p0, v1

    #@13
    .line 635
    :goto_13
    sget v1, Landroid/widget/Scroller;->sViscousFluidNormalize:F

    #@15
    mul-float/2addr p0, v1

    #@16
    .line 636
    return p0

    #@17
    .line 631
    :cond_17
    const v0, 0x3ebc5ab2

    #@1a
    .line 632
    .local v0, start:F
    sub-float v1, v3, p0

    #@1c
    float-to-double v1, v1

    #@1d
    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    #@20
    move-result-wide v1

    #@21
    double-to-float v1, v1

    #@22
    sub-float p0, v3, v1

    #@24
    .line 633
    sub-float v1, v3, v0

    #@26
    mul-float/2addr v1, p0

    #@27
    add-float p0, v0, v1

    #@29
    goto :goto_13
.end method


# virtual methods
.method public abortAnimation()V
    .registers 2

    #@0
    .prologue
    .line 647
    iget v0, p0, Landroid/widget/Scroller;->mFinalX:I

    #@2
    iput v0, p0, Landroid/widget/Scroller;->mCurrX:I

    #@4
    .line 648
    iget v0, p0, Landroid/widget/Scroller;->mFinalY:I

    #@6
    iput v0, p0, Landroid/widget/Scroller;->mCurrY:I

    #@8
    .line 649
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/widget/Scroller;->mFinished:Z

    #@b
    .line 650
    return-void
.end method

.method public computeScrollOffset()Z
    .registers 18

    #@0
    .prologue
    .line 369
    move-object/from16 v0, p0

    #@2
    iget-boolean v13, v0, Landroid/widget/Scroller;->mFinished:Z

    #@4
    if-eqz v13, :cond_8

    #@6
    .line 370
    const/4 v13, 0x0

    #@7
    .line 448
    :goto_7
    return v13

    #@8
    .line 373
    :cond_8
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@b
    move-result-wide v13

    #@c
    move-object/from16 v0, p0

    #@e
    iget-wide v15, v0, Landroid/widget/Scroller;->mStartTime:J

    #@10
    sub-long/2addr v13, v15

    #@11
    long-to-int v10, v13

    #@12
    .line 375
    .local v10, timePassed:I
    sget-boolean v13, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@14
    if-eqz v13, :cond_8b

    #@16
    .line 376
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@19
    move-result-wide v1

    #@1a
    .line 378
    .local v1, currentTime:J
    move-object/from16 v0, p0

    #@1c
    iget-boolean v13, v0, Landroid/widget/Scroller;->isNormalFlick:Z

    #@1e
    if-eqz v13, :cond_8b

    #@20
    .line 379
    move-object/from16 v0, p0

    #@22
    iget-object v13, v0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@24
    iget v14, v13, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@26
    add-int/lit8 v14, v14, 0x1

    #@28
    iput v14, v13, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@2a
    .line 380
    move-object/from16 v0, p0

    #@2c
    iget-object v13, v0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@2e
    move-object/from16 v0, p0

    #@30
    iget-wide v14, v0, Landroid/widget/Scroller;->mStartTime:J

    #@32
    sub-long v14, v1, v14

    #@34
    iput-wide v14, v13, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@36
    .line 382
    move-object/from16 v0, p0

    #@38
    iget v13, v0, Landroid/widget/Scroller;->flingComputeMode:I

    #@3a
    const/4 v14, 0x1

    #@3b
    if-ne v13, v14, :cond_6b

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget-wide v13, v0, Landroid/widget/Scroller;->lastTimePassed:J

    #@41
    sub-long v13, v1, v13

    #@43
    const-wide/16 v15, 0x28

    #@45
    cmp-long v13, v13, v15

    #@47
    if-ltz v13, :cond_6b

    #@49
    move-object/from16 v0, p0

    #@4b
    iget-object v13, v0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@4d
    iget v13, v13, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@4f
    const/4 v14, 0x1

    #@50
    if-le v13, v14, :cond_6b

    #@52
    .line 384
    move-object/from16 v0, p0

    #@54
    iget-object v13, v0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@56
    iget v14, v13, Landroid/widget/Scroller$FrameRate;->extraTime:I

    #@58
    move-object/from16 v0, p0

    #@5a
    iget-wide v15, v0, Landroid/widget/Scroller;->lastTimePassed:J

    #@5c
    sub-long v15, v1, v15

    #@5e
    long-to-float v15, v15

    #@5f
    move-object/from16 v0, p0

    #@61
    iget v0, v0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@63
    move/from16 v16, v0

    #@65
    sub-float v15, v15, v16

    #@67
    float-to-int v15, v15

    #@68
    add-int/2addr v14, v15

    #@69
    iput v14, v13, Landroid/widget/Scroller$FrameRate;->extraTime:I

    #@6b
    .line 386
    :cond_6b
    move-object/from16 v0, p0

    #@6d
    iput-wide v1, v0, Landroid/widget/Scroller;->lastTimePassed:J

    #@6f
    .line 388
    move-object/from16 v0, p0

    #@71
    iget v13, v0, Landroid/widget/Scroller;->flingComputeMode:I

    #@73
    const/4 v14, 0x1

    #@74
    if-ne v13, v14, :cond_8b

    #@76
    .line 389
    move-object/from16 v0, p0

    #@78
    iget v13, v0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget-object v14, v0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@7e
    iget v14, v14, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@80
    int-to-float v14, v14

    #@81
    mul-float/2addr v13, v14

    #@82
    float-to-int v13, v13

    #@83
    move-object/from16 v0, p0

    #@85
    iget-object v14, v0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@87
    iget v14, v14, Landroid/widget/Scroller$FrameRate;->extraTime:I

    #@89
    add-int v10, v13, v14

    #@8b
    .line 394
    .end local v1           #currentTime:J
    :cond_8b
    move-object/from16 v0, p0

    #@8d
    iget v13, v0, Landroid/widget/Scroller;->mDuration:I

    #@8f
    if-ge v10, v13, :cond_1ac

    #@91
    .line 395
    move-object/from16 v0, p0

    #@93
    iget v13, v0, Landroid/widget/Scroller;->mMode:I

    #@95
    packed-switch v13, :pswitch_data_1cc

    #@98
    .line 448
    :cond_98
    :goto_98
    const/4 v13, 0x1

    #@99
    goto/16 :goto_7

    #@9b
    .line 397
    :pswitch_9b
    int-to-float v13, v10

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget v14, v0, Landroid/widget/Scroller;->mDurationReciprocal:F

    #@a0
    mul-float v12, v13, v14

    #@a2
    .line 399
    .local v12, x:F
    move-object/from16 v0, p0

    #@a4
    iget-object v13, v0, Landroid/widget/Scroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@a6
    if-nez v13, :cond_d1

    #@a8
    .line 400
    invoke-static {v12}, Landroid/widget/Scroller;->viscousFluid(F)F

    #@ab
    move-result v12

    #@ac
    .line 404
    :goto_ac
    move-object/from16 v0, p0

    #@ae
    iget v13, v0, Landroid/widget/Scroller;->mStartX:I

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget v14, v0, Landroid/widget/Scroller;->mDeltaX:F

    #@b4
    mul-float/2addr v14, v12

    #@b5
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    #@b8
    move-result v14

    #@b9
    add-int/2addr v13, v14

    #@ba
    move-object/from16 v0, p0

    #@bc
    iput v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@be
    .line 405
    move-object/from16 v0, p0

    #@c0
    iget v13, v0, Landroid/widget/Scroller;->mStartY:I

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget v14, v0, Landroid/widget/Scroller;->mDeltaY:F

    #@c6
    mul-float/2addr v14, v12

    #@c7
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    #@ca
    move-result v14

    #@cb
    add-int/2addr v13, v14

    #@cc
    move-object/from16 v0, p0

    #@ce
    iput v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@d0
    goto :goto_98

    #@d1
    .line 402
    :cond_d1
    move-object/from16 v0, p0

    #@d3
    iget-object v13, v0, Landroid/widget/Scroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@d5
    invoke-interface {v13, v12}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@d8
    move-result v12

    #@d9
    goto :goto_ac

    #@da
    .line 408
    .end local v12           #x:F
    :pswitch_da
    int-to-float v13, v10

    #@db
    move-object/from16 v0, p0

    #@dd
    iget v14, v0, Landroid/widget/Scroller;->mDuration:I

    #@df
    int-to-float v14, v14

    #@e0
    div-float v7, v13, v14

    #@e2
    .line 409
    .local v7, t:F
    const/high16 v13, 0x42c8

    #@e4
    mul-float/2addr v13, v7

    #@e5
    float-to-int v6, v13

    #@e6
    .line 410
    .local v6, index:I
    const/high16 v5, 0x3f80

    #@e8
    .line 411
    .local v5, distanceCoef:F
    const/4 v11, 0x0

    #@e9
    .line 412
    .local v11, velocityCoef:F
    const/16 v13, 0x64

    #@eb
    if-ge v6, v13, :cond_10e

    #@ed
    .line 413
    int-to-float v13, v6

    #@ee
    const/high16 v14, 0x42c8

    #@f0
    div-float v8, v13, v14

    #@f2
    .line 414
    .local v8, t_inf:F
    add-int/lit8 v13, v6, 0x1

    #@f4
    int-to-float v13, v13

    #@f5
    const/high16 v14, 0x42c8

    #@f7
    div-float v9, v13, v14

    #@f9
    .line 415
    .local v9, t_sup:F
    sget-object v13, Landroid/widget/Scroller;->SPLINE_POSITION:[F

    #@fb
    aget v3, v13, v6

    #@fd
    .line 416
    .local v3, d_inf:F
    sget-object v13, Landroid/widget/Scroller;->SPLINE_POSITION:[F

    #@ff
    add-int/lit8 v14, v6, 0x1

    #@101
    aget v4, v13, v14

    #@103
    .line 417
    .local v4, d_sup:F
    sub-float v13, v4, v3

    #@105
    sub-float v14, v9, v8

    #@107
    div-float v11, v13, v14

    #@109
    .line 418
    sub-float v13, v7, v8

    #@10b
    mul-float/2addr v13, v11

    #@10c
    add-float v5, v3, v13

    #@10e
    .line 421
    .end local v3           #d_inf:F
    .end local v4           #d_sup:F
    .end local v8           #t_inf:F
    .end local v9           #t_sup:F
    :cond_10e
    move-object/from16 v0, p0

    #@110
    iget v13, v0, Landroid/widget/Scroller;->mDistance:I

    #@112
    int-to-float v13, v13

    #@113
    mul-float/2addr v13, v11

    #@114
    move-object/from16 v0, p0

    #@116
    iget v14, v0, Landroid/widget/Scroller;->mDuration:I

    #@118
    int-to-float v14, v14

    #@119
    div-float/2addr v13, v14

    #@11a
    const/high16 v14, 0x447a

    #@11c
    mul-float/2addr v13, v14

    #@11d
    move-object/from16 v0, p0

    #@11f
    iput v13, v0, Landroid/widget/Scroller;->mCurrVelocity:F

    #@121
    .line 423
    move-object/from16 v0, p0

    #@123
    iget v13, v0, Landroid/widget/Scroller;->mStartX:I

    #@125
    move-object/from16 v0, p0

    #@127
    iget v14, v0, Landroid/widget/Scroller;->mFinalX:I

    #@129
    move-object/from16 v0, p0

    #@12b
    iget v15, v0, Landroid/widget/Scroller;->mStartX:I

    #@12d
    sub-int/2addr v14, v15

    #@12e
    int-to-float v14, v14

    #@12f
    mul-float/2addr v14, v5

    #@130
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    #@133
    move-result v14

    #@134
    add-int/2addr v13, v14

    #@135
    move-object/from16 v0, p0

    #@137
    iput v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@139
    .line 425
    move-object/from16 v0, p0

    #@13b
    iget v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@13d
    move-object/from16 v0, p0

    #@13f
    iget v14, v0, Landroid/widget/Scroller;->mMaxX:I

    #@141
    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    #@144
    move-result v13

    #@145
    move-object/from16 v0, p0

    #@147
    iput v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@149
    .line 426
    move-object/from16 v0, p0

    #@14b
    iget v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@14d
    move-object/from16 v0, p0

    #@14f
    iget v14, v0, Landroid/widget/Scroller;->mMinX:I

    #@151
    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    #@154
    move-result v13

    #@155
    move-object/from16 v0, p0

    #@157
    iput v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@159
    .line 428
    move-object/from16 v0, p0

    #@15b
    iget v13, v0, Landroid/widget/Scroller;->mStartY:I

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget v14, v0, Landroid/widget/Scroller;->mFinalY:I

    #@161
    move-object/from16 v0, p0

    #@163
    iget v15, v0, Landroid/widget/Scroller;->mStartY:I

    #@165
    sub-int/2addr v14, v15

    #@166
    int-to-float v14, v14

    #@167
    mul-float/2addr v14, v5

    #@168
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    #@16b
    move-result v14

    #@16c
    add-int/2addr v13, v14

    #@16d
    move-object/from16 v0, p0

    #@16f
    iput v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@171
    .line 430
    move-object/from16 v0, p0

    #@173
    iget v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@175
    move-object/from16 v0, p0

    #@177
    iget v14, v0, Landroid/widget/Scroller;->mMaxY:I

    #@179
    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    #@17c
    move-result v13

    #@17d
    move-object/from16 v0, p0

    #@17f
    iput v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@181
    .line 431
    move-object/from16 v0, p0

    #@183
    iget v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@185
    move-object/from16 v0, p0

    #@187
    iget v14, v0, Landroid/widget/Scroller;->mMinY:I

    #@189
    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    #@18c
    move-result v13

    #@18d
    move-object/from16 v0, p0

    #@18f
    iput v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@191
    .line 433
    move-object/from16 v0, p0

    #@193
    iget v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@195
    move-object/from16 v0, p0

    #@197
    iget v14, v0, Landroid/widget/Scroller;->mFinalX:I

    #@199
    if-ne v13, v14, :cond_98

    #@19b
    move-object/from16 v0, p0

    #@19d
    iget v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@19f
    move-object/from16 v0, p0

    #@1a1
    iget v14, v0, Landroid/widget/Scroller;->mFinalY:I

    #@1a3
    if-ne v13, v14, :cond_98

    #@1a5
    .line 434
    const/4 v13, 0x1

    #@1a6
    move-object/from16 v0, p0

    #@1a8
    iput-boolean v13, v0, Landroid/widget/Scroller;->mFinished:Z

    #@1aa
    goto/16 :goto_98

    #@1ac
    .line 441
    .end local v5           #distanceCoef:F
    .end local v6           #index:I
    .end local v7           #t:F
    .end local v11           #velocityCoef:F
    :cond_1ac
    move-object/from16 v0, p0

    #@1ae
    iget v13, v0, Landroid/widget/Scroller;->mFinalX:I

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    iput v13, v0, Landroid/widget/Scroller;->mCurrX:I

    #@1b4
    .line 442
    move-object/from16 v0, p0

    #@1b6
    iget v13, v0, Landroid/widget/Scroller;->mFinalY:I

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    iput v13, v0, Landroid/widget/Scroller;->mCurrY:I

    #@1bc
    .line 443
    const/4 v13, 0x1

    #@1bd
    move-object/from16 v0, p0

    #@1bf
    iput-boolean v13, v0, Landroid/widget/Scroller;->mFinished:Z

    #@1c1
    .line 444
    sget-boolean v13, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@1c3
    if-eqz v13, :cond_98

    #@1c5
    .line 445
    const/4 v13, 0x0

    #@1c6
    move-object/from16 v0, p0

    #@1c8
    iput-boolean v13, v0, Landroid/widget/Scroller;->isNormalFlick:Z

    #@1ca
    goto/16 :goto_98

    #@1cc
    .line 395
    :pswitch_data_1cc
    .packed-switch 0x0
        :pswitch_9b
        :pswitch_da
    .end packed-switch
.end method

.method public extendDuration(I)V
    .registers 5
    .parameter "extend"

    #@0
    .prologue
    .line 661
    invoke-virtual {p0}, Landroid/widget/Scroller;->timePassed()I

    #@3
    move-result v0

    #@4
    .line 662
    .local v0, passed:I
    add-int v1, v0, p1

    #@6
    iput v1, p0, Landroid/widget/Scroller;->mDuration:I

    #@8
    .line 663
    const/high16 v1, 0x3f80

    #@a
    iget v2, p0, Landroid/widget/Scroller;->mDuration:I

    #@c
    int-to-float v2, v2

    #@d
    div-float/2addr v1, v2

    #@e
    iput v1, p0, Landroid/widget/Scroller;->mDurationReciprocal:F

    #@10
    .line 664
    const/4 v1, 0x0

    #@11
    iput-boolean v1, p0, Landroid/widget/Scroller;->mFinished:Z

    #@13
    .line 665
    return-void
.end method

.method public fling(IIIIIIII)V
    .registers 26
    .parameter "startX"
    .parameter "startY"
    .parameter "velocityX"
    .parameter "velocityY"
    .parameter "minX"
    .parameter "maxX"
    .parameter "minY"
    .parameter "maxY"

    #@0
    .prologue
    .line 557
    move-object/from16 v0, p0

    #@2
    iget-boolean v15, v0, Landroid/widget/Scroller;->mFlywheel:Z

    #@4
    if-eqz v15, :cond_68

    #@6
    move-object/from16 v0, p0

    #@8
    iget-boolean v15, v0, Landroid/widget/Scroller;->mFinished:Z

    #@a
    if-nez v15, :cond_68

    #@c
    .line 558
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Scroller;->getCurrVelocity()F

    #@f
    move-result v9

    #@10
    .line 560
    .local v9, oldVel:F
    move-object/from16 v0, p0

    #@12
    iget v15, v0, Landroid/widget/Scroller;->mFinalX:I

    #@14
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Landroid/widget/Scroller;->mStartX:I

    #@18
    move/from16 v16, v0

    #@1a
    sub-int v15, v15, v16

    #@1c
    int-to-float v4, v15

    #@1d
    .line 561
    .local v4, dx:F
    move-object/from16 v0, p0

    #@1f
    iget v15, v0, Landroid/widget/Scroller;->mFinalY:I

    #@21
    move-object/from16 v0, p0

    #@23
    iget v0, v0, Landroid/widget/Scroller;->mStartY:I

    #@25
    move/from16 v16, v0

    #@27
    sub-int v15, v15, v16

    #@29
    int-to-float v5, v15

    #@2a
    .line 562
    .local v5, dy:F
    mul-float v15, v4, v4

    #@2c
    mul-float v16, v5, v5

    #@2e
    add-float v15, v15, v16

    #@30
    invoke-static {v15}, Landroid/util/FloatMath;->sqrt(F)F

    #@33
    move-result v6

    #@34
    .line 564
    .local v6, hyp:F
    div-float v7, v4, v6

    #@36
    .line 565
    .local v7, ndx:F
    div-float v8, v5, v6

    #@38
    .line 567
    .local v8, ndy:F
    mul-float v10, v7, v9

    #@3a
    .line 568
    .local v10, oldVelocityX:F
    mul-float v11, v8, v9

    #@3c
    .line 569
    .local v11, oldVelocityY:F
    move/from16 v0, p3

    #@3e
    int-to-float v15, v0

    #@3f
    invoke-static {v15}, Ljava/lang/Math;->signum(F)F

    #@42
    move-result v15

    #@43
    invoke-static {v10}, Ljava/lang/Math;->signum(F)F

    #@46
    move-result v16

    #@47
    cmpl-float v15, v15, v16

    #@49
    if-nez v15, :cond_68

    #@4b
    move/from16 v0, p4

    #@4d
    int-to-float v15, v0

    #@4e
    invoke-static {v15}, Ljava/lang/Math;->signum(F)F

    #@51
    move-result v15

    #@52
    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    #@55
    move-result v16

    #@56
    cmpl-float v15, v15, v16

    #@58
    if-nez v15, :cond_68

    #@5a
    .line 571
    move/from16 v0, p3

    #@5c
    int-to-float v15, v0

    #@5d
    add-float/2addr v15, v10

    #@5e
    float-to-int v0, v15

    #@5f
    move/from16 p3, v0

    #@61
    .line 572
    move/from16 v0, p4

    #@63
    int-to-float v15, v0

    #@64
    add-float/2addr v15, v11

    #@65
    float-to-int v0, v15

    #@66
    move/from16 p4, v0

    #@68
    .line 576
    .end local v4           #dx:F
    .end local v5           #dy:F
    .end local v6           #hyp:F
    .end local v7           #ndx:F
    .end local v8           #ndy:F
    .end local v9           #oldVel:F
    .end local v10           #oldVelocityX:F
    .end local v11           #oldVelocityY:F
    :cond_68
    const/4 v15, 0x1

    #@69
    move-object/from16 v0, p0

    #@6b
    iput v15, v0, Landroid/widget/Scroller;->mMode:I

    #@6d
    .line 577
    const/4 v15, 0x0

    #@6e
    move-object/from16 v0, p0

    #@70
    iput-boolean v15, v0, Landroid/widget/Scroller;->mFinished:Z

    #@72
    .line 579
    mul-int v15, p3, p3

    #@74
    mul-int v16, p4, p4

    #@76
    add-int v15, v15, v16

    #@78
    int-to-float v15, v15

    #@79
    invoke-static {v15}, Landroid/util/FloatMath;->sqrt(F)F

    #@7c
    move-result v14

    #@7d
    .line 581
    .local v14, velocity:F
    move-object/from16 v0, p0

    #@7f
    iput v14, v0, Landroid/widget/Scroller;->mVelocity:F

    #@81
    .line 582
    move-object/from16 v0, p0

    #@83
    invoke-direct {v0, v14}, Landroid/widget/Scroller;->getSplineFlingDuration(F)I

    #@86
    move-result v15

    #@87
    move-object/from16 v0, p0

    #@89
    iput v15, v0, Landroid/widget/Scroller;->mDuration:I

    #@8b
    .line 583
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@8e
    move-result-wide v15

    #@8f
    move-object/from16 v0, p0

    #@91
    iput-wide v15, v0, Landroid/widget/Scroller;->mStartTime:J

    #@93
    .line 584
    move/from16 v0, p1

    #@95
    move-object/from16 v1, p0

    #@97
    iput v0, v1, Landroid/widget/Scroller;->mStartX:I

    #@99
    .line 585
    move/from16 v0, p2

    #@9b
    move-object/from16 v1, p0

    #@9d
    iput v0, v1, Landroid/widget/Scroller;->mStartY:I

    #@9f
    .line 587
    const/4 v15, 0x0

    #@a0
    cmpl-float v15, v14, v15

    #@a2
    if-nez v15, :cond_139

    #@a4
    const/high16 v2, 0x3f80

    #@a6
    .line 588
    .local v2, coeffX:F
    :goto_a6
    const/4 v15, 0x0

    #@a7
    cmpl-float v15, v14, v15

    #@a9
    if-nez v15, :cond_140

    #@ab
    const/high16 v3, 0x3f80

    #@ad
    .line 590
    .local v3, coeffY:F
    :goto_ad
    move-object/from16 v0, p0

    #@af
    invoke-direct {v0, v14}, Landroid/widget/Scroller;->getSplineFlingDistance(F)D

    #@b2
    move-result-wide v12

    #@b3
    .line 591
    .local v12, totalDistance:D
    invoke-static {v14}, Ljava/lang/Math;->signum(F)F

    #@b6
    move-result v15

    #@b7
    float-to-double v15, v15

    #@b8
    mul-double/2addr v15, v12

    #@b9
    double-to-int v15, v15

    #@ba
    move-object/from16 v0, p0

    #@bc
    iput v15, v0, Landroid/widget/Scroller;->mDistance:I

    #@be
    .line 593
    move/from16 v0, p5

    #@c0
    move-object/from16 v1, p0

    #@c2
    iput v0, v1, Landroid/widget/Scroller;->mMinX:I

    #@c4
    .line 594
    move/from16 v0, p6

    #@c6
    move-object/from16 v1, p0

    #@c8
    iput v0, v1, Landroid/widget/Scroller;->mMaxX:I

    #@ca
    .line 595
    move/from16 v0, p7

    #@cc
    move-object/from16 v1, p0

    #@ce
    iput v0, v1, Landroid/widget/Scroller;->mMinY:I

    #@d0
    .line 596
    move/from16 v0, p8

    #@d2
    move-object/from16 v1, p0

    #@d4
    iput v0, v1, Landroid/widget/Scroller;->mMaxY:I

    #@d6
    .line 598
    float-to-double v15, v2

    #@d7
    mul-double/2addr v15, v12

    #@d8
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->round(D)J

    #@db
    move-result-wide v15

    #@dc
    long-to-int v15, v15

    #@dd
    add-int v15, v15, p1

    #@df
    move-object/from16 v0, p0

    #@e1
    iput v15, v0, Landroid/widget/Scroller;->mFinalX:I

    #@e3
    .line 600
    move-object/from16 v0, p0

    #@e5
    iget v15, v0, Landroid/widget/Scroller;->mFinalX:I

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget v0, v0, Landroid/widget/Scroller;->mMaxX:I

    #@eb
    move/from16 v16, v0

    #@ed
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    #@f0
    move-result v15

    #@f1
    move-object/from16 v0, p0

    #@f3
    iput v15, v0, Landroid/widget/Scroller;->mFinalX:I

    #@f5
    .line 601
    move-object/from16 v0, p0

    #@f7
    iget v15, v0, Landroid/widget/Scroller;->mFinalX:I

    #@f9
    move-object/from16 v0, p0

    #@fb
    iget v0, v0, Landroid/widget/Scroller;->mMinX:I

    #@fd
    move/from16 v16, v0

    #@ff
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    #@102
    move-result v15

    #@103
    move-object/from16 v0, p0

    #@105
    iput v15, v0, Landroid/widget/Scroller;->mFinalX:I

    #@107
    .line 603
    float-to-double v15, v3

    #@108
    mul-double/2addr v15, v12

    #@109
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->round(D)J

    #@10c
    move-result-wide v15

    #@10d
    long-to-int v15, v15

    #@10e
    add-int v15, v15, p2

    #@110
    move-object/from16 v0, p0

    #@112
    iput v15, v0, Landroid/widget/Scroller;->mFinalY:I

    #@114
    .line 605
    move-object/from16 v0, p0

    #@116
    iget v15, v0, Landroid/widget/Scroller;->mFinalY:I

    #@118
    move-object/from16 v0, p0

    #@11a
    iget v0, v0, Landroid/widget/Scroller;->mMaxY:I

    #@11c
    move/from16 v16, v0

    #@11e
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    #@121
    move-result v15

    #@122
    move-object/from16 v0, p0

    #@124
    iput v15, v0, Landroid/widget/Scroller;->mFinalY:I

    #@126
    .line 606
    move-object/from16 v0, p0

    #@128
    iget v15, v0, Landroid/widget/Scroller;->mFinalY:I

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget v0, v0, Landroid/widget/Scroller;->mMinY:I

    #@12e
    move/from16 v16, v0

    #@130
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    #@133
    move-result v15

    #@134
    move-object/from16 v0, p0

    #@136
    iput v15, v0, Landroid/widget/Scroller;->mFinalY:I

    #@138
    .line 607
    return-void

    #@139
    .line 587
    .end local v2           #coeffX:F
    .end local v3           #coeffY:F
    .end local v12           #totalDistance:D
    :cond_139
    move/from16 v0, p3

    #@13b
    int-to-float v15, v0

    #@13c
    div-float v2, v15, v14

    #@13e
    goto/16 :goto_a6

    #@140
    .line 588
    .restart local v2       #coeffX:F
    :cond_140
    move/from16 v0, p4

    #@142
    int-to-float v15, v0

    #@143
    div-float v3, v15, v14

    #@145
    goto/16 :goto_ad
.end method

.method public final forceFinished(Z)V
    .registers 2
    .parameter "finished"

    #@0
    .prologue
    .line 230
    iput-boolean p1, p0, Landroid/widget/Scroller;->mFinished:Z

    #@2
    .line 231
    return-void
.end method

.method public getCurrVelocity()F
    .registers 4

    #@0
    .prologue
    .line 267
    iget v0, p0, Landroid/widget/Scroller;->mMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    iget v0, p0, Landroid/widget/Scroller;->mCurrVelocity:F

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    iget v0, p0, Landroid/widget/Scroller;->mVelocity:F

    #@a
    iget v1, p0, Landroid/widget/Scroller;->mDeceleration:F

    #@c
    invoke-virtual {p0}, Landroid/widget/Scroller;->timePassed()I

    #@f
    move-result v2

    #@10
    int-to-float v2, v2

    #@11
    mul-float/2addr v1, v2

    #@12
    const/high16 v2, 0x44fa

    #@14
    div-float/2addr v1, v2

    #@15
    sub-float/2addr v0, v1

    #@16
    goto :goto_7
.end method

.method public final getCurrX()I
    .registers 2

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/widget/Scroller;->mCurrX:I

    #@2
    return v0
.end method

.method public final getCurrY()I
    .registers 2

    #@0
    .prologue
    .line 257
    iget v0, p0, Landroid/widget/Scroller;->mCurrY:I

    #@2
    return v0
.end method

.method public final getDuration()I
    .registers 2

    #@0
    .prologue
    .line 239
    iget v0, p0, Landroid/widget/Scroller;->mDuration:I

    #@2
    return v0
.end method

.method public final getFinalX()I
    .registers 2

    #@0
    .prologue
    .line 295
    iget v0, p0, Landroid/widget/Scroller;->mFinalX:I

    #@2
    return v0
.end method

.method public final getFinalY()I
    .registers 2

    #@0
    .prologue
    .line 304
    iget v0, p0, Landroid/widget/Scroller;->mFinalY:I

    #@2
    return v0
.end method

.method public final getStartX()I
    .registers 2

    #@0
    .prologue
    .line 277
    iget v0, p0, Landroid/widget/Scroller;->mStartX:I

    #@2
    return v0
.end method

.method public final getStartY()I
    .registers 2

    #@0
    .prologue
    .line 286
    iget v0, p0, Landroid/widget/Scroller;->mStartY:I

    #@2
    return v0
.end method

.method public final isFinished()Z
    .registers 2

    #@0
    .prologue
    .line 221
    iget-boolean v0, p0, Landroid/widget/Scroller;->mFinished:Z

    #@2
    return v0
.end method

.method public isScrollingInDirection(FF)Z
    .registers 6
    .parameter "xvel"
    .parameter "yvel"

    #@0
    .prologue
    .line 706
    iget-boolean v0, p0, Landroid/widget/Scroller;->mFinished:Z

    #@2
    if-nez v0, :cond_2a

    #@4
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    #@7
    move-result v0

    #@8
    iget v1, p0, Landroid/widget/Scroller;->mFinalX:I

    #@a
    iget v2, p0, Landroid/widget/Scroller;->mStartX:I

    #@c
    sub-int/2addr v1, v2

    #@d
    int-to-float v1, v1

    #@e
    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    #@11
    move-result v1

    #@12
    cmpl-float v0, v0, v1

    #@14
    if-nez v0, :cond_2a

    #@16
    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    #@19
    move-result v0

    #@1a
    iget v1, p0, Landroid/widget/Scroller;->mFinalY:I

    #@1c
    iget v2, p0, Landroid/widget/Scroller;->mStartY:I

    #@1e
    sub-int/2addr v1, v2

    #@1f
    int-to-float v1, v1

    #@20
    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    #@23
    move-result v1

    #@24
    cmpl-float v0, v0, v1

    #@26
    if-nez v0, :cond_2a

    #@28
    const/4 v0, 0x1

    #@29
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method public setFinalX(I)V
    .registers 4
    .parameter "newX"

    #@0
    .prologue
    .line 684
    iput p1, p0, Landroid/widget/Scroller;->mFinalX:I

    #@2
    .line 685
    iget v0, p0, Landroid/widget/Scroller;->mFinalX:I

    #@4
    iget v1, p0, Landroid/widget/Scroller;->mStartX:I

    #@6
    sub-int/2addr v0, v1

    #@7
    int-to-float v0, v0

    #@8
    iput v0, p0, Landroid/widget/Scroller;->mDeltaX:F

    #@a
    .line 686
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/widget/Scroller;->mFinished:Z

    #@d
    .line 687
    return-void
.end method

.method public setFinalY(I)V
    .registers 4
    .parameter "newY"

    #@0
    .prologue
    .line 697
    iput p1, p0, Landroid/widget/Scroller;->mFinalY:I

    #@2
    .line 698
    iget v0, p0, Landroid/widget/Scroller;->mFinalY:I

    #@4
    iget v1, p0, Landroid/widget/Scroller;->mStartY:I

    #@6
    sub-int/2addr v0, v1

    #@7
    int-to-float v0, v0

    #@8
    iput v0, p0, Landroid/widget/Scroller;->mDeltaY:F

    #@a
    .line 699
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/widget/Scroller;->mFinished:Z

    #@d
    .line 700
    return-void
.end method

.method public final setFriction(F)V
    .registers 3
    .parameter "friction"

    #@0
    .prologue
    .line 203
    invoke-direct {p0, p1}, Landroid/widget/Scroller;->computeDeceleration(F)F

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/widget/Scroller;->mDeceleration:F

    #@6
    .line 204
    iput p1, p0, Landroid/widget/Scroller;->mFlingFriction:F

    #@8
    .line 205
    return-void
.end method

.method public startScroll(IIII)V
    .registers 11
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 466
    const/16 v5, 0xfa

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    #@a
    .line 467
    return-void
.end method

.method public startScroll(IIIII)V
    .registers 15
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"
    .parameter "duration"

    #@0
    .prologue
    const/high16 v8, 0x41b0

    #@2
    const/high16 v7, 0x4150

    #@4
    const v6, 0x3fa66666

    #@7
    const/4 v5, 0x1

    #@8
    const/4 v4, 0x0

    #@9
    .line 484
    iput v4, p0, Landroid/widget/Scroller;->mMode:I

    #@b
    .line 485
    iput-boolean v4, p0, Landroid/widget/Scroller;->mFinished:Z

    #@d
    .line 486
    iput p5, p0, Landroid/widget/Scroller;->mDuration:I

    #@f
    .line 487
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@12
    move-result-wide v0

    #@13
    iput-wide v0, p0, Landroid/widget/Scroller;->mStartTime:J

    #@15
    .line 488
    iput p1, p0, Landroid/widget/Scroller;->mStartX:I

    #@17
    .line 489
    iput p2, p0, Landroid/widget/Scroller;->mStartY:I

    #@19
    .line 490
    add-int v0, p1, p3

    #@1b
    iput v0, p0, Landroid/widget/Scroller;->mFinalX:I

    #@1d
    .line 491
    add-int v0, p2, p4

    #@1f
    iput v0, p0, Landroid/widget/Scroller;->mFinalY:I

    #@21
    .line 492
    int-to-float v0, p3

    #@22
    iput v0, p0, Landroid/widget/Scroller;->mDeltaX:F

    #@24
    .line 493
    int-to-float v0, p4

    #@25
    iput v0, p0, Landroid/widget/Scroller;->mDeltaY:F

    #@27
    .line 495
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@29
    if-eqz v0, :cond_75

    #@2b
    .line 496
    iput-boolean v5, p0, Landroid/widget/Scroller;->isNormalFlick:Z

    #@2d
    .line 498
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@2f
    iget v0, v0, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@31
    const/16 v1, 0xa

    #@33
    if-lt v0, v1, :cond_3f

    #@35
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@37
    iget-wide v0, v0, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@39
    const-wide/16 v2, 0x0

    #@3b
    cmp-long v0, v0, v2

    #@3d
    if-nez v0, :cond_89

    #@3f
    .line 499
    :cond_3f
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@41
    iput v4, v0, Landroid/widget/Scroller$FrameRate;->frameCount:I

    #@43
    .line 500
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@45
    const-wide/16 v1, 0x0

    #@47
    iput-wide v1, v0, Landroid/widget/Scroller$FrameRate;->timePassed:J

    #@49
    .line 501
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@4b
    iget v0, v0, Landroid/widget/Scroller$FrameRate;->frameTotalCount:I

    #@4d
    const/16 v1, 0x32

    #@4f
    if-le v0, v1, :cond_86

    #@51
    .line 502
    iput v5, p0, Landroid/widget/Scroller;->flingComputeMode:I

    #@53
    .line 503
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@55
    invoke-virtual {v0}, Landroid/widget/Scroller$FrameRate;->getTimeDiff()F

    #@58
    move-result v0

    #@59
    iput v0, p0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@5b
    .line 504
    iget v0, p0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@5d
    cmpg-float v0, v0, v8

    #@5f
    if-gez v0, :cond_7e

    #@61
    .line 505
    iput v5, p0, Landroid/widget/Scroller;->flingComputeMode:I

    #@63
    .line 506
    iget v0, p0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@65
    cmpg-float v0, v0, v7

    #@67
    if-gez v0, :cond_70

    #@69
    .line 507
    iget v0, p0, Landroid/widget/Scroller;->mDuration:I

    #@6b
    int-to-float v0, v0

    #@6c
    mul-float/2addr v0, v6

    #@6d
    float-to-int v0, v0

    #@6e
    iput v0, p0, Landroid/widget/Scroller;->mDuration:I

    #@70
    .line 529
    :cond_70
    :goto_70
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@72
    invoke-virtual {v0}, Landroid/widget/Scroller$FrameRate;->reset()V

    #@75
    .line 532
    :cond_75
    const/high16 v0, 0x3f80

    #@77
    iget v1, p0, Landroid/widget/Scroller;->mDuration:I

    #@79
    int-to-float v1, v1

    #@7a
    div-float/2addr v0, v1

    #@7b
    iput v0, p0, Landroid/widget/Scroller;->mDurationReciprocal:F

    #@7d
    .line 533
    return-void

    #@7e
    .line 510
    :cond_7e
    iput v4, p0, Landroid/widget/Scroller;->flingComputeMode:I

    #@80
    .line 511
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@82
    invoke-virtual {v0}, Landroid/widget/Scroller$FrameRate;->allReset()V

    #@85
    goto :goto_70

    #@86
    .line 514
    :cond_86
    iput v4, p0, Landroid/widget/Scroller;->flingComputeMode:I

    #@88
    goto :goto_70

    #@89
    .line 517
    :cond_89
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@8b
    invoke-virtual {v0}, Landroid/widget/Scroller$FrameRate;->getTimeDiff()F

    #@8e
    move-result v0

    #@8f
    iput v0, p0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@91
    .line 518
    iget v0, p0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@93
    cmpg-float v0, v0, v8

    #@95
    if-gez v0, :cond_a7

    #@97
    .line 519
    iput v5, p0, Landroid/widget/Scroller;->flingComputeMode:I

    #@99
    .line 520
    iget v0, p0, Landroid/widget/Scroller;->timePassedDiffConstant:F

    #@9b
    cmpg-float v0, v0, v7

    #@9d
    if-gez v0, :cond_70

    #@9f
    .line 521
    iget v0, p0, Landroid/widget/Scroller;->mDuration:I

    #@a1
    int-to-float v0, v0

    #@a2
    mul-float/2addr v0, v6

    #@a3
    float-to-int v0, v0

    #@a4
    iput v0, p0, Landroid/widget/Scroller;->mDuration:I

    #@a6
    goto :goto_70

    #@a7
    .line 524
    :cond_a7
    iput v4, p0, Landroid/widget/Scroller;->flingComputeMode:I

    #@a9
    .line 525
    iget-object v0, p0, Landroid/widget/Scroller;->frameRate:Landroid/widget/Scroller$FrameRate;

    #@ab
    invoke-virtual {v0}, Landroid/widget/Scroller$FrameRate;->allReset()V

    #@ae
    goto :goto_70
.end method

.method public timePassed()I
    .registers 5

    #@0
    .prologue
    .line 673
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    iget-wide v2, p0, Landroid/widget/Scroller;->mStartTime:J

    #@6
    sub-long/2addr v0, v2

    #@7
    long-to-int v0, v0

    #@8
    return v0
.end method
