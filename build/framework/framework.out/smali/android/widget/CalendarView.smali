.class public Landroid/widget/CalendarView;
.super Landroid/widget/FrameLayout;
.source "CalendarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/CalendarView$WeekView;,
        Landroid/widget/CalendarView$WeeksAdapter;,
        Landroid/widget/CalendarView$ScrollStateRunnable;,
        Landroid/widget/CalendarView$OnDateChangeListener;
    }
.end annotation


# static fields
.field private static final ADJUSTMENT_SCROLL_DURATION:I = 0x1f4

.field private static final DATE_FORMAT:Ljava/lang/String; = "MM/dd/yyyy"

.field private static final DAYS_PER_WEEK:I = 0x7

.field private static final DEFAULT_DATE_TEXT_SIZE:I = 0xe

.field private static final DEFAULT_MAX_DATE:Ljava/lang/String; = "01/01/2036"

.field private static final DEFAULT_MIN_DATE:Ljava/lang/String; = "01/01/1902"

.field private static final DEFAULT_SHOWN_WEEK_COUNT:I = 0x6

.field private static final DEFAULT_SHOW_WEEK_NUMBER:Z = true

.field private static final DEFAULT_WEEK_DAY_TEXT_APPEARANCE_RES_ID:I = -0x1

.field private static final GOTO_SCROLL_DURATION:I = 0x3e8

.field private static final LOG_TAG:Ljava/lang/String; = null

.field private static final MILLIS_IN_DAY:J = 0x5265c00L

.field private static final MILLIS_IN_WEEK:J = 0x240c8400L

.field private static final SCROLL_CHANGE_DELAY:I = 0x28

.field private static final SCROLL_HYST_WEEKS:I = 0x2

.field private static final UNSCALED_BOTTOM_BUFFER:I = 0x14

.field private static final UNSCALED_LIST_SCROLL_TOP_OFFSET:I = 0x2

.field private static final UNSCALED_SELECTED_DATE_VERTICAL_BAR_WIDTH:I = 0x6

.field private static final UNSCALED_WEEK_MIN_VISIBLE_HEIGHT:I = 0xc

.field private static final UNSCALED_WEEK_SEPARATOR_LINE_WIDTH:I = 0x1


# instance fields
.field private mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

.field private mBottomBuffer:I

.field private mCurrentLocale:Ljava/util/Locale;

.field private mCurrentMonthDisplayed:I

.field private mCurrentScrollState:I

.field private final mDateFormat:Ljava/text/DateFormat;

.field private mDateTextAppearanceResId:I

.field private mDateTextSize:I

.field private mDayLabels:[Ljava/lang/String;

.field private mDayNamesHeader:Landroid/view/ViewGroup;

.field private mDaysPerWeek:I

.field private mFirstDayOfMonth:Ljava/util/Calendar;

.field private mFirstDayOfWeek:I

.field private mFocusedMonthDateColor:I

.field private mFriction:F

.field private mIsScrollingUp:Z

.field private mListScrollTopOffset:I

.field private mListView:Landroid/widget/ListView;

.field private mMaxDate:Ljava/util/Calendar;

.field private mMinDate:Ljava/util/Calendar;

.field private mMonthName:Landroid/widget/TextView;

.field private mOnDateChangeListener:Landroid/widget/CalendarView$OnDateChangeListener;

.field private mPreviousScrollPosition:J

.field private mPreviousScrollState:I

.field private mScrollStateChangedRunnable:Landroid/widget/CalendarView$ScrollStateRunnable;

.field private mSelectedDateVerticalBar:Landroid/graphics/drawable/Drawable;

.field private final mSelectedDateVerticalBarWidth:I

.field private mSelectedWeekBackgroundColor:I

.field private mShowWeekNumber:Z

.field private mShownWeekCount:I

.field private mTempDate:Ljava/util/Calendar;

.field private mUnfocusedMonthDateColor:I

.field private mVelocityScale:F

.field private mWeekDayTextAppearanceResId:I

.field private mWeekMinVisibleHeight:I

.field private mWeekNumberColor:I

.field private mWeekSeparatorLineColor:I

.field private final mWeekSeperatorLineWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 81
    const-class v0, Landroid/widget/CalendarView;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/widget/CalendarView;->LOG_TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 329
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/CalendarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 330
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 333
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CalendarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 334
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 337
    const/4 v6, 0x0

    #@1
    invoke-direct {p0, p1, p2, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 180
    const/4 v6, 0x2

    #@5
    iput v6, p0, Landroid/widget/CalendarView;->mListScrollTopOffset:I

    #@7
    .line 185
    const/16 v6, 0xc

    #@9
    iput v6, p0, Landroid/widget/CalendarView;->mWeekMinVisibleHeight:I

    #@b
    .line 190
    const/16 v6, 0x14

    #@d
    iput v6, p0, Landroid/widget/CalendarView;->mBottomBuffer:I

    #@f
    .line 205
    const/4 v6, 0x7

    #@10
    iput v6, p0, Landroid/widget/CalendarView;->mDaysPerWeek:I

    #@12
    .line 210
    const v6, 0x3d4ccccd

    #@15
    iput v6, p0, Landroid/widget/CalendarView;->mFriction:F

    #@17
    .line 215
    const v6, 0x3eaa7efa

    #@1a
    iput v6, p0, Landroid/widget/CalendarView;->mVelocityScale:F

    #@1c
    .line 260
    const/4 v6, 0x0

    #@1d
    iput-boolean v6, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@1f
    .line 265
    const/4 v6, 0x0

    #@20
    iput v6, p0, Landroid/widget/CalendarView;->mPreviousScrollState:I

    #@22
    .line 270
    const/4 v6, 0x0

    #@23
    iput v6, p0, Landroid/widget/CalendarView;->mCurrentScrollState:I

    #@25
    .line 280
    new-instance v6, Landroid/widget/CalendarView$ScrollStateRunnable;

    #@27
    const/4 v7, 0x0

    #@28
    invoke-direct {v6, p0, v7}, Landroid/widget/CalendarView$ScrollStateRunnable;-><init>(Landroid/widget/CalendarView;Landroid/widget/CalendarView$1;)V

    #@2b
    iput-object v6, p0, Landroid/widget/CalendarView;->mScrollStateChangedRunnable:Landroid/widget/CalendarView$ScrollStateRunnable;

    #@2d
    .line 305
    new-instance v6, Ljava/text/SimpleDateFormat;

    #@2f
    const-string v7, "MM/dd/yyyy"

    #@31
    invoke-direct {v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@34
    iput-object v6, p0, Landroid/widget/CalendarView;->mDateFormat:Ljava/text/DateFormat;

    #@36
    .line 340
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@39
    move-result-object v6

    #@3a
    invoke-direct {p0, v6}, Landroid/widget/CalendarView;->setCurrentLocale(Ljava/util/Locale;)V

    #@3d
    .line 342
    sget-object v6, Lcom/android/internal/R$styleable;->CalendarView:[I

    #@3f
    const v7, 0x101035d

    #@42
    const/4 v8, 0x0

    #@43
    invoke-virtual {p1, p2, v6, v7, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@46
    move-result-object v0

    #@47
    .line 344
    .local v0, attributesArray:Landroid/content/res/TypedArray;
    const/4 v6, 0x1

    #@48
    const/4 v7, 0x1

    #@49
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4c
    move-result v6

    #@4d
    iput-boolean v6, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@4f
    .line 346
    const/4 v6, 0x0

    #@50
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@53
    move-result-object v7

    #@54
    invoke-static {v7}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@57
    move-result-object v7

    #@58
    iget-object v7, v7, Llibcore/icu/LocaleData;->firstDayOfWeek:Ljava/lang/Integer;

    #@5a
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@5d
    move-result v7

    #@5e
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@61
    move-result v6

    #@62
    iput v6, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@64
    .line 348
    const/4 v6, 0x2

    #@65
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    .line 349
    .local v5, minDate:Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6c
    move-result v6

    #@6d
    if-nez v6, :cond_77

    #@6f
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@71
    invoke-direct {p0, v5, v6}, Landroid/widget/CalendarView;->parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z

    #@74
    move-result v6

    #@75
    if-nez v6, :cond_7e

    #@77
    .line 350
    :cond_77
    const-string v6, "01/01/1902"

    #@79
    iget-object v7, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@7b
    invoke-direct {p0, v6, v7}, Landroid/widget/CalendarView;->parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z

    #@7e
    .line 352
    :cond_7e
    const/4 v6, 0x3

    #@7f
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@82
    move-result-object v4

    #@83
    .line 353
    .local v4, maxDate:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@86
    move-result v6

    #@87
    if-nez v6, :cond_91

    #@89
    iget-object v6, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@8b
    invoke-direct {p0, v4, v6}, Landroid/widget/CalendarView;->parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z

    #@8e
    move-result v6

    #@8f
    if-nez v6, :cond_98

    #@91
    .line 354
    :cond_91
    const-string v6, "01/01/2036"

    #@93
    iget-object v7, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@95
    invoke-direct {p0, v6, v7}, Landroid/widget/CalendarView;->parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z

    #@98
    .line 356
    :cond_98
    iget-object v6, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@9a
    iget-object v7, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@9c
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@9f
    move-result v6

    #@a0
    if-eqz v6, :cond_aa

    #@a2
    .line 357
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@a4
    const-string v7, "Max date cannot be before min date."

    #@a6
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a9
    throw v6

    #@aa
    .line 359
    :cond_aa
    const/4 v6, 0x4

    #@ab
    const/4 v7, 0x6

    #@ac
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@af
    move-result v6

    #@b0
    iput v6, p0, Landroid/widget/CalendarView;->mShownWeekCount:I

    #@b2
    .line 361
    const/4 v6, 0x5

    #@b3
    const/4 v7, 0x0

    #@b4
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@b7
    move-result v6

    #@b8
    iput v6, p0, Landroid/widget/CalendarView;->mSelectedWeekBackgroundColor:I

    #@ba
    .line 363
    const/4 v6, 0x6

    #@bb
    const/4 v7, 0x0

    #@bc
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@bf
    move-result v6

    #@c0
    iput v6, p0, Landroid/widget/CalendarView;->mFocusedMonthDateColor:I

    #@c2
    .line 365
    const/4 v6, 0x7

    #@c3
    const/4 v7, 0x0

    #@c4
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@c7
    move-result v6

    #@c8
    iput v6, p0, Landroid/widget/CalendarView;->mUnfocusedMonthDateColor:I

    #@ca
    .line 367
    const/16 v6, 0x9

    #@cc
    const/4 v7, 0x0

    #@cd
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@d0
    move-result v6

    #@d1
    iput v6, p0, Landroid/widget/CalendarView;->mWeekSeparatorLineColor:I

    #@d3
    .line 369
    const/16 v6, 0x8

    #@d5
    const/4 v7, 0x0

    #@d6
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@d9
    move-result v6

    #@da
    iput v6, p0, Landroid/widget/CalendarView;->mWeekNumberColor:I

    #@dc
    .line 370
    const/16 v6, 0xa

    #@de
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@e1
    move-result-object v6

    #@e2
    iput-object v6, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBar:Landroid/graphics/drawable/Drawable;

    #@e4
    .line 373
    const/16 v6, 0xc

    #@e6
    const v7, 0x1030046

    #@e9
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@ec
    move-result v6

    #@ed
    iput v6, p0, Landroid/widget/CalendarView;->mDateTextAppearanceResId:I

    #@ef
    .line 375
    invoke-direct {p0}, Landroid/widget/CalendarView;->updateDateTextSize()V

    #@f2
    .line 377
    const/16 v6, 0xb

    #@f4
    const/4 v7, -0x1

    #@f5
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@f8
    move-result v6

    #@f9
    iput v6, p0, Landroid/widget/CalendarView;->mWeekDayTextAppearanceResId:I

    #@fb
    .line 380
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@fe
    .line 382
    invoke-virtual {p0}, Landroid/widget/CalendarView;->getResources()Landroid/content/res/Resources;

    #@101
    move-result-object v6

    #@102
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@105
    move-result-object v2

    #@106
    .line 383
    .local v2, displayMetrics:Landroid/util/DisplayMetrics;
    const/4 v6, 0x1

    #@107
    const/high16 v7, 0x4140

    #@109
    invoke-static {v6, v7, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@10c
    move-result v6

    #@10d
    float-to-int v6, v6

    #@10e
    iput v6, p0, Landroid/widget/CalendarView;->mWeekMinVisibleHeight:I

    #@110
    .line 385
    const/4 v6, 0x1

    #@111
    const/high16 v7, 0x4000

    #@113
    invoke-static {v6, v7, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@116
    move-result v6

    #@117
    float-to-int v6, v6

    #@118
    iput v6, p0, Landroid/widget/CalendarView;->mListScrollTopOffset:I

    #@11a
    .line 387
    const/4 v6, 0x1

    #@11b
    const/high16 v7, 0x41a0

    #@11d
    invoke-static {v6, v7, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@120
    move-result v6

    #@121
    float-to-int v6, v6

    #@122
    iput v6, p0, Landroid/widget/CalendarView;->mBottomBuffer:I

    #@124
    .line 389
    const/4 v6, 0x1

    #@125
    const/high16 v7, 0x40c0

    #@127
    invoke-static {v6, v7, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@12a
    move-result v6

    #@12b
    float-to-int v6, v6

    #@12c
    iput v6, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBarWidth:I

    #@12e
    .line 391
    const/4 v6, 0x1

    #@12f
    const/high16 v7, 0x3f80

    #@131
    invoke-static {v6, v7, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@134
    move-result v6

    #@135
    float-to-int v6, v6

    #@136
    iput v6, p0, Landroid/widget/CalendarView;->mWeekSeperatorLineWidth:I

    #@138
    .line 394
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@13a
    const-string/jumbo v7, "layout_inflater"

    #@13d
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@140
    move-result-object v3

    #@141
    check-cast v3, Landroid/view/LayoutInflater;

    #@143
    .line 396
    .local v3, layoutInflater:Landroid/view/LayoutInflater;
    const v6, 0x1090030

    #@146
    const/4 v7, 0x0

    #@147
    const/4 v8, 0x0

    #@148
    invoke-virtual {v3, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@14b
    move-result-object v1

    #@14c
    .line 397
    .local v1, content:Landroid/view/View;
    invoke-virtual {p0, v1}, Landroid/widget/CalendarView;->addView(Landroid/view/View;)V

    #@14f
    .line 399
    const v6, 0x102000a

    #@152
    invoke-virtual {p0, v6}, Landroid/widget/CalendarView;->findViewById(I)Landroid/view/View;

    #@155
    move-result-object v6

    #@156
    check-cast v6, Landroid/widget/ListView;

    #@158
    iput-object v6, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@15a
    .line 400
    const v6, 0x1020281

    #@15d
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@160
    move-result-object v6

    #@161
    check-cast v6, Landroid/view/ViewGroup;

    #@163
    iput-object v6, p0, Landroid/widget/CalendarView;->mDayNamesHeader:Landroid/view/ViewGroup;

    #@165
    .line 401
    const v6, 0x1020280

    #@168
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@16b
    move-result-object v6

    #@16c
    check-cast v6, Landroid/widget/TextView;

    #@16e
    iput-object v6, p0, Landroid/widget/CalendarView;->mMonthName:Landroid/widget/TextView;

    #@170
    .line 403
    invoke-direct {p0}, Landroid/widget/CalendarView;->setUpHeader()V

    #@173
    .line 404
    invoke-direct {p0}, Landroid/widget/CalendarView;->setUpListView()V

    #@176
    .line 405
    invoke-direct {p0}, Landroid/widget/CalendarView;->setUpAdapter()V

    #@179
    .line 408
    iget-object v6, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@17b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@17e
    move-result-wide v7

    #@17f
    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@182
    .line 409
    iget-object v6, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@184
    iget-object v7, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@186
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@189
    move-result v6

    #@18a
    if-eqz v6, :cond_198

    #@18c
    .line 410
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@18e
    const/4 v7, 0x0

    #@18f
    const/4 v8, 0x1

    #@190
    const/4 v9, 0x1

    #@191
    invoke-direct {p0, v6, v7, v8, v9}, Landroid/widget/CalendarView;->goTo(Ljava/util/Calendar;ZZZ)V

    #@194
    .line 417
    :goto_194
    invoke-virtual {p0}, Landroid/widget/CalendarView;->invalidate()V

    #@197
    .line 418
    return-void

    #@198
    .line 411
    :cond_198
    iget-object v6, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@19a
    iget-object v7, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@19c
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@19f
    move-result v6

    #@1a0
    if-eqz v6, :cond_1ab

    #@1a2
    .line 412
    iget-object v6, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@1a4
    const/4 v7, 0x0

    #@1a5
    const/4 v8, 0x1

    #@1a6
    const/4 v9, 0x1

    #@1a7
    invoke-direct {p0, v6, v7, v8, v9}, Landroid/widget/CalendarView;->goTo(Ljava/util/Calendar;ZZZ)V

    #@1aa
    goto :goto_194

    #@1ab
    .line 414
    :cond_1ab
    iget-object v6, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@1ad
    const/4 v7, 0x0

    #@1ae
    const/4 v8, 0x1

    #@1af
    const/4 v9, 0x1

    #@1b0
    invoke-direct {p0, v6, v7, v8, v9}, Landroid/widget/CalendarView;->goTo(Ljava/util/Calendar;ZZZ)V

    #@1b3
    goto :goto_194
.end method

.method static synthetic access$1002(Landroid/widget/CalendarView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput p1, p0, Landroid/widget/CalendarView;->mCurrentScrollState:I

    #@2
    return p1
.end method

.method static synthetic access$1100(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mPreviousScrollState:I

    #@2
    return v0
.end method

.method static synthetic access$1102(Landroid/widget/CalendarView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput p1, p0, Landroid/widget/CalendarView;->mPreviousScrollState:I

    #@2
    return p1
.end method

.method static synthetic access$1200(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mListScrollTopOffset:I

    #@2
    return v0
.end method

.method static synthetic access$1300(Landroid/widget/CalendarView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-boolean v0, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@2
    return v0
.end method

.method static synthetic access$1402(Landroid/widget/CalendarView;Landroid/content/Context;)Landroid/content/Context;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Landroid/widget/CalendarView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/widget/CalendarView;Ljava/util/Calendar;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1}, Landroid/widget/CalendarView;->getWeeksSinceMinDate(Ljava/util/Calendar;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Landroid/widget/CalendarView;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Landroid/widget/CalendarView;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@2
    return v0
.end method

.method static synthetic access$2000(Landroid/widget/CalendarView;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Landroid/widget/CalendarView;)Landroid/widget/ListView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@2
    return-object v0
.end method

.method static synthetic access$2200(Landroid/widget/CalendarView;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Landroid/widget/CalendarView;Ljava/util/Calendar;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1}, Landroid/widget/CalendarView;->setMonthDisplayed(Ljava/util/Calendar;)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Landroid/widget/CalendarView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-boolean v0, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@2
    return v0
.end method

.method static synthetic access$2500(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mDaysPerWeek:I

    #@2
    return v0
.end method

.method static synthetic access$2600(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mDateTextSize:I

    #@2
    return v0
.end method

.method static synthetic access$2700(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mSelectedWeekBackgroundColor:I

    #@2
    return v0
.end method

.method static synthetic access$2800(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mWeekSeperatorLineWidth:I

    #@2
    return v0
.end method

.method static synthetic access$2900(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mFocusedMonthDateColor:I

    #@2
    return v0
.end method

.method static synthetic access$3000(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mUnfocusedMonthDateColor:I

    #@2
    return v0
.end method

.method static synthetic access$3100(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mWeekNumberColor:I

    #@2
    return v0
.end method

.method static synthetic access$3200(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mWeekSeparatorLineColor:I

    #@2
    return v0
.end method

.method static synthetic access$3300(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBarWidth:I

    #@2
    return v0
.end method

.method static synthetic access$3400(Landroid/widget/CalendarView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBar:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Landroid/widget/CalendarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/widget/CalendarView;->mShownWeekCount:I

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/widget/CalendarView;)Landroid/widget/CalendarView$OnDateChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mOnDateChangeListener:Landroid/widget/CalendarView$OnDateChangeListener;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/CalendarView;)Landroid/widget/CalendarView$WeeksAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/CalendarView;Landroid/widget/AbsListView;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/CalendarView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/widget/CalendarView;Landroid/widget/AbsListView;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/CalendarView;->onScroll(Landroid/widget/AbsListView;III)V

    #@3
    return-void
.end method

.method private getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;
    .registers 6
    .parameter "oldCalendar"
    .parameter "locale"

    #@0
    .prologue
    .line 983
    if-nez p1, :cond_7

    #@2
    .line 984
    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    #@5
    move-result-object v2

    #@6
    .line 989
    :goto_6
    return-object v2

    #@7
    .line 986
    :cond_7
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@a
    move-result-wide v0

    #@b
    .line 987
    .local v0, currentTimeMillis:J
    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    #@e
    move-result-object v2

    #@f
    .line 988
    .local v2, newCalendar:Ljava/util/Calendar;
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@12
    goto :goto_6
.end method

.method private getWeeksSinceMinDate(Ljava/util/Calendar;)I
    .registers 13
    .parameter "date"

    #@0
    .prologue
    .line 1271
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@2
    invoke-direct {p0, v6, p1}, Landroid/widget/CalendarView;->isSameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    #@5
    move-result v6

    #@6
    if-nez v6, :cond_3d

    #@8
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@a
    invoke-virtual {p1, v6}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_3d

    #@10
    .line 1273
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v7, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v8, "fromDate: "

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    iget-object v8, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@1f
    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    const-string v8, " does not precede toDate: "

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v7

    #@39
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v6

    #@3d
    .line 1276
    :cond_3d
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@40
    move-result-wide v6

    #@41
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@48
    move-result-wide v9

    #@49
    invoke-virtual {v8, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    #@4c
    move-result v8

    #@4d
    int-to-long v8, v8

    #@4e
    add-long v2, v6, v8

    #@50
    .line 1278
    .local v2, endTimeMillis:J
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@52
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    #@55
    move-result-wide v6

    #@56
    iget-object v8, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@58
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    #@5b
    move-result-object v8

    #@5c
    iget-object v9, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@5e
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    #@61
    move-result-wide v9

    #@62
    invoke-virtual {v8, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    #@65
    move-result v8

    #@66
    int-to-long v8, v8

    #@67
    add-long v4, v6, v8

    #@69
    .line 1280
    .local v4, startTimeMillis:J
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@6b
    const/4 v7, 0x7

    #@6c
    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    #@6f
    move-result v6

    #@70
    iget v7, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@72
    sub-int/2addr v6, v7

    #@73
    int-to-long v6, v6

    #@74
    const-wide/32 v8, 0x5265c00

    #@77
    mul-long v0, v6, v8

    #@79
    .line 1282
    .local v0, dayOffsetMillis:J
    sub-long v6, v2, v4

    #@7b
    add-long/2addr v6, v0

    #@7c
    const-wide/32 v8, 0x240c8400

    #@7f
    div-long/2addr v6, v8

    #@80
    long-to-int v6, v6

    #@81
    return v6
.end method

.method private goTo(Ljava/util/Calendar;ZZZ)V
    .registers 13
    .parameter "date"
    .parameter "animate"
    .parameter "setSelected"
    .parameter "forceScroll"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1102
    iget-object v4, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@3
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@6
    move-result v4

    #@7
    if-nez v4, :cond_11

    #@9
    iget-object v4, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@b
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_40

    #@11
    .line 1103
    :cond_11
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "Time not between "

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    iget-object v6, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@20
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@23
    move-result-object v6

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    const-string v6, " and "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    iget-object v6, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@30
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v5

    #@3c
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v4

    #@40
    .line 1107
    :cond_40
    iget-object v4, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@42
    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    #@45
    move-result v1

    #@46
    .line 1108
    .local v1, firstFullyVisiblePosition:I
    iget-object v4, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@48
    invoke-virtual {v4, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@4b
    move-result-object v0

    #@4c
    .line 1109
    .local v0, firstChild:Landroid/view/View;
    if-eqz v0, :cond_56

    #@4e
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@51
    move-result v4

    #@52
    if-gez v4, :cond_56

    #@54
    .line 1110
    add-int/lit8 v1, v1, 0x1

    #@56
    .line 1112
    :cond_56
    iget v4, p0, Landroid/widget/CalendarView;->mShownWeekCount:I

    #@58
    add-int/2addr v4, v1

    #@59
    add-int/lit8 v2, v4, -0x1

    #@5b
    .line 1113
    .local v2, lastFullyVisiblePosition:I
    if-eqz v0, :cond_67

    #@5d
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    #@60
    move-result v4

    #@61
    iget v5, p0, Landroid/widget/CalendarView;->mBottomBuffer:I

    #@63
    if-le v4, v5, :cond_67

    #@65
    .line 1114
    add-int/lit8 v2, v2, -0x1

    #@67
    .line 1116
    :cond_67
    if-eqz p3, :cond_6e

    #@69
    .line 1117
    iget-object v4, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@6b
    invoke-virtual {v4, p1}, Landroid/widget/CalendarView$WeeksAdapter;->setSelectedDay(Ljava/util/Calendar;)V

    #@6e
    .line 1120
    :cond_6e
    invoke-direct {p0, p1}, Landroid/widget/CalendarView;->getWeeksSinceMinDate(Ljava/util/Calendar;)I

    #@71
    move-result v3

    #@72
    .line 1124
    .local v3, position:I
    if-lt v3, v1, :cond_78

    #@74
    if-gt v3, v2, :cond_78

    #@76
    if-eqz p4, :cond_bb

    #@78
    .line 1126
    :cond_78
    iget-object v4, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@7a
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@7d
    move-result-wide v5

    #@7e
    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@81
    .line 1127
    iget-object v4, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@83
    const/4 v5, 0x5

    #@84
    const/4 v6, 0x1

    #@85
    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->set(II)V

    #@88
    .line 1129
    iget-object v4, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@8a
    invoke-direct {p0, v4}, Landroid/widget/CalendarView;->setMonthDisplayed(Ljava/util/Calendar;)V

    #@8d
    .line 1132
    iget-object v4, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@8f
    iget-object v5, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@91
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@94
    move-result v4

    #@95
    if-eqz v4, :cond_a7

    #@97
    .line 1133
    const/4 v3, 0x0

    #@98
    .line 1138
    :goto_98
    const/4 v4, 0x2

    #@99
    iput v4, p0, Landroid/widget/CalendarView;->mPreviousScrollState:I

    #@9b
    .line 1139
    if-eqz p2, :cond_ae

    #@9d
    .line 1140
    iget-object v4, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@9f
    iget v5, p0, Landroid/widget/CalendarView;->mListScrollTopOffset:I

    #@a1
    const/16 v6, 0x3e8

    #@a3
    invoke-virtual {v4, v3, v5, v6}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(III)V

    #@a6
    .line 1151
    :cond_a6
    :goto_a6
    return-void

    #@a7
    .line 1135
    :cond_a7
    iget-object v4, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@a9
    invoke-direct {p0, v4}, Landroid/widget/CalendarView;->getWeeksSinceMinDate(Ljava/util/Calendar;)I

    #@ac
    move-result v3

    #@ad
    goto :goto_98

    #@ae
    .line 1143
    :cond_ae
    iget-object v4, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@b0
    iget v5, p0, Landroid/widget/CalendarView;->mListScrollTopOffset:I

    #@b2
    invoke-virtual {v4, v3, v5}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    #@b5
    .line 1145
    iget-object v4, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@b7
    invoke-direct {p0, v4, v7}, Landroid/widget/CalendarView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    #@ba
    goto :goto_a6

    #@bb
    .line 1147
    :cond_bb
    if-eqz p3, :cond_a6

    #@bd
    .line 1149
    invoke-direct {p0, p1}, Landroid/widget/CalendarView;->setMonthDisplayed(Ljava/util/Calendar;)V

    #@c0
    goto :goto_a6
.end method

.method private invalidateAllWeekViews()V
    .registers 5

    #@0
    .prologue
    .line 951
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@2
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 952
    .local v0, childCount:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 953
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@b
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v2

    #@f
    .line 954
    .local v2, view:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    #@12
    .line 952
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_7

    #@15
    .line 956
    .end local v2           #view:Landroid/view/View;
    :cond_15
    return-void
.end method

.method private isSameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .registers 6
    .parameter "firstDate"
    .parameter "secondDate"

    #@0
    .prologue
    const/4 v2, 0x6

    #@1
    const/4 v0, 0x1

    #@2
    .line 998
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    #@5
    move-result v1

    #@6
    invoke-virtual {p2, v2}, Ljava/util/Calendar;->get(I)I

    #@9
    move-result v2

    #@a
    if-ne v1, v2, :cond_17

    #@c
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    #@f
    move-result v1

    #@10
    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    #@13
    move-result v2

    #@14
    if-ne v1, v2, :cond_17

    #@16
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method private onScroll(Landroid/widget/AbsListView;III)V
    .registers 14
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    #@0
    .prologue
    .line 1183
    const/4 v7, 0x0

    #@1
    invoke-virtual {p1, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    check-cast v0, Landroid/widget/CalendarView$WeekView;

    #@7
    .line 1184
    .local v0, child:Landroid/widget/CalendarView$WeekView;
    if-nez v0, :cond_a

    #@9
    .line 1242
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1189
    :cond_a
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    #@d
    move-result v7

    #@e
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeekView;->getHeight()I

    #@11
    move-result v8

    #@12
    mul-int/2addr v7, v8

    #@13
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeekView;->getBottom()I

    #@16
    move-result v8

    #@17
    sub-int/2addr v7, v8

    #@18
    int-to-long v1, v7

    #@19
    .line 1192
    .local v1, currScroll:J
    iget-wide v7, p0, Landroid/widget/CalendarView;->mPreviousScrollPosition:J

    #@1b
    cmp-long v7, v1, v7

    #@1d
    if-gez v7, :cond_6b

    #@1f
    .line 1193
    const/4 v7, 0x1

    #@20
    iput-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@22
    .line 1204
    :goto_22
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeekView;->getBottom()I

    #@25
    move-result v7

    #@26
    iget v8, p0, Landroid/widget/CalendarView;->mWeekMinVisibleHeight:I

    #@28
    if-ge v7, v8, :cond_75

    #@2a
    const/4 v6, 0x1

    #@2b
    .line 1205
    .local v6, offset:I
    :goto_2b
    iget-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@2d
    if-eqz v7, :cond_77

    #@2f
    .line 1206
    add-int/lit8 v7, v6, 0x2

    #@31
    invoke-virtual {p1, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@34
    move-result-object v0

    #@35
    .end local v0           #child:Landroid/widget/CalendarView$WeekView;
    check-cast v0, Landroid/widget/CalendarView$WeekView;

    #@37
    .line 1213
    .restart local v0       #child:Landroid/widget/CalendarView$WeekView;
    :cond_37
    :goto_37
    iget-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@39
    if-eqz v7, :cond_80

    #@3b
    .line 1214
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeekView;->getMonthOfFirstWeekDay()I

    #@3e
    move-result v4

    #@3f
    .line 1221
    .local v4, month:I
    :goto_3f
    iget v7, p0, Landroid/widget/CalendarView;->mCurrentMonthDisplayed:I

    #@41
    const/16 v8, 0xb

    #@43
    if-ne v7, v8, :cond_85

    #@45
    if-nez v4, :cond_85

    #@47
    .line 1222
    const/4 v5, 0x1

    #@48
    .line 1231
    .local v5, monthDiff:I
    :goto_48
    iget-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@4a
    if-nez v7, :cond_4e

    #@4c
    if-gtz v5, :cond_54

    #@4e
    :cond_4e
    iget-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@50
    if-eqz v7, :cond_64

    #@52
    if-gez v5, :cond_64

    #@54
    .line 1232
    :cond_54
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeekView;->getFirstDay()Ljava/util/Calendar;

    #@57
    move-result-object v3

    #@58
    .line 1233
    .local v3, firstDay:Ljava/util/Calendar;
    iget-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@5a
    if-eqz v7, :cond_94

    #@5c
    .line 1234
    const/4 v7, 0x5

    #@5d
    const/4 v8, -0x7

    #@5e
    invoke-virtual {v3, v7, v8}, Ljava/util/Calendar;->add(II)V

    #@61
    .line 1238
    :goto_61
    invoke-direct {p0, v3}, Landroid/widget/CalendarView;->setMonthDisplayed(Ljava/util/Calendar;)V

    #@64
    .line 1240
    .end local v3           #firstDay:Ljava/util/Calendar;
    :cond_64
    iput-wide v1, p0, Landroid/widget/CalendarView;->mPreviousScrollPosition:J

    #@66
    .line 1241
    iget v7, p0, Landroid/widget/CalendarView;->mCurrentScrollState:I

    #@68
    iput v7, p0, Landroid/widget/CalendarView;->mPreviousScrollState:I

    #@6a
    goto :goto_9

    #@6b
    .line 1194
    .end local v4           #month:I
    .end local v5           #monthDiff:I
    .end local v6           #offset:I
    :cond_6b
    iget-wide v7, p0, Landroid/widget/CalendarView;->mPreviousScrollPosition:J

    #@6d
    cmp-long v7, v1, v7

    #@6f
    if-lez v7, :cond_9

    #@71
    .line 1195
    const/4 v7, 0x0

    #@72
    iput-boolean v7, p0, Landroid/widget/CalendarView;->mIsScrollingUp:Z

    #@74
    goto :goto_22

    #@75
    .line 1204
    :cond_75
    const/4 v6, 0x0

    #@76
    goto :goto_2b

    #@77
    .line 1207
    .restart local v6       #offset:I
    :cond_77
    if-eqz v6, :cond_37

    #@79
    .line 1208
    invoke-virtual {p1, v6}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    #@7c
    move-result-object v0

    #@7d
    .end local v0           #child:Landroid/widget/CalendarView$WeekView;
    check-cast v0, Landroid/widget/CalendarView$WeekView;

    #@7f
    .restart local v0       #child:Landroid/widget/CalendarView$WeekView;
    goto :goto_37

    #@80
    .line 1216
    :cond_80
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeekView;->getMonthOfLastWeekDay()I

    #@83
    move-result v4

    #@84
    .restart local v4       #month:I
    goto :goto_3f

    #@85
    .line 1223
    :cond_85
    iget v7, p0, Landroid/widget/CalendarView;->mCurrentMonthDisplayed:I

    #@87
    if-nez v7, :cond_8f

    #@89
    const/16 v7, 0xb

    #@8b
    if-ne v4, v7, :cond_8f

    #@8d
    .line 1224
    const/4 v5, -0x1

    #@8e
    .restart local v5       #monthDiff:I
    goto :goto_48

    #@8f
    .line 1226
    .end local v5           #monthDiff:I
    :cond_8f
    iget v7, p0, Landroid/widget/CalendarView;->mCurrentMonthDisplayed:I

    #@91
    sub-int v5, v4, v7

    #@93
    .restart local v5       #monthDiff:I
    goto :goto_48

    #@94
    .line 1236
    .restart local v3       #firstDay:Ljava/util/Calendar;
    :cond_94
    const/4 v7, 0x5

    #@95
    const/4 v8, 0x7

    #@96
    invoke-virtual {v3, v7, v8}, Ljava/util/Calendar;->add(II)V

    #@99
    goto :goto_61
.end method

.method private onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 4
    .parameter "view"
    .parameter "scrollState"

    #@0
    .prologue
    .line 1174
    iget-object v0, p0, Landroid/widget/CalendarView;->mScrollStateChangedRunnable:Landroid/widget/CalendarView$ScrollStateRunnable;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/CalendarView$ScrollStateRunnable;->doScrollStateChange(Landroid/widget/AbsListView;I)V

    #@5
    .line 1175
    return-void
.end method

.method private parseDate(Ljava/lang/String;Ljava/util/Calendar;)Z
    .registers 7
    .parameter "date"
    .parameter "outDate"

    #@0
    .prologue
    .line 1161
    :try_start_0
    iget-object v1, p0, Landroid/widget/CalendarView;->mDateFormat:Ljava/text/DateFormat;

    #@2
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_9
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    .line 1162
    const/4 v1, 0x1

    #@a
    .line 1165
    :goto_a
    return v1

    #@b
    .line 1163
    :catch_b
    move-exception v0

    #@c
    .line 1164
    .local v0, e:Ljava/text/ParseException;
    sget-object v1, Landroid/widget/CalendarView;->LOG_TAG:Ljava/lang/String;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Date: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " not in format: "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, "MM/dd/yyyy"

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1165
    const/4 v1, 0x0

    #@31
    goto :goto_a
.end method

.method private setCurrentLocale(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 964
    iget-object v0, p0, Landroid/widget/CalendarView;->mCurrentLocale:Ljava/util/Locale;

    #@2
    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 974
    :goto_8
    return-void

    #@9
    .line 968
    :cond_9
    iput-object p1, p0, Landroid/widget/CalendarView;->mCurrentLocale:Ljava/util/Locale;

    #@b
    .line 970
    iget-object v0, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@d
    invoke-direct {p0, v0, p1}, Landroid/widget/CalendarView;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@13
    .line 971
    iget-object v0, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@15
    invoke-direct {p0, v0, p1}, Landroid/widget/CalendarView;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/widget/CalendarView;->mFirstDayOfMonth:Ljava/util/Calendar;

    #@1b
    .line 972
    iget-object v0, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@1d
    invoke-direct {p0, v0, p1}, Landroid/widget/CalendarView;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@20
    move-result-object v0

    #@21
    iput-object v0, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@23
    .line 973
    iget-object v0, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@25
    invoke-direct {p0, v0, p1}, Landroid/widget/CalendarView;->getCalendarForLocale(Ljava/util/Calendar;Ljava/util/Locale;)Ljava/util/Calendar;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@2b
    goto :goto_8
.end method

.method private setMonthDisplayed(Ljava/util/Calendar;)V
    .registers 11
    .parameter "calendar"

    #@0
    .prologue
    .line 1251
    const/4 v0, 0x2

    #@1
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    #@4
    move-result v7

    #@5
    .line 1252
    .local v7, newMonthDisplayed:I
    iget v0, p0, Landroid/widget/CalendarView;->mCurrentMonthDisplayed:I

    #@7
    if-eq v0, v7, :cond_2b

    #@9
    .line 1253
    iput v7, p0, Landroid/widget/CalendarView;->mCurrentMonthDisplayed:I

    #@b
    .line 1254
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@d
    iget v3, p0, Landroid/widget/CalendarView;->mCurrentMonthDisplayed:I

    #@f
    invoke-virtual {v0, v3}, Landroid/widget/CalendarView$WeeksAdapter;->setFocusMonth(I)V

    #@12
    .line 1255
    const/16 v6, 0x34

    #@14
    .line 1257
    .local v6, flags:I
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@17
    move-result-wide v1

    #@18
    .line 1258
    .local v1, millis:J
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@1a
    const/16 v5, 0x34

    #@1c
    move-wide v3, v1

    #@1d
    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@20
    move-result-object v8

    #@21
    .line 1259
    .local v8, newMonthName:Ljava/lang/String;
    iget-object v0, p0, Landroid/widget/CalendarView;->mMonthName:Landroid/widget/TextView;

    #@23
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@26
    .line 1260
    iget-object v0, p0, Landroid/widget/CalendarView;->mMonthName:Landroid/widget/TextView;

    #@28
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    #@2b
    .line 1262
    .end local v1           #millis:J
    .end local v6           #flags:I
    .end local v8           #newMonthName:Ljava/lang/String;
    :cond_2b
    return-void
.end method

.method private setUpAdapter()V
    .registers 3

    #@0
    .prologue
    .line 1006
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@2
    if-nez v0, :cond_20

    #@4
    .line 1007
    new-instance v0, Landroid/widget/CalendarView$WeeksAdapter;

    #@6
    invoke-virtual {p0}, Landroid/widget/CalendarView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v1

    #@a
    invoke-direct {v0, p0, v1}, Landroid/widget/CalendarView$WeeksAdapter;-><init>(Landroid/widget/CalendarView;Landroid/content/Context;)V

    #@d
    iput-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@f
    .line 1008
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@11
    new-instance v1, Landroid/widget/CalendarView$1;

    #@13
    invoke-direct {v1, p0}, Landroid/widget/CalendarView$1;-><init>(Landroid/widget/CalendarView;)V

    #@16
    invoke-virtual {v0, v1}, Landroid/widget/CalendarView$WeeksAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@19
    .line 1020
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@1b
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@20
    .line 1024
    :cond_20
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@22
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeeksAdapter;->notifyDataSetChanged()V

    #@25
    .line 1025
    return-void
.end method

.method private setUpHeader()V
    .registers 10

    #@0
    .prologue
    const/16 v8, 0x8

    #@2
    const/4 v7, 0x0

    #@3
    .line 1031
    iget v4, p0, Landroid/widget/CalendarView;->mDaysPerWeek:I

    #@5
    new-array v4, v4, [Ljava/lang/String;

    #@7
    iput-object v4, p0, Landroid/widget/CalendarView;->mDayLabels:[Ljava/lang/String;

    #@9
    .line 1032
    iget v2, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@b
    .local v2, i:I
    iget v4, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@d
    iget v5, p0, Landroid/widget/CalendarView;->mDaysPerWeek:I

    #@f
    add-int v1, v4, v5

    #@11
    .local v1, count:I
    :goto_11
    if-ge v2, v1, :cond_2b

    #@13
    .line 1033
    const/4 v4, 0x7

    #@14
    if-le v2, v4, :cond_29

    #@16
    add-int/lit8 v0, v2, -0x7

    #@18
    .line 1034
    .local v0, calendarDay:I
    :goto_18
    iget-object v4, p0, Landroid/widget/CalendarView;->mDayLabels:[Ljava/lang/String;

    #@1a
    iget v5, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@1c
    sub-int v5, v2, v5

    #@1e
    const/16 v6, 0x32

    #@20
    invoke-static {v0, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    #@23
    move-result-object v6

    #@24
    aput-object v6, v4, v5

    #@26
    .line 1032
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_11

    #@29
    .end local v0           #calendarDay:I
    :cond_29
    move v0, v2

    #@2a
    .line 1033
    goto :goto_18

    #@2b
    .line 1038
    :cond_2b
    iget-object v4, p0, Landroid/widget/CalendarView;->mDayNamesHeader:Landroid/view/ViewGroup;

    #@2d
    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@30
    move-result-object v3

    #@31
    check-cast v3, Landroid/widget/TextView;

    #@33
    .line 1039
    .local v3, label:Landroid/widget/TextView;
    iget-boolean v4, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@35
    if-eqz v4, :cond_6c

    #@37
    .line 1040
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    #@3a
    .line 1044
    :goto_3a
    const/4 v2, 0x1

    #@3b
    iget-object v4, p0, Landroid/widget/CalendarView;->mDayNamesHeader:Landroid/view/ViewGroup;

    #@3d
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    #@40
    move-result v1

    #@41
    :goto_41
    if-ge v2, v1, :cond_74

    #@43
    .line 1045
    iget-object v4, p0, Landroid/widget/CalendarView;->mDayNamesHeader:Landroid/view/ViewGroup;

    #@45
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@48
    move-result-object v3

    #@49
    .end local v3           #label:Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    #@4b
    .line 1046
    .restart local v3       #label:Landroid/widget/TextView;
    iget v4, p0, Landroid/widget/CalendarView;->mWeekDayTextAppearanceResId:I

    #@4d
    const/4 v5, -0x1

    #@4e
    if-le v4, v5, :cond_57

    #@50
    .line 1047
    iget-object v4, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@52
    iget v5, p0, Landroid/widget/CalendarView;->mWeekDayTextAppearanceResId:I

    #@54
    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@57
    .line 1049
    :cond_57
    iget v4, p0, Landroid/widget/CalendarView;->mDaysPerWeek:I

    #@59
    add-int/lit8 v4, v4, 0x1

    #@5b
    if-ge v2, v4, :cond_70

    #@5d
    .line 1050
    iget-object v4, p0, Landroid/widget/CalendarView;->mDayLabels:[Ljava/lang/String;

    #@5f
    add-int/lit8 v5, v2, -0x1

    #@61
    aget-object v4, v4, v5

    #@63
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@66
    .line 1051
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    #@69
    .line 1044
    :goto_69
    add-int/lit8 v2, v2, 0x1

    #@6b
    goto :goto_41

    #@6c
    .line 1042
    :cond_6c
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    #@6f
    goto :goto_3a

    #@70
    .line 1053
    :cond_70
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    #@73
    goto :goto_69

    #@74
    .line 1056
    :cond_74
    iget-object v4, p0, Landroid/widget/CalendarView;->mDayNamesHeader:Landroid/view/ViewGroup;

    #@76
    invoke-virtual {v4}, Landroid/view/ViewGroup;->invalidate()V

    #@79
    .line 1057
    return-void
.end method

.method private setUpListView()V
    .registers 3

    #@0
    .prologue
    .line 1064
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    #@6
    .line 1065
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    #@c
    .line 1066
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    #@12
    .line 1067
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@14
    new-instance v1, Landroid/widget/CalendarView$2;

    #@16
    invoke-direct {v1, p0}, Landroid/widget/CalendarView$2;-><init>(Landroid/widget/CalendarView;)V

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    #@1c
    .line 1080
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@1e
    iget v1, p0, Landroid/widget/CalendarView;->mFriction:F

    #@20
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFriction(F)V

    #@23
    .line 1081
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@25
    iget v1, p0, Landroid/widget/CalendarView;->mVelocityScale:F

    #@27
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVelocityScale(F)V

    #@2a
    .line 1082
    return-void
.end method

.method private updateDateTextSize()V
    .registers 5

    #@0
    .prologue
    .line 940
    invoke-virtual {p0}, Landroid/widget/CalendarView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    iget v2, p0, Landroid/widget/CalendarView;->mDateTextAppearanceResId:I

    #@6
    sget-object v3, Lcom/android/internal/R$styleable;->TextAppearance:[I

    #@8
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 942
    .local v0, dateTextAppearance:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@d
    const/16 v2, 0xe

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@12
    move-result v1

    #@13
    iput v1, p0, Landroid/widget/CalendarView;->mDateTextSize:I

    #@15
    .line 944
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@18
    .line 945
    return-void
.end method


# virtual methods
.method public getDate()J
    .registers 3

    #@0
    .prologue
    .line 897
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@2
    invoke-static {v0}, Landroid/widget/CalendarView$WeeksAdapter;->access$400(Landroid/widget/CalendarView$WeeksAdapter;)Ljava/util/Calendar;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@9
    move-result-wide v0

    #@a
    return-wide v0
.end method

.method public getDateTextAppearance()I
    .registers 2

    #@0
    .prologue
    .line 682
    iget v0, p0, Landroid/widget/CalendarView;->mDateTextAppearanceResId:I

    #@2
    return v0
.end method

.method public getFirstDayOfWeek()I
    .registers 2

    #@0
    .prologue
    .line 853
    iget v0, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@2
    return v0
.end method

.method public getFocusedMonthDateColor()I
    .registers 2

    #@0
    .prologue
    .line 504
    iget v0, p0, Landroid/widget/CalendarView;->mFocusedMonthDateColor:I

    #@2
    return v0
.end method

.method public getMaxDate()J
    .registers 3

    #@0
    .prologue
    .line 778
    iget-object v0, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@2
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getMinDate()J
    .registers 3

    #@0
    .prologue
    .line 726
    iget-object v0, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@2
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getSelectedDateVerticalBar()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 631
    iget-object v0, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBar:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getSelectedWeekBackgroundColor()I
    .registers 2

    #@0
    .prologue
    .line 473
    iget v0, p0, Landroid/widget/CalendarView;->mSelectedWeekBackgroundColor:I

    #@2
    return v0
.end method

.method public getShowWeekNumber()Z
    .registers 2

    #@0
    .prologue
    .line 834
    iget-boolean v0, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@2
    return v0
.end method

.method public getShownWeekCount()I
    .registers 2

    #@0
    .prologue
    .line 442
    iget v0, p0, Landroid/widget/CalendarView;->mShownWeekCount:I

    #@2
    return v0
.end method

.method public getUnfocusedMonthDateColor()I
    .registers 2

    #@0
    .prologue
    .line 535
    iget v0, p0, Landroid/widget/CalendarView;->mFocusedMonthDateColor:I

    #@2
    return v0
.end method

.method public getWeekDayTextAppearance()I
    .registers 2

    #@0
    .prologue
    .line 656
    iget v0, p0, Landroid/widget/CalendarView;->mWeekDayTextAppearanceResId:I

    #@2
    return v0
.end method

.method public getWeekNumberColor()I
    .registers 2

    #@0
    .prologue
    .line 562
    iget v0, p0, Landroid/widget/CalendarView;->mWeekNumberColor:I

    #@2
    return v0
.end method

.method public getWeekSeparatorLineColor()I
    .registers 2

    #@0
    .prologue
    .line 587
    iget v0, p0, Landroid/widget/CalendarView;->mWeekSeparatorLineColor:I

    #@2
    return v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 692
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@2
    invoke-virtual {v0}, Landroid/widget/ListView;->isEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 697
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 698
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5
    invoke-direct {p0, v0}, Landroid/widget/CalendarView;->setCurrentLocale(Ljava/util/Locale;)V

    #@8
    .line 699
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 703
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 704
    const-class v0, Landroid/widget/CalendarView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 705
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 709
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 710
    const-class v0, Landroid/widget/CalendarView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 711
    return-void
.end method

.method public setDate(J)V
    .registers 4
    .parameter "date"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 914
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/widget/CalendarView;->setDate(JZZ)V

    #@4
    .line 915
    return-void
.end method

.method public setDate(JZZ)V
    .registers 7
    .parameter "date"
    .parameter "animate"
    .parameter "center"

    #@0
    .prologue
    .line 932
    iget-object v0, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@5
    .line 933
    iget-object v0, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@7
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@9
    invoke-static {v1}, Landroid/widget/CalendarView$WeeksAdapter;->access$400(Landroid/widget/CalendarView$WeeksAdapter;)Ljava/util/Calendar;

    #@c
    move-result-object v1

    #@d
    invoke-direct {p0, v0, v1}, Landroid/widget/CalendarView;->isSameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_14

    #@13
    .line 937
    :goto_13
    return-void

    #@14
    .line 936
    :cond_14
    iget-object v0, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@16
    const/4 v1, 0x1

    #@17
    invoke-direct {p0, v0, p3, v1, p4}, Landroid/widget/CalendarView;->goTo(Ljava/util/Calendar;ZZZ)V

    #@1a
    goto :goto_13
.end method

.method public setDateTextAppearance(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 667
    iget v0, p0, Landroid/widget/CalendarView;->mDateTextAppearanceResId:I

    #@2
    if-eq v0, p1, :cond_c

    #@4
    .line 668
    iput p1, p0, Landroid/widget/CalendarView;->mDateTextAppearanceResId:I

    #@6
    .line 669
    invoke-direct {p0}, Landroid/widget/CalendarView;->updateDateTextSize()V

    #@9
    .line 670
    invoke-direct {p0}, Landroid/widget/CalendarView;->invalidateAllWeekViews()V

    #@c
    .line 672
    :cond_c
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 687
    iget-object v0, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    #@5
    .line 688
    return-void
.end method

.method public setFirstDayOfWeek(I)V
    .registers 3
    .parameter "firstDayOfWeek"

    #@0
    .prologue
    .line 872
    iget v0, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 879
    :goto_4
    return-void

    #@5
    .line 875
    :cond_5
    iput p1, p0, Landroid/widget/CalendarView;->mFirstDayOfWeek:I

    #@7
    .line 876
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@9
    invoke-static {v0}, Landroid/widget/CalendarView$WeeksAdapter;->access$500(Landroid/widget/CalendarView$WeeksAdapter;)V

    #@c
    .line 877
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@e
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeeksAdapter;->notifyDataSetChanged()V

    #@11
    .line 878
    invoke-direct {p0}, Landroid/widget/CalendarView;->setUpHeader()V

    #@14
    goto :goto_4
.end method

.method public setFocusedMonthDateColor(I)V
    .registers 6
    .parameter "color"

    #@0
    .prologue
    .line 484
    iget v3, p0, Landroid/widget/CalendarView;->mFocusedMonthDateColor:I

    #@2
    if-eq v3, p1, :cond_23

    #@4
    .line 485
    iput p1, p0, Landroid/widget/CalendarView;->mFocusedMonthDateColor:I

    #@6
    .line 486
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@8
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 487
    .local v0, childCount:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_23

    #@f
    .line 488
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@11
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/widget/CalendarView$WeekView;

    #@17
    .line 489
    .local v2, weekView:Landroid/widget/CalendarView$WeekView;
    invoke-static {v2}, Landroid/widget/CalendarView$WeekView;->access$200(Landroid/widget/CalendarView$WeekView;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_20

    #@1d
    .line 490
    invoke-virtual {v2}, Landroid/widget/CalendarView$WeekView;->invalidate()V

    #@20
    .line 487
    :cond_20
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_d

    #@23
    .line 494
    .end local v0           #childCount:I
    .end local v1           #i:I
    .end local v2           #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_23
    return-void
.end method

.method public setMaxDate(J)V
    .registers 7
    .parameter "maxDate"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 791
    iget-object v1, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@3
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@6
    .line 792
    iget-object v1, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@8
    iget-object v2, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@a
    invoke-direct {p0, v1, v2}, Landroid/widget/CalendarView;->isSameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 808
    :goto_10
    return-void

    #@11
    .line 795
    :cond_11
    iget-object v1, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@13
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@16
    .line 797
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@18
    invoke-static {v1}, Landroid/widget/CalendarView$WeeksAdapter;->access$500(Landroid/widget/CalendarView$WeeksAdapter;)V

    #@1b
    .line 798
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@1d
    invoke-static {v1}, Landroid/widget/CalendarView$WeeksAdapter;->access$400(Landroid/widget/CalendarView$WeeksAdapter;)Ljava/util/Calendar;

    #@20
    move-result-object v0

    #@21
    .line 799
    .local v0, date:Ljava/util/Calendar;
    iget-object v1, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@23
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_33

    #@29
    .line 800
    iget-object v1, p0, Landroid/widget/CalendarView;->mMaxDate:Ljava/util/Calendar;

    #@2b
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@2e
    move-result-wide v1

    #@2f
    invoke-virtual {p0, v1, v2}, Landroid/widget/CalendarView;->setDate(J)V

    #@32
    goto :goto_10

    #@33
    .line 806
    :cond_33
    const/4 v1, 0x1

    #@34
    invoke-direct {p0, v0, v3, v1, v3}, Landroid/widget/CalendarView;->goTo(Ljava/util/Calendar;ZZZ)V

    #@37
    goto :goto_10
.end method

.method public setMinDate(J)V
    .registers 7
    .parameter "minDate"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 739
    iget-object v1, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@3
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@6
    .line 740
    iget-object v1, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@8
    iget-object v2, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@a
    invoke-direct {p0, v1, v2}, Landroid/widget/CalendarView;->isSameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    .line 763
    :goto_10
    return-void

    #@11
    .line 743
    :cond_11
    iget-object v1, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@13
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@16
    .line 748
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@18
    invoke-static {v1}, Landroid/widget/CalendarView$WeeksAdapter;->access$400(Landroid/widget/CalendarView$WeeksAdapter;)Ljava/util/Calendar;

    #@1b
    move-result-object v0

    #@1c
    .line 749
    .local v0, date:Ljava/util/Calendar;
    iget-object v1, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@1e
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_2b

    #@24
    .line 750
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@26
    iget-object v2, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@28
    invoke-virtual {v1, v2}, Landroid/widget/CalendarView$WeeksAdapter;->setSelectedDay(Ljava/util/Calendar;)V

    #@2b
    .line 753
    :cond_2b
    iget-object v1, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@2d
    invoke-static {v1}, Landroid/widget/CalendarView$WeeksAdapter;->access$500(Landroid/widget/CalendarView$WeeksAdapter;)V

    #@30
    .line 754
    iget-object v1, p0, Landroid/widget/CalendarView;->mMinDate:Ljava/util/Calendar;

    #@32
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_42

    #@38
    .line 755
    iget-object v1, p0, Landroid/widget/CalendarView;->mTempDate:Ljava/util/Calendar;

    #@3a
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@3d
    move-result-wide v1

    #@3e
    invoke-virtual {p0, v1, v2}, Landroid/widget/CalendarView;->setDate(J)V

    #@41
    goto :goto_10

    #@42
    .line 761
    :cond_42
    const/4 v1, 0x1

    #@43
    invoke-direct {p0, v0, v3, v1, v3}, Landroid/widget/CalendarView;->goTo(Ljava/util/Calendar;ZZZ)V

    #@46
    goto :goto_10
.end method

.method public setOnDateChangeListener(Landroid/widget/CalendarView$OnDateChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 887
    iput-object p1, p0, Landroid/widget/CalendarView;->mOnDateChangeListener:Landroid/widget/CalendarView$OnDateChangeListener;

    #@2
    .line 888
    return-void
.end method

.method public setSelectedDateVerticalBar(I)V
    .registers 4
    .parameter "resourceId"

    #@0
    .prologue
    .line 599
    invoke-virtual {p0}, Landroid/widget/CalendarView;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    .line 600
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v0}, Landroid/widget/CalendarView;->setSelectedDateVerticalBar(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 601
    return-void
.end method

.method public setSelectedDateVerticalBar(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "drawable"

    #@0
    .prologue
    .line 612
    iget-object v3, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBar:Landroid/graphics/drawable/Drawable;

    #@2
    if-eq v3, p1, :cond_23

    #@4
    .line 613
    iput-object p1, p0, Landroid/widget/CalendarView;->mSelectedDateVerticalBar:Landroid/graphics/drawable/Drawable;

    #@6
    .line 614
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@8
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 615
    .local v0, childCount:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_23

    #@f
    .line 616
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@11
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/widget/CalendarView$WeekView;

    #@17
    .line 617
    .local v2, weekView:Landroid/widget/CalendarView$WeekView;
    invoke-static {v2}, Landroid/widget/CalendarView$WeekView;->access$100(Landroid/widget/CalendarView$WeekView;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_20

    #@1d
    .line 618
    invoke-virtual {v2}, Landroid/widget/CalendarView$WeekView;->invalidate()V

    #@20
    .line 615
    :cond_20
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_d

    #@23
    .line 622
    .end local v0           #childCount:I
    .end local v1           #i:I
    .end local v2           #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_23
    return-void
.end method

.method public setSelectedWeekBackgroundColor(I)V
    .registers 6
    .parameter "color"

    #@0
    .prologue
    .line 453
    iget v3, p0, Landroid/widget/CalendarView;->mSelectedWeekBackgroundColor:I

    #@2
    if-eq v3, p1, :cond_23

    #@4
    .line 454
    iput p1, p0, Landroid/widget/CalendarView;->mSelectedWeekBackgroundColor:I

    #@6
    .line 455
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@8
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 456
    .local v0, childCount:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_23

    #@f
    .line 457
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@11
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/widget/CalendarView$WeekView;

    #@17
    .line 458
    .local v2, weekView:Landroid/widget/CalendarView$WeekView;
    invoke-static {v2}, Landroid/widget/CalendarView$WeekView;->access$100(Landroid/widget/CalendarView$WeekView;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_20

    #@1d
    .line 459
    invoke-virtual {v2}, Landroid/widget/CalendarView$WeekView;->invalidate()V

    #@20
    .line 456
    :cond_20
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_d

    #@23
    .line 463
    .end local v0           #childCount:I
    .end local v1           #i:I
    .end local v2           #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_23
    return-void
.end method

.method public setShowWeekNumber(Z)V
    .registers 3
    .parameter "showWeekNumber"

    #@0
    .prologue
    .line 818
    iget-boolean v0, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 824
    :goto_4
    return-void

    #@5
    .line 821
    :cond_5
    iput-boolean p1, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@7
    .line 822
    iget-object v0, p0, Landroid/widget/CalendarView;->mAdapter:Landroid/widget/CalendarView$WeeksAdapter;

    #@9
    invoke-virtual {v0}, Landroid/widget/CalendarView$WeeksAdapter;->notifyDataSetChanged()V

    #@c
    .line 823
    invoke-direct {p0}, Landroid/widget/CalendarView;->setUpHeader()V

    #@f
    goto :goto_4
.end method

.method public setShownWeekCount(I)V
    .registers 3
    .parameter "count"

    #@0
    .prologue
    .line 428
    iget v0, p0, Landroid/widget/CalendarView;->mShownWeekCount:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 429
    iput p1, p0, Landroid/widget/CalendarView;->mShownWeekCount:I

    #@6
    .line 430
    invoke-virtual {p0}, Landroid/widget/CalendarView;->invalidate()V

    #@9
    .line 432
    :cond_9
    return-void
.end method

.method public setUnfocusedMonthDateColor(I)V
    .registers 6
    .parameter "color"

    #@0
    .prologue
    .line 515
    iget v3, p0, Landroid/widget/CalendarView;->mUnfocusedMonthDateColor:I

    #@2
    if-eq v3, p1, :cond_23

    #@4
    .line 516
    iput p1, p0, Landroid/widget/CalendarView;->mUnfocusedMonthDateColor:I

    #@6
    .line 517
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@8
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 518
    .local v0, childCount:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_23

    #@f
    .line 519
    iget-object v3, p0, Landroid/widget/CalendarView;->mListView:Landroid/widget/ListView;

    #@11
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/widget/CalendarView$WeekView;

    #@17
    .line 520
    .local v2, weekView:Landroid/widget/CalendarView$WeekView;
    invoke-static {v2}, Landroid/widget/CalendarView$WeekView;->access$300(Landroid/widget/CalendarView$WeekView;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_20

    #@1d
    .line 521
    invoke-virtual {v2}, Landroid/widget/CalendarView$WeekView;->invalidate()V

    #@20
    .line 518
    :cond_20
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_d

    #@23
    .line 525
    .end local v0           #childCount:I
    .end local v1           #i:I
    .end local v2           #weekView:Landroid/widget/CalendarView$WeekView;
    :cond_23
    return-void
.end method

.method public setWeekDayTextAppearance(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 642
    iget v0, p0, Landroid/widget/CalendarView;->mWeekDayTextAppearanceResId:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 643
    iput p1, p0, Landroid/widget/CalendarView;->mWeekDayTextAppearanceResId:I

    #@6
    .line 644
    invoke-direct {p0}, Landroid/widget/CalendarView;->setUpHeader()V

    #@9
    .line 646
    :cond_9
    return-void
.end method

.method public setWeekNumberColor(I)V
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 546
    iget v0, p0, Landroid/widget/CalendarView;->mWeekNumberColor:I

    #@2
    if-eq v0, p1, :cond_d

    #@4
    .line 547
    iput p1, p0, Landroid/widget/CalendarView;->mWeekNumberColor:I

    #@6
    .line 548
    iget-boolean v0, p0, Landroid/widget/CalendarView;->mShowWeekNumber:Z

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 549
    invoke-direct {p0}, Landroid/widget/CalendarView;->invalidateAllWeekViews()V

    #@d
    .line 552
    :cond_d
    return-void
.end method

.method public setWeekSeparatorLineColor(I)V
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 573
    iget v0, p0, Landroid/widget/CalendarView;->mWeekSeparatorLineColor:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 574
    iput p1, p0, Landroid/widget/CalendarView;->mWeekSeparatorLineColor:I

    #@6
    .line 575
    invoke-direct {p0}, Landroid/widget/CalendarView;->invalidateAllWeekViews()V

    #@9
    .line 577
    :cond_9
    return-void
.end method
