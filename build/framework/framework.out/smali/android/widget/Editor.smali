.class public Landroid/widget/Editor;
.super Ljava/lang/Object;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/Editor$UserDictionaryListener;,
        Landroid/widget/Editor$InputMethodState;,
        Landroid/widget/Editor$InputContentType;,
        Landroid/widget/Editor$ErrorPopup;,
        Landroid/widget/Editor$CorrectionHighlighter;,
        Landroid/widget/Editor$SelectionModifierCursorController;,
        Landroid/widget/Editor$InsertionPointCursorController;,
        Landroid/widget/Editor$CursorController;,
        Landroid/widget/Editor$SelectionEndHandleView;,
        Landroid/widget/Editor$SelectionStartHandleView;,
        Landroid/widget/Editor$InsertionHandleView;,
        Landroid/widget/Editor$HandleView;,
        Landroid/widget/Editor$ActionPopupWindow;,
        Landroid/widget/Editor$SelectionActionModeCallback;,
        Landroid/widget/Editor$SuggestionsPopupWindow;,
        Landroid/widget/Editor$PinnedPopupWindow;,
        Landroid/widget/Editor$PositionListener;,
        Landroid/widget/Editor$EasyEditPopupWindow;,
        Landroid/widget/Editor$EasyEditSpanController;,
        Landroid/widget/Editor$DragLocalState;,
        Landroid/widget/Editor$Blink;,
        Landroid/widget/Editor$TextViewPositionListener;
    }
.end annotation


# static fields
.field static final BLINK:I = 0x1f4

.field private static DRAG_SHADOW_MAX_TEXT_LENGTH:I = 0x0

.field static final EXTRACT_NOTHING:I = -0x2

.field static final EXTRACT_UNKNOWN:I = -0x1

.field public static final INPUT_TYPE_TEXT_ONLY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Editor"

.field private static final TEMP_POSITION:[F


# instance fields
.field private isHideCliptrayOpenCueAfterDelay:Z

.field mBlink:Landroid/widget/Editor$Blink;

.field mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

.field public mClipDataType:I

.field mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

.field private mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

.field mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

.field mCreatedWithASelection:Z

.field mCursorCount:I

.field final mCursorDrawable:[Landroid/graphics/drawable/Drawable;

.field mCursorVisible:Z

.field mCustomMode:Landroid/view/ActionMode;

.field mCustomSelectionActionModeCallback:Landroid/view/ActionMode$Callback;

.field private mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

.field mDiscardNextActionUp:Z

.field private mEasyEditSpanController:Landroid/widget/Editor$EasyEditSpanController;

.field mError:Ljava/lang/CharSequence;

.field mErrorPopup:Landroid/widget/Editor$ErrorPopup;

.field mErrorWasChanged:Z

.field mFrozenWithFocus:Z

.field private mHasWindowFocus:Z

.field mIgnoreActionUpEvent:Z

.field mInBatchEditControllers:Z

.field mInputContentType:Landroid/widget/Editor$InputContentType;

.field mInputMethodState:Landroid/widget/Editor$InputMethodState;

.field mInputType:I

.field mInsertionControllerEnabled:Z

.field mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

.field mIsAleadyBubblePopupStarted:Z

.field mKeyListener:Landroid/text/method/KeyListener;

.field mLastDownPositionX:F

.field mLastDownPositionY:F

.field mLastLayoutHeight:I

.field private mPositionListener:Landroid/widget/Editor$PositionListener;

.field mPreserveDetachedSelection:Z

.field mSelectAllOnFocus:Z

.field private mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleRight:Landroid/graphics/drawable/Drawable;

.field mSelectionActionMode:Landroid/view/ActionMode;

.field mSelectionControllerEnabled:Z

.field mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

.field mSelectionMoved:Z

.field mShowCursor:J

.field mShowErrorAfterAttach:Z

.field mShowSoftInputOnFocus:Z

.field mShowSuggestionRunnable:Ljava/lang/Runnable;

.field mSpellChecker:Landroid/widget/SpellChecker;

.field mSuggestionRangeSpan:Landroid/text/style/SuggestionRangeSpan;

.field mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

.field private mTempRect:Landroid/graphics/Rect;

.field mTemporaryDetach:Z

.field mTextDisplayLists:[Landroid/view/DisplayList;

.field mTextIsSelectable:Z

.field private mTextView:Landroid/widget/TextView;

.field mTouchFocusSelected:Z

.field private final mUserDictionaryListener:Landroid/widget/Editor$UserDictionaryListener;

.field mWordIterator:Landroid/text/method/WordIterator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 128
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [F

    #@3
    sput-object v0, Landroid/widget/Editor;->TEMP_POSITION:[F

    #@5
    .line 129
    const/16 v0, 0x14

    #@7
    sput v0, Landroid/widget/Editor;->DRAG_SHADOW_MAX_TEXT_LENGTH:I

    #@9
    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;)V
    .registers 6
    .parameter "textView"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 152
    iput v3, p0, Landroid/widget/Editor;->mInputType:I

    #@8
    .line 160
    iput-boolean v2, p0, Landroid/widget/Editor;->mCursorVisible:Z

    #@a
    .line 177
    iput-boolean v2, p0, Landroid/widget/Editor;->mShowSoftInputOnFocus:Z

    #@c
    .line 185
    const/4 v0, 0x2

    #@d
    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    #@f
    iput-object v0, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@11
    .line 210
    new-instance v0, Landroid/widget/Editor$UserDictionaryListener;

    #@13
    invoke-direct {v0}, Landroid/widget/Editor$UserDictionaryListener;-><init>()V

    #@16
    iput-object v0, p0, Landroid/widget/Editor;->mUserDictionaryListener:Landroid/widget/Editor$UserDictionaryListener;

    #@18
    .line 213
    iput-boolean v2, p0, Landroid/widget/Editor;->mHasWindowFocus:Z

    #@1a
    .line 219
    iput-object v1, p0, Landroid/widget/Editor;->mCustomMode:Landroid/view/ActionMode;

    #@1c
    .line 222
    iput-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@1e
    .line 223
    iput-object v1, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@20
    .line 224
    iput-object v1, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@22
    .line 225
    iput-boolean v3, p0, Landroid/widget/Editor;->isHideCliptrayOpenCueAfterDelay:Z

    #@24
    .line 232
    iput-object p1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@26
    .line 234
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@28
    if-eqz v0, :cond_3e

    #@2a
    .line 235
    new-instance v0, Landroid/widget/BubblePopupHelper;

    #@2c
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2e
    invoke-direct {v0, v1}, Landroid/widget/BubblePopupHelper;-><init>(Landroid/widget/TextView;)V

    #@31
    iput-object v0, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@33
    .line 236
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@35
    instance-of v0, v0, Landroid/inputmethodservice/ExtractEditText;

    #@37
    if-eqz v0, :cond_3e

    #@39
    iget-object v0, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@3b
    invoke-virtual {v0}, Landroid/widget/BubblePopupHelper;->setTargetHelper()V

    #@3e
    .line 239
    :cond_3e
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@40
    if-eqz v0, :cond_4f

    #@42
    .line 240
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@44
    if-nez v0, :cond_4f

    #@46
    .line 241
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@48
    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {p0, v0}, Landroid/widget/Editor;->createCliptrayManager(Landroid/content/Context;)V

    #@4f
    .line 244
    :cond_4f
    return-void
.end method

.method static synthetic access$000(Landroid/widget/Editor;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->extractedTextModeWillBeStarted()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->isFloatingWindow()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->getPositionListener()Landroid/widget/Editor$PositionListener;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1400(Landroid/widget/Editor;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    invoke-direct {p0, p1}, Landroid/widget/Editor;->isOffsetVisible(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2200(Landroid/widget/Editor;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$2202(Landroid/widget/Editor;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@2
    return-object p1
.end method

.method static synthetic access$2300(Landroid/widget/Editor;)Landroid/widget/Editor$UserDictionaryListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mUserDictionaryListener:Landroid/widget/Editor$UserDictionaryListener;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Landroid/widget/Editor;IIZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 125
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Editor;->updateSpellCheckSpans(IIZ)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@2
    return-object v0
.end method

.method static synthetic access$2600(Landroid/widget/Editor;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->isCursorInsideSuggestionSpan()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2800(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->hasPasswordTransformationMethod()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2900(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->isLockscreen()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3000(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-boolean v0, p0, Landroid/widget/Editor;->isHideCliptrayOpenCueAfterDelay:Z

    #@2
    return v0
.end method

.method static synthetic access$3002(Landroid/widget/Editor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    iput-boolean p1, p0, Landroid/widget/Editor;->isHideCliptrayOpenCueAfterDelay:Z

    #@2
    return p1
.end method

.method static synthetic access$3100(Landroid/widget/Editor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->hideCursorControllers()V

    #@3
    return-void
.end method

.method static synthetic access$3200(Landroid/widget/Editor;)Landroid/widget/Editor$PositionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mPositionListener:Landroid/widget/Editor$PositionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$3300(Landroid/widget/Editor;II)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Landroid/widget/Editor;->isPositionVisible(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3400(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method static synthetic access$3402(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Landroid/widget/Editor;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    #@2
    return-object p1
.end method

.method static synthetic access$3500(Landroid/widget/Editor;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->hideInsertionPointCursorController()V

    #@3
    return-void
.end method

.method static synthetic access$3600(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method static synthetic access$3602(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Landroid/widget/Editor;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    #@2
    return-object p1
.end method

.method static synthetic access$3700(Landroid/widget/Editor;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/widget/Editor;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method static synthetic access$3702(Landroid/widget/Editor;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Landroid/widget/Editor;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    #@2
    return-object p1
.end method

.method static synthetic access$3800(Landroid/widget/Editor;FF)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Landroid/widget/Editor;->isPositionOnText(FF)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3900(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->selectCurrentWord()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-direct {p0}, Landroid/widget/Editor;->shouldBlink()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Landroid/widget/Editor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 125
    iget-boolean v0, p0, Landroid/widget/Editor;->mHasWindowFocus:Z

    #@2
    return v0
.end method

.method private canSelectText()Z
    .registers 2

    #@0
    .prologue
    .line 681
    invoke-virtual {p0}, Landroid/widget/Editor;->hasSelectionController()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method private chooseSize(Landroid/widget/PopupWindow;Ljava/lang/CharSequence;Landroid/widget/TextView;)V
    .registers 16
    .parameter "pop"
    .parameter "text"
    .parameter "tv"

    #@0
    .prologue
    .line 646
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaddingLeft()I

    #@3
    move-result v1

    #@4
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaddingRight()I

    #@7
    move-result v2

    #@8
    add-int v11, v1, v2

    #@a
    .line 647
    .local v11, wid:I
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaddingTop()I

    #@d
    move-result v1

    #@e
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaddingBottom()I

    #@11
    move-result v2

    #@12
    add-int v8, v1, v2

    #@14
    .line 649
    .local v8, ht:I
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@16
    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v1

    #@1a
    const v2, 0x105004e

    #@1d
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@20
    move-result v3

    #@21
    .line 651
    .local v3, defaultWidthInPixels:I
    new-instance v0, Landroid/text/StaticLayout;

    #@23
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    #@26
    move-result-object v2

    #@27
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@29
    const/high16 v5, 0x3f80

    #@2b
    const/4 v6, 0x0

    #@2c
    const/4 v7, 0x1

    #@2d
    move-object v1, p2

    #@2e
    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    #@31
    .line 653
    .local v0, l:Landroid/text/Layout;
    const/4 v10, 0x0

    #@32
    .line 654
    .local v10, max:F
    const/4 v9, 0x0

    #@33
    .local v9, i:I
    :goto_33
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    #@36
    move-result v1

    #@37
    if-ge v9, v1, :cond_44

    #@39
    .line 655
    invoke-virtual {v0, v9}, Landroid/text/Layout;->getLineWidth(I)F

    #@3c
    move-result v1

    #@3d
    invoke-static {v10, v1}, Ljava/lang/Math;->max(FF)F

    #@40
    move-result v10

    #@41
    .line 654
    add-int/lit8 v9, v9, 0x1

    #@43
    goto :goto_33

    #@44
    .line 662
    :cond_44
    float-to-double v1, v10

    #@45
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    #@48
    move-result-wide v1

    #@49
    double-to-int v1, v1

    #@4a
    add-int/2addr v1, v11

    #@4b
    invoke-virtual {p1, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@4e
    .line 663
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    #@51
    move-result v1

    #@52
    add-int/2addr v1, v8

    #@53
    invoke-virtual {p1, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@56
    .line 664
    return-void
.end method

.method private downgradeEasyCorrectionSpans()V
    .registers 9

    #@0
    .prologue
    .line 1207
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v4

    #@6
    .line 1208
    .local v4, text:Ljava/lang/CharSequence;
    instance-of v5, v4, Landroid/text/Spannable;

    #@8
    if-eqz v5, :cond_36

    #@a
    move-object v2, v4

    #@b
    .line 1209
    check-cast v2, Landroid/text/Spannable;

    #@d
    .line 1210
    .local v2, spannable:Landroid/text/Spannable;
    const/4 v5, 0x0

    #@e
    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    #@11
    move-result v6

    #@12
    const-class v7, Landroid/text/style/SuggestionSpan;

    #@14
    invoke-interface {v2, v5, v6, v7}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, [Landroid/text/style/SuggestionSpan;

    #@1a
    .line 1212
    .local v3, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    array-length v5, v3

    #@1c
    if-ge v1, v5, :cond_36

    #@1e
    .line 1213
    aget-object v5, v3, v1

    #@20
    invoke-virtual {v5}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@23
    move-result v0

    #@24
    .line 1214
    .local v0, flags:I
    and-int/lit8 v5, v0, 0x1

    #@26
    if-eqz v5, :cond_33

    #@28
    and-int/lit8 v5, v0, 0x2

    #@2a
    if-nez v5, :cond_33

    #@2c
    .line 1216
    and-int/lit8 v0, v0, -0x2

    #@2e
    .line 1217
    aget-object v5, v3, v1

    #@30
    invoke-virtual {v5, v0}, Landroid/text/style/SuggestionSpan;->setFlags(I)V

    #@33
    .line 1212
    :cond_33
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_1b

    #@36
    .line 1221
    .end local v0           #flags:I
    .end local v1           #i:I
    .end local v2           #spannable:Landroid/text/Spannable;
    .end local v3           #suggestionSpans:[Landroid/text/style/SuggestionSpan;
    :cond_36
    return-void
.end method

.method private drawCursor(Landroid/graphics/Canvas;I)V
    .registers 7
    .parameter "canvas"
    .parameter "cursorOffsetVertical"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1725
    if-eqz p2, :cond_19

    #@3
    const/4 v1, 0x1

    #@4
    .line 1726
    .local v1, translate:Z
    :goto_4
    if-eqz v1, :cond_a

    #@6
    int-to-float v2, p2

    #@7
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@a
    .line 1727
    :cond_a
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    iget v2, p0, Landroid/widget/Editor;->mCursorCount:I

    #@d
    if-ge v0, v2, :cond_1b

    #@f
    .line 1728
    iget-object v2, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@11
    aget-object v2, v2, v0

    #@13
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@16
    .line 1727
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_b

    #@19
    .line 1725
    .end local v0           #i:I
    .end local v1           #translate:Z
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_4

    #@1b
    .line 1730
    .restart local v0       #i:I
    .restart local v1       #translate:Z
    :cond_1b
    if-eqz v1, :cond_22

    #@1d
    neg-int v2, p2

    #@1e
    int-to-float v2, v2

    #@1f
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@22
    .line 1731
    :cond_22
    return-void
.end method

.method private drawHardwareAccelerated(Landroid/graphics/Canvas;Landroid/text/Layout;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 40
    .parameter "canvas"
    .parameter "layout"
    .parameter "highlight"
    .parameter "highlightPaint"
    .parameter "cursorOffsetVertical"

    #@0
    .prologue
    .line 1602
    move-object/from16 v0, p2

    #@2
    move-object/from16 v1, p1

    #@4
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineRangeForDraw(Landroid/graphics/Canvas;)J

    #@7
    move-result-wide v26

    #@8
    .line 1603
    .local v26, lineRange:J
    invoke-static/range {v26 .. v27}, Landroid/text/TextUtils;->unpackRangeStartFromLong(J)I

    #@b
    move-result v9

    #@c
    .line 1604
    .local v9, firstLine:I
    invoke-static/range {v26 .. v27}, Landroid/text/TextUtils;->unpackRangeEndFromLong(J)I

    #@f
    move-result v10

    #@10
    .line 1605
    .local v10, lastLine:I
    if-gez v10, :cond_13

    #@12
    .line 1699
    :cond_12
    :goto_12
    return-void

    #@13
    :cond_13
    move-object/from16 v4, p2

    #@15
    move-object/from16 v5, p1

    #@17
    move-object/from16 v6, p3

    #@19
    move-object/from16 v7, p4

    #@1b
    move/from16 v8, p5

    #@1d
    .line 1607
    invoke-virtual/range {v4 .. v10}, Landroid/text/Layout;->drawBackground(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;III)V

    #@20
    .line 1610
    move-object/from16 v0, p2

    #@22
    instance-of v4, v0, Landroid/text/DynamicLayout;

    #@24
    if-eqz v4, :cond_17a

    #@26
    .line 1611
    move-object/from16 v0, p0

    #@28
    iget-object v4, v0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@2a
    if-nez v4, :cond_37

    #@2c
    .line 1612
    const/4 v4, 0x0

    #@2d
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->idealObjectArraySize(I)I

    #@30
    move-result v4

    #@31
    new-array v4, v4, [Landroid/view/DisplayList;

    #@33
    move-object/from16 v0, p0

    #@35
    iput-object v4, v0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@37
    .line 1619
    :cond_37
    invoke-virtual/range {p2 .. p2}, Landroid/text/Layout;->getHeight()I

    #@3a
    move-result v23

    #@3b
    .line 1620
    .local v23, layoutHeight:I
    move-object/from16 v0, p0

    #@3d
    iget v4, v0, Landroid/widget/Editor;->mLastLayoutHeight:I

    #@3f
    move/from16 v0, v23

    #@41
    if-eq v4, v0, :cond_4c

    #@43
    .line 1621
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@46
    .line 1622
    move/from16 v0, v23

    #@48
    move-object/from16 v1, p0

    #@4a
    iput v0, v1, Landroid/widget/Editor;->mLastLayoutHeight:I

    #@4c
    :cond_4c
    move-object/from16 v19, p2

    #@4e
    .line 1625
    check-cast v19, Landroid/text/DynamicLayout;

    #@50
    .line 1626
    .local v19, dynamicLayout:Landroid/text/DynamicLayout;
    invoke-virtual/range {v19 .. v19}, Landroid/text/DynamicLayout;->getBlockEndLines()[I

    #@53
    move-result-object v14

    #@54
    .line 1627
    .local v14, blockEndLines:[I
    invoke-virtual/range {v19 .. v19}, Landroid/text/DynamicLayout;->getBlockIndices()[I

    #@57
    move-result-object v16

    #@58
    .line 1628
    .local v16, blockIndices:[I
    invoke-virtual/range {v19 .. v19}, Landroid/text/DynamicLayout;->getNumberOfBlocks()I

    #@5b
    move-result v30

    #@5c
    .line 1630
    .local v30, numberOfBlocks:I
    const/16 v20, -0x1

    #@5e
    .line 1631
    .local v20, endOfPreviousBlock:I
    const/16 v32, 0x0

    #@60
    .line 1632
    .local v32, searchStartIndex:I
    const/16 v22, 0x0

    #@62
    .local v22, i:I
    :goto_62
    move/from16 v0, v22

    #@64
    move/from16 v1, v30

    #@66
    if-ge v0, v1, :cond_12

    #@68
    .line 1633
    aget v13, v14, v22

    #@6a
    .line 1634
    .local v13, blockEndLine:I
    aget v15, v16, v22

    #@6c
    .line 1636
    .local v15, blockIndex:I
    const/4 v4, -0x1

    #@6d
    if-ne v15, v4, :cond_102

    #@6f
    const/16 v17, 0x1

    #@71
    .line 1637
    .local v17, blockIsInvalid:Z
    :goto_71
    if-eqz v17, :cond_83

    #@73
    .line 1638
    move-object/from16 v0, p0

    #@75
    move-object/from16 v1, v16

    #@77
    move/from16 v2, v30

    #@79
    move/from16 v3, v32

    #@7b
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/Editor;->getAvailableDisplayListIndex([III)I

    #@7e
    move-result v15

    #@7f
    .line 1641
    aput v15, v16, v22

    #@81
    .line 1642
    add-int/lit8 v32, v15, 0x1

    #@83
    .line 1645
    :cond_83
    move-object/from16 v0, p0

    #@85
    iget-object v4, v0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@87
    aget-object v12, v4, v15

    #@89
    .line 1646
    .local v12, blockDisplayList:Landroid/view/DisplayList;
    if-nez v12, :cond_106

    #@8b
    .line 1647
    move-object/from16 v0, p0

    #@8d
    iget-object v4, v0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@8f
    move-object/from16 v0, p0

    #@91
    iget-object v5, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@93
    invoke-virtual {v5}, Landroid/widget/TextView;->getHardwareRenderer()Landroid/view/HardwareRenderer;

    #@96
    move-result-object v5

    #@97
    new-instance v6, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v7, "Text "

    #@9e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v6

    #@a6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v5, v6}, Landroid/view/HardwareRenderer;->createDisplayList(Ljava/lang/String;)Landroid/view/DisplayList;

    #@ad
    move-result-object v12

    #@ae
    .end local v12           #blockDisplayList:Landroid/view/DisplayList;
    aput-object v12, v4, v15

    #@b0
    .line 1653
    .restart local v12       #blockDisplayList:Landroid/view/DisplayList;
    :cond_b0
    :goto_b0
    invoke-virtual {v12}, Landroid/view/DisplayList;->isValid()Z

    #@b3
    move-result v4

    #@b4
    if-nez v4, :cond_154

    #@b6
    .line 1654
    add-int/lit8 v11, v20, 0x1

    #@b8
    .line 1655
    .local v11, blockBeginLine:I
    move-object/from16 v0, p2

    #@ba
    invoke-virtual {v0, v11}, Landroid/text/Layout;->getLineTop(I)I

    #@bd
    move-result v33

    #@be
    .line 1656
    .local v33, top:I
    move-object/from16 v0, p2

    #@c0
    invoke-virtual {v0, v13}, Landroid/text/Layout;->getLineBottom(I)I

    #@c3
    move-result v18

    #@c4
    .line 1657
    .local v18, bottom:I
    const/16 v24, 0x0

    #@c6
    .line 1658
    .local v24, left:I
    move-object/from16 v0, p0

    #@c8
    iget-object v4, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@ca
    invoke-virtual {v4}, Landroid/widget/TextView;->getWidth()I

    #@cd
    move-result v31

    #@ce
    .line 1659
    .local v31, right:I
    move-object/from16 v0, p0

    #@d0
    iget-object v4, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@d2
    invoke-virtual {v4}, Landroid/widget/TextView;->getHorizontallyScrolling()Z

    #@d5
    move-result v4

    #@d6
    if-eqz v4, :cond_118

    #@d8
    .line 1660
    const v29, 0x7f7fffff

    #@db
    .line 1661
    .local v29, min:F
    const/16 v28, 0x1

    #@dd
    .line 1662
    .local v28, max:F
    move/from16 v25, v11

    #@df
    .local v25, line:I
    :goto_df
    move/from16 v0, v25

    #@e1
    if-gt v0, v13, :cond_10c

    #@e3
    .line 1663
    move-object/from16 v0, p2

    #@e5
    move/from16 v1, v25

    #@e7
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineLeft(I)F

    #@ea
    move-result v4

    #@eb
    move/from16 v0, v29

    #@ed
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    #@f0
    move-result v29

    #@f1
    .line 1664
    move-object/from16 v0, p2

    #@f3
    move/from16 v1, v25

    #@f5
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineRight(I)F

    #@f8
    move-result v4

    #@f9
    move/from16 v0, v28

    #@fb
    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    #@fe
    move-result v28

    #@ff
    .line 1662
    add-int/lit8 v25, v25, 0x1

    #@101
    goto :goto_df

    #@102
    .line 1636
    .end local v11           #blockBeginLine:I
    .end local v12           #blockDisplayList:Landroid/view/DisplayList;
    .end local v17           #blockIsInvalid:Z
    .end local v18           #bottom:I
    .end local v24           #left:I
    .end local v25           #line:I
    .end local v28           #max:F
    .end local v29           #min:F
    .end local v31           #right:I
    .end local v33           #top:I
    :cond_102
    const/16 v17, 0x0

    #@104
    goto/16 :goto_71

    #@106
    .line 1650
    .restart local v12       #blockDisplayList:Landroid/view/DisplayList;
    .restart local v17       #blockIsInvalid:Z
    :cond_106
    if-eqz v17, :cond_b0

    #@108
    invoke-virtual {v12}, Landroid/view/DisplayList;->invalidate()V

    #@10b
    goto :goto_b0

    #@10c
    .line 1666
    .restart local v11       #blockBeginLine:I
    .restart local v18       #bottom:I
    .restart local v24       #left:I
    .restart local v25       #line:I
    .restart local v28       #max:F
    .restart local v29       #min:F
    .restart local v31       #right:I
    .restart local v33       #top:I
    :cond_10c
    move/from16 v0, v29

    #@10e
    float-to-int v0, v0

    #@10f
    move/from16 v24, v0

    #@111
    .line 1667
    const/high16 v4, 0x3f00

    #@113
    add-float v4, v4, v28

    #@115
    float-to-int v0, v4

    #@116
    move/from16 v31, v0

    #@118
    .line 1670
    .end local v25           #line:I
    .end local v28           #max:F
    .end local v29           #min:F
    :cond_118
    invoke-virtual {v12}, Landroid/view/DisplayList;->start()Landroid/view/HardwareCanvas;

    #@11b
    move-result-object v21

    #@11c
    .line 1673
    .local v21, hardwareCanvas:Landroid/view/HardwareCanvas;
    sub-int v4, v31, v24

    #@11e
    sub-int v5, v18, v33

    #@120
    :try_start_120
    move-object/from16 v0, v21

    #@122
    invoke-virtual {v0, v4, v5}, Landroid/view/HardwareCanvas;->setViewport(II)V

    #@125
    .line 1675
    const/4 v4, 0x0

    #@126
    move-object/from16 v0, v21

    #@128
    invoke-virtual {v0, v4}, Landroid/view/HardwareCanvas;->onPreDraw(Landroid/graphics/Rect;)I

    #@12b
    .line 1678
    move/from16 v0, v24

    #@12d
    neg-int v4, v0

    #@12e
    int-to-float v4, v4

    #@12f
    move/from16 v0, v33

    #@131
    neg-int v5, v0

    #@132
    int-to-float v5, v5

    #@133
    move-object/from16 v0, v21

    #@135
    invoke-virtual {v0, v4, v5}, Landroid/view/HardwareCanvas;->translate(FF)V

    #@138
    .line 1679
    move-object/from16 v0, p2

    #@13a
    move-object/from16 v1, v21

    #@13c
    invoke-virtual {v0, v1, v11, v13}, Landroid/text/Layout;->drawText(Landroid/graphics/Canvas;II)V
    :try_end_13f
    .catchall {:try_start_120 .. :try_end_13f} :catchall_163

    #@13f
    .line 1682
    invoke-virtual/range {v21 .. v21}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@142
    .line 1683
    invoke-virtual {v12}, Landroid/view/DisplayList;->end()V

    #@145
    .line 1684
    move/from16 v0, v24

    #@147
    move/from16 v1, v33

    #@149
    move/from16 v2, v31

    #@14b
    move/from16 v3, v18

    #@14d
    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    #@150
    .line 1686
    const/4 v4, 0x0

    #@151
    invoke-virtual {v12, v4}, Landroid/view/DisplayList;->setClipChildren(Z)V

    #@154
    .end local v11           #blockBeginLine:I
    .end local v18           #bottom:I
    .end local v21           #hardwareCanvas:Landroid/view/HardwareCanvas;
    .end local v24           #left:I
    .end local v31           #right:I
    .end local v33           #top:I
    :cond_154
    move-object/from16 v4, p1

    #@156
    .line 1690
    check-cast v4, Landroid/view/HardwareCanvas;

    #@158
    const/4 v5, 0x0

    #@159
    const/4 v6, 0x0

    #@15a
    invoke-virtual {v4, v12, v5, v6}, Landroid/view/HardwareCanvas;->drawDisplayList(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I

    #@15d
    .line 1693
    move/from16 v20, v13

    #@15f
    .line 1632
    add-int/lit8 v22, v22, 0x1

    #@161
    goto/16 :goto_62

    #@163
    .line 1682
    .restart local v11       #blockBeginLine:I
    .restart local v18       #bottom:I
    .restart local v21       #hardwareCanvas:Landroid/view/HardwareCanvas;
    .restart local v24       #left:I
    .restart local v31       #right:I
    .restart local v33       #top:I
    :catchall_163
    move-exception v4

    #@164
    invoke-virtual/range {v21 .. v21}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@167
    .line 1683
    invoke-virtual {v12}, Landroid/view/DisplayList;->end()V

    #@16a
    .line 1684
    move/from16 v0, v24

    #@16c
    move/from16 v1, v33

    #@16e
    move/from16 v2, v31

    #@170
    move/from16 v3, v18

    #@172
    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    #@175
    .line 1686
    const/4 v5, 0x0

    #@176
    invoke-virtual {v12, v5}, Landroid/view/DisplayList;->setClipChildren(Z)V

    #@179
    .line 1682
    throw v4

    #@17a
    .line 1697
    .end local v11           #blockBeginLine:I
    .end local v12           #blockDisplayList:Landroid/view/DisplayList;
    .end local v13           #blockEndLine:I
    .end local v14           #blockEndLines:[I
    .end local v15           #blockIndex:I
    .end local v16           #blockIndices:[I
    .end local v17           #blockIsInvalid:Z
    .end local v18           #bottom:I
    .end local v19           #dynamicLayout:Landroid/text/DynamicLayout;
    .end local v20           #endOfPreviousBlock:I
    .end local v21           #hardwareCanvas:Landroid/view/HardwareCanvas;
    .end local v22           #i:I
    .end local v23           #layoutHeight:I
    .end local v24           #left:I
    .end local v30           #numberOfBlocks:I
    .end local v31           #right:I
    .end local v32           #searchStartIndex:I
    .end local v33           #top:I
    :cond_17a
    move-object/from16 v0, p2

    #@17c
    move-object/from16 v1, p1

    #@17e
    invoke-virtual {v0, v1, v9, v10}, Landroid/text/Layout;->drawText(Landroid/graphics/Canvas;II)V

    #@181
    goto/16 :goto_12
.end method

.method private extractTextInternal(Landroid/view/inputmethod/ExtractedTextRequest;IIILandroid/view/inputmethod/ExtractedText;)Z
    .registers 14
    .parameter "request"
    .parameter "partialStartOffset"
    .parameter "partialEndOffset"
    .parameter "delta"
    .parameter "outText"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1403
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@6
    move-result-object v1

    #@7
    .line 1404
    .local v1, content:Ljava/lang/CharSequence;
    if-eqz v1, :cond_8a

    #@9
    .line 1405
    const/4 v7, -0x2

    #@a
    if-eq p2, v7, :cond_d0

    #@c
    .line 1406
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@f
    move-result v0

    #@10
    .line 1407
    .local v0, N:I
    if-gez p2, :cond_8b

    #@12
    .line 1408
    const/4 v7, -0x1

    #@13
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    #@15
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    #@17
    .line 1409
    const/4 p2, 0x0

    #@18
    .line 1410
    move p3, v0

    #@19
    .line 1443
    :cond_19
    :goto_19
    iget v7, p1, Landroid/view/inputmethod/ExtractedTextRequest;->flags:I

    #@1b
    and-int/lit8 v7, v7, 0x1

    #@1d
    if-eqz v7, :cond_c8

    #@1f
    .line 1444
    invoke-interface {v1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@22
    move-result-object v7

    #@23
    iput-object v7, p5, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@25
    .line 1455
    .end local v0           #N:I
    :goto_25
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@27
    .line 1456
    const/16 v7, 0x800

    #@29
    invoke-static {v1, v7}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@2c
    move-result v7

    #@2d
    if-eqz v7, :cond_35

    #@2f
    .line 1457
    iget v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@31
    or-int/lit8 v7, v7, 0x2

    #@33
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@35
    .line 1459
    :cond_35
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@37
    invoke-virtual {v7}, Landroid/widget/TextView;->isSingleLine()Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_43

    #@3d
    .line 1460
    iget v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@3f
    or-int/lit8 v7, v7, 0x1

    #@41
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@43
    .line 1463
    :cond_43
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@45
    if-eqz v7, :cond_77

    #@47
    .line 1464
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@49
    iget-boolean v7, v7, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@4b
    if-nez v7, :cond_53

    #@4d
    .line 1465
    iget v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@4f
    or-int/lit8 v7, v7, 0x4

    #@51
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@53
    .line 1467
    :cond_53
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@55
    iget-boolean v7, v7, Landroid/widget/TextView;->mCanPasteBubblePopup:Z

    #@57
    if-nez v7, :cond_5f

    #@59
    .line 1468
    iget v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@5b
    or-int/lit8 v7, v7, 0x8

    #@5d
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@5f
    .line 1470
    :cond_5f
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@61
    iget-boolean v7, v7, Landroid/widget/TextView;->mCanCutBubblePopup:Z

    #@63
    if-nez v7, :cond_6b

    #@65
    .line 1471
    iget v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@67
    or-int/lit8 v7, v7, 0x10

    #@69
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@6b
    .line 1473
    :cond_6b
    invoke-virtual {p0}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@6e
    move-result v7

    #@6f
    if-eqz v7, :cond_77

    #@71
    .line 1474
    iget v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@73
    or-int/lit8 v7, v7, 0x20

    #@75
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@77
    .line 1478
    :cond_77
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    #@79
    .line 1479
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@7b
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@7e
    move-result v6

    #@7f
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    #@81
    .line 1480
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@83
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionEnd()I

    #@86
    move-result v6

    #@87
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    #@89
    .line 1481
    const/4 v6, 0x1

    #@8a
    .line 1483
    :cond_8a
    return v6

    #@8b
    .line 1414
    .restart local v0       #N:I
    :cond_8b
    add-int/2addr p3, p4

    #@8c
    .line 1416
    instance-of v7, v1, Landroid/text/Spanned;

    #@8e
    if-eqz v7, :cond_b1

    #@90
    move-object v4, v1

    #@91
    .line 1417
    check-cast v4, Landroid/text/Spanned;

    #@93
    .line 1418
    .local v4, spanned:Landroid/text/Spanned;
    const-class v7, Landroid/text/ParcelableSpan;

    #@95
    invoke-interface {v4, p2, p3, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@98
    move-result-object v5

    #@99
    .line 1420
    .local v5, spans:[Ljava/lang/Object;
    array-length v2, v5

    #@9a
    .line 1421
    .local v2, i:I
    :cond_9a
    :goto_9a
    if-lez v2, :cond_b1

    #@9c
    .line 1422
    add-int/lit8 v2, v2, -0x1

    #@9e
    .line 1423
    aget-object v7, v5, v2

    #@a0
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@a3
    move-result v3

    #@a4
    .line 1424
    .local v3, j:I
    if-ge v3, p2, :cond_a7

    #@a6
    move p2, v3

    #@a7
    .line 1425
    :cond_a7
    aget-object v7, v5, v2

    #@a9
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@ac
    move-result v3

    #@ad
    .line 1426
    if-le v3, p3, :cond_9a

    #@af
    move p3, v3

    #@b0
    goto :goto_9a

    #@b1
    .line 1429
    .end local v2           #i:I
    .end local v3           #j:I
    .end local v4           #spanned:Landroid/text/Spanned;
    .end local v5           #spans:[Ljava/lang/Object;
    :cond_b1
    iput p2, p5, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    #@b3
    .line 1430
    sub-int v7, p3, p4

    #@b5
    iput v7, p5, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    #@b7
    .line 1432
    if-le p2, v0, :cond_bf

    #@b9
    .line 1433
    move p2, v0

    #@ba
    .line 1437
    :cond_ba
    :goto_ba
    if-le p3, v0, :cond_c3

    #@bc
    .line 1438
    move p3, v0

    #@bd
    goto/16 :goto_19

    #@bf
    .line 1434
    :cond_bf
    if-gez p2, :cond_ba

    #@c1
    .line 1435
    const/4 p2, 0x0

    #@c2
    goto :goto_ba

    #@c3
    .line 1439
    :cond_c3
    if-gez p3, :cond_19

    #@c5
    .line 1440
    const/4 p3, 0x0

    #@c6
    goto/16 :goto_19

    #@c8
    .line 1447
    :cond_c8
    invoke-static {v1, p2, p3}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    #@cb
    move-result-object v7

    #@cc
    iput-object v7, p5, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@ce
    goto/16 :goto_25

    #@d0
    .line 1451
    .end local v0           #N:I
    :cond_d0
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    #@d2
    .line 1452
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    #@d4
    .line 1453
    const-string v7, ""

    #@d6
    iput-object v7, p5, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@d8
    goto/16 :goto_25
.end method

.method private extractedTextModeWillBeStarted()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1879
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    instance-of v2, v2, Landroid/inputmethodservice/ExtractEditText;

    #@5
    if-nez v2, :cond_14

    #@7
    .line 1880
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@a
    move-result-object v0

    #@b
    .line 1881
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_14

    #@d
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_14

    #@13
    const/4 v1, 0x1

    #@14
    .line 1883
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_14
    return v1
.end method

.method private getAvailableDisplayListIndex([III)I
    .registers 12
    .parameter "blockIndices"
    .parameter "numberOfBlocks"
    .parameter "searchStartIndex"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1703
    iget-object v6, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@3
    array-length v4, v6

    #@4
    .line 1704
    .local v4, length:I
    move v2, p3

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v4, :cond_18

    #@7
    .line 1705
    const/4 v0, 0x0

    #@8
    .line 1706
    .local v0, blockIndexFound:Z
    const/4 v3, 0x0

    #@9
    .local v3, j:I
    :goto_9
    if-ge v3, p2, :cond_10

    #@b
    .line 1707
    aget v6, p1, v3

    #@d
    if-ne v6, v2, :cond_15

    #@f
    .line 1708
    const/4 v0, 0x1

    #@10
    .line 1712
    :cond_10
    if-eqz v0, :cond_28

    #@12
    .line 1704
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_5

    #@15
    .line 1706
    :cond_15
    add-int/lit8 v3, v3, 0x1

    #@17
    goto :goto_9

    #@18
    .line 1717
    .end local v0           #blockIndexFound:Z
    .end local v3           #j:I
    :cond_18
    add-int/lit8 v6, v4, 0x1

    #@1a
    invoke-static {v6}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@1d
    move-result v5

    #@1e
    .line 1718
    .local v5, newSize:I
    new-array v1, v5, [Landroid/view/DisplayList;

    #@20
    .line 1719
    .local v1, displayLists:[Landroid/view/DisplayList;
    iget-object v6, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@22
    invoke-static {v6, v7, v1, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@25
    .line 1720
    iput-object v1, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@27
    move v2, v4

    #@28
    .line 1721
    .end local v1           #displayLists:[Landroid/view/DisplayList;
    .end local v2           #i:I
    .end local v5           #newSize:I
    :cond_28
    return v2
.end method

.method private getCharRange(I)J
    .registers 9
    .parameter "offset"

    #@0
    .prologue
    .line 801
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v5

    #@6
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v4

    #@a
    .line 802
    .local v4, textLength:I
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@c
    if-eqz v5, :cond_7b

    #@e
    .line 803
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@10
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@13
    move-result-object v5

    #@14
    invoke-static {v5, p1}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_21

    #@1a
    .line 804
    add-int/lit8 v5, p1, 0x4

    #@1c
    invoke-static {p1, v5}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@1f
    move-result-wide v5

    #@20
    .line 840
    :goto_20
    return-wide v5

    #@21
    .line 805
    :cond_21
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@23
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@26
    move-result-object v5

    #@27
    add-int/lit8 v6, p1, -0x1

    #@29
    invoke-static {v5, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_38

    #@2f
    .line 806
    add-int/lit8 v5, p1, -0x1

    #@31
    add-int/lit8 v6, p1, 0x3

    #@33
    invoke-static {v5, v6}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@36
    move-result-wide v5

    #@37
    goto :goto_20

    #@38
    .line 807
    :cond_38
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3a
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@3d
    move-result-object v5

    #@3e
    add-int/lit8 v6, p1, -0x2

    #@40
    invoke-static {v5, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@43
    move-result v5

    #@44
    if-eqz v5, :cond_4f

    #@46
    .line 808
    add-int/lit8 v5, p1, -0x2

    #@48
    add-int/lit8 v6, p1, 0x2

    #@4a
    invoke-static {v5, v6}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@4d
    move-result-wide v5

    #@4e
    goto :goto_20

    #@4f
    .line 809
    :cond_4f
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@51
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@54
    move-result-object v5

    #@55
    add-int/lit8 v6, p1, -0x3

    #@57
    invoke-static {v5, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@5a
    move-result v5

    #@5b
    if-eqz v5, :cond_66

    #@5d
    .line 810
    add-int/lit8 v5, p1, -0x3

    #@5f
    add-int/lit8 v6, p1, 0x1

    #@61
    invoke-static {v5, v6}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@64
    move-result-wide v5

    #@65
    goto :goto_20

    #@66
    .line 811
    :cond_66
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@68
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@6b
    move-result-object v5

    #@6c
    add-int/lit8 v6, p1, -0x4

    #@6e
    invoke-static {v5, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@71
    move-result v5

    #@72
    if-eqz v5, :cond_7b

    #@74
    .line 812
    add-int/lit8 v5, p1, -0x4

    #@76
    invoke-static {v5, p1}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@79
    move-result-wide v5

    #@7a
    goto :goto_20

    #@7b
    .line 815
    :cond_7b
    add-int/lit8 v5, p1, 0x1

    #@7d
    if-ge v5, v4, :cond_b3

    #@7f
    .line 816
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@81
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@84
    move-result-object v5

    #@85
    invoke-interface {v5, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@88
    move-result v0

    #@89
    .line 817
    .local v0, currentChar:C
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8b
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@8e
    move-result-object v5

    #@8f
    add-int/lit8 v6, p1, 0x1

    #@91
    invoke-interface {v5, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@94
    move-result v1

    #@95
    .line 818
    .local v1, nextChar:C
    invoke-static {v0, v1}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@98
    move-result v5

    #@99
    if-nez v5, :cond_ab

    #@9b
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@9d
    if-eqz v5, :cond_b3

    #@9f
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a1
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@a4
    move-result-object v5

    #@a5
    invoke-static {v5, p1}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@a8
    move-result v5

    #@a9
    if-eqz v5, :cond_b3

    #@ab
    .line 820
    :cond_ab
    add-int/lit8 v5, p1, 0x2

    #@ad
    invoke-static {p1, v5}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@b0
    move-result-wide v5

    #@b1
    goto/16 :goto_20

    #@b3
    .line 823
    .end local v0           #currentChar:C
    .end local v1           #nextChar:C
    :cond_b3
    if-ge p1, v4, :cond_bd

    #@b5
    .line 824
    add-int/lit8 v5, p1, 0x1

    #@b7
    invoke-static {p1, v5}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@ba
    move-result-wide v5

    #@bb
    goto/16 :goto_20

    #@bd
    .line 826
    :cond_bd
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@bf
    if-eqz v5, :cond_d7

    #@c1
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@c3
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@c6
    move-result-object v5

    #@c7
    add-int/lit8 v6, p1, -0x4

    #@c9
    invoke-static {v5, v6}, Landroid/text/Layout;->isInCountryCodeTable(Ljava/lang/CharSequence;I)Z

    #@cc
    move-result v5

    #@cd
    if-eqz v5, :cond_d7

    #@cf
    .line 827
    add-int/lit8 v5, p1, -0x4

    #@d1
    invoke-static {v5, p1}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@d4
    move-result-wide v5

    #@d5
    goto/16 :goto_20

    #@d7
    .line 829
    :cond_d7
    add-int/lit8 v5, p1, -0x2

    #@d9
    if-ltz v5, :cond_113

    #@db
    .line 830
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@dd
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@e0
    move-result-object v5

    #@e1
    add-int/lit8 v6, p1, -0x1

    #@e3
    invoke-interface {v5, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@e6
    move-result v2

    #@e7
    .line 831
    .local v2, previousChar:C
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@e9
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@ec
    move-result-object v5

    #@ed
    add-int/lit8 v6, p1, -0x2

    #@ef
    invoke-interface {v5, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@f2
    move-result v3

    #@f3
    .line 832
    .local v3, previousPreviousChar:C
    invoke-static {v3, v2}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@f6
    move-result v5

    #@f7
    if-nez v5, :cond_10b

    #@f9
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@fb
    if-eqz v5, :cond_113

    #@fd
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@ff
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@102
    move-result-object v5

    #@103
    add-int/lit8 v6, p1, -0x2

    #@105
    invoke-static {v5, v6}, Landroid/text/Layout;->isDiacriticalMark(Ljava/lang/CharSequence;I)Z

    #@108
    move-result v5

    #@109
    if-eqz v5, :cond_113

    #@10b
    .line 834
    :cond_10b
    add-int/lit8 v5, p1, -0x2

    #@10d
    invoke-static {v5, p1}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@110
    move-result-wide v5

    #@111
    goto/16 :goto_20

    #@113
    .line 837
    .end local v2           #previousChar:C
    .end local v3           #previousPreviousChar:C
    :cond_113
    add-int/lit8 v5, p1, -0x1

    #@115
    if-ltz v5, :cond_11f

    #@117
    .line 838
    add-int/lit8 v5, p1, -0x1

    #@119
    invoke-static {v5, p1}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@11c
    move-result-wide v5

    #@11d
    goto/16 :goto_20

    #@11f
    .line 840
    :cond_11f
    invoke-static {p1, p1}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@122
    move-result-wide v5

    #@123
    goto/16 :goto_20
.end method

.method private getErrorX()I
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/high16 v8, 0x41c8

    #@3
    const/high16 v7, 0x3f00

    #@5
    .line 442
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@7
    invoke-virtual {v6}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v6

    #@b
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@e
    move-result-object v6

    #@f
    iget v4, v6, Landroid/util/DisplayMetrics;->density:F

    #@11
    .line 444
    .local v4, scale:F
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@13
    iget-object v0, v6, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@15
    .line 446
    .local v0, dr:Landroid/widget/TextView$Drawables;
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@17
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutDirection()I

    #@1a
    move-result v2

    #@1b
    .line 449
    .local v2, layoutDirection:I
    packed-switch v2, :pswitch_data_58

    #@1e
    .line 452
    if-eqz v0, :cond_22

    #@20
    iget v5, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeRight:I

    #@22
    :cond_22
    neg-int v5, v5

    #@23
    div-int/lit8 v5, v5, 0x2

    #@25
    mul-float v6, v8, v4

    #@27
    add-float/2addr v6, v7

    #@28
    float-to-int v6, v6

    #@29
    add-int v3, v5, v6

    #@2b
    .line 453
    .local v3, offset:I
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2d
    invoke-virtual {v5}, Landroid/widget/TextView;->getWidth()I

    #@30
    move-result v5

    #@31
    iget-object v6, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@33
    invoke-virtual {v6}, Landroid/widget/Editor$ErrorPopup;->getWidth()I

    #@36
    move-result v6

    #@37
    sub-int/2addr v5, v6

    #@38
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3a
    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    #@3d
    move-result v6

    #@3e
    sub-int/2addr v5, v6

    #@3f
    add-int v1, v5, v3

    #@41
    .line 461
    .local v1, errorX:I
    :goto_41
    return v1

    #@42
    .line 457
    .end local v1           #errorX:I
    .end local v3           #offset:I
    :pswitch_42
    if-eqz v0, :cond_46

    #@44
    iget v5, v0, Landroid/widget/TextView$Drawables;->mDrawableSizeLeft:I

    #@46
    :cond_46
    div-int/lit8 v5, v5, 0x2

    #@48
    mul-float v6, v8, v4

    #@4a
    add-float/2addr v6, v7

    #@4b
    float-to-int v6, v6

    #@4c
    sub-int v3, v5, v6

    #@4e
    .line 458
    .restart local v3       #offset:I
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@50
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingLeft()I

    #@53
    move-result v5

    #@54
    add-int v1, v5, v3

    #@56
    .restart local v1       #errorX:I
    goto :goto_41

    #@57
    .line 449
    nop

    #@58
    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_42
    .end packed-switch
.end method

.method private getErrorY()I
    .registers 11

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 473
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v7}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    #@6
    move-result v0

    #@7
    .line 474
    .local v0, compoundPaddingTop:I
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@9
    invoke-virtual {v7}, Landroid/widget/TextView;->getBottom()I

    #@c
    move-result v7

    #@d
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@f
    invoke-virtual {v8}, Landroid/widget/TextView;->getTop()I

    #@12
    move-result v8

    #@13
    sub-int/2addr v7, v8

    #@14
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@16
    invoke-virtual {v8}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    #@19
    move-result v8

    #@1a
    sub-int/2addr v7, v8

    #@1b
    sub-int v6, v7, v0

    #@1d
    .line 477
    .local v6, vspace:I
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1f
    iget-object v1, v7, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@21
    .line 479
    .local v1, dr:Landroid/widget/TextView$Drawables;
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@23
    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutDirection()I

    #@26
    move-result v4

    #@27
    .line 481
    .local v4, layoutDirection:I
    packed-switch v4, :pswitch_data_58

    #@2a
    .line 484
    if-eqz v1, :cond_2e

    #@2c
    iget v2, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightRight:I

    #@2e
    .line 491
    .local v2, height:I
    :cond_2e
    :goto_2e
    sub-int v7, v6, v2

    #@30
    div-int/lit8 v7, v7, 0x2

    #@32
    add-int v3, v0, v7

    #@34
    .line 497
    .local v3, icontop:I
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@36
    invoke-virtual {v7}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@3d
    move-result-object v7

    #@3e
    iget v5, v7, Landroid/util/DisplayMetrics;->density:F

    #@40
    .line 498
    .local v5, scale:F
    add-int v7, v3, v2

    #@42
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@44
    invoke-virtual {v8}, Landroid/widget/TextView;->getHeight()I

    #@47
    move-result v8

    #@48
    sub-int/2addr v7, v8

    #@49
    const/high16 v8, 0x4000

    #@4b
    mul-float/2addr v8, v5

    #@4c
    const/high16 v9, 0x3f00

    #@4e
    add-float/2addr v8, v9

    #@4f
    float-to-int v8, v8

    #@50
    sub-int/2addr v7, v8

    #@51
    return v7

    #@52
    .line 487
    .end local v2           #height:I
    .end local v3           #icontop:I
    .end local v5           #scale:F
    :pswitch_52
    if-eqz v1, :cond_56

    #@54
    iget v2, v1, Landroid/widget/TextView$Drawables;->mDrawableHeightLeft:I

    #@56
    .restart local v2       #height:I
    :cond_56
    goto :goto_2e

    #@57
    .line 481
    nop

    #@58
    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_52
    .end packed-switch
.end method

.method private getLastTapPosition()I
    .registers 3

    #@0
    .prologue
    .line 1234
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@2
    if-eqz v1, :cond_23

    #@4
    .line 1235
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@6
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionModifierCursorController;->getMinTouchOffset()I

    #@9
    move-result v0

    #@a
    .line 1236
    .local v0, lastTapPosition:I
    if-ltz v0, :cond_23

    #@c
    .line 1238
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@e
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@15
    move-result v1

    #@16
    if-le v0, v1, :cond_22

    #@18
    .line 1239
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1a
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@21
    move-result v0

    #@22
    .line 1245
    .end local v0           #lastTapPosition:I
    :cond_22
    :goto_22
    return v0

    #@23
    :cond_23
    const/4 v0, -0x1

    #@24
    goto :goto_22
.end method

.method private getLastTouchOffsets()J
    .registers 6

    #@0
    .prologue
    .line 1062
    invoke-virtual {p0}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@3
    move-result-object v2

    #@4
    .line 1063
    .local v2, selectionController:Landroid/widget/Editor$SelectionModifierCursorController;
    invoke-virtual {v2}, Landroid/widget/Editor$SelectionModifierCursorController;->getMinTouchOffset()I

    #@7
    move-result v1

    #@8
    .line 1064
    .local v1, minOffset:I
    invoke-virtual {v2}, Landroid/widget/Editor$SelectionModifierCursorController;->getMaxTouchOffset()I

    #@b
    move-result v0

    #@c
    .line 1065
    .local v0, maxOffset:I
    invoke-static {v1, v0}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@f
    move-result-wide v3

    #@10
    return-wide v3
.end method

.method private getPositionListener()Landroid/widget/Editor$PositionListener;
    .registers 3

    #@0
    .prologue
    .line 871
    iget-object v0, p0, Landroid/widget/Editor;->mPositionListener:Landroid/widget/Editor$PositionListener;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 872
    new-instance v0, Landroid/widget/Editor$PositionListener;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Landroid/widget/Editor$PositionListener;-><init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V

    #@a
    iput-object v0, p0, Landroid/widget/Editor;->mPositionListener:Landroid/widget/Editor$PositionListener;

    #@c
    .line 874
    :cond_c
    iget-object v0, p0, Landroid/widget/Editor;->mPositionListener:Landroid/widget/Editor$PositionListener;

    #@e
    return-object v0
.end method

.method private getPrimaryHorizontal(Landroid/text/Layout;Landroid/text/Layout;I)F
    .registers 5
    .parameter "layout"
    .parameter "hintLayout"
    .parameter "offset"

    #@0
    .prologue
    .line 1802
    invoke-virtual {p1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1b

    #@a
    if-eqz p2, :cond_1b

    #@c
    invoke-virtual {p2}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    #@f
    move-result-object v0

    #@10
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1b

    #@16
    .line 1805
    invoke-virtual {p2, p3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@19
    move-result v0

    #@1a
    .line 1807
    :goto_1a
    return v0

    #@1b
    :cond_1b
    invoke-virtual {p1, p3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@1e
    move-result v0

    #@1f
    goto :goto_1a
.end method

.method private getTextThumbnailBuilder(Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;
    .registers 9
    .parameter "text"

    #@0
    .prologue
    const/4 v6, -0x2

    #@1
    const/4 v5, 0x0

    #@2
    .line 2141
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@4
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@7
    move-result-object v2

    #@8
    const v3, 0x10900d6

    #@b
    const/4 v4, 0x0

    #@c
    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/widget/TextView;

    #@12
    .line 2144
    .local v0, shadowView:Landroid/widget/TextView;
    if-nez v0, :cond_1c

    #@14
    .line 2145
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v3, "Unable to inflate text drag thumbnail"

    #@18
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v2

    #@1c
    .line 2148
    :cond_1c
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@1f
    move-result v2

    #@20
    sget v3, Landroid/widget/Editor;->DRAG_SHADOW_MAX_TEXT_LENGTH:I

    #@22
    if-le v2, v3, :cond_2a

    #@24
    .line 2149
    sget v2, Landroid/widget/Editor;->DRAG_SHADOW_MAX_TEXT_LENGTH:I

    #@26
    invoke-interface {p1, v5, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@29
    move-result-object p1

    #@2a
    .line 2151
    :cond_2a
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@2d
    .line 2152
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2f
    invoke-virtual {v2}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    #@36
    .line 2154
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@38
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@3b
    move-result-object v2

    #@3c
    const/16 v3, 0x10

    #@3e
    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@41
    .line 2155
    const/16 v2, 0x11

    #@43
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    #@46
    .line 2157
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    #@48
    invoke-direct {v2, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@4b
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@4e
    .line 2160
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@51
    move-result v1

    #@52
    .line 2161
    .local v1, size:I
    invoke-virtual {v0, v1, v1}, Landroid/widget/TextView;->measure(II)V

    #@55
    .line 2163
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    #@58
    move-result v2

    #@59
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@5c
    move-result v3

    #@5d
    invoke-virtual {v0, v5, v5, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    #@60
    .line 2164
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    #@63
    .line 2165
    new-instance v2, Landroid/view/View$DragShadowBuilder;

    #@65
    invoke-direct {v2, v0}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    #@68
    return-object v2
.end method

.method private hasPasswordTransformationMethod()Z
    .registers 2

    #@0
    .prologue
    .line 700
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    #@5
    move-result-object v0

    #@6
    instance-of v0, v0, Landroid/text/method/PasswordTransformationMethod;

    #@8
    return v0
.end method

.method private hideCursorControllers()V
    .registers 2

    #@0
    .prologue
    .line 576
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$SuggestionsPopupWindow;->isShowingUp()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_11

    #@c
    .line 578
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@e
    invoke-virtual {v0}, Landroid/widget/Editor$SuggestionsPopupWindow;->hide()V

    #@11
    .line 580
    :cond_11
    invoke-direct {p0}, Landroid/widget/Editor;->hideInsertionPointCursorController()V

    #@14
    .line 581
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@17
    .line 582
    return-void
.end method

.method private hideError()V
    .registers 2

    #@0
    .prologue
    .line 424
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 425
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$ErrorPopup;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 426
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@e
    invoke-virtual {v0}, Landroid/widget/Editor$ErrorPopup;->dismiss()V

    #@11
    .line 430
    :cond_11
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/widget/Editor;->mShowErrorAfterAttach:Z

    #@14
    .line 431
    return-void
.end method

.method private hideInsertionPointCursorController()V
    .registers 2

    #@0
    .prologue
    .line 556
    iget-object v0, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 557
    iget-object v0, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$InsertionPointCursorController;->hide()V

    #@9
    .line 559
    :cond_9
    return-void
.end method

.method private hideSpanControllers()V
    .registers 2

    #@0
    .prologue
    .line 570
    iget-object v0, p0, Landroid/widget/Editor;->mEasyEditSpanController:Landroid/widget/Editor$EasyEditSpanController;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 571
    iget-object v0, p0, Landroid/widget/Editor;->mEasyEditSpanController:Landroid/widget/Editor$EasyEditSpanController;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$EasyEditSpanController;->hide()V

    #@9
    .line 573
    :cond_9
    return-void
.end method

.method private isCursorInsideEasyCorrectionSpan()Z
    .registers 7

    #@0
    .prologue
    .line 1903
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/text/Spannable;

    #@8
    .line 1904
    .local v1, spannable:Landroid/text/Spannable;
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionStart()I

    #@d
    move-result v3

    #@e
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@10
    invoke-virtual {v4}, Landroid/widget/TextView;->getSelectionEnd()I

    #@13
    move-result v4

    #@14
    const-class v5, Landroid/text/style/SuggestionSpan;

    #@16
    invoke-interface {v1, v3, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, [Landroid/text/style/SuggestionSpan;

    #@1c
    .line 1906
    .local v2, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    array-length v3, v2

    #@1e
    if-ge v0, v3, :cond_2f

    #@20
    .line 1907
    aget-object v3, v2, v0

    #@22
    invoke-virtual {v3}, Landroid/text/style/SuggestionSpan;->getFlags()I

    #@25
    move-result v3

    #@26
    and-int/lit8 v3, v3, 0x1

    #@28
    if-eqz v3, :cond_2c

    #@2a
    .line 1908
    const/4 v3, 0x1

    #@2b
    .line 1911
    :goto_2b
    return v3

    #@2c
    .line 1906
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1d

    #@2f
    .line 1911
    :cond_2f
    const/4 v3, 0x0

    #@30
    goto :goto_2b
.end method

.method private isCursorInsideSuggestionSpan()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1890
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@6
    move-result-object v1

    #@7
    .line 1891
    .local v1, text:Ljava/lang/CharSequence;
    instance-of v3, v1, Landroid/text/Spannable;

    #@9
    if-nez v3, :cond_c

    #@b
    .line 1895
    .end local v1           #text:Ljava/lang/CharSequence;
    :cond_b
    :goto_b
    return v2

    #@c
    .line 1893
    .restart local v1       #text:Ljava/lang/CharSequence;
    :cond_c
    check-cast v1, Landroid/text/Spannable;

    #@e
    .end local v1           #text:Ljava/lang/CharSequence;
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@10
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionStart()I

    #@13
    move-result v3

    #@14
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@16
    invoke-virtual {v4}, Landroid/widget/TextView;->getSelectionEnd()I

    #@19
    move-result v4

    #@1a
    const-class v5, Landroid/text/style/SuggestionSpan;

    #@1c
    invoke-interface {v1, v3, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, [Landroid/text/style/SuggestionSpan;

    #@22
    .line 1895
    .local v0, suggestionSpans:[Landroid/text/style/SuggestionSpan;
    array-length v3, v0

    #@23
    if-lez v3, :cond_b

    #@25
    const/4 v2, 0x1

    #@26
    goto :goto_b
.end method

.method private isFloatingWindow()Z
    .registers 6

    #@0
    .prologue
    .line 2094
    const/4 v0, 0x0

    #@1
    .line 2095
    .local v0, isFloatingWindow:Z
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v3}, Landroid/widget/TextView;->getRootView()Landroid/view/View;

    #@6
    move-result-object v2

    #@7
    .line 2096
    .local v2, rootView:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@a
    move-result-object v1

    #@b
    .line 2097
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v3, v1, Landroid/view/WindowManager$LayoutParams;

    #@d
    if-eqz v3, :cond_28

    #@f
    .line 2098
    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    #@11
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@13
    const/16 v4, 0x7d2

    #@15
    if-ne v3, v4, :cond_29

    #@17
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, "FrameView"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_29

    #@27
    const/4 v0, 0x1

    #@28
    .line 2102
    :cond_28
    :goto_28
    return v0

    #@29
    .line 2098
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_28
.end method

.method private isLockscreen()Z
    .registers 4

    #@0
    .prologue
    .line 947
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "keyguard"

    #@9
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/app/KeyguardManager;

    #@f
    .line 948
    .local v0, kgm:Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1d

    #@15
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_1d

    #@1b
    .line 949
    const/4 v1, 0x1

    #@1c
    .line 951
    :goto_1c
    return v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method private isOffsetVisible(I)Z
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    .line 923
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@5
    move-result-object v0

    #@6
    .line 924
    .local v0, layout:Landroid/text/Layout;
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    #@9
    move-result v1

    #@a
    .line 925
    .local v1, line:I
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBottom(I)I

    #@d
    move-result v2

    #@e
    .line 926
    .local v2, lineBottom:I
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@11
    move-result v4

    #@12
    float-to-int v3, v4

    #@13
    .line 927
    .local v3, primaryHorizontal:I
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@15
    invoke-virtual {v4}, Landroid/widget/TextView;->viewportToContentHorizontalOffset()I

    #@18
    move-result v4

    #@19
    add-int/2addr v4, v3

    #@1a
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1c
    invoke-virtual {v5}, Landroid/widget/TextView;->viewportToContentVerticalOffset()I

    #@1f
    move-result v5

    #@20
    add-int/2addr v5, v2

    #@21
    invoke-direct {p0, v4, v5}, Landroid/widget/Editor;->isPositionVisible(II)Z

    #@24
    move-result v4

    #@25
    return v4
.end method

.method private isPositionOnText(FF)Z
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 935
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@6
    move-result-object v0

    #@7
    .line 936
    .local v0, layout:Landroid/text/Layout;
    if-nez v0, :cond_a

    #@9
    .line 943
    :cond_9
    :goto_9
    return v2

    #@a
    .line 938
    :cond_a
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@c
    invoke-virtual {v3, p2}, Landroid/widget/TextView;->getLineAtCoordinate(F)I

    #@f
    move-result v1

    #@10
    .line 939
    .local v1, line:I
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->convertToLocalHorizontalCoordinate(F)F

    #@15
    move-result p1

    #@16
    .line 941
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineLeft(I)F

    #@19
    move-result v3

    #@1a
    cmpg-float v3, p1, v3

    #@1c
    if-ltz v3, :cond_9

    #@1e
    .line 942
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineRight(I)F

    #@21
    move-result v3

    #@22
    cmpl-float v3, p1, v3

    #@24
    if-gtz v3, :cond_9

    #@26
    .line 943
    const/4 v2, 0x1

    #@27
    goto :goto_9
.end method

.method private isPositionVisible(II)Z
    .registers 14
    .parameter "positionX"
    .parameter "positionY"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 883
    sget-object v6, Landroid/widget/Editor;->TEMP_POSITION:[F

    #@5
    monitor-enter v6

    #@6
    .line 884
    :try_start_6
    sget-object v2, Landroid/widget/Editor;->TEMP_POSITION:[F

    #@8
    .line 885
    .local v2, position:[F
    const/4 v7, 0x0

    #@9
    int-to-float v8, p1

    #@a
    aput v8, v2, v7

    #@c
    .line 886
    const/4 v7, 0x1

    #@d
    int-to-float v8, p2

    #@e
    aput v8, v2, v7

    #@10
    .line 887
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    .line 889
    .local v3, view:Landroid/view/View;
    :goto_12
    if-eqz v3, :cond_8c

    #@14
    .line 890
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@16
    if-eq v3, v7, :cond_2e

    #@18
    .line 892
    const/4 v7, 0x0

    #@19
    aget v8, v2, v7

    #@1b
    invoke-virtual {v3}, Landroid/view/View;->getScrollX()I

    #@1e
    move-result v9

    #@1f
    int-to-float v9, v9

    #@20
    sub-float/2addr v8, v9

    #@21
    aput v8, v2, v7

    #@23
    .line 893
    const/4 v7, 0x1

    #@24
    aget v8, v2, v7

    #@26
    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    #@29
    move-result v9

    #@2a
    int-to-float v9, v9

    #@2b
    sub-float/2addr v8, v9

    #@2c
    aput v8, v2, v7

    #@2e
    .line 896
    :cond_2e
    const/4 v7, 0x0

    #@2f
    aget v7, v2, v7

    #@31
    cmpg-float v7, v7, v10

    #@33
    if-ltz v7, :cond_54

    #@35
    const/4 v7, 0x1

    #@36
    aget v7, v2, v7

    #@38
    cmpg-float v7, v7, v10

    #@3a
    if-ltz v7, :cond_54

    #@3c
    const/4 v7, 0x0

    #@3d
    aget v7, v2, v7

    #@3f
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    #@42
    move-result v8

    #@43
    int-to-float v8, v8

    #@44
    cmpl-float v7, v7, v8

    #@46
    if-gtz v7, :cond_54

    #@48
    const/4 v7, 0x1

    #@49
    aget v7, v2, v7

    #@4b
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    #@4e
    move-result v8

    #@4f
    int-to-float v8, v8

    #@50
    cmpl-float v7, v7, v8

    #@52
    if-lez v7, :cond_56

    #@54
    .line 898
    :cond_54
    monitor-exit v6

    #@55
    .line 919
    :goto_55
    return v4

    #@56
    .line 901
    :cond_56
    invoke-virtual {v3}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@59
    move-result-object v7

    #@5a
    invoke-virtual {v7}, Landroid/graphics/Matrix;->isIdentity()Z

    #@5d
    move-result v7

    #@5e
    if-nez v7, :cond_67

    #@60
    .line 902
    invoke-virtual {v3}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v7, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@67
    .line 905
    :cond_67
    const/4 v7, 0x0

    #@68
    aget v8, v2, v7

    #@6a
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    #@6d
    move-result v9

    #@6e
    int-to-float v9, v9

    #@6f
    add-float/2addr v8, v9

    #@70
    aput v8, v2, v7

    #@72
    .line 906
    const/4 v7, 0x1

    #@73
    aget v8, v2, v7

    #@75
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    #@78
    move-result v9

    #@79
    int-to-float v9, v9

    #@7a
    add-float/2addr v8, v9

    #@7b
    aput v8, v2, v7

    #@7d
    .line 908
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@80
    move-result-object v1

    #@81
    .line 909
    .local v1, parent:Landroid/view/ViewParent;
    instance-of v7, v1, Landroid/view/View;

    #@83
    if-eqz v7, :cond_8a

    #@85
    .line 910
    move-object v0, v1

    #@86
    check-cast v0, Landroid/view/View;

    #@88
    move-object v3, v0

    #@89
    goto :goto_12

    #@8a
    .line 913
    :cond_8a
    const/4 v3, 0x0

    #@8b
    goto :goto_12

    #@8c
    .line 916
    .end local v1           #parent:Landroid/view/ViewParent;
    :cond_8c
    monitor-exit v6

    #@8d
    move v4, v5

    #@8e
    .line 919
    goto :goto_55

    #@8f
    .line 916
    .end local v2           #position:[F
    .end local v3           #view:Landroid/view/View;
    :catchall_8f
    move-exception v4

    #@90
    monitor-exit v6
    :try_end_91
    .catchall {:try_start_6 .. :try_end_91} :catchall_8f

    #@91
    throw v4
.end method

.method private resumeBlink()V
    .registers 2

    #@0
    .prologue
    .line 617
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 618
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$Blink;->uncancel()V

    #@9
    .line 619
    invoke-virtual {p0}, Landroid/widget/Editor;->makeBlink()V

    #@c
    .line 621
    :cond_c
    return-void
.end method

.method private selectCurrentWord()Z
    .registers 19

    #@0
    .prologue
    .line 708
    invoke-direct/range {p0 .. p0}, Landroid/widget/Editor;->canSelectText()Z

    #@3
    move-result v15

    #@4
    if-nez v15, :cond_8

    #@6
    .line 709
    const/4 v15, 0x0

    #@7
    .line 782
    :goto_7
    return v15

    #@8
    .line 712
    :cond_8
    invoke-direct/range {p0 .. p0}, Landroid/widget/Editor;->hasPasswordTransformationMethod()Z

    #@b
    move-result v15

    #@c
    if-eqz v15, :cond_17

    #@e
    .line 716
    move-object/from16 v0, p0

    #@10
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    invoke-virtual {v15}, Landroid/widget/TextView;->selectAllText()Z

    #@15
    move-result v15

    #@16
    goto :goto_7

    #@17
    .line 719
    :cond_17
    move-object/from16 v0, p0

    #@19
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1b
    invoke-virtual {v15}, Landroid/widget/TextView;->getInputType()I

    #@1e
    move-result v1

    #@1f
    .line 720
    .local v1, inputType:I
    and-int/lit8 v2, v1, 0xf

    #@21
    .line 721
    .local v2, klass:I
    and-int/lit16 v13, v1, 0xff0

    #@23
    .line 724
    .local v13, variation:I
    const/4 v15, 0x2

    #@24
    if-eq v2, v15, :cond_3c

    #@26
    const/4 v15, 0x3

    #@27
    if-eq v2, v15, :cond_3c

    #@29
    const/4 v15, 0x4

    #@2a
    if-eq v2, v15, :cond_3c

    #@2c
    const/16 v15, 0x10

    #@2e
    if-eq v13, v15, :cond_3c

    #@30
    const/16 v15, 0x20

    #@32
    if-eq v13, v15, :cond_3c

    #@34
    const/16 v15, 0xd0

    #@36
    if-eq v13, v15, :cond_3c

    #@38
    const/16 v15, 0xb0

    #@3a
    if-ne v13, v15, :cond_45

    #@3c
    .line 731
    :cond_3c
    move-object/from16 v0, p0

    #@3e
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@40
    invoke-virtual {v15}, Landroid/widget/TextView;->selectAllText()Z

    #@43
    move-result v15

    #@44
    goto :goto_7

    #@45
    .line 734
    :cond_45
    invoke-direct/range {p0 .. p0}, Landroid/widget/Editor;->getLastTouchOffsets()J

    #@48
    move-result-wide v3

    #@49
    .line 735
    .local v3, lastTouchOffsets:J
    invoke-static {v3, v4}, Landroid/text/TextUtils;->unpackRangeStartFromLong(J)I

    #@4c
    move-result v6

    #@4d
    .line 736
    .local v6, minOffset:I
    invoke-static {v3, v4}, Landroid/text/TextUtils;->unpackRangeEndFromLong(J)I

    #@50
    move-result v5

    #@51
    .line 739
    .local v5, maxOffset:I
    if-ltz v6, :cond_61

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@57
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5a
    move-result-object v15

    #@5b
    invoke-interface {v15}, Ljava/lang/CharSequence;->length()I

    #@5e
    move-result v15

    #@5f
    if-lt v6, v15, :cond_63

    #@61
    :cond_61
    const/4 v15, 0x0

    #@62
    goto :goto_7

    #@63
    .line 740
    :cond_63
    if-ltz v5, :cond_73

    #@65
    move-object/from16 v0, p0

    #@67
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@69
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@6c
    move-result-object v15

    #@6d
    invoke-interface {v15}, Ljava/lang/CharSequence;->length()I

    #@70
    move-result v15

    #@71
    if-lt v5, v15, :cond_75

    #@73
    :cond_73
    const/4 v15, 0x0

    #@74
    goto :goto_7

    #@75
    .line 745
    :cond_75
    move-object/from16 v0, p0

    #@77
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@79
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@7c
    move-result-object v15

    #@7d
    check-cast v15, Landroid/text/Spanned;

    #@7f
    const-class v16, Landroid/text/style/URLSpan;

    #@81
    move-object/from16 v0, v16

    #@83
    invoke-interface {v15, v6, v5, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@86
    move-result-object v12

    #@87
    check-cast v12, [Landroid/text/style/URLSpan;

    #@89
    .line 747
    .local v12, urlSpans:[Landroid/text/style/URLSpan;
    array-length v15, v12

    #@8a
    const/16 v16, 0x1

    #@8c
    move/from16 v0, v16

    #@8e
    if-lt v15, v0, :cond_c1

    #@90
    .line 748
    const/4 v15, 0x0

    #@91
    aget-object v11, v12, v15

    #@93
    .line 749
    .local v11, urlSpan:Landroid/text/style/URLSpan;
    move-object/from16 v0, p0

    #@95
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@97
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9a
    move-result-object v15

    #@9b
    check-cast v15, Landroid/text/Spanned;

    #@9d
    invoke-interface {v15, v11}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@a0
    move-result v10

    #@a1
    .line 750
    .local v10, selectionStart:I
    move-object/from16 v0, p0

    #@a3
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a5
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@a8
    move-result-object v15

    #@a9
    check-cast v15, Landroid/text/Spanned;

    #@ab
    invoke-interface {v15, v11}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@ae
    move-result v9

    #@af
    .line 781
    .end local v11           #urlSpan:Landroid/text/style/URLSpan;
    .local v9, selectionEnd:I
    :cond_af
    :goto_af
    move-object/from16 v0, p0

    #@b1
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@b3
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@b6
    move-result-object v15

    #@b7
    check-cast v15, Landroid/text/Spannable;

    #@b9
    invoke-static {v15, v10, v9}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@bc
    .line 782
    if-le v9, v10, :cond_191

    #@be
    const/4 v15, 0x1

    #@bf
    goto/16 :goto_7

    #@c1
    .line 752
    .end local v9           #selectionEnd:I
    .end local v10           #selectionStart:I
    :cond_c1
    sget-boolean v15, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@c3
    if-eqz v15, :cond_162

    #@c5
    add-int/lit8 v15, v6, 0x1

    #@c7
    move-object/from16 v0, p0

    #@c9
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@cb
    move-object/from16 v16, v0

    #@cd
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@d0
    move-result-object v16

    #@d1
    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->length()I

    #@d4
    move-result v16

    #@d5
    move/from16 v0, v16

    #@d7
    if-ge v15, v0, :cond_162

    #@d9
    move-object/from16 v0, p0

    #@db
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@dd
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@e0
    move-result-object v15

    #@e1
    invoke-interface {v15, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@e4
    move-result v15

    #@e5
    move-object/from16 v0, p0

    #@e7
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@e9
    move-object/from16 v16, v0

    #@eb
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@ee
    move-result-object v16

    #@ef
    add-int/lit8 v17, v6, 0x1

    #@f1
    invoke-interface/range {v16 .. v17}, Ljava/lang/CharSequence;->charAt(I)C

    #@f4
    move-result v16

    #@f5
    invoke-static/range {v15 .. v16}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@f8
    move-result v15

    #@f9
    if-eqz v15, :cond_162

    #@fb
    .line 755
    move v10, v6

    #@fc
    .line 756
    .restart local v10       #selectionStart:I
    add-int/lit8 v9, v6, 0x2

    #@fe
    .line 757
    .restart local v9       #selectionEnd:I
    :goto_fe
    add-int/lit8 v15, v10, -0x2

    #@100
    if-ltz v15, :cond_129

    #@102
    move-object/from16 v0, p0

    #@104
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@106
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@109
    move-result-object v15

    #@10a
    add-int/lit8 v16, v10, -0x2

    #@10c
    invoke-interface/range {v15 .. v16}, Ljava/lang/CharSequence;->charAt(I)C

    #@10f
    move-result v15

    #@110
    move-object/from16 v0, p0

    #@112
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@114
    move-object/from16 v16, v0

    #@116
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@119
    move-result-object v16

    #@11a
    add-int/lit8 v17, v10, -0x1

    #@11c
    invoke-interface/range {v16 .. v17}, Ljava/lang/CharSequence;->charAt(I)C

    #@11f
    move-result v16

    #@120
    invoke-static/range {v15 .. v16}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@123
    move-result v15

    #@124
    if-eqz v15, :cond_129

    #@126
    .line 759
    add-int/lit8 v10, v10, -0x2

    #@128
    goto :goto_fe

    #@129
    .line 761
    :cond_129
    :goto_129
    add-int/lit8 v15, v9, 0x1

    #@12b
    move-object/from16 v0, p0

    #@12d
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12f
    move-object/from16 v16, v0

    #@131
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@134
    move-result-object v16

    #@135
    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->length()I

    #@138
    move-result v16

    #@139
    move/from16 v0, v16

    #@13b
    if-ge v15, v0, :cond_af

    #@13d
    move-object/from16 v0, p0

    #@13f
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@141
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@144
    move-result-object v15

    #@145
    invoke-interface {v15, v9}, Ljava/lang/CharSequence;->charAt(I)C

    #@148
    move-result v15

    #@149
    move-object/from16 v0, p0

    #@14b
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@14d
    move-object/from16 v16, v0

    #@14f
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@152
    move-result-object v16

    #@153
    add-int/lit8 v17, v9, 0x1

    #@155
    invoke-interface/range {v16 .. v17}, Ljava/lang/CharSequence;->charAt(I)C

    #@158
    move-result v16

    #@159
    invoke-static/range {v15 .. v16}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@15c
    move-result v15

    #@15d
    if-eqz v15, :cond_af

    #@15f
    .line 763
    add-int/lit8 v9, v9, 0x2

    #@161
    goto :goto_129

    #@162
    .line 766
    .end local v9           #selectionEnd:I
    .end local v10           #selectionStart:I
    :cond_162
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor;->getWordIterator()Landroid/text/method/WordIterator;

    #@165
    move-result-object v14

    #@166
    .line 767
    .local v14, wordIterator:Landroid/text/method/WordIterator;
    move-object/from16 v0, p0

    #@168
    iget-object v15, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@16a
    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@16d
    move-result-object v15

    #@16e
    invoke-virtual {v14, v15, v6, v5}, Landroid/text/method/WordIterator;->setCharSequence(Ljava/lang/CharSequence;II)V

    #@171
    .line 769
    invoke-virtual {v14, v6}, Landroid/text/method/WordIterator;->getBeginning(I)I

    #@174
    move-result v10

    #@175
    .line 770
    .restart local v10       #selectionStart:I
    invoke-virtual {v14, v5}, Landroid/text/method/WordIterator;->getEnd(I)I

    #@178
    move-result v9

    #@179
    .line 772
    .restart local v9       #selectionEnd:I
    const/4 v15, -0x1

    #@17a
    if-eq v10, v15, :cond_181

    #@17c
    const/4 v15, -0x1

    #@17d
    if-eq v9, v15, :cond_181

    #@17f
    if-ne v10, v9, :cond_af

    #@181
    .line 775
    :cond_181
    move-object/from16 v0, p0

    #@183
    invoke-direct {v0, v6}, Landroid/widget/Editor;->getCharRange(I)J

    #@186
    move-result-wide v7

    #@187
    .line 776
    .local v7, range:J
    invoke-static {v7, v8}, Landroid/text/TextUtils;->unpackRangeStartFromLong(J)I

    #@18a
    move-result v10

    #@18b
    .line 777
    invoke-static {v7, v8}, Landroid/text/TextUtils;->unpackRangeEndFromLong(J)I

    #@18e
    move-result v9

    #@18f
    goto/16 :goto_af

    #@191
    .line 782
    .end local v7           #range:J
    .end local v14           #wordIterator:Landroid/text/method/WordIterator;
    :cond_191
    const/4 v15, 0x0

    #@192
    goto/16 :goto_7
.end method

.method private setErrorIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "icon"

    #@0
    .prologue
    .line 412
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    iget-object v0, v1, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@4
    .line 413
    .local v0, dr:Landroid/widget/TextView$Drawables;
    if-nez v0, :cond_f

    #@6
    .line 414
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8
    new-instance v0, Landroid/widget/TextView$Drawables;

    #@a
    .end local v0           #dr:Landroid/widget/TextView$Drawables;
    invoke-direct {v0}, Landroid/widget/TextView$Drawables;-><init>()V

    #@d
    .restart local v0       #dr:Landroid/widget/TextView$Drawables;
    iput-object v0, v1, Landroid/widget/TextView;->mDrawables:Landroid/widget/TextView$Drawables;

    #@f
    .line 416
    :cond_f
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@11
    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView$Drawables;->setErrorDrawable(Landroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    #@14
    .line 418
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@16
    invoke-virtual {v1}, Landroid/widget/TextView;->resetResolvedDrawables()V

    #@19
    .line 419
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1b
    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    #@1e
    .line 420
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@20
    invoke-virtual {v1}, Landroid/widget/TextView;->requestLayout()V

    #@23
    .line 421
    return-void
.end method

.method private shouldBlink()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2062
    invoke-virtual {p0}, Landroid/widget/Editor;->isCursorVisible()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_f

    #@7
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@9
    invoke-virtual {v3}, Landroid/widget/TextView;->isFocused()Z

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_10

    #@f
    .line 2070
    :cond_f
    :goto_f
    return v2

    #@10
    .line 2064
    :cond_10
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionStart()I

    #@15
    move-result v1

    #@16
    .line 2065
    .local v1, start:I
    if-ltz v1, :cond_f

    #@18
    .line 2067
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1a
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionEnd()I

    #@1d
    move-result v0

    #@1e
    .line 2068
    .local v0, end:I
    if-ltz v0, :cond_f

    #@20
    .line 2070
    if-ne v1, v0, :cond_f

    #@22
    const/4 v2, 0x1

    #@23
    goto :goto_f
.end method

.method private showError()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/high16 v7, 0x3f00

    #@4
    .line 362
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v4}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    #@9
    move-result-object v4

    #@a
    if-nez v4, :cond_f

    #@c
    .line 363
    iput-boolean v9, p0, Landroid/widget/Editor;->mShowErrorAfterAttach:Z

    #@e
    .line 387
    :goto_e
    return-void

    #@f
    .line 367
    :cond_f
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@11
    if-nez v4, :cond_53

    #@13
    .line 368
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@15
    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@18
    move-result-object v4

    #@19
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@1c
    move-result-object v1

    #@1d
    .line 369
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x10900de

    #@20
    const/4 v5, 0x0

    #@21
    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/widget/TextView;

    #@27
    .line 372
    .local v0, err:Landroid/widget/TextView;
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@29
    invoke-virtual {v4}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@30
    move-result-object v4

    #@31
    iget v2, v4, Landroid/util/DisplayMetrics;->density:F

    #@33
    .line 373
    .local v2, scale:F
    new-instance v4, Landroid/widget/Editor$ErrorPopup;

    #@35
    const/high16 v5, 0x4348

    #@37
    mul-float/2addr v5, v2

    #@38
    add-float/2addr v5, v7

    #@39
    float-to-int v5, v5

    #@3a
    const/high16 v6, 0x4248

    #@3c
    mul-float/2addr v6, v2

    #@3d
    add-float/2addr v6, v7

    #@3e
    float-to-int v6, v6

    #@3f
    invoke-direct {v4, v0, v5, v6}, Landroid/widget/Editor$ErrorPopup;-><init>(Landroid/widget/TextView;II)V

    #@42
    iput-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@44
    .line 374
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@46
    invoke-virtual {v4, v8}, Landroid/widget/Editor$ErrorPopup;->setFocusable(Z)V

    #@49
    .line 377
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@4b
    invoke-virtual {v4, v9}, Landroid/widget/Editor$ErrorPopup;->setInputMethodMode(I)V

    #@4e
    .line 378
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@50
    invoke-virtual {v4, v8}, Landroid/widget/Editor$ErrorPopup;->setAllowScrollingAnchorParent(Z)V

    #@53
    .line 381
    .end local v0           #err:Landroid/widget/TextView;
    .end local v1           #inflater:Landroid/view/LayoutInflater;
    .end local v2           #scale:F
    :cond_53
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@55
    invoke-virtual {v4}, Landroid/widget/Editor$ErrorPopup;->getContentView()Landroid/view/View;

    #@58
    move-result-object v3

    #@59
    check-cast v3, Landroid/widget/TextView;

    #@5b
    .line 382
    .local v3, tv:Landroid/widget/TextView;
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@5d
    iget-object v5, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@5f
    invoke-direct {p0, v4, v5, v3}, Landroid/widget/Editor;->chooseSize(Landroid/widget/PopupWindow;Ljava/lang/CharSequence;Landroid/widget/TextView;)V

    #@62
    .line 383
    iget-object v4, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@64
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@67
    .line 385
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@69
    iget-object v5, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@6b
    invoke-direct {p0}, Landroid/widget/Editor;->getErrorX()I

    #@6e
    move-result v6

    #@6f
    invoke-direct {p0}, Landroid/widget/Editor;->getErrorY()I

    #@72
    move-result v7

    #@73
    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/Editor$ErrorPopup;->showAsDropDown(Landroid/view/View;II)V

    #@76
    .line 386
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@78
    iget-object v5, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@7a
    invoke-virtual {v5}, Landroid/widget/Editor$ErrorPopup;->isAboveAnchor()Z

    #@7d
    move-result v5

    #@7e
    invoke-virtual {v4, v5}, Landroid/widget/Editor$ErrorPopup;->fixDirection(Z)V

    #@81
    goto :goto_e
.end method

.method private suspendBlink()V
    .registers 2

    #@0
    .prologue
    .line 611
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 612
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$Blink;->cancel()V

    #@9
    .line 614
    :cond_9
    return-void
.end method

.method private touchPositionIsInSelection()Z
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 844
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@6
    move-result v4

    #@7
    .line 845
    .local v4, selectionStart:I
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@9
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionEnd()I

    #@c
    move-result v3

    #@d
    .line 847
    .local v3, selectionEnd:I
    if-ne v4, v3, :cond_10

    #@f
    .line 867
    :cond_f
    :goto_f
    return v7

    #@10
    .line 851
    :cond_10
    if-le v4, v3, :cond_20

    #@12
    .line 852
    move v5, v4

    #@13
    .line 853
    .local v5, tmp:I
    move v4, v3

    #@14
    .line 854
    move v3, v5

    #@15
    .line 855
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@17
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@1a
    move-result-object v6

    #@1b
    check-cast v6, Landroid/text/Spannable;

    #@1d
    invoke-static {v6, v4, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@20
    .line 858
    .end local v5           #tmp:I
    :cond_20
    invoke-virtual {p0}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@23
    move-result-object v2

    #@24
    .line 862
    .local v2, selectionController:Landroid/widget/Editor$SelectionModifierCursorController;
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@26
    if-eqz v6, :cond_2a

    #@28
    if-eqz v2, :cond_f

    #@2a
    .line 864
    :cond_2a
    invoke-virtual {v2}, Landroid/widget/Editor$SelectionModifierCursorController;->getMinTouchOffset()I

    #@2d
    move-result v1

    #@2e
    .line 865
    .local v1, minOffset:I
    invoke-virtual {v2}, Landroid/widget/Editor$SelectionModifierCursorController;->getMaxTouchOffset()I

    #@31
    move-result v0

    #@32
    .line 867
    .local v0, maxOffset:I
    if-lt v1, v4, :cond_39

    #@34
    if-ge v0, v3, :cond_39

    #@36
    const/4 v6, 0x1

    #@37
    :goto_37
    move v7, v6

    #@38
    goto :goto_f

    #@39
    :cond_39
    move v6, v7

    #@3a
    goto :goto_37
.end method

.method private updateCursorPosition(IIIF)V
    .registers 11
    .parameter "cursorIndex"
    .parameter "top"
    .parameter "bottom"
    .parameter "horizontal"

    #@0
    .prologue
    const/high16 v5, 0x3f00

    #@2
    .line 2009
    iget-object v2, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@4
    aget-object v2, v2, p1

    #@6
    if-nez v2, :cond_1a

    #@8
    .line 2010
    iget-object v2, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@a
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@c
    invoke-virtual {v3}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v3

    #@10
    iget-object v4, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    iget v4, v4, Landroid/widget/TextView;->mCursorDrawableRes:I

    #@14
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v2, p1

    #@1a
    .line 2013
    :cond_1a
    iget-object v2, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@1c
    if-nez v2, :cond_25

    #@1e
    new-instance v2, Landroid/graphics/Rect;

    #@20
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@23
    iput-object v2, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@25
    .line 2014
    :cond_25
    iget-object v2, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@27
    aget-object v2, v2, p1

    #@29
    iget-object v3, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@2b
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@2e
    .line 2015
    iget-object v2, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@30
    aget-object v2, v2, p1

    #@32
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@35
    move-result v1

    #@36
    .line 2016
    .local v1, width:I
    sub-float v2, p4, v5

    #@38
    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    #@3b
    move-result p4

    #@3c
    .line 2017
    float-to-int v2, p4

    #@3d
    iget-object v3, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@3f
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@41
    sub-int v0, v2, v3

    #@43
    .line 2018
    .local v0, left:I
    iget-object v2, p0, Landroid/widget/Editor;->mCursorDrawable:[Landroid/graphics/drawable/Drawable;

    #@45
    aget-object v2, v2, p1

    #@47
    iget-object v3, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@49
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@4b
    sub-int v3, p2, v3

    #@4d
    add-int v4, v0, v1

    #@4f
    iget-object v5, p0, Landroid/widget/Editor;->mTempRect:Landroid/graphics/Rect;

    #@51
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@53
    add-int/2addr v5, p3

    #@54
    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@57
    .line 2020
    return-void
.end method

.method private updateSpellCheckSpans(IIZ)V
    .registers 6
    .parameter "start"
    .parameter "end"
    .parameter "createSpellChecker"

    #@0
    .prologue
    .line 588
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->isTextEditable()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_2e

    #@8
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a
    invoke-virtual {v0}, Landroid/widget/TextView;->isSuggestionsEnabled()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_2e

    #@10
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    instance-of v0, v0, Landroid/inputmethodservice/ExtractEditText;

    #@14
    if-nez v0, :cond_2e

    #@16
    .line 590
    iget-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@18
    if-nez v0, :cond_25

    #@1a
    if-eqz p3, :cond_25

    #@1c
    .line 591
    new-instance v0, Landroid/widget/SpellChecker;

    #@1e
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@20
    invoke-direct {v0, v1}, Landroid/widget/SpellChecker;-><init>(Landroid/widget/TextView;)V

    #@23
    iput-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@25
    .line 593
    :cond_25
    iget-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@27
    if-eqz v0, :cond_2e

    #@29
    .line 594
    iget-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@2b
    invoke-virtual {v0, p1, p2}, Landroid/widget/SpellChecker;->spellCheck(II)V

    #@2e
    .line 597
    :cond_2e
    return-void
.end method


# virtual methods
.method public addSpanWatchers(Landroid/text/Spannable;)V
    .registers 6
    .parameter "text"

    #@0
    .prologue
    const/16 v3, 0x12

    #@2
    const/4 v2, 0x0

    #@3
    .line 2247
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    #@6
    move-result v0

    #@7
    .line 2249
    .local v0, textLength:I
    iget-object v1, p0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@9
    if-eqz v1, :cond_10

    #@b
    .line 2250
    iget-object v1, p0, Landroid/widget/Editor;->mKeyListener:Landroid/text/method/KeyListener;

    #@d
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@10
    .line 2253
    :cond_10
    iget-object v1, p0, Landroid/widget/Editor;->mEasyEditSpanController:Landroid/widget/Editor$EasyEditSpanController;

    #@12
    if-nez v1, :cond_1b

    #@14
    .line 2254
    new-instance v1, Landroid/widget/Editor$EasyEditSpanController;

    #@16
    invoke-direct {v1, p0}, Landroid/widget/Editor$EasyEditSpanController;-><init>(Landroid/widget/Editor;)V

    #@19
    iput-object v1, p0, Landroid/widget/Editor;->mEasyEditSpanController:Landroid/widget/Editor$EasyEditSpanController;

    #@1b
    .line 2256
    :cond_1b
    iget-object v1, p0, Landroid/widget/Editor;->mEasyEditSpanController:Landroid/widget/Editor$EasyEditSpanController;

    #@1d
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@20
    .line 2257
    return-void
.end method

.method adjustInputType(ZZZZ)V
    .registers 7
    .parameter "password"
    .parameter "passwordInputType"
    .parameter "webPasswordInputType"
    .parameter "numberPasswordInputType"

    #@0
    .prologue
    .line 628
    iget v0, p0, Landroid/widget/Editor;->mInputType:I

    #@2
    and-int/lit8 v0, v0, 0xf

    #@4
    const/4 v1, 0x1

    #@5
    if-ne v0, v1, :cond_1e

    #@7
    .line 629
    if-nez p1, :cond_b

    #@9
    if-eqz p2, :cond_13

    #@b
    .line 630
    :cond_b
    iget v0, p0, Landroid/widget/Editor;->mInputType:I

    #@d
    and-int/lit16 v0, v0, -0xff1

    #@f
    or-int/lit16 v0, v0, 0x80

    #@11
    iput v0, p0, Landroid/widget/Editor;->mInputType:I

    #@13
    .line 633
    :cond_13
    if-eqz p3, :cond_1d

    #@15
    .line 634
    iget v0, p0, Landroid/widget/Editor;->mInputType:I

    #@17
    and-int/lit16 v0, v0, -0xff1

    #@19
    or-int/lit16 v0, v0, 0xe0

    #@1b
    iput v0, p0, Landroid/widget/Editor;->mInputType:I

    #@1d
    .line 643
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 637
    :cond_1e
    iget v0, p0, Landroid/widget/Editor;->mInputType:I

    #@20
    and-int/lit8 v0, v0, 0xf

    #@22
    const/4 v1, 0x2

    #@23
    if-ne v0, v1, :cond_1d

    #@25
    .line 638
    if-eqz p4, :cond_1d

    #@27
    .line 639
    iget v0, p0, Landroid/widget/Editor;->mInputType:I

    #@29
    and-int/lit16 v0, v0, -0xff1

    #@2b
    or-int/lit8 v0, v0, 0x10

    #@2d
    iput v0, p0, Landroid/widget/Editor;->mInputType:I

    #@2f
    goto :goto_1d
.end method

.method areSuggestionsShown()Z
    .registers 2

    #@0
    .prologue
    .line 2049
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$SuggestionsPopupWindow;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public beginBatchEdit()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 1334
    iput-boolean v5, p0, Landroid/widget/Editor;->mInBatchEditControllers:Z

    #@5
    .line 1335
    iget-object v0, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@7
    .line 1336
    .local v0, ims:Landroid/widget/Editor$InputMethodState;
    if-eqz v0, :cond_2c

    #@9
    .line 1337
    iget v2, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@b
    add-int/lit8 v1, v2, 0x1

    #@d
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@f
    .line 1338
    .local v1, nesting:I
    if-ne v1, v5, :cond_2c

    #@11
    .line 1339
    iput-boolean v3, v0, Landroid/widget/Editor$InputMethodState;->mCursorChanged:Z

    #@13
    .line 1340
    iput v3, v0, Landroid/widget/Editor$InputMethodState;->mChangedDelta:I

    #@15
    .line 1341
    iget-boolean v2, v0, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@17
    if-eqz v2, :cond_2d

    #@19
    .line 1344
    iput v3, v0, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@1b
    .line 1345
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1d
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@20
    move-result-object v2

    #@21
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@24
    move-result v2

    #@25
    iput v2, v0, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@27
    .line 1351
    :goto_27
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@29
    invoke-virtual {v2}, Landroid/widget/TextView;->onBeginBatchEdit()V

    #@2c
    .line 1354
    .end local v1           #nesting:I
    :cond_2c
    return-void

    #@2d
    .line 1347
    .restart local v1       #nesting:I
    :cond_2d
    iput v4, v0, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@2f
    .line 1348
    iput v4, v0, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@31
    .line 1349
    iput-boolean v3, v0, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@33
    goto :goto_27
.end method

.method public createCliptrayManager(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 4988
    sget-object v1, Lcom/lge/loader/RuntimeLibraryLoader;->CLIPTRAY_MANAGER:Ljava/lang/String;

    #@2
    invoke-static {v1}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@5
    move-result-object v0

    #@6
    .line 4989
    .local v0, ic:Lcom/lge/loader/InstanceCreator;
    if-nez v0, :cond_10

    #@8
    .line 4990
    const-string v1, "Editor"

    #@a
    const-string v2, "Editor::create Editor, cannot get CliptrayManager from RuntimeLibraryLoader"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 4999
    :cond_f
    :goto_f
    return-void

    #@10
    .line 4994
    :cond_10
    invoke-virtual {v0, p1}, Lcom/lge/loader/InstanceCreator;->getDefaultInstance(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@16
    iput-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@18
    .line 4996
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@1a
    if-nez v1, :cond_f

    #@1c
    .line 4997
    const-string v1, "Editor"

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "Editor::create Editor, can\'t get CliptrayManager(mClipManager==null) / view id: "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2b
    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    #@2e
    move-result v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_f
.end method

.method createInputContentTypeIfNeeded()V
    .registers 2

    #@0
    .prologue
    .line 502
    iget-object v0, p0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 503
    new-instance v0, Landroid/widget/Editor$InputContentType;

    #@6
    invoke-direct {v0}, Landroid/widget/Editor$InputContentType;-><init>()V

    #@9
    iput-object v0, p0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@b
    .line 505
    :cond_b
    return-void
.end method

.method createInputMethodStateIfNeeded()V
    .registers 3

    #@0
    .prologue
    .line 508
    iget-object v1, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 509
    new-instance v1, Landroid/widget/Editor$InputMethodState;

    #@6
    invoke-direct {v1}, Landroid/widget/Editor$InputMethodState;-><init>()V

    #@9
    iput-object v1, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@b
    .line 517
    :goto_b
    return-void

    #@c
    .line 513
    :cond_c
    iget-object v0, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@e
    .line 514
    .local v0, ims:Landroid/widget/Editor$InputMethodState;
    const/4 v1, 0x0

    #@f
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@11
    goto :goto_b
.end method

.method public endBatchEdit()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1357
    iput-boolean v3, p0, Landroid/widget/Editor;->mInBatchEditControllers:Z

    #@3
    .line 1358
    iget-object v0, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@5
    .line 1359
    .local v0, ims:Landroid/widget/Editor$InputMethodState;
    if-eqz v0, :cond_16

    #@7
    .line 1360
    iget v2, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@9
    add-int/lit8 v1, v2, -0x1

    #@b
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@d
    .line 1361
    .local v1, nesting:I
    if-nez v1, :cond_12

    #@f
    .line 1362
    invoke-virtual {p0, v0}, Landroid/widget/Editor;->finishBatchEdit(Landroid/widget/Editor$InputMethodState;)V

    #@12
    .line 1365
    :cond_12
    if-gez v1, :cond_16

    #@14
    .line 1366
    iput v3, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@16
    .line 1370
    .end local v1           #nesting:I
    :cond_16
    return-void
.end method

.method ensureEndedBatchEdit()V
    .registers 3

    #@0
    .prologue
    .line 1373
    iget-object v0, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@2
    .line 1374
    .local v0, ims:Landroid/widget/Editor$InputMethodState;
    if-eqz v0, :cond_e

    #@4
    iget v1, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 1375
    const/4 v1, 0x0

    #@9
    iput v1, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@b
    .line 1376
    invoke-virtual {p0, v0}, Landroid/widget/Editor;->finishBatchEdit(Landroid/widget/Editor$InputMethodState;)V

    #@e
    .line 1378
    :cond_e
    return-void
.end method

.method extractText(Landroid/view/inputmethod/ExtractedTextRequest;Landroid/view/inputmethod/ExtractedText;)Z
    .registers 9
    .parameter "request"
    .parameter "outText"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 1396
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v3, v2

    #@4
    move v4, v2

    #@5
    move-object v5, p2

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/widget/Editor;->extractTextInternal(Landroid/view/inputmethod/ExtractedTextRequest;IIILandroid/view/inputmethod/ExtractedText;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method finishBatchEdit(Landroid/widget/Editor$InputMethodState;)V
    .registers 3
    .parameter "ims"

    #@0
    .prologue
    .line 1381
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->onEndBatchEdit()V

    #@5
    .line 1383
    iget-boolean v0, p1, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@7
    if-nez v0, :cond_d

    #@9
    iget-boolean v0, p1, Landroid/widget/Editor$InputMethodState;->mSelectionModeChanged:Z

    #@b
    if-eqz v0, :cond_16

    #@d
    .line 1384
    :cond_d
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@f
    invoke-virtual {v0}, Landroid/widget/TextView;->updateAfterEdit()V

    #@12
    .line 1385
    invoke-virtual {p0}, Landroid/widget/Editor;->reportExtractedText()Z

    #@15
    .line 1390
    :cond_15
    :goto_15
    return-void

    #@16
    .line 1386
    :cond_16
    iget-boolean v0, p1, Landroid/widget/Editor$InputMethodState;->mCursorChanged:Z

    #@18
    if-eqz v0, :cond_15

    #@1a
    .line 1388
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1c
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidateCursor()V

    #@1f
    goto :goto_15
.end method

.method getInsertionController()Landroid/widget/Editor$InsertionPointCursorController;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1979
    iget-boolean v2, p0, Landroid/widget/Editor;->mInsertionControllerEnabled:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1990
    :goto_5
    return-object v1

    #@6
    .line 1983
    :cond_6
    iget-object v2, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@8
    if-nez v2, :cond_1c

    #@a
    .line 1984
    new-instance v2, Landroid/widget/Editor$InsertionPointCursorController;

    #@c
    invoke-direct {v2, p0, v1}, Landroid/widget/Editor$InsertionPointCursorController;-><init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V

    #@f
    iput-object v2, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@11
    .line 1986
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@13
    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@16
    move-result-object v0

    #@17
    .line 1987
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@19
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@1c
    .line 1990
    .end local v0           #observer:Landroid/view/ViewTreeObserver;
    :cond_1c
    iget-object v1, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@1e
    goto :goto_5
.end method

.method getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;
    .registers 3

    #@0
    .prologue
    .line 1994
    iget-boolean v1, p0, Landroid/widget/Editor;->mSelectionControllerEnabled:Z

    #@2
    if-nez v1, :cond_6

    #@4
    .line 1995
    const/4 v1, 0x0

    #@5
    .line 2005
    :goto_5
    return-object v1

    #@6
    .line 1998
    :cond_6
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@8
    if-nez v1, :cond_1c

    #@a
    .line 1999
    new-instance v1, Landroid/widget/Editor$SelectionModifierCursorController;

    #@c
    invoke-direct {v1, p0}, Landroid/widget/Editor$SelectionModifierCursorController;-><init>(Landroid/widget/Editor;)V

    #@f
    iput-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@11
    .line 2001
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@13
    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@16
    move-result-object v0

    #@17
    .line 2002
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@19
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@1c
    .line 2005
    .end local v0           #observer:Landroid/view/ViewTreeObserver;
    :cond_1c
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@1e
    goto :goto_5
.end method

.method public getWordIterator()Landroid/text/method/WordIterator;
    .registers 3

    #@0
    .prologue
    .line 794
    iget-object v0, p0, Landroid/widget/Editor;->mWordIterator:Landroid/text/method/WordIterator;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 795
    new-instance v0, Landroid/text/method/WordIterator;

    #@6
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextServicesLocale()Ljava/util/Locale;

    #@b
    move-result-object v1

    #@c
    invoke-direct {v0, v1}, Landroid/text/method/WordIterator;-><init>(Ljava/util/Locale;)V

    #@f
    iput-object v0, p0, Landroid/widget/Editor;->mWordIterator:Landroid/text/method/WordIterator;

    #@11
    .line 797
    :cond_11
    iget-object v0, p0, Landroid/widget/Editor;->mWordIterator:Landroid/text/method/WordIterator;

    #@13
    return-object v0
.end method

.method hasInsertionController()Z
    .registers 2

    #@0
    .prologue
    .line 1968
    iget-boolean v0, p0, Landroid/widget/Editor;->mInsertionControllerEnabled:Z

    #@2
    return v0
.end method

.method hasSelectionController()Z
    .registers 2

    #@0
    .prologue
    .line 1975
    iget-boolean v0, p0, Landroid/widget/Editor;->mSelectionControllerEnabled:Z

    #@2
    return v0
.end method

.method hideControllers()V
    .registers 1

    #@0
    .prologue
    .line 565
    invoke-direct {p0}, Landroid/widget/Editor;->hideCursorControllers()V

    #@3
    .line 566
    invoke-direct {p0}, Landroid/widget/Editor;->hideSpanControllers()V

    #@6
    .line 567
    return-void
.end method

.method invalidateTextDisplayList()V
    .registers 3

    #@0
    .prologue
    .line 1766
    iget-object v1, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@2
    if-eqz v1, :cond_1a

    #@4
    .line 1767
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    iget-object v1, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@7
    array-length v1, v1

    #@8
    if-ge v0, v1, :cond_1a

    #@a
    .line 1768
    iget-object v1, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@c
    aget-object v1, v1, v0

    #@e
    if-eqz v1, :cond_17

    #@10
    iget-object v1, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@12
    aget-object v1, v1, v0

    #@14
    invoke-virtual {v1}, Landroid/view/DisplayList;->invalidate()V

    #@17
    .line 1767
    :cond_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 1771
    .end local v0           #i:I
    :cond_1a
    return-void
.end method

.method invalidateTextDisplayList(Landroid/text/Layout;II)V
    .registers 13
    .parameter "layout"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1737
    iget-object v8, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@2
    if-eqz v8, :cond_38

    #@4
    instance-of v8, p1, Landroid/text/DynamicLayout;

    #@6
    if-eqz v8, :cond_38

    #@8
    .line 1738
    invoke-virtual {p1, p2}, Landroid/text/Layout;->getLineForOffset(I)I

    #@b
    move-result v4

    #@c
    .line 1739
    .local v4, firstLine:I
    invoke-virtual {p1, p3}, Landroid/text/Layout;->getLineForOffset(I)I

    #@f
    move-result v6

    #@10
    .local v6, lastLine:I
    move-object v3, p1

    #@11
    .line 1741
    check-cast v3, Landroid/text/DynamicLayout;

    #@13
    .line 1742
    .local v3, dynamicLayout:Landroid/text/DynamicLayout;
    invoke-virtual {v3}, Landroid/text/DynamicLayout;->getBlockEndLines()[I

    #@16
    move-result-object v0

    #@17
    .line 1743
    .local v0, blockEndLines:[I
    invoke-virtual {v3}, Landroid/text/DynamicLayout;->getBlockIndices()[I

    #@1a
    move-result-object v2

    #@1b
    .line 1744
    .local v2, blockIndices:[I
    invoke-virtual {v3}, Landroid/text/DynamicLayout;->getNumberOfBlocks()I

    #@1e
    move-result v7

    #@1f
    .line 1746
    .local v7, numberOfBlocks:I
    const/4 v5, 0x0

    #@20
    .line 1748
    .local v5, i:I
    :goto_20
    if-ge v5, v7, :cond_26

    #@22
    .line 1749
    aget v8, v0, v5

    #@24
    if-lt v8, v4, :cond_39

    #@26
    .line 1754
    :cond_26
    :goto_26
    if-ge v5, v7, :cond_38

    #@28
    .line 1755
    aget v1, v2, v5

    #@2a
    .line 1756
    .local v1, blockIndex:I
    const/4 v8, -0x1

    #@2b
    if-eq v1, v8, :cond_34

    #@2d
    .line 1757
    iget-object v8, p0, Landroid/widget/Editor;->mTextDisplayLists:[Landroid/view/DisplayList;

    #@2f
    aget-object v8, v8, v1

    #@31
    invoke-virtual {v8}, Landroid/view/DisplayList;->invalidate()V

    #@34
    .line 1759
    :cond_34
    aget v8, v0, v5

    #@36
    if-lt v8, v6, :cond_3c

    #@38
    .line 1763
    .end local v0           #blockEndLines:[I
    .end local v1           #blockIndex:I
    .end local v2           #blockIndices:[I
    .end local v3           #dynamicLayout:Landroid/text/DynamicLayout;
    .end local v4           #firstLine:I
    .end local v5           #i:I
    .end local v6           #lastLine:I
    .end local v7           #numberOfBlocks:I
    :cond_38
    return-void

    #@39
    .line 1750
    .restart local v0       #blockEndLines:[I
    .restart local v2       #blockIndices:[I
    .restart local v3       #dynamicLayout:Landroid/text/DynamicLayout;
    .restart local v4       #firstLine:I
    .restart local v5       #i:I
    .restart local v6       #lastLine:I
    .restart local v7       #numberOfBlocks:I
    :cond_39
    add-int/lit8 v5, v5, 0x1

    #@3b
    goto :goto_20

    #@3c
    .line 1760
    .restart local v1       #blockIndex:I
    :cond_3c
    add-int/lit8 v5, v5, 0x1

    #@3e
    .line 1761
    goto :goto_26
.end method

.method isCursorVisible()Z
    .registers 2

    #@0
    .prologue
    .line 521
    iget-boolean v0, p0, Landroid/widget/Editor;->mCursorVisible:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v0}, Landroid/widget/TextView;->isTextEditable()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method isSelectionDragging()Z
    .registers 4

    #@0
    .prologue
    .line 1053
    iget-object v2, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@2
    if-eqz v2, :cond_22

    #@4
    .line 1054
    iget-object v2, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@6
    invoke-virtual {v2}, Landroid/widget/Editor$SelectionModifierCursorController;->getStartHandle()Landroid/widget/Editor$SelectionStartHandleView;

    #@9
    move-result-object v1

    #@a
    .line 1055
    .local v1, start:Landroid/widget/Editor$HandleView;
    iget-object v2, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@c
    invoke-virtual {v2}, Landroid/widget/Editor$SelectionModifierCursorController;->getEndHandle()Landroid/widget/Editor$SelectionEndHandleView;

    #@f
    move-result-object v0

    #@10
    .line 1056
    .local v0, end:Landroid/widget/Editor$HandleView;
    if-eqz v1, :cond_18

    #@12
    invoke-virtual {v1}, Landroid/widget/Editor$HandleView;->isDragging()Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_20

    #@18
    :cond_18
    if-eqz v0, :cond_22

    #@1a
    invoke-virtual {v0}, Landroid/widget/Editor$HandleView;->isDragging()Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_22

    #@20
    :cond_20
    const/4 v2, 0x1

    #@21
    .line 1058
    .end local v0           #end:Landroid/widget/Editor$HandleView;
    .end local v1           #start:Landroid/widget/Editor$HandleView;
    :goto_21
    return v2

    #@22
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_21
.end method

.method isShowingBubblePopup()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1044
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 1049
    :goto_5
    return v0

    #@6
    .line 1045
    :cond_6
    iget-object v1, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@8
    if-nez v1, :cond_12

    #@a
    .line 1046
    const-string v1, "Editor"

    #@c
    const-string v2, "Because BubblePopupHelper is null, we cannot get popup-showing status"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    goto :goto_5

    #@12
    .line 1049
    :cond_12
    iget-object v0, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@14
    invoke-virtual {v0}, Landroid/widget/BubblePopupHelper;->isShowingBubblePopup()Z

    #@17
    move-result v0

    #@18
    goto :goto_5
.end method

.method makeBlink()V
    .registers 7

    #@0
    .prologue
    .line 2074
    invoke-direct {p0}, Landroid/widget/Editor;->shouldBlink()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_30

    #@6
    .line 2080
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@8
    if-nez v0, :cond_12

    #@a
    new-instance v0, Landroid/widget/Editor$Blink;

    #@c
    const/4 v1, 0x0

    #@d
    invoke-direct {v0, p0, v1}, Landroid/widget/Editor$Blink;-><init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V

    #@10
    iput-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@12
    .line 2081
    :cond_12
    iget-boolean v0, p0, Landroid/widget/Editor;->mHasWindowFocus:Z

    #@14
    if-eqz v0, :cond_2f

    #@16
    .line 2082
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@19
    move-result-wide v0

    #@1a
    iput-wide v0, p0, Landroid/widget/Editor;->mShowCursor:J

    #@1c
    .line 2083
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@1e
    iget-object v1, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@20
    invoke-virtual {v0, v1}, Landroid/widget/Editor$Blink;->removeCallbacks(Ljava/lang/Runnable;)V

    #@23
    .line 2084
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@25
    iget-object v1, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@27
    iget-wide v2, p0, Landroid/widget/Editor;->mShowCursor:J

    #@29
    const-wide/16 v4, 0x1f4

    #@2b
    add-long/2addr v2, v4

    #@2c
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Editor$Blink;->postAtTime(Ljava/lang/Runnable;J)Z

    #@2f
    .line 2090
    :cond_2f
    :goto_2f
    return-void

    #@30
    .line 2088
    :cond_30
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@32
    if-eqz v0, :cond_2f

    #@34
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@36
    iget-object v1, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@38
    invoke-virtual {v0, v1}, Landroid/widget/Editor$Blink;->removeCallbacks(Ljava/lang/Runnable;)V

    #@3b
    goto :goto_2f
.end method

.method onAttachedToWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 247
    iget-boolean v1, p0, Landroid/widget/Editor;->mShowErrorAfterAttach:Z

    #@3
    if-eqz v1, :cond_a

    #@5
    .line 248
    invoke-direct {p0}, Landroid/widget/Editor;->showError()V

    #@8
    .line 249
    iput-boolean v3, p0, Landroid/widget/Editor;->mShowErrorAfterAttach:Z

    #@a
    .line 251
    :cond_a
    iput-boolean v3, p0, Landroid/widget/Editor;->mTemporaryDetach:Z

    #@c
    .line 253
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@e
    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@11
    move-result-object v0

    #@12
    .line 256
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@14
    if-eqz v1, :cond_1b

    #@16
    .line 257
    iget-object v1, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@18
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@1b
    .line 259
    :cond_1b
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@1d
    if-eqz v1, :cond_29

    #@1f
    .line 260
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@21
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionModifierCursorController;->resetTouchOffsets()V

    #@24
    .line 261
    iget-object v1, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@26
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    #@29
    .line 263
    :cond_29
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2b
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@2e
    move-result-object v1

    #@2f
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@32
    move-result v1

    #@33
    const/4 v2, 0x1

    #@34
    invoke-direct {p0, v3, v1, v2}, Landroid/widget/Editor;->updateSpellCheckSpans(IIZ)V

    #@37
    .line 266
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@39
    invoke-virtual {v1}, Landroid/widget/TextView;->hasTransientState()Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_55

    #@3f
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@41
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionStart()I

    #@44
    move-result v1

    #@45
    iget-object v2, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@47
    invoke-virtual {v2}, Landroid/widget/TextView;->getSelectionEnd()I

    #@4a
    move-result v2

    #@4b
    if-eq v1, v2, :cond_55

    #@4d
    .line 271
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@4f
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setHasTransientState(Z)V

    #@52
    .line 274
    invoke-virtual {p0}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@55
    .line 276
    :cond_55
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@57
    if-eqz v1, :cond_88

    #@59
    .line 277
    iget-object v1, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@5b
    if-nez v1, :cond_64

    #@5d
    .line 278
    new-instance v1, Landroid/widget/Editor$1;

    #@5f
    invoke-direct {v1, p0}, Landroid/widget/Editor$1;-><init>(Landroid/widget/Editor;)V

    #@62
    iput-object v1, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@64
    .line 287
    :cond_64
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@66
    if-eqz v1, :cond_98

    #@68
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6a
    invoke-interface {v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@6d
    move-result v1

    #@6e
    if-eqz v1, :cond_98

    #@70
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@72
    invoke-virtual {v1}, Landroid/widget/TextView;->isFocused()Z

    #@75
    move-result v1

    #@76
    if-eqz v1, :cond_98

    #@78
    .line 288
    iget-object v1, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@7a
    if-nez v1, :cond_89

    #@7c
    .line 289
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@7e
    iget-object v2, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@80
    invoke-interface {v1, v2}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@83
    .line 290
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@85
    invoke-interface {v1, v3}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@88
    .line 305
    :cond_88
    :goto_88
    return-void

    #@89
    .line 292
    :cond_89
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@8b
    iget-object v2, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@8d
    invoke-interface {v1, v2}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@90
    .line 293
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@92
    iget v2, p0, Landroid/widget/Editor;->mClipDataType:I

    #@94
    invoke-interface {v1, v2}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@97
    goto :goto_88

    #@98
    .line 296
    :cond_98
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@9a
    if-nez v1, :cond_a4

    #@9c
    .line 297
    const-string v1, "Editor"

    #@9e
    const-string v2, "can not add paste listener, service manager is null"

    #@a0
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_88

    #@a4
    .line 300
    :cond_a4
    iget-object v1, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@a6
    invoke-interface {v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@a9
    move-result v1

    #@aa
    if-nez v1, :cond_88

    #@ac
    .line 301
    const-string v1, "Editor"

    #@ae
    const-string v2, "can not add paste listener, connection is null"

    #@b0
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    goto :goto_88
.end method

.method public onCommitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 2031
    iget-object v0, p0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 2032
    new-instance v0, Landroid/widget/Editor$CorrectionHighlighter;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/Editor$CorrectionHighlighter;-><init>(Landroid/widget/Editor;)V

    #@9
    iput-object v0, p0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@b
    .line 2037
    :goto_b
    iget-object v0, p0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@d
    invoke-virtual {v0, p1}, Landroid/widget/Editor$CorrectionHighlighter;->highlight(Landroid/view/inputmethod/CorrectionInfo;)V

    #@10
    .line 2038
    return-void

    #@11
    .line 2034
    :cond_11
    iget-object v0, p0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@13
    const/4 v1, 0x0

    #@14
    #calls: Landroid/widget/Editor$CorrectionHighlighter;->invalidate(Z)V
    invoke-static {v0, v1}, Landroid/widget/Editor$CorrectionHighlighter;->access$500(Landroid/widget/Editor$CorrectionHighlighter;Z)V

    #@17
    goto :goto_b
.end method

.method onDetachedFromWindow()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 308
    iget-object v0, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 309
    invoke-direct {p0}, Landroid/widget/Editor;->hideError()V

    #@8
    .line 312
    :cond_8
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@a
    if-eqz v0, :cond_13

    #@c
    .line 313
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@e
    iget-object v1, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/Editor$Blink;->removeCallbacks(Ljava/lang/Runnable;)V

    #@13
    .line 316
    :cond_13
    iget-object v0, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@15
    if-eqz v0, :cond_1c

    #@17
    .line 317
    iget-object v0, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@19
    invoke-virtual {v0}, Landroid/widget/Editor$InsertionPointCursorController;->onDetached()V

    #@1c
    .line 320
    :cond_1c
    iget-object v0, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@1e
    if-eqz v0, :cond_25

    #@20
    .line 321
    iget-object v0, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@22
    invoke-virtual {v0}, Landroid/widget/Editor$SelectionModifierCursorController;->onDetached()V

    #@25
    .line 325
    :cond_25
    invoke-virtual {p0}, Landroid/widget/Editor;->suggestionPopupWindowWhenDetached()V

    #@28
    .line 326
    iget-object v0, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@2a
    if-eqz v0, :cond_33

    #@2c
    .line 327
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2e
    iget-object v1, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@33
    .line 330
    :cond_33
    invoke-virtual {p0}, Landroid/widget/Editor;->invalidateTextDisplayList()V

    #@36
    .line 332
    iget-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@38
    if-eqz v0, :cond_42

    #@3a
    .line 333
    iget-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@3c
    invoke-virtual {v0}, Landroid/widget/SpellChecker;->closeSession()V

    #@3f
    .line 336
    const/4 v0, 0x0

    #@40
    iput-object v0, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@42
    .line 339
    :cond_42
    const/4 v0, 0x1

    #@43
    iput-boolean v0, p0, Landroid/widget/Editor;->mPreserveDetachedSelection:Z

    #@45
    .line 340
    invoke-virtual {p0}, Landroid/widget/Editor;->hideControllers()V

    #@48
    .line 341
    iput-boolean v2, p0, Landroid/widget/Editor;->mPreserveDetachedSelection:Z

    #@4a
    .line 342
    iput-boolean v2, p0, Landroid/widget/Editor;->mTemporaryDetach:Z

    #@4c
    .line 343
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@4e
    if-eqz v0, :cond_68

    #@50
    .line 344
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@52
    if-eqz v0, :cond_68

    #@54
    .line 345
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@56
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@59
    move-result v0

    #@5a
    if-nez v0, :cond_68

    #@5c
    .line 346
    const-string v0, "cliptray_editer"

    #@5e
    const-string v1, "Editor::onDetachedFromWindow, hideCliptray()"

    #@60
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 347
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@65
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptray()V

    #@68
    .line 351
    :cond_68
    return-void
.end method

.method onDraw(Landroid/graphics/Canvas;Landroid/text/Layout;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 28
    .parameter "canvas"
    .parameter "layout"
    .parameter "highlight"
    .parameter "highlightPaint"
    .parameter "cursorOffsetVertical"

    #@0
    .prologue
    .line 1528
    move-object/from16 v0, p0

    #@2
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@4
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    #@7
    move-result v7

    #@8
    .line 1529
    .local v7, selectionStart:I
    move-object/from16 v0, p0

    #@a
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@c
    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionEnd()I

    #@f
    move-result v8

    #@10
    .line 1531
    .local v8, selectionEnd:I
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@14
    move-object/from16 v17, v0

    #@16
    .line 1532
    .local v17, ims:Landroid/widget/Editor$InputMethodState;
    if-eqz v17, :cond_111

    #@18
    move-object/from16 v0, v17

    #@1a
    iget v6, v0, Landroid/widget/Editor$InputMethodState;->mBatchEditNesting:I

    #@1c
    if-nez v6, :cond_111

    #@1e
    .line 1533
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@21
    move-result-object v5

    #@22
    .line 1534
    .local v5, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v5, :cond_111

    #@24
    .line 1535
    move-object/from16 v0, p0

    #@26
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@28
    invoke-virtual {v5, v6}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_6b

    #@2e
    .line 1536
    const/16 v18, 0x0

    #@30
    .line 1537
    .local v18, reported:Z
    move-object/from16 v0, v17

    #@32
    iget-boolean v6, v0, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@34
    if-nez v6, :cond_3c

    #@36
    move-object/from16 v0, v17

    #@38
    iget-boolean v6, v0, Landroid/widget/Editor$InputMethodState;->mSelectionModeChanged:Z

    #@3a
    if-eqz v6, :cond_40

    #@3c
    .line 1541
    :cond_3c
    invoke-virtual/range {p0 .. p0}, Landroid/widget/Editor;->reportExtractedText()Z

    #@3f
    move-result v18

    #@40
    .line 1543
    :cond_40
    if-nez v18, :cond_6b

    #@42
    if-eqz p3, :cond_6b

    #@44
    .line 1544
    const/4 v9, -0x1

    #@45
    .line 1545
    .local v9, candStart:I
    const/4 v10, -0x1

    #@46
    .line 1546
    .local v10, candEnd:I
    move-object/from16 v0, p0

    #@48
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@4a
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@4d
    move-result-object v6

    #@4e
    instance-of v6, v6, Landroid/text/Spannable;

    #@50
    if-eqz v6, :cond_64

    #@52
    .line 1547
    move-object/from16 v0, p0

    #@54
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@56
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@59
    move-result-object v19

    #@5a
    check-cast v19, Landroid/text/Spannable;

    #@5c
    .line 1548
    .local v19, sp:Landroid/text/Spannable;
    invoke-static/range {v19 .. v19}, Lcom/android/internal/widget/EditableInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@5f
    move-result v9

    #@60
    .line 1549
    invoke-static/range {v19 .. v19}, Lcom/android/internal/widget/EditableInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@63
    move-result v10

    #@64
    .line 1551
    .end local v19           #sp:Landroid/text/Spannable;
    :cond_64
    move-object/from16 v0, p0

    #@66
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@68
    invoke-virtual/range {v5 .. v10}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    #@6b
    .line 1560
    .end local v9           #candStart:I
    .end local v10           #candEnd:I
    .end local v18           #reported:Z
    :cond_6b
    move-object/from16 v0, p0

    #@6d
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@6f
    invoke-virtual {v5, v6}, Landroid/view/inputmethod/InputMethodManager;->isWatchingCursor(Landroid/view/View;)Z

    #@72
    move-result v6

    #@73
    if-eqz v6, :cond_111

    #@75
    if-eqz p3, :cond_111

    #@77
    .line 1561
    move-object/from16 v0, v17

    #@79
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@7b
    const/4 v11, 0x1

    #@7c
    move-object/from16 v0, p3

    #@7e
    invoke-virtual {v0, v6, v11}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@81
    .line 1562
    move-object/from16 v0, v17

    #@83
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mTmpOffset:[F

    #@85
    const/4 v11, 0x0

    #@86
    move-object/from16 v0, v17

    #@88
    iget-object v12, v0, Landroid/widget/Editor$InputMethodState;->mTmpOffset:[F

    #@8a
    const/4 v13, 0x1

    #@8b
    const/4 v14, 0x0

    #@8c
    aput v14, v12, v13

    #@8e
    aput v14, v6, v11

    #@90
    .line 1564
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    #@93
    move-result-object v6

    #@94
    move-object/from16 v0, v17

    #@96
    iget-object v11, v0, Landroid/widget/Editor$InputMethodState;->mTmpOffset:[F

    #@98
    invoke-virtual {v6, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@9b
    .line 1565
    move-object/from16 v0, v17

    #@9d
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@9f
    move-object/from16 v0, v17

    #@a1
    iget-object v11, v0, Landroid/widget/Editor$InputMethodState;->mTmpOffset:[F

    #@a3
    const/4 v12, 0x0

    #@a4
    aget v11, v11, v12

    #@a6
    move-object/from16 v0, v17

    #@a8
    iget-object v12, v0, Landroid/widget/Editor$InputMethodState;->mTmpOffset:[F

    #@aa
    const/4 v13, 0x1

    #@ab
    aget v12, v12, v13

    #@ad
    invoke-virtual {v6, v11, v12}, Landroid/graphics/RectF;->offset(FF)V

    #@b0
    .line 1567
    move-object/from16 v0, v17

    #@b2
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@b4
    const/4 v11, 0x0

    #@b5
    move/from16 v0, p5

    #@b7
    int-to-float v12, v0

    #@b8
    invoke-virtual {v6, v11, v12}, Landroid/graphics/RectF;->offset(FF)V

    #@bb
    .line 1569
    move-object/from16 v0, v17

    #@bd
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mCursorRectInWindow:Landroid/graphics/Rect;

    #@bf
    move-object/from16 v0, v17

    #@c1
    iget-object v11, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@c3
    iget v11, v11, Landroid/graphics/RectF;->left:F

    #@c5
    float-to-double v11, v11

    #@c6
    const-wide/high16 v13, 0x3fe0

    #@c8
    add-double/2addr v11, v13

    #@c9
    double-to-int v11, v11

    #@ca
    move-object/from16 v0, v17

    #@cc
    iget-object v12, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@ce
    iget v12, v12, Landroid/graphics/RectF;->top:F

    #@d0
    float-to-double v12, v12

    #@d1
    const-wide/high16 v14, 0x3fe0

    #@d3
    add-double/2addr v12, v14

    #@d4
    double-to-int v12, v12

    #@d5
    move-object/from16 v0, v17

    #@d7
    iget-object v13, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@d9
    iget v13, v13, Landroid/graphics/RectF;->right:F

    #@db
    float-to-double v13, v13

    #@dc
    const-wide/high16 v15, 0x3fe0

    #@de
    add-double/2addr v13, v15

    #@df
    double-to-int v13, v13

    #@e0
    move-object/from16 v0, v17

    #@e2
    iget-object v14, v0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@e4
    iget v14, v14, Landroid/graphics/RectF;->bottom:F

    #@e6
    float-to-double v14, v14

    #@e7
    const-wide/high16 v20, 0x3fe0

    #@e9
    add-double v14, v14, v20

    #@eb
    double-to-int v14, v14

    #@ec
    invoke-virtual {v6, v11, v12, v13, v14}, Landroid/graphics/Rect;->set(IIII)V

    #@ef
    .line 1574
    move-object/from16 v0, p0

    #@f1
    iget-object v12, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@f3
    move-object/from16 v0, v17

    #@f5
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mCursorRectInWindow:Landroid/graphics/Rect;

    #@f7
    iget v13, v6, Landroid/graphics/Rect;->left:I

    #@f9
    move-object/from16 v0, v17

    #@fb
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mCursorRectInWindow:Landroid/graphics/Rect;

    #@fd
    iget v14, v6, Landroid/graphics/Rect;->top:I

    #@ff
    move-object/from16 v0, v17

    #@101
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mCursorRectInWindow:Landroid/graphics/Rect;

    #@103
    iget v15, v6, Landroid/graphics/Rect;->right:I

    #@105
    move-object/from16 v0, v17

    #@107
    iget-object v6, v0, Landroid/widget/Editor$InputMethodState;->mCursorRectInWindow:Landroid/graphics/Rect;

    #@109
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    #@10b
    move/from16 v16, v0

    #@10d
    move-object v11, v5

    #@10e
    invoke-virtual/range {v11 .. v16}, Landroid/view/inputmethod/InputMethodManager;->updateCursor(Landroid/view/View;IIII)V

    #@111
    .line 1581
    .end local v5           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_111
    move-object/from16 v0, p0

    #@113
    iget-object v6, v0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@115
    if-eqz v6, :cond_122

    #@117
    .line 1582
    move-object/from16 v0, p0

    #@119
    iget-object v6, v0, Landroid/widget/Editor;->mCorrectionHighlighter:Landroid/widget/Editor$CorrectionHighlighter;

    #@11b
    move-object/from16 v0, p1

    #@11d
    move/from16 v1, p5

    #@11f
    invoke-virtual {v6, v0, v1}, Landroid/widget/Editor$CorrectionHighlighter;->draw(Landroid/graphics/Canvas;I)V

    #@122
    .line 1585
    :cond_122
    if-eqz p3, :cond_137

    #@124
    if-ne v7, v8, :cond_137

    #@126
    move-object/from16 v0, p0

    #@128
    iget v6, v0, Landroid/widget/Editor;->mCursorCount:I

    #@12a
    if-lez v6, :cond_137

    #@12c
    .line 1586
    move-object/from16 v0, p0

    #@12e
    move-object/from16 v1, p1

    #@130
    move/from16 v2, p5

    #@132
    invoke-direct {v0, v1, v2}, Landroid/widget/Editor;->drawCursor(Landroid/graphics/Canvas;I)V

    #@135
    .line 1589
    const/16 p3, 0x0

    #@137
    .line 1592
    :cond_137
    move-object/from16 v0, p0

    #@139
    iget-object v6, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@13b
    invoke-virtual {v6}, Landroid/widget/TextView;->canHaveDisplayList()Z

    #@13e
    move-result v6

    #@13f
    if-eqz v6, :cond_14b

    #@141
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@144
    move-result v6

    #@145
    if-eqz v6, :cond_14b

    #@147
    .line 1593
    invoke-direct/range {p0 .. p5}, Landroid/widget/Editor;->drawHardwareAccelerated(Landroid/graphics/Canvas;Landroid/text/Layout;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@14a
    .line 1598
    :goto_14a
    return-void

    #@14b
    .line 1596
    :cond_14b
    move-object/from16 v0, p2

    #@14d
    move-object/from16 v1, p1

    #@14f
    move-object/from16 v2, p3

    #@151
    move-object/from16 v3, p4

    #@153
    move/from16 v4, p5

    #@155
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@158
    goto :goto_14a
.end method

.method onDrop(Landroid/view/DragEvent;)V
    .registers 28
    .parameter "event"

    #@0
    .prologue
    .line 2180
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    const-string v23, ""

    #@4
    move-object/from16 v0, v23

    #@6
    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 2181
    .local v4, content:Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    #@c
    move-result-object v3

    #@d
    .line 2182
    .local v3, clipData:Landroid/content/ClipData;
    invoke-virtual {v3}, Landroid/content/ClipData;->getItemCount()I

    #@10
    move-result v11

    #@11
    .line 2183
    .local v11, itemCount:I
    const/4 v9, 0x0

    #@12
    .local v9, i:I
    :goto_12
    if-ge v9, v11, :cond_30

    #@14
    .line 2184
    invoke-virtual {v3, v9}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    #@17
    move-result-object v10

    #@18
    .line 2185
    .local v10, item:Landroid/content/ClipData$Item;
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1c
    move-object/from16 v23, v0

    #@1e
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@21
    move-result-object v23

    #@22
    move-object/from16 v0, v23

    #@24
    invoke-virtual {v10, v0}, Landroid/content/ClipData$Item;->coerceToStyledText(Landroid/content/Context;)Ljava/lang/CharSequence;

    #@27
    move-result-object v23

    #@28
    move-object/from16 v0, v23

    #@2a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2d
    .line 2183
    add-int/lit8 v9, v9, 0x1

    #@2f
    goto :goto_12

    #@30
    .line 2188
    .end local v10           #item:Landroid/content/ClipData$Item;
    :cond_30
    move-object/from16 v0, p0

    #@32
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@34
    move-object/from16 v23, v0

    #@36
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getX()F

    #@39
    move-result v24

    #@3a
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getY()F

    #@3d
    move-result v25

    #@3e
    invoke-virtual/range {v23 .. v25}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@41
    move-result v18

    #@42
    .line 2190
    .local v18, offset:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    #@45
    move-result-object v12

    #@46
    .line 2191
    .local v12, localState:Ljava/lang/Object;
    const/4 v6, 0x0

    #@47
    .line 2192
    .local v6, dragLocalState:Landroid/widget/Editor$DragLocalState;
    instance-of v0, v12, Landroid/widget/Editor$DragLocalState;

    #@49
    move/from16 v23, v0

    #@4b
    if-eqz v23, :cond_50

    #@4d
    move-object v6, v12

    #@4e
    .line 2193
    check-cast v6, Landroid/widget/Editor$DragLocalState;

    #@50
    .line 2195
    :cond_50
    if-eqz v6, :cond_7a

    #@52
    iget-object v0, v6, Landroid/widget/Editor$DragLocalState;->sourceTextView:Landroid/widget/TextView;

    #@54
    move-object/from16 v23, v0

    #@56
    move-object/from16 v0, p0

    #@58
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@5a
    move-object/from16 v24, v0

    #@5c
    move-object/from16 v0, v23

    #@5e
    move-object/from16 v1, v24

    #@60
    if-ne v0, v1, :cond_7a

    #@62
    const/4 v5, 0x1

    #@63
    .line 2198
    .local v5, dragDropIntoItself:Z
    :goto_63
    if-eqz v5, :cond_7c

    #@65
    .line 2206
    iget v0, v6, Landroid/widget/Editor$DragLocalState;->start:I

    #@67
    move/from16 v23, v0

    #@69
    move/from16 v0, v18

    #@6b
    move/from16 v1, v23

    #@6d
    if-lt v0, v1, :cond_7c

    #@6f
    iget v0, v6, Landroid/widget/Editor$DragLocalState;->end:I

    #@71
    move/from16 v23, v0

    #@73
    move/from16 v0, v18

    #@75
    move/from16 v1, v23

    #@77
    if-gt v0, v1, :cond_7c

    #@79
    .line 2244
    :cond_79
    :goto_79
    return-void

    #@7a
    .line 2195
    .end local v5           #dragDropIntoItself:Z
    :cond_7a
    const/4 v5, 0x0

    #@7b
    goto :goto_63

    #@7c
    .line 2213
    .restart local v5       #dragDropIntoItself:Z
    :cond_7c
    move-object/from16 v0, p0

    #@7e
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@80
    move-object/from16 v23, v0

    #@82
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@85
    move-result-object v23

    #@86
    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->length()I

    #@89
    move-result v19

    #@8a
    .line 2214
    .local v19, originalLength:I
    move-object/from16 v0, p0

    #@8c
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8e
    move-object/from16 v23, v0

    #@90
    move-object/from16 v0, v23

    #@92
    move/from16 v1, v18

    #@94
    move/from16 v2, v18

    #@96
    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/TextView;->prepareSpacesAroundPaste(IILjava/lang/CharSequence;)J

    #@99
    move-result-wide v15

    #@9a
    .line 2215
    .local v15, minMax:J
    invoke-static/range {v15 .. v16}, Landroid/text/TextUtils;->unpackRangeStartFromLong(J)I

    #@9d
    move-result v14

    #@9e
    .line 2216
    .local v14, min:I
    invoke-static/range {v15 .. v16}, Landroid/text/TextUtils;->unpackRangeEndFromLong(J)I

    #@a1
    move-result v13

    #@a2
    .line 2218
    .local v13, max:I
    move-object/from16 v0, p0

    #@a4
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a6
    move-object/from16 v23, v0

    #@a8
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@ab
    move-result-object v23

    #@ac
    check-cast v23, Landroid/text/Spannable;

    #@ae
    move-object/from16 v0, v23

    #@b0
    invoke-static {v0, v13}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@b3
    .line 2219
    move-object/from16 v0, p0

    #@b5
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@b7
    move-object/from16 v23, v0

    #@b9
    move-object/from16 v0, v23

    #@bb
    invoke-virtual {v0, v14, v13, v4}, Landroid/widget/TextView;->replaceText_internal(IILjava/lang/CharSequence;)V

    #@be
    .line 2221
    if-eqz v5, :cond_79

    #@c0
    .line 2222
    iget v8, v6, Landroid/widget/Editor$DragLocalState;->start:I

    #@c2
    .line 2223
    .local v8, dragSourceStart:I
    iget v7, v6, Landroid/widget/Editor$DragLocalState;->end:I

    #@c4
    .line 2224
    .local v7, dragSourceEnd:I
    if-gt v13, v8, :cond_da

    #@c6
    .line 2226
    move-object/from16 v0, p0

    #@c8
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@ca
    move-object/from16 v23, v0

    #@cc
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@cf
    move-result-object v23

    #@d0
    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->length()I

    #@d3
    move-result v23

    #@d4
    sub-int v21, v23, v19

    #@d6
    .line 2227
    .local v21, shift:I
    add-int v8, v8, v21

    #@d8
    .line 2228
    add-int v7, v7, v21

    #@da
    .line 2232
    .end local v21           #shift:I
    :cond_da
    move-object/from16 v0, p0

    #@dc
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@de
    move-object/from16 v23, v0

    #@e0
    move-object/from16 v0, v23

    #@e2
    invoke-virtual {v0, v8, v7}, Landroid/widget/TextView;->deleteText_internal(II)V

    #@e5
    .line 2235
    const/16 v23, 0x0

    #@e7
    add-int/lit8 v24, v8, -0x1

    #@e9
    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(II)I

    #@ec
    move-result v20

    #@ed
    .line 2236
    .local v20, prevCharIdx:I
    move-object/from16 v0, p0

    #@ef
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@f1
    move-object/from16 v23, v0

    #@f3
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@f6
    move-result-object v23

    #@f7
    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->length()I

    #@fa
    move-result v23

    #@fb
    add-int/lit8 v24, v8, 0x1

    #@fd
    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(II)I

    #@100
    move-result v17

    #@101
    .line 2237
    .local v17, nextCharIdx:I
    add-int/lit8 v23, v20, 0x1

    #@103
    move/from16 v0, v17

    #@105
    move/from16 v1, v23

    #@107
    if-le v0, v1, :cond_79

    #@109
    .line 2238
    move-object/from16 v0, p0

    #@10b
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@10d
    move-object/from16 v23, v0

    #@10f
    move-object/from16 v0, v23

    #@111
    move/from16 v1, v20

    #@113
    move/from16 v2, v17

    #@115
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@118
    move-result-object v22

    #@119
    .line 2239
    .local v22, t:Ljava/lang/CharSequence;
    const/16 v23, 0x0

    #@11b
    invoke-interface/range {v22 .. v23}, Ljava/lang/CharSequence;->charAt(I)C

    #@11e
    move-result v23

    #@11f
    invoke-static/range {v23 .. v23}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@122
    move-result v23

    #@123
    if-eqz v23, :cond_79

    #@125
    const/16 v23, 0x1

    #@127
    invoke-interface/range {v22 .. v23}, Ljava/lang/CharSequence;->charAt(I)C

    #@12a
    move-result v23

    #@12b
    invoke-static/range {v23 .. v23}, Ljava/lang/Character;->isSpaceChar(C)Z

    #@12e
    move-result v23

    #@12f
    if-eqz v23, :cond_79

    #@131
    .line 2240
    move-object/from16 v0, p0

    #@133
    iget-object v0, v0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@135
    move-object/from16 v23, v0

    #@137
    add-int/lit8 v24, v20, 0x1

    #@139
    move-object/from16 v0, v23

    #@13b
    move/from16 v1, v20

    #@13d
    move/from16 v2, v24

    #@13f
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->deleteText_internal(II)V

    #@142
    goto/16 :goto_79
.end method

.method onFocusChanged(ZI)V
    .registers 16
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 1069
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v11

    #@6
    iput-wide v11, p0, Landroid/widget/Editor;->mShowCursor:J

    #@8
    .line 1070
    invoke-virtual {p0}, Landroid/widget/Editor;->ensureEndedBatchEdit()V

    #@b
    .line 1072
    if-eqz p1, :cond_104

    #@d
    .line 1073
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@f
    if-eqz v8, :cond_2f

    #@11
    .line 1074
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@13
    if-eqz v8, :cond_2f

    #@15
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@17
    invoke-interface {v8}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@1a
    move-result v8

    #@1b
    if-eqz v8, :cond_2f

    #@1d
    .line 1075
    iget-object v8, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@1f
    if-eqz v8, :cond_f0

    #@21
    .line 1076
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@23
    iget-object v11, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@25
    invoke-interface {v8, v11}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@28
    .line 1077
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2a
    iget v11, p0, Landroid/widget/Editor;->mClipDataType:I

    #@2c
    invoke-interface {v8, v11}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@2f
    .line 1085
    :cond_2f
    :goto_2f
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@31
    if-eqz v8, :cond_38

    #@33
    iget-object v8, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@35
    invoke-virtual {v8}, Landroid/widget/BubblePopupHelper;->setTargetHelper()V

    #@38
    .line 1086
    :cond_38
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3a
    invoke-virtual {v8}, Landroid/widget/TextView;->getSelectionStart()I

    #@3d
    move-result v7

    #@3e
    .line 1087
    .local v7, selStart:I
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@40
    invoke-virtual {v8}, Landroid/widget/TextView;->getSelectionEnd()I

    #@43
    move-result v6

    #@44
    .line 1091
    .local v6, selEnd:I
    iget-boolean v8, p0, Landroid/widget/Editor;->mSelectAllOnFocus:Z

    #@46
    if-eqz v8, :cond_fe

    #@48
    if-nez v7, :cond_fe

    #@4a
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@4c
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@4f
    move-result-object v8

    #@50
    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    #@53
    move-result v8

    #@54
    if-ne v6, v8, :cond_fe

    #@56
    move v2, v9

    #@57
    .line 1094
    .local v2, isFocusHighlighted:Z
    :goto_57
    iget-boolean v8, p0, Landroid/widget/Editor;->mFrozenWithFocus:Z

    #@59
    if-eqz v8, :cond_101

    #@5b
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@5d
    invoke-virtual {v8}, Landroid/widget/TextView;->hasSelection()Z

    #@60
    move-result v8

    #@61
    if-eqz v8, :cond_101

    #@63
    if-nez v2, :cond_101

    #@65
    move v8, v9

    #@66
    :goto_66
    iput-boolean v8, p0, Landroid/widget/Editor;->mCreatedWithASelection:Z

    #@68
    .line 1097
    iget-boolean v8, p0, Landroid/widget/Editor;->mFrozenWithFocus:Z

    #@6a
    if-eqz v8, :cond_70

    #@6c
    if-ltz v7, :cond_70

    #@6e
    if-gez v6, :cond_e1

    #@70
    .line 1100
    :cond_70
    invoke-direct {p0}, Landroid/widget/Editor;->getLastTapPosition()I

    #@73
    move-result v4

    #@74
    .line 1101
    .local v4, lastTapPosition:I
    if-ltz v4, :cond_87

    #@76
    invoke-static {}, Landroid/widget/BubblePopupHelper;->isShowingAnyBubblePopup()Z

    #@79
    move-result v8

    #@7a
    if-nez v8, :cond_87

    #@7c
    .line 1102
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@7e
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@81
    move-result-object v8

    #@82
    check-cast v8, Landroid/text/Spannable;

    #@84
    invoke-static {v8, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@87
    .line 1106
    :cond_87
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@89
    invoke-virtual {v8}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    #@8c
    move-result-object v5

    #@8d
    .line 1107
    .local v5, mMovement:Landroid/text/method/MovementMethod;
    if-eqz v5, :cond_a2

    #@8f
    invoke-static {}, Landroid/widget/BubblePopupHelper;->isShowingAnyBubblePopup()Z

    #@92
    move-result v8

    #@93
    if-nez v8, :cond_a2

    #@95
    .line 1108
    iget-object v11, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@97
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@99
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@9c
    move-result-object v8

    #@9d
    check-cast v8, Landroid/text/Spannable;

    #@9f
    invoke-interface {v5, v11, v8, p2}, Landroid/text/method/MovementMethod;->onTakeFocus(Landroid/widget/TextView;Landroid/text/Spannable;I)V

    #@a2
    .line 1116
    :cond_a2
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a4
    instance-of v8, v8, Landroid/inputmethodservice/ExtractEditText;

    #@a6
    if-nez v8, :cond_ac

    #@a8
    iget-boolean v8, p0, Landroid/widget/Editor;->mSelectionMoved:Z

    #@aa
    if-eqz v8, :cond_bb

    #@ac
    :cond_ac
    if-ltz v7, :cond_bb

    #@ae
    if-ltz v6, :cond_bb

    #@b0
    .line 1127
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@b2
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@b5
    move-result-object v8

    #@b6
    check-cast v8, Landroid/text/Spannable;

    #@b8
    invoke-static {v8, v7, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@bb
    .line 1130
    :cond_bb
    const/4 v3, 0x0

    #@bc
    .line 1131
    .local v3, isShowingHandle:Z
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@be
    if-eqz v8, :cond_d0

    #@c0
    .line 1132
    invoke-virtual {p0}, Landroid/widget/Editor;->getInsertionController()Landroid/widget/Editor$InsertionPointCursorController;

    #@c3
    move-result-object v0

    #@c4
    .line 1133
    .local v0, controller:Landroid/widget/Editor$InsertionPointCursorController;
    if-eqz v0, :cond_d0

    #@c6
    .line 1134
    #calls: Landroid/widget/Editor$InsertionPointCursorController;->getHandle()Landroid/widget/Editor$InsertionHandleView;
    invoke-static {v0}, Landroid/widget/Editor$InsertionPointCursorController;->access$200(Landroid/widget/Editor$InsertionPointCursorController;)Landroid/widget/Editor$InsertionHandleView;

    #@c9
    move-result-object v1

    #@ca
    .line 1135
    .local v1, handle:Landroid/widget/Editor$HandleView;
    if-eqz v1, :cond_d0

    #@cc
    invoke-virtual {v1}, Landroid/widget/Editor$HandleView;->isShowing()Z

    #@cf
    move-result v3

    #@d0
    .line 1139
    .end local v0           #controller:Landroid/widget/Editor$InsertionPointCursorController;
    .end local v1           #handle:Landroid/widget/Editor$HandleView;
    :cond_d0
    iget-boolean v8, p0, Landroid/widget/Editor;->mSelectAllOnFocus:Z

    #@d2
    if-eqz v8, :cond_df

    #@d4
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@d6
    if-eqz v8, :cond_da

    #@d8
    if-nez v3, :cond_df

    #@da
    .line 1140
    :cond_da
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@dc
    invoke-virtual {v8}, Landroid/widget/TextView;->selectAllText()Z

    #@df
    .line 1143
    :cond_df
    iput-boolean v9, p0, Landroid/widget/Editor;->mTouchFocusSelected:Z

    #@e1
    .line 1146
    .end local v3           #isShowingHandle:Z
    .end local v4           #lastTapPosition:I
    .end local v5           #mMovement:Landroid/text/method/MovementMethod;
    :cond_e1
    iput-boolean v10, p0, Landroid/widget/Editor;->mFrozenWithFocus:Z

    #@e3
    .line 1147
    iput-boolean v10, p0, Landroid/widget/Editor;->mSelectionMoved:Z

    #@e5
    .line 1149
    iget-object v8, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@e7
    if-eqz v8, :cond_ec

    #@e9
    .line 1150
    invoke-direct {p0}, Landroid/widget/Editor;->showError()V

    #@ec
    .line 1153
    :cond_ec
    invoke-virtual {p0}, Landroid/widget/Editor;->makeBlink()V

    #@ef
    .line 1200
    .end local v2           #isFocusHighlighted:Z
    .end local v6           #selEnd:I
    .end local v7           #selStart:I
    :cond_ef
    :goto_ef
    return-void

    #@f0
    .line 1079
    :cond_f0
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@f2
    iget-object v11, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@f4
    invoke-interface {v8, v11}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@f7
    .line 1080
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@f9
    invoke-interface {v8, v10}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@fc
    goto/16 :goto_2f

    #@fe
    .restart local v6       #selEnd:I
    .restart local v7       #selStart:I
    :cond_fe
    move v2, v10

    #@ff
    .line 1091
    goto/16 :goto_57

    #@101
    .restart local v2       #isFocusHighlighted:Z
    :cond_101
    move v8, v10

    #@102
    .line 1094
    goto/16 :goto_66

    #@104
    .line 1155
    .end local v2           #isFocusHighlighted:Z
    .end local v6           #selEnd:I
    .end local v7           #selStart:I
    :cond_104
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@106
    if-eqz v8, :cond_120

    #@108
    .line 1156
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@10a
    if-eqz v8, :cond_120

    #@10c
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@10e
    invoke-interface {v8}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@111
    move-result v8

    #@112
    if-nez v8, :cond_120

    #@114
    .line 1157
    const-string v8, "cliptray_editer"

    #@116
    const-string v11, "Editor::onFocusChanged, hideCliptray()"

    #@118
    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11b
    .line 1158
    iget-object v8, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@11d
    invoke-interface {v8}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptray()V

    #@120
    .line 1161
    :cond_120
    iget-object v8, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@122
    if-eqz v8, :cond_127

    #@124
    .line 1162
    invoke-direct {p0}, Landroid/widget/Editor;->hideError()V

    #@127
    .line 1165
    :cond_127
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@129
    invoke-virtual {v8}, Landroid/widget/TextView;->onEndBatchEdit()V

    #@12c
    .line 1167
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12e
    instance-of v8, v8, Landroid/inputmethodservice/ExtractEditText;

    #@130
    if-eqz v8, :cond_156

    #@132
    .line 1170
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@134
    invoke-virtual {v8}, Landroid/widget/TextView;->getSelectionStart()I

    #@137
    move-result v7

    #@138
    .line 1171
    .restart local v7       #selStart:I
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@13a
    invoke-virtual {v8}, Landroid/widget/TextView;->getSelectionEnd()I

    #@13d
    move-result v6

    #@13e
    .line 1172
    .restart local v6       #selEnd:I
    invoke-virtual {p0}, Landroid/widget/Editor;->hideControllers()V

    #@141
    .line 1173
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@143
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@146
    move-result-object v8

    #@147
    check-cast v8, Landroid/text/Spannable;

    #@149
    invoke-static {v8, v7, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@14c
    .line 1196
    .end local v6           #selEnd:I
    .end local v7           #selStart:I
    :goto_14c
    iget-object v8, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@14e
    if-eqz v8, :cond_ef

    #@150
    .line 1197
    iget-object v8, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@152
    invoke-virtual {v8}, Landroid/widget/Editor$SelectionModifierCursorController;->resetTouchOffsets()V

    #@155
    goto :goto_ef

    #@156
    .line 1175
    :cond_156
    iget-boolean v8, p0, Landroid/widget/Editor;->mTemporaryDetach:Z

    #@158
    if-eqz v8, :cond_15c

    #@15a
    iput-boolean v9, p0, Landroid/widget/Editor;->mPreserveDetachedSelection:Z

    #@15c
    .line 1176
    :cond_15c
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@15e
    if-eqz v8, :cond_18b

    #@160
    iget-boolean v8, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@162
    if-eqz v8, :cond_18b

    #@164
    iget-object v8, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@166
    invoke-virtual {v8}, Landroid/widget/BubblePopupHelper;->isFullscreenMode()Z

    #@169
    move-result v8

    #@16a
    if-nez v8, :cond_18b

    #@16c
    .line 1177
    invoke-virtual {p0, v10}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@16f
    .line 1178
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@172
    .line 1179
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@174
    invoke-virtual {v8}, Landroid/widget/TextView;->hasSelection()Z

    #@177
    move-result v8

    #@178
    if-eqz v8, :cond_18b

    #@17a
    .line 1180
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@17c
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@17f
    move-result-object v8

    #@180
    check-cast v8, Landroid/text/Spannable;

    #@182
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@184
    invoke-virtual {v9}, Landroid/widget/TextView;->getSelectionEnd()I

    #@187
    move-result v9

    #@188
    invoke-static {v8, v9}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@18b
    .line 1183
    :cond_18b
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@18d
    if-eqz v8, :cond_1a9

    #@18f
    iget-boolean v8, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@191
    if-nez v8, :cond_1a9

    #@193
    iget-object v8, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@195
    invoke-virtual {v8}, Landroid/widget/BubblePopupHelper;->isFullscreenMode()Z

    #@198
    move-result v8

    #@199
    if-nez v8, :cond_1a9

    #@19b
    .line 1184
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@19d
    invoke-virtual {v8}, Landroid/widget/TextView;->hasSelection()Z

    #@1a0
    move-result v8

    #@1a1
    if-nez v8, :cond_1a9

    #@1a3
    .line 1185
    invoke-virtual {p0, v10}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@1a6
    .line 1186
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@1a9
    .line 1190
    :cond_1a9
    invoke-virtual {p0}, Landroid/widget/Editor;->hideControllers()V

    #@1ac
    .line 1191
    iget-boolean v8, p0, Landroid/widget/Editor;->mTemporaryDetach:Z

    #@1ae
    if-eqz v8, :cond_1b2

    #@1b0
    iput-boolean v10, p0, Landroid/widget/Editor;->mPreserveDetachedSelection:Z

    #@1b2
    .line 1192
    :cond_1b2
    invoke-direct {p0}, Landroid/widget/Editor;->downgradeEasyCorrectionSpans()V

    #@1b5
    goto :goto_14c
.end method

.method onLocaleChanged()V
    .registers 2

    #@0
    .prologue
    .line 787
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/widget/Editor;->mWordIterator:Landroid/text/method/WordIterator;

    #@3
    .line 788
    return-void
.end method

.method onScreenStateChanged(I)V
    .registers 2
    .parameter "screenState"

    #@0
    .prologue
    .line 600
    packed-switch p1, :pswitch_data_c

    #@3
    .line 608
    :goto_3
    return-void

    #@4
    .line 602
    :pswitch_4
    invoke-direct {p0}, Landroid/widget/Editor;->resumeBlink()V

    #@7
    goto :goto_3

    #@8
    .line 605
    :pswitch_8
    invoke-direct {p0}, Landroid/widget/Editor;->suspendBlink()V

    #@b
    goto :goto_3

    #@c
    .line 600
    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method

.method onScrollChanged()V
    .registers 2

    #@0
    .prologue
    .line 2053
    iget-object v0, p0, Landroid/widget/Editor;->mPositionListener:Landroid/widget/Editor$PositionListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2054
    iget-object v0, p0, Landroid/widget/Editor;->mPositionListener:Landroid/widget/Editor$PositionListener;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$PositionListener;->onScrollChanged()V

    #@9
    .line 2056
    :cond_9
    return-void
.end method

.method onTouchEvent(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1313
    invoke-virtual {p0}, Landroid/widget/Editor;->hasSelectionController()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 1314
    invoke-virtual {p0}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Landroid/widget/Editor$SelectionModifierCursorController;->onTouchEvent(Landroid/view/MotionEvent;)V

    #@e
    .line 1317
    :cond_e
    iget-object v0, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@10
    if-eqz v0, :cond_1c

    #@12
    .line 1318
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@14
    iget-object v1, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@16
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@19
    .line 1319
    const/4 v0, 0x0

    #@1a
    iput-object v0, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@1c
    .line 1322
    :cond_1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_32

    #@22
    .line 1323
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/widget/Editor;->mLastDownPositionX:F

    #@28
    .line 1324
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Landroid/widget/Editor;->mLastDownPositionY:F

    #@2e
    .line 1328
    iput-boolean v2, p0, Landroid/widget/Editor;->mTouchFocusSelected:Z

    #@30
    .line 1329
    iput-boolean v2, p0, Landroid/widget/Editor;->mIgnoreActionUpEvent:Z

    #@32
    .line 1331
    :cond_32
    return-void
.end method

.method onTouchUpEvent(Landroid/view/MotionEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    .line 1915
    iget-boolean v3, p0, Landroid/widget/Editor;->mSelectAllOnFocus:Z

    #@2
    if-eqz v3, :cond_5a

    #@4
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v3}, Landroid/widget/TextView;->didTouchFocusSelect()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_5a

    #@c
    const/4 v1, 0x1

    #@d
    .line 1916
    .local v1, selectAllGotFocus:Z
    :goto_d
    invoke-virtual {p0}, Landroid/widget/Editor;->hideControllers()V

    #@10
    .line 1917
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@12
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@15
    move-result-object v2

    #@16
    .line 1918
    .local v2, text:Ljava/lang/CharSequence;
    if-nez v1, :cond_59

    #@18
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@1b
    move-result v3

    #@1c
    if-lez v3, :cond_59

    #@1e
    .line 1920
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@20
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@23
    move-result v4

    #@24
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@27
    move-result v5

    #@28
    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@2b
    move-result v0

    #@2c
    .line 1921
    .local v0, offset:I
    check-cast v2, Landroid/text/Spannable;

    #@2e
    .end local v2           #text:Ljava/lang/CharSequence;
    invoke-static {v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@31
    .line 1922
    iget-object v3, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@33
    if-eqz v3, :cond_3a

    #@35
    .line 1924
    iget-object v3, p0, Landroid/widget/Editor;->mSpellChecker:Landroid/widget/SpellChecker;

    #@37
    invoke-virtual {v3}, Landroid/widget/SpellChecker;->onSelectionChanged()V

    #@3a
    .line 1926
    :cond_3a
    invoke-direct {p0}, Landroid/widget/Editor;->extractedTextModeWillBeStarted()Z

    #@3d
    move-result v3

    #@3e
    if-nez v3, :cond_59

    #@40
    .line 1927
    invoke-direct {p0}, Landroid/widget/Editor;->isCursorInsideEasyCorrectionSpan()Z

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_5c

    #@46
    .line 1928
    new-instance v3, Landroid/widget/Editor$2;

    #@48
    invoke-direct {v3, p0}, Landroid/widget/Editor$2;-><init>(Landroid/widget/Editor;)V

    #@4b
    iput-object v3, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@4d
    .line 1934
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@4f
    iget-object v4, p0, Landroid/widget/Editor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    #@51
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@54
    move-result v5

    #@55
    int-to-long v5, v5

    #@56
    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@59
    .line 1941
    .end local v0           #offset:I
    :cond_59
    :goto_59
    return-void

    #@5a
    .line 1915
    .end local v1           #selectAllGotFocus:Z
    :cond_5a
    const/4 v1, 0x0

    #@5b
    goto :goto_d

    #@5c
    .line 1936
    .restart local v0       #offset:I
    .restart local v1       #selectAllGotFocus:Z
    :cond_5c
    invoke-virtual {p0}, Landroid/widget/Editor;->hasInsertionController()Z

    #@5f
    move-result v3

    #@60
    if-eqz v3, :cond_59

    #@62
    .line 1937
    invoke-virtual {p0}, Landroid/widget/Editor;->getInsertionController()Landroid/widget/Editor$InsertionPointCursorController;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Landroid/widget/Editor$InsertionPointCursorController;->show()V

    #@69
    goto :goto_59
.end method

.method onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasWindowFocus"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1250
    iput-boolean p1, p0, Landroid/widget/Editor;->mHasWindowFocus:Z

    #@3
    .line 1252
    if-eqz p1, :cond_49

    #@5
    .line 1253
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 1254
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@b
    invoke-virtual {v0}, Landroid/widget/Editor$Blink;->uncancel()V

    #@e
    .line 1255
    invoke-virtual {p0}, Landroid/widget/Editor;->makeBlink()V

    #@11
    .line 1258
    :cond_11
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@13
    if-eqz v0, :cond_39

    #@15
    .line 1259
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@17
    if-eqz v0, :cond_39

    #@19
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@1b
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isServiceConnected()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_39

    #@21
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@23
    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_39

    #@29
    .line 1260
    iget-object v0, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@2b
    if-nez v0, :cond_3a

    #@2d
    .line 1261
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2f
    iget-object v1, p0, Landroid/widget/Editor;->mDefaultPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@31
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@34
    .line 1262
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@36
    invoke-interface {v0, v2}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@39
    .line 1310
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 1264
    :cond_3a
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@3c
    iget-object v1, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@3e
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;)V

    #@41
    .line 1265
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@43
    iget v1, p0, Landroid/widget/Editor;->mClipDataType:I

    #@45
    invoke-interface {v0, v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->setInputType(I)V

    #@48
    goto :goto_39

    #@49
    .line 1270
    :cond_49
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@4b
    if-eqz v0, :cond_70

    #@4d
    iget-boolean v0, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@4f
    if-eqz v0, :cond_70

    #@51
    .line 1274
    invoke-virtual {p0, v2}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@54
    .line 1275
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@57
    .line 1276
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@59
    invoke-virtual {v0}, Landroid/widget/TextView;->hasSelection()Z

    #@5c
    move-result v0

    #@5d
    if-eqz v0, :cond_70

    #@5f
    .line 1277
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@61
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@64
    move-result-object v0

    #@65
    check-cast v0, Landroid/text/Spannable;

    #@67
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@69
    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    #@6c
    move-result v1

    #@6d
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@70
    .line 1281
    :cond_70
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@72
    if-eqz v0, :cond_94

    #@74
    .line 1282
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@76
    if-eqz v0, :cond_94

    #@78
    .line 1284
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@7a
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@7d
    move-result v0

    #@7e
    if-eqz v0, :cond_88

    #@80
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@82
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->isCliptraycueShowing()Z

    #@85
    move-result v0

    #@86
    if-eqz v0, :cond_94

    #@88
    .line 1286
    :cond_88
    const-string v0, "cliptray_editer"

    #@8a
    const-string v1, "Editor::onWindowFocusChanged, hideCliptray()"

    #@8c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 1287
    iget-object v0, p0, Landroid/widget/Editor;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@91
    invoke-interface {v0}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->hideCliptray()V

    #@94
    .line 1292
    :cond_94
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@96
    if-eqz v0, :cond_9d

    #@98
    .line 1293
    iget-object v0, p0, Landroid/widget/Editor;->mBlink:Landroid/widget/Editor$Blink;

    #@9a
    invoke-virtual {v0}, Landroid/widget/Editor$Blink;->cancel()V

    #@9d
    .line 1295
    :cond_9d
    iget-object v0, p0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@9f
    if-eqz v0, :cond_a5

    #@a1
    .line 1296
    iget-object v0, p0, Landroid/widget/Editor;->mInputContentType:Landroid/widget/Editor$InputContentType;

    #@a3
    iput-boolean v2, v0, Landroid/widget/Editor$InputContentType;->enterDown:Z

    #@a5
    .line 1300
    :cond_a5
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@a7
    if-nez v0, :cond_ac

    #@a9
    .line 1301
    invoke-virtual {p0}, Landroid/widget/Editor;->hideControllers()V

    #@ac
    .line 1303
    :cond_ac
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@ae
    if-eqz v0, :cond_b5

    #@b0
    .line 1304
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@b2
    invoke-virtual {v0}, Landroid/widget/Editor$SuggestionsPopupWindow;->onParentLostFocus()V

    #@b5
    .line 1308
    :cond_b5
    invoke-virtual {p0}, Landroid/widget/Editor;->ensureEndedBatchEdit()V

    #@b8
    goto :goto_39
.end method

.method public performLongClick(Z)Z
    .registers 15
    .parameter "handled"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 955
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@4
    if-eqz v8, :cond_e

    #@6
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8
    iget-boolean v8, v8, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@a
    if-nez v8, :cond_e

    #@c
    move v8, v9

    #@d
    .line 1031
    :goto_d
    return v8

    #@e
    .line 958
    :cond_e
    if-nez p1, :cond_45

    #@10
    iget v8, p0, Landroid/widget/Editor;->mLastDownPositionX:F

    #@12
    iget v10, p0, Landroid/widget/Editor;->mLastDownPositionY:F

    #@14
    invoke-direct {p0, v8, v10}, Landroid/widget/Editor;->isPositionOnText(FF)Z

    #@17
    move-result v8

    #@18
    if-nez v8, :cond_45

    #@1a
    iget-boolean v8, p0, Landroid/widget/Editor;->mInsertionControllerEnabled:Z

    #@1c
    if-eqz v8, :cond_45

    #@1e
    .line 960
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@20
    if-eqz v8, :cond_25

    #@22
    invoke-virtual {p0, v9}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@25
    .line 961
    :cond_25
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@27
    iget v10, p0, Landroid/widget/Editor;->mLastDownPositionX:F

    #@29
    iget v11, p0, Landroid/widget/Editor;->mLastDownPositionY:F

    #@2b
    invoke-virtual {v8, v10, v11}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@2e
    move-result v4

    #@2f
    .line 963
    .local v4, offset:I
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@32
    .line 964
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@34
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@37
    move-result-object v8

    #@38
    check-cast v8, Landroid/text/Spannable;

    #@3a
    invoke-static {v8, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@3d
    .line 965
    invoke-virtual {p0}, Landroid/widget/Editor;->getInsertionController()Landroid/widget/Editor$InsertionPointCursorController;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Landroid/widget/Editor$InsertionPointCursorController;->showWithActionPopup()V

    #@44
    .line 966
    const/4 p1, 0x1

    #@45
    .line 969
    .end local v4           #offset:I
    :cond_45
    if-nez p1, :cond_eb

    #@47
    invoke-direct {p0}, Landroid/widget/Editor;->hasPasswordTransformationMethod()Z

    #@4a
    move-result v8

    #@4b
    if-nez v8, :cond_eb

    #@4d
    invoke-virtual {p0}, Landroid/widget/Editor;->isShowingBubblePopup()Z

    #@50
    move-result v8

    #@51
    if-eqz v8, :cond_eb

    #@53
    invoke-direct {p0}, Landroid/widget/Editor;->isLockscreen()Z

    #@56
    move-result v8

    #@57
    if-nez v8, :cond_eb

    #@59
    iget-object v8, p0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@5b
    if-nez v8, :cond_61

    #@5d
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@5f
    if-eqz v8, :cond_eb

    #@61
    .line 970
    :cond_61
    invoke-direct {p0}, Landroid/widget/Editor;->touchPositionIsInSelection()Z

    #@64
    move-result v8

    #@65
    if-eqz v8, :cond_105

    #@67
    .line 971
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@69
    if-eqz v8, :cond_6f

    #@6b
    .line 972
    const/4 p1, 0x1

    #@6c
    .line 973
    invoke-virtual {p0, v9}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@6f
    .line 976
    :cond_6f
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@71
    invoke-virtual {v8}, Landroid/widget/TextView;->getSelectionStart()I

    #@74
    move-result v7

    #@75
    .line 977
    .local v7, start:I
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@77
    invoke-virtual {v8}, Landroid/widget/TextView;->getSelectionEnd()I

    #@7a
    move-result v1

    #@7b
    .line 978
    .local v1, end:I
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@7d
    invoke-virtual {v8, v7, v1}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    #@80
    move-result-object v5

    #@81
    .line 980
    .local v5, selectedText:Ljava/lang/CharSequence;
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@83
    if-eqz v8, :cond_a9

    #@85
    .line 982
    instance-of v8, v5, Landroid/text/Spanned;

    #@87
    if-eqz v8, :cond_a9

    #@89
    .line 984
    instance-of v8, v5, Landroid/text/Spannable;

    #@8b
    if-eqz v8, :cond_a2

    #@8d
    move-object v6, v5

    #@8e
    .line 985
    check-cast v6, Landroid/text/Spannable;

    #@90
    .line 991
    .local v6, spannable:Landroid/text/Spannable;
    :goto_90
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@93
    move-result v8

    #@94
    const-class v10, Landroid/text/style/DynamicDrawableSpan;

    #@96
    invoke-interface {v6, v9, v8, v10}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@99
    move-result-object v2

    #@9a
    check-cast v2, [Landroid/text/style/DynamicDrawableSpan;

    #@9c
    .line 992
    .local v2, image:[Landroid/text/style/DynamicDrawableSpan;
    array-length v8, v2

    #@9d
    if-lez v8, :cond_a9

    #@9f
    move v8, v9

    #@a0
    .line 993
    goto/16 :goto_d

    #@a2
    .line 987
    .end local v2           #image:[Landroid/text/style/DynamicDrawableSpan;
    .end local v6           #spannable:Landroid/text/Spannable;
    :cond_a2
    new-instance v6, Landroid/text/SpannableString;

    #@a4
    invoke-direct {v6, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    #@a7
    .line 988
    .restart local v6       #spannable:Landroid/text/Spannable;
    move-object v5, v6

    #@a8
    goto :goto_90

    #@a9
    .line 997
    .end local v6           #spannable:Landroid/text/Spannable;
    :cond_a9
    const/4 v8, 0x0

    #@aa
    invoke-static {v8, v5}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@ad
    move-result-object v0

    #@ae
    .line 998
    .local v0, data:Landroid/content/ClipData;
    new-instance v3, Landroid/widget/Editor$DragLocalState;

    #@b0
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@b2
    invoke-direct {v3, v8, v7, v1}, Landroid/widget/Editor$DragLocalState;-><init>(Landroid/widget/TextView;II)V

    #@b5
    .line 1000
    .local v3, localState:Landroid/widget/Editor$DragLocalState;
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@b7
    if-nez v8, :cond_fb

    #@b9
    .line 1001
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@bb
    invoke-direct {p0, v5}, Landroid/widget/Editor;->getTextThumbnailBuilder(Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;

    #@be
    move-result-object v10

    #@bf
    invoke-virtual {v8, v0, v10, v3, v9}, Landroid/widget/TextView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    #@c2
    .line 1006
    :goto_c2
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@c4
    if-eqz v8, :cond_e3

    #@c6
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@c8
    invoke-virtual {v8}, Landroid/widget/TextView;->hasSelection()Z

    #@cb
    move-result v8

    #@cc
    if-eqz v8, :cond_e3

    #@ce
    .line 1007
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@d0
    iget v9, p0, Landroid/widget/Editor;->mLastDownPositionX:F

    #@d2
    iget v10, p0, Landroid/widget/Editor;->mLastDownPositionY:F

    #@d4
    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    #@d7
    move-result v4

    #@d8
    .line 1008
    .restart local v4       #offset:I
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@da
    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@dd
    move-result-object v8

    #@de
    check-cast v8, Landroid/text/Spannable;

    #@e0
    invoke-static {v8, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@e3
    .line 1010
    .end local v4           #offset:I
    :cond_e3
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@e6
    .line 1022
    .end local v0           #data:Landroid/content/ClipData;
    .end local v1           #end:I
    .end local v3           #localState:Landroid/widget/Editor$DragLocalState;
    .end local v5           #selectedText:Ljava/lang/CharSequence;
    .end local v7           #start:I
    :goto_e6
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@e8
    if-nez v8, :cond_eb

    #@ea
    const/4 p1, 0x1

    #@eb
    .line 1026
    :cond_eb
    if-nez p1, :cond_f8

    #@ed
    .line 1027
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@ef
    if-eqz v8, :cond_f4

    #@f1
    invoke-virtual {p0, v12}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@f4
    .line 1028
    :cond_f4
    invoke-virtual {p0}, Landroid/widget/Editor;->startSelectionActionMode()Z

    #@f7
    move-result p1

    #@f8
    :cond_f8
    move v8, p1

    #@f9
    .line 1031
    goto/16 :goto_d

    #@fb
    .line 1003
    .restart local v0       #data:Landroid/content/ClipData;
    .restart local v1       #end:I
    .restart local v3       #localState:Landroid/widget/Editor$DragLocalState;
    .restart local v5       #selectedText:Ljava/lang/CharSequence;
    .restart local v7       #start:I
    :cond_fb
    iget-object v8, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@fd
    invoke-direct {p0, v5}, Landroid/widget/Editor;->getTextThumbnailBuilder(Ljava/lang/CharSequence;)Landroid/view/View$DragShadowBuilder;

    #@100
    move-result-object v9

    #@101
    invoke-virtual {v8, v0, v9, v3, v12}, Landroid/widget/TextView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    #@104
    goto :goto_c2

    #@105
    .line 1012
    .end local v0           #data:Landroid/content/ClipData;
    .end local v1           #end:I
    .end local v3           #localState:Landroid/widget/Editor$DragLocalState;
    .end local v5           #selectedText:Ljava/lang/CharSequence;
    .end local v7           #start:I
    :cond_105
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@107
    if-nez v8, :cond_11b

    #@109
    .line 1013
    invoke-virtual {p0}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@10c
    move-result-object v8

    #@10d
    invoke-virtual {v8}, Landroid/widget/Editor$SelectionModifierCursorController;->hide()V

    #@110
    .line 1014
    invoke-direct {p0}, Landroid/widget/Editor;->selectCurrentWord()Z

    #@113
    .line 1015
    invoke-virtual {p0}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@116
    move-result-object v8

    #@117
    invoke-virtual {v8}, Landroid/widget/Editor$SelectionModifierCursorController;->show()V

    #@11a
    goto :goto_e6

    #@11b
    .line 1017
    :cond_11b
    invoke-virtual {p0, v9}, Landroid/widget/Editor;->setShowingBubblePopup(Z)V

    #@11e
    .line 1018
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@121
    .line 1019
    invoke-direct {p0}, Landroid/widget/Editor;->selectCurrentWord()Z

    #@124
    goto :goto_e6
.end method

.method prepareCursorControllers()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 525
    const/4 v3, 0x0

    #@4
    .line 527
    .local v3, windowSupportsHandles:Z
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v6}, Landroid/widget/TextView;->getRootView()Landroid/view/View;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@d
    move-result-object v1

    #@e
    .line 528
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v6, v1, Landroid/view/WindowManager$LayoutParams;

    #@10
    if-eqz v6, :cond_22

    #@12
    move-object v2, v1

    #@13
    .line 529
    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    #@15
    .line 530
    .local v2, windowParams:Landroid/view/WindowManager$LayoutParams;
    iget v6, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@17
    const/16 v7, 0x3e8

    #@19
    if-lt v6, v7, :cond_21

    #@1b
    iget v6, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1d
    const/16 v7, 0x7cf

    #@1f
    if-le v6, v7, :cond_69

    #@21
    :cond_21
    move v3, v5

    #@22
    .line 534
    .end local v2           #windowParams:Landroid/view/WindowManager$LayoutParams;
    :cond_22
    :goto_22
    if-eqz v3, :cond_6b

    #@24
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@26
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@29
    move-result-object v6

    #@2a
    if-eqz v6, :cond_6b

    #@2c
    move v0, v5

    #@2d
    .line 535
    .local v0, enabled:Z
    :goto_2d
    if-eqz v0, :cond_6d

    #@2f
    invoke-virtual {p0}, Landroid/widget/Editor;->isCursorVisible()Z

    #@32
    move-result v6

    #@33
    if-eqz v6, :cond_6d

    #@35
    move v6, v5

    #@36
    :goto_36
    iput-boolean v6, p0, Landroid/widget/Editor;->mInsertionControllerEnabled:Z

    #@38
    .line 536
    if-eqz v0, :cond_6f

    #@3a
    iget-object v6, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@3c
    invoke-virtual {v6}, Landroid/widget/TextView;->textCanBeSelected()Z

    #@3f
    move-result v6

    #@40
    if-eqz v6, :cond_6f

    #@42
    :goto_42
    iput-boolean v5, p0, Landroid/widget/Editor;->mSelectionControllerEnabled:Z

    #@44
    .line 538
    iget-boolean v4, p0, Landroid/widget/Editor;->mInsertionControllerEnabled:Z

    #@46
    if-nez v4, :cond_56

    #@48
    .line 539
    invoke-direct {p0}, Landroid/widget/Editor;->hideInsertionPointCursorController()V

    #@4b
    .line 540
    iget-object v4, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@4d
    if-eqz v4, :cond_56

    #@4f
    .line 541
    iget-object v4, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@51
    invoke-virtual {v4}, Landroid/widget/Editor$InsertionPointCursorController;->onDetached()V

    #@54
    .line 542
    iput-object v8, p0, Landroid/widget/Editor;->mInsertionPointCursorController:Landroid/widget/Editor$InsertionPointCursorController;

    #@56
    .line 546
    :cond_56
    iget-boolean v4, p0, Landroid/widget/Editor;->mSelectionControllerEnabled:Z

    #@58
    if-nez v4, :cond_68

    #@5a
    .line 547
    invoke-virtual {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    #@5d
    .line 548
    iget-object v4, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@5f
    if-eqz v4, :cond_68

    #@61
    .line 549
    iget-object v4, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@63
    invoke-virtual {v4}, Landroid/widget/Editor$SelectionModifierCursorController;->onDetached()V

    #@66
    .line 550
    iput-object v8, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@68
    .line 553
    :cond_68
    return-void

    #@69
    .end local v0           #enabled:Z
    .restart local v2       #windowParams:Landroid/view/WindowManager$LayoutParams;
    :cond_69
    move v3, v4

    #@6a
    .line 530
    goto :goto_22

    #@6b
    .end local v2           #windowParams:Landroid/view/WindowManager$LayoutParams;
    :cond_6b
    move v0, v4

    #@6c
    .line 534
    goto :goto_2d

    #@6d
    .restart local v0       #enabled:Z
    :cond_6d
    move v6, v4

    #@6e
    .line 535
    goto :goto_36

    #@6f
    :cond_6f
    move v5, v4

    #@70
    .line 536
    goto :goto_42
.end method

.method reportExtractedText()Z
    .registers 12

    #@0
    .prologue
    const/4 v10, -0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1487
    iget-object v8, p0, Landroid/widget/Editor;->mInputMethodState:Landroid/widget/Editor$InputMethodState;

    #@4
    .line 1488
    .local v8, ims:Landroid/widget/Editor$InputMethodState;
    if-eqz v8, :cond_47

    #@6
    .line 1489
    iget-boolean v6, v8, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@8
    .line 1490
    .local v6, contentChanged:Z
    if-nez v6, :cond_e

    #@a
    iget-boolean v0, v8, Landroid/widget/Editor$InputMethodState;->mSelectionModeChanged:Z

    #@c
    if-eqz v0, :cond_47

    #@e
    .line 1491
    :cond_e
    iput-boolean v9, v8, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@10
    .line 1492
    iput-boolean v9, v8, Landroid/widget/Editor$InputMethodState;->mSelectionModeChanged:Z

    #@12
    .line 1493
    iget-object v1, v8, Landroid/widget/Editor$InputMethodState;->mExtractedTextRequest:Landroid/view/inputmethod/ExtractedTextRequest;

    #@14
    .line 1494
    .local v1, req:Landroid/view/inputmethod/ExtractedTextRequest;
    if-eqz v1, :cond_47

    #@16
    .line 1495
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@19
    move-result-object v7

    #@1a
    .line 1496
    .local v7, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v7, :cond_47

    #@1c
    .line 1501
    iget v0, v8, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@1e
    if-gez v0, :cond_25

    #@20
    if-nez v6, :cond_25

    #@22
    .line 1502
    const/4 v0, -0x2

    #@23
    iput v0, v8, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@25
    .line 1504
    :cond_25
    iget v2, v8, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@27
    iget v3, v8, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@29
    iget v4, v8, Landroid/widget/Editor$InputMethodState;->mChangedDelta:I

    #@2b
    iget-object v5, v8, Landroid/widget/Editor$InputMethodState;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@2d
    move-object v0, p0

    #@2e
    invoke-direct/range {v0 .. v5}, Landroid/widget/Editor;->extractTextInternal(Landroid/view/inputmethod/ExtractedTextRequest;IIILandroid/view/inputmethod/ExtractedText;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_47

    #@34
    .line 1512
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@36
    iget v2, v1, Landroid/view/inputmethod/ExtractedTextRequest;->token:I

    #@38
    iget-object v3, v8, Landroid/widget/Editor$InputMethodState;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@3a
    invoke-virtual {v7, v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->updateExtractedText(Landroid/view/View;ILandroid/view/inputmethod/ExtractedText;)V

    #@3d
    .line 1513
    iput v10, v8, Landroid/widget/Editor$InputMethodState;->mChangedStart:I

    #@3f
    .line 1514
    iput v10, v8, Landroid/widget/Editor$InputMethodState;->mChangedEnd:I

    #@41
    .line 1515
    iput v9, v8, Landroid/widget/Editor$InputMethodState;->mChangedDelta:I

    #@43
    .line 1516
    iput-boolean v9, v8, Landroid/widget/Editor$InputMethodState;->mContentChanged:Z

    #@45
    .line 1517
    const/4 v0, 0x1

    #@46
    .line 1523
    .end local v1           #req:Landroid/view/inputmethod/ExtractedTextRequest;
    .end local v6           #contentChanged:Z
    .end local v7           #imm:Landroid/view/inputmethod/InputMethodManager;
    :goto_46
    return v0

    #@47
    :cond_47
    move v0, v9

    #@48
    goto :goto_46
.end method

.method sendOnTextChanged(II)V
    .registers 5
    .parameter "start"
    .parameter "after"

    #@0
    .prologue
    .line 1224
    add-int v0, p1, p2

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, p1, v0, v1}, Landroid/widget/Editor;->updateSpellCheckSpans(IIZ)V

    #@6
    .line 1229
    invoke-direct {p0}, Landroid/widget/Editor;->hideCursorControllers()V

    #@9
    .line 1230
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "error"
    .parameter "icon"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 390
    invoke-static {p1}, Landroid/text/TextUtils;->stringOrSpannedString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@4
    move-result-object v0

    #@5
    iput-object v0, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@7
    .line 391
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/widget/Editor;->mErrorWasChanged:Z

    #@a
    .line 393
    iget-object v0, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@c
    if-nez v0, :cond_25

    #@e
    .line 394
    invoke-direct {p0, v1}, Landroid/widget/Editor;->setErrorIcon(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 395
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@13
    if-eqz v0, :cond_24

    #@15
    .line 396
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@17
    invoke-virtual {v0}, Landroid/widget/Editor$ErrorPopup;->isShowing()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_22

    #@1d
    .line 397
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@1f
    invoke-virtual {v0}, Landroid/widget/Editor$ErrorPopup;->dismiss()V

    #@22
    .line 400
    :cond_22
    iput-object v1, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@24
    .line 409
    :cond_24
    :goto_24
    return-void

    #@25
    .line 404
    :cond_25
    invoke-direct {p0, p2}, Landroid/widget/Editor;->setErrorIcon(Landroid/graphics/drawable/Drawable;)V

    #@28
    .line 405
    iget-object v0, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2a
    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_24

    #@30
    .line 406
    invoke-direct {p0}, Landroid/widget/Editor;->showError()V

    #@33
    goto :goto_24
.end method

.method setFrame()V
    .registers 8

    #@0
    .prologue
    .line 667
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@2
    if-eqz v0, :cond_2e

    #@4
    .line 668
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$ErrorPopup;->getContentView()Landroid/view/View;

    #@9
    move-result-object v6

    #@a
    check-cast v6, Landroid/widget/TextView;

    #@c
    .line 669
    .local v6, tv:Landroid/widget/TextView;
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@e
    iget-object v1, p0, Landroid/widget/Editor;->mError:Ljava/lang/CharSequence;

    #@10
    invoke-direct {p0, v0, v1, v6}, Landroid/widget/Editor;->chooseSize(Landroid/widget/PopupWindow;Ljava/lang/CharSequence;Landroid/widget/TextView;)V

    #@13
    .line 670
    iget-object v0, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@15
    iget-object v1, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@17
    invoke-direct {p0}, Landroid/widget/Editor;->getErrorX()I

    #@1a
    move-result v2

    #@1b
    invoke-direct {p0}, Landroid/widget/Editor;->getErrorY()I

    #@1e
    move-result v3

    #@1f
    iget-object v4, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@21
    invoke-virtual {v4}, Landroid/widget/Editor$ErrorPopup;->getWidth()I

    #@24
    move-result v4

    #@25
    iget-object v5, p0, Landroid/widget/Editor;->mErrorPopup:Landroid/widget/Editor$ErrorPopup;

    #@27
    invoke-virtual {v5}, Landroid/widget/Editor$ErrorPopup;->getHeight()I

    #@2a
    move-result v5

    #@2b
    invoke-virtual/range {v0 .. v5}, Landroid/widget/Editor$ErrorPopup;->update(Landroid/view/View;IIII)V

    #@2e
    .line 673
    .end local v6           #tv:Landroid/widget/TextView;
    :cond_2e
    return-void
.end method

.method public setPasteListener(Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;I)V
    .registers 3
    .parameter "listener"
    .parameter "type"

    #@0
    .prologue
    .line 4981
    iput-object p1, p0, Landroid/widget/Editor;->mCliptrayPasteListener:Lcom/lge/loader/cliptray/ICliptrayManagerLoader$OnPasteListener;

    #@2
    .line 4982
    iput p2, p0, Landroid/widget/Editor;->mClipDataType:I

    #@4
    .line 4983
    return-void
.end method

.method setShowingBubblePopup(Z)V
    .registers 5
    .parameter "showing"

    #@0
    .prologue
    .line 1035
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1041
    :goto_4
    return-void

    #@5
    .line 1036
    :cond_5
    iget-object v0, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 1037
    iget-object v0, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@b
    invoke-virtual {v0, p1}, Landroid/widget/BubblePopupHelper;->setShowingBubblePopup(Z)V

    #@e
    goto :goto_4

    #@f
    .line 1039
    :cond_f
    const-string v0, "Editor"

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "Because BubblePopupHelper is null, popup-showing status could not be set by showing = "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_4
.end method

.method showSuggestions()V
    .registers 2

    #@0
    .prologue
    .line 2041
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 2042
    new-instance v0, Landroid/widget/Editor$SuggestionsPopupWindow;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/Editor$SuggestionsPopupWindow;-><init>(Landroid/widget/Editor;)V

    #@9
    iput-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@b
    .line 2044
    :cond_b
    invoke-virtual {p0}, Landroid/widget/Editor;->hideControllers()V

    #@e
    .line 2045
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@10
    invoke-virtual {v0}, Landroid/widget/Editor$SuggestionsPopupWindow;->show()V

    #@13
    .line 2046
    return-void
.end method

.method startSelectionActionMode()Z
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v11, 0x0

    #@3
    .line 1815
    iget-object v9, p0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@5
    if-nez v9, :cond_f

    #@7
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@9
    if-eqz v9, :cond_10

    #@b
    iget-boolean v9, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@d
    if-eqz v9, :cond_10

    #@f
    .line 1875
    :cond_f
    :goto_f
    return v11

    #@10
    .line 1820
    :cond_10
    invoke-direct {p0}, Landroid/widget/Editor;->canSelectText()Z

    #@13
    move-result v9

    #@14
    if-eqz v9, :cond_1e

    #@16
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@18
    invoke-virtual {v9}, Landroid/widget/TextView;->requestFocus()Z

    #@1b
    move-result v9

    #@1c
    if-nez v9, :cond_26

    #@1e
    .line 1821
    :cond_1e
    const-string v9, "TextView"

    #@20
    const-string v10, "TextView does not support text selection. Action mode cancelled."

    #@22
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_f

    #@26
    .line 1826
    :cond_26
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@28
    invoke-virtual {v9}, Landroid/widget/TextView;->hasSelection()Z

    #@2b
    move-result v9

    #@2c
    if-nez v9, :cond_34

    #@2e
    .line 1828
    invoke-direct {p0}, Landroid/widget/Editor;->selectCurrentWord()Z

    #@31
    move-result v9

    #@32
    if-eqz v9, :cond_f

    #@34
    .line 1834
    :cond_34
    invoke-direct {p0}, Landroid/widget/Editor;->extractedTextModeWillBeStarted()Z

    #@37
    move-result v8

    #@38
    .line 1838
    .local v8, willExtract:Z
    if-eqz v8, :cond_3e

    #@3a
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@3c
    if-eqz v9, :cond_4b

    #@3e
    .line 1839
    :cond_3e
    new-instance v0, Landroid/widget/Editor$SelectionActionModeCallback;

    #@40
    invoke-direct {v0, p0, v12}, Landroid/widget/Editor$SelectionActionModeCallback;-><init>(Landroid/widget/Editor;Landroid/widget/Editor$1;)V

    #@43
    .line 1840
    .local v0, actionModeCallback:Landroid/view/ActionMode$Callback;
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@45
    invoke-virtual {v9, v0}, Landroid/widget/TextView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@48
    move-result-object v9

    #@49
    iput-object v9, p0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@4b
    .line 1843
    .end local v0           #actionModeCallback:Landroid/view/ActionMode$Callback;
    :cond_4b
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@4d
    if-eqz v9, :cond_d4

    #@4f
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@51
    invoke-virtual {v9}, Landroid/widget/TextView;->hasSelection()Z

    #@54
    move-result v9

    #@55
    if-eqz v9, :cond_d4

    #@57
    iget-object v9, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@59
    invoke-virtual {v9}, Landroid/widget/BubblePopupHelper;->checkIsPossibleShowBubblePopup()Z

    #@5c
    move-result v9

    #@5d
    if-eqz v9, :cond_d4

    #@5f
    move v7, v10

    #@60
    .line 1846
    .local v7, startBubblepopup:Z
    :goto_60
    iget-object v9, p0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@62
    if-nez v9, :cond_74

    #@64
    if-nez v8, :cond_74

    #@66
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@68
    invoke-virtual {v9}, Landroid/widget/TextView;->hasSelection()Z

    #@6b
    move-result v9

    #@6c
    if-nez v9, :cond_74

    #@6e
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@70
    if-eqz v9, :cond_d6

    #@72
    if-eqz v7, :cond_d6

    #@74
    :cond_74
    move v6, v10

    #@75
    .line 1849
    .local v6, selectionStarted:Z
    :goto_75
    if-eqz v6, :cond_8e

    #@77
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@79
    invoke-virtual {v9}, Landroid/widget/TextView;->isTextSelectable()Z

    #@7c
    move-result v9

    #@7d
    if-nez v9, :cond_8e

    #@7f
    iget-boolean v9, p0, Landroid/widget/Editor;->mShowSoftInputOnFocus:Z

    #@81
    if-eqz v9, :cond_8e

    #@83
    .line 1851
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@86
    move-result-object v3

    #@87
    .line 1852
    .local v3, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v3, :cond_8e

    #@89
    .line 1853
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@8b
    invoke-virtual {v3, v9, v11, v12}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    #@8e
    .line 1857
    .end local v3           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_8e
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@90
    if-eqz v9, :cond_c9

    #@92
    if-eqz v7, :cond_c9

    #@94
    .line 1858
    invoke-virtual {p0}, Landroid/widget/Editor;->getSelectionController()Landroid/widget/Editor$SelectionModifierCursorController;

    #@97
    move-result-object v1

    #@98
    .line 1859
    .local v1, controller:Landroid/widget/Editor$SelectionModifierCursorController;
    if-eqz v1, :cond_c4

    #@9a
    .line 1860
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@9c
    invoke-virtual {v9}, Landroid/widget/TextView;->getSelectionStart()I

    #@9f
    move-result v5

    #@a0
    .line 1861
    .local v5, selStart:I
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@a2
    invoke-virtual {v9}, Landroid/widget/TextView;->getSelectionEnd()I

    #@a5
    move-result v4

    #@a6
    .line 1862
    .local v4, selEnd:I
    if-le v5, v4, :cond_b6

    #@a8
    .line 1863
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@aa
    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@ad
    move-result-object v9

    #@ae
    check-cast v9, Landroid/text/Spannable;

    #@b0
    invoke-static {v9, v4, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@b3
    .line 1864
    invoke-virtual {v1, v11}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@b6
    .line 1867
    :cond_b6
    iput-boolean v10, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@b8
    .line 1868
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionModifierCursorController;->show()V

    #@bb
    .line 1869
    invoke-virtual {v1}, Landroid/widget/Editor$SelectionModifierCursorController;->getStartHandle()Landroid/widget/Editor$SelectionStartHandleView;

    #@be
    move-result-object v2

    #@bf
    .line 1870
    .local v2, handle:Landroid/widget/Editor$HandleView;
    if-eqz v2, :cond_c4

    #@c1
    invoke-virtual {v2, v11}, Landroid/widget/Editor$HandleView;->showActionPopupWindow(I)V

    #@c4
    .line 1872
    .end local v2           #handle:Landroid/widget/Editor$HandleView;
    .end local v4           #selEnd:I
    .end local v5           #selStart:I
    :cond_c4
    iget-object v9, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@c6
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setHasTransientState(Z)V

    #@c9
    .line 1875
    .end local v1           #controller:Landroid/widget/Editor$SelectionModifierCursorController;
    :cond_c9
    if-nez v6, :cond_d1

    #@cb
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@cd
    if-eqz v9, :cond_f

    #@cf
    if-eqz v7, :cond_f

    #@d1
    :cond_d1
    move v11, v10

    #@d2
    goto/16 :goto_f

    #@d4
    .end local v6           #selectionStarted:Z
    .end local v7           #startBubblepopup:Z
    :cond_d4
    move v7, v11

    #@d5
    .line 1843
    goto :goto_60

    #@d6
    .restart local v7       #startBubblepopup:Z
    :cond_d6
    move v6, v11

    #@d7
    .line 1846
    goto :goto_75
.end method

.method protected stopSelectionActionMode()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1944
    iget-object v3, p0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@3
    if-eqz v3, :cond_a

    #@5
    .line 1946
    iget-object v3, p0, Landroid/widget/Editor;->mSelectionActionMode:Landroid/view/ActionMode;

    #@7
    invoke-virtual {v3}, Landroid/view/ActionMode;->finish()V

    #@a
    .line 1949
    :cond_a
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@c
    if-eqz v3, :cond_40

    #@e
    iget-boolean v3, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@10
    if-eqz v3, :cond_40

    #@12
    iget-object v3, p0, Landroid/widget/Editor;->mBubblePopupHelper:Landroid/widget/BubblePopupHelper;

    #@14
    invoke-virtual {v3}, Landroid/widget/BubblePopupHelper;->checkIsPossibleHideBubblePopup()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_40

    #@1a
    iget-object v3, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@1c
    if-eqz v3, :cond_40

    #@1e
    .line 1951
    iput-boolean v4, p0, Landroid/widget/Editor;->mIsAleadyBubblePopupStarted:Z

    #@20
    .line 1952
    iget-object v3, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@22
    invoke-virtual {v3}, Landroid/widget/Editor$SelectionModifierCursorController;->hide()V

    #@25
    .line 1954
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@27
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionStart()I

    #@2a
    move-result v2

    #@2b
    .line 1955
    .local v2, selStart:I
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@2d
    invoke-virtual {v3}, Landroid/widget/TextView;->getSelectionEnd()I

    #@30
    move-result v1

    #@31
    .line 1956
    .local v1, selEnd:I
    iget-object v3, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@33
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@36
    move-result-object v0

    #@37
    .line 1958
    .local v0, layout:Landroid/text/Layout;
    if-gt v2, v1, :cond_40

    #@39
    if-eqz v0, :cond_40

    #@3b
    .line 1959
    iget-object v3, p0, Landroid/widget/Editor;->mSelectionModifierCursorController:Landroid/widget/Editor$SelectionModifierCursorController;

    #@3d
    invoke-virtual {v3, v4}, Landroid/widget/Editor$SelectionModifierCursorController;->crossHandles(Z)V

    #@40
    .line 1962
    .end local v0           #layout:Landroid/text/Layout;
    .end local v1           #selEnd:I
    .end local v2           #selStart:I
    :cond_40
    return-void
.end method

.method public suggestionPopupWindowWhenDetached()V
    .registers 2

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 356
    iget-object v0, p0, Landroid/widget/Editor;->mSuggestionsPopupWindow:Landroid/widget/Editor$SuggestionsPopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/Editor$SuggestionsPopupWindow;->onParentLostFocus()V

    #@9
    .line 358
    :cond_9
    return-void
.end method

.method updateCursorsPositions()V
    .registers 12

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    const/4 v8, 0x2

    #@3
    .line 1774
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@5
    iget v7, v7, Landroid/widget/TextView;->mCursorDrawableRes:I

    #@7
    if-nez v7, :cond_c

    #@9
    .line 1775
    iput v10, p0, Landroid/widget/Editor;->mCursorCount:I

    #@b
    .line 1799
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1779
    :cond_c
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@e
    invoke-virtual {v7}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@11
    move-result-object v2

    #@12
    .line 1780
    .local v2, layout:Landroid/text/Layout;
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@14
    invoke-virtual {v7}, Landroid/widget/TextView;->getHintLayout()Landroid/text/Layout;

    #@17
    move-result-object v1

    #@18
    .line 1781
    .local v1, hintLayout:Landroid/text/Layout;
    iget-object v7, p0, Landroid/widget/Editor;->mTextView:Landroid/widget/TextView;

    #@1a
    invoke-virtual {v7}, Landroid/widget/TextView;->getSelectionStart()I

    #@1d
    move-result v5

    #@1e
    .line 1782
    .local v5, offset:I
    invoke-virtual {v2, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    #@21
    move-result v3

    #@22
    .line 1783
    .local v3, line:I
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineTop(I)I

    #@25
    move-result v6

    #@26
    .line 1784
    .local v6, top:I
    add-int/lit8 v7, v3, 0x1

    #@28
    invoke-virtual {v2, v7}, Landroid/text/Layout;->getLineTop(I)I

    #@2b
    move-result v0

    #@2c
    .line 1786
    .local v0, bottom:I
    invoke-virtual {v2, v5}, Landroid/text/Layout;->isLevelBoundary(I)Z

    #@2f
    move-result v7

    #@30
    if-eqz v7, :cond_51

    #@32
    move v7, v8

    #@33
    :goto_33
    iput v7, p0, Landroid/widget/Editor;->mCursorCount:I

    #@35
    .line 1788
    move v4, v0

    #@36
    .line 1789
    .local v4, middle:I
    iget v7, p0, Landroid/widget/Editor;->mCursorCount:I

    #@38
    if-ne v7, v8, :cond_3e

    #@3a
    .line 1791
    add-int v7, v6, v0

    #@3c
    shr-int/lit8 v4, v7, 0x1

    #@3e
    .line 1794
    :cond_3e
    invoke-direct {p0, v2, v1, v5}, Landroid/widget/Editor;->getPrimaryHorizontal(Landroid/text/Layout;Landroid/text/Layout;I)F

    #@41
    move-result v7

    #@42
    invoke-direct {p0, v10, v6, v4, v7}, Landroid/widget/Editor;->updateCursorPosition(IIIF)V

    #@45
    .line 1796
    iget v7, p0, Landroid/widget/Editor;->mCursorCount:I

    #@47
    if-ne v7, v8, :cond_b

    #@49
    .line 1797
    invoke-virtual {v2, v5}, Landroid/text/Layout;->getSecondaryHorizontal(I)F

    #@4c
    move-result v7

    #@4d
    invoke-direct {p0, v9, v4, v0, v7}, Landroid/widget/Editor;->updateCursorPosition(IIIF)V

    #@50
    goto :goto_b

    #@51
    .end local v4           #middle:I
    :cond_51
    move v7, v9

    #@52
    .line 1786
    goto :goto_33
.end method
