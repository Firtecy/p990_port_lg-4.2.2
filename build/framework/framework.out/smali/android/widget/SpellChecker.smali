.class public Landroid/widget/SpellChecker;
.super Ljava/lang/Object;
.source "SpellChecker.java"

# interfaces
.implements Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/SpellChecker$SpellParser;
    }
.end annotation


# static fields
.field public static final AVERAGE_WORD_LENGTH:I = 0x7

.field private static final DBG:Z = false

.field public static final MAX_NUMBER_OF_WORDS:I = 0x32

.field private static final MIN_SENTENCE_LENGTH:I = 0x32

.field private static final SPELL_PAUSE_DURATION:I = 0x190

.field private static final SUGGESTION_SPAN_CACHE_SIZE:I = 0xa

.field private static final TAG:Ljava/lang/String; = null

.field private static final USE_SPAN_RANGE:I = -0x1

.field public static final WORD_ITERATOR_INTERVAL:I = 0x15e


# instance fields
.field final mCookie:I

.field private mCurrentLocale:Ljava/util/Locale;

.field private mIds:[I

.field private mIsSentenceSpellCheckSupported:Z

.field private mLength:I

.field private mSpanSequenceCounter:I

.field private mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

.field mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

.field private mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

.field private mSpellRunnable:Ljava/lang/Runnable;

.field private final mSuggestionSpanCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Landroid/text/style/SuggestionSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mTextServicesManager:Landroid/view/textservice/TextServicesManager;

.field private final mTextView:Landroid/widget/TextView;

.field private mWordIterator:Landroid/text/method/WordIterator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 49
    const-class v0, Landroid/widget/SpellChecker;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/widget/SpellChecker;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/widget/TextView;)V
    .registers 5
    .parameter "textView"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 104
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 86
    new-array v1, v2, [Landroid/widget/SpellChecker$SpellParser;

    #@6
    iput-object v1, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@8
    .line 88
    iput v2, p0, Landroid/widget/SpellChecker;->mSpanSequenceCounter:I

    #@a
    .line 101
    new-instance v1, Landroid/util/LruCache;

    #@c
    const/16 v2, 0xa

    #@e
    invoke-direct {v1, v2}, Landroid/util/LruCache;-><init>(I)V

    #@11
    iput-object v1, p0, Landroid/widget/SpellChecker;->mSuggestionSpanCache:Landroid/util/LruCache;

    #@13
    .line 105
    iput-object p1, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@15
    .line 108
    const/4 v1, 0x1

    #@16
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->idealObjectArraySize(I)I

    #@19
    move-result v0

    #@1a
    .line 109
    .local v0, size:I
    new-array v1, v0, [I

    #@1c
    iput-object v1, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@1e
    .line 110
    new-array v1, v0, [Landroid/text/style/SpellCheckSpan;

    #@20
    iput-object v1, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@22
    .line 112
    iget-object v1, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@24
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextServicesLocale()Ljava/util/Locale;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Landroid/widget/SpellChecker;->setLocale(Ljava/util/Locale;)V

    #@2b
    .line 114
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@2e
    move-result v1

    #@2f
    iput v1, p0, Landroid/widget/SpellChecker;->mCookie:I

    #@31
    .line 115
    return-void
.end method

.method static synthetic access$100(Landroid/widget/SpellChecker;)[Landroid/widget/SpellChecker$SpellParser;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/widget/SpellChecker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/widget/SpellChecker;->spellCheck()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/widget/SpellChecker;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 48
    sget-object v0, Landroid/widget/SpellChecker;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/widget/SpellChecker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-boolean v0, p0, Landroid/widget/SpellChecker;->mIsSentenceSpellCheckSupported:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/widget/SpellChecker;)Landroid/text/method/WordIterator;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/SpellChecker;->mWordIterator:Landroid/text/method/WordIterator;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/widget/SpellChecker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget v0, p0, Landroid/widget/SpellChecker;->mLength:I

    #@2
    return v0
.end method

.method static synthetic access$700(Landroid/widget/SpellChecker;)[Landroid/text/style/SpellCheckSpan;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/widget/SpellChecker;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/widget/SpellChecker;Landroid/text/Editable;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SpellChecker;->addSpellCheckSpan(Landroid/text/Editable;II)V

    #@3
    return-void
.end method

.method private addSpellCheckSpan(Landroid/text/Editable;II)V
    .registers 9
    .parameter "editable"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 200
    invoke-direct {p0}, Landroid/widget/SpellChecker;->nextSpellCheckSpanIndex()I

    #@3
    move-result v0

    #@4
    .line 201
    .local v0, index:I
    iget-object v2, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@6
    aget-object v1, v2, v0

    #@8
    .line 202
    .local v1, spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    const/16 v2, 0x21

    #@a
    invoke-interface {p1, v1, p2, p3, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@d
    .line 203
    const/4 v2, 0x0

    #@e
    invoke-virtual {v1, v2}, Landroid/text/style/SpellCheckSpan;->setSpellCheckInProgress(Z)V

    #@11
    .line 204
    iget-object v2, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@13
    iget v3, p0, Landroid/widget/SpellChecker;->mSpanSequenceCounter:I

    #@15
    add-int/lit8 v4, v3, 0x1

    #@17
    iput v4, p0, Landroid/widget/SpellChecker;->mSpanSequenceCounter:I

    #@19
    aput v3, v2, v0

    #@1b
    .line 205
    return-void
.end method

.method private createMisspelledSuggestionSpan(Landroid/text/Editable;Landroid/view/textservice/SuggestionsInfo;Landroid/text/style/SpellCheckSpan;II)V
    .registers 19
    .parameter "editable"
    .parameter "suggestionsInfo"
    .parameter "spellCheckSpan"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 456
    move-object/from16 v0, p3

    #@2
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@5
    move-result v5

    #@6
    .line 457
    .local v5, spellCheckSpanStart:I
    move-object/from16 v0, p3

    #@8
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@b
    move-result v4

    #@c
    .line 458
    .local v4, spellCheckSpanEnd:I
    if-ltz v5, :cond_10

    #@e
    if-gt v4, v5, :cond_11

    #@10
    .line 501
    :cond_10
    :goto_10
    return-void

    #@11
    .line 463
    :cond_11
    const/4 v11, -0x1

    #@12
    move/from16 v0, p4

    #@14
    if-eq v0, v11, :cond_33

    #@16
    const/4 v11, -0x1

    #@17
    move/from16 v0, p5

    #@19
    if-eq v0, v11, :cond_33

    #@1b
    .line 464
    add-int v6, v5, p4

    #@1d
    .line 465
    .local v6, start:I
    add-int v1, v6, p5

    #@1f
    .line 471
    .local v1, end:I
    :goto_1f
    invoke-virtual {p2}, Landroid/view/textservice/SuggestionsInfo;->getSuggestionsCount()I

    #@22
    move-result v9

    #@23
    .line 473
    .local v9, suggestionsCount:I
    if-lez v9, :cond_36

    #@25
    .line 474
    new-array v8, v9, [Ljava/lang/String;

    #@27
    .line 475
    .local v8, suggestions:[Ljava/lang/String;
    const/4 v2, 0x0

    #@28
    .local v2, i:I
    :goto_28
    if-ge v2, v9, :cond_3e

    #@2a
    .line 476
    invoke-virtual {p2, v2}, Landroid/view/textservice/SuggestionsInfo;->getSuggestionAt(I)Ljava/lang/String;

    #@2d
    move-result-object v11

    #@2e
    aput-object v11, v8, v2

    #@30
    .line 475
    add-int/lit8 v2, v2, 0x1

    #@32
    goto :goto_28

    #@33
    .line 467
    .end local v1           #end:I
    .end local v2           #i:I
    .end local v6           #start:I
    .end local v8           #suggestions:[Ljava/lang/String;
    .end local v9           #suggestionsCount:I
    :cond_33
    move v6, v5

    #@34
    .line 468
    .restart local v6       #start:I
    move v1, v4

    #@35
    .restart local v1       #end:I
    goto :goto_1f

    #@36
    .line 479
    .restart local v9       #suggestionsCount:I
    :cond_36
    const-class v11, Ljava/lang/String;

    #@38
    invoke-static {v11}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@3b
    move-result-object v8

    #@3c
    check-cast v8, [Ljava/lang/String;

    #@3e
    .line 482
    .restart local v8       #suggestions:[Ljava/lang/String;
    :cond_3e
    new-instance v7, Landroid/text/style/SuggestionSpan;

    #@40
    iget-object v11, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@42
    invoke-virtual {v11}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@45
    move-result-object v11

    #@46
    const/4 v12, 0x3

    #@47
    invoke-direct {v7, v11, v8, v12}, Landroid/text/style/SuggestionSpan;-><init>(Landroid/content/Context;[Ljava/lang/String;I)V

    #@4a
    .line 486
    .local v7, suggestionSpan:Landroid/text/style/SuggestionSpan;
    iget-boolean v11, p0, Landroid/widget/SpellChecker;->mIsSentenceSpellCheckSupported:Z

    #@4c
    if-eqz v11, :cond_68

    #@4e
    .line 487
    invoke-static {v6, v1}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@51
    move-result-wide v11

    #@52
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@55
    move-result-object v3

    #@56
    .line 488
    .local v3, key:Ljava/lang/Long;
    iget-object v11, p0, Landroid/widget/SpellChecker;->mSuggestionSpanCache:Landroid/util/LruCache;

    #@58
    invoke-virtual {v11, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    move-result-object v10

    #@5c
    check-cast v10, Landroid/text/style/SuggestionSpan;

    #@5e
    .line 489
    .local v10, tempSuggestionSpan:Landroid/text/style/SuggestionSpan;
    if-eqz v10, :cond_63

    #@60
    .line 494
    invoke-interface {p1, v10}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@63
    .line 496
    :cond_63
    iget-object v11, p0, Landroid/widget/SpellChecker;->mSuggestionSpanCache:Landroid/util/LruCache;

    #@65
    invoke-virtual {v11, v3, v7}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    .line 498
    .end local v3           #key:Ljava/lang/Long;
    .end local v10           #tempSuggestionSpan:Landroid/text/style/SuggestionSpan;
    :cond_68
    const/16 v11, 0x21

    #@6a
    invoke-interface {p1, v7, v6, v1, v11}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@6d
    .line 500
    iget-object v11, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@6f
    const/4 v12, 0x0

    #@70
    invoke-virtual {v11, v6, v1, v12}, Landroid/widget/TextView;->invalidateRegion(IIZ)V

    #@73
    goto :goto_10
.end method

.method private isSessionActive()Z
    .registers 2

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private nextSpellCheckSpanIndex()I
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 180
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget v4, p0, Landroid/widget/SpellChecker;->mLength:I

    #@4
    if-ge v0, v4, :cond_10

    #@6
    .line 181
    iget-object v4, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@8
    aget v4, v4, v0

    #@a
    if-gez v4, :cond_d

    #@c
    .line 196
    .end local v0           #i:I
    :goto_c
    return v0

    #@d
    .line 180
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_2

    #@10
    .line 184
    :cond_10
    iget v4, p0, Landroid/widget/SpellChecker;->mLength:I

    #@12
    iget-object v5, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@14
    array-length v5, v5

    #@15
    if-ne v4, v5, :cond_31

    #@17
    .line 185
    iget v4, p0, Landroid/widget/SpellChecker;->mLength:I

    #@19
    mul-int/lit8 v2, v4, 0x2

    #@1b
    .line 186
    .local v2, newSize:I
    new-array v1, v2, [I

    #@1d
    .line 187
    .local v1, newIds:[I
    new-array v3, v2, [Landroid/text/style/SpellCheckSpan;

    #@1f
    .line 188
    .local v3, newSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;
    iget-object v4, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@21
    iget v5, p0, Landroid/widget/SpellChecker;->mLength:I

    #@23
    invoke-static {v4, v6, v1, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@26
    .line 189
    iget-object v4, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@28
    iget v5, p0, Landroid/widget/SpellChecker;->mLength:I

    #@2a
    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2d
    .line 190
    iput-object v1, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@2f
    .line 191
    iput-object v3, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@31
    .line 194
    .end local v1           #newIds:[I
    .end local v2           #newSize:I
    .end local v3           #newSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;
    :cond_31
    iget-object v4, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@33
    iget v5, p0, Landroid/widget/SpellChecker;->mLength:I

    #@35
    new-instance v6, Landroid/text/style/SpellCheckSpan;

    #@37
    invoke-direct {v6}, Landroid/text/style/SpellCheckSpan;-><init>()V

    #@3a
    aput-object v6, v4, v5

    #@3c
    .line 195
    iget v4, p0, Landroid/widget/SpellChecker;->mLength:I

    #@3e
    add-int/lit8 v4, v4, 0x1

    #@40
    iput v4, p0, Landroid/widget/SpellChecker;->mLength:I

    #@42
    .line 196
    iget v4, p0, Landroid/widget/SpellChecker;->mLength:I

    #@44
    add-int/lit8 v0, v4, -0x1

    #@46
    goto :goto_c
.end method

.method private onGetSuggestionsInternal(Landroid/view/textservice/SuggestionsInfo;II)Landroid/text/style/SpellCheckSpan;
    .registers 22
    .parameter "suggestionsInfo"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 326
    if-eqz p1, :cond_c

    #@2
    invoke-virtual/range {p1 .. p1}, Landroid/view/textservice/SuggestionsInfo;->getCookie()I

    #@5
    move-result v1

    #@6
    move-object/from16 v0, p0

    #@8
    iget v3, v0, Landroid/widget/SpellChecker;->mCookie:I

    #@a
    if-eq v1, v3, :cond_e

    #@c
    .line 327
    :cond_c
    const/4 v4, 0x0

    #@d
    .line 379
    :cond_d
    :goto_d
    return-object v4

    #@e
    .line 329
    :cond_e
    move-object/from16 v0, p0

    #@10
    iget-object v1, v0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@12
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/text/Editable;

    #@18
    .line 330
    .local v2, editable:Landroid/text/Editable;
    invoke-virtual/range {p1 .. p1}, Landroid/view/textservice/SuggestionsInfo;->getSequence()I

    #@1b
    move-result v13

    #@1c
    .line 331
    .local v13, sequenceNumber:I
    const/4 v10, 0x0

    #@1d
    .local v10, k:I
    :goto_1d
    move-object/from16 v0, p0

    #@1f
    iget v1, v0, Landroid/widget/SpellChecker;->mLength:I

    #@21
    if-ge v10, v1, :cond_a3

    #@23
    .line 332
    move-object/from16 v0, p0

    #@25
    iget-object v1, v0, Landroid/widget/SpellChecker;->mIds:[I

    #@27
    aget v1, v1, v10

    #@29
    if-ne v13, v1, :cond_9f

    #@2b
    .line 333
    invoke-virtual/range {p1 .. p1}, Landroid/view/textservice/SuggestionsInfo;->getSuggestionsAttributes()I

    #@2e
    move-result v7

    #@2f
    .line 334
    .local v7, attributes:I
    and-int/lit8 v1, v7, 0x1

    #@31
    if-lez v1, :cond_4f

    #@33
    const/4 v9, 0x1

    #@34
    .line 336
    .local v9, isInDictionary:Z
    :goto_34
    and-int/lit8 v1, v7, 0x2

    #@36
    if-lez v1, :cond_51

    #@38
    const/4 v12, 0x1

    #@39
    .line 339
    .local v12, looksLikeTypo:Z
    :goto_39
    move-object/from16 v0, p0

    #@3b
    iget-object v1, v0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@3d
    aget-object v4, v1, v10

    #@3f
    .line 342
    .local v4, spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    if-nez v9, :cond_53

    #@41
    if-eqz v12, :cond_53

    #@43
    move-object/from16 v1, p0

    #@45
    move-object/from16 v3, p1

    #@47
    move/from16 v5, p2

    #@49
    move/from16 v6, p3

    #@4b
    .line 343
    invoke-direct/range {v1 .. v6}, Landroid/widget/SpellChecker;->createMisspelledSuggestionSpan(Landroid/text/Editable;Landroid/view/textservice/SuggestionsInfo;Landroid/text/style/SpellCheckSpan;II)V

    #@4e
    goto :goto_d

    #@4f
    .line 334
    .end local v4           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    .end local v9           #isInDictionary:Z
    .end local v12           #looksLikeTypo:Z
    :cond_4f
    const/4 v9, 0x0

    #@50
    goto :goto_34

    #@51
    .line 336
    .restart local v9       #isInDictionary:Z
    :cond_51
    const/4 v12, 0x0

    #@52
    goto :goto_39

    #@53
    .line 347
    .restart local v4       #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    .restart local v12       #looksLikeTypo:Z
    :cond_53
    move-object/from16 v0, p0

    #@55
    iget-boolean v1, v0, Landroid/widget/SpellChecker;->mIsSentenceSpellCheckSupported:Z

    #@57
    if-eqz v1, :cond_d

    #@59
    .line 350
    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@5c
    move-result v15

    #@5d
    .line 351
    .local v15, spellCheckSpanStart:I
    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@60
    move-result v14

    #@61
    .line 354
    .local v14, spellCheckSpanEnd:I
    const/4 v1, -0x1

    #@62
    move/from16 v0, p2

    #@64
    if-eq v0, v1, :cond_9b

    #@66
    const/4 v1, -0x1

    #@67
    move/from16 v0, p3

    #@69
    if-eq v0, v1, :cond_9b

    #@6b
    .line 355
    add-int v16, v15, p2

    #@6d
    .line 356
    .local v16, start:I
    add-int v8, v16, p3

    #@6f
    .line 361
    .local v8, end:I
    :goto_6f
    if-ltz v15, :cond_d

    #@71
    if-le v14, v15, :cond_d

    #@73
    move/from16 v0, v16

    #@75
    if-le v8, v0, :cond_d

    #@77
    .line 363
    move/from16 v0, v16

    #@79
    invoke-static {v0, v8}, Landroid/text/TextUtils;->packRangeInLong(II)J

    #@7c
    move-result-wide v5

    #@7d
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@80
    move-result-object v11

    #@81
    .line 364
    .local v11, key:Ljava/lang/Long;
    move-object/from16 v0, p0

    #@83
    iget-object v1, v0, Landroid/widget/SpellChecker;->mSuggestionSpanCache:Landroid/util/LruCache;

    #@85
    invoke-virtual {v1, v11}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@88
    move-result-object v17

    #@89
    check-cast v17, Landroid/text/style/SuggestionSpan;

    #@8b
    .line 365
    .local v17, tempSuggestionSpan:Landroid/text/style/SuggestionSpan;
    if-eqz v17, :cond_d

    #@8d
    .line 370
    move-object/from16 v0, v17

    #@8f
    invoke-interface {v2, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@92
    .line 371
    move-object/from16 v0, p0

    #@94
    iget-object v1, v0, Landroid/widget/SpellChecker;->mSuggestionSpanCache:Landroid/util/LruCache;

    #@96
    invoke-virtual {v1, v11}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@99
    goto/16 :goto_d

    #@9b
    .line 358
    .end local v8           #end:I
    .end local v11           #key:Ljava/lang/Long;
    .end local v16           #start:I
    .end local v17           #tempSuggestionSpan:Landroid/text/style/SuggestionSpan;
    :cond_9b
    move/from16 v16, v15

    #@9d
    .line 359
    .restart local v16       #start:I
    move v8, v14

    #@9e
    .restart local v8       #end:I
    goto :goto_6f

    #@9f
    .line 331
    .end local v4           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    .end local v7           #attributes:I
    .end local v8           #end:I
    .end local v9           #isInDictionary:Z
    .end local v12           #looksLikeTypo:Z
    .end local v14           #spellCheckSpanEnd:I
    .end local v15           #spellCheckSpanStart:I
    .end local v16           #start:I
    :cond_9f
    add-int/lit8 v10, v10, 0x1

    #@a1
    goto/16 :goto_1d

    #@a3
    .line 379
    :cond_a3
    const/4 v4, 0x0

    #@a4
    goto/16 :goto_d
.end method

.method private resetSession()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 118
    invoke-virtual {p0}, Landroid/widget/SpellChecker;->closeSession()V

    #@6
    .line 120
    iget-object v1, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@8
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    const-string/jumbo v2, "textservices"

    #@f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/view/textservice/TextServicesManager;

    #@15
    iput-object v1, p0, Landroid/widget/SpellChecker;->mTextServicesManager:Landroid/view/textservice/TextServicesManager;

    #@17
    .line 122
    iget-object v1, p0, Landroid/widget/SpellChecker;->mTextServicesManager:Landroid/view/textservice/TextServicesManager;

    #@19
    invoke-virtual {v1}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_27

    #@1f
    iget-object v1, p0, Landroid/widget/SpellChecker;->mTextServicesManager:Landroid/view/textservice/TextServicesManager;

    #@21
    invoke-virtual {v1, v4}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellCheckerSubtype(Z)Landroid/view/textservice/SpellCheckerSubtype;

    #@24
    move-result-object v1

    #@25
    if-nez v1, :cond_36

    #@27
    .line 124
    :cond_27
    iput-object v5, p0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@29
    .line 134
    :goto_29
    const/4 v0, 0x0

    #@2a
    .local v0, i:I
    :goto_2a
    iget v1, p0, Landroid/widget/SpellChecker;->mLength:I

    #@2c
    if-ge v0, v1, :cond_43

    #@2e
    .line 135
    iget-object v1, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@30
    const/4 v2, -0x1

    #@31
    aput v2, v1, v0

    #@33
    .line 134
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_2a

    #@36
    .line 126
    .end local v0           #i:I
    :cond_36
    iget-object v1, p0, Landroid/widget/SpellChecker;->mTextServicesManager:Landroid/view/textservice/TextServicesManager;

    #@38
    iget-object v2, p0, Landroid/widget/SpellChecker;->mCurrentLocale:Ljava/util/Locale;

    #@3a
    invoke-virtual {v1, v5, v2, p0, v3}, Landroid/view/textservice/TextServicesManager;->newSpellCheckerSession(Landroid/os/Bundle;Ljava/util/Locale;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Z)Landroid/view/textservice/SpellCheckerSession;

    #@3d
    move-result-object v1

    #@3e
    iput-object v1, p0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@40
    .line 130
    iput-boolean v4, p0, Landroid/widget/SpellChecker;->mIsSentenceSpellCheckSupported:Z

    #@42
    goto :goto_29

    #@43
    .line 137
    .restart local v0       #i:I
    :cond_43
    iput v3, p0, Landroid/widget/SpellChecker;->mLength:I

    #@45
    .line 140
    iget-object v2, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@47
    iget-object v1, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@49
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@4c
    move-result-object v1

    #@4d
    check-cast v1, Landroid/text/Editable;

    #@4f
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->removeMisspelledSpans(Landroid/text/Spannable;)V

    #@52
    .line 141
    iget-object v1, p0, Landroid/widget/SpellChecker;->mSuggestionSpanCache:Landroid/util/LruCache;

    #@54
    invoke-virtual {v1}, Landroid/util/LruCache;->evictAll()V

    #@57
    .line 142
    return-void
.end method

.method private scheduleNewSpellCheck()V
    .registers 5

    #@0
    .prologue
    .line 433
    iget-object v0, p0, Landroid/widget/SpellChecker;->mSpellRunnable:Ljava/lang/Runnable;

    #@2
    if-nez v0, :cond_15

    #@4
    .line 434
    new-instance v0, Landroid/widget/SpellChecker$1;

    #@6
    invoke-direct {v0, p0}, Landroid/widget/SpellChecker$1;-><init>(Landroid/widget/SpellChecker;)V

    #@9
    iput-object v0, p0, Landroid/widget/SpellChecker;->mSpellRunnable:Ljava/lang/Runnable;

    #@b
    .line 451
    :goto_b
    iget-object v0, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@d
    iget-object v1, p0, Landroid/widget/SpellChecker;->mSpellRunnable:Ljava/lang/Runnable;

    #@f
    const-wide/16 v2, 0x190

    #@11
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@14
    .line 452
    return-void

    #@15
    .line 448
    :cond_15
    iget-object v0, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@17
    iget-object v1, p0, Landroid/widget/SpellChecker;->mSpellRunnable:Ljava/lang/Runnable;

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@1c
    goto :goto_b
.end method

.method private setLocale(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 145
    iput-object p1, p0, Landroid/widget/SpellChecker;->mCurrentLocale:Ljava/util/Locale;

    #@2
    .line 147
    invoke-direct {p0}, Landroid/widget/SpellChecker;->resetSession()V

    #@5
    .line 150
    new-instance v0, Landroid/text/method/WordIterator;

    #@7
    invoke-direct {v0, p1}, Landroid/text/method/WordIterator;-><init>(Ljava/util/Locale;)V

    #@a
    iput-object v0, p0, Landroid/widget/SpellChecker;->mWordIterator:Landroid/text/method/WordIterator;

    #@c
    .line 153
    iget-object v0, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@e
    invoke-virtual {v0}, Landroid/widget/TextView;->onLocaleChanged()V

    #@11
    .line 154
    return-void
.end method

.method private spellCheck()V
    .registers 18

    #@0
    .prologue
    .line 266
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@4
    if-nez v14, :cond_7

    #@6
    .line 322
    :cond_6
    :goto_6
    return-void

    #@7
    .line 268
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget-object v14, v0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@b
    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/text/Editable;

    #@11
    .line 269
    .local v1, editable:Landroid/text/Editable;
    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@14
    move-result v6

    #@15
    .line 270
    .local v6, selectionStart:I
    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@18
    move-result v5

    #@19
    .line 272
    .local v5, selectionEnd:I
    move-object/from16 v0, p0

    #@1b
    iget v14, v0, Landroid/widget/SpellChecker;->mLength:I

    #@1d
    new-array v9, v14, [Landroid/view/textservice/TextInfo;

    #@1f
    .line 273
    .local v9, textInfos:[Landroid/view/textservice/TextInfo;
    const/4 v11, 0x0

    #@20
    .line 275
    .local v11, textInfosCount:I
    const/4 v3, 0x0

    #@21
    .local v3, i:I
    :goto_21
    move-object/from16 v0, p0

    #@23
    iget v14, v0, Landroid/widget/SpellChecker;->mLength:I

    #@25
    if-ge v3, v14, :cond_92

    #@27
    .line 276
    move-object/from16 v0, p0

    #@29
    iget-object v14, v0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@2b
    aget-object v7, v14, v3

    #@2d
    .line 277
    .local v7, spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    move-object/from16 v0, p0

    #@2f
    iget-object v14, v0, Landroid/widget/SpellChecker;->mIds:[I

    #@31
    aget v14, v14, v3

    #@33
    if-ltz v14, :cond_3b

    #@35
    invoke-virtual {v7}, Landroid/text/style/SpellCheckSpan;->isSpellCheckInProgress()Z

    #@38
    move-result v14

    #@39
    if-eqz v14, :cond_3e

    #@3b
    .line 275
    :cond_3b
    :goto_3b
    add-int/lit8 v3, v3, 0x1

    #@3d
    goto :goto_21

    #@3e
    .line 279
    :cond_3e
    invoke-interface {v1, v7}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    #@41
    move-result v8

    #@42
    .line 280
    .local v8, start:I
    invoke-interface {v1, v7}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    #@45
    move-result v2

    #@46
    .line 284
    .local v2, end:I
    move-object/from16 v0, p0

    #@48
    iget-boolean v14, v0, Landroid/widget/SpellChecker;->mIsSentenceSpellCheckSupported:Z

    #@4a
    if-eqz v14, :cond_81

    #@4c
    .line 288
    if-le v5, v8, :cond_50

    #@4e
    if-le v6, v2, :cond_7f

    #@50
    :cond_50
    const/4 v4, 0x1

    #@51
    .line 292
    .local v4, isEditing:Z
    :goto_51
    if-ltz v8, :cond_3b

    #@53
    if-le v2, v8, :cond_3b

    #@55
    if-eqz v4, :cond_3b

    #@57
    .line 293
    instance-of v14, v1, Landroid/text/SpannableStringBuilder;

    #@59
    if-eqz v14, :cond_89

    #@5b
    move-object v14, v1

    #@5c
    check-cast v14, Landroid/text/SpannableStringBuilder;

    #@5e
    invoke-virtual {v14, v8, v2}, Landroid/text/SpannableStringBuilder;->substring(II)Ljava/lang/String;

    #@61
    move-result-object v13

    #@62
    .line 296
    .local v13, word:Ljava/lang/String;
    :goto_62
    const/4 v14, 0x1

    #@63
    invoke-virtual {v7, v14}, Landroid/text/style/SpellCheckSpan;->setSpellCheckInProgress(Z)V

    #@66
    .line 297
    add-int/lit8 v12, v11, 0x1

    #@68
    .end local v11           #textInfosCount:I
    .local v12, textInfosCount:I
    new-instance v14, Landroid/view/textservice/TextInfo;

    #@6a
    move-object/from16 v0, p0

    #@6c
    iget v15, v0, Landroid/widget/SpellChecker;->mCookie:I

    #@6e
    move-object/from16 v0, p0

    #@70
    iget-object v0, v0, Landroid/widget/SpellChecker;->mIds:[I

    #@72
    move-object/from16 v16, v0

    #@74
    aget v16, v16, v3

    #@76
    move/from16 v0, v16

    #@78
    invoke-direct {v14, v13, v15, v0}, Landroid/view/textservice/TextInfo;-><init>(Ljava/lang/String;II)V

    #@7b
    aput-object v14, v9, v11

    #@7d
    move v11, v12

    #@7e
    .end local v12           #textInfosCount:I
    .restart local v11       #textInfosCount:I
    goto :goto_3b

    #@7f
    .line 288
    .end local v4           #isEditing:Z
    .end local v13           #word:Ljava/lang/String;
    :cond_7f
    const/4 v4, 0x0

    #@80
    goto :goto_51

    #@81
    .line 290
    :cond_81
    if-lt v5, v8, :cond_85

    #@83
    if-le v6, v2, :cond_87

    #@85
    :cond_85
    const/4 v4, 0x1

    #@86
    .restart local v4       #isEditing:Z
    :goto_86
    goto :goto_51

    #@87
    .end local v4           #isEditing:Z
    :cond_87
    const/4 v4, 0x0

    #@88
    goto :goto_86

    #@89
    .line 293
    .restart local v4       #isEditing:Z
    :cond_89
    invoke-interface {v1, v8, v2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@8c
    move-result-object v14

    #@8d
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@90
    move-result-object v13

    #@91
    goto :goto_62

    #@92
    .line 307
    .end local v2           #end:I
    .end local v4           #isEditing:Z
    .end local v7           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    .end local v8           #start:I
    :cond_92
    if-lez v11, :cond_6

    #@94
    .line 308
    array-length v14, v9

    #@95
    if-ge v11, v14, :cond_9f

    #@97
    .line 309
    new-array v10, v11, [Landroid/view/textservice/TextInfo;

    #@99
    .line 310
    .local v10, textInfosCopy:[Landroid/view/textservice/TextInfo;
    const/4 v14, 0x0

    #@9a
    const/4 v15, 0x0

    #@9b
    invoke-static {v9, v14, v10, v15, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@9e
    .line 311
    move-object v9, v10

    #@9f
    .line 314
    .end local v10           #textInfosCopy:[Landroid/view/textservice/TextInfo;
    :cond_9f
    move-object/from16 v0, p0

    #@a1
    iget-boolean v14, v0, Landroid/widget/SpellChecker;->mIsSentenceSpellCheckSupported:Z

    #@a3
    if-eqz v14, :cond_af

    #@a5
    .line 315
    move-object/from16 v0, p0

    #@a7
    iget-object v14, v0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@a9
    const/4 v15, 0x5

    #@aa
    invoke-virtual {v14, v9, v15}, Landroid/view/textservice/SpellCheckerSession;->getSentenceSuggestions([Landroid/view/textservice/TextInfo;I)V

    #@ad
    goto/16 :goto_6

    #@af
    .line 318
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iget-object v14, v0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@b3
    const/4 v15, 0x5

    #@b4
    const/16 v16, 0x0

    #@b6
    move/from16 v0, v16

    #@b8
    invoke-virtual {v14, v9, v15, v0}, Landroid/view/textservice/SpellCheckerSession;->getSuggestions([Landroid/view/textservice/TextInfo;IZ)V

    #@bb
    goto/16 :goto_6
.end method


# virtual methods
.method public closeSession()V
    .registers 5

    #@0
    .prologue
    .line 165
    iget-object v2, p0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@2
    if-eqz v2, :cond_9

    #@4
    .line 166
    iget-object v2, p0, Landroid/widget/SpellChecker;->mSpellCheckerSession:Landroid/view/textservice/SpellCheckerSession;

    #@6
    invoke-virtual {v2}, Landroid/view/textservice/SpellCheckerSession;->close()V

    #@9
    .line 169
    :cond_9
    iget-object v2, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@b
    array-length v1, v2

    #@c
    .line 170
    .local v1, length:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v1, :cond_19

    #@f
    .line 171
    iget-object v2, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@11
    aget-object v2, v2, v0

    #@13
    invoke-virtual {v2}, Landroid/widget/SpellChecker$SpellParser;->stop()V

    #@16
    .line 170
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_d

    #@19
    .line 174
    :cond_19
    iget-object v2, p0, Landroid/widget/SpellChecker;->mSpellRunnable:Ljava/lang/Runnable;

    #@1b
    if-eqz v2, :cond_24

    #@1d
    .line 175
    iget-object v2, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@1f
    iget-object v3, p0, Landroid/widget/SpellChecker;->mSpellRunnable:Ljava/lang/Runnable;

    #@21
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@24
    .line 177
    :cond_24
    return-void
.end method

.method public onGetSentenceSuggestions([Landroid/view/textservice/SentenceSuggestionsInfo;)V
    .registers 12
    .parameter "results"

    #@0
    .prologue
    .line 398
    iget-object v9, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/text/Editable;

    #@8
    .line 400
    .local v0, editable:Landroid/text/Editable;
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    array-length v9, p1

    #@a
    if-ge v1, v9, :cond_3c

    #@c
    .line 401
    aget-object v7, p1, v1

    #@e
    .line 402
    .local v7, ssi:Landroid/view/textservice/SentenceSuggestionsInfo;
    if-nez v7, :cond_13

    #@10
    .line 400
    :cond_10
    :goto_10
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_9

    #@13
    .line 405
    :cond_13
    const/4 v6, 0x0

    #@14
    .line 406
    .local v6, spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    const/4 v2, 0x0

    #@15
    .local v2, j:I
    :goto_15
    invoke-virtual {v7}, Landroid/view/textservice/SentenceSuggestionsInfo;->getSuggestionsCount()I

    #@18
    move-result v9

    #@19
    if-ge v2, v9, :cond_36

    #@1b
    .line 407
    invoke-virtual {v7, v2}, Landroid/view/textservice/SentenceSuggestionsInfo;->getSuggestionsInfoAt(I)Landroid/view/textservice/SuggestionsInfo;

    #@1e
    move-result-object v8

    #@1f
    .line 408
    .local v8, suggestionsInfo:Landroid/view/textservice/SuggestionsInfo;
    if-nez v8, :cond_24

    #@21
    .line 406
    :cond_21
    :goto_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_15

    #@24
    .line 411
    :cond_24
    invoke-virtual {v7, v2}, Landroid/view/textservice/SentenceSuggestionsInfo;->getOffsetAt(I)I

    #@27
    move-result v4

    #@28
    .line 412
    .local v4, offset:I
    invoke-virtual {v7, v2}, Landroid/view/textservice/SentenceSuggestionsInfo;->getLengthAt(I)I

    #@2b
    move-result v3

    #@2c
    .line 413
    .local v3, length:I
    invoke-direct {p0, v8, v4, v3}, Landroid/widget/SpellChecker;->onGetSuggestionsInternal(Landroid/view/textservice/SuggestionsInfo;II)Landroid/text/style/SpellCheckSpan;

    #@2f
    move-result-object v5

    #@30
    .line 415
    .local v5, scs:Landroid/text/style/SpellCheckSpan;
    if-nez v6, :cond_21

    #@32
    if-eqz v5, :cond_21

    #@34
    .line 418
    move-object v6, v5

    #@35
    goto :goto_21

    #@36
    .line 421
    .end local v3           #length:I
    .end local v4           #offset:I
    .end local v5           #scs:Landroid/text/style/SpellCheckSpan;
    .end local v8           #suggestionsInfo:Landroid/view/textservice/SuggestionsInfo;
    :cond_36
    if-eqz v6, :cond_10

    #@38
    .line 423
    invoke-interface {v0, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@3b
    goto :goto_10

    #@3c
    .line 426
    .end local v2           #j:I
    .end local v6           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    .end local v7           #ssi:Landroid/view/textservice/SentenceSuggestionsInfo;
    :cond_3c
    invoke-direct {p0}, Landroid/widget/SpellChecker;->scheduleNewSpellCheck()V

    #@3f
    .line 427
    return-void
.end method

.method public onGetSuggestions([Landroid/view/textservice/SuggestionsInfo;)V
    .registers 7
    .parameter "results"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 384
    iget-object v3, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/text/Editable;

    #@9
    .line 385
    .local v0, editable:Landroid/text/Editable;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    array-length v3, p1

    #@b
    if-ge v1, v3, :cond_1b

    #@d
    .line 386
    aget-object v3, p1, v1

    #@f
    invoke-direct {p0, v3, v4, v4}, Landroid/widget/SpellChecker;->onGetSuggestionsInternal(Landroid/view/textservice/SuggestionsInfo;II)Landroid/text/style/SpellCheckSpan;

    #@12
    move-result-object v2

    #@13
    .line 388
    .local v2, spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    if-eqz v2, :cond_18

    #@15
    .line 390
    invoke-interface {v0, v2}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    #@18
    .line 385
    :cond_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_a

    #@1b
    .line 393
    .end local v2           #spellCheckSpan:Landroid/text/style/SpellCheckSpan;
    :cond_1b
    invoke-direct {p0}, Landroid/widget/SpellChecker;->scheduleNewSpellCheck()V

    #@1e
    .line 394
    return-void
.end method

.method public onSelectionChanged()V
    .registers 1

    #@0
    .prologue
    .line 218
    invoke-direct {p0}, Landroid/widget/SpellChecker;->spellCheck()V

    #@3
    .line 219
    return-void
.end method

.method public onSpellCheckSpanRemoved(Landroid/text/style/SpellCheckSpan;)V
    .registers 5
    .parameter "spellCheckSpan"

    #@0
    .prologue
    .line 209
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/widget/SpellChecker;->mLength:I

    #@3
    if-ge v0, v1, :cond_10

    #@5
    .line 210
    iget-object v1, p0, Landroid/widget/SpellChecker;->mSpellCheckSpans:[Landroid/text/style/SpellCheckSpan;

    #@7
    aget-object v1, v1, v0

    #@9
    if-ne v1, p1, :cond_11

    #@b
    .line 211
    iget-object v1, p0, Landroid/widget/SpellChecker;->mIds:[I

    #@d
    const/4 v2, -0x1

    #@e
    aput v2, v1, v0

    #@10
    .line 215
    :cond_10
    return-void

    #@11
    .line 209
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_1
.end method

.method public spellCheck(II)V
    .registers 12
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 225
    iget-object v7, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@3
    invoke-virtual {v7}, Landroid/widget/TextView;->getTextServicesLocale()Ljava/util/Locale;

    #@6
    move-result-object v3

    #@7
    .line 226
    .local v3, locale:Ljava/util/Locale;
    invoke-direct {p0}, Landroid/widget/SpellChecker;->isSessionActive()Z

    #@a
    move-result v1

    #@b
    .line 227
    .local v1, isSessionActive:Z
    iget-object v7, p0, Landroid/widget/SpellChecker;->mCurrentLocale:Ljava/util/Locale;

    #@d
    if-eqz v7, :cond_17

    #@f
    iget-object v7, p0, Landroid/widget/SpellChecker;->mCurrentLocale:Ljava/util/Locale;

    #@11
    invoke-virtual {v7, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v7

    #@15
    if-nez v7, :cond_28

    #@17
    .line 228
    :cond_17
    invoke-direct {p0, v3}, Landroid/widget/SpellChecker;->setLocale(Ljava/util/Locale;)V

    #@1a
    .line 230
    const/4 p1, 0x0

    #@1b
    .line 231
    iget-object v7, p0, Landroid/widget/SpellChecker;->mTextView:Landroid/widget/TextView;

    #@1d
    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@20
    move-result-object v7

    #@21
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    #@24
    move-result p2

    #@25
    .line 240
    :cond_25
    :goto_25
    if-nez v1, :cond_34

    #@27
    .line 263
    :goto_27
    return-void

    #@28
    .line 233
    :cond_28
    iget-object v7, p0, Landroid/widget/SpellChecker;->mTextServicesManager:Landroid/view/textservice/TextServicesManager;

    #@2a
    invoke-virtual {v7}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    #@2d
    move-result v5

    #@2e
    .line 234
    .local v5, spellCheckerActivated:Z
    if-eq v1, v5, :cond_25

    #@30
    .line 236
    invoke-direct {p0}, Landroid/widget/SpellChecker;->resetSession()V

    #@33
    goto :goto_25

    #@34
    .line 243
    .end local v5           #spellCheckerActivated:Z
    :cond_34
    iget-object v7, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@36
    array-length v2, v7

    #@37
    .line 244
    .local v2, length:I
    const/4 v0, 0x0

    #@38
    .local v0, i:I
    :goto_38
    if-ge v0, v2, :cond_4b

    #@3a
    .line 245
    iget-object v7, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@3c
    aget-object v6, v7, v0

    #@3e
    .line 246
    .local v6, spellParser:Landroid/widget/SpellChecker$SpellParser;
    invoke-virtual {v6}, Landroid/widget/SpellChecker$SpellParser;->isFinished()Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_48

    #@44
    .line 247
    invoke-virtual {v6, p1, p2}, Landroid/widget/SpellChecker$SpellParser;->parse(II)V

    #@47
    goto :goto_27

    #@48
    .line 244
    :cond_48
    add-int/lit8 v0, v0, 0x1

    #@4a
    goto :goto_38

    #@4b
    .line 256
    .end local v6           #spellParser:Landroid/widget/SpellChecker$SpellParser;
    :cond_4b
    add-int/lit8 v7, v2, 0x1

    #@4d
    new-array v4, v7, [Landroid/widget/SpellChecker$SpellParser;

    #@4f
    .line 257
    .local v4, newSpellParsers:[Landroid/widget/SpellChecker$SpellParser;
    iget-object v7, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@51
    invoke-static {v7, v8, v4, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@54
    .line 258
    iput-object v4, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@56
    .line 260
    new-instance v6, Landroid/widget/SpellChecker$SpellParser;

    #@58
    const/4 v7, 0x0

    #@59
    invoke-direct {v6, p0, v7}, Landroid/widget/SpellChecker$SpellParser;-><init>(Landroid/widget/SpellChecker;Landroid/widget/SpellChecker$1;)V

    #@5c
    .line 261
    .restart local v6       #spellParser:Landroid/widget/SpellChecker$SpellParser;
    iget-object v7, p0, Landroid/widget/SpellChecker;->mSpellParsers:[Landroid/widget/SpellChecker$SpellParser;

    #@5e
    aput-object v6, v7, v2

    #@60
    .line 262
    invoke-virtual {v6, p1, p2}, Landroid/widget/SpellChecker$SpellParser;->parse(II)V

    #@63
    goto :goto_27
.end method
