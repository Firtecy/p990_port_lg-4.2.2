.class public Landroid/widget/OverScroller;
.super Ljava/lang/Object;
.source "OverScroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/OverScroller$SplineOverScroller;
    }
.end annotation


# static fields
.field private static final DEFAULT_DURATION:I = 0xfa

.field private static final FLING_MODE:I = 0x1

.field private static final MAX_ACCELERATION_TIME_GAP:I = 0x3e8

.field private static final MAX_ACCELERATION_VELOCITY:I = 0x9c40

.field private static final MIN_VELOCITY_ACCELERATION:I = 0x3e8

.field private static final SCROLL_MODE:I = 0x0

.field private static final START_NUM_VELOCITY_ACCELERATION:I = 0x3


# instance fields
.field private accelerationCount:I

.field private lastFlingTime:J

.field private final mFlywheel:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mMode:I

.field private final mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

.field private final mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 61
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    #@4
    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .registers 4
    .parameter "context"
    .parameter "interpolator"

    #@0
    .prologue
    .line 71
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    #@4
    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;FF)V
    .registers 6
    .parameter "context"
    .parameter "interpolator"
    .parameter "bounceCoefficientX"
    .parameter "bounceCoefficientY"

    #@0
    .prologue
    .line 107
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    #@4
    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;FFZ)V
    .registers 6
    .parameter "context"
    .parameter "interpolator"
    .parameter "bounceCoefficientX"
    .parameter "bounceCoefficientY"
    .parameter "flywheel"

    #@0
    .prologue
    .line 125
    invoke-direct {p0, p1, p2, p5}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    #@3
    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V
    .registers 5
    .parameter "context"
    .parameter "interpolator"
    .parameter "flywheel"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    iput-object p2, p0, Landroid/widget/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@5
    .line 84
    iput-boolean p3, p0, Landroid/widget/OverScroller;->mFlywheel:Z

    #@7
    .line 85
    new-instance v0, Landroid/widget/OverScroller$SplineOverScroller;

    #@9
    invoke-direct {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;-><init>(Landroid/content/Context;)V

    #@c
    iput-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@e
    .line 86
    new-instance v0, Landroid/widget/OverScroller$SplineOverScroller;

    #@10
    invoke-direct {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;-><init>(Landroid/content/Context;)V

    #@13
    iput-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@15
    .line 88
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@17
    if-eqz v0, :cond_1c

    #@19
    .line 89
    const/4 v0, 0x0

    #@1a
    iput v0, p0, Landroid/widget/OverScroller;->accelerationCount:I

    #@1c
    .line 91
    :cond_1c
    return-void
.end method


# virtual methods
.method public abortAnimation()V
    .registers 2

    #@0
    .prologue
    .line 581
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0}, Landroid/widget/OverScroller$SplineOverScroller;->finish()V

    #@5
    .line 582
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@7
    invoke-virtual {v0}, Landroid/widget/OverScroller$SplineOverScroller;->finish()V

    #@a
    .line 583
    return-void
.end method

.method public computeScrollOffset()Z
    .registers 9

    #@0
    .prologue
    .line 306
    invoke-virtual {p0}, Landroid/widget/OverScroller;->isFinished()Z

    #@3
    move-result v6

    #@4
    if-eqz v6, :cond_8

    #@6
    .line 307
    const/4 v6, 0x0

    #@7
    .line 354
    :goto_7
    return v6

    #@8
    .line 310
    :cond_8
    iget v6, p0, Landroid/widget/OverScroller;->mMode:I

    #@a
    packed-switch v6, :pswitch_data_84

    #@d
    .line 354
    :cond_d
    :goto_d
    const/4 v6, 0x1

    #@e
    goto :goto_7

    #@f
    .line 312
    :pswitch_f
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@12
    move-result-wide v4

    #@13
    .line 315
    .local v4, time:J
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@15
    invoke-static {v6}, Landroid/widget/OverScroller$SplineOverScroller;->access$600(Landroid/widget/OverScroller$SplineOverScroller;)J

    #@18
    move-result-wide v6

    #@19
    sub-long v1, v4, v6

    #@1b
    .line 317
    .local v1, elapsedTime:J
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@1d
    invoke-static {v6}, Landroid/widget/OverScroller$SplineOverScroller;->access$500(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@20
    move-result v0

    #@21
    .line 318
    .local v0, duration:I
    int-to-long v6, v0

    #@22
    cmp-long v6, v1, v6

    #@24
    if-gez v6, :cond_44

    #@26
    .line 319
    long-to-float v6, v1

    #@27
    int-to-float v7, v0

    #@28
    div-float v3, v6, v7

    #@2a
    .line 321
    .local v3, q:F
    iget-object v6, p0, Landroid/widget/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2c
    if-nez v6, :cond_3d

    #@2e
    .line 322
    invoke-static {v3}, Landroid/widget/Scroller;->viscousFluid(F)F

    #@31
    move-result v3

    #@32
    .line 327
    :goto_32
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@34
    invoke-virtual {v6, v3}, Landroid/widget/OverScroller$SplineOverScroller;->updateScroll(F)V

    #@37
    .line 328
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@39
    invoke-virtual {v6, v3}, Landroid/widget/OverScroller$SplineOverScroller;->updateScroll(F)V

    #@3c
    goto :goto_d

    #@3d
    .line 324
    :cond_3d
    iget-object v6, p0, Landroid/widget/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@3f
    invoke-interface {v6, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@42
    move-result v3

    #@43
    goto :goto_32

    #@44
    .line 330
    .end local v3           #q:F
    :cond_44
    invoke-virtual {p0}, Landroid/widget/OverScroller;->abortAnimation()V

    #@47
    goto :goto_d

    #@48
    .line 335
    .end local v0           #duration:I
    .end local v1           #elapsedTime:J
    .end local v4           #time:J
    :pswitch_48
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@4a
    invoke-static {v6}, Landroid/widget/OverScroller$SplineOverScroller;->access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z

    #@4d
    move-result v6

    #@4e
    if-nez v6, :cond_65

    #@50
    .line 336
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@52
    invoke-virtual {v6}, Landroid/widget/OverScroller$SplineOverScroller;->update()Z

    #@55
    move-result v6

    #@56
    if-nez v6, :cond_65

    #@58
    .line 337
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@5a
    invoke-virtual {v6}, Landroid/widget/OverScroller$SplineOverScroller;->continueWhenFinished()Z

    #@5d
    move-result v6

    #@5e
    if-nez v6, :cond_65

    #@60
    .line 338
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@62
    invoke-virtual {v6}, Landroid/widget/OverScroller$SplineOverScroller;->finish()V

    #@65
    .line 343
    :cond_65
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@67
    invoke-static {v6}, Landroid/widget/OverScroller$SplineOverScroller;->access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z

    #@6a
    move-result v6

    #@6b
    if-nez v6, :cond_d

    #@6d
    .line 344
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@6f
    invoke-virtual {v6}, Landroid/widget/OverScroller$SplineOverScroller;->update()Z

    #@72
    move-result v6

    #@73
    if-nez v6, :cond_d

    #@75
    .line 345
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@77
    invoke-virtual {v6}, Landroid/widget/OverScroller$SplineOverScroller;->continueWhenFinished()Z

    #@7a
    move-result v6

    #@7b
    if-nez v6, :cond_d

    #@7d
    .line 346
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@7f
    invoke-virtual {v6}, Landroid/widget/OverScroller$SplineOverScroller;->finish()V

    #@82
    goto :goto_d

    #@83
    .line 310
    nop

    #@84
    :pswitch_data_84
    .packed-switch 0x0
        :pswitch_f
        :pswitch_48
    .end packed-switch
.end method

.method public extendDuration(I)V
    .registers 3
    .parameter "extend"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->extendDuration(I)V

    #@5
    .line 262
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@7
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->extendDuration(I)V

    #@a
    .line 263
    return-void
.end method

.method public fling(IIIIIIII)V
    .registers 20
    .parameter "startX"
    .parameter "startY"
    .parameter "velocityX"
    .parameter "velocityY"
    .parameter "minX"
    .parameter "maxX"
    .parameter "minY"
    .parameter "maxY"

    #@0
    .prologue
    .line 417
    const/4 v9, 0x0

    #@1
    const/4 v10, 0x0

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    move/from16 v5, p5

    #@9
    move/from16 v6, p6

    #@b
    move/from16 v7, p7

    #@d
    move/from16 v8, p8

    #@f
    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    #@12
    .line 418
    return-void
.end method

.method public fling(IIIIIIIIII)V
    .registers 22
    .parameter "startX"
    .parameter "startY"
    .parameter "velocityX"
    .parameter "velocityY"
    .parameter "minX"
    .parameter "maxX"
    .parameter "minY"
    .parameter "maxY"
    .parameter "overX"
    .parameter "overY"

    #@0
    .prologue
    .line 450
    iget-boolean v0, p0, Landroid/widget/OverScroller;->mFlywheel:Z

    #@2
    if-eqz v0, :cond_9f

    #@4
    invoke-virtual {p0}, Landroid/widget/OverScroller;->isFinished()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_9f

    #@a
    .line 451
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@c
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$200(Landroid/widget/OverScroller$SplineOverScroller;)F

    #@f
    move-result v8

    #@10
    .line 452
    .local v8, oldVelocityX:F
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@12
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$200(Landroid/widget/OverScroller$SplineOverScroller;)F

    #@15
    move-result v9

    #@16
    .line 454
    .local v9, oldVelocityY:F
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@18
    if-eqz v0, :cond_da

    #@1a
    .line 455
    const/4 v10, 0x1

    #@1b
    .line 456
    .local v10, usingAcceleration:Z
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@1e
    move-result-wide v6

    #@1f
    .line 458
    .local v6, nowFlingTime:J
    int-to-float v0, p3

    #@20
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    #@23
    move-result v0

    #@24
    float-to-int v0, v0

    #@25
    invoke-static {v8}, Ljava/lang/Math;->signum(F)F

    #@28
    move-result v1

    #@29
    float-to-int v1, v1

    #@2a
    if-ne v0, v1, :cond_39

    #@2c
    int-to-float v0, p4

    #@2d
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    #@30
    move-result v0

    #@31
    float-to-int v0, v0

    #@32
    invoke-static {v9}, Ljava/lang/Math;->signum(F)F

    #@35
    move-result v1

    #@36
    float-to-int v1, v1

    #@37
    if-eq v0, v1, :cond_64

    #@39
    .line 460
    :cond_39
    const/4 v10, 0x0

    #@3a
    .line 461
    invoke-static {v8}, Ljava/lang/Float;->isNaN(F)Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_4f

    #@40
    int-to-float v0, p4

    #@41
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    #@44
    move-result v0

    #@45
    float-to-int v0, v0

    #@46
    invoke-static {v9}, Ljava/lang/Math;->signum(F)F

    #@49
    move-result v1

    #@4a
    float-to-int v1, v1

    #@4b
    if-ne v0, v1, :cond_4f

    #@4d
    .line 462
    const/4 v8, 0x0

    #@4e
    .line 463
    const/4 v10, 0x1

    #@4f
    .line 465
    :cond_4f
    invoke-static {v9}, Ljava/lang/Float;->isNaN(F)Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_64

    #@55
    int-to-float v0, p3

    #@56
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    #@59
    move-result v0

    #@5a
    float-to-int v0, v0

    #@5b
    invoke-static {v8}, Ljava/lang/Math;->signum(F)F

    #@5e
    move-result v1

    #@5f
    float-to-int v1, v1

    #@60
    if-ne v0, v1, :cond_64

    #@62
    .line 466
    const/4 v9, 0x0

    #@63
    .line 467
    const/4 v10, 0x1

    #@64
    .line 471
    :cond_64
    iget-wide v0, p0, Landroid/widget/OverScroller;->lastFlingTime:J

    #@66
    sub-long v0, v6, v0

    #@68
    const-wide/16 v2, 0x3e8

    #@6a
    cmp-long v0, v0, v2

    #@6c
    if-lez v0, :cond_6f

    #@6e
    .line 472
    const/4 v10, 0x0

    #@6f
    .line 475
    :cond_6f
    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    #@72
    move-result v0

    #@73
    const/16 v1, 0x3e8

    #@75
    if-ge v0, v1, :cond_78

    #@77
    .line 476
    const/4 v10, 0x0

    #@78
    .line 479
    :cond_78
    iput-wide v6, p0, Landroid/widget/OverScroller;->lastFlingTime:J

    #@7a
    .line 481
    if-eqz v10, :cond_d6

    #@7c
    .line 482
    iget v0, p0, Landroid/widget/OverScroller;->accelerationCount:I

    #@7e
    add-int/lit8 v0, v0, 0x1

    #@80
    iput v0, p0, Landroid/widget/OverScroller;->accelerationCount:I

    #@82
    .line 483
    iget v0, p0, Landroid/widget/OverScroller;->accelerationCount:I

    #@84
    const/4 v1, 0x3

    #@85
    if-lt v0, v1, :cond_9f

    #@87
    .line 484
    int-to-float v0, p3

    #@88
    add-float/2addr v0, v8

    #@89
    float-to-int p3, v0

    #@8a
    .line 485
    int-to-float v0, p4

    #@8b
    add-float/2addr v0, v9

    #@8c
    float-to-int p4, v0

    #@8d
    .line 487
    if-lez p3, :cond_c6

    #@8f
    .line 488
    const v0, 0x9c40

    #@92
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    #@95
    move-result p3

    #@96
    .line 492
    :goto_96
    if-lez p4, :cond_ce

    #@98
    .line 493
    const v0, 0x9c40

    #@9b
    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    #@9e
    move-result p4

    #@9f
    .line 510
    .end local v6           #nowFlingTime:J
    .end local v8           #oldVelocityX:F
    .end local v9           #oldVelocityY:F
    .end local v10           #usingAcceleration:Z
    :cond_9f
    :goto_9f
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@a1
    if-eqz v0, :cond_a8

    #@a3
    .line 511
    iget-boolean v0, p0, Landroid/widget/OverScroller;->mFlywheel:Z

    #@a5
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->setFlyWheelStatus(Z)V

    #@a8
    .line 514
    :cond_a8
    const/4 v0, 0x1

    #@a9
    iput v0, p0, Landroid/widget/OverScroller;->mMode:I

    #@ab
    .line 515
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@ad
    move v1, p1

    #@ae
    move v2, p3

    #@af
    move/from16 v3, p5

    #@b1
    move/from16 v4, p6

    #@b3
    move/from16 v5, p9

    #@b5
    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller$SplineOverScroller;->fling(IIIII)V

    #@b8
    .line 516
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@ba
    move v1, p2

    #@bb
    move v2, p4

    #@bc
    move/from16 v3, p7

    #@be
    move/from16 v4, p8

    #@c0
    move/from16 v5, p10

    #@c2
    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller$SplineOverScroller;->fling(IIIII)V

    #@c5
    .line 517
    return-void

    #@c6
    .line 490
    .restart local v6       #nowFlingTime:J
    .restart local v8       #oldVelocityX:F
    .restart local v9       #oldVelocityY:F
    .restart local v10       #usingAcceleration:Z
    :cond_c6
    const v0, -0x9c40

    #@c9
    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    #@cc
    move-result p3

    #@cd
    goto :goto_96

    #@ce
    .line 495
    :cond_ce
    const v0, -0x9c40

    #@d1
    invoke-static {p4, v0}, Ljava/lang/Math;->max(II)I

    #@d4
    move-result p4

    #@d5
    goto :goto_9f

    #@d6
    .line 499
    :cond_d6
    const/4 v0, 0x0

    #@d7
    iput v0, p0, Landroid/widget/OverScroller;->accelerationCount:I

    #@d9
    goto :goto_9f

    #@da
    .line 502
    .end local v6           #nowFlingTime:J
    .end local v10           #usingAcceleration:Z
    :cond_da
    int-to-float v0, p3

    #@db
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    #@de
    move-result v0

    #@df
    invoke-static {v8}, Ljava/lang/Math;->signum(F)F

    #@e2
    move-result v1

    #@e3
    cmpl-float v0, v0, v1

    #@e5
    if-nez v0, :cond_9f

    #@e7
    int-to-float v0, p4

    #@e8
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    #@eb
    move-result v0

    #@ec
    invoke-static {v9}, Ljava/lang/Math;->signum(F)F

    #@ef
    move-result v1

    #@f0
    cmpl-float v0, v0, v1

    #@f2
    if-nez v0, :cond_9f

    #@f4
    .line 504
    int-to-float v0, p3

    #@f5
    add-float/2addr v0, v8

    #@f6
    float-to-int p3, v0

    #@f7
    .line 505
    int-to-float v0, p4

    #@f8
    add-float/2addr v0, v9

    #@f9
    float-to-int p4, v0

    #@fa
    goto :goto_9f
.end method

.method public final forceFinished(Z)V
    .registers 4
    .parameter "finished"

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    iget-object v1, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@4
    invoke-static {v1, p1}, Landroid/widget/OverScroller$SplineOverScroller;->access$002(Landroid/widget/OverScroller$SplineOverScroller;Z)Z

    #@7
    move-result v1

    #@8
    invoke-static {v0, v1}, Landroid/widget/OverScroller$SplineOverScroller;->access$002(Landroid/widget/OverScroller$SplineOverScroller;Z)Z

    #@b
    .line 164
    return-void
.end method

.method public getCurrVelocity()F
    .registers 4

    #@0
    .prologue
    .line 190
    iget-object v1, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v1}, Landroid/widget/OverScroller$SplineOverScroller;->access$200(Landroid/widget/OverScroller$SplineOverScroller;)F

    #@5
    move-result v1

    #@6
    iget-object v2, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@8
    invoke-static {v2}, Landroid/widget/OverScroller$SplineOverScroller;->access$200(Landroid/widget/OverScroller$SplineOverScroller;)F

    #@b
    move-result v2

    #@c
    mul-float v0, v1, v2

    #@e
    .line 191
    .local v0, squaredNorm:F
    iget-object v1, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@10
    invoke-static {v1}, Landroid/widget/OverScroller$SplineOverScroller;->access$200(Landroid/widget/OverScroller$SplineOverScroller;)F

    #@13
    move-result v1

    #@14
    iget-object v2, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@16
    invoke-static {v2}, Landroid/widget/OverScroller$SplineOverScroller;->access$200(Landroid/widget/OverScroller$SplineOverScroller;)F

    #@19
    move-result v2

    #@1a
    mul-float/2addr v1, v2

    #@1b
    add-float/2addr v0, v1

    #@1c
    .line 192
    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    #@1f
    move-result v1

    #@20
    return v1
.end method

.method public final getCurrX()I
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$100(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getCurrY()I
    .registers 2

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$100(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getDuration()I
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$500(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@8
    invoke-static {v1}, Landroid/widget/OverScroller$SplineOverScroller;->access$500(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@b
    move-result v1

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public final getFinalX()I
    .registers 2

    #@0
    .prologue
    .line 219
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$400(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getFinalY()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$400(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getStartX()I
    .registers 2

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$300(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getStartY()I
    .registers 2

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$300(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final isFinished()Z
    .registers 2

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@a
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public isOverScrolled()Z
    .registers 2

    #@0
    .prologue
    .line 567
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@a
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$700(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_20

    #@10
    :cond_10
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@12
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$000(Landroid/widget/OverScroller$SplineOverScroller;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_22

    #@18
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@1a
    invoke-static {v0}, Landroid/widget/OverScroller$SplineOverScroller;->access$700(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public isScrollingInDirection(FF)Z
    .registers 7
    .parameter "xvel"
    .parameter "yvel"

    #@0
    .prologue
    .line 602
    iget-object v2, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-static {v2}, Landroid/widget/OverScroller$SplineOverScroller;->access$400(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@5
    move-result v2

    #@6
    iget-object v3, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@8
    invoke-static {v3}, Landroid/widget/OverScroller$SplineOverScroller;->access$300(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@b
    move-result v3

    #@c
    sub-int v0, v2, v3

    #@e
    .line 603
    .local v0, dx:I
    iget-object v2, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@10
    invoke-static {v2}, Landroid/widget/OverScroller$SplineOverScroller;->access$400(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@13
    move-result v2

    #@14
    iget-object v3, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@16
    invoke-static {v3}, Landroid/widget/OverScroller$SplineOverScroller;->access$300(Landroid/widget/OverScroller$SplineOverScroller;)I

    #@19
    move-result v3

    #@1a
    sub-int v1, v2, v3

    #@1c
    .line 604
    .local v1, dy:I
    invoke-virtual {p0}, Landroid/widget/OverScroller;->isFinished()Z

    #@1f
    move-result v2

    #@20
    if-nez v2, :cond_3e

    #@22
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    #@25
    move-result v2

    #@26
    int-to-float v3, v0

    #@27
    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    #@2a
    move-result v3

    #@2b
    cmpl-float v2, v2, v3

    #@2d
    if-nez v2, :cond_3e

    #@2f
    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    #@32
    move-result v2

    #@33
    int-to-float v3, v1

    #@34
    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    #@37
    move-result v3

    #@38
    cmpl-float v2, v2, v3

    #@3a
    if-nez v2, :cond_3e

    #@3c
    const/4 v2, 0x1

    #@3d
    :goto_3d
    return v2

    #@3e
    :cond_3e
    const/4 v2, 0x0

    #@3f
    goto :goto_3d
.end method

.method public notifyHorizontalEdgeReached(III)V
    .registers 5
    .parameter "startX"
    .parameter "finalX"
    .parameter "overX"

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/OverScroller$SplineOverScroller;->notifyEdgeReached(III)V

    #@5
    .line 534
    return-void
.end method

.method public notifyVerticalEdgeReached(III)V
    .registers 5
    .parameter "startY"
    .parameter "finalY"
    .parameter "overY"

    #@0
    .prologue
    .line 550
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/OverScroller$SplineOverScroller;->notifyEdgeReached(III)V

    #@5
    .line 551
    return-void
.end method

.method public setFinalX(I)V
    .registers 3
    .parameter "newX"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 280
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->setFinalPosition(I)V

    #@5
    .line 281
    return-void
.end method

.method public setFinalY(I)V
    .registers 3
    .parameter "newY"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 298
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->setFinalPosition(I)V

    #@5
    .line 299
    return-void
.end method

.method public final setFriction(F)V
    .registers 3
    .parameter "friction"

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->setFriction(F)V

    #@5
    .line 141
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@7
    invoke-virtual {v0, p1}, Landroid/widget/OverScroller$SplineOverScroller;->setFriction(F)V

    #@a
    .line 142
    return-void
.end method

.method setInterpolator(Landroid/view/animation/Interpolator;)V
    .registers 2
    .parameter "interpolator"

    #@0
    .prologue
    .line 129
    iput-object p1, p0, Landroid/widget/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    .line 130
    return-void
.end method

.method public springBack(IIIIII)Z
    .registers 11
    .parameter "startX"
    .parameter "startY"
    .parameter "minX"
    .parameter "maxX"
    .parameter "minY"
    .parameter "maxY"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 407
    iput v2, p0, Landroid/widget/OverScroller;->mMode:I

    #@3
    .line 410
    iget-object v3, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@5
    invoke-virtual {v3, p1, p3, p4}, Landroid/widget/OverScroller$SplineOverScroller;->springback(III)Z

    #@8
    move-result v0

    #@9
    .line 411
    .local v0, spingbackX:Z
    iget-object v3, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@b
    invoke-virtual {v3, p2, p5, p6}, Landroid/widget/OverScroller$SplineOverScroller;->springback(III)Z

    #@e
    move-result v1

    #@f
    .line 412
    .local v1, spingbackY:Z
    if-nez v0, :cond_13

    #@11
    if-eqz v1, :cond_14

    #@13
    :cond_13
    :goto_13
    return v2

    #@14
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_13
.end method

.method public startScroll(IIII)V
    .registers 11
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 372
    const/16 v5, 0xfa

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    #@a
    .line 373
    return-void
.end method

.method public startScroll(IIIII)V
    .registers 7
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"
    .parameter "duration"

    #@0
    .prologue
    .line 389
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/widget/OverScroller;->mMode:I

    #@3
    .line 390
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@5
    invoke-virtual {v0, p1, p3, p5}, Landroid/widget/OverScroller$SplineOverScroller;->startScroll(III)V

    #@8
    .line 391
    iget-object v0, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@a
    invoke-virtual {v0, p2, p4, p5}, Landroid/widget/OverScroller$SplineOverScroller;->startScroll(III)V

    #@d
    .line 392
    return-void
.end method

.method public timePassed()I
    .registers 9

    #@0
    .prologue
    .line 593
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v2

    #@4
    .line 594
    .local v2, time:J
    iget-object v4, p0, Landroid/widget/OverScroller;->mScrollerX:Landroid/widget/OverScroller$SplineOverScroller;

    #@6
    invoke-static {v4}, Landroid/widget/OverScroller$SplineOverScroller;->access$600(Landroid/widget/OverScroller$SplineOverScroller;)J

    #@9
    move-result-wide v4

    #@a
    iget-object v6, p0, Landroid/widget/OverScroller;->mScrollerY:Landroid/widget/OverScroller$SplineOverScroller;

    #@c
    invoke-static {v6}, Landroid/widget/OverScroller$SplineOverScroller;->access$600(Landroid/widget/OverScroller$SplineOverScroller;)J

    #@f
    move-result-wide v6

    #@10
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    #@13
    move-result-wide v0

    #@14
    .line 595
    .local v0, startTime:J
    sub-long v4, v2, v0

    #@16
    long-to-int v4, v4

    #@17
    return v4
.end method
