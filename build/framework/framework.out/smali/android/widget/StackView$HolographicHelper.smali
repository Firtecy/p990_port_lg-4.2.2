.class Landroid/widget/StackView$HolographicHelper;
.super Ljava/lang/Object;
.source "StackView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/StackView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HolographicHelper"
.end annotation


# static fields
.field private static final CLICK_FEEDBACK:I = 0x1

.field private static final RES_OUT:I


# instance fields
.field private final mBlurPaint:Landroid/graphics/Paint;

.field private final mCanvas:Landroid/graphics/Canvas;

.field private mDensity:F

.field private final mErasePaint:Landroid/graphics/Paint;

.field private final mHolographicPaint:Landroid/graphics/Paint;

.field private final mIdentityMatrix:Landroid/graphics/Matrix;

.field private mLargeBlurMaskFilter:Landroid/graphics/BlurMaskFilter;

.field private final mMaskCanvas:Landroid/graphics/Canvas;

.field private mSmallBlurMaskFilter:Landroid/graphics/BlurMaskFilter;

.field private final mTmpXY:[I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1383
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 1370
    new-instance v0, Landroid/graphics/Paint;

    #@6
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@9
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mHolographicPaint:Landroid/graphics/Paint;

    #@b
    .line 1371
    new-instance v0, Landroid/graphics/Paint;

    #@d
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@10
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mErasePaint:Landroid/graphics/Paint;

    #@12
    .line 1372
    new-instance v0, Landroid/graphics/Paint;

    #@14
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@17
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mBlurPaint:Landroid/graphics/Paint;

    #@19
    .line 1378
    new-instance v0, Landroid/graphics/Canvas;

    #@1b
    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    #@1e
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mCanvas:Landroid/graphics/Canvas;

    #@20
    .line 1379
    new-instance v0, Landroid/graphics/Canvas;

    #@22
    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    #@25
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mMaskCanvas:Landroid/graphics/Canvas;

    #@27
    .line 1380
    const/4 v0, 0x2

    #@28
    new-array v0, v0, [I

    #@2a
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mTmpXY:[I

    #@2c
    .line 1381
    new-instance v0, Landroid/graphics/Matrix;

    #@2e
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@31
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mIdentityMatrix:Landroid/graphics/Matrix;

    #@33
    .line 1384
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@3a
    move-result-object v0

    #@3b
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    #@3d
    iput v0, p0, Landroid/widget/StackView$HolographicHelper;->mDensity:F

    #@3f
    .line 1386
    iget-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mHolographicPaint:Landroid/graphics/Paint;

    #@41
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@44
    .line 1387
    iget-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mHolographicPaint:Landroid/graphics/Paint;

    #@46
    const/4 v1, 0x0

    #@47
    const/16 v2, 0x1e

    #@49
    invoke-static {v1, v2}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    #@50
    .line 1388
    iget-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mErasePaint:Landroid/graphics/Paint;

    #@52
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    #@54
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    #@56
    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@59
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@5c
    .line 1389
    iget-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mErasePaint:Landroid/graphics/Paint;

    #@5e
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@61
    .line 1391
    new-instance v0, Landroid/graphics/BlurMaskFilter;

    #@63
    const/high16 v1, 0x4000

    #@65
    iget v2, p0, Landroid/widget/StackView$HolographicHelper;->mDensity:F

    #@67
    mul-float/2addr v1, v2

    #@68
    sget-object v2, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    #@6a
    invoke-direct {v0, v1, v2}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    #@6d
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mSmallBlurMaskFilter:Landroid/graphics/BlurMaskFilter;

    #@6f
    .line 1392
    new-instance v0, Landroid/graphics/BlurMaskFilter;

    #@71
    const/high16 v1, 0x4080

    #@73
    iget v2, p0, Landroid/widget/StackView$HolographicHelper;->mDensity:F

    #@75
    mul-float/2addr v1, v2

    #@76
    sget-object v2, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    #@78
    invoke-direct {v0, v1, v2}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    #@7b
    iput-object v0, p0, Landroid/widget/StackView$HolographicHelper;->mLargeBlurMaskFilter:Landroid/graphics/BlurMaskFilter;

    #@7d
    .line 1393
    return-void
.end method


# virtual methods
.method createClickOutline(Landroid/view/View;I)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "v"
    .parameter "color"

    #@0
    .prologue
    .line 1396
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/StackView$HolographicHelper;->createOutline(Landroid/view/View;II)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method createOutline(Landroid/view/View;II)Landroid/graphics/Bitmap;
    .registers 15
    .parameter "v"
    .parameter "type"
    .parameter "color"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v10, 0x0

    #@2
    .line 1404
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mHolographicPaint:Landroid/graphics/Paint;

    #@4
    invoke-virtual {v6, p3}, Landroid/graphics/Paint;->setColor(I)V

    #@7
    .line 1405
    if-nez p2, :cond_1e

    #@9
    .line 1406
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mBlurPaint:Landroid/graphics/Paint;

    #@b
    iget-object v7, p0, Landroid/widget/StackView$HolographicHelper;->mSmallBlurMaskFilter:Landroid/graphics/BlurMaskFilter;

    #@d
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    #@10
    .line 1411
    :cond_10
    :goto_10
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@13
    move-result v6

    #@14
    if-eqz v6, :cond_1c

    #@16
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_29

    #@1c
    :cond_1c
    move-object v0, v5

    #@1d
    .line 1435
    :goto_1d
    return-object v0

    #@1e
    .line 1407
    :cond_1e
    const/4 v6, 0x1

    #@1f
    if-ne p2, v6, :cond_10

    #@21
    .line 1408
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mBlurPaint:Landroid/graphics/Paint;

    #@23
    iget-object v7, p0, Landroid/widget/StackView$HolographicHelper;->mLargeBlurMaskFilter:Landroid/graphics/BlurMaskFilter;

    #@25
    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    #@28
    goto :goto_10

    #@29
    .line 1415
    :cond_29
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@34
    move-result v7

    #@35
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@38
    move-result v8

    #@39
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@3b
    invoke-static {v6, v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@3e
    move-result-object v0

    #@3f
    .line 1417
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mCanvas:Landroid/graphics/Canvas;

    #@41
    invoke-virtual {v6, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@44
    .line 1419
    invoke-virtual {p1}, Landroid/view/View;->getRotationX()F

    #@47
    move-result v2

    #@48
    .line 1420
    .local v2, rotationX:F
    invoke-virtual {p1}, Landroid/view/View;->getRotation()F

    #@4b
    move-result v1

    #@4c
    .line 1421
    .local v1, rotation:F
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    #@4f
    move-result v4

    #@50
    .line 1422
    .local v4, translationY:F
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    #@53
    move-result v3

    #@54
    .line 1423
    .local v3, translationX:F
    invoke-virtual {p1, v10}, Landroid/view/View;->setRotationX(F)V

    #@57
    .line 1424
    invoke-virtual {p1, v10}, Landroid/view/View;->setRotation(F)V

    #@5a
    .line 1425
    invoke-virtual {p1, v10}, Landroid/view/View;->setTranslationY(F)V

    #@5d
    .line 1426
    invoke-virtual {p1, v10}, Landroid/view/View;->setTranslationX(F)V

    #@60
    .line 1427
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mCanvas:Landroid/graphics/Canvas;

    #@62
    invoke-virtual {p1, v6}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    #@65
    .line 1428
    invoke-virtual {p1, v2}, Landroid/view/View;->setRotationX(F)V

    #@68
    .line 1429
    invoke-virtual {p1, v1}, Landroid/view/View;->setRotation(F)V

    #@6b
    .line 1430
    invoke-virtual {p1, v4}, Landroid/view/View;->setTranslationY(F)V

    #@6e
    .line 1431
    invoke-virtual {p1, v3}, Landroid/view/View;->setTranslationX(F)V

    #@71
    .line 1433
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mCanvas:Landroid/graphics/Canvas;

    #@73
    invoke-virtual {p0, v6, v0}, Landroid/widget/StackView$HolographicHelper;->drawOutline(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    #@76
    .line 1434
    iget-object v6, p0, Landroid/widget/StackView$HolographicHelper;->mCanvas:Landroid/graphics/Canvas;

    #@78
    invoke-virtual {v6, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@7b
    goto :goto_1d
.end method

.method createResOutline(Landroid/view/View;I)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "v"
    .parameter "color"

    #@0
    .prologue
    .line 1400
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0, p2}, Landroid/widget/StackView$HolographicHelper;->createOutline(Landroid/view/View;II)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method drawOutline(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .registers 11
    .parameter "dest"
    .parameter "src"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1439
    iget-object v1, p0, Landroid/widget/StackView$HolographicHelper;->mTmpXY:[I

    #@4
    .line 1440
    .local v1, xy:[I
    iget-object v2, p0, Landroid/widget/StackView$HolographicHelper;->mBlurPaint:Landroid/graphics/Paint;

    #@6
    invoke-virtual {p2, v2, v1}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    #@9
    move-result-object v0

    #@a
    .line 1441
    .local v0, mask:Landroid/graphics/Bitmap;
    iget-object v2, p0, Landroid/widget/StackView$HolographicHelper;->mMaskCanvas:Landroid/graphics/Canvas;

    #@c
    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@f
    .line 1442
    iget-object v2, p0, Landroid/widget/StackView$HolographicHelper;->mMaskCanvas:Landroid/graphics/Canvas;

    #@11
    aget v3, v1, v6

    #@13
    neg-int v3, v3

    #@14
    int-to-float v3, v3

    #@15
    aget v4, v1, v7

    #@17
    neg-int v4, v4

    #@18
    int-to-float v4, v4

    #@19
    iget-object v5, p0, Landroid/widget/StackView$HolographicHelper;->mErasePaint:Landroid/graphics/Paint;

    #@1b
    invoke-virtual {v2, p2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@1e
    .line 1443
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@20
    invoke-virtual {p1, v6, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@23
    .line 1444
    iget-object v2, p0, Landroid/widget/StackView$HolographicHelper;->mIdentityMatrix:Landroid/graphics/Matrix;

    #@25
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    #@28
    .line 1445
    aget v2, v1, v6

    #@2a
    int-to-float v2, v2

    #@2b
    aget v3, v1, v7

    #@2d
    int-to-float v3, v3

    #@2e
    iget-object v4, p0, Landroid/widget/StackView$HolographicHelper;->mHolographicPaint:Landroid/graphics/Paint;

    #@30
    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@33
    .line 1446
    iget-object v2, p0, Landroid/widget/StackView$HolographicHelper;->mMaskCanvas:Landroid/graphics/Canvas;

    #@35
    const/4 v3, 0x0

    #@36
    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@39
    .line 1447
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@3c
    .line 1448
    return-void
.end method
