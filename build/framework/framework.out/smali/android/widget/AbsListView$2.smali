.class Landroid/widget/AbsListView$2;
.super Ljava/lang/Object;
.source "AbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/widget/AbsListView;->clearScrollingCache()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AbsListView;


# direct methods
.method constructor <init>(Landroid/widget/AbsListView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 5003
    iput-object p1, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5005
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@3
    iget-boolean v0, v0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@5
    if-eqz v0, :cond_30

    #@7
    .line 5006
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@9
    iget-object v1, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@b
    iput-boolean v2, v1, Landroid/widget/AbsListView;->mCachingActive:Z

    #@d
    iput-boolean v2, v0, Landroid/widget/AbsListView;->mCachingStarted:Z

    #@f
    .line 5007
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@11
    invoke-static {v0, v2}, Landroid/widget/AbsListView;->access$3100(Landroid/widget/AbsListView;Z)V

    #@14
    .line 5008
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@16
    invoke-static {v0}, Landroid/widget/AbsListView;->access$3200(Landroid/widget/AbsListView;)I

    #@19
    move-result v0

    #@1a
    and-int/lit8 v0, v0, 0x2

    #@1c
    if-nez v0, :cond_23

    #@1e
    .line 5009
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@20
    invoke-static {v0, v2}, Landroid/widget/AbsListView;->access$3300(Landroid/widget/AbsListView;Z)V

    #@23
    .line 5011
    :cond_23
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@25
    invoke-virtual {v0}, Landroid/widget/AbsListView;->isAlwaysDrawnWithCacheEnabled()Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_30

    #@2b
    .line 5012
    iget-object v0, p0, Landroid/widget/AbsListView$2;->this$0:Landroid/widget/AbsListView;

    #@2d
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    #@30
    .line 5015
    :cond_30
    return-void
.end method
