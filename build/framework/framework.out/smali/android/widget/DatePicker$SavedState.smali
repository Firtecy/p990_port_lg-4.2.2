.class Landroid/widget/DatePicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "DatePicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/DatePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/widget/DatePicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDay:I

.field private final mMonth:I

.field private final mYear:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 855
    new-instance v0, Landroid/widget/DatePicker$SavedState$1;

    #@2
    invoke-direct {v0}, Landroid/widget/DatePicker$SavedState$1;-><init>()V

    #@5
    sput-object v0, Landroid/widget/DatePicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 839
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    #@3
    .line 840
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/widget/DatePicker$SavedState;->mYear:I

    #@9
    .line 841
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/widget/DatePicker$SavedState;->mMonth:I

    #@f
    .line 842
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/widget/DatePicker$SavedState;->mDay:I

    #@15
    .line 843
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/widget/DatePicker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 817
    invoke-direct {p0, p1}, Landroid/widget/DatePicker$SavedState;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;III)V
    .registers 5
    .parameter "superState"
    .parameter "year"
    .parameter "month"
    .parameter "day"

    #@0
    .prologue
    .line 829
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    #@3
    .line 830
    iput p2, p0, Landroid/widget/DatePicker$SavedState;->mYear:I

    #@5
    .line 831
    iput p3, p0, Landroid/widget/DatePicker$SavedState;->mMonth:I

    #@7
    .line 832
    iput p4, p0, Landroid/widget/DatePicker$SavedState;->mDay:I

    #@9
    .line 833
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;IIILandroid/widget/DatePicker$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 817
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/DatePicker$SavedState;-><init>(Landroid/os/Parcelable;III)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/widget/DatePicker$SavedState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 817
    iget v0, p0, Landroid/widget/DatePicker$SavedState;->mYear:I

    #@2
    return v0
.end method

.method static synthetic access$1300(Landroid/widget/DatePicker$SavedState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 817
    iget v0, p0, Landroid/widget/DatePicker$SavedState;->mMonth:I

    #@2
    return v0
.end method

.method static synthetic access$1400(Landroid/widget/DatePicker$SavedState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 817
    iget v0, p0, Landroid/widget/DatePicker$SavedState;->mDay:I

    #@2
    return v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 847
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    #@3
    .line 848
    iget v0, p0, Landroid/widget/DatePicker$SavedState;->mYear:I

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 849
    iget v0, p0, Landroid/widget/DatePicker$SavedState;->mMonth:I

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 850
    iget v0, p0, Landroid/widget/DatePicker$SavedState;->mDay:I

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 851
    return-void
.end method
