.class final Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;
.super Landroid/os/AsyncTask;
.source "ActivityChooserModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ActivityChooserModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PersistHistoryAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/ActivityChooserModel;


# direct methods
.method private constructor <init>(Landroid/widget/ActivityChooserModel;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1049
    iput-object p1, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ActivityChooserModel;Landroid/widget/ActivityChooserModel$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1049
    invoke-direct {p0, p1}, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;-><init>(Landroid/widget/ActivityChooserModel;)V

    #@3
    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 1049
    invoke-virtual {p0, p1}, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .registers 17
    .parameter "args"

    #@0
    .prologue
    .line 1054
    const/4 v11, 0x0

    #@1
    aget-object v2, p1, v11

    #@3
    check-cast v2, Ljava/util/List;

    #@5
    .line 1055
    .local v2, historicalRecords:Ljava/util/List;,"Ljava/util/List<Landroid/widget/ActivityChooserModel$HistoricalRecord;>;"
    const/4 v11, 0x1

    #@6
    aget-object v3, p1, v11

    #@8
    check-cast v3, Ljava/lang/String;

    #@a
    .line 1057
    .local v3, hostoryFileName:Ljava/lang/String;
    const/4 v1, 0x0

    #@b
    .line 1060
    .local v1, fos:Ljava/io/FileOutputStream;
    :try_start_b
    iget-object v11, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@d
    invoke-static {v11}, Landroid/widget/ActivityChooserModel;->access$300(Landroid/widget/ActivityChooserModel;)Landroid/content/Context;

    #@10
    move-result-object v11

    #@11
    const/4 v12, 0x0

    #@12
    invoke-virtual {v11, v3, v12}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_15} :catch_71

    #@15
    move-result-object v1

    #@16
    .line 1066
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    #@19
    move-result-object v10

    #@1a
    .line 1069
    .local v10, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const/4 v11, 0x0

    #@1b
    :try_start_1b
    invoke-interface {v10, v1, v11}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@1e
    .line 1070
    const-string v11, "UTF-8"

    #@20
    const/4 v12, 0x1

    #@21
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@24
    move-result-object v12

    #@25
    invoke-interface {v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@28
    .line 1071
    const/4 v11, 0x0

    #@29
    const-string v12, "historical-records"

    #@2b
    invoke-interface {v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2e
    .line 1073
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@31
    move-result v9

    #@32
    .line 1074
    .local v9, recordCount:I
    const/4 v4, 0x0

    #@33
    .local v4, i:I
    :goto_33
    if-ge v4, v9, :cond_8e

    #@35
    .line 1075
    const/4 v11, 0x0

    #@36
    invoke-interface {v2, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@39
    move-result-object v8

    #@3a
    check-cast v8, Landroid/widget/ActivityChooserModel$HistoricalRecord;

    #@3c
    .line 1076
    .local v8, record:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    const/4 v11, 0x0

    #@3d
    const-string v12, "historical-record"

    #@3f
    invoke-interface {v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@42
    .line 1077
    const/4 v11, 0x0

    #@43
    const-string v12, "activity"

    #@45
    iget-object v13, v8, Landroid/widget/ActivityChooserModel$HistoricalRecord;->activity:Landroid/content/ComponentName;

    #@47
    invoke-virtual {v13}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@4a
    move-result-object v13

    #@4b
    invoke-interface {v10, v11, v12, v13}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4e
    .line 1079
    const/4 v11, 0x0

    #@4f
    const-string/jumbo v12, "time"

    #@52
    iget-wide v13, v8, Landroid/widget/ActivityChooserModel$HistoricalRecord;->time:J

    #@54
    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@57
    move-result-object v13

    #@58
    invoke-interface {v10, v11, v12, v13}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5b
    .line 1080
    const/4 v11, 0x0

    #@5c
    const-string/jumbo v12, "weight"

    #@5f
    iget v13, v8, Landroid/widget/ActivityChooserModel$HistoricalRecord;->weight:F

    #@61
    invoke-static {v13}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    #@64
    move-result-object v13

    #@65
    invoke-interface {v10, v11, v12, v13}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@68
    .line 1081
    const/4 v11, 0x0

    #@69
    const-string v12, "historical-record"

    #@6b
    invoke-interface {v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_6e
    .catchall {:try_start_1b .. :try_end_6e} :catchall_12e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b .. :try_end_6e} :catch_a4
    .catch Ljava/lang/IllegalStateException; {:try_start_1b .. :try_end_6e} :catch_d3
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_6e} :catch_100

    #@6e
    .line 1074
    add-int/lit8 v4, v4, 0x1

    #@70
    goto :goto_33

    #@71
    .line 1061
    .end local v4           #i:I
    .end local v8           #record:Landroid/widget/ActivityChooserModel$HistoricalRecord;
    .end local v9           #recordCount:I
    .end local v10           #serializer:Lorg/xmlpull/v1/XmlSerializer;
    :catch_71
    move-exception v0

    #@72
    .line 1062
    .local v0, fnfe:Ljava/io/FileNotFoundException;
    invoke-static {}, Landroid/widget/ActivityChooserModel;->access$400()Ljava/lang/String;

    #@75
    move-result-object v11

    #@76
    new-instance v12, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v13, "Error writing historical recrod file: "

    #@7d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v12

    #@81
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v12

    #@85
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v12

    #@89
    invoke-static {v11, v12, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8c
    .line 1063
    const/4 v11, 0x0

    #@8d
    .line 1109
    .end local v0           #fnfe:Ljava/io/FileNotFoundException;
    :goto_8d
    return-object v11

    #@8e
    .line 1087
    .restart local v4       #i:I
    .restart local v9       #recordCount:I
    .restart local v10       #serializer:Lorg/xmlpull/v1/XmlSerializer;
    :cond_8e
    const/4 v11, 0x0

    #@8f
    :try_start_8f
    const-string v12, "historical-records"

    #@91
    invoke-interface {v10, v11, v12}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@94
    .line 1088
    invoke-interface {v10}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V
    :try_end_97
    .catchall {:try_start_8f .. :try_end_97} :catchall_12e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8f .. :try_end_97} :catch_a4
    .catch Ljava/lang/IllegalStateException; {:try_start_8f .. :try_end_97} :catch_d3
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_97} :catch_100

    #@97
    .line 1100
    iget-object v11, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@99
    const/4 v12, 0x1

    #@9a
    invoke-static {v11, v12}, Landroid/widget/ActivityChooserModel;->access$602(Landroid/widget/ActivityChooserModel;Z)Z

    #@9d
    .line 1101
    if-eqz v1, :cond_a2

    #@9f
    .line 1103
    :try_start_9f
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_a2
    .catch Ljava/io/IOException; {:try_start_9f .. :try_end_a2} :catch_d1

    #@a2
    .line 1109
    .end local v4           #i:I
    .end local v9           #recordCount:I
    :cond_a2
    :goto_a2
    const/4 v11, 0x0

    #@a3
    goto :goto_8d

    #@a4
    .line 1093
    :catch_a4
    move-exception v5

    #@a5
    .line 1094
    .local v5, iae:Ljava/lang/IllegalArgumentException;
    :try_start_a5
    invoke-static {}, Landroid/widget/ActivityChooserModel;->access$400()Ljava/lang/String;

    #@a8
    move-result-object v11

    #@a9
    new-instance v12, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v13, "Error writing historical recrod file: "

    #@b0
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v12

    #@b4
    iget-object v13, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@b6
    invoke-static {v13}, Landroid/widget/ActivityChooserModel;->access$500(Landroid/widget/ActivityChooserModel;)Ljava/lang/String;

    #@b9
    move-result-object v13

    #@ba
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v12

    #@be
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v12

    #@c2
    invoke-static {v11, v12, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c5
    .catchall {:try_start_a5 .. :try_end_c5} :catchall_12e

    #@c5
    .line 1100
    iget-object v11, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@c7
    const/4 v12, 0x1

    #@c8
    invoke-static {v11, v12}, Landroid/widget/ActivityChooserModel;->access$602(Landroid/widget/ActivityChooserModel;Z)Z

    #@cb
    .line 1101
    if-eqz v1, :cond_a2

    #@cd
    .line 1103
    :try_start_cd
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_d0
    .catch Ljava/io/IOException; {:try_start_cd .. :try_end_d0} :catch_d1

    #@d0
    goto :goto_a2

    #@d1
    .line 1104
    .end local v5           #iae:Ljava/lang/IllegalArgumentException;
    :catch_d1
    move-exception v11

    #@d2
    goto :goto_a2

    #@d3
    .line 1095
    :catch_d3
    move-exception v7

    #@d4
    .line 1096
    .local v7, ise:Ljava/lang/IllegalStateException;
    :try_start_d4
    invoke-static {}, Landroid/widget/ActivityChooserModel;->access$400()Ljava/lang/String;

    #@d7
    move-result-object v11

    #@d8
    new-instance v12, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v13, "Error writing historical recrod file: "

    #@df
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v12

    #@e3
    iget-object v13, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@e5
    invoke-static {v13}, Landroid/widget/ActivityChooserModel;->access$500(Landroid/widget/ActivityChooserModel;)Ljava/lang/String;

    #@e8
    move-result-object v13

    #@e9
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v12

    #@ed
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v12

    #@f1
    invoke-static {v11, v12, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f4
    .catchall {:try_start_d4 .. :try_end_f4} :catchall_12e

    #@f4
    .line 1100
    iget-object v11, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@f6
    const/4 v12, 0x1

    #@f7
    invoke-static {v11, v12}, Landroid/widget/ActivityChooserModel;->access$602(Landroid/widget/ActivityChooserModel;Z)Z

    #@fa
    .line 1101
    if-eqz v1, :cond_a2

    #@fc
    .line 1103
    :try_start_fc
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_ff
    .catch Ljava/io/IOException; {:try_start_fc .. :try_end_ff} :catch_d1

    #@ff
    goto :goto_a2

    #@100
    .line 1097
    .end local v7           #ise:Ljava/lang/IllegalStateException;
    :catch_100
    move-exception v6

    #@101
    .line 1098
    .local v6, ioe:Ljava/io/IOException;
    :try_start_101
    invoke-static {}, Landroid/widget/ActivityChooserModel;->access$400()Ljava/lang/String;

    #@104
    move-result-object v11

    #@105
    new-instance v12, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v13, "Error writing historical recrod file: "

    #@10c
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v12

    #@110
    iget-object v13, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@112
    invoke-static {v13}, Landroid/widget/ActivityChooserModel;->access$500(Landroid/widget/ActivityChooserModel;)Ljava/lang/String;

    #@115
    move-result-object v13

    #@116
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v12

    #@11a
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11d
    move-result-object v12

    #@11e
    invoke-static {v11, v12, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_121
    .catchall {:try_start_101 .. :try_end_121} :catchall_12e

    #@121
    .line 1100
    iget-object v11, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@123
    const/4 v12, 0x1

    #@124
    invoke-static {v11, v12}, Landroid/widget/ActivityChooserModel;->access$602(Landroid/widget/ActivityChooserModel;Z)Z

    #@127
    .line 1101
    if-eqz v1, :cond_a2

    #@129
    .line 1103
    :try_start_129
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_12c
    .catch Ljava/io/IOException; {:try_start_129 .. :try_end_12c} :catch_d1

    #@12c
    goto/16 :goto_a2

    #@12e
    .line 1100
    .end local v6           #ioe:Ljava/io/IOException;
    :catchall_12e
    move-exception v11

    #@12f
    iget-object v12, p0, Landroid/widget/ActivityChooserModel$PersistHistoryAsyncTask;->this$0:Landroid/widget/ActivityChooserModel;

    #@131
    const/4 v13, 0x1

    #@132
    invoke-static {v12, v13}, Landroid/widget/ActivityChooserModel;->access$602(Landroid/widget/ActivityChooserModel;Z)Z

    #@135
    .line 1101
    if-eqz v1, :cond_13a

    #@137
    .line 1103
    :try_start_137
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_13a
    .catch Ljava/io/IOException; {:try_start_137 .. :try_end_13a} :catch_13b

    #@13a
    .line 1100
    :cond_13a
    :goto_13a
    throw v11

    #@13b
    .line 1104
    :catch_13b
    move-exception v12

    #@13c
    goto :goto_13a
.end method
