.class public Landroid/widget/AnalogClock;
.super Landroid/view/View;
.source "AnalogClock.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# instance fields
.field private mAttached:Z

.field private mCalendar:Landroid/text/format/Time;

.field private mChanged:Z

.field private mDial:Landroid/graphics/drawable/Drawable;

.field private mDialHeight:I

.field private mDialWidth:I

.field private final mHandler:Landroid/os/Handler;

.field private mHour:F

.field private mHourHand:Landroid/graphics/drawable/Drawable;

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mMinuteHand:Landroid/graphics/drawable/Drawable;

.field private mMinutes:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 63
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/AnalogClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AnalogClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 57
    new-instance v2, Landroid/os/Handler;

    #@6
    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    #@9
    iput-object v2, p0, Landroid/widget/AnalogClock;->mHandler:Landroid/os/Handler;

    #@b
    .line 240
    new-instance v2, Landroid/widget/AnalogClock$1;

    #@d
    invoke-direct {v2, p0}, Landroid/widget/AnalogClock$1;-><init>(Landroid/widget/AnalogClock;)V

    #@10
    iput-object v2, p0, Landroid/widget/AnalogClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@12
    .line 73
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v1

    #@18
    .line 74
    .local v1, r:Landroid/content/res/Resources;
    sget-object v2, Lcom/android/internal/R$styleable;->AnalogClock:[I

    #@1a
    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1d
    move-result-object v0

    #@1e
    .line 78
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@21
    move-result-object v2

    #@22
    iput-object v2, p0, Landroid/widget/AnalogClock;->mDial:Landroid/graphics/drawable/Drawable;

    #@24
    .line 79
    iget-object v2, p0, Landroid/widget/AnalogClock;->mDial:Landroid/graphics/drawable/Drawable;

    #@26
    if-nez v2, :cond_31

    #@28
    .line 80
    const v2, 0x1080214

    #@2b
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2e
    move-result-object v2

    #@2f
    iput-object v2, p0, Landroid/widget/AnalogClock;->mDial:Landroid/graphics/drawable/Drawable;

    #@31
    .line 83
    :cond_31
    const/4 v2, 0x1

    #@32
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@35
    move-result-object v2

    #@36
    iput-object v2, p0, Landroid/widget/AnalogClock;->mHourHand:Landroid/graphics/drawable/Drawable;

    #@38
    .line 84
    iget-object v2, p0, Landroid/widget/AnalogClock;->mHourHand:Landroid/graphics/drawable/Drawable;

    #@3a
    if-nez v2, :cond_45

    #@3c
    .line 85
    const v2, 0x1080215

    #@3f
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@42
    move-result-object v2

    #@43
    iput-object v2, p0, Landroid/widget/AnalogClock;->mHourHand:Landroid/graphics/drawable/Drawable;

    #@45
    .line 88
    :cond_45
    const/4 v2, 0x2

    #@46
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@49
    move-result-object v2

    #@4a
    iput-object v2, p0, Landroid/widget/AnalogClock;->mMinuteHand:Landroid/graphics/drawable/Drawable;

    #@4c
    .line 89
    iget-object v2, p0, Landroid/widget/AnalogClock;->mMinuteHand:Landroid/graphics/drawable/Drawable;

    #@4e
    if-nez v2, :cond_59

    #@50
    .line 90
    const v2, 0x1080216

    #@53
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@56
    move-result-object v2

    #@57
    iput-object v2, p0, Landroid/widget/AnalogClock;->mMinuteHand:Landroid/graphics/drawable/Drawable;

    #@59
    .line 93
    :cond_59
    new-instance v2, Landroid/text/format/Time;

    #@5b
    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    #@5e
    iput-object v2, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@60
    .line 95
    iget-object v2, p0, Landroid/widget/AnalogClock;->mDial:Landroid/graphics/drawable/Drawable;

    #@62
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@65
    move-result v2

    #@66
    iput v2, p0, Landroid/widget/AnalogClock;->mDialWidth:I

    #@68
    .line 96
    iget-object v2, p0, Landroid/widget/AnalogClock;->mDial:Landroid/graphics/drawable/Drawable;

    #@6a
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@6d
    move-result v2

    #@6e
    iput v2, p0, Landroid/widget/AnalogClock;->mDialHeight:I

    #@70
    .line 97
    return-void
.end method

.method static synthetic access$002(Landroid/widget/AnalogClock;Landroid/text/format/Time;)Landroid/text/format/Time;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Landroid/widget/AnalogClock;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/widget/AnalogClock;->onTimeChanged()V

    #@3
    return-void
.end method

.method private onTimeChanged()V
    .registers 7

    #@0
    .prologue
    const/high16 v5, 0x4270

    #@2
    .line 227
    iget-object v3, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@4
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    #@7
    .line 229
    iget-object v3, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@9
    iget v0, v3, Landroid/text/format/Time;->hour:I

    #@b
    .line 230
    .local v0, hour:I
    iget-object v3, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@d
    iget v1, v3, Landroid/text/format/Time;->minute:I

    #@f
    .line 231
    .local v1, minute:I
    iget-object v3, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@11
    iget v2, v3, Landroid/text/format/Time;->second:I

    #@13
    .line 233
    .local v2, second:I
    int-to-float v3, v1

    #@14
    int-to-float v4, v2

    #@15
    div-float/2addr v4, v5

    #@16
    add-float/2addr v3, v4

    #@17
    iput v3, p0, Landroid/widget/AnalogClock;->mMinutes:F

    #@19
    .line 234
    int-to-float v3, v0

    #@1a
    iget v4, p0, Landroid/widget/AnalogClock;->mMinutes:F

    #@1c
    div-float/2addr v4, v5

    #@1d
    add-float/2addr v3, v4

    #@1e
    iput v3, p0, Landroid/widget/AnalogClock;->mHour:F

    #@20
    .line 235
    const/4 v3, 0x1

    #@21
    iput-boolean v3, p0, Landroid/widget/AnalogClock;->mChanged:Z

    #@23
    .line 237
    iget-object v3, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@25
    invoke-direct {p0, v3}, Landroid/widget/AnalogClock;->updateContentDescription(Landroid/text/format/Time;)V

    #@28
    .line 238
    return-void
.end method

.method private updateContentDescription(Landroid/text/format/Time;)V
    .registers 8
    .parameter "time"

    #@0
    .prologue
    .line 255
    const/16 v1, 0x81

    #@2
    .line 256
    .local v1, flags:I
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {p1, v3}, Landroid/text/format/Time;->toMillis(Z)J

    #@8
    move-result-wide v3

    #@9
    const/16 v5, 0x81

    #@b
    invoke-static {v2, v3, v4, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 258
    .local v0, contentDescription:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/widget/AnalogClock;->setContentDescription(Ljava/lang/CharSequence;)V

    #@12
    .line 259
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .registers 6

    #@0
    .prologue
    .line 101
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@3
    .line 103
    iget-boolean v1, p0, Landroid/widget/AnalogClock;->mAttached:Z

    #@5
    if-nez v1, :cond_2a

    #@7
    .line 104
    const/4 v1, 0x1

    #@8
    iput-boolean v1, p0, Landroid/widget/AnalogClock;->mAttached:Z

    #@a
    .line 105
    new-instance v0, Landroid/content/IntentFilter;

    #@c
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@f
    .line 107
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 108
    const-string v1, "android.intent.action.TIME_SET"

    #@16
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19
    .line 109
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 111
    invoke-virtual {p0}, Landroid/widget/AnalogClock;->getContext()Landroid/content/Context;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/widget/AnalogClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@24
    const/4 v3, 0x0

    #@25
    iget-object v4, p0, Landroid/widget/AnalogClock;->mHandler:Landroid/os/Handler;

    #@27
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@2a
    .line 118
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_2a
    new-instance v1, Landroid/text/format/Time;

    #@2c
    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    #@2f
    iput-object v1, p0, Landroid/widget/AnalogClock;->mCalendar:Landroid/text/format/Time;

    #@31
    .line 121
    invoke-direct {p0}, Landroid/widget/AnalogClock;->onTimeChanged()V

    #@34
    .line 122
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 126
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@3
    .line 127
    iget-boolean v0, p0, Landroid/widget/AnalogClock;->mAttached:Z

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 128
    invoke-virtual {p0}, Landroid/widget/AnalogClock;->getContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/widget/AnalogClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@10
    .line 129
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/widget/AnalogClock;->mAttached:Z

    #@13
    .line 131
    :cond_13
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 19
    .parameter "canvas"

    #@0
    .prologue
    .line 166
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 168
    move-object/from16 v0, p0

    #@5
    iget-boolean v3, v0, Landroid/widget/AnalogClock;->mChanged:Z

    #@7
    .line 169
    .local v3, changed:Z
    if-eqz v3, :cond_e

    #@9
    .line 170
    const/4 v13, 0x0

    #@a
    move-object/from16 v0, p0

    #@c
    iput-boolean v13, v0, Landroid/widget/AnalogClock;->mChanged:Z

    #@e
    .line 173
    :cond_e
    move-object/from16 v0, p0

    #@10
    iget v13, v0, Landroid/view/View;->mRight:I

    #@12
    move-object/from16 v0, p0

    #@14
    iget v14, v0, Landroid/view/View;->mLeft:I

    #@16
    sub-int v2, v13, v14

    #@18
    .line 174
    .local v2, availableWidth:I
    move-object/from16 v0, p0

    #@1a
    iget v13, v0, Landroid/view/View;->mBottom:I

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget v14, v0, Landroid/view/View;->mTop:I

    #@20
    sub-int v1, v13, v14

    #@22
    .line 176
    .local v1, availableHeight:I
    div-int/lit8 v11, v2, 0x2

    #@24
    .line 177
    .local v11, x:I
    div-int/lit8 v12, v1, 0x2

    #@26
    .line 179
    .local v12, y:I
    move-object/from16 v0, p0

    #@28
    iget-object v4, v0, Landroid/widget/AnalogClock;->mDial:Landroid/graphics/drawable/Drawable;

    #@2a
    .line 180
    .local v4, dial:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@2d
    move-result v10

    #@2e
    .line 181
    .local v10, w:I
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@31
    move-result v5

    #@32
    .line 183
    .local v5, h:I
    const/4 v9, 0x0

    #@33
    .line 185
    .local v9, scaled:Z
    if-lt v2, v10, :cond_37

    #@35
    if-ge v1, v5, :cond_4c

    #@37
    .line 186
    :cond_37
    const/4 v9, 0x1

    #@38
    .line 187
    int-to-float v13, v2

    #@39
    int-to-float v14, v10

    #@3a
    div-float/2addr v13, v14

    #@3b
    int-to-float v14, v1

    #@3c
    int-to-float v15, v5

    #@3d
    div-float/2addr v14, v15

    #@3e
    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    #@41
    move-result v8

    #@42
    .line 189
    .local v8, scale:F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@45
    .line 190
    int-to-float v13, v11

    #@46
    int-to-float v14, v12

    #@47
    move-object/from16 v0, p1

    #@49
    invoke-virtual {v0, v8, v8, v13, v14}, Landroid/graphics/Canvas;->scale(FFFF)V

    #@4c
    .line 193
    .end local v8           #scale:F
    :cond_4c
    if-eqz v3, :cond_62

    #@4e
    .line 194
    div-int/lit8 v13, v10, 0x2

    #@50
    sub-int v13, v11, v13

    #@52
    div-int/lit8 v14, v5, 0x2

    #@54
    sub-int v14, v12, v14

    #@56
    div-int/lit8 v15, v10, 0x2

    #@58
    add-int/2addr v15, v11

    #@59
    div-int/lit8 v16, v5, 0x2

    #@5b
    add-int v16, v16, v12

    #@5d
    move/from16 v0, v16

    #@5f
    invoke-virtual {v4, v13, v14, v15, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@62
    .line 196
    :cond_62
    move-object/from16 v0, p1

    #@64
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@67
    .line 198
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@6a
    .line 199
    move-object/from16 v0, p0

    #@6c
    iget v13, v0, Landroid/widget/AnalogClock;->mHour:F

    #@6e
    const/high16 v14, 0x4140

    #@70
    div-float/2addr v13, v14

    #@71
    const/high16 v14, 0x43b4

    #@73
    mul-float/2addr v13, v14

    #@74
    int-to-float v14, v11

    #@75
    int-to-float v15, v12

    #@76
    move-object/from16 v0, p1

    #@78
    invoke-virtual {v0, v13, v14, v15}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@7b
    .line 200
    move-object/from16 v0, p0

    #@7d
    iget-object v6, v0, Landroid/widget/AnalogClock;->mHourHand:Landroid/graphics/drawable/Drawable;

    #@7f
    .line 201
    .local v6, hourHand:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_9d

    #@81
    .line 202
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@84
    move-result v10

    #@85
    .line 203
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@88
    move-result v5

    #@89
    .line 204
    div-int/lit8 v13, v10, 0x2

    #@8b
    sub-int v13, v11, v13

    #@8d
    div-int/lit8 v14, v5, 0x2

    #@8f
    sub-int v14, v12, v14

    #@91
    div-int/lit8 v15, v10, 0x2

    #@93
    add-int/2addr v15, v11

    #@94
    div-int/lit8 v16, v5, 0x2

    #@96
    add-int v16, v16, v12

    #@98
    move/from16 v0, v16

    #@9a
    invoke-virtual {v6, v13, v14, v15, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@9d
    .line 206
    :cond_9d
    move-object/from16 v0, p1

    #@9f
    invoke-virtual {v6, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@a2
    .line 207
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@a5
    .line 209
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@a8
    .line 210
    move-object/from16 v0, p0

    #@aa
    iget v13, v0, Landroid/widget/AnalogClock;->mMinutes:F

    #@ac
    const/high16 v14, 0x4270

    #@ae
    div-float/2addr v13, v14

    #@af
    const/high16 v14, 0x43b4

    #@b1
    mul-float/2addr v13, v14

    #@b2
    int-to-float v14, v11

    #@b3
    int-to-float v15, v12

    #@b4
    move-object/from16 v0, p1

    #@b6
    invoke-virtual {v0, v13, v14, v15}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@b9
    .line 212
    move-object/from16 v0, p0

    #@bb
    iget-object v7, v0, Landroid/widget/AnalogClock;->mMinuteHand:Landroid/graphics/drawable/Drawable;

    #@bd
    .line 213
    .local v7, minuteHand:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_db

    #@bf
    .line 214
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@c2
    move-result v10

    #@c3
    .line 215
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@c6
    move-result v5

    #@c7
    .line 216
    div-int/lit8 v13, v10, 0x2

    #@c9
    sub-int v13, v11, v13

    #@cb
    div-int/lit8 v14, v5, 0x2

    #@cd
    sub-int v14, v12, v14

    #@cf
    div-int/lit8 v15, v10, 0x2

    #@d1
    add-int/2addr v15, v11

    #@d2
    div-int/lit8 v16, v5, 0x2

    #@d4
    add-int v16, v16, v12

    #@d6
    move/from16 v0, v16

    #@d8
    invoke-virtual {v7, v13, v14, v15, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@db
    .line 218
    :cond_db
    move-object/from16 v0, p1

    #@dd
    invoke-virtual {v7, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@e0
    .line 219
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@e3
    .line 221
    if-eqz v9, :cond_e8

    #@e5
    .line 222
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@e8
    .line 224
    :cond_e8
    return-void
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 136
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v5

    #@5
    .line 137
    .local v5, widthMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v6

    #@9
    .line 138
    .local v6, widthSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@c
    move-result v1

    #@d
    .line 139
    .local v1, heightMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@10
    move-result v2

    #@11
    .line 141
    .local v2, heightSize:I
    const/high16 v0, 0x3f80

    #@13
    .line 142
    .local v0, hScale:F
    const/high16 v4, 0x3f80

    #@15
    .line 144
    .local v4, vScale:F
    if-eqz v5, :cond_21

    #@17
    iget v7, p0, Landroid/widget/AnalogClock;->mDialWidth:I

    #@19
    if-ge v6, v7, :cond_21

    #@1b
    .line 145
    int-to-float v7, v6

    #@1c
    iget v8, p0, Landroid/widget/AnalogClock;->mDialWidth:I

    #@1e
    int-to-float v8, v8

    #@1f
    div-float v0, v7, v8

    #@21
    .line 148
    :cond_21
    if-eqz v1, :cond_2d

    #@23
    iget v7, p0, Landroid/widget/AnalogClock;->mDialHeight:I

    #@25
    if-ge v2, v7, :cond_2d

    #@27
    .line 149
    int-to-float v7, v2

    #@28
    iget v8, p0, Landroid/widget/AnalogClock;->mDialHeight:I

    #@2a
    int-to-float v8, v8

    #@2b
    div-float v4, v7, v8

    #@2d
    .line 152
    :cond_2d
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    #@30
    move-result v3

    #@31
    .line 154
    .local v3, scale:F
    iget v7, p0, Landroid/widget/AnalogClock;->mDialWidth:I

    #@33
    int-to-float v7, v7

    #@34
    mul-float/2addr v7, v3

    #@35
    float-to-int v7, v7

    #@36
    invoke-static {v7, p1, v9}, Landroid/widget/AnalogClock;->resolveSizeAndState(III)I

    #@39
    move-result v7

    #@3a
    iget v8, p0, Landroid/widget/AnalogClock;->mDialHeight:I

    #@3c
    int-to-float v8, v8

    #@3d
    mul-float/2addr v8, v3

    #@3e
    float-to-int v8, v8

    #@3f
    invoke-static {v8, p2, v9}, Landroid/widget/AnalogClock;->resolveSizeAndState(III)I

    #@42
    move-result v8

    #@43
    invoke-virtual {p0, v7, v8}, Landroid/widget/AnalogClock;->setMeasuredDimension(II)V

    #@46
    .line 156
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 160
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    #@3
    .line 161
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/widget/AnalogClock;->mChanged:Z

    #@6
    .line 162
    return-void
.end method
