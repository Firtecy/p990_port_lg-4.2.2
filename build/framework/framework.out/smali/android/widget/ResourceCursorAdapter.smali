.class public abstract Landroid/widget/ResourceCursorAdapter;
.super Landroid/widget/CursorAdapter;
.source "ResourceCursorAdapter.java"


# instance fields
.field private mDropDownLayout:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .registers 5
    .parameter "context"
    .parameter "layout"
    .parameter "c"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    #@3
    .line 53
    iput p2, p0, Landroid/widget/ResourceCursorAdapter;->mDropDownLayout:I

    #@5
    iput p2, p0, Landroid/widget/ResourceCursorAdapter;->mLayout:I

    #@7
    .line 54
    const-string/jumbo v0, "layout_inflater"

    #@a
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/view/LayoutInflater;

    #@10
    iput-object v0, p0, Landroid/widget/ResourceCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@12
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .registers 6
    .parameter "context"
    .parameter "layout"
    .parameter "c"
    .parameter "flags"

    #@0
    .prologue
    .line 91
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    #@3
    .line 92
    iput p2, p0, Landroid/widget/ResourceCursorAdapter;->mDropDownLayout:I

    #@5
    iput p2, p0, Landroid/widget/ResourceCursorAdapter;->mLayout:I

    #@7
    .line 93
    const-string/jumbo v0, "layout_inflater"

    #@a
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/view/LayoutInflater;

    #@10
    iput-object v0, p0, Landroid/widget/ResourceCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@12
    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;Z)V
    .registers 6
    .parameter "context"
    .parameter "layout"
    .parameter "c"
    .parameter "autoRequery"

    #@0
    .prologue
    .line 74
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    #@3
    .line 75
    iput p2, p0, Landroid/widget/ResourceCursorAdapter;->mDropDownLayout:I

    #@5
    iput p2, p0, Landroid/widget/ResourceCursorAdapter;->mLayout:I

    #@7
    .line 76
    const-string/jumbo v0, "layout_inflater"

    #@a
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/view/LayoutInflater;

    #@10
    iput-object v0, p0, Landroid/widget/ResourceCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@12
    .line 77
    return-void
.end method


# virtual methods
.method public newDropDownView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/widget/ResourceCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@2
    iget v1, p0, Landroid/widget/ResourceCursorAdapter;->mDropDownLayout:I

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/widget/ResourceCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@2
    iget v1, p0, Landroid/widget/ResourceCursorAdapter;->mLayout:I

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public setDropDownViewResource(I)V
    .registers 2
    .parameter "dropDownLayout"

    #@0
    .prologue
    .line 127
    iput p1, p0, Landroid/widget/ResourceCursorAdapter;->mDropDownLayout:I

    #@2
    .line 128
    return-void
.end method

.method public setViewResource(I)V
    .registers 2
    .parameter "layout"

    #@0
    .prologue
    .line 118
    iput p1, p0, Landroid/widget/ResourceCursorAdapter;->mLayout:I

    #@2
    .line 119
    return-void
.end method
