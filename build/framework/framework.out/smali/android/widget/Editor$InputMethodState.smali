.class Landroid/widget/Editor$InputMethodState;
.super Ljava/lang/Object;
.source "Editor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Editor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InputMethodState"
.end annotation


# instance fields
.field mBatchEditNesting:I

.field mChangedDelta:I

.field mChangedEnd:I

.field mChangedStart:I

.field mContentChanged:Z

.field mCursorChanged:Z

.field mCursorRectInWindow:Landroid/graphics/Rect;

.field final mExtractedText:Landroid/view/inputmethod/ExtractedText;

.field mExtractedTextRequest:Landroid/view/inputmethod/ExtractedTextRequest;

.field mSelectionModeChanged:Z

.field mTmpOffset:[F

.field mTmpRectF:Landroid/graphics/RectF;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 4963
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 4964
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/widget/Editor$InputMethodState;->mCursorRectInWindow:Landroid/graphics/Rect;

    #@a
    .line 4965
    new-instance v0, Landroid/graphics/RectF;

    #@c
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@f
    iput-object v0, p0, Landroid/widget/Editor$InputMethodState;->mTmpRectF:Landroid/graphics/RectF;

    #@11
    .line 4966
    const/4 v0, 0x2

    #@12
    new-array v0, v0, [F

    #@14
    iput-object v0, p0, Landroid/widget/Editor$InputMethodState;->mTmpOffset:[F

    #@16
    .line 4968
    new-instance v0, Landroid/view/inputmethod/ExtractedText;

    #@18
    invoke-direct {v0}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/widget/Editor$InputMethodState;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@1d
    return-void
.end method
