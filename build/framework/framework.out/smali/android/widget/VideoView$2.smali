.class Landroid/widget/VideoView$2;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/VideoView;


# direct methods
.method constructor <init>(Landroid/widget/VideoView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 290
    iput-object p1, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .registers 10
    .parameter "mp"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 292
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@6
    invoke-static {v2, v6}, Landroid/widget/VideoView;->access$202(Landroid/widget/VideoView;I)I

    #@9
    .line 295
    invoke-virtual {p1, v3, v3}, Landroid/media/MediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    #@c
    move-result-object v0

    #@d
    .line 298
    .local v0, data:Landroid/media/Metadata;
    if-eqz v0, :cond_f5

    #@f
    .line 299
    iget-object v5, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@11
    invoke-virtual {v0, v4}, Landroid/media/Metadata;->has(I)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_1d

    #@17
    invoke-virtual {v0, v4}, Landroid/media/Metadata;->getBoolean(I)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_ec

    #@1d
    :cond_1d
    move v2, v4

    #@1e
    :goto_1e
    invoke-static {v5, v2}, Landroid/widget/VideoView;->access$302(Landroid/widget/VideoView;Z)Z

    #@21
    .line 301
    iget-object v5, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@23
    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_2f

    #@29
    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getBoolean(I)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_ef

    #@2f
    :cond_2f
    move v2, v4

    #@30
    :goto_30
    invoke-static {v5, v2}, Landroid/widget/VideoView;->access$402(Landroid/widget/VideoView;Z)Z

    #@33
    .line 303
    iget-object v5, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@35
    invoke-virtual {v0, v7}, Landroid/media/Metadata;->has(I)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_41

    #@3b
    invoke-virtual {v0, v7}, Landroid/media/Metadata;->getBoolean(I)Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_f2

    #@41
    :cond_41
    move v2, v4

    #@42
    :goto_42
    invoke-static {v5, v2}, Landroid/widget/VideoView;->access$502(Landroid/widget/VideoView;Z)Z

    #@45
    .line 309
    :goto_45
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@47
    invoke-static {v2}, Landroid/widget/VideoView;->access$600(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    #@4a
    move-result-object v2

    #@4b
    if-eqz v2, :cond_5c

    #@4d
    .line 310
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@4f
    invoke-static {v2}, Landroid/widget/VideoView;->access$600(Landroid/widget/VideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    #@52
    move-result-object v2

    #@53
    iget-object v5, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@55
    invoke-static {v5}, Landroid/widget/VideoView;->access$700(Landroid/widget/VideoView;)Landroid/media/MediaPlayer;

    #@58
    move-result-object v5

    #@59
    invoke-interface {v2, v5}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    #@5c
    .line 312
    :cond_5c
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@5e
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@61
    move-result-object v2

    #@62
    if-eqz v2, :cond_6d

    #@64
    .line 313
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@66
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2, v4}, Landroid/widget/MediaController;->setEnabled(Z)V

    #@6d
    .line 315
    :cond_6d
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@6f
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    #@72
    move-result v4

    #@73
    invoke-static {v2, v4}, Landroid/widget/VideoView;->access$002(Landroid/widget/VideoView;I)I

    #@76
    .line 316
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@78
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    #@7b
    move-result v4

    #@7c
    invoke-static {v2, v4}, Landroid/widget/VideoView;->access$102(Landroid/widget/VideoView;I)I

    #@7f
    .line 318
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@81
    invoke-static {v2}, Landroid/widget/VideoView;->access$900(Landroid/widget/VideoView;)I

    #@84
    move-result v1

    #@85
    .line 319
    .local v1, seekToPosition:I
    if-eqz v1, :cond_8c

    #@87
    .line 320
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@89
    invoke-virtual {v2, v1}, Landroid/widget/VideoView;->seekTo(I)V

    #@8c
    .line 322
    :cond_8c
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@8e
    invoke-static {v2}, Landroid/widget/VideoView;->access$000(Landroid/widget/VideoView;)I

    #@91
    move-result v2

    #@92
    if-eqz v2, :cond_12c

    #@94
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@96
    invoke-static {v2}, Landroid/widget/VideoView;->access$100(Landroid/widget/VideoView;)I

    #@99
    move-result v2

    #@9a
    if-eqz v2, :cond_12c

    #@9c
    .line 324
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@9e
    invoke-virtual {v2}, Landroid/widget/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    #@a1
    move-result-object v2

    #@a2
    iget-object v4, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@a4
    invoke-static {v4}, Landroid/widget/VideoView;->access$000(Landroid/widget/VideoView;)I

    #@a7
    move-result v4

    #@a8
    iget-object v5, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@aa
    invoke-static {v5}, Landroid/widget/VideoView;->access$100(Landroid/widget/VideoView;)I

    #@ad
    move-result v5

    #@ae
    invoke-interface {v2, v4, v5}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    #@b1
    .line 325
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@b3
    invoke-static {v2}, Landroid/widget/VideoView;->access$1000(Landroid/widget/VideoView;)I

    #@b6
    move-result v2

    #@b7
    iget-object v4, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@b9
    invoke-static {v4}, Landroid/widget/VideoView;->access$000(Landroid/widget/VideoView;)I

    #@bc
    move-result v4

    #@bd
    if-ne v2, v4, :cond_eb

    #@bf
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@c1
    invoke-static {v2}, Landroid/widget/VideoView;->access$1100(Landroid/widget/VideoView;)I

    #@c4
    move-result v2

    #@c5
    iget-object v4, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@c7
    invoke-static {v4}, Landroid/widget/VideoView;->access$100(Landroid/widget/VideoView;)I

    #@ca
    move-result v4

    #@cb
    if-ne v2, v4, :cond_eb

    #@cd
    .line 329
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@cf
    invoke-static {v2}, Landroid/widget/VideoView;->access$1200(Landroid/widget/VideoView;)I

    #@d2
    move-result v2

    #@d3
    if-ne v2, v7, :cond_108

    #@d5
    .line 330
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@d7
    invoke-virtual {v2}, Landroid/widget/VideoView;->start()V

    #@da
    .line 331
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@dc
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@df
    move-result-object v2

    #@e0
    if-eqz v2, :cond_eb

    #@e2
    .line 332
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@e4
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@e7
    move-result-object v2

    #@e8
    invoke-virtual {v2}, Landroid/widget/MediaController;->show()V

    #@eb
    .line 349
    :cond_eb
    :goto_eb
    return-void

    #@ec
    .end local v1           #seekToPosition:I
    :cond_ec
    move v2, v3

    #@ed
    .line 299
    goto/16 :goto_1e

    #@ef
    :cond_ef
    move v2, v3

    #@f0
    .line 301
    goto/16 :goto_30

    #@f2
    :cond_f2
    move v2, v3

    #@f3
    .line 303
    goto/16 :goto_42

    #@f5
    .line 306
    :cond_f5
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@f7
    iget-object v5, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@f9
    iget-object v6, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@fb
    invoke-static {v6, v4}, Landroid/widget/VideoView;->access$502(Landroid/widget/VideoView;Z)Z

    #@fe
    move-result v6

    #@ff
    invoke-static {v5, v6}, Landroid/widget/VideoView;->access$402(Landroid/widget/VideoView;Z)Z

    #@102
    move-result v5

    #@103
    invoke-static {v2, v5}, Landroid/widget/VideoView;->access$302(Landroid/widget/VideoView;Z)Z

    #@106
    goto/16 :goto_45

    #@108
    .line 334
    .restart local v1       #seekToPosition:I
    :cond_108
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@10a
    invoke-virtual {v2}, Landroid/widget/VideoView;->isPlaying()Z

    #@10d
    move-result v2

    #@10e
    if-nez v2, :cond_eb

    #@110
    if-nez v1, :cond_11a

    #@112
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@114
    invoke-virtual {v2}, Landroid/widget/VideoView;->getCurrentPosition()I

    #@117
    move-result v2

    #@118
    if-lez v2, :cond_eb

    #@11a
    .line 336
    :cond_11a
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@11c
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@11f
    move-result-object v2

    #@120
    if-eqz v2, :cond_eb

    #@122
    .line 338
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@124
    invoke-static {v2}, Landroid/widget/VideoView;->access$800(Landroid/widget/VideoView;)Landroid/widget/MediaController;

    #@127
    move-result-object v2

    #@128
    invoke-virtual {v2, v3}, Landroid/widget/MediaController;->show(I)V

    #@12b
    goto :goto_eb

    #@12c
    .line 345
    :cond_12c
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@12e
    invoke-static {v2}, Landroid/widget/VideoView;->access$1200(Landroid/widget/VideoView;)I

    #@131
    move-result v2

    #@132
    if-ne v2, v7, :cond_eb

    #@134
    .line 346
    iget-object v2, p0, Landroid/widget/VideoView$2;->this$0:Landroid/widget/VideoView;

    #@136
    invoke-virtual {v2}, Landroid/widget/VideoView;->start()V

    #@139
    goto :goto_eb
.end method
