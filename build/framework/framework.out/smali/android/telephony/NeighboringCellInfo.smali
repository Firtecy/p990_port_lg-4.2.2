.class public Landroid/telephony/NeighboringCellInfo;
.super Ljava/lang/Object;
.source "NeighboringCellInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final UNKNOWN_CID:I = -0x1

.field public static final UNKNOWN_RSSI:I = 0x63


# instance fields
.field private mCid:I

.field private mLac:I

.field private mNetworkType:I

.field private mPsc:I

.field private mRssi:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 289
    new-instance v0, Landroid/telephony/NeighboringCellInfo$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/NeighboringCellInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/NeighboringCellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 81
    const/16 v0, 0x63

    #@6
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@8
    .line 82
    iput v1, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@a
    .line 83
    iput v1, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@c
    .line 84
    iput v1, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@e
    .line 85
    const/4 v0, 0x0

    #@f
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@11
    .line 86
    return-void
.end method

.method public constructor <init>(II)V
    .registers 3
    .parameter "rssi"
    .parameter "cid"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 99
    iput p1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@5
    .line 100
    iput p2, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@7
    .line 101
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .registers 11
    .parameter "rssi"
    .parameter "location"
    .parameter "radioType"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v5, -0x1

    #@4
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 115
    iput p1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@9
    .line 116
    iput v6, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@b
    .line 117
    iput v5, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@d
    .line 118
    iput v5, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@f
    .line 119
    iput v5, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@11
    .line 123
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@14
    move-result v2

    #@15
    .line 124
    .local v2, l:I
    if-le v2, v3, :cond_18

    #@17
    .line 157
    :cond_17
    :goto_17
    return-void

    #@18
    .line 125
    :cond_18
    if-ge v2, v3, :cond_35

    #@1a
    .line 126
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    rsub-int/lit8 v3, v2, 0x8

    #@1d
    if-ge v1, v3, :cond_35

    #@1f
    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "0"

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object p2

    #@32
    .line 126
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_1b

    #@35
    .line 132
    .end local v1           #i:I
    :cond_35
    packed-switch p3, :pswitch_data_80

    #@38
    :pswitch_38
    goto :goto_17

    #@39
    .line 135
    :pswitch_39
    :try_start_39
    iput p3, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@3b
    .line 137
    const-string v3, "FFFFFFFF"

    #@3d
    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@40
    move-result v3

    #@41
    if-nez v3, :cond_17

    #@43
    .line 138
    const/4 v3, 0x4

    #@44
    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    const/16 v4, 0x10

    #@4a
    invoke-static {v3, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@51
    move-result v3

    #@52
    iput v3, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@54
    .line 139
    const/4 v3, 0x0

    #@55
    const/4 v4, 0x4

    #@56
    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    const/16 v4, 0x10

    #@5c
    invoke-static {v3, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@63
    move-result v3

    #@64
    iput v3, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I
    :try_end_66
    .catch Ljava/lang/NumberFormatException; {:try_start_39 .. :try_end_66} :catch_67

    #@66
    goto :goto_17

    #@67
    .line 150
    :catch_67
    move-exception v0

    #@68
    .line 152
    .local v0, e:Ljava/lang/NumberFormatException;
    iput v5, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@6a
    .line 153
    iput v5, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@6c
    .line 154
    iput v5, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@6e
    .line 155
    iput v6, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@70
    goto :goto_17

    #@71
    .line 146
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :pswitch_71
    :try_start_71
    iput p3, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@73
    .line 147
    const/16 v3, 0x10

    #@75
    invoke-static {p2, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    #@78
    move-result-object v3

    #@79
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@7c
    move-result v3

    #@7d
    iput v3, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I
    :try_end_7f
    .catch Ljava/lang/NumberFormatException; {:try_start_71 .. :try_end_7f} :catch_67

    #@7f
    goto :goto_17

    #@80
    .line 132
    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_39
        :pswitch_39
        :pswitch_71
        :pswitch_38
        :pswitch_38
        :pswitch_38
        :pswitch_38
        :pswitch_71
        :pswitch_71
        :pswitch_71
    .end packed-switch
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 162
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@9
    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@f
    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@15
    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@1b
    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@21
    .line 168
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 278
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCid()I
    .registers 2

    #@0
    .prologue
    .line 194
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@2
    return v0
.end method

.method public getLac()I
    .registers 2

    #@0
    .prologue
    .line 186
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@2
    return v0
.end method

.method public getNetworkType()I
    .registers 2

    #@0
    .prologue
    .line 227
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@2
    return v0
.end method

.method public getPsc()I
    .registers 2

    #@0
    .prologue
    .line 202
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@2
    return v0
.end method

.method public getRssi()I
    .registers 2

    #@0
    .prologue
    .line 178
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@2
    return v0
.end method

.method public setCid(I)V
    .registers 2
    .parameter "cid"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 241
    iput p1, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@2
    .line 242
    return-void
.end method

.method public setRssi(I)V
    .registers 2
    .parameter "rssi"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 256
    iput p1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@2
    .line 257
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x63

    #@2
    const/4 v2, -0x1

    #@3
    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    .line 263
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "["

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 264
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@f
    if-eq v1, v2, :cond_3b

    #@11
    .line 265
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@13
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "@"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@23
    if-ne v1, v3, :cond_34

    #@25
    const-string v1, "-"

    #@27
    :goto_27
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    .line 272
    :cond_2a
    :goto_2a
    const-string v1, "]"

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 274
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    return-object v1

    #@34
    .line 265
    :cond_34
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@36
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v1

    #@3a
    goto :goto_27

    #@3b
    .line 267
    :cond_3b
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@3d
    if-eq v1, v2, :cond_2a

    #@3f
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@41
    if-eq v1, v2, :cond_2a

    #@43
    .line 268
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@45
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    iget v2, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@4f
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    const-string v2, "@"

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@5f
    if-ne v1, v3, :cond_67

    #@61
    const-string v1, "-"

    #@63
    :goto_63
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    goto :goto_2a

    #@67
    :cond_67
    iget v1, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@69
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6c
    move-result-object v1

    #@6d
    goto :goto_63
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 282
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mRssi:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 283
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mLac:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 284
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mCid:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 285
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mPsc:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 286
    iget v0, p0, Landroid/telephony/NeighboringCellInfo;->mNetworkType:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 287
    return-void
.end method
