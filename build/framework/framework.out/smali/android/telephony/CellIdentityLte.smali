.class public final Landroid/telephony/CellIdentityLte;
.super Ljava/lang/Object;
.source "CellIdentityLte.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellIdentityLte;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "CellIdentityLte"


# instance fields
.field private final mCi:I

.field private final mMcc:I

.field private final mMnc:I

.field private final mPci:I

.field private final mTac:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 185
    new-instance v0, Landroid/telephony/CellIdentityLte$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellIdentityLte$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellIdentityLte;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 46
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@8
    .line 47
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@a
    .line 48
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@c
    .line 49
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@e
    .line 50
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@10
    .line 51
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 6
    .parameter "mcc"
    .parameter "mnc"
    .parameter "ci"
    .parameter "pci"
    .parameter "tac"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 64
    iput p1, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@5
    .line 65
    iput p2, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@7
    .line 66
    iput p3, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@9
    .line 67
    iput p4, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@b
    .line 68
    iput p5, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@d
    .line 69
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 174
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@9
    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@f
    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@15
    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@1b
    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@21
    .line 181
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellIdentityLte$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/telephony/CellIdentityLte;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/telephony/CellIdentityLte;)V
    .registers 3
    .parameter "cid"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    iget v0, p1, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@5
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@7
    .line 73
    iget v0, p1, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@9
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@b
    .line 74
    iget v0, p1, Landroid/telephony/CellIdentityLte;->mCi:I

    #@d
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@f
    .line 75
    iget v0, p1, Landroid/telephony/CellIdentityLte;->mPci:I

    #@11
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@13
    .line 76
    iget v0, p1, Landroid/telephony/CellIdentityLte;->mTac:I

    #@15
    iput v0, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@17
    .line 77
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 202
    const-string v0, "CellIdentityLte"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 203
    return-void
.end method


# virtual methods
.method copy()Landroid/telephony/CellIdentityLte;
    .registers 2

    #@0
    .prologue
    .line 80
    new-instance v0, Landroid/telephony/CellIdentityLte;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellIdentityLte;-><init>(Landroid/telephony/CellIdentityLte;)V

    #@5
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 159
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "other"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 127
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@4
    move-result v4

    #@5
    if-eqz v4, :cond_2a

    #@7
    .line 129
    :try_start_7
    move-object v0, p1

    #@8
    check-cast v0, Landroid/telephony/CellIdentityLte;

    #@a
    move-object v2, v0

    #@b
    .line 130
    .local v2, o:Landroid/telephony/CellIdentityLte;
    iget v4, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@d
    iget v5, v2, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@f
    if-ne v4, v5, :cond_2a

    #@11
    iget v4, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@13
    iget v5, v2, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@15
    if-ne v4, v5, :cond_2a

    #@17
    iget v4, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@19
    iget v5, v2, Landroid/telephony/CellIdentityLte;->mCi:I

    #@1b
    if-ne v4, v5, :cond_2a

    #@1d
    iget v4, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@1f
    iget v5, v2, Landroid/telephony/CellIdentityLte;->mCi:I

    #@21
    if-ne v4, v5, :cond_2a

    #@23
    iget v4, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@25
    iget v5, v2, Landroid/telephony/CellIdentityLte;->mTac:I
    :try_end_27
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_27} :catch_2b

    #@27
    if-ne v4, v5, :cond_2a

    #@29
    const/4 v3, 0x1

    #@2a
    .line 139
    .end local v2           #o:Landroid/telephony/CellIdentityLte;
    :cond_2a
    :goto_2a
    return v3

    #@2b
    .line 135
    :catch_2b
    move-exception v1

    #@2c
    .line 136
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_2a
.end method

.method public getCi()I
    .registers 2

    #@0
    .prologue
    .line 101
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@2
    return v0
.end method

.method public getMcc()I
    .registers 2

    #@0
    .prologue
    .line 87
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@2
    return v0
.end method

.method public getMnc()I
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@2
    return v0
.end method

.method public getPci()I
    .registers 2

    #@0
    .prologue
    .line 108
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@2
    return v0
.end method

.method public getTac()I
    .registers 2

    #@0
    .prologue
    .line 115
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 120
    const/16 v0, 0x1f

    #@2
    .line 121
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@b
    mul-int/2addr v2, v0

    #@c
    add-int/2addr v1, v2

    #@d
    iget v2, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@f
    mul-int/2addr v2, v0

    #@10
    add-int/2addr v1, v2

    #@11
    iget v2, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@13
    mul-int/2addr v2, v0

    #@14
    add-int/2addr v1, v2

    #@15
    return v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "LteCellIdentitiy:"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 146
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 147
    const-string v1, " mMcc="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    iget v1, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    .line 148
    const-string v1, " mMnc="

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    iget v1, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    .line 149
    const-string v1, " mCi="

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    iget v1, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    .line 150
    const-string v1, " mPci="

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    iget v1, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    .line 151
    const-string v1, " mTac="

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    iget v1, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    .line 153
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 166
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mMcc:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 167
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mMnc:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 168
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mCi:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 169
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mPci:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 170
    iget v0, p0, Landroid/telephony/CellIdentityLte;->mTac:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 171
    return-void
.end method
