.class public Landroid/telephony/cdma/CdmaCellLocation;
.super Landroid/telephony/CellLocation;
.source "CdmaCellLocation.java"


# static fields
.field public static final INVALID_LAT_LONG:I = 0x7fffffff


# instance fields
.field private mBaseStationId:I

.field private mBaseStationLatitude:I

.field private mBaseStationLongitude:I

.field private mNetworkId:I

.field private mSystemId:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const v1, 0x7fffffff

    #@3
    const/4 v0, -0x1

    #@4
    .line 57
    invoke-direct {p0}, Landroid/telephony/CellLocation;-><init>()V

    #@7
    .line 26
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@9
    .line 39
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@b
    .line 47
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@d
    .line 49
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@f
    .line 50
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@11
    .line 58
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@13
    .line 59
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@15
    .line 60
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@17
    .line 61
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@19
    .line 62
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@1b
    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .registers 4
    .parameter "bundle"

    #@0
    .prologue
    const v1, 0x7fffffff

    #@3
    const/4 v0, -0x1

    #@4
    .line 68
    invoke-direct {p0}, Landroid/telephony/CellLocation;-><init>()V

    #@7
    .line 26
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@9
    .line 39
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@b
    .line 47
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@d
    .line 49
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@f
    .line 50
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@11
    .line 69
    const-string v0, "baseStationId"

    #@13
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@15
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@1b
    .line 70
    const-string v0, "baseStationLatitude"

    #@1d
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@1f
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@22
    move-result v0

    #@23
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@25
    .line 71
    const-string v0, "baseStationLongitude"

    #@27
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@29
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@2c
    move-result v0

    #@2d
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@2f
    .line 72
    const-string/jumbo v0, "systemId"

    #@32
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@34
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@37
    move-result v0

    #@38
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@3a
    .line 73
    const-string/jumbo v0, "networkId"

    #@3d
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@3f
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@42
    move-result v0

    #@43
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@45
    .line 74
    return-void
.end method

.method public static convertQuartSecToDecDegrees(I)D
    .registers 5
    .parameter "quartSec"

    #@0
    .prologue
    .line 240
    int-to-double v0, p0

    #@1
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_11

    #@7
    const v0, -0x278d00

    #@a
    if-lt p0, v0, :cond_11

    #@c
    const v0, 0x278d00

    #@f
    if-le p0, v0, :cond_2a

    #@11
    .line 242
    :cond_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "Invalid coordiante value:"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 244
    :cond_2a
    int-to-double v0, p0

    #@2b
    const-wide v2, 0x40cc200000000000L

    #@30
    div-double/2addr v0, v2

    #@31
    return-wide v0
.end method

.method private static equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 203
    if-nez p0, :cond_8

    #@2
    if-nez p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 169
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 174
    .local v2, s:Landroid/telephony/cdma/CdmaCellLocation;
    if-nez p1, :cond_a

    #@7
    .line 178
    .end local v2           #s:Landroid/telephony/cdma/CdmaCellLocation;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 170
    :catch_8
    move-exception v1

    #@9
    .line 171
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 178
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/cdma/CdmaCellLocation;
    :cond_a
    iget v4, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v4

    #@10
    iget v5, v2, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@12
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v5

    #@16
    invoke-static {v4, v5}, Landroid/telephony/cdma/CdmaCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_7

    #@1c
    iget v4, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@1e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    iget v5, v2, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@24
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v5

    #@28
    invoke-static {v4, v5}, Landroid/telephony/cdma/CdmaCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_7

    #@2e
    iget v4, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@30
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v4

    #@34
    iget v5, v2, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@36
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v5

    #@3a
    invoke-static {v4, v5}, Landroid/telephony/cdma/CdmaCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_7

    #@40
    iget v4, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@42
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v4

    #@46
    iget v5, v2, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@48
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v5

    #@4c
    invoke-static {v4, v5}, Landroid/telephony/cdma/CdmaCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_7

    #@52
    iget v4, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@54
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57
    move-result-object v4

    #@58
    iget v5, v2, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@5a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5d
    move-result-object v5

    #@5e
    invoke-static {v4, v5}, Landroid/telephony/cdma/CdmaCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@61
    move-result v4

    #@62
    if-eqz v4, :cond_7

    #@64
    const/4 v3, 0x1

    #@65
    goto :goto_7
.end method

.method public fillInNotifierBundle(Landroid/os/Bundle;)V
    .registers 4
    .parameter "bundleToFill"

    #@0
    .prologue
    .line 212
    const-string v0, "baseStationId"

    #@2
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@4
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@7
    .line 213
    const-string v0, "baseStationLatitude"

    #@9
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@b
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@e
    .line 214
    const-string v0, "baseStationLongitude"

    #@10
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@12
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@15
    .line 215
    const-string/jumbo v0, "systemId"

    #@18
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@1a
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1d
    .line 216
    const-string/jumbo v0, "networkId"

    #@20
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@22
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@25
    .line 217
    return-void
.end method

.method public getBaseStationId()I
    .registers 2

    #@0
    .prologue
    .line 80
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@2
    return v0
.end method

.method public getBaseStationLatitude()I
    .registers 2

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@2
    return v0
.end method

.method public getBaseStationLongitude()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@2
    return v0
.end method

.method public getNetworkId()I
    .registers 2

    #@0
    .prologue
    .line 120
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@2
    return v0
.end method

.method public getSystemId()I
    .registers 2

    #@0
    .prologue
    .line 113
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 160
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@2
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@4
    xor-int/2addr v0, v1

    #@5
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@7
    xor-int/2addr v0, v1

    #@8
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@a
    xor-int/2addr v0, v1

    #@b
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@d
    xor-int/2addr v0, v1

    #@e
    return v0
.end method

.method public isEmpty()Z
    .registers 4

    #@0
    .prologue
    const v2, 0x7fffffff

    #@3
    const/4 v1, -0x1

    #@4
    .line 223
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@6
    if-ne v0, v1, :cond_1a

    #@8
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@a
    if-ne v0, v2, :cond_1a

    #@c
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@e
    if-ne v0, v2, :cond_1a

    #@10
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@12
    if-ne v0, v1, :cond_1a

    #@14
    iget v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@16
    if-ne v0, v1, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public setCellLocationData(III)V
    .registers 4
    .parameter "baseStationId"
    .parameter "baseStationLatitude"
    .parameter "baseStationLongitude"

    #@0
    .prologue
    .line 140
    iput p1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@2
    .line 141
    iput p2, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@4
    .line 142
    iput p3, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@6
    .line 143
    return-void
.end method

.method public setCellLocationData(IIIII)V
    .registers 6
    .parameter "baseStationId"
    .parameter "baseStationLatitude"
    .parameter "baseStationLongitude"
    .parameter "systemId"
    .parameter "networkId"

    #@0
    .prologue
    .line 151
    iput p1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@2
    .line 152
    iput p2, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@4
    .line 153
    iput p3, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@6
    .line 154
    iput p4, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@8
    .line 155
    iput p5, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@a
    .line 156
    return-void
.end method

.method public setStateInvalid()V
    .registers 3

    #@0
    .prologue
    const v1, 0x7fffffff

    #@3
    const/4 v0, -0x1

    #@4
    .line 127
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@6
    .line 128
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@8
    .line 129
    iput v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@a
    .line 130
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@c
    .line 131
    iput v0, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@e
    .line 132
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationId:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ","

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLatitude:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ","

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mBaseStationLongitude:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ","

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mSystemId:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ","

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/telephony/cdma/CdmaCellLocation;->mNetworkId:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, "]"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    return-object v0
.end method
