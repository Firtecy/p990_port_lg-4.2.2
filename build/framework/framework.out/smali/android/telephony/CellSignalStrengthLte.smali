.class public final Landroid/telephony/CellSignalStrengthLte;
.super Landroid/telephony/CellSignalStrength;
.source "CellSignalStrengthLte.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellSignalStrengthLte;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "CellSignalStrengthLte"


# instance fields
.field private mCqi:I

.field private mRsrp:I

.field private mRsrq:I

.field private mRssnr:I

.field private mSignalStrength:I

.field private mTimingAdvance:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 279
    new-instance v0, Landroid/telephony/CellSignalStrengthLte$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellSignalStrengthLte$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellSignalStrengthLte;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 43
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 44
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthLte;->setDefaultValues()V

    #@6
    .line 45
    return-void
.end method

.method public constructor <init>(IIIIII)V
    .registers 7
    .parameter "signalStrength"
    .parameter "rsrp"
    .parameter "rsrq"
    .parameter "rssnr"
    .parameter "cqi"
    .parameter "timingAdvance"

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 54
    invoke-virtual/range {p0 .. p6}, Landroid/telephony/CellSignalStrengthLte;->initialize(IIIIII)V

    #@6
    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 261
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@9
    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@f
    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@15
    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@1b
    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@21
    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@27
    .line 269
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellSignalStrengthLte$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/telephony/CellSignalStrengthLte;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/telephony/CellSignalStrengthLte;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 64
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 65
    invoke-virtual {p0, p1}, Landroid/telephony/CellSignalStrengthLte;->copyFrom(Landroid/telephony/CellSignalStrengthLte;)V

    #@6
    .line 66
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 296
    const-string v0, "CellSignalStrengthLte"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 297
    return-void
.end method


# virtual methods
.method public bridge synthetic copy()Landroid/telephony/CellSignalStrength;
    .registers 2

    #@0
    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthLte;->copy()Landroid/telephony/CellSignalStrengthLte;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public copy()Landroid/telephony/CellSignalStrengthLte;
    .registers 2

    #@0
    .prologue
    .line 122
    new-instance v0, Landroid/telephony/CellSignalStrengthLte;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellSignalStrengthLte;-><init>(Landroid/telephony/CellSignalStrengthLte;)V

    #@5
    return-object v0
.end method

.method protected copyFrom(Landroid/telephony/CellSignalStrengthLte;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 109
    iget v0, p1, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@2
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@4
    .line 110
    iget v0, p1, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@6
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@8
    .line 111
    iget v0, p1, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@a
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@c
    .line 112
    iget v0, p1, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@e
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@10
    .line 113
    iget v0, p1, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@12
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@14
    .line 114
    iget v0, p1, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@16
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@18
    .line 115
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 274
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 214
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/CellSignalStrengthLte;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 219
    .local v2, s:Landroid/telephony/CellSignalStrengthLte;
    if-nez p1, :cond_a

    #@7
    .line 223
    .end local v2           #s:Landroid/telephony/CellSignalStrengthLte;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 215
    :catch_8
    move-exception v1

    #@9
    .line 216
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 223
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/CellSignalStrengthLte;
    :cond_a
    iget v4, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@c
    iget v5, v2, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@e
    if-ne v4, v5, :cond_7

    #@10
    iget v4, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@12
    iget v5, v2, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@14
    if-ne v4, v5, :cond_7

    #@16
    iget v4, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@18
    iget v5, v2, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@1a
    if-ne v4, v5, :cond_7

    #@1c
    iget v4, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@1e
    iget v5, v2, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@20
    if-ne v4, v5, :cond_7

    #@22
    iget v4, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@24
    iget v5, v2, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@26
    if-ne v4, v5, :cond_7

    #@28
    iget v4, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@2a
    iget v5, v2, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@2c
    if-ne v4, v5, :cond_7

    #@2e
    const/4 v3, 0x1

    #@2f
    goto :goto_7
.end method

.method public getAsuLevel()I
    .registers 4

    #@0
    .prologue
    .line 184
    const/16 v0, 0x63

    #@2
    .line 185
    .local v0, lteAsuLevel:I
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    #@5
    move-result v1

    #@6
    .line 186
    .local v1, lteDbm:I
    const/16 v2, -0x8c

    #@8
    if-gt v1, v2, :cond_c

    #@a
    const/4 v0, 0x0

    #@b
    .line 190
    :goto_b
    return v0

    #@c
    .line 187
    :cond_c
    const/16 v2, -0x2b

    #@e
    if-lt v1, v2, :cond_13

    #@10
    const/16 v0, 0x61

    #@12
    goto :goto_b

    #@13
    .line 188
    :cond_13
    add-int/lit16 v0, v1, 0x8c

    #@15
    goto :goto_b
.end method

.method public getDbm()I
    .registers 2

    #@0
    .prologue
    .line 175
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@2
    return v0
.end method

.method public getLevel()I
    .registers 7

    #@0
    .prologue
    const v5, 0x7fffffff

    #@3
    .line 141
    const/4 v1, 0x0

    #@4
    .line 142
    .local v1, levelRsrp:I
    const/4 v2, 0x0

    #@5
    .line 144
    .local v2, levelRssnr:I
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@7
    if-ne v3, v5, :cond_15

    #@9
    const/4 v1, 0x0

    #@a
    .line 151
    :goto_a
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@c
    if-ne v3, v5, :cond_2f

    #@e
    const/4 v2, 0x0

    #@f
    .line 158
    :goto_f
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@11
    if-ne v3, v5, :cond_49

    #@13
    .line 159
    move v0, v2

    #@14
    .line 167
    .local v0, level:I
    :goto_14
    return v0

    #@15
    .line 145
    .end local v0           #level:I
    :cond_15
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@17
    const/16 v4, -0x5f

    #@19
    if-lt v3, v4, :cond_1d

    #@1b
    const/4 v1, 0x4

    #@1c
    goto :goto_a

    #@1d
    .line 146
    :cond_1d
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@1f
    const/16 v4, -0x69

    #@21
    if-lt v3, v4, :cond_25

    #@23
    const/4 v1, 0x3

    #@24
    goto :goto_a

    #@25
    .line 147
    :cond_25
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@27
    const/16 v4, -0x73

    #@29
    if-lt v3, v4, :cond_2d

    #@2b
    const/4 v1, 0x2

    #@2c
    goto :goto_a

    #@2d
    .line 148
    :cond_2d
    const/4 v1, 0x1

    #@2e
    goto :goto_a

    #@2f
    .line 152
    :cond_2f
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@31
    const/16 v4, 0x2d

    #@33
    if-lt v3, v4, :cond_37

    #@35
    const/4 v2, 0x4

    #@36
    goto :goto_f

    #@37
    .line 153
    :cond_37
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@39
    const/16 v4, 0xa

    #@3b
    if-lt v3, v4, :cond_3f

    #@3d
    const/4 v2, 0x3

    #@3e
    goto :goto_f

    #@3f
    .line 154
    :cond_3f
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@41
    const/16 v4, -0x1e

    #@43
    if-lt v3, v4, :cond_47

    #@45
    const/4 v2, 0x2

    #@46
    goto :goto_f

    #@47
    .line 155
    :cond_47
    const/4 v2, 0x1

    #@48
    goto :goto_f

    #@49
    .line 160
    :cond_49
    iget v3, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@4b
    if-ne v3, v5, :cond_4f

    #@4d
    .line 161
    move v0, v1

    #@4e
    .restart local v0       #level:I
    goto :goto_14

    #@4f
    .line 163
    .end local v0           #level:I
    :cond_4f
    if-ge v2, v1, :cond_53

    #@51
    move v0, v2

    #@52
    .restart local v0       #level:I
    :goto_52
    goto :goto_14

    #@53
    .end local v0           #level:I
    :cond_53
    move v0, v1

    #@54
    goto :goto_52
.end method

.method public getTimingAdvance()I
    .registers 2

    #@0
    .prologue
    .line 198
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 203
    const/16 v0, 0x1f

    #@2
    .line 204
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@b
    mul-int/2addr v2, v0

    #@c
    add-int/2addr v1, v2

    #@d
    iget v2, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@f
    mul-int/2addr v2, v0

    #@10
    add-int/2addr v1, v2

    #@11
    iget v2, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@13
    mul-int/2addr v2, v0

    #@14
    add-int/2addr v1, v2

    #@15
    iget v2, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@17
    mul-int/2addr v2, v0

    #@18
    add-int/2addr v1, v2

    #@19
    return v1
.end method

.method public initialize(IIIIII)V
    .registers 7
    .parameter "lteSignalStrength"
    .parameter "rsrp"
    .parameter "rsrq"
    .parameter "rssnr"
    .parameter "cqi"
    .parameter "timingAdvance"

    #@0
    .prologue
    .line 81
    iput p1, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@2
    .line 82
    iput p2, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@4
    .line 83
    iput p3, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@6
    .line 84
    iput p4, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@8
    .line 85
    iput p5, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@a
    .line 86
    iput p6, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@c
    .line 87
    return-void
.end method

.method public initialize(Landroid/telephony/SignalStrength;I)V
    .registers 4
    .parameter "ss"
    .parameter "timingAdvance"

    #@0
    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLteSignalStrenght()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@6
    .line 98
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLteRsrp()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@c
    .line 99
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLteRsrq()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@12
    .line 100
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLteRssnr()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@18
    .line 101
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLteCqi()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@1e
    .line 102
    iput p2, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@20
    .line 103
    return-void
.end method

.method public setDefaultValues()V
    .registers 2

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    .line 128
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@5
    .line 129
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@7
    .line 130
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@9
    .line 131
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@b
    .line 132
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@d
    .line 133
    iput v0, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@f
    .line 134
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "CellSignalStrengthLte: ss="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " rsrp="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " rsrq="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " rssnr="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " cqi="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, " ta="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 249
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mSignalStrength:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 250
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrp:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 251
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRsrq:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 252
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mRssnr:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 253
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mCqi:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 254
    iget v0, p0, Landroid/telephony/CellSignalStrengthLte;->mTimingAdvance:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 255
    return-void
.end method
