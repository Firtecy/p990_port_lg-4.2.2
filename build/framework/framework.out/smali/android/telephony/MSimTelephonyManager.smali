.class public Landroid/telephony/MSimTelephonyManager;
.super Ljava/lang/Object;
.source "MSimTelephonyManager.java"


# static fields
.field protected static multiSimConfig:Ljava/lang/String;

.field private static sContext:Landroid/content/Context;

.field private static sInstance:Landroid/telephony/MSimTelephonyManager;

.field protected static sRegistryMsim:Lcom/android/internal/telephony/ITelephonyRegistryMSim;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    const-string/jumbo v0, "persist.multisim.config"

    #@3
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Landroid/telephony/MSimTelephonyManager;->multiSimConfig:Ljava/lang/String;

    #@9
    .line 88
    new-instance v0, Landroid/telephony/MSimTelephonyManager;

    #@b
    invoke-direct {v0}, Landroid/telephony/MSimTelephonyManager;-><init>()V

    #@e
    sput-object v0, Landroid/telephony/MSimTelephonyManager;->sInstance:Landroid/telephony/MSimTelephonyManager;

    #@10
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 71
    sget-object v1, Landroid/telephony/MSimTelephonyManager;->sContext:Landroid/content/Context;

    #@5
    if-nez v1, :cond_1c

    #@7
    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    .line 73
    .local v0, appContext:Landroid/content/Context;
    if-eqz v0, :cond_1d

    #@d
    .line 74
    sput-object v0, Landroid/telephony/MSimTelephonyManager;->sContext:Landroid/content/Context;

    #@f
    .line 79
    :goto_f
    const-string/jumbo v1, "telephony.msim.registry"

    #@12
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@15
    move-result-object v1

    #@16
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephonyRegistryMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistryMSim;

    #@19
    move-result-object v1

    #@1a
    sput-object v1, Landroid/telephony/MSimTelephonyManager;->sRegistryMsim:Lcom/android/internal/telephony/ITelephonyRegistryMSim;

    #@1c
    .line 82
    .end local v0           #appContext:Landroid/content/Context;
    :cond_1c
    return-void

    #@1d
    .line 76
    .restart local v0       #appContext:Landroid/content/Context;
    :cond_1d
    sput-object p1, Landroid/telephony/MSimTelephonyManager;->sContext:Landroid/content/Context;

    #@1f
    goto :goto_f
.end method

.method public static from(Landroid/content/Context;)Landroid/telephony/MSimTelephonyManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 98
    const-string/jumbo v0, "phone_msim"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/telephony/MSimTelephonyManager;

    #@9
    return-object v0
.end method

.method public static getDefault()Landroid/telephony/MSimTelephonyManager;
    .registers 1

    #@0
    .prologue
    .line 93
    sget-object v0, Landroid/telephony/MSimTelephonyManager;->sInstance:Landroid/telephony/MSimTelephonyManager;

    #@2
    return-object v0
.end method

.method private getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;
    .registers 2

    #@0
    .prologue
    .line 630
    const-string/jumbo v0, "phone_msim"

    #@3
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static getIntAtIndex(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 874
    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 875
    .local v0, v:Ljava/lang/String;
    if-eqz v0, :cond_1d

    #@6
    .line 876
    const-string v2, ","

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 877
    .local v1, valArray:[Ljava/lang/String;
    if-ltz p2, :cond_1d

    #@e
    array-length v2, v1

    #@f
    if-ge p2, v2, :cond_1d

    #@11
    aget-object v2, v1, p2

    #@13
    if-eqz v2, :cond_1d

    #@15
    .line 879
    :try_start_15
    aget-object v2, v1, p2

    #@17
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1a
    .catch Ljava/lang/NumberFormatException; {:try_start_15 .. :try_end_1a} :catch_1c

    #@1a
    move-result v2

    #@1b
    return v2

    #@1c
    .line 880
    :catch_1c
    move-exception v2

    #@1d
    .line 885
    .end local v1           #valArray:[Ljava/lang/String;
    :cond_1d
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@1f
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@22
    throw v2
.end method

.method private getPhoneTypeFromNetworkType(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    .line 202
    const-string/jumbo v1, "ro.telephony.default_network"

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v1, p1, v2}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 203
    .local v0, mode:Ljava/lang/String;
    if-eqz v0, :cond_13

    #@a
    .line 204
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@d
    move-result v1

    #@e
    invoke-static {v1}, Landroid/telephony/TelephonyManager;->getPhoneType(I)I

    #@11
    move-result v1

    #@12
    .line 206
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method private getPhoneTypeFromProperty(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    .line 188
    const-string v1, "gsm.current.phone-type"

    #@2
    const/4 v2, 0x0

    #@3
    invoke-static {v1, p1, v2}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 191
    .local v0, type:Ljava/lang/String;
    if-eqz v0, :cond_e

    #@9
    .line 192
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c
    move-result v1

    #@d
    .line 194
    :goto_d
    return v1

    #@e
    :cond_e
    invoke-direct {p0, p1}, Landroid/telephony/MSimTelephonyManager;->getPhoneTypeFromNetworkType(I)I

    #@11
    move-result v1

    #@12
    goto :goto_d
.end method

.method public static getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "property"
    .parameter "index"
    .parameter "defaultVal"

    #@0
    .prologue
    .line 786
    const/4 v1, 0x0

    #@1
    .line 787
    .local v1, propVal:Ljava/lang/String;
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 789
    .local v0, prop:Ljava/lang/String;
    if-eqz v0, :cond_1e

    #@7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@a
    move-result v3

    #@b
    if-lez v3, :cond_1e

    #@d
    .line 790
    const-string v3, ","

    #@f
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 791
    .local v2, values:[Ljava/lang/String;
    if-ltz p1, :cond_1e

    #@15
    array-length v3, v2

    #@16
    if-ge p1, v3, :cond_1e

    #@18
    aget-object v3, v2, p1

    #@1a
    if-eqz v3, :cond_1e

    #@1c
    .line 792
    aget-object v1, v2, p1

    #@1e
    .line 795
    .end local v2           #values:[Ljava/lang/String;
    :cond_1e
    if-nez v1, :cond_21

    #@20
    .end local p2
    :goto_20
    return-object p2

    #@21
    .restart local p2
    :cond_21
    move-object p2, v1

    #@22
    goto :goto_20
.end method

.method public static putIntAtIndex(Landroid/content/ContentResolver;Ljava/lang/String;II)Z
    .registers 11
    .parameter "cr"
    .parameter "name"
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    .line 905
    const-string v0, ""

    #@2
    .line 906
    .local v0, data:Ljava/lang/String;
    const/4 v4, 0x0

    #@3
    .line 907
    .local v4, valArray:[Ljava/lang/String;
    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    .line 909
    .local v3, v:Ljava/lang/String;
    if-eqz v3, :cond_f

    #@9
    .line 910
    const-string v5, ","

    #@b
    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    .line 914
    :cond_f
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    if-ge v1, p2, :cond_35

    #@12
    .line 915
    const-string v2, ""

    #@14
    .line 916
    .local v2, str:Ljava/lang/String;
    if-eqz v4, :cond_1b

    #@16
    array-length v5, v4

    #@17
    if-ge v1, v5, :cond_1b

    #@19
    .line 917
    aget-object v2, v4, v1

    #@1b
    .line 919
    :cond_1b
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    const-string v6, ","

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    .line 914
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_10

    #@35
    .line 922
    .end local v2           #str:Ljava/lang/String;
    :cond_35
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    .line 925
    if-eqz v4, :cond_69

    #@48
    .line 926
    add-int/lit8 v1, p2, 0x1

    #@4a
    :goto_4a
    array-length v5, v4

    #@4b
    if-ge v1, v5, :cond_69

    #@4d
    .line 927
    new-instance v5, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    const-string v6, ","

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    aget-object v6, v4, v1

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    .line 926
    add-int/lit8 v1, v1, 0x1

    #@68
    goto :goto_4a

    #@69
    .line 930
    :cond_69
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@6c
    move-result v5

    #@6d
    return v5
.end method

.method public static setTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)V
    .registers 10
    .parameter "property"
    .parameter "index"
    .parameter "value"

    #@0
    .prologue
    .line 753
    const-string v3, ""

    #@2
    .line 754
    .local v3, propVal:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    .line 755
    .local v1, p:[Ljava/lang/String;
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 757
    .local v2, prop:Ljava/lang/String;
    if-eqz v2, :cond_f

    #@9
    .line 758
    const-string v5, ","

    #@b
    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 761
    :cond_f
    if-gez p1, :cond_12

    #@11
    .line 778
    :goto_11
    return-void

    #@12
    .line 763
    :cond_12
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, p1, :cond_38

    #@15
    .line 764
    const-string v4, ""

    #@17
    .line 765
    .local v4, str:Ljava/lang/String;
    if-eqz v1, :cond_1e

    #@19
    array-length v5, v1

    #@1a
    if-ge v0, v5, :cond_1e

    #@1c
    .line 766
    aget-object v4, v1, v0

    #@1e
    .line 768
    :cond_1e
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    const-string v6, ","

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    .line 763
    add-int/lit8 v0, v0, 0x1

    #@37
    goto :goto_13

    #@38
    .line 771
    .end local v4           #str:Ljava/lang/String;
    :cond_38
    new-instance v5, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    .line 772
    if-eqz v1, :cond_6c

    #@4b
    .line 773
    add-int/lit8 v0, p1, 0x1

    #@4d
    :goto_4d
    array-length v5, v1

    #@4e
    if-ge v0, v5, :cond_6c

    #@50
    .line 774
    new-instance v5, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    const-string v6, ","

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    aget-object v6, v1, v0

    #@61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v5

    #@65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    .line 773
    add-int/lit8 v0, v0, 0x1

    #@6b
    goto :goto_4d

    #@6c
    .line 777
    :cond_6c
    invoke-static {p0, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    goto :goto_11
.end method


# virtual methods
.method public getAllCellInfo()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 739
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getAllCellInfo()Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 743
    :goto_9
    return-object v1

    #@a
    .line 740
    :catch_a
    move-exception v0

    #@b
    .line 741
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 742
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 743
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCallState(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 577
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getCallState(I)I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 583
    :goto_9
    return v1

    #@a
    .line 578
    :catch_a
    move-exception v0

    #@b
    .line 580
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 581
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 583
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriIconIndex(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 683
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getCdmaEriIconIndex(I)I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 688
    :goto_9
    return v1

    #@a
    .line 684
    :catch_a
    move-exception v0

    #@b
    .line 686
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 687
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 688
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriIconMode(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 701
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getCdmaEriIconMode(I)I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 706
    :goto_9
    return v1

    #@a
    .line 702
    :catch_a
    move-exception v0

    #@b
    .line 704
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 705
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 706
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriText(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 717
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getCdmaEriText(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 722
    :goto_9
    return-object v1

    #@a
    .line 718
    :catch_a
    move-exception v0

    #@b
    .line 720
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 721
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 722
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCompleteVoiceMailNumber(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 512
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getCompleteVoiceMailNumber(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 517
    :goto_9
    return-object v1

    #@a
    .line 513
    :catch_a
    move-exception v0

    #@b
    .line 514
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 515
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 517
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCurrentPhoneType(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    .line 169
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@3
    move-result-object v1

    #@4
    .line 170
    .local v1, telephony:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    if-eqz v1, :cond_b

    #@6
    .line 171
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getActivePhoneType(I)I

    #@9
    move-result v2

    #@a
    .line 183
    .end local v1           #telephony:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :goto_a
    return v2

    #@b
    .line 174
    .restart local v1       #telephony:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :cond_b
    invoke-direct {p0, p1}, Landroid/telephony/MSimTelephonyManager;->getPhoneTypeFromProperty(I)I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_10
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_e} :catch_16

    #@e
    move-result v2

    #@f
    goto :goto_a

    #@10
    .line 176
    .end local v1           #telephony:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :catch_10
    move-exception v0

    #@11
    .line 179
    .local v0, ex:Landroid/os/RemoteException;
    invoke-direct {p0, p1}, Landroid/telephony/MSimTelephonyManager;->getPhoneTypeFromProperty(I)I

    #@14
    move-result v2

    #@15
    goto :goto_a

    #@16
    .line 180
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_16
    move-exception v0

    #@17
    .line 183
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-direct {p0, p1}, Landroid/telephony/MSimTelephonyManager;->getPhoneTypeFromProperty(I)I

    #@1a
    move-result v2

    #@1b
    goto :goto_a
.end method

.method public getDataActivity()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 599
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getDataActivity()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 605
    :goto_9
    return v1

    #@a
    .line 600
    :catch_a
    move-exception v0

    #@b
    .line 602
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 603
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 605
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDataState()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 620
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getDataState()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 625
    :goto_9
    return v1

    #@a
    .line 621
    :catch_a
    move-exception v0

    #@b
    .line 623
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 624
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 625
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDefaultSubscription()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 804
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getDefaultSubscription()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 808
    :goto_9
    return v1

    #@a
    .line 805
    :catch_a
    move-exception v0

    #@b
    .line 806
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 807
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 808
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDeviceId(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 148
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getDeviceId(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 152
    :goto_9
    return-object v1

    #@a
    .line 149
    :catch_a
    move-exception v0

    #@b
    .line 150
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 151
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 152
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDeviceSoftwareVersion(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 128
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getDeviceSvn(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 132
    :goto_9
    return-object v1

    #@a
    .line 129
    :catch_a
    move-exception v0

    #@b
    .line 130
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 131
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 132
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getLine1AlphaTag(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 473
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getLine1AlphaTag(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 478
    :goto_9
    return-object v1

    #@a
    .line 474
    :catch_a
    move-exception v0

    #@b
    .line 475
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 476
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 478
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getLine1Number(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 451
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getLine1Number(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 456
    :goto_9
    return-object v1

    #@a
    .line 452
    :catch_a
    move-exception v0

    #@b
    .line 453
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 454
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 456
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getLteOnCdmaMode(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 403
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getLteOnCdmaMode(I)I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 409
    :goto_9
    return v1

    #@a
    .line 404
    :catch_a
    move-exception v0

    #@b
    .line 406
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 407
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 409
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method protected getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;
    .registers 2

    #@0
    .prologue
    .line 566
    const-string v0, "iphonesubinfo_msim"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getNetworkOperator(I)Ljava/lang/String;
    .registers 4
    .parameter "subscription"

    #@0
    .prologue
    .line 242
    const-string v0, "gsm.operator.numeric"

    #@2
    const-string v1, ""

    #@4
    invoke-static {v0, p1, v1}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getNetworkOperatorName(I)Ljava/lang/String;
    .registers 4
    .parameter "subscription"

    #@0
    .prologue
    .line 226
    const-string v0, "gsm.operator.alpha"

    #@2
    const-string v1, ""

    #@4
    invoke-static {v0, p1, v1}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getNetworkType(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 285
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v1

    #@5
    .line 286
    .local v1, iTelephony:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    if-eqz v1, :cond_b

    #@7
    .line 287
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getNetworkType(I)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_c
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_a} :catch_e

    #@a
    move-result v2

    #@b
    .line 297
    .end local v1           #iTelephony:Lcom/android/internal/telephony/msim/ITelephonyMSim;
    :cond_b
    :goto_b
    return v2

    #@c
    .line 292
    :catch_c
    move-exception v0

    #@d
    .line 294
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_b

    #@e
    .line 295
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_e
    move-exception v0

    #@f
    .line 297
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_b
.end method

.method public getNetworkTypeName(I)Ljava/lang/String;
    .registers 3
    .parameter "subscription"

    #@0
    .prologue
    .line 310
    invoke-virtual {p0, p1}, Landroid/telephony/MSimTelephonyManager;->getNetworkType(I)I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Landroid/telephony/TelephonyManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getPhoneCount()I
    .registers 3

    #@0
    .prologue
    .line 111
    const/4 v0, 0x1

    #@1
    .line 112
    .local v0, phoneCount:I
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_8

    #@7
    .line 113
    const/4 v0, 0x2

    #@8
    .line 115
    :cond_8
    return v0
.end method

.method public getPreferredDataSubscription()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 817
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getPreferredDataSubscription()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 821
    :goto_9
    return v1

    #@a
    .line 818
    :catch_a
    move-exception v0

    #@b
    .line 819
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 820
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 821
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getPreferredVoiceSubscription()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 843
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getPreferredVoiceSubscription()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 847
    :goto_9
    return v1

    #@a
    .line 844
    :catch_a
    move-exception v0

    #@b
    .line 845
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 846
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 847
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getSimSerialNumber(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 382
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getIccSerialNumber(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 387
    :goto_9
    return-object v1

    #@a
    .line 383
    :catch_a
    move-exception v0

    #@b
    .line 384
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 385
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 387
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getSimState(I)I
    .registers 5
    .parameter "slotId"

    #@0
    .prologue
    .line 351
    const-string v1, "gsm.sim.state"

    #@2
    const-string v2, ""

    #@4
    invoke-static {v1, p1, v2}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 353
    .local v0, prop:Ljava/lang/String;
    const-string v1, "ABSENT"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 354
    const/4 v1, 0x1

    #@11
    .line 369
    :goto_11
    return v1

    #@12
    .line 356
    :cond_12
    const-string v1, "PIN_REQUIRED"

    #@14
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    .line 357
    const/4 v1, 0x2

    #@1b
    goto :goto_11

    #@1c
    .line 359
    :cond_1c
    const-string v1, "PUK_REQUIRED"

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_26

    #@24
    .line 360
    const/4 v1, 0x3

    #@25
    goto :goto_11

    #@26
    .line 362
    :cond_26
    const-string v1, "NETWORK_LOCKED"

    #@28
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_30

    #@2e
    .line 363
    const/4 v1, 0x4

    #@2f
    goto :goto_11

    #@30
    .line 365
    :cond_30
    const-string v1, "READY"

    #@32
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_3a

    #@38
    .line 366
    const/4 v1, 0x5

    #@39
    goto :goto_11

    #@3a
    .line 369
    :cond_3a
    const/4 v1, 0x0

    #@3b
    goto :goto_11
.end method

.method public getSubscriberId(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 431
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getSubscriberId(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 436
    :goto_9
    return-object v1

    #@a
    .line 432
    :catch_a
    move-exception v0

    #@b
    .line 433
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 434
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 436
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getVoiceMailAlphaTag(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 552
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getVoiceMailAlphaTag(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 557
    :goto_9
    return-object v1

    #@a
    .line 553
    :catch_a
    move-exception v0

    #@b
    .line 554
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 555
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 557
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getVoiceMailNumber(I)Ljava/lang/String;
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 492
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/MSimTelephonyManager;->getMSimSubscriberInfo()Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/IPhoneSubInfoMSim;->getVoiceMailNumber(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 497
    :goto_9
    return-object v1

    #@a
    .line 493
    :catch_a
    move-exception v0

    #@b
    .line 494
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 495
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 497
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getVoiceMessageCount(I)I
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 532
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->getVoiceMessageCount(I)I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 537
    :goto_9
    return v1

    #@a
    .line 533
    :catch_a
    move-exception v0

    #@b
    .line 534
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 535
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 537
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public hasIccCard(I)Z
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 327
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->hasIccCard(I)Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 333
    :goto_9
    return v1

    #@a
    .line 328
    :catch_a
    move-exception v0

    #@b
    .line 330
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 331
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 333
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public isMultiSimEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 102
    sget-object v0, Landroid/telephony/MSimTelephonyManager;->multiSimConfig:Ljava/lang/String;

    #@2
    const-string v1, "dsds"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    sget-object v0, Landroid/telephony/MSimTelephonyManager;->multiSimConfig:Ljava/lang/String;

    #@c
    const-string v1, "dsda"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public isNetworkRoaming(I)Z
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    .line 255
    const-string/jumbo v0, "true"

    #@3
    const-string v1, "gsm.operator.isroaming"

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {v1, p1, v2}, Landroid/telephony/MSimTelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public listen(Landroid/telephony/PhoneStateListener;I)V
    .registers 10
    .parameter "listener"
    .parameter "events"

    #@0
    .prologue
    .line 664
    sget-object v0, Landroid/telephony/MSimTelephonyManager;->sContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_24

    #@4
    sget-object v0, Landroid/telephony/MSimTelephonyManager;->sContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 666
    .local v1, pkgForDebug:Ljava/lang/String;
    :goto_a
    :try_start_a
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@d
    move-result-object v0

    #@e
    if-eqz v0, :cond_27

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@14
    move-result-object v6

    #@15
    .line 667
    .local v6, notifyNow:Ljava/lang/Boolean;
    sget-object v0, Landroid/telephony/MSimTelephonyManager;->sRegistryMsim:Lcom/android/internal/telephony/ITelephonyRegistryMSim;

    #@17
    iget-object v2, p1, Landroid/telephony/PhoneStateListener;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@19
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    #@1c
    move-result v4

    #@1d
    iget v5, p1, Landroid/telephony/PhoneStateListener;->mSubscription:I

    #@1f
    move v3, p2

    #@20
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/ITelephonyRegistryMSim;->listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZI)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_23} :catch_2b
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_23} :catch_29

    #@23
    .line 674
    .end local v6           #notifyNow:Ljava/lang/Boolean;
    :goto_23
    return-void

    #@24
    .line 664
    .end local v1           #pkgForDebug:Ljava/lang/String;
    :cond_24
    const-string v1, "<unknown>"

    #@26
    goto :goto_a

    #@27
    .line 666
    .restart local v1       #pkgForDebug:Ljava/lang/String;
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_11

    #@29
    .line 671
    :catch_29
    move-exception v0

    #@2a
    goto :goto_23

    #@2b
    .line 669
    :catch_2b
    move-exception v0

    #@2c
    goto :goto_23
.end method

.method public setPreferredDataSubscription(I)Z
    .registers 5
    .parameter "subscription"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 830
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/MSimTelephonyManager;->getITelephonyMSim()Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->setPreferredDataSubscription(I)Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 834
    :goto_9
    return v1

    #@a
    .line 831
    :catch_a
    move-exception v0

    #@b
    .line 832
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 833
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 834
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method
