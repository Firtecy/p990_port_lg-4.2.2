.class public Landroid/telephony/AssistDialPhoneNumberUtils;
.super Ljava/lang/Object;
.source "AssistDialPhoneNumberUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telephony/AssistDialPhoneNumberUtils$1;,
        Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;,
        Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;
    }
.end annotation


# static fields
.field static final DBG:Z = true

.field static final LOG_TAG:Ljava/lang/String; = "AssistDialPhoneNumberUtils"

.field static final MAX_SEND_DIAL_LENGTH:I = 0x20

.field private static final NANP_LENGTH:I = 0xa

.field private static afterNumber:Ljava/lang/String;


# instance fields
.field private areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

.field private countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

.field private mRefCountryList:[Landroid/provider/ReferenceCountry;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 24
    iput-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6
    .line 25
    iput-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8
    .line 26
    iput-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@a
    .line 32
    invoke-direct {p0}, Landroid/telephony/AssistDialPhoneNumberUtils;->initAreaCodeInfo()V

    #@d
    .line 33
    invoke-direct {p0}, Landroid/telephony/AssistDialPhoneNumberUtils;->initSIDRangeInfo()V

    #@10
    .line 34
    return-void
.end method

.method private callAssistedDialSetIntNetwork(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Ljava/lang/String;
    .registers 14
    .parameter "pNumber"
    .parameter "currentCountry"
    .parameter "context"

    #@0
    .prologue
    .line 1062
    const/4 v0, 0x0

    #@1
    .line 1063
    .local v0, bNumberPos:I
    const/4 v6, 0x0

    #@2
    .line 1064
    .local v6, retVal:Z
    const/4 v5, 0x0

    #@3
    .line 1067
    .local v5, otaNdd:Ljava/lang/String;
    invoke-static {p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getDialLengthBeforePause(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    .line 1069
    sput-object p1, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@9
    .line 1071
    const-string v7, "AssistDialPhoneNumberUtils"

    #@b
    new-instance v8, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v9, "callAssistedDialSetIntNetwork start : pNumber = "

    #@12
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v8

    #@16
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v8

    #@1a
    const-string v9, ", currentCountry = "

    #@1c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v8

    #@20
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v8

    #@28
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1073
    const/4 v7, 0x0

    #@2c
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@2f
    move-result v7

    #@30
    const/16 v8, 0x2a

    #@32
    if-eq v7, v8, :cond_3d

    #@34
    const/4 v7, 0x0

    #@35
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@38
    move-result v7

    #@39
    const/16 v8, 0x23

    #@3b
    if-ne v7, v8, :cond_40

    #@3d
    .line 1075
    :cond_3d
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@3f
    .line 1206
    :goto_3f
    return-object v7

    #@40
    .line 1077
    :cond_40
    const/16 v7, 0x20

    #@42
    if-gt v0, v7, :cond_4c

    #@44
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@47
    move-result v7

    #@48
    const/16 v8, 0x20

    #@4a
    if-le v7, v8, :cond_4f

    #@4c
    .line 1080
    :cond_4c
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@4e
    goto :goto_3f

    #@4f
    .line 1082
    :cond_4f
    const/16 v7, 0xa

    #@51
    if-gt v0, v7, :cond_167

    #@53
    .line 1084
    const/4 v1, 0x0

    #@54
    .line 1086
    .local v1, dialfromContact:I
    :try_start_54
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@57
    move-result-object v7

    #@58
    const-string v8, "assist_dial_from_contact"

    #@5a
    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_5d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_54 .. :try_end_5d} :catch_d9

    #@5d
    move-result v1

    #@5e
    .line 1092
    :goto_5e
    invoke-virtual {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getIsAssistedDialCheck(Landroid/content/Context;)I

    #@61
    move-result v7

    #@62
    const/4 v8, 0x1

    #@63
    if-ne v7, v8, :cond_165

    #@65
    const/4 v7, 0x1

    #@66
    if-ne v1, v7, :cond_165

    #@68
    .line 1093
    invoke-direct {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@6b
    move-result-object v3

    #@6c
    .line 1095
    .local v3, mRefer:Landroid/provider/ReferenceCountry;
    if-eqz v3, :cond_14b

    #@6e
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getIndex()I

    #@71
    move-result v7

    #@72
    if-nez v7, :cond_14b

    #@74
    .line 1096
    const-string v7, "AssistDialPhoneNumberUtils"

    #@76
    const-string v8, "****** 11"

    #@78
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 1097
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@7d
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@80
    move-result v7

    #@81
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getNumLength()Ljava/lang/String;

    #@84
    move-result-object v8

    #@85
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@88
    move-result v8

    #@89
    if-ne v7, v8, :cond_de

    #@8b
    .line 1099
    const-string v7, "AssistDialPhoneNumberUtils"

    #@8d
    const-string v8, "****** 12"

    #@8f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 1100
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@94
    invoke-virtual {p0, v7}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAreacodeInfoforContactNumber(Ljava/lang/String;)Z

    #@97
    move-result v7

    #@98
    const/4 v8, 0x1

    #@99
    if-ne v7, v8, :cond_de

    #@9b
    .line 1101
    const-string v7, "AssistDialPhoneNumberUtils"

    #@9d
    const-string v8, "****** 13"

    #@9f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 1102
    new-instance v7, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@aa
    move-result-object v8

    #@ab
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v7

    #@af
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@b2
    move-result-object v8

    #@b3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v7

    #@b7
    sget-object v8, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@b9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v7

    #@bd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v7

    #@c1
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@c3
    .line 1103
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@c5
    invoke-static {p1, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@c8
    move-result v7

    #@c9
    if-nez v7, :cond_d5

    #@cb
    .line 1104
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@ce
    move-result-object v7

    #@cf
    const-string v8, "assist_dial_new_number_check"

    #@d1
    const/4 v9, 0x1

    #@d2
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@d5
    .line 1106
    :cond_d5
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@d7
    goto/16 :goto_3f

    #@d9
    .line 1087
    .end local v3           #mRefer:Landroid/provider/ReferenceCountry;
    :catch_d9
    move-exception v2

    #@da
    .line 1089
    .local v2, e:Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    #@dd
    goto :goto_5e

    #@de
    .line 1109
    .end local v2           #e:Landroid/provider/Settings$SettingNotFoundException;
    .restart local v3       #mRefer:Landroid/provider/ReferenceCountry;
    :cond_de
    const-string v7, "AssistDialPhoneNumberUtils"

    #@e0
    const-string v8, "****** 14"

    #@e2
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 1111
    const/4 v4, 0x0

    #@e6
    .line 1112
    .local v4, mReferAreaCodeLen:I
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@e9
    move-result-object v7

    #@ea
    if-eqz v7, :cond_f4

    #@ec
    .line 1113
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@ef
    move-result-object v7

    #@f0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@f3
    move-result v4

    #@f4
    .line 1116
    :cond_f4
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@f6
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@f9
    move-result v7

    #@fa
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getNumLength()Ljava/lang/String;

    #@fd
    move-result-object v8

    #@fe
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@101
    move-result v8

    #@102
    sub-int/2addr v8, v4

    #@103
    if-ne v7, v8, :cond_14b

    #@105
    .line 1117
    const-string v7, "AssistDialPhoneNumberUtils"

    #@107
    const-string v8, "****** 15"

    #@109
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    .line 1118
    new-instance v7, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@114
    move-result-object v8

    #@115
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v7

    #@119
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@11c
    move-result-object v8

    #@11d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v7

    #@121
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@124
    move-result-object v8

    #@125
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v7

    #@129
    sget-object v8, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@12b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v7

    #@12f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v7

    #@133
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@135
    .line 1122
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@137
    invoke-static {p1, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@13a
    move-result v7

    #@13b
    if-nez v7, :cond_147

    #@13d
    .line 1123
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@140
    move-result-object v7

    #@141
    const-string v8, "assist_dial_new_number_check"

    #@143
    const/4 v9, 0x1

    #@144
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@147
    .line 1125
    :cond_147
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@149
    goto/16 :goto_3f

    #@14b
    .line 1128
    .end local v4           #mReferAreaCodeLen:I
    :cond_14b
    const-string v7, "AssistDialPhoneNumberUtils"

    #@14d
    const-string v8, "****** 16"

    #@14f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@152
    .line 1174
    .end local v1           #dialfromContact:I
    .end local v3           #mRefer:Landroid/provider/ReferenceCountry;
    :cond_152
    :goto_152
    if-nez v6, :cond_235

    #@154
    .line 1176
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@157
    move-result-object v5

    #@158
    .line 1178
    const/4 v7, 0x0

    #@159
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@15c
    move-result v7

    #@15d
    const/16 v8, 0x2b

    #@15f
    if-ne v7, v8, :cond_1fc

    #@161
    .line 1180
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@163
    goto/16 :goto_3f

    #@165
    .line 1131
    .restart local v1       #dialfromContact:I
    :cond_165
    const/4 v6, 0x1

    #@166
    goto :goto_152

    #@167
    .line 1134
    .end local v1           #dialfromContact:I
    :cond_167
    const/16 v7, 0xb

    #@169
    if-lt v0, v7, :cond_152

    #@16b
    .line 1136
    const/4 v7, 0x0

    #@16c
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@16f
    move-result v7

    #@170
    const/16 v8, 0x2b

    #@172
    if-ne v7, v8, :cond_1a0

    #@174
    .line 1139
    const/4 v7, 0x1

    #@175
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@178
    move-result-object v7

    #@179
    invoke-direct {p0, v7, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->callAssistedLocal_CountryCode(Ljava/lang/String;Landroid/provider/ReferenceCountry;)Z

    #@17c
    move-result v7

    #@17d
    if-nez v7, :cond_19b

    #@17f
    .line 1142
    new-instance v7, Ljava/lang/StringBuilder;

    #@181
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@184
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@187
    move-result-object v8

    #@188
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v7

    #@18c
    const/4 v8, 0x1

    #@18d
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@190
    move-result-object v8

    #@191
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v7

    #@195
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v7

    #@199
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@19b
    .line 1144
    :cond_19b
    const/4 v6, 0x1

    #@19c
    .line 1145
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@19e
    goto/16 :goto_3f

    #@1a0
    .line 1150
    :cond_1a0
    if-nez v6, :cond_1e0

    #@1a2
    .line 1152
    invoke-direct {p0, p1, p2, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->callAssistedDialing_Int_Numbers(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z

    #@1a5
    move-result v6

    #@1a6
    .line 1154
    if-nez v6, :cond_1e0

    #@1a8
    .line 1156
    invoke-virtual {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getIsAssistedDialCheck(Landroid/content/Context;)I

    #@1ab
    move-result v7

    #@1ac
    const/4 v8, 0x1

    #@1ad
    if-ne v7, v8, :cond_1e0

    #@1af
    .line 1158
    invoke-direct {p0, p1, p2, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->isNeeded_OTAIddPrefix(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z

    #@1b2
    move-result v7

    #@1b3
    const/4 v8, 0x1

    #@1b4
    if-ne v7, v8, :cond_1e0

    #@1b6
    .line 1160
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1bb
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@1be
    move-result-object v8

    #@1bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v7

    #@1c3
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v7

    #@1c7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ca
    move-result-object v7

    #@1cb
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@1cd
    .line 1161
    const/4 v6, 0x1

    #@1ce
    .line 1163
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@1d0
    invoke-static {p1, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1d3
    move-result v7

    #@1d4
    if-nez v7, :cond_1e0

    #@1d6
    .line 1164
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d9
    move-result-object v7

    #@1da
    const-string v8, "assist_dial_new_number_check"

    #@1dc
    const/4 v9, 0x1

    #@1dd
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1e0
    .line 1170
    :cond_1e0
    const-string v7, "AssistDialPhoneNumberUtils"

    #@1e2
    new-instance v8, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v9, " afterNumber : "

    #@1e9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v8

    #@1ed
    sget-object v9, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@1ef
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v8

    #@1f3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f6
    move-result-object v8

    #@1f7
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1fa
    goto/16 :goto_152

    #@1fc
    .line 1183
    :cond_1fc
    if-eqz v5, :cond_212

    #@1fe
    const/4 v7, 0x0

    #@1ff
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@202
    move-result v8

    #@203
    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@206
    move-result-object v7

    #@207
    invoke-static {v7, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@20a
    move-result v7

    #@20b
    const/4 v8, 0x1

    #@20c
    if-ne v7, v8, :cond_212

    #@20e
    .line 1186
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@210
    goto/16 :goto_3f

    #@212
    .line 1190
    :cond_212
    invoke-virtual {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getIsAssistedDialCheck(Landroid/content/Context;)I

    #@215
    move-result v7

    #@216
    const/4 v8, 0x1

    #@217
    if-ne v7, v8, :cond_24f

    #@219
    .line 1191
    invoke-direct {p0, p1, v0, p2, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->callAssistedDial_NumLength(Ljava/lang/String;ILandroid/provider/ReferenceCountry;Landroid/content/Context;)Z

    #@21c
    move-result v6

    #@21d
    .line 1192
    if-eqz v6, :cond_24f

    #@21f
    .line 1194
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@221
    invoke-static {p1, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@224
    move-result v7

    #@225
    if-nez v7, :cond_231

    #@227
    .line 1195
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@22a
    move-result-object v7

    #@22b
    const-string v8, "assist_dial_new_number_check"

    #@22d
    const/4 v9, 0x1

    #@22e
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@231
    .line 1197
    :cond_231
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@233
    goto/16 :goto_3f

    #@235
    .line 1204
    :cond_235
    const-string v7, "AssistDialPhoneNumberUtils"

    #@237
    new-instance v8, Ljava/lang/StringBuilder;

    #@239
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23c
    const-string v9, " afterNumber : "

    #@23e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v8

    #@242
    sget-object v9, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@244
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@247
    move-result-object v8

    #@248
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24b
    move-result-object v8

    #@24c
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24f
    .line 1206
    :cond_24f
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@251
    goto/16 :goto_3f
.end method

.method private callAssistedDialSetVZWNetwork(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Ljava/lang/String;
    .registers 14
    .parameter "pNumber"
    .parameter "currentCountry"
    .parameter "context"

    #@0
    .prologue
    const/16 v7, 0x20

    #@2
    const/4 v9, 0x0

    #@3
    const/4 v8, 0x1

    #@4
    .line 957
    const/4 v3, 0x0

    #@5
    .line 958
    .local v3, retVal:Z
    const/4 v0, 0x0

    #@6
    .line 963
    .local v0, bNumberPos:I
    invoke-static {p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getDialLengthBeforePause(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    .line 964
    sput-object p1, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@c
    .line 966
    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v5

    #@10
    const/16 v6, 0x2a

    #@12
    if-eq v5, v6, :cond_1c

    #@14
    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v5

    #@18
    const/16 v6, 0x23

    #@1a
    if-ne v5, v6, :cond_1d

    #@1c
    .line 1058
    .end local p1
    :cond_1c
    :goto_1c
    return-object p1

    #@1d
    .line 970
    .restart local p1
    :cond_1d
    if-gt v0, v7, :cond_1c

    #@1f
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@22
    move-result v5

    #@23
    if-gt v5, v7, :cond_1c

    #@25
    .line 977
    const/16 v5, 0xb

    #@27
    if-lt v0, v5, :cond_1b0

    #@29
    .line 979
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    .line 980
    .local v2, otaNdd:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    .line 982
    .local v1, otaCC:Ljava/lang/String;
    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    #@34
    move-result v5

    #@35
    const/16 v6, 0x2b

    #@37
    if-ne v5, v6, :cond_13e

    #@39
    .line 985
    if-eqz v2, :cond_77

    #@3b
    .line 986
    const-string v5, "AssistDialPhoneNumberUtils"

    #@3d
    new-instance v6, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string/jumbo v7, "ota_ndd : "

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v6

    #@51
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 987
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    .line 988
    .local v4, temp:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@5b
    move-result v5

    #@5c
    invoke-virtual {v4, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@63
    move-result v5

    #@64
    if-eqz v5, :cond_107

    #@66
    .line 991
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {p0, v5}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAreacodeInfo(Ljava/lang/String;)Z

    #@6d
    move-result v5

    #@6e
    if-ne v5, v8, :cond_e4

    #@70
    .line 992
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    sput-object v5, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@76
    .line 1005
    :goto_76
    const/4 v3, 0x1

    #@77
    .line 1026
    .end local v4           #temp:Ljava/lang/String;
    :cond_77
    :goto_77
    if-nez v3, :cond_8c

    #@79
    if-eqz v1, :cond_8c

    #@7b
    .line 1027
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@7e
    move-result v5

    #@7f
    invoke-virtual {p1, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    invoke-static {v5, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@86
    move-result v5

    #@87
    if-ne v5, v8, :cond_196

    #@89
    .line 1030
    sput-object p1, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@8b
    .line 1031
    const/4 v3, 0x1

    #@8c
    .line 1036
    :cond_8c
    :goto_8c
    if-nez v3, :cond_c6

    #@8e
    .line 1038
    invoke-direct {p0, p1, p2, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->isNeeded_OTAIddPrefix(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z

    #@91
    move-result v5

    #@92
    if-ne v5, v8, :cond_199

    #@94
    .line 1039
    new-instance v5, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@9c
    move-result-object v6

    #@9d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v5

    #@a1
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v5

    #@a9
    sput-object v5, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@ab
    .line 1040
    const/4 v3, 0x1

    #@ac
    .line 1041
    const-string v5, "AssistDialPhoneNumberUtils"

    #@ae
    new-instance v6, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v7, "afterNumber : "

    #@b5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v6

    #@b9
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@bb
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v6

    #@bf
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v6

    #@c3
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 1053
    :cond_c6
    :goto_c6
    const-string v5, "AssistDialPhoneNumberUtils"

    #@c8
    new-instance v6, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v7, "afterNumber : "

    #@cf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v6

    #@d3
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@d5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v6

    #@d9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v6

    #@dd
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 1058
    .end local v1           #otaCC:Ljava/lang/String;
    .end local v2           #otaNdd:Ljava/lang/String;
    :goto_e0
    sget-object p1, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@e2
    goto/16 :goto_1c

    #@e4
    .line 995
    .restart local v1       #otaCC:Ljava/lang/String;
    .restart local v2       #otaNdd:Ljava/lang/String;
    .restart local v4       #temp:Ljava/lang/String;
    :cond_e4
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@e7
    move-result-object v5

    #@e8
    sput-object v5, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@ea
    .line 996
    const-string/jumbo v5, "nsalty"

    #@ed
    new-instance v6, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v7, "Not in the area code range but delete + :"

    #@f4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v6

    #@f8
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@fa
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v6

    #@fe
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v6

    #@102
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    goto/16 :goto_76

    #@107
    .line 1001
    :cond_107
    new-instance v5, Ljava/lang/StringBuilder;

    #@109
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10c
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@10f
    move-result-object v6

    #@110
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v5

    #@114
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@117
    move-result-object v6

    #@118
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v5

    #@11c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v5

    #@120
    sput-object v5, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@122
    .line 1003
    const-string v5, "AssistDialPhoneNumberUtils"

    #@124
    new-instance v6, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v7, "afterNumber : "

    #@12b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v6

    #@12f
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@131
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v6

    #@135
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@138
    move-result-object v6

    #@139
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13c
    goto/16 :goto_76

    #@13e
    .line 1009
    .end local v4           #temp:Ljava/lang/String;
    :cond_13e
    if-eqz v2, :cond_77

    #@140
    .line 1010
    const-string v5, "AssistDialPhoneNumberUtils"

    #@142
    new-instance v6, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string/jumbo v7, "pNumber.substring(0, ota_ndd.length()) : "

    #@14a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v6

    #@14e
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@151
    move-result v7

    #@152
    invoke-virtual {p1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@155
    move-result-object v7

    #@156
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v6

    #@15a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v6

    #@15e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@161
    .line 1013
    const-string v5, "AssistDialPhoneNumberUtils"

    #@163
    new-instance v6, Ljava/lang/StringBuilder;

    #@165
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@168
    const-string/jumbo v7, "ota_ndd : "

    #@16b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v6

    #@16f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v6

    #@173
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@176
    move-result-object v6

    #@177
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17a
    .line 1014
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@17d
    move-result v5

    #@17e
    invoke-virtual {p1, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@181
    move-result-object v5

    #@182
    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@185
    move-result v5

    #@186
    if-ne v5, v8, :cond_193

    #@188
    invoke-virtual {p0, p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAreacodeInfo(Ljava/lang/String;)Z

    #@18b
    move-result v5

    #@18c
    if-ne v5, v8, :cond_193

    #@18e
    .line 1018
    sput-object p1, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@190
    .line 1019
    const/4 v3, 0x1

    #@191
    goto/16 :goto_77

    #@193
    .line 1021
    :cond_193
    const/4 v3, 0x0

    #@194
    goto/16 :goto_77

    #@196
    .line 1033
    :cond_196
    const/4 v3, 0x0

    #@197
    goto/16 :goto_8c

    #@199
    .line 1045
    :cond_199
    invoke-virtual {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getIsAssistedDialCheck(Landroid/content/Context;)I

    #@19c
    move-result v5

    #@19d
    if-ne v5, v8, :cond_c6

    #@19f
    .line 1046
    invoke-direct {p0, p1, p2, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->callAssistedNonUS_IDDPrefix(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z

    #@1a2
    move-result v3

    #@1a3
    .line 1047
    if-eqz v3, :cond_c6

    #@1a5
    .line 1048
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a8
    move-result-object v5

    #@1a9
    const-string v6, "assist_dial_new_number_check"

    #@1ab
    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1ae
    goto/16 :goto_c6

    #@1b0
    .line 1056
    .end local v1           #otaCC:Ljava/lang/String;
    .end local v2           #otaNdd:Ljava/lang/String;
    :cond_1b0
    const-string v5, "AssistDialPhoneNumberUtils"

    #@1b2
    new-instance v6, Ljava/lang/StringBuilder;

    #@1b4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1b7
    const-string v7, "afterNumber : "

    #@1b9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v6

    #@1bd
    sget-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@1bf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v6

    #@1c3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c6
    move-result-object v6

    #@1c7
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ca
    goto/16 :goto_e0
.end method

.method private callAssistedDial_NumLength(Ljava/lang/String;ILandroid/provider/ReferenceCountry;Landroid/content/Context;)Z
    .registers 23
    .parameter "pNumber"
    .parameter "bNumberPos"
    .parameter "currentCountry"
    .parameter "context"

    #@0
    .prologue
    .line 844
    const/4 v14, 0x0

    #@1
    .line 846
    .local v14, retVal:Z
    const/4 v11, 0x0

    #@2
    .line 847
    .local v11, refNdd:Ljava/lang/String;
    const/4 v7, 0x0

    #@3
    .line 848
    .local v7, otaNdd:Ljava/lang/String;
    const/4 v6, 0x0

    #@4
    .line 849
    .local v6, otaIDD:Ljava/lang/String;
    const/4 v10, 0x0

    #@5
    .line 850
    .local v10, refCC:Ljava/lang/String;
    const/4 v5, 0x0

    #@6
    .line 851
    .local v5, otaCC:Ljava/lang/String;
    const/4 v2, 0x0

    #@7
    .line 852
    .local v2, areaCode:Ljava/lang/String;
    const/4 v9, 0x0

    #@8
    .line 853
    .local v9, prefixNumber:Ljava/lang/String;
    const/4 v12, 0x0

    #@9
    .line 854
    .local v12, refNddLen:I
    const/4 v8, 0x0

    #@a
    .line 855
    .local v8, otaNddLen:I
    const/4 v3, 0x0

    #@b
    .line 856
    .local v3, areaCodeLen:I
    const/4 v13, 0x0

    #@c
    .line 860
    .local v13, refNumLen:I
    move-object/from16 v0, p0

    #@e
    move-object/from16 v1, p4

    #@10
    invoke-direct {v0, v1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@13
    move-result-object v4

    #@14
    .line 862
    .local v4, mRefer:Landroid/provider/ReferenceCountry;
    if-nez v4, :cond_18

    #@16
    move v15, v14

    #@17
    .line 951
    .end local v14           #retVal:Z
    .local v15, retVal:I
    :goto_17
    return v15

    #@18
    .line 866
    .end local v15           #retVal:I
    .restart local v14       #retVal:Z
    :cond_18
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@1b
    move-result-object v11

    #@1c
    .line 867
    if-eqz v11, :cond_22

    #@1e
    .line 869
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@21
    move-result v12

    #@22
    .line 871
    :cond_22
    invoke-virtual/range {p3 .. p3}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@25
    move-result-object v7

    #@26
    .line 872
    invoke-virtual/range {p3 .. p3}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    .line 874
    if-eqz v7, :cond_31

    #@2c
    .line 876
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@2f
    move-result v8

    #@30
    .line 877
    move-object v9, v7

    #@31
    .line 880
    :cond_31
    invoke-virtual/range {p3 .. p3}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    .line 881
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@38
    move-result-object v10

    #@39
    .line 883
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getNumLength()Ljava/lang/String;

    #@3c
    move-result-object v16

    #@3d
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@40
    move-result v13

    #@41
    .line 887
    if-eqz v12, :cond_a4

    #@43
    const/16 v16, 0x0

    #@45
    :try_start_45
    move-object/from16 v0, p1

    #@47
    move/from16 v1, v16

    #@49
    invoke-virtual {v0, v1, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4c
    move-result-object v16

    #@4d
    move-object/from16 v0, v16

    #@4f
    invoke-static {v0, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@52
    move-result v16

    #@53
    const/16 v17, 0x1

    #@55
    move/from16 v0, v16

    #@57
    move/from16 v1, v17

    #@59
    if-ne v0, v1, :cond_a4

    #@5b
    add-int v16, v13, v12

    #@5d
    move/from16 v0, p2

    #@5f
    move/from16 v1, v16

    #@61
    if-ne v0, v1, :cond_a4

    #@63
    .line 890
    invoke-static {v10, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@66
    move-result v16

    #@67
    const/16 v17, 0x1

    #@69
    move/from16 v0, v16

    #@6b
    move/from16 v1, v17

    #@6d
    if-ne v0, v1, :cond_72

    #@6f
    .line 892
    const/4 v14, 0x1

    #@70
    :cond_70
    :goto_70
    move v15, v14

    #@71
    .line 951
    .restart local v15       #retVal:I
    goto :goto_17

    #@72
    .line 896
    .end local v15           #retVal:I
    :cond_72
    new-instance v16, Ljava/lang/StringBuilder;

    #@74
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    move-object/from16 v0, v16

    #@79
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v16

    #@7d
    move-object/from16 v0, v16

    #@7f
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v16

    #@83
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v9

    #@87
    .line 897
    new-instance v16, Ljava/lang/StringBuilder;

    #@89
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    move-object/from16 v0, v16

    #@8e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v16

    #@92
    move-object/from16 v0, p1

    #@94
    invoke-virtual {v0, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@97
    move-result-object v17

    #@98
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v16

    #@9c
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v16

    #@a0
    sput-object v16, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@a2
    .line 898
    const/4 v14, 0x1

    #@a3
    goto :goto_70

    #@a4
    .line 902
    :cond_a4
    if-eqz v8, :cond_107

    #@a6
    const/16 v16, 0x0

    #@a8
    move-object/from16 v0, p1

    #@aa
    move/from16 v1, v16

    #@ac
    invoke-virtual {v0, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@af
    move-result-object v16

    #@b0
    move-object/from16 v0, v16

    #@b2
    invoke-static {v0, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@b5
    move-result v16

    #@b6
    const/16 v17, 0x1

    #@b8
    move/from16 v0, v16

    #@ba
    move/from16 v1, v17

    #@bc
    if-ne v0, v1, :cond_107

    #@be
    add-int v16, v13, v12

    #@c0
    move/from16 v0, p2

    #@c2
    move/from16 v1, v16

    #@c4
    if-ne v0, v1, :cond_107

    #@c6
    .line 906
    invoke-static {v10, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@c9
    move-result v16

    #@ca
    const/16 v17, 0x1

    #@cc
    move/from16 v0, v16

    #@ce
    move/from16 v1, v17

    #@d0
    if-ne v0, v1, :cond_d4

    #@d2
    .line 908
    const/4 v14, 0x1

    #@d3
    goto :goto_70

    #@d4
    .line 912
    :cond_d4
    new-instance v16, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    move-object/from16 v0, v16

    #@db
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v16

    #@df
    move-object/from16 v0, v16

    #@e1
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v16

    #@e5
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v9

    #@e9
    .line 913
    new-instance v16, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    move-object/from16 v0, v16

    #@f0
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v16

    #@f4
    move-object/from16 v0, p1

    #@f6
    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@f9
    move-result-object v17

    #@fa
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v16

    #@fe
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v16

    #@102
    sput-object v16, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@104
    .line 914
    const/4 v14, 0x1

    #@105
    goto/16 :goto_70

    #@107
    .line 917
    :cond_107
    move/from16 v0, p2

    #@109
    if-gt v0, v13, :cond_198

    #@10b
    .line 920
    move/from16 v0, p2

    #@10d
    if-ne v0, v13, :cond_140

    #@10f
    .line 922
    new-instance v16, Ljava/lang/StringBuilder;

    #@111
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    move-object/from16 v0, v16

    #@116
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v16

    #@11a
    move-object/from16 v0, v16

    #@11c
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v16

    #@120
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v9

    #@124
    .line 923
    new-instance v16, Ljava/lang/StringBuilder;

    #@126
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    move-object/from16 v0, v16

    #@12b
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v16

    #@12f
    move-object/from16 v0, v16

    #@131
    move-object/from16 v1, p1

    #@133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v16

    #@137
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v16

    #@13b
    sput-object v16, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@13d
    .line 924
    const/4 v14, 0x1

    #@13e
    goto/16 :goto_70

    #@140
    .line 929
    :cond_140
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@143
    move-result-object v2

    #@144
    .line 931
    if-eqz v2, :cond_14a

    #@146
    .line 933
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@149
    move-result v3

    #@14a
    .line 935
    :cond_14a
    sub-int v16, v13, v3

    #@14c
    move/from16 v0, p2

    #@14e
    move/from16 v1, v16

    #@150
    if-ne v0, v1, :cond_70

    #@152
    .line 937
    new-instance v16, Ljava/lang/StringBuilder;

    #@154
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    move-object/from16 v0, v16

    #@159
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v16

    #@15d
    move-object/from16 v0, v16

    #@15f
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v16

    #@163
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v9

    #@167
    .line 938
    new-instance v16, Ljava/lang/StringBuilder;

    #@169
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@16c
    move-object/from16 v0, v16

    #@16e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v16

    #@172
    move-object/from16 v0, v16

    #@174
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v16

    #@178
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17b
    move-result-object v9

    #@17c
    .line 939
    new-instance v16, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    move-object/from16 v0, v16

    #@183
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v16

    #@187
    move-object/from16 v0, v16

    #@189
    move-object/from16 v1, p1

    #@18b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v16

    #@18f
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@192
    move-result-object v16

    #@193
    sput-object v16, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;
    :try_end_195
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_45 .. :try_end_195} :catch_19b

    #@195
    .line 940
    const/4 v14, 0x1

    #@196
    goto/16 :goto_70

    #@198
    .line 946
    :cond_198
    const/4 v14, 0x0

    #@199
    goto/16 :goto_70

    #@19b
    .line 948
    :catch_19b
    move-exception v16

    #@19c
    goto/16 :goto_70
.end method

.method private callAssistedDialing_Int_Numbers(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z
    .registers 15
    .parameter "pNumber"
    .parameter "currentCountry"
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 772
    const/4 v5, 0x0

    #@2
    .line 773
    .local v5, refIdd:Ljava/lang/String;
    const/4 v4, 0x0

    #@3
    .line 774
    .local v4, otaIdd:Ljava/lang/String;
    const/4 v2, 0x0

    #@4
    .line 775
    .local v2, localCC:Ljava/lang/String;
    const/4 v0, 0x0

    #@5
    .line 776
    .local v0, iddPrefixLen:I
    const/4 v6, 0x0

    #@6
    .line 777
    .local v6, retVal:Z
    const/4 v1, 0x0

    #@7
    .line 783
    .local v1, isAssistDialing:Z
    invoke-direct {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@a
    move-result-object v3

    #@b
    .line 784
    .local v3, mRefer:Landroid/provider/ReferenceCountry;
    if-eqz v3, :cond_11

    #@d
    .line 785
    invoke-virtual {v3}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    .line 787
    :cond_11
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 790
    if-eqz v4, :cond_2b

    #@17
    .line 792
    const/4 v8, 0x0

    #@18
    :try_start_18
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@1b
    move-result v9

    #@1c
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v8

    #@20
    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@23
    move-result v8

    #@24
    if-ne v8, v10, :cond_2b

    #@26
    .line 794
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@29
    move-result v0

    #@2a
    .line 795
    const/4 v6, 0x1

    #@2b
    .line 798
    :cond_2b
    if-nez v6, :cond_4a

    #@2d
    if-eqz v5, :cond_4a

    #@2f
    .line 800
    const/4 v8, 0x0

    #@30
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@33
    move-result v9

    #@34
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-static {v8, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3b
    move-result v8

    #@3c
    if-ne v8, v10, :cond_4a

    #@3e
    .line 802
    invoke-virtual {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getIsAssistedDialCheck(Landroid/content/Context;)I

    #@41
    move-result v8

    #@42
    if-ne v8, v10, :cond_4a

    #@44
    .line 803
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@47
    move-result v0

    #@48
    .line 804
    const/4 v6, 0x1

    #@49
    .line 805
    const/4 v1, 0x1

    #@4a
    .line 809
    :cond_4a
    if-eqz v6, :cond_f0

    #@4c
    .line 811
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    .line 812
    if-eqz v2, :cond_f0

    #@52
    .line 815
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@55
    move-result-object v7

    #@56
    .line 816
    .local v7, temp:Ljava/lang/String;
    const/4 v8, 0x0

    #@57
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@5a
    move-result v9

    #@5b
    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5e
    move-result-object v8

    #@5f
    invoke-static {v8, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@62
    move-result v8

    #@63
    if-ne v8, v10, :cond_f1

    #@65
    .line 819
    new-instance v8, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@75
    move-result v9

    #@76
    add-int/2addr v9, v0

    #@77
    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@7a
    move-result-object v9

    #@7b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v8

    #@7f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v8

    #@83
    sput-object v8, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@85
    .line 820
    const-string v8, "AssistDialPhoneNumberUtils"

    #@87
    new-instance v9, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v10, "callAssistedDialing_Int_Numbers() ==>  currentCountry.getNddPrefix() : "

    #@8e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v9

    #@92
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@95
    move-result-object v10

    #@96
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v9

    #@9a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v9

    #@9e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 821
    const-string v8, "AssistDialPhoneNumberUtils"

    #@a3
    new-instance v9, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v10, "callAssistedDialing_Int_Numbers() ==>  pNumber.substring(idd_prefix_len + local_CC.length()) : "

    #@aa
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@b1
    move-result v10

    #@b2
    add-int/2addr v10, v0

    #@b3
    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@b6
    move-result-object v10

    #@b7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v9

    #@bb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v9

    #@bf
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 822
    const-string v8, "AssistDialPhoneNumberUtils"

    #@c4
    new-instance v9, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v10, "callAssistedDialing_Int_Numbers() ==>  afterNumber : "

    #@cb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v9

    #@cf
    sget-object v10, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@d1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v9

    #@d5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v9

    #@d9
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 831
    :cond_dc
    :goto_dc
    if-eqz v1, :cond_f0

    #@de
    sget-object v8, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@e0
    invoke-static {p1, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@e3
    move-result v8

    #@e4
    if-nez v8, :cond_f0

    #@e6
    .line 832
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e9
    move-result-object v8

    #@ea
    const-string v9, "assist_dial_new_number_check"

    #@ec
    const/4 v10, 0x1

    #@ed
    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@f0
    .line 840
    .end local v7           #temp:Ljava/lang/String;
    :cond_f0
    :goto_f0
    return v6

    #@f1
    .line 825
    .restart local v7       #temp:Ljava/lang/String;
    :cond_f1
    if-eqz v1, :cond_dc

    #@f3
    .line 828
    new-instance v8, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@fb
    move-result-object v9

    #@fc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v8

    #@100
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@103
    move-result-object v9

    #@104
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v8

    #@108
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v8

    #@10c
    sput-object v8, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;
    :try_end_10e
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_18 .. :try_end_10e} :catch_10f

    #@10e
    goto :goto_dc

    #@10f
    .line 836
    .end local v7           #temp:Ljava/lang/String;
    :catch_10f
    move-exception v8

    #@110
    goto :goto_f0
.end method

.method private callAssistedLocal_CountryCode(Ljava/lang/String;Landroid/provider/ReferenceCountry;)Z
    .registers 8
    .parameter "pNumber"
    .parameter "currentCountry"

    #@0
    .prologue
    .line 749
    const/4 v1, 0x0

    #@1
    .line 752
    .local v1, retVal:Z
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 754
    .local v0, retOtaToken:Ljava/lang/String;
    if-eqz v0, :cond_51

    #@7
    .line 757
    const/4 v2, 0x0

    #@8
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@b
    move-result v3

    #@c
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@13
    move-result v2

    #@14
    const/4 v3, 0x1

    #@15
    if-ne v2, v3, :cond_51

    #@17
    .line 760
    const-string v2, "AssistDialPhoneNumberUtils"

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "callAssistedLocal_CountryCode() ==> afterNumber : "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    sget-object v4, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 761
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@41
    move-result v3

    #@42
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    sput-object v2, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;
    :try_end_50
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_8 .. :try_end_50} :catch_52

    #@50
    .line 762
    const/4 v1, 0x1

    #@51
    .line 768
    :cond_51
    :goto_51
    return v1

    #@52
    .line 765
    :catch_52
    move-exception v2

    #@53
    goto :goto_51
.end method

.method private callAssistedNonUS_IDDPrefix(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z
    .registers 16
    .parameter "pNumber"
    .parameter "currentCountry"
    .parameter "context"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 609
    const/4 v3, 0x0

    #@3
    .line 610
    .local v3, otaIdd:Ljava/lang/String;
    const/4 v6, 0x0

    #@4
    .line 611
    .local v6, refIdd:Ljava/lang/String;
    const/4 v5, 0x0

    #@5
    .line 612
    .local v5, refCountryIdd:Ljava/lang/String;
    const/4 v4, 0x0

    #@6
    .line 614
    .local v4, refCc:Ljava/lang/String;
    invoke-direct {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@9
    move-result-object v2

    #@a
    .line 615
    .local v2, mRefer:Landroid/provider/ReferenceCountry;
    if-eqz v2, :cond_10

    #@c
    .line 616
    invoke-virtual {v2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@f
    move-result-object v5

    #@10
    .line 619
    :cond_10
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 621
    const-string v9, "AssistDialPhoneNumberUtils"

    #@16
    new-instance v10, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v11, "callAssistedNonUS_IDDPrefix() ==> pNumber.substring(0, ota_idd.length()-1) : "

    #@1d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v10

    #@21
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@24
    move-result v11

    #@25
    invoke-virtual {p1, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v11

    #@29
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v10

    #@2d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v10

    #@31
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 622
    const-string v9, "AssistDialPhoneNumberUtils"

    #@36
    new-instance v10, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v11, "callAssistedNonUS_IDDPrefix() ==> ota_idd : "

    #@3d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v10

    #@41
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v10

    #@45
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v10

    #@49
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 623
    if-nez v3, :cond_4f

    #@4e
    .line 678
    :cond_4e
    :goto_4e
    return v7

    #@4f
    .line 627
    :cond_4f
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@52
    move-result v9

    #@53
    invoke-virtual {p1, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@56
    move-result-object v9

    #@57
    invoke-static {v9, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5a
    move-result v9

    #@5b
    if-eq v9, v8, :cond_4e

    #@5d
    .line 633
    if-eqz v5, :cond_e3

    #@5f
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@62
    move-result v9

    #@63
    if-lez v9, :cond_e3

    #@65
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@68
    move-result v9

    #@69
    invoke-virtual {p1, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6c
    move-result-object v9

    #@6d
    invoke-static {v9, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@70
    move-result v9

    #@71
    if-ne v9, v8, :cond_e3

    #@73
    .line 637
    const/4 v0, 0x0

    #@74
    .local v0, i:I
    :goto_74
    iget-object v9, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@76
    array-length v9, v9

    #@77
    if-ge v0, v9, :cond_e3

    #@79
    .line 638
    iget-object v9, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@7b
    aget-object v9, v9, v0

    #@7d
    invoke-virtual {v9}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    .line 640
    if-eqz v4, :cond_e0

    #@83
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@86
    move-result v9

    #@87
    if-lez v9, :cond_e0

    #@89
    .line 642
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@8c
    move-result v9

    #@8d
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@90
    move-result v10

    #@91
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@94
    move-result v11

    #@95
    add-int/2addr v10, v11

    #@96
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@99
    move-result-object v9

    #@9a
    invoke-static {v9, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@9d
    move-result v9

    #@9e
    if-ne v9, v8, :cond_e0

    #@a0
    .line 643
    const-string v7, "AssistDialPhoneNumberUtils"

    #@a2
    new-instance v9, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v10, "callAssistedNonUS_IDDPrefix() ==> refCountryIdd : "

    #@a9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v9

    #@b1
    const-string v10, ", refCc : "

    #@b3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v9

    #@b7
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v9

    #@bb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v9

    #@bf
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 645
    new-instance v7, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v7

    #@cb
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@ce
    move-result v9

    #@cf
    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@d2
    move-result-object v9

    #@d3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v7

    #@d7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v7

    #@db
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@dd
    move v7, v8

    #@de
    .line 646
    goto/16 :goto_4e

    #@e0
    .line 637
    :cond_e0
    add-int/lit8 v0, v0, 0x1

    #@e2
    goto :goto_74

    #@e3
    .line 653
    .end local v0           #i:I
    :cond_e3
    const/4 v0, 0x0

    #@e4
    .restart local v0       #i:I
    :goto_e4
    iget-object v9, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@e6
    array-length v9, v9

    #@e7
    if-ge v0, v9, :cond_4e

    #@e9
    .line 655
    iget-object v9, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@eb
    aget-object v9, v9, v0

    #@ed
    invoke-virtual {v9}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@f0
    move-result-object v6

    #@f1
    .line 657
    if-eqz v6, :cond_18c

    #@f3
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@f6
    move-result v9

    #@f7
    if-lez v9, :cond_18c

    #@f9
    .line 659
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@fc
    move-result v9

    #@fd
    invoke-virtual {p1, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@100
    move-result-object v9

    #@101
    invoke-static {v9, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@104
    move-result v9

    #@105
    if-ne v9, v8, :cond_18c

    #@107
    .line 661
    const/4 v1, 0x0

    #@108
    .local v1, j:I
    :goto_108
    iget-object v9, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@10a
    array-length v9, v9

    #@10b
    if-ge v1, v9, :cond_18c

    #@10d
    .line 662
    iget-object v9, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@10f
    aget-object v9, v9, v1

    #@111
    invoke-virtual {v9}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@114
    move-result-object v4

    #@115
    .line 664
    if-eqz v4, :cond_188

    #@117
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@11a
    move-result v9

    #@11b
    if-lez v9, :cond_188

    #@11d
    .line 666
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@120
    move-result v9

    #@121
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@124
    move-result v10

    #@125
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@128
    move-result v11

    #@129
    add-int/2addr v10, v11

    #@12a
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@12d
    move-result-object v9

    #@12e
    invoke-static {v9, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@131
    move-result v9

    #@132
    if-ne v9, v8, :cond_188

    #@134
    .line 667
    const-string v7, "AssistDialPhoneNumberUtils"

    #@136
    new-instance v9, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v10, "callAssistedNonUS_IDDPrefix() ==> refIdd : "

    #@13d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v9

    #@141
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v9

    #@145
    const-string v10, ", refCc : "

    #@147
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v9

    #@14b
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v9

    #@14f
    const-string v10, ", i="

    #@151
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v9

    #@155
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@158
    move-result-object v9

    #@159
    const-string v10, ", j="

    #@15b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v9

    #@15f
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@162
    move-result-object v9

    #@163
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v9

    #@167
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16a
    .line 669
    new-instance v7, Ljava/lang/StringBuilder;

    #@16c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16f
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v7

    #@173
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@176
    move-result v9

    #@177
    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@17a
    move-result-object v9

    #@17b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v7

    #@17f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v7

    #@183
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@185
    move v7, v8

    #@186
    .line 670
    goto/16 :goto_4e

    #@188
    .line 661
    :cond_188
    add-int/lit8 v1, v1, 0x1

    #@18a
    goto/16 :goto_108

    #@18c
    .line 653
    .end local v1           #j:I
    :cond_18c
    add-int/lit8 v0, v0, 0x1

    #@18e
    goto/16 :goto_e4
.end method

.method private static getDialLengthBeforePause(Ljava/lang/String;)I
    .registers 5
    .parameter "pNumber"

    #@0
    .prologue
    .line 360
    const/4 v0, 0x0

    #@1
    .line 361
    .local v0, count:I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    if-ge v0, v1, :cond_1b

    #@7
    .line 362
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v1

    #@b
    const/16 v2, 0x2c

    #@d
    if-eq v1, v2, :cond_17

    #@f
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v1

    #@13
    const/16 v2, 0x3b

    #@15
    if-ne v1, v2, :cond_18

    #@17
    .line 369
    :cond_17
    :goto_17
    return v0

    #@18
    .line 366
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 368
    :cond_1b
    const-string v1, "AssistDialPhoneNumberUtils"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "getDialLengthBeforePause(String pNumber) ==> count : "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_17
.end method

.method private getMccForAssistedDial(I)Ljava/lang/String;
    .registers 8
    .parameter "mcc"

    #@0
    .prologue
    .line 189
    const/4 v3, 0x0

    #@1
    .line 190
    .local v3, temp:I
    const/16 v4, 0xa

    #@3
    new-array v1, v4, [I

    #@5
    fill-array-data v1, :array_44

    #@8
    .line 192
    .local v1, conv_arr:[I
    const/16 v4, 0x3ff

    #@a
    if-ne p1, v4, :cond_18

    #@c
    .line 194
    const-string v4, "AssistDialPhoneNumberUtils"

    #@e
    const-string v5, " UnKnown MCC"

    #@10
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 195
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    .line 227
    :goto_17
    return-object v4

    #@18
    .line 198
    :cond_18
    add-int/lit8 v3, p1, 0x6f

    #@1a
    .line 199
    move p1, v3

    #@1b
    .line 200
    move v2, p1

    #@1c
    .line 202
    .local v2, ret_mcc:I
    rem-int/lit8 v4, p1, 0xa

    #@1e
    aget v0, v1, v4

    #@20
    .line 203
    .local v0, check_digit:I
    if-nez v0, :cond_25

    #@22
    .line 205
    add-int/lit8 v2, v3, -0xa

    #@24
    .line 206
    move v3, v2

    #@25
    .line 209
    :cond_25
    div-int/lit8 p1, p1, 0xa

    #@27
    .line 210
    rem-int/lit8 v4, p1, 0xa

    #@29
    aget v0, v1, v4

    #@2b
    .line 211
    if-nez v0, :cond_30

    #@2d
    .line 213
    add-int/lit8 v2, v3, -0x64

    #@2f
    .line 214
    move v3, v2

    #@30
    .line 217
    :cond_30
    div-int/lit8 p1, p1, 0xa

    #@32
    .line 218
    rem-int/lit8 v4, p1, 0xa

    #@34
    aget v0, v1, v4

    #@36
    .line 219
    if-nez v0, :cond_3a

    #@38
    .line 221
    add-int/lit16 v2, v3, -0x3e8

    #@3a
    .line 223
    :cond_3a
    const/16 v4, 0x3e7

    #@3c
    if-le v2, v4, :cond_3f

    #@3e
    .line 225
    const/4 v2, 0x0

    #@3f
    .line 227
    :cond_3f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    goto :goto_17

    #@44
    .line 190
    :array_44
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;
    .registers 15
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 522
    const/4 v10, 0x0

    #@2
    .line 523
    .local v10, mRefCountry:Landroid/provider/ReferenceCountry;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "assist_dial_reference_country"

    #@8
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v12

    #@c
    .line 525
    .local v12, refCountryIndex:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v0

    #@10
    .line 527
    .local v0, mContentResolver:Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "countryindex = "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    .line 530
    .local v3, where:Ljava/lang/String;
    const-string v1, "AssistDialPhoneNumberUtils"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "WHERE : "

    #@2c
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 532
    sget-object v1, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@3d
    sget-object v2, Landroid/provider/Settings$AssistDial;->PROJECTION:[Ljava/lang/String;

    #@3f
    move-object v5, v4

    #@40
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@43
    move-result-object v8

    #@44
    .line 537
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_4f

    #@46
    .line 538
    const-string v1, "AssistDialPhoneNumberUtils"

    #@48
    const-string v2, "cursor is null"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    move-object v11, v10

    #@4e
    .line 604
    .end local v10           #mRefCountry:Landroid/provider/ReferenceCountry;
    .local v11, mRefCountry:Landroid/provider/ReferenceCountry;
    :goto_4e
    return-object v11

    #@4f
    .line 543
    .end local v11           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v10       #mRefCountry:Landroid/provider/ReferenceCountry;
    :cond_4f
    :try_start_4f
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@52
    move-result v1

    #@53
    if-eqz v1, :cond_12d

    #@55
    .line 545
    new-instance v11, Landroid/provider/ReferenceCountry;

    #@57
    invoke-direct {v11}, Landroid/provider/ReferenceCountry;-><init>()V
    :try_end_5a
    .catchall {:try_start_4f .. :try_end_5a} :catchall_13f
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_5a} :catch_135

    #@5a
    .line 547
    .end local v10           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v11       #mRefCountry:Landroid/provider/ReferenceCountry;
    :try_start_5a
    const-string v1, "countryindex"

    #@5c
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@5f
    move-result v7

    #@60
    .line 548
    .local v7, columnIndex:I
    const-string v1, "AssistDialPhoneNumberUtils"

    #@62
    new-instance v2, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v4, " columnIndex : "

    #@69
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 549
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getInt(I)I

    #@7b
    move-result v6

    #@7c
    .line 550
    .local v6, aa:I
    const-string v1, "AssistDialPhoneNumberUtils"

    #@7e
    new-instance v2, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v4, " aa : "

    #@85
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 551
    invoke-virtual {v11, v6}, Landroid/provider/ReferenceCountry;->setIndex(I)V

    #@97
    .line 553
    const-string v1, "countryindex"

    #@99
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@9c
    move-result v1

    #@9d
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    #@a0
    move-result v1

    #@a1
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setIndex(I)V

    #@a4
    .line 557
    const-string v1, "countryname"

    #@a6
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a9
    move-result v1

    #@aa
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setCountryName(Ljava/lang/String;)V

    #@b1
    .line 562
    const-string/jumbo v1, "mcc"

    #@b4
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@b7
    move-result v1

    #@b8
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@bb
    move-result-object v1

    #@bc
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setMccCode(Ljava/lang/String;)V

    #@bf
    .line 565
    const-string v1, "countrycode"

    #@c1
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@c4
    move-result v1

    #@c5
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c8
    move-result-object v1

    #@c9
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setCountryCode(Ljava/lang/String;)V

    #@cc
    .line 570
    const-string v1, "iddprefix"

    #@ce
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@d1
    move-result v1

    #@d2
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setIddPrefix(Ljava/lang/String;)V

    #@d9
    .line 575
    const-string/jumbo v1, "nddprefix"

    #@dc
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@df
    move-result v1

    #@e0
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@e3
    move-result-object v1

    #@e4
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setNddPrefix(Ljava/lang/String;)V

    #@e7
    .line 580
    const-string/jumbo v1, "nanp"

    #@ea
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@ed
    move-result v1

    #@ee
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f1
    move-result-object v1

    #@f2
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setNanp(Ljava/lang/String;)V

    #@f5
    .line 585
    const-string v1, "area"

    #@f7
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@fa
    move-result v1

    #@fb
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setAreaCode(Ljava/lang/String;)V

    #@102
    .line 590
    const-string/jumbo v1, "length"

    #@105
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@108
    move-result v1

    #@109
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@10c
    move-result-object v1

    #@10d
    invoke-virtual {v11, v1}, Landroid/provider/ReferenceCountry;->setNumLength(Ljava/lang/String;)V

    #@110
    .line 595
    const-string v1, "AssistDialPhoneNumberUtils"

    #@112
    new-instance v2, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v4, "getRefCountry() ==> mRefCountry\'s country name : "

    #@119
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v2

    #@11d
    invoke-virtual {v11}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@120
    move-result-object v4

    #@121
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v2

    #@125
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v2

    #@129
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12c
    .catchall {:try_start_5a .. :try_end_12c} :catchall_146
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_12c} :catch_149

    #@12c
    move-object v10, v11

    #@12d
    .line 601
    .end local v6           #aa:I
    .end local v7           #columnIndex:I
    .end local v11           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v10       #mRefCountry:Landroid/provider/ReferenceCountry;
    :cond_12d
    if-eqz v8, :cond_132

    #@12f
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@132
    :cond_132
    :goto_132
    move-object v11, v10

    #@133
    .line 604
    .end local v10           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v11       #mRefCountry:Landroid/provider/ReferenceCountry;
    goto/16 :goto_4e

    #@135
    .line 598
    .end local v11           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v10       #mRefCountry:Landroid/provider/ReferenceCountry;
    :catch_135
    move-exception v9

    #@136
    .line 599
    .local v9, e:Ljava/lang/Exception;
    :goto_136
    :try_start_136
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_139
    .catchall {:try_start_136 .. :try_end_139} :catchall_13f

    #@139
    .line 601
    if-eqz v8, :cond_132

    #@13b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@13e
    goto :goto_132

    #@13f
    .end local v9           #e:Ljava/lang/Exception;
    :catchall_13f
    move-exception v1

    #@140
    :goto_140
    if-eqz v8, :cond_145

    #@142
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@145
    :cond_145
    throw v1

    #@146
    .end local v10           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v11       #mRefCountry:Landroid/provider/ReferenceCountry;
    :catchall_146
    move-exception v1

    #@147
    move-object v10, v11

    #@148
    .end local v11           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v10       #mRefCountry:Landroid/provider/ReferenceCountry;
    goto :goto_140

    #@149
    .line 598
    .end local v10           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v11       #mRefCountry:Landroid/provider/ReferenceCountry;
    :catch_149
    move-exception v9

    #@14a
    move-object v10, v11

    #@14b
    .end local v11           #mRefCountry:Landroid/provider/ReferenceCountry;
    .restart local v10       #mRefCountry:Landroid/provider/ReferenceCountry;
    goto :goto_136
.end method

.method private initAreaCodeInfo()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1210
    iget-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3
    if-nez v0, :cond_f4c

    #@5
    .line 1211
    const/16 v0, 0x12d

    #@7
    new-array v0, v0, [Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9
    const/4 v1, 0x0

    #@a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c
    const-string v3, "201"

    #@e
    const-string v4, "New Jersey"

    #@10
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@13
    aput-object v2, v0, v1

    #@15
    const/4 v1, 0x1

    #@16
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@18
    const-string v3, "203"

    #@1a
    const-string v4, "Connecticut"

    #@1c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1f
    aput-object v2, v0, v1

    #@21
    const/4 v1, 0x2

    #@22
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@24
    const-string v3, "204"

    #@26
    const-string v4, "Manitoba"

    #@28
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2b
    aput-object v2, v0, v1

    #@2d
    const/4 v1, 0x3

    #@2e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@30
    const-string v3, "205"

    #@32
    const-string v4, "Alabama"

    #@34
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@37
    aput-object v2, v0, v1

    #@39
    const/4 v1, 0x4

    #@3a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3c
    const-string v3, "206"

    #@3e
    const-string v4, "Washington"

    #@40
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@43
    aput-object v2, v0, v1

    #@45
    const/4 v1, 0x5

    #@46
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@48
    const-string v3, "207"

    #@4a
    const-string v4, "Maine"

    #@4c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4f
    aput-object v2, v0, v1

    #@51
    const/4 v1, 0x6

    #@52
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@54
    const-string v3, "208"

    #@56
    const-string v4, "Idaho"

    #@58
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5b
    aput-object v2, v0, v1

    #@5d
    const/4 v1, 0x7

    #@5e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@60
    const-string v3, "209"

    #@62
    const-string v4, "California"

    #@64
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@67
    aput-object v2, v0, v1

    #@69
    const/16 v1, 0x8

    #@6b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6d
    const-string v3, "210"

    #@6f
    const-string v4, "Texas"

    #@71
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@74
    aput-object v2, v0, v1

    #@76
    const/16 v1, 0x9

    #@78
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7a
    const-string v3, "212"

    #@7c
    const-string v4, "New York"

    #@7e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@81
    aput-object v2, v0, v1

    #@83
    const/16 v1, 0xa

    #@85
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@87
    const-string v3, "213"

    #@89
    const-string v4, "California"

    #@8b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8e
    aput-object v2, v0, v1

    #@90
    const/16 v1, 0xb

    #@92
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@94
    const-string v3, "214"

    #@96
    const-string v4, "Texas"

    #@98
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9b
    aput-object v2, v0, v1

    #@9d
    const/16 v1, 0xc

    #@9f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a1
    const-string v3, "215"

    #@a3
    const-string v4, "Pennsylvania"

    #@a5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/16 v1, 0xd

    #@ac
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ae
    const-string v3, "216"

    #@b0
    const-string v4, "Ohio"

    #@b2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b5
    aput-object v2, v0, v1

    #@b7
    const/16 v1, 0xe

    #@b9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@bb
    const-string v3, "217"

    #@bd
    const-string v4, "Illinois"

    #@bf
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c2
    aput-object v2, v0, v1

    #@c4
    const/16 v1, 0xf

    #@c6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c8
    const-string v3, "218"

    #@ca
    const-string v4, "Minnesota"

    #@cc
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@cf
    aput-object v2, v0, v1

    #@d1
    const/16 v1, 0x10

    #@d3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d5
    const-string v3, "219"

    #@d7
    const-string v4, "Indiana"

    #@d9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@dc
    aput-object v2, v0, v1

    #@de
    const/16 v1, 0x11

    #@e0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e2
    const-string v3, "224"

    #@e4
    const-string v4, "Illinois"

    #@e6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e9
    aput-object v2, v0, v1

    #@eb
    const/16 v1, 0x12

    #@ed
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ef
    const-string v3, "225"

    #@f1
    const-string v4, "Louisiana"

    #@f3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f6
    aput-object v2, v0, v1

    #@f8
    const/16 v1, 0x13

    #@fa
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@fc
    const-string v3, "226"

    #@fe
    const-string v4, "Ontario"

    #@100
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@103
    aput-object v2, v0, v1

    #@105
    const/16 v1, 0x14

    #@107
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@109
    const-string v3, "228"

    #@10b
    const-string v4, "Mississippi"

    #@10d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@110
    aput-object v2, v0, v1

    #@112
    const/16 v1, 0x15

    #@114
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@116
    const-string v3, "229"

    #@118
    const-string v4, "Georgia"

    #@11a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@11d
    aput-object v2, v0, v1

    #@11f
    const/16 v1, 0x16

    #@121
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@123
    const-string v3, "231"

    #@125
    const-string v4, "Michigan"

    #@127
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@12a
    aput-object v2, v0, v1

    #@12c
    const/16 v1, 0x17

    #@12e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@130
    const-string v3, "234"

    #@132
    const-string v4, "Ohio"

    #@134
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@137
    aput-object v2, v0, v1

    #@139
    const/16 v1, 0x18

    #@13b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@13d
    const-string v3, "239"

    #@13f
    const-string v4, "Florida"

    #@141
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@144
    aput-object v2, v0, v1

    #@146
    const/16 v1, 0x19

    #@148
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@14a
    const-string v3, "240"

    #@14c
    const-string v4, "Maryland"

    #@14e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@151
    aput-object v2, v0, v1

    #@153
    const/16 v1, 0x1a

    #@155
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@157
    const-string v3, "248"

    #@159
    const-string v4, "Michigan"

    #@15b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@15e
    aput-object v2, v0, v1

    #@160
    const/16 v1, 0x1b

    #@162
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@164
    const-string v3, "250"

    #@166
    const-string v4, "British Columbia"

    #@168
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@16b
    aput-object v2, v0, v1

    #@16d
    const/16 v1, 0x1c

    #@16f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@171
    const-string v3, "251"

    #@173
    const-string v4, "Alabama"

    #@175
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@178
    aput-object v2, v0, v1

    #@17a
    const/16 v1, 0x1d

    #@17c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@17e
    const-string v3, "252"

    #@180
    const-string v4, "North Carolina"

    #@182
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@185
    aput-object v2, v0, v1

    #@187
    const/16 v1, 0x1e

    #@189
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@18b
    const-string v3, "253"

    #@18d
    const-string v4, "Washington"

    #@18f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@192
    aput-object v2, v0, v1

    #@194
    const/16 v1, 0x1f

    #@196
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@198
    const-string v3, "254"

    #@19a
    const-string v4, "Texas"

    #@19c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@19f
    aput-object v2, v0, v1

    #@1a1
    const/16 v1, 0x20

    #@1a3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1a5
    const-string v3, "256"

    #@1a7
    const-string v4, "Alabama"

    #@1a9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1ac
    aput-object v2, v0, v1

    #@1ae
    const/16 v1, 0x21

    #@1b0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1b2
    const-string v3, "260"

    #@1b4
    const-string v4, "Indiana"

    #@1b6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1b9
    aput-object v2, v0, v1

    #@1bb
    const/16 v1, 0x22

    #@1bd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1bf
    const-string v3, "262"

    #@1c1
    const-string v4, "Wisconsin"

    #@1c3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1c6
    aput-object v2, v0, v1

    #@1c8
    const/16 v1, 0x23

    #@1ca
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1cc
    const-string v3, "267"

    #@1ce
    const-string v4, "Pennsylvania"

    #@1d0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1d3
    aput-object v2, v0, v1

    #@1d5
    const/16 v1, 0x24

    #@1d7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1d9
    const-string v3, "269"

    #@1db
    const-string v4, "Michigan"

    #@1dd
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1e0
    aput-object v2, v0, v1

    #@1e2
    const/16 v1, 0x25

    #@1e4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1e6
    const-string v3, "270"

    #@1e8
    const-string v4, "Kentucky"

    #@1ea
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1ed
    aput-object v2, v0, v1

    #@1ef
    const/16 v1, 0x26

    #@1f1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@1f3
    const-string v3, "276"

    #@1f5
    const-string v4, "Virginia"

    #@1f7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@1fa
    aput-object v2, v0, v1

    #@1fc
    const/16 v1, 0x27

    #@1fe
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@200
    const-string v3, "281"

    #@202
    const-string v4, "Texas"

    #@204
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@207
    aput-object v2, v0, v1

    #@209
    const/16 v1, 0x28

    #@20b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@20d
    const-string v3, "289"

    #@20f
    const-string v4, "Ontario"

    #@211
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@214
    aput-object v2, v0, v1

    #@216
    const/16 v1, 0x29

    #@218
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@21a
    const-string v3, "301"

    #@21c
    const-string v4, "Maryland"

    #@21e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@221
    aput-object v2, v0, v1

    #@223
    const/16 v1, 0x2a

    #@225
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@227
    const-string v3, "302"

    #@229
    const-string v4, "Delaware"

    #@22b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@22e
    aput-object v2, v0, v1

    #@230
    const/16 v1, 0x2b

    #@232
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@234
    const-string v3, "303"

    #@236
    const-string v4, "Colorado"

    #@238
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@23b
    aput-object v2, v0, v1

    #@23d
    const/16 v1, 0x2c

    #@23f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@241
    const-string v3, "304"

    #@243
    const-string v4, "West Virginia"

    #@245
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@248
    aput-object v2, v0, v1

    #@24a
    const/16 v1, 0x2d

    #@24c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@24e
    const-string v3, "305"

    #@250
    const-string v4, "Florida"

    #@252
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@255
    aput-object v2, v0, v1

    #@257
    const/16 v1, 0x2e

    #@259
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@25b
    const-string v3, "306"

    #@25d
    const-string v4, "Saskatchewan"

    #@25f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@262
    aput-object v2, v0, v1

    #@264
    const/16 v1, 0x2f

    #@266
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@268
    const-string v3, "307"

    #@26a
    const-string v4, "Wyoming"

    #@26c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@26f
    aput-object v2, v0, v1

    #@271
    const/16 v1, 0x30

    #@273
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@275
    const-string v3, "308"

    #@277
    const-string v4, "Nebraska"

    #@279
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@27c
    aput-object v2, v0, v1

    #@27e
    const/16 v1, 0x31

    #@280
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@282
    const-string v3, "309"

    #@284
    const-string v4, "Illinois"

    #@286
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@289
    aput-object v2, v0, v1

    #@28b
    const/16 v1, 0x32

    #@28d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@28f
    const-string v3, "310"

    #@291
    const-string v4, "California"

    #@293
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@296
    aput-object v2, v0, v1

    #@298
    const/16 v1, 0x33

    #@29a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@29c
    const-string v3, "312"

    #@29e
    const-string v4, "Illinois"

    #@2a0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2a3
    aput-object v2, v0, v1

    #@2a5
    const/16 v1, 0x34

    #@2a7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2a9
    const-string v3, "313"

    #@2ab
    const-string v4, "Michigan"

    #@2ad
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2b0
    aput-object v2, v0, v1

    #@2b2
    const/16 v1, 0x35

    #@2b4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2b6
    const-string v3, "314"

    #@2b8
    const-string v4, "Missouri"

    #@2ba
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2bd
    aput-object v2, v0, v1

    #@2bf
    const/16 v1, 0x36

    #@2c1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2c3
    const-string v3, "315"

    #@2c5
    const-string v4, "New York"

    #@2c7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2ca
    aput-object v2, v0, v1

    #@2cc
    const/16 v1, 0x37

    #@2ce
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2d0
    const-string v3, "316"

    #@2d2
    const-string v4, "Kansas"

    #@2d4
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2d7
    aput-object v2, v0, v1

    #@2d9
    const/16 v1, 0x38

    #@2db
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2dd
    const-string v3, "317"

    #@2df
    const-string v4, "Indiana"

    #@2e1
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2e4
    aput-object v2, v0, v1

    #@2e6
    const/16 v1, 0x39

    #@2e8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2ea
    const-string v3, "318"

    #@2ec
    const-string v4, "Louisiana"

    #@2ee
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2f1
    aput-object v2, v0, v1

    #@2f3
    const/16 v1, 0x3a

    #@2f5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@2f7
    const-string v3, "319"

    #@2f9
    const-string v4, "Iowa"

    #@2fb
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@2fe
    aput-object v2, v0, v1

    #@300
    const/16 v1, 0x3b

    #@302
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@304
    const-string v3, "320"

    #@306
    const-string v4, "Minnesota"

    #@308
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@30b
    aput-object v2, v0, v1

    #@30d
    const/16 v1, 0x3c

    #@30f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@311
    const-string v3, "321"

    #@313
    const-string v4, "Florida"

    #@315
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@318
    aput-object v2, v0, v1

    #@31a
    const/16 v1, 0x3d

    #@31c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@31e
    const-string v3, "323"

    #@320
    const-string v4, "California"

    #@322
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@325
    aput-object v2, v0, v1

    #@327
    const/16 v1, 0x3e

    #@329
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@32b
    const-string v3, "325"

    #@32d
    const-string v4, "Texas"

    #@32f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@332
    aput-object v2, v0, v1

    #@334
    const/16 v1, 0x3f

    #@336
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@338
    const-string v3, "330"

    #@33a
    const-string v4, "Ohio"

    #@33c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@33f
    aput-object v2, v0, v1

    #@341
    const/16 v1, 0x40

    #@343
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@345
    const-string v3, "334"

    #@347
    const-string v4, "Alabama"

    #@349
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@34c
    aput-object v2, v0, v1

    #@34e
    const/16 v1, 0x41

    #@350
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@352
    const-string v3, "336"

    #@354
    const-string v4, "North Carolina"

    #@356
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@359
    aput-object v2, v0, v1

    #@35b
    const/16 v1, 0x42

    #@35d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@35f
    const-string v3, "337"

    #@361
    const-string v4, "Louisiana"

    #@363
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@366
    aput-object v2, v0, v1

    #@368
    const/16 v1, 0x43

    #@36a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@36c
    const-string v3, "339"

    #@36e
    const-string v4, "Massachusetts"

    #@370
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@373
    aput-object v2, v0, v1

    #@375
    const/16 v1, 0x44

    #@377
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@379
    const-string v3, "347"

    #@37b
    const-string v4, "New York"

    #@37d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@380
    aput-object v2, v0, v1

    #@382
    const/16 v1, 0x45

    #@384
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@386
    const-string v3, "351"

    #@388
    const-string v4, "Massachusetts"

    #@38a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@38d
    aput-object v2, v0, v1

    #@38f
    const/16 v1, 0x46

    #@391
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@393
    const-string v3, "352"

    #@395
    const-string v4, "Florida"

    #@397
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@39a
    aput-object v2, v0, v1

    #@39c
    const/16 v1, 0x47

    #@39e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3a0
    const-string v3, "360"

    #@3a2
    const-string v4, "Washington"

    #@3a4
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3a7
    aput-object v2, v0, v1

    #@3a9
    const/16 v1, 0x48

    #@3ab
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3ad
    const-string v3, "361"

    #@3af
    const-string v4, "Texas"

    #@3b1
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3b4
    aput-object v2, v0, v1

    #@3b6
    const/16 v1, 0x49

    #@3b8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3ba
    const-string v3, "386"

    #@3bc
    const-string v4, "Florida"

    #@3be
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3c1
    aput-object v2, v0, v1

    #@3c3
    const/16 v1, 0x4a

    #@3c5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3c7
    const-string v3, "401"

    #@3c9
    const-string v4, "Rhode Island"

    #@3cb
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3ce
    aput-object v2, v0, v1

    #@3d0
    const/16 v1, 0x4b

    #@3d2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3d4
    const-string v3, "402"

    #@3d6
    const-string v4, "Nebraska"

    #@3d8
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3db
    aput-object v2, v0, v1

    #@3dd
    const/16 v1, 0x4c

    #@3df
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3e1
    const-string v3, "403"

    #@3e3
    const-string v4, "Alberta"

    #@3e5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3e8
    aput-object v2, v0, v1

    #@3ea
    const/16 v1, 0x4d

    #@3ec
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3ee
    const-string v3, "404"

    #@3f0
    const-string v4, "Georgia"

    #@3f2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@3f5
    aput-object v2, v0, v1

    #@3f7
    const/16 v1, 0x4e

    #@3f9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@3fb
    const-string v3, "405"

    #@3fd
    const-string v4, "Oklahoma"

    #@3ff
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@402
    aput-object v2, v0, v1

    #@404
    const/16 v1, 0x4f

    #@406
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@408
    const-string v3, "406"

    #@40a
    const-string v4, "Montana"

    #@40c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@40f
    aput-object v2, v0, v1

    #@411
    const/16 v1, 0x50

    #@413
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@415
    const-string v3, "407"

    #@417
    const-string v4, "Florida"

    #@419
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@41c
    aput-object v2, v0, v1

    #@41e
    const/16 v1, 0x51

    #@420
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@422
    const-string v3, "408"

    #@424
    const-string v4, "California"

    #@426
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@429
    aput-object v2, v0, v1

    #@42b
    const/16 v1, 0x52

    #@42d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@42f
    const-string v3, "409"

    #@431
    const-string v4, "Texas"

    #@433
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@436
    aput-object v2, v0, v1

    #@438
    const/16 v1, 0x53

    #@43a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@43c
    const-string v3, "410"

    #@43e
    const-string v4, "Maryland"

    #@440
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@443
    aput-object v2, v0, v1

    #@445
    const/16 v1, 0x54

    #@447
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@449
    const-string v3, "412"

    #@44b
    const-string v4, "Pennsylvania"

    #@44d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@450
    aput-object v2, v0, v1

    #@452
    const/16 v1, 0x55

    #@454
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@456
    const-string v3, "413"

    #@458
    const-string v4, "Massachusetts"

    #@45a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@45d
    aput-object v2, v0, v1

    #@45f
    const/16 v1, 0x56

    #@461
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@463
    const-string v3, "414"

    #@465
    const-string v4, "Wisconsin"

    #@467
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@46a
    aput-object v2, v0, v1

    #@46c
    const/16 v1, 0x57

    #@46e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@470
    const-string v3, "415"

    #@472
    const-string v4, "California"

    #@474
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@477
    aput-object v2, v0, v1

    #@479
    const/16 v1, 0x58

    #@47b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@47d
    const-string v3, "416"

    #@47f
    const-string v4, "Ontario"

    #@481
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@484
    aput-object v2, v0, v1

    #@486
    const/16 v1, 0x59

    #@488
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@48a
    const-string v3, "417"

    #@48c
    const-string v4, "Missouri"

    #@48e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@491
    aput-object v2, v0, v1

    #@493
    const/16 v1, 0x5a

    #@495
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@497
    const-string v3, "418"

    #@499
    const-string v4, "Quebec"

    #@49b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@49e
    aput-object v2, v0, v1

    #@4a0
    const/16 v1, 0x5b

    #@4a2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4a4
    const-string v3, "419"

    #@4a6
    const-string v4, "Ohio"

    #@4a8
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4ab
    aput-object v2, v0, v1

    #@4ad
    const/16 v1, 0x5c

    #@4af
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4b1
    const-string v3, "423"

    #@4b3
    const-string v4, "Tennessee"

    #@4b5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4b8
    aput-object v2, v0, v1

    #@4ba
    const/16 v1, 0x5d

    #@4bc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4be
    const-string v3, "424"

    #@4c0
    const-string v4, "California"

    #@4c2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4c5
    aput-object v2, v0, v1

    #@4c7
    const/16 v1, 0x5e

    #@4c9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4cb
    const-string v3, "425"

    #@4cd
    const-string v4, "Washington"

    #@4cf
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4d2
    aput-object v2, v0, v1

    #@4d4
    const/16 v1, 0x5f

    #@4d6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4d8
    const-string v3, "430"

    #@4da
    const-string v4, "Texas"

    #@4dc
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4df
    aput-object v2, v0, v1

    #@4e1
    const/16 v1, 0x60

    #@4e3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4e5
    const-string v3, "432"

    #@4e7
    const-string v4, "Texas"

    #@4e9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4ec
    aput-object v2, v0, v1

    #@4ee
    const/16 v1, 0x61

    #@4f0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4f2
    const-string v3, "434"

    #@4f4
    const-string v4, "Virginia"

    #@4f6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@4f9
    aput-object v2, v0, v1

    #@4fb
    const/16 v1, 0x62

    #@4fd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@4ff
    const-string v3, "435"

    #@501
    const-string v4, "Utah"

    #@503
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@506
    aput-object v2, v0, v1

    #@508
    const/16 v1, 0x63

    #@50a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@50c
    const-string v3, "438"

    #@50e
    const-string v4, "Quebec"

    #@510
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@513
    aput-object v2, v0, v1

    #@515
    const/16 v1, 0x64

    #@517
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@519
    const-string v3, "440"

    #@51b
    const-string v4, "Ohio"

    #@51d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@520
    aput-object v2, v0, v1

    #@522
    const/16 v1, 0x65

    #@524
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@526
    const-string v3, "443"

    #@528
    const-string v4, "Maryland"

    #@52a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@52d
    aput-object v2, v0, v1

    #@52f
    const/16 v1, 0x66

    #@531
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@533
    const-string v3, "450"

    #@535
    const-string v4, "Quebec"

    #@537
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@53a
    aput-object v2, v0, v1

    #@53c
    const/16 v1, 0x67

    #@53e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@540
    const-string v3, "469"

    #@542
    const-string v4, "Texas"

    #@544
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@547
    aput-object v2, v0, v1

    #@549
    const/16 v1, 0x68

    #@54b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@54d
    const-string v3, "478"

    #@54f
    const-string v4, "Georgia"

    #@551
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@554
    aput-object v2, v0, v1

    #@556
    const/16 v1, 0x69

    #@558
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@55a
    const-string v3, "479"

    #@55c
    const-string v4, "Arkansas"

    #@55e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@561
    aput-object v2, v0, v1

    #@563
    const/16 v1, 0x6a

    #@565
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@567
    const-string v3, "480"

    #@569
    const-string v4, "Arizona"

    #@56b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@56e
    aput-object v2, v0, v1

    #@570
    const/16 v1, 0x6b

    #@572
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@574
    const-string v3, "484"

    #@576
    const-string v4, "Pennsylvania"

    #@578
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@57b
    aput-object v2, v0, v1

    #@57d
    const/16 v1, 0x6c

    #@57f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@581
    const-string v3, "501"

    #@583
    const-string v4, "Arkansas"

    #@585
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@588
    aput-object v2, v0, v1

    #@58a
    const/16 v1, 0x6d

    #@58c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@58e
    const-string v3, "502"

    #@590
    const-string v4, "Kentucky"

    #@592
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@595
    aput-object v2, v0, v1

    #@597
    const/16 v1, 0x6e

    #@599
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@59b
    const-string v3, "503"

    #@59d
    const-string v4, "Oregon"

    #@59f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5a2
    aput-object v2, v0, v1

    #@5a4
    const/16 v1, 0x6f

    #@5a6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5a8
    const-string v3, "504"

    #@5aa
    const-string v4, "Louisiana"

    #@5ac
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5af
    aput-object v2, v0, v1

    #@5b1
    const/16 v1, 0x70

    #@5b3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5b5
    const-string v3, "505"

    #@5b7
    const-string v4, "New Mexico"

    #@5b9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5bc
    aput-object v2, v0, v1

    #@5be
    const/16 v1, 0x71

    #@5c0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5c2
    const-string v3, "506"

    #@5c4
    const-string v4, "New Brunswick"

    #@5c6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5c9
    aput-object v2, v0, v1

    #@5cb
    const/16 v1, 0x72

    #@5cd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5cf
    const-string v3, "507"

    #@5d1
    const-string v4, "Minnesota"

    #@5d3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5d6
    aput-object v2, v0, v1

    #@5d8
    const/16 v1, 0x73

    #@5da
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5dc
    const-string v3, "508"

    #@5de
    const-string v4, "Massachusetts"

    #@5e0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5e3
    aput-object v2, v0, v1

    #@5e5
    const/16 v1, 0x74

    #@5e7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5e9
    const-string v3, "509"

    #@5eb
    const-string v4, "Washington"

    #@5ed
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5f0
    aput-object v2, v0, v1

    #@5f2
    const/16 v1, 0x75

    #@5f4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@5f6
    const-string v3, "510"

    #@5f8
    const-string v4, "California"

    #@5fa
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@5fd
    aput-object v2, v0, v1

    #@5ff
    const/16 v1, 0x76

    #@601
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@603
    const-string v3, "512"

    #@605
    const-string v4, "Texas"

    #@607
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@60a
    aput-object v2, v0, v1

    #@60c
    const/16 v1, 0x77

    #@60e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@610
    const-string v3, "513"

    #@612
    const-string v4, "Ohio"

    #@614
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@617
    aput-object v2, v0, v1

    #@619
    const/16 v1, 0x78

    #@61b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@61d
    const-string v3, "514"

    #@61f
    const-string v4, "Quebec"

    #@621
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@624
    aput-object v2, v0, v1

    #@626
    const/16 v1, 0x79

    #@628
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@62a
    const-string v3, "515"

    #@62c
    const-string v4, "Iowa"

    #@62e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@631
    aput-object v2, v0, v1

    #@633
    const/16 v1, 0x7a

    #@635
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@637
    const-string v3, "516"

    #@639
    const-string v4, "New York"

    #@63b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@63e
    aput-object v2, v0, v1

    #@640
    const/16 v1, 0x7b

    #@642
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@644
    const-string v3, "517"

    #@646
    const-string v4, "Michigan"

    #@648
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@64b
    aput-object v2, v0, v1

    #@64d
    const/16 v1, 0x7c

    #@64f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@651
    const-string v3, "518"

    #@653
    const-string v4, "New York"

    #@655
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@658
    aput-object v2, v0, v1

    #@65a
    const/16 v1, 0x7d

    #@65c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@65e
    const-string v3, "519"

    #@660
    const-string v4, "Ontario"

    #@662
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@665
    aput-object v2, v0, v1

    #@667
    const/16 v1, 0x7e

    #@669
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@66b
    const-string v3, "520"

    #@66d
    const-string v4, "Arizona"

    #@66f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@672
    aput-object v2, v0, v1

    #@674
    const/16 v1, 0x7f

    #@676
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@678
    const-string v3, "530"

    #@67a
    const-string v4, "California"

    #@67c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@67f
    aput-object v2, v0, v1

    #@681
    const/16 v1, 0x80

    #@683
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@685
    const-string v3, "540"

    #@687
    const-string v4, "Virginia"

    #@689
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@68c
    aput-object v2, v0, v1

    #@68e
    const/16 v1, 0x81

    #@690
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@692
    const-string v3, "541"

    #@694
    const-string v4, "Oregon"

    #@696
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@699
    aput-object v2, v0, v1

    #@69b
    const/16 v1, 0x82

    #@69d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@69f
    const-string v3, "551"

    #@6a1
    const-string v4, "New Jersey"

    #@6a3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6a6
    aput-object v2, v0, v1

    #@6a8
    const/16 v1, 0x83

    #@6aa
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6ac
    const-string v3, "559"

    #@6ae
    const-string v4, "California"

    #@6b0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6b3
    aput-object v2, v0, v1

    #@6b5
    const/16 v1, 0x84

    #@6b7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6b9
    const-string v3, "561"

    #@6bb
    const-string v4, "Florida"

    #@6bd
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6c0
    aput-object v2, v0, v1

    #@6c2
    const/16 v1, 0x85

    #@6c4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6c6
    const-string v3, "562"

    #@6c8
    const-string v4, "California"

    #@6ca
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6cd
    aput-object v2, v0, v1

    #@6cf
    const/16 v1, 0x86

    #@6d1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6d3
    const-string v3, "563"

    #@6d5
    const-string v4, "Iowa"

    #@6d7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6da
    aput-object v2, v0, v1

    #@6dc
    const/16 v1, 0x87

    #@6de
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6e0
    const-string v3, "567"

    #@6e2
    const-string v4, "Ohio"

    #@6e4
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6e7
    aput-object v2, v0, v1

    #@6e9
    const/16 v1, 0x88

    #@6eb
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6ed
    const-string v3, "570"

    #@6ef
    const-string v4, "Pennsylvania"

    #@6f1
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@6f4
    aput-object v2, v0, v1

    #@6f6
    const/16 v1, 0x89

    #@6f8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@6fa
    const-string v3, "571"

    #@6fc
    const-string v4, "Virginia"

    #@6fe
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@701
    aput-object v2, v0, v1

    #@703
    const/16 v1, 0x8a

    #@705
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@707
    const-string v3, "573"

    #@709
    const-string v4, "Missouri"

    #@70b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@70e
    aput-object v2, v0, v1

    #@710
    const/16 v1, 0x8b

    #@712
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@714
    const-string v3, "574"

    #@716
    const-string v4, "Indiana"

    #@718
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@71b
    aput-object v2, v0, v1

    #@71d
    const/16 v1, 0x8c

    #@71f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@721
    const-string v3, "575"

    #@723
    const-string v4, "New Mexico"

    #@725
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@728
    aput-object v2, v0, v1

    #@72a
    const/16 v1, 0x8d

    #@72c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@72e
    const-string v3, "580"

    #@730
    const-string v4, "Oklahoma"

    #@732
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@735
    aput-object v2, v0, v1

    #@737
    const/16 v1, 0x8e

    #@739
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@73b
    const-string v3, "585"

    #@73d
    const-string v4, "New York"

    #@73f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@742
    aput-object v2, v0, v1

    #@744
    const/16 v1, 0x8f

    #@746
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@748
    const-string v3, "586"

    #@74a
    const-string v4, "Michigan"

    #@74c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@74f
    aput-object v2, v0, v1

    #@751
    const/16 v1, 0x90

    #@753
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@755
    const-string v3, "601"

    #@757
    const-string v4, "Mississippi"

    #@759
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@75c
    aput-object v2, v0, v1

    #@75e
    const/16 v1, 0x91

    #@760
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@762
    const-string v3, "602"

    #@764
    const-string v4, "Arizona"

    #@766
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@769
    aput-object v2, v0, v1

    #@76b
    const/16 v1, 0x92

    #@76d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@76f
    const-string v3, "603"

    #@771
    const-string v4, "New Hampshire"

    #@773
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@776
    aput-object v2, v0, v1

    #@778
    const/16 v1, 0x93

    #@77a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@77c
    const-string v3, "604"

    #@77e
    const-string v4, "British Columbia"

    #@780
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@783
    aput-object v2, v0, v1

    #@785
    const/16 v1, 0x94

    #@787
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@789
    const-string v3, "605"

    #@78b
    const-string v4, "South Dakota"

    #@78d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@790
    aput-object v2, v0, v1

    #@792
    const/16 v1, 0x95

    #@794
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@796
    const-string v3, "606"

    #@798
    const-string v4, "Kentucky"

    #@79a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@79d
    aput-object v2, v0, v1

    #@79f
    const/16 v1, 0x96

    #@7a1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7a3
    const-string v3, "607"

    #@7a5
    const-string v4, "New York"

    #@7a7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7aa
    aput-object v2, v0, v1

    #@7ac
    const/16 v1, 0x97

    #@7ae
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7b0
    const-string v3, "608"

    #@7b2
    const-string v4, "Wisconsin"

    #@7b4
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7b7
    aput-object v2, v0, v1

    #@7b9
    const/16 v1, 0x98

    #@7bb
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7bd
    const-string v3, "609"

    #@7bf
    const-string v4, "New Jersey"

    #@7c1
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7c4
    aput-object v2, v0, v1

    #@7c6
    const/16 v1, 0x99

    #@7c8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7ca
    const-string v3, "610"

    #@7cc
    const-string v4, "Pennsylvania"

    #@7ce
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7d1
    aput-object v2, v0, v1

    #@7d3
    const/16 v1, 0x9a

    #@7d5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7d7
    const-string v3, "612"

    #@7d9
    const-string v4, "Minnesota"

    #@7db
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7de
    aput-object v2, v0, v1

    #@7e0
    const/16 v1, 0x9b

    #@7e2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7e4
    const-string v3, "613"

    #@7e6
    const-string v4, "Ontario"

    #@7e8
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7eb
    aput-object v2, v0, v1

    #@7ed
    const/16 v1, 0x9c

    #@7ef
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7f1
    const-string v3, "614"

    #@7f3
    const-string v4, "Ohio"

    #@7f5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@7f8
    aput-object v2, v0, v1

    #@7fa
    const/16 v1, 0x9d

    #@7fc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@7fe
    const-string v3, "615"

    #@800
    const-string v4, "Tennessee"

    #@802
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@805
    aput-object v2, v0, v1

    #@807
    const/16 v1, 0x9e

    #@809
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@80b
    const-string v3, "616"

    #@80d
    const-string v4, "Michigan"

    #@80f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@812
    aput-object v2, v0, v1

    #@814
    const/16 v1, 0x9f

    #@816
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@818
    const-string v3, "617"

    #@81a
    const-string v4, "Massachusetts"

    #@81c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@81f
    aput-object v2, v0, v1

    #@821
    const/16 v1, 0xa0

    #@823
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@825
    const-string v3, "618"

    #@827
    const-string v4, "Illinois"

    #@829
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@82c
    aput-object v2, v0, v1

    #@82e
    const/16 v1, 0xa1

    #@830
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@832
    const-string v3, "619"

    #@834
    const-string v4, "California"

    #@836
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@839
    aput-object v2, v0, v1

    #@83b
    const/16 v1, 0xa2

    #@83d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@83f
    const-string v3, "620"

    #@841
    const-string v4, "Kansas"

    #@843
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@846
    aput-object v2, v0, v1

    #@848
    const/16 v1, 0xa3

    #@84a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@84c
    const-string v3, "623"

    #@84e
    const-string v4, "Arizona"

    #@850
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@853
    aput-object v2, v0, v1

    #@855
    const/16 v1, 0xa4

    #@857
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@859
    const-string v3, "626"

    #@85b
    const-string v4, "California"

    #@85d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@860
    aput-object v2, v0, v1

    #@862
    const/16 v1, 0xa5

    #@864
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@866
    const-string v3, "630"

    #@868
    const-string v4, "Illinois"

    #@86a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@86d
    aput-object v2, v0, v1

    #@86f
    const/16 v1, 0xa6

    #@871
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@873
    const-string v3, "631"

    #@875
    const-string v4, "New York"

    #@877
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@87a
    aput-object v2, v0, v1

    #@87c
    const/16 v1, 0xa7

    #@87e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@880
    const-string v3, "636"

    #@882
    const-string v4, "Missouri"

    #@884
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@887
    aput-object v2, v0, v1

    #@889
    const/16 v1, 0xa8

    #@88b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@88d
    const-string v3, "641"

    #@88f
    const-string v4, "Iowa"

    #@891
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@894
    aput-object v2, v0, v1

    #@896
    const/16 v1, 0xa9

    #@898
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@89a
    const-string v3, "646"

    #@89c
    const-string v4, "New York"

    #@89e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8a1
    aput-object v2, v0, v1

    #@8a3
    const/16 v1, 0xaa

    #@8a5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8a7
    const-string v3, "647"

    #@8a9
    const-string v4, "Ontario"

    #@8ab
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8ae
    aput-object v2, v0, v1

    #@8b0
    const/16 v1, 0xab

    #@8b2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8b4
    const-string v3, "650"

    #@8b6
    const-string v4, "California"

    #@8b8
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8bb
    aput-object v2, v0, v1

    #@8bd
    const/16 v1, 0xac

    #@8bf
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8c1
    const-string v3, "651"

    #@8c3
    const-string v4, "Minnesota"

    #@8c5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8c8
    aput-object v2, v0, v1

    #@8ca
    const/16 v1, 0xad

    #@8cc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8ce
    const-string v3, "660"

    #@8d0
    const-string v4, "Missouri"

    #@8d2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8d5
    aput-object v2, v0, v1

    #@8d7
    const/16 v1, 0xae

    #@8d9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8db
    const-string v3, "661"

    #@8dd
    const-string v4, "California"

    #@8df
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8e2
    aput-object v2, v0, v1

    #@8e4
    const/16 v1, 0xaf

    #@8e6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8e8
    const-string v3, "662"

    #@8ea
    const-string v4, "Mississippi"

    #@8ec
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8ef
    aput-object v2, v0, v1

    #@8f1
    const/16 v1, 0xb0

    #@8f3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@8f5
    const-string v3, "678"

    #@8f7
    const-string v4, "Georgia"

    #@8f9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@8fc
    aput-object v2, v0, v1

    #@8fe
    const/16 v1, 0xb1

    #@900
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@902
    const-string v3, "682"

    #@904
    const-string v4, "Texas"

    #@906
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@909
    aput-object v2, v0, v1

    #@90b
    const/16 v1, 0xb2

    #@90d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@90f
    const-string v3, "701"

    #@911
    const-string v4, "North Dakota"

    #@913
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@916
    aput-object v2, v0, v1

    #@918
    const/16 v1, 0xb3

    #@91a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@91c
    const-string v3, "702"

    #@91e
    const-string v4, "Nevada"

    #@920
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@923
    aput-object v2, v0, v1

    #@925
    const/16 v1, 0xb4

    #@927
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@929
    const-string v3, "703"

    #@92b
    const-string v4, "Virginia"

    #@92d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@930
    aput-object v2, v0, v1

    #@932
    const/16 v1, 0xb5

    #@934
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@936
    const-string v3, "704"

    #@938
    const-string v4, "North Carolina"

    #@93a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@93d
    aput-object v2, v0, v1

    #@93f
    const/16 v1, 0xb6

    #@941
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@943
    const-string v3, "705"

    #@945
    const-string v4, "Ontario"

    #@947
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@94a
    aput-object v2, v0, v1

    #@94c
    const/16 v1, 0xb7

    #@94e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@950
    const-string v3, "706"

    #@952
    const-string v4, "Georgia"

    #@954
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@957
    aput-object v2, v0, v1

    #@959
    const/16 v1, 0xb8

    #@95b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@95d
    const-string v3, "707"

    #@95f
    const-string v4, "California"

    #@961
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@964
    aput-object v2, v0, v1

    #@966
    const/16 v1, 0xb9

    #@968
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@96a
    const-string v3, "708"

    #@96c
    const-string v4, "Illinois"

    #@96e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@971
    aput-object v2, v0, v1

    #@973
    const/16 v1, 0xba

    #@975
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@977
    const-string v3, "709"

    #@979
    const-string v4, "Newfoundland"

    #@97b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@97e
    aput-object v2, v0, v1

    #@980
    const/16 v1, 0xbb

    #@982
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@984
    const-string v3, "712"

    #@986
    const-string v4, "Iowa"

    #@988
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@98b
    aput-object v2, v0, v1

    #@98d
    const/16 v1, 0xbc

    #@98f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@991
    const-string v3, "713"

    #@993
    const-string v4, "Texas"

    #@995
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@998
    aput-object v2, v0, v1

    #@99a
    const/16 v1, 0xbd

    #@99c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@99e
    const-string v3, "714"

    #@9a0
    const-string v4, "California"

    #@9a2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9a5
    aput-object v2, v0, v1

    #@9a7
    const/16 v1, 0xbe

    #@9a9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9ab
    const-string v3, "715"

    #@9ad
    const-string v4, "Wisconsin"

    #@9af
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9b2
    aput-object v2, v0, v1

    #@9b4
    const/16 v1, 0xbf

    #@9b6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9b8
    const-string v3, "716"

    #@9ba
    const-string v4, "New York"

    #@9bc
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9bf
    aput-object v2, v0, v1

    #@9c1
    const/16 v1, 0xc0

    #@9c3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9c5
    const-string v3, "717"

    #@9c7
    const-string v4, "Pennsylvania"

    #@9c9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9cc
    aput-object v2, v0, v1

    #@9ce
    const/16 v1, 0xc1

    #@9d0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9d2
    const-string v3, "718"

    #@9d4
    const-string v4, "New York"

    #@9d6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9d9
    aput-object v2, v0, v1

    #@9db
    const/16 v1, 0xc2

    #@9dd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9df
    const-string v3, "719"

    #@9e1
    const-string v4, "Colorado"

    #@9e3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9e6
    aput-object v2, v0, v1

    #@9e8
    const/16 v1, 0xc3

    #@9ea
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9ec
    const-string v3, "720"

    #@9ee
    const-string v4, "Colorado"

    #@9f0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@9f3
    aput-object v2, v0, v1

    #@9f5
    const/16 v1, 0xc4

    #@9f7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@9f9
    const-string v3, "724"

    #@9fb
    const-string v4, "Pennsylvania"

    #@9fd
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a00
    aput-object v2, v0, v1

    #@a02
    const/16 v1, 0xc5

    #@a04
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a06
    const-string v3, "727"

    #@a08
    const-string v4, "Florida"

    #@a0a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a0d
    aput-object v2, v0, v1

    #@a0f
    const/16 v1, 0xc6

    #@a11
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a13
    const-string v3, "731"

    #@a15
    const-string v4, "Tennessee"

    #@a17
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a1a
    aput-object v2, v0, v1

    #@a1c
    const/16 v1, 0xc7

    #@a1e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a20
    const-string v3, "732"

    #@a22
    const-string v4, "New Jersey"

    #@a24
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a27
    aput-object v2, v0, v1

    #@a29
    const/16 v1, 0xc8

    #@a2b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a2d
    const-string v3, "734"

    #@a2f
    const-string v4, "Michigan"

    #@a31
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a34
    aput-object v2, v0, v1

    #@a36
    const/16 v1, 0xc9

    #@a38
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a3a
    const-string v3, "740"

    #@a3c
    const-string v4, "Ohio"

    #@a3e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a41
    aput-object v2, v0, v1

    #@a43
    const/16 v1, 0xca

    #@a45
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a47
    const-string v3, "754"

    #@a49
    const-string v4, "Florida"

    #@a4b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a4e
    aput-object v2, v0, v1

    #@a50
    const/16 v1, 0xcb

    #@a52
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a54
    const-string v3, "760"

    #@a56
    const-string v4, "California"

    #@a58
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a5b
    aput-object v2, v0, v1

    #@a5d
    const/16 v1, 0xcc

    #@a5f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a61
    const-string v3, "763"

    #@a63
    const-string v4, "Minnesota"

    #@a65
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a68
    aput-object v2, v0, v1

    #@a6a
    const/16 v1, 0xcd

    #@a6c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a6e
    const-string v3, "765"

    #@a70
    const-string v4, "Indiana"

    #@a72
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a75
    aput-object v2, v0, v1

    #@a77
    const/16 v1, 0xce

    #@a79
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a7b
    const-string v3, "767"

    #@a7d
    const-string v4, "Dominica"

    #@a7f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a82
    aput-object v2, v0, v1

    #@a84
    const/16 v1, 0xcf

    #@a86
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a88
    const-string v3, "769"

    #@a8a
    const-string v4, "Mississippi"

    #@a8c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a8f
    aput-object v2, v0, v1

    #@a91
    const/16 v1, 0xd0

    #@a93
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@a95
    const-string v3, "770"

    #@a97
    const-string v4, "Georgia"

    #@a99
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@a9c
    aput-object v2, v0, v1

    #@a9e
    const/16 v1, 0xd1

    #@aa0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@aa2
    const-string v3, "772"

    #@aa4
    const-string v4, "Florida"

    #@aa6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@aa9
    aput-object v2, v0, v1

    #@aab
    const/16 v1, 0xd2

    #@aad
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@aaf
    const-string v3, "773"

    #@ab1
    const-string v4, "Illinois"

    #@ab3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ab6
    aput-object v2, v0, v1

    #@ab8
    const/16 v1, 0xd3

    #@aba
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@abc
    const-string v3, "774"

    #@abe
    const-string v4, "Massachusetts"

    #@ac0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ac3
    aput-object v2, v0, v1

    #@ac5
    const/16 v1, 0xd4

    #@ac7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ac9
    const-string v3, "775"

    #@acb
    const-string v4, "Nevada"

    #@acd
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ad0
    aput-object v2, v0, v1

    #@ad2
    const/16 v1, 0xd5

    #@ad4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ad6
    const-string v3, "778"

    #@ad8
    const-string v4, "British Columbia"

    #@ada
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@add
    aput-object v2, v0, v1

    #@adf
    const/16 v1, 0xd6

    #@ae1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ae3
    const-string v3, "780"

    #@ae5
    const-string v4, "Alberta"

    #@ae7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@aea
    aput-object v2, v0, v1

    #@aec
    const/16 v1, 0xd7

    #@aee
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@af0
    const-string v3, "781"

    #@af2
    const-string v4, "Massachusetts"

    #@af4
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@af7
    aput-object v2, v0, v1

    #@af9
    const/16 v1, 0xd8

    #@afb
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@afd
    const-string v3, "785"

    #@aff
    const-string v4, "Kansas"

    #@b01
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b04
    aput-object v2, v0, v1

    #@b06
    const/16 v1, 0xd9

    #@b08
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b0a
    const-string v3, "786"

    #@b0c
    const-string v4, "Florida"

    #@b0e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b11
    aput-object v2, v0, v1

    #@b13
    const/16 v1, 0xda

    #@b15
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b17
    const-string v3, "800"

    #@b19
    const-string v4, "Toll Free Numbers"

    #@b1b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b1e
    aput-object v2, v0, v1

    #@b20
    const/16 v1, 0xdb

    #@b22
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b24
    const-string v3, "801"

    #@b26
    const-string v4, "Utah"

    #@b28
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b2b
    aput-object v2, v0, v1

    #@b2d
    const/16 v1, 0xdc

    #@b2f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b31
    const-string v3, "802"

    #@b33
    const-string v4, "Vermont"

    #@b35
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b38
    aput-object v2, v0, v1

    #@b3a
    const/16 v1, 0xdd

    #@b3c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b3e
    const-string v3, "803"

    #@b40
    const-string v4, "South Carolina"

    #@b42
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b45
    aput-object v2, v0, v1

    #@b47
    const/16 v1, 0xde

    #@b49
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b4b
    const-string v3, "804"

    #@b4d
    const-string v4, "Virginia"

    #@b4f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b52
    aput-object v2, v0, v1

    #@b54
    const/16 v1, 0xdf

    #@b56
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b58
    const-string v3, "805"

    #@b5a
    const-string v4, "California"

    #@b5c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b5f
    aput-object v2, v0, v1

    #@b61
    const/16 v1, 0xe0

    #@b63
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b65
    const-string v3, "806"

    #@b67
    const-string v4, "Texas"

    #@b69
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b6c
    aput-object v2, v0, v1

    #@b6e
    const/16 v1, 0xe1

    #@b70
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b72
    const-string v3, "807"

    #@b74
    const-string v4, "Ontario"

    #@b76
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b79
    aput-object v2, v0, v1

    #@b7b
    const/16 v1, 0xe2

    #@b7d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b7f
    const-string v3, "808"

    #@b81
    const-string v4, "Hawaii"

    #@b83
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b86
    aput-object v2, v0, v1

    #@b88
    const/16 v1, 0xe3

    #@b8a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b8c
    const-string v3, "809"

    #@b8e
    const-string v4, "Dominican Republic"

    #@b90
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@b93
    aput-object v2, v0, v1

    #@b95
    const/16 v1, 0xe4

    #@b97
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@b99
    const-string v3, "812"

    #@b9b
    const-string v4, "Indiana"

    #@b9d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ba0
    aput-object v2, v0, v1

    #@ba2
    const/16 v1, 0xe5

    #@ba4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ba6
    const-string v3, "813"

    #@ba8
    const-string v4, "Florida"

    #@baa
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@bad
    aput-object v2, v0, v1

    #@baf
    const/16 v1, 0xe6

    #@bb1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@bb3
    const-string v3, "814"

    #@bb5
    const-string v4, "Pennsylvania"

    #@bb7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@bba
    aput-object v2, v0, v1

    #@bbc
    const/16 v1, 0xe7

    #@bbe
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@bc0
    const-string v3, "815"

    #@bc2
    const-string v4, "Illinois"

    #@bc4
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@bc7
    aput-object v2, v0, v1

    #@bc9
    const/16 v1, 0xe8

    #@bcb
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@bcd
    const-string v3, "816"

    #@bcf
    const-string v4, "Missouri"

    #@bd1
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@bd4
    aput-object v2, v0, v1

    #@bd6
    const/16 v1, 0xe9

    #@bd8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@bda
    const-string v3, "817"

    #@bdc
    const-string v4, "Texas"

    #@bde
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@be1
    aput-object v2, v0, v1

    #@be3
    const/16 v1, 0xea

    #@be5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@be7
    const-string v3, "818"

    #@be9
    const-string v4, "California"

    #@beb
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@bee
    aput-object v2, v0, v1

    #@bf0
    const/16 v1, 0xeb

    #@bf2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@bf4
    const-string v3, "819"

    #@bf6
    const-string v4, "Quebec"

    #@bf8
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@bfb
    aput-object v2, v0, v1

    #@bfd
    const/16 v1, 0xec

    #@bff
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c01
    const-string v3, "828"

    #@c03
    const-string v4, "North Carolina"

    #@c05
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c08
    aput-object v2, v0, v1

    #@c0a
    const/16 v1, 0xed

    #@c0c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c0e
    const-string v3, "830"

    #@c10
    const-string v4, "Texas"

    #@c12
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c15
    aput-object v2, v0, v1

    #@c17
    const/16 v1, 0xee

    #@c19
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c1b
    const-string v3, "831"

    #@c1d
    const-string v4, "California"

    #@c1f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c22
    aput-object v2, v0, v1

    #@c24
    const/16 v1, 0xef

    #@c26
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c28
    const-string v3, "832"

    #@c2a
    const-string v4, "Texas"

    #@c2c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c2f
    aput-object v2, v0, v1

    #@c31
    const/16 v1, 0xf0

    #@c33
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c35
    const-string v3, "843"

    #@c37
    const-string v4, "South Carolina"

    #@c39
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c3c
    aput-object v2, v0, v1

    #@c3e
    const/16 v1, 0xf1

    #@c40
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c42
    const-string v3, "845"

    #@c44
    const-string v4, "New York"

    #@c46
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c49
    aput-object v2, v0, v1

    #@c4b
    const/16 v1, 0xf2

    #@c4d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c4f
    const-string v3, "847"

    #@c51
    const-string v4, "Illinois"

    #@c53
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c56
    aput-object v2, v0, v1

    #@c58
    const/16 v1, 0xf3

    #@c5a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c5c
    const-string v3, "848"

    #@c5e
    const-string v4, "New Jersey"

    #@c60
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c63
    aput-object v2, v0, v1

    #@c65
    const/16 v1, 0xf4

    #@c67
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c69
    const-string v3, "850"

    #@c6b
    const-string v4, "Florida"

    #@c6d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c70
    aput-object v2, v0, v1

    #@c72
    const/16 v1, 0xf5

    #@c74
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c76
    const-string v3, "856"

    #@c78
    const-string v4, "New Jersey"

    #@c7a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c7d
    aput-object v2, v0, v1

    #@c7f
    const/16 v1, 0xf6

    #@c81
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c83
    const-string v3, "857"

    #@c85
    const-string v4, "Massachusetts"

    #@c87
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c8a
    aput-object v2, v0, v1

    #@c8c
    const/16 v1, 0xf7

    #@c8e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c90
    const-string v3, "858"

    #@c92
    const-string v4, "California"

    #@c94
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@c97
    aput-object v2, v0, v1

    #@c99
    const/16 v1, 0xf8

    #@c9b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@c9d
    const-string v3, "859"

    #@c9f
    const-string v4, "Kentucky"

    #@ca1
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ca4
    aput-object v2, v0, v1

    #@ca6
    const/16 v1, 0xf9

    #@ca8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@caa
    const-string v3, "860"

    #@cac
    const-string v4, "Connecticut"

    #@cae
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@cb1
    aput-object v2, v0, v1

    #@cb3
    const/16 v1, 0xfa

    #@cb5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@cb7
    const-string v3, "861"

    #@cb9
    const-string v4, "Florida"

    #@cbb
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@cbe
    aput-object v2, v0, v1

    #@cc0
    const/16 v1, 0xfb

    #@cc2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@cc4
    const-string v3, "863"

    #@cc6
    const-string v4, "Florida"

    #@cc8
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ccb
    aput-object v2, v0, v1

    #@ccd
    const/16 v1, 0xfc

    #@ccf
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@cd1
    const-string v3, "864"

    #@cd3
    const-string v4, "South Carolina"

    #@cd5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@cd8
    aput-object v2, v0, v1

    #@cda
    const/16 v1, 0xfd

    #@cdc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@cde
    const-string v3, "865"

    #@ce0
    const-string v4, "Tennessee"

    #@ce2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ce5
    aput-object v2, v0, v1

    #@ce7
    const/16 v1, 0xfe

    #@ce9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ceb
    const-string v3, "866"

    #@ced
    const-string v4, "Toll Free Numbers"

    #@cef
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@cf2
    aput-object v2, v0, v1

    #@cf4
    const/16 v1, 0xff

    #@cf6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@cf8
    const-string v3, "867"

    #@cfa
    const-string v4, "Yukon, NW Terr., Nunavut"

    #@cfc
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@cff
    aput-object v2, v0, v1

    #@d01
    const/16 v1, 0x100

    #@d03
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d05
    const-string v3, "870"

    #@d07
    const-string v4, "Arkansas"

    #@d09
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d0c
    aput-object v2, v0, v1

    #@d0e
    const/16 v1, 0x101

    #@d10
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d12
    const-string v3, "877"

    #@d14
    const-string v4, "Toll Free Numbers"

    #@d16
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d19
    aput-object v2, v0, v1

    #@d1b
    const/16 v1, 0x102

    #@d1d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d1f
    const-string v3, "878"

    #@d21
    const-string v4, "Pennsylvania"

    #@d23
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d26
    aput-object v2, v0, v1

    #@d28
    const/16 v1, 0x103

    #@d2a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d2c
    const-string v3, "888"

    #@d2e
    const-string v4, "Toll Free Numbers"

    #@d30
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d33
    aput-object v2, v0, v1

    #@d35
    const/16 v1, 0x104

    #@d37
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d39
    const-string v3, "901"

    #@d3b
    const-string v4, "Tennessee"

    #@d3d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d40
    aput-object v2, v0, v1

    #@d42
    const/16 v1, 0x105

    #@d44
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d46
    const-string v3, "902"

    #@d48
    const-string v4, "Nova Scotia"

    #@d4a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d4d
    aput-object v2, v0, v1

    #@d4f
    const/16 v1, 0x106

    #@d51
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d53
    const-string v3, "903"

    #@d55
    const-string v4, "Texas"

    #@d57
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d5a
    aput-object v2, v0, v1

    #@d5c
    const/16 v1, 0x107

    #@d5e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d60
    const-string v3, "904"

    #@d62
    const-string v4, "Florida"

    #@d64
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d67
    aput-object v2, v0, v1

    #@d69
    const/16 v1, 0x108

    #@d6b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d6d
    const-string v3, "905"

    #@d6f
    const-string v4, "Ontario"

    #@d71
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d74
    aput-object v2, v0, v1

    #@d76
    const/16 v1, 0x109

    #@d78
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d7a
    const-string v3, "906"

    #@d7c
    const-string v4, "Michigan"

    #@d7e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d81
    aput-object v2, v0, v1

    #@d83
    const/16 v1, 0x10a

    #@d85
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d87
    const-string v3, "907"

    #@d89
    const-string v4, "Alaska"

    #@d8b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d8e
    aput-object v2, v0, v1

    #@d90
    const/16 v1, 0x10b

    #@d92
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@d94
    const-string v3, "908"

    #@d96
    const-string v4, "New Jersey"

    #@d98
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@d9b
    aput-object v2, v0, v1

    #@d9d
    const/16 v1, 0x10c

    #@d9f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@da1
    const-string v3, "909"

    #@da3
    const-string v4, "California"

    #@da5
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@da8
    aput-object v2, v0, v1

    #@daa
    const/16 v1, 0x10d

    #@dac
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@dae
    const-string v3, "910"

    #@db0
    const-string v4, "North Carolina"

    #@db2
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@db5
    aput-object v2, v0, v1

    #@db7
    const/16 v1, 0x10e

    #@db9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@dbb
    const-string v3, "912"

    #@dbd
    const-string v4, "Georgia"

    #@dbf
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@dc2
    aput-object v2, v0, v1

    #@dc4
    const/16 v1, 0x10f

    #@dc6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@dc8
    const-string v3, "913"

    #@dca
    const-string v4, "Kansas"

    #@dcc
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@dcf
    aput-object v2, v0, v1

    #@dd1
    const/16 v1, 0x110

    #@dd3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@dd5
    const-string v3, "914"

    #@dd7
    const-string v4, "New York"

    #@dd9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ddc
    aput-object v2, v0, v1

    #@dde
    const/16 v1, 0x111

    #@de0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@de2
    const-string v3, "915"

    #@de4
    const-string v4, "Texas"

    #@de6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@de9
    aput-object v2, v0, v1

    #@deb
    const/16 v1, 0x112

    #@ded
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@def
    const-string v3, "916"

    #@df1
    const-string v4, "California"

    #@df3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@df6
    aput-object v2, v0, v1

    #@df8
    const/16 v1, 0x113

    #@dfa
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@dfc
    const-string v3, "917"

    #@dfe
    const-string v4, "New York"

    #@e00
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e03
    aput-object v2, v0, v1

    #@e05
    const/16 v1, 0x114

    #@e07
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e09
    const-string v3, "918"

    #@e0b
    const-string v4, "Oklahoma"

    #@e0d
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e10
    aput-object v2, v0, v1

    #@e12
    const/16 v1, 0x115

    #@e14
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e16
    const-string v3, "919"

    #@e18
    const-string v4, "North Carolina"

    #@e1a
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e1d
    aput-object v2, v0, v1

    #@e1f
    const/16 v1, 0x116

    #@e21
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e23
    const-string v3, "920"

    #@e25
    const-string v4, "Wisconsin"

    #@e27
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e2a
    aput-object v2, v0, v1

    #@e2c
    const/16 v1, 0x117

    #@e2e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e30
    const-string v3, "925"

    #@e32
    const-string v4, "California"

    #@e34
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e37
    aput-object v2, v0, v1

    #@e39
    const/16 v1, 0x118

    #@e3b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e3d
    const-string v3, "928"

    #@e3f
    const-string v4, "Arizona"

    #@e41
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e44
    aput-object v2, v0, v1

    #@e46
    const/16 v1, 0x119

    #@e48
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e4a
    const-string v3, "931"

    #@e4c
    const-string v4, "Tennessee"

    #@e4e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e51
    aput-object v2, v0, v1

    #@e53
    const/16 v1, 0x11a

    #@e55
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e57
    const-string v3, "936"

    #@e59
    const-string v4, "Texas"

    #@e5b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e5e
    aput-object v2, v0, v1

    #@e60
    const/16 v1, 0x11b

    #@e62
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e64
    const-string v3, "937"

    #@e66
    const-string v4, "Ohio"

    #@e68
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e6b
    aput-object v2, v0, v1

    #@e6d
    const/16 v1, 0x11c

    #@e6f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e71
    const-string v3, "940"

    #@e73
    const-string v4, "Texas"

    #@e75
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e78
    aput-object v2, v0, v1

    #@e7a
    const/16 v1, 0x11d

    #@e7c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e7e
    const-string v3, "941"

    #@e80
    const-string v4, "Florida"

    #@e82
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e85
    aput-object v2, v0, v1

    #@e87
    const/16 v1, 0x11e

    #@e89
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e8b
    const-string v3, "947"

    #@e8d
    const-string v4, "Michigan"

    #@e8f
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e92
    aput-object v2, v0, v1

    #@e94
    const/16 v1, 0x11f

    #@e96
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@e98
    const-string v3, "949"

    #@e9a
    const-string v4, "California"

    #@e9c
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@e9f
    aput-object v2, v0, v1

    #@ea1
    const/16 v1, 0x120

    #@ea3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ea5
    const-string v3, "951"

    #@ea7
    const-string v4, "California"

    #@ea9
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@eac
    aput-object v2, v0, v1

    #@eae
    const/16 v1, 0x121

    #@eb0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@eb2
    const-string v3, "952"

    #@eb4
    const-string v4, "Minnesota"

    #@eb6
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@eb9
    aput-object v2, v0, v1

    #@ebb
    const/16 v1, 0x122

    #@ebd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ebf
    const-string v3, "954"

    #@ec1
    const-string v4, "Florida"

    #@ec3
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ec6
    aput-object v2, v0, v1

    #@ec8
    const/16 v1, 0x123

    #@eca
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ecc
    const-string v3, "956"

    #@ece
    const-string v4, "Texas"

    #@ed0
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ed3
    aput-object v2, v0, v1

    #@ed5
    const/16 v1, 0x124

    #@ed7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ed9
    const-string v3, "970"

    #@edb
    const-string v4, "Colorado"

    #@edd
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@ee0
    aput-object v2, v0, v1

    #@ee2
    const/16 v1, 0x125

    #@ee4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ee6
    const-string v3, "971"

    #@ee8
    const-string v4, "Oregon"

    #@eea
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@eed
    aput-object v2, v0, v1

    #@eef
    const/16 v1, 0x126

    #@ef1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@ef3
    const-string v3, "972"

    #@ef5
    const-string v4, "Texas"

    #@ef7
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@efa
    aput-object v2, v0, v1

    #@efc
    const/16 v1, 0x127

    #@efe
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f00
    const-string v3, "973"

    #@f02
    const-string v4, "New Jersey"

    #@f04
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f07
    aput-object v2, v0, v1

    #@f09
    const/16 v1, 0x128

    #@f0b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f0d
    const-string v3, "978"

    #@f0f
    const-string v4, "Massachusetts"

    #@f11
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f14
    aput-object v2, v0, v1

    #@f16
    const/16 v1, 0x129

    #@f18
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f1a
    const-string v3, "979"

    #@f1c
    const-string v4, "Texas"

    #@f1e
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f21
    aput-object v2, v0, v1

    #@f23
    const/16 v1, 0x12a

    #@f25
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f27
    const-string v3, "980"

    #@f29
    const-string v4, "North Carolina"

    #@f2b
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f2e
    aput-object v2, v0, v1

    #@f30
    const/16 v1, 0x12b

    #@f32
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f34
    const-string v3, "985"

    #@f36
    const-string v4, "Louisiana"

    #@f38
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f3b
    aput-object v2, v0, v1

    #@f3d
    const/16 v1, 0x12c

    #@f3f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f41
    const-string v3, "989"

    #@f43
    const-string v4, "Michigan"

    #@f45
    invoke-direct {v2, p0, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V

    #@f48
    aput-object v2, v0, v1

    #@f4a
    iput-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@f4c
    .line 1514
    :cond_f4c
    return-void
.end method

.method private initSIDRangeInfo()V
    .registers 12

    #@0
    .prologue
    const/16 v10, 0x2a

    #@2
    const/4 v5, 0x1

    #@3
    const/16 v9, 0xd5

    #@5
    const/16 v8, 0xa6

    #@7
    const/4 v7, 0x0

    #@8
    .line 1517
    iget-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a
    if-nez v0, :cond_eaf

    #@c
    .line 1518
    const/16 v0, 0xe5

    #@e
    new-array v0, v0, [Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@10
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@12
    const/16 v2, 0x87f

    #@14
    invoke-direct {v1, v5, v7, v5, v2}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@17
    aput-object v1, v0, v7

    #@19
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@1b
    const/4 v2, 0x2

    #@1c
    const/16 v3, 0x3a80

    #@1e
    const/16 v4, 0x3aff

    #@20
    invoke-direct {v1, v2, v5, v3, v4}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@23
    aput-object v1, v0, v5

    #@25
    const/4 v1, 0x2

    #@26
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@28
    const/4 v3, 0x3

    #@29
    const/4 v4, 0x2

    #@2a
    const/16 v5, 0x7ae0

    #@2c
    const/16 v6, 0x7aef

    #@2e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@31
    aput-object v2, v0, v1

    #@33
    const/4 v1, 0x3

    #@34
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@36
    const/4 v3, 0x4

    #@37
    const/4 v4, 0x3

    #@38
    const/16 v5, 0x2060

    #@3a
    const/16 v6, 0x207f

    #@3c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3f
    aput-object v2, v0, v1

    #@41
    const/4 v1, 0x4

    #@42
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@44
    const/4 v3, 0x5

    #@45
    const/4 v4, 0x4

    #@46
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@49
    aput-object v2, v0, v1

    #@4b
    const/4 v1, 0x5

    #@4c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4e
    const/4 v3, 0x6

    #@4f
    const/4 v4, 0x5

    #@50
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@53
    aput-object v2, v0, v1

    #@55
    const/4 v1, 0x6

    #@56
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@58
    const/4 v3, 0x7

    #@59
    const/4 v4, 0x6

    #@5a
    const/16 v5, 0x2520

    #@5c
    const/16 v6, 0x253f

    #@5e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@61
    aput-object v2, v0, v1

    #@63
    const/4 v1, 0x7

    #@64
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@66
    const/16 v3, 0x8

    #@68
    const/4 v4, 0x7

    #@69
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@6c
    aput-object v2, v0, v1

    #@6e
    const/16 v1, 0x8

    #@70
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@72
    const/16 v3, 0x9

    #@74
    const/16 v4, 0x8

    #@76
    const/16 v5, 0x1fd0

    #@78
    const/16 v6, 0x1fdf

    #@7a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7d
    aput-object v2, v0, v1

    #@7f
    const/16 v1, 0x9

    #@81
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@83
    const/16 v3, 0xa

    #@85
    const/16 v4, 0x9

    #@87
    const/16 v5, 0x7d80

    #@89
    const/16 v6, 0x7dff

    #@8b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8e
    aput-object v2, v0, v1

    #@90
    const/16 v1, 0xa

    #@92
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@94
    const/16 v3, 0xb

    #@96
    const/16 v4, 0xa

    #@98
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9b
    aput-object v2, v0, v1

    #@9d
    const/16 v1, 0xb

    #@9f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a1
    const/16 v3, 0xc

    #@a3
    const/16 v4, 0xb

    #@a5
    const/16 v5, 0x25b0

    #@a7
    const/16 v6, 0x25bf

    #@a9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/16 v1, 0xc

    #@b0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b2
    const/16 v3, 0xd

    #@b4
    const/16 v4, 0xc

    #@b6
    const/16 v5, 0x1e00

    #@b8
    const/16 v6, 0x1e7f

    #@ba
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@bd
    aput-object v2, v0, v1

    #@bf
    const/16 v1, 0xd

    #@c1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c3
    const/16 v3, 0xe

    #@c5
    const/16 v4, 0xd

    #@c7
    const/16 v5, 0x5980

    #@c9
    const/16 v6, 0x59ff

    #@cb
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ce
    aput-object v2, v0, v1

    #@d0
    const/16 v1, 0xe

    #@d2
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d4
    const/16 v3, 0xf

    #@d6
    const/16 v4, 0xe

    #@d8
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@db
    aput-object v2, v0, v1

    #@dd
    const/16 v1, 0xf

    #@df
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e1
    const/16 v3, 0x10

    #@e3
    const/16 v4, 0xf

    #@e5
    const/16 v5, 0x1f90

    #@e7
    const/16 v6, 0x1f9f

    #@e9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ec
    aput-object v2, v0, v1

    #@ee
    const/16 v1, 0x10

    #@f0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@f2
    const/16 v3, 0x11

    #@f4
    const/16 v4, 0x10

    #@f6
    const/16 v5, 0x7ab0

    #@f8
    const/16 v6, 0x7abf

    #@fa
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@fd
    aput-object v2, v0, v1

    #@ff
    const/16 v1, 0x11

    #@101
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@103
    const/16 v3, 0x12

    #@105
    const/16 v4, 0x11

    #@107
    const/16 v5, 0x34a0

    #@109
    const/16 v6, 0x34bf

    #@10b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@10e
    aput-object v2, v0, v1

    #@110
    const/16 v1, 0x12

    #@112
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@114
    const/16 v3, 0x13

    #@116
    const/16 v4, 0x12

    #@118
    const/16 v5, 0x1fe0

    #@11a
    const/16 v6, 0x1fef

    #@11c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@11f
    aput-object v2, v0, v1

    #@121
    const/16 v1, 0x13

    #@123
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@125
    const/16 v3, 0x14

    #@127
    const/16 v4, 0x13

    #@129
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@12c
    aput-object v2, v0, v1

    #@12e
    const/16 v1, 0x14

    #@130
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@132
    const/16 v3, 0x15

    #@134
    const/16 v4, 0x14

    #@136
    const/16 v5, 0x5d80

    #@138
    const/16 v6, 0x5dff

    #@13a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@13d
    aput-object v2, v0, v1

    #@13f
    const/16 v1, 0x15

    #@141
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@143
    const/16 v3, 0x16

    #@145
    const/16 v4, 0x15

    #@147
    const/16 v5, 0x7f80

    #@149
    const/16 v6, 0x7f89

    #@14b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@14e
    aput-object v2, v0, v1

    #@150
    const/16 v1, 0x16

    #@152
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@154
    const/16 v3, 0x17

    #@156
    const/16 v4, 0x16

    #@158
    const/16 v5, 0x2380

    #@15a
    const/16 v6, 0x239f

    #@15c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@15f
    aput-object v2, v0, v1

    #@161
    const/16 v1, 0x17

    #@163
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@165
    const/16 v3, 0x18

    #@167
    const/16 v4, 0x17

    #@169
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@16c
    aput-object v2, v0, v1

    #@16e
    const/16 v1, 0x18

    #@170
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@172
    const/16 v3, 0x19

    #@174
    const/16 v4, 0x18

    #@176
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@179
    aput-object v2, v0, v1

    #@17b
    const/16 v1, 0x19

    #@17d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@17f
    const/16 v3, 0x1a

    #@181
    const/16 v4, 0x19

    #@183
    const/16 v5, 0x7a00

    #@185
    const/16 v6, 0x7a1f

    #@187
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@18a
    aput-object v2, v0, v1

    #@18c
    const/16 v1, 0x1a

    #@18e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@190
    const/16 v3, 0x1b

    #@192
    const/16 v4, 0x1a

    #@194
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@197
    aput-object v2, v0, v1

    #@199
    const/16 v1, 0x1b

    #@19b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@19d
    const/16 v3, 0x1c

    #@19f
    const/16 v4, 0x1b

    #@1a1
    const/16 v5, 0x27c0

    #@1a3
    const/16 v6, 0x27df

    #@1a5
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@1a8
    aput-object v2, v0, v1

    #@1aa
    const/16 v1, 0x1c

    #@1ac
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@1ae
    const/16 v3, 0x1d

    #@1b0
    const/16 v4, 0x1c

    #@1b2
    const/16 v5, 0x7c80

    #@1b4
    const/16 v6, 0x7d7f

    #@1b6
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@1b9
    aput-object v2, v0, v1

    #@1bb
    const/16 v1, 0x1d

    #@1bd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@1bf
    const/16 v3, 0x1e

    #@1c1
    const/16 v4, 0x1d

    #@1c3
    const/16 v5, 0x1fb0

    #@1c5
    const/16 v6, 0x1fbf

    #@1c7
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@1ca
    aput-object v2, v0, v1

    #@1cc
    const/16 v1, 0x1e

    #@1ce
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@1d0
    const/16 v3, 0x1f

    #@1d2
    const/16 v4, 0x1e

    #@1d4
    const/16 v5, 0x2a80

    #@1d6
    const/16 v6, 0x2a8f

    #@1d8
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@1db
    aput-object v2, v0, v1

    #@1dd
    const/16 v1, 0x1f

    #@1df
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@1e1
    const/16 v3, 0x20

    #@1e3
    const/16 v4, 0x1f

    #@1e5
    const/16 v5, 0x3ce0

    #@1e7
    const/16 v6, 0x3cff

    #@1e9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@1ec
    aput-object v2, v0, v1

    #@1ee
    const/16 v1, 0x20

    #@1f0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@1f2
    const/16 v3, 0x21

    #@1f4
    const/16 v4, 0x20

    #@1f6
    const/16 v5, 0x2320

    #@1f8
    const/16 v6, 0x233f

    #@1fa
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@1fd
    aput-object v2, v0, v1

    #@1ff
    const/16 v1, 0x21

    #@201
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@203
    const/16 v3, 0x22

    #@205
    const/16 v4, 0x21

    #@207
    const/16 v5, 0x26e0

    #@209
    const/16 v6, 0x26ff

    #@20b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@20e
    aput-object v2, v0, v1

    #@210
    const/16 v1, 0x22

    #@212
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@214
    const/16 v3, 0x23

    #@216
    const/16 v4, 0x22

    #@218
    const/16 v5, 0x2b60

    #@21a
    const/16 v6, 0x2b7f

    #@21c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@21f
    aput-object v2, v0, v1

    #@221
    const/16 v1, 0x23

    #@223
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@225
    const/16 v3, 0x24

    #@227
    const/16 v4, 0x23

    #@229
    const/16 v5, 0x2480

    #@22b
    const/16 v6, 0x249f

    #@22d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@230
    aput-object v2, v0, v1

    #@232
    const/16 v1, 0x24

    #@234
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@236
    const/16 v3, 0x25

    #@238
    const/16 v4, 0x24

    #@23a
    const/16 v5, 0x4000

    #@23c
    const/16 v6, 0x47ff

    #@23e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@241
    aput-object v2, v0, v1

    #@243
    const/16 v1, 0x25

    #@245
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@247
    const/16 v3, 0x26

    #@249
    const/16 v4, 0x25

    #@24b
    const/16 v5, 0x2290

    #@24d
    const/16 v6, 0x229f

    #@24f
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@252
    aput-object v2, v0, v1

    #@254
    const/16 v1, 0x26

    #@256
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@258
    const/16 v3, 0x27

    #@25a
    const/16 v4, 0x26

    #@25c
    const/16 v5, 0x1fc0

    #@25e
    const/16 v6, 0x1fcf

    #@260
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@263
    aput-object v2, v0, v1

    #@265
    const/16 v1, 0x27

    #@267
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@269
    const/16 v3, 0x28

    #@26b
    const/16 v4, 0x27

    #@26d
    const/16 v5, 0x2460

    #@26f
    const/16 v6, 0x247f

    #@271
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@274
    aput-object v2, v0, v1

    #@276
    const/16 v1, 0x28

    #@278
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@27a
    const/16 v3, 0x29

    #@27c
    const/16 v4, 0x28

    #@27e
    const/16 v5, 0x2440

    #@280
    const/16 v6, 0x245f

    #@282
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@285
    aput-object v2, v0, v1

    #@287
    const/16 v1, 0x29

    #@289
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@28b
    const/16 v3, 0x29

    #@28d
    const/16 v4, 0x7c00

    #@28f
    const/16 v5, 0x7c0a

    #@291
    invoke-direct {v2, v10, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@294
    aput-object v2, v0, v1

    #@296
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@298
    const/16 v2, 0x2b

    #@29a
    const/16 v3, 0x3500

    #@29c
    const/16 v4, 0x37ff

    #@29e
    invoke-direct {v1, v2, v10, v3, v4}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@2a1
    aput-object v1, v0, v10

    #@2a3
    const/16 v1, 0x2b

    #@2a5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@2a7
    const/16 v3, 0x2c

    #@2a9
    const/16 v4, 0x2b

    #@2ab
    const/16 v5, 0x7b80

    #@2ad
    const/16 v6, 0x7bff

    #@2af
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@2b2
    aput-object v2, v0, v1

    #@2b4
    const/16 v1, 0x2c

    #@2b6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@2b8
    const/16 v3, 0x2d

    #@2ba
    const/16 v4, 0x2c

    #@2bc
    const/16 v5, 0x27e0

    #@2be
    const/16 v6, 0x27ff

    #@2c0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@2c3
    aput-object v2, v0, v1

    #@2c5
    const/16 v1, 0x2d

    #@2c7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@2c9
    const/16 v3, 0x2e

    #@2cb
    const/16 v4, 0x2d

    #@2cd
    const/16 v5, 0x2b80

    #@2cf
    const/16 v6, 0x2b8f

    #@2d1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@2d4
    aput-object v2, v0, v1

    #@2d6
    const/16 v1, 0x2e

    #@2d8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@2da
    const/16 v3, 0x2f

    #@2dc
    const/16 v4, 0x2e

    #@2de
    const/16 v5, 0x7f20

    #@2e0
    const/16 v6, 0x7f3f

    #@2e2
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@2e5
    aput-object v2, v0, v1

    #@2e7
    const/16 v1, 0x2f

    #@2e9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@2eb
    const/16 v3, 0x30

    #@2ed
    const/16 v4, 0x2f

    #@2ef
    const/16 v5, 0x2300

    #@2f1
    const/16 v6, 0x231f

    #@2f3
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@2f6
    aput-object v2, v0, v1

    #@2f8
    const/16 v1, 0x30

    #@2fa
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@2fc
    const/16 v3, 0x31

    #@2fe
    const/16 v4, 0x30

    #@300
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@303
    aput-object v2, v0, v1

    #@305
    const/16 v1, 0x31

    #@307
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@309
    const/16 v3, 0x32

    #@30b
    const/16 v4, 0x31

    #@30d
    const/16 v5, 0x7e00

    #@30f
    const/16 v6, 0x7e7f

    #@311
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@314
    aput-object v2, v0, v1

    #@316
    const/16 v1, 0x32

    #@318
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@31a
    const/16 v3, 0x33

    #@31c
    const/16 v4, 0x32

    #@31e
    const/16 v5, 0x79e0

    #@320
    const/16 v6, 0x79ff

    #@322
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@325
    aput-object v2, v0, v1

    #@327
    const/16 v1, 0x33

    #@329
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@32b
    const/16 v3, 0x34

    #@32d
    const/16 v4, 0x33

    #@32f
    const/16 v5, 0x3f00

    #@331
    const/16 v6, 0x3f7f

    #@333
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@336
    aput-object v2, v0, v1

    #@338
    const/16 v1, 0x34

    #@33a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@33c
    const/16 v3, 0x35

    #@33e
    const/16 v4, 0x34

    #@340
    const/16 v5, 0x24e0

    #@342
    const/16 v6, 0x24ff

    #@344
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@347
    aput-object v2, v0, v1

    #@349
    const/16 v1, 0x35

    #@34b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@34d
    const/16 v3, 0x36

    #@34f
    const/16 v4, 0x35

    #@351
    const/16 v5, 0x5800

    #@353
    const/16 v6, 0x580f

    #@355
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@358
    aput-object v2, v0, v1

    #@35a
    const/16 v1, 0x36

    #@35c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@35e
    const/16 v3, 0x37

    #@360
    const/16 v4, 0x36

    #@362
    const/16 v5, 0x22d0

    #@364
    const/16 v6, 0x22df

    #@366
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@369
    aput-object v2, v0, v1

    #@36b
    const/16 v1, 0x37

    #@36d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@36f
    const/16 v3, 0x38

    #@371
    const/16 v4, 0x37

    #@373
    const/16 v5, 0x2590

    #@375
    const/16 v6, 0x259f

    #@377
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@37a
    aput-object v2, v0, v1

    #@37c
    const/16 v1, 0x38

    #@37e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@380
    const/16 v3, 0x39

    #@382
    const/16 v4, 0x38

    #@384
    const/16 v5, 0x2280

    #@386
    const/16 v6, 0x228f

    #@388
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@38b
    aput-object v2, v0, v1

    #@38d
    const/16 v1, 0x39

    #@38f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@391
    const/16 v3, 0x3a

    #@393
    const/16 v4, 0x39

    #@395
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@398
    aput-object v2, v0, v1

    #@39a
    const/16 v1, 0x3a

    #@39c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@39e
    const/16 v3, 0x3b

    #@3a0
    const/16 v4, 0x3a

    #@3a2
    const/16 v5, 0x7a40

    #@3a4
    const/16 v6, 0x7a5f

    #@3a6
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3a9
    aput-object v2, v0, v1

    #@3ab
    const/16 v1, 0x3b

    #@3ad
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@3af
    const/16 v3, 0x3c

    #@3b1
    const/16 v4, 0x3b

    #@3b3
    const/16 v5, 0x2020

    #@3b5
    const/16 v6, 0x203f

    #@3b7
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3ba
    aput-object v2, v0, v1

    #@3bc
    const/16 v1, 0x3c

    #@3be
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@3c0
    const/16 v3, 0x3d

    #@3c2
    const/16 v4, 0x3c

    #@3c4
    const/16 v5, 0x7fc0

    #@3c6
    const/16 v6, 0x7fdf

    #@3c8
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3cb
    aput-object v2, v0, v1

    #@3cd
    const/16 v1, 0x3d

    #@3cf
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@3d1
    const/16 v3, 0x3e

    #@3d3
    const/16 v4, 0x3d

    #@3d5
    const/16 v5, 0x24a0

    #@3d7
    const/16 v6, 0x24bf

    #@3d9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3dc
    aput-object v2, v0, v1

    #@3de
    const/16 v1, 0x3e

    #@3e0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@3e2
    const/16 v3, 0x3f

    #@3e4
    const/16 v4, 0x3e

    #@3e6
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3e9
    aput-object v2, v0, v1

    #@3eb
    const/16 v1, 0x3f

    #@3ed
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@3ef
    const/16 v3, 0x40

    #@3f1
    const/16 v4, 0x3f

    #@3f3
    const/16 v5, 0x2a78

    #@3f5
    const/16 v6, 0x2a7f

    #@3f7
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@3fa
    aput-object v2, v0, v1

    #@3fc
    const/16 v1, 0x40

    #@3fe
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@400
    const/16 v3, 0x41

    #@402
    const/16 v4, 0x40

    #@404
    const/16 v5, 0x2620

    #@406
    const/16 v6, 0x263f

    #@408
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@40b
    aput-object v2, v0, v1

    #@40d
    const/16 v1, 0x41

    #@40f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@411
    const/16 v3, 0x42

    #@413
    const/16 v4, 0x41

    #@415
    const/16 v5, 0x1f00

    #@417
    const/16 v6, 0x1f0f

    #@419
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@41c
    aput-object v2, v0, v1

    #@41e
    const/16 v1, 0x42

    #@420
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@422
    const/16 v3, 0x43

    #@424
    const/16 v4, 0x42

    #@426
    const/16 v5, 0x2b20

    #@428
    const/16 v6, 0x2b3f

    #@42a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@42d
    aput-object v2, v0, v1

    #@42f
    const/16 v1, 0x43

    #@431
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@433
    const/16 v3, 0x44

    #@435
    const/16 v4, 0x43

    #@437
    const/16 v5, 0x2ad0

    #@439
    const/16 v6, 0x2adf

    #@43b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@43e
    aput-object v2, v0, v1

    #@440
    const/16 v1, 0x44

    #@442
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@444
    const/16 v3, 0x45

    #@446
    const/16 v4, 0x44

    #@448
    const/16 v5, 0x5f80

    #@44a
    const/16 v6, 0x5f83

    #@44c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@44f
    aput-object v2, v0, v1

    #@451
    const/16 v1, 0x45

    #@453
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@455
    const/16 v3, 0x46

    #@457
    const/16 v4, 0x45

    #@459
    const/16 v5, 0x5c80

    #@45b
    const/16 v6, 0x5d7f

    #@45d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@460
    aput-object v2, v0, v1

    #@462
    const/16 v1, 0x46

    #@464
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@466
    const/16 v3, 0x47

    #@468
    const/16 v4, 0x46

    #@46a
    const/16 v5, 0x7a60

    #@46c
    const/16 v6, 0x7a7f

    #@46e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@471
    aput-object v2, v0, v1

    #@473
    const/16 v1, 0x47

    #@475
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@477
    const/16 v3, 0x48

    #@479
    const/16 v4, 0x47

    #@47b
    const/16 v5, 0x2bf0

    #@47d
    const/16 v6, 0x2be4

    #@47f
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@482
    aput-object v2, v0, v1

    #@484
    const/16 v1, 0x48

    #@486
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@488
    const/16 v3, 0x49

    #@48a
    const/16 v4, 0x48

    #@48c
    const/16 v5, 0x24c0

    #@48e
    const/16 v6, 0x24df

    #@490
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@493
    aput-object v2, v0, v1

    #@495
    const/16 v1, 0x49

    #@497
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@499
    const/16 v3, 0x4a

    #@49b
    const/16 v4, 0x49

    #@49d
    const/16 v5, 0x2160

    #@49f
    const/16 v6, 0x217f

    #@4a1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@4a4
    aput-object v2, v0, v1

    #@4a6
    const/16 v1, 0x4a

    #@4a8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4aa
    const/16 v3, 0x4b

    #@4ac
    const/16 v4, 0x4a

    #@4ae
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@4b1
    aput-object v2, v0, v1

    #@4b3
    const/16 v1, 0x4b

    #@4b5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4b7
    const/16 v3, 0x4c

    #@4b9
    const/16 v4, 0x4b

    #@4bb
    const/16 v5, 0x5600

    #@4bd
    const/16 v6, 0x56ff

    #@4bf
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@4c2
    aput-object v2, v0, v1

    #@4c4
    const/16 v1, 0x4c

    #@4c6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4c8
    const/16 v3, 0x4d

    #@4ca
    const/16 v4, 0x4c

    #@4cc
    const/16 v5, 0x2400

    #@4ce
    const/16 v6, 0x241f

    #@4d0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@4d3
    aput-object v2, v0, v1

    #@4d5
    const/16 v1, 0x4d

    #@4d7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4d9
    const/16 v3, 0x4e

    #@4db
    const/16 v4, 0x4d

    #@4dd
    const/16 v5, 0x7ad0

    #@4df
    const/16 v6, 0x7adf

    #@4e1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@4e4
    aput-object v2, v0, v1

    #@4e6
    const/16 v1, 0x4e

    #@4e8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4ea
    const/16 v3, 0x4f

    #@4ec
    const/16 v4, 0x4e

    #@4ee
    const/16 v5, 0x5e80

    #@4f0
    const/16 v6, 0x5eff

    #@4f2
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@4f5
    aput-object v2, v0, v1

    #@4f7
    const/16 v1, 0x4f

    #@4f9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@4fb
    const/16 v3, 0x50

    #@4fd
    const/16 v4, 0x4f

    #@4ff
    const/16 v5, 0x5e80

    #@501
    const/16 v6, 0x5eff

    #@503
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@506
    aput-object v2, v0, v1

    #@508
    const/16 v1, 0x50

    #@50a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@50c
    const/16 v3, 0x51

    #@50e
    const/16 v4, 0x50

    #@510
    const/16 v5, 0x1f80

    #@512
    const/16 v6, 0x1f8f

    #@514
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@517
    aput-object v2, v0, v1

    #@519
    const/16 v1, 0x51

    #@51b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@51d
    const/16 v3, 0x52

    #@51f
    const/16 v4, 0x51

    #@521
    const/16 v5, 0x7a80

    #@523
    const/16 v6, 0x7a8f

    #@525
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@528
    aput-object v2, v0, v1

    #@52a
    const/16 v1, 0x52

    #@52c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@52e
    const/16 v3, 0x53

    #@530
    const/16 v4, 0x52

    #@532
    const/16 v5, 0x25e0

    #@534
    const/16 v6, 0x25ef

    #@536
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@539
    aput-object v2, v0, v1

    #@53b
    const/16 v1, 0x53

    #@53d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@53f
    const/16 v3, 0x54

    #@541
    const/16 v4, 0x53

    #@543
    const/16 v5, 0x7fa0

    #@545
    const/16 v6, 0x7fbf

    #@547
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@54a
    aput-object v2, v0, v1

    #@54c
    const/16 v1, 0x54

    #@54e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@550
    const/16 v3, 0x55

    #@552
    const/16 v4, 0x54

    #@554
    const/16 v5, 0x2260

    #@556
    const/16 v6, 0x227f

    #@558
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@55b
    aput-object v2, v0, v1

    #@55d
    const/16 v1, 0x55

    #@55f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@561
    const/16 v3, 0x56

    #@563
    const/16 v4, 0x55

    #@565
    const/16 v5, 0x2260

    #@567
    const/16 v6, 0x227f

    #@569
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@56c
    aput-object v2, v0, v1

    #@56e
    const/16 v1, 0x56

    #@570
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@572
    const/16 v3, 0x57

    #@574
    const/16 v4, 0x56

    #@576
    const/16 v5, 0x7a20

    #@578
    const/16 v6, 0x7a3f

    #@57a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@57d
    aput-object v2, v0, v1

    #@57f
    const/16 v1, 0x57

    #@581
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@583
    const/16 v3, 0x58

    #@585
    const/16 v4, 0x57

    #@587
    const/16 v5, 0x7f60

    #@589
    const/16 v6, 0x7f7f

    #@58b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@58e
    aput-object v2, v0, v1

    #@590
    const/16 v1, 0x58

    #@592
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@594
    const/16 v3, 0x59

    #@596
    const/16 v4, 0x58

    #@598
    const/16 v5, 0x7fe0

    #@59a
    const/16 v6, 0x7fff

    #@59c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@59f
    aput-object v2, v0, v1

    #@5a1
    const/16 v1, 0x59

    #@5a3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@5a5
    const/16 v3, 0x5a

    #@5a7
    const/16 v4, 0x59

    #@5a9
    const/16 v5, 0x2990

    #@5ab
    const/16 v6, 0x299f

    #@5ad
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@5b0
    aput-object v2, v0, v1

    #@5b2
    const/16 v1, 0x5a

    #@5b4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@5b6
    const/16 v3, 0x5b

    #@5b8
    const/16 v4, 0x5a

    #@5ba
    const v5, 0x26701

    #@5bd
    const/16 v6, 0x3dff

    #@5bf
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@5c2
    aput-object v2, v0, v1

    #@5c4
    const/16 v1, 0x5b

    #@5c6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@5c8
    const/16 v3, 0x5c

    #@5ca
    const/16 v4, 0x5b

    #@5cc
    const/16 v5, 0x5f60

    #@5ce
    const/16 v6, 0x5f7f

    #@5d0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@5d3
    aput-object v2, v0, v1

    #@5d5
    const/16 v1, 0x5c

    #@5d7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@5d9
    const/16 v3, 0x5d

    #@5db
    const/16 v4, 0x5c

    #@5dd
    const/16 v5, 0x3880

    #@5df
    const/16 v6, 0x39ff

    #@5e1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@5e4
    aput-object v2, v0, v1

    #@5e6
    const/16 v1, 0x5d

    #@5e8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@5ea
    const/16 v3, 0x5e

    #@5ec
    const/16 v4, 0x5d

    #@5ee
    const/16 v5, 0x2900

    #@5f0
    const/16 v6, 0x297f

    #@5f2
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@5f5
    aput-object v2, v0, v1

    #@5f7
    const/16 v1, 0x5e

    #@5f9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@5fb
    const/16 v3, 0x5f

    #@5fd
    const/16 v4, 0x5e

    #@5ff
    const/16 v5, 0x3d00

    #@601
    const/16 v6, 0x3d7f

    #@603
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@606
    aput-object v2, v0, v1

    #@608
    const/16 v1, 0x5f

    #@60a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@60c
    const/16 v3, 0x60

    #@60e
    const/16 v4, 0x5f

    #@610
    const/16 v5, 0x3c60

    #@612
    const/16 v6, 0x3c7f

    #@614
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@617
    aput-object v2, v0, v1

    #@619
    const/16 v1, 0x60

    #@61b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@61d
    const/16 v3, 0x61

    #@61f
    const/16 v4, 0x60

    #@621
    const/16 v5, 0x5f40

    #@623
    const/16 v6, 0x5f43

    #@625
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@628
    aput-object v2, v0, v1

    #@62a
    const/16 v1, 0x61

    #@62c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@62e
    const/16 v3, 0x62

    #@630
    const/16 v4, 0x61

    #@632
    const/16 v5, 0x2100

    #@634
    const/16 v6, 0x211f

    #@636
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@639
    aput-object v2, v0, v1

    #@63b
    const/16 v1, 0x62

    #@63d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@63f
    const/16 v3, 0x63

    #@641
    const/16 v4, 0x62

    #@643
    const/16 v5, 0x5a80

    #@645
    const/16 v6, 0x5b7f

    #@647
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@64a
    aput-object v2, v0, v1

    #@64c
    const/16 v1, 0x63

    #@64e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@650
    const/16 v3, 0x64

    #@652
    const/16 v4, 0x63

    #@654
    const/16 v5, 0x1ff0

    #@656
    const/16 v6, 0x1fff

    #@658
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@65b
    aput-object v2, v0, v1

    #@65d
    const/16 v1, 0x64

    #@65f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@661
    const/16 v3, 0x65

    #@663
    const/16 v4, 0x64

    #@665
    const/16 v5, 0x3000

    #@667
    const/16 v6, 0x33ff

    #@669
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@66c
    aput-object v2, v0, v1

    #@66e
    const/16 v1, 0x65

    #@670
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@672
    const/16 v3, 0x66

    #@674
    const/16 v4, 0x65

    #@676
    const/16 v5, 0x3c20

    #@678
    const/16 v6, 0x3c3f

    #@67a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@67d
    aput-object v2, v0, v1

    #@67f
    const/16 v1, 0x66

    #@681
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@683
    const/16 v3, 0x67

    #@685
    const/16 v4, 0x66

    #@687
    const/16 v5, 0x22e0

    #@689
    const/16 v6, 0x22ef

    #@68b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@68e
    aput-object v2, v0, v1

    #@690
    const/16 v1, 0x67

    #@692
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@694
    const/16 v3, 0x68

    #@696
    const/16 v4, 0x67

    #@698
    const/16 v5, 0x2680

    #@69a
    const/16 v6, 0x269f

    #@69c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@69f
    aput-object v2, v0, v1

    #@6a1
    const/16 v1, 0x68

    #@6a3
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@6a5
    const/16 v3, 0x69

    #@6a7
    const/16 v4, 0x68

    #@6a9
    const/16 v5, 0x2bc0

    #@6ab
    const/16 v6, 0x2bcf

    #@6ad
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@6b0
    aput-object v2, v0, v1

    #@6b2
    const/16 v1, 0x69

    #@6b4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@6b6
    const/16 v3, 0x6a

    #@6b8
    const/16 v4, 0x69

    #@6ba
    const/16 v5, 0x2b40

    #@6bc
    const/16 v6, 0x2b5f

    #@6be
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@6c1
    aput-object v2, v0, v1

    #@6c3
    const/16 v1, 0x6a

    #@6c5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@6c7
    const/16 v3, 0x6b

    #@6c9
    const/16 v4, 0x6a

    #@6cb
    const/16 v5, 0x880

    #@6cd
    const/16 v6, 0x8ff

    #@6cf
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@6d2
    aput-object v2, v0, v1

    #@6d4
    const/16 v1, 0x6b

    #@6d6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@6d8
    const/16 v3, 0x6c

    #@6da
    const/16 v4, 0x6b

    #@6dc
    const/16 v5, 0x2c30

    #@6de
    const/16 v6, 0x2c3f

    #@6e0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@6e3
    aput-object v2, v0, v1

    #@6e5
    const/16 v1, 0x6c

    #@6e7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@6e9
    const/16 v3, 0x6d

    #@6eb
    const/16 v4, 0x6c

    #@6ed
    const/16 v5, 0x542e

    #@6ef
    const/16 v6, 0x543e

    #@6f1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@6f4
    aput-object v2, v0, v1

    #@6f6
    const/16 v1, 0x6d

    #@6f8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@6fa
    const/16 v3, 0x6e

    #@6fc
    const/16 v4, 0x6d

    #@6fe
    const/16 v5, 0x3458

    #@700
    const/16 v6, 0x349f

    #@702
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@705
    aput-object v2, v0, v1

    #@707
    const/16 v1, 0x6e

    #@709
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@70b
    const/16 v3, 0x6f

    #@70d
    const/16 v4, 0x6e

    #@70f
    const/16 v5, 0x2a20

    #@711
    const/16 v6, 0x2a21

    #@713
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@716
    aput-object v2, v0, v1

    #@718
    const/16 v1, 0x6f

    #@71a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@71c
    const/16 v3, 0x70

    #@71e
    const/16 v4, 0x6f

    #@720
    const/16 v5, 0x3c00

    #@722
    const/16 v6, 0x3c1f

    #@724
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@727
    aput-object v2, v0, v1

    #@729
    const/16 v1, 0x70

    #@72b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@72d
    const/16 v3, 0x71

    #@72f
    const/16 v4, 0x70

    #@731
    const/16 v5, 0x2580

    #@733
    const/16 v6, 0x258f

    #@735
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@738
    aput-object v2, v0, v1

    #@73a
    const/16 v1, 0x71

    #@73c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@73e
    const/16 v3, 0x72

    #@740
    const/16 v4, 0x71

    #@742
    const/16 v5, 0x23c0

    #@744
    const/16 v6, 0x23df

    #@746
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@749
    aput-object v2, v0, v1

    #@74b
    const/16 v1, 0x72

    #@74d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@74f
    const/16 v3, 0x73

    #@751
    const/16 v4, 0x72

    #@753
    const/16 v5, 0x2140

    #@755
    const/16 v6, 0x215f

    #@757
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@75a
    aput-object v2, v0, v1

    #@75c
    const/16 v1, 0x73

    #@75e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@760
    const/16 v3, 0x74

    #@762
    const/16 v4, 0x73

    #@764
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@767
    aput-object v2, v0, v1

    #@769
    const/16 v1, 0x74

    #@76b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@76d
    const/16 v3, 0x75

    #@76f
    const/16 v4, 0x74

    #@771
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@774
    aput-object v2, v0, v1

    #@776
    const/16 v1, 0x75

    #@778
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@77a
    const/16 v3, 0x76

    #@77c
    const/16 v4, 0x75

    #@77e
    const/16 v5, 0x5f20

    #@780
    const/16 v6, 0x5f3f

    #@782
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@785
    aput-object v2, v0, v1

    #@787
    const/16 v1, 0x76

    #@789
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@78b
    const/16 v3, 0x77

    #@78d
    const/16 v4, 0x76

    #@78f
    const/16 v5, 0x2c20

    #@791
    const/16 v6, 0x2c2f

    #@793
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@796
    aput-object v2, v0, v1

    #@798
    const/16 v1, 0x77

    #@79a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@79c
    const/16 v3, 0x78

    #@79e
    const/16 v4, 0x77

    #@7a0
    const/16 v5, 0x2740

    #@7a2
    const/16 v6, 0x275f

    #@7a4
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7a7
    aput-object v2, v0, v1

    #@7a9
    const/16 v1, 0x78

    #@7ab
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@7ad
    const/16 v3, 0x79

    #@7af
    const/16 v4, 0x78

    #@7b1
    const/16 v5, 0x27a0

    #@7b3
    const/16 v6, 0x27bf

    #@7b5
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7b8
    aput-object v2, v0, v1

    #@7ba
    const/16 v1, 0x79

    #@7bc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@7be
    const/16 v3, 0x7a

    #@7c0
    const/16 v4, 0x79

    #@7c2
    const/16 v5, 0x2880

    #@7c4
    const/16 v6, 0x28ff

    #@7c6
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7c9
    aput-object v2, v0, v1

    #@7cb
    const/16 v1, 0x7a

    #@7cd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@7cf
    const/16 v3, 0x7b

    #@7d1
    const/16 v4, 0x7a

    #@7d3
    const/16 v5, 0x34e0

    #@7d5
    const/16 v6, 0x34ff

    #@7d7
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7da
    aput-object v2, v0, v1

    #@7dc
    const/16 v1, 0x7b

    #@7de
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@7e0
    const/16 v3, 0x7c

    #@7e2
    const/16 v4, 0x7b

    #@7e4
    const/16 v5, 0x2240

    #@7e6
    const/16 v6, 0x225f

    #@7e8
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7eb
    aput-object v2, v0, v1

    #@7ed
    const/16 v1, 0x7c

    #@7ef
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@7f1
    const/16 v3, 0x7d

    #@7f3
    const/16 v4, 0x7c

    #@7f5
    const/16 v5, 0x7af0

    #@7f7
    const/16 v6, 0x7af3

    #@7f9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@7fc
    aput-object v2, v0, v1

    #@7fe
    const/16 v1, 0x7d

    #@800
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@802
    const/16 v3, 0x7e

    #@804
    const/16 v4, 0x7d

    #@806
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@809
    aput-object v2, v0, v1

    #@80b
    const/16 v1, 0x7e

    #@80d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@80f
    const/16 v3, 0x7f

    #@811
    const/16 v4, 0x7e

    #@813
    const/16 v5, 0x2c10

    #@815
    const/16 v6, 0x2c1f

    #@817
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@81a
    aput-object v2, v0, v1

    #@81c
    const/16 v1, 0x7f

    #@81e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@820
    const/16 v3, 0x80

    #@822
    const/16 v4, 0x7f

    #@824
    const/16 v5, 0x7a90

    #@826
    const/16 v6, 0x7a9f

    #@828
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@82b
    aput-object v2, v0, v1

    #@82d
    const/16 v1, 0x80

    #@82f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@831
    const/16 v3, 0x81

    #@833
    const/16 v4, 0x80

    #@835
    const/16 v5, 0x2220

    #@837
    const/16 v6, 0x223f

    #@839
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@83c
    aput-object v2, v0, v1

    #@83e
    const/16 v1, 0x81

    #@840
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@842
    const/16 v3, 0x82

    #@844
    const/16 v4, 0x81

    #@846
    const/16 v5, 0x23a0

    #@848
    const/16 v6, 0x23bf

    #@84a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@84d
    aput-object v2, v0, v1

    #@84f
    const/16 v1, 0x82

    #@851
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@853
    const/16 v3, 0x83

    #@855
    const/16 v4, 0x82

    #@857
    const/16 v5, 0x6000

    #@859
    const/16 v6, 0x61f3

    #@85b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@85e
    aput-object v2, v0, v1

    #@860
    const/16 v1, 0x83

    #@862
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@864
    const/16 v3, 0x84

    #@866
    const/16 v4, 0x83

    #@868
    const/16 v5, 0x3e32

    #@86a
    const/16 v6, 0x3e41

    #@86c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@86f
    aput-object v2, v0, v1

    #@871
    const/16 v1, 0x84

    #@873
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@875
    const/16 v3, 0x85

    #@877
    const/16 v4, 0x84

    #@879
    const/16 v5, 0x2a00

    #@87b
    const/16 v6, 0x2a0f

    #@87d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@880
    aput-object v2, v0, v1

    #@882
    const/16 v1, 0x85

    #@884
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@886
    const/16 v3, 0x86

    #@888
    const/16 v4, 0x85

    #@88a
    const/16 v5, 0x3ca0

    #@88c
    const/16 v6, 0x3cbf

    #@88e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@891
    aput-object v2, v0, v1

    #@893
    const/16 v1, 0x86

    #@895
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@897
    const/16 v3, 0x87

    #@899
    const/16 v4, 0x86

    #@89b
    const/16 v5, 0x1f70

    #@89d
    const/16 v6, 0x1f7f

    #@89f
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8a2
    aput-object v2, v0, v1

    #@8a4
    const/16 v1, 0x87

    #@8a6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8a8
    const/16 v3, 0x88

    #@8aa
    const/16 v4, 0x87

    #@8ac
    const/16 v5, 0x2040

    #@8ae
    const/16 v6, 0x205f

    #@8b0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8b3
    aput-object v2, v0, v1

    #@8b5
    const/16 v1, 0x88

    #@8b7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8b9
    const/16 v3, 0x89

    #@8bb
    const/16 v4, 0x88

    #@8bd
    const/16 v5, 0x2700

    #@8bf
    const/16 v6, 0x271f

    #@8c1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8c4
    aput-object v2, v0, v1

    #@8c6
    const/16 v1, 0x89

    #@8c8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8ca
    const/16 v3, 0x8a

    #@8cc
    const/16 v4, 0x89

    #@8ce
    const/16 v5, 0x3b80

    #@8d0
    const/16 v6, 0x3bff

    #@8d2
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8d5
    aput-object v2, v0, v1

    #@8d7
    const/16 v1, 0x8a

    #@8d9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8db
    const/16 v3, 0x8b

    #@8dd
    const/16 v4, 0x8a

    #@8df
    const/16 v5, 0x2780

    #@8e1
    const/16 v6, 0x279f

    #@8e3
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8e6
    aput-object v2, v0, v1

    #@8e8
    const/16 v1, 0x8b

    #@8ea
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8ec
    const/16 v3, 0x8c

    #@8ee
    const/16 v4, 0x8b

    #@8f0
    const/16 v5, 0x2a90

    #@8f2
    const/16 v6, 0x2a9f

    #@8f4
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@8f7
    aput-object v2, v0, v1

    #@8f9
    const/16 v1, 0x8c

    #@8fb
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@8fd
    const/16 v3, 0x8d

    #@8ff
    const/16 v4, 0x8c

    #@901
    const/16 v5, 0x3cc0

    #@903
    const/16 v6, 0x3cdf

    #@905
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@908
    aput-object v2, v0, v1

    #@90a
    const/16 v1, 0x8d

    #@90c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@90e
    const/16 v3, 0x8e

    #@910
    const/16 v4, 0x8d

    #@912
    const/16 v5, 0x5e00

    #@914
    const/16 v6, 0x5e7f

    #@916
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@919
    aput-object v2, v0, v1

    #@91b
    const/16 v1, 0x8e

    #@91d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@91f
    const/16 v3, 0x8f

    #@921
    const/16 v4, 0x8e

    #@923
    const/16 v5, 0x7aa0

    #@925
    const/16 v6, 0x7aaf

    #@927
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@92a
    aput-object v2, v0, v1

    #@92c
    const/16 v1, 0x8f

    #@92e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@930
    const/16 v3, 0x90

    #@932
    const/16 v4, 0x8f

    #@934
    const/16 v5, 0x2bd0

    #@936
    const/16 v6, 0x2bdf

    #@938
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@93b
    aput-object v2, v0, v1

    #@93d
    const/16 v1, 0x90

    #@93f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@941
    const/16 v3, 0x91

    #@943
    const/16 v4, 0x90

    #@945
    const/16 v5, 0x2180

    #@947
    const/16 v6, 0x21ff

    #@949
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@94c
    aput-object v2, v0, v1

    #@94e
    const/16 v1, 0x91

    #@950
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@952
    const/16 v3, 0x92

    #@954
    const/16 v4, 0x91

    #@956
    const/16 v5, 0x7f00

    #@958
    const/16 v6, 0x7f1f

    #@95a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@95d
    aput-object v2, v0, v1

    #@95f
    const/16 v1, 0x92

    #@961
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@963
    const/16 v3, 0x93

    #@965
    const/16 v4, 0x92

    #@967
    const/16 v5, 0x2340

    #@969
    const/16 v6, 0x235f

    #@96b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@96e
    aput-object v2, v0, v1

    #@970
    const/16 v1, 0x93

    #@972
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@974
    const/16 v3, 0x94

    #@976
    const/16 v4, 0x93

    #@978
    const/16 v5, 0x2420

    #@97a
    const/16 v6, 0x243f

    #@97c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@97f
    aput-object v2, v0, v1

    #@981
    const/16 v1, 0x94

    #@983
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@985
    const/16 v3, 0x95

    #@987
    const/16 v4, 0x94

    #@989
    const/16 v5, 0x25d0

    #@98b
    const/16 v6, 0x25df

    #@98d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@990
    aput-object v2, v0, v1

    #@992
    const/16 v1, 0x95

    #@994
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@996
    const/16 v3, 0x96

    #@998
    const/16 v4, 0x95

    #@99a
    const/16 v5, 0x5700

    #@99c
    const/16 v6, 0x577f

    #@99e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9a1
    aput-object v2, v0, v1

    #@9a3
    const/16 v1, 0x96

    #@9a5
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@9a7
    const/16 v3, 0x97

    #@9a9
    const/16 v4, 0x96

    #@9ab
    const/16 v5, 0x2c50

    #@9ad
    const/16 v6, 0x2c5f

    #@9af
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9b2
    aput-object v2, v0, v1

    #@9b4
    const/16 v1, 0x97

    #@9b6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@9b8
    const/16 v3, 0x98

    #@9ba
    const/16 v4, 0x97

    #@9bc
    const/16 v5, 0x3a00

    #@9be
    const/16 v6, 0x3a7f

    #@9c0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9c3
    aput-object v2, v0, v1

    #@9c5
    const/16 v1, 0x98

    #@9c7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@9c9
    const/16 v3, 0x99

    #@9cb
    const/16 v4, 0x98

    #@9cd
    const/16 v5, 0x2ae0

    #@9cf
    const/16 v6, 0x2aef

    #@9d1
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9d4
    aput-object v2, v0, v1

    #@9d6
    const/16 v1, 0x99

    #@9d8
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@9da
    const/16 v3, 0x9a

    #@9dc
    const/16 v4, 0x99

    #@9de
    const/16 v5, 0x7f40

    #@9e0
    const/16 v6, 0x7f5f

    #@9e2
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9e5
    aput-object v2, v0, v1

    #@9e7
    const/16 v1, 0x9a

    #@9e9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@9eb
    const/16 v3, 0x9b

    #@9ed
    const/16 v4, 0x9a

    #@9ef
    const/16 v5, 0x2b00

    #@9f1
    const/16 v6, 0x2b1f

    #@9f3
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@9f6
    aput-object v2, v0, v1

    #@9f8
    const/16 v1, 0x9b

    #@9fa
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@9fc
    const/16 v3, 0x9c

    #@9fe
    const/16 v4, 0x9b

    #@a00
    const/16 v5, 0x7980

    #@a02
    const/16 v6, 0x799f

    #@a04
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a07
    aput-object v2, v0, v1

    #@a09
    const/16 v1, 0x9c

    #@a0b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a0d
    const/16 v3, 0x9d

    #@a0f
    const/16 v4, 0x9c

    #@a11
    const/16 v5, 0x7e80

    #@a13
    const/16 v6, 0x7eff

    #@a15
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a18
    aput-object v2, v0, v1

    #@a1a
    const/16 v1, 0x9d

    #@a1c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a1e
    const/16 v3, 0x9e

    #@a20
    const/16 v4, 0x9d

    #@a22
    const/16 v5, 0x2980

    #@a24
    const/16 v6, 0x298f

    #@a26
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a29
    aput-object v2, v0, v1

    #@a2b
    const/16 v1, 0x9e

    #@a2d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a2f
    const/16 v3, 0x9f

    #@a31
    const/16 v4, 0x9e

    #@a33
    const/16 v5, 0x3f80

    #@a35
    const/16 v6, 0x3f8f

    #@a37
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a3a
    aput-object v2, v0, v1

    #@a3c
    const/16 v1, 0x9f

    #@a3e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a40
    const/16 v3, 0xa0

    #@a42
    const/16 v4, 0x9e

    #@a44
    const/16 v5, 0x3f90

    #@a46
    const/16 v6, 0x3fff

    #@a48
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a4b
    aput-object v2, v0, v1

    #@a4d
    const/16 v1, 0xa0

    #@a4f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a51
    const/16 v3, 0xa1

    #@a53
    const/16 v4, 0x9f

    #@a55
    const/16 v5, 0x5f00

    #@a57
    const/16 v6, 0x5f1f

    #@a59
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a5c
    aput-object v2, v0, v1

    #@a5e
    const/16 v1, 0xa1

    #@a60
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a62
    const/16 v3, 0xa2

    #@a64
    const/16 v4, 0xa0

    #@a66
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a69
    aput-object v2, v0, v1

    #@a6b
    const/16 v1, 0xa2

    #@a6d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a6f
    const/16 v3, 0xa3

    #@a71
    const/16 v4, 0xa1

    #@a73
    const/16 v5, 0x7ac0

    #@a75
    const/16 v6, 0x7acf

    #@a77
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a7a
    aput-object v2, v0, v1

    #@a7c
    const/16 v1, 0xa3

    #@a7e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a80
    const/16 v3, 0xa4

    #@a82
    const/16 v4, 0xa2

    #@a84
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a87
    aput-object v2, v0, v1

    #@a89
    const/16 v1, 0xa4

    #@a8b
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a8d
    const/16 v3, 0xa5

    #@a8f
    const/16 v4, 0xa3

    #@a91
    const/16 v5, 0x24e0

    #@a93
    const/16 v6, 0x24ff

    #@a95
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@a98
    aput-object v2, v0, v1

    #@a9a
    const/16 v1, 0xa5

    #@a9c
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@a9e
    const/16 v3, 0xa4

    #@aa0
    const/16 v4, 0x22f0

    #@aa2
    const/16 v5, 0x22ff

    #@aa4
    invoke-direct {v2, v8, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@aa7
    aput-object v2, v0, v1

    #@aa9
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@aab
    const/16 v2, 0xa7

    #@aad
    const/16 v3, 0xa5

    #@aaf
    const/16 v4, 0x3e80

    #@ab1
    const/16 v5, 0x3eff

    #@ab3
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ab6
    aput-object v1, v0, v8

    #@ab8
    const/16 v1, 0xa7

    #@aba
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@abc
    const/16 v3, 0xa8

    #@abe
    const/16 v4, 0x2c80

    #@ac0
    const/16 v5, 0x2d22

    #@ac2
    invoke-direct {v2, v3, v8, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ac5
    aput-object v2, v0, v1

    #@ac7
    const/16 v1, 0xa8

    #@ac9
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@acb
    const/16 v3, 0xa9

    #@acd
    const/16 v4, 0xa7

    #@acf
    const/16 v5, 0x2600

    #@ad1
    const/16 v6, 0x261f

    #@ad3
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ad6
    aput-object v2, v0, v1

    #@ad8
    const/16 v1, 0xa9

    #@ada
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@adc
    const/16 v3, 0xaa

    #@ade
    const/16 v4, 0xa8

    #@ae0
    const/16 v5, 0x1f60

    #@ae2
    const/16 v6, 0x1f6f

    #@ae4
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ae7
    aput-object v2, v0, v1

    #@ae9
    const/16 v1, 0xaa

    #@aeb
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@aed
    const/16 v3, 0xab

    #@aef
    const/16 v4, 0xa9

    #@af1
    const/16 v5, 0x1f50

    #@af3
    const/16 v6, 0x1f5f

    #@af5
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@af8
    aput-object v2, v0, v1

    #@afa
    const/16 v1, 0xab

    #@afc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@afe
    const/16 v3, 0xac

    #@b00
    const/16 v4, 0xaa

    #@b02
    const/16 v5, 0x1f30

    #@b04
    const/16 v6, 0x1f3f

    #@b06
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b09
    aput-object v2, v0, v1

    #@b0b
    const/16 v1, 0xac

    #@b0d
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b0f
    const/16 v3, 0xad

    #@b11
    const/16 v4, 0xab

    #@b13
    const/16 v5, 0x1f40

    #@b15
    const/16 v6, 0x1f47

    #@b17
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b1a
    aput-object v2, v0, v1

    #@b1c
    const/16 v1, 0xad

    #@b1e
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b20
    const/16 v3, 0xae

    #@b22
    const/16 v4, 0xac

    #@b24
    const/16 v5, 0x2bb0

    #@b26
    const/16 v6, 0x2bbf

    #@b28
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b2b
    aput-object v2, v0, v1

    #@b2d
    const/16 v1, 0xae

    #@b2f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b31
    const/16 v3, 0xaf

    #@b33
    const/16 v4, 0xad

    #@b35
    const/16 v5, 0x2a10

    #@b37
    const/16 v6, 0x2a1f

    #@b39
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b3c
    aput-object v2, v0, v1

    #@b3e
    const/16 v1, 0xaf

    #@b40
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b42
    const/16 v3, 0xb0

    #@b44
    const/16 v4, 0xae

    #@b46
    const/16 v5, 0x22a0

    #@b48
    const/16 v6, 0x22af

    #@b4a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b4d
    aput-object v2, v0, v1

    #@b4f
    const/16 v1, 0xb0

    #@b51
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b53
    const/16 v3, 0xb1

    #@b55
    const/16 v4, 0xaf

    #@b57
    const/16 v5, 0x3c80

    #@b59
    const/16 v6, 0x3c9f

    #@b5b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b5e
    aput-object v2, v0, v1

    #@b60
    const/16 v1, 0xb1

    #@b62
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b64
    const/16 v3, 0xb2

    #@b66
    const/16 v4, 0xb0

    #@b68
    const/16 v5, 0x2200

    #@b6a
    const/16 v6, 0x221f

    #@b6c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b6f
    aput-object v2, v0, v1

    #@b71
    const/16 v1, 0xb2

    #@b73
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b75
    const/16 v3, 0xb3

    #@b77
    const/16 v4, 0xb1

    #@b79
    const/16 v5, 0x5b80

    #@b7b
    const/16 v6, 0x5bff

    #@b7d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b80
    aput-object v2, v0, v1

    #@b82
    const/16 v1, 0xb3

    #@b84
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b86
    const/16 v3, 0xb4

    #@b88
    const/16 v4, 0xb2

    #@b8a
    const/16 v5, 0x22c0

    #@b8c
    const/16 v6, 0x22cf

    #@b8e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@b91
    aput-object v2, v0, v1

    #@b93
    const/16 v1, 0xb4

    #@b95
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@b97
    const/16 v3, 0xb5

    #@b99
    const/16 v4, 0xb3

    #@b9b
    const/16 v5, 0x23e0

    #@b9d
    const/16 v6, 0x23ff

    #@b9f
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ba2
    aput-object v2, v0, v1

    #@ba4
    const/16 v1, 0xb5

    #@ba6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@ba8
    const/16 v3, 0xb6

    #@baa
    const/16 v4, 0xb4

    #@bac
    const/16 v5, 0x25c0

    #@bae
    const/16 v6, 0x25cf

    #@bb0
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@bb3
    aput-object v2, v0, v1

    #@bb5
    const/16 v1, 0xb6

    #@bb7
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@bb9
    const/16 v3, 0xb7

    #@bbb
    const/16 v4, 0xb5

    #@bbd
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@bc0
    aput-object v2, v0, v1

    #@bc2
    const/16 v1, 0xb7

    #@bc4
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@bc6
    const/16 v3, 0xb8

    #@bc8
    const/16 v4, 0xb6

    #@bca
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@bcd
    aput-object v2, v0, v1

    #@bcf
    const/16 v1, 0xb8

    #@bd1
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@bd3
    const/16 v3, 0xb9

    #@bd5
    const/16 v4, 0xb7

    #@bd7
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@bda
    aput-object v2, v0, v1

    #@bdc
    const/16 v1, 0xb9

    #@bde
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@be0
    const/16 v3, 0xba

    #@be2
    const/16 v4, 0xb8

    #@be4
    const/16 v5, 0x2640

    #@be6
    const/16 v6, 0x265f

    #@be8
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@beb
    aput-object v2, v0, v1

    #@bed
    const/16 v1, 0xba

    #@bef
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@bf1
    const/16 v3, 0xbb

    #@bf3
    const/16 v4, 0xb9

    #@bf5
    const v5, 0x19007

    #@bf8
    const/16 v6, 0x287f

    #@bfa
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@bfd
    aput-object v2, v0, v1

    #@bff
    const/16 v1, 0xbb

    #@c01
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c03
    const/16 v3, 0xbc

    #@c05
    const/16 v4, 0xba

    #@c07
    const/16 v5, 0x5c00

    #@c09
    const/16 v6, 0x5c7f

    #@c0b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c0e
    aput-object v2, v0, v1

    #@c10
    const/16 v1, 0xbc

    #@c12
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c14
    const/16 v3, 0xbd

    #@c16
    const/16 v4, 0xbb

    #@c18
    const/16 v5, 0x3b00

    #@c1a
    const/16 v6, 0x3b7f

    #@c1c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c1f
    aput-object v2, v0, v1

    #@c21
    const/16 v1, 0xbd

    #@c23
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c25
    const/16 v3, 0xbe

    #@c27
    const/16 v4, 0xbc

    #@c29
    const/16 v5, 0x2560

    #@c2b
    const/16 v6, 0x257f

    #@c2d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c30
    aput-object v2, v0, v1

    #@c32
    const/16 v1, 0xbe

    #@c34
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c36
    const/16 v3, 0xbf

    #@c38
    const/16 v4, 0xbd

    #@c3a
    const/16 v5, 0x79a0

    #@c3c
    const/16 v6, 0x79bf

    #@c3e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c41
    aput-object v2, v0, v1

    #@c43
    const/16 v1, 0xbf

    #@c45
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c47
    const/16 v3, 0xc0

    #@c49
    const/16 v4, 0xbe

    #@c4b
    const/16 v5, 0x25a0

    #@c4d
    const/16 v6, 0x25af

    #@c4f
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c52
    aput-object v2, v0, v1

    #@c54
    const/16 v1, 0xc0

    #@c56
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c58
    const/16 v3, 0xc1

    #@c5a
    const/16 v4, 0xbf

    #@c5c
    const/16 v5, 0x5780

    #@c5e
    const/16 v6, 0x5783

    #@c60
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c63
    aput-object v2, v0, v1

    #@c65
    const/16 v1, 0xc1

    #@c67
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c69
    const/16 v3, 0xc2

    #@c6b
    const/16 v4, 0xc0

    #@c6d
    const/16 v5, 0x5a00

    #@c6f
    const/16 v6, 0x5a7f

    #@c71
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c74
    aput-object v2, v0, v1

    #@c76
    const/16 v1, 0xc2

    #@c78
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c7a
    const/16 v3, 0xc3

    #@c7c
    const/16 v4, 0xc1

    #@c7e
    const/16 v5, 0x3c40

    #@c80
    const/16 v6, 0x3c5f

    #@c82
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c85
    aput-object v2, v0, v1

    #@c87
    const/16 v1, 0xc3

    #@c89
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c8b
    const/16 v3, 0xc4

    #@c8d
    const/16 v4, 0xc2

    #@c8f
    const/16 v5, 0x34c0

    #@c91
    const/16 v6, 0x34df

    #@c93
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@c96
    aput-object v2, v0, v1

    #@c98
    const/16 v1, 0xc4

    #@c9a
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@c9c
    const/16 v3, 0xc5

    #@c9e
    const/16 v4, 0xc3

    #@ca0
    const/16 v5, 0x2a68

    #@ca2
    const/16 v6, 0x2a77

    #@ca4
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ca7
    aput-object v2, v0, v1

    #@ca9
    const/16 v1, 0xc5

    #@cab
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@cad
    const/16 v3, 0xc6

    #@caf
    const/16 v4, 0xc4

    #@cb1
    const/16 v5, 0x26a0

    #@cb3
    const/16 v6, 0x26bf

    #@cb5
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@cb8
    aput-object v2, v0, v1

    #@cba
    const/16 v1, 0xc6

    #@cbc
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@cbe
    const/16 v3, 0xc7

    #@cc0
    const/16 v4, 0xc5

    #@cc2
    const/16 v5, 0x2000

    #@cc4
    const/16 v6, 0x201f

    #@cc6
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@cc9
    aput-object v2, v0, v1

    #@ccb
    const/16 v1, 0xc7

    #@ccd
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@ccf
    const/16 v3, 0xc8

    #@cd1
    const/16 v4, 0xc6

    #@cd3
    const/16 v5, 0x2360

    #@cd5
    const/16 v6, 0x237f

    #@cd7
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@cda
    aput-object v2, v0, v1

    #@cdc
    const/16 v1, 0xc8

    #@cde
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@ce0
    const/16 v3, 0xc9

    #@ce2
    const/16 v4, 0xc7

    #@ce4
    const/16 v5, 0x2aa0

    #@ce6
    const/16 v6, 0x2aaf

    #@ce8
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ceb
    aput-object v2, v0, v1

    #@ced
    const/16 v1, 0xc9

    #@cef
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@cf1
    const/16 v3, 0xca

    #@cf3
    const/16 v4, 0xc8

    #@cf5
    const/16 v5, 0x2660

    #@cf7
    const/16 v6, 0x267f

    #@cf9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@cfc
    aput-object v2, v0, v1

    #@cfe
    const/16 v1, 0xca

    #@d00
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d02
    const/16 v3, 0xcb

    #@d04
    const/16 v4, 0xc9

    #@d06
    const/16 v5, 0x2120

    #@d08
    const/16 v6, 0x213f

    #@d0a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d0d
    aput-object v2, v0, v1

    #@d0f
    const/16 v1, 0xcb

    #@d11
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d13
    const/16 v3, 0xcc

    #@d15
    const/16 v4, 0xca

    #@d17
    const/16 v5, 0x3800

    #@d19
    const/16 v6, 0x387f

    #@d1b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d1e
    aput-object v2, v0, v1

    #@d20
    const/16 v1, 0xcc

    #@d22
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d24
    const/16 v3, 0xcd

    #@d26
    const/16 v4, 0xcb

    #@d28
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d2b
    aput-object v2, v0, v1

    #@d2d
    const/16 v1, 0xcd

    #@d2f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d31
    const/16 v3, 0xce

    #@d33
    const/16 v4, 0xcc

    #@d35
    const/16 v5, 0x2a30

    #@d37
    const/16 v6, 0x2a3f

    #@d39
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d3c
    aput-object v2, v0, v1

    #@d3e
    const/16 v1, 0xce

    #@d40
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d42
    const/16 v3, 0xcf

    #@d44
    const/16 v4, 0xcd

    #@d46
    const/16 v5, 0x26c0

    #@d48
    const/16 v6, 0x26df

    #@d4a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d4d
    aput-object v2, v0, v1

    #@d4f
    const/16 v1, 0xcf

    #@d51
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d53
    const/16 v3, 0xd0

    #@d55
    const/16 v4, 0xce

    #@d57
    const/16 v5, 0x3e22

    #@d59
    const/16 v6, 0x3e31

    #@d5b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d5e
    aput-object v2, v0, v1

    #@d60
    const/16 v1, 0xd0

    #@d62
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d64
    const/16 v3, 0xd1

    #@d66
    const/16 v4, 0xcf

    #@d68
    const/16 v5, 0x2c70

    #@d6a
    const/16 v6, 0x2c7f

    #@d6c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d6f
    aput-object v2, v0, v1

    #@d71
    const/16 v1, 0xd1

    #@d73
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d75
    const/16 v3, 0xd2

    #@d77
    const/16 v4, 0xd0

    #@d79
    const/16 v5, 0x2c70

    #@d7b
    const/16 v6, 0x2c7f

    #@d7d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d80
    aput-object v2, v0, v1

    #@d82
    const/16 v1, 0xd2

    #@d84
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d86
    const/16 v3, 0xd3

    #@d88
    const/16 v4, 0xd1

    #@d8a
    const/16 v5, 0x2c70

    #@d8c
    const/16 v6, 0x2c7f

    #@d8e
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@d91
    aput-object v2, v0, v1

    #@d93
    const/16 v1, 0xd3

    #@d95
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@d97
    const/16 v3, 0xd4

    #@d99
    const/16 v4, 0xd2

    #@d9b
    const/16 v5, 0x5880

    #@d9d
    const/16 v6, 0x597f

    #@d9f
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@da2
    aput-object v2, v0, v1

    #@da4
    const/16 v1, 0xd4

    #@da6
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@da8
    const/16 v3, 0xd3

    #@daa
    invoke-direct {v2, v9, v3, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@dad
    aput-object v2, v0, v1

    #@daf
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@db1
    const/16 v2, 0xd6

    #@db3
    const/16 v3, 0xd4

    #@db5
    const/16 v4, 0x79c0

    #@db7
    const/16 v5, 0x79df

    #@db9
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@dbc
    aput-object v1, v0, v9

    #@dbe
    const/16 v1, 0xd6

    #@dc0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@dc2
    const/16 v3, 0xd7

    #@dc4
    const/16 v4, 0x2a50

    #@dc6
    const/16 v5, 0x2a67

    #@dc8
    invoke-direct {v2, v3, v9, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@dcb
    aput-object v2, v0, v1

    #@dcd
    const/16 v1, 0xd7

    #@dcf
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@dd1
    const/16 v3, 0xd8

    #@dd3
    const/16 v4, 0xd6

    #@dd5
    const/16 v5, 0x2ac0

    #@dd7
    const/16 v6, 0x2acf

    #@dd9
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@ddc
    aput-object v2, v0, v1

    #@dde
    const/16 v1, 0xd8

    #@de0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@de2
    const/16 v3, 0xd9

    #@de4
    const/16 v4, 0xd7

    #@de6
    invoke-direct {v2, v3, v4, v7, v7}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@de9
    aput-object v2, v0, v1

    #@deb
    const/16 v1, 0xd9

    #@ded
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@def
    const/16 v3, 0xda

    #@df1
    const/16 v4, 0xd8

    #@df3
    const/16 v5, 0x7b00

    #@df5
    const/16 v6, 0x7b7f

    #@df7
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@dfa
    aput-object v2, v0, v1

    #@dfc
    const/16 v1, 0xda

    #@dfe
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e00
    const/16 v3, 0xdb

    #@e02
    const/16 v4, 0xd9

    #@e04
    const/16 v5, 0x3400

    #@e06
    const/16 v6, 0x347f

    #@e08
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e0b
    aput-object v2, v0, v1

    #@e0d
    const/16 v1, 0xdb

    #@e0f
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e11
    const/16 v3, 0xdc

    #@e13
    const/16 v4, 0xda

    #@e15
    const/16 v5, 0x2af0

    #@e17
    const/16 v6, 0x2aff

    #@e19
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e1c
    aput-object v2, v0, v1

    #@e1e
    const/16 v1, 0xdc

    #@e20
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e22
    const/16 v3, 0xdd

    #@e24
    const/16 v4, 0xdb

    #@e26
    const/16 v5, 0x2c40

    #@e28
    const/16 v6, 0x2c4f

    #@e2a
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e2d
    aput-object v2, v0, v1

    #@e2f
    const/16 v1, 0xdd

    #@e31
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e33
    const/16 v3, 0xde

    #@e35
    const/16 v4, 0xdb

    #@e37
    const/16 v5, 0x2c60

    #@e39
    const/16 v6, 0x2c6f

    #@e3b
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e3e
    aput-object v2, v0, v1

    #@e40
    const/16 v1, 0xde

    #@e42
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e44
    const/16 v3, 0xdf

    #@e46
    const/16 v4, 0xdc

    #@e48
    const/16 v5, 0x2720

    #@e4a
    const/16 v6, 0x273f

    #@e4c
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e4f
    aput-object v2, v0, v1

    #@e51
    const/16 v1, 0xdf

    #@e53
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e55
    const/16 v3, 0xe0

    #@e57
    const/16 v4, 0xdd

    #@e59
    const/16 v5, 0x2760

    #@e5b
    const/16 v6, 0x277f

    #@e5d
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e60
    aput-object v2, v0, v1

    #@e62
    const/16 v1, 0xe0

    #@e64
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e66
    const/16 v3, 0xe1

    #@e68
    const/16 v4, 0x6400

    #@e6a
    const/16 v5, 0x65ff

    #@e6c
    invoke-direct {v2, v3, v10, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e6f
    aput-object v2, v0, v1

    #@e71
    const/16 v1, 0xe1

    #@e73
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e75
    const/16 v3, 0xe2

    #@e77
    const/16 v4, 0x2d24

    #@e79
    const/16 v5, 0x2d4f

    #@e7b
    invoke-direct {v2, v3, v8, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e7e
    aput-object v2, v0, v1

    #@e80
    const/16 v1, 0xe2

    #@e82
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e84
    const/16 v3, 0xe3

    #@e86
    const/16 v4, 0x2d51

    #@e88
    const/16 v5, 0x2fff

    #@e8a
    invoke-direct {v2, v3, v8, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e8d
    aput-object v2, v0, v1

    #@e8f
    const/16 v1, 0xe3

    #@e91
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e93
    const/16 v3, 0xe4

    #@e95
    const/16 v4, 0x2a50

    #@e97
    const/16 v5, 0x2d23

    #@e99
    invoke-direct {v2, v3, v9, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@e9c
    aput-object v2, v0, v1

    #@e9e
    const/16 v1, 0xe4

    #@ea0
    new-instance v2, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@ea2
    const/16 v3, 0xe5

    #@ea4
    const/16 v4, 0x2d50

    #@ea6
    const/16 v5, 0x2d50

    #@ea8
    invoke-direct {v2, v3, v9, v4, v5}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;-><init>(IIII)V

    #@eab
    aput-object v2, v0, v1

    #@ead
    iput-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@eaf
    .line 1750
    :cond_eaf
    return-void
.end method

.method public static isNanpSimplified(Ljava/lang/String;)Z
    .registers 4
    .parameter "dialStr"

    #@0
    .prologue
    .line 1905
    const/4 v0, 0x0

    #@1
    .line 1907
    .local v0, retVal:Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    const/16 v2, 0xa

    #@7
    if-ne v1, v2, :cond_20

    #@9
    .line 1908
    const/4 v1, 0x0

    #@a
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v1

    #@e
    invoke-static {v1}, Landroid/telephony/AssistDialPhoneNumberUtils;->isTwoToNine(C)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_20

    #@14
    .line 1909
    const/4 v1, 0x3

    #@15
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@18
    move-result v1

    #@19
    invoke-static {v1}, Landroid/telephony/AssistDialPhoneNumberUtils;->isTwoToNine(C)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_20

    #@1f
    .line 1910
    const/4 v0, 0x1

    #@20
    .line 1915
    :cond_20
    return v0
.end method

.method private isNeeded_OTAIddPrefix(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Z
    .registers 16
    .parameter "pNumber"
    .parameter "currentCountry"
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 683
    const/4 v4, 0x0

    #@3
    .line 684
    .local v4, refCC:Ljava/lang/String;
    const/4 v2, 0x0

    #@4
    .line 685
    .local v2, otaCC:Ljava/lang/String;
    const/4 v3, 0x0

    #@5
    .line 686
    .local v3, otherCC:Ljava/lang/String;
    const/4 v5, 0x0

    #@6
    .line 688
    .local v5, retVal:Z
    invoke-direct {p0, p3}, Landroid/telephony/AssistDialPhoneNumberUtils;->getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@9
    move-result-object v1

    #@a
    .line 689
    .local v1, mRefer:Landroid/provider/ReferenceCountry;
    if-eqz v1, :cond_10

    #@c
    .line 690
    invoke-virtual {v1}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@f
    move-result-object v4

    #@10
    .line 692
    :cond_10
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    .line 696
    if-eqz v2, :cond_7e

    #@16
    .line 698
    :try_start_16
    const-string v8, "AssistDialPhoneNumberUtils"

    #@18
    new-instance v9, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v10, " isNeeded_OTAIddPrefix() ==> pNumber.substring(0, ota_CC.length()) : "

    #@1f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v9

    #@23
    const/4 v10, 0x0

    #@24
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@27
    move-result v11

    #@28
    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b
    move-result-object v10

    #@2c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v9

    #@30
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v9

    #@34
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 700
    const-string v8, "AssistDialPhoneNumberUtils"

    #@39
    new-instance v9, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v10, " isNeeded_OTAIddPrefix() ==>  ota_CC : "

    #@40
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v9

    #@48
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v9

    #@4c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 702
    const/4 v8, 0x0

    #@50
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@53
    move-result v9

    #@54
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@57
    move-result-object v8

    #@58
    invoke-static {v8, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5b
    move-result v8

    #@5c
    if-ne v8, v7, :cond_7e

    #@5e
    .line 704
    invoke-virtual {p2}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@61
    move-result-object v7

    #@62
    const-string v8, "1"

    #@64
    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@67
    move-result v7

    #@68
    if-nez v7, :cond_7c

    #@6a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6d
    move-result v7

    #@6e
    const/16 v8, 0xa

    #@70
    if-le v7, v8, :cond_7c

    #@72
    .line 707
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@75
    move-result v7

    #@76
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    sput-object v7, Landroid/telephony/AssistDialPhoneNumberUtils;->afterNumber:Ljava/lang/String;

    #@7c
    .line 709
    :cond_7c
    const/4 v5, 0x1

    #@7d
    .line 743
    :goto_7d
    return v6

    #@7e
    .line 714
    :cond_7e
    if-eqz v4, :cond_cb

    #@80
    .line 716
    const-string v6, "AssistDialPhoneNumberUtils"

    #@82
    new-instance v8, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v9, " isNeeded_OTAIddPrefix() ==> pNumber.substring(0, ref_CC.length()) : "

    #@89
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    const/4 v9, 0x0

    #@8e
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@91
    move-result v10

    #@92
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@95
    move-result-object v9

    #@96
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v8

    #@9a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v8

    #@9e
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 718
    const-string v6, "AssistDialPhoneNumberUtils"

    #@a3
    new-instance v8, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v9, " isNeeded_OTAIddPrefix() ==>  ref_CC : "

    #@aa
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v8

    #@ae
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v8

    #@b2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v8

    #@b6
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    .line 719
    const/4 v6, 0x0

    #@ba
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@bd
    move-result v8

    #@be
    invoke-virtual {p1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@c5
    move-result v6

    #@c6
    if-ne v6, v7, :cond_cb

    #@c8
    .line 720
    const/4 v5, 0x1

    #@c9
    move v6, v7

    #@ca
    .line 721
    goto :goto_7d

    #@cb
    .line 725
    :cond_cb
    if-nez v5, :cond_fd

    #@cd
    .line 727
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@cf
    if-eqz v6, :cond_fd

    #@d1
    .line 728
    const/4 v0, 0x0

    #@d2
    .local v0, i:I
    :goto_d2
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@d4
    array-length v6, v6

    #@d5
    if-ge v0, v6, :cond_fd

    #@d7
    .line 730
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@d9
    aget-object v6, v6, v0

    #@db
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@de
    move-result-object v3

    #@df
    .line 731
    if-eqz v3, :cond_f9

    #@e1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@e4
    move-result v6

    #@e5
    if-lez v6, :cond_f9

    #@e7
    .line 732
    const/4 v6, 0x0

    #@e8
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@eb
    move-result v8

    #@ec
    invoke-virtual {p1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ef
    move-result-object v6

    #@f0
    invoke-static {v6, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_f3
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_16 .. :try_end_f3} :catch_fc

    #@f3
    move-result v6

    #@f4
    if-ne v6, v7, :cond_f9

    #@f6
    .line 733
    const/4 v5, 0x1

    #@f7
    move v6, v7

    #@f8
    .line 734
    goto :goto_7d

    #@f9
    .line 728
    :cond_f9
    add-int/lit8 v0, v0, 0x1

    #@fb
    goto :goto_d2

    #@fc
    .line 740
    .end local v0           #i:I
    :catch_fc
    move-exception v6

    #@fd
    :cond_fd
    move v6, v5

    #@fe
    .line 743
    goto/16 :goto_7d
.end method

.method private static isTwoToNine(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 1893
    const/16 v0, 0x32

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_a

    #@8
    .line 1894
    const/4 v0, 0x1

    #@9
    .line 1896
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private loadAllCountryForAssistedDial(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 450
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 451
    .local v0, mContentResolver:Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@7
    sget-object v2, Landroid/provider/Settings$AssistDial;->PROJECTION:[Ljava/lang/String;

    #@9
    move-object v4, v3

    #@a
    move-object v5, v3

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v6

    #@f
    .line 456
    .local v6, cursor:Landroid/database/Cursor;
    if-nez v6, :cond_19

    #@11
    .line 457
    const-string v1, "AssistDialPhoneNumberUtils"

    #@13
    const-string v2, "cursor is null"

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 519
    :cond_18
    :goto_18
    return-void

    #@19
    .line 461
    :cond_19
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@1c
    move-result v1

    #@1d
    new-array v1, v1, [Landroid/provider/ReferenceCountry;

    #@1f
    iput-object v1, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@21
    .line 463
    const-string v1, "AssistDialPhoneNumberUtils"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, " cursor.getCount()  : "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@31
    move-result v3

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 466
    :try_start_3d
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@40
    move-result v1

    #@41
    if-eqz v1, :cond_d3

    #@43
    .line 467
    const/4 v8, 0x0

    #@44
    .local v8, i:I
    :goto_44
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@47
    move-result v1

    #@48
    if-ge v8, v1, :cond_d3

    #@4a
    .line 469
    new-instance v9, Landroid/provider/ReferenceCountry;

    #@4c
    invoke-direct {v9}, Landroid/provider/ReferenceCountry;-><init>()V

    #@4f
    .line 471
    .local v9, mRc:Landroid/provider/ReferenceCountry;
    const-string v1, "countryindex"

    #@51
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@54
    move-result v1

    #@55
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    #@58
    move-result v1

    #@59
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setIndex(I)V

    #@5c
    .line 475
    const-string v1, "countryname"

    #@5e
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@61
    move-result v1

    #@62
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setCountryName(Ljava/lang/String;)V

    #@69
    .line 479
    const-string/jumbo v1, "mcc"

    #@6c
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6f
    move-result v1

    #@70
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setMccCode(Ljava/lang/String;)V

    #@77
    .line 483
    const-string v1, "countrycode"

    #@79
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@7c
    move-result v1

    #@7d
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@80
    move-result-object v1

    #@81
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setCountryCode(Ljava/lang/String;)V

    #@84
    .line 487
    const-string v1, "iddprefix"

    #@86
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@89
    move-result v1

    #@8a
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8d
    move-result-object v1

    #@8e
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setIddPrefix(Ljava/lang/String;)V

    #@91
    .line 491
    const-string/jumbo v1, "nddprefix"

    #@94
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@97
    move-result v1

    #@98
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9b
    move-result-object v1

    #@9c
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setNddPrefix(Ljava/lang/String;)V

    #@9f
    .line 495
    const-string/jumbo v1, "nanp"

    #@a2
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@a5
    move-result v1

    #@a6
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a9
    move-result-object v1

    #@aa
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setNanp(Ljava/lang/String;)V

    #@ad
    .line 499
    const-string v1, "area"

    #@af
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@b2
    move-result v1

    #@b3
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b6
    move-result-object v1

    #@b7
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setAreaCode(Ljava/lang/String;)V

    #@ba
    .line 503
    const-string/jumbo v1, "length"

    #@bd
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@c0
    move-result v1

    #@c1
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c4
    move-result-object v1

    #@c5
    invoke-virtual {v9, v1}, Landroid/provider/ReferenceCountry;->setNumLength(Ljava/lang/String;)V

    #@c8
    .line 507
    iget-object v1, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@ca
    aput-object v9, v1, v8

    #@cc
    .line 509
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_cf
    .catchall {:try_start_3d .. :try_end_cf} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_cf} :catch_da

    #@cf
    .line 467
    add-int/lit8 v8, v8, 0x1

    #@d1
    goto/16 :goto_44

    #@d3
    .line 516
    .end local v8           #i:I
    .end local v9           #mRc:Landroid/provider/ReferenceCountry;
    :cond_d3
    if-eqz v6, :cond_18

    #@d5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@d8
    goto/16 :goto_18

    #@da
    .line 513
    :catch_da
    move-exception v7

    #@db
    .line 514
    .local v7, e:Ljava/lang/Exception;
    :try_start_db
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_de
    .catchall {:try_start_db .. :try_end_de} :catchall_e5

    #@de
    .line 516
    if-eqz v6, :cond_18

    #@e0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@e3
    goto/16 :goto_18

    #@e5
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_e5
    move-exception v1

    #@e6
    if-eqz v6, :cond_eb

    #@e8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@eb
    :cond_eb
    throw v1
.end method

.method private reCreateReferenceCountryList(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 435
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v1

    #@5
    .line 436
    .local v1, mContentResolver:Landroid/content/ContentResolver;
    sget-object v2, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@7
    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@a
    .line 437
    const-string v2, "assist_dial_check"

    #@c
    const/4 v3, 0x0

    #@d
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@10
    .line 438
    const-string v2, "area"

    #@12
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 440
    .local v0, mAreaCode:Ljava/lang/String;
    const-string v2, "AssistDialPhoneNumberUtils"

    #@18
    const-string/jumbo v3, "reCreateReferenceCountryList - reCreate assist_dail DB"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 442
    if-eqz v0, :cond_29

    #@20
    const-string v2, ""

    #@22
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@25
    move-result v2

    #@26
    const/4 v3, 0x1

    #@27
    if-ne v2, v3, :cond_2d

    #@29
    .line 443
    :cond_29
    invoke-static {v1}, Landroid/provider/Settings$AssistDial;->initAssistDialCountryDetailList(Landroid/content/ContentResolver;)V

    #@2c
    .line 447
    :goto_2c
    return-void

    #@2d
    .line 445
    :cond_2d
    invoke-static {v1, v0}, Landroid/provider/Settings$AssistDial;->initAssistDialCountryDetailList(Landroid/content/ContentResolver;Ljava/lang/String;)V

    #@30
    goto :goto_2c
.end method


# virtual methods
.method public convertToCdma611Number(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .registers 8
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    .line 1871
    const/4 v1, 0x0

    #@1
    .line 1872
    .local v1, IddNumber:Ljava/lang/String;
    const-string v0, "19085594899"

    #@3
    .line 1874
    .local v0, Cdma611Number:Ljava/lang/String;
    const-string v2, "AssistDialPhoneNumberUtils"

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "convertToCdma611Number()... before number "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1876
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->isCdmaVzWNetwork(Landroid/content/Context;)Z

    #@1e
    move-result v2

    #@1f
    if-nez v2, :cond_3c

    #@21
    .line 1877
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->isInternationalCdmaIdd(Landroid/content/Context;)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    .line 1878
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v2

    #@29
    if-nez v2, :cond_55

    #@2b
    .line 1879
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object p1

    #@3c
    .line 1885
    :cond_3c
    :goto_3c
    const-string v2, "AssistDialPhoneNumberUtils"

    #@3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "convertToCdma611Number()... after number = "

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 1886
    return-object p1

    #@55
    .line 1881
    :cond_55
    move-object p1, v0

    #@56
    goto :goto_3c
.end method

.method public convertToCdmaVoiceMailNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .registers 7
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    .line 1854
    const/4 v0, 0x0

    #@1
    .line 1855
    .local v0, IddNumber:Ljava/lang/String;
    const-string v1, "AssistDialPhoneNumberUtils"

    #@3
    const-string v2, "convertToCdmaVoiceMailNumber()..."

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1857
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->isCdmaVzWNetwork(Landroid/content/Context;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_37

    #@e
    .line 1858
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->isInternationalCdmaIdd(Landroid/content/Context;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1859
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_50

    #@18
    .line 1860
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "1"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object p1

    #@37
    .line 1866
    :cond_37
    :goto_37
    const-string v1, "AssistDialPhoneNumberUtils"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "convertToCdmaVoiceMailNumber()... number = "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1867
    return-object p1

    #@50
    .line 1862
    :cond_50
    new-instance v1, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v2, "1"

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object p1

    #@6b
    goto :goto_37
.end method

.method public getAreacodeInfo(Ljava/lang/String;)Z
    .registers 9
    .parameter "pNumber"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 376
    const/4 v1, 0x0

    #@3
    .line 377
    .local v1, i:I
    const/4 v0, 0x0

    #@4
    .line 379
    .local v0, areacode:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7
    move-result v4

    #@8
    const/16 v5, 0xa

    #@a
    if-le v4, v5, :cond_a0

    #@c
    .line 380
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v4

    #@10
    const/16 v5, 0x31

    #@12
    if-ne v4, v5, :cond_96

    #@14
    .line 381
    const/4 v4, 0x4

    #@15
    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 385
    :goto_19
    const-string v4, "AssistDialPhoneNumberUtils"

    #@1b
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v6, " getAreacodeInfo(String pNumber) ==> areacode : "

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 387
    const-string v4, "AssistDialPhoneNumberUtils"

    #@33
    new-instance v5, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v6, " getAreacodeInfo(String pNumber) ==> areacodeInfo.toString() : "

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@40
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 390
    const/4 v1, 0x0

    #@50
    :goto_50
    iget-object v4, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@52
    array-length v4, v4

    #@53
    if-ge v1, v4, :cond_a0

    #@55
    .line 391
    const-string v4, "AssistDialPhoneNumberUtils"

    #@57
    new-instance v5, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v6, " getAreacodeInfo(String pNumber) ==> areacodeInfo[i].getAreacode() : "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@64
    aget-object v6, v6, v1

    #@66
    invoke-virtual {v6}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->getAreacode()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    const-string v6, "  :  "

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@76
    aget-object v6, v6, v1

    #@78
    invoke-virtual {v6}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->getCityname()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 395
    iget-object v4, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@89
    aget-object v4, v4, v1

    #@8b
    invoke-virtual {v4}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->getAreacode()Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@92
    move-result v4

    #@93
    if-eqz v4, :cond_9d

    #@95
    .line 400
    :goto_95
    return v2

    #@96
    .line 383
    :cond_96
    const/4 v4, 0x3

    #@97
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9a
    move-result-object v0

    #@9b
    goto/16 :goto_19

    #@9d
    .line 390
    :cond_9d
    add-int/lit8 v1, v1, 0x1

    #@9f
    goto :goto_50

    #@a0
    :cond_a0
    move v2, v3

    #@a1
    .line 400
    goto :goto_95
.end method

.method public getAreacodeInfoforContactNumber(Ljava/lang/String;)Z
    .registers 9
    .parameter "pNumber"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 404
    const/4 v1, 0x0

    #@3
    .line 405
    .local v1, i:I
    const/4 v0, 0x0

    #@4
    .line 407
    .local v0, areacode:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7
    move-result v4

    #@8
    const/16 v5, 0xa

    #@a
    if-lt v4, v5, :cond_a0

    #@c
    .line 408
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v4

    #@10
    const/16 v5, 0x31

    #@12
    if-ne v4, v5, :cond_96

    #@14
    .line 409
    const/4 v4, 0x4

    #@15
    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 413
    :goto_19
    const-string v4, "AssistDialPhoneNumberUtils"

    #@1b
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v6, " getAreacodeInfoContactNumber(String pNumber) ==> areacode : "

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 415
    const-string v4, "AssistDialPhoneNumberUtils"

    #@33
    new-instance v5, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v6, " getAreacodeInfoContactNumber(String pNumber) ==> areacodeInfo.toString() : "

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@40
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 418
    const/4 v1, 0x0

    #@50
    :goto_50
    iget-object v4, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@52
    array-length v4, v4

    #@53
    if-ge v1, v4, :cond_a0

    #@55
    .line 419
    const-string v4, "AssistDialPhoneNumberUtils"

    #@57
    new-instance v5, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v6, " getAreacodeInfoContactNumber(String pNumber) ==> areacodeInfo[i].getAreacode() : "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@64
    aget-object v6, v6, v1

    #@66
    invoke-virtual {v6}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->getAreacode()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    const-string v6, "  :  "

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    iget-object v6, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@76
    aget-object v6, v6, v1

    #@78
    invoke-virtual {v6}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->getCityname()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 423
    iget-object v4, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->areacodeInfo:[Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;

    #@89
    aget-object v4, v4, v1

    #@8b
    invoke-virtual {v4}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->getAreacode()Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@92
    move-result v4

    #@93
    if-eqz v4, :cond_9d

    #@95
    .line 428
    :goto_95
    return v2

    #@96
    .line 411
    :cond_96
    const/4 v4, 0x3

    #@97
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9a
    move-result-object v0

    #@9b
    goto/16 :goto_19

    #@9d
    .line 418
    :cond_9d
    add-int/lit8 v1, v1, 0x1

    #@9f
    goto :goto_50

    #@a0
    :cond_a0
    move v2, v3

    #@a1
    .line 428
    goto :goto_95
.end method

.method public getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;
    .registers 18
    .parameter "context"

    #@0
    .prologue
    .line 234
    const/4 v4, 0x0

    #@1
    .line 235
    .local v4, currentCountry:Landroid/provider/ReferenceCountry;
    const/16 v2, 0x3ff

    #@3
    .line 237
    .local v2, beforeMcc:I
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v13

    #@7
    const-string v14, "assist_dial_ota_mcc"

    #@9
    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_3 .. :try_end_c} :catch_67

    #@c
    move-result v2

    #@d
    .line 243
    :goto_d
    const-string/jumbo v13, "phone"

    #@10
    move-object/from16 v0, p1

    #@12
    invoke-virtual {v0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@15
    move-result-object v11

    #@16
    check-cast v11, Landroid/telephony/TelephonyManager;

    #@18
    .line 246
    .local v11, telephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1b
    move-result-object v9

    #@1c
    .line 248
    .local v9, mcc:Ljava/lang/String;
    const-string v13, "AssistDialPhoneNumberUtils"

    #@1e
    new-instance v14, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v15, " ***** getAssistedDialCurrentCountry() Real ==> MCC : "

    #@25
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v14

    #@29
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v14

    #@2d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v14

    #@31
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 250
    move-object/from16 v0, p0

    #@36
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@38
    if-nez v13, :cond_3d

    #@3a
    .line 252
    invoke-direct/range {p0 .. p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->loadAllCountryForAssistedDial(Landroid/content/Context;)V

    #@3d
    .line 255
    :cond_3d
    move-object/from16 v0, p0

    #@3f
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@41
    if-eqz v13, :cond_4a

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@47
    array-length v13, v13

    #@48
    if-nez v13, :cond_6c

    #@4a
    .line 256
    :cond_4a
    invoke-direct/range {p0 .. p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->reCreateReferenceCountryList(Landroid/content/Context;)V

    #@4d
    .line 257
    invoke-direct/range {p0 .. p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->loadAllCountryForAssistedDial(Landroid/content/Context;)V

    #@50
    .line 258
    move-object/from16 v0, p0

    #@52
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@54
    if-eqz v13, :cond_5d

    #@56
    move-object/from16 v0, p0

    #@58
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@5a
    array-length v13, v13

    #@5b
    if-nez v13, :cond_6c

    #@5d
    .line 259
    :cond_5d
    const-string v13, "AssistDialPhoneNumberUtils"

    #@5f
    const-string/jumbo v14, "mRefCountryList is not vaild"

    #@62
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    move-object v5, v4

    #@66
    .line 350
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    .local v5, currentCountry:Landroid/provider/ReferenceCountry;
    :goto_66
    return-object v5

    #@67
    .line 238
    .end local v5           #currentCountry:Landroid/provider/ReferenceCountry;
    .end local v9           #mcc:Ljava/lang/String;
    .end local v11           #telephonyManager:Landroid/telephony/TelephonyManager;
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    :catch_67
    move-exception v6

    #@68
    .line 240
    .local v6, e:Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v6}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    #@6b
    goto :goto_d

    #@6c
    .line 264
    .end local v6           #e:Landroid/provider/Settings$SettingNotFoundException;
    .restart local v9       #mcc:Ljava/lang/String;
    .restart local v11       #telephonyManager:Landroid/telephony/TelephonyManager;
    :cond_6c
    invoke-virtual {v11}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@6f
    move-result v13

    #@70
    const/4 v14, 0x1

    #@71
    if-ne v13, v14, :cond_138

    #@73
    .line 265
    const-string v13, "AssistDialPhoneNumberUtils"

    #@75
    const-string v14, " ***** PHONE_TYPE_GSM"

    #@77
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 266
    const/4 v7, 0x0

    #@7b
    .local v7, i:I
    :goto_7b
    move-object/from16 v0, p0

    #@7d
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@7f
    array-length v13, v13

    #@80
    if-ge v7, v13, :cond_cf

    #@82
    .line 267
    move-object/from16 v0, p0

    #@84
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@86
    aget-object v13, v13, v7

    #@88
    invoke-virtual {v13}, Landroid/provider/ReferenceCountry;->getMccCode()Ljava/lang/String;

    #@8b
    move-result-object v12

    #@8c
    .line 268
    .local v12, tempMcc:Ljava/lang/String;
    const-string v13, ","

    #@8e
    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@91
    move-result-object v1

    #@92
    .line 269
    .local v1, allMccForOneCountry:[Ljava/lang/String;
    const/4 v8, 0x0

    #@93
    .local v8, j:I
    :goto_93
    array-length v13, v1

    #@94
    if-ge v8, v13, :cond_cc

    #@96
    .line 270
    aget-object v13, v1, v8

    #@98
    invoke-static {v9, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@9b
    move-result v13

    #@9c
    if-eqz v13, :cond_c9

    #@9e
    .line 271
    new-instance v4, Landroid/provider/ReferenceCountry;

    #@a0
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    invoke-direct {v4}, Landroid/provider/ReferenceCountry;-><init>()V

    #@a3
    .line 272
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    move-object/from16 v0, p0

    #@a5
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@a7
    aget-object v4, v13, v7

    #@a9
    .line 273
    if-eqz v4, :cond_c7

    #@ab
    .line 274
    const-string v13, "AssistDialPhoneNumberUtils"

    #@ad
    new-instance v14, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v15, " ***USE MCC*** correntCountry  ===> "

    #@b4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v14

    #@b8
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@bb
    move-result-object v15

    #@bc
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v14

    #@c0
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v14

    #@c4
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    :cond_c7
    move-object v5, v4

    #@c8
    .line 276
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v5       #currentCountry:Landroid/provider/ReferenceCountry;
    goto :goto_66

    #@c9
    .line 269
    .end local v5           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    :cond_c9
    add-int/lit8 v8, v8, 0x1

    #@cb
    goto :goto_93

    #@cc
    .line 266
    :cond_cc
    add-int/lit8 v7, v7, 0x1

    #@ce
    goto :goto_7b

    #@cf
    .line 281
    .end local v1           #allMccForOneCountry:[Ljava/lang/String;
    .end local v8           #j:I
    .end local v12           #tempMcc:Ljava/lang/String;
    :cond_cf
    const/4 v3, 0x0

    #@d0
    .line 284
    .local v3, countrySID:I
    :try_start_d0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d3
    move-result-object v13

    #@d4
    const-string v14, "assist_dial_ota_sid"

    #@d6
    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_d9
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_d0 .. :try_end_d9} :catch_130

    #@d9
    move-result v3

    #@da
    .line 292
    :goto_da
    const/4 v7, 0x0

    #@db
    :goto_db
    move-object/from16 v0, p0

    #@dd
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@df
    array-length v13, v13

    #@e0
    if-ge v7, v13, :cond_207

    #@e2
    .line 293
    move-object/from16 v0, p0

    #@e4
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@e6
    aget-object v13, v13, v7

    #@e8
    invoke-virtual {v13}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->getStart()I

    #@eb
    move-result v13

    #@ec
    if-lt v3, v13, :cond_135

    #@ee
    move-object/from16 v0, p0

    #@f0
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@f2
    aget-object v13, v13, v7

    #@f4
    invoke-virtual {v13}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->getEnd()I

    #@f7
    move-result v13

    #@f8
    if-gt v3, v13, :cond_135

    #@fa
    .line 295
    move-object/from16 v0, p0

    #@fc
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@fe
    aget-object v13, v13, v7

    #@100
    invoke-virtual {v13}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->getCountryIndex()I

    #@103
    move-result v10

    #@104
    .line 296
    .local v10, otaIndex:I
    new-instance v4, Landroid/provider/ReferenceCountry;

    #@106
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    invoke-direct {v4}, Landroid/provider/ReferenceCountry;-><init>()V

    #@109
    .line 297
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    move-object/from16 v0, p0

    #@10b
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@10d
    aget-object v4, v13, v10

    #@10f
    .line 298
    if-eqz v4, :cond_12d

    #@111
    .line 299
    const-string v13, "AssistDialPhoneNumberUtils"

    #@113
    new-instance v14, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v15, " ***USE MCC*** correntCountry  ===> "

    #@11a
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v14

    #@11e
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@121
    move-result-object v15

    #@122
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v14

    #@126
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@129
    move-result-object v14

    #@12a
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    :cond_12d
    move-object v5, v4

    #@12e
    .line 301
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v5       #currentCountry:Landroid/provider/ReferenceCountry;
    goto/16 :goto_66

    #@130
    .line 287
    .end local v5           #currentCountry:Landroid/provider/ReferenceCountry;
    .end local v10           #otaIndex:I
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    :catch_130
    move-exception v6

    #@131
    .line 289
    .restart local v6       #e:Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v6}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    #@134
    goto :goto_da

    #@135
    .line 292
    .end local v6           #e:Landroid/provider/Settings$SettingNotFoundException;
    :cond_135
    add-int/lit8 v7, v7, 0x1

    #@137
    goto :goto_db

    #@138
    .line 305
    .end local v3           #countrySID:I
    .end local v7           #i:I
    :cond_138
    invoke-virtual {v11}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@13b
    move-result v13

    #@13c
    const/4 v14, 0x2

    #@13d
    if-ne v13, v14, :cond_207

    #@13f
    .line 306
    const-string v13, "AssistDialPhoneNumberUtils"

    #@141
    const-string v14, " ***** PHONE_TYPE_CDMA"

    #@143
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 307
    const/4 v3, 0x0

    #@147
    .line 310
    .restart local v3       #countrySID:I
    :try_start_147
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@14a
    move-result-object v13

    #@14b
    const-string v14, "assist_dial_ota_sid"

    #@14d
    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_150
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_147 .. :try_end_150} :catch_1a9

    #@150
    move-result v3

    #@151
    .line 318
    :goto_151
    if-eqz v3, :cond_1b1

    #@153
    .line 319
    const/4 v7, 0x0

    #@154
    .restart local v7       #i:I
    :goto_154
    move-object/from16 v0, p0

    #@156
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@158
    array-length v13, v13

    #@159
    if-ge v7, v13, :cond_1b1

    #@15b
    .line 320
    move-object/from16 v0, p0

    #@15d
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@15f
    aget-object v13, v13, v7

    #@161
    invoke-virtual {v13}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->getStart()I

    #@164
    move-result v13

    #@165
    if-lt v3, v13, :cond_1ae

    #@167
    move-object/from16 v0, p0

    #@169
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@16b
    aget-object v13, v13, v7

    #@16d
    invoke-virtual {v13}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->getEnd()I

    #@170
    move-result v13

    #@171
    if-gt v3, v13, :cond_1ae

    #@173
    .line 322
    move-object/from16 v0, p0

    #@175
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->countrySIDDataTable:[Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;

    #@177
    aget-object v13, v13, v7

    #@179
    invoke-virtual {v13}, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->getCountryIndex()I

    #@17c
    move-result v10

    #@17d
    .line 323
    .restart local v10       #otaIndex:I
    new-instance v4, Landroid/provider/ReferenceCountry;

    #@17f
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    invoke-direct {v4}, Landroid/provider/ReferenceCountry;-><init>()V

    #@182
    .line 324
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    move-object/from16 v0, p0

    #@184
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@186
    aget-object v4, v13, v10

    #@188
    .line 325
    if-eqz v4, :cond_1a6

    #@18a
    .line 326
    const-string v13, "AssistDialPhoneNumberUtils"

    #@18c
    new-instance v14, Ljava/lang/StringBuilder;

    #@18e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@191
    const-string v15, " ***USE SID*** correntCountry  ===> "

    #@193
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v14

    #@197
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@19a
    move-result-object v15

    #@19b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v14

    #@19f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a2
    move-result-object v14

    #@1a3
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a6
    :cond_1a6
    move-object v5, v4

    #@1a7
    .line 328
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v5       #currentCountry:Landroid/provider/ReferenceCountry;
    goto/16 :goto_66

    #@1a9
    .line 313
    .end local v5           #currentCountry:Landroid/provider/ReferenceCountry;
    .end local v7           #i:I
    .end local v10           #otaIndex:I
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    :catch_1a9
    move-exception v6

    #@1aa
    .line 315
    .restart local v6       #e:Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v6}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    #@1ad
    goto :goto_151

    #@1ae
    .line 319
    .end local v6           #e:Landroid/provider/Settings$SettingNotFoundException;
    .restart local v7       #i:I
    :cond_1ae
    add-int/lit8 v7, v7, 0x1

    #@1b0
    goto :goto_154

    #@1b1
    .line 333
    .end local v7           #i:I
    :cond_1b1
    const/4 v7, 0x0

    #@1b2
    .restart local v7       #i:I
    :goto_1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@1b6
    array-length v13, v13

    #@1b7
    if-ge v7, v13, :cond_207

    #@1b9
    .line 334
    move-object/from16 v0, p0

    #@1bb
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@1bd
    aget-object v13, v13, v7

    #@1bf
    invoke-virtual {v13}, Landroid/provider/ReferenceCountry;->getMccCode()Ljava/lang/String;

    #@1c2
    move-result-object v12

    #@1c3
    .line 335
    .restart local v12       #tempMcc:Ljava/lang/String;
    const-string v13, ","

    #@1c5
    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1c8
    move-result-object v1

    #@1c9
    .line 336
    .restart local v1       #allMccForOneCountry:[Ljava/lang/String;
    const/4 v8, 0x0

    #@1ca
    .restart local v8       #j:I
    :goto_1ca
    array-length v13, v1

    #@1cb
    if-ge v8, v13, :cond_204

    #@1cd
    .line 337
    aget-object v13, v1, v8

    #@1cf
    invoke-static {v9, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1d2
    move-result v13

    #@1d3
    if-eqz v13, :cond_201

    #@1d5
    .line 338
    new-instance v4, Landroid/provider/ReferenceCountry;

    #@1d7
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    invoke-direct {v4}, Landroid/provider/ReferenceCountry;-><init>()V

    #@1da
    .line 339
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    move-object/from16 v0, p0

    #@1dc
    iget-object v13, v0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@1de
    aget-object v4, v13, v7

    #@1e0
    .line 340
    if-eqz v4, :cond_1fe

    #@1e2
    .line 341
    const-string v13, "AssistDialPhoneNumberUtils"

    #@1e4
    new-instance v14, Ljava/lang/StringBuilder;

    #@1e6
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1e9
    const-string v15, " ***USE MCC*** correntCountry  ===> "

    #@1eb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v14

    #@1ef
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@1f2
    move-result-object v15

    #@1f3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v14

    #@1f7
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fa
    move-result-object v14

    #@1fb
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1fe
    :cond_1fe
    move-object v5, v4

    #@1ff
    .line 343
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v5       #currentCountry:Landroid/provider/ReferenceCountry;
    goto/16 :goto_66

    #@201
    .line 336
    .end local v5           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v4       #currentCountry:Landroid/provider/ReferenceCountry;
    :cond_201
    add-int/lit8 v8, v8, 0x1

    #@203
    goto :goto_1ca

    #@204
    .line 333
    :cond_204
    add-int/lit8 v7, v7, 0x1

    #@206
    goto :goto_1b2

    #@207
    .end local v1           #allMccForOneCountry:[Ljava/lang/String;
    .end local v3           #countrySID:I
    .end local v7           #i:I
    .end local v8           #j:I
    .end local v12           #tempMcc:Ljava/lang/String;
    :cond_207
    move-object v5, v4

    #@208
    .line 350
    .end local v4           #currentCountry:Landroid/provider/ReferenceCountry;
    .restart local v5       #currentCountry:Landroid/provider/ReferenceCountry;
    goto/16 :goto_66
.end method

.method public getAssitedDialFinalNumberForCDMA(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .registers 8
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    .line 148
    const-string v2, "AssistDialPhoneNumberUtils"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "before Assisted Dial Number: "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 150
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "assist_dial_new_number_check"

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@22
    .line 153
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@25
    move-result v2

    #@26
    const/4 v3, 0x7

    #@27
    if-ge v2, v3, :cond_43

    #@29
    .line 155
    const-string v2, "AssistDialPhoneNumberUtils"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "after Assisted Dial Number: "

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    move-object v1, p1

    #@42
    .line 182
    .end local p1
    .local v1, number:Ljava/lang/String;
    :goto_42
    return-object v1

    #@43
    .line 159
    .end local v1           #number:Ljava/lang/String;
    .restart local p1
    :cond_43
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@46
    move-result-object v0

    #@47
    .line 160
    .local v0, currentCountry:Landroid/provider/ReferenceCountry;
    if-nez v0, :cond_50

    #@49
    .line 162
    const-string v2, "AssistedDialPhoneNumberUtils"

    #@4b
    const-string v3, " ****** currentCountry ===> null "

    #@4d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 164
    :cond_50
    if-eqz v0, :cond_93

    #@52
    .line 165
    invoke-virtual {v0}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    const-string v3, "1"

    #@58
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5b
    move-result v2

    #@5c
    const/4 v3, 0x1

    #@5d
    if-ne v2, v3, :cond_87

    #@5f
    .line 167
    const-string v2, "AssistDialPhoneNumberUtils"

    #@61
    const-string v3, " VZW NEWWORK"

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 168
    invoke-direct {p0, p1, v0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->callAssistedDialSetVZWNetwork(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Ljava/lang/String;

    #@69
    move-result-object p1

    #@6a
    .line 178
    :goto_6a
    const-string v2, "AssistDialPhoneNumberUtils"

    #@6c
    new-instance v3, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v4, "after Assisted Dial Number: "

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 180
    const/4 v2, 0x0

    #@83
    iput-object v2, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@85
    move-object v1, p1

    #@86
    .line 182
    .end local p1
    .restart local v1       #number:Ljava/lang/String;
    goto :goto_42

    #@87
    .line 171
    .end local v1           #number:Ljava/lang/String;
    .restart local p1
    :cond_87
    const-string v2, "AssistDialPhoneNumberUtils"

    #@89
    const-string v3, " INT NETWORK"

    #@8b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 172
    invoke-direct {p0, p1, v0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->callAssistedDialSetIntNetwork(Ljava/lang/String;Landroid/provider/ReferenceCountry;Landroid/content/Context;)Ljava/lang/String;

    #@91
    move-result-object p1

    #@92
    goto :goto_6a

    #@93
    .line 175
    :cond_93
    const-string v2, "AssistDialPhoneNumberUtils"

    #@95
    const-string v3, "currentCountry ===> null"

    #@97
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    goto :goto_6a
.end method

.method public getAssitedDialFinalNumberForGSM(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .registers 16
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    .line 40
    const/4 v9, 0x0

    #@1
    .line 41
    .local v9, ret_val:Z
    const-string v10, "AssistDialPhoneNumberUtils"

    #@3
    new-instance v11, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v12, "before Assisted Dial Number: "

    #@a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v11

    #@e
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v11

    #@12
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v11

    #@16
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 42
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->getIsAssistedDialCheck(Landroid/content/Context;)I

    #@1c
    move-result v0

    #@1d
    .line 43
    .local v0, assistDialCheck:I
    const/4 v2, 0x0

    #@1e
    .line 44
    .local v2, dialframContact:I
    if-eqz v0, :cond_27

    #@20
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@23
    move-result v10

    #@24
    const/4 v11, 0x7

    #@25
    if-ge v10, v11, :cond_48

    #@27
    .line 46
    :cond_27
    const-string v10, "AssistDialPhoneNumberUtils"

    #@29
    const-string v11, " assistDialCheck==0 || number.length()<7 "

    #@2b
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 47
    const-string v10, "AssistDialPhoneNumberUtils"

    #@30
    new-instance v11, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v12, "after Assisted Dial Number: "

    #@37
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v11

    #@3b
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v11

    #@3f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v11

    #@43
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    move-object v6, p1

    #@47
    .line 140
    .end local p1
    .local v6, number:Ljava/lang/String;
    :goto_47
    return-object v6

    #@48
    .line 51
    .end local v6           #number:Ljava/lang/String;
    .restart local p1
    :cond_48
    invoke-virtual {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@4b
    move-result-object v1

    #@4c
    .line 53
    .local v1, currentCountry:Landroid/provider/ReferenceCountry;
    if-eqz v1, :cond_23c

    #@4e
    .line 55
    const-string v10, "AssistDialPhoneNumberUtils"

    #@50
    new-instance v11, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v12, " Assited Dial for GSM  : before Number :  "

    #@57
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v11

    #@5b
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v11

    #@5f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v11

    #@63
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 56
    const/4 v7, 0x0

    #@67
    .line 57
    .local v7, ref_Idd:Ljava/lang/String;
    invoke-direct {p0, p2}, Landroid/telephony/AssistDialPhoneNumberUtils;->getRefCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@6a
    move-result-object v4

    #@6b
    .line 58
    .local v4, mRefer:Landroid/provider/ReferenceCountry;
    if-eqz v4, :cond_233

    #@6d
    .line 59
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@70
    move-result-object v7

    #@71
    .line 61
    const-string v10, "AssistDialPhoneNumberUtils"

    #@73
    new-instance v11, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v12, "before Assisted Dial ref_Idd: "

    #@7a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v11

    #@7e
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v11

    #@82
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v11

    #@86
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 62
    if-eqz v7, :cond_c1

    #@8b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8e
    move-result v10

    #@8f
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@92
    move-result v11

    #@93
    if-lt v10, v11, :cond_c1

    #@95
    .line 64
    const/4 v10, 0x0

    #@96
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@99
    move-result v11

    #@9a
    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9d
    move-result-object v10

    #@9e
    invoke-static {v10, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@a1
    move-result v10

    #@a2
    const/4 v11, 0x1

    #@a3
    if-ne v10, v11, :cond_c1

    #@a5
    .line 66
    new-instance v10, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v11, "+"

    #@ac
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v10

    #@b0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@b3
    move-result v11

    #@b4
    invoke-virtual {p1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@b7
    move-result-object v11

    #@b8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v10

    #@bc
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object p1

    #@c0
    .line 67
    const/4 v9, 0x1

    #@c1
    .line 72
    :cond_c1
    :try_start_c1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c4
    move-result-object v10

    #@c5
    const-string v11, "assist_dial_from_contact"

    #@c7
    invoke-static {v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_ca
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_c1 .. :try_end_ca} :catch_22d

    #@ca
    move-result v2

    #@cb
    .line 77
    :goto_cb
    const-string v10, "AssistDialPhoneNumberUtils"

    #@cd
    new-instance v11, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v12, "before Assisted Dial dialframContact: "

    #@d4
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v11

    #@d8
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v11

    #@dc
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v11

    #@e0
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 78
    const-string v10, "AssistDialPhoneNumberUtils"

    #@e5
    new-instance v11, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v12, "before Assisted Dial mRefer.getIndex(): "

    #@ec
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v11

    #@f0
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getIndex()I

    #@f3
    move-result v12

    #@f4
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v11

    #@f8
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v11

    #@fc
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 79
    const/4 v10, 0x1

    #@100
    if-ne v2, v10, :cond_205

    #@102
    if-nez v9, :cond_205

    #@104
    .line 81
    const/4 v8, 0x0

    #@105
    .line 82
    .local v8, ref_Ndd:Ljava/lang/String;
    if-eqz v4, :cond_10b

    #@107
    .line 83
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@10a
    move-result-object v8

    #@10b
    .line 85
    :cond_10b
    if-eqz v8, :cond_166

    #@10d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@110
    move-result v10

    #@111
    const/4 v11, 0x7

    #@112
    if-le v10, v11, :cond_166

    #@114
    .line 87
    const-string v10, "AssistDialPhoneNumberUtils"

    #@116
    const-string v11, "****** 1 "

    #@118
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11b
    .line 88
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@11e
    move-result v10

    #@11f
    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@122
    move-result-object v10

    #@123
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@126
    move-result v10

    #@127
    const/16 v11, 0xa

    #@129
    if-gt v10, v11, :cond_166

    #@12b
    .line 89
    const/4 v10, 0x0

    #@12c
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@12f
    move-result v11

    #@130
    invoke-virtual {p1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@133
    move-result-object v10

    #@134
    invoke-static {v10, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@137
    move-result v10

    #@138
    const/4 v11, 0x1

    #@139
    if-ne v10, v11, :cond_166

    #@13b
    .line 91
    const-string v10, "AssistDialPhoneNumberUtils"

    #@13d
    const-string v11, "****** 2 "

    #@13f
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@142
    .line 92
    new-instance v10, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v11, "+"

    #@149
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v10

    #@14d
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@150
    move-result-object v11

    #@151
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v10

    #@155
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@158
    move-result v11

    #@159
    invoke-virtual {p1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@15c
    move-result-object v11

    #@15d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v10

    #@161
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object p1

    #@165
    .line 94
    const/4 v9, 0x1

    #@166
    .line 99
    :cond_166
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getIndex()I

    #@169
    move-result v10

    #@16a
    if-nez v10, :cond_205

    #@16c
    .line 100
    const-string v10, "AssistDialPhoneNumberUtils"

    #@16e
    const-string v11, "****** 3"

    #@170
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@173
    .line 101
    if-nez v9, :cond_1b4

    #@175
    .line 102
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@178
    move-result v10

    #@179
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getNumLength()Ljava/lang/String;

    #@17c
    move-result-object v11

    #@17d
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@180
    move-result v11

    #@181
    if-ne v10, v11, :cond_1b4

    #@183
    .line 103
    const-string v10, "AssistDialPhoneNumberUtils"

    #@185
    const-string v11, "****** 4"

    #@187
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18a
    .line 104
    invoke-virtual {p0, p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAreacodeInfoforContactNumber(Ljava/lang/String;)Z

    #@18d
    move-result v10

    #@18e
    const/4 v11, 0x1

    #@18f
    if-ne v10, v11, :cond_1b4

    #@191
    .line 106
    const-string v10, "AssistDialPhoneNumberUtils"

    #@193
    const-string v11, "****** 5"

    #@195
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@198
    .line 107
    new-instance v10, Ljava/lang/StringBuilder;

    #@19a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@19d
    const-string v11, "+"

    #@19f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v10

    #@1a3
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@1a6
    move-result-object v11

    #@1a7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v10

    #@1ab
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v10

    #@1af
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b2
    move-result-object p1

    #@1b3
    .line 108
    const/4 v9, 0x1

    #@1b4
    .line 112
    :cond_1b4
    if-nez v9, :cond_205

    #@1b6
    .line 113
    const-string v10, "AssistDialPhoneNumberUtils"

    #@1b8
    const-string v11, "****** 6"

    #@1ba
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1bd
    .line 115
    const/4 v5, 0x0

    #@1be
    .line 116
    .local v5, mReferAreaCodeLen:I
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@1c1
    move-result-object v10

    #@1c2
    if-eqz v10, :cond_1cc

    #@1c4
    .line 117
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@1c7
    move-result-object v10

    #@1c8
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@1cb
    move-result v5

    #@1cc
    .line 119
    :cond_1cc
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1cf
    move-result v10

    #@1d0
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getNumLength()Ljava/lang/String;

    #@1d3
    move-result-object v11

    #@1d4
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1d7
    move-result v11

    #@1d8
    sub-int/2addr v11, v5

    #@1d9
    if-ne v10, v11, :cond_205

    #@1db
    .line 120
    const-string v10, "AssistDialPhoneNumberUtils"

    #@1dd
    const-string v11, "****** 7"

    #@1df
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e2
    .line 121
    new-instance v10, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v11, "+"

    #@1e9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v10

    #@1ed
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@1f0
    move-result-object v11

    #@1f1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v10

    #@1f5
    invoke-virtual {v4}, Landroid/provider/ReferenceCountry;->getAreaCode()Ljava/lang/String;

    #@1f8
    move-result-object v11

    #@1f9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v10

    #@1fd
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v10

    #@201
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@204
    move-result-object p1

    #@205
    .line 135
    .end local v4           #mRefer:Landroid/provider/ReferenceCountry;
    .end local v5           #mReferAreaCodeLen:I
    .end local v7           #ref_Idd:Ljava/lang/String;
    .end local v8           #ref_Ndd:Ljava/lang/String;
    :cond_205
    :goto_205
    const-string v10, "AssistDialPhoneNumberUtils"

    #@207
    new-instance v11, Ljava/lang/StringBuilder;

    #@209
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@20c
    const-string v12, " Assited Dial for GSM  : after Number :  "

    #@20e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@211
    move-result-object v11

    #@212
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@215
    move-result-object v11

    #@216
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@219
    move-result-object v11

    #@21a
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21d
    .line 136
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@220
    move-result-object v10

    #@221
    const-string v11, "assist_dial_from_contact"

    #@223
    const/4 v12, 0x0

    #@224
    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@227
    .line 138
    const/4 v10, 0x0

    #@228
    iput-object v10, p0, Landroid/telephony/AssistDialPhoneNumberUtils;->mRefCountryList:[Landroid/provider/ReferenceCountry;

    #@22a
    move-object v6, p1

    #@22b
    .line 140
    .end local p1
    .restart local v6       #number:Ljava/lang/String;
    goto/16 :goto_47

    #@22d
    .line 73
    .end local v6           #number:Ljava/lang/String;
    .restart local v4       #mRefer:Landroid/provider/ReferenceCountry;
    .restart local v7       #ref_Idd:Ljava/lang/String;
    .restart local p1
    :catch_22d
    move-exception v3

    #@22e
    .line 75
    .local v3, e:Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v3}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    #@231
    goto/16 :goto_cb

    #@233
    .line 128
    .end local v3           #e:Landroid/provider/Settings$SettingNotFoundException;
    :cond_233
    const-string v10, "AssistDialPhoneNumberUtils"

    #@235
    const-string/jumbo v11, "mRefer ===> null"

    #@238
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23b
    goto :goto_205

    #@23c
    .line 132
    .end local v4           #mRefer:Landroid/provider/ReferenceCountry;
    .end local v7           #ref_Idd:Ljava/lang/String;
    :cond_23c
    const-string v10, "AssistDialPhoneNumberUtils"

    #@23e
    const-string v11, "currentCountry ===> null"

    #@240
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@243
    goto :goto_205
.end method

.method public getIsAssistedDialCheck(Landroid/content/Context;)I
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 354
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    const-string v2, "assist_dial"

    #@6
    const/4 v3, 0x0

    #@7
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a
    move-result v0

    #@b
    .line 356
    .local v0, isAssistedDialCheck:I
    return v0
.end method

.method public isCdmaVzWMccCode(Ljava/lang/String;)Z
    .registers 8
    .parameter "Mcc"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1804
    const/4 v4, 0x7

    #@3
    new-array v1, v4, [Ljava/lang/String;

    #@5
    const-string v4, "310"

    #@7
    aput-object v4, v1, v3

    #@9
    const-string v4, "311"

    #@b
    aput-object v4, v1, v2

    #@d
    const/4 v4, 0x2

    #@e
    const-string v5, "312"

    #@10
    aput-object v5, v1, v4

    #@12
    const/4 v4, 0x3

    #@13
    const-string v5, "313"

    #@15
    aput-object v5, v1, v4

    #@17
    const/4 v4, 0x4

    #@18
    const-string v5, "314"

    #@1a
    aput-object v5, v1, v4

    #@1c
    const/4 v4, 0x5

    #@1d
    const-string v5, "315"

    #@1f
    aput-object v5, v1, v4

    #@21
    const/4 v4, 0x6

    #@22
    const-string v5, "316"

    #@24
    aput-object v5, v1, v4

    #@26
    .line 1807
    .local v1, vzwMcc:[Ljava/lang/String;
    const/4 v0, 0x0

    #@27
    .local v0, i:I
    :goto_27
    array-length v4, v1

    #@28
    if-ge v0, v4, :cond_36

    #@2a
    .line 1809
    aget-object v4, v1, v0

    #@2c
    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_33

    #@32
    .line 1814
    :goto_32
    return v2

    #@33
    .line 1807
    :cond_33
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_27

    #@36
    :cond_36
    move v2, v3

    #@37
    .line 1814
    goto :goto_32
.end method

.method public isCdmaVzWNetwork(Landroid/content/Context;)Z
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1819
    invoke-virtual {p0, p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@5
    move-result-object v0

    #@6
    .line 1820
    .local v0, currentCountry:Landroid/provider/ReferenceCountry;
    if-nez v0, :cond_f

    #@8
    .line 1822
    const-string v3, "isCdmaVzWNetwork"

    #@a
    const-string v4, " ****** currentCountry ===> null "

    #@c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1824
    :cond_f
    if-eqz v0, :cond_2e

    #@11
    .line 1825
    invoke-virtual {v0}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    const-string v4, "1"

    #@17
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1a
    move-result v3

    #@1b
    if-ne v3, v1, :cond_25

    #@1d
    .line 1826
    const-string v2, "isCdmaVzWNetwork"

    #@1f
    const-string v3, " ****** vzw "

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1834
    :goto_24
    return v1

    #@25
    .line 1830
    :cond_25
    const-string v1, "isCdmaVzWNetwork"

    #@27
    const-string v3, " ****** non vzw "

    #@29
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    move v1, v2

    #@2d
    .line 1831
    goto :goto_24

    #@2e
    :cond_2e
    move v1, v2

    #@2f
    .line 1834
    goto :goto_24
.end method

.method public isInternationalCdmaIdd(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 1840
    invoke-virtual {p0, p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@3
    move-result-object v0

    #@4
    .line 1842
    .local v0, currentCountry:Landroid/provider/ReferenceCountry;
    if-eqz v0, :cond_b

    #@6
    .line 1843
    invoke-virtual {v0}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 1845
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method
