.class final Landroid/telephony/CellInfoLte$1;
.super Ljava/lang/Object;
.source "CellInfoLte.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/CellInfoLte;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/telephony/CellInfoLte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/telephony/CellInfoLte;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    .line 133
    invoke-static {p1}, Landroid/telephony/CellInfoLte;->createFromParcelBody(Landroid/os/Parcel;)Landroid/telephony/CellInfoLte;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-virtual {p0, p1}, Landroid/telephony/CellInfoLte$1;->createFromParcel(Landroid/os/Parcel;)Landroid/telephony/CellInfoLte;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/telephony/CellInfoLte;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 138
    new-array v0, p1, [Landroid/telephony/CellInfoLte;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 129
    invoke-virtual {p0, p1}, Landroid/telephony/CellInfoLte$1;->newArray(I)[Landroid/telephony/CellInfoLte;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
