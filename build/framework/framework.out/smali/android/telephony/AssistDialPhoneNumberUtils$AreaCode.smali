.class Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;
.super Ljava/lang/Object;
.source "AssistDialPhoneNumberUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/AssistDialPhoneNumberUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AreaCode"
.end annotation


# instance fields
.field private areacode:Ljava/lang/String;

.field private cityname:Ljava/lang/String;

.field final synthetic this$0:Landroid/telephony/AssistDialPhoneNumberUtils;


# direct methods
.method private constructor <init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "areacode"
    .parameter "cityname"

    #@0
    .prologue
    .line 1765
    iput-object p1, p0, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->this$0:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1766
    iput-object p2, p0, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->areacode:Ljava/lang/String;

    #@7
    .line 1767
    iput-object p3, p0, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->cityname:Ljava/lang/String;

    #@9
    .line 1769
    return-void
.end method

.method synthetic constructor <init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;Landroid/telephony/AssistDialPhoneNumberUtils$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 1752
    invoke-direct {p0, p1, p2, p3}, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;-><init>(Landroid/telephony/AssistDialPhoneNumberUtils;Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method


# virtual methods
.method public getAreacode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1757
    iget-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->areacode:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCityname()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1761
    iget-object v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils$AreaCode;->cityname:Ljava/lang/String;

    #@2
    return-object v0
.end method
