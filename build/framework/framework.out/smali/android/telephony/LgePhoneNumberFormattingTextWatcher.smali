.class public Landroid/telephony/LgePhoneNumberFormattingTextWatcher;
.super Ljava/lang/Object;
.source "LgePhoneNumberFormattingTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field private static sCachedLocale:Ljava/util/Locale;

.field private static sFormatType:I


# instance fields
.field private mDeletingBackward:Z

.field private mDeletingHyphen:Z

.field private mFormatting:Z

.field private mHyphenStart:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    sget-object v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sCachedLocale:Ljava/util/Locale;

    #@5
    if-eqz v0, :cond_f

    #@7
    sget-object v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sCachedLocale:Ljava/util/Locale;

    #@9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@c
    move-result-object v1

    #@d
    if-eq v0, v1, :cond_25

    #@f
    .line 53
    :cond_f
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@12
    move-result-object v0

    #@13
    sput-object v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sCachedLocale:Ljava/util/Locale;

    #@15
    .line 55
    const-string v0, "KR"

    #@17
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_26

    #@1d
    .line 56
    sget-object v0, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    #@1f
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@22
    move-result v0

    #@23
    sput v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sFormatType:I

    #@25
    .line 61
    :cond_25
    :goto_25
    return-void

    #@26
    .line 59
    :cond_26
    sget-object v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sCachedLocale:Ljava/util/Locale;

    #@28
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@2b
    move-result v0

    #@2c
    sput v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sFormatType:I

    #@2e
    goto :goto_25
.end method


# virtual methods
.method public declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 65
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mFormatting:Z

    #@3
    if-nez v0, :cond_2f

    #@5
    .line 66
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mFormatting:Z

    #@8
    .line 69
    iget-boolean v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mDeletingHyphen:Z

    #@a
    if-eqz v0, :cond_27

    #@c
    iget v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@e
    if-lez v0, :cond_27

    #@10
    .line 70
    iget-boolean v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mDeletingBackward:Z

    #@12
    if-eqz v0, :cond_31

    #@14
    .line 71
    iget v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@16
    add-int/lit8 v0, v0, -0x1

    #@18
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@1b
    move-result v1

    #@1c
    if-ge v0, v1, :cond_27

    #@1e
    .line 72
    iget v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@20
    add-int/lit8 v0, v0, -0x1

    #@22
    iget v1, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@24
    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@27
    .line 79
    :cond_27
    :goto_27
    sget v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->sFormatType:I

    #@29
    invoke-static {p1, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    #@2c
    .line 81
    const/4 v0, 0x0

    #@2d
    iput-boolean v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mFormatting:Z
    :try_end_2f
    .catchall {:try_start_1 .. :try_end_2f} :catchall_43

    #@2f
    .line 83
    :cond_2f
    monitor-exit p0

    #@30
    return-void

    #@31
    .line 74
    :cond_31
    :try_start_31
    iget v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@33
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@36
    move-result v1

    #@37
    if-ge v0, v1, :cond_27

    #@39
    .line 75
    iget v0, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@3b
    iget v1, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@3d
    add-int/lit8 v1, v1, 0x1

    #@3f
    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;
    :try_end_42
    .catchall {:try_start_31 .. :try_end_42} :catchall_43

    #@42
    goto :goto_27

    #@43
    .line 65
    :catchall_43
    move-exception v0

    #@44
    monitor-exit p0

    #@45
    throw v0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 11
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 87
    iget-boolean v2, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mFormatting:Z

    #@4
    if-nez v2, :cond_2c

    #@6
    .line 89
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@9
    move-result v1

    #@a
    .line 90
    .local v1, selStart:I
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@d
    move-result v0

    #@e
    .line 91
    .local v0, selEnd:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@11
    move-result v2

    #@12
    if-le v2, v4, :cond_30

    #@14
    if-ne p3, v4, :cond_30

    #@16
    if-nez p4, :cond_30

    #@18
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    #@1b
    move-result v2

    #@1c
    const/16 v3, 0x2d

    #@1e
    if-ne v2, v3, :cond_30

    #@20
    if-ne v1, v0, :cond_30

    #@22
    .line 96
    iput-boolean v4, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mDeletingHyphen:Z

    #@24
    .line 97
    iput p2, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mHyphenStart:I

    #@26
    .line 99
    add-int/lit8 v2, p2, 0x1

    #@28
    if-ne v1, v2, :cond_2d

    #@2a
    .line 100
    iput-boolean v4, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mDeletingBackward:Z

    #@2c
    .line 108
    .end local v0           #selEnd:I
    .end local v1           #selStart:I
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 102
    .restart local v0       #selEnd:I
    .restart local v1       #selStart:I
    :cond_2d
    iput-boolean v5, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mDeletingBackward:Z

    #@2f
    goto :goto_2c

    #@30
    .line 105
    :cond_30
    iput-boolean v5, p0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->mDeletingHyphen:Z

    #@32
    goto :goto_2c
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 112
    return-void
.end method
