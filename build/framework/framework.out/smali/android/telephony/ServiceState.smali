.class public Landroid/telephony/ServiceState;
.super Ljava/lang/Object;
.source "ServiceState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/ServiceState;",
            ">;"
        }
    .end annotation
.end field

.field public static final DOMESTIC_ROAMING:I = 0x3

.field public static final HOME:I = 0x1

.field public static final INTERNATIONAL_ROAMING:I = 0x2

.field static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field public static final REGISTRATION_STATE_HOME_NETWORK:I = 0x1

.field public static final REGISTRATION_STATE_NOT_REGISTERED_AND_NOT_SEARCHING:I = 0x0

.field public static final REGISTRATION_STATE_NOT_REGISTERED_AND_SEARCHING:I = 0x2

.field public static final REGISTRATION_STATE_REGISTRATION_DENIED:I = 0x3

.field public static final REGISTRATION_STATE_ROAMING:I = 0x5

.field public static final REGISTRATION_STATE_UNKNOWN:I = 0x4

.field public static final RIL_RADIO_TECHNOLOGY_1xRTT:I = 0x6

.field public static final RIL_RADIO_TECHNOLOGY_EDGE:I = 0x2

.field public static final RIL_RADIO_TECHNOLOGY_EHRPD:I = 0xd

.field public static final RIL_RADIO_TECHNOLOGY_EVDO_0:I = 0x7

.field public static final RIL_RADIO_TECHNOLOGY_EVDO_A:I = 0x8

.field public static final RIL_RADIO_TECHNOLOGY_EVDO_B:I = 0xc

.field public static final RIL_RADIO_TECHNOLOGY_GPRS:I = 0x1

.field public static final RIL_RADIO_TECHNOLOGY_GSM:I = 0x10

.field public static final RIL_RADIO_TECHNOLOGY_HSDPA:I = 0x9

.field public static final RIL_RADIO_TECHNOLOGY_HSPA:I = 0xb

.field public static final RIL_RADIO_TECHNOLOGY_HSPAP:I = 0xf

.field public static final RIL_RADIO_TECHNOLOGY_HSUPA:I = 0xa

.field public static final RIL_RADIO_TECHNOLOGY_IS95A:I = 0x4

.field public static final RIL_RADIO_TECHNOLOGY_IS95B:I = 0x5

.field public static final RIL_RADIO_TECHNOLOGY_LTE:I = 0xe

.field public static final RIL_RADIO_TECHNOLOGY_TD_SCDMA:I = 0x11

.field public static final RIL_RADIO_TECHNOLOGY_UMTS:I = 0x3

.field public static final RIL_RADIO_TECHNOLOGY_UNKNOWN:I = 0x0

.field public static final STATE_EMERGENCY_ONLY:I = 0x2

.field public static final STATE_IN_SERVICE:I = 0x0

.field public static final STATE_OUT_OF_SERVICE:I = 0x1

.field public static final STATE_POWER_OFF:I = 0x3

.field private static mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

.field public static mRssiLevel:I


# instance fields
.field private mCdmaDefaultRoamingIndicator:I

.field private mCdmaEriIconIndex:I

.field private mCdmaEriIconMode:I

.field private mCdmaRoamingIndicator:I

.field private mCssIndicator:Z

.field private mDataRoaming:Z

.field private mDataState:I

.field private mIsDataSearching:Z

.field private mIsEmergencyOnly:Z

.field private mIsManualNetworkSelection:Z

.field private mIsVoiceSearching:Z

.field private mNetworkId:I

.field private mOperatorAlphaLong:Ljava/lang/String;

.field private mOperatorAlphaShort:Ljava/lang/String;

.field private mOperatorNumeric:Ljava/lang/String;

.field private mRadioTechnology:I

.field private mRoaming:Z

.field private mState:I

.field private mSystemId:I

.field private mVoiceRoaming:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 178
    invoke-static {}, Landroid/telephony/ServiceState;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeRssiData;->getRssiLevel()I

    #@7
    move-result v0

    #@8
    sput v0, Landroid/telephony/ServiceState;->mRssiLevel:I

    #@a
    .line 312
    new-instance v0, Landroid/telephony/ServiceState$1;

    #@c
    invoke-direct {v0}, Landroid/telephony/ServiceState$1;-><init>()V

    #@f
    sput-object v0, Landroid/telephony/ServiceState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@11
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 203
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 137
    iput v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@6
    .line 138
    iput v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@8
    .line 163
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@a
    .line 204
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 6
    .parameter "in"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 248
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 137
    iput v1, p0, Landroid/telephony/ServiceState;->mState:I

    #@7
    .line 138
    iput v1, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@9
    .line 163
    iput v1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@b
    .line 249
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@11
    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_a5

    #@17
    move v0, v1

    #@18
    :goto_18
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@1a
    .line 252
    const/4 v0, 0x0

    #@1b
    const-string/jumbo v3, "vzw_roaming_state"

    #@1e
    invoke-static {v0, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_36

    #@24
    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_a8

    #@2a
    move v0, v1

    #@2b
    :goto_2b
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@2d
    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_aa

    #@33
    move v0, v1

    #@34
    :goto_34
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@36
    .line 257
    :cond_36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@3c
    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@42
    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@48
    .line 260
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_ac

    #@4e
    move v0, v1

    #@4f
    :goto_4f
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@51
    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v0

    #@55
    iput v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@57
    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_ae

    #@5d
    move v0, v1

    #@5e
    :goto_5e
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@60
    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v0

    #@64
    iput v0, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@66
    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v0

    #@6a
    iput v0, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@6c
    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v0

    #@70
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@72
    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@75
    move-result v0

    #@76
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@78
    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7b
    move-result v0

    #@7c
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@7e
    .line 268
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v0

    #@82
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@84
    .line 269
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v0

    #@88
    if-eqz v0, :cond_b0

    #@8a
    move v0, v1

    #@8b
    :goto_8b
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@8d
    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v0

    #@91
    iput v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@93
    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@96
    move-result v0

    #@97
    if-eqz v0, :cond_b2

    #@99
    move v0, v1

    #@9a
    :goto_9a
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@9c
    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9f
    move-result v0

    #@a0
    if-eqz v0, :cond_b4

    #@a2
    :goto_a2
    iput-boolean v1, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@a4
    .line 276
    return-void

    #@a5
    :cond_a5
    move v0, v2

    #@a6
    .line 250
    goto/16 :goto_18

    #@a8
    :cond_a8
    move v0, v2

    #@a9
    .line 253
    goto :goto_2b

    #@aa
    :cond_aa
    move v0, v2

    #@ab
    .line 254
    goto :goto_34

    #@ac
    :cond_ac
    move v0, v2

    #@ad
    .line 260
    goto :goto_4f

    #@ae
    :cond_ae
    move v0, v2

    #@af
    .line 262
    goto :goto_5e

    #@b0
    :cond_b0
    move v0, v2

    #@b1
    .line 269
    goto :goto_8b

    #@b2
    :cond_b2
    move v0, v2

    #@b3
    .line 272
    goto :goto_9a

    #@b4
    :cond_b4
    move v1, v2

    #@b5
    .line 273
    goto :goto_a2
.end method

.method public constructor <init>(Landroid/telephony/ServiceState;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 211
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 137
    iput v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@6
    .line 138
    iput v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@8
    .line 163
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@a
    .line 212
    invoke-virtual {p0, p1}, Landroid/telephony/ServiceState;->copyFrom(Landroid/telephony/ServiceState;)V

    #@d
    .line 213
    return-void
.end method

.method private static equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 750
    if-nez p0, :cond_8

    #@2
    if-nez p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public static getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;
    .registers 2

    #@0
    .prologue
    .line 1022
    sget-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@2
    if-nez v0, :cond_16

    #@4
    .line 1023
    invoke-static {}, Lcom/android/internal/telephony/LgeRssiData;->getInstance()Lcom/android/internal/telephony/LgeRssiData;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@a
    .line 1024
    sget-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeRssiData;->init()V

    #@f
    .line 1025
    const-string v0, "PHONE"

    #@11
    const-string v1, "getLgeRssiData instance is null"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 1027
    :cond_16
    sget-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@18
    return-object v0
.end method

.method public static isCdma(I)Z
    .registers 2
    .parameter "radioTechnology"

    #@0
    .prologue
    .line 929
    const/4 v0, 0x4

    #@1
    if-eq p0, v0, :cond_18

    #@3
    const/4 v0, 0x5

    #@4
    if-eq p0, v0, :cond_18

    #@6
    const/4 v0, 0x6

    #@7
    if-eq p0, v0, :cond_18

    #@9
    const/4 v0, 0x7

    #@a
    if-eq p0, v0, :cond_18

    #@c
    const/16 v0, 0x8

    #@e
    if-eq p0, v0, :cond_18

    #@10
    const/16 v0, 0xc

    #@12
    if-eq p0, v0, :cond_18

    #@14
    const/16 v0, 0xd

    #@16
    if-ne p0, v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public static isCdmaFormat(I)Z
    .registers 2
    .parameter "radioTechnology"

    #@0
    .prologue
    .line 1033
    const/4 v0, 0x4

    #@1
    if-eq p0, v0, :cond_14

    #@3
    const/4 v0, 0x5

    #@4
    if-eq p0, v0, :cond_14

    #@6
    const/4 v0, 0x7

    #@7
    if-eq p0, v0, :cond_14

    #@9
    const/16 v0, 0x8

    #@b
    if-eq p0, v0, :cond_14

    #@d
    const/16 v0, 0xc

    #@f
    if-eq p0, v0, :cond_14

    #@11
    const/4 v0, 0x6

    #@12
    if-ne p0, v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public static isEhrpd(I)Z
    .registers 2
    .parameter "radioTechnology"

    #@0
    .prologue
    .line 1042
    const/16 v0, 0xd

    #@2
    if-ne p0, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public static isGsm(I)Z
    .registers 3
    .parameter "radioTechnology"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 915
    if-eq p0, v0, :cond_25

    #@3
    const/4 v1, 0x2

    #@4
    if-eq p0, v1, :cond_25

    #@6
    const/4 v1, 0x3

    #@7
    if-eq p0, v1, :cond_25

    #@9
    const/16 v1, 0x9

    #@b
    if-eq p0, v1, :cond_25

    #@d
    const/16 v1, 0xa

    #@f
    if-eq p0, v1, :cond_25

    #@11
    const/16 v1, 0xb

    #@13
    if-eq p0, v1, :cond_25

    #@15
    const/16 v1, 0xe

    #@17
    if-eq p0, v1, :cond_25

    #@19
    const/16 v1, 0xf

    #@1b
    if-eq p0, v1, :cond_25

    #@1d
    const/16 v1, 0x10

    #@1f
    if-eq p0, v1, :cond_25

    #@21
    const/16 v1, 0x11

    #@23
    if-ne p0, v1, :cond_26

    #@25
    :cond_25
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method public static newFromBundle(Landroid/os/Bundle;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "m"

    #@0
    .prologue
    .line 195
    new-instance v0, Landroid/telephony/ServiceState;

    #@2
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@5
    .line 196
    .local v0, ret:Landroid/telephony/ServiceState;
    invoke-direct {v0, p0}, Landroid/telephony/ServiceState;->setFromNotifierBundle(Landroid/os/Bundle;)V

    #@8
    .line 197
    return-object v0
.end method

.method public static rilRadioTechnologyToString(I)Ljava/lang/String;
    .registers 5
    .parameter "rt"

    #@0
    .prologue
    .line 532
    packed-switch p0, :pswitch_data_54

    #@3
    .line 588
    const-string v0, "Unexpected"

    #@5
    .line 589
    .local v0, rtString:Ljava/lang/String;
    const-string v1, "PHONE"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "Unexpected radioTechnology="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 592
    :goto_1d
    return-object v0

    #@1e
    .line 534
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_1e
    const-string v0, "Unknown"

    #@20
    .line 535
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@21
    .line 537
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_21
    const-string v0, "GPRS"

    #@23
    .line 538
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@24
    .line 540
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_24
    const-string v0, "EDGE"

    #@26
    .line 541
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@27
    .line 543
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_27
    const-string v0, "UMTS"

    #@29
    .line 544
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@2a
    .line 546
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_2a
    const-string v0, "CDMA-IS95A"

    #@2c
    .line 547
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@2d
    .line 549
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_2d
    const-string v0, "CDMA-IS95B"

    #@2f
    .line 550
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@30
    .line 552
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_30
    const-string v0, "1xRTT"

    #@32
    .line 553
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@33
    .line 555
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_33
    const-string v0, "EvDo-rev.0"

    #@35
    .line 556
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@36
    .line 558
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_36
    const-string v0, "EvDo-rev.A"

    #@38
    .line 559
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@39
    .line 561
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_39
    const-string v0, "HSDPA"

    #@3b
    .line 562
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@3c
    .line 564
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_3c
    const-string v0, "HSUPA"

    #@3e
    .line 565
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@3f
    .line 567
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_3f
    const-string v0, "HSPA"

    #@41
    .line 568
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@42
    .line 570
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_42
    const-string v0, "EvDo-rev.B"

    #@44
    .line 571
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@45
    .line 573
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_45
    const-string v0, "eHRPD"

    #@47
    .line 574
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@48
    .line 576
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_48
    const-string v0, "LTE"

    #@4a
    .line 577
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@4b
    .line 579
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_4b
    const-string v0, "HSPAP"

    #@4d
    .line 580
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@4e
    .line 582
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_4e
    const-string v0, "GSM"

    #@50
    .line 583
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@51
    .line 585
    .end local v0           #rtString:Ljava/lang/String;
    :pswitch_51
    const-string v0, "TD-SCDMA"

    #@53
    .line 586
    .restart local v0       #rtString:Ljava/lang/String;
    goto :goto_1d

    #@54
    .line 532
    :pswitch_data_54
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
        :pswitch_2a
        :pswitch_2d
        :pswitch_30
        :pswitch_33
        :pswitch_36
        :pswitch_39
        :pswitch_3c
        :pswitch_3f
        :pswitch_42
        :pswitch_45
        :pswitch_48
        :pswitch_4b
        :pswitch_4e
        :pswitch_51
    .end packed-switch
.end method

.method private setFromNotifierBundle(Landroid/os/Bundle;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 760
    const-string/jumbo v0, "state"

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@9
    .line 762
    const-string v0, "dataState"

    #@b
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@11
    .line 764
    const-string/jumbo v0, "roaming"

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@17
    move-result v0

    #@18
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@1a
    .line 766
    const/4 v0, 0x0

    #@1b
    const-string/jumbo v1, "vzw_roaming_state"

    #@1e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_35

    #@24
    .line 767
    const-string/jumbo v0, "voiceRoaming"

    #@27
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@2a
    move-result v0

    #@2b
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@2d
    .line 768
    const-string v0, "dataRoaming"

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@32
    move-result v0

    #@33
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@35
    .line 771
    :cond_35
    const-string/jumbo v0, "operator-alpha-long"

    #@38
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@3e
    .line 772
    const-string/jumbo v0, "operator-alpha-short"

    #@41
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@47
    .line 773
    const-string/jumbo v0, "operator-numeric"

    #@4a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v0

    #@4e
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@50
    .line 774
    const-string/jumbo v0, "manual"

    #@53
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@56
    move-result v0

    #@57
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@59
    .line 775
    const-string/jumbo v0, "radioTechnology"

    #@5c
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@5f
    move-result v0

    #@60
    iput v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@62
    .line 776
    const-string v0, "cssIndicator"

    #@64
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@67
    move-result v0

    #@68
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@6a
    .line 777
    const-string/jumbo v0, "networkId"

    #@6d
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@70
    move-result v0

    #@71
    iput v0, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@73
    .line 778
    const-string/jumbo v0, "systemId"

    #@76
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@79
    move-result v0

    #@7a
    iput v0, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@7c
    .line 779
    const-string v0, "cdmaRoamingIndicator"

    #@7e
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@81
    move-result v0

    #@82
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@84
    .line 780
    const-string v0, "cdmaDefaultRoamingIndicator"

    #@86
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@89
    move-result v0

    #@8a
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@8c
    .line 782
    const-string v0, "cdmaEriIconIndex"

    #@8e
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@91
    move-result v0

    #@92
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@94
    .line 783
    const-string v0, "cdmaEriIconMode"

    #@96
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@99
    move-result v0

    #@9a
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@9c
    .line 785
    const-string v0, "emergencyOnly"

    #@9e
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@a1
    move-result v0

    #@a2
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@a4
    .line 787
    const-string/jumbo v0, "voiceSearching"

    #@a7
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@aa
    move-result v0

    #@ab
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@ad
    .line 788
    const-string v0, "dataSearching"

    #@af
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@b2
    move-result v0

    #@b3
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@b5
    .line 791
    return-void
.end method

.method private setNullState(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    const/4 v0, 0x0

    #@3
    .line 624
    iput p1, p0, Landroid/telephony/ServiceState;->mState:I

    #@5
    .line 625
    iput p1, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@7
    .line 626
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@9
    .line 628
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@b
    .line 629
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@d
    .line 631
    iput-object v2, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@f
    .line 632
    iput-object v2, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@11
    .line 633
    iput-object v2, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@13
    .line 634
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@15
    .line 635
    iput v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@17
    .line 636
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@19
    .line 637
    iput v1, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@1b
    .line 638
    iput v1, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@1d
    .line 639
    iput v1, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@1f
    .line 640
    iput v1, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@21
    .line 641
    iput v1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@23
    .line 642
    iput v1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@25
    .line 643
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@27
    .line 645
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@29
    .line 646
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@2b
    .line 648
    return-void
.end method


# virtual methods
.method protected copyFrom(Landroid/telephony/ServiceState;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 216
    iget v0, p1, Landroid/telephony/ServiceState;->mState:I

    #@2
    iput v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@4
    .line 217
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mRoaming:Z

    #@6
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@8
    .line 219
    const/4 v0, 0x0

    #@9
    const-string/jumbo v1, "vzw_roaming_state"

    #@c
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1a

    #@12
    .line 220
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@14
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@16
    .line 221
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@18
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@1a
    .line 224
    :cond_1a
    iget-object v0, p1, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@1c
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@1e
    .line 225
    iget-object v0, p1, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@20
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@22
    .line 226
    iget-object v0, p1, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@24
    iput-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@26
    .line 227
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@28
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@2a
    .line 228
    iget v0, p1, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@2c
    iput v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@2e
    .line 229
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@30
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@32
    .line 230
    iget v0, p1, Landroid/telephony/ServiceState;->mNetworkId:I

    #@34
    iput v0, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@36
    .line 231
    iget v0, p1, Landroid/telephony/ServiceState;->mSystemId:I

    #@38
    iput v0, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@3a
    .line 232
    iget v0, p1, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@3c
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@3e
    .line 233
    iget v0, p1, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@40
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@42
    .line 234
    iget v0, p1, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@44
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@46
    .line 235
    iget v0, p1, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@48
    iput v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@4a
    .line 236
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@4c
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@4e
    .line 237
    iget v0, p1, Landroid/telephony/ServiceState;->mDataState:I

    #@50
    iput v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@52
    .line 239
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@54
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@56
    .line 240
    iget-boolean v0, p1, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@58
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@5a
    .line 243
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 309
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 490
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/ServiceState;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 495
    .local v2, s:Landroid/telephony/ServiceState;
    if-nez p1, :cond_a

    #@7
    .line 499
    .end local v2           #s:Landroid/telephony/ServiceState;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 491
    :catch_8
    move-exception v1

    #@9
    .line 492
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 499
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/ServiceState;
    :cond_a
    iget v4, p0, Landroid/telephony/ServiceState;->mState:I

    #@c
    iget v5, v2, Landroid/telephony/ServiceState;->mState:I

    #@e
    if-ne v4, v5, :cond_7

    #@10
    iget-boolean v4, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@12
    iget-boolean v5, v2, Landroid/telephony/ServiceState;->mRoaming:Z

    #@14
    if-ne v4, v5, :cond_7

    #@16
    iget-boolean v4, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@18
    iget-boolean v5, v2, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@1a
    if-ne v4, v5, :cond_7

    #@1c
    iget-object v4, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@1e
    iget-object v5, v2, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@20
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_7

    #@26
    iget-object v4, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@28
    iget-object v5, v2, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@2a
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_7

    #@30
    iget-object v4, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@32
    iget-object v5, v2, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@34
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_7

    #@3a
    iget v4, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@3c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v4

    #@40
    iget v5, v2, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@42
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v5

    #@46
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@49
    move-result v4

    #@4a
    if-eqz v4, :cond_7

    #@4c
    iget-boolean v4, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@4e
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@51
    move-result-object v4

    #@52
    iget-boolean v5, v2, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@54
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@57
    move-result-object v5

    #@58
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@5b
    move-result v4

    #@5c
    if-eqz v4, :cond_7

    #@5e
    iget v4, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@60
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@63
    move-result-object v4

    #@64
    iget v5, v2, Landroid/telephony/ServiceState;->mNetworkId:I

    #@66
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v5

    #@6a
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@6d
    move-result v4

    #@6e
    if-eqz v4, :cond_7

    #@70
    iget v4, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@72
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@75
    move-result-object v4

    #@76
    iget v5, v2, Landroid/telephony/ServiceState;->mSystemId:I

    #@78
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@7f
    move-result v4

    #@80
    if-eqz v4, :cond_7

    #@82
    iget v4, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@84
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@87
    move-result-object v4

    #@88
    iget v5, v2, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@8a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8d
    move-result-object v5

    #@8e
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@91
    move-result v4

    #@92
    if-eqz v4, :cond_7

    #@94
    iget v4, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@96
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@99
    move-result-object v4

    #@9a
    iget v5, v2, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@9c
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9f
    move-result-object v5

    #@a0
    invoke-static {v4, v5}, Landroid/telephony/ServiceState;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@a3
    move-result v4

    #@a4
    if-eqz v4, :cond_7

    #@a6
    iget-boolean v4, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@a8
    iget-boolean v5, v2, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@aa
    if-ne v4, v5, :cond_7

    #@ac
    iget v4, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@ae
    iget v5, v2, Landroid/telephony/ServiceState;->mDataState:I

    #@b0
    if-ne v4, v5, :cond_7

    #@b2
    iget-boolean v4, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@b4
    iget-boolean v5, v2, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@b6
    if-ne v4, v5, :cond_7

    #@b8
    iget-boolean v4, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@ba
    iget-boolean v5, v2, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@bc
    if-ne v4, v5, :cond_7

    #@be
    const/4 v3, 0x1

    #@bf
    goto/16 :goto_7
.end method

.method public fillInNotifierBundle(Landroid/os/Bundle;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 800
    const-string/jumbo v0, "state"

    #@3
    iget v1, p0, Landroid/telephony/ServiceState;->mState:I

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@8
    .line 802
    const-string v0, "dataState"

    #@a
    iget v1, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@f
    .line 804
    const-string/jumbo v0, "roaming"

    #@12
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@14
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@1b
    move-result v1

    #@1c
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@1f
    .line 806
    const/4 v0, 0x0

    #@20
    const-string/jumbo v1, "vzw_roaming_state"

    #@23
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_48

    #@29
    .line 807
    const-string/jumbo v0, "voiceRoaming"

    #@2c
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@2e
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@35
    move-result v1

    #@36
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@39
    .line 808
    const-string v0, "dataRoaming"

    #@3b
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@3d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@44
    move-result v1

    #@45
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@48
    .line 811
    :cond_48
    const-string/jumbo v0, "operator-alpha-long"

    #@4b
    iget-object v1, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@4d
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@50
    .line 812
    const-string/jumbo v0, "operator-alpha-short"

    #@53
    iget-object v1, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@55
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 813
    const-string/jumbo v0, "operator-numeric"

    #@5b
    iget-object v1, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@5d
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 814
    const-string/jumbo v0, "manual"

    #@63
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@65
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@6c
    move-result v1

    #@6d
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@70
    .line 815
    const-string/jumbo v0, "radioTechnology"

    #@73
    iget v1, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@75
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@78
    .line 816
    const-string v0, "cssIndicator"

    #@7a
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@7c
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@7f
    .line 817
    const-string/jumbo v0, "networkId"

    #@82
    iget v1, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@84
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@87
    .line 818
    const-string/jumbo v0, "systemId"

    #@8a
    iget v1, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@8c
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@8f
    .line 819
    const-string v0, "cdmaRoamingIndicator"

    #@91
    iget v1, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@93
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@96
    .line 820
    const-string v0, "cdmaDefaultRoamingIndicator"

    #@98
    iget v1, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@9a
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@9d
    .line 822
    const-string v0, "cdmaEriIconIndex"

    #@9f
    iget v1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@a1
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@a4
    .line 823
    const-string v0, "cdmaEriIconMode"

    #@a6
    iget v1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@a8
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@ab
    .line 825
    const-string v0, "emergencyOnly"

    #@ad
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@af
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@b2
    move-result-object v1

    #@b3
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@b6
    move-result v1

    #@b7
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@ba
    .line 827
    const-string/jumbo v0, "voiceSearching"

    #@bd
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@bf
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@c2
    move-result-object v1

    #@c3
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@c6
    move-result v1

    #@c7
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@ca
    .line 828
    const-string v0, "dataSearching"

    #@cc
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@ce
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@d1
    move-result-object v1

    #@d2
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@d5
    move-result v1

    #@d6
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@d9
    .line 831
    return-void
.end method

.method public getCdmaDefaultRoamingIndicator()I
    .registers 2

    #@0
    .prologue
    .line 402
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@2
    return v0
.end method

.method public getCdmaEriIconIndex()I
    .registers 2

    #@0
    .prologue
    .line 409
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@2
    return v0
.end method

.method public getCdmaEriIconMode()I
    .registers 2

    #@0
    .prologue
    .line 416
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@2
    return v0
.end method

.method public getCdmaRoamingIndicator()I
    .registers 2

    #@0
    .prologue
    .line 395
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@2
    return v0
.end method

.method public getCssIndicator()I
    .registers 2

    #@0
    .prologue
    .line 900
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public getDataRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 380
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@2
    return v0
.end method

.method public getDataState()I
    .registers 2

    #@0
    .prologue
    .line 353
    iget v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@2
    return v0
.end method

.method public getIsManualSelection()Z
    .registers 2

    #@0
    .prologue
    .line 464
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@2
    return v0
.end method

.method public getNetworkId()I
    .registers 2

    #@0
    .prologue
    .line 905
    iget v0, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@2
    return v0
.end method

.method public getNetworkType()I
    .registers 2

    #@0
    .prologue
    .line 861
    iget v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@2
    packed-switch v0, :pswitch_data_2e

    #@5
    .line 894
    :pswitch_5
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 863
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 865
    :pswitch_9
    const/4 v0, 0x2

    #@a
    goto :goto_6

    #@b
    .line 867
    :pswitch_b
    const/4 v0, 0x3

    #@c
    goto :goto_6

    #@d
    .line 869
    :pswitch_d
    const/16 v0, 0x8

    #@f
    goto :goto_6

    #@10
    .line 871
    :pswitch_10
    const/16 v0, 0x9

    #@12
    goto :goto_6

    #@13
    .line 873
    :pswitch_13
    const/16 v0, 0xa

    #@15
    goto :goto_6

    #@16
    .line 876
    :pswitch_16
    const/4 v0, 0x4

    #@17
    goto :goto_6

    #@18
    .line 878
    :pswitch_18
    const/4 v0, 0x7

    #@19
    goto :goto_6

    #@1a
    .line 880
    :pswitch_1a
    const/4 v0, 0x5

    #@1b
    goto :goto_6

    #@1c
    .line 882
    :pswitch_1c
    const/4 v0, 0x6

    #@1d
    goto :goto_6

    #@1e
    .line 884
    :pswitch_1e
    const/16 v0, 0xc

    #@20
    goto :goto_6

    #@21
    .line 886
    :pswitch_21
    const/16 v0, 0xe

    #@23
    goto :goto_6

    #@24
    .line 888
    :pswitch_24
    const/16 v0, 0xd

    #@26
    goto :goto_6

    #@27
    .line 890
    :pswitch_27
    const/16 v0, 0xf

    #@29
    goto :goto_6

    #@2a
    .line 892
    :pswitch_2a
    const/16 v0, 0x11

    #@2c
    goto :goto_6

    #@2d
    .line 861
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_16
        :pswitch_16
        :pswitch_18
        :pswitch_1a
        :pswitch_1c
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
        :pswitch_5
        :pswitch_2a
    .end packed-switch
.end method

.method public getOperatorAlphaLong()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOperatorAlphaShort()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 439
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOperatorNumeric()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRadioTechnology()I
    .registers 2

    #@0
    .prologue
    .line 856
    invoke-virtual {p0}, Landroid/telephony/ServiceState;->getRilRadioTechnology()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getRilRadioTechnology()I
    .registers 2

    #@0
    .prologue
    .line 852
    iget v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@2
    return v0
.end method

.method public getRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 365
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@2
    return v0
.end method

.method public getRoamingType()I
    .registers 3

    #@0
    .prologue
    .line 941
    const/4 v1, 0x1

    #@1
    .line 942
    .local v1, rvalue:I
    invoke-virtual {p0}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@4
    move-result v0

    #@5
    .line 944
    .local v0, roamingIndcation:I
    sparse-switch v0, :sswitch_data_e

    #@8
    .line 973
    const/4 v1, 0x3

    #@9
    .line 975
    :goto_9
    return v1

    #@a
    .line 947
    :sswitch_a
    const/4 v1, 0x1

    #@b
    .line 948
    goto :goto_9

    #@c
    .line 970
    :sswitch_c
    const/4 v1, 0x2

    #@d
    .line 971
    goto :goto_9

    #@e
    .line 944
    :sswitch_data_e
    .sparse-switch
        0x1 -> :sswitch_a
        0x4a -> :sswitch_c
        0x7c -> :sswitch_c
        0x7d -> :sswitch_c
        0x7e -> :sswitch_c
        0x9d -> :sswitch_c
        0x9e -> :sswitch_c
        0x9f -> :sswitch_c
        0xc1 -> :sswitch_c
        0xc2 -> :sswitch_c
        0xc3 -> :sswitch_c
        0xc4 -> :sswitch_c
        0xc5 -> :sswitch_c
        0xc6 -> :sswitch_c
        0xe4 -> :sswitch_c
        0xe5 -> :sswitch_c
        0xe6 -> :sswitch_c
        0xe7 -> :sswitch_c
        0xe8 -> :sswitch_c
        0xe9 -> :sswitch_c
        0xea -> :sswitch_c
        0xeb -> :sswitch_c
    .end sparse-switch
.end method

.method public getState()I
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 333
    const/4 v1, 0x0

    #@2
    const-string/jumbo v2, "lgu_lte_single_device"

    #@5
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_2c

    #@b
    .line 334
    const-string/jumbo v1, "persist.radio.camped_mccmnc"

    #@e
    const-string v2, ""

    #@10
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 335
    .local v0, campedMccMnc:Ljava/lang/String;
    const-string v1, "450"

    #@16
    const/4 v2, 0x0

    #@17
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_2c

    #@21
    iget v1, p0, Landroid/telephony/ServiceState;->mState:I

    #@23
    if-eqz v1, :cond_2c

    #@25
    iget v1, p0, Landroid/telephony/ServiceState;->mState:I

    #@27
    if-eq v1, v3, :cond_2c

    #@29
    .line 336
    iget v1, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@2b
    .line 340
    .end local v0           #campedMccMnc:Ljava/lang/String;
    :goto_2b
    return v1

    #@2c
    :cond_2c
    iget v1, p0, Landroid/telephony/ServiceState;->mState:I

    #@2e
    goto :goto_2b
.end method

.method public getSystemId()I
    .registers 2

    #@0
    .prologue
    .line 910
    iget v0, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@2
    return v0
.end method

.method public getVoiceRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 373
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 469
    iget v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@4
    mul-int/lit16 v3, v0, 0x1234

    #@6
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@8
    if-eqz v0, :cond_3c

    #@a
    move v0, v1

    #@b
    :goto_b
    add-int/2addr v3, v0

    #@c
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@e
    if-eqz v0, :cond_3e

    #@10
    move v0, v1

    #@11
    :goto_11
    add-int/2addr v3, v0

    #@12
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@14
    if-nez v0, :cond_40

    #@16
    move v0, v2

    #@17
    :goto_17
    add-int/2addr v3, v0

    #@18
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@1a
    if-nez v0, :cond_47

    #@1c
    move v0, v2

    #@1d
    :goto_1d
    add-int/2addr v3, v0

    #@1e
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@20
    if-nez v0, :cond_4e

    #@22
    move v0, v2

    #@23
    :goto_23
    add-int/2addr v0, v3

    #@24
    iget v3, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@26
    add-int/2addr v0, v3

    #@27
    iget v3, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@29
    add-int/2addr v3, v0

    #@2a
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@2c
    if-eqz v0, :cond_55

    #@2e
    move v0, v1

    #@2f
    :goto_2f
    add-int/2addr v3, v0

    #@30
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@32
    if-eqz v0, :cond_57

    #@34
    move v0, v1

    #@35
    :goto_35
    add-int/2addr v0, v3

    #@36
    iget-boolean v3, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@38
    if-eqz v3, :cond_59

    #@3a
    :goto_3a
    add-int/2addr v0, v1

    #@3b
    return v0

    #@3c
    :cond_3c
    move v0, v2

    #@3d
    goto :goto_b

    #@3e
    :cond_3e
    move v0, v2

    #@3f
    goto :goto_11

    #@40
    :cond_40
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@42
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@45
    move-result v0

    #@46
    goto :goto_17

    #@47
    :cond_47
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@49
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@4c
    move-result v0

    #@4d
    goto :goto_1d

    #@4e
    :cond_4e
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@50
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@53
    move-result v0

    #@54
    goto :goto_23

    #@55
    :cond_55
    move v0, v2

    #@56
    goto :goto_2f

    #@57
    :cond_57
    move v0, v2

    #@58
    goto :goto_35

    #@59
    :cond_59
    move v1, v2

    #@5a
    goto :goto_3a
.end method

.method public isDataSearching()Z
    .registers 2

    #@0
    .prologue
    .line 997
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@2
    return v0
.end method

.method public isEmergencyOnly()Z
    .registers 2

    #@0
    .prologue
    .line 388
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@2
    return v0
.end method

.method public isVoiceSearching()Z
    .registers 2

    #@0
    .prologue
    .line 984
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@2
    return v0
.end method

.method public setCdmaDefaultRoamingIndicator(I)V
    .registers 2
    .parameter "roaming"

    #@0
    .prologue
    .line 705
    iput p1, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@2
    .line 706
    return-void
.end method

.method public setCdmaEriIconIndex(I)V
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 712
    iput p1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@2
    .line 713
    return-void
.end method

.method public setCdmaEriIconMode(I)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 719
    iput p1, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@2
    .line 720
    return-void
.end method

.method public setCdmaRoamingIndicator(I)V
    .registers 2
    .parameter "roaming"

    #@0
    .prologue
    .line 698
    iput p1, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@2
    .line 699
    return-void
.end method

.method public setCssIndicator(I)V
    .registers 3
    .parameter "css"

    #@0
    .prologue
    .line 841
    if-eqz p1, :cond_6

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    iput-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@5
    .line 842
    return-void

    #@6
    .line 841
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_3
.end method

.method public setDataRoaming(Z)V
    .registers 2
    .parameter "dataRoaming"

    #@0
    .prologue
    .line 683
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@2
    .line 684
    return-void
.end method

.method public setDataSearching(Z)V
    .registers 2
    .parameter "isDataSearching"

    #@0
    .prologue
    .line 1004
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@2
    .line 1005
    return-void
.end method

.method public setDataState(I)V
    .registers 2
    .parameter "dataState"

    #@0
    .prologue
    .line 664
    iput p1, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@2
    .line 665
    return-void
.end method

.method public setEmergencyOnly(Z)V
    .registers 2
    .parameter "emergencyOnly"

    #@0
    .prologue
    .line 691
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@2
    .line 692
    return-void
.end method

.method public setIsManualSelection(Z)V
    .registers 2
    .parameter "isManual"

    #@0
    .prologue
    .line 739
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@2
    .line 740
    return-void
.end method

.method public setLgeRssiData(Lcom/android/internal/telephony/LgeRssiData;)V
    .registers 4
    .parameter "instance"

    #@0
    .prologue
    .line 1010
    sput-object p1, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@2
    .line 1011
    sget-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@4
    if-nez v0, :cond_19

    #@6
    .line 1012
    invoke-static {}, Lcom/android/internal/telephony/LgeRssiData;->getInstance()Lcom/android/internal/telephony/LgeRssiData;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@c
    .line 1013
    sget-object v0, Landroid/telephony/ServiceState;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeRssiData;->init()V

    #@11
    .line 1014
    const-string v0, "PHONE"

    #@13
    const-string/jumbo v1, "setLgeRssiData instance is null"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1018
    :cond_19
    return-void
.end method

.method public setOperatorAlphaLong(Ljava/lang/String;)V
    .registers 2
    .parameter "longName"

    #@0
    .prologue
    .line 735
    iput-object p1, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@2
    .line 736
    return-void
.end method

.method public setOperatorName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "longName"
    .parameter "shortName"
    .parameter "numeric"

    #@0
    .prologue
    .line 723
    iput-object p1, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@2
    .line 724
    iput-object p2, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@4
    .line 725
    iput-object p3, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@6
    .line 726
    return-void
.end method

.method public setRadioTechnology(I)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 836
    iput p1, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@2
    .line 837
    return-void
.end method

.method public setRoaming(Z)V
    .registers 2
    .parameter "roaming"

    #@0
    .prologue
    .line 668
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@2
    .line 669
    return-void
.end method

.method public setState(I)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 659
    iput p1, p0, Landroid/telephony/ServiceState;->mState:I

    #@2
    .line 660
    return-void
.end method

.method public setStateOff()V
    .registers 2

    #@0
    .prologue
    .line 655
    const/4 v0, 0x3

    #@1
    invoke-direct {p0, v0}, Landroid/telephony/ServiceState;->setNullState(I)V

    #@4
    .line 656
    return-void
.end method

.method public setStateOutOfService()V
    .registers 2

    #@0
    .prologue
    .line 651
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/telephony/ServiceState;->setNullState(I)V

    #@4
    .line 652
    return-void
.end method

.method public setSystemAndNetworkId(II)V
    .registers 3
    .parameter "systemId"
    .parameter "networkId"

    #@0
    .prologue
    .line 846
    iput p1, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@2
    .line 847
    iput p2, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@4
    .line 848
    return-void
.end method

.method public setVoiceRoaming(Z)V
    .registers 2
    .parameter "voiceRoaming"

    #@0
    .prologue
    .line 676
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@2
    .line 677
    return-void
.end method

.method public setVoiceSearching(Z)V
    .registers 2
    .parameter "isVoiceSearching"

    #@0
    .prologue
    .line 991
    iput-boolean p1, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@2
    .line 992
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 597
    iget v1, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@2
    invoke-static {v1}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 599
    .local v0, radioTechnology:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    iget v2, p0, Landroid/telephony/ServiceState;->mState:I

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@19
    if-eqz v1, :cond_f6

    #@1b
    const-string/jumbo v1, "roaming"

    #@1e
    :goto_1e
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, " "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@2a
    if-eqz v1, :cond_fa

    #@2c
    const-string/jumbo v1, "voice roaming"

    #@2f
    :goto_2f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, " "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@3b
    if-eqz v1, :cond_fe

    #@3d
    const-string v1, "data roaming"

    #@3f
    :goto_3f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    const-string v2, " "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    iget-object v2, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, " "

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget-object v2, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    const-string v2, " "

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    iget-object v2, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    const-string v2, " "

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@6f
    if-eqz v1, :cond_102

    #@71
    const-string v1, "(manual)"

    #@73
    :goto_73
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    const-string v2, " "

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    const-string v2, " "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    iget-boolean v1, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@89
    if-eqz v1, :cond_106

    #@8b
    const-string v1, "CSS supported"

    #@8d
    :goto_8d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    const-string v2, " "

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v1

    #@97
    iget v2, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    const-string v2, " "

    #@9f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    iget v2, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v1

    #@a9
    const-string v2, " mDataState="

    #@ab
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v1

    #@af
    iget v2, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@b1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v1

    #@b5
    const-string v2, " RoamInd="

    #@b7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v1

    #@bb
    iget v2, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@bd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v1

    #@c1
    const-string v2, " DefRoamInd="

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v1

    #@c7
    iget v2, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@c9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v1

    #@cd
    const-string v2, " EmergOnly="

    #@cf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v1

    #@d3
    iget-boolean v2, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@d5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v1

    #@d9
    const-string v2, " mIsVoiceSearching="

    #@db
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v1

    #@df
    iget-boolean v2, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@e1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v1

    #@e5
    const-string v2, " mIsDataSearching="

    #@e7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v1

    #@eb
    iget-boolean v2, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@ed
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v1

    #@f1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v1

    #@f5
    return-object v1

    #@f6
    :cond_f6
    const-string v1, "home"

    #@f8
    goto/16 :goto_1e

    #@fa
    :cond_fa
    const-string v1, ""

    #@fc
    goto/16 :goto_2f

    #@fe
    :cond_fe
    const-string v1, ""

    #@100
    goto/16 :goto_3f

    #@102
    :cond_102
    const-string v1, ""

    #@104
    goto/16 :goto_73

    #@106
    :cond_106
    const-string v1, "CSS not supported"

    #@108
    goto :goto_8d
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 279
    iget v0, p0, Landroid/telephony/ServiceState;->mState:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 280
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mRoaming:Z

    #@9
    if-eqz v0, :cond_88

    #@b
    move v0, v1

    #@c
    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 282
    const/4 v0, 0x0

    #@10
    const-string/jumbo v3, "vzw_roaming_state"

    #@13
    invoke-static {v0, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_29

    #@19
    .line 283
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mVoiceRoaming:Z

    #@1b
    if-eqz v0, :cond_8a

    #@1d
    move v0, v1

    #@1e
    :goto_1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 284
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mDataRoaming:Z

    #@23
    if-eqz v0, :cond_8c

    #@25
    move v0, v1

    #@26
    :goto_26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 287
    :cond_29
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaLong:Ljava/lang/String;

    #@2b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2e
    .line 288
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorAlphaShort:Ljava/lang/String;

    #@30
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@33
    .line 289
    iget-object v0, p0, Landroid/telephony/ServiceState;->mOperatorNumeric:Ljava/lang/String;

    #@35
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@38
    .line 290
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsManualNetworkSelection:Z

    #@3a
    if-eqz v0, :cond_8e

    #@3c
    move v0, v1

    #@3d
    :goto_3d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 291
    iget v0, p0, Landroid/telephony/ServiceState;->mRadioTechnology:I

    #@42
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    .line 292
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mCssIndicator:Z

    #@47
    if-eqz v0, :cond_90

    #@49
    move v0, v1

    #@4a
    :goto_4a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4d
    .line 293
    iget v0, p0, Landroid/telephony/ServiceState;->mNetworkId:I

    #@4f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    .line 294
    iget v0, p0, Landroid/telephony/ServiceState;->mSystemId:I

    #@54
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    .line 295
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaRoamingIndicator:I

    #@59
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    .line 296
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaDefaultRoamingIndicator:I

    #@5e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@61
    .line 297
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconIndex:I

    #@63
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@66
    .line 298
    iget v0, p0, Landroid/telephony/ServiceState;->mCdmaEriIconMode:I

    #@68
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6b
    .line 299
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsEmergencyOnly:Z

    #@6d
    if-eqz v0, :cond_92

    #@6f
    move v0, v1

    #@70
    :goto_70
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@73
    .line 300
    iget v0, p0, Landroid/telephony/ServiceState;->mDataState:I

    #@75
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@78
    .line 302
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsVoiceSearching:Z

    #@7a
    if-eqz v0, :cond_94

    #@7c
    move v0, v1

    #@7d
    :goto_7d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@80
    .line 303
    iget-boolean v0, p0, Landroid/telephony/ServiceState;->mIsDataSearching:Z

    #@82
    if-eqz v0, :cond_96

    #@84
    :goto_84
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@87
    .line 306
    return-void

    #@88
    :cond_88
    move v0, v2

    #@89
    .line 280
    goto :goto_c

    #@8a
    :cond_8a
    move v0, v2

    #@8b
    .line 283
    goto :goto_1e

    #@8c
    :cond_8c
    move v0, v2

    #@8d
    .line 284
    goto :goto_26

    #@8e
    :cond_8e
    move v0, v2

    #@8f
    .line 290
    goto :goto_3d

    #@90
    :cond_90
    move v0, v2

    #@91
    .line 292
    goto :goto_4a

    #@92
    :cond_92
    move v0, v2

    #@93
    .line 299
    goto :goto_70

    #@94
    :cond_94
    move v0, v2

    #@95
    .line 302
    goto :goto_7d

    #@96
    :cond_96
    move v1, v2

    #@97
    .line 303
    goto :goto_84
.end method
