.class public final Landroid/telephony/CellIdentityCdma;
.super Ljava/lang/Object;
.source "CellIdentityCdma.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellIdentityCdma;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "CellSignalStrengthCdma"


# instance fields
.field private final mBasestationId:I

.field private final mLatitude:I

.field private final mLongitude:I

.field private final mNetworkId:I

.field private final mSystemId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 205
    new-instance v0, Landroid/telephony/CellIdentityCdma$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellIdentityCdma$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellIdentityCdma;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 56
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@8
    .line 57
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@a
    .line 58
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@c
    .line 59
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@e
    .line 60
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@10
    .line 61
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 6
    .parameter "nid"
    .parameter "sid"
    .parameter "bid"
    .parameter "lon"
    .parameter "lat"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    iput p1, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@5
    .line 77
    iput p2, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@7
    .line 78
    iput p3, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@9
    .line 79
    iput p4, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@b
    .line 80
    iput p5, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@d
    .line 81
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 194
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@9
    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@f
    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@15
    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@1b
    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@21
    .line 201
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellIdentityCdma$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/telephony/CellIdentityCdma;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/telephony/CellIdentityCdma;)V
    .registers 3
    .parameter "cid"

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 84
    iget v0, p1, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@5
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@7
    .line 85
    iget v0, p1, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@9
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@b
    .line 86
    iget v0, p1, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@d
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@f
    .line 87
    iget v0, p1, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@11
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@13
    .line 88
    iget v0, p1, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@15
    iput v0, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@17
    .line 89
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 222
    const-string v0, "CellSignalStrengthCdma"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 223
    return-void
.end method


# virtual methods
.method copy()Landroid/telephony/CellIdentityCdma;
    .registers 2

    #@0
    .prologue
    .line 92
    new-instance v0, Landroid/telephony/CellIdentityCdma;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellIdentityCdma;-><init>(Landroid/telephony/CellIdentityCdma;)V

    #@5
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 179
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "other"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 147
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@4
    move-result v4

    #@5
    if-eqz v4, :cond_2a

    #@7
    .line 149
    :try_start_7
    move-object v0, p1

    #@8
    check-cast v0, Landroid/telephony/CellIdentityCdma;

    #@a
    move-object v2, v0

    #@b
    .line 150
    .local v2, o:Landroid/telephony/CellIdentityCdma;
    iget v4, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@d
    iget v5, v2, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@f
    if-ne v4, v5, :cond_2a

    #@11
    iget v4, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@13
    iget v5, v2, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@15
    if-ne v4, v5, :cond_2a

    #@17
    iget v4, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@19
    iget v5, v2, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@1b
    if-ne v4, v5, :cond_2a

    #@1d
    iget v4, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@1f
    iget v5, v2, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@21
    if-ne v4, v5, :cond_2a

    #@23
    iget v4, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@25
    iget v5, v2, Landroid/telephony/CellIdentityCdma;->mLongitude:I
    :try_end_27
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_27} :catch_2b

    #@27
    if-ne v4, v5, :cond_2a

    #@29
    const/4 v3, 0x1

    #@2a
    .line 159
    .end local v2           #o:Landroid/telephony/CellIdentityCdma;
    :cond_2a
    :goto_2a
    return v3

    #@2b
    .line 155
    :catch_2b
    move-exception v1

    #@2c
    .line 156
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_2a
.end method

.method public getBasestationId()I
    .registers 2

    #@0
    .prologue
    .line 113
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@2
    return v0
.end method

.method public getLatitude()I
    .registers 2

    #@0
    .prologue
    .line 135
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@2
    return v0
.end method

.method public getLongitude()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@2
    return v0
.end method

.method public getNetworkId()I
    .registers 2

    #@0
    .prologue
    .line 99
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@2
    return v0
.end method

.method public getSystemId()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 140
    const/16 v0, 0x1f

    #@2
    .line 141
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@b
    mul-int/2addr v2, v0

    #@c
    add-int/2addr v1, v2

    #@d
    iget v2, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@f
    mul-int/2addr v2, v0

    #@10
    add-int/2addr v1, v2

    #@11
    iget v2, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@13
    mul-int/2addr v2, v0

    #@14
    add-int/2addr v1, v2

    #@15
    return v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "CdmaCellIdentitiy:"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 166
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 167
    const-string v1, " mNetworkId="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    iget v1, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    .line 168
    const-string v1, " mSystemId="

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    iget v1, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    .line 169
    const-string v1, " mBasestationId="

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    iget v1, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    .line 170
    const-string v1, " mLongitude="

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    iget v1, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    .line 171
    const-string v1, " mLatitude="

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    iget v1, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    .line 173
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 186
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mNetworkId:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 187
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mSystemId:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 188
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mBasestationId:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 189
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mLongitude:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 190
    iget v0, p0, Landroid/telephony/CellIdentityCdma;->mLatitude:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 191
    return-void
.end method
