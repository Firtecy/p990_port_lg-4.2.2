.class public Landroid/telephony/SignalStrength;
.super Ljava/lang/Object;
.source "SignalStrength.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/SignalStrength;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = true

.field public static final INVALID:I = 0x7fffffff

.field private static final LOG_TAG:Ljava/lang/String; = "SignalStrength"

.field public static final NUM_SIGNAL_STRENGTH_BEST:I = 0x6

#the value of this static final field might be set in the static constructor
.field public static final NUM_SIGNAL_STRENGTH_BINS:I = 0x0

.field public static final SIGNAL_STRENGTH_AWESOME:I = 0x5

.field public static final SIGNAL_STRENGTH_GOOD:I = 0x3

.field public static final SIGNAL_STRENGTH_GREAT:I = 0x4

.field public static final SIGNAL_STRENGTH_MODERATE:I = 0x2

.field public static final SIGNAL_STRENGTH_NAMES:[Ljava/lang/String; = null

.field public static final SIGNAL_STRENGTH_NONE_OR_UNKNOWN:I = 0x0

.field public static final SIGNAL_STRENGTH_POOR:I = 0x1

.field private static lastEcIoIndex:I

.field private static lastEcIoValues:[I

.field private static mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;


# instance fields
.field private LGE_GSM_ASU_VALUE:[I

.field private LGE_LTE_RSRP_VALUE:[I

.field private LGE_UMTS_ASU_VALUE:[I

.field private datafeature:I

.field private isGsm:Z

.field private mCdmaDbm:I

.field private mCdmaEcio:I

.field private mEvdoDbm:I

.field private mEvdoEcio:I

.field private mEvdoSnr:I

.field private mGsmBitErrorRate:I

.field private mGsmSignalStrength:I

.field private mLteCqi:I

.field private mLteRsrp:I

.field private mLteRsrq:I

.field private mLteRssnr:I

.field private mLteSignalStrength:I

.field private mRadioTechnology:I

.field private mTdScdmaRscp:I

.field private sDataNetType:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v3, 0x0

    #@2
    .line 75
    invoke-static {}, Landroid/telephony/SignalStrength;->getNumSignalStrength()I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@8
    .line 77
    const/4 v0, 0x7

    #@9
    new-array v0, v0, [Ljava/lang/String;

    #@b
    const-string/jumbo v1, "none"

    #@e
    aput-object v1, v0, v3

    #@10
    const/4 v1, 0x1

    #@11
    const-string/jumbo v2, "poor"

    #@14
    aput-object v2, v0, v1

    #@16
    const/4 v1, 0x2

    #@17
    const-string/jumbo v2, "moderate"

    #@1a
    aput-object v2, v0, v1

    #@1c
    const/4 v1, 0x3

    #@1d
    const-string v2, "good"

    #@1f
    aput-object v2, v0, v1

    #@21
    const/4 v1, 0x4

    #@22
    const-string v2, "great"

    #@24
    aput-object v2, v0, v1

    #@26
    const-string v1, "awesome"

    #@28
    aput-object v1, v0, v4

    #@2a
    const/4 v1, 0x6

    #@2b
    const-string v2, "best"

    #@2d
    aput-object v2, v0, v1

    #@2f
    sput-object v0, Landroid/telephony/SignalStrength;->SIGNAL_STRENGTH_NAMES:[Ljava/lang/String;

    #@31
    .line 115
    new-array v0, v4, [I

    #@33
    fill-array-data v0, :array_42

    #@36
    sput-object v0, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@38
    .line 117
    sput v3, Landroid/telephony/SignalStrength;->lastEcIoIndex:I

    #@3a
    .line 419
    new-instance v0, Landroid/telephony/SignalStrength$1;

    #@3c
    invoke-direct {v0}, Landroid/telephony/SignalStrength$1;-><init>()V

    #@3f
    sput-object v0, Landroid/telephony/SignalStrength;->CREATOR:Landroid/os/Parcelable$Creator;

    #@41
    return-void

    #@42
    .line 115
    :array_42
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x63

    #@2
    const/4 v4, 0x4

    #@3
    const/4 v3, 0x0

    #@4
    const v2, 0x7fffffff

    #@7
    const/4 v1, -0x1

    #@8
    .line 151
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    .line 108
    iput v3, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@d
    .line 121
    new-array v0, v4, [I

    #@f
    fill-array-data v0, :array_44

    #@12
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@14
    .line 122
    new-array v0, v4, [I

    #@16
    fill-array-data v0, :array_50

    #@19
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@1b
    .line 123
    new-array v0, v4, [I

    #@1d
    fill-array-data v0, :array_5c

    #@20
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@22
    .line 152
    iput v5, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@24
    .line 153
    iput v1, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@26
    .line 154
    iput v1, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@28
    .line 155
    iput v1, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@2a
    .line 156
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@2c
    .line 157
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@2e
    .line 158
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@30
    .line 159
    iput v5, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@32
    .line 160
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@34
    .line 161
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@36
    .line 162
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@38
    .line 163
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@3a
    .line 164
    iput v2, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@3c
    .line 165
    const/4 v0, 0x1

    #@3d
    iput-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@3f
    .line 167
    iput v3, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@41
    .line 171
    iput v3, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@43
    .line 173
    return-void

    #@44
    .line 121
    :array_44
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@50
    .line 122
    :array_50
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5c
    .line 123
    :array_5c
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(IIIIIIIIIIIIIZ)V
    .registers 30
    .parameter "gsmSignalStrength"
    .parameter "gsmBitErrorRate"
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"
    .parameter "lteSignalStrength"
    .parameter "lteRsrp"
    .parameter "lteRsrq"
    .parameter "lteRssnr"
    .parameter "lteCqi"
    .parameter "tdScdmaRscp"
    .parameter "gsmFlag"

    #@0
    .prologue
    .line 224
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    const/4 v1, 0x0

    #@4
    iput v1, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@6
    .line 121
    const/4 v1, 0x4

    #@7
    new-array v1, v1, [I

    #@9
    fill-array-data v1, :array_42

    #@c
    iput-object v1, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@e
    .line 122
    const/4 v1, 0x4

    #@f
    new-array v1, v1, [I

    #@11
    fill-array-data v1, :array_4e

    #@14
    iput-object v1, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@16
    .line 123
    const/4 v1, 0x4

    #@17
    new-array v1, v1, [I

    #@19
    fill-array-data v1, :array_5a

    #@1c
    iput-object v1, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@1e
    move-object v1, p0

    #@1f
    move/from16 v2, p1

    #@21
    move/from16 v3, p2

    #@23
    move/from16 v4, p3

    #@25
    move/from16 v5, p4

    #@27
    move/from16 v6, p5

    #@29
    move/from16 v7, p6

    #@2b
    move/from16 v8, p7

    #@2d
    move/from16 v9, p8

    #@2f
    move/from16 v10, p9

    #@31
    move/from16 v11, p10

    #@33
    move/from16 v12, p11

    #@35
    move/from16 v13, p12

    #@37
    move/from16 v14, p14

    #@39
    .line 225
    invoke-virtual/range {v1 .. v14}, Landroid/telephony/SignalStrength;->initialize(IIIIIIIIIIIIZ)V

    #@3c
    .line 228
    move/from16 v0, p13

    #@3e
    iput v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@40
    .line 229
    return-void

    #@41
    .line 121
    nop

    #@42
    :array_42
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@4e
    .line 122
    :array_4e
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@5a
    .line 123
    :array_5a
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(IIIIIIIIIIIIZ)V
    .registers 16
    .parameter "gsmSignalStrength"
    .parameter "gsmBitErrorRate"
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"
    .parameter "lteSignalStrength"
    .parameter "lteRsrp"
    .parameter "lteRsrq"
    .parameter "lteRssnr"
    .parameter "lteCqi"
    .parameter "gsmFlag"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    .line 209
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 108
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@7
    .line 121
    new-array v0, v1, [I

    #@9
    fill-array-data v0, :array_20

    #@c
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@e
    .line 122
    new-array v0, v1, [I

    #@10
    fill-array-data v0, :array_2c

    #@13
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@15
    .line 123
    new-array v0, v1, [I

    #@17
    fill-array-data v0, :array_38

    #@1a
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@1c
    .line 210
    invoke-virtual/range {p0 .. p13}, Landroid/telephony/SignalStrength;->initialize(IIIIIIIIIIIIZ)V

    #@1f
    .line 213
    return-void

    #@20
    .line 121
    :array_20
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@2c
    .line 122
    :array_2c
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@38
    .line 123
    :array_38
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(IIIIIIIZ)V
    .registers 23
    .parameter "gsmSignalStrength"
    .parameter "gsmBitErrorRate"
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"
    .parameter "gsmFlag"

    #@0
    .prologue
    .line 239
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@6
    .line 121
    const/4 v0, 0x4

    #@7
    new-array v0, v0, [I

    #@9
    fill-array-data v0, :array_40

    #@c
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@e
    .line 122
    const/4 v0, 0x4

    #@f
    new-array v0, v0, [I

    #@11
    fill-array-data v0, :array_4c

    #@14
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@16
    .line 123
    const/4 v0, 0x4

    #@17
    new-array v0, v0, [I

    #@19
    fill-array-data v0, :array_58

    #@1c
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@1e
    .line 240
    const/16 v8, 0x63

    #@20
    const v9, 0x7fffffff

    #@23
    const v10, 0x7fffffff

    #@26
    const v11, 0x7fffffff

    #@29
    const v12, 0x7fffffff

    #@2c
    move-object v0, p0

    #@2d
    move v1, p1

    #@2e
    move/from16 v2, p2

    #@30
    move/from16 v3, p3

    #@32
    move/from16 v4, p4

    #@34
    move/from16 v5, p5

    #@36
    move/from16 v6, p6

    #@38
    move/from16 v7, p7

    #@3a
    move/from16 v13, p8

    #@3c
    invoke-virtual/range {v0 .. v13}, Landroid/telephony/SignalStrength;->initialize(IIIIIIIIIIIIZ)V

    #@3f
    .line 243
    return-void

    #@40
    .line 121
    :array_40
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@4c
    .line 122
    :array_4c
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@58
    .line 123
    :array_58
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x4

    #@2
    .line 352
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 108
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@7
    .line 121
    new-array v1, v2, [I

    #@9
    fill-array-data v1, :array_9a

    #@c
    iput-object v1, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@e
    .line 122
    new-array v1, v2, [I

    #@10
    fill-array-data v1, :array_a6

    #@13
    iput-object v1, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@15
    .line 123
    new-array v1, v2, [I

    #@17
    fill-array-data v1, :array_b2

    #@1a
    iput-object v1, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@1c
    .line 353
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v2, "Size of signalstrength parcel:"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {p1}, Landroid/os/Parcel;->dataSize()I

    #@2a
    move-result v2

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v1}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@36
    .line 355
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v1

    #@3a
    iput v1, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@3c
    .line 356
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v1

    #@40
    iput v1, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@42
    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v1

    #@46
    iput v1, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@48
    .line 358
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v1

    #@4c
    iput v1, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@4e
    .line 359
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v1

    #@52
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@54
    .line 360
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v1

    #@58
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@5a
    .line 361
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v1

    #@5e
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@60
    .line 362
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v1

    #@64
    iput v1, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@66
    .line 363
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v1

    #@6a
    iput v1, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@6c
    .line 364
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v1

    #@70
    iput v1, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@72
    .line 365
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@75
    move-result v1

    #@76
    iput v1, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@78
    .line 366
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7b
    move-result v1

    #@7c
    iput v1, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@7e
    .line 367
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v1

    #@82
    iput v1, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@84
    .line 368
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v1

    #@88
    if-eqz v1, :cond_8b

    #@8a
    const/4 v0, 0x1

    #@8b
    :cond_8b
    iput-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@8d
    .line 371
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v0

    #@91
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@93
    .line 375
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@96
    move-result v0

    #@97
    iput v0, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@99
    .line 377
    return-void

    #@9a
    .line 121
    :array_9a
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@a6
    .line 122
    :array_a6
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@b2
    .line 123
    :array_b2
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/telephony/SignalStrength;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    .line 252
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 108
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@7
    .line 121
    new-array v0, v1, [I

    #@9
    fill-array-data v0, :array_20

    #@c
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@e
    .line 122
    new-array v0, v1, [I

    #@10
    fill-array-data v0, :array_2c

    #@13
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@15
    .line 123
    new-array v0, v1, [I

    #@17
    fill-array-data v0, :array_38

    #@1a
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@1c
    .line 253
    invoke-virtual {p0, p1}, Landroid/telephony/SignalStrength;->copyFrom(Landroid/telephony/SignalStrength;)V

    #@1f
    .line 254
    return-void

    #@20
    .line 121
    :array_20
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@2c
    .line 122
    :array_2c
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@38
    .line 123
    :array_38
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Z)V
    .registers 7
    .parameter "gsmFlag"

    #@0
    .prologue
    const/16 v4, 0x63

    #@2
    const/4 v3, 0x4

    #@3
    const v2, 0x7fffffff

    #@6
    const/4 v1, -0x1

    #@7
    .line 183
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@a
    .line 108
    const/4 v0, 0x0

    #@b
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@d
    .line 121
    new-array v0, v3, [I

    #@f
    fill-array-data v0, :array_40

    #@12
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@14
    .line 122
    new-array v0, v3, [I

    #@16
    fill-array-data v0, :array_4c

    #@19
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@1b
    .line 123
    new-array v0, v3, [I

    #@1d
    fill-array-data v0, :array_58

    #@20
    iput-object v0, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@22
    .line 184
    iput v4, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@24
    .line 185
    iput v1, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@26
    .line 186
    iput v1, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@28
    .line 187
    iput v1, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@2a
    .line 188
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@2c
    .line 189
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@2e
    .line 190
    iput v1, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@30
    .line 191
    iput v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@32
    .line 192
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@34
    .line 193
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@36
    .line 194
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@38
    .line 195
    iput v2, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@3a
    .line 196
    iput v2, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@3c
    .line 197
    iput-boolean p1, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@3e
    .line 198
    return-void

    #@3f
    .line 121
    nop

    #@40
    :array_40
    .array-data 0x4
        0xact 0xfft 0xfft 0xfft
        0xa2t 0xfft 0xfft 0xfft
        0x98t 0xfft 0xfft 0xfft
        0x8et 0xfft 0xfft 0xfft
    .end array-data

    #@4c
    .line 122
    :array_4c
    .array-data 0x4
        0x11t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    #@58
    .line 123
    :array_58
    .array-data 0x4
        0x13t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private getATTGsmUmtsLevel()I
    .registers 6

    #@0
    .prologue
    .line 1683
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    #@3
    move-result v1

    #@4
    .line 1684
    .local v1, asu_dBm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    #@7
    move-result v0

    #@8
    .line 1691
    .local v0, asu_Ecio:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@b
    move-result v3

    #@c
    packed-switch v3, :pswitch_data_8c

    #@f
    .line 1711
    :pswitch_f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "RadioTech is = "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@1d
    move-result v4

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, " asu_dBm ="

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, " asu_Ecio="

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@3d
    .line 1713
    const v3, -0x37849

    #@40
    if-ne v0, v3, :cond_87

    #@42
    .line 1715
    invoke-direct {p0, v1}, Landroid/telephony/SignalStrength;->getRSSIindexGSM(I)I

    #@45
    move-result v2

    #@46
    .line 1723
    .local v2, level:I
    :goto_46
    return v2

    #@47
    .line 1696
    .end local v2           #level:I
    :pswitch_47
    new-instance v3, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v4, "GSM asu_dBm ="

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@5d
    .line 1697
    invoke-direct {p0, v1}, Landroid/telephony/SignalStrength;->getRSSIindexGSM(I)I

    #@60
    move-result v2

    #@61
    .line 1699
    .restart local v2       #level:I
    goto :goto_46

    #@62
    .line 1705
    .end local v2           #level:I
    :pswitch_62
    new-instance v3, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v4, "UMTS/HSPA  asu_dBm ="

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    const-string v4, " asu_Ecio="

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@82
    .line 1707
    invoke-direct {p0, v1, v0}, Landroid/telephony/SignalStrength;->getRSSIindexUMTS(II)I

    #@85
    move-result v2

    #@86
    .line 1708
    .restart local v2       #level:I
    goto :goto_46

    #@87
    .line 1717
    .end local v2           #level:I
    :cond_87
    invoke-direct {p0, v1, v0}, Landroid/telephony/SignalStrength;->getRSSIindexUMTS(II)I

    #@8a
    move-result v2

    #@8b
    .restart local v2       #level:I
    goto :goto_46

    #@8c
    .line 1691
    :pswitch_data_8c
    .packed-switch 0x1
        :pswitch_47
        :pswitch_47
        :pswitch_62
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_62
        :pswitch_62
        :pswitch_62
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_47
    .end packed-switch
.end method

.method private getItemLevel(II[I)I
    .registers 8
    .parameter "value"
    .parameter "valid"
    .parameter "range"

    #@0
    .prologue
    const v3, 0x7fffffff

    #@3
    const/16 v2, 0x63

    #@5
    .line 1811
    const/4 v1, 0x0

    #@6
    .line 1814
    .local v1, level:I
    if-nez p3, :cond_a

    #@8
    .line 1815
    const/4 v2, -0x1

    #@9
    .line 1826
    :goto_9
    return v2

    #@a
    .line 1817
    :cond_a
    if-ne p2, v2, :cond_e

    #@c
    if-eq p1, v2, :cond_14

    #@e
    :cond_e
    if-ne p2, v3, :cond_12

    #@10
    if-eq p1, v3, :cond_14

    #@12
    :cond_12
    if-le p1, p2, :cond_16

    #@14
    .line 1818
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_9

    #@16
    .line 1820
    :cond_16
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    :goto_17
    array-length v2, p3

    #@18
    if-ge v0, v2, :cond_1e

    #@1a
    .line 1821
    aget v2, p3, v0

    #@1c
    if-lt p1, v2, :cond_26

    #@1e
    .line 1825
    :cond_1e
    sget v2, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@20
    add-int/lit8 v3, v0, 0x1

    #@22
    sub-int v1, v2, v3

    #@24
    move v2, v1

    #@25
    .line 1826
    goto :goto_9

    #@26
    .line 1820
    :cond_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_17
.end method

.method private getItemLevel(I[I)I
    .registers 7
    .parameter "value"
    .parameter "range"

    #@0
    .prologue
    .line 1831
    const/4 v1, 0x0

    #@1
    .line 1834
    .local v1, level:I
    if-nez p2, :cond_5

    #@3
    .line 1835
    const/4 v2, -0x1

    #@4
    .line 1842
    :goto_4
    return v2

    #@5
    .line 1836
    :cond_5
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    array-length v2, p2

    #@7
    if-ge v0, v2, :cond_d

    #@9
    .line 1837
    aget v2, p2, v0

    #@b
    if-lt p1, v2, :cond_15

    #@d
    .line 1841
    :cond_d
    sget v2, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@f
    add-int/lit8 v3, v0, 0x1

    #@11
    sub-int v1, v2, v3

    #@13
    move v2, v1

    #@14
    .line 1842
    goto :goto_4

    #@15
    .line 1836
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_6
.end method

.method private getLevelKDDI()I
    .registers 6

    #@0
    .prologue
    .line 1641
    const/4 v2, 0x0

    #@1
    .line 1643
    .local v2, level:I
    iget-boolean v3, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@3
    if-eqz v3, :cond_56

    #@5
    .line 1644
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@8
    move-result v2

    #@9
    .line 1645
    if-nez v2, :cond_15

    #@b
    .line 1646
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaLevel()I

    #@e
    move-result v2

    #@f
    .line 1647
    if-nez v2, :cond_15

    #@11
    .line 1648
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmLevel()I

    #@14
    move-result v2

    #@15
    .line 1652
    :cond_15
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_3f

    #@1f
    iget v3, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@21
    const/16 v4, -0x78

    #@23
    if-eq v3, v4, :cond_3f

    #@25
    iget v3, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@27
    const/16 v4, -0xa0

    #@29
    if-eq v3, v4, :cond_3f

    #@2b
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@32
    move-result v3

    #@33
    const/4 v4, 0x2

    #@34
    if-ne v3, v4, :cond_3f

    #@36
    .line 1655
    const-string v3, "######### myoungsuk.kim KDDI signalstrength ########"

    #@38
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@3b
    .line 1656
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@3e
    move-result v2

    #@3f
    .line 1673
    :cond_3f
    :goto_3f
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v4, "getLevelKDDI="

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@55
    .line 1674
    return v2

    #@56
    .line 1660
    :cond_56
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@59
    move-result v0

    #@5a
    .line 1661
    .local v0, cdmaLevel:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    #@5d
    move-result v1

    #@5e
    .line 1662
    .local v1, evdoLevel:I
    if-nez v1, :cond_62

    #@60
    .line 1664
    move v2, v0

    #@61
    goto :goto_3f

    #@62
    .line 1665
    :cond_62
    if-nez v0, :cond_66

    #@64
    .line 1667
    move v2, v1

    #@65
    goto :goto_3f

    #@66
    .line 1670
    :cond_66
    if-ge v0, v1, :cond_6a

    #@68
    move v2, v0

    #@69
    :goto_69
    goto :goto_3f

    #@6a
    :cond_6a
    move v2, v1

    #@6b
    goto :goto_69
.end method

.method private getLevelKR()I
    .registers 4

    #@0
    .prologue
    .line 639
    iget-boolean v1, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@2
    if-eqz v1, :cond_29

    #@4
    .line 642
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@7
    move-result v0

    #@8
    .line 643
    .local v0, level:I
    if-nez v0, :cond_e

    #@a
    .line 644
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmLevel()I

    #@d
    move-result v0

    #@e
    .line 660
    :cond_e
    :goto_e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "getLevelKR level = "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v1}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@24
    .line 662
    const/4 v1, -0x1

    #@25
    if-ne v0, v1, :cond_28

    #@27
    const/4 v0, 0x0

    #@28
    .end local v0           #level:I
    :cond_28
    return v0

    #@29
    .line 657
    :cond_29
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@2c
    move-result v0

    #@2d
    .restart local v0       #level:I
    goto :goto_e
.end method

.method private getLevelSPR()I
    .registers 4

    #@0
    .prologue
    .line 1573
    const/4 v0, 0x0

    #@1
    .line 1575
    .local v0, level:I
    iget-boolean v1, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@3
    if-eqz v1, :cond_20

    #@5
    .line 1576
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmLevel()I

    #@8
    move-result v0

    #@9
    .line 1585
    :cond_9
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Report RSSI  (SPR) level = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v1}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@1f
    .line 1586
    return v0

    #@20
    .line 1578
    :cond_20
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@23
    move-result v0

    #@24
    .line 1579
    if-nez v0, :cond_9

    #@26
    .line 1581
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@29
    move-result v0

    #@2a
    goto :goto_9
.end method

.method private getLevelVZW()I
    .registers 7

    #@0
    .prologue
    .line 1593
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v4

    #@4
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@7
    move-result v3

    #@8
    .line 1595
    .local v3, networkType:I
    iget-boolean v4, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@a
    if-eqz v4, :cond_41

    #@c
    .line 1596
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@f
    move-result v2

    #@10
    .line 1597
    .local v2, level:I
    if-nez v2, :cond_2a

    #@12
    .line 1598
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmLevel()I

    #@15
    move-result v2

    #@16
    .line 1599
    if-nez v2, :cond_2a

    #@18
    .line 1600
    const/4 v4, 0x5

    #@19
    if-eq v3, v4, :cond_26

    #@1b
    const/4 v4, 0x6

    #@1c
    if-eq v3, v4, :cond_26

    #@1e
    const/16 v4, 0xc

    #@20
    if-eq v3, v4, :cond_26

    #@22
    const/16 v4, 0xe

    #@24
    if-ne v3, v4, :cond_2a

    #@26
    .line 1604
    :cond_26
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    #@29
    move-result v2

    #@2a
    .line 1632
    :cond_2a
    :goto_2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "getLevelVZW="

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-static {v4}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@40
    .line 1634
    return v2

    #@41
    .line 1609
    .end local v2           #level:I
    :cond_41
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@44
    move-result v2

    #@45
    .line 1610
    .restart local v2       #level:I
    if-nez v2, :cond_55

    #@47
    .line 1611
    packed-switch v3, :pswitch_data_82

    #@4a
    .line 1623
    :pswitch_4a
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@4d
    move-result v0

    #@4e
    .line 1624
    .local v0, cdmaLevel:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    #@51
    move-result v1

    #@52
    .line 1625
    .local v1, evdoLevel:I
    if-lt v0, v1, :cond_80

    #@54
    move v2, v0

    #@55
    .line 1629
    .end local v0           #cdmaLevel:I
    .end local v1           #evdoLevel:I
    :cond_55
    :goto_55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "getLevelVZW() notGSM - networkType = "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, " level = "

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    invoke-static {v4}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@75
    goto :goto_2a

    #@76
    .line 1614
    :pswitch_76
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@79
    move-result v2

    #@7a
    .line 1615
    goto :goto_55

    #@7b
    .line 1620
    :pswitch_7b
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    #@7e
    move-result v2

    #@7f
    .line 1621
    goto :goto_55

    #@80
    .restart local v0       #cdmaLevel:I
    .restart local v1       #evdoLevel:I
    :cond_80
    move v2, v1

    #@81
    .line 1625
    goto :goto_55

    #@82
    .line 1611
    :pswitch_data_82
    .packed-switch 0x4
        :pswitch_76
        :pswitch_7b
        :pswitch_7b
        :pswitch_76
        :pswitch_4a
        :pswitch_4a
        :pswitch_4a
        :pswitch_4a
        :pswitch_7b
        :pswitch_4a
        :pswitch_7b
    .end packed-switch
.end method

.method private getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;
    .registers 2

    #@0
    .prologue
    .line 1803
    sget-object v0, Landroid/telephony/SignalStrength;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@2
    if-nez v0, :cond_14

    #@4
    .line 1804
    invoke-static {}, Lcom/android/internal/telephony/LgeRssiData;->getInstance()Lcom/android/internal/telephony/LgeRssiData;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/telephony/SignalStrength;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@a
    .line 1805
    sget-object v0, Landroid/telephony/SignalStrength;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeRssiData;->init()V

    #@f
    .line 1806
    const-string v0, "getLgeRssiData: mLgeRssiData is null"

    #@11
    invoke-static {v0}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@14
    .line 1808
    :cond_14
    sget-object v0, Landroid/telephony/SignalStrength;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@16
    return-object v0
.end method

.method public static getNumSignalStrength()I
    .registers 2

    #@0
    .prologue
    .line 1553
    const-string v0, "BM"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    const-string v0, "SPR"

    #@a
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 1555
    :cond_10
    const/4 v0, 0x7

    #@11
    .line 1566
    :goto_11
    return v0

    #@12
    .line 1559
    :cond_12
    const-string v0, "DCM"

    #@14
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    .line 1561
    const/4 v0, 0x5

    #@1b
    goto :goto_11

    #@1c
    .line 1563
    :cond_1c
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v1, "getNumSignalStrength: mRssiLevel = "

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    sget v1, Landroid/telephony/ServiceState;->mRssiLevel:I

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    invoke-static {v0}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@34
    .line 1564
    sget v0, Landroid/telephony/ServiceState;->mRssiLevel:I

    #@36
    const/4 v1, -0x1

    #@37
    if-ne v0, v1, :cond_3b

    #@39
    .line 1565
    const/4 v0, 0x6

    #@3a
    goto :goto_11

    #@3b
    .line 1566
    :cond_3b
    sget v0, Landroid/telephony/ServiceState;->mRssiLevel:I

    #@3d
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_11
.end method

.method private getOperatorSpecLevel()I
    .registers 3

    #@0
    .prologue
    .line 1500
    const v0, 0xffff

    #@3
    .line 1503
    .local v0, level:I
    const-string v1, "KR"

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_10

    #@b
    .line 1504
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLevelKR()I

    #@e
    move-result v0

    #@f
    .line 1526
    .end local v0           #level:I
    :cond_f
    :goto_f
    return v0

    #@10
    .line 1509
    .restart local v0       #level:I
    :cond_10
    const-string v1, "VZW"

    #@12
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1d

    #@18
    .line 1510
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLevelVZW()I

    #@1b
    move-result v0

    #@1c
    goto :goto_f

    #@1d
    .line 1515
    :cond_1d
    const-string v1, "SPR"

    #@1f
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_2a

    #@25
    .line 1516
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLevelSPR()I

    #@28
    move-result v0

    #@29
    goto :goto_f

    #@2a
    .line 1521
    :cond_2a
    const-string v1, "KDDI"

    #@2c
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_f

    #@32
    .line 1522
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLevelKDDI()I

    #@35
    move-result v0

    #@36
    goto :goto_f
.end method

.method private final getRSSIindexGSM(I)I
    .registers 6
    .parameter "asu"

    #@0
    .prologue
    const/16 v3, -0x4f

    #@2
    const/16 v1, -0x58

    #@4
    const/16 v2, -0x61

    #@6
    .line 1727
    const/4 v0, 0x0

    #@7
    .line 1738
    .local v0, iconLevel:I
    if-lt p1, v3, :cond_b

    #@9
    const/4 v0, 0x5

    #@a
    .line 1746
    :goto_a
    return v0

    #@b
    .line 1739
    :cond_b
    if-lt p1, v1, :cond_11

    #@d
    if-ge p1, v3, :cond_11

    #@f
    const/4 v0, 0x4

    #@10
    goto :goto_a

    #@11
    .line 1740
    :cond_11
    if-lt p1, v2, :cond_17

    #@13
    if-ge p1, v1, :cond_17

    #@15
    const/4 v0, 0x3

    #@16
    goto :goto_a

    #@17
    .line 1741
    :cond_17
    const/16 v1, -0x67

    #@19
    if-lt p1, v1, :cond_1f

    #@1b
    if-ge p1, v2, :cond_1f

    #@1d
    const/4 v0, 0x2

    #@1e
    goto :goto_a

    #@1f
    .line 1742
    :cond_1f
    const/16 v1, -0x68

    #@21
    if-gt p1, v1, :cond_25

    #@23
    const/4 v0, 0x1

    #@24
    goto :goto_a

    #@25
    .line 1743
    :cond_25
    const/4 v0, 0x1

    #@26
    goto :goto_a
.end method

.method private final getRSSIindexUMTS(II)I
    .registers 12
    .parameter "asu"
    .parameter "asu_Ecio"

    #@0
    .prologue
    const/16 v8, -0x4f

    #@2
    const/16 v7, -0x59

    #@4
    const/16 v6, -0x63

    #@6
    const/16 v5, -0x69

    #@8
    .line 1750
    const/4 v1, 0x0

    #@9
    .line 1751
    .local v1, iconLevel_ecio:I
    const/4 v3, 0x0

    #@a
    .line 1752
    .local v3, iconLevel_ecio_sum:I
    const/4 v2, 0x0

    #@b
    .line 1753
    .local v2, iconLevel_ecio_avr:I
    const/4 v0, 0x0

    #@c
    .line 1763
    .local v0, iconLevel:I
    if-lt p1, v8, :cond_5b

    #@e
    const/4 v0, 0x5

    #@f
    .line 1770
    :goto_f
    const/16 v5, -0x64

    #@11
    if-le p2, v5, :cond_73

    #@13
    const/4 v1, 0x5

    #@14
    .line 1776
    :goto_14
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v6, "[BRIGHTHY] lastEcIoIndex="

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    sget v6, Landroid/telephony/SignalStrength;->lastEcIoIndex:I

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    const-string v6, " lastEcIoValues.length="

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    sget-object v6, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@2d
    array-length v6, v6

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    invoke-static {v5}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@39
    .line 1777
    sget-object v5, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@3b
    sget v6, Landroid/telephony/SignalStrength;->lastEcIoIndex:I

    #@3d
    add-int/lit8 v7, v6, 0x1

    #@3f
    sput v7, Landroid/telephony/SignalStrength;->lastEcIoIndex:I

    #@41
    aput v1, v5, v6

    #@43
    .line 1778
    sget v5, Landroid/telephony/SignalStrength;->lastEcIoIndex:I

    #@45
    sget-object v6, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@47
    array-length v6, v6

    #@48
    if-ne v5, v6, :cond_4d

    #@4a
    const/4 v5, 0x0

    #@4b
    sput v5, Landroid/telephony/SignalStrength;->lastEcIoIndex:I

    #@4d
    .line 1780
    :cond_4d
    const/4 v4, 0x0

    #@4e
    .local v4, kk:I
    :goto_4e
    sget-object v5, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@50
    array-length v5, v5

    #@51
    if-ge v4, v5, :cond_87

    #@53
    .line 1781
    sget-object v5, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@55
    aget v5, v5, v4

    #@57
    add-int/2addr v3, v5

    #@58
    .line 1780
    add-int/lit8 v4, v4, 0x1

    #@5a
    goto :goto_4e

    #@5b
    .line 1764
    .end local v4           #kk:I
    :cond_5b
    if-lt p1, v7, :cond_61

    #@5d
    if-ge p1, v8, :cond_61

    #@5f
    const/4 v0, 0x4

    #@60
    goto :goto_f

    #@61
    .line 1765
    :cond_61
    if-lt p1, v6, :cond_67

    #@63
    if-ge p1, v7, :cond_67

    #@65
    const/4 v0, 0x3

    #@66
    goto :goto_f

    #@67
    .line 1766
    :cond_67
    if-lt p1, v5, :cond_6d

    #@69
    if-ge p1, v6, :cond_6d

    #@6b
    const/4 v0, 0x2

    #@6c
    goto :goto_f

    #@6d
    .line 1767
    :cond_6d
    if-ge p1, v5, :cond_71

    #@6f
    const/4 v0, 0x1

    #@70
    goto :goto_f

    #@71
    .line 1768
    :cond_71
    const/4 v0, 0x1

    #@72
    goto :goto_f

    #@73
    .line 1771
    :cond_73
    const/16 v5, -0x78

    #@75
    if-le p2, v5, :cond_79

    #@77
    const/4 v1, 0x4

    #@78
    goto :goto_14

    #@79
    .line 1772
    :cond_79
    const/16 v5, -0x8c

    #@7b
    if-le p2, v5, :cond_7f

    #@7d
    const/4 v1, 0x3

    #@7e
    goto :goto_14

    #@7f
    .line 1773
    :cond_7f
    const/16 v5, -0xa0

    #@81
    if-le p2, v5, :cond_85

    #@83
    const/4 v1, 0x2

    #@84
    goto :goto_14

    #@85
    .line 1774
    :cond_85
    const/4 v1, 0x1

    #@86
    goto :goto_14

    #@87
    .line 1783
    .restart local v4       #kk:I
    :cond_87
    sget-object v5, Landroid/telephony/SignalStrength;->lastEcIoValues:[I

    #@89
    array-length v5, v5

    #@8a
    div-int v2, v3, v5

    #@8c
    .line 1784
    if-le v1, v2, :cond_91

    #@8e
    .line 1790
    :goto_8e
    if-ge v0, v1, :cond_93

    #@90
    .line 1791
    :goto_90
    return v0

    #@91
    :cond_91
    move v1, v2

    #@92
    .line 1784
    goto :goto_8e

    #@93
    :cond_93
    move v0, v1

    #@94
    .line 1790
    goto :goto_90
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 1494
    const-string v0, "SignalStrength"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 1495
    return-void
.end method

.method public static newFromBundle(Landroid/os/Bundle;)Landroid/telephony/SignalStrength;
    .registers 2
    .parameter "m"

    #@0
    .prologue
    .line 141
    new-instance v0, Landroid/telephony/SignalStrength;

    #@2
    invoke-direct {v0}, Landroid/telephony/SignalStrength;-><init>()V

    #@5
    .line 142
    .local v0, ret:Landroid/telephony/SignalStrength;
    invoke-direct {v0, p0}, Landroid/telephony/SignalStrength;->setFromNotifierBundle(Landroid/os/Bundle;)V

    #@8
    .line 143
    return-object v0
.end method

.method private setFromNotifierBundle(Landroid/os/Bundle;)V
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 1436
    const-string v0, "GsmSignalStrength"

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@8
    .line 1437
    const-string v0, "GsmBitErrorRate"

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@10
    .line 1438
    const-string v0, "CdmaDbm"

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@18
    .line 1439
    const-string v0, "CdmaEcio"

    #@1a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@1d
    move-result v0

    #@1e
    iput v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@20
    .line 1440
    const-string v0, "EvdoDbm"

    #@22
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@28
    .line 1441
    const-string v0, "EvdoEcio"

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@30
    .line 1442
    const-string v0, "EvdoSnr"

    #@32
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@35
    move-result v0

    #@36
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@38
    .line 1443
    const-string v0, "LteSignalStrength"

    #@3a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@40
    .line 1444
    const-string v0, "LteRsrp"

    #@42
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@45
    move-result v0

    #@46
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@48
    .line 1445
    const-string v0, "LteRsrq"

    #@4a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@4d
    move-result v0

    #@4e
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@50
    .line 1446
    const-string v0, "LteRssnr"

    #@52
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@55
    move-result v0

    #@56
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@58
    .line 1447
    const-string v0, "LteCqi"

    #@5a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@5d
    move-result v0

    #@5e
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@60
    .line 1448
    const-string v0, "TdScdma"

    #@62
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@65
    move-result v0

    #@66
    iput v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@68
    .line 1449
    const-string v0, "isGsm"

    #@6a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@6d
    move-result v0

    #@6e
    iput-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@70
    .line 1451
    const-string/jumbo v0, "mRadioTechnology"

    #@73
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@76
    move-result v0

    #@77
    iput v0, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@79
    .line 1455
    const-string v0, "Dataf"

    #@7b
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@7e
    move-result v0

    #@7f
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@81
    .line 1458
    return-void
.end method


# virtual methods
.method protected copyFrom(Landroid/telephony/SignalStrength;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 324
    iget v0, p1, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@2
    iput v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@4
    .line 325
    iget v0, p1, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@6
    iput v0, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@8
    .line 326
    iget v0, p1, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@a
    iput v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@c
    .line 327
    iget v0, p1, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@e
    iput v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@10
    .line 328
    iget v0, p1, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@12
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@14
    .line 329
    iget v0, p1, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@16
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@18
    .line 330
    iget v0, p1, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@1a
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@1c
    .line 331
    iget v0, p1, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1e
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@20
    .line 332
    iget v0, p1, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@22
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@24
    .line 333
    iget v0, p1, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@26
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@28
    .line 334
    iget v0, p1, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@2a
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@2c
    .line 335
    iget v0, p1, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@2e
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@30
    .line 336
    iget v0, p1, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@32
    iput v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@34
    .line 337
    iget-boolean v0, p1, Landroid/telephony/SignalStrength;->isGsm:Z

    #@36
    iput-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@38
    .line 339
    iget v0, p1, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@3a
    iput v0, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@3c
    .line 343
    iget v0, p1, Landroid/telephony/SignalStrength;->datafeature:I

    #@3e
    iput v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@40
    .line 345
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 411
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1378
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/SignalStrength;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 1383
    .local v2, s:Landroid/telephony/SignalStrength;
    if-nez p1, :cond_a

    #@7
    .line 1387
    .end local v2           #s:Landroid/telephony/SignalStrength;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 1379
    :catch_8
    move-exception v1

    #@9
    .line 1380
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 1387
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/SignalStrength;
    :cond_a
    iget v4, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@c
    iget v5, v2, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@e
    if-ne v4, v5, :cond_7

    #@10
    iget v4, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@12
    iget v5, v2, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@14
    if-ne v4, v5, :cond_7

    #@16
    iget v4, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@18
    iget v5, v2, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@1a
    if-ne v4, v5, :cond_7

    #@1c
    iget v4, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@1e
    iget v5, v2, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@20
    if-ne v4, v5, :cond_7

    #@22
    iget v4, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@24
    iget v5, v2, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@26
    if-ne v4, v5, :cond_7

    #@28
    iget v4, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@2a
    iget v5, v2, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@2c
    if-ne v4, v5, :cond_7

    #@2e
    iget v4, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@30
    iget v5, v2, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@32
    if-ne v4, v5, :cond_7

    #@34
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@36
    iget v5, v2, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@38
    if-ne v4, v5, :cond_7

    #@3a
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@3c
    iget v5, v2, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@3e
    if-ne v4, v5, :cond_7

    #@40
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@42
    iget v5, v2, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@44
    if-ne v4, v5, :cond_7

    #@46
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@48
    iget v5, v2, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@4a
    if-ne v4, v5, :cond_7

    #@4c
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@4e
    iget v5, v2, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@50
    if-ne v4, v5, :cond_7

    #@52
    iget v4, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@54
    iget v5, v2, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@56
    if-ne v4, v5, :cond_7

    #@58
    iget-boolean v4, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@5a
    iget-boolean v5, v2, Landroid/telephony/SignalStrength;->isGsm:Z

    #@5c
    if-ne v4, v5, :cond_7

    #@5e
    const/4 v3, 0x1

    #@5f
    goto :goto_7
.end method

.method public fillInNotifierBundle(Landroid/os/Bundle;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 1467
    const-string v0, "GsmSignalStrength"

    #@2
    iget v1, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@4
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@7
    .line 1468
    const-string v0, "GsmBitErrorRate"

    #@9
    iget v1, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@b
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@e
    .line 1469
    const-string v0, "CdmaDbm"

    #@10
    iget v1, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@12
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@15
    .line 1470
    const-string v0, "CdmaEcio"

    #@17
    iget v1, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@19
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1c
    .line 1471
    const-string v0, "EvdoDbm"

    #@1e
    iget v1, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@23
    .line 1472
    const-string v0, "EvdoEcio"

    #@25
    iget v1, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@27
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2a
    .line 1473
    const-string v0, "EvdoSnr"

    #@2c
    iget v1, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@2e
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@31
    .line 1474
    const-string v0, "LteSignalStrength"

    #@33
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@35
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@38
    .line 1475
    const-string v0, "LteRsrp"

    #@3a
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@3c
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@3f
    .line 1476
    const-string v0, "LteRsrq"

    #@41
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@43
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@46
    .line 1477
    const-string v0, "LteRssnr"

    #@48
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@4a
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@4d
    .line 1478
    const-string v0, "LteCqi"

    #@4f
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@51
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@54
    .line 1479
    const-string v0, "TdScdma"

    #@56
    iget v1, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@58
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@5b
    .line 1480
    const-string v0, "isGsm"

    #@5d
    iget-boolean v1, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@5f
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@66
    move-result v1

    #@67
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@6a
    .line 1482
    const-string/jumbo v0, "mRadioTechnology"

    #@6d
    iget v1, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@6f
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@72
    .line 1486
    const-string v0, "Dataf"

    #@74
    iget v1, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@76
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@79
    .line 1488
    return-void
.end method

.method public getAsuLevel()I
    .registers 6

    #@0
    .prologue
    .line 672
    const/4 v0, 0x0

    #@1
    .line 673
    .local v0, asuLevel:I
    iget-boolean v3, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@3
    if-eqz v3, :cond_36

    #@5
    .line 674
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@8
    move-result v3

    #@9
    if-nez v3, :cond_31

    #@b
    .line 675
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaLevel()I

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_2c

    #@11
    .line 676
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmAsuLevel()I

    #@14
    move-result v0

    #@15
    .line 697
    :goto_15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "getAsuLevel="

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@2b
    .line 698
    return v0

    #@2c
    .line 678
    :cond_2c
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaAsuLevel()I

    #@2f
    move-result v0

    #@30
    goto :goto_15

    #@31
    .line 681
    :cond_31
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteAsuLevel()I

    #@34
    move-result v0

    #@35
    goto :goto_15

    #@36
    .line 684
    :cond_36
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaAsuLevel()I

    #@39
    move-result v1

    #@3a
    .line 685
    .local v1, cdmaAsuLevel:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoAsuLevel()I

    #@3d
    move-result v2

    #@3e
    .line 686
    .local v2, evdoAsuLevel:I
    if-nez v2, :cond_42

    #@40
    .line 688
    move v0, v1

    #@41
    goto :goto_15

    #@42
    .line 689
    :cond_42
    if-nez v1, :cond_46

    #@44
    .line 691
    move v0, v2

    #@45
    goto :goto_15

    #@46
    .line 694
    :cond_46
    if-ge v1, v2, :cond_4a

    #@48
    move v0, v1

    #@49
    :goto_49
    goto :goto_15

    #@4a
    :cond_4a
    move v0, v2

    #@4b
    goto :goto_49
.end method

.method public getCdmaAsuLevel()I
    .registers 9

    #@0
    .prologue
    const/16 v7, -0x5a

    #@2
    const/16 v6, -0x64

    #@4
    .line 989
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    #@7
    move-result v1

    #@8
    .line 990
    .local v1, cdmaDbm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    #@b
    move-result v2

    #@c
    .line 994
    .local v2, cdmaEcio:I
    const/16 v5, -0x4b

    #@e
    if-lt v1, v5, :cond_30

    #@10
    const/16 v0, 0x10

    #@12
    .line 1002
    .local v0, cdmaAsuLevel:I
    :goto_12
    if-lt v2, v7, :cond_48

    #@14
    const/16 v3, 0x10

    #@16
    .line 1009
    .local v3, ecioAsuLevel:I
    :goto_16
    if-ge v0, v3, :cond_62

    #@18
    move v4, v0

    #@19
    .line 1010
    .local v4, level:I
    :goto_19
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v6, "getCdmaAsuLevel="

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {v5}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@2f
    .line 1011
    return v4

    #@30
    .line 995
    .end local v0           #cdmaAsuLevel:I
    .end local v3           #ecioAsuLevel:I
    .end local v4           #level:I
    :cond_30
    const/16 v5, -0x52

    #@32
    if-lt v1, v5, :cond_37

    #@34
    const/16 v0, 0x8

    #@36
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@37
    .line 996
    .end local v0           #cdmaAsuLevel:I
    :cond_37
    if-lt v1, v7, :cond_3b

    #@39
    const/4 v0, 0x4

    #@3a
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@3b
    .line 997
    .end local v0           #cdmaAsuLevel:I
    :cond_3b
    const/16 v5, -0x5f

    #@3d
    if-lt v1, v5, :cond_41

    #@3f
    const/4 v0, 0x2

    #@40
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@41
    .line 998
    .end local v0           #cdmaAsuLevel:I
    :cond_41
    if-lt v1, v6, :cond_45

    #@43
    const/4 v0, 0x1

    #@44
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@45
    .line 999
    .end local v0           #cdmaAsuLevel:I
    :cond_45
    const/16 v0, 0x63

    #@47
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@48
    .line 1003
    :cond_48
    if-lt v2, v6, :cond_4d

    #@4a
    const/16 v3, 0x8

    #@4c
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@4d
    .line 1004
    .end local v3           #ecioAsuLevel:I
    :cond_4d
    const/16 v5, -0x73

    #@4f
    if-lt v2, v5, :cond_53

    #@51
    const/4 v3, 0x4

    #@52
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@53
    .line 1005
    .end local v3           #ecioAsuLevel:I
    :cond_53
    const/16 v5, -0x82

    #@55
    if-lt v2, v5, :cond_59

    #@57
    const/4 v3, 0x2

    #@58
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@59
    .line 1006
    .end local v3           #ecioAsuLevel:I
    :cond_59
    const/16 v5, -0x96

    #@5b
    if-lt v2, v5, :cond_5f

    #@5d
    const/4 v3, 0x1

    #@5e
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@5f
    .line 1007
    .end local v3           #ecioAsuLevel:I
    :cond_5f
    const/16 v3, 0x63

    #@61
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@62
    :cond_62
    move v4, v3

    #@63
    .line 1009
    goto :goto_19
.end method

.method public getCdmaDbm()I
    .registers 2

    #@0
    .prologue
    .line 510
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@2
    return v0
.end method

.method public getCdmaEcio()I
    .registers 2

    #@0
    .prologue
    .line 517
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@2
    return v0
.end method

.method public getCdmaLevel()I
    .registers 11

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    .line 940
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    #@4
    move-result v0

    #@5
    .line 941
    .local v0, cdmaDbm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    #@8
    move-result v1

    #@9
    .line 942
    .local v1, cdmaEcio:I
    const/4 v3, 0x0

    #@a
    .line 943
    .local v3, levelDbm:I
    const/4 v4, 0x0

    #@b
    .line 946
    .local v4, levelEcio:I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@e
    move-result-object v7

    #@f
    invoke-virtual {v7}, Lcom/android/internal/telephony/LgeRssiData;->getCdmaDbmValue()[I

    #@12
    move-result-object v5

    #@13
    .line 947
    .local v5, mDbmValue:[I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@16
    move-result-object v7

    #@17
    invoke-virtual {v7}, Lcom/android/internal/telephony/LgeRssiData;->getEcioValue()[I

    #@1a
    move-result-object v6

    #@1b
    .line 948
    .local v6, mEcioValue:[I
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "getCdmaLevel - mDbmValue :"

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, " mEcioValue :"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-static {v6}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v7

    #@40
    invoke-static {v7}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@43
    .line 951
    if-nez v5, :cond_47

    #@45
    if-eqz v6, :cond_64

    #@47
    .line 952
    :cond_47
    invoke-direct {p0, v0, v5}, Landroid/telephony/SignalStrength;->getItemLevel(I[I)I

    #@4a
    move-result v3

    #@4b
    .line 953
    invoke-direct {p0, v1, v6}, Landroid/telephony/SignalStrength;->getItemLevel(I[I)I

    #@4e
    move-result v4

    #@4f
    .line 955
    if-eq v3, v9, :cond_5a

    #@51
    if-eq v4, v9, :cond_5a

    #@53
    .line 956
    if-ge v3, v4, :cond_58

    #@55
    move v7, v3

    #@56
    :goto_56
    move v2, v7

    #@57
    .line 980
    :goto_57
    return v2

    #@58
    :cond_58
    move v7, v4

    #@59
    .line 956
    goto :goto_56

    #@5a
    .line 957
    :cond_5a
    if-eq v3, v9, :cond_5e

    #@5c
    move v2, v3

    #@5d
    goto :goto_57

    #@5e
    .line 958
    :cond_5e
    if-eq v4, v9, :cond_62

    #@60
    move v2, v4

    #@61
    goto :goto_57

    #@62
    .line 960
    :cond_62
    const/4 v2, 0x0

    #@63
    goto :goto_57

    #@64
    .line 965
    :cond_64
    const/16 v7, -0x4b

    #@66
    if-lt v0, v7, :cond_88

    #@68
    const/4 v3, 0x4

    #@69
    .line 972
    :goto_69
    const/16 v7, -0x5a

    #@6b
    if-lt v1, v7, :cond_9c

    #@6d
    const/4 v4, 0x4

    #@6e
    .line 978
    :goto_6e
    if-ge v3, v4, :cond_b0

    #@70
    move v2, v3

    #@71
    .line 979
    .local v2, level:I
    :goto_71
    new-instance v7, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v8, "getCdmaLevel="

    #@78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v7

    #@84
    invoke-static {v7}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@87
    goto :goto_57

    #@88
    .line 966
    .end local v2           #level:I
    :cond_88
    const/16 v7, -0x55

    #@8a
    if-lt v0, v7, :cond_8e

    #@8c
    const/4 v3, 0x3

    #@8d
    goto :goto_69

    #@8e
    .line 967
    :cond_8e
    const/16 v7, -0x5f

    #@90
    if-lt v0, v7, :cond_94

    #@92
    const/4 v3, 0x2

    #@93
    goto :goto_69

    #@94
    .line 968
    :cond_94
    const/16 v7, -0x64

    #@96
    if-lt v0, v7, :cond_9a

    #@98
    const/4 v3, 0x1

    #@99
    goto :goto_69

    #@9a
    .line 969
    :cond_9a
    const/4 v3, 0x0

    #@9b
    goto :goto_69

    #@9c
    .line 973
    :cond_9c
    const/16 v7, -0x6e

    #@9e
    if-lt v1, v7, :cond_a2

    #@a0
    const/4 v4, 0x3

    #@a1
    goto :goto_6e

    #@a2
    .line 974
    :cond_a2
    const/16 v7, -0x82

    #@a4
    if-lt v1, v7, :cond_a8

    #@a6
    const/4 v4, 0x2

    #@a7
    goto :goto_6e

    #@a8
    .line 975
    :cond_a8
    const/16 v7, -0x96

    #@aa
    if-lt v1, v7, :cond_ae

    #@ac
    const/4 v4, 0x1

    #@ad
    goto :goto_6e

    #@ae
    .line 976
    :cond_ae
    const/4 v4, 0x0

    #@af
    goto :goto_6e

    #@b0
    :cond_b0
    move v2, v4

    #@b1
    .line 978
    goto :goto_71
.end method

.method public getDCMGsmUmtsLevel()I
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x63

    #@2
    .line 875
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@5
    move-result v0

    #@6
    .line 882
    .local v0, asu:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@9
    move-result v2

    #@a
    sparse-switch v2, :sswitch_data_6a

    #@d
    .line 908
    const/4 v2, -0x1

    #@e
    if-lt v0, v2, :cond_12

    #@10
    if-ne v0, v4, :cond_59

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    .line 915
    .local v1, level:I
    :goto_13
    return v1

    #@14
    .line 887
    .end local v1           #level:I
    :sswitch_14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "[jongdae123] GSM asu="

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v2}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@2a
    .line 888
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "[mang] GSM asu="

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v2}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@40
    .line 889
    const/4 v2, 0x3

    #@41
    if-lt v0, v2, :cond_45

    #@43
    if-ne v0, v4, :cond_47

    #@45
    :cond_45
    const/4 v1, 0x0

    #@46
    .restart local v1       #level:I
    goto :goto_13

    #@47
    .line 890
    .end local v1           #level:I
    :cond_47
    const/16 v2, 0xb

    #@49
    if-lt v0, v2, :cond_4d

    #@4b
    const/4 v1, 0x4

    #@4c
    .restart local v1       #level:I
    goto :goto_13

    #@4d
    .line 891
    .end local v1           #level:I
    :cond_4d
    const/4 v2, 0x7

    #@4e
    if-lt v0, v2, :cond_52

    #@50
    const/4 v1, 0x3

    #@51
    .restart local v1       #level:I
    goto :goto_13

    #@52
    .line 892
    .end local v1           #level:I
    :cond_52
    const/4 v2, 0x5

    #@53
    if-lt v0, v2, :cond_57

    #@55
    const/4 v1, 0x2

    #@56
    .restart local v1       #level:I
    goto :goto_13

    #@57
    .line 893
    .end local v1           #level:I
    :cond_57
    const/4 v1, 0x1

    #@58
    .line 894
    .restart local v1       #level:I
    goto :goto_13

    #@59
    .line 909
    .end local v1           #level:I
    :cond_59
    const/4 v2, 0x6

    #@5a
    if-lt v0, v2, :cond_5e

    #@5c
    const/4 v1, 0x4

    #@5d
    .restart local v1       #level:I
    goto :goto_13

    #@5e
    .line 910
    .end local v1           #level:I
    :cond_5e
    const/4 v2, 0x4

    #@5f
    if-lt v0, v2, :cond_63

    #@61
    const/4 v1, 0x3

    #@62
    .restart local v1       #level:I
    goto :goto_13

    #@63
    .line 911
    .end local v1           #level:I
    :cond_63
    const/4 v2, 0x1

    #@64
    if-lt v0, v2, :cond_68

    #@66
    const/4 v1, 0x2

    #@67
    .restart local v1       #level:I
    goto :goto_13

    #@68
    .line 912
    .end local v1           #level:I
    :cond_68
    const/4 v1, 0x1

    #@69
    .restart local v1       #level:I
    goto :goto_13

    #@6a
    .line 882
    :sswitch_data_6a
    .sparse-switch
        0x1 -> :sswitch_14
        0x2 -> :sswitch_14
        0x10 -> :sswitch_14
    .end sparse-switch
.end method

.method public getDbm()I
    .registers 6

    #@0
    .prologue
    const/16 v4, -0x78

    #@2
    .line 707
    const v1, 0x7fffffff

    #@5
    .line 709
    .local v1, dBm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->isGsm()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_3d

    #@b
    .line 710
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_38

    #@11
    .line 711
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaLevel()I

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_33

    #@17
    .line 712
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmDbm()I

    #@1a
    move-result v1

    #@1b
    .line 726
    :goto_1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "getDbm="

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@31
    move v0, v1

    #@32
    .line 727
    :cond_32
    :goto_32
    return v0

    #@33
    .line 714
    :cond_33
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaDbm()I

    #@36
    move-result v1

    #@37
    goto :goto_1b

    #@38
    .line 717
    :cond_38
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteDbm()I

    #@3b
    move-result v1

    #@3c
    goto :goto_1b

    #@3d
    .line 720
    :cond_3d
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    #@40
    move-result v0

    #@41
    .line 721
    .local v0, cdmaDbm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    #@44
    move-result v2

    #@45
    .line 723
    .local v2, evdoDbm:I
    if-eq v2, v4, :cond_32

    #@47
    if-ne v0, v4, :cond_4b

    #@49
    move v0, v2

    #@4a
    goto :goto_32

    #@4b
    :cond_4b
    if-lt v0, v2, :cond_32

    #@4d
    move v0, v2

    #@4e
    goto :goto_32
.end method

.method public getEvdoAsuLevel()I
    .registers 8

    #@0
    .prologue
    .line 1068
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    #@3
    move-result v0

    #@4
    .line 1069
    .local v0, evdoDbm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoSnr()I

    #@7
    move-result v1

    #@8
    .line 1073
    .local v1, evdoSnr:I
    const/16 v5, -0x41

    #@a
    if-lt v0, v5, :cond_2d

    #@c
    const/16 v3, 0x10

    #@e
    .line 1080
    .local v3, levelEvdoDbm:I
    :goto_e
    const/4 v5, 0x7

    #@f
    if-lt v1, v5, :cond_49

    #@11
    const/16 v4, 0x10

    #@13
    .line 1087
    .local v4, levelEvdoSnr:I
    :goto_13
    if-ge v3, v4, :cond_61

    #@15
    move v2, v3

    #@16
    .line 1088
    .local v2, level:I
    :goto_16
    new-instance v5, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v6, "getEvdoAsuLevel="

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-static {v5}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@2c
    .line 1089
    return v2

    #@2d
    .line 1074
    .end local v2           #level:I
    .end local v3           #levelEvdoDbm:I
    .end local v4           #levelEvdoSnr:I
    :cond_2d
    const/16 v5, -0x4b

    #@2f
    if-lt v0, v5, :cond_34

    #@31
    const/16 v3, 0x8

    #@33
    .restart local v3       #levelEvdoDbm:I
    goto :goto_e

    #@34
    .line 1075
    .end local v3           #levelEvdoDbm:I
    :cond_34
    const/16 v5, -0x55

    #@36
    if-lt v0, v5, :cond_3a

    #@38
    const/4 v3, 0x4

    #@39
    .restart local v3       #levelEvdoDbm:I
    goto :goto_e

    #@3a
    .line 1076
    .end local v3           #levelEvdoDbm:I
    :cond_3a
    const/16 v5, -0x5f

    #@3c
    if-lt v0, v5, :cond_40

    #@3e
    const/4 v3, 0x2

    #@3f
    .restart local v3       #levelEvdoDbm:I
    goto :goto_e

    #@40
    .line 1077
    .end local v3           #levelEvdoDbm:I
    :cond_40
    const/16 v5, -0x69

    #@42
    if-lt v0, v5, :cond_46

    #@44
    const/4 v3, 0x1

    #@45
    .restart local v3       #levelEvdoDbm:I
    goto :goto_e

    #@46
    .line 1078
    .end local v3           #levelEvdoDbm:I
    :cond_46
    const/16 v3, 0x63

    #@48
    .restart local v3       #levelEvdoDbm:I
    goto :goto_e

    #@49
    .line 1081
    :cond_49
    const/4 v5, 0x6

    #@4a
    if-lt v1, v5, :cond_4f

    #@4c
    const/16 v4, 0x8

    #@4e
    .restart local v4       #levelEvdoSnr:I
    goto :goto_13

    #@4f
    .line 1082
    .end local v4           #levelEvdoSnr:I
    :cond_4f
    const/4 v5, 0x5

    #@50
    if-lt v1, v5, :cond_54

    #@52
    const/4 v4, 0x4

    #@53
    .restart local v4       #levelEvdoSnr:I
    goto :goto_13

    #@54
    .line 1083
    .end local v4           #levelEvdoSnr:I
    :cond_54
    const/4 v5, 0x3

    #@55
    if-lt v1, v5, :cond_59

    #@57
    const/4 v4, 0x2

    #@58
    .restart local v4       #levelEvdoSnr:I
    goto :goto_13

    #@59
    .line 1084
    .end local v4           #levelEvdoSnr:I
    :cond_59
    const/4 v5, 0x1

    #@5a
    if-lt v1, v5, :cond_5e

    #@5c
    const/4 v4, 0x1

    #@5d
    .restart local v4       #levelEvdoSnr:I
    goto :goto_13

    #@5e
    .line 1085
    .end local v4           #levelEvdoSnr:I
    :cond_5e
    const/16 v4, 0x63

    #@60
    .restart local v4       #levelEvdoSnr:I
    goto :goto_13

    #@61
    :cond_61
    move v2, v4

    #@62
    .line 1087
    goto :goto_16
.end method

.method public getEvdoDbm()I
    .registers 2

    #@0
    .prologue
    .line 524
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@2
    return v0
.end method

.method public getEvdoEcio()I
    .registers 2

    #@0
    .prologue
    .line 531
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@2
    return v0
.end method

.method public getEvdoLevel()I
    .registers 11

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    .line 1020
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    #@4
    move-result v0

    #@5
    .line 1021
    .local v0, evdoDbm:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoSnr()I

    #@8
    move-result v1

    #@9
    .line 1022
    .local v1, evdoSnr:I
    const/4 v3, 0x0

    #@a
    .line 1023
    .local v3, levelEvdoDbm:I
    const/4 v4, 0x0

    #@b
    .line 1026
    .local v4, levelEvdoSnr:I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@e
    move-result-object v7

    #@f
    invoke-virtual {v7}, Lcom/android/internal/telephony/LgeRssiData;->getEvdoDbmValue()[I

    #@12
    move-result-object v5

    #@13
    .line 1027
    .local v5, mEvdoDbmValue:[I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@16
    move-result-object v7

    #@17
    invoke-virtual {v7}, Lcom/android/internal/telephony/LgeRssiData;->getEvdoSnrValue()[I

    #@1a
    move-result-object v6

    #@1b
    .line 1028
    .local v6, mEvdoSnrValue:[I
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "getEvdoLevel - mEvdoDbmValue : "

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, " mEvdoSnrValue :"

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-static {v6}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v7

    #@40
    invoke-static {v7}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@43
    .line 1031
    if-nez v5, :cond_47

    #@45
    if-eqz v6, :cond_64

    #@47
    .line 1032
    :cond_47
    invoke-direct {p0, v0, v5}, Landroid/telephony/SignalStrength;->getItemLevel(I[I)I

    #@4a
    move-result v3

    #@4b
    .line 1033
    invoke-direct {p0, v1, v6}, Landroid/telephony/SignalStrength;->getItemLevel(I[I)I

    #@4e
    move-result v4

    #@4f
    .line 1035
    if-eq v3, v9, :cond_5a

    #@51
    if-eq v4, v9, :cond_5a

    #@53
    .line 1036
    if-ge v3, v4, :cond_58

    #@55
    move v7, v3

    #@56
    :goto_56
    move v2, v7

    #@57
    .line 1059
    :goto_57
    return v2

    #@58
    :cond_58
    move v7, v4

    #@59
    .line 1036
    goto :goto_56

    #@5a
    .line 1037
    :cond_5a
    if-eq v3, v9, :cond_5e

    #@5c
    move v2, v3

    #@5d
    goto :goto_57

    #@5e
    .line 1038
    :cond_5e
    if-eq v4, v9, :cond_62

    #@60
    move v2, v4

    #@61
    goto :goto_57

    #@62
    .line 1040
    :cond_62
    const/4 v2, 0x0

    #@63
    goto :goto_57

    #@64
    .line 1045
    :cond_64
    const/16 v7, -0x41

    #@66
    if-lt v0, v7, :cond_87

    #@68
    const/4 v3, 0x4

    #@69
    .line 1051
    :goto_69
    const/4 v7, 0x7

    #@6a
    if-lt v1, v7, :cond_9b

    #@6c
    const/4 v4, 0x4

    #@6d
    .line 1057
    :goto_6d
    if-ge v3, v4, :cond_ac

    #@6f
    move v2, v3

    #@70
    .line 1058
    .local v2, level:I
    :goto_70
    new-instance v7, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v8, "getEvdoLevel="

    #@77
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v7

    #@7b
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v7

    #@7f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v7

    #@83
    invoke-static {v7}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@86
    goto :goto_57

    #@87
    .line 1046
    .end local v2           #level:I
    :cond_87
    const/16 v7, -0x4b

    #@89
    if-lt v0, v7, :cond_8d

    #@8b
    const/4 v3, 0x3

    #@8c
    goto :goto_69

    #@8d
    .line 1047
    :cond_8d
    const/16 v7, -0x5a

    #@8f
    if-lt v0, v7, :cond_93

    #@91
    const/4 v3, 0x2

    #@92
    goto :goto_69

    #@93
    .line 1048
    :cond_93
    const/16 v7, -0x69

    #@95
    if-lt v0, v7, :cond_99

    #@97
    const/4 v3, 0x1

    #@98
    goto :goto_69

    #@99
    .line 1049
    :cond_99
    const/4 v3, 0x0

    #@9a
    goto :goto_69

    #@9b
    .line 1052
    :cond_9b
    const/4 v7, 0x5

    #@9c
    if-lt v1, v7, :cond_a0

    #@9e
    const/4 v4, 0x3

    #@9f
    goto :goto_6d

    #@a0
    .line 1053
    :cond_a0
    const/4 v7, 0x3

    #@a1
    if-lt v1, v7, :cond_a5

    #@a3
    const/4 v4, 0x2

    #@a4
    goto :goto_6d

    #@a5
    .line 1054
    :cond_a5
    const/4 v7, 0x1

    #@a6
    if-lt v1, v7, :cond_aa

    #@a8
    const/4 v4, 0x1

    #@a9
    goto :goto_6d

    #@aa
    .line 1055
    :cond_aa
    const/4 v4, 0x0

    #@ab
    goto :goto_6d

    #@ac
    :cond_ac
    move v2, v4

    #@ad
    .line 1057
    goto :goto_70
.end method

.method public getEvdoSnr()I
    .registers 2

    #@0
    .prologue
    .line 538
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@2
    return v0
.end method

.method public getGsmAsuLevel()I
    .registers 4

    #@0
    .prologue
    .line 929
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@3
    move-result v0

    #@4
    .line 930
    .local v0, level:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "getGsmAsuLevel="

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@1a
    .line 931
    return v0
.end method

.method public getGsmBitErrorRate()I
    .registers 2

    #@0
    .prologue
    .line 503
    iget v0, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@2
    return v0
.end method

.method public getGsmDbm()I
    .registers 6

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 738
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@4
    move-result v2

    #@5
    .line 739
    .local v2, gsmSignalStrength:I
    const/16 v4, 0x63

    #@7
    if-ne v2, v4, :cond_27

    #@9
    move v0, v3

    #@a
    .line 740
    .local v0, asu:I
    :goto_a
    if-eq v0, v3, :cond_29

    #@c
    .line 741
    mul-int/lit8 v3, v0, 0x2

    #@e
    add-int/lit8 v1, v3, -0x71

    #@10
    .line 745
    .local v1, dBm:I
    :goto_10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "getGsmDbm="

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v3}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@26
    .line 746
    return v1

    #@27
    .end local v0           #asu:I
    .end local v1           #dBm:I
    :cond_27
    move v0, v2

    #@28
    .line 739
    goto :goto_a

    #@29
    .line 743
    .restart local v0       #asu:I
    :cond_29
    const/4 v1, -0x1

    #@2a
    .restart local v1       #dBm:I
    goto :goto_10
.end method

.method public getGsmLevel()I
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v9, -0x1

    #@2
    const/16 v8, 0x63

    #@4
    .line 755
    const/4 v1, 0x0

    #@5
    .line 759
    .local v1, level:I
    const-string v6, "DCM"

    #@7
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_12

    #@d
    .line 760
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getDCMGsmUmtsLevel()I

    #@10
    move-result v5

    #@11
    .line 866
    :cond_11
    :goto_11
    return v5

    #@12
    .line 765
    :cond_12
    const-string v6, "ATT"

    #@14
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_1f

    #@1a
    .line 766
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getATTGsmUmtsLevel()I

    #@1d
    move-result v5

    #@1e
    goto :goto_11

    #@1f
    .line 774
    :cond_1f
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@22
    move-result v0

    #@23
    .line 777
    .local v0, asu:I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6}, Lcom/android/internal/telephony/LgeRssiData;->getAsuGsmValue()[I

    #@2a
    move-result-object v3

    #@2b
    .line 778
    .local v3, mAsuGsmValue:[I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6}, Lcom/android/internal/telephony/LgeRssiData;->getAsuUmtsValue()[I

    #@32
    move-result-object v4

    #@33
    .line 779
    .local v4, mAsuUmtsValue:[I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Lcom/android/internal/telephony/LgeRssiData;->getAsuEtcValue()[I

    #@3a
    move-result-object v2

    #@3b
    .line 780
    .local v2, mAsuEtcValue:[I
    new-instance v6, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v7, "getGsmLevel - mAsuGsmValue : "

    #@42
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@49
    move-result-object v7

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    const-string v7, " mAsuUmtsValue :"

    #@50
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    invoke-static {v4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@63
    .line 783
    if-nez v3, :cond_a9

    #@65
    if-nez v4, :cond_a9

    #@67
    sget v6, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@69
    const/4 v7, 0x6

    #@6a
    if-ne v6, v7, :cond_a9

    #@6c
    .line 785
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@6f
    move-result v6

    #@70
    sparse-switch v6, :sswitch_data_15e

    #@73
    .line 798
    iget-object v6, p0, Landroid/telephony/SignalStrength;->LGE_UMTS_ASU_VALUE:[I

    #@75
    invoke-direct {p0, v0, v8, v6}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@78
    move-result v1

    #@79
    .line 802
    :goto_79
    new-instance v6, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v7, "LGE Spec: getGsmLevel getRadioTechnology()="

    #@80
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@87
    move-result v7

    #@88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v6

    #@8c
    const-string v7, ", level="

    #@8e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@95
    move-result-object v6

    #@96
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v6

    #@9a
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@9d
    .line 804
    if-eq v1, v9, :cond_11

    #@9f
    move v5, v1

    #@a0
    goto/16 :goto_11

    #@a2
    .line 790
    :sswitch_a2
    iget-object v6, p0, Landroid/telephony/SignalStrength;->LGE_GSM_ASU_VALUE:[I

    #@a4
    invoke-direct {p0, v0, v8, v6}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@a7
    move-result v1

    #@a8
    .line 791
    goto :goto_79

    #@a9
    .line 805
    :cond_a9
    if-nez v3, :cond_ad

    #@ab
    if-eqz v4, :cond_12c

    #@ad
    .line 806
    :cond_ad
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@b0
    move-result v6

    #@b1
    packed-switch v6, :pswitch_data_170

    #@b4
    .line 833
    :cond_b4
    :pswitch_b4
    const-string v6, "KR"

    #@b6
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@b9
    move-result v6

    #@ba
    if-eqz v6, :cond_c2

    #@bc
    .line 834
    if-eqz v3, :cond_c2

    #@be
    .line 835
    invoke-direct {p0, v0, v8, v3}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@c1
    move-result v1

    #@c2
    .line 839
    :cond_c2
    const-string v6, "VZW"

    #@c4
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@c7
    move-result v6

    #@c8
    if-eqz v6, :cond_ce

    #@ca
    .line 840
    invoke-direct {p0, v0, v8, v4}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@cd
    move-result v1

    #@ce
    .line 844
    :cond_ce
    const-string v6, "US"

    #@d0
    const-string v7, "TMO"

    #@d2
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@d5
    move-result v6

    #@d6
    if-eqz v6, :cond_de

    #@d8
    .line 845
    if-eqz v4, :cond_de

    #@da
    .line 846
    invoke-direct {p0, v0, v8, v4}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@dd
    move-result v1

    #@de
    .line 854
    :cond_de
    :goto_de
    new-instance v6, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v7, "Operator Spec: getGsmLevel getRadioTechnology()="

    #@e5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v6

    #@e9
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getRadioTechnology()I

    #@ec
    move-result v7

    #@ed
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v6

    #@f1
    const-string v7, ", asu="

    #@f3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v6

    #@f7
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v6

    #@fb
    const-string v7, ", level="

    #@fd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@104
    move-result-object v6

    #@105
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v6

    #@109
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@10c
    .line 856
    if-eq v1, v9, :cond_11

    #@10e
    move v5, v1

    #@10f
    goto/16 :goto_11

    #@111
    .line 810
    :pswitch_111
    invoke-direct {p0, v0, v8, v3}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@114
    move-result v1

    #@115
    .line 811
    goto :goto_de

    #@116
    .line 818
    :pswitch_116
    invoke-direct {p0, v0, v8, v4}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@119
    move-result v1

    #@11a
    .line 819
    goto :goto_de

    #@11b
    .line 823
    :pswitch_11b
    const-string v6, "US"

    #@11d
    const-string v7, "TMO"

    #@11f
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@122
    move-result v6

    #@123
    if-eqz v6, :cond_b4

    #@125
    .line 824
    if-eqz v3, :cond_b4

    #@127
    .line 825
    invoke-direct {p0, v0, v8, v3}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@12a
    move-result v1

    #@12b
    .line 826
    goto :goto_de

    #@12c
    .line 860
    :cond_12c
    const/4 v5, 0x2

    #@12d
    if-le v0, v5, :cond_131

    #@12f
    if-ne v0, v8, :cond_14b

    #@131
    :cond_131
    const/4 v1, 0x0

    #@132
    .line 865
    :goto_132
    new-instance v5, Ljava/lang/StringBuilder;

    #@134
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@137
    const-string v6, "getGsmLevel="

    #@139
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v5

    #@13d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@140
    move-result-object v5

    #@141
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v5

    #@145
    invoke-static {v5}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@148
    move v5, v1

    #@149
    .line 866
    goto/16 :goto_11

    #@14b
    .line 861
    :cond_14b
    const/16 v5, 0xc

    #@14d
    if-lt v0, v5, :cond_151

    #@14f
    const/4 v1, 0x4

    #@150
    goto :goto_132

    #@151
    .line 862
    :cond_151
    const/16 v5, 0x8

    #@153
    if-lt v0, v5, :cond_157

    #@155
    const/4 v1, 0x3

    #@156
    goto :goto_132

    #@157
    .line 863
    :cond_157
    const/4 v5, 0x5

    #@158
    if-lt v0, v5, :cond_15c

    #@15a
    const/4 v1, 0x2

    #@15b
    goto :goto_132

    #@15c
    .line 864
    :cond_15c
    const/4 v1, 0x1

    #@15d
    goto :goto_132

    #@15e
    .line 785
    :sswitch_data_15e
    .sparse-switch
        0x0 -> :sswitch_a2
        0x1 -> :sswitch_a2
        0x2 -> :sswitch_a2
        0x10 -> :sswitch_a2
    .end sparse-switch

    #@170
    .line 806
    :pswitch_data_170
    .packed-switch 0x1
        :pswitch_111
        :pswitch_111
        :pswitch_116
        :pswitch_b4
        :pswitch_b4
        :pswitch_b4
        :pswitch_b4
        :pswitch_b4
        :pswitch_116
        :pswitch_116
        :pswitch_116
        :pswitch_b4
        :pswitch_b4
        :pswitch_b4
        :pswitch_116
        :pswitch_11b
    .end packed-switch
.end method

.method public getGsmSignalStrength()I
    .registers 2

    #@0
    .prologue
    .line 496
    iget v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@2
    return v0
.end method

.method public getLevel()I
    .registers 7

    #@0
    .prologue
    .line 588
    const/4 v2, 0x0

    #@1
    .line 591
    .local v2, level:I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getOperatorSpecLevel()I

    #@4
    move-result v2

    #@5
    const v4, 0xffff

    #@8
    if-eq v2, v4, :cond_28

    #@a
    .line 592
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "getLevel="

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, " by getOperatorSpecLevel"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v4}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@26
    move v3, v2

    #@27
    .line 633
    .end local v2           #level:I
    .local v3, level:I
    :goto_27
    return v3

    #@28
    .line 595
    .end local v3           #level:I
    .restart local v2       #level:I
    :cond_28
    const/4 v2, 0x0

    #@29
    .line 598
    iget-boolean v4, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@2b
    if-eqz v4, :cond_6f

    #@2d
    .line 600
    const-string v4, "DCM"

    #@2f
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@32
    move-result v4

    #@33
    if-eqz v4, :cond_5f

    #@35
    .line 601
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel_dcm()I

    #@38
    move-result v2

    #@39
    .line 606
    :goto_39
    if-nez v2, :cond_47

    #@3b
    .line 608
    const-string v4, "DCM"

    #@3d
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_64

    #@43
    .line 609
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getDCMGsmUmtsLevel()I

    #@46
    move-result v2

    #@47
    .line 632
    :cond_47
    :goto_47
    new-instance v4, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v5, "getLevel="

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v4}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@5d
    move v3, v2

    #@5e
    .line 633
    .end local v2           #level:I
    .restart local v3       #level:I
    goto :goto_27

    #@5f
    .line 603
    .end local v3           #level:I
    .restart local v2       #level:I
    :cond_5f
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteLevel()I

    #@62
    move-result v2

    #@63
    goto :goto_39

    #@64
    .line 611
    :cond_64
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaLevel()I

    #@67
    move-result v2

    #@68
    .line 612
    if-nez v2, :cond_47

    #@6a
    .line 613
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmLevel()I

    #@6d
    move-result v2

    #@6e
    goto :goto_47

    #@6f
    .line 619
    :cond_6f
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaLevel()I

    #@72
    move-result v0

    #@73
    .line 620
    .local v0, cdmaLevel:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoLevel()I

    #@76
    move-result v1

    #@77
    .line 621
    .local v1, evdoLevel:I
    if-nez v1, :cond_7b

    #@79
    .line 623
    move v2, v0

    #@7a
    goto :goto_47

    #@7b
    .line 624
    :cond_7b
    if-nez v0, :cond_7f

    #@7d
    .line 626
    move v2, v1

    #@7e
    goto :goto_47

    #@7f
    .line 629
    :cond_7f
    if-ge v0, v1, :cond_83

    #@81
    move v2, v0

    #@82
    :goto_82
    goto :goto_47

    #@83
    :cond_83
    move v2, v1

    #@84
    goto :goto_82
.end method

.method public getLteAsuLevel()I
    .registers 5

    #@0
    .prologue
    .line 1279
    const/16 v0, 0x63

    #@2
    .line 1280
    .local v0, lteAsuLevel:I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getLteDbm()I

    #@5
    move-result v1

    #@6
    .line 1294
    .local v1, lteDbm:I
    const v2, 0x7fffffff

    #@9
    if-ne v1, v2, :cond_24

    #@b
    const/16 v0, 0xff

    #@d
    .line 1296
    :goto_d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Lte Asu level: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v2}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@23
    .line 1297
    return v0

    #@24
    .line 1295
    :cond_24
    add-int/lit16 v0, v1, 0x8c

    #@26
    goto :goto_d
.end method

.method public getLteCqi()I
    .registers 2

    #@0
    .prologue
    .line 568
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@2
    return v0
.end method

.method public getLteDbm()I
    .registers 2

    #@0
    .prologue
    .line 1098
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@2
    return v0
.end method

.method public getLteLevel()I
    .registers 13

    #@0
    .prologue
    const/16 v11, 0x12c

    #@2
    const/16 v10, 0x3f

    #@4
    const/16 v9, -0x2c

    #@6
    const/4 v8, -0x1

    #@7
    .line 1113
    const/4 v4, 0x0

    #@8
    .local v4, rssiIconLevel:I
    const/4 v3, -0x1

    #@9
    .local v3, rsrpIconLevel:I
    const/4 v5, -0x1

    #@a
    .line 1116
    .local v5, snrIconLevel:I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@d
    move-result-object v6

    #@e
    invoke-virtual {v6}, Lcom/android/internal/telephony/LgeRssiData;->getRsrpValue()[I

    #@11
    move-result-object v0

    #@12
    .line 1117
    .local v0, mLteRsrpValue:[I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Lcom/android/internal/telephony/LgeRssiData;->getRssnrValue()[I

    #@19
    move-result-object v1

    #@1a
    .line 1118
    .local v1, mLteRssnrValue:[I
    invoke-direct {p0}, Landroid/telephony/SignalStrength;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6}, Lcom/android/internal/telephony/LgeRssiData;->getLteSignalValue()[I

    #@21
    move-result-object v2

    #@22
    .line 1120
    .local v2, mLteSignalValue:[I
    new-instance v6, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v7, "getLteLevel - mLteRsrp : "

    #@29
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, " mLteRssnr :"

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    const-string v7, " mLteSignalStrength : "

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@58
    .line 1124
    if-nez v0, :cond_6f

    #@5a
    if-nez v1, :cond_6f

    #@5c
    if-nez v2, :cond_6f

    #@5e
    sget v6, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@60
    const/4 v7, 0x6

    #@61
    if-ne v6, v7, :cond_6f

    #@63
    .line 1125
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@65
    const v7, 0x7fffffff

    #@68
    iget-object v8, p0, Landroid/telephony/SignalStrength;->LGE_LTE_RSRP_VALUE:[I

    #@6a
    invoke-direct {p0, v6, v7, v8}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@6d
    move-result v6

    #@6e
    .line 1196
    :goto_6e
    return v6

    #@6f
    .line 1126
    :cond_6f
    if-nez v0, :cond_75

    #@71
    if-nez v1, :cond_75

    #@73
    if-eqz v2, :cond_fb

    #@75
    .line 1127
    :cond_75
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@77
    invoke-direct {p0, v6, v9, v0}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@7a
    move-result v3

    #@7b
    .line 1128
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@7d
    invoke-direct {p0, v6, v11, v1}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@80
    move-result v5

    #@81
    .line 1130
    new-instance v6, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v7, "getLTELevel - rsrp:"

    #@88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v6

    #@8c
    iget v7, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@8e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    const-string v7, " snr:"

    #@94
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v6

    #@98
    iget v7, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@9a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v6

    #@9e
    const-string v7, " rsrpIconLevel:"

    #@a0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v6

    #@a4
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v6

    #@a8
    const-string v7, " snrIconLevel:"

    #@aa
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v6

    #@b2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v6

    #@b6
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@b9
    .line 1133
    if-eq v5, v8, :cond_c3

    #@bb
    if-eq v3, v8, :cond_c3

    #@bd
    .line 1134
    if-ge v3, v5, :cond_c1

    #@bf
    move v6, v3

    #@c0
    goto :goto_6e

    #@c1
    :cond_c1
    move v6, v5

    #@c2
    goto :goto_6e

    #@c3
    .line 1137
    :cond_c3
    if-eq v5, v8, :cond_c7

    #@c5
    move v6, v5

    #@c6
    goto :goto_6e

    #@c7
    .line 1139
    :cond_c7
    if-eq v3, v8, :cond_cb

    #@c9
    move v6, v3

    #@ca
    goto :goto_6e

    #@cb
    .line 1141
    :cond_cb
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@cd
    invoke-direct {p0, v6, v10, v2}, Landroid/telephony/SignalStrength;->getItemLevel(II[I)I

    #@d0
    move-result v4

    #@d1
    .line 1142
    new-instance v6, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v7, "getLTELevel - rssi:"

    #@d8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v6

    #@dc
    iget v7, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@de
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v6

    #@e2
    const-string v7, " rssiIconLevel:"

    #@e4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v6

    #@e8
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v6

    #@ec
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v6

    #@f0
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@f3
    .line 1145
    if-ne v4, v8, :cond_f8

    #@f5
    const/4 v6, 0x0

    #@f6
    goto/16 :goto_6e

    #@f8
    :cond_f8
    move v6, v4

    #@f9
    goto/16 :goto_6e

    #@fb
    .line 1150
    :cond_fb
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@fd
    if-le v6, v9, :cond_146

    #@ff
    const/4 v3, -0x1

    #@100
    .line 1163
    :cond_100
    :goto_100
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@102
    if-le v6, v11, :cond_16e

    #@104
    const/4 v5, -0x1

    #@105
    .line 1171
    :cond_105
    :goto_105
    new-instance v6, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v7, "getLTELevel - rsrp:"

    #@10c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v6

    #@110
    iget v7, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@112
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@115
    move-result-object v6

    #@116
    const-string v7, " snr:"

    #@118
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v6

    #@11c
    iget v7, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@11e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@121
    move-result-object v6

    #@122
    const-string v7, " rsrpIconLevel:"

    #@124
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v6

    #@128
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v6

    #@12c
    const-string v7, " snrIconLevel:"

    #@12e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v6

    #@132
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v6

    #@136
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v6

    #@13a
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@13d
    .line 1175
    if-eq v5, v8, :cond_19b

    #@13f
    if-eq v3, v8, :cond_19b

    #@141
    .line 1181
    if-ge v3, v5, :cond_198

    #@143
    move v6, v3

    #@144
    goto/16 :goto_6e

    #@146
    .line 1151
    :cond_146
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@148
    const/16 v7, -0x55

    #@14a
    if-lt v6, v7, :cond_14e

    #@14c
    const/4 v3, 0x4

    #@14d
    goto :goto_100

    #@14e
    .line 1152
    :cond_14e
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@150
    const/16 v7, -0x5f

    #@152
    if-lt v6, v7, :cond_156

    #@154
    const/4 v3, 0x3

    #@155
    goto :goto_100

    #@156
    .line 1153
    :cond_156
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@158
    const/16 v7, -0x69

    #@15a
    if-lt v6, v7, :cond_15e

    #@15c
    const/4 v3, 0x2

    #@15d
    goto :goto_100

    #@15e
    .line 1154
    :cond_15e
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@160
    const/16 v7, -0x73

    #@162
    if-lt v6, v7, :cond_166

    #@164
    const/4 v3, 0x1

    #@165
    goto :goto_100

    #@166
    .line 1155
    :cond_166
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@168
    const/16 v7, -0x8c

    #@16a
    if-lt v6, v7, :cond_100

    #@16c
    const/4 v3, 0x0

    #@16d
    goto :goto_100

    #@16e
    .line 1164
    :cond_16e
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@170
    const/16 v7, 0x82

    #@172
    if-lt v6, v7, :cond_176

    #@174
    const/4 v5, 0x4

    #@175
    goto :goto_105

    #@176
    .line 1165
    :cond_176
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@178
    const/16 v7, 0x2d

    #@17a
    if-lt v6, v7, :cond_17e

    #@17c
    const/4 v5, 0x3

    #@17d
    goto :goto_105

    #@17e
    .line 1166
    :cond_17e
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@180
    const/16 v7, 0xa

    #@182
    if-lt v6, v7, :cond_186

    #@184
    const/4 v5, 0x2

    #@185
    goto :goto_105

    #@186
    .line 1167
    :cond_186
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@188
    const/16 v7, -0x1e

    #@18a
    if-lt v6, v7, :cond_18f

    #@18c
    const/4 v5, 0x1

    #@18d
    goto/16 :goto_105

    #@18f
    .line 1168
    :cond_18f
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@191
    const/16 v7, -0xc8

    #@193
    if-lt v6, v7, :cond_105

    #@195
    .line 1169
    const/4 v5, 0x0

    #@196
    goto/16 :goto_105

    #@198
    :cond_198
    move v6, v5

    #@199
    .line 1181
    goto/16 :goto_6e

    #@19b
    .line 1184
    :cond_19b
    if-eq v5, v8, :cond_1a0

    #@19d
    move v6, v5

    #@19e
    goto/16 :goto_6e

    #@1a0
    .line 1186
    :cond_1a0
    if-eq v3, v8, :cond_1a5

    #@1a2
    move v6, v3

    #@1a3
    goto/16 :goto_6e

    #@1a5
    .line 1189
    :cond_1a5
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1a7
    if-le v6, v10, :cond_1cf

    #@1a9
    const/4 v4, 0x0

    #@1aa
    .line 1194
    :cond_1aa
    :goto_1aa
    new-instance v6, Ljava/lang/StringBuilder;

    #@1ac
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1af
    const-string v7, "getLTELevel - rssi:"

    #@1b1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v6

    #@1b5
    iget v7, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1b7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v6

    #@1bb
    const-string v7, " rssiIconLevel:"

    #@1bd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v6

    #@1c1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v6

    #@1c5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c8
    move-result-object v6

    #@1c9
    invoke-static {v6}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@1cc
    move v6, v4

    #@1cd
    .line 1196
    goto/16 :goto_6e

    #@1cf
    .line 1190
    :cond_1cf
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1d1
    const/16 v7, 0xc

    #@1d3
    if-lt v6, v7, :cond_1d7

    #@1d5
    const/4 v4, 0x4

    #@1d6
    goto :goto_1aa

    #@1d7
    .line 1191
    :cond_1d7
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1d9
    const/16 v7, 0x8

    #@1db
    if-lt v6, v7, :cond_1df

    #@1dd
    const/4 v4, 0x3

    #@1de
    goto :goto_1aa

    #@1df
    .line 1192
    :cond_1df
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1e1
    const/4 v7, 0x5

    #@1e2
    if-lt v6, v7, :cond_1e6

    #@1e4
    const/4 v4, 0x2

    #@1e5
    goto :goto_1aa

    #@1e6
    .line 1193
    :cond_1e6
    iget v6, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1e8
    if-ltz v6, :cond_1aa

    #@1ea
    const/4 v4, 0x1

    #@1eb
    goto :goto_1aa
.end method

.method public getLteLevel_dcm()I
    .registers 9

    #@0
    .prologue
    const v7, 0x7fffffff

    #@3
    const/4 v6, -0x1

    #@4
    .line 1208
    const/4 v2, 0x0

    #@5
    .local v2, rssiIconLevel:I
    const/4 v0, -0x1

    #@6
    .local v0, rsrpIconLevel:I
    const/4 v3, -0x1

    #@7
    .line 1211
    .local v3, snrIconLevel:I
    const/4 v1, -0x1

    #@8
    .line 1222
    .local v1, rsrqIconLevel:I
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@a
    if-ne v4, v7, :cond_51

    #@c
    const/4 v0, 0x0

    #@d
    .line 1236
    :goto_d
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@f
    if-ne v4, v7, :cond_7b

    #@11
    const/4 v1, 0x0

    #@12
    .line 1243
    :goto_12
    new-instance v4, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "getLTELevel - rsrp:"

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    iget v5, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, " rsrq:"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    iget v5, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, " rsrpIconLevel:"

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " rsrqIconLevel:"

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v4}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@4a
    .line 1247
    if-eq v1, v6, :cond_a1

    #@4c
    if-eq v0, v6, :cond_a1

    #@4e
    .line 1253
    if-ge v0, v1, :cond_9f

    #@50
    .line 1268
    .end local v0           #rsrpIconLevel:I
    :cond_50
    :goto_50
    return v0

    #@51
    .line 1223
    .restart local v0       #rsrpIconLevel:I
    :cond_51
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@53
    const/16 v5, -0x2c

    #@55
    if-le v4, v5, :cond_59

    #@57
    const/4 v0, -0x1

    #@58
    goto :goto_d

    #@59
    .line 1224
    :cond_59
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@5b
    const/16 v5, -0x71

    #@5d
    if-lt v4, v5, :cond_61

    #@5f
    const/4 v0, 0x4

    #@60
    goto :goto_d

    #@61
    .line 1225
    :cond_61
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@63
    const/16 v5, -0x76

    #@65
    if-lt v4, v5, :cond_69

    #@67
    const/4 v0, 0x3

    #@68
    goto :goto_d

    #@69
    .line 1226
    :cond_69
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@6b
    const/16 v5, -0x7b

    #@6d
    if-lt v4, v5, :cond_71

    #@6f
    const/4 v0, 0x2

    #@70
    goto :goto_d

    #@71
    .line 1227
    :cond_71
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@73
    const/16 v5, -0x80

    #@75
    if-lt v4, v5, :cond_79

    #@77
    const/4 v0, 0x1

    #@78
    goto :goto_d

    #@79
    .line 1228
    :cond_79
    const/4 v0, 0x0

    #@7a
    goto :goto_d

    #@7b
    .line 1237
    :cond_7b
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@7d
    const/16 v5, -0xc

    #@7f
    if-lt v4, v5, :cond_83

    #@81
    const/4 v1, 0x4

    #@82
    goto :goto_12

    #@83
    .line 1238
    :cond_83
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@85
    const/16 v5, -0xe

    #@87
    if-lt v4, v5, :cond_8b

    #@89
    const/4 v1, 0x3

    #@8a
    goto :goto_12

    #@8b
    .line 1239
    :cond_8b
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@8d
    const/16 v5, -0x11

    #@8f
    if-lt v4, v5, :cond_93

    #@91
    const/4 v1, 0x2

    #@92
    goto :goto_12

    #@93
    .line 1240
    :cond_93
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@95
    const/16 v5, -0x13

    #@97
    if-lt v4, v5, :cond_9c

    #@99
    const/4 v1, 0x1

    #@9a
    goto/16 :goto_12

    #@9c
    .line 1241
    :cond_9c
    const/4 v1, 0x0

    #@9d
    goto/16 :goto_12

    #@9f
    :cond_9f
    move v0, v1

    #@a0
    .line 1253
    goto :goto_50

    #@a1
    .line 1256
    :cond_a1
    if-eq v1, v6, :cond_a5

    #@a3
    move v0, v1

    #@a4
    goto :goto_50

    #@a5
    .line 1258
    :cond_a5
    if-ne v0, v6, :cond_50

    #@a7
    .line 1261
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@a9
    const/16 v5, 0x3f

    #@ab
    if-le v4, v5, :cond_d3

    #@ad
    const/4 v2, 0x0

    #@ae
    .line 1266
    :cond_ae
    :goto_ae
    new-instance v4, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v5, "getLTELevel - rssi:"

    #@b5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v4

    #@b9
    iget v5, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@bb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v4

    #@bf
    const-string v5, " rssiIconLevel:"

    #@c1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v4

    #@c5
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v4

    #@c9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v4

    #@cd
    invoke-static {v4}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@d0
    move v0, v2

    #@d1
    .line 1268
    goto/16 :goto_50

    #@d3
    .line 1262
    :cond_d3
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@d5
    const/16 v5, 0xc

    #@d7
    if-lt v4, v5, :cond_db

    #@d9
    const/4 v2, 0x4

    #@da
    goto :goto_ae

    #@db
    .line 1263
    :cond_db
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@dd
    const/16 v5, 0x8

    #@df
    if-lt v4, v5, :cond_e3

    #@e1
    const/4 v2, 0x3

    #@e2
    goto :goto_ae

    #@e3
    .line 1264
    :cond_e3
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@e5
    const/4 v5, 0x5

    #@e6
    if-lt v4, v5, :cond_ea

    #@e8
    const/4 v2, 0x2

    #@e9
    goto :goto_ae

    #@ea
    .line 1265
    :cond_ea
    iget v4, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@ec
    if-ltz v4, :cond_ae

    #@ee
    const/4 v2, 0x1

    #@ef
    goto :goto_ae
.end method

.method public getLteRsrp()I
    .registers 2

    #@0
    .prologue
    .line 553
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@2
    return v0
.end method

.method public getLteRsrq()I
    .registers 2

    #@0
    .prologue
    .line 558
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@2
    return v0
.end method

.method public getLteRssi()I
    .registers 2

    #@0
    .prologue
    .line 548
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@2
    mul-int/lit8 v0, v0, 0x2

    #@4
    add-int/lit8 v0, v0, -0x72

    #@6
    return v0
.end method

.method public getLteRssnr()I
    .registers 2

    #@0
    .prologue
    .line 563
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@2
    return v0
.end method

.method public getLteSignalStrenght()I
    .registers 2

    #@0
    .prologue
    .line 543
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@2
    return v0
.end method

.method public getLteSnr()I
    .registers 2

    #@0
    .prologue
    .line 578
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@2
    return v0
.end method

.method public getRadioTechnology()I
    .registers 2

    #@0
    .prologue
    .line 1544
    iget v0, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@2
    return v0
.end method

.method public getTdScdmaAsuLevel()I
    .registers 5

    #@0
    .prologue
    .line 1346
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaDbm()I

    #@3
    move-result v1

    #@4
    .line 1349
    .local v1, tdScdmaDbm:I
    const v2, 0x7fffffff

    #@7
    if-ne v1, v2, :cond_22

    #@9
    const/16 v0, 0xff

    #@b
    .line 1351
    .local v0, tdScdmaAsuLevel:I
    :goto_b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "TD-SCDMA Asu level: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@21
    .line 1352
    return v0

    #@22
    .line 1350
    .end local v0           #tdScdmaAsuLevel:I
    :cond_22
    add-int/lit8 v0, v1, 0x78

    #@24
    .restart local v0       #tdScdmaAsuLevel:I
    goto :goto_b
.end method

.method public getTdScdmaDbm()I
    .registers 2

    #@0
    .prologue
    .line 1313
    iget v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@2
    return v0
.end method

.method public getTdScdmaLevel()I
    .registers 5

    #@0
    .prologue
    .line 1325
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getTdScdmaDbm()I

    #@3
    move-result v1

    #@4
    .line 1328
    .local v1, tdScdmaDbm:I
    const/16 v2, -0x19

    #@6
    if-gt v1, v2, :cond_d

    #@8
    const v2, 0x7fffffff

    #@b
    if-ne v1, v2, :cond_25

    #@d
    .line 1329
    :cond_d
    const/4 v0, 0x0

    #@e
    .line 1336
    .local v0, level:I
    :goto_e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "getTdScdmaLevel = "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v2}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@24
    .line 1337
    return v0

    #@25
    .line 1330
    .end local v0           #level:I
    :cond_25
    const/16 v2, -0x31

    #@27
    if-lt v1, v2, :cond_2b

    #@29
    const/4 v0, 0x4

    #@2a
    .restart local v0       #level:I
    goto :goto_e

    #@2b
    .line 1331
    .end local v0           #level:I
    :cond_2b
    const/16 v2, -0x49

    #@2d
    if-lt v1, v2, :cond_31

    #@2f
    const/4 v0, 0x3

    #@30
    .restart local v0       #level:I
    goto :goto_e

    #@31
    .line 1332
    .end local v0           #level:I
    :cond_31
    const/16 v2, -0x61

    #@33
    if-lt v1, v2, :cond_37

    #@35
    const/4 v0, 0x2

    #@36
    .restart local v0       #level:I
    goto :goto_e

    #@37
    .line 1333
    .end local v0           #level:I
    :cond_37
    const/16 v2, -0x78

    #@39
    if-lt v1, v2, :cond_3d

    #@3b
    const/4 v0, 0x1

    #@3c
    .restart local v0       #level:I
    goto :goto_e

    #@3d
    .line 1334
    .end local v0           #level:I
    :cond_3d
    const/4 v0, 0x0

    #@3e
    .restart local v0       #level:I
    goto :goto_e
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 1360
    const/16 v0, 0x1f

    #@2
    .line 1361
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@b
    mul-int/2addr v2, v0

    #@c
    add-int/2addr v1, v2

    #@d
    iget v2, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@f
    mul-int/2addr v2, v0

    #@10
    add-int/2addr v1, v2

    #@11
    iget v2, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@13
    mul-int/2addr v2, v0

    #@14
    add-int/2addr v1, v2

    #@15
    iget v2, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@17
    mul-int/2addr v2, v0

    #@18
    add-int/2addr v1, v2

    #@19
    iget v2, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@1b
    mul-int/2addr v2, v0

    #@1c
    add-int/2addr v1, v2

    #@1d
    iget v2, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@1f
    mul-int/2addr v2, v0

    #@20
    add-int/2addr v1, v2

    #@21
    iget v2, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@23
    mul-int/2addr v2, v0

    #@24
    add-int/2addr v1, v2

    #@25
    iget v2, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@27
    mul-int/2addr v2, v0

    #@28
    add-int/2addr v1, v2

    #@29
    iget v2, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@2b
    mul-int/2addr v2, v0

    #@2c
    add-int/2addr v1, v2

    #@2d
    iget v2, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@2f
    mul-int/2addr v2, v0

    #@30
    add-int/2addr v1, v2

    #@31
    iget v2, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@33
    mul-int/2addr v2, v0

    #@34
    add-int/2addr v2, v1

    #@35
    iget-boolean v1, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@37
    if-eqz v1, :cond_3c

    #@39
    const/4 v1, 0x1

    #@3a
    :goto_3a
    add-int/2addr v1, v2

    #@3b
    return v1

    #@3c
    :cond_3c
    const/4 v1, 0x0

    #@3d
    goto :goto_3a
.end method

.method public initialize(IIIIIIIIIIIIZ)V
    .registers 16
    .parameter "gsmSignalStrength"
    .parameter "gsmBitErrorRate"
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"
    .parameter "lteSignalStrength"
    .parameter "lteRsrp"
    .parameter "lteRsrq"
    .parameter "lteRssnr"
    .parameter "lteCqi"
    .parameter "gsm"

    #@0
    .prologue
    .line 303
    iput p1, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@2
    .line 304
    iput p2, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@4
    .line 305
    iput p3, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@6
    .line 306
    iput p4, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@8
    .line 307
    iput p5, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@a
    .line 308
    iput p6, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@c
    .line 309
    iput p7, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@e
    .line 310
    iput p8, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@10
    .line 311
    iput p9, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@12
    .line 312
    iput p10, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@14
    .line 313
    iput p11, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@16
    .line 314
    iput p12, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@18
    .line 315
    const v0, 0x7fffffff

    #@1b
    iput v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@1d
    .line 316
    iput-boolean p13, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@1f
    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v1, "initialize: "

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    invoke-static {v0}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@39
    .line 318
    return-void
.end method

.method public initialize(IIIIIIIZ)V
    .registers 23
    .parameter "gsmSignalStrength"
    .parameter "gsmBitErrorRate"
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"
    .parameter "gsm"

    #@0
    .prologue
    .line 274
    const/16 v8, 0x63

    #@2
    const v9, 0x7fffffff

    #@5
    const v10, 0x7fffffff

    #@8
    const v11, 0x7fffffff

    #@b
    const v12, 0x7fffffff

    #@e
    move-object v0, p0

    #@f
    move v1, p1

    #@10
    move/from16 v2, p2

    #@12
    move/from16 v3, p3

    #@14
    move/from16 v4, p4

    #@16
    move/from16 v5, p5

    #@18
    move/from16 v6, p6

    #@1a
    move/from16 v7, p7

    #@1c
    move/from16 v13, p8

    #@1e
    invoke-virtual/range {v0 .. v13}, Landroid/telephony/SignalStrength;->initialize(IIIIIIIIIIIIZ)V

    #@21
    .line 277
    return-void
.end method

.method public isGsm()Z
    .registers 2

    #@0
    .prologue
    .line 1304
    iget-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@2
    return v0
.end method

.method public setGsm(Z)V
    .registers 2
    .parameter "gsmFlag"

    #@0
    .prologue
    .line 481
    iput-boolean p1, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@2
    .line 482
    return-void
.end method

.method public setLgeRssiData(Lcom/android/internal/telephony/LgeRssiData;)V
    .registers 2
    .parameter "instance"

    #@0
    .prologue
    .line 1799
    sput-object p1, Landroid/telephony/SignalStrength;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@2
    .line 1800
    return-void
.end method

.method public setRadioTechnology(I)V
    .registers 2
    .parameter "radioTechnology"

    #@0
    .prologue
    .line 1535
    iput p1, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@2
    .line 1536
    return-void
.end method

.method public setfeature(I)V
    .registers 2
    .parameter "feature"

    #@0
    .prologue
    .line 487
    iput p1, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@2
    .line 488
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1408
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SignalStrength: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, " "

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, " "

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget v1, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, " "

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    const-string v1, " "

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    const-string v1, " "

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    const-string v1, " "

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    const-string v1, " "

    #@8b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v0

    #@95
    const-string v1, " "

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v0

    #@9b
    iget v1, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v0

    #@a1
    const-string v1, " "

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v1

    #@a7
    iget-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@a9
    if-eqz v0, :cond_c2

    #@ab
    const-string v0, "gsm|lte"

    #@ad
    :goto_ad
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v0

    #@b1
    const-string v1, " "

    #@b3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v0

    #@b7
    iget v1, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@b9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v0

    #@bd
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v0

    #@c1
    return-object v0

    #@c2
    :cond_c2
    const-string v0, "cdma"

    #@c4
    goto :goto_ad
.end method

.method public validateInput()V
    .registers 8

    #@0
    .prologue
    const/16 v1, 0x63

    #@2
    const/16 v6, 0x8

    #@4
    const/16 v2, -0x78

    #@6
    const/4 v3, -0x1

    #@7
    const v4, 0x7fffffff

    #@a
    .line 441
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "Signal before validate="

    #@11
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@20
    .line 443
    iget v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@22
    if-ltz v0, :cond_c1

    #@24
    iget v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@26
    :goto_26
    iput v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@28
    .line 446
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@2a
    if-lez v0, :cond_c4

    #@2c
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@2e
    neg-int v0, v0

    #@2f
    :goto_2f
    iput v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@31
    .line 447
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@33
    if-lez v0, :cond_c7

    #@35
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@37
    neg-int v0, v0

    #@38
    :goto_38
    iput v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@3a
    .line 449
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@3c
    if-lez v0, :cond_41

    #@3e
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@40
    neg-int v2, v0

    #@41
    :cond_41
    iput v2, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@43
    .line 450
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@45
    if-ltz v0, :cond_cb

    #@47
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@49
    neg-int v0, v0

    #@4a
    :goto_4a
    iput v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@4c
    .line 452
    const-string v0, "KDDI"

    #@4e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_ce

    #@54
    .line 453
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@56
    if-ltz v0, :cond_5e

    #@58
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@5a
    if-gt v0, v6, :cond_5e

    #@5c
    iget v3, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@5e
    :cond_5e
    iput v3, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@60
    .line 459
    :goto_60
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@62
    if-ltz v0, :cond_66

    #@64
    iget v1, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@66
    :cond_66
    iput v1, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@68
    .line 460
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@6a
    const/16 v1, 0x2c

    #@6c
    if-lt v0, v1, :cond_db

    #@6e
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@70
    const/16 v1, 0x8c

    #@72
    if-gt v0, v1, :cond_db

    #@74
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@76
    neg-int v0, v0

    #@77
    :goto_77
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@79
    .line 461
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@7b
    const/4 v1, 0x3

    #@7c
    if-lt v0, v1, :cond_dd

    #@7e
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@80
    const/16 v1, 0x14

    #@82
    if-gt v0, v1, :cond_dd

    #@84
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@86
    neg-int v0, v0

    #@87
    :goto_87
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@89
    .line 462
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@8b
    const/16 v1, -0xc8

    #@8d
    if-lt v0, v1, :cond_df

    #@8f
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@91
    const/16 v1, 0x12c

    #@93
    if-gt v0, v1, :cond_df

    #@95
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@97
    :goto_97
    iput v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@99
    .line 465
    iget v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@9b
    const/16 v1, 0x19

    #@9d
    if-lt v0, v1, :cond_a8

    #@9f
    iget v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@a1
    const/16 v1, 0x78

    #@a3
    if-gt v0, v1, :cond_a8

    #@a5
    iget v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@a7
    neg-int v4, v0

    #@a8
    :cond_a8
    iput v4, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@aa
    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v1, "Signal after validate="

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v0

    #@b5
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v0

    #@b9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v0

    #@bd
    invoke-static {v0}, Landroid/telephony/SignalStrength;->log(Ljava/lang/String;)V

    #@c0
    .line 470
    return-void

    #@c1
    :cond_c1
    move v0, v1

    #@c2
    .line 443
    goto/16 :goto_26

    #@c4
    :cond_c4
    move v0, v2

    #@c5
    .line 446
    goto/16 :goto_2f

    #@c7
    .line 447
    :cond_c7
    const/16 v0, -0xa0

    #@c9
    goto/16 :goto_38

    #@cb
    :cond_cb
    move v0, v3

    #@cc
    .line 450
    goto/16 :goto_4a

    #@ce
    .line 456
    :cond_ce
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@d0
    if-lez v0, :cond_d8

    #@d2
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@d4
    if-gt v0, v6, :cond_d8

    #@d6
    iget v3, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@d8
    :cond_d8
    iput v3, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@da
    goto :goto_60

    #@db
    :cond_db
    move v0, v4

    #@dc
    .line 460
    goto :goto_77

    #@dd
    :cond_dd
    move v0, v4

    #@de
    .line 461
    goto :goto_87

    #@df
    :cond_df
    move v0, v4

    #@e0
    .line 462
    goto :goto_97
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 383
    iget v0, p0, Landroid/telephony/SignalStrength;->mGsmSignalStrength:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 384
    iget v0, p0, Landroid/telephony/SignalStrength;->mGsmBitErrorRate:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 385
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaDbm:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 386
    iget v0, p0, Landroid/telephony/SignalStrength;->mCdmaEcio:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 387
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoDbm:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 388
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoEcio:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 389
    iget v0, p0, Landroid/telephony/SignalStrength;->mEvdoSnr:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 390
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteSignalStrength:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 391
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrp:I

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 392
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRsrq:I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 393
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteRssnr:I

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 394
    iget v0, p0, Landroid/telephony/SignalStrength;->mLteCqi:I

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 395
    iget v0, p0, Landroid/telephony/SignalStrength;->mTdScdmaRscp:I

    #@3e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 396
    iget-boolean v0, p0, Landroid/telephony/SignalStrength;->isGsm:Z

    #@43
    if-eqz v0, :cond_54

    #@45
    const/4 v0, 0x1

    #@46
    :goto_46
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 398
    iget v0, p0, Landroid/telephony/SignalStrength;->mRadioTechnology:I

    #@4b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4e
    .line 402
    iget v0, p0, Landroid/telephony/SignalStrength;->datafeature:I

    #@50
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    .line 405
    return-void

    #@54
    .line 396
    :cond_54
    const/4 v0, 0x0

    #@55
    goto :goto_46
.end method
