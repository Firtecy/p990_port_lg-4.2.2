.class public final Landroid/telephony/CellIdentityGsm;
.super Ljava/lang/Object;
.source "CellIdentityGsm.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellIdentityGsm;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "CellIdentityGsm"


# instance fields
.field private final mCid:I

.field private final mLac:I

.field private final mMcc:I

.field private final mMnc:I

.field private final mPsc:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 190
    new-instance v0, Landroid/telephony/CellIdentityGsm$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellIdentityGsm$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellIdentityGsm;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 47
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@8
    .line 48
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@a
    .line 49
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@c
    .line 50
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@e
    .line 51
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@10
    .line 52
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 6
    .parameter "mcc"
    .parameter "mnc"
    .parameter "lac"
    .parameter "cid"
    .parameter "psc"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 64
    iput p1, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@5
    .line 65
    iput p2, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@7
    .line 66
    iput p3, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@9
    .line 67
    iput p4, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@b
    .line 68
    iput p5, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@d
    .line 69
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 179
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@9
    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@f
    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@15
    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@1b
    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@21
    .line 186
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellIdentityGsm$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/telephony/CellIdentityGsm;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/telephony/CellIdentityGsm;)V
    .registers 3
    .parameter "cid"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    iget v0, p1, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@5
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@7
    .line 73
    iget v0, p1, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@9
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@b
    .line 74
    iget v0, p1, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@d
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@f
    .line 75
    iget v0, p1, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@11
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@13
    .line 76
    iget v0, p1, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@15
    iput v0, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@17
    .line 77
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 207
    const-string v0, "CellIdentityGsm"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 208
    return-void
.end method


# virtual methods
.method copy()Landroid/telephony/CellIdentityGsm;
    .registers 2

    #@0
    .prologue
    .line 80
    new-instance v0, Landroid/telephony/CellIdentityGsm;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellIdentityGsm;-><init>(Landroid/telephony/CellIdentityGsm;)V

    #@5
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 164
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "other"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 132
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@4
    move-result v4

    #@5
    if-eqz v4, :cond_2a

    #@7
    .line 134
    :try_start_7
    move-object v0, p1

    #@8
    check-cast v0, Landroid/telephony/CellIdentityGsm;

    #@a
    move-object v2, v0

    #@b
    .line 135
    .local v2, o:Landroid/telephony/CellIdentityGsm;
    iget v4, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@d
    iget v5, v2, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@f
    if-ne v4, v5, :cond_2a

    #@11
    iget v4, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@13
    iget v5, v2, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@15
    if-ne v4, v5, :cond_2a

    #@17
    iget v4, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@19
    iget v5, v2, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@1b
    if-ne v4, v5, :cond_2a

    #@1d
    iget v4, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@1f
    iget v5, v2, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@21
    if-ne v4, v5, :cond_2a

    #@23
    iget v4, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@25
    iget v5, v2, Landroid/telephony/CellIdentityGsm;->mPsc:I
    :try_end_27
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_27} :catch_2b

    #@27
    if-ne v4, v5, :cond_2a

    #@29
    const/4 v3, 0x1

    #@2a
    .line 144
    .end local v2           #o:Landroid/telephony/CellIdentityGsm;
    :cond_2a
    :goto_2a
    return v3

    #@2b
    .line 140
    :catch_2b
    move-exception v1

    #@2c
    .line 141
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_2a
.end method

.method public getCid()I
    .registers 2

    #@0
    .prologue
    .line 112
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@2
    return v0
.end method

.method public getLac()I
    .registers 2

    #@0
    .prologue
    .line 101
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@2
    return v0
.end method

.method public getMcc()I
    .registers 2

    #@0
    .prologue
    .line 87
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@2
    return v0
.end method

.method public getMnc()I
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@2
    return v0
.end method

.method public getPsc()I
    .registers 2

    #@0
    .prologue
    .line 120
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 125
    const/16 v0, 0x1f

    #@2
    .line 126
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@b
    mul-int/2addr v2, v0

    #@c
    add-int/2addr v1, v2

    #@d
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@f
    mul-int/2addr v2, v0

    #@10
    add-int/2addr v1, v2

    #@11
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@13
    mul-int/2addr v2, v0

    #@14
    add-int/2addr v1, v2

    #@15
    return v1
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "GsmCellIdentitiy:"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 151
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 152
    const-string v1, " mMcc="

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    .line 153
    const-string v1, " mMnc="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    .line 154
    const-string v1, " mLac="

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    .line 155
    const-string v1, " mCid="

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    .line 156
    const-string v1, " mPsc="

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    iget v2, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    .line 158
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 171
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mMcc:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 172
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mMnc:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 173
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mLac:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 174
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mCid:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 175
    iget v0, p0, Landroid/telephony/CellIdentityGsm;->mPsc:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 176
    return-void
.end method
