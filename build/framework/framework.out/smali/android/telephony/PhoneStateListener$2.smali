.class Landroid/telephony/PhoneStateListener$2;
.super Landroid/os/Handler;
.source "PhoneStateListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/PhoneStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/telephony/PhoneStateListener;


# direct methods
.method constructor <init>(Landroid/telephony/PhoneStateListener;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 369
    iput-object p1, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 372
    iget v2, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v2, :sswitch_data_7e

    #@7
    .line 407
    :goto_7
    return-void

    #@8
    .line 374
    :sswitch_8
    iget-object v1, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v0, Landroid/telephony/ServiceState;

    #@e
    invoke-virtual {v1, v0}, Landroid/telephony/PhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V

    #@11
    goto :goto_7

    #@12
    .line 377
    :sswitch_12
    iget-object v0, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@14
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@16
    invoke-virtual {v0, v1}, Landroid/telephony/PhoneStateListener;->onSignalStrengthChanged(I)V

    #@19
    goto :goto_7

    #@1a
    .line 380
    :sswitch_1a
    iget-object v2, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@1c
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@1e
    if-eqz v3, :cond_24

    #@20
    :goto_20
    invoke-virtual {v2, v0}, Landroid/telephony/PhoneStateListener;->onMessageWaitingIndicatorChanged(Z)V

    #@23
    goto :goto_7

    #@24
    :cond_24
    move v0, v1

    #@25
    goto :goto_20

    #@26
    .line 383
    :sswitch_26
    iget-object v2, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@28
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@2a
    if-eqz v3, :cond_30

    #@2c
    :goto_2c
    invoke-virtual {v2, v0}, Landroid/telephony/PhoneStateListener;->onCallForwardingIndicatorChanged(Z)V

    #@2f
    goto :goto_7

    #@30
    :cond_30
    move v0, v1

    #@31
    goto :goto_2c

    #@32
    .line 386
    :sswitch_32
    iget-object v1, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@34
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@36
    check-cast v0, Landroid/telephony/CellLocation;

    #@38
    invoke-virtual {v1, v0}, Landroid/telephony/PhoneStateListener;->onCellLocationChanged(Landroid/telephony/CellLocation;)V

    #@3b
    goto :goto_7

    #@3c
    .line 389
    :sswitch_3c
    iget-object v1, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@3e
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@40
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@42
    check-cast v0, Ljava/lang/String;

    #@44
    invoke-virtual {v1, v2, v0}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    #@47
    goto :goto_7

    #@48
    .line 392
    :sswitch_48
    iget-object v0, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@4a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@4c
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@4e
    invoke-virtual {v0, v1, v2}, Landroid/telephony/PhoneStateListener;->onDataConnectionStateChanged(II)V

    #@51
    .line 393
    iget-object v0, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@53
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@55
    invoke-virtual {v0, v1}, Landroid/telephony/PhoneStateListener;->onDataConnectionStateChanged(I)V

    #@58
    goto :goto_7

    #@59
    .line 396
    :sswitch_59
    iget-object v0, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@5b
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@5d
    invoke-virtual {v0, v1}, Landroid/telephony/PhoneStateListener;->onDataActivity(I)V

    #@60
    goto :goto_7

    #@61
    .line 399
    :sswitch_61
    iget-object v1, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@63
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@65
    check-cast v0, Landroid/telephony/SignalStrength;

    #@67
    invoke-virtual {v1, v0}, Landroid/telephony/PhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V

    #@6a
    goto :goto_7

    #@6b
    .line 402
    :sswitch_6b
    iget-object v0, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@6d
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@6f
    invoke-virtual {v0, v1}, Landroid/telephony/PhoneStateListener;->onOtaspChanged(I)V

    #@72
    goto :goto_7

    #@73
    .line 405
    :sswitch_73
    iget-object v1, p0, Landroid/telephony/PhoneStateListener$2;->this$0:Landroid/telephony/PhoneStateListener;

    #@75
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@77
    check-cast v0, Ljava/util/List;

    #@79
    invoke-virtual {v1, v0}, Landroid/telephony/PhoneStateListener;->onCellInfoChanged(Ljava/util/List;)V

    #@7c
    goto :goto_7

    #@7d
    .line 372
    nop

    #@7e
    :sswitch_data_7e
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_12
        0x4 -> :sswitch_1a
        0x8 -> :sswitch_26
        0x10 -> :sswitch_32
        0x20 -> :sswitch_3c
        0x40 -> :sswitch_48
        0x80 -> :sswitch_59
        0x100 -> :sswitch_61
        0x200 -> :sswitch_6b
        0x400 -> :sswitch_73
    .end sparse-switch
.end method
