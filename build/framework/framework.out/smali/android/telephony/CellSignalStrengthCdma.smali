.class public final Landroid/telephony/CellSignalStrengthCdma;
.super Landroid/telephony/CellSignalStrength;
.source "CellSignalStrengthCdma.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellSignalStrengthCdma;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "CellSignalStrengthCdma"


# instance fields
.field private mCdmaDbm:I

.field private mCdmaEcio:I

.field private mEvdoDbm:I

.field private mEvdoEcio:I

.field private mEvdoSnr:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 362
    new-instance v0, Landroid/telephony/CellSignalStrengthCdma$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellSignalStrengthCdma$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellSignalStrengthCdma;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 43
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->setDefaultValues()V

    #@6
    .line 44
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 6
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 53
    invoke-virtual/range {p0 .. p5}, Landroid/telephony/CellSignalStrengthCdma;->initialize(IIIII)V

    #@6
    .line 54
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 345
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 346
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@9
    .line 347
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@f
    .line 348
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@15
    .line 349
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@1b
    .line 350
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@21
    .line 352
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellSignalStrengthCdma$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/telephony/CellSignalStrengthCdma;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/telephony/CellSignalStrengthCdma;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 64
    invoke-virtual {p0, p1}, Landroid/telephony/CellSignalStrengthCdma;->copyFrom(Landroid/telephony/CellSignalStrengthCdma;)V

    #@6
    .line 65
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 379
    const-string v0, "CellSignalStrengthCdma"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 380
    return-void
.end method


# virtual methods
.method public bridge synthetic copy()Landroid/telephony/CellSignalStrength;
    .registers 2

    #@0
    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->copy()Landroid/telephony/CellSignalStrengthCdma;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public copy()Landroid/telephony/CellSignalStrengthCdma;
    .registers 2

    #@0
    .prologue
    .line 102
    new-instance v0, Landroid/telephony/CellSignalStrengthCdma;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellSignalStrengthCdma;-><init>(Landroid/telephony/CellSignalStrengthCdma;)V

    #@5
    return-object v0
.end method

.method protected copyFrom(Landroid/telephony/CellSignalStrengthCdma;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 90
    iget v0, p1, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@2
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@4
    .line 91
    iget v0, p1, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@6
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@8
    .line 92
    iget v0, p1, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@a
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@c
    .line 93
    iget v0, p1, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@e
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@10
    .line 94
    iget v0, p1, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@12
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@14
    .line 95
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 357
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 301
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/CellSignalStrengthCdma;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 306
    .local v2, s:Landroid/telephony/CellSignalStrengthCdma;
    if-nez p1, :cond_a

    #@7
    .line 310
    .end local v2           #s:Landroid/telephony/CellSignalStrengthCdma;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 302
    :catch_8
    move-exception v1

    #@9
    .line 303
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 310
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/CellSignalStrengthCdma;
    :cond_a
    iget v4, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@c
    iget v5, v2, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@e
    if-ne v4, v5, :cond_7

    #@10
    iget v4, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@12
    iget v5, v2, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@14
    if-ne v4, v5, :cond_7

    #@16
    iget v4, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@18
    iget v5, v2, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@1a
    if-ne v4, v5, :cond_7

    #@1c
    iget v4, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@1e
    iget v5, v2, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@20
    if-ne v4, v5, :cond_7

    #@22
    iget v4, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@24
    iget v5, v2, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@26
    if-ne v4, v5, :cond_7

    #@28
    const/4 v3, 0x1

    #@29
    goto :goto_7
.end method

.method public getAsuLevel()I
    .registers 9

    #@0
    .prologue
    const/16 v7, -0x5a

    #@2
    const/16 v6, -0x64

    #@4
    .line 144
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaDbm()I

    #@7
    move-result v1

    #@8
    .line 145
    .local v1, cdmaDbm:I
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaEcio()I

    #@b
    move-result v2

    #@c
    .line 149
    .local v2, cdmaEcio:I
    const/16 v5, -0x4b

    #@e
    if-lt v1, v5, :cond_1a

    #@10
    const/16 v0, 0x10

    #@12
    .line 157
    .local v0, cdmaAsuLevel:I
    :goto_12
    if-lt v2, v7, :cond_32

    #@14
    const/16 v3, 0x10

    #@16
    .line 164
    .local v3, ecioAsuLevel:I
    :goto_16
    if-ge v0, v3, :cond_4c

    #@18
    move v4, v0

    #@19
    .line 166
    .local v4, level:I
    :goto_19
    return v4

    #@1a
    .line 150
    .end local v0           #cdmaAsuLevel:I
    .end local v3           #ecioAsuLevel:I
    .end local v4           #level:I
    :cond_1a
    const/16 v5, -0x52

    #@1c
    if-lt v1, v5, :cond_21

    #@1e
    const/16 v0, 0x8

    #@20
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@21
    .line 151
    .end local v0           #cdmaAsuLevel:I
    :cond_21
    if-lt v1, v7, :cond_25

    #@23
    const/4 v0, 0x4

    #@24
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@25
    .line 152
    .end local v0           #cdmaAsuLevel:I
    :cond_25
    const/16 v5, -0x5f

    #@27
    if-lt v1, v5, :cond_2b

    #@29
    const/4 v0, 0x2

    #@2a
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@2b
    .line 153
    .end local v0           #cdmaAsuLevel:I
    :cond_2b
    if-lt v1, v6, :cond_2f

    #@2d
    const/4 v0, 0x1

    #@2e
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@2f
    .line 154
    .end local v0           #cdmaAsuLevel:I
    :cond_2f
    const/16 v0, 0x63

    #@31
    .restart local v0       #cdmaAsuLevel:I
    goto :goto_12

    #@32
    .line 158
    :cond_32
    if-lt v2, v6, :cond_37

    #@34
    const/16 v3, 0x8

    #@36
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@37
    .line 159
    .end local v3           #ecioAsuLevel:I
    :cond_37
    const/16 v5, -0x73

    #@39
    if-lt v2, v5, :cond_3d

    #@3b
    const/4 v3, 0x4

    #@3c
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@3d
    .line 160
    .end local v3           #ecioAsuLevel:I
    :cond_3d
    const/16 v5, -0x82

    #@3f
    if-lt v2, v5, :cond_43

    #@41
    const/4 v3, 0x2

    #@42
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@43
    .line 161
    .end local v3           #ecioAsuLevel:I
    :cond_43
    const/16 v5, -0x96

    #@45
    if-lt v2, v5, :cond_49

    #@47
    const/4 v3, 0x1

    #@48
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@49
    .line 162
    .end local v3           #ecioAsuLevel:I
    :cond_49
    const/16 v3, 0x63

    #@4b
    .restart local v3       #ecioAsuLevel:I
    goto :goto_16

    #@4c
    :cond_4c
    move v4, v3

    #@4d
    .line 164
    goto :goto_19
.end method

.method public getCdmaDbm()I
    .registers 2

    #@0
    .prologue
    .line 238
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@2
    return v0
.end method

.method public getCdmaEcio()I
    .registers 2

    #@0
    .prologue
    .line 249
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@2
    return v0
.end method

.method public getCdmaLevel()I
    .registers 7

    #@0
    .prologue
    .line 173
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaDbm()I

    #@3
    move-result v0

    #@4
    .line 174
    .local v0, cdmaDbm:I
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaEcio()I

    #@7
    move-result v1

    #@8
    .line 178
    .local v1, cdmaEcio:I
    const/16 v5, -0x4b

    #@a
    if-lt v0, v5, :cond_16

    #@c
    const/4 v3, 0x4

    #@d
    .line 185
    .local v3, levelDbm:I
    :goto_d
    const/16 v5, -0x5a

    #@f
    if-lt v1, v5, :cond_2a

    #@11
    const/4 v4, 0x4

    #@12
    .line 191
    .local v4, levelEcio:I
    :goto_12
    if-ge v3, v4, :cond_3e

    #@14
    move v2, v3

    #@15
    .line 193
    .local v2, level:I
    :goto_15
    return v2

    #@16
    .line 179
    .end local v2           #level:I
    .end local v3           #levelDbm:I
    .end local v4           #levelEcio:I
    :cond_16
    const/16 v5, -0x55

    #@18
    if-lt v0, v5, :cond_1c

    #@1a
    const/4 v3, 0x3

    #@1b
    .restart local v3       #levelDbm:I
    goto :goto_d

    #@1c
    .line 180
    .end local v3           #levelDbm:I
    :cond_1c
    const/16 v5, -0x5f

    #@1e
    if-lt v0, v5, :cond_22

    #@20
    const/4 v3, 0x2

    #@21
    .restart local v3       #levelDbm:I
    goto :goto_d

    #@22
    .line 181
    .end local v3           #levelDbm:I
    :cond_22
    const/16 v5, -0x64

    #@24
    if-lt v0, v5, :cond_28

    #@26
    const/4 v3, 0x1

    #@27
    .restart local v3       #levelDbm:I
    goto :goto_d

    #@28
    .line 182
    .end local v3           #levelDbm:I
    :cond_28
    const/4 v3, 0x0

    #@29
    .restart local v3       #levelDbm:I
    goto :goto_d

    #@2a
    .line 186
    :cond_2a
    const/16 v5, -0x6e

    #@2c
    if-lt v1, v5, :cond_30

    #@2e
    const/4 v4, 0x3

    #@2f
    .restart local v4       #levelEcio:I
    goto :goto_12

    #@30
    .line 187
    .end local v4           #levelEcio:I
    :cond_30
    const/16 v5, -0x82

    #@32
    if-lt v1, v5, :cond_36

    #@34
    const/4 v4, 0x2

    #@35
    .restart local v4       #levelEcio:I
    goto :goto_12

    #@36
    .line 188
    .end local v4           #levelEcio:I
    :cond_36
    const/16 v5, -0x96

    #@38
    if-lt v1, v5, :cond_3c

    #@3a
    const/4 v4, 0x1

    #@3b
    .restart local v4       #levelEcio:I
    goto :goto_12

    #@3c
    .line 189
    .end local v4           #levelEcio:I
    :cond_3c
    const/4 v4, 0x0

    #@3d
    .restart local v4       #levelEcio:I
    goto :goto_12

    #@3e
    :cond_3e
    move v2, v4

    #@3f
    .line 191
    goto :goto_15
.end method

.method public getDbm()I
    .registers 3

    #@0
    .prologue
    .line 227
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaDbm()I

    #@3
    move-result v0

    #@4
    .line 228
    .local v0, cdmaDbm:I
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoDbm()I

    #@7
    move-result v1

    #@8
    .line 231
    .local v1, evdoDbm:I
    if-ge v0, v1, :cond_b

    #@a
    .end local v0           #cdmaDbm:I
    :goto_a
    return v0

    #@b
    .restart local v0       #cdmaDbm:I
    :cond_b
    move v0, v1

    #@c
    goto :goto_a
.end method

.method public getEvdoDbm()I
    .registers 2

    #@0
    .prologue
    .line 260
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@2
    return v0
.end method

.method public getEvdoEcio()I
    .registers 2

    #@0
    .prologue
    .line 271
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@2
    return v0
.end method

.method public getEvdoLevel()I
    .registers 7

    #@0
    .prologue
    .line 200
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoDbm()I

    #@3
    move-result v0

    #@4
    .line 201
    .local v0, evdoDbm:I
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoSnr()I

    #@7
    move-result v1

    #@8
    .line 205
    .local v1, evdoSnr:I
    const/16 v5, -0x41

    #@a
    if-lt v0, v5, :cond_15

    #@c
    const/4 v3, 0x4

    #@d
    .line 211
    .local v3, levelEvdoDbm:I
    :goto_d
    const/4 v5, 0x7

    #@e
    if-lt v1, v5, :cond_29

    #@10
    const/4 v4, 0x4

    #@11
    .line 217
    .local v4, levelEvdoSnr:I
    :goto_11
    if-ge v3, v4, :cond_3a

    #@13
    move v2, v3

    #@14
    .line 219
    .local v2, level:I
    :goto_14
    return v2

    #@15
    .line 206
    .end local v2           #level:I
    .end local v3           #levelEvdoDbm:I
    .end local v4           #levelEvdoSnr:I
    :cond_15
    const/16 v5, -0x4b

    #@17
    if-lt v0, v5, :cond_1b

    #@19
    const/4 v3, 0x3

    #@1a
    .restart local v3       #levelEvdoDbm:I
    goto :goto_d

    #@1b
    .line 207
    .end local v3           #levelEvdoDbm:I
    :cond_1b
    const/16 v5, -0x5a

    #@1d
    if-lt v0, v5, :cond_21

    #@1f
    const/4 v3, 0x2

    #@20
    .restart local v3       #levelEvdoDbm:I
    goto :goto_d

    #@21
    .line 208
    .end local v3           #levelEvdoDbm:I
    :cond_21
    const/16 v5, -0x69

    #@23
    if-lt v0, v5, :cond_27

    #@25
    const/4 v3, 0x1

    #@26
    .restart local v3       #levelEvdoDbm:I
    goto :goto_d

    #@27
    .line 209
    .end local v3           #levelEvdoDbm:I
    :cond_27
    const/4 v3, 0x0

    #@28
    .restart local v3       #levelEvdoDbm:I
    goto :goto_d

    #@29
    .line 212
    :cond_29
    const/4 v5, 0x5

    #@2a
    if-lt v1, v5, :cond_2e

    #@2c
    const/4 v4, 0x3

    #@2d
    .restart local v4       #levelEvdoSnr:I
    goto :goto_11

    #@2e
    .line 213
    .end local v4           #levelEvdoSnr:I
    :cond_2e
    const/4 v5, 0x3

    #@2f
    if-lt v1, v5, :cond_33

    #@31
    const/4 v4, 0x2

    #@32
    .restart local v4       #levelEvdoSnr:I
    goto :goto_11

    #@33
    .line 214
    .end local v4           #levelEvdoSnr:I
    :cond_33
    const/4 v5, 0x1

    #@34
    if-lt v1, v5, :cond_38

    #@36
    const/4 v4, 0x1

    #@37
    .restart local v4       #levelEvdoSnr:I
    goto :goto_11

    #@38
    .line 215
    .end local v4           #levelEvdoSnr:I
    :cond_38
    const/4 v4, 0x0

    #@39
    .restart local v4       #levelEvdoSnr:I
    goto :goto_11

    #@3a
    :cond_3a
    move v2, v4

    #@3b
    .line 217
    goto :goto_14
.end method

.method public getEvdoSnr()I
    .registers 2

    #@0
    .prologue
    .line 282
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@2
    return v0
.end method

.method public getLevel()I
    .registers 4

    #@0
    .prologue
    .line 122
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaLevel()I

    #@3
    move-result v0

    #@4
    .line 123
    .local v0, cdmaLevel:I
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoLevel()I

    #@7
    move-result v1

    #@8
    .line 124
    .local v1, evdoLevel:I
    if-nez v1, :cond_f

    #@a
    .line 126
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaLevel()I

    #@d
    move-result v2

    #@e
    .line 135
    .local v2, level:I
    :goto_e
    return v2

    #@f
    .line 127
    .end local v2           #level:I
    :cond_f
    if-nez v0, :cond_16

    #@11
    .line 129
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoLevel()I

    #@14
    move-result v2

    #@15
    .restart local v2       #level:I
    goto :goto_e

    #@16
    .line 132
    .end local v2           #level:I
    :cond_16
    if-ge v0, v1, :cond_1a

    #@18
    move v2, v0

    #@19
    .restart local v2       #level:I
    :goto_19
    goto :goto_e

    #@1a
    .end local v2           #level:I
    :cond_1a
    move v2, v1

    #@1b
    goto :goto_19
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 291
    const/16 v0, 0x1f

    #@2
    .line 292
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@b
    mul-int/2addr v2, v0

    #@c
    add-int/2addr v1, v2

    #@d
    iget v2, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@f
    mul-int/2addr v2, v0

    #@10
    add-int/2addr v1, v2

    #@11
    iget v2, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@13
    mul-int/2addr v2, v0

    #@14
    add-int/2addr v1, v2

    #@15
    return v1
.end method

.method public initialize(IIIII)V
    .registers 6
    .parameter "cdmaDbm"
    .parameter "cdmaEcio"
    .parameter "evdoDbm"
    .parameter "evdoEcio"
    .parameter "evdoSnr"

    #@0
    .prologue
    .line 79
    iput p1, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@2
    .line 80
    iput p2, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@4
    .line 81
    iput p3, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@6
    .line 82
    iput p4, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@8
    .line 83
    iput p5, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@a
    .line 84
    return-void
.end method

.method public setCdmaDbm(I)V
    .registers 2
    .parameter "cdmaDbm"

    #@0
    .prologue
    .line 242
    iput p1, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@2
    .line 243
    return-void
.end method

.method public setCdmaEcio(I)V
    .registers 2
    .parameter "cdmaEcio"

    #@0
    .prologue
    .line 253
    iput p1, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@2
    .line 254
    return-void
.end method

.method public setDefaultValues()V
    .registers 2

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    .line 108
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@5
    .line 109
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@7
    .line 110
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@9
    .line 111
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@b
    .line 112
    iput v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@d
    .line 113
    return-void
.end method

.method public setEvdoDbm(I)V
    .registers 2
    .parameter "evdoDbm"

    #@0
    .prologue
    .line 264
    iput p1, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@2
    .line 265
    return-void
.end method

.method public setEvdoEcio(I)V
    .registers 2
    .parameter "evdoEcio"

    #@0
    .prologue
    .line 275
    iput p1, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@2
    .line 276
    return-void
.end method

.method public setEvdoSnr(I)V
    .registers 2
    .parameter "evdoSnr"

    #@0
    .prologue
    .line 286
    iput p1, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@2
    .line 287
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "CellSignalStrengthCdma: cdmaDbm="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " cdmaEcio="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " evdoDbm="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " evdoEcio="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " evdoSnr="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 334
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaDbm:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 335
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mCdmaEcio:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 336
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoDbm:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 337
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoEcio:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 338
    iget v0, p0, Landroid/telephony/CellSignalStrengthCdma;->mEvdoSnr:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 339
    return-void
.end method
