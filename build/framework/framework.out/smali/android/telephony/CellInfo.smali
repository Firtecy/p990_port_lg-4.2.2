.class public abstract Landroid/telephony/CellInfo;
.super Ljava/lang/Object;
.source "CellInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final TIMESTAMP_TYPE_ANTENNA:I = 0x1

.field public static final TIMESTAMP_TYPE_JAVA_RIL:I = 0x4

.field public static final TIMESTAMP_TYPE_MODEM:I = 0x2

.field public static final TIMESTAMP_TYPE_OEM_RIL:I = 0x3

.field public static final TIMESTAMP_TYPE_UNKNOWN:I = 0x0

.field protected static final TYPE_CDMA:I = 0x2

.field protected static final TYPE_GSM:I = 0x1

.field protected static final TYPE_LTE:I = 0x3


# instance fields
.field private mRegistered:Z

.field private mTimeStamp:J

.field private mTimeStampType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 196
    new-instance v0, Landroid/telephony/CellInfo$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method protected constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 60
    iput-boolean v0, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@6
    .line 61
    iput v0, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@8
    .line 62
    const-wide v0, 0x7fffffffffffffffL

    #@d
    iput-wide v0, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@f
    .line 63
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 189
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v1

    #@8
    if-ne v1, v0, :cond_19

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@c
    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@12
    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@15
    move-result-wide v0

    #@16
    iput-wide v0, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@18
    .line 193
    return-void

    #@19
    .line 190
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_a
.end method

.method public constructor <init>(Landroid/telephony/CellInfo;)V
    .registers 4
    .parameter "ci"

    #@0
    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 67
    iget-boolean v0, p1, Landroid/telephony/CellInfo;->mRegistered:Z

    #@5
    iput-boolean v0, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@7
    .line 68
    iget v0, p1, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@9
    iput v0, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@b
    .line 69
    iget-wide v0, p1, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@d
    iput-wide v0, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@f
    .line 70
    return-void
.end method

.method private static timeStampTypeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 133
    packed-switch p0, :pswitch_data_16

    #@3
    .line 143
    const-string/jumbo v0, "unknown"

    #@6
    :goto_6
    return-object v0

    #@7
    .line 135
    :pswitch_7
    const-string v0, "antenna"

    #@9
    goto :goto_6

    #@a
    .line 137
    :pswitch_a
    const-string/jumbo v0, "modem"

    #@d
    goto :goto_6

    #@e
    .line 139
    :pswitch_e
    const-string/jumbo v0, "oem_ril"

    #@11
    goto :goto_6

    #@12
    .line 141
    :pswitch_12
    const-string/jumbo v0, "java_ril"

    #@15
    goto :goto_6

    #@16
    .line 133
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_7
        :pswitch_a
        :pswitch_e
        :pswitch_12
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 165
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 11
    .parameter "other"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 117
    if-nez p1, :cond_5

    #@4
    .line 128
    :goto_4
    return v4

    #@5
    .line 120
    :cond_5
    if-ne p0, p1, :cond_9

    #@7
    move v4, v3

    #@8
    .line 121
    goto :goto_4

    #@9
    .line 124
    :cond_9
    :try_start_9
    move-object v0, p1

    #@a
    check-cast v0, Landroid/telephony/CellInfo;

    #@c
    move-object v2, v0

    #@d
    .line 125
    .local v2, o:Landroid/telephony/CellInfo;
    iget-boolean v5, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@f
    iget-boolean v6, v2, Landroid/telephony/CellInfo;->mRegistered:Z

    #@11
    if-ne v5, v6, :cond_23

    #@13
    iget-wide v5, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@15
    iget-wide v7, v2, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@17
    cmp-long v5, v5, v7

    #@19
    if-nez v5, :cond_23

    #@1b
    iget v5, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@1d
    iget v6, v2, Landroid/telephony/CellInfo;->mTimeStampType:I
    :try_end_1f
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_1f} :catch_25

    #@1f
    if-ne v5, v6, :cond_23

    #@21
    :goto_21
    move v4, v3

    #@22
    goto :goto_4

    #@23
    :cond_23
    move v3, v4

    #@24
    goto :goto_21

    #@25
    .line 127
    .end local v2           #o:Landroid/telephony/CellInfo;
    :catch_25
    move-exception v1

    #@26
    .line 128
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_4
.end method

.method public getTimeStamp()J
    .registers 3

    #@0
    .prologue
    .line 83
    iget-wide v0, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@2
    return-wide v0
.end method

.method public getTimeStampType()I
    .registers 2

    #@0
    .prologue
    .line 97
    iget v0, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 7

    #@0
    .prologue
    .line 110
    const/16 v0, 0x1f

    #@2
    .line 111
    .local v0, primeNum:I
    iget-boolean v1, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@4
    if-eqz v1, :cond_15

    #@6
    const/4 v1, 0x0

    #@7
    :goto_7
    mul-int/2addr v1, v0

    #@8
    iget-wide v2, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@a
    const-wide/16 v4, 0x3e8

    #@c
    div-long/2addr v2, v4

    #@d
    long-to-int v2, v2

    #@e
    mul-int/2addr v2, v0

    #@f
    add-int/2addr v1, v2

    #@10
    iget v2, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@12
    mul-int/2addr v2, v0

    #@13
    add-int/2addr v1, v2

    #@14
    return v1

    #@15
    :cond_15
    const/4 v1, 0x1

    #@16
    goto :goto_7
.end method

.method public isRegistered()Z
    .registers 2

    #@0
    .prologue
    .line 74
    iget-boolean v0, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@2
    return v0
.end method

.method public setRegisterd(Z)V
    .registers 2
    .parameter "registered"

    #@0
    .prologue
    .line 78
    iput-boolean p1, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@2
    .line 79
    return-void
.end method

.method public setTimeStamp(J)V
    .registers 3
    .parameter "timeStamp"

    #@0
    .prologue
    .line 87
    iput-wide p1, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@2
    .line 88
    return-void
.end method

.method public setTimeStampType(I)V
    .registers 3
    .parameter "timeStampType"

    #@0
    .prologue
    .line 101
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x4

    #@3
    if-le p1, v0, :cond_9

    #@5
    .line 102
    :cond_5
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@8
    .line 106
    :goto_8
    return-void

    #@9
    .line 104
    :cond_9
    iput p1, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@b
    goto :goto_8
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 152
    .local v0, sb:Ljava/lang/StringBuffer;
    const-string v2, " mRegistered="

    #@7
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a
    move-result-object v3

    #@b
    iget-boolean v2, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@d
    if-eqz v2, :cond_3a

    #@f
    const-string v2, "YES"

    #@11
    :goto_11
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@14
    .line 153
    iget v2, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@16
    invoke-static {v2}, Landroid/telephony/CellInfo;->timeStampTypeToString(I)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 154
    .local v1, timeStampType:Ljava/lang/String;
    const-string v2, " mTimeStampType="

    #@1c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@23
    .line 155
    const-string v2, " mTimeStamp="

    #@25
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@28
    move-result-object v2

    #@29
    iget-wide v3, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@2b
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    #@2e
    move-result-object v2

    #@2f
    const-string/jumbo v3, "ns"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@35
    .line 157
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    return-object v2

    #@3a
    .line 152
    .end local v1           #timeStampType:Ljava/lang/String;
    :cond_3a
    const-string v2, "NO"

    #@3c
    goto :goto_11
.end method

.method public abstract writeToParcel(Landroid/os/Parcel;I)V
.end method

.method protected writeToParcel(Landroid/os/Parcel;II)V
    .registers 6
    .parameter "dest"
    .parameter "flags"
    .parameter "type"

    #@0
    .prologue
    .line 178
    invoke-virtual {p1, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@3
    .line 179
    iget-boolean v0, p0, Landroid/telephony/CellInfo;->mRegistered:Z

    #@5
    if-eqz v0, :cond_16

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 180
    iget v0, p0, Landroid/telephony/CellInfo;->mTimeStampType:I

    #@d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 181
    iget-wide v0, p0, Landroid/telephony/CellInfo;->mTimeStamp:J

    #@12
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@15
    .line 182
    return-void

    #@16
    .line 179
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_8
.end method
