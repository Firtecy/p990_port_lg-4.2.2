.class public abstract Landroid/telephony/CellSignalStrength;
.super Ljava/lang/Object;
.source "CellSignalStrength.java"


# static fields
.field public static final NUM_SIGNAL_STRENGTH_BINS:I = 0x5

.field public static final SIGNAL_STRENGTH_GOOD:I = 0x3

.field public static final SIGNAL_STRENGTH_GREAT:I = 0x4

.field public static final SIGNAL_STRENGTH_MODERATE:I = 0x2

.field public static final SIGNAL_STRENGTH_NAMES:[Ljava/lang/String; = null

.field public static final SIGNAL_STRENGTH_NONE_OR_UNKNOWN:I = 0x0

.field public static final SIGNAL_STRENGTH_POOR:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 40
    const/4 v0, 0x5

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string/jumbo v2, "none"

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    const-string/jumbo v2, "poor"

    #@d
    aput-object v2, v0, v1

    #@f
    const/4 v1, 0x2

    #@10
    const-string/jumbo v2, "moderate"

    #@13
    aput-object v2, v0, v1

    #@15
    const/4 v1, 0x3

    #@16
    const-string v2, "good"

    #@18
    aput-object v2, v0, v1

    #@1a
    const/4 v1, 0x4

    #@1b
    const-string v2, "great"

    #@1d
    aput-object v2, v0, v1

    #@1f
    sput-object v0, Landroid/telephony/CellSignalStrength;->SIGNAL_STRENGTH_NAMES:[Ljava/lang/String;

    #@21
    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    return-void
.end method


# virtual methods
.method public abstract copy()Landroid/telephony/CellSignalStrength;
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public abstract getAsuLevel()I
.end method

.method public abstract getDbm()I
.end method

.method public abstract getLevel()I
.end method

.method public abstract hashCode()I
.end method

.method public abstract setDefaultValues()V
.end method
