.class public Landroid/telephony/PhoneNumberUtils;
.super Ljava/lang/Object;
.source "PhoneNumberUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;,
        Landroid/telephony/PhoneNumberUtils$SpecialNumbers;,
        Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;
    }
.end annotation


# static fields
.field private static final BOOST:Ljava/lang/String; = "311870"

#the value of this static final field might be set in the static constructor
.field private static final CCC_LENGTH:I = 0x0

.field private static final CLIR_OFF:Ljava/lang/String; = "#31#"

.field private static final CLIR_ON:Ljava/lang/String; = "*31#"

.field private static final COUNTRY_CALLING_CALL:[Z = null

.field private static final DBG:Z = false

.field private static ENABLE_PRIVACY_LOG_CALL:Z = false

.field public static final FORMAT_JAPAN:I = 0x2

.field public static final FORMAT_KOREA:I = 0x3

.field public static final FORMAT_NANP:I = 0x1

.field public static final FORMAT_UNKNOWN:I = 0x0

.field private static final GLOBAL_PHONE_NUMBER_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final KDDI_LINECODE:Ljava/lang/String; = null

.field private static final KDDI_MAX_DIGITS_LENGTH:I = 0x20

.field private static final KDDI_MIN_DIGITS_LENGTH:I = 0x2

.field private static final KDDI_PREFIX_PLUS:Ljava/lang/String; = "010"

.field private static final KDDI_SPECIALNUMBER:[Ljava/lang/String; = null

.field private static final KEYPAD_MAP:Landroid/util/SparseIntArray; = null

.field private static final LGE_LGT_UICC:I = 0x5

.field static final LOG_TAG:Ljava/lang/String; = "PhoneNumberUtils"

.field static final MIN_MATCH:I = 0x7

.field private static final NANP_COUNTRIES:[Ljava/lang/String; = null

.field private static final NANP_IDP_STRING:Ljava/lang/String; = "011"

.field private static final NANP_LENGTH:I = 0xa

.field private static final NANP_STATE_DASH:I = 0x4

.field private static final NANP_STATE_DIGIT:I = 0x1

.field private static final NANP_STATE_ONE:I = 0x3

.field private static final NANP_STATE_PLUS:I = 0x2

.field private static final NetworkCode:Ljava/lang/String; = null

.field public static final PAUSE:C = ','

.field private static final PLUS_SIGN_CHAR:C = '+'

.field private static final PLUS_SIGN_STRING:Ljava/lang/String; = "+"

.field private static final SPRINT:Ljava/lang/String; = "310120"

.field public static final TOA_International:I = 0x91

.field public static final TOA_Unknown:I = 0x81

.field private static final VIRGIN:Ljava/lang/String; = "311490"

.field public static final WAIT:C = ';'

.field public static final WILD:C = 'N'

.field private static country_code:Ljava/lang/String;

.field private static op_code:Ljava/lang/String;

.field private static specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

.field private static specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

.field private static specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/16 v7, 0x32

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    const/16 v6, 0x39

    #@6
    const/16 v5, 0x37

    #@8
    .line 114
    const-string/jumbo v3, "persist.service.privacy.enable"

    #@b
    const-string v0, "TMO"

    #@d
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_54b

    #@13
    const-string v0, "US"

    #@15
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_54b

    #@1b
    move v0, v1

    #@1c
    :goto_1c
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@1f
    move-result v0

    #@20
    sput-boolean v0, Landroid/telephony/PhoneNumberUtils;->ENABLE_PRIVACY_LOG_CALL:Z

    #@22
    .line 120
    const-string/jumbo v0, "ro.build.target_operator"

    #@25
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->op_code:Ljava/lang/String;

    #@2b
    .line 121
    const-string/jumbo v0, "ro.build.target_country"

    #@2e
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->country_code:Ljava/lang/String;

    #@34
    .line 128
    const-string v0, "[\\+]?[0-9.-]+"

    #@36
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@39
    move-result-object v0

    #@3a
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->GLOBAL_PHONE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

    #@3c
    .line 132
    const-string/jumbo v0, "ro.cdma.home.operator.numeric"

    #@3f
    const-string v3, "310000"

    #@41
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->NetworkCode:Ljava/lang/String;

    #@47
    .line 1504
    const/16 v0, 0x18

    #@49
    new-array v0, v0, [Ljava/lang/String;

    #@4b
    const-string v3, "US"

    #@4d
    aput-object v3, v0, v1

    #@4f
    const-string v3, "CA"

    #@51
    aput-object v3, v0, v2

    #@53
    const/4 v3, 0x2

    #@54
    const-string v4, "AS"

    #@56
    aput-object v4, v0, v3

    #@58
    const/4 v3, 0x3

    #@59
    const-string v4, "AI"

    #@5b
    aput-object v4, v0, v3

    #@5d
    const/4 v3, 0x4

    #@5e
    const-string v4, "AG"

    #@60
    aput-object v4, v0, v3

    #@62
    const/4 v3, 0x5

    #@63
    const-string v4, "BS"

    #@65
    aput-object v4, v0, v3

    #@67
    const/4 v3, 0x6

    #@68
    const-string v4, "BB"

    #@6a
    aput-object v4, v0, v3

    #@6c
    const/4 v3, 0x7

    #@6d
    const-string v4, "BM"

    #@6f
    aput-object v4, v0, v3

    #@71
    const/16 v3, 0x8

    #@73
    const-string v4, "VG"

    #@75
    aput-object v4, v0, v3

    #@77
    const/16 v3, 0x9

    #@79
    const-string v4, "KY"

    #@7b
    aput-object v4, v0, v3

    #@7d
    const/16 v3, 0xa

    #@7f
    const-string v4, "DM"

    #@81
    aput-object v4, v0, v3

    #@83
    const/16 v3, 0xb

    #@85
    const-string v4, "DO"

    #@87
    aput-object v4, v0, v3

    #@89
    const/16 v3, 0xc

    #@8b
    const-string v4, "GD"

    #@8d
    aput-object v4, v0, v3

    #@8f
    const/16 v3, 0xd

    #@91
    const-string v4, "GU"

    #@93
    aput-object v4, v0, v3

    #@95
    const/16 v3, 0xe

    #@97
    const-string v4, "JM"

    #@99
    aput-object v4, v0, v3

    #@9b
    const/16 v3, 0xf

    #@9d
    const-string v4, "PR"

    #@9f
    aput-object v4, v0, v3

    #@a1
    const/16 v3, 0x10

    #@a3
    const-string v4, "MS"

    #@a5
    aput-object v4, v0, v3

    #@a7
    const/16 v3, 0x11

    #@a9
    const-string v4, "MP"

    #@ab
    aput-object v4, v0, v3

    #@ad
    const/16 v3, 0x12

    #@af
    const-string v4, "KN"

    #@b1
    aput-object v4, v0, v3

    #@b3
    const/16 v3, 0x13

    #@b5
    const-string v4, "LC"

    #@b7
    aput-object v4, v0, v3

    #@b9
    const/16 v3, 0x14

    #@bb
    const-string v4, "VC"

    #@bd
    aput-object v4, v0, v3

    #@bf
    const/16 v3, 0x15

    #@c1
    const-string v4, "TT"

    #@c3
    aput-object v4, v0, v3

    #@c5
    const/16 v3, 0x16

    #@c7
    const-string v4, "TC"

    #@c9
    aput-object v4, v0, v3

    #@cb
    const/16 v3, 0x17

    #@cd
    const-string v4, "VI"

    #@cf
    aput-object v4, v0, v3

    #@d1
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->NANP_COUNTRIES:[Ljava/lang/String;

    #@d3
    .line 2587
    const/4 v0, 0x2

    #@d4
    new-array v0, v0, [Ljava/lang/String;

    #@d6
    const-string v3, "184"

    #@d8
    aput-object v3, v0, v1

    #@da
    const-string v3, "186"

    #@dc
    aput-object v3, v0, v2

    #@de
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->KDDI_SPECIALNUMBER:[Ljava/lang/String;

    #@e0
    .line 2977
    new-instance v0, Landroid/util/SparseIntArray;

    #@e2
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@e5
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@e7
    .line 2979
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@e9
    const/16 v3, 0x61

    #@eb
    invoke-virtual {v0, v3, v7}, Landroid/util/SparseIntArray;->put(II)V

    #@ee
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@f0
    const/16 v3, 0x62

    #@f2
    invoke-virtual {v0, v3, v7}, Landroid/util/SparseIntArray;->put(II)V

    #@f5
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@f7
    const/16 v3, 0x63

    #@f9
    invoke-virtual {v0, v3, v7}, Landroid/util/SparseIntArray;->put(II)V

    #@fc
    .line 2980
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@fe
    const/16 v3, 0x41

    #@100
    invoke-virtual {v0, v3, v7}, Landroid/util/SparseIntArray;->put(II)V

    #@103
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@105
    const/16 v3, 0x42

    #@107
    invoke-virtual {v0, v3, v7}, Landroid/util/SparseIntArray;->put(II)V

    #@10a
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@10c
    const/16 v3, 0x43

    #@10e
    invoke-virtual {v0, v3, v7}, Landroid/util/SparseIntArray;->put(II)V

    #@111
    .line 2982
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@113
    const/16 v3, 0x64

    #@115
    const/16 v4, 0x33

    #@117
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@11a
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@11c
    const/16 v3, 0x65

    #@11e
    const/16 v4, 0x33

    #@120
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@123
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@125
    const/16 v3, 0x66

    #@127
    const/16 v4, 0x33

    #@129
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@12c
    .line 2983
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@12e
    const/16 v3, 0x44

    #@130
    const/16 v4, 0x33

    #@132
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@135
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@137
    const/16 v3, 0x45

    #@139
    const/16 v4, 0x33

    #@13b
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@13e
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@140
    const/16 v3, 0x46

    #@142
    const/16 v4, 0x33

    #@144
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@147
    .line 2985
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@149
    const/16 v3, 0x67

    #@14b
    const/16 v4, 0x34

    #@14d
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@150
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@152
    const/16 v3, 0x68

    #@154
    const/16 v4, 0x34

    #@156
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@159
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@15b
    const/16 v3, 0x69

    #@15d
    const/16 v4, 0x34

    #@15f
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@162
    .line 2986
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@164
    const/16 v3, 0x47

    #@166
    const/16 v4, 0x34

    #@168
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@16b
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@16d
    const/16 v3, 0x48

    #@16f
    const/16 v4, 0x34

    #@171
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@174
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@176
    const/16 v3, 0x49

    #@178
    const/16 v4, 0x34

    #@17a
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@17d
    .line 2988
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@17f
    const/16 v3, 0x6a

    #@181
    const/16 v4, 0x35

    #@183
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@186
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@188
    const/16 v3, 0x6b

    #@18a
    const/16 v4, 0x35

    #@18c
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@18f
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@191
    const/16 v3, 0x6c

    #@193
    const/16 v4, 0x35

    #@195
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@198
    .line 2989
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@19a
    const/16 v3, 0x4a

    #@19c
    const/16 v4, 0x35

    #@19e
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1a1
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1a3
    const/16 v3, 0x4b

    #@1a5
    const/16 v4, 0x35

    #@1a7
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1aa
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1ac
    const/16 v3, 0x4c

    #@1ae
    const/16 v4, 0x35

    #@1b0
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1b3
    .line 2991
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1b5
    const/16 v3, 0x6d

    #@1b7
    const/16 v4, 0x36

    #@1b9
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1bc
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1be
    const/16 v3, 0x6e

    #@1c0
    const/16 v4, 0x36

    #@1c2
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1c5
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1c7
    const/16 v3, 0x6f

    #@1c9
    const/16 v4, 0x36

    #@1cb
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1ce
    .line 2992
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1d0
    const/16 v3, 0x4d

    #@1d2
    const/16 v4, 0x36

    #@1d4
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1d7
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1d9
    const/16 v3, 0x4e

    #@1db
    const/16 v4, 0x36

    #@1dd
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1e0
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1e2
    const/16 v3, 0x4f

    #@1e4
    const/16 v4, 0x36

    #@1e6
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@1e9
    .line 2994
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1eb
    const/16 v3, 0x70

    #@1ed
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@1f0
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1f2
    const/16 v3, 0x71

    #@1f4
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@1f7
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@1f9
    const/16 v3, 0x72

    #@1fb
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@1fe
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@200
    const/16 v3, 0x73

    #@202
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@205
    .line 2995
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@207
    const/16 v3, 0x50

    #@209
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@20c
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@20e
    const/16 v3, 0x51

    #@210
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@213
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@215
    const/16 v3, 0x52

    #@217
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@21a
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@21c
    const/16 v3, 0x53

    #@21e
    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    #@221
    .line 2997
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@223
    const/16 v3, 0x74

    #@225
    const/16 v4, 0x38

    #@227
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@22a
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@22c
    const/16 v3, 0x75

    #@22e
    const/16 v4, 0x38

    #@230
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@233
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@235
    const/16 v3, 0x76

    #@237
    const/16 v4, 0x38

    #@239
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@23c
    .line 2998
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@23e
    const/16 v3, 0x54

    #@240
    const/16 v4, 0x38

    #@242
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@245
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@247
    const/16 v3, 0x55

    #@249
    const/16 v4, 0x38

    #@24b
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@24e
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@250
    const/16 v3, 0x56

    #@252
    const/16 v4, 0x38

    #@254
    invoke-virtual {v0, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@257
    .line 3000
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@259
    const/16 v3, 0x77

    #@25b
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@25e
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@260
    const/16 v3, 0x78

    #@262
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@265
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@267
    const/16 v3, 0x79

    #@269
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@26c
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@26e
    const/16 v3, 0x7a

    #@270
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@273
    .line 3001
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@275
    const/16 v3, 0x57

    #@277
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@27a
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@27c
    const/16 v3, 0x58

    #@27e
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@281
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@283
    const/16 v3, 0x59

    #@285
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@288
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@28a
    const/16 v3, 0x5a

    #@28c
    invoke-virtual {v0, v3, v6}, Landroid/util/SparseIntArray;->put(II)V

    #@28f
    .line 3666
    const/16 v0, 0x64

    #@291
    new-array v0, v0, [Z

    #@293
    fill-array-data v0, :array_54e

    #@296
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->COUNTRY_CALLING_CALL:[Z

    #@298
    .line 3678
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->COUNTRY_CALLING_CALL:[Z

    #@29a
    array-length v0, v0

    #@29b
    sput v0, Landroid/telephony/PhoneNumberUtils;->CCC_LENGTH:I

    #@29d
    .line 3875
    const/16 v0, 0xa

    #@29f
    new-array v0, v0, [Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2a1
    new-instance v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2a3
    const-string v4, "*2"

    #@2a5
    const v5, 0x20902d9

    #@2a8
    invoke-direct {v3, v4, v5}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2ab
    aput-object v3, v0, v1

    #@2ad
    new-instance v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2af
    const-string v4, "*4"

    #@2b1
    const v5, 0x20902da

    #@2b4
    invoke-direct {v3, v4, v5}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2b7
    aput-object v3, v0, v2

    #@2b9
    const/4 v3, 0x2

    #@2ba
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2bc
    const-string v5, "0"

    #@2be
    const v6, 0x20902db

    #@2c1
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2c4
    aput-object v4, v0, v3

    #@2c6
    const/4 v3, 0x3

    #@2c7
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2c9
    const-string v5, "211"

    #@2cb
    const v6, 0x209015e

    #@2ce
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2d1
    aput-object v4, v0, v3

    #@2d3
    const/4 v3, 0x4

    #@2d4
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2d6
    const-string v5, "311"

    #@2d8
    const v6, 0x209015f

    #@2db
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2de
    aput-object v4, v0, v3

    #@2e0
    const/4 v3, 0x5

    #@2e1
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2e3
    const-string v5, "411"

    #@2e5
    const v6, 0x2090160

    #@2e8
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2eb
    aput-object v4, v0, v3

    #@2ed
    const/4 v3, 0x6

    #@2ee
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2f0
    const-string v5, "511"

    #@2f2
    const v6, 0x2090161

    #@2f5
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@2f8
    aput-object v4, v0, v3

    #@2fa
    const/4 v3, 0x7

    #@2fb
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2fd
    const-string v5, "611"

    #@2ff
    const v6, 0x2090162

    #@302
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@305
    aput-object v4, v0, v3

    #@307
    const/16 v3, 0x8

    #@309
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@30b
    const-string v5, "711"

    #@30d
    const v6, 0x2090163

    #@310
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@313
    aput-object v4, v0, v3

    #@315
    const/16 v3, 0x9

    #@317
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@319
    const-string v5, "811"

    #@31b
    const v6, 0x2090164

    #@31e
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@321
    aput-object v4, v0, v3

    #@323
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@325
    .line 3889
    const/4 v0, 0x7

    #@326
    new-array v0, v0, [Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@328
    new-instance v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@32a
    const-string v4, "211"

    #@32c
    const v5, 0x209015e

    #@32f
    invoke-direct {v3, v4, v5}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@332
    aput-object v3, v0, v1

    #@334
    new-instance v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@336
    const-string v4, "311"

    #@338
    const v5, 0x209015f

    #@33b
    invoke-direct {v3, v4, v5}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@33e
    aput-object v3, v0, v2

    #@340
    const/4 v3, 0x2

    #@341
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@343
    const-string v5, "411"

    #@345
    const v6, 0x2090160

    #@348
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@34b
    aput-object v4, v0, v3

    #@34d
    const/4 v3, 0x3

    #@34e
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@350
    const-string v5, "511"

    #@352
    const v6, 0x2090161

    #@355
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@358
    aput-object v4, v0, v3

    #@35a
    const/4 v3, 0x4

    #@35b
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@35d
    const-string v5, "611"

    #@35f
    const v6, 0x2090162

    #@362
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@365
    aput-object v4, v0, v3

    #@367
    const/4 v3, 0x5

    #@368
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@36a
    const-string v5, "711"

    #@36c
    const v6, 0x2090163

    #@36f
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@372
    aput-object v4, v0, v3

    #@374
    const/4 v3, 0x6

    #@375
    new-instance v4, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@377
    const-string v5, "811"

    #@379
    const v6, 0x2090164

    #@37c
    invoke-direct {v4, v5, v6}, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;-><init>(Ljava/lang/String;I)V

    #@37f
    aput-object v4, v0, v3

    #@381
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@383
    .line 3909
    const/16 v0, 0x12

    #@385
    new-array v0, v0, [Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@387
    new-instance v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@389
    const-string v4, "adc.n11.first.number"

    #@38b
    const-string v5, ""

    #@38d
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@390
    move-result-object v4

    #@391
    const-string v5, "adc.n11.first.name"

    #@393
    const-string v6, ""

    #@395
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@398
    move-result-object v5

    #@399
    invoke-direct {v3, v4, v5}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@39c
    aput-object v3, v0, v1

    #@39e
    new-instance v1, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@3a0
    const-string v3, "adc.n11.second.number"

    #@3a2
    const-string v4, ""

    #@3a4
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3a7
    move-result-object v3

    #@3a8
    const-string v4, "adc.n11.second.name"

    #@3aa
    const-string v5, ""

    #@3ac
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3af
    move-result-object v4

    #@3b0
    invoke-direct {v1, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3b3
    aput-object v1, v0, v2

    #@3b5
    const/4 v1, 0x2

    #@3b6
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@3b8
    const-string v3, "adc.n11.third.number"

    #@3ba
    const-string v4, ""

    #@3bc
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3bf
    move-result-object v3

    #@3c0
    const-string v4, "adc.n11.third.name"

    #@3c2
    const-string v5, ""

    #@3c4
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3c7
    move-result-object v4

    #@3c8
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3cb
    aput-object v2, v0, v1

    #@3cd
    const/4 v1, 0x3

    #@3ce
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@3d0
    const-string v3, "adc.n11.forth.number"

    #@3d2
    const-string v4, ""

    #@3d4
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3d7
    move-result-object v3

    #@3d8
    const-string v4, "adc.n11.forth.name"

    #@3da
    const-string v5, ""

    #@3dc
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3df
    move-result-object v4

    #@3e0
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3e3
    aput-object v2, v0, v1

    #@3e5
    const/4 v1, 0x4

    #@3e6
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@3e8
    const-string v3, "adc.n11.fifth.number"

    #@3ea
    const-string v4, ""

    #@3ec
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3ef
    move-result-object v3

    #@3f0
    const-string v4, "adc.n11.fifth.name"

    #@3f2
    const-string v5, ""

    #@3f4
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3f7
    move-result-object v4

    #@3f8
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3fb
    aput-object v2, v0, v1

    #@3fd
    const/4 v1, 0x5

    #@3fe
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@400
    const-string v3, "adc.n11.sixth.number"

    #@402
    const-string v4, ""

    #@404
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@407
    move-result-object v3

    #@408
    const-string v4, "adc.n11.sixth.name"

    #@40a
    const-string v5, ""

    #@40c
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@40f
    move-result-object v4

    #@410
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@413
    aput-object v2, v0, v1

    #@415
    const/4 v1, 0x6

    #@416
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@418
    const-string v3, "adc.n11.seventh.number"

    #@41a
    const-string v4, ""

    #@41c
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@41f
    move-result-object v3

    #@420
    const-string v4, "adc.n11.seventh.name"

    #@422
    const-string v5, ""

    #@424
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@427
    move-result-object v4

    #@428
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@42b
    aput-object v2, v0, v1

    #@42d
    const/4 v1, 0x7

    #@42e
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@430
    const-string v3, "adc.n11.eighth.number"

    #@432
    const-string v4, ""

    #@434
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@437
    move-result-object v3

    #@438
    const-string v4, "adc.n11.eighth.name"

    #@43a
    const-string v5, ""

    #@43c
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@43f
    move-result-object v4

    #@440
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@443
    aput-object v2, v0, v1

    #@445
    const/16 v1, 0x8

    #@447
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@449
    const-string v3, "adc.n11.ninth.number"

    #@44b
    const-string v4, ""

    #@44d
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@450
    move-result-object v3

    #@451
    const-string v4, "adc.n11.ninth.name"

    #@453
    const-string v5, ""

    #@455
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@458
    move-result-object v4

    #@459
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@45c
    aput-object v2, v0, v1

    #@45e
    const/16 v1, 0x9

    #@460
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@462
    const-string v3, "adc.n11.tenth.number"

    #@464
    const-string v4, ""

    #@466
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@469
    move-result-object v3

    #@46a
    const-string v4, "adc.n11.tenth.name"

    #@46c
    const-string v5, ""

    #@46e
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@471
    move-result-object v4

    #@472
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@475
    aput-object v2, v0, v1

    #@477
    const/16 v1, 0xa

    #@479
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@47b
    const-string v3, "adc.n11.elevnth.number"

    #@47d
    const-string v4, ""

    #@47f
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@482
    move-result-object v3

    #@483
    const-string v4, "adc.n11.elevnth.name"

    #@485
    const-string v5, ""

    #@487
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48a
    move-result-object v4

    #@48b
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@48e
    aput-object v2, v0, v1

    #@490
    const/16 v1, 0xb

    #@492
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@494
    const-string v3, "adc.n11.twelfth.number"

    #@496
    const-string v4, ""

    #@498
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@49b
    move-result-object v3

    #@49c
    const-string v4, "adc.n11.twelfth.name"

    #@49e
    const-string v5, ""

    #@4a0
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4a3
    move-result-object v4

    #@4a4
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4a7
    aput-object v2, v0, v1

    #@4a9
    const/16 v1, 0xc

    #@4ab
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@4ad
    const-string v3, "adc.n11.thirteenth.number"

    #@4af
    const-string v4, ""

    #@4b1
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4b4
    move-result-object v3

    #@4b5
    const-string v4, "adc.n11.thirteenth.name"

    #@4b7
    const-string v5, ""

    #@4b9
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4bc
    move-result-object v4

    #@4bd
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4c0
    aput-object v2, v0, v1

    #@4c2
    const/16 v1, 0xd

    #@4c4
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@4c6
    const-string v3, "adc.n11.fourteenth.number"

    #@4c8
    const-string v4, ""

    #@4ca
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4cd
    move-result-object v3

    #@4ce
    const-string v4, "adc.n11.fourteenth.name"

    #@4d0
    const-string v5, ""

    #@4d2
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4d5
    move-result-object v4

    #@4d6
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4d9
    aput-object v2, v0, v1

    #@4db
    const/16 v1, 0xe

    #@4dd
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@4df
    const-string v3, "adc.n11.fifteenth.number"

    #@4e1
    const-string v4, ""

    #@4e3
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4e6
    move-result-object v3

    #@4e7
    const-string v4, "adc.n11.fifteenth.name"

    #@4e9
    const-string v5, ""

    #@4eb
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4ee
    move-result-object v4

    #@4ef
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4f2
    aput-object v2, v0, v1

    #@4f4
    const/16 v1, 0xf

    #@4f6
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@4f8
    const-string v3, "adc.n11.sixteenth.number"

    #@4fa
    const-string v4, ""

    #@4fc
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4ff
    move-result-object v3

    #@500
    const-string v4, "adc.n11.sixteenth.name"

    #@502
    const-string v5, ""

    #@504
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@507
    move-result-object v4

    #@508
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@50b
    aput-object v2, v0, v1

    #@50d
    const/16 v1, 0x10

    #@50f
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@511
    const-string v3, "adc.n11.seventeenth.number"

    #@513
    const-string v4, ""

    #@515
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@518
    move-result-object v3

    #@519
    const-string v4, "adc.n11.seventeenth.name"

    #@51b
    const-string v5, ""

    #@51d
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@520
    move-result-object v4

    #@521
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@524
    aput-object v2, v0, v1

    #@526
    const/16 v1, 0x11

    #@528
    new-instance v2, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@52a
    const-string v3, "adc.n11.eighteenth.number"

    #@52c
    const-string v4, ""

    #@52e
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@531
    move-result-object v3

    #@532
    const-string v4, "adc.n11.eighteenth.name"

    #@534
    const-string v5, ""

    #@536
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@539
    move-result-object v4

    #@53a
    invoke-direct {v2, v3, v4}, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@53d
    aput-object v2, v0, v1

    #@53f
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@541
    .line 4038
    const-string/jumbo v0, "line.separator"

    #@544
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@547
    move-result-object v0

    #@548
    sput-object v0, Landroid/telephony/PhoneNumberUtils;->KDDI_LINECODE:Ljava/lang/String;

    #@54a
    return-void

    #@54b
    :cond_54b
    move v0, v2

    #@54c
    .line 114
    goto/16 :goto_1c

    #@54e
    .line 3666
    :array_54e
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3899
    return-void
.end method

.method public static KRSMSextractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 326
    if-nez p0, :cond_4

    #@2
    .line 327
    const/4 v5, 0x0

    #@3
    .line 344
    :goto_3
    return-object v5

    #@4
    .line 330
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v3

    #@8
    .line 331
    .local v3, len:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 332
    .local v4, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@e
    .line 334
    .local v1, firstCharAdded:Z
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v3, :cond_2e

    #@11
    .line 335
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v0

    #@15
    .line 336
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isKRSMSDialable(C)Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_28

    #@1b
    const/16 v5, 0x2b

    #@1d
    if-ne v0, v5, :cond_21

    #@1f
    if-nez v1, :cond_28

    #@21
    .line 337
    :cond_21
    const/4 v1, 0x1

    #@22
    .line 338
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    .line 334
    :cond_25
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_f

    #@28
    .line 339
    :cond_28
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isStartsPostDial(C)Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_25

    #@2e
    .line 344
    .end local v0           #c:C
    :cond_2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    goto :goto_3
.end method

.method private static KRsmsbcdToChar(B)C
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 1257
    const/16 v0, 0xa

    #@2
    if-ge p0, v0, :cond_8

    #@4
    .line 1258
    add-int/lit8 v0, p0, 0x30

    #@6
    int-to-char v0, v0

    #@7
    .line 1267
    :goto_7
    return v0

    #@8
    .line 1260
    :cond_8
    packed-switch p0, :pswitch_data_1c

    #@b
    .line 1267
    const/4 v0, 0x0

    #@c
    goto :goto_7

    #@d
    .line 1261
    :pswitch_d
    const/16 v0, 0x2a

    #@f
    goto :goto_7

    #@10
    .line 1262
    :pswitch_10
    const/16 v0, 0x23

    #@12
    goto :goto_7

    #@13
    .line 1263
    :pswitch_13
    const/16 v0, 0x61

    #@15
    goto :goto_7

    #@16
    .line 1264
    :pswitch_16
    const/16 v0, 0x62

    #@18
    goto :goto_7

    #@19
    .line 1265
    :pswitch_19
    const/16 v0, 0x63

    #@1b
    goto :goto_7

    #@1c
    .line 1260
    :pswitch_data_1c
    .packed-switch 0xa
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_16
        :pswitch_19
    .end packed-switch
.end method

.method public static KRsmscalledPartyBCDToString([BII)Ljava/lang/String;
    .registers 8
    .parameter "bytes"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 1093
    const/4 v0, 0x0

    #@1
    .line 1094
    .local v0, prependPlus:Z
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    mul-int/lit8 v3, p2, 0x2

    #@5
    add-int/lit8 v3, v3, 0x1

    #@7
    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@a
    .line 1096
    .local v1, ret:Ljava/lang/StringBuilder;
    const/4 v3, 0x2

    #@b
    if-ge p2, v3, :cond_10

    #@d
    .line 1097
    const-string v3, ""

    #@f
    .line 1119
    :goto_f
    return-object v3

    #@10
    .line 1100
    :cond_10
    aget-byte v3, p0, p1

    #@12
    and-int/lit16 v3, v3, 0xff

    #@14
    const/16 v4, 0x91

    #@16
    if-ne v3, v4, :cond_19

    #@18
    .line 1101
    const/4 v0, 0x1

    #@19
    .line 1104
    :cond_19
    add-int/lit8 v3, p1, 0x1

    #@1b
    add-int/lit8 v4, p2, -0x1

    #@1d
    invoke-static {v1, p0, v3, v4}, Landroid/telephony/PhoneNumberUtils;->KRsmsinternalCalledPartyBCDFragmentToString(Ljava/lang/StringBuilder;[BII)V

    #@20
    .line 1107
    if-eqz v0, :cond_2b

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_2b

    #@28
    .line 1109
    const-string v3, ""

    #@2a
    goto :goto_f

    #@2b
    .line 1112
    :cond_2b
    if-eqz v0, :cond_3e

    #@2d
    .line 1113
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 1115
    .local v2, retString:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@33
    .end local v1           #ret:Ljava/lang/StringBuilder;
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    .line 1116
    .restart local v1       #ret:Ljava/lang/StringBuilder;
    const/16 v3, 0x2b

    #@38
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3b
    .line 1117
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 1119
    .end local v2           #retString:Ljava/lang/String;
    :cond_3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    goto :goto_f
.end method

.method private static KRsmscharToBCD(C)I
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 1276
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    .line 1277
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 1288
    :goto_a
    return v0

    #@b
    .line 1279
    :cond_b
    const/16 v0, 0x2a

    #@d
    if-ne p0, v0, :cond_12

    #@f
    .line 1280
    const/16 v0, 0xa

    #@11
    goto :goto_a

    #@12
    .line 1281
    :cond_12
    const/16 v0, 0x23

    #@14
    if-ne p0, v0, :cond_19

    #@16
    .line 1282
    const/16 v0, 0xb

    #@18
    goto :goto_a

    #@19
    .line 1283
    :cond_19
    const/16 v0, 0x61

    #@1b
    if-ne p0, v0, :cond_20

    #@1d
    .line 1284
    const/16 v0, 0xc

    #@1f
    goto :goto_a

    #@20
    .line 1285
    :cond_20
    const/16 v0, 0x62

    #@22
    if-ne p0, v0, :cond_27

    #@24
    .line 1286
    const/16 v0, 0xd

    #@26
    goto :goto_a

    #@27
    .line 1287
    :cond_27
    const/16 v0, 0x63

    #@29
    if-ne p0, v0, :cond_2e

    #@2b
    .line 1288
    const/16 v0, 0xe

    #@2d
    goto :goto_a

    #@2e
    .line 1290
    :cond_2e
    new-instance v0, Ljava/lang/RuntimeException;

    #@30
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v2, "invalid char for BCD "

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@46
    throw v0
.end method

.method private static KRsmsinternalCalledPartyBCDFragmentToString(Ljava/lang/StringBuilder;[BII)V
    .registers 9
    .parameter "sb"
    .parameter "bytes"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 1129
    move v2, p2

    #@1
    .local v2, i:I
    :goto_1
    add-int v3, p3, p2

    #@3
    if-ge v2, v3, :cond_10

    #@5
    .line 1133
    aget-byte v3, p1, v2

    #@7
    and-int/lit8 v3, v3, 0xf

    #@9
    int-to-byte v3, v3

    #@a
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->KRsmsbcdToChar(B)C

    #@d
    move-result v1

    #@e
    .line 1135
    .local v1, c:C
    if-nez v1, :cond_11

    #@10
    .line 1162
    .end local v1           #c:C
    :cond_10
    return-void

    #@11
    .line 1138
    .restart local v1       #c:C
    :cond_11
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 1147
    aget-byte v3, p1, v2

    #@16
    shr-int/lit8 v3, v3, 0x4

    #@18
    and-int/lit8 v3, v3, 0xf

    #@1a
    int-to-byte v0, v3

    #@1b
    .line 1149
    .local v0, b:B
    const/16 v3, 0xf

    #@1d
    if-ne v0, v3, :cond_25

    #@1f
    add-int/lit8 v3, v2, 0x1

    #@21
    add-int v4, p3, p2

    #@23
    if-eq v3, v4, :cond_10

    #@25
    .line 1154
    :cond_25
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->KRsmsbcdToChar(B)C

    #@28
    move-result v1

    #@29
    .line 1155
    if-eqz v1, :cond_10

    #@2b
    .line 1159
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    .line 1129
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1
.end method

.method public static KRsmsnetworkPortionToCalledPartyBCD(Ljava/lang/String;)[B
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 1304
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->KRSMSextractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->KRsmsnumberToCalledPartyBCD(Ljava/lang/String;)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static KRsmsnumberToCalledPartyBCD(Ljava/lang/String;)[B
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 1317
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/telephony/PhoneNumberUtils;->KRsmsnumberToCalledPartyBCDHelper(Ljava/lang/String;Z)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static KRsmsnumberToCalledPartyBCDHelper(Ljava/lang/String;Z)[B
    .registers 17
    .parameter "number"
    .parameter "includeLength"

    #@0
    .prologue
    .line 1321
    if-nez p0, :cond_b

    #@2
    .line 1322
    const-string v12, "PhoneNumberUtils"

    #@4
    const-string v13, "KRsmsnumberToCalledPartyBCDHelper : number is null"

    #@6
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1323
    const/4 v9, 0x0

    #@a
    .line 1356
    :goto_a
    return-object v9

    #@b
    .line 1326
    :cond_b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@e
    move-result v6

    #@f
    .line 1327
    .local v6, numberLenReal:I
    move v5, v6

    #@10
    .line 1328
    .local v5, numberLenEffective:I
    const/16 v12, 0x2b

    #@12
    invoke-virtual {p0, v12}, Ljava/lang/String;->indexOf(I)I

    #@15
    move-result v12

    #@16
    const/4 v13, -0x1

    #@17
    if-eq v12, v13, :cond_22

    #@19
    const/4 v3, 0x1

    #@1a
    .line 1329
    .local v3, hasPlus:Z
    :goto_1a
    if-eqz v3, :cond_1e

    #@1c
    add-int/lit8 v5, v5, -0x1

    #@1e
    .line 1331
    :cond_1e
    if-nez v5, :cond_24

    #@20
    const/4 v9, 0x0

    #@21
    goto :goto_a

    #@22
    .line 1328
    .end local v3           #hasPlus:Z
    :cond_22
    const/4 v3, 0x0

    #@23
    goto :goto_1a

    #@24
    .line 1333
    .restart local v3       #hasPlus:Z
    :cond_24
    add-int/lit8 v12, v5, 0x1

    #@26
    div-int/lit8 v10, v12, 0x2

    #@28
    .line 1334
    .local v10, resultLen:I
    const/4 v2, 0x1

    #@29
    .line 1335
    .local v2, extraBytes:I
    if-eqz p1, :cond_2d

    #@2b
    add-int/lit8 v2, v2, 0x1

    #@2d
    .line 1336
    :cond_2d
    add-int/2addr v10, v2

    #@2e
    .line 1338
    new-array v9, v10, [B

    #@30
    .line 1340
    .local v9, result:[B
    const/4 v1, 0x0

    #@31
    .line 1341
    .local v1, digitCount:I
    const/4 v4, 0x0

    #@32
    .local v4, i:I
    :goto_32
    if-ge v4, v6, :cond_5b

    #@34
    .line 1342
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@37
    move-result v0

    #@38
    .line 1343
    .local v0, c:C
    const/16 v12, 0x2b

    #@3a
    if-ne v0, v12, :cond_3f

    #@3c
    .line 1341
    :goto_3c
    add-int/lit8 v4, v4, 0x1

    #@3e
    goto :goto_32

    #@3f
    .line 1344
    :cond_3f
    and-int/lit8 v12, v1, 0x1

    #@41
    const/4 v13, 0x1

    #@42
    if-ne v12, v13, :cond_59

    #@44
    const/4 v11, 0x4

    #@45
    .line 1345
    .local v11, shift:I
    :goto_45
    shr-int/lit8 v12, v1, 0x1

    #@47
    add-int/2addr v12, v2

    #@48
    aget-byte v13, v9, v12

    #@4a
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->KRsmscharToBCD(C)I

    #@4d
    move-result v14

    #@4e
    and-int/lit8 v14, v14, 0xf

    #@50
    shl-int/2addr v14, v11

    #@51
    int-to-byte v14, v14

    #@52
    or-int/2addr v13, v14

    #@53
    int-to-byte v13, v13

    #@54
    aput-byte v13, v9, v12

    #@56
    .line 1346
    add-int/lit8 v1, v1, 0x1

    #@58
    goto :goto_3c

    #@59
    .line 1344
    .end local v11           #shift:I
    :cond_59
    const/4 v11, 0x0

    #@5a
    goto :goto_45

    #@5b
    .line 1350
    .end local v0           #c:C
    :cond_5b
    and-int/lit8 v12, v1, 0x1

    #@5d
    const/4 v13, 0x1

    #@5e
    if-ne v12, v13, :cond_6a

    #@60
    shr-int/lit8 v12, v1, 0x1

    #@62
    add-int/2addr v12, v2

    #@63
    aget-byte v13, v9, v12

    #@65
    or-int/lit16 v13, v13, 0xf0

    #@67
    int-to-byte v13, v13

    #@68
    aput-byte v13, v9, v12

    #@6a
    .line 1352
    :cond_6a
    const/4 v7, 0x0

    #@6b
    .line 1353
    .local v7, offset:I
    if-eqz p1, :cond_75

    #@6d
    add-int/lit8 v8, v7, 0x1

    #@6f
    .end local v7           #offset:I
    .local v8, offset:I
    add-int/lit8 v12, v10, -0x1

    #@71
    int-to-byte v12, v12

    #@72
    aput-byte v12, v9, v7

    #@74
    move v7, v8

    #@75
    .line 1354
    .end local v8           #offset:I
    .restart local v7       #offset:I
    :cond_75
    if-eqz v3, :cond_7d

    #@77
    const/16 v12, 0x91

    #@79
    :goto_79
    int-to-byte v12, v12

    #@7a
    aput-byte v12, v9, v7

    #@7c
    goto :goto_a

    #@7d
    :cond_7d
    const/16 v12, 0x81

    #@7f
    goto :goto_79
.end method

.method private static appendPwCharBackToOrigDialStr(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "dialableIndex"
    .parameter "origStr"
    .parameter "dialStr"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3323
    const/4 v3, 0x1

    #@2
    if-ne p0, v3, :cond_16

    #@4
    .line 3324
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 3325
    .local v1, ret:Ljava/lang/StringBuilder;
    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v3

    #@d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    .line 3326
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    .line 3333
    .end local v1           #ret:Ljava/lang/StringBuilder;
    .local v2, retStr:Ljava/lang/String;
    :goto_15
    return-object v2

    #@16
    .line 3330
    .end local v2           #retStr:Ljava/lang/String;
    :cond_16
    invoke-virtual {p2, v4, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 3331
    .local v0, nonDigitStr:Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .restart local v2       #retStr:Ljava/lang/String;
    goto :goto_15
.end method

.method private static bcdToChar(B)C
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 1220
    const/16 v0, 0xa

    #@2
    if-ge p0, v0, :cond_8

    #@4
    .line 1221
    add-int/lit8 v0, p0, 0x30

    #@6
    int-to-char v0, v0

    #@7
    .line 1228
    :goto_7
    return v0

    #@8
    .line 1222
    :cond_8
    packed-switch p0, :pswitch_data_1a

    #@b
    .line 1228
    const/4 v0, 0x0

    #@c
    goto :goto_7

    #@d
    .line 1223
    :pswitch_d
    const/16 v0, 0x2a

    #@f
    goto :goto_7

    #@10
    .line 1224
    :pswitch_10
    const/16 v0, 0x23

    #@12
    goto :goto_7

    #@13
    .line 1225
    :pswitch_13
    const/16 v0, 0x2c

    #@15
    goto :goto_7

    #@16
    .line 1226
    :pswitch_16
    const/16 v0, 0x4e

    #@18
    goto :goto_7

    #@19
    .line 1222
    nop

    #@1a
    :pswitch_data_1a
    .packed-switch 0xa
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_16
    .end packed-switch
.end method

.method public static calledPartyBCDFragmentToString([BII)Ljava/lang/String;
    .registers 5
    .parameter "bytes"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 1210
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    mul-int/lit8 v1, p2, 0x2

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 1212
    .local v0, ret:Ljava/lang/StringBuilder;
    invoke-static {v0, p0, p1, p2}, Landroid/telephony/PhoneNumberUtils;->internalCalledPartyBCDFragmentToString(Ljava/lang/StringBuilder;[BII)V

    #@a
    .line 1214
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method public static calledPartyBCDToString([BII)Ljava/lang/String;
    .registers 15
    .parameter "bytes"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/4 v11, 0x5

    #@1
    const/4 v10, 0x4

    #@2
    const/4 v9, 0x3

    #@3
    const/4 v8, 0x2

    #@4
    const/4 v7, 0x1

    #@5
    .line 962
    const/4 v2, 0x0

    #@6
    .line 963
    .local v2, prependPlus:Z
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    mul-int/lit8 v5, p2, 0x2

    #@a
    add-int/lit8 v5, v5, 0x1

    #@c
    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@f
    .line 965
    .local v3, ret:Ljava/lang/StringBuilder;
    if-ge p2, v8, :cond_14

    #@11
    .line 966
    const-string v5, ""

    #@13
    .line 1068
    :goto_13
    return-object v5

    #@14
    .line 970
    :cond_14
    aget-byte v5, p0, p1

    #@16
    and-int/lit16 v5, v5, 0xf0

    #@18
    const/16 v6, 0x90

    #@1a
    if-ne v5, v6, :cond_1d

    #@1c
    .line 971
    const/4 v2, 0x1

    #@1d
    .line 974
    :cond_1d
    add-int/lit8 v5, p1, 0x1

    #@1f
    add-int/lit8 v6, p2, -0x1

    #@21
    invoke-static {v3, p0, v5, v6}, Landroid/telephony/PhoneNumberUtils;->internalCalledPartyBCDFragmentToString(Ljava/lang/StringBuilder;[BII)V

    #@24
    .line 977
    if-eqz v2, :cond_2f

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    #@29
    move-result v5

    #@2a
    if-nez v5, :cond_2f

    #@2c
    .line 979
    const-string v5, ""

    #@2e
    goto :goto_13

    #@2f
    .line 982
    :cond_2f
    if-eqz v2, :cond_77

    #@31
    .line 1008
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 1009
    .local v4, retString:Ljava/lang/String;
    const-string v5, "(^[#*])(.*)([#*])(.*)(#)$"

    #@37
    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@3a
    move-result-object v1

    #@3b
    .line 1010
    .local v1, p:Ljava/util/regex/Pattern;
    invoke-virtual {v1, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@3e
    move-result-object v0

    #@3f
    .line 1011
    .local v0, m:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@42
    move-result v5

    #@43
    if-eqz v5, :cond_aa

    #@45
    .line 1012
    const-string v5, ""

    #@47
    invoke-virtual {v0, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v5

    #@4f
    if-eqz v5, :cond_7c

    #@51
    .line 1016
    new-instance v3, Ljava/lang/StringBuilder;

    #@53
    .end local v3           #ret:Ljava/lang/StringBuilder;
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    .line 1017
    .restart local v3       #ret:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    .line 1018
    invoke-virtual {v0, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    .line 1019
    invoke-virtual {v0, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    .line 1020
    invoke-virtual {v0, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    .line 1021
    const-string v5, "+"

    #@74
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    .line 1068
    .end local v0           #m:Ljava/util/regex/Matcher;
    .end local v1           #p:Ljava/util/regex/Pattern;
    .end local v4           #retString:Ljava/lang/String;
    :cond_77
    :goto_77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v5

    #@7b
    goto :goto_13

    #@7c
    .line 1026
    .restart local v0       #m:Ljava/util/regex/Matcher;
    .restart local v1       #p:Ljava/util/regex/Pattern;
    .restart local v4       #retString:Ljava/lang/String;
    :cond_7c
    new-instance v3, Ljava/lang/StringBuilder;

    #@7e
    .end local v3           #ret:Ljava/lang/StringBuilder;
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    .line 1027
    .restart local v3       #ret:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    .line 1028
    invoke-virtual {v0, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    .line 1029
    invoke-virtual {v0, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@92
    move-result-object v5

    #@93
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    .line 1030
    const-string v5, "+"

    #@98
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    .line 1031
    invoke-virtual {v0, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    .line 1032
    invoke-virtual {v0, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@a5
    move-result-object v5

    #@a6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    goto :goto_77

    #@aa
    .line 1035
    :cond_aa
    const-string v5, "(^[#*])(.*)([#*])(.*)"

    #@ac
    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@af
    move-result-object v1

    #@b0
    .line 1036
    invoke-virtual {v1, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@b3
    move-result-object v0

    #@b4
    .line 1037
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@b7
    move-result v5

    #@b8
    if-eqz v5, :cond_e1

    #@ba
    .line 1042
    new-instance v3, Ljava/lang/StringBuilder;

    #@bc
    .end local v3           #ret:Ljava/lang/StringBuilder;
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    .line 1043
    .restart local v3       #ret:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@c2
    move-result-object v5

    #@c3
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    .line 1044
    invoke-virtual {v0, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@c9
    move-result-object v5

    #@ca
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    .line 1045
    invoke-virtual {v0, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    .line 1046
    const-string v5, "+"

    #@d6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    .line 1047
    invoke-virtual {v0, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@dc
    move-result-object v5

    #@dd
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    goto :goto_77

    #@e1
    .line 1050
    :cond_e1
    new-instance v3, Ljava/lang/StringBuilder;

    #@e3
    .end local v3           #ret:Ljava/lang/StringBuilder;
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    .line 1052
    .restart local v3       #ret:Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    #@e7
    const-string/jumbo v6, "not_support_sms_nbpcd"

    #@ea
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@ed
    move-result v5

    #@ee
    if-ne v5, v7, :cond_122

    #@f0
    .line 1053
    const-string v5, "PhoneNumberUtils"

    #@f2
    new-instance v6, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v7, "calledPartyBCDToString, retString: "

    #@f9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v6

    #@fd
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v6

    #@101
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v6

    #@105
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    .line 1054
    const-string v5, "011"

    #@10a
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10d
    move-result v5

    #@10e
    if-eqz v5, :cond_11c

    #@110
    .line 1055
    const-string v5, "PhoneNumberUtils"

    #@112
    const-string v6, "calledPartyBCDToString, TPOA starts with 011 So Do not append +"

    #@114
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 1063
    :goto_117
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    goto/16 :goto_77

    #@11c
    .line 1057
    :cond_11c
    const/16 v5, 0x2b

    #@11e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@121
    goto :goto_117

    #@122
    .line 1060
    :cond_122
    const/16 v5, 0x2b

    #@124
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@127
    goto :goto_117
.end method

.method public static cdmaCheckAndProcessPlusCode(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "dialStr"

    #@0
    .prologue
    .line 3039
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_3f

    #@6
    .line 3040
    const/4 v2, 0x0

    #@7
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v2

    #@b
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isReallyDialable(C)Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_3f

    #@11
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(Ljava/lang/String;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_3f

    #@17
    .line 3042
    const-string v2, "gsm.operator.iso-country"

    #@19
    const-string v3, ""

    #@1b
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 3043
    .local v0, currIso:Ljava/lang/String;
    const-string v2, "gsm.sim.operator.iso-country"

    #@21
    const-string v3, ""

    #@23
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 3044
    .local v1, defaultIso:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2a
    move-result v2

    #@2b
    if-nez v2, :cond_3f

    #@2d
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_3f

    #@33
    .line 3045
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeFromCountryCode(Ljava/lang/String;)I

    #@36
    move-result v2

    #@37
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeFromCountryCode(Ljava/lang/String;)I

    #@3a
    move-result v3

    #@3b
    invoke-static {p0, v2, v3}, Landroid/telephony/PhoneNumberUtils;->cdmaCheckAndProcessPlusCodeByNumberFormat(Ljava/lang/String;II)Ljava/lang/String;

    #@3e
    move-result-object p0

    #@3f
    .line 3051
    .end local v0           #currIso:Ljava/lang/String;
    .end local v1           #defaultIso:Ljava/lang/String;
    .end local p0
    :cond_3f
    return-object p0
.end method

.method public static cdmaCheckAndProcessPlusCodeByNumberFormat(Ljava/lang/String;II)Ljava/lang/String;
    .registers 11
    .parameter "dialStr"
    .parameter "currFormat"
    .parameter "defaultFormat"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 3083
    move-object v3, p0

    #@2
    .line 3086
    .local v3, retStr:Ljava/lang/String;
    if-eqz p0, :cond_49

    #@4
    const-string v5, "+"

    #@6
    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@9
    move-result v5

    #@a
    const/4 v6, -0x1

    #@b
    if-eq v5, v6, :cond_49

    #@d
    .line 3090
    if-ne p1, p2, :cond_61

    #@f
    if-ne p1, v7, :cond_61

    #@11
    .line 3092
    const/4 v2, 0x0

    #@12
    .line 3093
    .local v2, postDialStr:Ljava/lang/String;
    move-object v4, p0

    #@13
    .line 3096
    .local v4, tempDialStr:Ljava/lang/String;
    const/4 v3, 0x0

    #@14
    .line 3103
    :cond_14
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    .line 3105
    .local v1, networkDialStr:Ljava/lang/String;
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->processPlusCodeWithinNanp(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 3108
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_50

    #@22
    .line 3109
    if-nez v3, :cond_4b

    #@24
    .line 3110
    move-object v3, v1

    #@25
    .line 3121
    :goto_25
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    .line 3122
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2c
    move-result v5

    #@2d
    if-nez v5, :cond_3d

    #@2f
    .line 3123
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->findDialableIndexFromPostDialStr(Ljava/lang/String;)I

    #@32
    move-result v0

    #@33
    .line 3126
    .local v0, dialableIndex:I
    if-lt v0, v7, :cond_56

    #@35
    .line 3127
    invoke-static {v0, v3, v2}, Landroid/telephony/PhoneNumberUtils;->appendPwCharBackToOrigDialStr(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    .line 3130
    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    .line 3142
    .end local v0           #dialableIndex:I
    :cond_3d
    :goto_3d
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@40
    move-result v5

    #@41
    if-nez v5, :cond_49

    #@43
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@46
    move-result v5

    #@47
    if-eqz v5, :cond_14

    #@49
    .end local v1           #networkDialStr:Ljava/lang/String;
    .end local v2           #postDialStr:Ljava/lang/String;
    .end local v4           #tempDialStr:Ljava/lang/String;
    :cond_49
    :goto_49
    move-object p0, v3

    #@4a
    .line 3150
    .end local p0
    :goto_4a
    return-object p0

    #@4b
    .line 3112
    .restart local v1       #networkDialStr:Ljava/lang/String;
    .restart local v2       #postDialStr:Ljava/lang/String;
    .restart local v4       #tempDialStr:Ljava/lang/String;
    .restart local p0
    :cond_4b
    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    goto :goto_25

    #@50
    .line 3118
    :cond_50
    const-string v5, "checkAndProcessPlusCode: null newDialStr"

    #@52
    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_4a

    #@56
    .line 3135
    .restart local v0       #dialableIndex:I
    :cond_56
    if-gez v0, :cond_5a

    #@58
    .line 3136
    const-string v2, ""

    #@5a
    .line 3138
    :cond_5a
    const-string/jumbo v5, "wrong postDialStr="

    #@5d
    invoke-static {v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_3d

    #@61
    .line 3147
    .end local v0           #dialableIndex:I
    .end local v1           #networkDialStr:Ljava/lang/String;
    .end local v2           #postDialStr:Ljava/lang/String;
    .end local v4           #tempDialStr:Ljava/lang/String;
    :cond_61
    const-string v5, "checkAndProcessPlusCode:non-NANP not supported"

    #@63
    invoke-static {v5, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    goto :goto_49
.end method

.method private static charToBCD(C)I
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 1234
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    .line 1235
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 1243
    :goto_a
    return v0

    #@b
    .line 1236
    :cond_b
    const/16 v0, 0x2a

    #@d
    if-ne p0, v0, :cond_12

    #@f
    .line 1237
    const/16 v0, 0xa

    #@11
    goto :goto_a

    #@12
    .line 1238
    :cond_12
    const/16 v0, 0x23

    #@14
    if-ne p0, v0, :cond_19

    #@16
    .line 1239
    const/16 v0, 0xb

    #@18
    goto :goto_a

    #@19
    .line 1240
    :cond_19
    const/16 v0, 0x2c

    #@1b
    if-ne p0, v0, :cond_20

    #@1d
    .line 1241
    const/16 v0, 0xc

    #@1f
    goto :goto_a

    #@20
    .line 1242
    :cond_20
    const/16 v0, 0x4e

    #@22
    if-ne p0, v0, :cond_27

    #@24
    .line 1243
    const/16 v0, 0xd

    #@26
    goto :goto_a

    #@27
    .line 1245
    :cond_27
    new-instance v0, Ljava/lang/RuntimeException;

    #@29
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "invalid char for BCD "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v0
.end method

.method public static checkPlefixPlusOnly(Ljava/lang/String;)Z
    .registers 4
    .parameter "phoneNumber"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 4131
    if-eqz p0, :cond_a

    #@4
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 4140
    :cond_a
    :goto_a
    return v0

    #@b
    .line 4136
    :cond_b
    const-string v2, "010"

    #@d
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_23

    #@13
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@16
    move-result v2

    #@17
    if-ne v2, v1, :cond_a

    #@19
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@1c
    move-result v2

    #@1d
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isPlus(C)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_a

    #@23
    :cond_23
    move v0, v1

    #@24
    .line 4137
    goto :goto_a
.end method

.method private static checkPrefixIsIgnorable(Ljava/lang/String;II)Z
    .registers 6
    .parameter "str"
    .parameter "forwardIndex"
    .parameter "backwardIndex"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3829
    const/4 v0, 0x0

    #@2
    .line 3830
    .local v0, trunk_prefix_was_read:Z
    :goto_2
    if-lt p2, p1, :cond_20

    #@4
    .line 3831
    invoke-virtual {p0, p2}, Ljava/lang/String;->charAt(I)C

    #@7
    move-result v2

    #@8
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->tryGetISODigit(C)I

    #@b
    move-result v2

    #@c
    if-ltz v2, :cond_15

    #@e
    .line 3832
    if-eqz v0, :cond_11

    #@10
    .line 3847
    :goto_10
    return v1

    #@11
    .line 3838
    :cond_11
    const/4 v0, 0x1

    #@12
    .line 3844
    :cond_12
    add-int/lit8 p2, p2, -0x1

    #@14
    goto :goto_2

    #@15
    .line 3840
    :cond_15
    invoke-virtual {p0, p2}, Ljava/lang/String;->charAt(I)C

    #@18
    move-result v2

    #@19
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_12

    #@1f
    goto :goto_10

    #@20
    .line 3847
    :cond_20
    const/4 v1, 0x1

    #@21
    goto :goto_10
.end method

.method public static compare(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "context"
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 539
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    const v2, 0x1110023

    #@7
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@a
    move-result v0

    #@b
    .line 541
    .local v0, useStrict:Z
    invoke-static {p1, p2, v0}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public static compare(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 525
    invoke-static {}, Lcom/lge/util/MatchingDigitsHelper;->isVenezuela()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 527
    invoke-static {p0, p1}, Lcom/lge/util/MatchingDigitsHelper;->compareForMatching(Ljava/lang/String;Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    .line 529
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@f
    move-result v0

    #@10
    goto :goto_a
.end method

.method public static compare(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 4
    .parameter "a"
    .parameter "b"
    .parameter "useStrictComparation"

    #@0
    .prologue
    .line 548
    if-eqz p2, :cond_7

    #@2
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->compareStrictly(Ljava/lang/String;Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    goto :goto_6
.end method

.method public static compareLoosely(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 566
    const/4 v0, 0x7

    #@1
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 18
    .parameter "a"
    .parameter "b"
    .parameter "minMatch"

    #@0
    .prologue
    .line 576
    const/4 v10, 0x0

    #@1
    .line 577
    .local v10, numNonDialableCharsInA:I
    const/4 v11, 0x0

    #@2
    .line 579
    .local v11, numNonDialableCharsInB:I
    if-eqz p0, :cond_6

    #@4
    if-nez p1, :cond_e

    #@6
    :cond_6
    move-object/from16 v0, p1

    #@8
    if-ne p0, v0, :cond_c

    #@a
    const/4 v13, 0x1

    #@b
    .line 702
    :goto_b
    return v13

    #@c
    .line 579
    :cond_c
    const/4 v13, 0x0

    #@d
    goto :goto_b

    #@e
    .line 581
    :cond_e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@11
    move-result v13

    #@12
    if-eqz v13, :cond_1a

    #@14
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@17
    move-result v13

    #@18
    if-nez v13, :cond_1c

    #@1a
    .line 582
    :cond_1a
    const/4 v13, 0x0

    #@1b
    goto :goto_b

    #@1c
    .line 585
    :cond_1c
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->indexOfLastNetworkChar(Ljava/lang/String;)I

    #@1f
    move-result v7

    #@20
    .line 586
    .local v7, ia:I
    invoke-static/range {p1 .. p1}, Landroid/telephony/PhoneNumberUtils;->indexOfLastNetworkChar(Ljava/lang/String;)I

    #@23
    move-result v8

    #@24
    .line 587
    .local v8, ib:I
    const/4 v9, 0x0

    #@25
    .line 589
    .local v9, matched:I
    :cond_25
    :goto_25
    if-ltz v7, :cond_56

    #@27
    if-ltz v8, :cond_56

    #@29
    .line 591
    const/4 v12, 0x0

    #@2a
    .line 593
    .local v12, skipCmp:Z
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    #@2d
    move-result v3

    #@2e
    .line 595
    .local v3, ca:C
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@31
    move-result v13

    #@32
    if-nez v13, :cond_39

    #@34
    .line 596
    add-int/lit8 v7, v7, -0x1

    #@36
    .line 597
    const/4 v12, 0x1

    #@37
    .line 598
    add-int/lit8 v10, v10, 0x1

    #@39
    .line 601
    :cond_39
    move-object/from16 v0, p1

    #@3b
    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    #@3e
    move-result v4

    #@3f
    .line 603
    .local v4, cb:C
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@42
    move-result v13

    #@43
    if-nez v13, :cond_4a

    #@45
    .line 604
    add-int/lit8 v8, v8, -0x1

    #@47
    .line 605
    const/4 v12, 0x1

    #@48
    .line 606
    add-int/lit8 v11, v11, 0x1

    #@4a
    .line 609
    :cond_4a
    if-nez v12, :cond_25

    #@4c
    .line 610
    if-eq v4, v3, :cond_6c

    #@4e
    const/16 v13, 0x4e

    #@50
    if-eq v3, v13, :cond_6c

    #@52
    const/16 v13, 0x4e

    #@54
    if-eq v4, v13, :cond_6c

    #@56
    .line 617
    .end local v3           #ca:C
    .end local v4           #cb:C
    .end local v12           #skipCmp:Z
    :cond_56
    move/from16 v0, p2

    #@58
    if-ge v9, v0, :cond_75

    #@5a
    .line 618
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5d
    move-result v13

    #@5e
    sub-int v5, v13, v10

    #@60
    .line 619
    .local v5, effectiveALen:I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@63
    move-result v13

    #@64
    sub-int v6, v13, v11

    #@66
    .line 624
    .local v6, effectiveBLen:I
    if-ne v5, v6, :cond_73

    #@68
    if-ne v5, v9, :cond_73

    #@6a
    .line 625
    const/4 v13, 0x1

    #@6b
    goto :goto_b

    #@6c
    .line 613
    .end local v5           #effectiveALen:I
    .end local v6           #effectiveBLen:I
    .restart local v3       #ca:C
    .restart local v4       #cb:C
    .restart local v12       #skipCmp:Z
    :cond_6c
    add-int/lit8 v7, v7, -0x1

    #@6e
    add-int/lit8 v8, v8, -0x1

    #@70
    add-int/lit8 v9, v9, 0x1

    #@72
    goto :goto_25

    #@73
    .line 628
    .end local v3           #ca:C
    .end local v4           #cb:C
    .end local v12           #skipCmp:Z
    .restart local v5       #effectiveALen:I
    .restart local v6       #effectiveBLen:I
    :cond_73
    const/4 v13, 0x0

    #@74
    goto :goto_b

    #@75
    .line 632
    .end local v5           #effectiveALen:I
    .end local v6           #effectiveBLen:I
    :cond_75
    move/from16 v0, p2

    #@77
    if-lt v9, v0, :cond_7f

    #@79
    if-ltz v7, :cond_7d

    #@7b
    if-gez v8, :cond_7f

    #@7d
    .line 633
    :cond_7d
    const/4 v13, 0x1

    #@7e
    goto :goto_b

    #@7f
    .line 646
    :cond_7f
    const-string v13, "KDDI"

    #@81
    invoke-static {v13}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@84
    move-result v13

    #@85
    if-eqz v13, :cond_ea

    #@87
    .line 648
    add-int/lit8 v13, v7, 0x1

    #@89
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefix(Ljava/lang/String;I)Z

    #@8c
    move-result v13

    #@8d
    if-nez v13, :cond_97

    #@8f
    add-int/lit8 v13, v7, 0x1

    #@91
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefix_KDDI(Ljava/lang/String;I)Z

    #@94
    move-result v13

    #@95
    if-eqz v13, :cond_ae

    #@97
    :cond_97
    add-int/lit8 v13, v8, 0x1

    #@99
    move-object/from16 v0, p1

    #@9b
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefix(Ljava/lang/String;I)Z

    #@9e
    move-result v13

    #@9f
    if-nez v13, :cond_ab

    #@a1
    add-int/lit8 v13, v8, 0x1

    #@a3
    move-object/from16 v0, p1

    #@a5
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefix_KDDI(Ljava/lang/String;I)Z

    #@a8
    move-result v13

    #@a9
    if-eqz v13, :cond_ae

    #@ab
    .line 651
    :cond_ab
    const/4 v13, 0x1

    #@ac
    goto/16 :goto_b

    #@ae
    .line 654
    :cond_ae
    add-int/lit8 v13, v7, 0x1

    #@b0
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchTrunkPrefix(Ljava/lang/String;I)Z

    #@b3
    move-result v13

    #@b4
    if-eqz v13, :cond_cd

    #@b6
    add-int/lit8 v13, v8, 0x1

    #@b8
    move-object/from16 v0, p1

    #@ba
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefixAndCC(Ljava/lang/String;I)Z

    #@bd
    move-result v13

    #@be
    if-nez v13, :cond_ca

    #@c0
    add-int/lit8 v13, v8, 0x1

    #@c2
    move-object/from16 v0, p1

    #@c4
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefixAndCC_KDDI(Ljava/lang/String;I)Z

    #@c7
    move-result v13

    #@c8
    if-eqz v13, :cond_cd

    #@ca
    .line 657
    :cond_ca
    const/4 v13, 0x1

    #@cb
    goto/16 :goto_b

    #@cd
    .line 660
    :cond_cd
    add-int/lit8 v13, v8, 0x1

    #@cf
    move-object/from16 v0, p1

    #@d1
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchTrunkPrefix(Ljava/lang/String;I)Z

    #@d4
    move-result v13

    #@d5
    if-eqz v13, :cond_129

    #@d7
    add-int/lit8 v13, v7, 0x1

    #@d9
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefixAndCC(Ljava/lang/String;I)Z

    #@dc
    move-result v13

    #@dd
    if-nez v13, :cond_e7

    #@df
    add-int/lit8 v13, v7, 0x1

    #@e1
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefixAndCC_KDDI(Ljava/lang/String;I)Z

    #@e4
    move-result v13

    #@e5
    if-eqz v13, :cond_129

    #@e7
    .line 663
    :cond_e7
    const/4 v13, 0x1

    #@e8
    goto/16 :goto_b

    #@ea
    .line 667
    :cond_ea
    add-int/lit8 v13, v7, 0x1

    #@ec
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefix(Ljava/lang/String;I)Z

    #@ef
    move-result v13

    #@f0
    if-eqz v13, :cond_ff

    #@f2
    add-int/lit8 v13, v8, 0x1

    #@f4
    move-object/from16 v0, p1

    #@f6
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefix(Ljava/lang/String;I)Z

    #@f9
    move-result v13

    #@fa
    if-eqz v13, :cond_ff

    #@fc
    .line 670
    const/4 v13, 0x1

    #@fd
    goto/16 :goto_b

    #@ff
    .line 673
    :cond_ff
    add-int/lit8 v13, v7, 0x1

    #@101
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchTrunkPrefix(Ljava/lang/String;I)Z

    #@104
    move-result v13

    #@105
    if-eqz v13, :cond_114

    #@107
    add-int/lit8 v13, v8, 0x1

    #@109
    move-object/from16 v0, p1

    #@10b
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefixAndCC(Ljava/lang/String;I)Z

    #@10e
    move-result v13

    #@10f
    if-eqz v13, :cond_114

    #@111
    .line 676
    const/4 v13, 0x1

    #@112
    goto/16 :goto_b

    #@114
    .line 679
    :cond_114
    add-int/lit8 v13, v8, 0x1

    #@116
    move-object/from16 v0, p1

    #@118
    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->matchTrunkPrefix(Ljava/lang/String;I)Z

    #@11b
    move-result v13

    #@11c
    if-eqz v13, :cond_129

    #@11e
    add-int/lit8 v13, v7, 0x1

    #@120
    invoke-static {p0, v13}, Landroid/telephony/PhoneNumberUtils;->matchIntlPrefixAndCC(Ljava/lang/String;I)Z

    #@123
    move-result v13

    #@124
    if-eqz v13, :cond_129

    #@126
    .line 682
    const/4 v13, 0x1

    #@127
    goto/16 :goto_b

    #@129
    .line 696
    :cond_129
    const/4 v13, 0x0

    #@12a
    invoke-virtual {p0, v13}, Ljava/lang/String;->charAt(I)C

    #@12d
    move-result v13

    #@12e
    const/16 v14, 0x2b

    #@130
    if-ne v13, v14, :cond_150

    #@132
    const/4 v1, 0x1

    #@133
    .line 697
    .local v1, aPlusFirst:Z
    :goto_133
    const/4 v13, 0x0

    #@134
    move-object/from16 v0, p1

    #@136
    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    #@139
    move-result v13

    #@13a
    const/16 v14, 0x2b

    #@13c
    if-ne v13, v14, :cond_152

    #@13e
    const/4 v2, 0x1

    #@13f
    .line 698
    .local v2, bPlusFirst:Z
    :goto_13f
    const/4 v13, 0x4

    #@140
    if-ge v7, v13, :cond_154

    #@142
    const/4 v13, 0x4

    #@143
    if-ge v8, v13, :cond_154

    #@145
    if-nez v1, :cond_149

    #@147
    if-eqz v2, :cond_154

    #@149
    :cond_149
    if-eqz v1, :cond_14d

    #@14b
    if-nez v2, :cond_154

    #@14d
    .line 699
    :cond_14d
    const/4 v13, 0x1

    #@14e
    goto/16 :goto_b

    #@150
    .line 696
    .end local v1           #aPlusFirst:Z
    .end local v2           #bPlusFirst:Z
    :cond_150
    const/4 v1, 0x0

    #@151
    goto :goto_133

    #@152
    .line 697
    .restart local v1       #aPlusFirst:Z
    :cond_152
    const/4 v2, 0x0

    #@153
    goto :goto_13f

    #@154
    .line 702
    .restart local v2       #bPlusFirst:Z
    :cond_154
    const/4 v13, 0x0

    #@155
    goto/16 :goto_b
.end method

.method public static compareStrictly(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 710
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->compareStrictly(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static compareStrictly(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 23
    .parameter "a"
    .parameter "b"
    .parameter "acceptInvalidCCCPrefix"

    #@0
    .prologue
    .line 718
    if-eqz p0, :cond_4

    #@2
    if-nez p1, :cond_10

    #@4
    .line 719
    :cond_4
    move-object/from16 v0, p0

    #@6
    move-object/from16 v1, p1

    #@8
    if-ne v0, v1, :cond_d

    #@a
    const/16 v18, 0x1

    #@c
    .line 856
    :goto_c
    return v18

    #@d
    .line 719
    :cond_d
    const/16 v18, 0x0

    #@f
    goto :goto_c

    #@10
    .line 720
    :cond_10
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    #@13
    move-result v18

    #@14
    if-nez v18, :cond_1f

    #@16
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@19
    move-result v18

    #@1a
    if-nez v18, :cond_1f

    #@1c
    .line 721
    const/16 v18, 0x0

    #@1e
    goto :goto_c

    #@1f
    .line 724
    :cond_1f
    const/4 v10, 0x0

    #@20
    .line 725
    .local v10, forwardIndexA:I
    const/4 v11, 0x0

    #@21
    .line 727
    .local v11, forwardIndexB:I
    move-object/from16 v0, p0

    #@23
    move/from16 v1, p2

    #@25
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->tryGetCountryCallingCodeAndNewIndex(Ljava/lang/String;Z)Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;

    #@28
    move-result-object v6

    #@29
    .line 729
    .local v6, cccA:Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;
    invoke-static/range {p1 .. p2}, Landroid/telephony/PhoneNumberUtils;->tryGetCountryCallingCodeAndNewIndex(Ljava/lang/String;Z)Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;

    #@2c
    move-result-object v7

    #@2d
    .line 731
    .local v7, cccB:Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;
    const/4 v5, 0x0

    #@2e
    .line 732
    .local v5, bothHasCountryCallingCode:Z
    const/4 v13, 0x1

    #@2f
    .line 733
    .local v13, okToIgnorePrefix:Z
    const/16 v16, 0x0

    #@31
    .line 734
    .local v16, trunkPrefixIsOmittedA:Z
    const/16 v17, 0x0

    #@33
    .line 735
    .local v17, trunkPrefixIsOmittedB:Z
    if-eqz v6, :cond_84

    #@35
    if-eqz v7, :cond_84

    #@37
    .line 736
    iget v0, v6, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;->countryCallingCode:I

    #@39
    move/from16 v18, v0

    #@3b
    iget v0, v7, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;->countryCallingCode:I

    #@3d
    move/from16 v19, v0

    #@3f
    move/from16 v0, v18

    #@41
    move/from16 v1, v19

    #@43
    if-eq v0, v1, :cond_48

    #@45
    .line 738
    const/16 v18, 0x0

    #@47
    goto :goto_c

    #@48
    .line 742
    :cond_48
    const/4 v13, 0x0

    #@49
    .line 743
    const/4 v5, 0x1

    #@4a
    .line 744
    iget v10, v6, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;->newIndex:I

    #@4c
    .line 745
    iget v11, v7, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;->newIndex:I

    #@4e
    .line 771
    :cond_4e
    :goto_4e
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    #@51
    move-result v18

    #@52
    add-int/lit8 v3, v18, -0x1

    #@54
    .line 772
    .local v3, backwardIndexA:I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@57
    move-result v18

    #@58
    add-int/lit8 v4, v18, -0x1

    #@5a
    .line 773
    .local v4, backwardIndexB:I
    :cond_5a
    :goto_5a
    if-lt v3, v10, :cond_b8

    #@5c
    if-lt v4, v11, :cond_b8

    #@5e
    .line 774
    const/4 v14, 0x0

    #@5f
    .line 775
    .local v14, skip_compare:Z
    move-object/from16 v0, p0

    #@61
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    #@64
    move-result v8

    #@65
    .line 776
    .local v8, chA:C
    move-object/from16 v0, p1

    #@67
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    #@6a
    move-result v9

    #@6b
    .line 777
    .local v9, chB:C
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->isSeparator(C)Z

    #@6e
    move-result v18

    #@6f
    if-eqz v18, :cond_74

    #@71
    .line 778
    add-int/lit8 v3, v3, -0x1

    #@73
    .line 779
    const/4 v14, 0x1

    #@74
    .line 781
    :cond_74
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->isSeparator(C)Z

    #@77
    move-result v18

    #@78
    if-eqz v18, :cond_7d

    #@7a
    .line 782
    add-int/lit8 v4, v4, -0x1

    #@7c
    .line 783
    const/4 v14, 0x1

    #@7d
    .line 786
    :cond_7d
    if-nez v14, :cond_5a

    #@7f
    .line 787
    if-eq v8, v9, :cond_b3

    #@81
    .line 788
    const/16 v18, 0x0

    #@83
    goto :goto_c

    #@84
    .line 746
    .end local v3           #backwardIndexA:I
    .end local v4           #backwardIndexB:I
    .end local v8           #chA:C
    .end local v9           #chB:C
    .end local v14           #skip_compare:Z
    :cond_84
    if-nez v6, :cond_8a

    #@86
    if-nez v7, :cond_8a

    #@88
    .line 749
    const/4 v13, 0x0

    #@89
    goto :goto_4e

    #@8a
    .line 751
    :cond_8a
    if-eqz v6, :cond_93

    #@8c
    .line 752
    iget v10, v6, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;->newIndex:I

    #@8e
    .line 760
    :cond_8e
    :goto_8e
    if-eqz v7, :cond_a3

    #@90
    .line 761
    iget v11, v7, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;->newIndex:I

    #@92
    goto :goto_4e

    #@93
    .line 754
    :cond_93
    const/16 v18, 0x0

    #@95
    move-object/from16 v0, p1

    #@97
    move/from16 v1, v18

    #@99
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->tryGetTrunkPrefixOmittedIndex(Ljava/lang/String;I)I

    #@9c
    move-result v15

    #@9d
    .line 755
    .local v15, tmp:I
    if-ltz v15, :cond_8e

    #@9f
    .line 756
    move v10, v15

    #@a0
    .line 757
    const/16 v16, 0x1

    #@a2
    goto :goto_8e

    #@a3
    .line 763
    .end local v15           #tmp:I
    :cond_a3
    const/16 v18, 0x0

    #@a5
    move-object/from16 v0, p1

    #@a7
    move/from16 v1, v18

    #@a9
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->tryGetTrunkPrefixOmittedIndex(Ljava/lang/String;I)I

    #@ac
    move-result v15

    #@ad
    .line 764
    .restart local v15       #tmp:I
    if-ltz v15, :cond_4e

    #@af
    .line 765
    move v11, v15

    #@b0
    .line 766
    const/16 v17, 0x1

    #@b2
    goto :goto_4e

    #@b3
    .line 790
    .end local v15           #tmp:I
    .restart local v3       #backwardIndexA:I
    .restart local v4       #backwardIndexB:I
    .restart local v8       #chA:C
    .restart local v9       #chB:C
    .restart local v14       #skip_compare:Z
    :cond_b3
    add-int/lit8 v3, v3, -0x1

    #@b5
    .line 791
    add-int/lit8 v4, v4, -0x1

    #@b7
    goto :goto_5a

    #@b8
    .line 795
    .end local v8           #chA:C
    .end local v9           #chB:C
    .end local v14           #skip_compare:Z
    :cond_b8
    if-eqz v13, :cond_fa

    #@ba
    .line 796
    if-eqz v16, :cond_be

    #@bc
    if-le v10, v3, :cond_c6

    #@be
    :cond_be
    move-object/from16 v0, p0

    #@c0
    invoke-static {v0, v10, v3}, Landroid/telephony/PhoneNumberUtils;->checkPrefixIsIgnorable(Ljava/lang/String;II)Z

    #@c3
    move-result v18

    #@c4
    if-nez v18, :cond_da

    #@c6
    .line 798
    :cond_c6
    if-eqz p2, :cond_d6

    #@c8
    .line 808
    const/16 v18, 0x0

    #@ca
    move-object/from16 v0, p0

    #@cc
    move-object/from16 v1, p1

    #@ce
    move/from16 v2, v18

    #@d0
    invoke-static {v0, v1, v2}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@d3
    move-result v18

    #@d4
    goto/16 :goto_c

    #@d6
    .line 810
    :cond_d6
    const/16 v18, 0x0

    #@d8
    goto/16 :goto_c

    #@da
    .line 813
    :cond_da
    if-eqz v17, :cond_de

    #@dc
    if-le v11, v4, :cond_e6

    #@de
    :cond_de
    move-object/from16 v0, p1

    #@e0
    invoke-static {v0, v10, v4}, Landroid/telephony/PhoneNumberUtils;->checkPrefixIsIgnorable(Ljava/lang/String;II)Z

    #@e3
    move-result v18

    #@e4
    if-nez v18, :cond_147

    #@e6
    .line 815
    :cond_e6
    if-eqz p2, :cond_f6

    #@e8
    .line 816
    const/16 v18, 0x0

    #@ea
    move-object/from16 v0, p0

    #@ec
    move-object/from16 v1, p1

    #@ee
    move/from16 v2, v18

    #@f0
    invoke-static {v0, v1, v2}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@f3
    move-result v18

    #@f4
    goto/16 :goto_c

    #@f6
    .line 818
    :cond_f6
    const/16 v18, 0x0

    #@f8
    goto/16 :goto_c

    #@fa
    .line 831
    :cond_fa
    if-nez v5, :cond_11d

    #@fc
    const/4 v12, 0x1

    #@fd
    .line 832
    .local v12, maybeNamp:Z
    :goto_fd
    if-lt v3, v10, :cond_123

    #@ff
    .line 833
    move-object/from16 v0, p0

    #@101
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    #@104
    move-result v8

    #@105
    .line 834
    .restart local v8       #chA:C
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@108
    move-result v18

    #@109
    if-eqz v18, :cond_11a

    #@10b
    .line 835
    if-eqz v12, :cond_11f

    #@10d
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->tryGetISODigit(C)I

    #@110
    move-result v18

    #@111
    const/16 v19, 0x1

    #@113
    move/from16 v0, v18

    #@115
    move/from16 v1, v19

    #@117
    if-ne v0, v1, :cond_11f

    #@119
    .line 836
    const/4 v12, 0x0

    #@11a
    .line 841
    :cond_11a
    add-int/lit8 v3, v3, -0x1

    #@11c
    .line 842
    goto :goto_fd

    #@11d
    .line 831
    .end local v8           #chA:C
    .end local v12           #maybeNamp:Z
    :cond_11d
    const/4 v12, 0x0

    #@11e
    goto :goto_fd

    #@11f
    .line 838
    .restart local v8       #chA:C
    .restart local v12       #maybeNamp:Z
    :cond_11f
    const/16 v18, 0x0

    #@121
    goto/16 :goto_c

    #@123
    .line 843
    .end local v8           #chA:C
    :cond_123
    :goto_123
    if-lt v4, v11, :cond_147

    #@125
    .line 844
    move-object/from16 v0, p1

    #@127
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    #@12a
    move-result v9

    #@12b
    .line 845
    .restart local v9       #chB:C
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@12e
    move-result v18

    #@12f
    if-eqz v18, :cond_140

    #@131
    .line 846
    if-eqz v12, :cond_143

    #@133
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->tryGetISODigit(C)I

    #@136
    move-result v18

    #@137
    const/16 v19, 0x1

    #@139
    move/from16 v0, v18

    #@13b
    move/from16 v1, v19

    #@13d
    if-ne v0, v1, :cond_143

    #@13f
    .line 847
    const/4 v12, 0x0

    #@140
    .line 852
    :cond_140
    add-int/lit8 v4, v4, -0x1

    #@142
    .line 853
    goto :goto_123

    #@143
    .line 849
    :cond_143
    const/16 v18, 0x0

    #@145
    goto/16 :goto_c

    #@147
    .line 856
    .end local v9           #chB:C
    .end local v12           #maybeNamp:Z
    :cond_147
    const/16 v18, 0x1

    #@149
    goto/16 :goto_c
.end method

.method public static convertAndStrip(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 421
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 2954
    if-nez p0, :cond_3

    #@2
    .line 2970
    .end local p0
    :cond_2
    :goto_2
    return-object p0

    #@3
    .line 2957
    .restart local p0
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v2

    #@7
    .line 2958
    .local v2, len:I
    if-eqz v2, :cond_2

    #@9
    .line 2962
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    #@c
    move-result-object v3

    #@d
    .line 2964
    .local v3, out:[C
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v2, :cond_1e

    #@10
    .line 2965
    aget-char v0, v3, v1

    #@12
    .line 2967
    .local v0, c:C
    sget-object v4, Landroid/telephony/PhoneNumberUtils;->KEYPAD_MAP:Landroid/util/SparseIntArray;

    #@14
    invoke-virtual {v4, v0, v0}, Landroid/util/SparseIntArray;->get(II)I

    #@17
    move-result v4

    #@18
    int-to-char v4, v4

    #@19
    aput-char v4, v3, v1

    #@1b
    .line 2964
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_e

    #@1e
    .line 2970
    .end local v0           #c:C
    :cond_1e
    new-instance p0, Ljava/lang/String;

    #@20
    .end local p0
    invoke-direct {p0, v3}, Ljava/lang/String;-><init>([C)V

    #@23
    goto :goto_2
.end method

.method public static convertPlusToNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "phoneNumber"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 4091
    if-eqz p0, :cond_a

    #@4
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_b

    #@a
    .line 4120
    :cond_a
    :goto_a
    return-object p0

    #@b
    .line 4094
    :cond_b
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@12
    move-result v4

    #@13
    if-eq v4, v8, :cond_a

    #@15
    .line 4098
    new-instance v1, Ljava/lang/StringBuffer;

    #@17
    invoke-direct {v1, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@1a
    .line 4099
    .local v1, buffer:Ljava/lang/StringBuffer;
    const-string v2, ""

    #@1c
    .line 4101
    .local v2, prefix:Ljava/lang/String;
    new-array v3, v8, [Ljava/lang/String;

    #@1e
    const-string v4, ""

    #@20
    aput-object v4, v3, v7

    #@22
    .local v3, prefixs:[Ljava/lang/String;
    move-object v4, v3

    #@23
    .line 4102
    check-cast v4, [Ljava/lang/Object;

    #@25
    const/4 v5, 0x2

    #@26
    new-array v5, v5, [Ljava/lang/String;

    #@28
    const-string v6, "184"

    #@2a
    aput-object v6, v5, v7

    #@2c
    const-string v6, "186"

    #@2e
    aput-object v6, v5, v8

    #@30
    invoke-static {p0, v4, v5}, Landroid/telephony/PhoneNumberUtils;->getStartWithPrefix(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_38

    #@36
    .line 4103
    aget-object v2, v3, v7

    #@38
    .line 4107
    :cond_38
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@3b
    move-result v4

    #@3c
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3f
    move-result v5

    #@40
    if-ge v4, v5, :cond_6b

    #@42
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@45
    move-result v4

    #@46
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    #@49
    move-result v4

    #@4a
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isPlus(C)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_6b

    #@50
    .line 4109
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@57
    move-result v4

    #@58
    if-eqz v4, :cond_70

    #@5a
    .line 4111
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->roamingIntlPrefix()Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    .line 4112
    .local v0, RoamingPrefix:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@61
    move-result v4

    #@62
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@65
    move-result v5

    #@66
    add-int/lit8 v5, v5, 0x1

    #@68
    invoke-virtual {v1, v4, v5, v0}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    #@6b
    .line 4118
    .end local v0           #RoamingPrefix:Ljava/lang/String;
    :cond_6b
    :goto_6b
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@6e
    move-result-object p0

    #@6f
    .line 4120
    goto :goto_a

    #@70
    .line 4115
    :cond_70
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@73
    move-result v4

    #@74
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@77
    move-result v5

    #@78
    add-int/lit8 v5, v5, 0x1

    #@7a
    const-string v6, "010"

    #@7c
    invoke-virtual {v1, v4, v5, v6}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    #@7f
    goto :goto_6b
.end method

.method public static convertPreDial(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 431
    if-nez p0, :cond_4

    #@2
    .line 432
    const/4 v4, 0x0

    #@3
    .line 447
    :goto_3
    return-object v4

    #@4
    .line 434
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 435
    .local v2, len:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 437
    .local v3, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v2, :cond_2b

    #@10
    .line 438
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    .line 440
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isPause(C)Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_22

    #@1a
    .line 441
    const/16 v0, 0x2c

    #@1c
    .line 445
    :cond_1c
    :goto_1c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 437
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_e

    #@22
    .line 442
    :cond_22
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isToneWait(C)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_1c

    #@28
    .line 443
    const/16 v0, 0x3b

    #@2a
    goto :goto_1c

    #@2b
    .line 447
    .end local v0           #c:C
    :cond_2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    goto :goto_3
.end method

.method public static extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "phoneNumber"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 276
    if-nez p0, :cond_4

    #@3
    .line 307
    :goto_3
    return-object v6

    #@4
    .line 281
    :cond_4
    const-string/jumbo v7, "support_copytosim_tp_oa_address_ntt_docomo"

    #@7
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_18

    #@d
    const-string v6, "NTT DOCOMO"

    #@f
    invoke-virtual {p0, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@12
    move-result v6

    #@13
    if-eqz v6, :cond_18

    #@15
    .line 282
    const-string v6, ""

    #@17
    goto :goto_3

    #@18
    .line 285
    :cond_18
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@1b
    move-result v3

    #@1c
    .line 286
    .local v3, len:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@21
    .line 288
    .local v5, ret:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@22
    .local v2, i:I
    :goto_22
    if-ge v2, v3, :cond_69

    #@24
    .line 289
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@27
    move-result v0

    #@28
    .line 291
    .local v0, c:C
    const/16 v6, 0xa

    #@2a
    invoke-static {v0, v6}, Ljava/lang/Character;->digit(CI)I

    #@2d
    move-result v1

    #@2e
    .line 292
    .local v1, digit:I
    const/4 v6, -0x1

    #@2f
    if-eq v1, v6, :cond_37

    #@31
    .line 293
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    .line 288
    :cond_34
    :goto_34
    add-int/lit8 v2, v2, 0x1

    #@36
    goto :goto_22

    #@37
    .line 294
    :cond_37
    const/16 v6, 0x2b

    #@39
    if-ne v0, v6, :cond_59

    #@3b
    .line 296
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    .line 297
    .local v4, prefix:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@42
    move-result v6

    #@43
    if-eqz v6, :cond_55

    #@45
    const-string v6, "*31#"

    #@47
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v6

    #@4b
    if-nez v6, :cond_55

    #@4d
    const-string v6, "#31#"

    #@4f
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v6

    #@53
    if-eqz v6, :cond_34

    #@55
    .line 298
    :cond_55
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@58
    goto :goto_34

    #@59
    .line 300
    .end local v4           #prefix:Ljava/lang/String;
    :cond_59
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@5c
    move-result v6

    #@5d
    if-eqz v6, :cond_63

    #@5f
    .line 301
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@62
    goto :goto_34

    #@63
    .line 302
    :cond_63
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isStartsPostDial(C)Z

    #@66
    move-result v6

    #@67
    if-eqz v6, :cond_34

    #@69
    .line 307
    .end local v0           #c:C
    .end local v1           #digit:I
    :cond_69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    goto :goto_3
.end method

.method public static extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 358
    if-nez p0, :cond_4

    #@2
    .line 359
    const/4 v5, 0x0

    #@3
    .line 381
    :goto_3
    return-object v5

    #@4
    .line 362
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v3

    #@8
    .line 363
    .local v3, len:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 364
    .local v4, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@e
    .line 366
    .local v1, haveSeenPlus:Z
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v3, :cond_2f

    #@11
    .line 367
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v0

    #@15
    .line 368
    .local v0, c:C
    const/16 v5, 0x2b

    #@17
    if-ne v0, v5, :cond_1f

    #@19
    .line 369
    if-eqz v1, :cond_1e

    #@1b
    .line 366
    :cond_1b
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_f

    #@1e
    .line 372
    :cond_1e
    const/4 v1, 0x1

    #@1f
    .line 374
    :cond_1f
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_29

    #@25
    .line 375
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    goto :goto_1b

    #@29
    .line 376
    :cond_29
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isStartsPostDial(C)Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_1b

    #@2f
    .line 381
    .end local v0           #c:C
    :cond_2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    goto :goto_3
.end method

.method public static extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 500
    if-nez p0, :cond_4

    #@2
    const/4 v5, 0x0

    #@3
    .line 516
    :goto_3
    return-object v5

    #@4
    .line 503
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 505
    .local v2, ret:Ljava/lang/StringBuilder;
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->indexOfLastNetworkChar(Ljava/lang/String;)I

    #@c
    move-result v4

    #@d
    .line 507
    .local v4, trimIndex:I
    add-int/lit8 v1, v4, 0x1

    #@f
    .local v1, i:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@12
    move-result v3

    #@13
    .line 508
    .local v3, s:I
    :goto_13
    if-ge v1, v3, :cond_25

    #@15
    .line 510
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@18
    move-result v0

    #@19
    .line 511
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_22

    #@1f
    .line 512
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    .line 508
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_13

    #@25
    .line 516
    .end local v0           #c:C
    :cond_25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    goto :goto_3
.end method

.method public static extractSpecialNumberPortion(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "number"

    #@0
    .prologue
    .line 2597
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_7

    #@6
    .line 2612
    :cond_6
    :goto_6
    return-object p0

    #@7
    .line 2602
    :cond_7
    sget-object v2, Landroid/telephony/PhoneNumberUtils;->KDDI_SPECIALNUMBER:[Ljava/lang/String;

    #@9
    array-length v1, v2

    #@a
    .line 2603
    .local v1, max:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v1, :cond_6

    #@d
    .line 2605
    sget-object v2, Landroid/telephony/PhoneNumberUtils;->KDDI_SPECIALNUMBER:[Ljava/lang/String;

    #@f
    aget-object v2, v2, v0

    #@11
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_24

    #@17
    .line 2607
    sget-object v2, Landroid/telephony/PhoneNumberUtils;->KDDI_SPECIALNUMBER:[Ljava/lang/String;

    #@19
    aget-object v2, v2, v0

    #@1b
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@22
    move-result-object p0

    #@23
    .line 2608
    goto :goto_6

    #@24
    .line 2603
    :cond_24
    add-int/lit8 v0, v0, 0x1

    #@26
    goto :goto_b
.end method

.method private static findDialableIndexFromPostDialStr(Ljava/lang/String;)I
    .registers 4
    .parameter "postDialStr"

    #@0
    .prologue
    .line 3307
    const/4 v1, 0x0

    #@1
    .local v1, index:I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v2

    #@5
    if-ge v1, v2, :cond_15

    #@7
    .line 3308
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v0

    #@b
    .line 3309
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isReallyDialable(C)Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_12

    #@11
    .line 3313
    .end local v0           #c:C
    .end local v1           #index:I
    :goto_11
    return v1

    #@12
    .line 3307
    .restart local v0       #c:C
    .restart local v1       #index:I
    :cond_12
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_1

    #@15
    .line 3313
    .end local v0           #c:C
    :cond_15
    const/4 v1, -0x1

    #@16
    goto :goto_11
.end method

.method public static formatJapaneseNumber(Landroid/text/Editable;)V
    .registers 1
    .parameter "text"

    #@0
    .prologue
    .line 1802
    invoke-static {p0}, Landroid/telephony/JapanesePhoneNumberFormatter;->format(Landroid/text/Editable;)V

    #@3
    .line 1803
    return-void
.end method

.method public static formatKoreanNumber(Landroid/text/Editable;)V
    .registers 1
    .parameter "text"

    #@0
    .prologue
    .line 1810
    invoke-static {p0}, Landroid/telephony/LGKoreanPhoneNumberFormatter;->format(Landroid/text/Editable;)V

    #@3
    .line 1811
    return-void
.end method

.method public static formatNanpNumber(Landroid/text/Editable;)V
    .registers 16
    .parameter "text"

    #@0
    .prologue
    .line 1679
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@3
    move-result v4

    #@4
    .line 1682
    .local v4, length:I
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@7
    move-result v12

    #@8
    if-eqz v12, :cond_27

    #@a
    .line 1683
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@d
    move-result-object v10

    #@e
    .line 1684
    .local v10, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    const/4 v12, 0x3

    #@f
    if-eq v4, v12, :cond_14

    #@11
    const/4 v12, 0x4

    #@12
    if-ne v4, v12, :cond_27

    #@14
    :cond_14
    invoke-virtual {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@17
    move-result-object v12

    #@18
    if-eqz v12, :cond_27

    #@1a
    invoke-virtual {v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@1d
    move-result-object v12

    #@1e
    const-string v13, "231"

    #@20
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v12

    #@24
    if-eqz v12, :cond_27

    #@26
    .line 1784
    .end local v10           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    :cond_26
    :goto_26
    return-void

    #@27
    .line 1690
    :cond_27
    const-string v12, "+1-nnn-nnn-nnnn"

    #@29
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@2c
    move-result v12

    #@2d
    if-gt v4, v12, :cond_26

    #@2f
    .line 1693
    const/4 v12, 0x5

    #@30
    if-le v4, v12, :cond_26

    #@32
    .line 1698
    const/4 v12, 0x0

    #@33
    invoke-interface {p0, v12, v4}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@36
    move-result-object v9

    #@37
    .line 1701
    .local v9, saved:Ljava/lang/CharSequence;
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->removeDashes(Landroid/text/Editable;)V

    #@3a
    .line 1702
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@3d
    move-result v4

    #@3e
    .line 1707
    const/4 v12, 0x3

    #@3f
    new-array v1, v12, [I

    #@41
    .line 1708
    .local v1, dashPositions:[I
    const/4 v5, 0x0

    #@42
    .line 1710
    .local v5, numDashes:I
    const/4 v11, 0x1

    #@43
    .line 1711
    .local v11, state:I
    const/4 v7, 0x0

    #@44
    .line 1712
    .local v7, numDigits:I
    const/4 v2, 0x0

    #@45
    .local v2, i:I
    move v6, v5

    #@46
    .end local v5           #numDashes:I
    .local v6, numDashes:I
    :goto_46
    if-ge v2, v4, :cond_88

    #@48
    .line 1713
    invoke-interface {p0, v2}, Landroid/text/Editable;->charAt(I)C

    #@4b
    move-result v0

    #@4c
    .line 1714
    .local v0, c:C
    packed-switch v0, :pswitch_data_ba

    #@4f
    .line 1758
    :cond_4f
    :pswitch_4f
    const/4 v12, 0x0

    #@50
    invoke-interface {p0, v12, v4, v9}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@53
    goto :goto_26

    #@54
    .line 1716
    :pswitch_54
    if-eqz v7, :cond_59

    #@56
    const/4 v12, 0x2

    #@57
    if-ne v11, v12, :cond_5f

    #@59
    .line 1717
    :cond_59
    const/4 v11, 0x3

    #@5a
    move v5, v6

    #@5b
    .line 1712
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    :goto_5b
    add-int/lit8 v2, v2, 0x1

    #@5d
    move v6, v5

    #@5e
    .end local v5           #numDashes:I
    .restart local v6       #numDashes:I
    goto :goto_46

    #@5f
    .line 1730
    :cond_5f
    :pswitch_5f
    const/4 v12, 0x2

    #@60
    if-ne v11, v12, :cond_67

    #@62
    .line 1732
    const/4 v12, 0x0

    #@63
    invoke-interface {p0, v12, v4, v9}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@66
    goto :goto_26

    #@67
    .line 1734
    :cond_67
    const/4 v12, 0x3

    #@68
    if-ne v11, v12, :cond_72

    #@6a
    .line 1736
    add-int/lit8 v5, v6, 0x1

    #@6c
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    aput v2, v1, v6

    #@6e
    .line 1741
    :goto_6e
    const/4 v11, 0x1

    #@6f
    .line 1742
    add-int/lit8 v7, v7, 0x1

    #@71
    .line 1743
    goto :goto_5b

    #@72
    .line 1737
    .end local v5           #numDashes:I
    .restart local v6       #numDashes:I
    :cond_72
    const/4 v12, 0x4

    #@73
    if-eq v11, v12, :cond_b8

    #@75
    const/4 v12, 0x3

    #@76
    if-eq v7, v12, :cond_7b

    #@78
    const/4 v12, 0x6

    #@79
    if-ne v7, v12, :cond_b8

    #@7b
    .line 1739
    :cond_7b
    add-int/lit8 v5, v6, 0x1

    #@7d
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    aput v2, v1, v6

    #@7f
    goto :goto_6e

    #@80
    .line 1746
    .end local v5           #numDashes:I
    .restart local v6       #numDashes:I
    :pswitch_80
    const/4 v11, 0x4

    #@81
    move v5, v6

    #@82
    .line 1747
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    goto :goto_5b

    #@83
    .line 1750
    .end local v5           #numDashes:I
    .restart local v6       #numDashes:I
    :pswitch_83
    if-nez v2, :cond_4f

    #@85
    .line 1752
    const/4 v11, 0x2

    #@86
    move v5, v6

    #@87
    .line 1753
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    goto :goto_5b

    #@88
    .line 1763
    .end local v0           #c:C
    .end local v5           #numDashes:I
    .restart local v6       #numDashes:I
    :cond_88
    const/4 v12, 0x7

    #@89
    if-ne v7, v12, :cond_b6

    #@8b
    .line 1765
    add-int/lit8 v5, v6, -0x1

    #@8d
    .line 1769
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    :goto_8d
    const/4 v2, 0x0

    #@8e
    :goto_8e
    if-ge v2, v5, :cond_9e

    #@90
    .line 1770
    aget v8, v1, v2

    #@92
    .line 1771
    .local v8, pos:I
    add-int v12, v8, v2

    #@94
    add-int v13, v8, v2

    #@96
    const-string v14, "-"

    #@98
    invoke-interface {p0, v12, v13, v14}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@9b
    .line 1769
    add-int/lit8 v2, v2, 0x1

    #@9d
    goto :goto_8e

    #@9e
    .line 1775
    .end local v8           #pos:I
    :cond_9e
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@a1
    move-result v3

    #@a2
    .line 1776
    .local v3, len:I
    :goto_a2
    if-lez v3, :cond_26

    #@a4
    .line 1777
    add-int/lit8 v12, v3, -0x1

    #@a6
    invoke-interface {p0, v12}, Landroid/text/Editable;->charAt(I)C

    #@a9
    move-result v12

    #@aa
    const/16 v13, 0x2d

    #@ac
    if-ne v12, v13, :cond_26

    #@ae
    .line 1778
    add-int/lit8 v12, v3, -0x1

    #@b0
    invoke-interface {p0, v12, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@b3
    .line 1779
    add-int/lit8 v3, v3, -0x1

    #@b5
    goto :goto_a2

    #@b6
    .end local v3           #len:I
    .end local v5           #numDashes:I
    .restart local v6       #numDashes:I
    :cond_b6
    move v5, v6

    #@b7
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    goto :goto_8d

    #@b8
    .end local v5           #numDashes:I
    .restart local v0       #c:C
    .restart local v6       #numDashes:I
    :cond_b8
    move v5, v6

    #@b9
    .end local v6           #numDashes:I
    .restart local v5       #numDashes:I
    goto :goto_6e

    #@ba
    .line 1714
    :pswitch_data_ba
    .packed-switch 0x2b
        :pswitch_83
        :pswitch_4f
        :pswitch_80
        :pswitch_4f
        :pswitch_4f
        :pswitch_5f
        :pswitch_54
        :pswitch_5f
        :pswitch_5f
        :pswitch_5f
        :pswitch_5f
        :pswitch_5f
        :pswitch_5f
        :pswitch_5f
        :pswitch_5f
    .end packed-switch
.end method

.method public static formatNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "source"

    #@0
    .prologue
    .line 1540
    new-instance v3, Landroid/text/SpannableStringBuilder;

    #@2
    invoke-direct {v3, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@5
    .line 1545
    .local v3, text:Landroid/text/SpannableStringBuilder;
    const-string v4, "KR"

    #@7
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_37

    #@d
    .line 1546
    const-string v4, "gsm.operator.iso-country"

    #@f
    const-string v5, ""

    #@11
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 1547
    .local v0, currIso:Ljava/lang/String;
    const-string v4, "KR"

    #@17
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_2b

    #@1d
    .line 1548
    sget-object v4, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    #@1f
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@22
    move-result v4

    #@23
    invoke-static {v3, v4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    #@26
    .line 1567
    .end local v0           #currIso:Ljava/lang/String;
    :goto_26
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    :goto_2a
    return-object v4

    #@2b
    .line 1550
    .restart local v0       #currIso:Ljava/lang/String;
    :cond_2b
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@2e
    move-result-object v4

    #@2f
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@32
    move-result v4

    #@33
    invoke-static {v3, v4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    #@36
    goto :goto_26

    #@37
    .line 1553
    .end local v0           #currIso:Ljava/lang/String;
    :cond_37
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@3a
    move-result v4

    #@3b
    if-eqz v4, :cond_6e

    #@3d
    .line 1554
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@40
    move-result-object v1

    #@41
    .line 1555
    .local v1, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    #@44
    move-result v2

    #@45
    .line 1556
    .local v2, strLength:I
    const/4 v4, 0x3

    #@46
    if-eq v2, v4, :cond_4b

    #@48
    const/4 v4, 0x4

    #@49
    if-ne v2, v4, :cond_62

    #@4b
    :cond_4b
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    if-eqz v4, :cond_62

    #@51
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    const-string v5, "231"

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v4

    #@5b
    if-eqz v4, :cond_62

    #@5d
    .line 1557
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    goto :goto_2a

    #@62
    .line 1560
    :cond_62
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@65
    move-result-object v4

    #@66
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@69
    move-result v4

    #@6a
    invoke-static {v3, v4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    #@6d
    goto :goto_26

    #@6e
    .line 1564
    .end local v1           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .end local v2           #strLength:I
    :cond_6e
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@71
    move-result-object v4

    #@72
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@75
    move-result v4

    #@76
    invoke-static {v3, v4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    #@79
    goto :goto_26
.end method

.method public static formatNumber(Ljava/lang/String;I)Ljava/lang/String;
    .registers 7
    .parameter "source"
    .parameter "defaultFormattingType"

    #@0
    .prologue
    .line 1582
    new-instance v2, Landroid/text/SpannableStringBuilder;

    #@2
    invoke-direct {v2, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@5
    .line 1584
    .local v2, text:Landroid/text/SpannableStringBuilder;
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_30

    #@b
    .line 1585
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@e
    move-result-object v0

    #@f
    .line 1586
    .local v0, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    #@12
    move-result v1

    #@13
    .line 1587
    .local v1, strLength:I
    const/4 v3, 0x3

    #@14
    if-eq v1, v3, :cond_19

    #@16
    const/4 v3, 0x4

    #@17
    if-ne v1, v3, :cond_30

    #@19
    :cond_19
    invoke-virtual {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    if-eqz v3, :cond_30

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    const-string v4, "231"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_30

    #@2b
    .line 1588
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    .line 1593
    .end local v0           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .end local v1           #strLength:I
    :goto_2f
    return-object v3

    #@30
    .line 1592
    :cond_30
    invoke-static {v2, p1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    #@33
    .line 1593
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    goto :goto_2f
.end method

.method public static formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "phoneNumber"
    .parameter "defaultCountryIso"

    #@0
    .prologue
    .line 1891
    const-string v5, "#"

    #@2
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_10

    #@8
    const-string v5, "*"

    #@a
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_11

    #@10
    .line 1920
    .end local p0
    :cond_10
    :goto_10
    return-object p0

    #@11
    .line 1895
    .restart local p0
    :cond_11
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@14
    move-result-object v4

    #@15
    .line 1896
    .local v4, util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    const/4 v1, 0x0

    #@16
    .line 1899
    .local v1, result:Ljava/lang/String;
    :try_start_16
    const-string v5, "KR"

    #@18
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_58

    #@1e
    const-string v5, "KR"

    #@20
    invoke-virtual {v5, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@23
    move-result v5

    #@24
    if-nez v5, :cond_58

    #@26
    .line 1900
    sget-object v5, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    #@28
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    #@2b
    move-result v5

    #@2c
    invoke-static {p0, v5}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_2f
    .catch Lcom/android/i18n/phonenumbers/NumberParseException; {:try_start_16 .. :try_end_2f} :catch_61

    #@2f
    move-result-object v1

    #@30
    .line 1912
    :goto_30
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@33
    move-result v5

    #@34
    if-eqz v5, :cond_56

    #@36
    .line 1913
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@39
    move-result-object v2

    #@3a
    .line 1914
    .local v2, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3d
    move-result v3

    #@3e
    .line 1915
    .local v3, strLength:I
    const/4 v5, 0x3

    #@3f
    if-eq v3, v5, :cond_44

    #@41
    const/4 v5, 0x4

    #@42
    if-ne v3, v5, :cond_56

    #@44
    :cond_44
    invoke-virtual {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    if-eqz v5, :cond_56

    #@4a
    invoke-virtual {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    const-string v6, "231"

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v5

    #@54
    if-nez v5, :cond_10

    #@56
    .end local v2           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .end local v3           #strLength:I
    :cond_56
    move-object p0, v1

    #@57
    .line 1920
    goto :goto_10

    #@58
    .line 1903
    :cond_58
    :try_start_58
    invoke-virtual {v4, p0, p1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->parseAndKeepRawInput(Ljava/lang/String;Ljava/lang/String;)Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;

    #@5b
    move-result-object v0

    #@5c
    .line 1904
    .local v0, pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v4, v0, p1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->formatInOriginalFormat(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5f
    .catch Lcom/android/i18n/phonenumbers/NumberParseException; {:try_start_58 .. :try_end_5f} :catch_61

    #@5f
    move-result-object v1

    #@60
    goto :goto_30

    #@61
    .line 1908
    .end local v0           #pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :catch_61
    move-exception v5

    #@62
    goto :goto_30
.end method

.method public static formatNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "phoneNumber"
    .parameter "phoneNumberE164"
    .parameter "defaultCountryIso"

    #@0
    .prologue
    .line 1945
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v1

    #@4
    .line 1946
    .local v1, len:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_15

    #@7
    .line 1947
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v8

    #@b
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@e
    move-result v8

    #@f
    if-nez v8, :cond_12

    #@11
    .line 1978
    .end local p0
    :cond_11
    :goto_11
    return-object p0

    #@12
    .line 1946
    .restart local p0
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_5

    #@15
    .line 1951
    :cond_15
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@18
    move-result-object v7

    #@19
    .line 1953
    .local v7, util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    if-eqz p1, :cond_4b

    #@1b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1e
    move-result v8

    #@1f
    const/4 v9, 0x2

    #@20
    if-lt v8, v9, :cond_4b

    #@22
    const/4 v8, 0x0

    #@23
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    #@26
    move-result v8

    #@27
    const/16 v9, 0x2b

    #@29
    if-ne v8, v9, :cond_4b

    #@2b
    .line 1958
    :try_start_2b
    const-string v8, "ZZ"

    #@2d
    invoke-virtual {v7, p1, v8}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;

    #@30
    move-result-object v2

    #@31
    .line 1959
    .local v2, pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v7, v2}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getRegionCodeForNumber(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;)Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    .line 1960
    .local v3, regionCode:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@38
    move-result v8

    #@39
    if-nez v8, :cond_4b

    #@3b
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v8

    #@3f
    const/4 v9, 0x1

    #@40
    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_47
    .catch Lcom/android/i18n/phonenumbers/NumberParseException; {:try_start_2b .. :try_end_47} :catch_7b

    #@47
    move-result v8

    #@48
    if-gtz v8, :cond_4b

    #@4a
    .line 1963
    move-object p2, v3

    #@4b
    .line 1968
    .end local v2           #pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .end local v3           #regionCode:Ljava/lang/String;
    :cond_4b
    :goto_4b
    invoke-static {p0, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    .line 1970
    .local v4, result:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@52
    move-result v8

    #@53
    if-eqz v8, :cond_75

    #@55
    .line 1971
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@58
    move-result-object v5

    #@59
    .line 1972
    .local v5, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5c
    move-result v6

    #@5d
    .line 1973
    .local v6, strLength:I
    const/4 v8, 0x3

    #@5e
    if-eq v6, v8, :cond_63

    #@60
    const/4 v8, 0x4

    #@61
    if-ne v6, v8, :cond_75

    #@63
    :cond_63
    invoke-virtual {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@66
    move-result-object v8

    #@67
    if-eqz v8, :cond_75

    #@69
    invoke-virtual {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@6c
    move-result-object v8

    #@6d
    const-string v9, "231"

    #@6f
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v8

    #@73
    if-nez v8, :cond_11

    #@75
    .line 1978
    .end local v5           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .end local v6           #strLength:I
    :cond_75
    if-eqz v4, :cond_79

    #@77
    .end local v4           #result:Ljava/lang/String;
    :goto_77
    move-object p0, v4

    #@78
    goto :goto_11

    #@79
    .restart local v4       #result:Ljava/lang/String;
    :cond_79
    move-object v4, p0

    #@7a
    goto :goto_77

    #@7b
    .line 1965
    .end local v4           #result:Ljava/lang/String;
    :catch_7b
    move-exception v8

    #@7c
    goto :goto_4b
.end method

.method public static formatNumber(Landroid/text/Editable;I)V
    .registers 11
    .parameter "text"
    .parameter "defaultFormattingType"

    #@0
    .prologue
    const/16 v8, 0x31

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v6, 0x2

    #@4
    const/4 v5, 0x1

    #@5
    .line 1618
    move v0, p1

    #@6
    .line 1620
    .local v0, formatType:I
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@9
    move-result v3

    #@a
    if-le v3, v6, :cond_1c

    #@c
    const/4 v3, 0x0

    #@d
    invoke-interface {p0, v3}, Landroid/text/Editable;->charAt(I)C

    #@10
    move-result v3

    #@11
    const/16 v4, 0x2b

    #@13
    if-ne v3, v4, :cond_1c

    #@15
    .line 1621
    invoke-interface {p0, v5}, Landroid/text/Editable;->charAt(I)C

    #@18
    move-result v3

    #@19
    if-ne v3, v8, :cond_46

    #@1b
    .line 1622
    const/4 v0, 0x1

    #@1c
    .line 1632
    :cond_1c
    :goto_1c
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_42

    #@22
    .line 1633
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@25
    move-result-object v1

    #@26
    .line 1634
    .local v1, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@29
    move-result v2

    #@2a
    .line 1635
    .local v2, strLength:I
    if-eq v2, v7, :cond_2f

    #@2c
    const/4 v3, 0x4

    #@2d
    if-ne v2, v3, :cond_42

    #@2f
    :cond_2f
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    if-eqz v3, :cond_42

    #@35
    invoke-virtual {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    const-string v4, "231"

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_42

    #@41
    .line 1636
    const/4 v0, 0x0

    #@42
    .line 1641
    .end local v1           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    .end local v2           #strLength:I
    :cond_42
    packed-switch v0, :pswitch_data_6e

    #@45
    .line 1657
    :goto_45
    return-void

    #@46
    .line 1623
    :cond_46
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@49
    move-result v3

    #@4a
    if-lt v3, v7, :cond_5c

    #@4c
    invoke-interface {p0, v5}, Landroid/text/Editable;->charAt(I)C

    #@4f
    move-result v3

    #@50
    const/16 v4, 0x38

    #@52
    if-ne v3, v4, :cond_5c

    #@54
    invoke-interface {p0, v6}, Landroid/text/Editable;->charAt(I)C

    #@57
    move-result v3

    #@58
    if-ne v3, v8, :cond_5c

    #@5a
    .line 1625
    const/4 v0, 0x2

    #@5b
    goto :goto_1c

    #@5c
    .line 1627
    :cond_5c
    const/4 v0, 0x0

    #@5d
    goto :goto_1c

    #@5e
    .line 1643
    :pswitch_5e
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->formatNanpNumber(Landroid/text/Editable;)V

    #@61
    goto :goto_45

    #@62
    .line 1646
    :pswitch_62
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->formatJapaneseNumber(Landroid/text/Editable;)V

    #@65
    goto :goto_45

    #@66
    .line 1650
    :pswitch_66
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->formatKoreanNumber(Landroid/text/Editable;)V

    #@69
    goto :goto_45

    #@6a
    .line 1654
    :pswitch_6a
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->removeDashes(Landroid/text/Editable;)V

    #@6d
    goto :goto_45

    #@6e
    .line 1641
    :pswitch_data_6e
    .packed-switch 0x0
        :pswitch_6a
        :pswitch_5e
        :pswitch_62
        :pswitch_66
    .end packed-switch
.end method

.method public static formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "phoneNumber"
    .parameter "defaultCountryIso"

    #@0
    .prologue
    .line 1862
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@3
    move-result-object v2

    #@4
    .line 1863
    .local v2, util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    const/4 v1, 0x0

    #@5
    .line 1865
    .local v1, result:Ljava/lang/String;
    :try_start_5
    invoke-virtual {v2, p0, p1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;

    #@8
    move-result-object v0

    #@9
    .line 1866
    .local v0, pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v2, v0}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->isValidNumber(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_15

    #@f
    .line 1867
    sget-object v3, Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->E164:Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    #@11
    invoke-virtual {v2, v0, v3}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->format(Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/android/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;
    :try_end_14
    .catch Lcom/android/i18n/phonenumbers/NumberParseException; {:try_start_5 .. :try_end_14} :catch_16

    #@14
    move-result-object v1

    #@15
    .line 1871
    .end local v0           #pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :cond_15
    :goto_15
    return-object v1

    #@16
    .line 1869
    :catch_16
    move-exception v3

    #@17
    goto :goto_15
.end method

.method private static getDefaultIdp()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3155
    const/4 v0, 0x0

    #@1
    .line 3156
    .local v0, ps:Ljava/lang/String;
    const-string/jumbo v1, "ro.cdma.idpstring"

    #@4
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    .line 3157
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_f

    #@d
    .line 3158
    const-string v0, "011"

    #@f
    .line 3160
    :cond_f
    return-object v0
.end method

.method public static getFormatTypeForLocale(Ljava/util/Locale;)I
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 1604
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1606
    .local v0, country:Ljava/lang/String;
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeFromCountryCode(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    return v1
.end method

.method private static getFormatTypeFromCountryCode(Ljava/lang/String;)I
    .registers 4
    .parameter "country"

    #@0
    .prologue
    .line 3173
    sget-object v2, Landroid/telephony/PhoneNumberUtils;->NANP_COUNTRIES:[Ljava/lang/String;

    #@2
    array-length v1, v2

    #@3
    .line 3174
    .local v1, length:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_15

    #@6
    .line 3175
    sget-object v2, Landroid/telephony/PhoneNumberUtils;->NANP_COUNTRIES:[Ljava/lang/String;

    #@8
    aget-object v2, v2, v0

    #@a
    invoke-virtual {v2, p0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_12

    #@10
    .line 3176
    const/4 v2, 0x1

    #@11
    .line 3187
    :goto_11
    return v2

    #@12
    .line 3174
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_4

    #@15
    .line 3179
    :cond_15
    const-string/jumbo v2, "jp"

    #@18
    invoke-virtual {v2, p0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_20

    #@1e
    .line 3180
    const/4 v2, 0x2

    #@1f
    goto :goto_11

    #@20
    .line 3183
    :cond_20
    const-string/jumbo v2, "kr"

    #@23
    invoke-virtual {v2, p0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_2b

    #@29
    .line 3184
    const/4 v2, 0x3

    #@2a
    goto :goto_11

    #@2b
    .line 3187
    :cond_2b
    const/4 v2, 0x0

    #@2c
    goto :goto_11
.end method

.method public static getN11OrSpecialNumberString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 3982
    const/4 v1, 0x0

    #@2
    .line 3984
    .local v1, resString:Ljava/lang/String;
    if-nez p1, :cond_6

    #@4
    move-object v2, v1

    #@5
    .line 4016
    .end local v1           #resString:Ljava/lang/String;
    .local v2, resString:Ljava/lang/String;
    :goto_5
    return-object v2

    #@6
    .line 3988
    .end local v2           #resString:Ljava/lang/String;
    .restart local v1       #resString:Ljava/lang/String;
    :cond_6
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object p1

    #@a
    .line 3991
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->NetworkCode:Ljava/lang/String;

    #@c
    const-string v4, "311870"

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v3

    #@12
    if-nez v3, :cond_1e

    #@14
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->NetworkCode:Ljava/lang/String;

    #@16
    const-string v4, "311490"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_3d

    #@1e
    .line 3993
    :cond_1e
    const/4 v0, 0x0

    #@1f
    .local v0, i:I
    :goto_1f
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@21
    array-length v3, v3

    #@22
    if-ge v0, v3, :cond_5c

    #@24
    .line 3994
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@26
    aget-object v3, v3, v0

    #@28
    iget-object v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;->number:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v3

    #@2e
    if-ne v3, v5, :cond_3a

    #@30
    .line 3995
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@32
    aget-object v3, v3, v0

    #@34
    iget v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;->resIDOfNums:I

    #@36
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    .line 3993
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_1f

    #@3d
    .line 4002
    .end local v0           #i:I
    :cond_3d
    const/4 v0, 0x0

    #@3e
    .restart local v0       #i:I
    :goto_3e
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@40
    array-length v3, v3

    #@41
    if-ge v0, v3, :cond_5c

    #@43
    .line 4003
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@45
    aget-object v3, v3, v0

    #@47
    iget-object v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;->number:Ljava/lang/String;

    #@49
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v3

    #@4d
    if-ne v3, v5, :cond_59

    #@4f
    .line 4004
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@51
    aget-object v3, v3, v0

    #@53
    iget v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;->resIDOfNums:I

    #@55
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    .line 4002
    :cond_59
    add-int/lit8 v0, v0, 0x1

    #@5b
    goto :goto_3e

    #@5c
    .line 4010
    :cond_5c
    const/4 v0, 0x0

    #@5d
    :goto_5d
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@5f
    array-length v3, v3

    #@60
    if-ge v0, v3, :cond_77

    #@62
    .line 4011
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@64
    aget-object v3, v3, v0

    #@66
    iget-object v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;->number:Ljava/lang/String;

    #@68
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v3

    #@6c
    if-ne v3, v5, :cond_74

    #@6e
    .line 4012
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@70
    aget-object v3, v3, v0

    #@72
    iget-object v1, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;->resString:Ljava/lang/String;

    #@74
    .line 4010
    :cond_74
    add-int/lit8 v0, v0, 0x1

    #@76
    goto :goto_5d

    #@77
    :cond_77
    move-object v2, v1

    #@78
    .line 4016
    .end local v1           #resString:Ljava/lang/String;
    .restart local v2       #resString:Ljava/lang/String;
    goto :goto_5
.end method

.method public static getNumberFromIntent(Landroid/content/Intent;Landroid/content/Context;)Ljava/lang/String;
    .registers 15
    .parameter "intent"
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 212
    const/4 v8, 0x0

    #@2
    .line 214
    .local v8, number:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@5
    move-result-object v1

    #@6
    .line 215
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@9
    move-result-object v10

    #@a
    .line 217
    .local v10, scheme:Ljava/lang/String;
    const-string/jumbo v0, "tel"

    #@d
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1c

    #@13
    const-string/jumbo v0, "sip"

    #@16
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_21

    #@1c
    .line 218
    :cond_1c
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 261
    :cond_20
    :goto_20
    return-object v3

    #@21
    .line 223
    :cond_21
    const-string/jumbo v0, "voicemail"

    #@24
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_55

    #@2a
    .line 224
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_4c

    #@34
    .line 225
    const-string/jumbo v0, "subscription"

    #@37
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->getDefaultSubscription()I

    #@3e
    move-result v2

    #@3f
    invoke-virtual {p0, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@42
    move-result v11

    #@43
    .line 227
    .local v11, subscription:I
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0, v11}, Landroid/telephony/MSimTelephonyManager;->getCompleteVoiceMailNumber(I)Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    goto :goto_20

    #@4c
    .line 230
    .end local v11           #subscription:I
    :cond_4c
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCompleteVoiceMailNumber()Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    goto :goto_20

    #@55
    .line 233
    :cond_55
    if-eqz p1, :cond_20

    #@57
    .line 237
    invoke-virtual {p0, p1}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    #@5a
    move-result-object v12

    #@5b
    .line 238
    .local v12, type:Ljava/lang/String;
    const/4 v9, 0x0

    #@5c
    .line 241
    .local v9, phoneColumn:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    .line 242
    .local v6, authority:Ljava/lang/String;
    const-string v0, "contacts"

    #@62
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v0

    #@66
    if-eqz v0, :cond_90

    #@68
    .line 243
    const-string/jumbo v9, "number"

    #@6b
    .line 248
    :cond_6b
    :goto_6b
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6e
    move-result-object v0

    #@6f
    const/4 v2, 0x1

    #@70
    new-array v2, v2, [Ljava/lang/String;

    #@72
    const/4 v4, 0x0

    #@73
    aput-object v9, v2, v4

    #@75
    move-object v4, v3

    #@76
    move-object v5, v3

    #@77
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@7a
    move-result-object v7

    #@7b
    .line 251
    .local v7, c:Landroid/database/Cursor;
    if-eqz v7, :cond_8e

    #@7d
    .line 253
    :try_start_7d
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@80
    move-result v0

    #@81
    if-eqz v0, :cond_8b

    #@83
    .line 254
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@86
    move-result v0

    #@87
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_8a
    .catchall {:try_start_7d .. :try_end_8a} :catchall_9b

    #@8a
    move-result-object v8

    #@8b
    .line 257
    :cond_8b
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8e
    :cond_8e
    move-object v3, v8

    #@8f
    .line 261
    goto :goto_20

    #@90
    .line 244
    .end local v7           #c:Landroid/database/Cursor;
    :cond_90
    const-string v0, "com.android.contacts"

    #@92
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95
    move-result v0

    #@96
    if-eqz v0, :cond_6b

    #@98
    .line 245
    const-string v9, "data1"

    #@9a
    goto :goto_6b

    #@9b
    .line 257
    .restart local v7       #c:Landroid/database/Cursor;
    :catchall_9b
    move-exception v0

    #@9c
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@9f
    throw v0
.end method

.method public static varargs getStartWithPrefix(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/String;)Z
    .registers 10
    .parameter "number"
    .parameter "output"
    .parameter "prefixs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4052
    if-nez p2, :cond_4

    #@3
    .line 4080
    :cond_3
    :goto_3
    return v4

    #@4
    .line 4057
    :cond_4
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    array-length v5, p2

    #@6
    if-ge v0, v5, :cond_3

    #@8
    .line 4059
    new-instance v3, Ljava/lang/StringBuffer;

    #@a
    aget-object v5, p2, v0

    #@c
    invoke-direct {v3, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@f
    .line 4060
    .local v3, work:Ljava/lang/StringBuffer;
    aget-object v5, p2, v0

    #@11
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@14
    move-result v1

    #@15
    .local v1, ii:I
    :goto_15
    if-ltz v1, :cond_3a

    #@17
    .line 4063
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    #@1a
    move-result v5

    #@1b
    aget-object v6, p2, v0

    #@1d
    invoke-virtual {v3, v4, v5, v6}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    .line 4066
    if-lez v1, :cond_27

    #@22
    .line 4067
    sget-object v5, Landroid/telephony/PhoneNumberUtils;->KDDI_LINECODE:Ljava/lang/String;

    #@24
    invoke-virtual {v3, v1, v5}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    #@27
    .line 4070
    :cond_27
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    .line 4071
    .local v2, prefix:Ljava/lang/String;
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_37

    #@31
    .line 4073
    if-eqz p1, :cond_35

    #@33
    aput-object v2, p1, v4

    #@35
    .line 4075
    :cond_35
    const/4 v4, 0x1

    #@36
    goto :goto_3

    #@37
    .line 4060
    :cond_37
    add-int/lit8 v1, v1, -0x1

    #@39
    goto :goto_15

    #@3a
    .line 4057
    .end local v2           #prefix:Ljava/lang/String;
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_5
.end method

.method public static getStrippedReversed(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 883
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 885
    .local v0, np:Ljava/lang/String;
    if-nez v0, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    .line 887
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@b
    move-result v1

    #@c
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->internalGetStrippedReversed(Ljava/lang/String;I)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    goto :goto_7
.end method

.method public static getUsernameFromUriNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "number"

    #@0
    .prologue
    .line 3261
    const/16 v1, 0x40

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    #@5
    move-result v0

    #@6
    .line 3262
    .local v0, delimiterIndex:I
    if-gez v0, :cond_e

    #@8
    .line 3263
    const-string v1, "%40"

    #@a
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    .line 3265
    :cond_e
    if-gez v0, :cond_36

    #@10
    .line 3267
    sget-boolean v1, Landroid/telephony/PhoneNumberUtils;->ENABLE_PRIVACY_LOG_CALL:Z

    #@12
    if-eqz v1, :cond_32

    #@14
    const-string v1, "PhoneNumberUtils"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "getUsernameFromUriNumber: no delimiter found in SIP addr \'"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, "\'"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 3270
    :cond_32
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@35
    move-result v0

    #@36
    .line 3272
    :cond_36
    const/4 v1, 0x0

    #@37
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    return-object v1
.end method

.method private static getUsimType()I
    .registers 4

    #@0
    .prologue
    .line 4022
    const-string v1, "PhoneNumberUtils"

    #@2
    const-string v2, "getUsimType()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 4024
    const-string v1, "gsm.sim.type"

    #@9
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 4025
    .local v0, gsmSimType:Ljava/lang/String;
    const-string v1, "PhoneNumberUtils"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "getUsimType() "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 4027
    const-string/jumbo v1, "skt"

    #@28
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_30

    #@2e
    const/4 v1, 0x1

    #@2f
    .line 4033
    :goto_2f
    return v1

    #@30
    .line 4028
    :cond_30
    const-string/jumbo v1, "kt"

    #@33
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_3b

    #@39
    const/4 v1, 0x2

    #@3a
    goto :goto_2f

    #@3b
    .line 4029
    :cond_3b
    const-string/jumbo v1, "lgu"

    #@3e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_46

    #@44
    const/4 v1, 0x5

    #@45
    goto :goto_2f

    #@46
    .line 4030
    :cond_46
    const-string/jumbo v1, "test"

    #@49
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v1

    #@4d
    if-eqz v1, :cond_51

    #@4f
    const/4 v1, 0x4

    #@50
    goto :goto_2f

    #@51
    .line 4031
    :cond_51
    const-string/jumbo v1, "unknown"

    #@54
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v1

    #@58
    if-eqz v1, :cond_5c

    #@5a
    const/4 v1, 0x3

    #@5b
    goto :goto_2f

    #@5c
    .line 4033
    :cond_5c
    const/4 v1, 0x0

    #@5d
    goto :goto_2f
.end method

.method private static indexOfLastNetworkChar(Ljava/lang/String;)I
    .registers 6
    .parameter "a"

    #@0
    .prologue
    .line 476
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    .line 478
    .local v0, origLength:I
    const/16 v4, 0x2c

    #@6
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    #@9
    move-result v1

    #@a
    .line 479
    .local v1, pIndex:I
    const/16 v4, 0x3b

    #@c
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    #@f
    move-result v3

    #@10
    .line 481
    .local v3, wIndex:I
    invoke-static {v1, v3}, Landroid/telephony/PhoneNumberUtils;->minPositive(II)I

    #@13
    move-result v2

    #@14
    .line 483
    .local v2, trimIndex:I
    if-gez v2, :cond_19

    #@16
    .line 484
    add-int/lit8 v4, v0, -0x1

    #@18
    .line 486
    :goto_18
    return v4

    #@19
    :cond_19
    add-int/lit8 v4, v2, -0x1

    #@1b
    goto :goto_18
.end method

.method private static internalCalledPartyBCDFragmentToString(Ljava/lang/StringBuilder;[BII)V
    .registers 9
    .parameter "sb"
    .parameter "bytes"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 1168
    move v2, p2

    #@1
    .local v2, i:I
    :goto_1
    add-int v3, p3, p2

    #@3
    if-ge v2, v3, :cond_10

    #@5
    .line 1172
    aget-byte v3, p1, v2

    #@7
    and-int/lit8 v3, v3, 0xf

    #@9
    int-to-byte v3, v3

    #@a
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->bcdToChar(B)C

    #@d
    move-result v1

    #@e
    .line 1174
    .local v1, c:C
    if-nez v1, :cond_11

    #@10
    .line 1201
    .end local v1           #c:C
    :cond_10
    return-void

    #@11
    .line 1177
    .restart local v1       #c:C
    :cond_11
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 1186
    aget-byte v3, p1, v2

    #@16
    shr-int/lit8 v3, v3, 0x4

    #@18
    and-int/lit8 v3, v3, 0xf

    #@1a
    int-to-byte v0, v3

    #@1b
    .line 1188
    .local v0, b:B
    const/16 v3, 0xf

    #@1d
    if-ne v0, v3, :cond_25

    #@1f
    add-int/lit8 v3, v2, 0x1

    #@21
    add-int v4, p3, p2

    #@23
    if-eq v3, v4, :cond_10

    #@25
    .line 1193
    :cond_25
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->bcdToChar(B)C

    #@28
    move-result v1

    #@29
    .line 1194
    if-eqz v1, :cond_10

    #@2b
    .line 1198
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    .line 1168
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1
.end method

.method private static internalGetStrippedReversed(Ljava/lang/String;I)Ljava/lang/String;
    .registers 8
    .parameter "np"
    .parameter "numDigits"

    #@0
    .prologue
    .line 896
    if-nez p0, :cond_4

    #@2
    const/4 v5, 0x0

    #@3
    .line 909
    :goto_3
    return-object v5

    #@4
    .line 898
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 899
    .local v3, ret:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@c
    move-result v2

    #@d
    .line 901
    .local v2, length:I
    add-int/lit8 v1, v2, -0x1

    #@f
    .local v1, i:I
    move v4, v2

    #@10
    .line 902
    .local v4, s:I
    :goto_10
    if-ltz v1, :cond_20

    #@12
    sub-int v5, v4, v1

    #@14
    if-gt v5, p1, :cond_20

    #@16
    .line 904
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@19
    move-result v0

    #@1a
    .line 906
    .local v0, c:C
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1d
    .line 902
    add-int/lit8 v1, v1, -0x1

    #@1f
    goto :goto_10

    #@20
    .line 909
    .end local v0           #c:C
    :cond_20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v5

    #@24
    goto :goto_3
.end method

.method public static final is12Key(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 146
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x39

    #@6
    if-le p0, v0, :cond_10

    #@8
    :cond_8
    const/16 v0, 0x2a

    #@a
    if-eq p0, v0, :cond_10

    #@c
    const/16 v0, 0x23

    #@e
    if-ne p0, v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public static is611DialNumber(Ljava/lang/String;)Z
    .registers 3
    .parameter "number"

    #@0
    .prologue
    .line 3855
    const-string v0, "*611"

    #@2
    .line 3856
    .local v0, is611Check:Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_10

    #@8
    invoke-static {p0, v0}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_10

    #@e
    .line 3857
    const/4 v1, 0x1

    #@f
    .line 3859
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method private static isCountryCallingCode(I)Z
    .registers 2
    .parameter "countryCallingCodeCandidate"

    #@0
    .prologue
    .line 3684
    if-lez p0, :cond_e

    #@2
    sget v0, Landroid/telephony/PhoneNumberUtils;->CCC_LENGTH:I

    #@4
    if-ge p0, v0, :cond_e

    #@6
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->COUNTRY_CALLING_CALL:[Z

    #@8
    aget-boolean v0, v0, p0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public static final isDialable(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 152
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x39

    #@6
    if-le p0, v0, :cond_18

    #@8
    :cond_8
    const/16 v0, 0x2a

    #@a
    if-eq p0, v0, :cond_18

    #@c
    const/16 v0, 0x23

    #@e
    if-eq p0, v0, :cond_18

    #@10
    const/16 v0, 0x2b

    #@12
    if-eq p0, v0, :cond_18

    #@14
    const/16 v0, 0x4e

    #@16
    if-ne p0, v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method private static isDialable(Ljava/lang/String;)Z
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 1384
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v0

    #@5
    .local v0, count:I
    :goto_5
    if-ge v1, v0, :cond_16

    #@7
    .line 1385
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v2

    #@b
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_13

    #@11
    .line 1386
    const/4 v2, 0x0

    #@12
    .line 1389
    :goto_12
    return v2

    #@13
    .line 1384
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_5

    #@16
    .line 1389
    :cond_16
    const/4 v2, 0x1

    #@17
    goto :goto_12
.end method

.method public static isEmergencyNumber(Ljava/lang/String;)Z
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 2061
    const/4 v0, 0x1

    #@1
    invoke-static {p0, v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumberInternal(Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static isEmergencyNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "number"
    .parameter "defaultCountryIso"

    #@0
    .prologue
    .line 2128
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumberInternal(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method private static isEmergencyNumberInternal(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 14
    .parameter "number"
    .parameter "defaultCountryIso"
    .parameter "useExactMatch"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 2178
    if-nez p0, :cond_5

    #@4
    .line 2289
    :cond_4
    :goto_4
    return v7

    #@5
    .line 2187
    :cond_5
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    #@8
    move-result v9

    #@9
    if-nez v9, :cond_4

    #@b
    .line 2193
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object p0

    #@f
    .line 2197
    const-string v9, "JP"

    #@11
    const-string v10, "KDDI"

    #@13
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@16
    move-result v9

    #@17
    if-eqz v9, :cond_1e

    #@19
    .line 2198
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isKDDIEmergencyNumber(Ljava/lang/String;)Z

    #@1c
    move-result v7

    #@1d
    goto :goto_4

    #@1e
    .line 2203
    :cond_1e
    const-string v9, "JP"

    #@20
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@23
    move-result v9

    #@24
    if-eqz v9, :cond_2b

    #@26
    .line 2204
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isJapanEmergencyNumber(Ljava/lang/String;)Z

    #@29
    move-result v7

    #@2a
    goto :goto_4

    #@2b
    .line 2207
    :cond_2b
    const-string v9, "KR"

    #@2d
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@30
    move-result v9

    #@31
    if-eqz v9, :cond_38

    #@33
    .line 2213
    invoke-static {p0, v8}, Landroid/telephony/PhoneNumberUtils;->isKoreaEmergencyNumber(Ljava/lang/String;Z)Z

    #@36
    move-result v7

    #@37
    goto :goto_4

    #@38
    .line 2219
    :cond_38
    const-string/jumbo v9, "ril.ecclist"

    #@3b
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    .line 2222
    .local v5, numbers:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getEccListMerged(Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    .line 2236
    const/4 v9, 0x0

    #@44
    const-string v10, "ecc_number_in_hidden_menu"

    #@46
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@49
    move-result v9

    #@4a
    if-eqz v9, :cond_61

    #@4c
    .line 2237
    const-string/jumbo v9, "persist.service.ecc.hiddenmenu"

    #@4f
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    .line 2238
    .local v0, HiddenMenuEccNumber:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@56
    move-result v9

    #@57
    if-nez v9, :cond_61

    #@59
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v9

    #@5d
    if-eqz v9, :cond_61

    #@5f
    move v7, v8

    #@60
    .line 2240
    goto :goto_4

    #@61
    .line 2245
    .end local v0           #HiddenMenuEccNumber:Ljava/lang/String;
    :cond_61
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@64
    move-result v9

    #@65
    if-nez v9, :cond_9a

    #@67
    .line 2248
    const-string v9, ","

    #@69
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    .local v1, arr$:[Ljava/lang/String;
    array-length v4, v1

    #@6e
    .local v4, len$:I
    const/4 v3, 0x0

    #@6f
    .local v3, i$:I
    :goto_6f
    if-ge v3, v4, :cond_91

    #@71
    aget-object v2, v1, v3

    #@73
    .line 2251
    .local v2, emergencyNum:Ljava/lang/String;
    if-nez p2, :cond_7d

    #@75
    const-string v9, "BR"

    #@77
    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7a
    move-result v9

    #@7b
    if-eqz v9, :cond_85

    #@7d
    .line 2252
    :cond_7d
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@80
    move-result v9

    #@81
    if-eqz v9, :cond_8e

    #@83
    move v7, v8

    #@84
    .line 2253
    goto :goto_4

    #@85
    .line 2256
    :cond_85
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@88
    move-result v9

    #@89
    if-eqz v9, :cond_8e

    #@8b
    move v7, v8

    #@8c
    .line 2257
    goto/16 :goto_4

    #@8e
    .line 2248
    :cond_8e
    add-int/lit8 v3, v3, 0x1

    #@90
    goto :goto_6f

    #@91
    .line 2263
    .end local v2           #emergencyNum:Ljava/lang/String;
    :cond_91
    invoke-static {p0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->checkEccIdleMode(Ljava/lang/String;)Z

    #@94
    move-result v9

    #@95
    if-eqz v9, :cond_4

    #@97
    move v7, v8

    #@98
    .line 2267
    goto/16 :goto_4

    #@9a
    .line 2274
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_9a
    const-string v9, "PhoneNumberUtils"

    #@9c
    const-string v10, "System property doesn\'t provide any emergency numbers. Use embedded logic for determining ones."

    #@9e
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 2278
    if-eqz p1, :cond_b6

    #@a3
    .line 2279
    new-instance v6, Lcom/android/i18n/phonenumbers/ShortNumberUtil;

    #@a5
    invoke-direct {v6}, Lcom/android/i18n/phonenumbers/ShortNumberUtil;-><init>()V

    #@a8
    .line 2280
    .local v6, util:Lcom/android/i18n/phonenumbers/ShortNumberUtil;
    if-eqz p2, :cond_b0

    #@aa
    .line 2281
    invoke-virtual {v6, p0, p1}, Lcom/android/i18n/phonenumbers/ShortNumberUtil;->isEmergencyNumber(Ljava/lang/String;Ljava/lang/String;)Z

    #@ad
    move-result v7

    #@ae
    goto/16 :goto_4

    #@b0
    .line 2283
    :cond_b0
    invoke-virtual {v6, p0, p1}, Lcom/android/i18n/phonenumbers/ShortNumberUtil;->connectsToEmergencyNumber(Ljava/lang/String;Ljava/lang/String;)Z

    #@b3
    move-result v7

    #@b4
    goto/16 :goto_4

    #@b6
    .line 2286
    .end local v6           #util:Lcom/android/i18n/phonenumbers/ShortNumberUtil;
    :cond_b6
    if-eqz p2, :cond_cb

    #@b8
    .line 2287
    const-string v9, "112"

    #@ba
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bd
    move-result v9

    #@be
    if-nez v9, :cond_c8

    #@c0
    const-string v9, "911"

    #@c2
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v9

    #@c6
    if-eqz v9, :cond_4

    #@c8
    :cond_c8
    move v7, v8

    #@c9
    goto/16 :goto_4

    #@cb
    .line 2289
    :cond_cb
    const-string v9, "112"

    #@cd
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d0
    move-result v9

    #@d1
    if-nez v9, :cond_db

    #@d3
    const-string v9, "911"

    #@d5
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d8
    move-result v9

    #@d9
    if-eqz v9, :cond_4

    #@db
    :cond_db
    move v7, v8

    #@dc
    goto/16 :goto_4
.end method

.method private static isEmergencyNumberInternal(Ljava/lang/String;Z)Z
    .registers 3
    .parameter "number"
    .parameter "useExactMatch"

    #@0
    .prologue
    .line 2111
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 2114
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    invoke-static {p0, v0, p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumberInternal(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@c
    move-result v0

    #@d
    goto :goto_7
.end method

.method public static isExactOrPotentialLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z
    .registers 5
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2882
    if-nez p0, :cond_4

    #@3
    .line 2888
    :cond_3
    :goto_3
    return v2

    #@4
    .line 2885
    :cond_4
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@7
    move-result v0

    #@8
    .line 2886
    .local v0, isExactEmergencyNumber:Z
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->isPotentialLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@b
    move-result v1

    #@c
    .line 2888
    .local v1, isPotentialEmergencyNumber:Z
    if-nez v0, :cond_10

    #@e
    if-eqz v1, :cond_3

    #@10
    :cond_10
    const/4 v2, 0x1

    #@11
    goto :goto_3
.end method

.method public static isGlobalPhoneNumber(Ljava/lang/String;)Z
    .registers 3
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 1375
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_8

    #@6
    .line 1376
    const/4 v1, 0x0

    #@7
    .line 1380
    :goto_7
    return v1

    #@8
    .line 1379
    :cond_8
    sget-object v1, Landroid/telephony/PhoneNumberUtils;->GLOBAL_PHONE_NUMBER_PATTERN:Ljava/util/regex/Pattern;

    #@a
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@d
    move-result-object v0

    #@e
    .line 1380
    .local v0, match:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@11
    move-result v1

    #@12
    goto :goto_7
.end method

.method public static isISODigit(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 140
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static isJapanEmergencyNumber(Ljava/lang/String;)Z
    .registers 13
    .parameter "number"

    #@0
    .prologue
    .line 2619
    if-nez p0, :cond_4

    #@2
    const/4 v9, 0x0

    #@3
    .line 2871
    :goto_3
    return v9

    #@4
    .line 2623
    :cond_4
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object p0

    #@8
    .line 2627
    const-string/jumbo v9, "ril.ecclist"

    #@b
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v5

    #@f
    .line 2628
    .local v5, numbers:Ljava/lang/String;
    const-string v7, ""

    #@11
    .line 2629
    .local v7, tempERNumber:Ljava/lang/String;
    const-string v4, "0"

    #@13
    .line 2631
    .local v4, mcc:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v10, "[Telephony]isJapanEmergencyNumber, number ="

    #@1a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v9

    #@1e
    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v9

    #@22
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v9

    #@26
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@29
    .line 2633
    if-eqz v5, :cond_41

    #@2b
    .line 2634
    new-instance v9, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v10, "[Telephony]isJapanEmergencyNumber, numbers ="

    #@32
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v9

    #@36
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v9

    #@3e
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@41
    .line 2636
    :cond_41
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@44
    move-result v9

    #@45
    if-eqz v9, :cond_4e

    #@47
    .line 2638
    const-string/jumbo v9, "ro.ril.ecclist"

    #@4a
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    .line 2641
    :cond_4e
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@51
    move-result-object v8

    #@52
    .line 2648
    .local v8, tm:Landroid/telephony/TelephonyManager;
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@55
    move-result v9

    #@56
    const/4 v10, 0x1

    #@57
    if-eq v9, v10, :cond_60

    #@59
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@5c
    move-result v9

    #@5d
    const/4 v10, 0x4

    #@5e
    if-ne v9, v10, :cond_10f

    #@60
    .line 2650
    :cond_60
    const-string v9, "*31#"

    #@62
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@65
    move-result v9

    #@66
    if-nez v9, :cond_70

    #@68
    const-string v9, "#31#"

    #@6a
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@6d
    move-result v9

    #@6e
    if-eqz v9, :cond_102

    #@70
    .line 2652
    :cond_70
    const/4 v9, 0x4

    #@71
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    .line 2660
    :goto_75
    new-instance v9, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v9

    #@7e
    const-string v10, ","

    #@80
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v9

    #@84
    const-string v10, "08"

    #@86
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v9

    #@8a
    const-string v10, ","

    #@8c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v9

    #@90
    const-string v10, "000"

    #@92
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v9

    #@96
    const-string v10, ","

    #@98
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v9

    #@9c
    const-string v10, "110"

    #@9e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v9

    #@a2
    const-string v10, ","

    #@a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    const-string v10, "112"

    #@aa
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    const-string v10, ","

    #@b0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v9

    #@b4
    const-string v10, "118"

    #@b6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v9

    #@ba
    const-string v10, ","

    #@bc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v9

    #@c0
    const-string v10, "119"

    #@c2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v9

    #@c6
    const-string v10, ","

    #@c8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v9

    #@cc
    const-string v10, "911"

    #@ce
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v9

    #@d2
    const-string v10, ","

    #@d4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v9

    #@d8
    const-string v10, "999"

    #@da
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v9

    #@de
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v5

    #@e2
    .line 2663
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e5
    move-result v9

    #@e6
    if-nez v9, :cond_10c

    #@e8
    .line 2664
    const-string v9, ","

    #@ea
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@ed
    move-result-object v0

    #@ee
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@ef
    .local v3, len$:I
    const/4 v2, 0x0

    #@f0
    .local v2, i$:I
    :goto_f0
    if-ge v2, v3, :cond_10c

    #@f2
    aget-object v1, v0, v2

    #@f4
    .line 2665
    .local v1, emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f7
    move-result v9

    #@f8
    if-eqz v9, :cond_109

    #@fa
    .line 2666
    const-string v9, "[Telephony]isEmergencyNumber, emergencyNum.equals(number), No USIM"

    #@fc
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@ff
    .line 2667
    const/4 v9, 0x1

    #@100
    goto/16 :goto_3

    #@102
    .line 2656
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_102
    const/4 v9, 0x0

    #@103
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@106
    move-result-object v7

    #@107
    goto/16 :goto_75

    #@109
    .line 2664
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_109
    add-int/lit8 v2, v2, 0x1

    #@10b
    goto :goto_f0

    #@10c
    .line 2671
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_10c
    const/4 v9, 0x0

    #@10d
    goto/16 :goto_3

    #@10f
    .line 2676
    :cond_10f
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@112
    move-result v9

    #@113
    if-nez v9, :cond_1c5

    #@115
    const-string v9, "0"

    #@117
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11a
    move-result v9

    #@11b
    if-eqz v9, :cond_1c5

    #@11d
    .line 2678
    new-instance v9, Ljava/lang/StringBuilder;

    #@11f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@122
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v9

    #@126
    const-string v10, ","

    #@128
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v9

    #@12c
    const-string v10, "08"

    #@12e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v9

    #@132
    const-string v10, ","

    #@134
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v9

    #@138
    const-string v10, "000"

    #@13a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v9

    #@13e
    const-string v10, ","

    #@140
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v9

    #@144
    const-string v10, "110"

    #@146
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v9

    #@14a
    const-string v10, ","

    #@14c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v9

    #@150
    const-string v10, "112"

    #@152
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v9

    #@156
    const-string v10, ","

    #@158
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v9

    #@15c
    const-string v10, "118"

    #@15e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v9

    #@162
    const-string v10, ","

    #@164
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v9

    #@168
    const-string v10, "119"

    #@16a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v9

    #@16e
    const-string v10, ","

    #@170
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v9

    #@174
    const-string v10, "911"

    #@176
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v9

    #@17a
    const-string v10, ","

    #@17c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v9

    #@180
    const-string v10, "999"

    #@182
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v9

    #@186
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@189
    move-result-object v5

    #@18a
    .line 2682
    const-string v9, "*31#"

    #@18c
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@18f
    move-result v9

    #@190
    if-nez v9, :cond_19a

    #@192
    const-string v9, "#31#"

    #@194
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@197
    move-result v9

    #@198
    if-eqz v9, :cond_1b9

    #@19a
    .line 2684
    :cond_19a
    const/4 v9, 0x4

    #@19b
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@19e
    move-result-object v7

    #@19f
    .line 2692
    :goto_19f
    const-string v9, ","

    #@1a1
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1a4
    move-result-object v0

    #@1a5
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@1a6
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@1a7
    .restart local v2       #i$:I
    :goto_1a7
    if-ge v2, v3, :cond_1c2

    #@1a9
    aget-object v1, v0, v2

    #@1ab
    .line 2693
    .restart local v1       #emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ae
    move-result v9

    #@1af
    if-eqz v9, :cond_1bf

    #@1b1
    .line 2694
    const-string v9, "[Telephony]isEmergencyNumber, emergencyNum.equals(number), number in EF_ECC"

    #@1b3
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@1b6
    .line 2695
    const/4 v9, 0x1

    #@1b7
    goto/16 :goto_3

    #@1b9
    .line 2688
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_1b9
    const/4 v9, 0x0

    #@1ba
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1bd
    move-result-object v7

    #@1be
    goto :goto_19f

    #@1bf
    .line 2692
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_1bf
    add-int/lit8 v2, v2, 0x1

    #@1c1
    goto :goto_1a7

    #@1c2
    .line 2698
    .end local v1           #emergencyNum:Ljava/lang/String;
    :cond_1c2
    const/4 v9, 0x0

    #@1c3
    goto/16 :goto_3

    #@1c5
    .line 2704
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_1c5
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@1c8
    move-result-object v9

    #@1c9
    if-eqz v9, :cond_1e0

    #@1cb
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@1ce
    move-result-object v9

    #@1cf
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@1d2
    move-result v9

    #@1d3
    const/4 v10, 0x4

    #@1d4
    if-le v9, v10, :cond_1e0

    #@1d6
    .line 2705
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@1d9
    move-result-object v9

    #@1da
    const/4 v10, 0x0

    #@1db
    const/4 v11, 0x3

    #@1dc
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1df
    move-result-object v4

    #@1e0
    .line 2709
    :cond_1e0
    const-string v9, "PhoneNumberUtils"

    #@1e2
    new-instance v10, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v11, "[Telephony] isEmergencyNumber, tm.getSimOperator() = "

    #@1e9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v10

    #@1ed
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@1f0
    move-result-object v11

    #@1f1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v10

    #@1f5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f8
    move-result-object v10

    #@1f9
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1fc
    .line 2710
    const/4 v6, 0x0

    #@1fd
    .line 2711
    .local v6, simMcc:Ljava/lang/String;
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@200
    move-result-object v9

    #@201
    if-eqz v9, :cond_22e

    #@203
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@206
    move-result-object v9

    #@207
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@20a
    move-result v9

    #@20b
    const/4 v10, 0x4

    #@20c
    if-le v9, v10, :cond_22e

    #@20e
    .line 2712
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@211
    move-result-object v9

    #@212
    const/4 v10, 0x0

    #@213
    const/4 v11, 0x3

    #@214
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@217
    move-result-object v6

    #@218
    .line 2713
    new-instance v9, Ljava/lang/StringBuilder;

    #@21a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@21d
    const-string v10, "[Telephony]isEmergencyNumber, simMcc ="

    #@21f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v9

    #@223
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v9

    #@227
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v9

    #@22b
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@22e
    .line 2717
    :cond_22e
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@231
    move-result v9

    #@232
    const/4 v10, 0x5

    #@233
    if-ne v9, v10, :cond_342

    #@235
    const-string v9, "0"

    #@237
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23a
    move-result v9

    #@23b
    if-eqz v9, :cond_342

    #@23d
    .line 2719
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@240
    move-result v9

    #@241
    if-nez v9, :cond_27b

    #@243
    .line 2724
    const-string v9, "*31#"

    #@245
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@248
    move-result v9

    #@249
    if-nez v9, :cond_253

    #@24b
    const-string v9, "#31#"

    #@24d
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@250
    move-result v9

    #@251
    if-eqz v9, :cond_272

    #@253
    .line 2726
    :cond_253
    const/4 v9, 0x4

    #@254
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@257
    move-result-object v7

    #@258
    .line 2734
    :goto_258
    const-string v9, ","

    #@25a
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@25d
    move-result-object v0

    #@25e
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@25f
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@260
    .restart local v2       #i$:I
    :goto_260
    if-ge v2, v3, :cond_27b

    #@262
    aget-object v1, v0, v2

    #@264
    .line 2735
    .restart local v1       #emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@267
    move-result v9

    #@268
    if-eqz v9, :cond_278

    #@26a
    .line 2736
    const-string v9, "[Telephony]isEmergencyNumber, emergencyNum.equals(number), number in EF_ECC"

    #@26c
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@26f
    .line 2737
    const/4 v9, 0x1

    #@270
    goto/16 :goto_3

    #@272
    .line 2730
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_272
    const/4 v9, 0x0

    #@273
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@276
    move-result-object v7

    #@277
    goto :goto_258

    #@278
    .line 2734
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_278
    add-int/lit8 v2, v2, 0x1

    #@27a
    goto :goto_260

    #@27b
    .line 2744
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_27b
    if-eqz v6, :cond_313

    #@27d
    const-string v9, "440"

    #@27f
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@282
    move-result v9

    #@283
    if-nez v9, :cond_295

    #@285
    const-string v9, "441"

    #@287
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28a
    move-result v9

    #@28b
    if-nez v9, :cond_295

    #@28d
    const-string v9, "001"

    #@28f
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@292
    move-result v9

    #@293
    if-eqz v9, :cond_313

    #@295
    .line 2745
    :cond_295
    new-instance v9, Ljava/lang/StringBuilder;

    #@297
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@29a
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29d
    move-result-object v9

    #@29e
    const-string v10, ","

    #@2a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a3
    move-result-object v9

    #@2a4
    const-string v10, "110"

    #@2a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v9

    #@2aa
    const-string v10, ","

    #@2ac
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2af
    move-result-object v9

    #@2b0
    const-string v10, "118"

    #@2b2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b5
    move-result-object v9

    #@2b6
    const-string v10, ","

    #@2b8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v9

    #@2bc
    const-string v10, "119"

    #@2be
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v9

    #@2c2
    const-string v10, ","

    #@2c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v9

    #@2c8
    const-string v10, "112"

    #@2ca
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v9

    #@2ce
    const-string v10, ","

    #@2d0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d3
    move-result-object v9

    #@2d4
    const-string v10, "911"

    #@2d6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d9
    move-result-object v9

    #@2da
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2dd
    move-result-object v5

    #@2de
    .line 2751
    :goto_2de
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2e1
    move-result v9

    #@2e2
    if-nez v9, :cond_342

    #@2e4
    .line 2759
    const-string v9, "*31#"

    #@2e6
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2e9
    move-result v9

    #@2ea
    if-nez v9, :cond_2f4

    #@2ec
    const-string v9, "#31#"

    #@2ee
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2f1
    move-result v9

    #@2f2
    if-eqz v9, :cond_339

    #@2f4
    .line 2760
    :cond_2f4
    const/4 v9, 0x4

    #@2f5
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2f8
    move-result-object v7

    #@2f9
    .line 2767
    :goto_2f9
    const-string v9, ","

    #@2fb
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2fe
    move-result-object v0

    #@2ff
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@300
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@301
    .restart local v2       #i$:I
    :goto_301
    if-ge v2, v3, :cond_342

    #@303
    aget-object v1, v0, v2

    #@305
    .line 2768
    .restart local v1       #emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@308
    move-result v9

    #@309
    if-eqz v9, :cond_33f

    #@30b
    .line 2769
    const-string v9, "[Telephony]isEmergencyNumber, emergencyNum.equals(number), DCM Emergency list"

    #@30d
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@310
    .line 2770
    const/4 v9, 0x1

    #@311
    goto/16 :goto_3

    #@313
    .line 2747
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_313
    new-instance v9, Ljava/lang/StringBuilder;

    #@315
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@318
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31b
    move-result-object v9

    #@31c
    const-string v10, ","

    #@31e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@321
    move-result-object v9

    #@322
    const-string v10, "112"

    #@324
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@327
    move-result-object v9

    #@328
    const-string v10, ","

    #@32a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32d
    move-result-object v9

    #@32e
    const-string v10, "911"

    #@330
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@333
    move-result-object v9

    #@334
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@337
    move-result-object v5

    #@338
    goto :goto_2de

    #@339
    .line 2763
    :cond_339
    const/4 v9, 0x0

    #@33a
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@33d
    move-result-object v7

    #@33e
    goto :goto_2f9

    #@33f
    .line 2767
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_33f
    add-int/lit8 v2, v2, 0x1

    #@341
    goto :goto_301

    #@342
    .line 2780
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_342
    const-string v9, "440"

    #@344
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@347
    move-result v9

    #@348
    if-nez v9, :cond_3b5

    #@34a
    const-string v9, "441"

    #@34c
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34f
    move-result v9

    #@350
    if-nez v9, :cond_3b5

    #@352
    .line 2782
    const-string v9, "*31#"

    #@354
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@357
    move-result v9

    #@358
    if-nez v9, :cond_362

    #@35a
    const-string v9, "#31#"

    #@35c
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@35f
    move-result v9

    #@360
    if-eqz v9, :cond_3ac

    #@362
    .line 2784
    :cond_362
    const/4 v9, 0x4

    #@363
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@366
    move-result-object v7

    #@367
    .line 2791
    :goto_367
    new-instance v9, Ljava/lang/StringBuilder;

    #@369
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@36c
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36f
    move-result-object v9

    #@370
    const-string v10, ","

    #@372
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@375
    move-result-object v9

    #@376
    const-string v10, "112"

    #@378
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37b
    move-result-object v9

    #@37c
    const-string v10, ","

    #@37e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@381
    move-result-object v9

    #@382
    const-string v10, "911"

    #@384
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@387
    move-result-object v9

    #@388
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38b
    move-result-object v5

    #@38c
    .line 2796
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@38f
    move-result v9

    #@390
    if-nez v9, :cond_3b5

    #@392
    .line 2797
    const-string v9, ","

    #@394
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@397
    move-result-object v0

    #@398
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@399
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@39a
    .restart local v2       #i$:I
    :goto_39a
    if-ge v2, v3, :cond_3b5

    #@39c
    aget-object v1, v0, v2

    #@39e
    .line 2798
    .restart local v1       #emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a1
    move-result v9

    #@3a2
    if-eqz v9, :cond_3b2

    #@3a4
    .line 2799
    const-string v9, "[Telephony]isEmergencyNumber, emergencyNum.equals(number), Roaming"

    #@3a6
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@3a9
    .line 2800
    const/4 v9, 0x1

    #@3aa
    goto/16 :goto_3

    #@3ac
    .line 2788
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_3ac
    const/4 v9, 0x0

    #@3ad
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3b0
    move-result-object v7

    #@3b1
    goto :goto_367

    #@3b2
    .line 2797
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_3b2
    add-int/lit8 v2, v2, 0x1

    #@3b4
    goto :goto_39a

    #@3b5
    .line 2809
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_3b5
    const-string v9, "440"

    #@3b7
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ba
    move-result v9

    #@3bb
    if-nez v9, :cond_3c5

    #@3bd
    const-string v9, "441"

    #@3bf
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c2
    move-result v9

    #@3c3
    if-eqz v9, :cond_4c0

    #@3c5
    .line 2810
    :cond_3c5
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3c8
    move-result v9

    #@3c9
    if-nez v9, :cond_3fe

    #@3cb
    .line 2815
    const-string v9, "*31#"

    #@3cd
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3d0
    move-result v9

    #@3d1
    if-nez v9, :cond_3db

    #@3d3
    const-string v9, "#31#"

    #@3d5
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3d8
    move-result v9

    #@3d9
    if-eqz v9, :cond_3f5

    #@3db
    .line 2817
    :cond_3db
    const/4 v9, 0x4

    #@3dc
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3df
    move-result-object v7

    #@3e0
    .line 2826
    :goto_3e0
    const-string v9, ","

    #@3e2
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3e5
    move-result-object v0

    #@3e6
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@3e7
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@3e8
    .restart local v2       #i$:I
    :goto_3e8
    if-ge v2, v3, :cond_3fe

    #@3ea
    aget-object v1, v0, v2

    #@3ec
    .line 2827
    .restart local v1       #emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ef
    move-result v9

    #@3f0
    if-eqz v9, :cond_3fb

    #@3f2
    .line 2829
    const/4 v9, 0x1

    #@3f3
    goto/16 :goto_3

    #@3f5
    .line 2821
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_3f5
    const/4 v9, 0x0

    #@3f6
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3f9
    move-result-object v7

    #@3fa
    goto :goto_3e0

    #@3fb
    .line 2826
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_3fb
    add-int/lit8 v2, v2, 0x1

    #@3fd
    goto :goto_3e8

    #@3fe
    .line 2836
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_3fe
    if-eqz v6, :cond_491

    #@400
    const-string v9, "440"

    #@402
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@405
    move-result v9

    #@406
    if-nez v9, :cond_418

    #@408
    const-string v9, "441"

    #@40a
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40d
    move-result v9

    #@40e
    if-nez v9, :cond_418

    #@410
    const-string v9, "001"

    #@412
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@415
    move-result v9

    #@416
    if-eqz v9, :cond_491

    #@418
    .line 2837
    :cond_418
    new-instance v9, Ljava/lang/StringBuilder;

    #@41a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@41d
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@420
    move-result-object v9

    #@421
    const-string v10, ","

    #@423
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@426
    move-result-object v9

    #@427
    const-string v10, "110"

    #@429
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42c
    move-result-object v9

    #@42d
    const-string v10, ","

    #@42f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@432
    move-result-object v9

    #@433
    const-string v10, "118"

    #@435
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@438
    move-result-object v9

    #@439
    const-string v10, ","

    #@43b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43e
    move-result-object v9

    #@43f
    const-string v10, "119"

    #@441
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@444
    move-result-object v9

    #@445
    const-string v10, ","

    #@447
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44a
    move-result-object v9

    #@44b
    const-string v10, "112"

    #@44d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@450
    move-result-object v9

    #@451
    const-string v10, ","

    #@453
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@456
    move-result-object v9

    #@457
    const-string v10, "911"

    #@459
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45c
    move-result-object v9

    #@45d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@460
    move-result-object v5

    #@461
    .line 2843
    :goto_461
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@464
    move-result v9

    #@465
    if-nez v9, :cond_4c0

    #@467
    .line 2851
    const-string v9, "*31#"

    #@469
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@46c
    move-result v9

    #@46d
    if-nez v9, :cond_477

    #@46f
    const-string v9, "#31#"

    #@471
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@474
    move-result v9

    #@475
    if-eqz v9, :cond_4b7

    #@477
    .line 2853
    :cond_477
    const/4 v9, 0x4

    #@478
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@47b
    move-result-object v7

    #@47c
    .line 2861
    :goto_47c
    const-string v9, ","

    #@47e
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@481
    move-result-object v0

    #@482
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@483
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@484
    .restart local v2       #i$:I
    :goto_484
    if-ge v2, v3, :cond_4c0

    #@486
    aget-object v1, v0, v2

    #@488
    .line 2862
    .restart local v1       #emergencyNum:Ljava/lang/String;
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48b
    move-result v9

    #@48c
    if-eqz v9, :cond_4bd

    #@48e
    .line 2864
    const/4 v9, 0x1

    #@48f
    goto/16 :goto_3

    #@491
    .line 2839
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_491
    new-instance v9, Ljava/lang/StringBuilder;

    #@493
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@496
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@499
    move-result-object v9

    #@49a
    const-string v10, ","

    #@49c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49f
    move-result-object v9

    #@4a0
    const-string v10, "112"

    #@4a2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a5
    move-result-object v9

    #@4a6
    const-string v10, ","

    #@4a8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ab
    move-result-object v9

    #@4ac
    const-string v10, "911"

    #@4ae
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b1
    move-result-object v9

    #@4b2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b5
    move-result-object v5

    #@4b6
    goto :goto_461

    #@4b7
    .line 2857
    :cond_4b7
    const/4 v9, 0x0

    #@4b8
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4bb
    move-result-object v7

    #@4bc
    goto :goto_47c

    #@4bd
    .line 2861
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_4bd
    add-int/lit8 v2, v2, 0x1

    #@4bf
    goto :goto_484

    #@4c0
    .line 2871
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_4c0
    const/4 v9, 0x0

    #@4c1
    goto/16 :goto_3
.end method

.method private static isKDDIEmergencyNumber(Ljava/lang/String;)Z
    .registers 10
    .parameter "number"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2537
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v7

    #@5
    if-eqz v7, :cond_8

    #@7
    .line 2584
    :cond_7
    :goto_7
    return v6

    #@8
    .line 2541
    :cond_8
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object p0

    #@c
    .line 2542
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v7

    #@10
    if-nez v7, :cond_7

    #@12
    .line 2543
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractSpecialNumberPortion(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object p0

    #@16
    .line 2544
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v7

    #@1a
    if-nez v7, :cond_7

    #@1c
    .line 2546
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@23
    move-result v7

    #@24
    const/4 v8, 0x2

    #@25
    if-ne v7, v8, :cond_bc

    #@27
    .line 2547
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@2e
    move-result v7

    #@2f
    if-nez v7, :cond_7d

    #@31
    .line 2548
    const-string/jumbo v7, "ril.ecclist"

    #@34
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    .line 2549
    .local v4, numbers:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3b
    move-result v7

    #@3c
    if-eqz v7, :cond_40

    #@3e
    .line 2550
    const-string v4, "110,118,119"

    #@40
    .line 2571
    :cond_40
    :goto_40
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@43
    move-result v7

    #@44
    if-eqz v7, :cond_4d

    #@46
    .line 2573
    const-string/jumbo v7, "ro.ril.ecclist"

    #@49
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    .line 2575
    :cond_4d
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@50
    move-result v7

    #@51
    if-nez v7, :cond_7

    #@53
    .line 2576
    const-string v7, ","

    #@55
    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@5a
    .local v3, len$:I
    const/4 v2, 0x0

    #@5b
    .local v2, i$:I
    :goto_5b
    if-ge v2, v3, :cond_7

    #@5d
    aget-object v1, v0, v2

    #@5f
    .line 2577
    .local v1, emergencyNum:Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v7

    #@63
    if-eqz v7, :cond_cd

    #@65
    .line 2578
    new-instance v6, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v7, "[Telephony] isKDDIEmergencyNumber is true : "

    #@6c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v6

    #@78
    invoke-static {v6}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@7b
    .line 2579
    const/4 v6, 0x1

    #@7c
    goto :goto_7

    #@7d
    .line 2554
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #emergencyNum:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #numbers:Ljava/lang/String;
    :cond_7d
    const/4 v7, 0x0

    #@7e
    const-string/jumbo v8, "roaming_ecclist"

    #@81
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v4

    #@85
    .line 2555
    .restart local v4       #numbers:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@88
    move-result v7

    #@89
    if-nez v7, :cond_40

    #@8b
    .line 2556
    const-string/jumbo v7, "ril.roaming.ecclist"

    #@8e
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@91
    move-result-object v5

    #@92
    .line 2557
    .local v5, roaming_numbers:Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@95
    move-result v7

    #@96
    if-eqz v7, :cond_40

    #@98
    .line 2558
    const-string/jumbo v7, "ril.roaming.ecclist"

    #@9b
    invoke-static {v7, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9e
    .line 2559
    new-instance v7, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v8, "[Telephony] ril.roaming.ecclist ="

    #@a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v7

    #@a9
    const-string/jumbo v8, "ril.roaming.ecclist"

    #@ac
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v7

    #@b4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v7

    #@b8
    invoke-static {v7}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@bb
    goto :goto_40

    #@bc
    .line 2565
    .end local v4           #numbers:Ljava/lang/String;
    .end local v5           #roaming_numbers:Ljava/lang/String;
    :cond_bc
    const-string/jumbo v7, "ril.ecclist"

    #@bf
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v4

    #@c3
    .line 2566
    .restart local v4       #numbers:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c6
    move-result v7

    #@c7
    if-eqz v7, :cond_40

    #@c9
    .line 2567
    const-string v4, "110,118,119"

    #@cb
    goto/16 :goto_40

    #@cd
    .line 2576
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #emergencyNum:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_cd
    add-int/lit8 v2, v2, 0x1

    #@cf
    goto :goto_5b
.end method

.method public static final isKRSMSDialable(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 162
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x39

    #@6
    if-le p0, v0, :cond_24

    #@8
    :cond_8
    const/16 v0, 0x2a

    #@a
    if-eq p0, v0, :cond_24

    #@c
    const/16 v0, 0x23

    #@e
    if-eq p0, v0, :cond_24

    #@10
    const/16 v0, 0x2b

    #@12
    if-eq p0, v0, :cond_24

    #@14
    const/16 v0, 0x4e

    #@16
    if-eq p0, v0, :cond_24

    #@18
    const/16 v0, 0x61

    #@1a
    if-eq p0, v0, :cond_24

    #@1c
    const/16 v0, 0x62

    #@1e
    if-eq p0, v0, :cond_24

    #@20
    const/16 v0, 0x63

    #@22
    if-ne p0, v0, :cond_26

    #@24
    :cond_24
    const/4 v0, 0x1

    #@25
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method private static isKoreaEmergencyNumber(Ljava/lang/String;Z)Z
    .registers 5
    .parameter "number"
    .parameter "useExactMatch"

    #@0
    .prologue
    .line 2380
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "isKoreaEmergencyNumber number : "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ", useExactMatch : "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@20
    .line 2382
    const/4 v0, 0x0

    #@21
    .line 2384
    .local v0, retB:Z
    const-string v1, "KR"

    #@23
    const-string v2, "SKT"

    #@25
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_30

    #@2b
    .line 2385
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->isKoreaSKTEmergencyNumber(Ljava/lang/String;Z)Z

    #@2e
    move-result v0

    #@2f
    .line 2393
    :cond_2f
    :goto_2f
    return v0

    #@30
    .line 2386
    :cond_30
    const-string v1, "KR"

    #@32
    const-string v2, "KT"

    #@34
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_3f

    #@3a
    .line 2387
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->isKoreaKTEmergencyNumber(Ljava/lang/String;Z)Z

    #@3d
    move-result v0

    #@3e
    goto :goto_2f

    #@3f
    .line 2388
    :cond_3f
    const-string v1, "KR"

    #@41
    const-string v2, "LGU"

    #@43
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_2f

    #@49
    .line 2389
    invoke-static {p0, p1}, Landroid/telephony/PhoneNumberUtils;->isKoreaLGUEmergencyNumber(Ljava/lang/String;Z)Z

    #@4c
    move-result v0

    #@4d
    goto :goto_2f
.end method

.method private static isKoreaKTEmergencyNumber(Ljava/lang/String;Z)Z
    .registers 7
    .parameter "number"
    .parameter "useExactMatch"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 2457
    const/4 v4, 0x0

    #@3
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getECCList(Landroid/content/Context;)[Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2461
    .local v0, NumbersfromXML:[Ljava/lang/String;
    if-eqz v0, :cond_26

    #@9
    .line 2462
    const/4 v1, 0x0

    #@a
    .local v1, index:I
    :goto_a
    array-length v4, v0

    #@b
    if-ge v1, v4, :cond_18

    #@d
    .line 2463
    if-eqz p1, :cond_19

    #@f
    .line 2464
    aget-object v4, v0, v1

    #@11
    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_23

    #@17
    move v2, v3

    #@18
    .line 2478
    .end local v1           #index:I
    :cond_18
    :goto_18
    return v2

    #@19
    .line 2467
    .restart local v1       #index:I
    :cond_19
    aget-object v4, v0, v1

    #@1b
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_23

    #@21
    move v2, v3

    #@22
    .line 2468
    goto :goto_18

    #@23
    .line 2462
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_a

    #@26
    .line 2475
    .end local v1           #index:I
    :cond_26
    if-eqz p1, :cond_3a

    #@28
    .line 2476
    const-string v4, "112"

    #@2a
    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_38

    #@30
    const-string v4, "911"

    #@32
    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_18

    #@38
    :cond_38
    move v2, v3

    #@39
    goto :goto_18

    #@3a
    .line 2478
    :cond_3a
    const-string v4, "112"

    #@3c
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3f
    move-result v4

    #@40
    if-nez v4, :cond_4a

    #@42
    const-string v4, "911"

    #@44
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@47
    move-result v4

    #@48
    if-eqz v4, :cond_18

    #@4a
    :cond_4a
    move v2, v3

    #@4b
    goto :goto_18
.end method

.method private static isKoreaLGUEmergencyNumber(Ljava/lang/String;Z)Z
    .registers 15
    .parameter "number"
    .parameter "useExactMatch"

    #@0
    .prologue
    const/4 v12, 0x3

    #@1
    const/4 v11, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 2483
    const/4 v5, 0x0

    #@5
    .line 2484
    .local v5, numbers:Ljava/lang/String;
    const/4 v0, 0x0

    #@6
    .line 2485
    .local v0, IsUsimRoaming:Z
    const-string/jumbo v9, "persist.radio.camped_mccmnc"

    #@9
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v6

    #@d
    .line 2490
    .local v6, usimMccMnc:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v9

    #@11
    if-nez v9, :cond_32

    #@13
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@16
    move-result v9

    #@17
    if-lez v9, :cond_32

    #@19
    .line 2491
    invoke-virtual {v6, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object v9

    #@1d
    const-string v10, "450"

    #@1f
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v9

    #@23
    if-nez v9, :cond_32

    #@25
    invoke-virtual {v6, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v9

    #@29
    const-string v10, "001"

    #@2b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v9

    #@2f
    if-nez v9, :cond_32

    #@31
    .line 2492
    const/4 v0, 0x1

    #@32
    .line 2495
    :cond_32
    const-string/jumbo v9, "true"

    #@35
    const-string v10, "gsm.operator.isroaming"

    #@37
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3e
    move-result v9

    #@3f
    if-nez v9, :cond_43

    #@41
    if-eqz v0, :cond_65

    #@43
    .line 2496
    :cond_43
    const-string v9, "LGU_roaming_ecclist"

    #@45
    invoke-static {v11, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    .line 2505
    :goto_49
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4c
    move-result v9

    #@4d
    if-nez v9, :cond_a3

    #@4f
    .line 2508
    const-string v9, ","

    #@51
    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    .local v1, arr$:[Ljava/lang/String;
    array-length v4, v1

    #@56
    .local v4, len$:I
    const/4 v3, 0x0

    #@57
    .local v3, i$:I
    :goto_57
    if-ge v3, v4, :cond_64

    #@59
    aget-object v2, v1, v3

    #@5b
    .line 2509
    .local v2, emergencyNum:Ljava/lang/String;
    if-eqz p1, :cond_98

    #@5d
    .line 2510
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v9

    #@61
    if-eqz v9, :cond_a0

    #@63
    move v7, v8

    #@64
    .line 2527
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #emergencyNum:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_64
    :goto_64
    return v7

    #@65
    .line 2497
    :cond_65
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->getUsimType()I

    #@68
    move-result v9

    #@69
    const/4 v10, 0x5

    #@6a
    if-ne v9, v10, :cond_91

    #@6c
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6f
    move-result-object v9

    #@70
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@73
    move-result v9

    #@74
    const/4 v10, 0x2

    #@75
    if-ne v9, v10, :cond_91

    #@77
    .line 2499
    new-instance v9, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v10, "LGU_ecclist"

    #@7e
    invoke-static {v11, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@81
    move-result-object v10

    #@82
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v9

    #@86
    const-string v10, ",114"

    #@88
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v9

    #@8c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v5

    #@90
    goto :goto_49

    #@91
    .line 2501
    :cond_91
    const-string v9, "LGU_ecclist"

    #@93
    invoke-static {v11, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@96
    move-result-object v5

    #@97
    goto :goto_49

    #@98
    .line 2514
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #emergencyNum:Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_98
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@9b
    move-result v9

    #@9c
    if-eqz v9, :cond_a0

    #@9e
    move v7, v8

    #@9f
    .line 2515
    goto :goto_64

    #@a0
    .line 2508
    :cond_a0
    add-int/lit8 v3, v3, 0x1

    #@a2
    goto :goto_57

    #@a3
    .line 2524
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #emergencyNum:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_a3
    if-eqz p1, :cond_b7

    #@a5
    .line 2525
    const-string v9, "112"

    #@a7
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v9

    #@ab
    if-nez v9, :cond_b5

    #@ad
    const-string v9, "911"

    #@af
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b2
    move-result v9

    #@b3
    if-eqz v9, :cond_64

    #@b5
    :cond_b5
    move v7, v8

    #@b6
    goto :goto_64

    #@b7
    .line 2527
    :cond_b7
    const-string v9, "112"

    #@b9
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@bc
    move-result v9

    #@bd
    if-nez v9, :cond_c7

    #@bf
    const-string v9, "911"

    #@c1
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c4
    move-result v9

    #@c5
    if-eqz v9, :cond_64

    #@c7
    :cond_c7
    move v7, v8

    #@c8
    goto :goto_64
.end method

.method private static isKoreaSKTEmergencyNumber(Ljava/lang/String;Z)Z
    .registers 14
    .parameter "number"
    .parameter "useExactMatch"

    #@0
    .prologue
    .line 2398
    const/4 v6, 0x0

    #@1
    .line 2399
    .local v6, numbers:Ljava/lang/String;
    const/4 v0, 0x0

    #@2
    .line 2400
    .local v0, IsUsimRoaming:Z
    const-string/jumbo v9, "persist.radio.camped_mccmnc"

    #@5
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v8

    #@9
    .line 2405
    .local v8, usimMccMnc:Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v9

    #@d
    if-nez v9, :cond_32

    #@f
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@12
    move-result v9

    #@13
    if-lez v9, :cond_32

    #@15
    .line 2406
    const/4 v9, 0x0

    #@16
    const/4 v10, 0x3

    #@17
    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v9

    #@1b
    const-string v10, "450"

    #@1d
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v9

    #@21
    if-nez v9, :cond_32

    #@23
    const/4 v9, 0x0

    #@24
    const/4 v10, 0x3

    #@25
    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v9

    #@29
    const-string v10, "001"

    #@2b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v9

    #@2f
    if-nez v9, :cond_32

    #@31
    .line 2407
    const/4 v0, 0x1

    #@32
    .line 2410
    :cond_32
    const-string/jumbo v9, "true"

    #@35
    const-string v10, "gsm.operator.isroaming"

    #@37
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3e
    move-result v9

    #@3f
    if-nez v9, :cond_43

    #@41
    if-eqz v0, :cond_bc

    #@43
    .line 2415
    :cond_43
    const-string v9, "gsm.operator.numeric"

    #@45
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    .line 2416
    .local v7, numeric:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4c
    move-result v9

    #@4d
    if-eqz v9, :cond_ac

    #@4f
    const-string v5, ""

    #@51
    .line 2421
    .local v5, mcc:Ljava/lang/String;
    :goto_51
    const-string v9, "202"

    #@53
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v9

    #@57
    if-nez v9, :cond_71

    #@59
    const-string v9, "206"

    #@5b
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v9

    #@5f
    if-nez v9, :cond_71

    #@61
    const-string v9, "222"

    #@63
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v9

    #@67
    if-nez v9, :cond_71

    #@69
    const-string v9, "505"

    #@6b
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v9

    #@6f
    if-eqz v9, :cond_b3

    #@71
    .line 2422
    :cond_71
    const/4 v9, 0x0

    #@72
    new-instance v10, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string/jumbo v11, "roaming"

    #@7a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v10

    #@7e
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v10

    #@82
    const-string v11, "_ecclist"

    #@84
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v10

    #@88
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v10

    #@8c
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@8f
    move-result-object v6

    #@90
    .line 2430
    .end local v5           #mcc:Ljava/lang/String;
    .end local v7           #numeric:Ljava/lang/String;
    :goto_90
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@93
    move-result v9

    #@94
    if-nez v9, :cond_d1

    #@96
    .line 2433
    const-string v9, ","

    #@98
    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@9b
    move-result-object v1

    #@9c
    .local v1, arr$:[Ljava/lang/String;
    array-length v4, v1

    #@9d
    .local v4, len$:I
    const/4 v3, 0x0

    #@9e
    .local v3, i$:I
    :goto_9e
    if-ge v3, v4, :cond_cf

    #@a0
    aget-object v2, v1, v3

    #@a2
    .line 2434
    .local v2, emergencyNum:Ljava/lang/String;
    if-eqz p1, :cond_c4

    #@a4
    .line 2435
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v9

    #@a8
    if-eqz v9, :cond_cc

    #@aa
    .line 2436
    const/4 v9, 0x1

    #@ab
    .line 2452
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #emergencyNum:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :goto_ab
    return v9

    #@ac
    .line 2416
    .restart local v7       #numeric:Ljava/lang/String;
    :cond_ac
    const/4 v9, 0x0

    #@ad
    const/4 v10, 0x3

    #@ae
    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b1
    move-result-object v5

    #@b2
    goto :goto_51

    #@b3
    .line 2424
    .restart local v5       #mcc:Ljava/lang/String;
    :cond_b3
    const/4 v9, 0x0

    #@b4
    const-string/jumbo v10, "roaming_ecclist"

    #@b7
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@ba
    move-result-object v6

    #@bb
    goto :goto_90

    #@bc
    .line 2426
    .end local v5           #mcc:Ljava/lang/String;
    .end local v7           #numeric:Ljava/lang/String;
    :cond_bc
    const/4 v9, 0x0

    #@bd
    const-string v10, "domestic_ecclist"

    #@bf
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getFeatureInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v6

    #@c3
    goto :goto_90

    #@c4
    .line 2439
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #emergencyNum:Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_c4
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c7
    move-result v9

    #@c8
    if-eqz v9, :cond_cc

    #@ca
    .line 2440
    const/4 v9, 0x1

    #@cb
    goto :goto_ab

    #@cc
    .line 2433
    :cond_cc
    add-int/lit8 v3, v3, 0x1

    #@ce
    goto :goto_9e

    #@cf
    .line 2445
    .end local v2           #emergencyNum:Ljava/lang/String;
    :cond_cf
    const/4 v9, 0x0

    #@d0
    goto :goto_ab

    #@d1
    .line 2449
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_d1
    if-eqz p1, :cond_e7

    #@d3
    .line 2450
    const-string v9, "112"

    #@d5
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v9

    #@d9
    if-nez v9, :cond_e3

    #@db
    const-string v9, "911"

    #@dd
    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0
    move-result v9

    #@e1
    if-eqz v9, :cond_e5

    #@e3
    :cond_e3
    const/4 v9, 0x1

    #@e4
    goto :goto_ab

    #@e5
    :cond_e5
    const/4 v9, 0x0

    #@e6
    goto :goto_ab

    #@e7
    .line 2452
    :cond_e7
    const-string v9, "112"

    #@e9
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@ec
    move-result v9

    #@ed
    if-nez v9, :cond_f7

    #@ef
    const-string v9, "911"

    #@f1
    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@f4
    move-result v9

    #@f5
    if-eqz v9, :cond_f9

    #@f7
    :cond_f7
    const/4 v9, 0x1

    #@f8
    goto :goto_ab

    #@f9
    :cond_f9
    const/4 v9, 0x0

    #@fa
    goto :goto_ab
.end method

.method public static isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z
    .registers 3
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    .line 2307
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumberInternal(Ljava/lang/String;Landroid/content/Context;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method private static isLocalEmergencyNumberInternal(Ljava/lang/String;Landroid/content/Context;Z)Z
    .registers 9
    .parameter "number"
    .parameter "context"
    .parameter "useExactMatch"

    #@0
    .prologue
    .line 2360
    const/4 v0, 0x0

    #@1
    .line 2362
    .local v0, countryIso:Ljava/lang/String;
    const-string v3, "country_detector"

    #@3
    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/location/CountryDetector;

    #@9
    .line 2366
    .local v1, detector:Landroid/location/CountryDetector;
    if-eqz v1, :cond_1e

    #@b
    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    #@e
    move-result-object v3

    #@f
    if-eqz v3, :cond_1e

    #@11
    .line 2368
    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 2375
    :goto_19
    invoke-static {p0, v0, p2}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumberInternal(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@1c
    move-result v3

    #@1d
    return v3

    #@1e
    .line 2370
    :cond_1e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@25
    move-result-object v3

    #@26
    iget-object v2, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@28
    .line 2371
    .local v2, locale:Ljava/util/Locale;
    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 2372
    const-string v3, "PhoneNumberUtils"

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "No CountryDetector; falling back to countryIso based on locale: "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_19
.end method

.method public static isN11orSpecialNumber(Ljava/lang/String;)Z
    .registers 6
    .parameter "number"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 3936
    if-nez p0, :cond_5

    #@4
    .line 3976
    :cond_4
    :goto_4
    return v1

    #@5
    .line 3938
    :cond_5
    const/4 v3, 0x0

    #@6
    const-string/jumbo v4, "support_sprint_n11"

    #@9
    invoke-static {v3, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_4

    #@f
    .line 3945
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object p0

    #@13
    .line 3948
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->NetworkCode:Ljava/lang/String;

    #@15
    const-string v4, "311870"

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v3

    #@1b
    if-nez v3, :cond_27

    #@1d
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->NetworkCode:Ljava/lang/String;

    #@1f
    const-string v4, "311490"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_3e

    #@27
    .line 3950
    :cond_27
    const/4 v0, 0x0

    #@28
    .local v0, i:I
    :goto_28
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2a
    array-length v3, v3

    #@2b
    if-ge v0, v3, :cond_55

    #@2d
    .line 3951
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersPrepaid:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@2f
    aget-object v3, v3, v0

    #@31
    iget-object v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;->number:Ljava/lang/String;

    #@33
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v3

    #@37
    if-ne v3, v2, :cond_3b

    #@39
    move v1, v2

    #@3a
    .line 3952
    goto :goto_4

    #@3b
    .line 3950
    :cond_3b
    add-int/lit8 v0, v0, 0x1

    #@3d
    goto :goto_28

    #@3e
    .line 3959
    .end local v0           #i:I
    :cond_3e
    const/4 v0, 0x0

    #@3f
    .restart local v0       #i:I
    :goto_3f
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@41
    array-length v3, v3

    #@42
    if-ge v0, v3, :cond_55

    #@44
    .line 3960
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbers:[Landroid/telephony/PhoneNumberUtils$SpecialNumbers;

    #@46
    aget-object v3, v3, v0

    #@48
    iget-object v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbers;->number:Ljava/lang/String;

    #@4a
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v3

    #@4e
    if-ne v3, v2, :cond_52

    #@50
    move v1, v2

    #@51
    .line 3961
    goto :goto_4

    #@52
    .line 3959
    :cond_52
    add-int/lit8 v0, v0, 0x1

    #@54
    goto :goto_3f

    #@55
    .line 3967
    :cond_55
    const/4 v0, 0x0

    #@56
    :goto_56
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@58
    array-length v3, v3

    #@59
    if-ge v0, v3, :cond_4

    #@5b
    .line 3971
    sget-object v3, Landroid/telephony/PhoneNumberUtils;->specialNumbersAddon:[Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;

    #@5d
    aget-object v3, v3, v0

    #@5f
    iget-object v3, v3, Landroid/telephony/PhoneNumberUtils$SpecialNumbersAddon;->number:Ljava/lang/String;

    #@61
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v3

    #@65
    if-ne v3, v2, :cond_69

    #@67
    move v1, v2

    #@68
    .line 3972
    goto :goto_4

    #@69
    .line 3967
    :cond_69
    add-int/lit8 v0, v0, 0x1

    #@6b
    goto :goto_56
.end method

.method private static isNanp(Ljava/lang/String;)Z
    .registers 6
    .parameter "dialStr"

    #@0
    .prologue
    const/16 v4, 0xa

    #@2
    .line 3195
    const/4 v2, 0x0

    #@3
    .line 3196
    .local v2, retVal:Z
    if-eqz p0, :cond_34

    #@5
    .line 3197
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    if-ne v3, v4, :cond_30

    #@b
    .line 3198
    const/4 v3, 0x0

    #@c
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v3

    #@10
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isTwoToNine(C)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_30

    #@16
    const/4 v3, 0x3

    #@17
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@1a
    move-result v3

    #@1b
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isTwoToNine(C)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_30

    #@21
    .line 3200
    const/4 v2, 0x1

    #@22
    .line 3201
    const/4 v1, 0x1

    #@23
    .local v1, i:I
    :goto_23
    if-ge v1, v4, :cond_30

    #@25
    .line 3202
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@28
    move-result v0

    #@29
    .line 3203
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@2c
    move-result v3

    #@2d
    if-nez v3, :cond_31

    #@2f
    .line 3204
    const/4 v2, 0x0

    #@30
    .line 3213
    .end local v0           #c:C
    .end local v1           #i:I
    :cond_30
    :goto_30
    return v2

    #@31
    .line 3201
    .restart local v0       #c:C
    .restart local v1       #i:I
    :cond_31
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_23

    #@34
    .line 3211
    .end local v0           #c:C
    .end local v1           #i:I
    :cond_34
    const-string v3, "isNanp: null dialStr passed in"

    #@36
    invoke-static {v3, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_30
.end method

.method public static isNoSlashNumberFormatCountry()Z
    .registers 2

    #@0
    .prologue
    .line 1819
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->country_code:Ljava/lang/String;

    #@2
    const-string v1, "EU"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_28

    #@a
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->op_code:Ljava/lang/String;

    #@c
    const-string v1, "VDF"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_28

    #@14
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->op_code:Ljava/lang/String;

    #@16
    const-string v1, "TMO"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_28

    #@1e
    sget-object v0, Landroid/telephony/PhoneNumberUtils;->op_code:Ljava/lang/String;

    #@20
    const-string v1, "ORG"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_2a

    #@28
    .line 1820
    :cond_28
    const/4 v0, 0x1

    #@29
    .line 1822
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method public static final isNonSeparator(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 175
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x39

    #@6
    if-le p0, v0, :cond_20

    #@8
    :cond_8
    const/16 v0, 0x2a

    #@a
    if-eq p0, v0, :cond_20

    #@c
    const/16 v0, 0x23

    #@e
    if-eq p0, v0, :cond_20

    #@10
    const/16 v0, 0x2b

    #@12
    if-eq p0, v0, :cond_20

    #@14
    const/16 v0, 0x4e

    #@16
    if-eq p0, v0, :cond_20

    #@18
    const/16 v0, 0x3b

    #@1a
    if-eq p0, v0, :cond_20

    #@1c
    const/16 v0, 0x2c

    #@1e
    if-ne p0, v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method private static isNonSeparator(Ljava/lang/String;)Z
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 1393
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v0

    #@5
    .local v0, count:I
    :goto_5
    if-ge v1, v0, :cond_16

    #@7
    .line 1394
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v2

    #@b
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_13

    #@11
    .line 1395
    const/4 v2, 0x0

    #@12
    .line 1398
    :goto_12
    return v2

    #@13
    .line 1393
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_5

    #@16
    .line 1398
    :cond_16
    const/4 v2, 0x1

    #@17
    goto :goto_12
.end method

.method private static isOneNanp(Ljava/lang/String;)Z
    .registers 5
    .parameter "dialStr"

    #@0
    .prologue
    .line 3220
    const/4 v1, 0x0

    #@1
    .line 3221
    .local v1, retVal:Z
    if-eqz p0, :cond_19

    #@3
    .line 3222
    const/4 v2, 0x1

    #@4
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 3223
    .local v0, newDialStr:Ljava/lang/String;
    const/4 v2, 0x0

    #@9
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v2

    #@d
    const/16 v3, 0x31

    #@f
    if-ne v2, v3, :cond_18

    #@11
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNanp(Ljava/lang/String;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_18

    #@17
    .line 3224
    const/4 v1, 0x1

    #@18
    .line 3229
    .end local v0           #newDialStr:Ljava/lang/String;
    :cond_18
    :goto_18
    return v1

    #@19
    .line 3227
    :cond_19
    const-string v2, "isOneNanp: null dialStr passed in"

    #@1b
    invoke-static {v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_18
.end method

.method private static isPause(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 189
    const/16 v0, 0x70

    #@2
    if-eq p0, v0, :cond_8

    #@4
    const/16 v0, 0x50

    #@6
    if-ne p0, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static final isPlus(C)Z
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 4150
    const/16 v1, 0x2b

    #@2
    if-ne p0, v1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 4151
    .local v0, ret:Z
    :goto_5
    return v0

    #@6
    .line 4150
    .end local v0           #ret:Z
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public static isPotentialEmergencyNumber(Ljava/lang/String;)Z
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 2087
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumberInternal(Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static isPotentialEmergencyNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 3
    .parameter "number"
    .parameter "defaultCountryIso"

    #@0
    .prologue
    .line 2155
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumberInternal(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static isPotentialLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z
    .registers 3
    .parameter "number"
    .parameter "context"

    #@0
    .prologue
    .line 2335
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumberInternal(Ljava/lang/String;Landroid/content/Context;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static final isReallyDialable(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 169
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x39

    #@6
    if-le p0, v0, :cond_14

    #@8
    :cond_8
    const/16 v0, 0x2a

    #@a
    if-eq p0, v0, :cond_14

    #@c
    const/16 v0, 0x23

    #@e
    if-eq p0, v0, :cond_14

    #@10
    const/16 v0, 0x2b

    #@12
    if-ne p0, v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method private static isSeparator(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 200
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_18

    #@6
    const/16 v0, 0x61

    #@8
    if-gt v0, p0, :cond_e

    #@a
    const/16 v0, 0x7a

    #@c
    if-le p0, v0, :cond_18

    #@e
    :cond_e
    const/16 v0, 0x41

    #@10
    if-gt v0, p0, :cond_16

    #@12
    const/16 v0, 0x5a

    #@14
    if-le p0, v0, :cond_18

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public static final isStartsPostDial(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 184
    const/16 v0, 0x2c

    #@2
    if-eq p0, v0, :cond_8

    #@4
    const/16 v0, 0x3b

    #@6
    if-ne p0, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static isToneWait(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 194
    const/16 v0, 0x77

    #@2
    if-eq p0, v0, :cond_8

    #@4
    const/16 v0, 0x57

    #@6
    if-ne p0, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static isTwoToNine(C)Z
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 3164
    const/16 v0, 0x32

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_a

    #@8
    .line 3165
    const/4 v0, 0x1

    #@9
    .line 3167
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static isUriNumber(Ljava/lang/String;)Z
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 3245
    if-eqz p0, :cond_14

    #@2
    const-string v0, "@"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    const-string v0, "%40"

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    :cond_12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public static isVoiceMailNumber(Ljava/lang/String;)Z
    .registers 10
    .parameter "number"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2906
    :try_start_2
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@5
    move-result-object v6

    #@6
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_7c

    #@c
    .line 2907
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->getPreferredVoiceSubscription()I

    #@13
    move-result v2

    #@14
    .line 2909
    .local v2, subscription:I
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6, v2}, Landroid/telephony/MSimTelephonyManager;->getVoiceMailNumber(I)Ljava/lang/String;
    :try_end_1b
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_1b} :catch_85

    #@1b
    move-result-object v3

    #@1c
    .line 2918
    .end local v2           #subscription:I
    .local v3, vmNumber:Ljava/lang/String;
    :goto_1c
    const/4 v1, 0x0

    #@1d
    .line 2920
    .local v1, mdnVmNumber:Ljava/lang/String;
    const-string v6, "VZW"

    #@1f
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_2d

    #@25
    .line 2921
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 2927
    :cond_2d
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@30
    move-result-object p0

    #@31
    .line 2933
    sget-boolean v6, Landroid/telephony/PhoneNumberUtils;->ENABLE_PRIVACY_LOG_CALL:Z

    #@33
    if-eqz v6, :cond_61

    #@35
    const-string v6, "PhoneNumberUtils"

    #@37
    new-instance v7, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v8, " number = "

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    const-string v8, ", vmNumber = "

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    const-string v8, ", mdnVmNumber = "

    #@52
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v7

    #@56
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v7

    #@5e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 2935
    :cond_61
    const-string v6, "VZW"

    #@63
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@66
    move-result v6

    #@67
    if-eqz v6, :cond_8a

    #@69
    .line 2936
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6c
    move-result v6

    #@6d
    if-nez v6, :cond_88

    #@6f
    invoke-static {p0, v3}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    #@72
    move-result v6

    #@73
    if-nez v6, :cond_7b

    #@75
    invoke-static {p0, v1}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    #@78
    move-result v6

    #@79
    if-eqz v6, :cond_88

    #@7b
    .line 2940
    .end local v1           #mdnVmNumber:Ljava/lang/String;
    .end local v3           #vmNumber:Ljava/lang/String;
    :cond_7b
    :goto_7b
    return v4

    #@7c
    .line 2911
    :cond_7c
    :try_start_7c
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getVoiceMailNumber()Ljava/lang/String;
    :try_end_83
    .catch Ljava/lang/SecurityException; {:try_start_7c .. :try_end_83} :catch_85

    #@83
    move-result-object v3

    #@84
    .restart local v3       #vmNumber:Ljava/lang/String;
    goto :goto_1c

    #@85
    .line 2913
    .end local v3           #vmNumber:Ljava/lang/String;
    :catch_85
    move-exception v0

    #@86
    .local v0, ex:Ljava/lang/SecurityException;
    move v4, v5

    #@87
    .line 2914
    goto :goto_7b

    #@88
    .end local v0           #ex:Ljava/lang/SecurityException;
    .restart local v1       #mdnVmNumber:Ljava/lang/String;
    .restart local v3       #vmNumber:Ljava/lang/String;
    :cond_88
    move v4, v5

    #@89
    .line 2936
    goto :goto_7b

    #@8a
    .line 2940
    :cond_8a
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8d
    move-result v6

    #@8e
    if-nez v6, :cond_96

    #@90
    invoke-static {p0, v3}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    #@93
    move-result v6

    #@94
    if-nez v6, :cond_7b

    #@96
    :cond_96
    move v4, v5

    #@97
    goto :goto_7b
.end method

.method public static isWellFormedSmsAddress(Ljava/lang/String;)Z
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 1366
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1369
    .local v0, networkPortion:Ljava/lang/String;
    const-string v1, "+"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_1a

    #@c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_1a

    #@12
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isDialable(Ljava/lang/String;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1a

    #@18
    const/4 v1, 0x1

    #@19
    :goto_19
    return v1

    #@1a
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_19
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 465
    const-string v0, "PhoneNumberUtils"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 466
    return-void
.end method

.method private static matchIntlPrefix(Ljava/lang/String;I)Z
    .registers 10
    .parameter "a"
    .parameter "len"

    #@0
    .prologue
    const/16 v7, 0x31

    #@2
    const/16 v6, 0x30

    #@4
    const/4 v4, 0x1

    #@5
    const/4 v3, 0x0

    #@6
    .line 3354
    const/4 v2, 0x0

    #@7
    .line 3355
    .local v2, state:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, p1, :cond_45

    #@a
    .line 3356
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v0

    #@e
    .line 3358
    .local v0, c:C
    packed-switch v2, :pswitch_data_50

    #@11
    .line 3377
    :pswitch_11
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_1d

    #@17
    .line 3383
    .end local v0           #c:C
    :cond_17
    :goto_17
    return v3

    #@18
    .line 3360
    .restart local v0       #c:C
    :pswitch_18
    const/16 v5, 0x2b

    #@1a
    if-ne v0, v5, :cond_20

    #@1c
    const/4 v2, 0x1

    #@1d
    .line 3355
    :cond_1d
    :goto_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_8

    #@20
    .line 3361
    :cond_20
    if-ne v0, v6, :cond_24

    #@22
    const/4 v2, 0x2

    #@23
    goto :goto_1d

    #@24
    .line 3362
    :cond_24
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_1d

    #@2a
    goto :goto_17

    #@2b
    .line 3366
    :pswitch_2b
    if-ne v0, v6, :cond_2f

    #@2d
    const/4 v2, 0x3

    #@2e
    goto :goto_1d

    #@2f
    .line 3367
    :cond_2f
    if-ne v0, v7, :cond_33

    #@31
    const/4 v2, 0x4

    #@32
    goto :goto_1d

    #@33
    .line 3368
    :cond_33
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@36
    move-result v5

    #@37
    if-eqz v5, :cond_1d

    #@39
    goto :goto_17

    #@3a
    .line 3372
    :pswitch_3a
    if-ne v0, v7, :cond_3e

    #@3c
    const/4 v2, 0x5

    #@3d
    goto :goto_1d

    #@3e
    .line 3373
    :cond_3e
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@41
    move-result v5

    #@42
    if-eqz v5, :cond_1d

    #@44
    goto :goto_17

    #@45
    .line 3383
    .end local v0           #c:C
    :cond_45
    if-eq v2, v4, :cond_4d

    #@47
    const/4 v5, 0x3

    #@48
    if-eq v2, v5, :cond_4d

    #@4a
    const/4 v5, 0x5

    #@4b
    if-ne v2, v5, :cond_17

    #@4d
    :cond_4d
    move v3, v4

    #@4e
    goto :goto_17

    #@4f
    .line 3358
    nop

    #@50
    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_18
        :pswitch_11
        :pswitch_2b
        :pswitch_11
        :pswitch_3a
    .end packed-switch
.end method

.method private static matchIntlPrefixAndCC(Ljava/lang/String;I)Z
    .registers 9
    .parameter "a"
    .parameter "len"

    #@0
    .prologue
    const/16 v6, 0x31

    #@2
    const/16 v5, 0x30

    #@4
    const/4 v3, 0x0

    #@5
    .line 3393
    const/4 v2, 0x0

    #@6
    .line 3394
    .local v2, state:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, p1, :cond_63

    #@9
    .line 3395
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v0

    #@d
    .line 3397
    .local v0, c:C
    packed-switch v2, :pswitch_data_70

    #@10
    .line 3429
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_1c

    #@16
    .line 3433
    .end local v0           #c:C
    :cond_16
    :goto_16
    return v3

    #@17
    .line 3399
    .restart local v0       #c:C
    :pswitch_17
    const/16 v4, 0x2b

    #@19
    if-ne v0, v4, :cond_1f

    #@1b
    const/4 v2, 0x1

    #@1c
    .line 3394
    :cond_1c
    :goto_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_7

    #@1f
    .line 3400
    :cond_1f
    if-ne v0, v5, :cond_23

    #@21
    const/4 v2, 0x2

    #@22
    goto :goto_1c

    #@23
    .line 3401
    :cond_23
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_1c

    #@29
    goto :goto_16

    #@2a
    .line 3405
    :pswitch_2a
    if-ne v0, v5, :cond_2e

    #@2c
    const/4 v2, 0x3

    #@2d
    goto :goto_1c

    #@2e
    .line 3406
    :cond_2e
    if-ne v0, v6, :cond_32

    #@30
    const/4 v2, 0x4

    #@31
    goto :goto_1c

    #@32
    .line 3407
    :cond_32
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_1c

    #@38
    goto :goto_16

    #@39
    .line 3411
    :pswitch_39
    if-ne v0, v6, :cond_3d

    #@3b
    const/4 v2, 0x5

    #@3c
    goto :goto_1c

    #@3d
    .line 3412
    :cond_3d
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_1c

    #@43
    goto :goto_16

    #@44
    .line 3418
    :pswitch_44
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@47
    move-result v4

    #@48
    if-eqz v4, :cond_4c

    #@4a
    const/4 v2, 0x6

    #@4b
    goto :goto_1c

    #@4c
    .line 3419
    :cond_4c
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_1c

    #@52
    goto :goto_16

    #@53
    .line 3424
    :pswitch_53
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@56
    move-result v4

    #@57
    if-eqz v4, :cond_5c

    #@59
    add-int/lit8 v2, v2, 0x1

    #@5b
    goto :goto_1c

    #@5c
    .line 3425
    :cond_5c
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_1c

    #@62
    goto :goto_16

    #@63
    .line 3433
    .end local v0           #c:C
    :cond_63
    const/4 v4, 0x6

    #@64
    if-eq v2, v4, :cond_6d

    #@66
    const/4 v4, 0x7

    #@67
    if-eq v2, v4, :cond_6d

    #@69
    const/16 v4, 0x8

    #@6b
    if-ne v2, v4, :cond_16

    #@6d
    :cond_6d
    const/4 v3, 0x1

    #@6e
    goto :goto_16

    #@6f
    .line 3397
    nop

    #@70
    :pswitch_data_70
    .packed-switch 0x0
        :pswitch_17
        :pswitch_44
        :pswitch_2a
        :pswitch_44
        :pswitch_39
        :pswitch_44
        :pswitch_53
        :pswitch_53
    .end packed-switch
.end method

.method private static matchIntlPrefixAndCC_KDDI(Ljava/lang/String;I)Z
    .registers 10
    .parameter "a"
    .parameter "len"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 3611
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@4
    move-result-object v7

    #@5
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@8
    move-result v7

    #@9
    if-eqz v7, :cond_25

    #@b
    .line 3612
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->roamingIntlPrefix()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 3615
    .local v0, RoamingPrefix:Ljava/lang/String;
    :goto_f
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@12
    move-result v3

    #@13
    .line 3617
    .local v3, prefixlen:I
    const/4 v5, 0x0

    #@14
    .line 3618
    .local v5, state:I
    const/4 v2, 0x0

    #@15
    .local v2, i:I
    :goto_15
    if-ge v2, p1, :cond_6b

    #@17
    .line 3619
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@1a
    move-result v1

    #@1b
    .line 3621
    .local v1, c:C
    packed-switch v5, :pswitch_data_78

    #@1e
    .line 3651
    :pswitch_1e
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@21
    move-result v7

    #@22
    if-eqz v7, :cond_2d

    #@24
    .line 3655
    .end local v1           #c:C
    :cond_24
    :goto_24
    return v6

    #@25
    .line 3614
    .end local v0           #RoamingPrefix:Ljava/lang/String;
    .end local v2           #i:I
    .end local v3           #prefixlen:I
    .end local v5           #state:I
    :cond_25
    const-string v0, "010"

    #@27
    .restart local v0       #RoamingPrefix:Ljava/lang/String;
    goto :goto_f

    #@28
    .line 3623
    .restart local v1       #c:C
    .restart local v2       #i:I
    .restart local v3       #prefixlen:I
    .restart local v5       #state:I
    :pswitch_28
    const/16 v7, 0x2b

    #@2a
    if-ne v1, v7, :cond_30

    #@2c
    const/4 v5, 0x1

    #@2d
    .line 3618
    :cond_2d
    :goto_2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_15

    #@30
    .line 3624
    :cond_30
    const/16 v7, 0x30

    #@32
    if-ne v1, v7, :cond_45

    #@34
    .line 3626
    if-lt p1, v3, :cond_24

    #@36
    .line 3628
    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    .line 3629
    .local v4, sa:Ljava/lang/String;
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_2d

    #@40
    .line 3631
    const/4 v5, 0x3

    #@41
    .line 3632
    add-int/lit8 v7, v3, -0x1

    #@43
    add-int/2addr v2, v7

    #@44
    goto :goto_2d

    #@45
    .line 3635
    .end local v4           #sa:Ljava/lang/String;
    :cond_45
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@48
    move-result v7

    #@49
    if-eqz v7, :cond_2d

    #@4b
    goto :goto_24

    #@4c
    .line 3640
    :pswitch_4c
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@4f
    move-result v7

    #@50
    if-eqz v7, :cond_54

    #@52
    const/4 v5, 0x6

    #@53
    goto :goto_2d

    #@54
    .line 3641
    :cond_54
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@57
    move-result v7

    #@58
    if-eqz v7, :cond_2d

    #@5a
    goto :goto_24

    #@5b
    .line 3646
    :pswitch_5b
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@5e
    move-result v7

    #@5f
    if-eqz v7, :cond_64

    #@61
    add-int/lit8 v5, v5, 0x1

    #@63
    goto :goto_2d

    #@64
    .line 3647
    :cond_64
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@67
    move-result v7

    #@68
    if-eqz v7, :cond_2d

    #@6a
    goto :goto_24

    #@6b
    .line 3655
    .end local v1           #c:C
    :cond_6b
    const/4 v7, 0x6

    #@6c
    if-eq v5, v7, :cond_75

    #@6e
    const/4 v7, 0x7

    #@6f
    if-eq v5, v7, :cond_75

    #@71
    const/16 v7, 0x8

    #@73
    if-ne v5, v7, :cond_24

    #@75
    :cond_75
    const/4 v6, 0x1

    #@76
    goto :goto_24

    #@77
    .line 3621
    nop

    #@78
    :pswitch_data_78
    .packed-switch 0x0
        :pswitch_28
        :pswitch_4c
        :pswitch_1e
        :pswitch_4c
        :pswitch_1e
        :pswitch_1e
        :pswitch_5b
        :pswitch_5b
    .end packed-switch
.end method

.method private static matchIntlPrefix_KDDI(Ljava/lang/String;I)Z
    .registers 11
    .parameter "a"
    .parameter "len"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 3569
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@5
    move-result-object v8

    #@6
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@9
    move-result v8

    #@a
    if-eqz v8, :cond_26

    #@c
    .line 3570
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->roamingIntlPrefix()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 3573
    .local v0, RoamingPrefix:Ljava/lang/String;
    :goto_10
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@13
    move-result v3

    #@14
    .line 3575
    .local v3, prefixlen:I
    const/4 v5, 0x0

    #@15
    .line 3576
    .local v5, state:I
    const/4 v2, 0x0

    #@16
    .local v2, i:I
    :goto_16
    if-ge v2, p1, :cond_4d

    #@18
    .line 3577
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@1b
    move-result v1

    #@1c
    .line 3579
    .local v1, c:C
    packed-switch v5, :pswitch_data_54

    #@1f
    .line 3597
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@22
    move-result v8

    #@23
    if-eqz v8, :cond_2e

    #@25
    .line 3603
    .end local v1           #c:C
    :cond_25
    :goto_25
    return v6

    #@26
    .line 3572
    .end local v0           #RoamingPrefix:Ljava/lang/String;
    .end local v2           #i:I
    .end local v3           #prefixlen:I
    .end local v5           #state:I
    :cond_26
    const-string v0, "010"

    #@28
    .restart local v0       #RoamingPrefix:Ljava/lang/String;
    goto :goto_10

    #@29
    .line 3581
    .restart local v1       #c:C
    .restart local v2       #i:I
    .restart local v3       #prefixlen:I
    .restart local v5       #state:I
    :pswitch_29
    const/16 v8, 0x2b

    #@2b
    if-ne v1, v8, :cond_31

    #@2d
    const/4 v5, 0x1

    #@2e
    .line 3576
    :cond_2e
    :goto_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_16

    #@31
    .line 3582
    :cond_31
    const/16 v8, 0x30

    #@33
    if-ne v1, v8, :cond_46

    #@35
    .line 3584
    if-lt p1, v3, :cond_25

    #@37
    .line 3586
    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    .line 3587
    .local v4, sa:Ljava/lang/String;
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v8

    #@3f
    if-eqz v8, :cond_2e

    #@41
    .line 3589
    const/4 v5, 0x3

    #@42
    .line 3590
    add-int/lit8 v8, v3, -0x1

    #@44
    add-int/2addr v2, v8

    #@45
    goto :goto_2e

    #@46
    .line 3593
    .end local v4           #sa:Ljava/lang/String;
    :cond_46
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@49
    move-result v8

    #@4a
    if-eqz v8, :cond_2e

    #@4c
    goto :goto_25

    #@4d
    .line 3603
    .end local v1           #c:C
    :cond_4d
    if-eq v5, v7, :cond_52

    #@4f
    const/4 v8, 0x3

    #@50
    if-ne v5, v8, :cond_25

    #@52
    :cond_52
    move v6, v7

    #@53
    goto :goto_25

    #@54
    .line 3579
    :pswitch_data_54
    .packed-switch 0x0
        :pswitch_29
    .end packed-switch
.end method

.method private static matchTrunkPrefix(Ljava/lang/String;I)Z
    .registers 6
    .parameter "a"
    .parameter "len"

    #@0
    .prologue
    .line 3441
    const/4 v1, 0x0

    #@1
    .line 3443
    .local v1, found:Z
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    if-ge v2, p1, :cond_19

    #@4
    .line 3444
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@7
    move-result v0

    #@8
    .line 3446
    .local v0, c:C
    const/16 v3, 0x30

    #@a
    if-ne v0, v3, :cond_12

    #@c
    if-nez v1, :cond_12

    #@e
    .line 3447
    const/4 v1, 0x1

    #@f
    .line 3443
    :cond_f
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_2

    #@12
    .line 3448
    :cond_12
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_f

    #@18
    .line 3449
    const/4 v1, 0x0

    #@19
    .line 3453
    .end local v0           #c:C
    .end local v1           #found:Z
    :cond_19
    return v1
.end method

.method private static minPositive(II)I
    .registers 2
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 453
    if-ltz p0, :cond_9

    #@2
    if-ltz p1, :cond_9

    #@4
    .line 454
    if-ge p0, p1, :cond_7

    #@6
    .line 460
    .end local p0
    :cond_6
    :goto_6
    return p0

    #@7
    .restart local p0
    :cond_7
    move p0, p1

    #@8
    .line 454
    goto :goto_6

    #@9
    .line 455
    :cond_9
    if-gez p0, :cond_6

    #@b
    .line 457
    if-ltz p1, :cond_f

    #@d
    move p0, p1

    #@e
    .line 458
    goto :goto_6

    #@f
    .line 460
    :cond_f
    const/4 p0, -0x1

    #@10
    goto :goto_6
.end method

.method public static networkPortionToCalledPartyBCD(Ljava/lang/String;)[B
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 1408
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1409
    .local v0, networkPortion:Ljava/lang/String;
    const/4 v1, 0x0

    #@5
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->numberToCalledPartyBCDHelper(Ljava/lang/String;Z)[B

    #@8
    move-result-object v1

    #@9
    return-object v1
.end method

.method public static networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 1418
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1419
    .local v0, networkPortion:Ljava/lang/String;
    const/4 v1, 0x1

    #@5
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->numberToCalledPartyBCDHelper(Ljava/lang/String;Z)[B

    #@8
    move-result-object v1

    #@9
    return-object v1
.end method

.method public static normalizeNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 1993
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1994
    .local v4, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    .line 1995
    .local v3, len:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v3, :cond_42

    #@c
    .line 1996
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v0

    #@10
    .line 1998
    .local v0, c:C
    const/16 v5, 0xa

    #@12
    invoke-static {v0, v5}, Ljava/lang/Character;->digit(CI)I

    #@15
    move-result v1

    #@16
    .line 1999
    .local v1, digit:I
    const/4 v5, -0x1

    #@17
    if-eq v1, v5, :cond_1f

    #@19
    .line 2000
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    .line 1995
    :cond_1c
    :goto_1c
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_a

    #@1f
    .line 2001
    :cond_1f
    if-nez v2, :cond_29

    #@21
    const/16 v5, 0x2b

    #@23
    if-ne v0, v5, :cond_29

    #@25
    .line 2002
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    goto :goto_1c

    #@29
    .line 2003
    :cond_29
    const/16 v5, 0x61

    #@2b
    if-lt v0, v5, :cond_31

    #@2d
    const/16 v5, 0x7a

    #@2f
    if-le v0, v5, :cond_39

    #@31
    :cond_31
    const/16 v5, 0x41

    #@33
    if-lt v0, v5, :cond_1c

    #@35
    const/16 v5, 0x5a

    #@37
    if-gt v0, v5, :cond_1c

    #@39
    .line 2004
    :cond_39
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    .line 2007
    .end local v0           #c:C
    .end local v1           #digit:I
    :goto_41
    return-object v5

    #@42
    :cond_42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    goto :goto_41
.end method

.method public static numberToCalledPartyBCD(Ljava/lang/String;)[B
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 1431
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/telephony/PhoneNumberUtils;->numberToCalledPartyBCDHelper(Ljava/lang/String;Z)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static numberToCalledPartyBCDHelper(Ljava/lang/String;Z)[B
    .registers 19
    .parameter "number"
    .parameter "includeLength"

    #@0
    .prologue
    .line 1440
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    #@3
    move-result v8

    #@4
    .line 1441
    .local v8, numberLenReal:I
    move v7, v8

    #@5
    .line 1442
    .local v7, numberLenEffective:I
    const/16 v14, 0x2b

    #@7
    move-object/from16 v0, p0

    #@9
    invoke-virtual {v0, v14}, Ljava/lang/String;->indexOf(I)I

    #@c
    move-result v14

    #@d
    const/4 v15, -0x1

    #@e
    if-eq v14, v15, :cond_1a

    #@10
    const/4 v5, 0x1

    #@11
    .line 1443
    .local v5, hasPlus:Z
    :goto_11
    if-eqz v5, :cond_15

    #@13
    add-int/lit8 v7, v7, -0x1

    #@15
    .line 1446
    :cond_15
    const/4 v1, 0x0

    #@16
    .line 1449
    .local v1, address_has_character:Z
    if-nez v7, :cond_1c

    #@18
    const/4 v11, 0x0

    #@19
    .line 1485
    :goto_19
    return-object v11

    #@1a
    .line 1442
    .end local v1           #address_has_character:Z
    .end local v5           #hasPlus:Z
    :cond_1a
    const/4 v5, 0x0

    #@1b
    goto :goto_11

    #@1c
    .line 1451
    .restart local v1       #address_has_character:Z
    .restart local v5       #hasPlus:Z
    :cond_1c
    add-int/lit8 v14, v7, 0x1

    #@1e
    div-int/lit8 v12, v14, 0x2

    #@20
    .line 1452
    .local v12, resultLen:I
    const/4 v4, 0x1

    #@21
    .line 1453
    .local v4, extraBytes:I
    if-eqz p1, :cond_25

    #@23
    add-int/lit8 v4, v4, 0x1

    #@25
    .line 1454
    :cond_25
    add-int/2addr v12, v4

    #@26
    .line 1456
    new-array v11, v12, [B

    #@28
    .line 1458
    .local v11, result:[B
    const/4 v3, 0x0

    #@29
    .line 1459
    .local v3, digitCount:I
    const/4 v6, 0x0

    #@2a
    .local v6, i:I
    :goto_2a
    if-ge v6, v8, :cond_62

    #@2c
    .line 1460
    move-object/from16 v0, p0

    #@2e
    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    #@31
    move-result v2

    #@32
    .line 1461
    .local v2, c:C
    const/16 v14, 0x2b

    #@34
    if-ne v2, v14, :cond_39

    #@36
    .line 1459
    :goto_36
    add-int/lit8 v6, v6, 0x1

    #@38
    goto :goto_2a

    #@39
    .line 1463
    :cond_39
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    #@3c
    move-result v14

    #@3d
    if-nez v14, :cond_40

    #@3f
    .line 1464
    const/4 v1, 0x1

    #@40
    .line 1467
    :cond_40
    and-int/lit8 v14, v3, 0x1

    #@42
    const/4 v15, 0x1

    #@43
    if-ne v14, v15, :cond_60

    #@45
    const/4 v13, 0x4

    #@46
    .line 1468
    .local v13, shift:I
    :goto_46
    shr-int/lit8 v14, v3, 0x1

    #@48
    add-int/2addr v14, v4

    #@49
    aget-byte v15, v11, v14

    #@4b
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->charToBCD(C)I

    #@4e
    move-result v16

    #@4f
    and-int/lit8 v16, v16, 0xf

    #@51
    shl-int v16, v16, v13

    #@53
    move/from16 v0, v16

    #@55
    int-to-byte v0, v0

    #@56
    move/from16 v16, v0

    #@58
    or-int v15, v15, v16

    #@5a
    int-to-byte v15, v15

    #@5b
    aput-byte v15, v11, v14

    #@5d
    .line 1469
    add-int/lit8 v3, v3, 0x1

    #@5f
    goto :goto_36

    #@60
    .line 1467
    .end local v13           #shift:I
    :cond_60
    const/4 v13, 0x0

    #@61
    goto :goto_46

    #@62
    .line 1473
    .end local v2           #c:C
    :cond_62
    and-int/lit8 v14, v3, 0x1

    #@64
    const/4 v15, 0x1

    #@65
    if-ne v14, v15, :cond_71

    #@67
    shr-int/lit8 v14, v3, 0x1

    #@69
    add-int/2addr v14, v4

    #@6a
    aget-byte v15, v11, v14

    #@6c
    or-int/lit16 v15, v15, 0xf0

    #@6e
    int-to-byte v15, v15

    #@6f
    aput-byte v15, v11, v14

    #@71
    .line 1475
    :cond_71
    const/4 v9, 0x0

    #@72
    .line 1476
    .local v9, offset:I
    if-eqz p1, :cond_7c

    #@74
    add-int/lit8 v10, v9, 0x1

    #@76
    .end local v9           #offset:I
    .local v10, offset:I
    add-int/lit8 v14, v12, -0x1

    #@78
    int-to-byte v14, v14

    #@79
    aput-byte v14, v11, v9

    #@7b
    move v9, v10

    #@7c
    .line 1478
    .end local v10           #offset:I
    .restart local v9       #offset:I
    :cond_7c
    const/4 v14, 0x0

    #@7d
    const-string v15, "DCM_NPI_4.3.1.1"

    #@7f
    invoke-static {v14, v15}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@82
    move-result v14

    #@83
    if-eqz v14, :cond_92

    #@85
    if-eqz v1, :cond_92

    #@87
    .line 1479
    if-eqz v5, :cond_8f

    #@89
    const/16 v14, 0x90

    #@8b
    :goto_8b
    int-to-byte v14, v14

    #@8c
    aput-byte v14, v11, v9

    #@8e
    goto :goto_19

    #@8f
    :cond_8f
    const/16 v14, 0x80

    #@91
    goto :goto_8b

    #@92
    .line 1482
    :cond_92
    if-eqz v5, :cond_9a

    #@94
    const/16 v14, 0x91

    #@96
    :goto_96
    int-to-byte v14, v14

    #@97
    aput-byte v14, v11, v9

    #@99
    goto :goto_19

    #@9a
    :cond_9a
    const/16 v14, 0x81

    #@9c
    goto :goto_96
.end method

.method private static processPlusCodeWithinNanp(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "networkDialStr"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 3282
    move-object v2, p0

    #@2
    .line 3287
    .local v2, retStr:Ljava/lang/String;
    if-eqz p0, :cond_1e

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@8
    move-result v3

    #@9
    const/16 v4, 0x2b

    #@b
    if-ne v3, v4, :cond_1e

    #@d
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@10
    move-result v3

    #@11
    if-le v3, v5, :cond_1e

    #@13
    .line 3290
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 3291
    .local v1, newStr:Ljava/lang/String;
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isOneNanp(Ljava/lang/String;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_1f

    #@1d
    .line 3293
    move-object v2, v1

    #@1e
    .line 3301
    .end local v1           #newStr:Ljava/lang/String;
    :cond_1e
    :goto_1e
    return-object v2

    #@1f
    .line 3295
    .restart local v1       #newStr:Ljava/lang/String;
    :cond_1f
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->getDefaultIdp()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 3297
    .local v0, idpStr:Ljava/lang/String;
    const-string v3, "[+]"

    #@25
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    goto :goto_1e
.end method

.method private static removeDashes(Landroid/text/Editable;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 1833
    const/4 v0, 0x0

    #@1
    .line 1834
    .local v0, p:I
    :goto_1
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@4
    move-result v1

    #@5
    if-ge v0, v1, :cond_18

    #@7
    .line 1835
    invoke-interface {p0, v0}, Landroid/text/Editable;->charAt(I)C

    #@a
    move-result v1

    #@b
    const/16 v2, 0x2d

    #@d
    if-ne v1, v2, :cond_15

    #@f
    .line 1836
    add-int/lit8 v1, v0, 0x1

    #@11
    invoke-interface {p0, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@14
    goto :goto_1

    #@15
    .line 1838
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_1

    #@18
    .line 1841
    :cond_18
    return-void
.end method

.method public static replaceUnicodeDigits(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "number"

    #@0
    .prologue
    .line 2019
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v6

    #@6
    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 2020
    .local v5, normalizedDigits:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    #@c
    move-result-object v0

    #@d
    .local v0, arr$:[C
    array-length v4, v0

    #@e
    .local v4, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    :goto_f
    if-ge v3, v4, :cond_26

    #@11
    aget-char v1, v0, v3

    #@13
    .line 2021
    .local v1, c:C
    const/16 v6, 0xa

    #@15
    invoke-static {v1, v6}, Ljava/lang/Character;->digit(CI)I

    #@18
    move-result v2

    #@19
    .line 2022
    .local v2, digit:I
    const/4 v6, -0x1

    #@1a
    if-eq v2, v6, :cond_22

    #@1c
    .line 2023
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    .line 2020
    :goto_1f
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_f

    #@22
    .line 2025
    :cond_22
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    goto :goto_1f

    #@26
    .line 2028
    .end local v1           #c:C
    .end local v2           #digit:I
    :cond_26
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    return-object v6
.end method

.method private static roamingIntlPrefix()Ljava/lang/String;
    .registers 14

    #@0
    .prologue
    .line 3459
    const/16 v10, 0x16

    #@2
    new-array v9, v10, [[Ljava/lang/String;

    #@4
    const/4 v10, 0x0

    #@5
    const/4 v11, 0x3

    #@6
    new-array v11, v11, [Ljava/lang/String;

    #@8
    const/4 v12, 0x0

    #@9
    const-string v13, "8576"

    #@b
    aput-object v13, v11, v12

    #@d
    const/4 v12, 0x1

    #@e
    const-string v13, "8703"

    #@10
    aput-object v13, v11, v12

    #@12
    const/4 v12, 0x2

    #@13
    const-string v13, "00"

    #@15
    aput-object v13, v11, v12

    #@17
    aput-object v11, v9, v10

    #@19
    const/4 v10, 0x1

    #@1a
    const/4 v11, 0x3

    #@1b
    new-array v11, v11, [Ljava/lang/String;

    #@1d
    const/4 v12, 0x0

    #@1e
    const-string v13, "13504"

    #@20
    aput-object v13, v11, v12

    #@22
    const/4 v12, 0x1

    #@23
    const-string v13, "13535"

    #@25
    aput-object v13, v11, v12

    #@27
    const/4 v12, 0x2

    #@28
    const-string v13, "005"

    #@2a
    aput-object v13, v11, v12

    #@2c
    aput-object v11, v9, v10

    #@2e
    const/4 v10, 0x2

    #@2f
    const/4 v11, 0x3

    #@30
    new-array v11, v11, [Ljava/lang/String;

    #@32
    const/4 v12, 0x0

    #@33
    const-string v13, "11296"

    #@35
    aput-object v13, v11, v12

    #@37
    const/4 v12, 0x1

    #@38
    const-string v13, "11311"

    #@3a
    aput-object v13, v11, v12

    #@3c
    const/4 v12, 0x2

    #@3d
    const-string v13, "00"

    #@3f
    aput-object v13, v11, v12

    #@41
    aput-object v11, v9, v10

    #@43
    const/4 v10, 0x3

    #@44
    const/4 v11, 0x3

    #@45
    new-array v11, v11, [Ljava/lang/String;

    #@47
    const/4 v12, 0x0

    #@48
    const-string v13, "1"

    #@4a
    aput-object v13, v11, v12

    #@4c
    const/4 v12, 0x1

    #@4d
    const-string v13, "2175"

    #@4f
    aput-object v13, v11, v12

    #@51
    const/4 v12, 0x2

    #@52
    const-string v13, "011"

    #@54
    aput-object v13, v11, v12

    #@56
    aput-object v11, v9, v10

    #@58
    const/4 v10, 0x4

    #@59
    const/4 v11, 0x3

    #@5a
    new-array v11, v11, [Ljava/lang/String;

    #@5c
    const/4 v12, 0x0

    #@5d
    const-string v13, "2304"

    #@5f
    aput-object v13, v11, v12

    #@61
    const/4 v12, 0x1

    #@62
    const-string v13, "7679"

    #@64
    aput-object v13, v11, v12

    #@66
    const/4 v12, 0x2

    #@67
    const-string v13, "011"

    #@69
    aput-object v13, v11, v12

    #@6b
    aput-object v11, v9, v10

    #@6d
    const/4 v10, 0x5

    #@6e
    const/4 v11, 0x3

    #@6f
    new-array v11, v11, [Ljava/lang/String;

    #@71
    const/4 v12, 0x0

    #@72
    const-string v13, "21567"

    #@74
    aput-object v13, v11, v12

    #@76
    const/4 v12, 0x1

    #@77
    const-string v13, "21630"

    #@79
    aput-object v13, v11, v12

    #@7b
    const/4 v12, 0x2

    #@7c
    const-string v13, "011"

    #@7e
    aput-object v13, v11, v12

    #@80
    aput-object v11, v9, v10

    #@82
    const/4 v10, 0x6

    #@83
    const/4 v11, 0x3

    #@84
    new-array v11, v11, [Ljava/lang/String;

    #@86
    const/4 v12, 0x0

    #@87
    const-string v13, "22404"

    #@89
    aput-object v13, v11, v12

    #@8b
    const/4 v12, 0x1

    #@8c
    const-string v13, "22527"

    #@8e
    aput-object v13, v11, v12

    #@90
    const/4 v12, 0x2

    #@91
    const-string v13, "011"

    #@93
    aput-object v13, v11, v12

    #@95
    aput-object v11, v9, v10

    #@97
    const/4 v10, 0x7

    #@98
    const/4 v11, 0x3

    #@99
    new-array v11, v11, [Ljava/lang/String;

    #@9b
    const/4 v12, 0x0

    #@9c
    const-string v13, "13472"

    #@9e
    aput-object v13, v11, v12

    #@a0
    const/4 v12, 0x1

    #@a1
    const-string v13, "13503"

    #@a3
    aput-object v13, v11, v12

    #@a5
    const/4 v12, 0x2

    #@a6
    const-string v13, "00"

    #@a8
    aput-object v13, v11, v12

    #@aa
    aput-object v11, v9, v10

    #@ac
    const/16 v10, 0x8

    #@ae
    const/4 v11, 0x3

    #@af
    new-array v11, v11, [Ljava/lang/String;

    #@b1
    const/4 v12, 0x0

    #@b2
    const-string v13, "24576"

    #@b4
    aput-object v13, v11, v12

    #@b6
    const/4 v12, 0x1

    #@b7
    const-string v13, "25075"

    #@b9
    aput-object v13, v11, v12

    #@bb
    const/4 v12, 0x2

    #@bc
    const-string v13, "00"

    #@be
    aput-object v13, v11, v12

    #@c0
    aput-object v11, v9, v10

    #@c2
    const/16 v10, 0x9

    #@c4
    const/4 v11, 0x3

    #@c5
    new-array v11, v11, [Ljava/lang/String;

    #@c7
    const/4 v12, 0x0

    #@c8
    const-string v13, "25100"

    #@ca
    aput-object v13, v11, v12

    #@cc
    const/4 v12, 0x1

    #@cd
    const-string v13, "25124"

    #@cf
    aput-object v13, v11, v12

    #@d1
    const/4 v12, 0x2

    #@d2
    const-string v13, "00"

    #@d4
    aput-object v13, v11, v12

    #@d6
    aput-object v11, v9, v10

    #@d8
    const/16 v10, 0xa

    #@da
    const/4 v11, 0x3

    #@db
    new-array v11, v11, [Ljava/lang/String;

    #@dd
    const/4 v12, 0x0

    #@de
    const-string v13, "13312"

    #@e0
    aput-object v13, v11, v12

    #@e2
    const/4 v12, 0x1

    #@e3
    const-string v13, "13439"

    #@e5
    aput-object v13, v11, v12

    #@e7
    const/4 v12, 0x2

    #@e8
    const-string v13, "00"

    #@ea
    aput-object v13, v11, v12

    #@ec
    aput-object v11, v9, v10

    #@ee
    const/16 v10, 0xb

    #@f0
    const/4 v11, 0x3

    #@f1
    new-array v11, v11, [Ljava/lang/String;

    #@f3
    const/4 v12, 0x0

    #@f4
    const-string v13, "8448"

    #@f6
    aput-object v13, v11, v12

    #@f8
    const/4 v12, 0x1

    #@f9
    const-string v13, "8479"

    #@fb
    aput-object v13, v11, v12

    #@fd
    const/4 v12, 0x2

    #@fe
    const-string v13, "00"

    #@100
    aput-object v13, v11, v12

    #@102
    aput-object v11, v9, v10

    #@104
    const/16 v10, 0xc

    #@106
    const/4 v11, 0x3

    #@107
    new-array v11, v11, [Ljava/lang/String;

    #@109
    const/4 v12, 0x0

    #@10a
    const-string v13, "14464"

    #@10c
    aput-object v13, v11, v12

    #@10e
    const/4 v12, 0x1

    #@10f
    const-string v13, "14847"

    #@111
    aput-object v13, v11, v12

    #@113
    const/4 v12, 0x2

    #@114
    const-string v13, "00"

    #@116
    aput-object v13, v11, v12

    #@118
    aput-object v11, v9, v10

    #@11a
    const/16 v10, 0xd

    #@11c
    const/4 v11, 0x3

    #@11d
    new-array v11, v11, [Ljava/lang/String;

    #@11f
    const/4 v12, 0x0

    #@120
    const-string v13, "10496"

    #@122
    aput-object v13, v11, v12

    #@124
    const/4 v12, 0x1

    #@125
    const-string v13, "10623"

    #@127
    aput-object v13, v11, v12

    #@129
    const/4 v12, 0x2

    #@12a
    const-string v13, "001"

    #@12c
    aput-object v13, v11, v12

    #@12e
    aput-object v11, v9, v10

    #@130
    const/16 v10, 0xe

    #@132
    const/4 v11, 0x3

    #@133
    new-array v11, v11, [Ljava/lang/String;

    #@135
    const/4 v12, 0x0

    #@136
    const-string v13, "2176"

    #@138
    aput-object v13, v11, v12

    #@13a
    const/4 v12, 0x1

    #@13b
    const-string v13, "2303"

    #@13d
    aput-object v13, v11, v12

    #@13f
    const/4 v12, 0x2

    #@140
    const-string v13, "00700"

    #@142
    aput-object v13, v11, v12

    #@144
    aput-object v11, v9, v10

    #@146
    const/16 v10, 0xf

    #@148
    const/4 v11, 0x3

    #@149
    new-array v11, v11, [Ljava/lang/String;

    #@14b
    const/4 v12, 0x0

    #@14c
    const-string v13, "13568"

    #@14e
    aput-object v13, v11, v12

    #@150
    const/4 v12, 0x1

    #@151
    const-string v13, "14335"

    #@153
    aput-object v13, v11, v12

    #@155
    const/4 v12, 0x2

    #@156
    const-string v13, "00"

    #@158
    aput-object v13, v11, v12

    #@15a
    aput-object v11, v9, v10

    #@15c
    const/16 v10, 0x10

    #@15e
    const/4 v11, 0x3

    #@15f
    new-array v11, v11, [Ljava/lang/String;

    #@161
    const/4 v12, 0x0

    #@162
    const-string v13, "25600"

    #@164
    aput-object v13, v11, v12

    #@166
    const/4 v12, 0x1

    #@167
    const-string v13, "26111"

    #@169
    aput-object v13, v11, v12

    #@16b
    const/4 v12, 0x2

    #@16c
    const-string v13, "00"

    #@16e
    aput-object v13, v11, v12

    #@170
    aput-object v11, v9, v10

    #@172
    const/16 v10, 0x11

    #@174
    const/4 v11, 0x3

    #@175
    new-array v11, v11, [Ljava/lang/String;

    #@177
    const/4 v12, 0x0

    #@178
    const-string v13, "16384"

    #@17a
    aput-object v13, v11, v12

    #@17c
    const/4 v12, 0x1

    #@17d
    const-string v13, "18431"

    #@17f
    aput-object v13, v11, v12

    #@181
    const/4 v12, 0x2

    #@182
    const-string v13, "011"

    #@184
    aput-object v13, v11, v12

    #@186
    aput-object v11, v9, v10

    #@188
    const/16 v10, 0x12

    #@18a
    const/4 v11, 0x3

    #@18b
    new-array v11, v11, [Ljava/lang/String;

    #@18d
    const/4 v12, 0x0

    #@18e
    const-string v13, "8192"

    #@190
    aput-object v13, v11, v12

    #@192
    const/4 v12, 0x1

    #@193
    const-string v13, "8223"

    #@195
    aput-object v13, v11, v12

    #@197
    const/4 v12, 0x2

    #@198
    const-string v13, "001"

    #@19a
    aput-object v13, v11, v12

    #@19c
    aput-object v11, v9, v10

    #@19e
    const/16 v10, 0x13

    #@1a0
    const/4 v11, 0x3

    #@1a1
    new-array v11, v11, [Ljava/lang/String;

    #@1a3
    const/4 v12, 0x0

    #@1a4
    const-string v13, "10640"

    #@1a6
    aput-object v13, v11, v12

    #@1a8
    const/4 v12, 0x1

    #@1a9
    const-string v13, "10655"

    #@1ab
    aput-object v13, v11, v12

    #@1ad
    const/4 v12, 0x2

    #@1ae
    const-string v13, "001"

    #@1b0
    aput-object v13, v11, v12

    #@1b2
    aput-object v11, v9, v10

    #@1b4
    const/16 v10, 0x14

    #@1b6
    const/4 v11, 0x3

    #@1b7
    new-array v11, v11, [Ljava/lang/String;

    #@1b9
    const/4 v12, 0x0

    #@1ba
    const-string v13, "9648"

    #@1bc
    aput-object v13, v11, v12

    #@1be
    const/4 v12, 0x1

    #@1bf
    const-string v13, "9663"

    #@1c1
    aput-object v13, v11, v12

    #@1c3
    const/4 v12, 0x2

    #@1c4
    const-string v13, "00"

    #@1c6
    aput-object v13, v11, v12

    #@1c8
    aput-object v11, v9, v10

    #@1ca
    const/16 v10, 0x15

    #@1cc
    const/4 v11, 0x3

    #@1cd
    new-array v11, v11, [Ljava/lang/String;

    #@1cf
    const/4 v12, 0x0

    #@1d0
    const-string v13, "----"

    #@1d2
    aput-object v13, v11, v12

    #@1d4
    const/4 v12, 0x1

    #@1d5
    const-string v13, ""

    #@1d7
    aput-object v13, v11, v12

    #@1d9
    const/4 v12, 0x2

    #@1da
    const-string/jumbo v13, "xx"

    #@1dd
    aput-object v13, v11, v12

    #@1df
    aput-object v11, v9, v10

    #@1e1
    .line 3490
    .local v9, sid_prefix:[[Ljava/lang/String;
    const/16 v10, 0x18

    #@1e3
    new-array v7, v10, [[Ljava/lang/String;

    #@1e5
    const/4 v10, 0x0

    #@1e6
    const/4 v11, 0x2

    #@1e7
    new-array v11, v11, [Ljava/lang/String;

    #@1e9
    const/4 v12, 0x0

    #@1ea
    const-string v13, "530"

    #@1ec
    aput-object v13, v11, v12

    #@1ee
    const/4 v12, 0x1

    #@1ef
    const-string v13, "00"

    #@1f1
    aput-object v13, v11, v12

    #@1f3
    aput-object v11, v7, v10

    #@1f5
    const/4 v10, 0x1

    #@1f6
    const/4 v11, 0x2

    #@1f7
    new-array v11, v11, [Ljava/lang/String;

    #@1f9
    const/4 v12, 0x0

    #@1fa
    const-string v13, "466"

    #@1fc
    aput-object v13, v11, v12

    #@1fe
    const/4 v12, 0x1

    #@1ff
    const-string v13, "005"

    #@201
    aput-object v13, v11, v12

    #@203
    aput-object v11, v7, v10

    #@205
    const/4 v10, 0x2

    #@206
    const/4 v11, 0x2

    #@207
    new-array v11, v11, [Ljava/lang/String;

    #@209
    const/4 v12, 0x0

    #@20a
    const-string v13, "455"

    #@20c
    aput-object v13, v11, v12

    #@20e
    const/4 v12, 0x1

    #@20f
    const-string v13, "00"

    #@211
    aput-object v13, v11, v12

    #@213
    aput-object v11, v7, v10

    #@215
    const/4 v10, 0x3

    #@216
    const/4 v11, 0x2

    #@217
    new-array v11, v11, [Ljava/lang/String;

    #@219
    const/4 v12, 0x0

    #@21a
    const-string v13, "310"

    #@21c
    aput-object v13, v11, v12

    #@21e
    const/4 v12, 0x1

    #@21f
    const-string v13, "011"

    #@221
    aput-object v13, v11, v12

    #@223
    aput-object v11, v7, v10

    #@225
    const/4 v10, 0x4

    #@226
    const/4 v11, 0x2

    #@227
    new-array v11, v11, [Ljava/lang/String;

    #@229
    const/4 v12, 0x0

    #@22a
    const-string v13, "311"

    #@22c
    aput-object v13, v11, v12

    #@22e
    const/4 v12, 0x1

    #@22f
    const-string v13, "011"

    #@231
    aput-object v13, v11, v12

    #@233
    aput-object v11, v7, v10

    #@235
    const/4 v10, 0x5

    #@236
    const/4 v11, 0x2

    #@237
    new-array v11, v11, [Ljava/lang/String;

    #@239
    const/4 v12, 0x0

    #@23a
    const-string v13, "316"

    #@23c
    aput-object v13, v11, v12

    #@23e
    const/4 v12, 0x1

    #@23f
    const-string v13, "011"

    #@241
    aput-object v13, v11, v12

    #@243
    aput-object v11, v7, v10

    #@245
    const/4 v10, 0x6

    #@246
    const/4 v11, 0x2

    #@247
    new-array v11, v11, [Ljava/lang/String;

    #@249
    const/4 v12, 0x0

    #@24a
    const-string v13, "470"

    #@24c
    aput-object v13, v11, v12

    #@24e
    const/4 v12, 0x1

    #@24f
    const-string v13, "00"

    #@251
    aput-object v13, v11, v12

    #@253
    aput-object v11, v7, v10

    #@255
    const/4 v10, 0x7

    #@256
    const/4 v11, 0x2

    #@257
    new-array v11, v11, [Ljava/lang/String;

    #@259
    const/4 v12, 0x0

    #@25a
    const-string v13, "334"

    #@25c
    aput-object v13, v11, v12

    #@25e
    const/4 v12, 0x1

    #@25f
    const-string v13, "00"

    #@261
    aput-object v13, v11, v12

    #@263
    aput-object v11, v7, v10

    #@265
    const/16 v10, 0x8

    #@267
    const/4 v11, 0x2

    #@268
    new-array v11, v11, [Ljava/lang/String;

    #@26a
    const/4 v12, 0x0

    #@26b
    const-string v13, "452"

    #@26d
    aput-object v13, v11, v12

    #@26f
    const/4 v12, 0x1

    #@270
    const-string v13, "00"

    #@272
    aput-object v13, v11, v12

    #@274
    aput-object v11, v7, v10

    #@276
    const/16 v10, 0x9

    #@278
    const/4 v11, 0x2

    #@279
    new-array v11, v11, [Ljava/lang/String;

    #@27b
    const/4 v12, 0x0

    #@27c
    const-string v13, "425"

    #@27e
    aput-object v13, v11, v12

    #@280
    const/4 v12, 0x1

    #@281
    const-string v13, "00"

    #@283
    aput-object v13, v11, v12

    #@285
    aput-object v11, v7, v10

    #@287
    const/16 v10, 0xa

    #@289
    const/4 v11, 0x2

    #@28a
    new-array v11, v11, [Ljava/lang/String;

    #@28c
    const/4 v12, 0x0

    #@28d
    const-string v13, "404"

    #@28f
    aput-object v13, v11, v12

    #@291
    const/4 v12, 0x1

    #@292
    const-string v13, "00"

    #@294
    aput-object v13, v11, v12

    #@296
    aput-object v11, v7, v10

    #@298
    const/16 v10, 0xb

    #@29a
    const/4 v11, 0x2

    #@29b
    new-array v11, v11, [Ljava/lang/String;

    #@29d
    const/4 v12, 0x0

    #@29e
    const-string v13, "405"

    #@2a0
    aput-object v13, v11, v12

    #@2a2
    const/4 v12, 0x1

    #@2a3
    const-string v13, "00"

    #@2a5
    aput-object v13, v11, v12

    #@2a7
    aput-object v11, v7, v10

    #@2a9
    const/16 v10, 0xc

    #@2ab
    const/4 v11, 0x2

    #@2ac
    new-array v11, v11, [Ljava/lang/String;

    #@2ae
    const/4 v12, 0x0

    #@2af
    const-string v13, "510"

    #@2b1
    aput-object v13, v11, v12

    #@2b3
    const/4 v12, 0x1

    #@2b4
    const-string v13, "001"

    #@2b6
    aput-object v13, v11, v12

    #@2b8
    aput-object v11, v7, v10

    #@2ba
    const/16 v10, 0xd

    #@2bc
    const/4 v11, 0x2

    #@2bd
    new-array v11, v11, [Ljava/lang/String;

    #@2bf
    const/4 v12, 0x0

    #@2c0
    const-string v13, "450"

    #@2c2
    aput-object v13, v11, v12

    #@2c4
    const/4 v12, 0x1

    #@2c5
    const-string v13, "00700"

    #@2c7
    aput-object v13, v11, v12

    #@2c9
    aput-object v11, v7, v10

    #@2cb
    const/16 v10, 0xe

    #@2cd
    const/4 v11, 0x2

    #@2ce
    new-array v11, v11, [Ljava/lang/String;

    #@2d0
    const/4 v12, 0x0

    #@2d1
    const-string v13, "460"

    #@2d3
    aput-object v13, v11, v12

    #@2d5
    const/4 v12, 0x1

    #@2d6
    const-string v13, "00"

    #@2d8
    aput-object v13, v11, v12

    #@2da
    aput-object v11, v7, v10

    #@2dc
    const/16 v10, 0xf

    #@2de
    const/4 v11, 0x2

    #@2df
    new-array v11, v11, [Ljava/lang/String;

    #@2e1
    const/4 v12, 0x0

    #@2e2
    const-string v13, "302"

    #@2e4
    aput-object v13, v11, v12

    #@2e6
    const/4 v12, 0x1

    #@2e7
    const-string v13, "011"

    #@2e9
    aput-object v13, v11, v12

    #@2eb
    aput-object v11, v7, v10

    #@2ed
    const/16 v10, 0x10

    #@2ef
    const/4 v11, 0x2

    #@2f0
    new-array v11, v11, [Ljava/lang/String;

    #@2f2
    const/4 v12, 0x0

    #@2f3
    const-string v13, "520"

    #@2f5
    aput-object v13, v11, v12

    #@2f7
    const/4 v12, 0x1

    #@2f8
    const-string v13, "001"

    #@2fa
    aput-object v13, v11, v12

    #@2fc
    aput-object v11, v7, v10

    #@2fe
    const/16 v10, 0x11

    #@300
    const/4 v11, 0x2

    #@301
    new-array v11, v11, [Ljava/lang/String;

    #@303
    const/4 v12, 0x0

    #@304
    const-string v13, "454"

    #@306
    aput-object v13, v11, v12

    #@308
    const/4 v12, 0x1

    #@309
    const-string v13, "001"

    #@30b
    aput-object v13, v11, v12

    #@30d
    aput-object v11, v7, v10

    #@30f
    const/16 v10, 0x12

    #@311
    const/4 v11, 0x2

    #@312
    new-array v11, v11, [Ljava/lang/String;

    #@314
    const/4 v12, 0x0

    #@315
    const-string v13, "363"

    #@317
    aput-object v13, v11, v12

    #@319
    const/4 v12, 0x1

    #@31a
    const-string v13, "00"

    #@31c
    aput-object v13, v11, v12

    #@31e
    aput-object v11, v7, v10

    #@320
    const/16 v10, 0x13

    #@322
    const/4 v11, 0x2

    #@323
    new-array v11, v11, [Ljava/lang/String;

    #@325
    const/4 v12, 0x0

    #@326
    const-string v13, "716"

    #@328
    aput-object v13, v11, v12

    #@32a
    const/4 v12, 0x1

    #@32b
    const-string v13, "00"

    #@32d
    aput-object v13, v11, v12

    #@32f
    aput-object v11, v7, v10

    #@331
    const/16 v10, 0x14

    #@333
    const/4 v11, 0x2

    #@334
    new-array v11, v11, [Ljava/lang/String;

    #@336
    const/4 v12, 0x0

    #@337
    const-string v13, "734"

    #@339
    aput-object v13, v11, v12

    #@33b
    const/4 v12, 0x1

    #@33c
    const-string v13, "00"

    #@33e
    aput-object v13, v11, v12

    #@340
    aput-object v11, v7, v10

    #@342
    const/16 v10, 0x15

    #@344
    const/4 v11, 0x2

    #@345
    new-array v11, v11, [Ljava/lang/String;

    #@347
    const/4 v12, 0x0

    #@348
    const-string v13, "350"

    #@34a
    aput-object v13, v11, v12

    #@34c
    const/4 v12, 0x1

    #@34d
    const-string v13, "011"

    #@34f
    aput-object v13, v11, v12

    #@351
    aput-object v11, v7, v10

    #@353
    const/16 v10, 0x16

    #@355
    const/4 v11, 0x2

    #@356
    new-array v11, v11, [Ljava/lang/String;

    #@358
    const/4 v12, 0x0

    #@359
    const-string v13, "364"

    #@35b
    aput-object v13, v11, v12

    #@35d
    const/4 v12, 0x1

    #@35e
    const-string v13, "011"

    #@360
    aput-object v13, v11, v12

    #@362
    aput-object v11, v7, v10

    #@364
    const/16 v10, 0x17

    #@366
    const/4 v11, 0x2

    #@367
    new-array v11, v11, [Ljava/lang/String;

    #@369
    const/4 v12, 0x0

    #@36a
    const-string v13, "----"

    #@36c
    aput-object v13, v11, v12

    #@36e
    const/4 v12, 0x1

    #@36f
    const-string/jumbo v13, "xx"

    #@372
    aput-object v13, v11, v12

    #@374
    aput-object v11, v7, v10

    #@376
    .line 3518
    .local v7, mcc_prefix:[[Ljava/lang/String;
    const-string/jumbo v2, "xx"

    #@379
    .line 3519
    .local v2, RoamingPrefix:Ljava/lang/String;
    const-string/jumbo v10, "ril.cdma.sid"

    #@37c
    const-string v11, "0"

    #@37e
    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@381
    move-result-object v3

    #@382
    .line 3520
    .local v3, SID:Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@385
    move-result v5

    #@386
    .line 3522
    .local v5, iSID:I
    const/4 v4, 0x0

    #@387
    .line 3524
    .local v4, i:I
    :goto_387
    aget-object v10, v9, v4

    #@389
    const/4 v11, 0x0

    #@38a
    aget-object v10, v10, v11

    #@38c
    const-string v11, "----"

    #@38e
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@391
    move-result v10

    #@392
    if-eqz v10, :cond_3f1

    #@394
    .line 3540
    :goto_394
    const-string/jumbo v10, "xx"

    #@397
    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39a
    move-result v10

    #@39b
    if-eqz v10, :cond_3e5

    #@39d
    .line 3542
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3a0
    move-result-object v10

    #@3a1
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    #@3a4
    move-result-object v1

    #@3a5
    .line 3543
    .local v1, NetworkOperator:Ljava/lang/String;
    const-string v0, "0"

    #@3a7
    .line 3544
    .local v0, MCC:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@3aa
    move-result v10

    #@3ab
    const/4 v11, 0x3

    #@3ac
    if-lt v10, v11, :cond_3b4

    #@3ae
    .line 3545
    const/4 v10, 0x0

    #@3af
    const/4 v11, 0x3

    #@3b0
    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3b3
    move-result-object v0

    #@3b4
    .line 3546
    :cond_3b4
    new-instance v10, Ljava/lang/StringBuilder;

    #@3b6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@3b9
    const-string v11, "NetworkOperatorName:"

    #@3bb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3be
    move-result-object v10

    #@3bf
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c2
    move-result-object v10

    #@3c3
    const-string v11, " MCC:"

    #@3c5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c8
    move-result-object v10

    #@3c9
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3cc
    move-result-object v10

    #@3cd
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d0
    move-result-object v10

    #@3d1
    invoke-static {v10}, Landroid/telephony/PhoneNumberUtils;->log(Ljava/lang/String;)V

    #@3d4
    .line 3547
    const/4 v4, 0x0

    #@3d5
    .line 3549
    :goto_3d5
    aget-object v10, v7, v4

    #@3d7
    const/4 v11, 0x0

    #@3d8
    aget-object v10, v10, v11

    #@3da
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3dd
    move-result v10

    #@3de
    if-eqz v10, :cond_411

    #@3e0
    .line 3551
    aget-object v10, v7, v4

    #@3e2
    const/4 v11, 0x1

    #@3e3
    aget-object v2, v10, v11

    #@3e5
    .line 3560
    .end local v0           #MCC:Ljava/lang/String;
    .end local v1           #NetworkOperator:Ljava/lang/String;
    :cond_3e5
    const-string/jumbo v10, "xx"

    #@3e8
    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3eb
    move-result v10

    #@3ec
    if-eqz v10, :cond_3f0

    #@3ee
    .line 3561
    const-string v2, "00"

    #@3f0
    .line 3562
    :cond_3f0
    return-object v2

    #@3f1
    .line 3529
    :cond_3f1
    aget-object v10, v9, v4

    #@3f3
    const/4 v11, 0x0

    #@3f4
    aget-object v10, v10, v11

    #@3f6
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3f9
    move-result v8

    #@3fa
    .line 3530
    .local v8, minSid:I
    aget-object v10, v9, v4

    #@3fc
    const/4 v11, 0x1

    #@3fd
    aget-object v10, v10, v11

    #@3ff
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@402
    move-result v6

    #@403
    .line 3532
    .local v6, maxSid:I
    if-lt v5, v8, :cond_40d

    #@405
    if-gt v5, v6, :cond_40d

    #@407
    .line 3534
    aget-object v10, v9, v4

    #@409
    const/4 v11, 0x2

    #@40a
    aget-object v2, v10, v11

    #@40c
    .line 3535
    goto :goto_394

    #@40d
    .line 3522
    :cond_40d
    add-int/lit8 v4, v4, 0x1

    #@40f
    goto/16 :goto_387

    #@411
    .line 3554
    .end local v6           #maxSid:I
    .end local v8           #minSid:I
    .restart local v0       #MCC:Ljava/lang/String;
    .restart local v1       #NetworkOperator:Ljava/lang/String;
    :cond_411
    aget-object v10, v7, v4

    #@413
    const/4 v11, 0x0

    #@414
    aget-object v10, v10, v11

    #@416
    const-string v11, "----"

    #@418
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41b
    move-result v10

    #@41c
    if-nez v10, :cond_3e5

    #@41e
    .line 3547
    add-int/lit8 v4, v4, 0x1

    #@420
    goto :goto_3d5
.end method

.method public static stringFromStringAndTOA(Ljava/lang/String;I)Ljava/lang/String;
    .registers 4
    .parameter "s"
    .parameter "TOA"

    #@0
    .prologue
    .line 920
    if-nez p0, :cond_4

    #@2
    const/4 p0, 0x0

    #@3
    .line 926
    .end local p0
    :cond_3
    :goto_3
    return-object p0

    #@4
    .line 922
    .restart local p0
    :cond_4
    const/16 v0, 0x91

    #@6
    if-ne p1, v0, :cond_3

    #@8
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@b
    move-result v0

    #@c
    if-lez v0, :cond_3

    #@e
    const/4 v0, 0x0

    #@f
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v0

    #@13
    const/16 v1, 0x2b

    #@15
    if-eq v0, v1, :cond_3

    #@17
    .line 923
    new-instance v0, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v1, "+"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object p0

    #@2a
    goto :goto_3
.end method

.method public static stripSeparators(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 390
    if-nez p0, :cond_4

    #@2
    .line 391
    const/4 v5, 0x0

    #@3
    .line 407
    :goto_3
    return-object v5

    #@4
    .line 393
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v3

    #@8
    .line 394
    .local v3, len:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 396
    .local v4, ret:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v3, :cond_2d

    #@10
    .line 397
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    .line 399
    .local v0, c:C
    const/16 v5, 0xa

    #@16
    invoke-static {v0, v5}, Ljava/lang/Character;->digit(CI)I

    #@19
    move-result v1

    #@1a
    .line 400
    .local v1, digit:I
    const/4 v5, -0x1

    #@1b
    if-eq v1, v5, :cond_23

    #@1d
    .line 401
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    .line 396
    :cond_20
    :goto_20
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_e

    #@23
    .line 402
    :cond_23
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_20

    #@29
    .line 403
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    goto :goto_20

    #@2d
    .line 407
    .end local v0           #c:C
    .end local v1           #digit:I
    :cond_2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    goto :goto_3
.end method

.method public static toCallerIDMinMatch(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 870
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 871
    .local v0, np:Ljava/lang/String;
    const/4 v1, 0x7

    #@5
    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->internalGetStrippedReversed(Ljava/lang/String;I)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    return-object v1
.end method

.method public static toaFromString(Ljava/lang/String;)I
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 936
    if-eqz p0, :cond_14

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    if-lez v0, :cond_14

    #@8
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v0

    #@d
    const/16 v1, 0x2b

    #@f
    if-ne v0, v1, :cond_14

    #@11
    .line 937
    const/16 v0, 0x91

    #@13
    .line 940
    :goto_13
    return v0

    #@14
    :cond_14
    const/16 v0, 0x81

    #@16
    goto :goto_13
.end method

.method private static tryGetCountryCallingCodeAndNewIndex(Ljava/lang/String;Z)Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;
    .registers 13
    .parameter "str"
    .parameter "acceptThailandCase"

    #@0
    .prologue
    const/16 v10, 0x36

    #@2
    const/16 v9, 0x30

    #@4
    const/16 v8, 0x31

    #@6
    const/4 v6, 0x0

    #@7
    .line 3723
    const/4 v5, 0x0

    #@8
    .line 3724
    .local v5, state:I
    const/4 v0, 0x0

    #@9
    .line 3725
    .local v0, ccc:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@c
    move-result v3

    #@d
    .line 3726
    .local v3, length:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v3, :cond_17

    #@10
    .line 3727
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v1

    #@14
    .line 3728
    .local v1, ch:C
    packed-switch v5, :pswitch_data_96

    #@17
    .line 3797
    .end local v1           #ch:C
    :cond_17
    :goto_17
    return-object v6

    #@18
    .line 3730
    .restart local v1       #ch:C
    :pswitch_18
    const/16 v7, 0x2b

    #@1a
    if-ne v1, v7, :cond_20

    #@1c
    const/4 v5, 0x1

    #@1d
    .line 3726
    :cond_1d
    :goto_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_e

    #@20
    .line 3731
    :cond_20
    if-ne v1, v9, :cond_24

    #@22
    const/4 v5, 0x2

    #@23
    goto :goto_1d

    #@24
    .line 3732
    :cond_24
    if-ne v1, v8, :cond_2b

    #@26
    .line 3733
    if-eqz p1, :cond_17

    #@28
    .line 3734
    const/16 v5, 0x8

    #@2a
    goto :goto_1d

    #@2b
    .line 3738
    :cond_2b
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@2e
    move-result v7

    #@2f
    if-eqz v7, :cond_1d

    #@31
    goto :goto_17

    #@32
    .line 3744
    :pswitch_32
    if-ne v1, v9, :cond_36

    #@34
    const/4 v5, 0x3

    #@35
    goto :goto_1d

    #@36
    .line 3745
    :cond_36
    if-ne v1, v8, :cond_3a

    #@38
    const/4 v5, 0x4

    #@39
    goto :goto_1d

    #@3a
    .line 3746
    :cond_3a
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_1d

    #@40
    goto :goto_17

    #@41
    .line 3752
    :pswitch_41
    if-ne v1, v8, :cond_45

    #@43
    const/4 v5, 0x5

    #@44
    goto :goto_1d

    #@45
    .line 3753
    :cond_45
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@48
    move-result v7

    #@49
    if-eqz v7, :cond_1d

    #@4b
    goto :goto_17

    #@4c
    .line 3764
    :pswitch_4c
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->tryGetISODigit(C)I

    #@4f
    move-result v4

    #@50
    .line 3765
    .local v4, ret:I
    if-lez v4, :cond_76

    #@52
    .line 3766
    mul-int/lit8 v7, v0, 0xa

    #@54
    add-int v0, v7, v4

    #@56
    .line 3767
    const/16 v7, 0x64

    #@58
    if-ge v0, v7, :cond_60

    #@5a
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isCountryCallingCode(I)Z

    #@5d
    move-result v7

    #@5e
    if-eqz v7, :cond_68

    #@60
    .line 3768
    :cond_60
    new-instance v6, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;

    #@62
    add-int/lit8 v7, v2, 0x1

    #@64
    invoke-direct {v6, v0, v7}, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;-><init>(II)V

    #@67
    goto :goto_17

    #@68
    .line 3770
    :cond_68
    const/4 v7, 0x1

    #@69
    if-eq v5, v7, :cond_71

    #@6b
    const/4 v7, 0x3

    #@6c
    if-eq v5, v7, :cond_71

    #@6e
    const/4 v7, 0x5

    #@6f
    if-ne v5, v7, :cond_73

    #@71
    .line 3771
    :cond_71
    const/4 v5, 0x6

    #@72
    goto :goto_1d

    #@73
    .line 3773
    :cond_73
    add-int/lit8 v5, v5, 0x1

    #@75
    goto :goto_1d

    #@76
    .line 3775
    :cond_76
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@79
    move-result v7

    #@7a
    if-eqz v7, :cond_1d

    #@7c
    goto :goto_17

    #@7d
    .line 3781
    .end local v4           #ret:I
    :pswitch_7d
    if-ne v1, v10, :cond_82

    #@7f
    const/16 v5, 0x9

    #@81
    goto :goto_1d

    #@82
    .line 3782
    :cond_82
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@85
    move-result v7

    #@86
    if-eqz v7, :cond_1d

    #@88
    goto :goto_17

    #@89
    .line 3787
    :pswitch_89
    if-ne v1, v10, :cond_17

    #@8b
    .line 3788
    new-instance v6, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;

    #@8d
    const/16 v7, 0x42

    #@8f
    add-int/lit8 v8, v2, 0x1

    #@91
    invoke-direct {v6, v7, v8}, Landroid/telephony/PhoneNumberUtils$CountryCallingCodeAndNewIndex;-><init>(II)V

    #@94
    goto :goto_17

    #@95
    .line 3728
    nop

    #@96
    :pswitch_data_96
    .packed-switch 0x0
        :pswitch_18
        :pswitch_4c
        :pswitch_32
        :pswitch_4c
        :pswitch_41
        :pswitch_4c
        :pswitch_4c
        :pswitch_4c
        :pswitch_7d
        :pswitch_89
    .end packed-switch
.end method

.method private static tryGetISODigit(C)I
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 3694
    const/16 v0, 0x30

    #@2
    if-gt v0, p0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    .line 3695
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 3697
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method private static tryGetTrunkPrefixOmittedIndex(Ljava/lang/String;I)I
    .registers 7
    .parameter "str"
    .parameter "currentIndex"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 3810
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v2

    #@5
    .line 3811
    .local v2, length:I
    move v1, p1

    #@6
    .local v1, i:I
    :goto_6
    if-ge v1, v2, :cond_14

    #@8
    .line 3812
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@b
    move-result v0

    #@c
    .line 3813
    .local v0, ch:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->tryGetISODigit(C)I

    #@f
    move-result v4

    #@10
    if-ltz v4, :cond_15

    #@12
    .line 3814
    add-int/lit8 v3, v1, 0x1

    #@14
    .line 3819
    .end local v0           #ch:C
    :cond_14
    return v3

    #@15
    .line 3815
    .restart local v0       #ch:C
    :cond_15
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_14

    #@1b
    .line 3811
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_6
.end method
