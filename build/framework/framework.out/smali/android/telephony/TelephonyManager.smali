.class public Landroid/telephony/TelephonyManager;
.super Ljava/lang/Object;
.source "TelephonyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telephony/TelephonyManager$GbaBootstrappingResponse;,
        Landroid/telephony/TelephonyManager$AkaResponse;
    }
.end annotation


# static fields
.field public static final ACTION_PHONE_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.PHONE_STATE"

.field public static final CALL_STATE_IDLE:I = 0x0

.field public static final CALL_STATE_OFFHOOK:I = 0x2

.field public static final CALL_STATE_RINGING:I = 0x1

.field public static final DATA_ACTIVITY_DORMANT:I = 0x4

.field public static final DATA_ACTIVITY_IN:I = 0x1

.field public static final DATA_ACTIVITY_INOUT:I = 0x3

.field public static final DATA_ACTIVITY_NONE:I = 0x0

.field public static final DATA_ACTIVITY_OUT:I = 0x2

.field public static final DATA_CONNECTED:I = 0x2

.field public static final DATA_CONNECTING:I = 0x1

.field public static final DATA_DISCONNECTED:I = 0x0

.field public static final DATA_SUSPENDED:I = 0x3

.field public static final DATA_UNKNOWN:I = -0x1

.field public static final EXTRA_INCOMING_NUMBER:Ljava/lang/String; = "incoming_number"

.field public static final EXTRA_STATE:Ljava/lang/String; = "state"

.field public static final EXTRA_STATE_IDLE:Ljava/lang/String; = null

.field public static final EXTRA_STATE_OFFHOOK:Ljava/lang/String; = null

.field public static final EXTRA_STATE_RINGING:Ljava/lang/String; = null

.field public static final NETWORK_CLASS_2_G:I = 0x1

.field public static final NETWORK_CLASS_3_G:I = 0x2

.field public static final NETWORK_CLASS_4_G:I = 0x3

.field public static final NETWORK_CLASS_UNKNOWN:I = 0x0

.field public static final NETWORK_TYPE_1xRTT:I = 0x7

.field public static final NETWORK_TYPE_CDMA:I = 0x4

.field public static final NETWORK_TYPE_EDGE:I = 0x2

.field public static final NETWORK_TYPE_EHRPD:I = 0xe

.field public static final NETWORK_TYPE_EVDO_0:I = 0x5

.field public static final NETWORK_TYPE_EVDO_A:I = 0x6

.field public static final NETWORK_TYPE_EVDO_B:I = 0xc

.field public static final NETWORK_TYPE_GPRS:I = 0x1

.field public static final NETWORK_TYPE_HSDPA:I = 0x8

.field public static final NETWORK_TYPE_HSPA:I = 0xa

.field public static final NETWORK_TYPE_HSPAP:I = 0xf

.field public static final NETWORK_TYPE_HSUPA:I = 0x9

.field public static final NETWORK_TYPE_IDEN:I = 0xb

.field public static final NETWORK_TYPE_LTE:I = 0xd

.field public static final NETWORK_TYPE_TD_SCDMA:I = 0x11

.field public static final NETWORK_TYPE_UMTS:I = 0x3

.field public static final NETWORK_TYPE_UNKNOWN:I = 0x0

.field public static final PHONE_TYPE_CDMA:I = 0x2

.field public static final PHONE_TYPE_GSM:I = 0x1

.field public static final PHONE_TYPE_NONE:I = 0x0

.field public static final PHONE_TYPE_SIP:I = 0x3

.field public static final SIM_STATE_ABSENT:I = 0x1

.field public static final SIM_STATE_CARD_IO_ERROR:I = 0x6

.field public static final SIM_STATE_NETWORK_LOCKED:I = 0x4

.field public static final SIM_STATE_PIN_REQUIRED:I = 0x2

.field public static final SIM_STATE_PUK_REQUIRED:I = 0x3

.field public static final SIM_STATE_READY:I = 0x5

.field public static final SIM_STATE_UNKNOWN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "TelephonyManager"

.field private static sContext:Landroid/content/Context;

.field private static sInstance:Landroid/telephony/TelephonyManager;

.field private static final sKernelCmdLine:Ljava/lang/String;

.field private static final sLteOnCdmaProductType:Ljava/lang/String;

.field private static final sProductTypePattern:Ljava/util/regex/Pattern;

.field private static sRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 100
    new-instance v0, Landroid/telephony/TelephonyManager;

    #@2
    invoke-direct {v0}, Landroid/telephony/TelephonyManager;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/TelephonyManager;->sInstance:Landroid/telephony/TelephonyManager;

    #@7
    .line 165
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneConstants$State;->toString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    sput-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    #@f
    .line 171
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneConstants$State;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@17
    .line 177
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@19
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneConstants$State;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    sput-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    #@1f
    .line 470
    invoke-static {}, Landroid/telephony/TelephonyManager;->getProcCmdLine()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    sput-object v0, Landroid/telephony/TelephonyManager;->sKernelCmdLine:Ljava/lang/String;

    #@25
    .line 473
    const-string v0, "\\sproduct_type\\s*=\\s*(\\w+)"

    #@27
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@2a
    move-result-object v0

    #@2b
    sput-object v0, Landroid/telephony/TelephonyManager;->sProductTypePattern:Ljava/util/regex/Pattern;

    #@2d
    .line 477
    const-string/jumbo v0, "telephony.lteOnCdmaProductType"

    #@30
    const-string v1, ""

    #@32
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    sput-object v0, Landroid/telephony/TelephonyManager;->sLteOnCdmaProductType:Ljava/lang/String;

    #@38
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 97
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    sget-object v1, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@5
    if-nez v1, :cond_1c

    #@7
    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    .line 85
    .local v0, appContext:Landroid/content/Context;
    if-eqz v0, :cond_1d

    #@d
    .line 86
    sput-object v0, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@f
    .line 91
    :goto_f
    const-string/jumbo v1, "telephony.registry"

    #@12
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@15
    move-result-object v1

    #@16
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephonyRegistry;

    #@19
    move-result-object v1

    #@1a
    sput-object v1, Landroid/telephony/TelephonyManager;->sRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@1c
    .line 94
    .end local v0           #appContext:Landroid/content/Context;
    :cond_1c
    return-void

    #@1d
    .line 88
    .restart local v0       #appContext:Landroid/content/Context;
    :cond_1d
    sput-object p1, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@1f
    goto :goto_f
.end method

.method public static from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 110
    const-string/jumbo v0, "phone"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@9
    return-object v0
.end method

.method public static getDefault()Landroid/telephony/TelephonyManager;
    .registers 1

    #@0
    .prologue
    .line 105
    sget-object v0, Landroid/telephony/TelephonyManager;->sInstance:Landroid/telephony/TelephonyManager;

    #@2
    return-object v0
.end method

.method public static getDefaultSubscription()I
    .registers 2

    #@0
    .prologue
    .line 596
    const-string/jumbo v0, "persist.default.subscription"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private getITelephony()Lcom/android/internal/telephony/ITelephony;
    .registers 2

    #@0
    .prologue
    .line 1287
    const-string/jumbo v0, "phone"

    #@3
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static getLteOnCdmaModeStatic()I
    .registers 7

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 493
    const-string v2, ""

    #@3
    .line 495
    .local v2, productType:Ljava/lang/String;
    const-string/jumbo v4, "telephony.lteOnCdmaDevice"

    #@6
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@9
    move-result v0

    #@a
    .line 497
    .local v0, curVal:I
    move v3, v0

    #@b
    .line 498
    .local v3, retVal:I
    if-ne v3, v5, :cond_29

    #@d
    .line 499
    sget-object v4, Landroid/telephony/TelephonyManager;->sProductTypePattern:Ljava/util/regex/Pattern;

    #@f
    sget-object v5, Landroid/telephony/TelephonyManager;->sKernelCmdLine:Ljava/lang/String;

    #@11
    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@14
    move-result-object v1

    #@15
    .line 500
    .local v1, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_6a

    #@1b
    .line 501
    const/4 v4, 0x1

    #@1c
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    .line 502
    sget-object v4, Landroid/telephony/TelephonyManager;->sLteOnCdmaProductType:Ljava/lang/String;

    #@22
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_68

    #@28
    .line 503
    const/4 v3, 0x1

    #@29
    .line 512
    .end local v1           #matcher:Ljava/util/regex/Matcher;
    :cond_29
    :goto_29
    const-string v4, "TelephonyManager"

    #@2b
    new-instance v5, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v6, "getLteOnCdmaMode="

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    const-string v6, " curVal="

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    const-string v6, " product_type=\'"

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    const-string v6, "\' lteOnCdmaProductType=\'"

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    sget-object v6, Landroid/telephony/TelephonyManager;->sLteOnCdmaProductType:Ljava/lang/String;

    #@56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    const-string v6, "\'"

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 515
    return v3

    #@68
    .line 505
    .restart local v1       #matcher:Ljava/util/regex/Matcher;
    :cond_68
    const/4 v3, 0x0

    #@69
    goto :goto_29

    #@6a
    .line 508
    :cond_6a
    const/4 v3, 0x0

    #@6b
    goto :goto_29
.end method

.method public static getNetworkClass(I)I
    .registers 2
    .parameter "networkType"

    #@0
    .prologue
    .line 709
    packed-switch p0, :pswitch_data_c

    #@3
    .line 730
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 715
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 726
    :pswitch_7
    const/4 v0, 0x2

    #@8
    goto :goto_4

    #@9
    .line 728
    :pswitch_9
    const/4 v0, 0x3

    #@a
    goto :goto_4

    #@b
    .line 709
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method public static getNetworkTypeName(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 747
    packed-switch p0, :pswitch_data_36

    #@3
    .line 781
    :pswitch_3
    const-string v0, "UNKNOWN"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 749
    :pswitch_6
    const-string v0, "GPRS"

    #@8
    goto :goto_5

    #@9
    .line 751
    :pswitch_9
    const-string v0, "EDGE"

    #@b
    goto :goto_5

    #@c
    .line 753
    :pswitch_c
    const-string v0, "UMTS"

    #@e
    goto :goto_5

    #@f
    .line 755
    :pswitch_f
    const-string v0, "HSDPA"

    #@11
    goto :goto_5

    #@12
    .line 757
    :pswitch_12
    const-string v0, "HSUPA"

    #@14
    goto :goto_5

    #@15
    .line 759
    :pswitch_15
    const-string v0, "HSPA"

    #@17
    goto :goto_5

    #@18
    .line 761
    :pswitch_18
    const-string v0, "CDMA"

    #@1a
    goto :goto_5

    #@1b
    .line 763
    :pswitch_1b
    const-string v0, "CDMA - EvDo rev. 0"

    #@1d
    goto :goto_5

    #@1e
    .line 765
    :pswitch_1e
    const-string v0, "CDMA - EvDo rev. A"

    #@20
    goto :goto_5

    #@21
    .line 767
    :pswitch_21
    const-string v0, "CDMA - EvDo rev. B"

    #@23
    goto :goto_5

    #@24
    .line 769
    :pswitch_24
    const-string v0, "CDMA - 1xRTT"

    #@26
    goto :goto_5

    #@27
    .line 771
    :pswitch_27
    const-string v0, "LTE"

    #@29
    goto :goto_5

    #@2a
    .line 773
    :pswitch_2a
    const-string v0, "CDMA - eHRPD"

    #@2c
    goto :goto_5

    #@2d
    .line 775
    :pswitch_2d
    const-string v0, "iDEN"

    #@2f
    goto :goto_5

    #@30
    .line 777
    :pswitch_30
    const-string v0, "HSPA+"

    #@32
    goto :goto_5

    #@33
    .line 779
    :pswitch_33
    const-string v0, "TD_SCDMA"

    #@35
    goto :goto_5

    #@36
    .line 747
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_24
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_2d
        :pswitch_21
        :pswitch_27
        :pswitch_2a
        :pswitch_30
        :pswitch_3
        :pswitch_33
    .end packed-switch
.end method

.method public static getPhoneType(I)I
    .registers 5
    .parameter "networkMode"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    .line 390
    packed-switch p0, :pswitch_data_2e

    #@5
    move v1, v2

    #@6
    .line 437
    :cond_6
    :goto_6
    :pswitch_6
    return v1

    #@7
    :pswitch_7
    move v1, v2

    #@8
    .line 410
    goto :goto_6

    #@9
    .line 421
    :pswitch_9
    invoke-static {}, Landroid/telephony/TelephonyManager;->getLteOnCdmaModeStatic()I

    #@c
    move-result v3

    #@d
    if-ne v3, v2, :cond_2b

    #@f
    .line 423
    const-string v3, "KDDI"

    #@11
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_6

    #@17
    .line 424
    sget v0, Lcom/android/internal/telephony/RILConstants;->PREFERRED_NETWORK_MODE:I

    #@19
    .line 425
    .local v0, mode:I
    const/16 v3, 0x9

    #@1b
    if-eq v0, v3, :cond_29

    #@1d
    const/16 v3, 0xa

    #@1f
    if-eq v0, v3, :cond_29

    #@21
    const/16 v3, 0xb

    #@23
    if-eq v0, v3, :cond_29

    #@25
    const/16 v3, 0xc

    #@27
    if-ne v0, v3, :cond_6

    #@29
    :cond_29
    move v1, v2

    #@2a
    .line 429
    goto :goto_6

    #@2b
    .end local v0           #mode:I
    :cond_2b
    move v1, v2

    #@2c
    .line 434
    goto :goto_6

    #@2d
    .line 390
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_6
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private getPhoneTypeFromNetworkType()I
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 374
    const-string/jumbo v1, "ro.telephony.default_network"

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    .line 375
    .local v0, mode:I
    if-ne v0, v2, :cond_c

    #@a
    .line 376
    const/4 v1, 0x0

    #@b
    .line 377
    :goto_b
    return v1

    #@c
    :cond_c
    invoke-static {v0}, Landroid/telephony/TelephonyManager;->getPhoneType(I)I

    #@f
    move-result v1

    #@10
    goto :goto_b
.end method

.method private getPhoneTypeFromProperty()I
    .registers 4

    #@0
    .prologue
    .line 364
    const-string v1, "gsm.current.phone-type"

    #@2
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getPhoneTypeFromNetworkType()I

    #@5
    move-result v2

    #@6
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@9
    move-result v0

    #@a
    .line 367
    .local v0, type:I
    return v0
.end method

.method private static getProcCmdLine()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    .line 446
    const-string v1, ""

    #@2
    .line 447
    .local v1, cmdline:Ljava/lang/String;
    const/4 v5, 0x0

    #@3
    .line 449
    .local v5, is:Ljava/io/FileInputStream;
    :try_start_3
    new-instance v6, Ljava/io/FileInputStream;

    #@5
    const-string v7, "/proc/cmdline"

    #@7
    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_5e
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_a} :catch_3d

    #@a
    .line 450
    .end local v5           #is:Ljava/io/FileInputStream;
    .local v6, is:Ljava/io/FileInputStream;
    const/16 v7, 0x800

    #@c
    :try_start_c
    new-array v0, v7, [B

    #@e
    .line 451
    .local v0, buffer:[B
    invoke-virtual {v6, v0}, Ljava/io/FileInputStream;->read([B)I

    #@11
    move-result v3

    #@12
    .line 452
    .local v3, count:I
    if-lez v3, :cond_1b

    #@14
    .line 453
    new-instance v2, Ljava/lang/String;

    #@16
    const/4 v7, 0x0

    #@17
    invoke-direct {v2, v0, v7, v3}, Ljava/lang/String;-><init>([BII)V
    :try_end_1a
    .catchall {:try_start_c .. :try_end_1a} :catchall_67
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_1a} :catch_6a

    #@1a
    .end local v1           #cmdline:Ljava/lang/String;
    .local v2, cmdline:Ljava/lang/String;
    move-object v1, v2

    #@1b
    .line 458
    .end local v2           #cmdline:Ljava/lang/String;
    .restart local v1       #cmdline:Ljava/lang/String;
    :cond_1b
    if-eqz v6, :cond_6d

    #@1d
    .line 460
    :try_start_1d
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_3a

    #@20
    move-object v5, v6

    #@21
    .line 465
    .end local v0           #buffer:[B
    .end local v3           #count:I
    .end local v6           #is:Ljava/io/FileInputStream;
    .restart local v5       #is:Ljava/io/FileInputStream;
    :cond_21
    :goto_21
    const-string v7, "TelephonyManager"

    #@23
    new-instance v8, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v9, "/proc/cmdline="

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v8

    #@32
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v8

    #@36
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 466
    return-object v1

    #@3a
    .line 461
    .end local v5           #is:Ljava/io/FileInputStream;
    .restart local v0       #buffer:[B
    .restart local v3       #count:I
    .restart local v6       #is:Ljava/io/FileInputStream;
    :catch_3a
    move-exception v7

    #@3b
    move-object v5, v6

    #@3c
    .line 462
    .end local v6           #is:Ljava/io/FileInputStream;
    .restart local v5       #is:Ljava/io/FileInputStream;
    goto :goto_21

    #@3d
    .line 455
    .end local v0           #buffer:[B
    .end local v3           #count:I
    :catch_3d
    move-exception v4

    #@3e
    .line 456
    .local v4, e:Ljava/io/IOException;
    :goto_3e
    :try_start_3e
    const-string v7, "TelephonyManager"

    #@40
    new-instance v8, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v9, "No /proc/cmdline exception="

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v8

    #@53
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_56
    .catchall {:try_start_3e .. :try_end_56} :catchall_5e

    #@56
    .line 458
    if-eqz v5, :cond_21

    #@58
    .line 460
    :try_start_58
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5b
    .catch Ljava/io/IOException; {:try_start_58 .. :try_end_5b} :catch_5c

    #@5b
    goto :goto_21

    #@5c
    .line 461
    :catch_5c
    move-exception v7

    #@5d
    goto :goto_21

    #@5e
    .line 458
    .end local v4           #e:Ljava/io/IOException;
    :catchall_5e
    move-exception v7

    #@5f
    :goto_5f
    if-eqz v5, :cond_64

    #@61
    .line 460
    :try_start_61
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_64} :catch_65

    #@64
    .line 462
    :cond_64
    :goto_64
    throw v7

    #@65
    .line 461
    :catch_65
    move-exception v8

    #@66
    goto :goto_64

    #@67
    .line 458
    .end local v5           #is:Ljava/io/FileInputStream;
    .restart local v6       #is:Ljava/io/FileInputStream;
    :catchall_67
    move-exception v7

    #@68
    move-object v5, v6

    #@69
    .end local v6           #is:Ljava/io/FileInputStream;
    .restart local v5       #is:Ljava/io/FileInputStream;
    goto :goto_5f

    #@6a
    .line 455
    .end local v5           #is:Ljava/io/FileInputStream;
    .restart local v6       #is:Ljava/io/FileInputStream;
    :catch_6a
    move-exception v4

    #@6b
    move-object v5, v6

    #@6c
    .end local v6           #is:Ljava/io/FileInputStream;
    .restart local v5       #is:Ljava/io/FileInputStream;
    goto :goto_3e

    #@6d
    .end local v5           #is:Ljava/io/FileInputStream;
    .restart local v0       #buffer:[B
    .restart local v3       #count:I
    .restart local v6       #is:Ljava/io/FileInputStream;
    :cond_6d
    move-object v5, v6

    #@6e
    .end local v6           #is:Ljava/io/FileInputStream;
    .restart local v5       #is:Ljava/io/FileInputStream;
    goto :goto_21
.end method

.method private getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;
    .registers 2

    #@0
    .prologue
    .line 1185
    const-string v0, "iphonesubinfo"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/IPhoneSubInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IPhoneSubInfo;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "property"
    .parameter "index"
    .parameter "defaultVal"

    #@0
    .prologue
    .line 606
    const/4 v1, 0x0

    #@1
    .line 607
    .local v1, propVal:Ljava/lang/String;
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 608
    .local v0, prop:Ljava/lang/String;
    if-eqz v0, :cond_1e

    #@7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@a
    move-result v3

    #@b
    if-lez v3, :cond_1e

    #@d
    .line 609
    const-string v3, ","

    #@f
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 610
    .local v2, values:[Ljava/lang/String;
    if-ltz p1, :cond_1e

    #@15
    array-length v3, v2

    #@16
    if-ge p1, v3, :cond_1e

    #@18
    aget-object v3, v2, p1

    #@1a
    if-eqz v3, :cond_1e

    #@1c
    .line 611
    aget-object v1, v2, p1

    #@1e
    .line 614
    .end local v2           #values:[Ljava/lang/String;
    :cond_1e
    if-nez v1, :cond_21

    #@20
    .end local p2
    :goto_20
    return-object p2

    #@21
    .restart local p2
    :cond_21
    move-object p2, v1

    #@22
    goto :goto_20
.end method

.method public static isMultiSimEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 115
    const/4 v0, 0x0

    #@1
    return v0
.end method


# virtual methods
.method public calculateAkaResponse([B[B)Landroid/telephony/TelephonyManager$AkaResponse;
    .registers 7
    .parameter "rand"
    .parameter "autn"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1833
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1, p2}, Lcom/android/internal/telephony/ITelephony;->calculateAkaResponse([B[B)Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 1834
    .local v0, bundle:Landroid/os/Bundle;
    new-instance v2, Landroid/telephony/TelephonyManager$AkaResponse;

    #@b
    invoke-direct {v2, p0, v0}, Landroid/telephony/TelephonyManager$AkaResponse;-><init>(Landroid/telephony/TelephonyManager;Landroid/os/Bundle;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_e} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_e} :catch_12

    #@e
    .line 1838
    .end local v0           #bundle:Landroid/os/Bundle;
    :goto_e
    return-object v2

    #@f
    .line 1835
    :catch_f
    move-exception v1

    #@10
    .local v1, ex:Landroid/os/RemoteException;
    move-object v2, v3

    #@11
    .line 1836
    goto :goto_e

    #@12
    .line 1837
    .end local v1           #ex:Landroid/os/RemoteException;
    :catch_12
    move-exception v1

    #@13
    .local v1, ex:Ljava/lang/NullPointerException;
    move-object v2, v3

    #@14
    .line 1838
    goto :goto_e
.end method

.method public calculateGbaBootstrappingResponse([B[B)Landroid/telephony/TelephonyManager$GbaBootstrappingResponse;
    .registers 7
    .parameter "rand"
    .parameter "autn"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1908
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1, p2}, Lcom/android/internal/telephony/ITelephony;->calculateGbaBootstrappingResponse([B[B)Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 1909
    .local v0, bundle:Landroid/os/Bundle;
    new-instance v2, Landroid/telephony/TelephonyManager$GbaBootstrappingResponse;

    #@b
    invoke-direct {v2, p0, v0}, Landroid/telephony/TelephonyManager$GbaBootstrappingResponse;-><init>(Landroid/telephony/TelephonyManager;Landroid/os/Bundle;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_e} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_e} :catch_12

    #@e
    .line 1913
    .end local v0           #bundle:Landroid/os/Bundle;
    :goto_e
    return-object v2

    #@f
    .line 1910
    :catch_f
    move-exception v1

    #@10
    .local v1, ex:Landroid/os/RemoteException;
    move-object v2, v3

    #@11
    .line 1911
    goto :goto_e

    #@12
    .line 1912
    .end local v1           #ex:Landroid/os/RemoteException;
    :catch_12
    move-exception v1

    #@13
    .local v1, ex:Ljava/lang/NullPointerException;
    move-object v2, v3

    #@14
    .line 1913
    goto :goto_e
.end method

.method public calculateNafExternalKey([B)[B
    .registers 5
    .parameter "nafId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1920
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->calculateNafExternalKey([B)[B
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1924
    :goto_9
    return-object v1

    #@a
    .line 1921
    :catch_a
    move-exception v0

    #@b
    .line 1922
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1923
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1924
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public disableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 283
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->disableLocationUpdates()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 287
    :goto_7
    return-void

    #@8
    .line 285
    :catch_8
    move-exception v0

    #@9
    goto :goto_7

    #@a
    .line 284
    :catch_a
    move-exception v0

    #@b
    goto :goto_7
.end method

.method public enableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 266
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->enableLocationUpdates()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 270
    :goto_7
    return-void

    #@8
    .line 268
    :catch_8
    move-exception v0

    #@9
    goto :goto_7

    #@a
    .line 267
    :catch_a
    move-exception v0

    #@b
    goto :goto_7
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1463
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getAllCellInfo()Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1467
    :goto_9
    return-object v1

    #@a
    .line 1464
    :catch_a
    move-exception v0

    #@b
    .line 1465
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1466
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1467
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getBtid()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1875
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getBtid()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1879
    :goto_9
    return-object v1

    #@a
    .line 1876
    :catch_a
    move-exception v0

    #@b
    .line 1877
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1878
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1879
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCallState()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1205
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getCallState()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1211
    :goto_9
    return v1

    #@a
    .line 1206
    :catch_a
    move-exception v0

    #@b
    .line 1208
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1209
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1211
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriHomeSystems()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1428
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriHomeSystems()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1432
    :goto_9
    return-object v1

    #@a
    .line 1429
    :catch_a
    move-exception v0

    #@b
    .line 1430
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1431
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1432
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriIconIndex()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1338
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriIconIndex()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1343
    :goto_9
    return v1

    #@a
    .line 1339
    :catch_a
    move-exception v0

    #@b
    .line 1341
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1342
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1343
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriIconMode()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1356
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriIconMode()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1361
    :goto_9
    return v1

    #@a
    .line 1357
    :catch_a
    move-exception v0

    #@b
    .line 1359
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1360
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1361
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCdmaEriText()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1372
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getCdmaEriText()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1377
    :goto_9
    return-object v1

    #@a
    .line 1373
    :catch_a
    move-exception v0

    #@b
    .line 1375
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1376
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1377
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCellLocation()Landroid/telephony/CellLocation;
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 242
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v4

    #@5
    invoke-interface {v4}, Lcom/android/internal/telephony/ITelephony;->getCellLocation()Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 243
    .local v0, bundle:Landroid/os/Bundle;
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_11

    #@f
    move-object v1, v3

    #@10
    .line 251
    .end local v0           #bundle:Landroid/os/Bundle;
    :cond_10
    :goto_10
    return-object v1

    #@11
    .line 244
    .restart local v0       #bundle:Landroid/os/Bundle;
    :cond_11
    invoke-static {v0}, Landroid/telephony/CellLocation;->newFromBundle(Landroid/os/Bundle;)Landroid/telephony/CellLocation;

    #@14
    move-result-object v1

    #@15
    .line 245
    .local v1, cl:Landroid/telephony/CellLocation;
    invoke-virtual {v1}, Landroid/telephony/CellLocation;->isEmpty()Z
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_18} :catch_1d
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_18} :catch_20

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_10

    #@1b
    move-object v1, v3

    #@1c
    .line 246
    goto :goto_10

    #@1d
    .line 248
    .end local v0           #bundle:Landroid/os/Bundle;
    .end local v1           #cl:Landroid/telephony/CellLocation;
    :catch_1d
    move-exception v2

    #@1e
    .local v2, ex:Landroid/os/RemoteException;
    move-object v1, v3

    #@1f
    .line 249
    goto :goto_10

    #@20
    .line 250
    .end local v2           #ex:Landroid/os/RemoteException;
    :catch_20
    move-exception v2

    #@21
    .local v2, ex:Ljava/lang/NullPointerException;
    move-object v1, v3

    #@22
    .line 251
    goto :goto_10
.end method

.method public getCompleteVoiceMailNumber()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1089
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getCompleteVoiceMailNumber()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1094
    :goto_9
    return-object v1

    #@a
    .line 1090
    :catch_a
    move-exception v0

    #@b
    .line 1091
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1092
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1094
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getCurrentPhoneType()I
    .registers 4

    #@0
    .prologue
    .line 329
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v1

    #@4
    .line 330
    .local v1, telephony:Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_b

    #@6
    .line 331
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getActivePhoneType()I

    #@9
    move-result v2

    #@a
    .line 343
    .end local v1           #telephony:Lcom/android/internal/telephony/ITelephony;
    :goto_a
    return v2

    #@b
    .line 334
    .restart local v1       #telephony:Lcom/android/internal/telephony/ITelephony;
    :cond_b
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getPhoneTypeFromProperty()I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_10
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_e} :catch_16

    #@e
    move-result v2

    #@f
    goto :goto_a

    #@10
    .line 336
    .end local v1           #telephony:Lcom/android/internal/telephony/ITelephony;
    :catch_10
    move-exception v0

    #@11
    .line 339
    .local v0, ex:Landroid/os/RemoteException;
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getPhoneTypeFromProperty()I

    #@14
    move-result v2

    #@15
    goto :goto_a

    #@16
    .line 340
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_16
    move-exception v0

    #@17
    .line 343
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getPhoneTypeFromProperty()I

    #@1a
    move-result v2

    #@1b
    goto :goto_a
.end method

.method public getDataActivity()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1241
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getDataActivity()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1247
    :goto_9
    return v1

    #@a
    .line 1242
    :catch_a
    move-exception v0

    #@b
    .line 1244
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1245
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1247
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDataState()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1277
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getDataState()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1282
    :goto_9
    return v1

    #@a
    .line 1278
    :catch_a
    move-exception v0

    #@b
    .line 1280
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1281
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1282
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDebugInfo(II)[I
    .registers 7
    .parameter "infotype"
    .parameter "itemnum"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1737
    :try_start_1
    const-string v2, "TelephonyManager"

    #@3
    const-string v3, "getDebugInfo"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1739
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@b
    move-result-object v2

    #@c
    if-nez v2, :cond_f

    #@e
    .line 1744
    :goto_e
    return-object v1

    #@f
    .line 1741
    :cond_f
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@12
    move-result-object v2

    #@13
    invoke-interface {v2, p1, p2}, Lcom/android/internal/telephony/ITelephony;->getDebugInfo(II)[I
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_16} :catch_18

    #@16
    move-result-object v1

    #@17
    goto :goto_e

    #@18
    .line 1742
    :catch_18
    move-exception v0

    #@19
    .line 1744
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_e
.end method

.method public getDeviceId()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 224
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getDeviceId()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 228
    :goto_9
    return-object v1

    #@a
    .line 225
    :catch_a
    move-exception v0

    #@b
    .line 226
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 227
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 228
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDeviceId(I)Ljava/lang/String;
    .registers 5
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1755
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/IPhoneSubInfo;->getDeviceId_type(I)Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1760
    :goto_9
    return-object v1

    #@a
    .line 1756
    :catch_a
    move-exception v0

    #@b
    .line 1757
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1758
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1760
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getDeviceSoftwareVersion()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 207
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getDeviceSvn()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 211
    :goto_9
    return-object v1

    #@a
    .line 208
    :catch_a
    move-exception v0

    #@b
    .line 209
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 210
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 211
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getIPPhoneState()Z
    .registers 4

    #@0
    .prologue
    .line 1714
    const-string v1, "TelephonyManager"

    #@2
    const-string v2, "getIPPhoneState: try modified."

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1716
    :try_start_7
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v1}, Lcom/android/internal/telephony/IPhoneSubInfo;->getIPPhoneState()Z
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_e} :catch_10
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_e} :catch_16

    #@e
    move-result v1

    #@f
    .line 1723
    :goto_f
    return v1

    #@10
    .line 1717
    :catch_10
    move-exception v0

    #@11
    .line 1718
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@14
    .line 1723
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_14
    const/4 v1, 0x0

    #@15
    goto :goto_f

    #@16
    .line 1719
    :catch_16
    move-exception v0

    #@17
    .line 1721
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@1a
    goto :goto_14
.end method

.method public getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1069
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1074
    :goto_9
    return-object v1

    #@a
    .line 1070
    :catch_a
    move-exception v0

    #@b
    .line 1071
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1072
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1074
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getIsimDomain()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1157
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getIsimDomain()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1162
    :goto_9
    return-object v1

    #@a
    .line 1158
    :catch_a
    move-exception v0

    #@b
    .line 1159
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1160
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1162
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getIsimImpi()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1141
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getIsimImpi()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1146
    :goto_9
    return-object v1

    #@a
    .line 1142
    :catch_a
    move-exception v0

    #@b
    .line 1143
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1144
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1146
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getIsimImpu()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1174
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getIsimImpu()[Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1179
    :goto_9
    return-object v1

    #@a
    .line 1175
    :catch_a
    move-exception v0

    #@b
    .line 1176
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1177
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1179
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getKeyLifetime()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1886
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getKeyLifetime()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1890
    :goto_9
    return-object v1

    #@a
    .line 1887
    :catch_a
    move-exception v0

    #@b
    .line 1888
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1889
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1890
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getLine1AlphaTag()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1012
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getLine1AlphaTag()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1017
    :goto_9
    return-object v1

    #@a
    .line 1013
    :catch_a
    move-exception v0

    #@b
    .line 1014
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1015
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1017
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 992
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getLine1Number()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 997
    :goto_9
    return-object v1

    #@a
    .line 993
    :catch_a
    move-exception v0

    #@b
    .line 994
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 995
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 997
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getLteOnCdmaMode()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 949
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getLteOnCdmaMode()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 955
    :goto_9
    return v1

    #@a
    .line 950
    :catch_a
    move-exception v0

    #@b
    .line 952
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 953
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 955
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getMSIN()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1770
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getMSIN()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1774
    :goto_9
    return-object v1

    #@a
    .line 1771
    :catch_a
    move-exception v0

    #@b
    .line 1772
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1773
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1774
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getMobileQualityInformation()Ljava/util/HashMap;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1523
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v1

    #@5
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getMobileQualityInformation()Ljava/util/Map;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Ljava/util/HashMap;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_b} :catch_c
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_b} :catch_f

    #@b
    .line 1527
    :goto_b
    return-object v1

    #@c
    .line 1524
    :catch_c
    move-exception v0

    #@d
    .local v0, ex:Landroid/os/RemoteException;
    move-object v1, v2

    #@e
    .line 1525
    goto :goto_b

    #@f
    .line 1526
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_f
    move-exception v0

    #@10
    .local v0, ex:Ljava/lang/NullPointerException;
    move-object v1, v2

    #@11
    .line 1527
    goto :goto_b
.end method

.method public getMsisdn()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1032
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getMsisdn()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1037
    :goto_9
    return-object v1

    #@a
    .line 1033
    :catch_a
    move-exception v0

    #@b
    .line 1034
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1035
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1037
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getNeighboringCellInfo()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/NeighboringCellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 299
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getNeighboringCellInfo()Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 303
    :goto_9
    return-object v1

    #@a
    .line 300
    :catch_a
    move-exception v0

    #@b
    .line 301
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 302
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 303
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getNetworkCountryIso()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 580
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_15

    #@a
    .line 581
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@d
    move-result v0

    #@e
    const/4 v1, 0x2

    #@f
    if-ne v0, v1, :cond_15

    #@11
    .line 582
    const-string/jumbo v0, "us"

    #@14
    .line 586
    :goto_14
    return-object v0

    #@15
    :cond_15
    const-string v0, "gsm.operator.iso-country"

    #@17
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefaultSubscription()I

    #@1a
    move-result v1

    #@1b
    const-string v2, ""

    #@1d
    invoke-static {v0, v1, v2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    goto :goto_14
.end method

.method public getNetworkOperator()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 551
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_19

    #@a
    .line 552
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@d
    move-result v0

    #@e
    const/4 v1, 0x2

    #@f
    if-ne v0, v1, :cond_19

    #@11
    .line 553
    const-string/jumbo v0, "ro.cdma.home.operator.numeric"

    #@14
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 557
    :goto_18
    return-object v0

    #@19
    :cond_19
    const-string v0, "gsm.operator.numeric"

    #@1b
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_18
.end method

.method public getNetworkOperatorName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 533
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_19

    #@a
    .line 534
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@d
    move-result v0

    #@e
    const/4 v1, 0x2

    #@f
    if-ne v0, v1, :cond_19

    #@11
    .line 535
    const-string/jumbo v0, "ro.cdma.home.operator.alpha"

    #@14
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 539
    :goto_18
    return-object v0

    #@19
    :cond_19
    const-string v0, "gsm.operator.alpha"

    #@1b
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_18
.end method

.method public getNetworkType()I
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 677
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v1

    #@5
    .line 678
    .local v1, telephony:Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_b

    #@7
    .line 679
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getNetworkType()I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_c
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_a} :catch_e

    #@a
    move-result v2

    #@b
    .line 689
    .end local v1           #telephony:Lcom/android/internal/telephony/ITelephony;
    :cond_b
    :goto_b
    return v2

    #@c
    .line 684
    :catch_c
    move-exception v0

    #@d
    .line 686
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_b

    #@e
    .line 687
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_e
    move-exception v0

    #@f
    .line 689
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_b
.end method

.method public getNetworkTypeName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 742
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Landroid/telephony/TelephonyManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 357
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->isVoiceCapable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 358
    const/4 v0, 0x0

    #@7
    .line 360
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method

.method public getRand()[B
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1864
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getRand()[B
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1868
    :goto_9
    return-object v1

    #@a
    .line 1865
    :catch_a
    move-exception v0

    #@b
    .line 1866
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1867
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1868
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getSimCountryIso()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 908
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_24

    #@a
    .line 909
    const-string v0, "gsm.sim.type"

    #@c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string/jumbo v1, "spr"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_20

    #@19
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@1c
    move-result v0

    #@1d
    const/4 v1, 0x1

    #@1e
    if-ne v0, v1, :cond_24

    #@20
    .line 911
    :cond_20
    const-string/jumbo v0, "us"

    #@23
    .line 915
    :goto_23
    return-object v0

    #@24
    :cond_24
    const-string v0, "gsm.sim.operator.iso-country"

    #@26
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefaultSubscription()I

    #@29
    move-result v1

    #@2a
    const-string v2, ""

    #@2c
    invoke-static {v0, v1, v2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    goto :goto_23
.end method

.method public getSimOperator()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 873
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_28

    #@a
    .line 874
    const-string v0, "gsm.sim.type"

    #@c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string/jumbo v1, "spr"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_20

    #@19
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@1c
    move-result v0

    #@1d
    const/4 v1, 0x1

    #@1e
    if-ne v0, v1, :cond_28

    #@20
    .line 876
    :cond_20
    const-string/jumbo v0, "ro.cdma.home.operator.numeric"

    #@23
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 880
    :goto_27
    return-object v0

    #@28
    :cond_28
    const-string v0, "gsm.sim.operator.numeric"

    #@2a
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefaultSubscription()I

    #@2d
    move-result v1

    #@2e
    const-string v2, ""

    #@30
    invoke-static {v0, v1, v2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    goto :goto_27
.end method

.method public getSimOperatorName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 893
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_28

    #@a
    .line 894
    const-string v0, "gsm.sim.type"

    #@c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string/jumbo v1, "spr"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_20

    #@19
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@1c
    move-result v0

    #@1d
    const/4 v1, 0x1

    #@1e
    if-ne v0, v1, :cond_28

    #@20
    .line 896
    :cond_20
    const-string/jumbo v0, "ro.cdma.home.operator.alpha"

    #@23
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 900
    :goto_27
    return-object v0

    #@28
    :cond_28
    const-string v0, "gsm.sim.operator.alpha"

    #@2a
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    goto :goto_27
.end method

.method public getSimSerialNumber()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 928
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getIccSerialNumber()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 933
    :goto_9
    return-object v1

    #@a
    .line 929
    :catch_a
    move-exception v0

    #@b
    .line 930
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 931
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 933
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getSimState()I
    .registers 5

    #@0
    .prologue
    .line 838
    const-string v1, "gsm.sim.state"

    #@2
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefaultSubscription()I

    #@5
    move-result v2

    #@6
    const-string v3, ""

    #@8
    invoke-static {v1, v2, v3}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 840
    .local v0, prop:Ljava/lang/String;
    const-string v1, "ABSENT"

    #@e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_16

    #@14
    .line 841
    const/4 v1, 0x1

    #@15
    .line 859
    :goto_15
    return v1

    #@16
    .line 843
    :cond_16
    const-string v1, "PIN_REQUIRED"

    #@18
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_20

    #@1e
    .line 844
    const/4 v1, 0x2

    #@1f
    goto :goto_15

    #@20
    .line 846
    :cond_20
    const-string v1, "PUK_REQUIRED"

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_2a

    #@28
    .line 847
    const/4 v1, 0x3

    #@29
    goto :goto_15

    #@2a
    .line 849
    :cond_2a
    const-string v1, "PERSO_LOCKED"

    #@2c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_34

    #@32
    .line 850
    const/4 v1, 0x4

    #@33
    goto :goto_15

    #@34
    .line 852
    :cond_34
    const-string v1, "READY"

    #@36
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_3e

    #@3c
    .line 853
    const/4 v1, 0x5

    #@3d
    goto :goto_15

    #@3e
    .line 855
    :cond_3e
    const-string v1, "CARD_IO_ERROR"

    #@40
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v1

    #@44
    if-eqz v1, :cond_48

    #@46
    .line 856
    const/4 v1, 0x6

    #@47
    goto :goto_15

    #@48
    .line 859
    :cond_48
    const/4 v1, 0x0

    #@49
    goto :goto_15
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 974
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getSubscriberId()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 979
    :goto_9
    return-object v1

    #@a
    .line 975
    :catch_a
    move-exception v0

    #@b
    .line 976
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 977
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 979
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1125
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getVoiceMailAlphaTag()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1130
    :goto_9
    return-object v1

    #@a
    .line 1126
    :catch_a
    move-exception v0

    #@b
    .line 1127
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1128
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1130
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1049
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->getVoiceMailNumber()Ljava/lang/String;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result-object v1

    #@9
    .line 1054
    :goto_9
    return-object v1

    #@a
    .line 1050
    :catch_a
    move-exception v0

    #@b
    .line 1051
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1052
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1054
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public getVoiceMessageCount()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1107
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getVoiceMessageCount()I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1112
    :goto_9
    return v1

    #@a
    .line 1108
    :catch_a
    move-exception v0

    #@b
    .line 1109
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1110
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1112
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public hasIccCard()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 815
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->hasIccCard()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 821
    :goto_9
    return v1

    #@a
    .line 816
    :catch_a
    move-exception v0

    #@b
    .line 818
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 819
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 821
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public hasIsim()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1791
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->hasIsim()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1795
    :goto_9
    return v1

    #@a
    .line 1792
    :catch_a
    move-exception v0

    #@b
    .line 1793
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1794
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1795
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public isGbaSupported()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1853
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getSubscriberInfo()Lcom/android/internal/telephony/IPhoneSubInfo;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/IPhoneSubInfo;->isGbaSupported()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1857
    :goto_9
    return v1

    #@a
    .line 1854
    :catch_a
    move-exception v0

    #@b
    .line 1855
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1856
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1857
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public isNetworkRoaming()Z
    .registers 3

    #@0
    .prologue
    .line 567
    const-string/jumbo v0, "true"

    #@3
    const-string v1, "gsm.operator.isroaming"

    #@5
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public isSmsCapable()Z
    .registers 3

    #@0
    .prologue
    .line 1448
    sget-object v0, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 1449
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@8
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    const v1, 0x1110031

    #@f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@12
    move-result v0

    #@13
    goto :goto_5
.end method

.method public isVoiceCapable()Z
    .registers 3

    #@0
    .prologue
    .line 1398
    sget-object v0, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 1399
    :goto_5
    return v0

    #@6
    :cond_6
    sget-object v0, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@8
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    const v1, 0x1110030

    #@f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@12
    move-result v0

    #@13
    goto :goto_5
.end method

.method public isVoiceInService()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1411
    :try_start_1
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v2

    #@5
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->isVoiceInService()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_8} :catch_c

    #@8
    move-result v1

    #@9
    .line 1415
    :goto_9
    return v1

    #@a
    .line 1412
    :catch_a
    move-exception v0

    #@b
    .line 1413
    .local v0, ex:Landroid/os/RemoteException;
    goto :goto_9

    #@c
    .line 1414
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    #@d
    .line 1415
    .local v0, ex:Ljava/lang/NullPointerException;
    goto :goto_9
.end method

.method public listen(Landroid/telephony/PhoneStateListener;I)V
    .registers 8
    .parameter "listener"
    .parameter "events"

    #@0
    .prologue
    .line 1320
    sget-object v2, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@2
    if-eqz v2, :cond_21

    #@4
    sget-object v2, Landroid/telephony/TelephonyManager;->sContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 1322
    .local v1, pkgForDebug:Ljava/lang/String;
    :goto_a
    :try_start_a
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@d
    move-result-object v2

    #@e
    if-eqz v2, :cond_24

    #@10
    const/4 v2, 0x1

    #@11
    :goto_11
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@14
    move-result-object v0

    #@15
    .line 1323
    .local v0, notifyNow:Ljava/lang/Boolean;
    sget-object v2, Landroid/telephony/TelephonyManager;->sRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;

    #@17
    iget-object v3, p1, Landroid/telephony/PhoneStateListener;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@19
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@1c
    move-result v4

    #@1d
    invoke-interface {v2, v1, v3, p2, v4}, Lcom/android/internal/telephony/ITelephonyRegistry;->listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZ)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_20} :catch_28
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_20} :catch_26

    #@20
    .line 1329
    .end local v0           #notifyNow:Ljava/lang/Boolean;
    :goto_20
    return-void

    #@21
    .line 1320
    .end local v1           #pkgForDebug:Ljava/lang/String;
    :cond_21
    const-string v1, "<unknown>"

    #@23
    goto :goto_a

    #@24
    .line 1322
    .restart local v1       #pkgForDebug:Ljava/lang/String;
    :cond_24
    const/4 v2, 0x0

    #@25
    goto :goto_11

    #@26
    .line 1326
    :catch_26
    move-exception v2

    #@27
    goto :goto_20

    #@28
    .line 1324
    :catch_28
    move-exception v2

    #@29
    goto :goto_20
.end method

.method public resetVoiceMessageCount()V
    .registers 2

    #@0
    .prologue
    .line 1542
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->resetVoiceMessageCount()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1549
    :goto_7
    return-void

    #@8
    .line 1545
    :catch_8
    move-exception v0

    #@9
    goto :goto_7

    #@a
    .line 1543
    :catch_a
    move-exception v0

    #@b
    goto :goto_7
.end method

.method public setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "rand"
    .parameter "btid"
    .parameter "keyLifetime"

    #@0
    .prologue
    .line 1897
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/ITelephony;->setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1903
    :goto_7
    return-void

    #@8
    .line 1900
    :catch_8
    move-exception v0

    #@9
    goto :goto_7

    #@a
    .line 1898
    :catch_a
    move-exception v0

    #@b
    goto :goto_7
.end method

.method public startMobileQualityInformation()V
    .registers 3

    #@0
    .prologue
    .line 1482
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->startMobileQualityInformation()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_d

    #@7
    .line 1489
    :goto_7
    return-void

    #@8
    .line 1483
    :catch_8
    move-exception v0

    #@9
    .line 1484
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@c
    goto :goto_7

    #@d
    .line 1485
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_d
    move-exception v0

    #@e
    .line 1486
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@11
    goto :goto_7
.end method

.method public stopMobileQualityInformation()V
    .registers 3

    #@0
    .prologue
    .line 1495
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->stopMobileQualityInformation()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_d

    #@7
    .line 1501
    :goto_7
    return-void

    #@8
    .line 1496
    :catch_8
    move-exception v0

    #@9
    .line 1497
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@c
    goto :goto_7

    #@d
    .line 1498
    .end local v0           #ex:Landroid/os/RemoteException;
    :catch_d
    move-exception v0

    #@e
    .line 1499
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@11
    goto :goto_7
.end method

.method public uknight_event_set([B)[B
    .registers 6
    .parameter "mask"

    #@0
    .prologue
    .line 1576
    if-nez p1, :cond_12

    #@2
    .line 1577
    const/4 v2, 0x1

    #@3
    :try_start_3
    new-array v1, v2, [B

    #@5
    .line 1578
    .local v1, unset:[B
    const/4 v2, 0x0

    #@6
    const/4 v3, -0x1

    #@7
    aput-byte v3, v1, v2

    #@9
    .line 1579
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@c
    move-result-object v2

    #@d
    invoke-interface {v2, v1}, Lcom/android/internal/telephony/ITelephony;->uknightEventSet([B)[B

    #@10
    move-result-object v2

    #@11
    .line 1589
    .end local v1           #unset:[B
    :goto_11
    return-object v2

    #@12
    .line 1582
    :cond_12
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@15
    move-result-object v2

    #@16
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->uknightEventSet([B)[B
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_19} :catch_1b
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_19} :catch_21

    #@19
    move-result-object v2

    #@1a
    goto :goto_11

    #@1b
    .line 1584
    :catch_1b
    move-exception v0

    #@1c
    .line 1585
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@1f
    .line 1589
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_1f
    const/4 v2, 0x0

    #@20
    goto :goto_11

    #@21
    .line 1586
    :catch_21
    move-exception v0

    #@22
    .line 1587
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@25
    goto :goto_1f
.end method

.method public uknight_get_data()[B
    .registers 12

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1618
    const/4 v5, 0x0

    #@2
    .line 1619
    .local v5, logDataTemp:[B
    const/4 v4, 0x0

    #@3
    .line 1621
    .local v4, logDataResult:[B
    const/4 v3, 0x0

    #@4
    .line 1622
    .local v3, knDataResp:Lcom/android/internal/telephony/KNDataResponse;
    const/4 v6, 0x0

    #@5
    .line 1623
    .local v6, totBufNum:I
    const/4 v1, 0x0

    #@6
    .line 1624
    .local v1, curBufNum:I
    const/4 v0, 0x0

    #@7
    .line 1626
    .local v0, cumuDataSize:I
    const-string v8, "TelephonyManager"

    #@9
    const-string/jumbo v9, "uknightGetData()++"

    #@c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1629
    :try_start_f
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@12
    move-result-object v8

    #@13
    invoke-interface {v8, v1}, Lcom/android/internal/telephony/ITelephony;->uknightGetData(I)Lcom/android/internal/telephony/KNDataResponse;

    #@16
    move-result-object v3

    #@17
    .line 1630
    if-nez v3, :cond_21

    #@19
    .line 1631
    const-string v8, "TelephonyManager"

    #@1b
    const-string v9, "Internal Start ERROR : KNDataResponse is NULL"

    #@1d
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1691
    :goto_20
    return-object v7

    #@21
    .line 1635
    :cond_21
    iget v6, v3, Lcom/android/internal/telephony/KNDataResponse;->send_buf_num:I

    #@23
    .line 1636
    if-lez v6, :cond_6c

    #@25
    const/16 v8, 0x1000

    #@27
    if-ge v6, v8, :cond_6c

    #@29
    .line 1637
    mul-int/lit16 v8, v6, 0x7f8

    #@2b
    new-array v5, v8, [B

    #@2d
    .line 1638
    const-string v8, "TelephonyManager"

    #@2f
    new-instance v9, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string/jumbo v10, "totBufNum="

    #@37
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v9

    #@3f
    const-string v10, ", local buffer size = "

    #@41
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v9

    #@45
    array-length v10, v5

    #@46
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v9

    #@4a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v9

    #@4e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1645
    :cond_51
    :goto_51
    if-ge v1, v6, :cond_af

    #@53
    .line 1646
    add-int/lit8 v1, v1, 0x1

    #@55
    .line 1647
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@58
    move-result-object v8

    #@59
    invoke-interface {v8, v1}, Lcom/android/internal/telephony/ITelephony;->uknightGetData(I)Lcom/android/internal/telephony/KNDataResponse;

    #@5c
    move-result-object v3

    #@5d
    .line 1649
    if-nez v3, :cond_8f

    #@5f
    .line 1650
    const-string v8, "TelephonyManager"

    #@61
    const-string v9, "Internal Loop ERROR  : KNDataResponse is NULL"

    #@63
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_66
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_66} :catch_67
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_66} :catch_9d

    #@66
    goto :goto_20

    #@67
    .line 1686
    :catch_67
    move-exception v2

    #@68
    .line 1687
    .local v2, ex:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@6b
    goto :goto_20

    #@6c
    .line 1641
    .end local v2           #ex:Landroid/os/RemoteException;
    :cond_6c
    :try_start_6c
    const-string v8, "TelephonyManager"

    #@6e
    new-instance v9, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string/jumbo v10, "totBufNum="

    #@76
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v9

    #@7a
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v9

    #@7e
    const-string v10, " is Invalid"

    #@80
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v9

    #@84
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v9

    #@88
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 1642
    if-eqz v6, :cond_51

    #@8d
    const/4 v6, 0x0

    #@8e
    goto :goto_51

    #@8f
    .line 1654
    :cond_8f
    iget v8, v3, Lcom/android/internal/telephony/KNDataResponse;->data_len:I

    #@91
    add-int/2addr v8, v0

    #@92
    array-length v9, v5

    #@93
    if-le v8, v9, :cond_a3

    #@95
    .line 1655
    const-string v8, "TelephonyManager"

    #@97
    const-string v9, "Internal Loop ERROR  : TOO Large data"

    #@99
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9c
    .catch Landroid/os/RemoteException; {:try_start_6c .. :try_end_9c} :catch_67
    .catch Ljava/lang/NullPointerException; {:try_start_6c .. :try_end_9c} :catch_9d

    #@9c
    goto :goto_20

    #@9d
    .line 1688
    :catch_9d
    move-exception v2

    #@9e
    .line 1689
    .local v2, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@a1
    goto/16 :goto_20

    #@a3
    .line 1659
    .end local v2           #ex:Ljava/lang/NullPointerException;
    :cond_a3
    :try_start_a3
    iget-object v8, v3, Lcom/android/internal/telephony/KNDataResponse;->data:[B

    #@a5
    const/4 v9, 0x0

    #@a6
    iget v10, v3, Lcom/android/internal/telephony/KNDataResponse;->data_len:I

    #@a8
    invoke-static {v8, v9, v5, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@ab
    .line 1660
    iget v8, v3, Lcom/android/internal/telephony/KNDataResponse;->data_len:I

    #@ad
    add-int/2addr v0, v8

    #@ae
    goto :goto_51

    #@af
    .line 1665
    :cond_af
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@b2
    move-result-object v8

    #@b3
    const v9, 0xffff

    #@b6
    invoke-interface {v8, v9}, Lcom/android/internal/telephony/ITelephony;->uknightGetData(I)Lcom/android/internal/telephony/KNDataResponse;

    #@b9
    move-result-object v3

    #@ba
    .line 1666
    if-nez v3, :cond_c5

    #@bc
    .line 1667
    const-string v8, "TelephonyManager"

    #@be
    const-string v9, "Internal Start ERROR : KNDataResponse is NULL"

    #@c0
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    goto/16 :goto_20

    #@c5
    .line 1671
    :cond_c5
    if-nez v0, :cond_e2

    #@c7
    .line 1672
    const-string v8, "TelephonyManager"

    #@c9
    new-instance v9, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string/jumbo v10, "uknightGetData()-- cumuDataSize="

    #@d1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v9

    #@d5
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v9

    #@d9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v9

    #@dd
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    goto/16 :goto_20

    #@e2
    .line 1676
    :cond_e2
    array-length v8, v5

    #@e3
    if-ge v0, v8, :cond_109

    #@e5
    .line 1677
    new-array v4, v0, [B

    #@e7
    .line 1678
    const/4 v8, 0x0

    #@e8
    const/4 v9, 0x0

    #@e9
    invoke-static {v5, v8, v4, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@ec
    .line 1679
    const-string v8, "TelephonyManager"

    #@ee
    new-instance v9, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string/jumbo v10, "uknightGetData()-- logDataResult.length="

    #@f6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v9

    #@fa
    array-length v10, v4

    #@fb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v9

    #@ff
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v9

    #@103
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@106
    move-object v7, v4

    #@107
    .line 1680
    goto/16 :goto_20

    #@109
    .line 1683
    :cond_109
    const-string v8, "TelephonyManager"

    #@10b
    new-instance v9, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string/jumbo v10, "uknightGetData()-- logData.length="

    #@113
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v9

    #@117
    array-length v10, v5

    #@118
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v9

    #@11c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v9

    #@120
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_123
    .catch Landroid/os/RemoteException; {:try_start_a3 .. :try_end_123} :catch_67
    .catch Ljava/lang/NullPointerException; {:try_start_a3 .. :try_end_123} :catch_9d

    #@123
    move-object v7, v5

    #@124
    .line 1684
    goto/16 :goto_20
.end method

.method public uknight_log_set([B)[B
    .registers 7
    .parameter "mask"

    #@0
    .prologue
    .line 1556
    if-nez p1, :cond_12

    #@2
    .line 1557
    const/4 v2, 0x1

    #@3
    :try_start_3
    new-array v1, v2, [B

    #@5
    .line 1558
    .local v1, unset:[B
    const/4 v2, 0x0

    #@6
    const/4 v3, -0x1

    #@7
    aput-byte v3, v1, v2

    #@9
    .line 1559
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@c
    move-result-object v2

    #@d
    invoke-interface {v2, v1}, Lcom/android/internal/telephony/ITelephony;->uknightLogSet([B)[B

    #@10
    move-result-object v2

    #@11
    .line 1570
    .end local v1           #unset:[B
    :goto_11
    return-object v2

    #@12
    .line 1562
    :cond_12
    const-string v2, "TelephonyManager"

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string/jumbo v4, "uknight_log_set:: mask.length="

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    array-length v4, p1

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1563
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@2f
    move-result-object v2

    #@30
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->uknightLogSet([B)[B
    :try_end_33
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_33} :catch_35
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_33} :catch_3b

    #@33
    move-result-object v2

    #@34
    goto :goto_11

    #@35
    .line 1565
    :catch_35
    move-exception v0

    #@36
    .line 1566
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@39
    .line 1570
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_39
    const/4 v2, 0x0

    #@3a
    goto :goto_11

    #@3b
    .line 1567
    :catch_3b
    move-exception v0

    #@3c
    .line 1568
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@3f
    goto :goto_39
.end method

.method public uknight_mem_check()[I
    .registers 3

    #@0
    .prologue
    .line 1697
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->uknightMemCheck()[I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_f

    #@7
    move-result-object v1

    #@8
    .line 1703
    :goto_8
    return-object v1

    #@9
    .line 1698
    :catch_9
    move-exception v0

    #@a
    .line 1699
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@d
    .line 1703
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_d
    const/4 v1, 0x0

    #@e
    goto :goto_8

    #@f
    .line 1700
    :catch_f
    move-exception v0

    #@10
    .line 1701
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@13
    goto :goto_d
.end method

.method public uknight_mem_set(I)Z
    .registers 4
    .parameter "persent"

    #@0
    .prologue
    .line 1607
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->uknightMemSet(I)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_f

    #@7
    move-result v1

    #@8
    .line 1613
    :goto_8
    return v1

    #@9
    .line 1608
    :catch_9
    move-exception v0

    #@a
    .line 1609
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@d
    .line 1613
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_d
    const/4 v1, 0x0

    #@e
    goto :goto_8

    #@f
    .line 1610
    :catch_f
    move-exception v0

    #@10
    .line 1611
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@13
    goto :goto_d
.end method

.method public uknight_state_change_set(I)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1595
    :try_start_0
    invoke-direct {p0}, Landroid/telephony/TelephonyManager;->getITelephony()Lcom/android/internal/telephony/ITelephony;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/ITelephony;->uknightStateChangeSet(I)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_f

    #@7
    move-result v1

    #@8
    .line 1601
    :goto_8
    return v1

    #@9
    .line 1596
    :catch_9
    move-exception v0

    #@a
    .line 1597
    .local v0, ex:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@d
    .line 1601
    .end local v0           #ex:Landroid/os/RemoteException;
    :goto_d
    const/4 v1, 0x0

    #@e
    goto :goto_8

    #@f
    .line 1598
    :catch_f
    move-exception v0

    #@10
    .line 1599
    .local v0, ex:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@13
    goto :goto_d
.end method
