.class Landroid/telephony/LGKoreanPhoneNumberFormatter;
.super Ljava/lang/Object;
.source "LGKoreanPhoneNumberFormatter.java"


# static fields
.field private static DBG:Z

.field private static DDD_3_Number:[S

.field private static DDD_5_Number:[S

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 29
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@3
    .line 30
    const-string v0, "LGKoreanPhoneNumberFormatter"

    #@5
    sput-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@7
    .line 32
    const/16 v0, 0x25

    #@9
    new-array v0, v0, [S

    #@b
    fill-array-data v0, :array_1a

    #@e
    sput-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_3_Number:[S

    #@10
    .line 39
    const/16 v0, 0x11

    #@12
    new-array v0, v0, [S

    #@14
    fill-array-data v0, :array_44

    #@17
    sput-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_5_Number:[S

    #@19
    return-void

    #@1a
    .line 32
    :array_1a
    .array-data 0x2
        0x1t 0x0t
        0x2t 0x0t
        0x3t 0x0t
        0x4t 0x0t
        0x5t 0x0t
        0x6t 0x0t
        0x7t 0x0t
        0x8t 0x0t
        0x9t 0x0t
        0xat 0x0t
        0xbt 0x0t
        0xct 0x0t
        0xdt 0x0t
        0xft 0x0t
        0x10t 0x0t
        0x11t 0x0t
        0x12t 0x0t
        0x13t 0x0t
        0x1ft 0x0t
        0x20t 0x0t
        0x21t 0x0t
        0x29t 0x0t
        0x2at 0x0t
        0x2bt 0x0t
        0x2ct 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x3ct 0x0t
        0x3dt 0x0t
        0x3et 0x0t
        0x3ft 0x0t
        0x40t 0x0t
        0x46t 0x0t
        0x50t 0x0t
    .end array-data

    #@43
    .line 39
    nop

    #@44
    :array_44
    .array-data 0x2
        0x2ct 0x1t
        0x41t 0x1t
        0x55t 0x1t
        0x58t 0x1t
        0x59t 0x1t
        0x6dt 0x1t
        0xbct 0x2t
        0xd7t 0x2t
        0xf3t 0x2t
        0xct 0x3t
        0xfet 0x2t
        0x13t 0x3t
        0x84t 0x1t
        0x2t 0x3t
        0x9t 0x3t
        0xf1t 0x2t
        0xc3t 0x2t
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static format(Landroid/text/Editable;)V
    .registers 15
    .parameter "oritext"

    #@0
    .prologue
    .line 47
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@7
    move-result-object v3

    #@8
    .line 48
    .local v3, text:Landroid/text/Editable;
    invoke-static {p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@b
    move-result v0

    #@c
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@f
    move-result v1

    #@10
    invoke-static {v3, v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@13
    .line 49
    invoke-interface {p0}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    #@16
    move-result-object v0

    #@17
    invoke-interface {v3, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    #@1a
    .line 52
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@1c
    if-eqz v0, :cond_36

    #@1e
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "format input = "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 55
    :cond_36
    const/4 v11, 0x0

    #@37
    .line 56
    .local v11, tmp1stHyphen:I
    const/4 v12, 0x0

    #@38
    .line 58
    .local v12, tmp2ndHyphen:I
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@3b
    move-result v9

    #@3c
    .line 60
    .local v9, length:I
    const/4 v10, 0x0

    #@3d
    .line 61
    .local v10, p:I
    :goto_3d
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@40
    move-result v0

    #@41
    if-ge v10, v0, :cond_54

    #@43
    .line 62
    invoke-interface {v3, v10}, Landroid/text/Editable;->charAt(I)C

    #@46
    move-result v0

    #@47
    const/16 v1, 0x2d

    #@49
    if-ne v0, v1, :cond_51

    #@4b
    .line 63
    add-int/lit8 v0, v10, 0x1

    #@4d
    invoke-interface {v3, v10, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@50
    goto :goto_3d

    #@51
    .line 65
    :cond_51
    add-int/lit8 v10, v10, 0x1

    #@53
    goto :goto_3d

    #@54
    .line 69
    :cond_54
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@57
    move-result v9

    #@58
    .line 72
    const/4 v6, 0x0

    #@59
    .local v6, i:I
    :goto_59
    if-ge v6, v9, :cond_a8

    #@5b
    .line 73
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@5e
    move-result v0

    #@5f
    const/16 v1, 0x30

    #@61
    if-lt v0, v1, :cond_6b

    #@63
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@66
    move-result v0

    #@67
    const/16 v1, 0x39

    #@69
    if-le v0, v1, :cond_a5

    #@6b
    :cond_6b
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@6e
    move-result v0

    #@6f
    const/16 v1, 0x2a

    #@71
    if-eq v0, v1, :cond_a5

    #@73
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@76
    move-result v0

    #@77
    const/16 v1, 0x23

    #@79
    if-eq v0, v1, :cond_a5

    #@7b
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@7e
    move-result v0

    #@7f
    const/16 v1, 0x57

    #@81
    if-eq v0, v1, :cond_a5

    #@83
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@86
    move-result v0

    #@87
    const/16 v1, 0x50

    #@89
    if-eq v0, v1, :cond_a5

    #@8b
    .line 77
    const/4 v1, 0x0

    #@8c
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@8f
    move-result v2

    #@90
    const/4 v4, 0x0

    #@91
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@94
    move-result v5

    #@95
    move-object v0, p0

    #@96
    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    #@99
    .line 78
    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@9c
    move-result v0

    #@9d
    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@a0
    move-result v1

    #@a1
    invoke-static {p0, v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@a4
    .line 238
    :cond_a4
    :goto_a4
    return-void

    #@a5
    .line 72
    :cond_a5
    add-int/lit8 v6, v6, 0x1

    #@a7
    goto :goto_59

    #@a8
    .line 93
    :cond_a8
    const/4 v0, 0x2

    #@a9
    if-lt v9, v0, :cond_a4

    #@ab
    .line 96
    const-string v0, "*"

    #@ad
    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v0

    #@b1
    if-nez v0, :cond_a4

    #@b3
    const-string v0, "#"

    #@b5
    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b8
    move-result v0

    #@b9
    if-nez v0, :cond_a4

    #@bb
    .line 99
    const/4 v0, 0x0

    #@bc
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@bf
    move-result v0

    #@c0
    const/16 v1, 0x30

    #@c2
    if-ne v0, v1, :cond_ce

    #@c4
    const/4 v0, 0x1

    #@c5
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@c8
    move-result v0

    #@c9
    const/16 v1, 0x32

    #@cb
    if-ne v0, v1, :cond_ce

    #@cd
    .line 100
    const/4 v11, 0x2

    #@ce
    .line 103
    :cond_ce
    const/4 v0, 0x3

    #@cf
    if-lt v9, v0, :cond_ed

    #@d1
    .line 104
    const/4 v0, 0x0

    #@d2
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@d5
    move-result v0

    #@d6
    const/16 v1, 0x30

    #@d8
    if-ne v0, v1, :cond_ed

    #@da
    const/4 v0, 0x1

    #@db
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@de
    move-result v0

    #@df
    const/16 v1, 0x32

    #@e1
    if-ne v0, v1, :cond_ed

    #@e3
    const/4 v0, 0x2

    #@e4
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@e7
    move-result v0

    #@e8
    const/16 v1, 0x30

    #@ea
    if-ne v0, v1, :cond_ed

    #@ec
    .line 105
    const/4 v11, 0x3

    #@ed
    .line 110
    :cond_ed
    const/4 v0, 0x4

    #@ee
    if-lt v9, v0, :cond_128

    #@f0
    .line 112
    const-string v0, "KR"

    #@f2
    const-string v1, "KT"

    #@f4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@f7
    move-result v0

    #@f8
    if-eqz v0, :cond_20c

    #@fa
    .line 113
    const/4 v0, 0x4

    #@fb
    if-ne v9, v0, :cond_1e0

    #@fd
    .line 114
    const/4 v0, 0x0

    #@fe
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@101
    move-result v0

    #@102
    add-int/lit8 v0, v0, -0x30

    #@104
    mul-int/lit16 v0, v0, 0x3e8

    #@106
    const/4 v1, 0x1

    #@107
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@10a
    move-result v1

    #@10b
    add-int/lit8 v1, v1, -0x30

    #@10d
    mul-int/lit8 v1, v1, 0x64

    #@10f
    add-int/2addr v0, v1

    #@110
    const/4 v1, 0x2

    #@111
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@114
    move-result v1

    #@115
    add-int/lit8 v1, v1, -0x30

    #@117
    mul-int/lit8 v1, v1, 0xa

    #@119
    add-int/2addr v0, v1

    #@11a
    const/4 v1, 0x3

    #@11b
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@11e
    move-result v1

    #@11f
    add-int/lit8 v1, v1, -0x30

    #@121
    add-int v13, v0, v1

    #@123
    .line 116
    .local v13, tmpDDDNumber:I
    const/16 v0, 0x7d4

    #@125
    if-ne v13, v0, :cond_128

    #@127
    .line 117
    const/4 v11, 0x0

    #@128
    .line 138
    .end local v13           #tmpDDDNumber:I
    :cond_128
    const/4 v0, 0x4

    #@129
    if-ne v9, v0, :cond_15e

    #@12b
    .line 139
    const/4 v0, 0x0

    #@12c
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@12f
    move-result v0

    #@130
    add-int/lit8 v0, v0, -0x30

    #@132
    mul-int/lit16 v0, v0, 0x3e8

    #@134
    const/4 v1, 0x1

    #@135
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@138
    move-result v1

    #@139
    add-int/lit8 v1, v1, -0x30

    #@13b
    mul-int/lit8 v1, v1, 0x64

    #@13d
    add-int/2addr v0, v1

    #@13e
    const/4 v1, 0x2

    #@13f
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@142
    move-result v1

    #@143
    add-int/lit8 v1, v1, -0x30

    #@145
    mul-int/lit8 v1, v1, 0xa

    #@147
    add-int/2addr v0, v1

    #@148
    const/4 v1, 0x3

    #@149
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@14c
    move-result v1

    #@14d
    add-int/lit8 v1, v1, -0x30

    #@14f
    add-int v13, v0, v1

    #@151
    .line 140
    .restart local v13       #tmpDDDNumber:I
    const/16 v0, 0x1f4

    #@153
    if-lt v13, v0, :cond_159

    #@155
    const/16 v0, 0x1fd

    #@157
    if-le v13, v0, :cond_15d

    #@159
    :cond_159
    const/16 v0, 0x82

    #@15b
    if-ne v13, v0, :cond_15e

    #@15d
    .line 141
    :cond_15d
    const/4 v11, 0x0

    #@15e
    .line 144
    .end local v13           #tmpDDDNumber:I
    :cond_15e
    const/4 v0, 0x4

    #@15f
    if-le v9, v0, :cond_194

    #@161
    .line 145
    const/4 v0, 0x0

    #@162
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@165
    move-result v0

    #@166
    add-int/lit8 v0, v0, -0x30

    #@168
    mul-int/lit16 v0, v0, 0x3e8

    #@16a
    const/4 v1, 0x1

    #@16b
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@16e
    move-result v1

    #@16f
    add-int/lit8 v1, v1, -0x30

    #@171
    mul-int/lit8 v1, v1, 0x64

    #@173
    add-int/2addr v0, v1

    #@174
    const/4 v1, 0x2

    #@175
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@178
    move-result v1

    #@179
    add-int/lit8 v1, v1, -0x30

    #@17b
    mul-int/lit8 v1, v1, 0xa

    #@17d
    add-int/2addr v0, v1

    #@17e
    const/4 v1, 0x3

    #@17f
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@182
    move-result v1

    #@183
    add-int/lit8 v1, v1, -0x30

    #@185
    add-int v13, v0, v1

    #@187
    .line 146
    .restart local v13       #tmpDDDNumber:I
    const/16 v0, 0x1f4

    #@189
    if-lt v13, v0, :cond_18f

    #@18b
    const/16 v0, 0x1fd

    #@18d
    if-le v13, v0, :cond_193

    #@18f
    :cond_18f
    const/16 v0, 0x82

    #@191
    if-ne v13, v0, :cond_194

    #@193
    .line 147
    :cond_193
    const/4 v11, 0x4

    #@194
    .line 150
    .end local v13           #tmpDDDNumber:I
    :cond_194
    const/4 v0, 0x6

    #@195
    if-lt v9, v0, :cond_238

    #@197
    const/4 v0, 0x1

    #@198
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@19b
    move-result v0

    #@19c
    const/16 v1, 0x30

    #@19e
    if-ne v0, v1, :cond_238

    #@1a0
    .line 151
    const/4 v0, 0x0

    #@1a1
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@1a4
    move-result v0

    #@1a5
    add-int/lit8 v0, v0, -0x30

    #@1a7
    mul-int/lit16 v0, v0, 0x2710

    #@1a9
    const/4 v1, 0x1

    #@1aa
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@1ad
    move-result v1

    #@1ae
    add-int/lit8 v1, v1, -0x30

    #@1b0
    mul-int/lit16 v1, v1, 0x3e8

    #@1b2
    add-int/2addr v0, v1

    #@1b3
    const/4 v1, 0x2

    #@1b4
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@1b7
    move-result v1

    #@1b8
    add-int/lit8 v1, v1, -0x30

    #@1ba
    mul-int/lit8 v1, v1, 0x64

    #@1bc
    add-int/2addr v0, v1

    #@1bd
    const/4 v1, 0x3

    #@1be
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@1c1
    move-result v1

    #@1c2
    add-int/lit8 v1, v1, -0x30

    #@1c4
    mul-int/lit8 v1, v1, 0xa

    #@1c6
    add-int/2addr v0, v1

    #@1c7
    const/4 v1, 0x4

    #@1c8
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@1cb
    move-result v1

    #@1cc
    add-int/lit8 v1, v1, -0x30

    #@1ce
    add-int v13, v0, v1

    #@1d0
    .line 152
    .restart local v13       #tmpDDDNumber:I
    const/4 v6, 0x0

    #@1d1
    :goto_1d1
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_5_Number:[S

    #@1d3
    array-length v0, v0

    #@1d4
    if-ge v6, v0, :cond_238

    #@1d6
    .line 153
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_5_Number:[S

    #@1d8
    aget-short v0, v0, v6

    #@1da
    if-ne v13, v0, :cond_1dd

    #@1dc
    .line 154
    const/4 v11, 0x5

    #@1dd
    .line 152
    :cond_1dd
    add-int/lit8 v6, v6, 0x1

    #@1df
    goto :goto_1d1

    #@1e0
    .line 121
    .end local v13           #tmpDDDNumber:I
    :cond_1e0
    const/4 v0, 0x0

    #@1e1
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@1e4
    move-result v0

    #@1e5
    add-int/lit8 v0, v0, -0x30

    #@1e7
    mul-int/lit8 v0, v0, 0x64

    #@1e9
    const/4 v1, 0x1

    #@1ea
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@1ed
    move-result v1

    #@1ee
    add-int/lit8 v1, v1, -0x30

    #@1f0
    mul-int/lit8 v1, v1, 0xa

    #@1f2
    add-int/2addr v0, v1

    #@1f3
    const/4 v1, 0x2

    #@1f4
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@1f7
    move-result v1

    #@1f8
    add-int/lit8 v1, v1, -0x30

    #@1fa
    add-int v13, v0, v1

    #@1fc
    .line 122
    .restart local v13       #tmpDDDNumber:I
    const/4 v6, 0x0

    #@1fd
    :goto_1fd
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_3_Number:[S

    #@1ff
    array-length v0, v0

    #@200
    if-ge v6, v0, :cond_128

    #@202
    .line 123
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_3_Number:[S

    #@204
    aget-short v0, v0, v6

    #@206
    if-ne v13, v0, :cond_209

    #@208
    .line 124
    const/4 v11, 0x3

    #@209
    .line 122
    :cond_209
    add-int/lit8 v6, v6, 0x1

    #@20b
    goto :goto_1fd

    #@20c
    .line 129
    .end local v13           #tmpDDDNumber:I
    :cond_20c
    const/4 v0, 0x0

    #@20d
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@210
    move-result v0

    #@211
    add-int/lit8 v0, v0, -0x30

    #@213
    mul-int/lit8 v0, v0, 0x64

    #@215
    const/4 v1, 0x1

    #@216
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@219
    move-result v1

    #@21a
    add-int/lit8 v1, v1, -0x30

    #@21c
    mul-int/lit8 v1, v1, 0xa

    #@21e
    add-int/2addr v0, v1

    #@21f
    const/4 v1, 0x2

    #@220
    invoke-interface {v3, v1}, Landroid/text/Editable;->charAt(I)C

    #@223
    move-result v1

    #@224
    add-int/lit8 v1, v1, -0x30

    #@226
    add-int v13, v0, v1

    #@228
    .line 130
    .restart local v13       #tmpDDDNumber:I
    const/4 v6, 0x0

    #@229
    :goto_229
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_3_Number:[S

    #@22b
    array-length v0, v0

    #@22c
    if-ge v6, v0, :cond_128

    #@22e
    .line 131
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DDD_3_Number:[S

    #@230
    aget-short v0, v0, v6

    #@232
    if-ne v13, v0, :cond_235

    #@234
    .line 132
    const/4 v11, 0x3

    #@235
    .line 130
    :cond_235
    add-int/lit8 v6, v6, 0x1

    #@237
    goto :goto_229

    #@238
    .line 158
    .end local v13           #tmpDDDNumber:I
    :cond_238
    const/4 v7, 0x0

    #@239
    .line 160
    .local v7, j:I
    if-lez v11, :cond_35e

    #@23b
    .line 161
    const/4 v6, 0x0

    #@23c
    const/4 v7, 0x0

    #@23d
    :goto_23d
    if-ge v6, v9, :cond_250

    #@23f
    .line 162
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@242
    move-result v0

    #@243
    const/16 v1, 0x50

    #@245
    if-eq v0, v1, :cond_24f

    #@247
    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    #@24a
    move-result v0

    #@24b
    const/16 v1, 0x57

    #@24d
    if-ne v0, v1, :cond_272

    #@24f
    .line 163
    :cond_24f
    move v7, v6

    #@250
    .line 167
    :cond_250
    if-eqz v7, :cond_275

    #@252
    .line 168
    move v12, v7

    #@253
    .line 214
    :cond_253
    :goto_253
    const/4 v8, 0x0

    #@254
    .local v8, k:I
    :goto_254
    if-ge v8, v9, :cond_3b0

    #@256
    .line 215
    if-ne v8, v11, :cond_25f

    #@258
    if-eqz v8, :cond_25f

    #@25a
    .line 216
    const-string v0, "-"

    #@25c
    invoke-interface {v3, v8, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    #@25f
    .line 218
    :cond_25f
    const/4 v0, 0x4

    #@260
    if-le v12, v0, :cond_3a5

    #@262
    .line 219
    add-int/lit8 v0, v8, 0x1

    #@264
    if-ne v0, v12, :cond_26f

    #@266
    if-eqz v8, :cond_26f

    #@268
    .line 220
    add-int/lit8 v0, v8, 0x1

    #@26a
    const-string v1, "-"

    #@26c
    invoke-interface {v3, v0, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    #@26f
    .line 214
    :cond_26f
    :goto_26f
    add-int/lit8 v8, v8, 0x1

    #@271
    goto :goto_254

    #@272
    .line 161
    .end local v8           #k:I
    :cond_272
    add-int/lit8 v6, v6, 0x1

    #@274
    goto :goto_23d

    #@275
    .line 171
    :cond_275
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@277
    if-eqz v0, :cond_292

    #@279
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@27b
    new-instance v1, Ljava/lang/StringBuilder;

    #@27d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@280
    const-string/jumbo v2, "tmp1stHyphen = "

    #@283
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@286
    move-result-object v1

    #@287
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28a
    move-result-object v1

    #@28b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28e
    move-result-object v1

    #@28f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@292
    .line 174
    :cond_292
    const/4 v0, 0x0

    #@293
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@296
    move-result v0

    #@297
    const/16 v1, 0x30

    #@299
    if-ne v0, v1, :cond_2cc

    #@29b
    const/4 v0, 0x1

    #@29c
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@29f
    move-result v0

    #@2a0
    const/16 v1, 0x31

    #@2a2
    if-ne v0, v1, :cond_2cc

    #@2a4
    const/4 v0, 0x2

    #@2a5
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@2a8
    move-result v0

    #@2a9
    const/16 v1, 0x30

    #@2ab
    if-ne v0, v1, :cond_2cc

    #@2ad
    .line 175
    add-int/lit8 v12, v11, 0x5

    #@2af
    .line 176
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@2b1
    if-eqz v0, :cond_253

    #@2b3
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@2b5
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2ba
    const-string v2, "case 1 tmp2ndHyphen = "

    #@2bc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bf
    move-result-object v1

    #@2c0
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c3
    move-result-object v1

    #@2c4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c7
    move-result-object v1

    #@2c8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2cb
    goto :goto_253

    #@2cc
    .line 178
    :cond_2cc
    add-int/lit8 v0, v9, -0x1

    #@2ce
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@2d1
    move-result v0

    #@2d2
    const/16 v1, 0x23

    #@2d4
    if-ne v0, v1, :cond_31a

    #@2d6
    .line 179
    add-int/lit8 v0, v11, 0x9

    #@2d8
    if-lt v9, v0, :cond_2fa

    #@2da
    .line 180
    add-int/lit8 v12, v11, 0x5

    #@2dc
    .line 181
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@2de
    if-eqz v0, :cond_253

    #@2e0
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@2e2
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e7
    const-string v2, "case 3 tmp2ndHyphen = "

    #@2e9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ec
    move-result-object v1

    #@2ed
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v1

    #@2f1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f4
    move-result-object v1

    #@2f5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f8
    goto/16 :goto_253

    #@2fa
    .line 183
    :cond_2fa
    add-int/lit8 v12, v11, 0x4

    #@2fc
    .line 184
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@2fe
    if-eqz v0, :cond_253

    #@300
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@302
    new-instance v1, Ljava/lang/StringBuilder;

    #@304
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@307
    const-string v2, "case 4 tmp2ndHyphen = "

    #@309
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30c
    move-result-object v1

    #@30d
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@310
    move-result-object v1

    #@311
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@314
    move-result-object v1

    #@315
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@318
    goto/16 :goto_253

    #@31a
    .line 187
    :cond_31a
    add-int/lit8 v0, v11, 0x8

    #@31c
    if-lt v9, v0, :cond_33e

    #@31e
    .line 188
    add-int/lit8 v12, v11, 0x5

    #@320
    .line 189
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@322
    if-eqz v0, :cond_253

    #@324
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@326
    new-instance v1, Ljava/lang/StringBuilder;

    #@328
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32b
    const-string v2, "case 5 tmp2ndHyphen = "

    #@32d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@330
    move-result-object v1

    #@331
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@334
    move-result-object v1

    #@335
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@338
    move-result-object v1

    #@339
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33c
    goto/16 :goto_253

    #@33e
    .line 191
    :cond_33e
    add-int/lit8 v12, v11, 0x4

    #@340
    .line 192
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@342
    if-eqz v0, :cond_253

    #@344
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@346
    new-instance v1, Ljava/lang/StringBuilder;

    #@348
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34b
    const-string v2, "case 6 tmp2ndHyphen = "

    #@34d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@350
    move-result-object v1

    #@351
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@354
    move-result-object v1

    #@355
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@358
    move-result-object v1

    #@359
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35c
    goto/16 :goto_253

    #@35e
    .line 198
    :cond_35e
    const/4 v11, 0x0

    #@35f
    .line 199
    const/4 v0, 0x0

    #@360
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@363
    move-result v0

    #@364
    const/16 v1, 0x2a

    #@366
    if-eq v0, v1, :cond_371

    #@368
    const/4 v0, 0x0

    #@369
    invoke-interface {v3, v0}, Landroid/text/Editable;->charAt(I)C

    #@36c
    move-result v0

    #@36d
    const/16 v1, 0x23

    #@36f
    if-ne v0, v1, :cond_374

    #@371
    .line 200
    :cond_371
    const/4 v12, 0x0

    #@372
    goto/16 :goto_253

    #@374
    .line 202
    :cond_374
    const/16 v0, 0x8

    #@376
    if-lt v9, v0, :cond_3a3

    #@378
    .line 203
    const/4 v12, 0x4

    #@379
    .line 207
    :goto_379
    const/4 v0, 0x4

    #@37a
    if-ne v9, v0, :cond_253

    #@37c
    const/4 v0, 0x0

    #@37d
    const/4 v1, 0x3

    #@37e
    invoke-interface {v3, v0, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@381
    move-result-object v0

    #@382
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@385
    move-result-object v0

    #@386
    const-string v1, "050"

    #@388
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38b
    move-result v0

    #@38c
    if-nez v0, :cond_3a0

    #@38e
    const/4 v0, 0x0

    #@38f
    const/4 v1, 0x4

    #@390
    invoke-interface {v3, v0, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@393
    move-result-object v0

    #@394
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@397
    move-result-object v0

    #@398
    const-string v1, "0130"

    #@39a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39d
    move-result v0

    #@39e
    if-eqz v0, :cond_253

    #@3a0
    .line 208
    :cond_3a0
    const/4 v12, 0x0

    #@3a1
    goto/16 :goto_253

    #@3a3
    .line 205
    :cond_3a3
    const/4 v12, 0x3

    #@3a4
    goto :goto_379

    #@3a5
    .line 223
    .restart local v8       #k:I
    :cond_3a5
    if-ne v8, v12, :cond_26f

    #@3a7
    if-eqz v8, :cond_26f

    #@3a9
    .line 224
    const-string v0, "-"

    #@3ab
    invoke-interface {v3, v8, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    #@3ae
    goto/16 :goto_26f

    #@3b0
    .line 229
    :cond_3b0
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@3b2
    if-eqz v0, :cond_3cd

    #@3b4
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@3b6
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3bb
    const-string/jumbo v2, "result tmp1stHyphen = "

    #@3be
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c1
    move-result-object v1

    #@3c2
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c5
    move-result-object v1

    #@3c6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c9
    move-result-object v1

    #@3ca
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3cd
    .line 230
    :cond_3cd
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@3cf
    if-eqz v0, :cond_3ea

    #@3d1
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@3d3
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d8
    const-string/jumbo v2, "result tmp2ndHyphen = "

    #@3db
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3de
    move-result-object v1

    #@3df
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e2
    move-result-object v1

    #@3e3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e6
    move-result-object v1

    #@3e7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3ea
    .line 232
    :cond_3ea
    sget-boolean v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->DBG:Z

    #@3ec
    if-eqz v0, :cond_406

    #@3ee
    sget-object v0, Landroid/telephony/LGKoreanPhoneNumberFormatter;->TAG:Ljava/lang/String;

    #@3f0
    new-instance v1, Ljava/lang/StringBuilder;

    #@3f2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f5
    const-string v2, "format output = "

    #@3f7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fa
    move-result-object v1

    #@3fb
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3fe
    move-result-object v1

    #@3ff
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@402
    move-result-object v1

    #@403
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@406
    .line 235
    :cond_406
    const/4 v1, 0x0

    #@407
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@40a
    move-result v2

    #@40b
    const/4 v4, 0x0

    #@40c
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@40f
    move-result v5

    #@410
    move-object v0, p0

    #@411
    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    #@414
    .line 236
    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@417
    move-result v0

    #@418
    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@41b
    move-result v1

    #@41c
    invoke-static {p0, v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@41f
    goto/16 :goto_a4
.end method
