.class public final Landroid/telephony/CellInfoGsm;
.super Landroid/telephony/CellInfo;
.source "CellInfoGsm.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellInfoGsm;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "CellInfoGsm"


# instance fields
.field private mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

.field private mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 123
    new-instance v0, Landroid/telephony/CellInfoGsm$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellInfoGsm$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellInfoGsm;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/telephony/CellInfo;-><init>()V

    #@3
    .line 37
    new-instance v0, Landroid/telephony/CellIdentityGsm;

    #@5
    invoke-direct {v0}, Landroid/telephony/CellIdentityGsm;-><init>()V

    #@8
    iput-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@a
    .line 38
    new-instance v0, Landroid/telephony/CellSignalStrengthGsm;

    #@c
    invoke-direct {v0}, Landroid/telephony/CellSignalStrengthGsm;-><init>()V

    #@f
    iput-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@11
    .line 39
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 117
    invoke-direct {p0, p1}, Landroid/telephony/CellInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    .line 118
    sget-object v0, Landroid/telephony/CellIdentityGsm;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/telephony/CellIdentityGsm;

    #@b
    iput-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@d
    .line 119
    sget-object v0, Landroid/telephony/CellSignalStrengthGsm;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/telephony/CellSignalStrengthGsm;

    #@15
    iput-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@17
    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/telephony/CellInfoGsm;)V
    .registers 3
    .parameter "ci"

    #@0
    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/telephony/CellInfo;-><init>(Landroid/telephony/CellInfo;)V

    #@3
    .line 44
    iget-object v0, p1, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@5
    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->copy()Landroid/telephony/CellIdentityGsm;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@b
    .line 45
    iget-object v0, p1, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@d
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthGsm;->copy()Landroid/telephony/CellSignalStrengthGsm;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@13
    .line 46
    return-void
.end method

.method protected static createFromParcelBody(Landroid/os/Parcel;)Landroid/telephony/CellInfoGsm;
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 138
    new-instance v0, Landroid/telephony/CellInfoGsm;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellInfoGsm;-><init>(Landroid/os/Parcel;)V

    #@5
    return-object v0
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 145
    const-string v0, "CellInfoGsm"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 146
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 101
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "other"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 74
    invoke-super {p0, p1}, Landroid/telephony/CellInfo;->equals(Ljava/lang/Object;)Z

    #@4
    move-result v4

    #@5
    if-nez v4, :cond_8

    #@7
    .line 82
    :cond_7
    :goto_7
    return v3

    #@8
    .line 78
    :cond_8
    :try_start_8
    move-object v0, p1

    #@9
    check-cast v0, Landroid/telephony/CellInfoGsm;

    #@b
    move-object v2, v0

    #@c
    .line 79
    .local v2, o:Landroid/telephony/CellInfoGsm;
    iget-object v4, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@e
    iget-object v5, v2, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@10
    invoke-virtual {v4, v5}, Landroid/telephony/CellIdentityGsm;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_7

    #@16
    iget-object v4, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@18
    iget-object v5, v2, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@1a
    invoke-virtual {v4, v5}, Landroid/telephony/CellSignalStrengthGsm;->equals(Ljava/lang/Object;)Z
    :try_end_1d
    .catch Ljava/lang/ClassCastException; {:try_start_8 .. :try_end_1d} :catch_22

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_7

    #@20
    const/4 v3, 0x1

    #@21
    goto :goto_7

    #@22
    .line 81
    .end local v2           #o:Landroid/telephony/CellInfoGsm;
    :catch_22
    move-exception v1

    #@23
    .line 82
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_7
.end method

.method public getCellIdentity()Landroid/telephony/CellIdentityGsm;
    .registers 2

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@2
    return-object v0
.end method

.method public getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;
    .registers 2

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 69
    invoke-super {p0}, Landroid/telephony/CellInfo;->hashCode()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@6
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->hashCode()I

    #@9
    move-result v1

    #@a
    add-int/2addr v0, v1

    #@b
    iget-object v1, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@d
    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthGsm;->hashCode()I

    #@10
    move-result v1

    #@11
    add-int/2addr v0, v1

    #@12
    return v0
.end method

.method public setCellIdentity(Landroid/telephony/CellIdentityGsm;)V
    .registers 2
    .parameter "cid"

    #@0
    .prologue
    .line 53
    iput-object p1, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@2
    .line 54
    return-void
.end method

.method public setCellSignalStrength(Landroid/telephony/CellSignalStrengthGsm;)V
    .registers 2
    .parameter "css"

    #@0
    .prologue
    .line 61
    iput-object p1, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@2
    .line 62
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 90
    .local v0, sb:Ljava/lang/StringBuffer;
    const-string v1, "CellInfoGsm:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a
    .line 91
    invoke-super {p0}, Landroid/telephony/CellInfo;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@11
    .line 92
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@16
    move-result-object v1

    #@17
    iget-object v2, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@1c
    .line 93
    const-string v1, ", "

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@27
    .line 95
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 107
    const/4 v0, 0x1

    #@1
    invoke-super {p0, p1, p2, v0}, Landroid/telephony/CellInfo;->writeToParcel(Landroid/os/Parcel;II)V

    #@4
    .line 108
    iget-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellIdentityGsm:Landroid/telephony/CellIdentityGsm;

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/telephony/CellIdentityGsm;->writeToParcel(Landroid/os/Parcel;I)V

    #@9
    .line 109
    iget-object v0, p0, Landroid/telephony/CellInfoGsm;->mCellSignalStrengthGsm:Landroid/telephony/CellSignalStrengthGsm;

    #@b
    invoke-virtual {v0, p1, p2}, Landroid/telephony/CellSignalStrengthGsm;->writeToParcel(Landroid/os/Parcel;I)V

    #@e
    .line 110
    return-void
.end method
