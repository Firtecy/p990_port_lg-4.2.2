.class public final Landroid/telephony/CellSignalStrengthGsm;
.super Landroid/telephony/CellSignalStrength;
.source "CellSignalStrengthGsm.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/telephony/CellSignalStrengthGsm;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final GSM_SIGNAL_STRENGTH_GOOD:I = 0x8

.field private static final GSM_SIGNAL_STRENGTH_GREAT:I = 0xc

.field private static final GSM_SIGNAL_STRENGTH_MODERATE:I = 0x8

.field private static final LOG_TAG:Ljava/lang/String; = "CellSignalStrengthGsm"


# instance fields
.field private mBitErrorRate:I

.field private mSignalStrength:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 215
    new-instance v0, Landroid/telephony/CellSignalStrengthGsm$1;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellSignalStrengthGsm$1;-><init>()V

    #@5
    sput-object v0, Landroid/telephony/CellSignalStrengthGsm;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 43
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 44
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthGsm;->setDefaultValues()V

    #@6
    .line 45
    return-void
.end method

.method public constructor <init>(II)V
    .registers 3
    .parameter "ss"
    .parameter "ber"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 53
    invoke-virtual {p0, p1, p2}, Landroid/telephony/CellSignalStrengthGsm;->initialize(II)V

    #@6
    .line 54
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 201
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@9
    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@f
    .line 205
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/telephony/CellSignalStrengthGsm$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/telephony/CellSignalStrengthGsm;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/telephony/CellSignalStrengthGsm;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/telephony/CellSignalStrength;-><init>()V

    #@3
    .line 64
    invoke-virtual {p0, p1}, Landroid/telephony/CellSignalStrengthGsm;->copyFrom(Landroid/telephony/CellSignalStrengthGsm;)V

    #@6
    .line 65
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 232
    const-string v0, "CellSignalStrengthGsm"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 233
    return-void
.end method


# virtual methods
.method public bridge synthetic copy()Landroid/telephony/CellSignalStrength;
    .registers 2

    #@0
    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/telephony/CellSignalStrengthGsm;->copy()Landroid/telephony/CellSignalStrengthGsm;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public copy()Landroid/telephony/CellSignalStrengthGsm;
    .registers 2

    #@0
    .prologue
    .line 92
    new-instance v0, Landroid/telephony/CellSignalStrengthGsm;

    #@2
    invoke-direct {v0, p0}, Landroid/telephony/CellSignalStrengthGsm;-><init>(Landroid/telephony/CellSignalStrengthGsm;)V

    #@5
    return-object v0
.end method

.method protected copyFrom(Landroid/telephony/CellSignalStrengthGsm;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 83
    iget v0, p1, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@2
    iput v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@4
    .line 84
    iget v0, p1, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@6
    iput v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@8
    .line 85
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 210
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 167
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/CellSignalStrengthGsm;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 172
    .local v2, s:Landroid/telephony/CellSignalStrengthGsm;
    if-nez p1, :cond_a

    #@7
    .line 176
    .end local v2           #s:Landroid/telephony/CellSignalStrengthGsm;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 168
    :catch_8
    move-exception v1

    #@9
    .line 169
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 176
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/CellSignalStrengthGsm;
    :cond_a
    iget v4, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@c
    iget v5, v2, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@e
    if-ne v4, v5, :cond_7

    #@10
    iget v4, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@12
    iget v5, v2, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@14
    if-ne v4, v5, :cond_7

    #@16
    const/4 v3, 0x1

    #@17
    goto :goto_7
.end method

.method public getAsuLevel()I
    .registers 2

    #@0
    .prologue
    .line 151
    iget v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@2
    .line 153
    .local v0, level:I
    return v0
.end method

.method public getDbm()I
    .registers 6

    #@0
    .prologue
    const v3, 0x7fffffff

    #@3
    .line 130
    iget v2, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@5
    .line 131
    .local v2, level:I
    const/16 v4, 0x63

    #@7
    if-ne v2, v4, :cond_11

    #@9
    move v0, v3

    #@a
    .line 132
    .local v0, asu:I
    :goto_a
    if-eq v0, v3, :cond_13

    #@c
    .line 133
    mul-int/lit8 v3, v0, 0x2

    #@e
    add-int/lit8 v1, v3, -0x71

    #@10
    .line 138
    .local v1, dBm:I
    :goto_10
    return v1

    #@11
    .end local v0           #asu:I
    .end local v1           #dBm:I
    :cond_11
    move v0, v2

    #@12
    .line 131
    goto :goto_a

    #@13
    .line 135
    .restart local v0       #asu:I
    :cond_13
    const v1, 0x7fffffff

    #@16
    .restart local v1       #dBm:I
    goto :goto_10
.end method

.method public getLevel()I
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    .line 113
    iget v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@4
    .line 114
    .local v0, asu:I
    const/4 v2, 0x2

    #@5
    if-le v0, v2, :cond_b

    #@7
    const/16 v2, 0x63

    #@9
    if-ne v0, v2, :cond_d

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    .line 120
    .local v1, level:I
    :goto_c
    return v1

    #@d
    .line 115
    .end local v1           #level:I
    :cond_d
    const/16 v2, 0xc

    #@f
    if-lt v0, v2, :cond_13

    #@11
    const/4 v1, 0x4

    #@12
    .restart local v1       #level:I
    goto :goto_c

    #@13
    .line 116
    .end local v1           #level:I
    :cond_13
    if-lt v0, v3, :cond_17

    #@15
    const/4 v1, 0x3

    #@16
    .restart local v1       #level:I
    goto :goto_c

    #@17
    .line 117
    .end local v1           #level:I
    :cond_17
    if-lt v0, v3, :cond_1b

    #@19
    const/4 v1, 0x2

    #@1a
    .restart local v1       #level:I
    goto :goto_c

    #@1b
    .line 118
    .end local v1           #level:I
    :cond_1b
    const/4 v1, 0x1

    #@1c
    .restart local v1       #level:I
    goto :goto_c
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 158
    const/16 v0, 0x1f

    #@2
    .line 159
    .local v0, primeNum:I
    iget v1, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@4
    mul-int/2addr v1, v0

    #@5
    iget v2, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@7
    mul-int/2addr v2, v0

    #@8
    add-int/2addr v1, v2

    #@9
    return v1
.end method

.method public initialize(II)V
    .registers 3
    .parameter "ss"
    .parameter "ber"

    #@0
    .prologue
    .line 75
    iput p1, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@2
    .line 76
    iput p2, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@4
    .line 77
    return-void
.end method

.method public setDefaultValues()V
    .registers 2

    #@0
    .prologue
    const v0, 0x7fffffff

    #@3
    .line 98
    iput v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@5
    .line 99
    iput v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@7
    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "CellSignalStrengthGsm: ss="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " ber="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 193
    iget v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mSignalStrength:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 194
    iget v0, p0, Landroid/telephony/CellSignalStrengthGsm;->mBitErrorRate:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 195
    return-void
.end method
