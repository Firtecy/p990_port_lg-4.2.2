.class public Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;
.super Ljava/lang/Object;
.source "AssistDialPhoneNumberUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/AssistDialPhoneNumberUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SIDRangeType"
.end annotation


# instance fields
.field private countryIndex:I

.field private end:I

.field private index:I

.field private start:I


# direct methods
.method constructor <init>(IIII)V
    .registers 5
    .parameter "index"
    .parameter "countryIndex"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1794
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1795
    iput p1, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->index:I

    #@5
    .line 1796
    iput p2, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->countryIndex:I

    #@7
    .line 1797
    iput p3, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->start:I

    #@9
    .line 1798
    iput p4, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->end:I

    #@b
    .line 1799
    return-void
.end method


# virtual methods
.method public getCountryIndex()I
    .registers 2

    #@0
    .prologue
    .line 1787
    iget v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->countryIndex:I

    #@2
    return v0
.end method

.method public getEnd()I
    .registers 2

    #@0
    .prologue
    .line 1791
    iget v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->end:I

    #@2
    return v0
.end method

.method public getStart()I
    .registers 2

    #@0
    .prologue
    .line 1783
    iget v0, p0, Landroid/telephony/AssistDialPhoneNumberUtils$SIDRangeType;->start:I

    #@2
    return v0
.end method
