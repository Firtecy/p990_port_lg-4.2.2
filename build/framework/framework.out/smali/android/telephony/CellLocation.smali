.class public abstract Landroid/telephony/CellLocation;
.super Ljava/lang/Object;
.source "CellLocation.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getEmpty()Landroid/telephony/CellLocation;
    .registers 1

    #@0
    .prologue
    .line 94
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@7
    move-result v0

    #@8
    packed-switch v0, :pswitch_data_1a

    #@b
    .line 100
    const/4 v0, 0x0

    #@c
    :goto_c
    return-object v0

    #@d
    .line 96
    :pswitch_d
    new-instance v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@f
    invoke-direct {v0}, Landroid/telephony/cdma/CdmaCellLocation;-><init>()V

    #@12
    goto :goto_c

    #@13
    .line 98
    :pswitch_13
    new-instance v0, Landroid/telephony/gsm/GsmCellLocation;

    #@15
    invoke-direct {v0}, Landroid/telephony/gsm/GsmCellLocation;-><init>()V

    #@18
    goto :goto_c

    #@19
    .line 94
    nop

    #@1a
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_13
        :pswitch_d
    .end packed-switch
.end method

.method public static newFromBundle(Landroid/os/Bundle;)Landroid/telephony/CellLocation;
    .registers 2
    .parameter "bundle"

    #@0
    .prologue
    .line 66
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    #@7
    move-result v0

    #@8
    packed-switch v0, :pswitch_data_1a

    #@b
    .line 72
    const/4 v0, 0x0

    #@c
    :goto_c
    return-object v0

    #@d
    .line 68
    :pswitch_d
    new-instance v0, Landroid/telephony/cdma/CdmaCellLocation;

    #@f
    invoke-direct {v0, p0}, Landroid/telephony/cdma/CdmaCellLocation;-><init>(Landroid/os/Bundle;)V

    #@12
    goto :goto_c

    #@13
    .line 70
    :pswitch_13
    new-instance v0, Landroid/telephony/gsm/GsmCellLocation;

    #@15
    invoke-direct {v0, p0}, Landroid/telephony/gsm/GsmCellLocation;-><init>(Landroid/os/Bundle;)V

    #@18
    goto :goto_c

    #@19
    .line 66
    nop

    #@1a
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_13
        :pswitch_d
    .end packed-switch
.end method

.method public static requestLocationUpdate()V
    .registers 2

    #@0
    .prologue
    .line 43
    :try_start_0
    const-string/jumbo v1, "phone"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v1

    #@7
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@a
    move-result-object v0

    #@b
    .line 44
    .local v0, phone:Lcom/android/internal/telephony/ITelephony;
    if-eqz v0, :cond_10

    #@d
    .line 45
    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->updateServiceLocation()V
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_10} :catch_11

    #@10
    .line 50
    :cond_10
    :goto_10
    return-void

    #@11
    .line 47
    :catch_11
    move-exception v1

    #@12
    goto :goto_10
.end method


# virtual methods
.method public abstract fillInNotifierBundle(Landroid/os/Bundle;)V
.end method

.method public abstract isEmpty()Z
.end method
