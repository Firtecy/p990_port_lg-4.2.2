.class public Landroid/telephony/PhoneNumberFormattingTextWatcher;
.super Ljava/lang/Object;
.source "PhoneNumberFormattingTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field LGEPhoneNumberFormattingTextWatcher:Landroid/telephony/LgePhoneNumberFormattingTextWatcher;

.field private mCountryCode:Ljava/lang/String;

.field private mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

.field private mSelfChange:Z

.field private mStopFormatting:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 71
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-direct {p0, v0}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>(Ljava/lang/String;)V

    #@b
    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "countryCode"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mSelfChange:Z

    #@6
    .line 64
    const-string v0, "KR"

    #@8
    iput-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mCountryCode:Ljava/lang/String;

    #@a
    .line 83
    if-nez p1, :cond_12

    #@c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@11
    throw v0

    #@12
    .line 85
    :cond_12
    const/4 v0, 0x0

    #@13
    const-string v1, "LGE_NumberFormat"

    #@15
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_3a

    #@1b
    .line 86
    iput-object p1, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mCountryCode:Ljava/lang/String;

    #@1d
    .line 87
    const-string v0, "KR"

    #@1f
    iget-object v1, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mCountryCode:Ljava/lang/String;

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@24
    move-result v0

    #@25
    if-nez v0, :cond_2f

    #@27
    .line 88
    new-instance v0, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;

    #@29
    invoke-direct {v0}, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;-><init>()V

    #@2c
    iput-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->LGEPhoneNumberFormattingTextWatcher:Landroid/telephony/LgePhoneNumberFormattingTextWatcher;

    #@2e
    .line 100
    :goto_2e
    return-void

    #@2f
    .line 91
    :cond_2f
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, p1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@39
    goto :goto_2e

    #@3a
    .line 96
    :cond_3a
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0, p1}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@44
    goto :goto_2e
.end method

.method private countNumbers(Landroid/text/Editable;)I
    .registers 6
    .parameter "text"

    #@0
    .prologue
    .line 250
    const/4 v1, 0x0

    #@1
    .line 251
    .local v1, p:I
    const/4 v0, 0x0

    #@2
    .line 252
    .local v0, count:I
    const/4 v1, 0x0

    #@3
    :goto_3
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_16

    #@9
    .line 253
    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    #@c
    move-result v2

    #@d
    const/16 v3, 0x2f

    #@f
    if-eq v2, v3, :cond_13

    #@11
    .line 254
    add-int/lit8 v0, v0, 0x1

    #@13
    .line 252
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_3

    #@16
    .line 257
    :cond_16
    return v0
.end method

.method private getFormattedNumber(CZ)Ljava/lang/String;
    .registers 4
    .parameter "lastNonSeparator"
    .parameter "hasCursor"

    #@0
    .prologue
    .line 219
    if-eqz p2, :cond_9

    #@2
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@4
    invoke-virtual {v0, p1}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->inputDigitAndRememberPosition(C)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    goto :goto_8
.end method

.method private hasSeparator(Ljava/lang/CharSequence;II)Z
    .registers 7
    .parameter "s"
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 229
    move v1, p2

    #@1
    .local v1, i:I
    :goto_1
    add-int v2, p2, p3

    #@3
    if-ge v1, v2, :cond_14

    #@5
    .line 230
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@8
    move-result v0

    #@9
    .line 231
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_11

    #@f
    .line 232
    const/4 v2, 0x1

    #@10
    .line 235
    .end local v0           #c:C
    :goto_10
    return v2

    #@11
    .line 229
    .restart local v0       #c:C
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_1

    #@14
    .line 235
    .end local v0           #c:C
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_10
.end method

.method private reformat(Ljava/lang/CharSequence;I)Ljava/lang/String;
    .registers 11
    .parameter "s"
    .parameter "cursor"

    #@0
    .prologue
    .line 193
    add-int/lit8 v1, p2, -0x1

    #@2
    .line 194
    .local v1, curIndex:I
    const/4 v2, 0x0

    #@3
    .line 195
    .local v2, formatted:Ljava/lang/String;
    iget-object v7, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@5
    invoke-virtual {v7}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->clear()V

    #@8
    .line 196
    const/4 v5, 0x0

    #@9
    .line 197
    .local v5, lastNonSeparator:C
    const/4 v3, 0x0

    #@a
    .line 198
    .local v3, hasCursor:Z
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v6

    #@e
    .line 199
    .local v6, len:I
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v6, :cond_29

    #@11
    .line 200
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@14
    move-result v0

    #@15
    .line 201
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isNonSeparator(C)Z

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_23

    #@1b
    .line 202
    if-eqz v5, :cond_22

    #@1d
    .line 203
    invoke-direct {p0, v5, v3}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->getFormattedNumber(CZ)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 204
    const/4 v3, 0x0

    #@22
    .line 206
    :cond_22
    move v5, v0

    #@23
    .line 208
    :cond_23
    if-ne v4, v1, :cond_26

    #@25
    .line 209
    const/4 v3, 0x1

    #@26
    .line 199
    :cond_26
    add-int/lit8 v4, v4, 0x1

    #@28
    goto :goto_f

    #@29
    .line 212
    .end local v0           #c:C
    :cond_29
    if-eqz v5, :cond_2f

    #@2b
    .line 213
    invoke-direct {p0, v5, v3}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->getFormattedNumber(CZ)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    .line 215
    :cond_2f
    return-object v2
.end method

.method private static removeSlashes(Landroid/text/Editable;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 240
    const/4 v0, 0x0

    #@1
    .line 241
    .local v0, p:I
    :goto_1
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    #@4
    move-result v1

    #@5
    if-ge v0, v1, :cond_18

    #@7
    .line 242
    invoke-interface {p0, v0}, Landroid/text/Editable;->charAt(I)C

    #@a
    move-result v1

    #@b
    const/16 v2, 0x2f

    #@d
    if-ne v1, v2, :cond_15

    #@f
    .line 243
    add-int/lit8 v1, v0, 0x1

    #@11
    invoke-interface {p0, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@14
    goto :goto_1

    #@15
    .line 245
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_1

    #@18
    .line 248
    :cond_18
    return-void
.end method

.method private stopFormatting()V
    .registers 2

    #@0
    .prologue
    .line 224
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mStopFormatting:Z

    #@3
    .line 225
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@5
    invoke-virtual {v0}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->clear()V

    #@8
    .line 226
    return-void
.end method


# virtual methods
.method public declared-synchronized afterTextChanged(Landroid/text/Editable;)V
    .registers 10
    .parameter "s"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 146
    monitor-enter p0

    #@3
    const/4 v2, 0x0

    #@4
    :try_start_4
    const-string v4, "LGE_NumberFormat"

    #@6
    invoke-static {v2, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1d

    #@c
    .line 148
    const-string v2, "KR"

    #@e
    iget-object v4, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mCountryCode:Ljava/lang/String;

    #@10
    invoke-virtual {v2, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_1d

    #@16
    .line 149
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->LGEPhoneNumberFormattingTextWatcher:Landroid/telephony/LgePhoneNumberFormattingTextWatcher;

    #@18
    invoke-virtual {v0, p1}, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_2a

    #@1b
    .line 184
    :cond_1b
    :goto_1b
    monitor-exit p0

    #@1c
    return-void

    #@1d
    .line 154
    :cond_1d
    :try_start_1d
    iget-boolean v2, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mStopFormatting:Z

    #@1f
    if-eqz v2, :cond_2f

    #@21
    .line 156
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_2d

    #@27
    :goto_27
    iput-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mStopFormatting:Z
    :try_end_29
    .catchall {:try_start_1d .. :try_end_29} :catchall_2a

    #@29
    goto :goto_1b

    #@2a
    .line 146
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit p0

    #@2c
    throw v0

    #@2d
    :cond_2d
    move v0, v1

    #@2e
    .line 156
    goto :goto_27

    #@2f
    .line 159
    :cond_2f
    :try_start_2f
    iget-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mSelfChange:Z

    #@31
    if-nez v0, :cond_1b

    #@33
    .line 163
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@36
    move-result v0

    #@37
    invoke-direct {p0, p1, v0}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->reformat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    .line 164
    .local v3, formatted:Ljava/lang/String;
    if-eqz v3, :cond_1b

    #@3d
    .line 165
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mFormatter:Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;

    #@3f
    invoke-virtual {v0}, Lcom/android/i18n/phonenumbers/AsYouTypeFormatter;->getRememberedPosition()I

    #@42
    move-result v6

    #@43
    .line 166
    .local v6, rememberedPos:I
    const/4 v0, 0x1

    #@44
    iput-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mSelfChange:Z

    #@46
    .line 167
    const/4 v1, 0x0

    #@47
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@4a
    move-result v2

    #@4b
    const/4 v4, 0x0

    #@4c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@4f
    move-result v5

    #@50
    move-object v0, p1

    #@51
    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    #@54
    .line 170
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@57
    move-result-object v0

    #@58
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_61

    #@5e
    .line 171
    invoke-static {p1, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@61
    .line 174
    :cond_61
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->isNoSlashNumberFormatCountry()Z

    #@64
    move-result v0

    #@65
    if-eqz v0, :cond_8e

    #@67
    .line 175
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getSimInfo()Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;

    #@6a
    move-result-object v7

    #@6b
    .line 176
    .local v7, simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    invoke-virtual {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@6e
    move-result-object v0

    #@6f
    if-eqz v0, :cond_8e

    #@71
    invoke-virtual {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;->getMcc()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    const-string v1, "231"

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7a
    move-result v0

    #@7b
    if-eqz v0, :cond_8e

    #@7d
    .line 177
    invoke-direct {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->countNumbers(Landroid/text/Editable;)I

    #@80
    move-result v0

    #@81
    const/4 v1, 0x3

    #@82
    if-eq v0, v1, :cond_8b

    #@84
    invoke-direct {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->countNumbers(Landroid/text/Editable;)I

    #@87
    move-result v0

    #@88
    const/4 v1, 0x4

    #@89
    if-ne v0, v1, :cond_8e

    #@8b
    .line 178
    :cond_8b
    invoke-static {p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->removeSlashes(Landroid/text/Editable;)V

    #@8e
    .line 182
    .end local v7           #simInfo:Lcom/android/internal/telephony/lgeautoprofiling/LgeSimInfo;
    :cond_8e
    const/4 v0, 0x0

    #@8f
    iput-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mSelfChange:Z
    :try_end_91
    .catchall {:try_start_2f .. :try_end_91} :catchall_2a

    #@91
    goto :goto_1b
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x0

    #@1
    const-string v1, "LGE_NumberFormat"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_19

    #@9
    .line 108
    const-string v0, "KR"

    #@b
    iget-object v1, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mCountryCode:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_19

    #@13
    .line 109
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->LGEPhoneNumberFormattingTextWatcher:Landroid/telephony/LgePhoneNumberFormattingTextWatcher;

    #@15
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    #@18
    .line 121
    :cond_18
    :goto_18
    return-void

    #@19
    .line 114
    :cond_19
    iget-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mSelfChange:Z

    #@1b
    if-nez v0, :cond_18

    #@1d
    iget-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mStopFormatting:Z

    #@1f
    if-nez v0, :cond_18

    #@21
    .line 118
    if-lez p3, :cond_18

    #@23
    invoke-direct {p0, p1, p2, p3}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->hasSeparator(Ljava/lang/CharSequence;II)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_18

    #@29
    .line 119
    invoke-direct {p0}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->stopFormatting()V

    #@2c
    goto :goto_18
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 126
    const/4 v0, 0x0

    #@1
    const-string v1, "LGE_NumberFormat"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_19

    #@9
    .line 128
    const-string v0, "KR"

    #@b
    iget-object v1, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mCountryCode:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_19

    #@13
    .line 129
    iget-object v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->LGEPhoneNumberFormattingTextWatcher:Landroid/telephony/LgePhoneNumberFormattingTextWatcher;

    #@15
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/telephony/LgePhoneNumberFormattingTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    #@18
    .line 141
    :cond_18
    :goto_18
    return-void

    #@19
    .line 134
    :cond_19
    iget-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mSelfChange:Z

    #@1b
    if-nez v0, :cond_18

    #@1d
    iget-boolean v0, p0, Landroid/telephony/PhoneNumberFormattingTextWatcher;->mStopFormatting:Z

    #@1f
    if-nez v0, :cond_18

    #@21
    .line 138
    if-lez p4, :cond_18

    #@23
    invoke-direct {p0, p1, p2, p4}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->hasSeparator(Ljava/lang/CharSequence;II)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_18

    #@29
    .line 139
    invoke-direct {p0}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->stopFormatting()V

    #@2c
    goto :goto_18
.end method
