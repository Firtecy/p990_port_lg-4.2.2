.class public Landroid/telephony/gsm/GsmCellLocation;
.super Landroid/telephony/CellLocation;
.source "GsmCellLocation.java"


# instance fields
.field private mCid:I

.field private mLac:I

.field private mPsc:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 33
    invoke-direct {p0}, Landroid/telephony/CellLocation;-><init>()V

    #@4
    .line 26
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@6
    .line 27
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@8
    .line 28
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@a
    .line 34
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@c
    .line 35
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@e
    .line 36
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@10
    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .registers 4
    .parameter "bundle"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 42
    invoke-direct {p0}, Landroid/telephony/CellLocation;-><init>()V

    #@4
    .line 26
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@6
    .line 27
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@8
    .line 28
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@a
    .line 43
    const-string/jumbo v0, "lac"

    #@d
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@f
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@15
    .line 44
    const-string v0, "cid"

    #@17
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@19
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@1f
    .line 45
    const-string/jumbo v0, "psc"

    #@22
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@24
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@2a
    .line 46
    return-void
.end method

.method private static equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 133
    if-nez p0, :cond_8

    #@2
    if-nez p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5

    #@8
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 107
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 112
    .local v2, s:Landroid/telephony/gsm/GsmCellLocation;
    if-nez p1, :cond_a

    #@7
    .line 116
    .end local v2           #s:Landroid/telephony/gsm/GsmCellLocation;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 108
    :catch_8
    move-exception v1

    #@9
    .line 109
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 116
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Landroid/telephony/gsm/GsmCellLocation;
    :cond_a
    iget v4, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v4

    #@10
    iget v5, v2, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@12
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v5

    #@16
    invoke-static {v4, v5}, Landroid/telephony/gsm/GsmCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_7

    #@1c
    iget v4, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@1e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    iget v5, v2, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@24
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v5

    #@28
    invoke-static {v4, v5}, Landroid/telephony/gsm/GsmCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_7

    #@2e
    iget v4, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@30
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v4

    #@34
    iget v5, v2, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@36
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v5

    #@3a
    invoke-static {v4, v5}, Landroid/telephony/gsm/GsmCellLocation;->equalsHandlesNulls(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_7

    #@40
    const/4 v3, 0x1

    #@41
    goto :goto_7
.end method

.method public fillInNotifierBundle(Landroid/os/Bundle;)V
    .registers 4
    .parameter "m"

    #@0
    .prologue
    .line 142
    const-string/jumbo v0, "lac"

    #@3
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@8
    .line 143
    const-string v0, "cid"

    #@a
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@f
    .line 144
    const-string/jumbo v0, "psc"

    #@12
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@14
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@17
    .line 145
    return-void
.end method

.method public getCid()I
    .registers 2

    #@0
    .prologue
    .line 59
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@2
    return v0
.end method

.method public getLac()I
    .registers 2

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@2
    return v0
.end method

.method public getPsc()I
    .registers 2

    #@0
    .prologue
    .line 69
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 99
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@2
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@4
    xor-int/2addr v0, v1

    #@5
    return v0
.end method

.method public isEmpty()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 151
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@3
    if-ne v0, v1, :cond_f

    #@5
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@7
    if-ne v0, v1, :cond_f

    #@9
    iget v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@b
    if-ne v0, v1, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public setLacAndCid(II)V
    .registers 3
    .parameter "lac"
    .parameter "cid"

    #@0
    .prologue
    .line 85
    iput p1, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@2
    .line 86
    iput p2, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@4
    .line 87
    return-void
.end method

.method public setPsc(I)V
    .registers 2
    .parameter "psc"

    #@0
    .prologue
    .line 94
    iput p1, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@2
    .line 95
    return-void
.end method

.method public setStateInvalid()V
    .registers 2

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 76
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@3
    .line 77
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@5
    .line 78
    iput v0, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@7
    .line 79
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mLac:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ","

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mCid:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ","

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/telephony/gsm/GsmCellLocation;->mPsc:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "]"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method
