.class public Landroid/gesture/GestureStroke;
.super Ljava/lang/Object;
.source "GestureStroke.java"


# static fields
.field static final TOUCH_TOLERANCE:F = 3.0f


# instance fields
.field public final boundingBox:Landroid/graphics/RectF;

.field public final length:F

.field private mCachedPath:Landroid/graphics/Path;

.field public final points:[F

.field private final timestamps:[J


# direct methods
.method private constructor <init>(Landroid/graphics/RectF;F[F[J)V
    .registers 10
    .parameter "bbx"
    .parameter "len"
    .parameter "pts"
    .parameter "times"

    #@0
    .prologue
    .line 88
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 89
    new-instance v0, Landroid/graphics/RectF;

    #@5
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@7
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@9
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@b
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@d
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@10
    iput-object v0, p0, Landroid/gesture/GestureStroke;->boundingBox:Landroid/graphics/RectF;

    #@12
    .line 90
    iput p2, p0, Landroid/gesture/GestureStroke;->length:F

    #@14
    .line 91
    invoke-virtual {p3}, [F->clone()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, [F

    #@1a
    iput-object v0, p0, Landroid/gesture/GestureStroke;->points:[F

    #@1c
    .line 92
    invoke-virtual {p4}, [J->clone()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, [J

    #@22
    iput-object v0, p0, Landroid/gesture/GestureStroke;->timestamps:[J

    #@24
    .line 93
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .registers 19
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GesturePoint;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 49
    .local p1, points:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GesturePoint;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    .line 51
    .local v2, count:I
    mul-int/lit8 v9, v2, 0x2

    #@9
    new-array v8, v9, [F

    #@b
    .line 52
    .local v8, tmpPoints:[F
    new-array v7, v2, [J

    #@d
    .line 54
    .local v7, times:[J
    const/4 v1, 0x0

    #@e
    .line 55
    .local v1, bx:Landroid/graphics/RectF;
    const/4 v5, 0x0

    #@f
    .line 56
    .local v5, len:F
    const/4 v4, 0x0

    #@10
    .line 58
    .local v4, index:I
    const/4 v3, 0x0

    #@11
    .local v3, i:I
    :goto_11
    if-ge v3, v2, :cond_7c

    #@13
    .line 59
    move-object/from16 v0, p1

    #@15
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v6

    #@19
    check-cast v6, Landroid/gesture/GesturePoint;

    #@1b
    .line 60
    .local v6, p:Landroid/gesture/GesturePoint;
    mul-int/lit8 v9, v3, 0x2

    #@1d
    iget v10, v6, Landroid/gesture/GesturePoint;->x:F

    #@1f
    aput v10, v8, v9

    #@21
    .line 61
    mul-int/lit8 v9, v3, 0x2

    #@23
    add-int/lit8 v9, v9, 0x1

    #@25
    iget v10, v6, Landroid/gesture/GesturePoint;->y:F

    #@27
    aput v10, v8, v9

    #@29
    .line 62
    iget-wide v9, v6, Landroid/gesture/GesturePoint;->timestamp:J

    #@2b
    aput-wide v9, v7, v4

    #@2d
    .line 64
    if-nez v1, :cond_4a

    #@2f
    .line 65
    new-instance v1, Landroid/graphics/RectF;

    #@31
    .end local v1           #bx:Landroid/graphics/RectF;
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    #@34
    .line 66
    .restart local v1       #bx:Landroid/graphics/RectF;
    iget v9, v6, Landroid/gesture/GesturePoint;->y:F

    #@36
    iput v9, v1, Landroid/graphics/RectF;->top:F

    #@38
    .line 67
    iget v9, v6, Landroid/gesture/GesturePoint;->x:F

    #@3a
    iput v9, v1, Landroid/graphics/RectF;->left:F

    #@3c
    .line 68
    iget v9, v6, Landroid/gesture/GesturePoint;->x:F

    #@3e
    iput v9, v1, Landroid/graphics/RectF;->right:F

    #@40
    .line 69
    iget v9, v6, Landroid/gesture/GesturePoint;->y:F

    #@42
    iput v9, v1, Landroid/graphics/RectF;->bottom:F

    #@44
    .line 70
    const/4 v5, 0x0

    #@45
    .line 76
    :goto_45
    add-int/lit8 v4, v4, 0x1

    #@47
    .line 58
    add-int/lit8 v3, v3, 0x1

    #@49
    goto :goto_11

    #@4a
    .line 72
    :cond_4a
    float-to-double v9, v5

    #@4b
    iget v11, v6, Landroid/gesture/GesturePoint;->x:F

    #@4d
    add-int/lit8 v12, v3, -0x1

    #@4f
    mul-int/lit8 v12, v12, 0x2

    #@51
    aget v12, v8, v12

    #@53
    sub-float/2addr v11, v12

    #@54
    float-to-double v11, v11

    #@55
    const-wide/high16 v13, 0x4000

    #@57
    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    #@5a
    move-result-wide v11

    #@5b
    iget v13, v6, Landroid/gesture/GesturePoint;->y:F

    #@5d
    add-int/lit8 v14, v3, -0x1

    #@5f
    mul-int/lit8 v14, v14, 0x2

    #@61
    add-int/lit8 v14, v14, 0x1

    #@63
    aget v14, v8, v14

    #@65
    sub-float/2addr v13, v14

    #@66
    float-to-double v13, v13

    #@67
    const-wide/high16 v15, 0x4000

    #@69
    invoke-static/range {v13 .. v16}, Ljava/lang/Math;->pow(DD)D

    #@6c
    move-result-wide v13

    #@6d
    add-double/2addr v11, v13

    #@6e
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@71
    move-result-wide v11

    #@72
    add-double/2addr v9, v11

    #@73
    double-to-float v5, v9

    #@74
    .line 74
    iget v9, v6, Landroid/gesture/GesturePoint;->x:F

    #@76
    iget v10, v6, Landroid/gesture/GesturePoint;->y:F

    #@78
    invoke-virtual {v1, v9, v10}, Landroid/graphics/RectF;->union(FF)V

    #@7b
    goto :goto_45

    #@7c
    .line 79
    .end local v6           #p:Landroid/gesture/GesturePoint;
    :cond_7c
    move-object/from16 v0, p0

    #@7e
    iput-object v7, v0, Landroid/gesture/GestureStroke;->timestamps:[J

    #@80
    .line 80
    move-object/from16 v0, p0

    #@82
    iput-object v8, v0, Landroid/gesture/GestureStroke;->points:[F

    #@84
    .line 81
    move-object/from16 v0, p0

    #@86
    iput-object v1, v0, Landroid/gesture/GestureStroke;->boundingBox:Landroid/graphics/RectF;

    #@88
    .line 82
    move-object/from16 v0, p0

    #@8a
    iput v5, v0, Landroid/gesture/GestureStroke;->length:F

    #@8c
    .line 83
    return-void
.end method

.method static deserialize(Ljava/io/DataInputStream;)Landroid/gesture/GestureStroke;
    .registers 5
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    #@3
    move-result v0

    #@4
    .line 223
    .local v0, count:I
    new-instance v2, Ljava/util/ArrayList;

    #@6
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    .line 224
    .local v2, points:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GesturePoint;>;"
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_16

    #@c
    .line 225
    invoke-static {p0}, Landroid/gesture/GesturePoint;->deserialize(Ljava/io/DataInputStream;)Landroid/gesture/GesturePoint;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 224
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_a

    #@16
    .line 228
    :cond_16
    new-instance v3, Landroid/gesture/GestureStroke;

    #@18
    invoke-direct {v3, v2}, Landroid/gesture/GestureStroke;-><init>(Ljava/util/ArrayList;)V

    #@1b
    return-object v3
.end method

.method private makePath()V
    .registers 15

    #@0
    .prologue
    const/high16 v13, 0x4040

    #@2
    const/high16 v12, 0x4000

    #@4
    .line 122
    iget-object v4, p0, Landroid/gesture/GestureStroke;->points:[F

    #@6
    .line 123
    .local v4, localPoints:[F
    array-length v0, v4

    #@7
    .line 125
    .local v0, count:I
    const/4 v7, 0x0

    #@8
    .line 127
    .local v7, path:Landroid/graphics/Path;
    const/4 v5, 0x0

    #@9
    .line 128
    .local v5, mX:F
    const/4 v6, 0x0

    #@a
    .line 130
    .local v6, mY:F
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_42

    #@d
    .line 131
    aget v8, v4, v3

    #@f
    .line 132
    .local v8, x:F
    add-int/lit8 v10, v3, 0x1

    #@11
    aget v9, v4, v10

    #@13
    .line 133
    .local v9, y:F
    if-nez v7, :cond_22

    #@15
    .line 134
    new-instance v7, Landroid/graphics/Path;

    #@17
    .end local v7           #path:Landroid/graphics/Path;
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    #@1a
    .line 135
    .restart local v7       #path:Landroid/graphics/Path;
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    #@1d
    .line 136
    move v5, v8

    #@1e
    .line 137
    move v6, v9

    #@1f
    .line 130
    :cond_1f
    :goto_1f
    add-int/lit8 v3, v3, 0x2

    #@21
    goto :goto_b

    #@22
    .line 139
    :cond_22
    sub-float v10, v8, v5

    #@24
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@27
    move-result v1

    #@28
    .line 140
    .local v1, dx:F
    sub-float v10, v9, v6

    #@2a
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@2d
    move-result v2

    #@2e
    .line 141
    .local v2, dy:F
    cmpl-float v10, v1, v13

    #@30
    if-gez v10, :cond_36

    #@32
    cmpl-float v10, v2, v13

    #@34
    if-ltz v10, :cond_1f

    #@36
    .line 142
    :cond_36
    add-float v10, v8, v5

    #@38
    div-float/2addr v10, v12

    #@39
    add-float v11, v9, v6

    #@3b
    div-float/2addr v11, v12

    #@3c
    invoke-virtual {v7, v5, v6, v10, v11}, Landroid/graphics/Path;->quadTo(FFFF)V

    #@3f
    .line 143
    move v5, v8

    #@40
    .line 144
    move v6, v9

    #@41
    goto :goto_1f

    #@42
    .line 149
    .end local v1           #dx:F
    .end local v2           #dy:F
    .end local v8           #x:F
    .end local v9           #y:F
    :cond_42
    iput-object v7, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@44
    .line 150
    return-void
.end method


# virtual methods
.method public clearPath()V
    .registers 2

    #@0
    .prologue
    .line 235
    iget-object v0, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@6
    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    #@9
    .line 236
    :cond_9
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .registers 6

    #@0
    .prologue
    .line 97
    new-instance v0, Landroid/gesture/GestureStroke;

    #@2
    iget-object v1, p0, Landroid/gesture/GestureStroke;->boundingBox:Landroid/graphics/RectF;

    #@4
    iget v2, p0, Landroid/gesture/GestureStroke;->length:F

    #@6
    iget-object v3, p0, Landroid/gesture/GestureStroke;->points:[F

    #@8
    iget-object v4, p0, Landroid/gesture/GestureStroke;->timestamps:[J

    #@a
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/gesture/GestureStroke;-><init>(Landroid/graphics/RectF;F[F[J)V

    #@d
    return-object v0
.end method

.method public computeOrientedBoundingBox()Landroid/gesture/OrientedBoundingBox;
    .registers 2

    #@0
    .prologue
    .line 244
    iget-object v0, p0, Landroid/gesture/GestureStroke;->points:[F

    #@2
    invoke-static {v0}, Landroid/gesture/GestureUtils;->computeOrientedBoundingBox([F)Landroid/gesture/OrientedBoundingBox;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .registers 4
    .parameter "canvas"
    .parameter "paint"

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 107
    invoke-direct {p0}, Landroid/gesture/GestureStroke;->makePath()V

    #@7
    .line 110
    :cond_7
    iget-object v0, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@9
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@c
    .line 111
    return-void
.end method

.method public getPath()Landroid/graphics/Path;
    .registers 2

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 115
    invoke-direct {p0}, Landroid/gesture/GestureStroke;->makePath()V

    #@7
    .line 118
    :cond_7
    iget-object v0, p0, Landroid/gesture/GestureStroke;->mCachedPath:Landroid/graphics/Path;

    #@9
    return-object v0
.end method

.method serialize(Ljava/io/DataOutputStream;)V
    .registers 8
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    iget-object v2, p0, Landroid/gesture/GestureStroke;->points:[F

    #@2
    .line 203
    .local v2, pts:[F
    iget-object v3, p0, Landroid/gesture/GestureStroke;->timestamps:[J

    #@4
    .line 204
    .local v3, times:[J
    iget-object v4, p0, Landroid/gesture/GestureStroke;->points:[F

    #@6
    array-length v0, v4

    #@7
    .line 207
    .local v0, count:I
    div-int/lit8 v4, v0, 0x2

    #@9
    invoke-virtual {p1, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@c
    .line 209
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_25

    #@f
    .line 211
    aget v4, v2, v1

    #@11
    invoke-virtual {p1, v4}, Ljava/io/DataOutputStream;->writeFloat(F)V

    #@14
    .line 213
    add-int/lit8 v4, v1, 0x1

    #@16
    aget v4, v2, v4

    #@18
    invoke-virtual {p1, v4}, Ljava/io/DataOutputStream;->writeFloat(F)V

    #@1b
    .line 215
    div-int/lit8 v4, v1, 0x2

    #@1d
    aget-wide v4, v3, v4

    #@1f
    invoke-virtual {p1, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    #@22
    .line 209
    add-int/lit8 v1, v1, 0x2

    #@24
    goto :goto_d

    #@25
    .line 217
    :cond_25
    return-void
.end method

.method public toPath(FFI)Landroid/graphics/Path;
    .registers 23
    .parameter "width"
    .parameter "height"
    .parameter "numSample"

    #@0
    .prologue
    .line 162
    move-object/from16 v0, p0

    #@2
    move/from16 v1, p3

    #@4
    invoke-static {v0, v1}, Landroid/gesture/GestureUtils;->temporalSampling(Landroid/gesture/GestureStroke;I)[F

    #@7
    move-result-object v9

    #@8
    .line 163
    .local v9, pts:[F
    move-object/from16 v0, p0

    #@a
    iget-object v10, v0, Landroid/gesture/GestureStroke;->boundingBox:Landroid/graphics/RectF;

    #@c
    .line 165
    .local v10, rect:Landroid/graphics/RectF;
    iget v0, v10, Landroid/graphics/RectF;->left:F

    #@e
    move/from16 v16, v0

    #@10
    move/from16 v0, v16

    #@12
    neg-float v0, v0

    #@13
    move/from16 v16, v0

    #@15
    iget v0, v10, Landroid/graphics/RectF;->top:F

    #@17
    move/from16 v17, v0

    #@19
    move/from16 v0, v17

    #@1b
    neg-float v0, v0

    #@1c
    move/from16 v17, v0

    #@1e
    move/from16 v0, v16

    #@20
    move/from16 v1, v17

    #@22
    invoke-static {v9, v0, v1}, Landroid/gesture/GestureUtils;->translate([FFF)[F

    #@25
    .line 167
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    #@28
    move-result v16

    #@29
    div-float v12, p1, v16

    #@2b
    .line 168
    .local v12, sx:F
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    #@2e
    move-result v16

    #@2f
    div-float v13, p2, v16

    #@31
    .line 169
    .local v13, sy:F
    cmpl-float v16, v12, v13

    #@33
    if-lez v16, :cond_55

    #@35
    move v11, v13

    #@36
    .line 170
    .local v11, scale:F
    :goto_36
    invoke-static {v9, v11, v11}, Landroid/gesture/GestureUtils;->scale([FFF)[F

    #@39
    .line 172
    const/4 v6, 0x0

    #@3a
    .line 173
    .local v6, mX:F
    const/4 v7, 0x0

    #@3b
    .line 175
    .local v7, mY:F
    const/4 v8, 0x0

    #@3c
    .line 177
    .local v8, path:Landroid/graphics/Path;
    array-length v2, v9

    #@3d
    .line 179
    .local v2, count:I
    const/4 v5, 0x0

    #@3e
    .local v5, i:I
    :goto_3e
    if-ge v5, v2, :cond_85

    #@40
    .line 180
    aget v14, v9, v5

    #@42
    .line 181
    .local v14, x:F
    add-int/lit8 v16, v5, 0x1

    #@44
    aget v15, v9, v16

    #@46
    .line 182
    .local v15, y:F
    if-nez v8, :cond_57

    #@48
    .line 183
    new-instance v8, Landroid/graphics/Path;

    #@4a
    .end local v8           #path:Landroid/graphics/Path;
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    #@4d
    .line 184
    .restart local v8       #path:Landroid/graphics/Path;
    invoke-virtual {v8, v14, v15}, Landroid/graphics/Path;->moveTo(FF)V

    #@50
    .line 185
    move v6, v14

    #@51
    .line 186
    move v7, v15

    #@52
    .line 179
    :cond_52
    :goto_52
    add-int/lit8 v5, v5, 0x2

    #@54
    goto :goto_3e

    #@55
    .end local v2           #count:I
    .end local v5           #i:I
    .end local v6           #mX:F
    .end local v7           #mY:F
    .end local v8           #path:Landroid/graphics/Path;
    .end local v11           #scale:F
    .end local v14           #x:F
    .end local v15           #y:F
    :cond_55
    move v11, v12

    #@56
    .line 169
    goto :goto_36

    #@57
    .line 188
    .restart local v2       #count:I
    .restart local v5       #i:I
    .restart local v6       #mX:F
    .restart local v7       #mY:F
    .restart local v8       #path:Landroid/graphics/Path;
    .restart local v11       #scale:F
    .restart local v14       #x:F
    .restart local v15       #y:F
    :cond_57
    sub-float v16, v14, v6

    #@59
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    #@5c
    move-result v3

    #@5d
    .line 189
    .local v3, dx:F
    sub-float v16, v15, v7

    #@5f
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    #@62
    move-result v4

    #@63
    .line 190
    .local v4, dy:F
    const/high16 v16, 0x4040

    #@65
    cmpl-float v16, v3, v16

    #@67
    if-gez v16, :cond_6f

    #@69
    const/high16 v16, 0x4040

    #@6b
    cmpl-float v16, v4, v16

    #@6d
    if-ltz v16, :cond_52

    #@6f
    .line 191
    :cond_6f
    add-float v16, v14, v6

    #@71
    const/high16 v17, 0x4000

    #@73
    div-float v16, v16, v17

    #@75
    add-float v17, v15, v7

    #@77
    const/high16 v18, 0x4000

    #@79
    div-float v17, v17, v18

    #@7b
    move/from16 v0, v16

    #@7d
    move/from16 v1, v17

    #@7f
    invoke-virtual {v8, v6, v7, v0, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    #@82
    .line 192
    move v6, v14

    #@83
    .line 193
    move v7, v15

    #@84
    goto :goto_52

    #@85
    .line 198
    .end local v3           #dx:F
    .end local v4           #dy:F
    .end local v14           #x:F
    .end local v15           #y:F
    :cond_85
    return-object v8
.end method
