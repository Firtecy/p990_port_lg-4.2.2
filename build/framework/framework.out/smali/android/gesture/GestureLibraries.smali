.class public final Landroid/gesture/GestureLibraries;
.super Ljava/lang/Object;
.source "GestureLibraries.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/gesture/GestureLibraries$ResourceGestureLibrary;,
        Landroid/gesture/GestureLibraries$FileGestureLibrary;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    return-void
.end method

.method public static fromFile(Ljava/io/File;)Landroid/gesture/GestureLibrary;
    .registers 2
    .parameter "path"

    #@0
    .prologue
    .line 40
    new-instance v0, Landroid/gesture/GestureLibraries$FileGestureLibrary;

    #@2
    invoke-direct {v0, p0}, Landroid/gesture/GestureLibraries$FileGestureLibrary;-><init>(Ljava/io/File;)V

    #@5
    return-object v0
.end method

.method public static fromFile(Ljava/lang/String;)Landroid/gesture/GestureLibrary;
    .registers 2
    .parameter "path"

    #@0
    .prologue
    .line 36
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    invoke-static {v0}, Landroid/gesture/GestureLibraries;->fromFile(Ljava/io/File;)Landroid/gesture/GestureLibrary;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static fromPrivateFile(Landroid/content/Context;Ljava/lang/String;)Landroid/gesture/GestureLibrary;
    .registers 3
    .parameter "context"
    .parameter "name"

    #@0
    .prologue
    .line 44
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/gesture/GestureLibraries;->fromFile(Ljava/io/File;)Landroid/gesture/GestureLibrary;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static fromRawResource(Landroid/content/Context;I)Landroid/gesture/GestureLibrary;
    .registers 3
    .parameter "context"
    .parameter "resourceId"

    #@0
    .prologue
    .line 48
    new-instance v0, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;-><init>(Landroid/content/Context;I)V

    #@5
    return-object v0
.end method
