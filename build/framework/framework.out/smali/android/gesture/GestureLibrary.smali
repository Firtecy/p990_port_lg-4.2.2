.class public abstract Landroid/gesture/GestureLibrary;
.super Ljava/lang/Object;
.source "GestureLibrary.java"


# instance fields
.field protected final mStore:Landroid/gesture/GestureStore;


# direct methods
.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    new-instance v0, Landroid/gesture/GestureStore;

    #@5
    invoke-direct {v0}, Landroid/gesture/GestureStore;-><init>()V

    #@8
    iput-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@a
    .line 28
    return-void
.end method


# virtual methods
.method public addGesture(Ljava/lang/String;Landroid/gesture/Gesture;)V
    .registers 4
    .parameter "entryName"
    .parameter "gesture"

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/gesture/GestureStore;->addGesture(Ljava/lang/String;Landroid/gesture/Gesture;)V

    #@5
    .line 69
    return-void
.end method

.method public getGestureEntries()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0}, Landroid/gesture/GestureStore;->getGestureEntries()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getGestures(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 3
    .parameter "entryName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/Gesture;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/gesture/GestureStore;->getGestures(Ljava/lang/String;)Ljava/util/ArrayList;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLearner()Landroid/gesture/Learner;
    .registers 2

    #@0
    .prologue
    .line 40
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0}, Landroid/gesture/GestureStore;->getLearner()Landroid/gesture/Learner;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getOrientationStyle()I
    .registers 2

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0}, Landroid/gesture/GestureStore;->getOrientationStyle()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSequenceType()I
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0}, Landroid/gesture/GestureStore;->getSequenceType()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isReadOnly()Z
    .registers 2

    #@0
    .prologue
    .line 35
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public abstract load()Z
.end method

.method public recognize(Landroid/gesture/Gesture;)Ljava/util/ArrayList;
    .registers 3
    .parameter "gesture"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/gesture/Gesture;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/Prediction;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/gesture/GestureStore;->recognize(Landroid/gesture/Gesture;)Ljava/util/ArrayList;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public removeEntry(Ljava/lang/String;)V
    .registers 3
    .parameter "entryName"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/gesture/GestureStore;->removeEntry(Ljava/lang/String;)V

    #@5
    .line 77
    return-void
.end method

.method public removeGesture(Ljava/lang/String;Landroid/gesture/Gesture;)V
    .registers 4
    .parameter "entryName"
    .parameter "gesture"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/gesture/GestureStore;->removeGesture(Ljava/lang/String;Landroid/gesture/Gesture;)V

    #@5
    .line 73
    return-void
.end method

.method public abstract save()Z
.end method

.method public setOrientationStyle(I)V
    .registers 3
    .parameter "style"

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/gesture/GestureStore;->setOrientationStyle(I)V

    #@5
    .line 45
    return-void
.end method

.method public setSequenceType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/gesture/GestureStore;->setSequenceType(I)V

    #@5
    .line 53
    return-void
.end method
