.class Landroid/gesture/InstanceLearner;
.super Landroid/gesture/Learner;
.source "InstanceLearner.java"


# static fields
.field private static final sComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/gesture/Prediction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 29
    new-instance v0, Landroid/gesture/InstanceLearner$1;

    #@2
    invoke-direct {v0}, Landroid/gesture/InstanceLearner$1;-><init>()V

    #@5
    sput-object v0, Landroid/gesture/InstanceLearner;->sComparator:Ljava/util/Comparator;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/gesture/Learner;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method classify(II[F)Ljava/util/ArrayList;
    .registers 23
    .parameter "sequenceType"
    .parameter "orientationType"
    .parameter "vector"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[F)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/Prediction;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 45
    new-instance v11, Ljava/util/ArrayList;

    #@2
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 46
    .local v11, predictions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/Prediction;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/gesture/InstanceLearner;->getInstances()Ljava/util/ArrayList;

    #@8
    move-result-object v8

    #@9
    .line 47
    .local v8, instances:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/Instance;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v3

    #@d
    .line 48
    .local v3, count:I
    new-instance v9, Ljava/util/TreeMap;

    #@f
    invoke-direct {v9}, Ljava/util/TreeMap;-><init>()V

    #@12
    .line 49
    .local v9, label2score:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Double;>;"
    const/4 v6, 0x0

    #@13
    .local v6, i:I
    :goto_13
    if-ge v6, v3, :cond_91

    #@15
    .line 50
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v12

    #@19
    check-cast v12, Landroid/gesture/Instance;

    #@1b
    .line 51
    .local v12, sample:Landroid/gesture/Instance;
    iget-object v0, v12, Landroid/gesture/Instance;->vector:[F

    #@1d
    move-object/from16 v17, v0

    #@1f
    move-object/from16 v0, v17

    #@21
    array-length v0, v0

    #@22
    move/from16 v17, v0

    #@24
    move-object/from16 v0, p3

    #@26
    array-length v0, v0

    #@27
    move/from16 v18, v0

    #@29
    move/from16 v0, v17

    #@2b
    move/from16 v1, v18

    #@2d
    if-eq v0, v1, :cond_32

    #@2f
    .line 49
    :cond_2f
    :goto_2f
    add-int/lit8 v6, v6, 0x1

    #@31
    goto :goto_13

    #@32
    .line 55
    :cond_32
    const/16 v17, 0x2

    #@34
    move/from16 v0, p1

    #@36
    move/from16 v1, v17

    #@38
    if-ne v0, v1, :cond_7c

    #@3a
    .line 56
    iget-object v0, v12, Landroid/gesture/Instance;->vector:[F

    #@3c
    move-object/from16 v17, v0

    #@3e
    move-object/from16 v0, v17

    #@40
    move-object/from16 v1, p3

    #@42
    move/from16 v2, p2

    #@44
    invoke-static {v0, v1, v2}, Landroid/gesture/GestureUtils;->minimumCosineDistance([F[FI)F

    #@47
    move-result v17

    #@48
    move/from16 v0, v17

    #@4a
    float-to-double v4, v0

    #@4b
    .line 61
    .local v4, distance:D
    :goto_4b
    const-wide/16 v17, 0x0

    #@4d
    cmpl-double v17, v4, v17

    #@4f
    if-nez v17, :cond_8c

    #@51
    .line 62
    const-wide v15, 0x7fefffffffffffffL

    #@56
    .line 66
    .local v15, weight:D
    :goto_56
    iget-object v0, v12, Landroid/gesture/Instance;->label:Ljava/lang/String;

    #@58
    move-object/from16 v17, v0

    #@5a
    move-object/from16 v0, v17

    #@5c
    invoke-virtual {v9, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5f
    move-result-object v13

    #@60
    check-cast v13, Ljava/lang/Double;

    #@62
    .line 67
    .local v13, score:Ljava/lang/Double;
    if-eqz v13, :cond_6c

    #@64
    invoke-virtual {v13}, Ljava/lang/Double;->doubleValue()D

    #@67
    move-result-wide v17

    #@68
    cmpl-double v17, v15, v17

    #@6a
    if-lez v17, :cond_2f

    #@6c
    .line 68
    :cond_6c
    iget-object v0, v12, Landroid/gesture/Instance;->label:Ljava/lang/String;

    #@6e
    move-object/from16 v17, v0

    #@70
    invoke-static/range {v15 .. v16}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@73
    move-result-object v18

    #@74
    move-object/from16 v0, v17

    #@76
    move-object/from16 v1, v18

    #@78
    invoke-virtual {v9, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    goto :goto_2f

    #@7c
    .line 58
    .end local v4           #distance:D
    .end local v13           #score:Ljava/lang/Double;
    .end local v15           #weight:D
    :cond_7c
    iget-object v0, v12, Landroid/gesture/Instance;->vector:[F

    #@7e
    move-object/from16 v17, v0

    #@80
    move-object/from16 v0, v17

    #@82
    move-object/from16 v1, p3

    #@84
    invoke-static {v0, v1}, Landroid/gesture/GestureUtils;->squaredEuclideanDistance([F[F)F

    #@87
    move-result v17

    #@88
    move/from16 v0, v17

    #@8a
    float-to-double v4, v0

    #@8b
    .restart local v4       #distance:D
    goto :goto_4b

    #@8c
    .line 64
    :cond_8c
    const-wide/high16 v17, 0x3ff0

    #@8e
    div-double v15, v17, v4

    #@90
    .restart local v15       #weight:D
    goto :goto_56

    #@91
    .line 73
    .end local v4           #distance:D
    .end local v12           #sample:Landroid/gesture/Instance;
    .end local v15           #weight:D
    :cond_91
    invoke-virtual {v9}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    #@94
    move-result-object v17

    #@95
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@98
    move-result-object v7

    #@99
    .local v7, i$:Ljava/util/Iterator;
    :goto_99
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@9c
    move-result v17

    #@9d
    if-eqz v17, :cond_bc

    #@9f
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a2
    move-result-object v10

    #@a3
    check-cast v10, Ljava/lang/String;

    #@a5
    .line 74
    .local v10, name:Ljava/lang/String;
    invoke-virtual {v9, v10}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a8
    move-result-object v17

    #@a9
    check-cast v17, Ljava/lang/Double;

    #@ab
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Double;->doubleValue()D

    #@ae
    move-result-wide v13

    #@af
    .line 76
    .local v13, score:D
    new-instance v17, Landroid/gesture/Prediction;

    #@b1
    move-object/from16 v0, v17

    #@b3
    invoke-direct {v0, v10, v13, v14}, Landroid/gesture/Prediction;-><init>(Ljava/lang/String;D)V

    #@b6
    move-object/from16 v0, v17

    #@b8
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bb
    goto :goto_99

    #@bc
    .line 84
    .end local v10           #name:Ljava/lang/String;
    .end local v13           #score:D
    :cond_bc
    sget-object v17, Landroid/gesture/InstanceLearner;->sComparator:Ljava/util/Comparator;

    #@be
    move-object/from16 v0, v17

    #@c0
    invoke-static {v11, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@c3
    .line 86
    return-object v11
.end method
