.class Landroid/gesture/GestureLibraries$ResourceGestureLibrary;
.super Landroid/gesture/GestureLibrary;
.source "GestureLibraries.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/gesture/GestureLibraries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResourceGestureLibrary"
.end annotation


# instance fields
.field private final mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final mResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceId"

    #@0
    .prologue
    .line 112
    invoke-direct {p0}, Landroid/gesture/GestureLibrary;-><init>()V

    #@3
    .line 113
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;->mContext:Ljava/lang/ref/WeakReference;

    #@a
    .line 114
    iput p2, p0, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;->mResourceId:I

    #@c
    .line 115
    return-void
.end method


# virtual methods
.method public isReadOnly()Z
    .registers 2

    #@0
    .prologue
    .line 119
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public load()Z
    .registers 9

    #@0
    .prologue
    .line 127
    const/4 v3, 0x0

    #@1
    .line 128
    .local v3, result:Z
    iget-object v4, p0, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;->mContext:Ljava/lang/ref/WeakReference;

    #@3
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/content/Context;

    #@9
    .line 129
    .local v0, context:Landroid/content/Context;
    if-eqz v0, :cond_1c

    #@b
    .line 130
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e
    move-result-object v4

    #@f
    iget v5, p0, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;->mResourceId:I

    #@11
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@14
    move-result-object v2

    #@15
    .line 132
    .local v2, in:Ljava/io/InputStream;
    :try_start_15
    iget-object v4, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@17
    const/4 v5, 0x1

    #@18
    invoke-virtual {v4, v2, v5}, Landroid/gesture/GestureStore;->load(Ljava/io/InputStream;Z)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_1b} :catch_1d

    #@1b
    .line 133
    const/4 v3, 0x1

    #@1c
    .line 140
    .end local v2           #in:Ljava/io/InputStream;
    :cond_1c
    :goto_1c
    return v3

    #@1d
    .line 134
    .restart local v2       #in:Ljava/io/InputStream;
    :catch_1d
    move-exception v1

    #@1e
    .line 135
    .local v1, e:Ljava/io/IOException;
    const-string v4, "Gestures"

    #@20
    new-instance v5, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v6, "Could not load the gesture library from raw resource "

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2e
    move-result-object v6

    #@2f
    iget v7, p0, Landroid/gesture/GestureLibraries$ResourceGestureLibrary;->mResourceId:I

    #@31
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v4, v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    goto :goto_1c
.end method

.method public save()Z
    .registers 2

    #@0
    .prologue
    .line 123
    const/4 v0, 0x0

    #@1
    return v0
.end method
