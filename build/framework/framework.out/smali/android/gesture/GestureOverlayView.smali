.class public Landroid/gesture/GestureOverlayView;
.super Landroid/widget/FrameLayout;
.source "GestureOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/gesture/GestureOverlayView$1;,
        Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;,
        Landroid/gesture/GestureOverlayView$OnGestureListener;,
        Landroid/gesture/GestureOverlayView$OnGesturingListener;,
        Landroid/gesture/GestureOverlayView$FadeOutRunnable;
    }
.end annotation


# static fields
.field private static final DITHER_FLAG:Z = true

.field private static final FADE_ANIMATION_RATE:I = 0x10

.field private static final GESTURE_RENDERING_ANTIALIAS:Z = true

.field public static final GESTURE_STROKE_TYPE_MULTIPLE:I = 0x1

.field public static final GESTURE_STROKE_TYPE_SINGLE:I = 0x0

.field public static final ORIENTATION_HORIZONTAL:I = 0x0

.field public static final ORIENTATION_VERTICAL:I = 0x1


# instance fields
.field private mCertainGestureColor:I

.field private mCurrentColor:I

.field private mCurrentGesture:Landroid/gesture/Gesture;

.field private mCurveEndX:F

.field private mCurveEndY:F

.field private mFadeDuration:J

.field private mFadeEnabled:Z

.field private mFadeOffset:J

.field private mFadingAlpha:F

.field private mFadingHasStarted:Z

.field private final mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

.field private mFadingStart:J

.field private final mGesturePaint:Landroid/graphics/Paint;

.field private mGestureStrokeAngleThreshold:F

.field private mGestureStrokeLengthThreshold:F

.field private mGestureStrokeSquarenessTreshold:F

.field private mGestureStrokeType:I

.field private mGestureStrokeWidth:F

.field private mGestureVisible:Z

.field private mHandleGestureActions:Z

.field private mInterceptEvents:Z

.field private final mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private final mInvalidRect:Landroid/graphics/Rect;

.field private mInvalidateExtraBorder:I

.field private mIsFadingOut:Z

.field private mIsGesturing:Z

.field private mIsListeningForGestures:Z

.field private final mOnGestureListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GestureOverlayView$OnGestureListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnGesturePerformedListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnGesturingListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GestureOverlayView$OnGesturingListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOrientation:I

.field private final mPath:Landroid/graphics/Path;

.field private mPreviousWasGesturing:Z

.field private mResetGesture:Z

.field private final mStrokeBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GesturePoint;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalLength:F

.field private mUncertainGestureColor:I

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 129
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@5
    .line 66
    new-instance v0, Landroid/graphics/Paint;

    #@7
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@a
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@c
    .line 68
    const-wide/16 v0, 0x96

    #@e
    iput-wide v0, p0, Landroid/gesture/GestureOverlayView;->mFadeDuration:J

    #@10
    .line 69
    const-wide/16 v0, 0x1a4

    #@12
    iput-wide v0, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@14
    .line 72
    iput-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@16
    .line 75
    const/16 v0, -0x100

    #@18
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@1a
    .line 76
    const v0, 0x48ffff00

    #@1d
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@1f
    .line 77
    const/high16 v0, 0x4140

    #@21
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@23
    .line 78
    const/16 v0, 0xa

    #@25
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mInvalidateExtraBorder:I

    #@27
    .line 80
    iput v2, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@29
    .line 81
    const/high16 v0, 0x4248

    #@2b
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@2d
    .line 82
    const v0, 0x3e8ccccd

    #@30
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@32
    .line 83
    const/high16 v0, 0x4220

    #@34
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@36
    .line 85
    iput v3, p0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@38
    .line 87
    new-instance v0, Landroid/graphics/Rect;

    #@3a
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@3d
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mInvalidRect:Landroid/graphics/Rect;

    #@3f
    .line 88
    new-instance v0, Landroid/graphics/Path;

    #@41
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    #@44
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@46
    .line 89
    iput-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mGestureVisible:Z

    #@48
    .line 98
    iput-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@4a
    .line 99
    iput-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mPreviousWasGesturing:Z

    #@4c
    .line 100
    iput-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@4e
    .line 106
    new-instance v0, Ljava/util/ArrayList;

    #@50
    const/16 v1, 0x64

    #@52
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@55
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@57
    .line 109
    new-instance v0, Ljava/util/ArrayList;

    #@59
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5c
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@5e
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    #@60
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@63
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@65
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    #@67
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@6a
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@6c
    .line 121
    iput-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@6e
    .line 122
    const/high16 v0, 0x3f80

    #@70
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@72
    .line 123
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@74
    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@77
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@79
    .line 126
    new-instance v0, Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@7b
    const/4 v1, 0x0

    #@7c
    invoke-direct {v0, p0, v1}, Landroid/gesture/GestureOverlayView$FadeOutRunnable;-><init>(Landroid/gesture/GestureOverlayView;Landroid/gesture/GestureOverlayView$1;)V

    #@7f
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@81
    .line 130
    invoke-direct {p0}, Landroid/gesture/GestureOverlayView;->init()V

    #@84
    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 134
    const v0, 0x10103db

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/gesture/GestureOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/16 v5, 0xa

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 138
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@7
    .line 66
    new-instance v1, Landroid/graphics/Paint;

    #@9
    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    #@c
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@e
    .line 68
    const-wide/16 v1, 0x96

    #@10
    iput-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeDuration:J

    #@12
    .line 69
    const-wide/16 v1, 0x1a4

    #@14
    iput-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@16
    .line 72
    iput-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@18
    .line 75
    const/16 v1, -0x100

    #@1a
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@1c
    .line 76
    const v1, 0x48ffff00

    #@1f
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@21
    .line 77
    const/high16 v1, 0x4140

    #@23
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@25
    .line 78
    iput v5, p0, Landroid/gesture/GestureOverlayView;->mInvalidateExtraBorder:I

    #@27
    .line 80
    iput v4, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@29
    .line 81
    const/high16 v1, 0x4248

    #@2b
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@2d
    .line 82
    const v1, 0x3e8ccccd

    #@30
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@32
    .line 83
    const/high16 v1, 0x4220

    #@34
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@36
    .line 85
    iput v3, p0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@38
    .line 87
    new-instance v1, Landroid/graphics/Rect;

    #@3a
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@3d
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mInvalidRect:Landroid/graphics/Rect;

    #@3f
    .line 88
    new-instance v1, Landroid/graphics/Path;

    #@41
    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    #@44
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@46
    .line 89
    iput-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mGestureVisible:Z

    #@48
    .line 98
    iput-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@4a
    .line 99
    iput-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mPreviousWasGesturing:Z

    #@4c
    .line 100
    iput-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@4e
    .line 106
    new-instance v1, Ljava/util/ArrayList;

    #@50
    const/16 v2, 0x64

    #@52
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@55
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@57
    .line 109
    new-instance v1, Ljava/util/ArrayList;

    #@59
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5c
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@5e
    .line 112
    new-instance v1, Ljava/util/ArrayList;

    #@60
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@63
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@65
    .line 115
    new-instance v1, Ljava/util/ArrayList;

    #@67
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@6a
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@6c
    .line 121
    iput-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@6e
    .line 122
    const/high16 v1, 0x3f80

    #@70
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@72
    .line 123
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@74
    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@77
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@79
    .line 126
    new-instance v1, Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@7b
    const/4 v2, 0x0

    #@7c
    invoke-direct {v1, p0, v2}, Landroid/gesture/GestureOverlayView$FadeOutRunnable;-><init>(Landroid/gesture/GestureOverlayView;Landroid/gesture/GestureOverlayView$1;)V

    #@7f
    iput-object v1, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@81
    .line 140
    sget-object v1, Lcom/android/internal/R$styleable;->GestureOverlayView:[I

    #@83
    invoke-virtual {p1, p2, v1, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@86
    move-result-object v0

    #@87
    .line 143
    .local v0, a:Landroid/content/res/TypedArray;
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@89
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@8c
    move-result v1

    #@8d
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@8f
    .line 145
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@91
    float-to-int v1, v1

    #@92
    add-int/lit8 v1, v1, -0x1

    #@94
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    #@97
    move-result v1

    #@98
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mInvalidateExtraBorder:I

    #@9a
    .line 146
    const/4 v1, 0x2

    #@9b
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@9d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    #@a0
    move-result v1

    #@a1
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@a3
    .line 148
    const/4 v1, 0x3

    #@a4
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@a6
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    #@a9
    move-result v1

    #@aa
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@ac
    .line 150
    const/4 v1, 0x5

    #@ad
    iget-wide v2, p0, Landroid/gesture/GestureOverlayView;->mFadeDuration:J

    #@af
    long-to-int v2, v2

    #@b0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@b3
    move-result v1

    #@b4
    int-to-long v1, v1

    #@b5
    iput-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeDuration:J

    #@b7
    .line 151
    const/4 v1, 0x4

    #@b8
    iget-wide v2, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@ba
    long-to-int v2, v2

    #@bb
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@be
    move-result v1

    #@bf
    int-to-long v1, v1

    #@c0
    iput-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@c2
    .line 152
    const/4 v1, 0x6

    #@c3
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@c5
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@c8
    move-result v1

    #@c9
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@cb
    .line 154
    const/4 v1, 0x7

    #@cc
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@ce
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@d1
    move-result v1

    #@d2
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@d4
    .line 157
    const/16 v1, 0x9

    #@d6
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@d8
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@db
    move-result v1

    #@dc
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@de
    .line 160
    const/16 v1, 0x8

    #@e0
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@e2
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@e5
    move-result v1

    #@e6
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@e8
    .line 163
    iget-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@ea
    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ed
    move-result v1

    #@ee
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@f0
    .line 165
    const/16 v1, 0xb

    #@f2
    iget-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@f4
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@f7
    move-result v1

    #@f8
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@fa
    .line 167
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@fc
    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@ff
    move-result v1

    #@100
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@102
    .line 169
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@105
    .line 171
    invoke-direct {p0}, Landroid/gesture/GestureOverlayView;->init()V

    #@108
    .line 172
    return-void
.end method

.method static synthetic access$100(Landroid/gesture/GestureOverlayView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Landroid/gesture/GestureOverlayView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@2
    return v0
.end method

.method static synthetic access$1002(Landroid/gesture/GestureOverlayView;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@2
    return p1
.end method

.method static synthetic access$102(Landroid/gesture/GestureOverlayView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Landroid/gesture/GestureOverlayView;)Landroid/view/animation/AccelerateDecelerateInterpolator;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Landroid/gesture/GestureOverlayView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mResetGesture:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/gesture/GestureOverlayView;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-wide v0, p0, Landroid/gesture/GestureOverlayView;->mFadingStart:J

    #@2
    return-wide v0
.end method

.method static synthetic access$300(Landroid/gesture/GestureOverlayView;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-wide v0, p0, Landroid/gesture/GestureOverlayView;->mFadeDuration:J

    #@2
    return-wide v0
.end method

.method static synthetic access$400(Landroid/gesture/GestureOverlayView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Landroid/gesture/GestureOverlayView;->fireOnGesturePerformed()V

    #@3
    return-void
.end method

.method static synthetic access$502(Landroid/gesture/GestureOverlayView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mPreviousWasGesturing:Z

    #@2
    return p1
.end method

.method static synthetic access$602(Landroid/gesture/GestureOverlayView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Landroid/gesture/GestureOverlayView;)Landroid/graphics/Path;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Landroid/gesture/GestureOverlayView;Landroid/gesture/Gesture;)Landroid/gesture/Gesture;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-object p1, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Landroid/gesture/GestureOverlayView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@3
    return-void
.end method

.method private cancelGesture(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 715
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@2
    .line 716
    .local v2, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 717
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 718
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/gesture/GestureOverlayView$OnGestureListener;

    #@f
    invoke-interface {v3, p0, p1}, Landroid/gesture/GestureOverlayView$OnGestureListener;->onGestureCancelled(Landroid/gesture/GestureOverlayView;Landroid/view/MotionEvent;)V

    #@12
    .line 717
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_7

    #@15
    .line 721
    :cond_15
    const/4 v3, 0x0

    #@16
    invoke-virtual {p0, v3}, Landroid/gesture/GestureOverlayView;->clear(Z)V

    #@19
    .line 722
    return-void
.end method

.method private clear(ZZZ)V
    .registers 9
    .parameter "animated"
    .parameter "fireActionPerformed"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/high16 v3, 0x3f80

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v1, 0x0

    #@5
    .line 412
    const/16 v0, 0xff

    #@7
    invoke-direct {p0, v0}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@a
    .line 413
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@c
    invoke-virtual {p0, v0}, Landroid/gesture/GestureOverlayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@f
    .line 414
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mResetGesture:Z

    #@11
    .line 415
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@13
    iput-boolean p2, v0, Landroid/gesture/GestureOverlayView$FadeOutRunnable;->fireActionPerformed:Z

    #@15
    .line 416
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@17
    iput-boolean v1, v0, Landroid/gesture/GestureOverlayView$FadeOutRunnable;->resetMultipleStrokes:Z

    #@19
    .line 418
    if-eqz p1, :cond_36

    #@1b
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@1d
    if-eqz v0, :cond_36

    #@1f
    .line 419
    iput v3, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@21
    .line 420
    iput-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@23
    .line 421
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@25
    .line 422
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@28
    move-result-wide v0

    #@29
    iget-wide v2, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@2b
    add-long/2addr v0, v2

    #@2c
    iput-wide v0, p0, Landroid/gesture/GestureOverlayView;->mFadingStart:J

    #@2e
    .line 424
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@30
    iget-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@32
    invoke-virtual {p0, v0, v1, v2}, Landroid/gesture/GestureOverlayView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@35
    .line 445
    :goto_35
    return-void

    #@36
    .line 426
    :cond_36
    iput v3, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@38
    .line 427
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@3a
    .line 428
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@3c
    .line 430
    if-eqz p3, :cond_49

    #@3e
    .line 431
    iput-object v4, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@40
    .line 432
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@42
    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    #@45
    .line 433
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@48
    goto :goto_35

    #@49
    .line 434
    :cond_49
    if-eqz p2, :cond_53

    #@4b
    .line 435
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@4d
    iget-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@4f
    invoke-virtual {p0, v0, v1, v2}, Landroid/gesture/GestureOverlayView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@52
    goto :goto_35

    #@53
    .line 436
    :cond_53
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@55
    if-ne v0, v2, :cond_63

    #@57
    .line 437
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@59
    iput-boolean v2, v0, Landroid/gesture/GestureOverlayView$FadeOutRunnable;->resetMultipleStrokes:Z

    #@5b
    .line 438
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@5d
    iget-wide v1, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@5f
    invoke-virtual {p0, v0, v1, v2}, Landroid/gesture/GestureOverlayView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@62
    goto :goto_35

    #@63
    .line 440
    :cond_63
    iput-object v4, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@65
    .line 441
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@67
    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    #@6a
    .line 442
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@6d
    goto :goto_35
.end method

.method private fireOnGesturePerformed()V
    .registers 6

    #@0
    .prologue
    .line 725
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@2
    .line 726
    .local v0, actionListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 727
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_17

    #@9
    .line 728
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;

    #@f
    iget-object v4, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@11
    invoke-interface {v3, p0, v4}, Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;->onGesturePerformed(Landroid/gesture/GestureOverlayView;Landroid/gesture/Gesture;)V

    #@14
    .line 727
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_7

    #@17
    .line 730
    :cond_17
    return-void
.end method

.method private init()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 175
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v1}, Landroid/gesture/GestureOverlayView;->setWillNotDraw(Z)V

    #@5
    .line 177
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@7
    .line 178
    .local v0, gesturePaint:Landroid/graphics/Paint;
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@a
    .line 179
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@c
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@f
    .line 180
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@11
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@14
    .line 181
    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    #@16
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    #@19
    .line 182
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    #@1b
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    #@1e
    .line 183
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@20
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@23
    .line 184
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    #@26
    .line 186
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@28
    iput v1, p0, Landroid/gesture/GestureOverlayView;->mCurrentColor:I

    #@2a
    .line 187
    const/16 v1, 0xff

    #@2c
    invoke-direct {p0, v1}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@2f
    .line 188
    return-void
.end method

.method private processEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 514
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@5
    move-result v3

    #@6
    packed-switch v3, :pswitch_data_36

    #@9
    :cond_9
    move v1, v2

    #@a
    .line 543
    :cond_a
    :goto_a
    return v1

    #@b
    .line 516
    :pswitch_b
    invoke-direct {p0, p1}, Landroid/gesture/GestureOverlayView;->touchDown(Landroid/view/MotionEvent;)V

    #@e
    .line 517
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@11
    goto :goto_a

    #@12
    .line 520
    :pswitch_12
    iget-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mIsListeningForGestures:Z

    #@14
    if-eqz v3, :cond_9

    #@16
    .line 521
    invoke-direct {p0, p1}, Landroid/gesture/GestureOverlayView;->touchMove(Landroid/view/MotionEvent;)Landroid/graphics/Rect;

    #@19
    move-result-object v0

    #@1a
    .line 522
    .local v0, rect:Landroid/graphics/Rect;
    if-eqz v0, :cond_a

    #@1c
    .line 523
    invoke-virtual {p0, v0}, Landroid/gesture/GestureOverlayView;->invalidate(Landroid/graphics/Rect;)V

    #@1f
    goto :goto_a

    #@20
    .line 529
    .end local v0           #rect:Landroid/graphics/Rect;
    :pswitch_20
    iget-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mIsListeningForGestures:Z

    #@22
    if-eqz v3, :cond_9

    #@24
    .line 530
    invoke-direct {p0, p1, v2}, Landroid/gesture/GestureOverlayView;->touchUp(Landroid/view/MotionEvent;Z)V

    #@27
    .line 531
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@2a
    goto :goto_a

    #@2b
    .line 536
    :pswitch_2b
    iget-boolean v3, p0, Landroid/gesture/GestureOverlayView;->mIsListeningForGestures:Z

    #@2d
    if-eqz v3, :cond_9

    #@2f
    .line 537
    invoke-direct {p0, p1, v1}, Landroid/gesture/GestureOverlayView;->touchUp(Landroid/view/MotionEvent;Z)V

    #@32
    .line 538
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@35
    goto :goto_a

    #@36
    .line 514
    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_b
        :pswitch_20
        :pswitch_12
        :pswitch_2b
    .end packed-switch
.end method

.method private setCurrentColor(I)V
    .registers 4
    .parameter "color"

    #@0
    .prologue
    .line 375
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mCurrentColor:I

    #@2
    .line 376
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@4
    if-eqz v0, :cond_13

    #@6
    .line 377
    const/high16 v0, 0x437f

    #@8
    iget v1, p0, Landroid/gesture/GestureOverlayView;->mFadingAlpha:F

    #@a
    mul-float/2addr v0, v1

    #@b
    float-to-int v0, v0

    #@c
    invoke-direct {p0, v0}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@f
    .line 381
    :goto_f
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@12
    .line 382
    return-void

    #@13
    .line 379
    :cond_13
    const/16 v0, 0xff

    #@15
    invoke-direct {p0, v0}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@18
    goto :goto_f
.end method

.method private setPaintAlpha(I)V
    .registers 7
    .parameter "alpha"

    #@0
    .prologue
    .line 401
    shr-int/lit8 v2, p1, 0x7

    #@2
    add-int/2addr p1, v2

    #@3
    .line 402
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mCurrentColor:I

    #@5
    ushr-int/lit8 v0, v2, 0x18

    #@7
    .line 403
    .local v0, baseAlpha:I
    mul-int v2, v0, p1

    #@9
    shr-int/lit8 v1, v2, 0x8

    #@b
    .line 404
    .local v1, useAlpha:I
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@d
    iget v3, p0, Landroid/gesture/GestureOverlayView;->mCurrentColor:I

    #@f
    shl-int/lit8 v3, v3, 0x8

    #@11
    ushr-int/lit8 v3, v3, 0x8

    #@13
    shl-int/lit8 v4, v1, 0x18

    #@15
    or-int/2addr v3, v4

    #@16
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    #@19
    .line 405
    return-void
.end method

.method private touchDown(Landroid/view/MotionEvent;)V
    .registers 13
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 547
    const/4 v6, 0x1

    #@2
    iput-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mIsListeningForGestures:Z

    #@4
    .line 549
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@7
    move-result v4

    #@8
    .line 550
    .local v4, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@b
    move-result v5

    #@c
    .line 552
    .local v5, y:F
    iput v4, p0, Landroid/gesture/GestureOverlayView;->mX:F

    #@e
    .line 553
    iput v5, p0, Landroid/gesture/GestureOverlayView;->mY:F

    #@10
    .line 555
    const/4 v6, 0x0

    #@11
    iput v6, p0, Landroid/gesture/GestureOverlayView;->mTotalLength:F

    #@13
    .line 556
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@15
    .line 558
    iget v6, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@17
    if-eqz v6, :cond_1d

    #@19
    iget-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mResetGesture:Z

    #@1b
    if-eqz v6, :cond_7d

    #@1d
    .line 559
    :cond_1d
    iget-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@1f
    if-eqz v6, :cond_26

    #@21
    iget v6, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@23
    invoke-direct {p0, v6}, Landroid/gesture/GestureOverlayView;->setCurrentColor(I)V

    #@26
    .line 560
    :cond_26
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mResetGesture:Z

    #@28
    .line 561
    const/4 v6, 0x0

    #@29
    iput-object v6, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@2b
    .line 562
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@2d
    invoke-virtual {v6}, Landroid/graphics/Path;->rewind()V

    #@30
    .line 568
    :cond_30
    :goto_30
    iget-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@32
    if-eqz v6, :cond_93

    #@34
    .line 569
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->cancelClearAnimation()V

    #@37
    .line 577
    :cond_37
    :goto_37
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@39
    if-nez v6, :cond_42

    #@3b
    .line 578
    new-instance v6, Landroid/gesture/Gesture;

    #@3d
    invoke-direct {v6}, Landroid/gesture/Gesture;-><init>()V

    #@40
    iput-object v6, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@42
    .line 581
    :cond_42
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@44
    new-instance v7, Landroid/gesture/GesturePoint;

    #@46
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@49
    move-result-wide v8

    #@4a
    invoke-direct {v7, v4, v5, v8, v9}, Landroid/gesture/GesturePoint;-><init>(FFJ)V

    #@4d
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@50
    .line 582
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@52
    invoke-virtual {v6, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    #@55
    .line 584
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mInvalidateExtraBorder:I

    #@57
    .line 585
    .local v0, border:I
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mInvalidRect:Landroid/graphics/Rect;

    #@59
    float-to-int v7, v4

    #@5a
    sub-int/2addr v7, v0

    #@5b
    float-to-int v8, v5

    #@5c
    sub-int/2addr v8, v0

    #@5d
    float-to-int v9, v4

    #@5e
    add-int/2addr v9, v0

    #@5f
    float-to-int v10, v5

    #@60
    add-int/2addr v10, v0

    #@61
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    #@64
    .line 587
    iput v4, p0, Landroid/gesture/GestureOverlayView;->mCurveEndX:F

    #@66
    .line 588
    iput v5, p0, Landroid/gesture/GestureOverlayView;->mCurveEndY:F

    #@68
    .line 591
    iget-object v3, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@6a
    .line 592
    .local v3, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6d
    move-result v1

    #@6e
    .line 593
    .local v1, count:I
    const/4 v2, 0x0

    #@6f
    .local v2, i:I
    :goto_6f
    if-ge v2, v1, :cond_a6

    #@71
    .line 594
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@74
    move-result-object v6

    #@75
    check-cast v6, Landroid/gesture/GestureOverlayView$OnGestureListener;

    #@77
    invoke-interface {v6, p0, p1}, Landroid/gesture/GestureOverlayView$OnGestureListener;->onGestureStarted(Landroid/gesture/GestureOverlayView;Landroid/view/MotionEvent;)V

    #@7a
    .line 593
    add-int/lit8 v2, v2, 0x1

    #@7c
    goto :goto_6f

    #@7d
    .line 563
    .end local v0           #border:I
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    :cond_7d
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@7f
    if-eqz v6, :cond_89

    #@81
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@83
    invoke-virtual {v6}, Landroid/gesture/Gesture;->getStrokesCount()I

    #@86
    move-result v6

    #@87
    if-nez v6, :cond_30

    #@89
    .line 564
    :cond_89
    iget-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@8b
    if-eqz v6, :cond_30

    #@8d
    iget v6, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@8f
    invoke-direct {p0, v6}, Landroid/gesture/GestureOverlayView;->setCurrentColor(I)V

    #@92
    goto :goto_30

    #@93
    .line 570
    :cond_93
    iget-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@95
    if-eqz v6, :cond_37

    #@97
    .line 571
    const/16 v6, 0xff

    #@99
    invoke-direct {p0, v6}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@9c
    .line 572
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@9e
    .line 573
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@a0
    .line 574
    iget-object v6, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@a2
    invoke-virtual {p0, v6}, Landroid/gesture/GestureOverlayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@a5
    goto :goto_37

    #@a6
    .line 596
    .restart local v0       #border:I
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v3       #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    :cond_a6
    return-void
.end method

.method private touchMove(Landroid/view/MotionEvent;)Landroid/graphics/Rect;
    .registers 27
    .parameter "event"

    #@0
    .prologue
    .line 599
    const/4 v6, 0x0

    #@1
    .line 601
    .local v6, areaToRefresh:Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@4
    move-result v19

    #@5
    .line 602
    .local v19, x:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@8
    move-result v20

    #@9
    .line 604
    .local v20, y:F
    move-object/from16 v0, p0

    #@b
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mX:F

    #@d
    move/from16 v17, v0

    #@f
    .line 605
    .local v17, previousX:F
    move-object/from16 v0, p0

    #@11
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mY:F

    #@13
    move/from16 v18, v0

    #@15
    .line 607
    .local v18, previousY:F
    sub-float v21, v19, v17

    #@17
    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    #@1a
    move-result v12

    #@1b
    .line 608
    .local v12, dx:F
    sub-float v21, v20, v18

    #@1d
    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    #@20
    move-result v13

    #@21
    .line 610
    .local v13, dy:F
    const/high16 v21, 0x4040

    #@23
    cmpl-float v21, v12, v21

    #@25
    if-gez v21, :cond_2d

    #@27
    const/high16 v21, 0x4040

    #@29
    cmpl-float v21, v13, v21

    #@2b
    if-ltz v21, :cond_1e2

    #@2d
    .line 611
    :cond_2d
    move-object/from16 v0, p0

    #@2f
    iget-object v6, v0, Landroid/gesture/GestureOverlayView;->mInvalidRect:Landroid/graphics/Rect;

    #@31
    .line 614
    move-object/from16 v0, p0

    #@33
    iget v7, v0, Landroid/gesture/GestureOverlayView;->mInvalidateExtraBorder:I

    #@35
    .line 615
    .local v7, border:I
    move-object/from16 v0, p0

    #@37
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mCurveEndX:F

    #@39
    move/from16 v21, v0

    #@3b
    move/from16 v0, v21

    #@3d
    float-to-int v0, v0

    #@3e
    move/from16 v21, v0

    #@40
    sub-int v21, v21, v7

    #@42
    move-object/from16 v0, p0

    #@44
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mCurveEndY:F

    #@46
    move/from16 v22, v0

    #@48
    move/from16 v0, v22

    #@4a
    float-to-int v0, v0

    #@4b
    move/from16 v22, v0

    #@4d
    sub-int v22, v22, v7

    #@4f
    move-object/from16 v0, p0

    #@51
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mCurveEndX:F

    #@53
    move/from16 v23, v0

    #@55
    move/from16 v0, v23

    #@57
    float-to-int v0, v0

    #@58
    move/from16 v23, v0

    #@5a
    add-int v23, v23, v7

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mCurveEndY:F

    #@60
    move/from16 v24, v0

    #@62
    move/from16 v0, v24

    #@64
    float-to-int v0, v0

    #@65
    move/from16 v24, v0

    #@67
    add-int v24, v24, v7

    #@69
    move/from16 v0, v21

    #@6b
    move/from16 v1, v22

    #@6d
    move/from16 v2, v23

    #@6f
    move/from16 v3, v24

    #@71
    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@74
    .line 618
    add-float v21, v19, v17

    #@76
    const/high16 v22, 0x4000

    #@78
    div-float v9, v21, v22

    #@7a
    move-object/from16 v0, p0

    #@7c
    iput v9, v0, Landroid/gesture/GestureOverlayView;->mCurveEndX:F

    #@7e
    .line 619
    .local v9, cX:F
    add-float v21, v20, v18

    #@80
    const/high16 v22, 0x4000

    #@82
    div-float v10, v21, v22

    #@84
    move-object/from16 v0, p0

    #@86
    iput v10, v0, Landroid/gesture/GestureOverlayView;->mCurveEndY:F

    #@88
    .line 621
    .local v10, cY:F
    move-object/from16 v0, p0

    #@8a
    iget-object v0, v0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@8c
    move-object/from16 v21, v0

    #@8e
    move-object/from16 v0, v21

    #@90
    move/from16 v1, v17

    #@92
    move/from16 v2, v18

    #@94
    invoke-virtual {v0, v1, v2, v9, v10}, Landroid/graphics/Path;->quadTo(FFFF)V

    #@97
    .line 624
    move/from16 v0, v17

    #@99
    float-to-int v0, v0

    #@9a
    move/from16 v21, v0

    #@9c
    sub-int v21, v21, v7

    #@9e
    move/from16 v0, v18

    #@a0
    float-to-int v0, v0

    #@a1
    move/from16 v22, v0

    #@a3
    sub-int v22, v22, v7

    #@a5
    move/from16 v0, v17

    #@a7
    float-to-int v0, v0

    #@a8
    move/from16 v23, v0

    #@aa
    add-int v23, v23, v7

    #@ac
    move/from16 v0, v18

    #@ae
    float-to-int v0, v0

    #@af
    move/from16 v24, v0

    #@b1
    add-int v24, v24, v7

    #@b3
    move/from16 v0, v21

    #@b5
    move/from16 v1, v22

    #@b7
    move/from16 v2, v23

    #@b9
    move/from16 v3, v24

    #@bb
    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;->union(IIII)V

    #@be
    .line 628
    float-to-int v0, v9

    #@bf
    move/from16 v21, v0

    #@c1
    sub-int v21, v21, v7

    #@c3
    float-to-int v0, v10

    #@c4
    move/from16 v22, v0

    #@c6
    sub-int v22, v22, v7

    #@c8
    float-to-int v0, v9

    #@c9
    move/from16 v23, v0

    #@cb
    add-int v23, v23, v7

    #@cd
    float-to-int v0, v10

    #@ce
    move/from16 v24, v0

    #@d0
    add-int v24, v24, v7

    #@d2
    move/from16 v0, v21

    #@d4
    move/from16 v1, v22

    #@d6
    move/from16 v2, v23

    #@d8
    move/from16 v3, v24

    #@da
    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;->union(IIII)V

    #@dd
    .line 631
    move/from16 v0, v19

    #@df
    move-object/from16 v1, p0

    #@e1
    iput v0, v1, Landroid/gesture/GestureOverlayView;->mX:F

    #@e3
    .line 632
    move/from16 v0, v20

    #@e5
    move-object/from16 v1, p0

    #@e7
    iput v0, v1, Landroid/gesture/GestureOverlayView;->mY:F

    #@e9
    .line 634
    move-object/from16 v0, p0

    #@eb
    iget-object v0, v0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@ed
    move-object/from16 v21, v0

    #@ef
    new-instance v22, Landroid/gesture/GesturePoint;

    #@f1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@f4
    move-result-wide v23

    #@f5
    move-object/from16 v0, v22

    #@f7
    move/from16 v1, v19

    #@f9
    move/from16 v2, v20

    #@fb
    move-wide/from16 v3, v23

    #@fd
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/gesture/GesturePoint;-><init>(FFJ)V

    #@100
    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@103
    .line 636
    move-object/from16 v0, p0

    #@105
    iget-boolean v0, v0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@107
    move/from16 v21, v0

    #@109
    if-eqz v21, :cond_1c5

    #@10b
    move-object/from16 v0, p0

    #@10d
    iget-boolean v0, v0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@10f
    move/from16 v21, v0

    #@111
    if-nez v21, :cond_1c5

    #@113
    .line 637
    move-object/from16 v0, p0

    #@115
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mTotalLength:F

    #@117
    move/from16 v21, v0

    #@119
    mul-float v22, v12, v12

    #@11b
    mul-float v23, v13, v13

    #@11d
    add-float v22, v22, v23

    #@11f
    move/from16 v0, v22

    #@121
    float-to-double v0, v0

    #@122
    move-wide/from16 v22, v0

    #@124
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    #@127
    move-result-wide v22

    #@128
    move-wide/from16 v0, v22

    #@12a
    double-to-float v0, v0

    #@12b
    move/from16 v22, v0

    #@12d
    add-float v21, v21, v22

    #@12f
    move/from16 v0, v21

    #@131
    move-object/from16 v1, p0

    #@133
    iput v0, v1, Landroid/gesture/GestureOverlayView;->mTotalLength:F

    #@135
    .line 639
    move-object/from16 v0, p0

    #@137
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mTotalLength:F

    #@139
    move/from16 v21, v0

    #@13b
    move-object/from16 v0, p0

    #@13d
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@13f
    move/from16 v22, v0

    #@141
    cmpl-float v21, v21, v22

    #@143
    if-lez v21, :cond_1c5

    #@145
    .line 640
    move-object/from16 v0, p0

    #@147
    iget-object v0, v0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@149
    move-object/from16 v21, v0

    #@14b
    invoke-static/range {v21 .. v21}, Landroid/gesture/GestureUtils;->computeOrientedBoundingBox(Ljava/util/ArrayList;)Landroid/gesture/OrientedBoundingBox;

    #@14e
    move-result-object v8

    #@14f
    .line 643
    .local v8, box:Landroid/gesture/OrientedBoundingBox;
    iget v0, v8, Landroid/gesture/OrientedBoundingBox;->orientation:F

    #@151
    move/from16 v21, v0

    #@153
    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    #@156
    move-result v5

    #@157
    .line 644
    .local v5, angle:F
    const/high16 v21, 0x42b4

    #@159
    cmpl-float v21, v5, v21

    #@15b
    if-lez v21, :cond_161

    #@15d
    .line 645
    const/high16 v21, 0x4334

    #@15f
    sub-float v5, v21, v5

    #@161
    .line 648
    :cond_161
    iget v0, v8, Landroid/gesture/OrientedBoundingBox;->squareness:F

    #@163
    move/from16 v21, v0

    #@165
    move-object/from16 v0, p0

    #@167
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@169
    move/from16 v22, v0

    #@16b
    cmpl-float v21, v21, v22

    #@16d
    if-gtz v21, :cond_187

    #@16f
    move-object/from16 v0, p0

    #@171
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@173
    move/from16 v21, v0

    #@175
    const/16 v22, 0x1

    #@177
    move/from16 v0, v21

    #@179
    move/from16 v1, v22

    #@17b
    if-ne v0, v1, :cond_1bb

    #@17d
    move-object/from16 v0, p0

    #@17f
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@181
    move/from16 v21, v0

    #@183
    cmpg-float v21, v5, v21

    #@185
    if-gez v21, :cond_1c5

    #@187
    .line 653
    :cond_187
    const/16 v21, 0x1

    #@189
    move/from16 v0, v21

    #@18b
    move-object/from16 v1, p0

    #@18d
    iput-boolean v0, v1, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@18f
    .line 654
    move-object/from16 v0, p0

    #@191
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@193
    move/from16 v21, v0

    #@195
    move-object/from16 v0, p0

    #@197
    move/from16 v1, v21

    #@199
    invoke-direct {v0, v1}, Landroid/gesture/GestureOverlayView;->setCurrentColor(I)V

    #@19c
    .line 656
    move-object/from16 v0, p0

    #@19e
    iget-object v0, v0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@1a0
    move-object/from16 v16, v0

    #@1a2
    .line 657
    .local v16, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturingListener;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@1a5
    move-result v11

    #@1a6
    .line 658
    .local v11, count:I
    const/4 v14, 0x0

    #@1a7
    .local v14, i:I
    :goto_1a7
    if-ge v14, v11, :cond_1c5

    #@1a9
    .line 659
    move-object/from16 v0, v16

    #@1ab
    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1ae
    move-result-object v21

    #@1af
    check-cast v21, Landroid/gesture/GestureOverlayView$OnGesturingListener;

    #@1b1
    move-object/from16 v0, v21

    #@1b3
    move-object/from16 v1, p0

    #@1b5
    invoke-interface {v0, v1}, Landroid/gesture/GestureOverlayView$OnGesturingListener;->onGesturingStarted(Landroid/gesture/GestureOverlayView;)V

    #@1b8
    .line 658
    add-int/lit8 v14, v14, 0x1

    #@1ba
    goto :goto_1a7

    #@1bb
    .line 648
    .end local v11           #count:I
    .end local v14           #i:I
    .end local v16           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturingListener;>;"
    :cond_1bb
    move-object/from16 v0, p0

    #@1bd
    iget v0, v0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@1bf
    move/from16 v21, v0

    #@1c1
    cmpl-float v21, v5, v21

    #@1c3
    if-gtz v21, :cond_187

    #@1c5
    .line 666
    .end local v5           #angle:F
    .end local v8           #box:Landroid/gesture/OrientedBoundingBox;
    :cond_1c5
    move-object/from16 v0, p0

    #@1c7
    iget-object v15, v0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@1c9
    .line 667
    .local v15, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@1cc
    move-result v11

    #@1cd
    .line 668
    .restart local v11       #count:I
    const/4 v14, 0x0

    #@1ce
    .restart local v14       #i:I
    :goto_1ce
    if-ge v14, v11, :cond_1e2

    #@1d0
    .line 669
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d3
    move-result-object v21

    #@1d4
    check-cast v21, Landroid/gesture/GestureOverlayView$OnGestureListener;

    #@1d6
    move-object/from16 v0, v21

    #@1d8
    move-object/from16 v1, p0

    #@1da
    move-object/from16 v2, p1

    #@1dc
    invoke-interface {v0, v1, v2}, Landroid/gesture/GestureOverlayView$OnGestureListener;->onGesture(Landroid/gesture/GestureOverlayView;Landroid/view/MotionEvent;)V

    #@1df
    .line 668
    add-int/lit8 v14, v14, 0x1

    #@1e1
    goto :goto_1ce

    #@1e2
    .line 673
    .end local v7           #border:I
    .end local v9           #cX:F
    .end local v10           #cY:F
    .end local v11           #count:I
    .end local v14           #i:I
    .end local v15           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    :cond_1e2
    return-object v6
.end method

.method private touchUp(Landroid/view/MotionEvent;Z)V
    .registers 12
    .parameter "event"
    .parameter "cancel"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 677
    iput-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mIsListeningForGestures:Z

    #@4
    .line 680
    iget-object v4, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@6
    if-eqz v4, :cond_67

    #@8
    .line 682
    iget-object v4, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@a
    new-instance v7, Landroid/gesture/GestureStroke;

    #@c
    iget-object v8, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@e
    invoke-direct {v7, v8}, Landroid/gesture/GestureStroke;-><init>(Ljava/util/ArrayList;)V

    #@11
    invoke-virtual {v4, v7}, Landroid/gesture/Gesture;->addStroke(Landroid/gesture/GestureStroke;)V

    #@14
    .line 684
    if-nez p2, :cond_63

    #@16
    .line 686
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@18
    .line 687
    .local v2, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v0

    #@1c
    .line 688
    .local v0, count:I
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v0, :cond_2b

    #@1f
    .line 689
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v4

    #@23
    check-cast v4, Landroid/gesture/GestureOverlayView$OnGestureListener;

    #@25
    invoke-interface {v4, p0, p1}, Landroid/gesture/GestureOverlayView$OnGestureListener;->onGestureEnded(Landroid/gesture/GestureOverlayView;Landroid/view/MotionEvent;)V

    #@28
    .line 688
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_1d

    #@2b
    .line 692
    :cond_2b
    iget-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@2d
    if-eqz v4, :cond_5f

    #@2f
    iget-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@31
    if-eqz v4, :cond_5f

    #@33
    move v4, v5

    #@34
    :goto_34
    iget-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@36
    if-eqz v7, :cond_61

    #@38
    iget-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@3a
    if-eqz v7, :cond_61

    #@3c
    :goto_3c
    invoke-direct {p0, v4, v5, v6}, Landroid/gesture/GestureOverlayView;->clear(ZZZ)V

    #@3f
    .line 702
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    :goto_3f
    iget-object v4, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@44
    .line 703
    iget-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@46
    iput-boolean v4, p0, Landroid/gesture/GestureOverlayView;->mPreviousWasGesturing:Z

    #@48
    .line 704
    iput-boolean v6, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@4a
    .line 706
    iget-object v3, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@4c
    .line 707
    .local v3, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturingListener;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@4f
    move-result v0

    #@50
    .line 708
    .restart local v0       #count:I
    const/4 v1, 0x0

    #@51
    .restart local v1       #i:I
    :goto_51
    if-ge v1, v0, :cond_6b

    #@53
    .line 709
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@56
    move-result-object v4

    #@57
    check-cast v4, Landroid/gesture/GestureOverlayView$OnGesturingListener;

    #@59
    invoke-interface {v4, p0}, Landroid/gesture/GestureOverlayView$OnGesturingListener;->onGesturingEnded(Landroid/gesture/GestureOverlayView;)V

    #@5c
    .line 708
    add-int/lit8 v1, v1, 0x1

    #@5e
    goto :goto_51

    #@5f
    .end local v3           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturingListener;>;"
    .restart local v2       #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    :cond_5f
    move v4, v6

    #@60
    .line 692
    goto :goto_34

    #@61
    :cond_61
    move v5, v6

    #@62
    goto :goto_3c

    #@63
    .line 695
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    :cond_63
    invoke-direct {p0, p1}, Landroid/gesture/GestureOverlayView;->cancelGesture(Landroid/view/MotionEvent;)V

    #@66
    goto :goto_3f

    #@67
    .line 699
    :cond_67
    invoke-direct {p0, p1}, Landroid/gesture/GestureOverlayView;->cancelGesture(Landroid/view/MotionEvent;)V

    #@6a
    goto :goto_3f

    #@6b
    .line 711
    .restart local v0       #count:I
    .restart local v1       #i:I
    .restart local v3       #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturingListener;>;"
    :cond_6b
    return-void
.end method


# virtual methods
.method public addOnGestureListener(Landroid/gesture/GestureOverlayView$OnGestureListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 329
    return-void
.end method

.method public addOnGesturePerformedListener(Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 340
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 341
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    if-lez v0, :cond_10

    #@d
    .line 342
    const/4 v0, 0x1

    #@e
    iput-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@10
    .line 344
    :cond_10
    return-void
.end method

.method public addOnGesturingListener(Landroid/gesture/GestureOverlayView$OnGesturingListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 359
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 360
    return-void
.end method

.method public cancelClearAnimation()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 448
    const/16 v0, 0xff

    #@3
    invoke-direct {p0, v0}, Landroid/gesture/GestureOverlayView;->setPaintAlpha(I)V

    #@6
    .line 449
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mIsFadingOut:Z

    #@8
    .line 450
    iput-boolean v1, p0, Landroid/gesture/GestureOverlayView;->mFadingHasStarted:Z

    #@a
    .line 451
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mFadingOut:Landroid/gesture/GestureOverlayView$FadeOutRunnable;

    #@c
    invoke-virtual {p0, v0}, Landroid/gesture/GestureOverlayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@f
    .line 452
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@11
    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    #@14
    .line 453
    const/4 v0, 0x0

    #@15
    iput-object v0, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@17
    .line 454
    return-void
.end method

.method public cancelGesture()V
    .registers 14

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 457
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mIsListeningForGestures:Z

    #@4
    .line 460
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@6
    new-instance v3, Landroid/gesture/GestureStroke;

    #@8
    iget-object v4, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@a
    invoke-direct {v3, v4}, Landroid/gesture/GestureStroke;-><init>(Ljava/util/ArrayList;)V

    #@d
    invoke-virtual {v2, v3}, Landroid/gesture/Gesture;->addStroke(Landroid/gesture/GestureStroke;)V

    #@10
    .line 463
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@13
    move-result-wide v0

    #@14
    .line 464
    .local v0, now:J
    const/4 v4, 0x3

    #@15
    move-wide v2, v0

    #@16
    move v6, v5

    #@17
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@1a
    move-result-object v9

    #@1b
    .line 467
    .local v9, event:Landroid/view/MotionEvent;
    iget-object v11, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@1d
    .line 468
    .local v11, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGestureListener;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@20
    move-result v8

    #@21
    .line 469
    .local v8, count:I
    const/4 v10, 0x0

    #@22
    .local v10, i:I
    :goto_22
    if-ge v10, v8, :cond_30

    #@24
    .line 470
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Landroid/gesture/GestureOverlayView$OnGestureListener;

    #@2a
    invoke-interface {v2, p0, v9}, Landroid/gesture/GestureOverlayView$OnGestureListener;->onGestureCancelled(Landroid/gesture/GestureOverlayView;Landroid/view/MotionEvent;)V

    #@2d
    .line 469
    add-int/lit8 v10, v10, 0x1

    #@2f
    goto :goto_22

    #@30
    .line 473
    :cond_30
    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    #@33
    .line 475
    invoke-virtual {p0, v7}, Landroid/gesture/GestureOverlayView;->clear(Z)V

    #@36
    .line 476
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@38
    .line 477
    iput-boolean v7, p0, Landroid/gesture/GestureOverlayView;->mPreviousWasGesturing:Z

    #@3a
    .line 478
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@3f
    .line 480
    iget-object v12, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@41
    .line 481
    .local v12, otherListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/gesture/GestureOverlayView$OnGesturingListener;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@44
    move-result v8

    #@45
    .line 482
    const/4 v10, 0x0

    #@46
    :goto_46
    if-ge v10, v8, :cond_54

    #@48
    .line 483
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4b
    move-result-object v2

    #@4c
    check-cast v2, Landroid/gesture/GestureOverlayView$OnGesturingListener;

    #@4e
    invoke-interface {v2, p0}, Landroid/gesture/GestureOverlayView$OnGesturingListener;->onGesturingEnded(Landroid/gesture/GestureOverlayView;)V

    #@51
    .line 482
    add-int/lit8 v10, v10, 0x1

    #@53
    goto :goto_46

    #@54
    .line 485
    :cond_54
    return-void
.end method

.method public clear(Z)V
    .registers 4
    .parameter "animated"

    #@0
    .prologue
    .line 408
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, v0, v1}, Landroid/gesture/GestureOverlayView;->clear(ZZZ)V

    #@5
    .line 409
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 494
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->isEnabled()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_2f

    #@7
    .line 495
    iget-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@9
    if-nez v2, :cond_1b

    #@b
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@d
    if-eqz v2, :cond_2d

    #@f
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@11
    invoke-virtual {v2}, Landroid/gesture/Gesture;->getStrokesCount()I

    #@14
    move-result v2

    #@15
    if-lez v2, :cond_2d

    #@17
    iget-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mPreviousWasGesturing:Z

    #@19
    if-eqz v2, :cond_2d

    #@1b
    :cond_1b
    iget-boolean v2, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@1d
    if-eqz v2, :cond_2d

    #@1f
    move v0, v1

    #@20
    .line 499
    .local v0, cancelDispatch:Z
    :goto_20
    invoke-direct {p0, p1}, Landroid/gesture/GestureOverlayView;->processEvent(Landroid/view/MotionEvent;)Z

    #@23
    .line 501
    if-eqz v0, :cond_29

    #@25
    .line 502
    const/4 v2, 0x3

    #@26
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    #@29
    .line 505
    :cond_29
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@2c
    .line 510
    .end local v0           #cancelDispatch:Z
    :goto_2c
    return v1

    #@2d
    .line 495
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_20

    #@2f
    .line 510
    :cond_2f
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@32
    move-result v1

    #@33
    goto :goto_2c
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    #@3
    .line 395
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@5
    if-eqz v0, :cond_12

    #@7
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mGestureVisible:Z

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 396
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@d
    iget-object v1, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@f
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@12
    .line 398
    :cond_12
    return-void
.end method

.method public getCurrentStroke()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/gesture/GesturePoint;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mStrokeBuffer:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getFadeOffset()J
    .registers 3

    #@0
    .prologue
    .line 320
    iget-wide v0, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@2
    return-wide v0
.end method

.method public getGesture()Landroid/gesture/Gesture;
    .registers 2

    #@0
    .prologue
    .line 277
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@2
    return-object v0
.end method

.method public getGestureColor()I
    .registers 2

    #@0
    .prologue
    .line 215
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@2
    return v0
.end method

.method public getGesturePaint()Landroid/graphics/Paint;
    .registers 2

    #@0
    .prologue
    .line 388
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@2
    return-object v0
.end method

.method public getGesturePath()Landroid/graphics/Path;
    .registers 2

    #@0
    .prologue
    .line 303
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@2
    return-object v0
.end method

.method public getGesturePath(Landroid/graphics/Path;)Landroid/graphics/Path;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 307
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    #@5
    .line 308
    return-object p1
.end method

.method public getGestureStrokeAngleThreshold()F
    .registers 2

    #@0
    .prologue
    .line 253
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@2
    return v0
.end method

.method public getGestureStrokeLengthThreshold()F
    .registers 2

    #@0
    .prologue
    .line 237
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@2
    return v0
.end method

.method public getGestureStrokeSquarenessTreshold()F
    .registers 2

    #@0
    .prologue
    .line 245
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@2
    return v0
.end method

.method public getGestureStrokeType()I
    .registers 2

    #@0
    .prologue
    .line 229
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@2
    return v0
.end method

.method public getGestureStrokeWidth()F
    .registers 2

    #@0
    .prologue
    .line 219
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@2
    return v0
.end method

.method public getOrientation()I
    .registers 2

    #@0
    .prologue
    .line 195
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@2
    return v0
.end method

.method public getUncertainGestureColor()I
    .registers 2

    #@0
    .prologue
    .line 211
    iget v0, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@2
    return v0
.end method

.method public isEventsInterceptionEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 261
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@2
    return v0
.end method

.method public isFadeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 269
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@2
    return v0
.end method

.method public isGestureVisible()Z
    .registers 2

    #@0
    .prologue
    .line 312
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mGestureVisible:Z

    #@2
    return v0
.end method

.method public isGesturing()Z
    .registers 2

    #@0
    .prologue
    .line 371
    iget-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mIsGesturing:Z

    #@2
    return v0
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 489
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->cancelClearAnimation()V

    #@3
    .line 490
    return-void
.end method

.method public removeAllOnGestureListeners()V
    .registers 2

    #@0
    .prologue
    .line 336
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 337
    return-void
.end method

.method public removeAllOnGesturePerformedListeners()V
    .registers 2

    #@0
    .prologue
    .line 354
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 355
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@8
    .line 356
    return-void
.end method

.method public removeAllOnGesturingListeners()V
    .registers 2

    #@0
    .prologue
    .line 367
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 368
    return-void
.end method

.method public removeOnGestureListener(Landroid/gesture/GestureOverlayView$OnGestureListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 332
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGestureListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 333
    return-void
.end method

.method public removeOnGesturePerformedListener(Landroid/gesture/GestureOverlayView$OnGesturePerformedListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 348
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturePerformedListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    if-gtz v0, :cond_10

    #@d
    .line 349
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Landroid/gesture/GestureOverlayView;->mHandleGestureActions:Z

    #@10
    .line 351
    :cond_10
    return-void
.end method

.method public removeOnGesturingListener(Landroid/gesture/GestureOverlayView$OnGesturingListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 363
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mOnGesturingListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 364
    return-void
.end method

.method public setEventsInterceptionEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 265
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mInterceptEvents:Z

    #@2
    .line 266
    return-void
.end method

.method public setFadeEnabled(Z)V
    .registers 2
    .parameter "fadeEnabled"

    #@0
    .prologue
    .line 273
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mFadeEnabled:Z

    #@2
    .line 274
    return-void
.end method

.method public setFadeOffset(J)V
    .registers 3
    .parameter "fadeOffset"

    #@0
    .prologue
    .line 324
    iput-wide p1, p0, Landroid/gesture/GestureOverlayView;->mFadeOffset:J

    #@2
    .line 325
    return-void
.end method

.method public setGesture(Landroid/gesture/Gesture;)V
    .registers 11
    .parameter "gesture"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/high16 v7, 0x4000

    #@3
    .line 281
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@5
    if-eqz v2, :cond_b

    #@7
    .line 282
    const/4 v2, 0x0

    #@8
    invoke-virtual {p0, v2}, Landroid/gesture/GestureOverlayView;->clear(Z)V

    #@b
    .line 285
    :cond_b
    iget v2, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@d
    invoke-direct {p0, v2}, Landroid/gesture/GestureOverlayView;->setCurrentColor(I)V

    #@10
    .line 286
    iput-object p1, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@12
    .line 288
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mCurrentGesture:Landroid/gesture/Gesture;

    #@14
    invoke-virtual {v2}, Landroid/gesture/Gesture;->toPath()Landroid/graphics/Path;

    #@17
    move-result-object v1

    #@18
    .line 289
    .local v1, path:Landroid/graphics/Path;
    new-instance v0, Landroid/graphics/RectF;

    #@1a
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@1d
    .line 290
    .local v0, bounds:Landroid/graphics/RectF;
    invoke-virtual {v1, v0, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@20
    .line 293
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@22
    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    #@25
    .line 294
    iget-object v2, p0, Landroid/gesture/GestureOverlayView;->mPath:Landroid/graphics/Path;

    #@27
    iget v3, v0, Landroid/graphics/RectF;->left:F

    #@29
    neg-float v3, v3

    #@2a
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->getWidth()I

    #@2d
    move-result v4

    #@2e
    int-to-float v4, v4

    #@2f
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    #@32
    move-result v5

    #@33
    sub-float/2addr v4, v5

    #@34
    div-float/2addr v4, v7

    #@35
    add-float/2addr v3, v4

    #@36
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@38
    neg-float v4, v4

    #@39
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->getHeight()I

    #@3c
    move-result v5

    #@3d
    int-to-float v5, v5

    #@3e
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    #@41
    move-result v6

    #@42
    sub-float/2addr v5, v6

    #@43
    div-float/2addr v5, v7

    #@44
    add-float/2addr v4, v5

    #@45
    invoke-virtual {v2, v1, v3, v4}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;FF)V

    #@48
    .line 297
    iput-boolean v8, p0, Landroid/gesture/GestureOverlayView;->mResetGesture:Z

    #@4a
    .line 299
    invoke-virtual {p0}, Landroid/gesture/GestureOverlayView;->invalidate()V

    #@4d
    .line 300
    return-void
.end method

.method public setGestureColor(I)V
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 203
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mCertainGestureColor:I

    #@2
    .line 204
    return-void
.end method

.method public setGestureStrokeAngleThreshold(F)V
    .registers 2
    .parameter "gestureStrokeAngleThreshold"

    #@0
    .prologue
    .line 257
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeAngleThreshold:F

    #@2
    .line 258
    return-void
.end method

.method public setGestureStrokeLengthThreshold(F)V
    .registers 2
    .parameter "gestureStrokeLengthThreshold"

    #@0
    .prologue
    .line 241
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeLengthThreshold:F

    #@2
    .line 242
    return-void
.end method

.method public setGestureStrokeSquarenessTreshold(F)V
    .registers 2
    .parameter "gestureStrokeSquarenessTreshold"

    #@0
    .prologue
    .line 249
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeSquarenessTreshold:F

    #@2
    .line 250
    return-void
.end method

.method public setGestureStrokeType(I)V
    .registers 2
    .parameter "gestureStrokeType"

    #@0
    .prologue
    .line 233
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeType:I

    #@2
    .line 234
    return-void
.end method

.method public setGestureStrokeWidth(F)V
    .registers 4
    .parameter "gestureStrokeWidth"

    #@0
    .prologue
    .line 223
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mGestureStrokeWidth:F

    #@2
    .line 224
    const/4 v0, 0x1

    #@3
    float-to-int v1, p1

    #@4
    add-int/lit8 v1, v1, -0x1

    #@6
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/gesture/GestureOverlayView;->mInvalidateExtraBorder:I

    #@c
    .line 225
    iget-object v0, p0, Landroid/gesture/GestureOverlayView;->mGesturePaint:Landroid/graphics/Paint;

    #@e
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@11
    .line 226
    return-void
.end method

.method public setGestureVisible(Z)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 316
    iput-boolean p1, p0, Landroid/gesture/GestureOverlayView;->mGestureVisible:Z

    #@2
    .line 317
    return-void
.end method

.method public setOrientation(I)V
    .registers 2
    .parameter "orientation"

    #@0
    .prologue
    .line 199
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mOrientation:I

    #@2
    .line 200
    return-void
.end method

.method public setUncertainGestureColor(I)V
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 207
    iput p1, p0, Landroid/gesture/GestureOverlayView;->mUncertainGestureColor:I

    #@2
    .line 208
    return-void
.end method
