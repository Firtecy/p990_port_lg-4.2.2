.class public Landroid/gesture/Prediction;
.super Ljava/lang/Object;
.source "Prediction.java"


# instance fields
.field public final name:Ljava/lang/String;

.field public score:D


# direct methods
.method constructor <init>(Ljava/lang/String;D)V
    .registers 4
    .parameter "label"
    .parameter "predictionScore"

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 25
    iput-object p1, p0, Landroid/gesture/Prediction;->name:Ljava/lang/String;

    #@5
    .line 26
    iput-wide p2, p0, Landroid/gesture/Prediction;->score:D

    #@7
    .line 27
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/gesture/Prediction;->name:Ljava/lang/String;

    #@2
    return-object v0
.end method
