.class Landroid/gesture/GestureLibraries$FileGestureLibrary;
.super Landroid/gesture/GestureLibrary;
.source "GestureLibraries.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/gesture/GestureLibraries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileGestureLibrary"
.end annotation


# instance fields
.field private final mPath:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .registers 2
    .parameter "path"

    #@0
    .prologue
    .line 54
    invoke-direct {p0}, Landroid/gesture/GestureLibrary;-><init>()V

    #@3
    .line 55
    iput-object p1, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@5
    .line 56
    return-void
.end method


# virtual methods
.method public isReadOnly()Z
    .registers 2

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public load()Z
    .registers 7

    #@0
    .prologue
    .line 91
    const/4 v2, 0x0

    #@1
    .line 92
    .local v2, result:Z
    iget-object v1, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@3
    .line 93
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_1b

    #@9
    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_1b

    #@f
    .line 95
    :try_start_f
    iget-object v3, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@11
    new-instance v4, Ljava/io/FileInputStream;

    #@13
    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@16
    const/4 v5, 0x1

    #@17
    invoke-virtual {v3, v4, v5}, Landroid/gesture/GestureStore;->load(Ljava/io/InputStream;Z)V
    :try_end_1a
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_1a} :catch_1c
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_1a} :catch_38

    #@1a
    .line 96
    const/4 v2, 0x1

    #@1b
    .line 104
    :cond_1b
    :goto_1b
    return v2

    #@1c
    .line 97
    :catch_1c
    move-exception v0

    #@1d
    .line 98
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v3, "Gestures"

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, "Could not load the gesture library from "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    iget-object v5, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@37
    goto :goto_1b

    #@38
    .line 99
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_38
    move-exception v0

    #@39
    .line 100
    .local v0, e:Ljava/io/IOException;
    const-string v3, "Gestures"

    #@3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "Could not load the gesture library from "

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    iget-object v5, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@48
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@53
    goto :goto_1b
.end method

.method public save()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 64
    iget-object v4, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@3
    invoke-virtual {v4}, Landroid/gesture/GestureStore;->hasChanged()Z

    #@6
    move-result v4

    #@7
    if-nez v4, :cond_a

    #@9
    .line 87
    :goto_9
    return v3

    #@a
    .line 66
    :cond_a
    iget-object v1, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@c
    .line 68
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@f
    move-result-object v2

    #@10
    .line 69
    .local v2, parentFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_1e

    #@16
    .line 70
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    #@19
    move-result v4

    #@1a
    if-nez v4, :cond_1e

    #@1c
    .line 71
    const/4 v3, 0x0

    #@1d
    goto :goto_9

    #@1e
    .line 75
    :cond_1e
    const/4 v3, 0x0

    #@1f
    .line 78
    .local v3, result:Z
    :try_start_1f
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    #@22
    .line 79
    iget-object v4, p0, Landroid/gesture/GestureLibrary;->mStore:Landroid/gesture/GestureStore;

    #@24
    new-instance v5, Ljava/io/FileOutputStream;

    #@26
    invoke-direct {v5, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@29
    const/4 v6, 0x1

    #@2a
    invoke-virtual {v4, v5, v6}, Landroid/gesture/GestureStore;->save(Ljava/io/OutputStream;Z)V
    :try_end_2d
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_2d} :catch_2f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2d} :catch_4b

    #@2d
    .line 80
    const/4 v3, 0x1

    #@2e
    goto :goto_9

    #@2f
    .line 81
    :catch_2f
    move-exception v0

    #@30
    .line 82
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v4, "Gestures"

    #@32
    new-instance v5, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v6, "Could not save the gesture library in "

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    iget-object v6, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    goto :goto_9

    #@4b
    .line 83
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_4b
    move-exception v0

    #@4c
    .line 84
    .local v0, e:Ljava/io/IOException;
    const-string v4, "Gestures"

    #@4e
    new-instance v5, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v6, "Could not save the gesture library in "

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    iget-object v6, p0, Landroid/gesture/GestureLibraries$FileGestureLibrary;->mPath:Ljava/io/File;

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-static {v4, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@66
    goto :goto_9
.end method
