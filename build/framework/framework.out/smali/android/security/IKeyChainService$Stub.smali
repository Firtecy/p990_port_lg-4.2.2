.class public abstract Landroid/security/IKeyChainService$Stub;
.super Landroid/os/Binder;
.source "IKeyChainService.java"

# interfaces
.implements Landroid/security/IKeyChainService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/security/IKeyChainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/security/IKeyChainService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.security.IKeyChainService"

.field static final TRANSACTION_deleteCaCertificate:I = 0x4

.field static final TRANSACTION_getCertificate:I = 0x2

.field static final TRANSACTION_hasGrant:I = 0x7

.field static final TRANSACTION_installCaCertificate:I = 0x3

.field static final TRANSACTION_requestPrivateKey:I = 0x1

.field static final TRANSACTION_reset:I = 0x5

.field static final TRANSACTION_setGrant:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.security.IKeyChainService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/security/IKeyChainService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/security/IKeyChainService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.security.IKeyChainService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/security/IKeyChainService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/security/IKeyChainService;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/security/IKeyChainService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/security/IKeyChainService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 44
    sparse-switch p1, :sswitch_data_ac

    #@5
    .line 124
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 48
    :sswitch_a
    const-string v4, "android.security.IKeyChainService"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 53
    :sswitch_10
    const-string v4, "android.security.IKeyChainService"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 56
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/security/IKeyChainService$Stub;->requestPrivateKey(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    .line 57
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 58
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    goto :goto_9

    #@24
    .line 63
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_24
    const-string v4, "android.security.IKeyChainService"

    #@26
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 66
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/security/IKeyChainService$Stub;->getCertificate(Ljava/lang/String;)[B

    #@30
    move-result-object v3

    #@31
    .line 67
    .local v3, _result:[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34
    .line 68
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@37
    goto :goto_9

    #@38
    .line 73
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:[B
    :sswitch_38
    const-string v4, "android.security.IKeyChainService"

    #@3a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@40
    move-result-object v0

    #@41
    .line 76
    .local v0, _arg0:[B
    invoke-virtual {p0, v0}, Landroid/security/IKeyChainService$Stub;->installCaCertificate([B)V

    #@44
    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@47
    goto :goto_9

    #@48
    .line 82
    .end local v0           #_arg0:[B
    :sswitch_48
    const-string v6, "android.security.IKeyChainService"

    #@4a
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@50
    move-result-object v0

    #@51
    .line 85
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/security/IKeyChainService$Stub;->deleteCaCertificate(Ljava/lang/String;)Z

    #@54
    move-result v3

    #@55
    .line 86
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@58
    .line 87
    if-eqz v3, :cond_5b

    #@5a
    move v4, v5

    #@5b
    :cond_5b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5e
    goto :goto_9

    #@5f
    .line 92
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_5f
    const-string v6, "android.security.IKeyChainService"

    #@61
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 93
    invoke-virtual {p0}, Landroid/security/IKeyChainService$Stub;->reset()Z

    #@67
    move-result v3

    #@68
    .line 94
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6b
    .line 95
    if-eqz v3, :cond_6e

    #@6d
    move v4, v5

    #@6e
    :cond_6e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@71
    goto :goto_9

    #@72
    .line 100
    .end local v3           #_result:Z
    :sswitch_72
    const-string v6, "android.security.IKeyChainService"

    #@74
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@77
    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7a
    move-result v0

    #@7b
    .line 104
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    .line 106
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@82
    move-result v6

    #@83
    if-eqz v6, :cond_8e

    #@85
    move v2, v5

    #@86
    .line 107
    .local v2, _arg2:Z
    :goto_86
    invoke-virtual {p0, v0, v1, v2}, Landroid/security/IKeyChainService$Stub;->setGrant(ILjava/lang/String;Z)V

    #@89
    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    goto/16 :goto_9

    #@8e
    .end local v2           #_arg2:Z
    :cond_8e
    move v2, v4

    #@8f
    .line 106
    goto :goto_86

    #@90
    .line 113
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_90
    const-string v6, "android.security.IKeyChainService"

    #@92
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95
    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@98
    move-result v0

    #@99
    .line 117
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9c
    move-result-object v1

    #@9d
    .line 118
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/security/IKeyChainService$Stub;->hasGrant(ILjava/lang/String;)Z

    #@a0
    move-result v3

    #@a1
    .line 119
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a4
    .line 120
    if-eqz v3, :cond_a7

    #@a6
    move v4, v5

    #@a7
    :cond_a7
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@aa
    goto/16 :goto_9

    #@ac
    .line 44
    :sswitch_data_ac
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_38
        0x4 -> :sswitch_48
        0x5 -> :sswitch_5f
        0x6 -> :sswitch_72
        0x7 -> :sswitch_90
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
