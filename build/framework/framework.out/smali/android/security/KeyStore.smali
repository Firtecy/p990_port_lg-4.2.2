.class public Landroid/security/KeyStore;
.super Ljava/lang/Object;
.source "KeyStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/security/KeyStore$State;
    }
.end annotation


# static fields
.field public static final KEY_NOT_FOUND:I = 0x7

.field public static final LOCKED:I = 0x2

.field public static final NO_ERROR:I = 0x1

.field public static final PERMISSION_DENIED:I = 0x6

.field public static final PROTOCOL_ERROR:I = 0x5

.field public static final SYSTEM_ERROR:I = 0x4

.field public static final UNDEFINED_ACTION:I = 0x9

.field public static final UNINITIALIZED:I = 0x3

.field public static final VALUE_CORRUPTED:I = 0x8

.field public static final WRONG_PASSWORD:I = 0xa

.field private static final sAddress:Landroid/net/LocalSocketAddress;


# instance fields
.field private mError:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 53
    new-instance v0, Landroid/net/LocalSocketAddress;

    #@2
    const-string/jumbo v1, "keystore"

    #@5
    sget-object v2, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@7
    invoke-direct {v0, v1, v2}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@a
    sput-object v0, Landroid/security/KeyStore;->sAddress:Landroid/net/LocalSocketAddress;

    #@c
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    const/4 v0, 0x1

    #@4
    iput v0, p0, Landroid/security/KeyStore;->mError:I

    #@6
    .line 58
    return-void
.end method

.method private contains([B)Z
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 102
    const/16 v2, 0x65

    #@4
    new-array v3, v0, [[B

    #@6
    aput-object p1, v3, v1

    #@8
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    .line 103
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private delKey([B)Z
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 188
    const/16 v2, 0x6b

    #@4
    new-array v3, v0, [[B

    #@6
    aput-object p1, v3, v1

    #@8
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    .line 189
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private delete([B)Z
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 93
    const/16 v2, 0x64

    #@4
    new-array v3, v0, [[B

    #@6
    aput-object p1, v3, v1

    #@8
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    .line 94
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private varargs execute(I[[B)Ljava/util/ArrayList;
    .registers 16
    .parameter "code"
    .parameter "parameters"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[[B)",
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation

    #@0
    .prologue
    .line 254
    const/4 v11, 0x5

    #@1
    iput v11, p0, Landroid/security/KeyStore;->mError:I

    #@3
    .line 256
    move-object v0, p2

    #@4
    .local v0, arr$:[[B
    array-length v5, v0

    #@5
    .local v5, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v5, :cond_17

    #@8
    aget-object v7, v0, v2

    #@a
    .line 257
    .local v7, parameter:[B
    if-eqz v7, :cond_12

    #@c
    array-length v11, v7

    #@d
    const v12, 0xffff

    #@10
    if-le v11, v12, :cond_14

    #@12
    .line 258
    :cond_12
    const/4 v10, 0x0

    #@13
    .line 310
    .end local v7           #parameter:[B
    :goto_13
    return-object v10

    #@14
    .line 256
    .restart local v7       #parameter:[B
    :cond_14
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_6

    #@17
    .line 262
    .end local v7           #parameter:[B
    :cond_17
    new-instance v8, Landroid/net/LocalSocket;

    #@19
    invoke-direct {v8}, Landroid/net/LocalSocket;-><init>()V

    #@1c
    .line 264
    .local v8, socket:Landroid/net/LocalSocket;
    :try_start_1c
    sget-object v11, Landroid/security/KeyStore;->sAddress:Landroid/net/LocalSocketAddress;

    #@1e
    invoke-virtual {v8, v11}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    #@21
    .line 266
    invoke-virtual {v8}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@24
    move-result-object v6

    #@25
    .line 267
    .local v6, out:Ljava/io/OutputStream;
    invoke-virtual {v6, p1}, Ljava/io/OutputStream;->write(I)V

    #@28
    .line 268
    move-object v0, p2

    #@29
    array-length v5, v0

    #@2a
    const/4 v2, 0x0

    #@2b
    :goto_2b
    if-ge v2, v5, :cond_3f

    #@2d
    aget-object v7, v0, v2

    #@2f
    .line 269
    .restart local v7       #parameter:[B
    array-length v11, v7

    #@30
    shr-int/lit8 v11, v11, 0x8

    #@32
    invoke-virtual {v6, v11}, Ljava/io/OutputStream;->write(I)V

    #@35
    .line 270
    array-length v11, v7

    #@36
    invoke-virtual {v6, v11}, Ljava/io/OutputStream;->write(I)V

    #@39
    .line 271
    invoke-virtual {v6, v7}, Ljava/io/OutputStream;->write([B)V

    #@3c
    .line 268
    add-int/lit8 v2, v2, 0x1

    #@3e
    goto :goto_2b

    #@3f
    .line 273
    .end local v7           #parameter:[B
    :cond_3f
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    #@42
    .line 274
    invoke-virtual {v8}, Landroid/net/LocalSocket;->shutdownOutput()V

    #@45
    .line 276
    invoke-virtual {v8}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@48
    move-result-object v3

    #@49
    .line 277
    .local v3, in:Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@4c
    move-result p1

    #@4d
    const/4 v11, 0x1

    #@4e
    if-eq p1, v11, :cond_5c

    #@50
    .line 278
    const/4 v11, -0x1

    #@51
    if-eq p1, v11, :cond_55

    #@53
    .line 279
    iput p1, p0, Landroid/security/KeyStore;->mError:I
    :try_end_55
    .catchall {:try_start_1c .. :try_end_55} :catchall_a7
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_55} :catch_a0

    #@55
    .line 281
    :cond_55
    const/4 v10, 0x0

    #@56
    .line 307
    :try_start_56
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_59} :catch_5a

    #@59
    goto :goto_13

    #@5a
    .line 308
    :catch_5a
    move-exception v11

    #@5b
    goto :goto_13

    #@5c
    .line 284
    :cond_5c
    :try_start_5c
    new-instance v10, Ljava/util/ArrayList;

    #@5e
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@61
    .line 287
    .local v10, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    :goto_61
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    #@64
    move-result v1

    #@65
    .local v1, i:I
    const/4 v11, -0x1

    #@66
    if-ne v1, v11, :cond_71

    #@68
    .line 301
    const/4 v11, 0x1

    #@69
    iput v11, p0, Landroid/security/KeyStore;->mError:I
    :try_end_6b
    .catchall {:try_start_5c .. :try_end_6b} :catchall_a7
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_6b} :catch_a0

    #@6b
    .line 307
    :try_start_6b
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6e} :catch_6f

    #@6e
    goto :goto_13

    #@6f
    .line 308
    :catch_6f
    move-exception v11

    #@70
    goto :goto_13

    #@71
    .line 290
    :cond_71
    :try_start_71
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I
    :try_end_74
    .catchall {:try_start_71 .. :try_end_74} :catchall_a7
    .catch Ljava/io/IOException; {:try_start_71 .. :try_end_74} :catch_a0

    #@74
    move-result v4

    #@75
    .local v4, j:I
    const/4 v11, -0x1

    #@76
    if-ne v4, v11, :cond_7f

    #@78
    .line 291
    const/4 v10, 0x0

    #@79
    .line 307
    .end local v10           #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    :try_start_79
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_7c} :catch_7d

    #@7c
    goto :goto_13

    #@7d
    .line 308
    :catch_7d
    move-exception v11

    #@7e
    goto :goto_13

    #@7f
    .line 293
    .restart local v10       #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    :cond_7f
    shl-int/lit8 v11, v1, 0x8

    #@81
    or-int/2addr v11, v4

    #@82
    :try_start_82
    new-array v9, v11, [B

    #@84
    .line 294
    .local v9, value:[B
    const/4 v1, 0x0

    #@85
    :goto_85
    array-length v11, v9

    #@86
    if-ge v1, v11, :cond_9c

    #@88
    .line 295
    array-length v11, v9

    #@89
    sub-int/2addr v11, v1

    #@8a
    invoke-virtual {v3, v9, v1, v11}, Ljava/io/InputStream;->read([BII)I
    :try_end_8d
    .catchall {:try_start_82 .. :try_end_8d} :catchall_a7
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_8d} :catch_a0

    #@8d
    move-result v4

    #@8e
    const/4 v11, -0x1

    #@8f
    if-ne v4, v11, :cond_9a

    #@91
    .line 296
    const/4 v10, 0x0

    #@92
    .line 307
    .end local v10           #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    :try_start_92
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_92 .. :try_end_95} :catch_97

    #@95
    goto/16 :goto_13

    #@97
    .line 308
    :catch_97
    move-exception v11

    #@98
    goto/16 :goto_13

    #@9a
    .line 294
    .restart local v10       #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    :cond_9a
    add-int/2addr v1, v4

    #@9b
    goto :goto_85

    #@9c
    .line 299
    :cond_9c
    :try_start_9c
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9f
    .catchall {:try_start_9c .. :try_end_9f} :catchall_a7
    .catch Ljava/io/IOException; {:try_start_9c .. :try_end_9f} :catch_a0

    #@9f
    goto :goto_61

    #@a0
    .line 303
    .end local v1           #i:I
    .end local v3           #in:Ljava/io/InputStream;
    .end local v4           #j:I
    .end local v6           #out:Ljava/io/OutputStream;
    .end local v9           #value:[B
    .end local v10           #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    :catch_a0
    move-exception v11

    #@a1
    .line 307
    :try_start_a1
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_a4
    .catch Ljava/io/IOException; {:try_start_a1 .. :try_end_a4} :catch_ac

    #@a4
    .line 310
    :goto_a4
    const/4 v10, 0x0

    #@a5
    goto/16 :goto_13

    #@a7
    .line 306
    :catchall_a7
    move-exception v11

    #@a8
    .line 307
    :try_start_a8
    invoke-virtual {v8}, Landroid/net/LocalSocket;->close()V
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_ab} :catch_ae

    #@ab
    .line 308
    :goto_ab
    throw v11

    #@ac
    :catch_ac
    move-exception v11

    #@ad
    goto :goto_a4

    #@ae
    :catch_ae
    move-exception v12

    #@af
    goto :goto_ab
.end method

.method private generate([B)Z
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 161
    const/16 v2, 0x61

    #@4
    new-array v3, v0, [[B

    #@6
    aput-object p1, v3, v1

    #@8
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    .line 162
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private get([B)[B
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 75
    const/16 v1, 0x67

    #@3
    const/4 v2, 0x1

    #@4
    new-array v2, v2, [[B

    #@6
    aput-object p1, v2, v3

    #@8
    invoke-direct {p0, v1, v2}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    .line 76
    .local v0, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    if-eqz v0, :cond_14

    #@e
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_16

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    :goto_15
    return-object v1

    #@16
    :cond_16
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, [B

    #@1c
    goto :goto_15
.end method

.method public static getInstance()Landroid/security/KeyStore;
    .registers 1

    #@0
    .prologue
    .line 61
    new-instance v0, Landroid/security/KeyStore;

    #@2
    invoke-direct {v0}, Landroid/security/KeyStore;-><init>()V

    #@5
    return-object v0
.end method

.method private static getKeyBytes(Ljava/lang/String;)[B
    .registers 6
    .parameter "string"

    #@0
    .prologue
    .line 319
    const/4 v3, 0x0

    #@1
    :try_start_1
    invoke-static {p0, v3}, Ljava/nio/charset/ModifiedUtf8;->countBytes(Ljava/lang/String;Z)J

    #@4
    move-result-wide v3

    #@5
    long-to-int v2, v3

    #@6
    .line 320
    .local v2, utfCount:I
    new-array v1, v2, [B

    #@8
    .line 321
    .local v1, result:[B
    const/4 v3, 0x0

    #@9
    invoke-static {v1, v3, p0}, Ljava/nio/charset/ModifiedUtf8;->encode([BILjava/lang/String;)V
    :try_end_c
    .catch Ljava/io/UTFDataFormatException; {:try_start_1 .. :try_end_c} :catch_d

    #@c
    .line 322
    return-object v1

    #@d
    .line 323
    .end local v1           #result:[B
    .end local v2           #utfCount:I
    :catch_d
    move-exception v0

    #@e
    .line 324
    .local v0, e:Ljava/io/UTFDataFormatException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@10
    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@13
    throw v3
.end method

.method private static getPasswordBytes(Ljava/lang/String;)[B
    .registers 2
    .parameter "password"

    #@0
    .prologue
    .line 337
    sget-object v0, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@2
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private getPubkey([B)[B
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 179
    const/16 v1, 0x62

    #@3
    const/4 v2, 0x1

    #@4
    new-array v2, v2, [[B

    #@6
    aput-object p1, v2, v3

    #@8
    invoke-direct {p0, v1, v2}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    .line 180
    .local v0, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    if-eqz v0, :cond_14

    #@e
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_16

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    :goto_15
    return-object v1

    #@16
    :cond_16
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, [B

    #@1c
    goto :goto_15
.end method

.method private static getUidBytes(I)[B
    .registers 3
    .parameter "uid"

    #@0
    .prologue
    .line 341
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private getmtime([B)J
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 233
    const/16 v1, 0x63

    #@3
    const/4 v2, 0x1

    #@4
    new-array v2, v2, [[B

    #@6
    aput-object p1, v2, v3

    #@8
    invoke-direct {p0, v1, v2}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    .line 234
    .local v0, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    if-eqz v0, :cond_14

    #@e
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_17

    #@14
    .line 235
    :cond_14
    const-wide/16 v1, -0x1

    #@16
    .line 238
    :goto_16
    return-wide v1

    #@17
    :cond_17
    new-instance v2, Ljava/lang/String;

    #@19
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, [B

    #@1f
    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    #@22
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@25
    move-result-wide v1

    #@26
    const-wide/16 v3, 0x3e8

    #@28
    mul-long/2addr v1, v3

    #@29
    goto :goto_16
.end method

.method private grant([B[B)Z
    .registers 7
    .parameter "key"
    .parameter "uid"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 215
    const/16 v2, 0x78

    #@4
    const/4 v3, 0x2

    #@5
    new-array v3, v3, [[B

    #@7
    aput-object p1, v3, v1

    #@9
    aput-object p2, v3, v0

    #@b
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@e
    .line 216
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@10
    if-ne v2, v0, :cond_13

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_12
.end method

.method private importKey([B[B)Z
    .registers 7
    .parameter "keyName"
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 170
    const/16 v2, 0x6d

    #@4
    const/4 v3, 0x2

    #@5
    new-array v3, v3, [[B

    #@7
    aput-object p1, v3, v1

    #@9
    aput-object p2, v3, v0

    #@b
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@e
    .line 171
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@10
    if-ne v2, v0, :cond_13

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_12
.end method

.method private password([B)Z
    .registers 6
    .parameter "password"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 133
    const/16 v2, 0x70

    #@4
    new-array v3, v0, [[B

    #@6
    aput-object p1, v3, v1

    #@8
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    .line 134
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private put([B[B)Z
    .registers 7
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 84
    const/16 v2, 0x69

    #@4
    const/4 v3, 0x2

    #@5
    new-array v3, v3, [[B

    #@7
    aput-object p1, v3, v1

    #@9
    aput-object p2, v3, v0

    #@b
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@e
    .line 85
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@10
    if-ne v2, v0, :cond_13

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_12
.end method

.method private sign([B[B)[B
    .registers 8
    .parameter "keyName"
    .parameter "data"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 197
    const/16 v1, 0x6e

    #@3
    const/4 v2, 0x2

    #@4
    new-array v2, v2, [[B

    #@6
    aput-object p1, v2, v4

    #@8
    const/4 v3, 0x1

    #@9
    aput-object p2, v2, v3

    #@b
    invoke-direct {p0, v1, v2}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@e
    move-result-object v0

    #@f
    .line 198
    .local v0, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    if-eqz v0, :cond_17

    #@11
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_19

    #@17
    :cond_17
    const/4 v1, 0x0

    #@18
    :goto_18
    return-object v1

    #@19
    :cond_19
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, [B

    #@1f
    goto :goto_18
.end method

.method private static toKeyString([B)Ljava/lang/String;
    .registers 5
    .parameter "bytes"

    #@0
    .prologue
    .line 330
    :try_start_0
    array-length v1, p0

    #@1
    new-array v1, v1, [C

    #@3
    const/4 v2, 0x0

    #@4
    array-length v3, p0

    #@5
    invoke-static {p0, v1, v2, v3}, Ljava/nio/charset/ModifiedUtf8;->decode([B[CII)Ljava/lang/String;
    :try_end_8
    .catch Ljava/io/UTFDataFormatException; {:try_start_0 .. :try_end_8} :catch_a

    #@8
    move-result-object v1

    #@9
    return-object v1

    #@a
    .line 331
    :catch_a
    move-exception v0

    #@b
    .line 332
    .local v0, e:Ljava/io/UTFDataFormatException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v1
.end method

.method private ungrant([B[B)Z
    .registers 7
    .parameter "key"
    .parameter "uid"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 224
    const/16 v2, 0x79

    #@4
    const/4 v3, 0x2

    #@5
    new-array v3, v3, [[B

    #@7
    aput-object p1, v3, v1

    #@9
    aput-object p2, v3, v0

    #@b
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@e
    .line 225
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@10
    if-ne v2, v0, :cond_13

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_12
.end method

.method private unlock([B)Z
    .registers 6
    .parameter "password"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 147
    const/16 v2, 0x75

    #@4
    new-array v3, v0, [[B

    #@6
    aput-object p1, v3, v1

    #@8
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    .line 148
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@d
    if-ne v2, v0, :cond_10

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    goto :goto_f
.end method

.method private verify([B[B[B)Z
    .registers 9
    .parameter "keyName"
    .parameter "data"
    .parameter "signature"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 206
    const/16 v2, 0x76

    #@4
    const/4 v3, 0x3

    #@5
    new-array v3, v3, [[B

    #@7
    aput-object p1, v3, v1

    #@9
    aput-object p2, v3, v0

    #@b
    const/4 v4, 0x2

    #@c
    aput-object p3, v3, v4

    #@e
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@11
    .line 207
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@13
    if-ne v2, v0, :cond_16

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    move v0, v1

    #@17
    goto :goto_15
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 107
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->contains([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public delKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 193
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->delKey([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public delete(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 98
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->delete([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public generate(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 166
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->generate([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public get(Ljava/lang/String;)[B
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 80
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->get([B)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getLastError()I
    .registers 2

    #@0
    .prologue
    .line 250
    iget v0, p0, Landroid/security/KeyStore;->mError:I

    #@2
    return v0
.end method

.method public getPubkey(Ljava/lang/String;)[B
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 184
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->getPubkey([B)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getmtime(Ljava/lang/String;)J
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 246
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->getmtime([B)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public grant(Ljava/lang/String;I)Z
    .registers 5
    .parameter "key"
    .parameter "uid"

    #@0
    .prologue
    .line 220
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {p2}, Landroid/security/KeyStore;->getUidBytes(I)[B

    #@7
    move-result-object v1

    #@8
    invoke-direct {p0, v0, v1}, Landroid/security/KeyStore;->grant([B[B)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public importKey(Ljava/lang/String;[B)Z
    .registers 4
    .parameter "keyName"
    .parameter "key"

    #@0
    .prologue
    .line 175
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2}, Landroid/security/KeyStore;->importKey([B[B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public isEmpty()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 156
    const/16 v1, 0x7a

    #@3
    new-array v2, v0, [[B

    #@5
    invoke-direct {p0, v1, v2}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@8
    .line 157
    iget v1, p0, Landroid/security/KeyStore;->mError:I

    #@a
    const/4 v2, 0x7

    #@b
    if-ne v1, v2, :cond_e

    #@d
    const/4 v0, 0x1

    #@e
    :cond_e
    return v0
.end method

.method public lock()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 142
    const/16 v2, 0x6c

    #@4
    new-array v3, v1, [[B

    #@6
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@9
    .line 143
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@b
    if-ne v2, v0, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    move v0, v1

    #@f
    goto :goto_d
.end method

.method public password(Ljava/lang/String;)Z
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 138
    invoke-static {p1}, Landroid/security/KeyStore;->getPasswordBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->password([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public put(Ljava/lang/String;[B)Z
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 89
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2}, Landroid/security/KeyStore;->put([B[B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public reset()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 128
    const/16 v2, 0x72

    #@4
    new-array v3, v1, [[B

    #@6
    invoke-direct {p0, v2, v3}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@9
    .line 129
    iget v2, p0, Landroid/security/KeyStore;->mError:I

    #@b
    if-ne v2, v0, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    move v0, v1

    #@f
    goto :goto_d
.end method

.method public saw(Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "prefix"

    #@0
    .prologue
    .line 116
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v3

    #@4
    invoke-virtual {p0, v3}, Landroid/security/KeyStore;->saw([B)[[B

    #@7
    move-result-object v2

    #@8
    .line 117
    .local v2, values:[[B
    if-nez v2, :cond_c

    #@a
    .line 118
    const/4 v1, 0x0

    #@b
    .line 124
    :cond_b
    return-object v1

    #@c
    .line 120
    :cond_c
    array-length v3, v2

    #@d
    new-array v1, v3, [Ljava/lang/String;

    #@f
    .line 121
    .local v1, strings:[Ljava/lang/String;
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    array-length v3, v2

    #@11
    if-ge v0, v3, :cond_b

    #@13
    .line 122
    aget-object v3, v2, v0

    #@15
    invoke-static {v3}, Landroid/security/KeyStore;->toKeyString([B)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v0

    #@1b
    .line 121
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_10
.end method

.method public saw([B)[[B
    .registers 6
    .parameter "prefix"

    #@0
    .prologue
    .line 111
    const/16 v1, 0x73

    #@2
    const/4 v2, 0x1

    #@3
    new-array v2, v2, [[B

    #@5
    const/4 v3, 0x0

    #@6
    aput-object p1, v2, v3

    #@8
    invoke-direct {p0, v1, v2}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    .line 112
    .local v0, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    if-nez v0, :cond_12

    #@e
    const/4 v1, 0x0

    #@f
    check-cast v1, [[B

    #@11
    :goto_11
    return-object v1

    #@12
    :cond_12
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v1

    #@16
    new-array v1, v1, [[B

    #@18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, [[B

    #@1e
    goto :goto_11
.end method

.method public sign(Ljava/lang/String;[B)[B
    .registers 4
    .parameter "key"
    .parameter "data"

    #@0
    .prologue
    .line 202
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2}, Landroid/security/KeyStore;->sign([B[B)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public state()Landroid/security/KeyStore$State;
    .registers 3

    #@0
    .prologue
    .line 65
    const/16 v0, 0x74

    #@2
    const/4 v1, 0x0

    #@3
    new-array v1, v1, [[B

    #@5
    invoke-direct {p0, v0, v1}, Landroid/security/KeyStore;->execute(I[[B)Ljava/util/ArrayList;

    #@8
    .line 66
    iget v0, p0, Landroid/security/KeyStore;->mError:I

    #@a
    packed-switch v0, :pswitch_data_1e

    #@d
    .line 70
    new-instance v0, Ljava/lang/AssertionError;

    #@f
    iget v1, p0, Landroid/security/KeyStore;->mError:I

    #@11
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(I)V

    #@14
    throw v0

    #@15
    .line 67
    :pswitch_15
    sget-object v0, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@17
    .line 69
    :goto_17
    return-object v0

    #@18
    .line 68
    :pswitch_18
    sget-object v0, Landroid/security/KeyStore$State;->LOCKED:Landroid/security/KeyStore$State;

    #@1a
    goto :goto_17

    #@1b
    .line 69
    :pswitch_1b
    sget-object v0, Landroid/security/KeyStore$State;->UNINITIALIZED:Landroid/security/KeyStore$State;

    #@1d
    goto :goto_17

    #@1e
    .line 66
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_15
        :pswitch_18
        :pswitch_1b
    .end packed-switch
.end method

.method public ungrant(Ljava/lang/String;I)Z
    .registers 5
    .parameter "key"
    .parameter "uid"

    #@0
    .prologue
    .line 229
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {p2}, Landroid/security/KeyStore;->getUidBytes(I)[B

    #@7
    move-result-object v1

    #@8
    invoke-direct {p0, v0, v1}, Landroid/security/KeyStore;->ungrant([B[B)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public unlock(Ljava/lang/String;)Z
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 152
    invoke-static {p1}, Landroid/security/KeyStore;->getPasswordBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/security/KeyStore;->unlock([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public verify(Ljava/lang/String;[B[B)Z
    .registers 5
    .parameter "key"
    .parameter "data"
    .parameter "signature"

    #@0
    .prologue
    .line 211
    invoke-static {p1}, Landroid/security/KeyStore;->getKeyBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2, p3}, Landroid/security/KeyStore;->verify([B[B[B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method
