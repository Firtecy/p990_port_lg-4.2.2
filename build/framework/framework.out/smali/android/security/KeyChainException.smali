.class public Landroid/security/KeyChainException;
.super Ljava/lang/Exception;
.source "KeyChainException.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    #@3
    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "detailMessage"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@3
    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 3
    .parameter "message"
    .parameter "cause"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3
    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .registers 3
    .parameter "cause"

    #@0
    .prologue
    .line 63
    if-nez p1, :cond_7

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    invoke-direct {p0, v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@6
    .line 64
    return-void

    #@7
    .line 63
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_3
.end method
