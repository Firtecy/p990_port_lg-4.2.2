.class public final Landroid/security/KeyChain;
.super Ljava/lang/Object;
.source "KeyChain.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/security/KeyChain$KeyChainConnection;,
        Landroid/security/KeyChain$AliasResponse;
    }
.end annotation


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.android.keychain"

.field private static final ACTION_CHOOSER:Ljava/lang/String; = "com.android.keychain.CHOOSER"

.field private static final ACTION_INSTALL:Ljava/lang/String; = "android.credentials.INSTALL"

.field public static final ACTION_STORAGE_CHANGED:Ljava/lang/String; = "android.security.STORAGE_CHANGED"

.field public static final EXTRA_ALIAS:Ljava/lang/String; = "alias"

.field public static final EXTRA_CERTIFICATE:Ljava/lang/String; = "CERT"

.field public static final EXTRA_HOST:Ljava/lang/String; = "host"

.field public static final EXTRA_NAME:Ljava/lang/String; = "name"

.field public static final EXTRA_PKCS12:Ljava/lang/String; = "PKCS12"

.field public static final EXTRA_PORT:Ljava/lang/String; = "port"

.field public static final EXTRA_RESPONSE:Ljava/lang/String; = "response"

.field public static final EXTRA_SENDER:Ljava/lang/String; = "sender"

.field private static final TAG:Ljava/lang/String; = "KeyChain"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 371
    return-void
.end method

.method public static bind(Landroid/content/Context;)Landroid/security/KeyChain$KeyChainConnection;
    .registers 7
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 396
    if-nez p0, :cond_b

    #@3
    .line 397
    new-instance v3, Ljava/lang/NullPointerException;

    #@5
    const-string v4, "context == null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 399
    :cond_b
    invoke-static {p0}, Landroid/security/KeyChain;->ensureNotOnMainThread(Landroid/content/Context;)V

    #@e
    .line 400
    new-instance v2, Ljava/util/concurrent/LinkedBlockingQueue;

    #@10
    invoke-direct {v2, v5}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    #@13
    .line 401
    .local v2, q:Ljava/util/concurrent/BlockingQueue;,"Ljava/util/concurrent/BlockingQueue<Landroid/security/IKeyChainService;>;"
    new-instance v1, Landroid/security/KeyChain$1;

    #@15
    invoke-direct {v1, v2}, Landroid/security/KeyChain$1;-><init>(Ljava/util/concurrent/BlockingQueue;)V

    #@18
    .line 415
    .local v1, keyChainServiceConnection:Landroid/content/ServiceConnection;
    new-instance v3, Landroid/content/Intent;

    #@1a
    const-class v4, Landroid/security/IKeyChainService;

    #@1c
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23
    invoke-virtual {p0, v3, v1, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@26
    move-result v0

    #@27
    .line 418
    .local v0, isBound:Z
    if-nez v0, :cond_31

    #@29
    .line 419
    new-instance v3, Ljava/lang/AssertionError;

    #@2b
    const-string v4, "could not bind to KeyChainService"

    #@2d
    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@30
    throw v3

    #@31
    .line 421
    :cond_31
    new-instance v4, Landroid/security/KeyChain$KeyChainConnection;

    #@33
    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Landroid/security/IKeyChainService;

    #@39
    const/4 v5, 0x0

    #@3a
    invoke-direct {v4, p0, v1, v3, v5}, Landroid/security/KeyChain$KeyChainConnection;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;Landroid/security/IKeyChainService;Landroid/security/KeyChain$1;)V

    #@3d
    return-object v4
.end method

.method public static choosePrivateKeyAlias(Landroid/app/Activity;Landroid/security/KeyChainAliasCallback;[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;ILjava/lang/String;)V
    .registers 12
    .parameter "activity"
    .parameter "response"
    .parameter "keyTypes"
    .parameter "issuers"
    .parameter "host"
    .parameter "port"
    .parameter "alias"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 262
    if-nez p0, :cond_b

    #@3
    .line 263
    new-instance v1, Ljava/lang/NullPointerException;

    #@5
    const-string v2, "activity == null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 265
    :cond_b
    if-nez p1, :cond_16

    #@d
    .line 266
    new-instance v1, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v2, "response == null"

    #@12
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1

    #@16
    .line 268
    :cond_16
    new-instance v0, Landroid/content/Intent;

    #@18
    const-string v1, "com.android.keychain.CHOOSER"

    #@1a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1d
    .line 269
    .local v0, intent:Landroid/content/Intent;
    const-string/jumbo v1, "response"

    #@20
    new-instance v2, Landroid/security/KeyChain$AliasResponse;

    #@22
    const/4 v3, 0x0

    #@23
    invoke-direct {v2, p1, v3}, Landroid/security/KeyChain$AliasResponse;-><init>(Landroid/security/KeyChainAliasCallback;Landroid/security/KeyChain$1;)V

    #@26
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/IBinder;)Landroid/content/Intent;

    #@29
    .line 270
    const-string v1, "host"

    #@2b
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2e
    .line 271
    const-string/jumbo v1, "port"

    #@31
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@34
    .line 272
    const-string v1, "alias"

    #@36
    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@39
    .line 274
    const-string/jumbo v1, "sender"

    #@3c
    new-instance v2, Landroid/content/Intent;

    #@3e
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@41
    invoke-static {p0, v4, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@48
    .line 275
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    #@4b
    .line 276
    return-void
.end method

.method public static createInstallIntent()Landroid/content/Intent;
    .registers 3

    #@0
    .prologue
    .line 202
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.credentials.INSTALL"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 203
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.certinstaller"

    #@9
    const-string v2, "com.android.certinstaller.CertInstallerMain"

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@e
    .line 205
    return-object v0
.end method

.method private static ensureNotOnMainThread(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 425
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    .line 426
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_14

    #@6
    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@9
    move-result-object v1

    #@a
    if-ne v0, v1, :cond_14

    #@c
    .line 427
    new-instance v1, Ljava/lang/IllegalStateException;

    #@e
    const-string v2, "calling this from your main thread can lead to deadlock"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 430
    :cond_14
    return-void
.end method

.method public static getCertificateChain(Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .registers 10
    .parameter "context"
    .parameter "alias"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyChainException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 333
    if-nez p1, :cond_a

    #@2
    .line 334
    new-instance v6, Ljava/lang/NullPointerException;

    #@4
    const-string v7, "alias == null"

    #@6
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v6

    #@a
    .line 336
    :cond_a
    invoke-static {p0}, Landroid/security/KeyChain;->bind(Landroid/content/Context;)Landroid/security/KeyChain$KeyChainConnection;

    #@d
    move-result-object v3

    #@e
    .line 338
    .local v3, keyChainConnection:Landroid/security/KeyChain$KeyChainConnection;
    :try_start_e
    invoke-virtual {v3}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    #@11
    move-result-object v4

    #@12
    .line 339
    .local v4, keyChainService:Landroid/security/IKeyChainService;
    invoke-interface {v4, p1}, Landroid/security/IKeyChainService;->getCertificate(Ljava/lang/String;)[B

    #@15
    move-result-object v0

    #@16
    .line 340
    .local v0, certificateBytes:[B
    new-instance v5, Lorg/apache/harmony/xnet/provider/jsse/TrustedCertificateStore;

    #@18
    invoke-direct {v5}, Lorg/apache/harmony/xnet/provider/jsse/TrustedCertificateStore;-><init>()V

    #@1b
    .line 341
    .local v5, store:Lorg/apache/harmony/xnet/provider/jsse/TrustedCertificateStore;
    invoke-static {v0}, Landroid/security/KeyChain;->toCertificate([B)Ljava/security/cert/X509Certificate;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v5, v6}, Lorg/apache/harmony/xnet/provider/jsse/TrustedCertificateStore;->getCertificateChain(Ljava/security/cert/X509Certificate;)Ljava/util/List;

    #@22
    move-result-object v1

    #@23
    .line 343
    .local v1, chain:Ljava/util/List;,"Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@26
    move-result v6

    #@27
    new-array v6, v6, [Ljava/security/cert/X509Certificate;

    #@29
    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@2c
    move-result-object v6

    #@2d
    check-cast v6, [Ljava/security/cert/X509Certificate;
    :try_end_2f
    .catchall {:try_start_e .. :try_end_2f} :catchall_3a
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_2f} :catch_33
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_2f} :catch_3f

    #@2f
    .line 350
    invoke-virtual {v3}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    #@32
    .line 343
    return-object v6

    #@33
    .line 344
    .end local v0           #certificateBytes:[B
    .end local v1           #chain:Ljava/util/List;,"Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    .end local v4           #keyChainService:Landroid/security/IKeyChainService;
    .end local v5           #store:Lorg/apache/harmony/xnet/provider/jsse/TrustedCertificateStore;
    :catch_33
    move-exception v2

    #@34
    .line 345
    .local v2, e:Landroid/os/RemoteException;
    :try_start_34
    new-instance v6, Landroid/security/KeyChainException;

    #@36
    invoke-direct {v6, v2}, Landroid/security/KeyChainException;-><init>(Ljava/lang/Throwable;)V

    #@39
    throw v6
    :try_end_3a
    .catchall {:try_start_34 .. :try_end_3a} :catchall_3a

    #@3a
    .line 350
    .end local v2           #e:Landroid/os/RemoteException;
    :catchall_3a
    move-exception v6

    #@3b
    invoke-virtual {v3}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    #@3e
    throw v6

    #@3f
    .line 346
    :catch_3f
    move-exception v2

    #@40
    .line 348
    .local v2, e:Ljava/lang/RuntimeException;
    :try_start_40
    new-instance v6, Landroid/security/KeyChainException;

    #@42
    invoke-direct {v6, v2}, Landroid/security/KeyChainException;-><init>(Ljava/lang/Throwable;)V

    #@45
    throw v6
    :try_end_46
    .catchall {:try_start_40 .. :try_end_46} :catchall_3a
.end method

.method public static getPrivateKey(Landroid/content/Context;Ljava/lang/String;)Ljava/security/PrivateKey;
    .registers 9
    .parameter "context"
    .parameter "alias"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyChainException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 298
    if-nez p1, :cond_a

    #@2
    .line 299
    new-instance v5, Ljava/lang/NullPointerException;

    #@4
    const-string v6, "alias == null"

    #@6
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v5

    #@a
    .line 301
    :cond_a
    invoke-static {p0}, Landroid/security/KeyChain;->bind(Landroid/content/Context;)Landroid/security/KeyChain$KeyChainConnection;

    #@d
    move-result-object v2

    #@e
    .line 303
    .local v2, keyChainConnection:Landroid/security/KeyChain$KeyChainConnection;
    :try_start_e
    invoke-virtual {v2}, Landroid/security/KeyChain$KeyChainConnection;->getService()Landroid/security/IKeyChainService;

    #@11
    move-result-object v3

    #@12
    .line 304
    .local v3, keyChainService:Landroid/security/IKeyChainService;
    invoke-interface {v3, p1}, Landroid/security/IKeyChainService;->requestPrivateKey(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    .line 305
    .local v4, keyId:Ljava/lang/String;
    if-nez v4, :cond_2d

    #@18
    .line 306
    new-instance v5, Landroid/security/KeyChainException;

    #@1a
    const-string/jumbo v6, "keystore had a problem"

    #@1d
    invoke-direct {v5, v6}, Landroid/security/KeyChainException;-><init>(Ljava/lang/String;)V

    #@20
    throw v5
    :try_end_21
    .catchall {:try_start_e .. :try_end_21} :catchall_28
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_21} :catch_21
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_21} :catch_3c
    .catch Ljava/security/InvalidKeyException; {:try_start_e .. :try_end_21} :catch_43

    #@21
    .line 311
    .end local v3           #keyChainService:Landroid/security/IKeyChainService;
    .end local v4           #keyId:Ljava/lang/String;
    :catch_21
    move-exception v0

    #@22
    .line 312
    .local v0, e:Landroid/os/RemoteException;
    :try_start_22
    new-instance v5, Landroid/security/KeyChainException;

    #@24
    invoke-direct {v5, v0}, Landroid/security/KeyChainException;-><init>(Ljava/lang/Throwable;)V

    #@27
    throw v5
    :try_end_28
    .catchall {:try_start_22 .. :try_end_28} :catchall_28

    #@28
    .line 319
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_28
    move-exception v5

    #@29
    invoke-virtual {v2}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    #@2c
    throw v5

    #@2d
    .line 309
    .restart local v3       #keyChainService:Landroid/security/IKeyChainService;
    .restart local v4       #keyId:Ljava/lang/String;
    :cond_2d
    :try_start_2d
    const-string/jumbo v5, "keystore"

    #@30
    invoke-static {v5}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;->getInstance(Ljava/lang/String;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;

    #@33
    move-result-object v1

    #@34
    .line 310
    .local v1, engine:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;
    invoke-virtual {v1, v4}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;->getPrivateKeyById(Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_28
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_37} :catch_21
    .catch Ljava/lang/RuntimeException; {:try_start_2d .. :try_end_37} :catch_3c
    .catch Ljava/security/InvalidKeyException; {:try_start_2d .. :try_end_37} :catch_43

    #@37
    move-result-object v5

    #@38
    .line 319
    invoke-virtual {v2}, Landroid/security/KeyChain$KeyChainConnection;->close()V

    #@3b
    .line 310
    return-object v5

    #@3c
    .line 313
    .end local v1           #engine:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;
    .end local v3           #keyChainService:Landroid/security/IKeyChainService;
    .end local v4           #keyId:Ljava/lang/String;
    :catch_3c
    move-exception v0

    #@3d
    .line 315
    .local v0, e:Ljava/lang/RuntimeException;
    :try_start_3d
    new-instance v5, Landroid/security/KeyChainException;

    #@3f
    invoke-direct {v5, v0}, Landroid/security/KeyChainException;-><init>(Ljava/lang/Throwable;)V

    #@42
    throw v5

    #@43
    .line 316
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catch_43
    move-exception v0

    #@44
    .line 317
    .local v0, e:Ljava/security/InvalidKeyException;
    new-instance v5, Landroid/security/KeyChainException;

    #@46
    invoke-direct {v5, v0}, Landroid/security/KeyChainException;-><init>(Ljava/lang/Throwable;)V

    #@49
    throw v5
    :try_end_4a
    .catchall {:try_start_3d .. :try_end_4a} :catchall_28
.end method

.method private static toCertificate([B)Ljava/security/cert/X509Certificate;
    .registers 6
    .parameter "bytes"

    #@0
    .prologue
    .line 355
    if-nez p0, :cond_a

    #@2
    .line 356
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v4, "bytes == null"

    #@6
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v3

    #@a
    .line 359
    :cond_a
    :try_start_a
    const-string v3, "X.509"

    #@c
    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@f
    move-result-object v1

    #@10
    .line 360
    .local v1, certFactory:Ljava/security/cert/CertificateFactory;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    #@12
    invoke-direct {v3, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@15
    invoke-virtual {v1, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@18
    move-result-object v0

    #@19
    .line 361
    .local v0, cert:Ljava/security/cert/Certificate;
    check-cast v0, Ljava/security/cert/X509Certificate;
    :try_end_1b
    .catch Ljava/security/cert/CertificateException; {:try_start_a .. :try_end_1b} :catch_1c

    #@1b
    .end local v0           #cert:Ljava/security/cert/Certificate;
    return-object v0

    #@1c
    .line 362
    .end local v1           #certFactory:Ljava/security/cert/CertificateFactory;
    :catch_1c
    move-exception v2

    #@1d
    .line 363
    .local v2, e:Ljava/security/cert/CertificateException;
    new-instance v3, Ljava/lang/AssertionError;

    #@1f
    invoke-direct {v3, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@22
    throw v3
.end method
