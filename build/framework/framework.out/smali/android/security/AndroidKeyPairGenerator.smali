.class public Landroid/security/AndroidKeyPairGenerator;
.super Ljava/security/KeyPairGeneratorSpi;
.source "AndroidKeyPairGenerator.java"


# static fields
.field public static final NAME:Ljava/lang/String; = "AndroidKeyPairGenerator"


# instance fields
.field private mKeyStore:Landroid/security/KeyStore;

.field private mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Ljava/security/KeyPairGeneratorSpi;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public generateKeyPair()Ljava/security/KeyPair;
    .registers 15

    #@0
    .prologue
    .line 77
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@2
    if-eqz v11, :cond_8

    #@4
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@6
    if-nez v11, :cond_10

    #@8
    .line 78
    :cond_8
    new-instance v11, Ljava/lang/IllegalStateException;

    #@a
    const-string v12, "Must call initialize with an AndroidKeyPairGeneratorSpec first"

    #@c
    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v11

    #@10
    .line 82
    :cond_10
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@12
    invoke-virtual {v11}, Landroid/security/AndroidKeyPairGeneratorSpec;->getKeystoreAlias()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 84
    .local v0, alias:Ljava/lang/String;
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@18
    invoke-static {v11, v0}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@1b
    .line 86
    new-instance v11, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v12, "USRPKEY_"

    #@22
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v11

    #@26
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v11

    #@2a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v8

    #@2e
    .line 87
    .local v8, privateKeyAlias:Ljava/lang/String;
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@30
    invoke-virtual {v11, v8}, Landroid/security/KeyStore;->generate(Ljava/lang/String;)Z

    #@33
    .line 90
    const-string/jumbo v11, "keystore"

    #@36
    invoke-static {v11}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;->getInstance(Ljava/lang/String;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;

    #@39
    move-result-object v5

    #@3a
    .line 92
    .local v5, engine:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;
    :try_start_3a
    invoke-virtual {v5, v8}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;->getPrivateKeyById(Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_3d
    .catch Ljava/security/InvalidKeyException; {:try_start_3a .. :try_end_3d} :catch_be

    #@3d
    move-result-object v7

    #@3e
    .line 97
    .local v7, privKey:Ljava/security/PrivateKey;
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@40
    invoke-virtual {v11, v8}, Landroid/security/KeyStore;->getPubkey(Ljava/lang/String;)[B

    #@43
    move-result-object v10

    #@44
    .line 101
    .local v10, pubKeyBytes:[B
    :try_start_44
    const-string v11, "RSA"

    #@46
    invoke-static {v11}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    #@49
    move-result-object v6

    #@4a
    .line 102
    .local v6, keyFact:Ljava/security/KeyFactory;
    new-instance v11, Ljava/security/spec/X509EncodedKeySpec;

    #@4c
    invoke-direct {v11, v10}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    #@4f
    invoke-virtual {v6, v11}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_52
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_44 .. :try_end_52} :catch_c7
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_44 .. :try_end_52} :catch_d0

    #@52
    move-result-object v9

    #@53
    .line 109
    .local v9, pubKey:Ljava/security/PublicKey;
    new-instance v3, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;

    #@55
    invoke-direct {v3}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;-><init>()V

    #@58
    .line 110
    .local v3, certGen:Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;
    invoke-virtual {v3, v9}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setPublicKey(Ljava/security/PublicKey;)V

    #@5b
    .line 111
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@5d
    invoke-virtual {v11}, Landroid/security/AndroidKeyPairGeneratorSpec;->getSerialNumber()Ljava/math/BigInteger;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v3, v11}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setSerialNumber(Ljava/math/BigInteger;)V

    #@64
    .line 112
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@66
    invoke-virtual {v11}, Landroid/security/AndroidKeyPairGeneratorSpec;->getSubjectDN()Ljavax/security/auth/x500/X500Principal;

    #@69
    move-result-object v11

    #@6a
    invoke-virtual {v3, v11}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setSubjectDN(Ljavax/security/auth/x500/X500Principal;)V

    #@6d
    .line 113
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@6f
    invoke-virtual {v11}, Landroid/security/AndroidKeyPairGeneratorSpec;->getSubjectDN()Ljavax/security/auth/x500/X500Principal;

    #@72
    move-result-object v11

    #@73
    invoke-virtual {v3, v11}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setIssuerDN(Ljavax/security/auth/x500/X500Principal;)V

    #@76
    .line 114
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@78
    invoke-virtual {v11}, Landroid/security/AndroidKeyPairGeneratorSpec;->getStartDate()Ljava/util/Date;

    #@7b
    move-result-object v11

    #@7c
    invoke-virtual {v3, v11}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setNotBefore(Ljava/util/Date;)V

    #@7f
    .line 115
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@81
    invoke-virtual {v11}, Landroid/security/AndroidKeyPairGeneratorSpec;->getEndDate()Ljava/util/Date;

    #@84
    move-result-object v11

    #@85
    invoke-virtual {v3, v11}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setNotAfter(Ljava/util/Date;)V

    #@88
    .line 116
    const-string/jumbo v11, "sha1WithRSA"

    #@8b
    invoke-virtual {v3, v11}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->setSignatureAlgorithm(Ljava/lang/String;)V

    #@8e
    .line 120
    :try_start_8e
    invoke-virtual {v3, v7}, Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;->generate(Ljava/security/PrivateKey;)Ljava/security/cert/X509Certificate;
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_8e .. :try_end_91} :catch_da

    #@91
    move-result-object v1

    #@92
    .line 128
    .local v1, cert:Ljava/security/cert/X509Certificate;
    :try_start_92
    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getEncoded()[B
    :try_end_95
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_92 .. :try_end_95} :catch_e8

    #@95
    move-result-object v2

    #@96
    .line 134
    .local v2, certBytes:[B
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@98
    new-instance v12, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v13, "USRCERT_"

    #@9f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v12

    #@a3
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v12

    #@a7
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v12

    #@ab
    invoke-virtual {v11, v12, v2}, Landroid/security/KeyStore;->put(Ljava/lang/String;[B)Z

    #@ae
    move-result v11

    #@af
    if-nez v11, :cond_f6

    #@b1
    .line 135
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@b3
    invoke-static {v11, v0}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@b6
    .line 136
    new-instance v11, Ljava/lang/IllegalStateException;

    #@b8
    const-string v12, "Can\'t store certificate in AndroidKeyStore"

    #@ba
    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@bd
    throw v11

    #@be
    .line 93
    .end local v1           #cert:Ljava/security/cert/X509Certificate;
    .end local v2           #certBytes:[B
    .end local v3           #certGen:Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;
    .end local v6           #keyFact:Ljava/security/KeyFactory;
    .end local v7           #privKey:Ljava/security/PrivateKey;
    .end local v9           #pubKey:Ljava/security/PublicKey;
    .end local v10           #pubKeyBytes:[B
    :catch_be
    move-exception v4

    #@bf
    .line 94
    .local v4, e:Ljava/security/InvalidKeyException;
    new-instance v11, Ljava/lang/RuntimeException;

    #@c1
    const-string v12, "Can\'t get key"

    #@c3
    invoke-direct {v11, v12, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c6
    throw v11

    #@c7
    .line 103
    .end local v4           #e:Ljava/security/InvalidKeyException;
    .restart local v7       #privKey:Ljava/security/PrivateKey;
    .restart local v10       #pubKeyBytes:[B
    :catch_c7
    move-exception v4

    #@c8
    .line 104
    .local v4, e:Ljava/security/NoSuchAlgorithmException;
    new-instance v11, Ljava/lang/IllegalStateException;

    #@ca
    const-string v12, "Can\'t instantiate RSA key generator"

    #@cc
    invoke-direct {v11, v12, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@cf
    throw v11

    #@d0
    .line 105
    .end local v4           #e:Ljava/security/NoSuchAlgorithmException;
    :catch_d0
    move-exception v4

    #@d1
    .line 106
    .local v4, e:Ljava/security/spec/InvalidKeySpecException;
    new-instance v11, Ljava/lang/IllegalStateException;

    #@d3
    const-string/jumbo v12, "keystore returned invalid key encoding"

    #@d6
    invoke-direct {v11, v12, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d9
    throw v11

    #@da
    .line 121
    .end local v4           #e:Ljava/security/spec/InvalidKeySpecException;
    .restart local v3       #certGen:Lcom/android/org/bouncycastle/x509/X509V3CertificateGenerator;
    .restart local v6       #keyFact:Ljava/security/KeyFactory;
    .restart local v9       #pubKey:Ljava/security/PublicKey;
    :catch_da
    move-exception v4

    #@db
    .line 122
    .local v4, e:Ljava/lang/Exception;
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@dd
    invoke-static {v11, v0}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@e0
    .line 123
    new-instance v11, Ljava/lang/IllegalStateException;

    #@e2
    const-string v12, "Can\'t generate certificate"

    #@e4
    invoke-direct {v11, v12, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@e7
    throw v11

    #@e8
    .line 129
    .end local v4           #e:Ljava/lang/Exception;
    .restart local v1       #cert:Ljava/security/cert/X509Certificate;
    :catch_e8
    move-exception v4

    #@e9
    .line 130
    .local v4, e:Ljava/security/cert/CertificateEncodingException;
    iget-object v11, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@eb
    invoke-static {v11, v0}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@ee
    .line 131
    new-instance v11, Ljava/lang/IllegalStateException;

    #@f0
    const-string v12, "Can\'t get encoding of certificate"

    #@f2
    invoke-direct {v11, v12, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@f5
    throw v11

    #@f6
    .line 139
    .end local v4           #e:Ljava/security/cert/CertificateEncodingException;
    .restart local v2       #certBytes:[B
    :cond_f6
    new-instance v11, Ljava/security/KeyPair;

    #@f8
    invoke-direct {v11, v9, v7}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    #@fb
    return-object v11
.end method

.method public initialize(ILjava/security/SecureRandom;)V
    .registers 5
    .parameter "keysize"
    .parameter "random"

    #@0
    .prologue
    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2
    const-string v1, "cannot specify keysize with AndroidKeyPairGenerator"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .registers 6
    .parameter "params"
    .parameter "random"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    #@0
    .prologue
    .line 150
    if-nez p1, :cond_b

    #@2
    .line 151
    new-instance v1, Ljava/security/InvalidAlgorithmParameterException;

    #@4
    const-string/jumbo v2, "must supply params of type AndroidKeyPairGenericSpec"

    #@7
    invoke-direct {v1, v2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 153
    :cond_b
    instance-of v1, p1, Landroid/security/AndroidKeyPairGeneratorSpec;

    #@d
    if-nez v1, :cond_18

    #@f
    .line 154
    new-instance v1, Ljava/security/InvalidAlgorithmParameterException;

    #@11
    const-string/jumbo v2, "params must be of type AndroidKeyPairGeneratorSpec"

    #@14
    invoke-direct {v1, v2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    :cond_18
    move-object v0, p1

    #@19
    .line 158
    check-cast v0, Landroid/security/AndroidKeyPairGeneratorSpec;

    #@1b
    .line 160
    .local v0, spec:Landroid/security/AndroidKeyPairGeneratorSpec;
    iput-object v0, p0, Landroid/security/AndroidKeyPairGenerator;->mSpec:Landroid/security/AndroidKeyPairGeneratorSpec;

    #@1d
    .line 161
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@20
    move-result-object v1

    #@21
    iput-object v1, p0, Landroid/security/AndroidKeyPairGenerator;->mKeyStore:Landroid/security/KeyStore;

    #@23
    .line 162
    return-void
.end method
