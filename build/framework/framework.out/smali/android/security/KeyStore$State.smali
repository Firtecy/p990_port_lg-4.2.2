.class public final enum Landroid/security/KeyStore$State;
.super Ljava/lang/Enum;
.source "KeyStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/security/KeyStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/security/KeyStore$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/security/KeyStore$State;

.field public static final enum LOCKED:Landroid/security/KeyStore$State;

.field public static final enum UNINITIALIZED:Landroid/security/KeyStore$State;

.field public static final enum UNLOCKED:Landroid/security/KeyStore$State;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 51
    new-instance v0, Landroid/security/KeyStore$State;

    #@5
    const-string v1, "UNLOCKED"

    #@7
    invoke-direct {v0, v1, v2}, Landroid/security/KeyStore$State;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@c
    new-instance v0, Landroid/security/KeyStore$State;

    #@e
    const-string v1, "LOCKED"

    #@10
    invoke-direct {v0, v1, v3}, Landroid/security/KeyStore$State;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Landroid/security/KeyStore$State;->LOCKED:Landroid/security/KeyStore$State;

    #@15
    new-instance v0, Landroid/security/KeyStore$State;

    #@17
    const-string v1, "UNINITIALIZED"

    #@19
    invoke-direct {v0, v1, v4}, Landroid/security/KeyStore$State;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Landroid/security/KeyStore$State;->UNINITIALIZED:Landroid/security/KeyStore$State;

    #@1e
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Landroid/security/KeyStore$State;

    #@21
    sget-object v1, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Landroid/security/KeyStore$State;->LOCKED:Landroid/security/KeyStore$State;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Landroid/security/KeyStore$State;->UNINITIALIZED:Landroid/security/KeyStore$State;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Landroid/security/KeyStore$State;->$VALUES:[Landroid/security/KeyStore$State;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/security/KeyStore$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 51
    const-class v0, Landroid/security/KeyStore$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/security/KeyStore$State;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/security/KeyStore$State;
    .registers 1

    #@0
    .prologue
    .line 51
    sget-object v0, Landroid/security/KeyStore$State;->$VALUES:[Landroid/security/KeyStore$State;

    #@2
    invoke-virtual {v0}, [Landroid/security/KeyStore$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/security/KeyStore$State;

    #@8
    return-object v0
.end method
