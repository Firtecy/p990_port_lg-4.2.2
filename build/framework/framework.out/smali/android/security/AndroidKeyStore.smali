.class public Landroid/security/AndroidKeyStore;
.super Ljava/security/KeyStoreSpi;
.source "AndroidKeyStore.java"


# static fields
.field public static final NAME:Ljava/lang/String; = "AndroidKeyStore"


# instance fields
.field private mKeyStore:Landroid/security/KeyStore;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Ljava/security/KeyStoreSpi;-><init>()V

    #@3
    return-void
.end method

.method private getModificationDate(Ljava/lang/String;)Ljava/util/Date;
    .registers 6
    .parameter "alias"

    #@0
    .prologue
    .line 167
    iget-object v2, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@2
    invoke-virtual {v2, p1}, Landroid/security/KeyStore;->getmtime(Ljava/lang/String;)J

    #@5
    move-result-wide v0

    #@6
    .line 168
    .local v0, epochMillis:J
    const-wide/16 v2, -0x1

    #@8
    cmp-long v2, v0, v2

    #@a
    if-nez v2, :cond_e

    #@c
    .line 169
    const/4 v2, 0x0

    #@d
    .line 172
    :goto_d
    return-object v2

    #@e
    :cond_e
    new-instance v2, Ljava/util/Date;

    #@10
    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    #@13
    goto :goto_d
.end method

.method private getUniqueAliases()Ljava/util/Set;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 378
    iget-object v7, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@2
    const-string v8, ""

    #@4
    invoke-virtual {v7, v8}, Landroid/security/KeyStore;->saw(Ljava/lang/String;)[Ljava/lang/String;

    #@7
    move-result-object v6

    #@8
    .line 379
    .local v6, rawAliases:[Ljava/lang/String;
    if-nez v6, :cond_10

    #@a
    .line 380
    new-instance v1, Ljava/util/HashSet;

    #@c
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    #@f
    .line 394
    :cond_f
    return-object v1

    #@10
    .line 383
    :cond_10
    new-instance v1, Ljava/util/HashSet;

    #@12
    array-length v7, v6

    #@13
    invoke-direct {v1, v7}, Ljava/util/HashSet;-><init>(I)V

    #@16
    .line 384
    .local v1, aliases:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    move-object v2, v6

    #@17
    .local v2, arr$:[Ljava/lang/String;
    array-length v5, v2

    #@18
    .local v5, len$:I
    const/4 v3, 0x0

    #@19
    .local v3, i$:I
    :goto_19
    if-ge v3, v5, :cond_f

    #@1b
    aget-object v0, v2, v3

    #@1d
    .line 385
    .local v0, alias:Ljava/lang/String;
    const/16 v7, 0x5f

    #@1f
    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(I)I

    #@22
    move-result v4

    #@23
    .line 386
    .local v4, idx:I
    const/4 v7, -0x1

    #@24
    if-eq v4, v7, :cond_2c

    #@26
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@29
    move-result v7

    #@2a
    if-gt v7, v4, :cond_47

    #@2c
    .line 387
    :cond_2c
    const-string v7, "AndroidKeyStore"

    #@2e
    new-instance v8, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v9, "invalid alias: "

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 384
    :goto_44
    add-int/lit8 v3, v3, 0x1

    #@46
    goto :goto_19

    #@47
    .line 391
    :cond_47
    new-instance v7, Ljava/lang/String;

    #@49
    add-int/lit8 v8, v4, 0x1

    #@4b
    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@52
    invoke-interface {v1, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@55
    goto :goto_44
.end method

.method private isCertificateEntry(Ljava/lang/String;)Z
    .registers 5
    .parameter "alias"

    #@0
    .prologue
    .line 432
    if-nez p1, :cond_a

    #@2
    .line 433
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "alias == null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 436
    :cond_a
    iget-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "CACERT_"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    return v0
.end method

.method private isKeyEntry(Ljava/lang/String;)Z
    .registers 5
    .parameter "alias"

    #@0
    .prologue
    .line 424
    if-nez p1, :cond_a

    #@2
    .line 425
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "alias == null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 428
    :cond_a
    iget-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "USRPKEY_"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    return v0
.end method

.method private setPrivateKeyEntry(Ljava/lang/String;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V
    .registers 23
    .parameter "alias"
    .parameter "key"
    .parameter "chain"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    const/4 v7, 0x0

    #@1
    .line 213
    .local v7, keyBytes:[B
    move-object/from16 v0, p2

    #@3
    instance-of v0, v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    #@5
    move/from16 v16, v0

    #@7
    if-eqz v16, :cond_5a

    #@9
    move-object/from16 v16, p2

    #@b
    .line 214
    check-cast v16, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    #@d
    invoke-virtual/range {v16 .. v16}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getPkeyAlias()Ljava/lang/String;

    #@10
    move-result-object v11

    #@11
    .line 222
    .local v11, pkeyAlias:Ljava/lang/String;
    :goto_11
    if-eqz v11, :cond_7f

    #@13
    const-string v16, "USRPKEY_"

    #@15
    move-object/from16 v0, v16

    #@17
    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1a
    move-result v16

    #@1b
    if-eqz v16, :cond_7f

    #@1d
    .line 223
    const-string v16, "USRPKEY_"

    #@1f
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    #@22
    move-result v16

    #@23
    move/from16 v0, v16

    #@25
    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v9

    #@29
    .line 224
    .local v9, keySubalias:Ljava/lang/String;
    move-object/from16 v0, p1

    #@2b
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v16

    #@2f
    if-nez v16, :cond_6d

    #@31
    .line 225
    new-instance v16, Ljava/security/KeyStoreException;

    #@33
    new-instance v17, Ljava/lang/StringBuilder;

    #@35
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v18, "Can only replace keys with same alias: "

    #@3a
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v17

    #@3e
    move-object/from16 v0, v17

    #@40
    move-object/from16 v1, p1

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v17

    #@46
    const-string v18, " != "

    #@48
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v17

    #@4c
    move-object/from16 v0, v17

    #@4e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v17

    #@52
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v17

    #@56
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@59
    throw v16

    #@5a
    .line 215
    .end local v9           #keySubalias:Ljava/lang/String;
    .end local v11           #pkeyAlias:Ljava/lang/String;
    :cond_5a
    move-object/from16 v0, p2

    #@5c
    instance-of v0, v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    #@5e
    move/from16 v16, v0

    #@60
    if-eqz v16, :cond_6b

    #@62
    move-object/from16 v16, p2

    #@64
    .line 216
    check-cast v16, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    #@66
    invoke-virtual/range {v16 .. v16}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;->getPkeyAlias()Ljava/lang/String;

    #@69
    move-result-object v11

    #@6a
    .restart local v11       #pkeyAlias:Ljava/lang/String;
    goto :goto_11

    #@6b
    .line 218
    .end local v11           #pkeyAlias:Ljava/lang/String;
    :cond_6b
    const/4 v11, 0x0

    #@6c
    .restart local v11       #pkeyAlias:Ljava/lang/String;
    goto :goto_11

    #@6d
    .line 229
    .restart local v9       #keySubalias:Ljava/lang/String;
    :cond_6d
    const/4 v12, 0x0

    #@6e
    .line 248
    .end local v9           #keySubalias:Ljava/lang/String;
    .local v12, shouldReplacePrivateKey:Z
    :goto_6e
    if-eqz p3, :cond_77

    #@70
    move-object/from16 v0, p3

    #@72
    array-length v0, v0

    #@73
    move/from16 v16, v0

    #@75
    if-nez v16, :cond_a7

    #@77
    .line 249
    :cond_77
    new-instance v16, Ljava/security/KeyStoreException;

    #@79
    const-string v17, "Must supply at least one Certificate with PrivateKey"

    #@7b
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@7e
    throw v16

    #@7f
    .line 232
    .end local v12           #shouldReplacePrivateKey:Z
    :cond_7f
    invoke-interface/range {p2 .. p2}, Ljava/security/PrivateKey;->getFormat()Ljava/lang/String;

    #@82
    move-result-object v8

    #@83
    .line 233
    .local v8, keyFormat:Ljava/lang/String;
    if-eqz v8, :cond_8f

    #@85
    const-string v16, "PKCS#8"

    #@87
    move-object/from16 v0, v16

    #@89
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v16

    #@8d
    if-nez v16, :cond_97

    #@8f
    .line 234
    :cond_8f
    new-instance v16, Ljava/security/KeyStoreException;

    #@91
    const-string v17, "Only PrivateKeys that can be encoded into PKCS#8 are supported"

    #@93
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@96
    throw v16

    #@97
    .line 239
    :cond_97
    invoke-interface/range {p2 .. p2}, Ljava/security/PrivateKey;->getEncoded()[B

    #@9a
    move-result-object v7

    #@9b
    .line 240
    if-nez v7, :cond_a5

    #@9d
    .line 241
    new-instance v16, Ljava/security/KeyStoreException;

    #@9f
    const-string v17, "PrivateKey has no encoding"

    #@a1
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@a4
    throw v16

    #@a5
    .line 244
    :cond_a5
    const/4 v12, 0x1

    #@a6
    .restart local v12       #shouldReplacePrivateKey:Z
    goto :goto_6e

    #@a7
    .line 253
    .end local v8           #keyFormat:Ljava/lang/String;
    :cond_a7
    move-object/from16 v0, p3

    #@a9
    array-length v0, v0

    #@aa
    move/from16 v16, v0

    #@ac
    move/from16 v0, v16

    #@ae
    new-array v15, v0, [Ljava/security/cert/X509Certificate;

    #@b0
    .line 254
    .local v15, x509chain:[Ljava/security/cert/X509Certificate;
    const/4 v6, 0x0

    #@b1
    .local v6, i:I
    :goto_b1
    move-object/from16 v0, p3

    #@b3
    array-length v0, v0

    #@b4
    move/from16 v16, v0

    #@b6
    move/from16 v0, v16

    #@b8
    if-ge v6, v0, :cond_111

    #@ba
    .line 255
    const-string v16, "X.509"

    #@bc
    aget-object v17, p3, v6

    #@be
    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/Certificate;->getType()Ljava/lang/String;

    #@c1
    move-result-object v17

    #@c2
    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v16

    #@c6
    if-nez v16, :cond_e3

    #@c8
    .line 256
    new-instance v16, Ljava/security/KeyStoreException;

    #@ca
    new-instance v17, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v18, "Certificates must be in X.509 format: invalid cert #"

    #@d1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v17

    #@d5
    move-object/from16 v0, v17

    #@d7
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@da
    move-result-object v17

    #@db
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v17

    #@df
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@e2
    throw v16

    #@e3
    .line 260
    :cond_e3
    aget-object v16, p3, v6

    #@e5
    move-object/from16 v0, v16

    #@e7
    instance-of v0, v0, Ljava/security/cert/X509Certificate;

    #@e9
    move/from16 v16, v0

    #@eb
    if-nez v16, :cond_108

    #@ed
    .line 261
    new-instance v16, Ljava/security/KeyStoreException;

    #@ef
    new-instance v17, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    const-string v18, "Certificates must be in X.509 format: invalid cert #"

    #@f6
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v17

    #@fa
    move-object/from16 v0, v17

    #@fc
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v17

    #@100
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v17

    #@104
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@107
    throw v16

    #@108
    .line 265
    :cond_108
    aget-object v16, p3, v6

    #@10a
    check-cast v16, Ljava/security/cert/X509Certificate;

    #@10c
    aput-object v16, v15, v6

    #@10e
    .line 254
    add-int/lit8 v6, v6, 0x1

    #@110
    goto :goto_b1

    #@111
    .line 270
    :cond_111
    const/16 v16, 0x0

    #@113
    :try_start_113
    aget-object v16, v15, v16

    #@115
    invoke-virtual/range {v16 .. v16}, Ljava/security/cert/X509Certificate;->getEncoded()[B
    :try_end_118
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_113 .. :try_end_118} :catch_14e

    #@118
    move-result-object v14

    #@119
    .line 281
    .local v14, userCertBytes:[B
    move-object/from16 v0, p3

    #@11b
    array-length v0, v0

    #@11c
    move/from16 v16, v0

    #@11e
    const/16 v17, 0x1

    #@120
    move/from16 v0, v16

    #@122
    move/from16 v1, v17

    #@124
    if-le v0, v1, :cond_19e

    #@126
    .line 286
    array-length v0, v15

    #@127
    move/from16 v16, v0

    #@129
    add-int/lit8 v16, v16, -0x1

    #@12b
    move/from16 v0, v16

    #@12d
    new-array v3, v0, [[B

    #@12f
    .line 287
    .local v3, certsBytes:[[B
    const/4 v13, 0x0

    #@130
    .line 288
    .local v13, totalCertLength:I
    const/4 v6, 0x0

    #@131
    :goto_131
    array-length v0, v3

    #@132
    move/from16 v16, v0

    #@134
    move/from16 v0, v16

    #@136
    if-ge v6, v0, :cond_17b

    #@138
    .line 290
    add-int/lit8 v16, v6, 0x1

    #@13a
    :try_start_13a
    aget-object v16, v15, v16

    #@13c
    invoke-virtual/range {v16 .. v16}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    #@13f
    move-result-object v16

    #@140
    aput-object v16, v3, v6

    #@142
    .line 291
    aget-object v16, v3, v6

    #@144
    move-object/from16 v0, v16

    #@146
    array-length v0, v0

    #@147
    move/from16 v16, v0
    :try_end_149
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_13a .. :try_end_149} :catch_15b

    #@149
    add-int v13, v13, v16

    #@14b
    .line 288
    add-int/lit8 v6, v6, 0x1

    #@14d
    goto :goto_131

    #@14e
    .line 271
    .end local v3           #certsBytes:[[B
    .end local v13           #totalCertLength:I
    .end local v14           #userCertBytes:[B
    :catch_14e
    move-exception v5

    #@14f
    .line 272
    .local v5, e:Ljava/security/cert/CertificateEncodingException;
    new-instance v16, Ljava/security/KeyStoreException;

    #@151
    const-string v17, "Couldn\'t encode certificate #1"

    #@153
    move-object/from16 v0, v16

    #@155
    move-object/from16 v1, v17

    #@157
    invoke-direct {v0, v1, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@15a
    throw v16

    #@15b
    .line 292
    .end local v5           #e:Ljava/security/cert/CertificateEncodingException;
    .restart local v3       #certsBytes:[[B
    .restart local v13       #totalCertLength:I
    .restart local v14       #userCertBytes:[B
    :catch_15b
    move-exception v5

    #@15c
    .line 293
    .restart local v5       #e:Ljava/security/cert/CertificateEncodingException;
    new-instance v16, Ljava/security/KeyStoreException;

    #@15e
    new-instance v17, Ljava/lang/StringBuilder;

    #@160
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v18, "Can\'t encode Certificate #"

    #@165
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v17

    #@169
    move-object/from16 v0, v17

    #@16b
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v17

    #@16f
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@172
    move-result-object v17

    #@173
    move-object/from16 v0, v16

    #@175
    move-object/from16 v1, v17

    #@177
    invoke-direct {v0, v1, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@17a
    throw v16

    #@17b
    .line 301
    .end local v5           #e:Ljava/security/cert/CertificateEncodingException;
    :cond_17b
    new-array v4, v13, [B

    #@17d
    .line 302
    .local v4, chainBytes:[B
    const/4 v10, 0x0

    #@17e
    .line 303
    .local v10, outputOffset:I
    const/4 v6, 0x0

    #@17f
    :goto_17f
    array-length v0, v3

    #@180
    move/from16 v16, v0

    #@182
    move/from16 v0, v16

    #@184
    if-ge v6, v0, :cond_19f

    #@186
    .line 304
    aget-object v16, v3, v6

    #@188
    move-object/from16 v0, v16

    #@18a
    array-length v2, v0

    #@18b
    .line 305
    .local v2, certLength:I
    aget-object v16, v3, v6

    #@18d
    const/16 v17, 0x0

    #@18f
    move-object/from16 v0, v16

    #@191
    move/from16 v1, v17

    #@193
    invoke-static {v0, v1, v4, v10, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@196
    .line 306
    add-int/2addr v10, v2

    #@197
    .line 307
    const/16 v16, 0x0

    #@199
    aput-object v16, v3, v6

    #@19b
    .line 303
    add-int/lit8 v6, v6, 0x1

    #@19d
    goto :goto_17f

    #@19e
    .line 310
    .end local v2           #certLength:I
    .end local v3           #certsBytes:[[B
    .end local v4           #chainBytes:[B
    .end local v10           #outputOffset:I
    .end local v13           #totalCertLength:I
    :cond_19e
    const/4 v4, 0x0

    #@19f
    .line 317
    .restart local v4       #chainBytes:[B
    :cond_19f
    if-eqz v12, :cond_1ec

    #@1a1
    .line 318
    move-object/from16 v0, p0

    #@1a3
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1a5
    move-object/from16 v16, v0

    #@1a7
    move-object/from16 v0, v16

    #@1a9
    move-object/from16 v1, p1

    #@1ab
    invoke-static {v0, v1}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@1ae
    .line 323
    :goto_1ae
    if-eqz v12, :cond_1fa

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1b4
    move-object/from16 v16, v0

    #@1b6
    new-instance v17, Ljava/lang/StringBuilder;

    #@1b8
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@1bb
    const-string v18, "USRPKEY_"

    #@1bd
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v17

    #@1c1
    move-object/from16 v0, v17

    #@1c3
    move-object/from16 v1, p1

    #@1c5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v17

    #@1c9
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cc
    move-result-object v17

    #@1cd
    move-object/from16 v0, v16

    #@1cf
    move-object/from16 v1, v17

    #@1d1
    invoke-virtual {v0, v1, v7}, Landroid/security/KeyStore;->importKey(Ljava/lang/String;[B)Z

    #@1d4
    move-result v16

    #@1d5
    if-nez v16, :cond_1fa

    #@1d7
    .line 325
    move-object/from16 v0, p0

    #@1d9
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1db
    move-object/from16 v16, v0

    #@1dd
    move-object/from16 v0, v16

    #@1df
    move-object/from16 v1, p1

    #@1e1
    invoke-static {v0, v1}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@1e4
    .line 326
    new-instance v16, Ljava/security/KeyStoreException;

    #@1e6
    const-string v17, "Couldn\'t put private key in keystore"

    #@1e8
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@1eb
    throw v16

    #@1ec
    .line 320
    :cond_1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1f0
    move-object/from16 v16, v0

    #@1f2
    move-object/from16 v0, v16

    #@1f4
    move-object/from16 v1, p1

    #@1f6
    invoke-static {v0, v1}, Landroid/security/Credentials;->deleteCertificateTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@1f9
    goto :goto_1ae

    #@1fa
    .line 327
    :cond_1fa
    move-object/from16 v0, p0

    #@1fc
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1fe
    move-object/from16 v16, v0

    #@200
    new-instance v17, Ljava/lang/StringBuilder;

    #@202
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v18, "USRCERT_"

    #@207
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v17

    #@20b
    move-object/from16 v0, v17

    #@20d
    move-object/from16 v1, p1

    #@20f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v17

    #@213
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@216
    move-result-object v17

    #@217
    move-object/from16 v0, v16

    #@219
    move-object/from16 v1, v17

    #@21b
    invoke-virtual {v0, v1, v14}, Landroid/security/KeyStore;->put(Ljava/lang/String;[B)Z

    #@21e
    move-result v16

    #@21f
    if-nez v16, :cond_236

    #@221
    .line 328
    move-object/from16 v0, p0

    #@223
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@225
    move-object/from16 v16, v0

    #@227
    move-object/from16 v0, v16

    #@229
    move-object/from16 v1, p1

    #@22b
    invoke-static {v0, v1}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@22e
    .line 329
    new-instance v16, Ljava/security/KeyStoreException;

    #@230
    const-string v17, "Couldn\'t put certificate #1 in keystore"

    #@232
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@235
    throw v16

    #@236
    .line 330
    :cond_236
    if-eqz v4, :cond_274

    #@238
    move-object/from16 v0, p0

    #@23a
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@23c
    move-object/from16 v16, v0

    #@23e
    new-instance v17, Ljava/lang/StringBuilder;

    #@240
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@243
    const-string v18, "CACERT_"

    #@245
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v17

    #@249
    move-object/from16 v0, v17

    #@24b
    move-object/from16 v1, p1

    #@24d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v17

    #@251
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@254
    move-result-object v17

    #@255
    move-object/from16 v0, v16

    #@257
    move-object/from16 v1, v17

    #@259
    invoke-virtual {v0, v1, v4}, Landroid/security/KeyStore;->put(Ljava/lang/String;[B)Z

    #@25c
    move-result v16

    #@25d
    if-nez v16, :cond_274

    #@25f
    .line 332
    move-object/from16 v0, p0

    #@261
    iget-object v0, v0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@263
    move-object/from16 v16, v0

    #@265
    move-object/from16 v0, v16

    #@267
    move-object/from16 v1, p1

    #@269
    invoke-static {v0, v1}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@26c
    .line 333
    new-instance v16, Ljava/security/KeyStoreException;

    #@26e
    const-string v17, "Couldn\'t put certificate chain in keystore"

    #@270
    invoke-direct/range {v16 .. v17}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@273
    throw v16

    #@274
    .line 335
    :cond_274
    return-void
.end method

.method private static toCertificate([B)Ljava/security/cert/X509Certificate;
    .registers 5
    .parameter "bytes"

    #@0
    .prologue
    .line 145
    :try_start_0
    const-string v2, "X.509"

    #@2
    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@5
    move-result-object v0

    #@6
    .line 146
    .local v0, certFactory:Ljava/security/cert/CertificateFactory;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    #@8
    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@b
    invoke-virtual {v0, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/security/cert/X509Certificate;
    :try_end_11
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 150
    .end local v0           #certFactory:Ljava/security/cert/CertificateFactory;
    :goto_11
    return-object v2

    #@12
    .line 148
    :catch_12
    move-exception v1

    #@13
    .line 149
    .local v1, e:Ljava/security/cert/CertificateException;
    const-string v2, "AndroidKeyStore"

    #@15
    const-string v3, "Couldn\'t parse certificate in keystore"

    #@17
    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 150
    const/4 v2, 0x0

    #@1b
    goto :goto_11
.end method

.method private static toCertificates([B)Ljava/util/Collection;
    .registers 5
    .parameter "bytes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/Collection",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 157
    :try_start_0
    const-string v2, "X.509"

    #@2
    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@5
    move-result-object v0

    #@6
    .line 158
    .local v0, certFactory:Ljava/security/cert/CertificateFactory;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    #@8
    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@b
    invoke-virtual {v0, v2}, Ljava/security/cert/CertificateFactory;->generateCertificates(Ljava/io/InputStream;)Ljava/util/Collection;
    :try_end_e
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_e} :catch_10

    #@e
    move-result-object v2

    #@f
    .line 162
    .end local v0           #certFactory:Ljava/security/cert/CertificateFactory;
    :goto_f
    return-object v2

    #@10
    .line 160
    :catch_10
    move-exception v1

    #@11
    .line 161
    .local v1, e:Ljava/security/cert/CertificateException;
    const-string v2, "AndroidKeyStore"

    #@13
    const-string v3, "Couldn\'t parse certificates in keystore"

    #@15
    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 162
    new-instance v2, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@1d
    goto :goto_f
.end method


# virtual methods
.method public engineAliases()Ljava/util/Enumeration;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 399
    invoke-direct {p0}, Landroid/security/AndroidKeyStore;->getUniqueAliases()Ljava/util/Set;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public engineContainsAlias(Ljava/lang/String;)Z
    .registers 5
    .parameter "alias"

    #@0
    .prologue
    .line 404
    if-nez p1, :cond_a

    #@2
    .line 405
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "alias == null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 408
    :cond_a
    iget-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "USRPKEY_"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_5b

    #@25
    iget-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "USRCERT_"

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_5b

    #@40
    iget-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "CACERT_"

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->contains(Ljava/lang/String;)Z

    #@58
    move-result v0

    #@59
    if-eqz v0, :cond_5d

    #@5b
    :cond_5b
    const/4 v0, 0x1

    #@5c
    :goto_5c
    return v0

    #@5d
    :cond_5d
    const/4 v0, 0x0

    #@5e
    goto :goto_5c
.end method

.method public engineDeleteEntry(Ljava/lang/String;)V
    .registers 5
    .parameter "alias"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    #@0
    .prologue
    .line 368
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_d

    #@6
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isCertificateEntry(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 375
    :cond_c
    return-void

    #@d
    .line 372
    :cond_d
    iget-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@f
    invoke-static {v0, p1}, Landroid/security/Credentials;->deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_c

    #@15
    .line 373
    new-instance v0, Ljava/security/KeyStoreException;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "No such entry "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v0
.end method

.method public engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
    .registers 6
    .parameter "alias"

    #@0
    .prologue
    .line 126
    if-nez p1, :cond_a

    #@2
    .line 127
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string v2, "alias == null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 130
    :cond_a
    iget-object v1, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "USRCERT_"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@22
    move-result-object v0

    #@23
    .line 131
    .local v0, certificate:[B
    if-eqz v0, :cond_2a

    #@25
    .line 132
    invoke-static {v0}, Landroid/security/AndroidKeyStore;->toCertificate([B)Ljava/security/cert/X509Certificate;

    #@28
    move-result-object v1

    #@29
    .line 140
    :goto_29
    return-object v1

    #@2a
    .line 135
    :cond_2a
    iget-object v1, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "CACERT_"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v1, v2}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@42
    move-result-object v0

    #@43
    .line 136
    if-eqz v0, :cond_4a

    #@45
    .line 137
    invoke-static {v0}, Landroid/security/AndroidKeyStore;->toCertificate([B)Ljava/security/cert/X509Certificate;

    #@48
    move-result-object v1

    #@49
    goto :goto_29

    #@4a
    .line 140
    :cond_4a
    const/4 v1, 0x0

    #@4b
    goto :goto_29
.end method

.method public engineGetCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;
    .registers 15
    .parameter "cert"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 446
    if-nez p1, :cond_5

    #@3
    move-object v0, v9

    #@4
    .line 494
    :goto_4
    return-object v0

    #@5
    .line 450
    :cond_5
    new-instance v8, Ljava/util/HashSet;

    #@7
    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    #@a
    .line 458
    .local v8, nonCaEntries:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v10, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@c
    const-string v11, "USRCERT_"

    #@e
    invoke-virtual {v10, v11}, Landroid/security/KeyStore;->saw(Ljava/lang/String;)[Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 459
    .local v4, certAliases:[Ljava/lang/String;
    move-object v1, v4

    #@13
    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    #@14
    .local v7, len$:I
    const/4 v6, 0x0

    #@15
    .local v6, i$:I
    :goto_15
    if-ge v6, v7, :cond_45

    #@17
    aget-object v0, v1, v6

    #@19
    .line 460
    .local v0, alias:Ljava/lang/String;
    iget-object v10, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1b
    new-instance v11, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v12, "USRCERT_"

    #@22
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v11

    #@26
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v11

    #@2a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v11

    #@2e
    invoke-virtual {v10, v11}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@31
    move-result-object v5

    #@32
    .line 461
    .local v5, certBytes:[B
    if-nez v5, :cond_37

    #@34
    .line 459
    :cond_34
    add-int/lit8 v6, v6, 0x1

    #@36
    goto :goto_15

    #@37
    .line 465
    :cond_37
    invoke-static {v5}, Landroid/security/AndroidKeyStore;->toCertificate([B)Ljava/security/cert/X509Certificate;

    #@3a
    move-result-object v2

    #@3b
    .line 466
    .local v2, c:Ljava/security/cert/Certificate;
    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@3e
    .line 468
    invoke-virtual {p1, v2}, Ljava/security/cert/Certificate;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v10

    #@42
    if-eqz v10, :cond_34

    #@44
    goto :goto_4

    #@45
    .line 477
    .end local v0           #alias:Ljava/lang/String;
    .end local v2           #c:Ljava/security/cert/Certificate;
    .end local v5           #certBytes:[B
    :cond_45
    iget-object v10, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@47
    const-string v11, "CACERT_"

    #@49
    invoke-virtual {v10, v11}, Landroid/security/KeyStore;->saw(Ljava/lang/String;)[Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    .line 478
    .local v3, caAliases:[Ljava/lang/String;
    move-object v1, v3

    #@4e
    array-length v7, v1

    #@4f
    const/4 v6, 0x0

    #@50
    :goto_50
    if-ge v6, v7, :cond_9d

    #@52
    aget-object v0, v1, v6

    #@54
    .line 479
    .restart local v0       #alias:Ljava/lang/String;
    invoke-interface {v8, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@57
    move-result v10

    #@58
    if-eqz v10, :cond_5d

    #@5a
    .line 478
    :cond_5a
    add-int/lit8 v6, v6, 0x1

    #@5c
    goto :goto_50

    #@5d
    .line 483
    :cond_5d
    iget-object v10, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@5f
    new-instance v11, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v12, "CACERT_"

    #@66
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v11

    #@6a
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v11

    #@6e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v11

    #@72
    invoke-virtual {v10, v11}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@75
    move-result-object v5

    #@76
    .line 484
    .restart local v5       #certBytes:[B
    if-eqz v5, :cond_5a

    #@78
    .line 488
    iget-object v10, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@7a
    new-instance v11, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v12, "CACERT_"

    #@81
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v11

    #@85
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v11

    #@89
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v11

    #@8d
    invoke-virtual {v10, v11}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@90
    move-result-object v10

    #@91
    invoke-static {v10}, Landroid/security/AndroidKeyStore;->toCertificate([B)Ljava/security/cert/X509Certificate;

    #@94
    move-result-object v2

    #@95
    .line 489
    .restart local v2       #c:Ljava/security/cert/Certificate;
    invoke-virtual {p1, v2}, Ljava/security/cert/Certificate;->equals(Ljava/lang/Object;)Z

    #@98
    move-result v10

    #@99
    if-eqz v10, :cond_5a

    #@9b
    goto/16 :goto_4

    #@9d
    .end local v0           #alias:Ljava/lang/String;
    .end local v2           #c:Ljava/security/cert/Certificate;
    .end local v5           #certBytes:[B
    :cond_9d
    move-object v0, v9

    #@9e
    .line 494
    goto/16 :goto_4
.end method

.method public engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    .registers 12
    .parameter "alias"

    #@0
    .prologue
    .line 93
    if-nez p1, :cond_a

    #@2
    .line 94
    new-instance v7, Ljava/lang/NullPointerException;

    #@4
    const-string v8, "alias == null"

    #@6
    invoke-direct {v7, v8}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v7

    #@a
    .line 97
    :cond_a
    invoke-virtual {p0, p1}, Landroid/security/AndroidKeyStore;->engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    #@d
    move-result-object v6

    #@e
    check-cast v6, Ljava/security/cert/X509Certificate;

    #@10
    .line 98
    .local v6, leaf:Ljava/security/cert/X509Certificate;
    if-nez v6, :cond_14

    #@12
    .line 99
    const/4 v2, 0x0

    #@13
    .line 121
    :goto_13
    return-object v2

    #@14
    .line 104
    :cond_14
    iget-object v7, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@16
    new-instance v8, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v9, "CACERT_"

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v7, v8}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@2c
    move-result-object v0

    #@2d
    .line 105
    .local v0, caBytes:[B
    if-eqz v0, :cond_52

    #@2f
    .line 106
    invoke-static {v0}, Landroid/security/AndroidKeyStore;->toCertificates([B)Ljava/util/Collection;

    #@32
    move-result-object v1

    #@33
    .line 108
    .local v1, caChain:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/security/cert/X509Certificate;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@36
    move-result v7

    #@37
    add-int/lit8 v7, v7, 0x1

    #@39
    new-array v2, v7, [Ljava/security/cert/Certificate;

    #@3b
    .line 110
    .local v2, caList:[Ljava/security/cert/Certificate;
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v5

    #@3f
    .line 111
    .local v5, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/security/cert/X509Certificate;>;"
    const/4 v3, 0x1

    #@40
    .line 112
    .local v3, i:I
    :goto_40
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@43
    move-result v7

    #@44
    if-eqz v7, :cond_55

    #@46
    .line 113
    add-int/lit8 v4, v3, 0x1

    #@48
    .end local v3           #i:I
    .local v4, i:I
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4b
    move-result-object v7

    #@4c
    check-cast v7, Ljava/security/cert/Certificate;

    #@4e
    aput-object v7, v2, v3

    #@50
    move v3, v4

    #@51
    .end local v4           #i:I
    .restart local v3       #i:I
    goto :goto_40

    #@52
    .line 116
    .end local v1           #caChain:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/security/cert/X509Certificate;>;"
    .end local v2           #caList:[Ljava/security/cert/Certificate;
    .end local v3           #i:I
    .end local v5           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/security/cert/X509Certificate;>;"
    :cond_52
    const/4 v7, 0x1

    #@53
    new-array v2, v7, [Ljava/security/cert/Certificate;

    #@55
    .line 119
    .restart local v2       #caList:[Ljava/security/cert/Certificate;
    :cond_55
    const/4 v7, 0x0

    #@56
    aput-object v6, v2, v7

    #@58
    goto :goto_13
.end method

.method public engineGetCreationDate(Ljava/lang/String;)Ljava/util/Date;
    .registers 5
    .parameter "alias"

    #@0
    .prologue
    .line 177
    if-nez p1, :cond_a

    #@2
    .line 178
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string v2, "alias == null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 181
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "USRPKEY_"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {p0, v1}, Landroid/security/AndroidKeyStore;->getModificationDate(Ljava/lang/String;)Ljava/util/Date;

    #@20
    move-result-object v0

    #@21
    .line 182
    .local v0, d:Ljava/util/Date;
    if-eqz v0, :cond_25

    #@23
    move-object v1, v0

    #@24
    .line 191
    :goto_24
    return-object v1

    #@25
    .line 186
    :cond_25
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, "USRCERT_"

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-direct {p0, v1}, Landroid/security/AndroidKeyStore;->getModificationDate(Ljava/lang/String;)Ljava/util/Date;

    #@3b
    move-result-object v0

    #@3c
    .line 187
    if-eqz v0, :cond_40

    #@3e
    move-object v1, v0

    #@3f
    .line 188
    goto :goto_24

    #@40
    .line 191
    :cond_40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "CACERT_"

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-direct {p0, v1}, Landroid/security/AndroidKeyStore;->getModificationDate(Ljava/lang/String;)Ljava/util/Date;

    #@56
    move-result-object v1

    #@57
    goto :goto_24
.end method

.method public engineGetKey(Ljava/lang/String;[C)Ljava/security/Key;
    .registers 8
    .parameter "alias"
    .parameter "password"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/UnrecoverableKeyException;
        }
    .end annotation

    #@0
    .prologue
    .line 77
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_8

    #@6
    .line 78
    const/4 v3, 0x0

    #@7
    .line 83
    :goto_7
    return-object v3

    #@8
    .line 81
    :cond_8
    const-string/jumbo v3, "keystore"

    #@b
    invoke-static {v3}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;->getInstance(Ljava/lang/String;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;

    #@e
    move-result-object v1

    #@f
    .line 83
    .local v1, engine:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;
    :try_start_f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "USRPKEY_"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v1, v3}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLEngine;->getPrivateKeyById(Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_25
    .catch Ljava/security/InvalidKeyException; {:try_start_f .. :try_end_25} :catch_27

    #@25
    move-result-object v3

    #@26
    goto :goto_7

    #@27
    .line 84
    :catch_27
    move-exception v0

    #@28
    .line 85
    .local v0, e:Ljava/security/InvalidKeyException;
    new-instance v2, Ljava/security/UnrecoverableKeyException;

    #@2a
    const-string v3, "Can\'t get key"

    #@2c
    invoke-direct {v2, v3}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    #@2f
    .line 86
    .local v2, t:Ljava/security/UnrecoverableKeyException;
    invoke-virtual {v2, v0}, Ljava/security/UnrecoverableKeyException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@32
    .line 87
    throw v2
.end method

.method public engineIsCertificateEntry(Ljava/lang/String;)Z
    .registers 3
    .parameter "alias"

    #@0
    .prologue
    .line 441
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isCertificateEntry(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public engineIsKeyEntry(Ljava/lang/String;)Z
    .registers 3
    .parameter "alias"

    #@0
    .prologue
    .line 420
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public engineLoad(Ljava/io/InputStream;[C)V
    .registers 5
    .parameter "stream"
    .parameter "password"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    #@0
    .prologue
    .line 506
    if-eqz p1, :cond_a

    #@2
    .line 507
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "InputStream not supported"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 510
    :cond_a
    if-eqz p2, :cond_15

    #@c
    .line 511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v1, "password not supported"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 515
    :cond_15
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1b
    .line 516
    return-void
.end method

.method public engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    .registers 8
    .parameter "alias"
    .parameter "cert"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    #@0
    .prologue
    .line 345
    invoke-direct {p0, p1}, Landroid/security/AndroidKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_e

    #@6
    .line 346
    new-instance v2, Ljava/security/KeyStoreException;

    #@8
    const-string v3, "Entry exists and is not a trusted certificate"

    #@a
    invoke-direct {v2, v3}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 350
    :cond_e
    if-nez p2, :cond_18

    #@10
    .line 351
    new-instance v2, Ljava/lang/NullPointerException;

    #@12
    const-string v3, "cert == null"

    #@14
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 356
    :cond_18
    :try_start_18
    invoke-virtual {p2}, Ljava/security/cert/Certificate;->getEncoded()[B
    :try_end_1b
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_18 .. :try_end_1b} :catch_3f

    #@1b
    move-result-object v1

    #@1c
    .line 361
    .local v1, encoded:[B
    iget-object v2, p0, Landroid/security/AndroidKeyStore;->mKeyStore:Landroid/security/KeyStore;

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "CACERT_"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v2, v3, v1}, Landroid/security/KeyStore;->put(Ljava/lang/String;[B)Z

    #@34
    move-result v2

    #@35
    if-nez v2, :cond_46

    #@37
    .line 362
    new-instance v2, Ljava/security/KeyStoreException;

    #@39
    const-string v3, "Couldn\'t insert certificate; is KeyStore initialized?"

    #@3b
    invoke-direct {v2, v3}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v2

    #@3f
    .line 357
    .end local v1           #encoded:[B
    :catch_3f
    move-exception v0

    #@40
    .line 358
    .local v0, e:Ljava/security/cert/CertificateEncodingException;
    new-instance v2, Ljava/security/KeyStoreException;

    #@42
    invoke-direct {v2, v0}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/Throwable;)V

    #@45
    throw v2

    #@46
    .line 364
    .end local v0           #e:Ljava/security/cert/CertificateEncodingException;
    .restart local v1       #encoded:[B
    :cond_46
    return-void
.end method

.method public engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
    .registers 7
    .parameter "alias"
    .parameter "key"
    .parameter "password"
    .parameter "chain"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    #@0
    .prologue
    .line 197
    if-eqz p3, :cond_d

    #@2
    array-length v0, p3

    #@3
    if-lez v0, :cond_d

    #@5
    .line 198
    new-instance v0, Ljava/security/KeyStoreException;

    #@7
    const-string v1, "entries cannot be protected with passwords"

    #@9
    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 201
    :cond_d
    instance-of v0, p2, Ljava/security/PrivateKey;

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 202
    check-cast p2, Ljava/security/PrivateKey;

    #@13
    .end local p2
    invoke-direct {p0, p1, p2, p4}, Landroid/security/AndroidKeyStore;->setPrivateKeyEntry(Ljava/lang/String;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V

    #@16
    .line 206
    return-void

    #@17
    .line 204
    .restart local p2
    :cond_17
    new-instance v0, Ljava/security/KeyStoreException;

    #@19
    const-string v1, "Only PrivateKeys are supported"

    #@1b
    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0
.end method

.method public engineSetKeyEntry(Ljava/lang/String;[B[Ljava/security/cert/Certificate;)V
    .registers 6
    .parameter "alias"
    .parameter "userKey"
    .parameter "chain"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    #@0
    .prologue
    .line 340
    new-instance v0, Ljava/security/KeyStoreException;

    #@2
    const-string v1, "Operation not supported because key encoding is unknown"

    #@4
    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public engineSize()I
    .registers 2

    #@0
    .prologue
    .line 415
    invoke-direct {p0}, Landroid/security/AndroidKeyStore;->getUniqueAliases()Ljava/util/Set;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/Set;->size()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public engineStore(Ljava/io/OutputStream;[C)V
    .registers 5
    .parameter "stream"
    .parameter "password"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    #@0
    .prologue
    .line 500
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Can not serialize AndroidKeyStore to OutputStream"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
