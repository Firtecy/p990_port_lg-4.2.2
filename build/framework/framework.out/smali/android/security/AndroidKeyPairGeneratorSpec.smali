.class public Landroid/security/AndroidKeyPairGeneratorSpec;
.super Ljava/lang/Object;
.source "AndroidKeyPairGeneratorSpec.java"

# interfaces
.implements Ljava/security/spec/AlgorithmParameterSpec;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mEndDate:Ljava/util/Date;

.field private final mKeystoreAlias:Ljava/lang/String;

.field private final mSerialNumber:Ljava/math/BigInteger;

.field private final mStartDate:Ljava/util/Date;

.field private final mSubjectDN:Ljavax/security/auth/x500/X500Principal;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljavax/security/auth/x500/X500Principal;Ljava/math/BigInteger;Ljava/util/Date;Ljava/util/Date;)V
    .registers 9
    .parameter "context"
    .parameter "keyStoreAlias"
    .parameter "subjectDN"
    .parameter "serialNumber"
    .parameter "startDate"
    .parameter "endDate"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    if-nez p1, :cond_d

    #@5
    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "context == null"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 82
    :cond_d
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_1c

    #@13
    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string/jumbo v1, "keyStoreAlias must not be empty"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 84
    :cond_1c
    if-nez p3, :cond_27

    #@1e
    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string/jumbo v1, "subjectDN == null"

    #@23
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 86
    :cond_27
    if-nez p4, :cond_32

    #@29
    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2b
    const-string/jumbo v1, "serialNumber == null"

    #@2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 88
    :cond_32
    if-nez p5, :cond_3d

    #@34
    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@36
    const-string/jumbo v1, "startDate == null"

    #@39
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v0

    #@3d
    .line 90
    :cond_3d
    if-nez p6, :cond_47

    #@3f
    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@41
    const-string v1, "endDate == null"

    #@43
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v0

    #@47
    .line 92
    :cond_47
    invoke-virtual {p6, p5}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    #@4a
    move-result v0

    #@4b
    if-eqz v0, :cond_55

    #@4d
    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4f
    const-string v1, "endDate < startDate"

    #@51
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@54
    throw v0

    #@55
    .line 96
    :cond_55
    iput-object p1, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mContext:Landroid/content/Context;

    #@57
    .line 97
    iput-object p2, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mKeystoreAlias:Ljava/lang/String;

    #@59
    .line 98
    iput-object p3, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mSubjectDN:Ljavax/security/auth/x500/X500Principal;

    #@5b
    .line 99
    iput-object p4, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mSerialNumber:Ljava/math/BigInteger;

    #@5d
    .line 100
    iput-object p5, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mStartDate:Ljava/util/Date;

    #@5f
    .line 101
    iput-object p6, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mEndDate:Ljava/util/Date;

    #@61
    .line 102
    return-void
.end method


# virtual methods
.method getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method getEndDate()Ljava/util/Date;
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mEndDate:Ljava/util/Date;

    #@2
    return-object v0
.end method

.method getKeystoreAlias()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mKeystoreAlias:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getSerialNumber()Ljava/math/BigInteger;
    .registers 2

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mSerialNumber:Ljava/math/BigInteger;

    #@2
    return-object v0
.end method

.method getStartDate()Ljava/util/Date;
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mStartDate:Ljava/util/Date;

    #@2
    return-object v0
.end method

.method getSubjectDN()Ljavax/security/auth/x500/X500Principal;
    .registers 2

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Landroid/security/AndroidKeyPairGeneratorSpec;->mSubjectDN:Ljavax/security/auth/x500/X500Principal;

    #@2
    return-object v0
.end method
