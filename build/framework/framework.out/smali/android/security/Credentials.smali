.class public Landroid/security/Credentials;
.super Ljava/lang/Object;
.source "Credentials.java"


# static fields
.field public static final CA_CERTIFICATE:Ljava/lang/String; = "CACERT_"

.field public static final EXTENSION_CER:Ljava/lang/String; = ".cer"

.field public static final EXTENSION_CRT:Ljava/lang/String; = ".crt"

.field public static final EXTENSION_P12:Ljava/lang/String; = ".p12"

.field public static final EXTENSION_PFX:Ljava/lang/String; = ".pfx"

.field public static final EXTRA_CA_CERTIFICATES_DATA:Ljava/lang/String; = "ca_certificates_data"

.field public static final EXTRA_CA_CERTIFICATES_NAME:Ljava/lang/String; = "ca_certificates_name"

.field public static final EXTRA_PRIVATE_KEY:Ljava/lang/String; = "PKEY"

.field public static final EXTRA_PUBLIC_KEY:Ljava/lang/String; = "KEY"

.field public static final EXTRA_USER_CERTIFICATE_DATA:Ljava/lang/String; = "user_certificate_data"

.field public static final EXTRA_USER_CERTIFICATE_NAME:Ljava/lang/String; = "user_certificate_name"

.field public static final EXTRA_USER_PRIVATE_KEY_DATA:Ljava/lang/String; = "user_private_key_data"

.field public static final EXTRA_USER_PRIVATE_KEY_NAME:Ljava/lang/String; = "user_private_key_name"

.field public static final INSTALL_ACTION:Ljava/lang/String; = "android.credentials.INSTALL"

.field public static final LOCKDOWN_VPN:Ljava/lang/String; = "LOCKDOWN_VPN"

.field private static final LOGTAG:Ljava/lang/String; = "Credentials"

.field public static final UNLOCK_ACTION:Ljava/lang/String; = "com.android.credentials.UNLOCK"

.field public static final USER_CERTIFICATE:Ljava/lang/String; = "USRCERT_"

.field public static final USER_PRIVATE_KEY:Ljava/lang/String; = "USRPKEY_"

.field public static final VPN:Ljava/lang/String; = "VPN_"

.field public static final WIFI:Ljava/lang/String; = "WIFI_"

.field private static singleton:Landroid/security/Credentials;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static convertFromPem([B)Ljava/util/List;
    .registers 11
    .parameter "bytes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    #@0
    .prologue
    .line 136
    new-instance v0, Ljava/io/ByteArrayInputStream;

    #@2
    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@5
    .line 137
    .local v0, bai:Ljava/io/ByteArrayInputStream;
    new-instance v5, Ljava/io/InputStreamReader;

    #@7
    sget-object v7, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@9
    invoke-direct {v5, v0, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    #@c
    .line 138
    .local v5, reader:Ljava/io/Reader;
    new-instance v4, Lcom/android/org/bouncycastle/util/io/pem/PemReader;

    #@e
    invoke-direct {v4, v5}, Lcom/android/org/bouncycastle/util/io/pem/PemReader;-><init>(Ljava/io/Reader;)V

    #@11
    .line 140
    .local v4, pr:Lcom/android/org/bouncycastle/util/io/pem/PemReader;
    const-string v7, "X509"

    #@13
    invoke-static {v7}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@16
    move-result-object v2

    #@17
    .line 142
    .local v2, cf:Ljava/security/cert/CertificateFactory;
    new-instance v6, Ljava/util/ArrayList;

    #@19
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@1c
    .line 144
    .local v6, result:Ljava/util/List;,"Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    :goto_1c
    invoke-virtual {v4}, Lcom/android/org/bouncycastle/util/io/pem/PemReader;->readPemObject()Lcom/android/org/bouncycastle/util/io/pem/PemObject;

    #@1f
    move-result-object v3

    #@20
    .local v3, o:Lcom/android/org/bouncycastle/util/io/pem/PemObject;
    if-eqz v3, :cond_5e

    #@22
    .line 145
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/util/io/pem/PemObject;->getType()Ljava/lang/String;

    #@25
    move-result-object v7

    #@26
    const-string v8, "CERTIFICATE"

    #@28
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v7

    #@2c
    if-eqz v7, :cond_41

    #@2e
    .line 146
    new-instance v7, Ljava/io/ByteArrayInputStream;

    #@30
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/util/io/pem/PemObject;->getContent()[B

    #@33
    move-result-object v8

    #@34
    invoke-direct {v7, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@37
    invoke-virtual {v2, v7}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@3a
    move-result-object v1

    #@3b
    .line 147
    .local v1, c:Ljava/security/cert/Certificate;
    check-cast v1, Ljava/security/cert/X509Certificate;

    #@3d
    .end local v1           #c:Ljava/security/cert/Certificate;
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@40
    goto :goto_1c

    #@41
    .line 149
    :cond_41
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@43
    new-instance v8, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v9, "Unknown type "

    #@4a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v8

    #@4e
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/util/io/pem/PemObject;->getType()Ljava/lang/String;

    #@51
    move-result-object v9

    #@52
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v8

    #@5a
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5d
    throw v7

    #@5e
    .line 152
    :cond_5e
    invoke-virtual {v4}, Lcom/android/org/bouncycastle/util/io/pem/PemReader;->close()V

    #@61
    .line 153
    return-object v6
.end method

.method public static varargs convertToPem([Ljava/security/cert/Certificate;)[B
    .registers 11
    .parameter "objects"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    #@0
    .prologue
    .line 121
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@5
    .line 122
    .local v1, bao:Ljava/io/ByteArrayOutputStream;
    new-instance v6, Ljava/io/OutputStreamWriter;

    #@7
    sget-object v7, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@9
    invoke-direct {v6, v1, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    #@c
    .line 123
    .local v6, writer:Ljava/io/Writer;
    new-instance v5, Lcom/android/org/bouncycastle/util/io/pem/PemWriter;

    #@e
    invoke-direct {v5, v6}, Lcom/android/org/bouncycastle/util/io/pem/PemWriter;-><init>(Ljava/io/Writer;)V

    #@11
    .line 124
    .local v5, pw:Lcom/android/org/bouncycastle/util/io/pem/PemWriter;
    move-object v0, p0

    #@12
    .local v0, arr$:[Ljava/security/cert/Certificate;
    array-length v3, v0

    #@13
    .local v3, len$:I
    const/4 v2, 0x0

    #@14
    .local v2, i$:I
    :goto_14
    if-ge v2, v3, :cond_29

    #@16
    aget-object v4, v0, v2

    #@18
    .line 125
    .local v4, o:Ljava/security/cert/Certificate;
    new-instance v7, Lcom/android/org/bouncycastle/util/io/pem/PemObject;

    #@1a
    const-string v8, "CERTIFICATE"

    #@1c
    invoke-virtual {v4}, Ljava/security/cert/Certificate;->getEncoded()[B

    #@1f
    move-result-object v9

    #@20
    invoke-direct {v7, v8, v9}, Lcom/android/org/bouncycastle/util/io/pem/PemObject;-><init>(Ljava/lang/String;[B)V

    #@23
    invoke-virtual {v5, v7}, Lcom/android/org/bouncycastle/util/io/pem/PemWriter;->writeObject(Lcom/android/org/bouncycastle/util/io/pem/PemObjectGenerator;)V

    #@26
    .line 124
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_14

    #@29
    .line 127
    .end local v4           #o:Ljava/security/cert/Certificate;
    :cond_29
    invoke-virtual {v5}, Lcom/android/org/bouncycastle/util/io/pem/PemWriter;->close()V

    #@2c
    .line 128
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@2f
    move-result-object v7

    #@30
    return-object v7
.end method

.method static deleteAllTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z
    .registers 4
    .parameter "keystore"
    .parameter "alias"

    #@0
    .prologue
    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "USRPKEY_"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Landroid/security/KeyStore;->delKey(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    invoke-static {p0, p1}, Landroid/security/Credentials;->deleteCertificateTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z

    #@1a
    move-result v1

    #@1b
    or-int/2addr v0, v1

    #@1c
    return v0
.end method

.method static deleteCertificateTypesForAlias(Landroid/security/KeyStore;Ljava/lang/String;)Z
    .registers 5
    .parameter "keystore"
    .parameter "alias"

    #@0
    .prologue
    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "USRCERT_"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Landroid/security/KeyStore;->delete(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "CACERT_"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {p0, v1}, Landroid/security/KeyStore;->delete(Ljava/lang/String;)Z

    #@2d
    move-result v1

    #@2e
    or-int/2addr v0, v1

    #@2f
    return v0
.end method

.method public static getInstance()Landroid/security/Credentials;
    .registers 1

    #@0
    .prologue
    .line 159
    sget-object v0, Landroid/security/Credentials;->singleton:Landroid/security/Credentials;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 160
    new-instance v0, Landroid/security/Credentials;

    #@6
    invoke-direct {v0}, Landroid/security/Credentials;-><init>()V

    #@9
    sput-object v0, Landroid/security/Credentials;->singleton:Landroid/security/Credentials;

    #@b
    .line 162
    :cond_b
    sget-object v0, Landroid/security/Credentials;->singleton:Landroid/security/Credentials;

    #@d
    return-object v0
.end method


# virtual methods
.method public install(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 176
    :try_start_0
    invoke-static {}, Landroid/security/KeyChain;->createInstallIntent()Landroid/content/Intent;

    #@3
    move-result-object v1

    #@4
    .line 177
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_7
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 181
    .end local v1           #intent:Landroid/content/Intent;
    :goto_7
    return-void

    #@8
    .line 178
    :catch_8
    move-exception v0

    #@9
    .line 179
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "Credentials"

    #@b
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    goto :goto_7
.end method

.method public install(Landroid/content/Context;Ljava/lang/String;[B)V
    .registers 8
    .parameter "context"
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 196
    :try_start_0
    invoke-static {}, Landroid/security/KeyChain;->createInstallIntent()Landroid/content/Intent;

    #@3
    move-result-object v1

    #@4
    .line 197
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@7
    .line 198
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_a} :catch_b

    #@a
    .line 202
    .end local v1           #intent:Landroid/content/Intent;
    :goto_a
    return-void

    #@b
    .line 199
    :catch_b
    move-exception v0

    #@c
    .line 200
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "Credentials"

    #@e
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_a
.end method

.method public install(Landroid/content/Context;Ljava/security/KeyPair;)V
    .registers 7
    .parameter "context"
    .parameter "pair"

    #@0
    .prologue
    .line 185
    :try_start_0
    invoke-static {}, Landroid/security/KeyChain;->createInstallIntent()Landroid/content/Intent;

    #@3
    move-result-object v1

    #@4
    .line 186
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "PKEY"

    #@6
    invoke-virtual {p2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    #@9
    move-result-object v3

    #@a
    invoke-interface {v3}, Ljava/security/PrivateKey;->getEncoded()[B

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@11
    .line 187
    const-string v2, "KEY"

    #@13
    invoke-virtual {p2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    #@16
    move-result-object v3

    #@17
    invoke-interface {v3}, Ljava/security/PublicKey;->getEncoded()[B

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@1e
    .line 188
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_21
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_21} :catch_22

    #@21
    .line 192
    .end local v1           #intent:Landroid/content/Intent;
    :goto_21
    return-void

    #@22
    .line 189
    :catch_22
    move-exception v0

    #@23
    .line 190
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "Credentials"

    #@25
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_21
.end method

.method public unlock(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 167
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "com.android.credentials.UNLOCK"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 168
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_a} :catch_b

    #@a
    .line 172
    .end local v1           #intent:Landroid/content/Intent;
    :goto_a
    return-void

    #@b
    .line 169
    :catch_b
    move-exception v0

    #@c
    .line 170
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "Credentials"

    #@e
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_a
.end method
