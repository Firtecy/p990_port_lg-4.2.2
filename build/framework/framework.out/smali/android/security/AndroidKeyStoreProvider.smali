.class public Landroid/security/AndroidKeyStoreProvider;
.super Ljava/security/Provider;
.source "AndroidKeyStoreProvider.java"


# static fields
.field public static final PROVIDER_NAME:Ljava/lang/String; = "AndroidKeyStoreProvider"


# direct methods
.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 30
    const-string v0, "AndroidKeyStoreProvider"

    #@2
    const-wide/high16 v1, 0x3ff0

    #@4
    const-string v3, "Android KeyStore security provider"

    #@6
    invoke-direct {p0, v0, v1, v2, v3}, Ljava/security/Provider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    #@9
    .line 33
    const-string v0, "KeyStore.AndroidKeyStore"

    #@b
    const-class v1, Landroid/security/AndroidKeyStore;

    #@d
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/security/AndroidKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 36
    const-string v0, "KeyPairGenerator.AndroidKeyPairGenerator"

    #@16
    const-class v1, Landroid/security/AndroidKeyPairGenerator;

    #@18
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {p0, v0, v1}, Landroid/security/AndroidKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    .line 38
    return-void
.end method
