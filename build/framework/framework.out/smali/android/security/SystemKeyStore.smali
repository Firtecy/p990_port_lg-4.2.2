.class public Landroid/security/SystemKeyStore;
.super Ljava/lang/Object;
.source "SystemKeyStore.java"


# static fields
.field private static final KEY_FILE_EXTENSION:Ljava/lang/String; = ".sks"

.field private static final SYSTEM_KEYSTORE_DIRECTORY:Ljava/lang/String; = "misc/systemkeys"

.field private static mInstance:Landroid/security/SystemKeyStore;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 42
    new-instance v0, Landroid/security/SystemKeyStore;

    #@2
    invoke-direct {v0}, Landroid/security/SystemKeyStore;-><init>()V

    #@5
    sput-object v0, Landroid/security/SystemKeyStore;->mInstance:Landroid/security/SystemKeyStore;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/security/SystemKeyStore;
    .registers 1

    #@0
    .prologue
    .line 47
    sget-object v0, Landroid/security/SystemKeyStore;->mInstance:Landroid/security/SystemKeyStore;

    #@2
    return-object v0
.end method

.method private getKeyFile(Ljava/lang/String;)Ljava/io/File;
    .registers 6
    .parameter "keyName"

    #@0
    .prologue
    .line 108
    new-instance v1, Ljava/io/File;

    #@2
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@5
    move-result-object v2

    #@6
    const-string/jumbo v3, "misc/systemkeys"

    #@9
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@c
    .line 110
    .local v1, sysKeystoreDir:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ".sks"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@24
    .line 111
    .local v0, keyFile:Ljava/io/File;
    return-object v0
.end method

.method public static toHexString([B)Ljava/lang/String;
    .registers 8
    .parameter "keyData"

    #@0
    .prologue
    .line 51
    if-nez p0, :cond_4

    #@2
    .line 52
    const/4 v5, 0x0

    #@3
    .line 64
    :goto_3
    return-object v5

    #@4
    .line 54
    :cond_4
    array-length v3, p0

    #@5
    .line 55
    .local v3, keyLen:I
    array-length v5, p0

    #@6
    mul-int/lit8 v0, v5, 0x2

    #@8
    .line 56
    .local v0, expectedStringLen:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 57
    .local v4, sb:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    array-length v5, p0

    #@f
    if-ge v2, v5, :cond_3b

    #@11
    .line 58
    aget-byte v5, p0, v2

    #@13
    and-int/lit16 v5, v5, 0xff

    #@15
    const/16 v6, 0x10

    #@17
    invoke-static {v5, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 59
    .local v1, hexStr:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1e
    move-result v5

    #@1f
    const/4 v6, 0x1

    #@20
    if-ne v5, v6, :cond_35

    #@22
    .line 60
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, "0"

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    .line 62
    :cond_35
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 57
    add-int/lit8 v2, v2, 0x1

    #@3a
    goto :goto_e

    #@3b
    .line 64
    .end local v1           #hexStr:Ljava/lang/String;
    :cond_3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    goto :goto_3
.end method


# virtual methods
.method public deleteKey(Ljava/lang/String;)V
    .registers 4
    .parameter "keyName"

    #@0
    .prologue
    .line 129
    invoke-direct {p0, p1}, Landroid/security/SystemKeyStore;->getKeyFile(Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    .line 130
    .local v0, keyFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_10

    #@a
    .line 131
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@f
    throw v1

    #@10
    .line 134
    :cond_10
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@13
    .line 135
    return-void
.end method

.method public generateNewKey(ILjava/lang/String;Ljava/lang/String;)[B
    .registers 15
    .parameter "numBits"
    .parameter "algName"
    .parameter "keyName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p3}, Landroid/security/SystemKeyStore;->getKeyFile(Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v2

    #@4
    .line 77
    .local v2, keyFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_10

    #@a
    .line 78
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@c
    invoke-direct {v7}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@f
    throw v7

    #@10
    .line 81
    :cond_10
    invoke-static {p2}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    #@13
    move-result-object v5

    #@14
    .line 82
    .local v5, skg:Ljavax/crypto/KeyGenerator;
    const-string v7, "SHA1PRNG"

    #@16
    invoke-static {v7}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    #@19
    move-result-object v6

    #@1a
    .line 83
    .local v6, srng:Ljava/security/SecureRandom;
    invoke-virtual {v5, p1, v6}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    #@1d
    .line 85
    invoke-virtual {v5}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    #@20
    move-result-object v4

    #@21
    .line 86
    .local v4, sk:Ljavax/crypto/SecretKey;
    invoke-interface {v4}, Ljavax/crypto/SecretKey;->getEncoded()[B

    #@24
    move-result-object v3

    #@25
    .line 90
    .local v3, retKey:[B
    :try_start_25
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    #@28
    move-result v7

    #@29
    if-nez v7, :cond_34

    #@2b
    .line 91
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@2d
    invoke-direct {v7}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@30
    throw v7

    #@31
    .line 101
    :catch_31
    move-exception v1

    #@32
    .line 102
    .local v1, ioe:Ljava/io/IOException;
    const/4 v3, 0x0

    #@33
    .line 104
    .end local v1           #ioe:Ljava/io/IOException;
    .end local v3           #retKey:[B
    :goto_33
    return-object v3

    #@34
    .line 94
    .restart local v3       #retKey:[B
    :cond_34
    new-instance v0, Ljava/io/FileOutputStream;

    #@36
    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@39
    .line 95
    .local v0, fos:Ljava/io/FileOutputStream;
    invoke-virtual {v0, v3}, Ljava/io/FileOutputStream;->write([B)V

    #@3c
    .line 96
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    #@3f
    .line 97
    invoke-static {v0}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@42
    .line 98
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    #@45
    .line 99
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    const/16 v8, 0x180

    #@4b
    const/4 v9, -0x1

    #@4c
    const/4 v10, -0x1

    #@4d
    invoke-static {v7, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_50} :catch_31

    #@50
    goto :goto_33
.end method

.method public generateNewKeyHexString(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "numBits"
    .parameter "algName"
    .parameter "keyName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    invoke-virtual {p0, p1, p2, p3}, Landroid/security/SystemKeyStore;->generateNewKey(ILjava/lang/String;Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/security/SystemKeyStore;->toHexString([B)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public retrieveKey(Ljava/lang/String;)[B
    .registers 4
    .parameter "keyName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 119
    invoke-direct {p0, p1}, Landroid/security/SystemKeyStore;->getKeyFile(Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    .line 120
    .local v0, keyFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_c

    #@a
    .line 121
    const/4 v1, 0x0

    #@b
    .line 123
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-static {v1}, Llibcore/io/IoUtils;->readFileAsByteArray(Ljava/lang/String;)[B

    #@13
    move-result-object v1

    #@14
    goto :goto_b
.end method

.method public retrieveKeyHexString(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "keyName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 115
    invoke-virtual {p0, p1}, Landroid/security/SystemKeyStore;->retrieveKey(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/security/SystemKeyStore;->toHexString([B)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
