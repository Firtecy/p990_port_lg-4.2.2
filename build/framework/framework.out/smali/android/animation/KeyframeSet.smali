.class Landroid/animation/KeyframeSet;
.super Ljava/lang/Object;
.source "KeyframeSet.java"


# instance fields
.field mEvaluator:Landroid/animation/TypeEvaluator;

.field mFirstKeyframe:Landroid/animation/Keyframe;

.field mInterpolator:Landroid/animation/TimeInterpolator;

.field mKeyframes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Keyframe;",
            ">;"
        }
    .end annotation
.end field

.field mLastKeyframe:Landroid/animation/Keyframe;

.field mNumKeyframes:I


# direct methods
.method public varargs constructor <init>([Landroid/animation/Keyframe;)V
    .registers 4
    .parameter "keyframes"

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    array-length v0, p1

    #@4
    iput v0, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@6
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@d
    .line 44
    iget-object v0, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@f
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@16
    .line 45
    iget-object v0, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/animation/Keyframe;

    #@1f
    iput-object v0, p0, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@21
    .line 46
    iget-object v0, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@23
    iget v1, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@25
    add-int/lit8 v1, v1, -0x1

    #@27
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/animation/Keyframe;

    #@2d
    iput-object v0, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@2f
    .line 47
    iget-object v0, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@31
    invoke-virtual {v0}, Landroid/animation/Keyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@34
    move-result-object v0

    #@35
    iput-object v0, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@37
    .line 48
    return-void
.end method

.method public static varargs ofFloat([F)Landroid/animation/KeyframeSet;
    .registers 8
    .parameter "values"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 66
    array-length v2, p0

    #@4
    .line 67
    .local v2, numKeyframes:I
    const/4 v3, 0x2

    #@5
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v3

    #@9
    new-array v1, v3, [Landroid/animation/Keyframe$FloatKeyframe;

    #@b
    .line 68
    .local v1, keyframes:[Landroid/animation/Keyframe$FloatKeyframe;
    if-ne v2, v6, :cond_27

    #@d
    .line 69
    invoke-static {v5}, Landroid/animation/Keyframe;->ofFloat(F)Landroid/animation/Keyframe;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/animation/Keyframe$FloatKeyframe;

    #@13
    aput-object v3, v1, v4

    #@15
    .line 70
    const/high16 v3, 0x3f80

    #@17
    aget v4, p0, v4

    #@19
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/animation/Keyframe$FloatKeyframe;

    #@1f
    aput-object v3, v1, v6

    #@21
    .line 77
    :cond_21
    new-instance v3, Landroid/animation/FloatKeyframeSet;

    #@23
    invoke-direct {v3, v1}, Landroid/animation/FloatKeyframeSet;-><init>([Landroid/animation/Keyframe$FloatKeyframe;)V

    #@26
    return-object v3

    #@27
    .line 72
    :cond_27
    aget v3, p0, v4

    #@29
    invoke-static {v5, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/animation/Keyframe$FloatKeyframe;

    #@2f
    aput-object v3, v1, v4

    #@31
    .line 73
    const/4 v0, 0x1

    #@32
    .local v0, i:I
    :goto_32
    if-ge v0, v2, :cond_21

    #@34
    .line 74
    int-to-float v3, v0

    #@35
    add-int/lit8 v4, v2, -0x1

    #@37
    int-to-float v4, v4

    #@38
    div-float/2addr v3, v4

    #@39
    aget v4, p0, v0

    #@3b
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Landroid/animation/Keyframe$FloatKeyframe;

    #@41
    aput-object v3, v1, v0

    #@43
    .line 73
    add-int/lit8 v0, v0, 0x1

    #@45
    goto :goto_32
.end method

.method public static varargs ofInt([I)Landroid/animation/KeyframeSet;
    .registers 8
    .parameter "values"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 51
    array-length v2, p0

    #@4
    .line 52
    .local v2, numKeyframes:I
    const/4 v3, 0x2

    #@5
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v3

    #@9
    new-array v1, v3, [Landroid/animation/Keyframe$IntKeyframe;

    #@b
    .line 53
    .local v1, keyframes:[Landroid/animation/Keyframe$IntKeyframe;
    if-ne v2, v6, :cond_27

    #@d
    .line 54
    invoke-static {v5}, Landroid/animation/Keyframe;->ofInt(F)Landroid/animation/Keyframe;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/animation/Keyframe$IntKeyframe;

    #@13
    aput-object v3, v1, v4

    #@15
    .line 55
    const/high16 v3, 0x3f80

    #@17
    aget v4, p0, v4

    #@19
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/animation/Keyframe$IntKeyframe;

    #@1f
    aput-object v3, v1, v6

    #@21
    .line 62
    :cond_21
    new-instance v3, Landroid/animation/IntKeyframeSet;

    #@23
    invoke-direct {v3, v1}, Landroid/animation/IntKeyframeSet;-><init>([Landroid/animation/Keyframe$IntKeyframe;)V

    #@26
    return-object v3

    #@27
    .line 57
    :cond_27
    aget v3, p0, v4

    #@29
    invoke-static {v5, v3}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/animation/Keyframe$IntKeyframe;

    #@2f
    aput-object v3, v1, v4

    #@31
    .line 58
    const/4 v0, 0x1

    #@32
    .local v0, i:I
    :goto_32
    if-ge v0, v2, :cond_21

    #@34
    .line 59
    int-to-float v3, v0

    #@35
    add-int/lit8 v4, v2, -0x1

    #@37
    int-to-float v4, v4

    #@38
    div-float/2addr v3, v4

    #@39
    aget v4, p0, v0

    #@3b
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Landroid/animation/Keyframe$IntKeyframe;

    #@41
    aput-object v3, v1, v0

    #@43
    .line 58
    add-int/lit8 v0, v0, 0x1

    #@45
    goto :goto_32
.end method

.method public static varargs ofKeyframe([Landroid/animation/Keyframe;)Landroid/animation/KeyframeSet;
    .registers 9
    .parameter "keyframes"

    #@0
    .prologue
    .line 82
    array-length v6, p0

    #@1
    .line 83
    .local v6, numKeyframes:I
    const/4 v1, 0x0

    #@2
    .line 84
    .local v1, hasFloat:Z
    const/4 v2, 0x0

    #@3
    .line 85
    .local v2, hasInt:Z
    const/4 v3, 0x0

    #@4
    .line 86
    .local v3, hasOther:Z
    const/4 v4, 0x0

    #@5
    .local v4, i:I
    :goto_5
    if-ge v4, v6, :cond_1b

    #@7
    .line 87
    aget-object v7, p0, v4

    #@9
    instance-of v7, v7, Landroid/animation/Keyframe$FloatKeyframe;

    #@b
    if-eqz v7, :cond_11

    #@d
    .line 88
    const/4 v1, 0x1

    #@e
    .line 86
    :goto_e
    add-int/lit8 v4, v4, 0x1

    #@10
    goto :goto_5

    #@11
    .line 89
    :cond_11
    aget-object v7, p0, v4

    #@13
    instance-of v7, v7, Landroid/animation/Keyframe$IntKeyframe;

    #@15
    if-eqz v7, :cond_19

    #@17
    .line 90
    const/4 v2, 0x1

    #@18
    goto :goto_e

    #@19
    .line 92
    :cond_19
    const/4 v3, 0x1

    #@1a
    goto :goto_e

    #@1b
    .line 95
    :cond_1b
    if-eqz v1, :cond_35

    #@1d
    if-nez v2, :cond_35

    #@1f
    if-nez v3, :cond_35

    #@21
    .line 96
    new-array v0, v6, [Landroid/animation/Keyframe$FloatKeyframe;

    #@23
    .line 97
    .local v0, floatKeyframes:[Landroid/animation/Keyframe$FloatKeyframe;
    const/4 v4, 0x0

    #@24
    :goto_24
    if-ge v4, v6, :cond_2f

    #@26
    .line 98
    aget-object v7, p0, v4

    #@28
    check-cast v7, Landroid/animation/Keyframe$FloatKeyframe;

    #@2a
    aput-object v7, v0, v4

    #@2c
    .line 97
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_24

    #@2f
    .line 100
    :cond_2f
    new-instance v7, Landroid/animation/FloatKeyframeSet;

    #@31
    invoke-direct {v7, v0}, Landroid/animation/FloatKeyframeSet;-><init>([Landroid/animation/Keyframe$FloatKeyframe;)V

    #@34
    .line 108
    .end local v0           #floatKeyframes:[Landroid/animation/Keyframe$FloatKeyframe;
    :goto_34
    return-object v7

    #@35
    .line 101
    :cond_35
    if-eqz v2, :cond_4f

    #@37
    if-nez v1, :cond_4f

    #@39
    if-nez v3, :cond_4f

    #@3b
    .line 102
    new-array v5, v6, [Landroid/animation/Keyframe$IntKeyframe;

    #@3d
    .line 103
    .local v5, intKeyframes:[Landroid/animation/Keyframe$IntKeyframe;
    const/4 v4, 0x0

    #@3e
    :goto_3e
    if-ge v4, v6, :cond_49

    #@40
    .line 104
    aget-object v7, p0, v4

    #@42
    check-cast v7, Landroid/animation/Keyframe$IntKeyframe;

    #@44
    aput-object v7, v5, v4

    #@46
    .line 103
    add-int/lit8 v4, v4, 0x1

    #@48
    goto :goto_3e

    #@49
    .line 106
    :cond_49
    new-instance v7, Landroid/animation/IntKeyframeSet;

    #@4b
    invoke-direct {v7, v5}, Landroid/animation/IntKeyframeSet;-><init>([Landroid/animation/Keyframe$IntKeyframe;)V

    #@4e
    goto :goto_34

    #@4f
    .line 108
    .end local v5           #intKeyframes:[Landroid/animation/Keyframe$IntKeyframe;
    :cond_4f
    new-instance v7, Landroid/animation/KeyframeSet;

    #@51
    invoke-direct {v7, p0}, Landroid/animation/KeyframeSet;-><init>([Landroid/animation/Keyframe;)V

    #@54
    goto :goto_34
.end method

.method public static varargs ofObject([Ljava/lang/Object;)Landroid/animation/KeyframeSet;
    .registers 8
    .parameter "values"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 113
    array-length v2, p0

    #@4
    .line 114
    .local v2, numKeyframes:I
    const/4 v3, 0x2

    #@5
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v3

    #@9
    new-array v1, v3, [Landroid/animation/Keyframe$ObjectKeyframe;

    #@b
    .line 115
    .local v1, keyframes:[Landroid/animation/Keyframe$ObjectKeyframe;
    if-ne v2, v6, :cond_27

    #@d
    .line 116
    invoke-static {v5}, Landroid/animation/Keyframe;->ofObject(F)Landroid/animation/Keyframe;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/animation/Keyframe$ObjectKeyframe;

    #@13
    aput-object v3, v1, v4

    #@15
    .line 117
    const/high16 v3, 0x3f80

    #@17
    aget-object v4, p0, v4

    #@19
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofObject(FLjava/lang/Object;)Landroid/animation/Keyframe;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/animation/Keyframe$ObjectKeyframe;

    #@1f
    aput-object v3, v1, v6

    #@21
    .line 124
    :cond_21
    new-instance v3, Landroid/animation/KeyframeSet;

    #@23
    invoke-direct {v3, v1}, Landroid/animation/KeyframeSet;-><init>([Landroid/animation/Keyframe;)V

    #@26
    return-object v3

    #@27
    .line 119
    :cond_27
    aget-object v3, p0, v4

    #@29
    invoke-static {v5, v3}, Landroid/animation/Keyframe;->ofObject(FLjava/lang/Object;)Landroid/animation/Keyframe;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/animation/Keyframe$ObjectKeyframe;

    #@2f
    aput-object v3, v1, v4

    #@31
    .line 120
    const/4 v0, 0x1

    #@32
    .local v0, i:I
    :goto_32
    if-ge v0, v2, :cond_21

    #@34
    .line 121
    int-to-float v3, v0

    #@35
    add-int/lit8 v4, v2, -0x1

    #@37
    int-to-float v4, v4

    #@38
    div-float/2addr v3, v4

    #@39
    aget-object v4, p0, v0

    #@3b
    invoke-static {v3, v4}, Landroid/animation/Keyframe;->ofObject(FLjava/lang/Object;)Landroid/animation/Keyframe;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Landroid/animation/Keyframe$ObjectKeyframe;

    #@41
    aput-object v3, v1, v0

    #@43
    .line 120
    add-int/lit8 v0, v0, 0x1

    #@45
    goto :goto_32
.end method


# virtual methods
.method public clone()Landroid/animation/KeyframeSet;
    .registers 7

    #@0
    .prologue
    .line 141
    iget-object v1, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@2
    .line 142
    .local v1, keyframes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Keyframe;>;"
    iget-object v5, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v4

    #@8
    .line 143
    .local v4, numKeyframes:I
    new-array v2, v4, [Landroid/animation/Keyframe;

    #@a
    .line 144
    .local v2, newKeyframes:[Landroid/animation/Keyframe;
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v4, :cond_1c

    #@d
    .line 145
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v5

    #@11
    check-cast v5, Landroid/animation/Keyframe;

    #@13
    invoke-virtual {v5}, Landroid/animation/Keyframe;->clone()Landroid/animation/Keyframe;

    #@16
    move-result-object v5

    #@17
    aput-object v5, v2, v0

    #@19
    .line 144
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_b

    #@1c
    .line 147
    :cond_1c
    new-instance v3, Landroid/animation/KeyframeSet;

    #@1e
    invoke-direct {v3, v2}, Landroid/animation/KeyframeSet;-><init>([Landroid/animation/Keyframe;)V

    #@21
    .line 148
    .local v3, newSet:Landroid/animation/KeyframeSet;
    return-object v3
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 30
    invoke-virtual {p0}, Landroid/animation/KeyframeSet;->clone()Landroid/animation/KeyframeSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getValue(F)Ljava/lang/Object;
    .registers 11
    .parameter "fraction"

    #@0
    .prologue
    .line 166
    iget v6, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@2
    const/4 v7, 0x2

    #@3
    if-ne v6, v7, :cond_22

    #@5
    .line 167
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@7
    if-eqz v6, :cond_f

    #@9
    .line 168
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@b
    invoke-interface {v6, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@e
    move-result p1

    #@f
    .line 170
    :cond_f
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@11
    iget-object v7, p0, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@13
    invoke-virtual {v7}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@16
    move-result-object v7

    #@17
    iget-object v8, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@19
    invoke-virtual {v8}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@1c
    move-result-object v8

    #@1d
    invoke-interface {v6, p1, v7, v8}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    move-result-object v6

    #@21
    .line 213
    :goto_21
    return-object v6

    #@22
    .line 173
    :cond_22
    const/4 v6, 0x0

    #@23
    cmpg-float v6, p1, v6

    #@25
    if-gtz v6, :cond_5a

    #@27
    .line 174
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@29
    const/4 v7, 0x1

    #@2a
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v3

    #@2e
    check-cast v3, Landroid/animation/Keyframe;

    #@30
    .line 175
    .local v3, nextKeyframe:Landroid/animation/Keyframe;
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@33
    move-result-object v1

    #@34
    .line 176
    .local v1, interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_3a

    #@36
    .line 177
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@39
    move-result p1

    #@3a
    .line 179
    :cond_3a
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@3c
    invoke-virtual {v6}, Landroid/animation/Keyframe;->getFraction()F

    #@3f
    move-result v4

    #@40
    .line 180
    .local v4, prevFraction:F
    sub-float v6, p1, v4

    #@42
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getFraction()F

    #@45
    move-result v7

    #@46
    sub-float/2addr v7, v4

    #@47
    div-float v2, v6, v7

    #@49
    .line 182
    .local v2, intervalFraction:F
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@4b
    iget-object v7, p0, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@4d
    invoke-virtual {v7}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@54
    move-result-object v8

    #@55
    invoke-interface {v6, v2, v7, v8}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    move-result-object v6

    #@59
    goto :goto_21

    #@5a
    .line 184
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v3           #nextKeyframe:Landroid/animation/Keyframe;
    .end local v4           #prevFraction:F
    :cond_5a
    const/high16 v6, 0x3f80

    #@5c
    cmpl-float v6, p1, v6

    #@5e
    if-ltz v6, :cond_98

    #@60
    .line 185
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@62
    iget v7, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@64
    add-int/lit8 v7, v7, -0x2

    #@66
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@69
    move-result-object v5

    #@6a
    check-cast v5, Landroid/animation/Keyframe;

    #@6c
    .line 186
    .local v5, prevKeyframe:Landroid/animation/Keyframe;
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@6e
    invoke-virtual {v6}, Landroid/animation/Keyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@71
    move-result-object v1

    #@72
    .line 187
    .restart local v1       #interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_78

    #@74
    .line 188
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@77
    move-result p1

    #@78
    .line 190
    :cond_78
    invoke-virtual {v5}, Landroid/animation/Keyframe;->getFraction()F

    #@7b
    move-result v4

    #@7c
    .line 191
    .restart local v4       #prevFraction:F
    sub-float v6, p1, v4

    #@7e
    iget-object v7, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@80
    invoke-virtual {v7}, Landroid/animation/Keyframe;->getFraction()F

    #@83
    move-result v7

    #@84
    sub-float/2addr v7, v4

    #@85
    div-float v2, v6, v7

    #@87
    .line 193
    .restart local v2       #intervalFraction:F
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@89
    invoke-virtual {v5}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@8c
    move-result-object v7

    #@8d
    iget-object v8, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@8f
    invoke-virtual {v8}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@92
    move-result-object v8

    #@93
    invoke-interface {v6, v2, v7, v8}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    move-result-object v6

    #@97
    goto :goto_21

    #@98
    .line 196
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v4           #prevFraction:F
    .end local v5           #prevKeyframe:Landroid/animation/Keyframe;
    :cond_98
    iget-object v5, p0, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@9a
    .line 197
    .restart local v5       #prevKeyframe:Landroid/animation/Keyframe;
    const/4 v0, 0x1

    #@9b
    .local v0, i:I
    :goto_9b
    iget v6, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@9d
    if-ge v0, v6, :cond_da

    #@9f
    .line 198
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@a1
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a4
    move-result-object v3

    #@a5
    check-cast v3, Landroid/animation/Keyframe;

    #@a7
    .line 199
    .restart local v3       #nextKeyframe:Landroid/animation/Keyframe;
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getFraction()F

    #@aa
    move-result v6

    #@ab
    cmpg-float v6, p1, v6

    #@ad
    if-gez v6, :cond_d6

    #@af
    .line 200
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@b2
    move-result-object v1

    #@b3
    .line 201
    .restart local v1       #interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_b9

    #@b5
    .line 202
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@b8
    move-result p1

    #@b9
    .line 204
    :cond_b9
    invoke-virtual {v5}, Landroid/animation/Keyframe;->getFraction()F

    #@bc
    move-result v4

    #@bd
    .line 205
    .restart local v4       #prevFraction:F
    sub-float v6, p1, v4

    #@bf
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getFraction()F

    #@c2
    move-result v7

    #@c3
    sub-float/2addr v7, v4

    #@c4
    div-float v2, v6, v7

    #@c6
    .line 207
    .restart local v2       #intervalFraction:F
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@c8
    invoke-virtual {v5}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@cb
    move-result-object v7

    #@cc
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@cf
    move-result-object v8

    #@d0
    invoke-interface {v6, v2, v7, v8}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d3
    move-result-object v6

    #@d4
    goto/16 :goto_21

    #@d6
    .line 210
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v4           #prevFraction:F
    :cond_d6
    move-object v5, v3

    #@d7
    .line 197
    add-int/lit8 v0, v0, 0x1

    #@d9
    goto :goto_9b

    #@da
    .line 213
    .end local v3           #nextKeyframe:Landroid/animation/Keyframe;
    :cond_da
    iget-object v6, p0, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@dc
    invoke-virtual {v6}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@df
    move-result-object v6

    #@e0
    goto/16 :goto_21
.end method

.method public setEvaluator(Landroid/animation/TypeEvaluator;)V
    .registers 2
    .parameter "evaluator"

    #@0
    .prologue
    .line 136
    iput-object p1, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@2
    .line 137
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 218
    const-string v1, " "

    #@2
    .line 219
    .local v1, returnVal:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    iget v2, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@5
    if-ge v0, v2, :cond_2d

    #@7
    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    iget-object v2, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/animation/Keyframe;

    #@18
    invoke-virtual {v2}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, "  "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 219
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_3

    #@2d
    .line 222
    :cond_2d
    return-object v1
.end method
