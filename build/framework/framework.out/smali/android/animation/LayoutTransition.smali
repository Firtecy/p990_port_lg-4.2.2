.class public Landroid/animation/LayoutTransition;
.super Ljava/lang/Object;
.source "LayoutTransition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/LayoutTransition$TransitionListener;
    }
.end annotation


# static fields
.field public static final APPEARING:I = 0x2

.field public static final CHANGE_APPEARING:I = 0x0

.field public static final CHANGE_DISAPPEARING:I = 0x1

.field public static final CHANGING:I = 0x4

.field private static DEFAULT_DURATION:J = 0x0L

.field public static final DISAPPEARING:I = 0x3

.field private static final FLAG_APPEARING:I = 0x1

.field private static final FLAG_CHANGE_APPEARING:I = 0x4

.field private static final FLAG_CHANGE_DISAPPEARING:I = 0x8

.field private static final FLAG_CHANGING:I = 0x10

.field private static final FLAG_DISAPPEARING:I = 0x2

.field private static defaultChange:Landroid/animation/ObjectAnimator;

.field private static defaultChangeIn:Landroid/animation/ObjectAnimator;

.field private static defaultChangeOut:Landroid/animation/ObjectAnimator;

.field private static defaultFadeIn:Landroid/animation/ObjectAnimator;

.field private static defaultFadeOut:Landroid/animation/ObjectAnimator;


# instance fields
.field private final currentAppearingAnimations:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private final currentChangingAnimations:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private final currentDisappearingAnimations:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private final layoutChangeListenerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/view/View$OnLayoutChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimateParentHierarchy:Z

.field private mAppearingAnim:Landroid/animation/Animator;

.field private mAppearingDelay:J

.field private mAppearingDuration:J

.field private mAppearingInterpolator:Landroid/animation/TimeInterpolator;

.field private mChangingAnim:Landroid/animation/Animator;

.field private mChangingAppearingAnim:Landroid/animation/Animator;

.field private mChangingAppearingDelay:J

.field private mChangingAppearingDuration:J

.field private mChangingAppearingInterpolator:Landroid/animation/TimeInterpolator;

.field private mChangingAppearingStagger:J

.field private mChangingDelay:J

.field private mChangingDisappearingAnim:Landroid/animation/Animator;

.field private mChangingDisappearingDelay:J

.field private mChangingDisappearingDuration:J

.field private mChangingDisappearingInterpolator:Landroid/animation/TimeInterpolator;

.field private mChangingDisappearingStagger:J

.field private mChangingDuration:J

.field private mChangingInterpolator:Landroid/animation/TimeInterpolator;

.field private mChangingStagger:J

.field private mDisappearingAnim:Landroid/animation/Animator;

.field private mDisappearingDelay:J

.field private mDisappearingDuration:J

.field private mDisappearingInterpolator:Landroid/animation/TimeInterpolator;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/LayoutTransition$TransitionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTransitionTypes:I

.field private final pendingAnimations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private staggerDelay:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 163
    const-wide/16 v0, 0x12c

    #@2
    sput-wide v0, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    const-wide/16 v11, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v10, 0x2

    #@5
    .line 265
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 144
    iput-object v7, p0, Landroid/animation/LayoutTransition;->mDisappearingAnim:Landroid/animation/Animator;

    #@a
    .line 145
    iput-object v7, p0, Landroid/animation/LayoutTransition;->mAppearingAnim:Landroid/animation/Animator;

    #@c
    .line 146
    iput-object v7, p0, Landroid/animation/LayoutTransition;->mChangingAppearingAnim:Landroid/animation/Animator;

    #@e
    .line 147
    iput-object v7, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingAnim:Landroid/animation/Animator;

    #@10
    .line 148
    iput-object v7, p0, Landroid/animation/LayoutTransition;->mChangingAnim:Landroid/animation/Animator;

    #@12
    .line 168
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@14
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDuration:J

    #@16
    .line 169
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@18
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDuration:J

    #@1a
    .line 170
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@1c
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingDuration:J

    #@1e
    .line 171
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@20
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mAppearingDuration:J

    #@22
    .line 172
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@24
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mDisappearingDuration:J

    #@26
    .line 180
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@28
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mAppearingDelay:J

    #@2a
    .line 181
    iput-wide v11, p0, Landroid/animation/LayoutTransition;->mDisappearingDelay:J

    #@2c
    .line 182
    iput-wide v11, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDelay:J

    #@2e
    .line 183
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@30
    iput-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDelay:J

    #@32
    .line 184
    iput-wide v11, p0, Landroid/animation/LayoutTransition;->mChangingDelay:J

    #@34
    .line 189
    iput-wide v11, p0, Landroid/animation/LayoutTransition;->mChangingAppearingStagger:J

    #@36
    .line 190
    iput-wide v11, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingStagger:J

    #@38
    .line 191
    iput-wide v11, p0, Landroid/animation/LayoutTransition;->mChangingStagger:J

    #@3a
    .line 196
    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@3c
    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@3f
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@41
    .line 197
    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@43
    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@46
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@48
    .line 198
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    #@4a
    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@4d
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mChangingAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@4f
    .line 199
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    #@51
    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@54
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@56
    .line 200
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    #@58
    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@5b
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mChangingInterpolator:Landroid/animation/TimeInterpolator;

    #@5d
    .line 210
    new-instance v6, Ljava/util/HashMap;

    #@5f
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    #@62
    iput-object v6, p0, Landroid/animation/LayoutTransition;->pendingAnimations:Ljava/util/HashMap;

    #@64
    .line 212
    new-instance v6, Ljava/util/LinkedHashMap;

    #@66
    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    #@69
    iput-object v6, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@6b
    .line 214
    new-instance v6, Ljava/util/LinkedHashMap;

    #@6d
    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    #@70
    iput-object v6, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@72
    .line 216
    new-instance v6, Ljava/util/LinkedHashMap;

    #@74
    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    #@77
    iput-object v6, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@79
    .line 228
    new-instance v6, Ljava/util/HashMap;

    #@7b
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    #@7e
    iput-object v6, p0, Landroid/animation/LayoutTransition;->layoutChangeListenerMap:Ljava/util/HashMap;

    #@80
    .line 243
    const/16 v6, 0xf

    #@82
    iput v6, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@84
    .line 257
    iput-boolean v13, p0, Landroid/animation/LayoutTransition;->mAnimateParentHierarchy:Z

    #@86
    .line 266
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@88
    if-nez v6, :cond_172

    #@8a
    .line 268
    const-string/jumbo v6, "left"

    #@8d
    new-array v8, v10, [I

    #@8f
    fill-array-data v8, :array_188

    #@92
    invoke-static {v6, v8}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@95
    move-result-object v1

    #@96
    .line 269
    .local v1, pvhLeft:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v6, "top"

    #@99
    new-array v8, v10, [I

    #@9b
    fill-array-data v8, :array_190

    #@9e
    invoke-static {v6, v8}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@a1
    move-result-object v5

    #@a2
    .line 270
    .local v5, pvhTop:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v6, "right"

    #@a5
    new-array v8, v10, [I

    #@a7
    fill-array-data v8, :array_198

    #@aa
    invoke-static {v6, v8}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@ad
    move-result-object v2

    #@ae
    .line 271
    .local v2, pvhRight:Landroid/animation/PropertyValuesHolder;
    const-string v6, "bottom"

    #@b0
    new-array v8, v10, [I

    #@b2
    fill-array-data v8, :array_1a0

    #@b5
    invoke-static {v6, v8}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@b8
    move-result-object v0

    #@b9
    .line 272
    .local v0, pvhBottom:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v6, "scrollX"

    #@bc
    new-array v8, v10, [I

    #@be
    fill-array-data v8, :array_1a8

    #@c1
    invoke-static {v6, v8}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@c4
    move-result-object v3

    #@c5
    .line 273
    .local v3, pvhScrollX:Landroid/animation/PropertyValuesHolder;
    const-string/jumbo v6, "scrollY"

    #@c8
    new-array v8, v10, [I

    #@ca
    fill-array-data v8, :array_1b0

    #@cd
    invoke-static {v6, v8}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@d0
    move-result-object v4

    #@d1
    .local v4, pvhScrollY:Landroid/animation/PropertyValuesHolder;
    move-object v6, v7

    #@d2
    .line 274
    check-cast v6, Ljava/lang/Object;

    #@d4
    const/4 v8, 0x6

    #@d5
    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    #@d7
    const/4 v9, 0x0

    #@d8
    aput-object v1, v8, v9

    #@da
    aput-object v5, v8, v13

    #@dc
    aput-object v2, v8, v10

    #@de
    const/4 v9, 0x3

    #@df
    aput-object v0, v8, v9

    #@e1
    const/4 v9, 0x4

    #@e2
    aput-object v3, v8, v9

    #@e4
    const/4 v9, 0x5

    #@e5
    aput-object v4, v8, v9

    #@e7
    invoke-static {v6, v8}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@ea
    move-result-object v6

    #@eb
    sput-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@ed
    .line 276
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@ef
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@f1
    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@f4
    .line 277
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@f6
    iget-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDelay:J

    #@f8
    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    #@fb
    .line 278
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@fd
    iget-object v8, p0, Landroid/animation/LayoutTransition;->mChangingAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@ff
    invoke-virtual {v6, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@102
    .line 279
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@104
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    #@107
    move-result-object v6

    #@108
    sput-object v6, Landroid/animation/LayoutTransition;->defaultChangeOut:Landroid/animation/ObjectAnimator;

    #@10a
    .line 280
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeOut:Landroid/animation/ObjectAnimator;

    #@10c
    iget-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDelay:J

    #@10e
    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    #@111
    .line 281
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeOut:Landroid/animation/ObjectAnimator;

    #@113
    iget-object v8, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@115
    invoke-virtual {v6, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@118
    .line 282
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@11a
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    #@11d
    move-result-object v6

    #@11e
    sput-object v6, Landroid/animation/LayoutTransition;->defaultChange:Landroid/animation/ObjectAnimator;

    #@120
    .line 283
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChange:Landroid/animation/ObjectAnimator;

    #@122
    iget-wide v8, p0, Landroid/animation/LayoutTransition;->mChangingDelay:J

    #@124
    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    #@127
    .line 284
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChange:Landroid/animation/ObjectAnimator;

    #@129
    iget-object v8, p0, Landroid/animation/LayoutTransition;->mChangingInterpolator:Landroid/animation/TimeInterpolator;

    #@12b
    invoke-virtual {v6, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@12e
    .line 286
    const-string v6, "alpha"

    #@130
    new-array v8, v10, [F

    #@132
    fill-array-data v8, :array_1b8

    #@135
    invoke-static {v7, v6, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@138
    move-result-object v6

    #@139
    sput-object v6, Landroid/animation/LayoutTransition;->defaultFadeIn:Landroid/animation/ObjectAnimator;

    #@13b
    .line 287
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeIn:Landroid/animation/ObjectAnimator;

    #@13d
    sget-wide v8, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@13f
    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@142
    .line 288
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeIn:Landroid/animation/ObjectAnimator;

    #@144
    iget-wide v8, p0, Landroid/animation/LayoutTransition;->mAppearingDelay:J

    #@146
    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    #@149
    .line 289
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeIn:Landroid/animation/ObjectAnimator;

    #@14b
    iget-object v8, p0, Landroid/animation/LayoutTransition;->mAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@14d
    invoke-virtual {v6, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@150
    .line 290
    const-string v6, "alpha"

    #@152
    new-array v8, v10, [F

    #@154
    fill-array-data v8, :array_1c0

    #@157
    invoke-static {v7, v6, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@15a
    move-result-object v6

    #@15b
    sput-object v6, Landroid/animation/LayoutTransition;->defaultFadeOut:Landroid/animation/ObjectAnimator;

    #@15d
    .line 291
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeOut:Landroid/animation/ObjectAnimator;

    #@15f
    sget-wide v7, Landroid/animation/LayoutTransition;->DEFAULT_DURATION:J

    #@161
    invoke-virtual {v6, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@164
    .line 292
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeOut:Landroid/animation/ObjectAnimator;

    #@166
    iget-wide v7, p0, Landroid/animation/LayoutTransition;->mDisappearingDelay:J

    #@168
    invoke-virtual {v6, v7, v8}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    #@16b
    .line 293
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeOut:Landroid/animation/ObjectAnimator;

    #@16d
    iget-object v7, p0, Landroid/animation/LayoutTransition;->mDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@16f
    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@172
    .line 295
    .end local v0           #pvhBottom:Landroid/animation/PropertyValuesHolder;
    .end local v1           #pvhLeft:Landroid/animation/PropertyValuesHolder;
    .end local v2           #pvhRight:Landroid/animation/PropertyValuesHolder;
    .end local v3           #pvhScrollX:Landroid/animation/PropertyValuesHolder;
    .end local v4           #pvhScrollY:Landroid/animation/PropertyValuesHolder;
    .end local v5           #pvhTop:Landroid/animation/PropertyValuesHolder;
    :cond_172
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@174
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mChangingAppearingAnim:Landroid/animation/Animator;

    #@176
    .line 296
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChangeOut:Landroid/animation/ObjectAnimator;

    #@178
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingAnim:Landroid/animation/Animator;

    #@17a
    .line 297
    sget-object v6, Landroid/animation/LayoutTransition;->defaultChange:Landroid/animation/ObjectAnimator;

    #@17c
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mChangingAnim:Landroid/animation/Animator;

    #@17e
    .line 298
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeIn:Landroid/animation/ObjectAnimator;

    #@180
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mAppearingAnim:Landroid/animation/Animator;

    #@182
    .line 299
    sget-object v6, Landroid/animation/LayoutTransition;->defaultFadeOut:Landroid/animation/ObjectAnimator;

    #@184
    iput-object v6, p0, Landroid/animation/LayoutTransition;->mDisappearingAnim:Landroid/animation/Animator;

    #@186
    .line 300
    return-void

    #@187
    .line 268
    nop

    #@188
    :array_188
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@190
    .line 269
    :array_190
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@198
    .line 270
    :array_198
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@1a0
    .line 271
    :array_1a0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@1a8
    .line 272
    :array_1a8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@1b0
    .line 273
    :array_1b0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@1b8
    .line 286
    :array_1b8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    #@1c0
    .line 290
    :array_1c0
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$000(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/animation/LayoutTransition;->layoutChangeListenerMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/animation/LayoutTransition;->pendingAnimations:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/animation/LayoutTransition;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/animation/LayoutTransition;->hasListeners()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Landroid/animation/LayoutTransition;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/animation/LayoutTransition;)Ljava/util/LinkedHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/animation/LayoutTransition;)Ljava/util/LinkedHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDelay:J

    #@2
    return-wide v0
.end method

.method static synthetic access$300(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->staggerDelay:J

    #@2
    return-wide v0
.end method

.method static synthetic access$314(Landroid/animation/LayoutTransition;J)J
    .registers 5
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->staggerDelay:J

    #@2
    add-long/2addr v0, p1

    #@3
    iput-wide v0, p0, Landroid/animation/LayoutTransition;->staggerDelay:J

    #@5
    return-wide v0
.end method

.method static synthetic access$400(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingStagger:J

    #@2
    return-wide v0
.end method

.method static synthetic access$500(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDelay:J

    #@2
    return-wide v0
.end method

.method static synthetic access$600(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingStagger:J

    #@2
    return-wide v0
.end method

.method static synthetic access$700(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDelay:J

    #@2
    return-wide v0
.end method

.method static synthetic access$800(Landroid/animation/LayoutTransition;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingStagger:J

    #@2
    return-wide v0
.end method

.method static synthetic access$900(Landroid/animation/LayoutTransition;)Ljava/util/LinkedHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2
    return-object v0
.end method

.method private addChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V
    .registers 11
    .parameter "parent"
    .parameter "child"
    .parameter "changesLayout"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x4

    #@2
    const/4 v4, 0x1

    #@3
    .line 1231
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWindowVisibility()I

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_a

    #@9
    .line 1256
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1234
    :cond_a
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@c
    and-int/lit8 v3, v3, 0x1

    #@e
    if-ne v3, v4, :cond_14

    #@10
    .line 1236
    const/4 v3, 0x3

    #@11
    invoke-virtual {p0, v3}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@14
    .line 1238
    :cond_14
    if-eqz p3, :cond_23

    #@16
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@18
    and-int/lit8 v3, v3, 0x4

    #@1a
    if-ne v3, v5, :cond_23

    #@1c
    .line 1240
    const/4 v3, 0x0

    #@1d
    invoke-virtual {p0, v3}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@20
    .line 1241
    invoke-virtual {p0, v5}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@23
    .line 1243
    :cond_23
    invoke-direct {p0}, Landroid/animation/LayoutTransition;->hasListeners()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_4b

    #@29
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@2b
    and-int/lit8 v3, v3, 0x1

    #@2d
    if-ne v3, v4, :cond_4b

    #@2f
    .line 1244
    iget-object v3, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@34
    move-result-object v2

    #@35
    check-cast v2, Ljava/util/ArrayList;

    #@37
    .line 1246
    .local v2, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3a
    move-result-object v0

    #@3b
    .local v0, i$:Ljava/util/Iterator;
    :goto_3b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_4b

    #@41
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@44
    move-result-object v1

    #@45
    check-cast v1, Landroid/animation/LayoutTransition$TransitionListener;

    #@47
    .line 1247
    .local v1, listener:Landroid/animation/LayoutTransition$TransitionListener;
    invoke-interface {v1, p0, p1, p2, v6}, Landroid/animation/LayoutTransition$TransitionListener;->startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@4a
    goto :goto_3b

    #@4b
    .line 1250
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/animation/LayoutTransition$TransitionListener;
    .end local v2           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    :cond_4b
    if-eqz p3, :cond_56

    #@4d
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@4f
    and-int/lit8 v3, v3, 0x4

    #@51
    if-ne v3, v5, :cond_56

    #@53
    .line 1251
    invoke-direct {p0, p1, p2, v6}, Landroid/animation/LayoutTransition;->runChangeTransition(Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@56
    .line 1253
    :cond_56
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@58
    and-int/lit8 v3, v3, 0x1

    #@5a
    if-ne v3, v4, :cond_9

    #@5c
    .line 1254
    invoke-direct {p0, p1, p2}, Landroid/animation/LayoutTransition;->runAppearingTransition(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@5f
    goto :goto_9
.end method

.method private hasListeners()Z
    .registers 2

    #@0
    .prologue
    .line 1259
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    if-lez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private removeChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V
    .registers 11
    .parameter "parent"
    .parameter "child"
    .parameter "changesLayout"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v4, 0x2

    #@4
    .line 1336
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWindowVisibility()I

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_b

    #@a
    .line 1363
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1339
    :cond_b
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@d
    and-int/lit8 v3, v3, 0x2

    #@f
    if-ne v3, v4, :cond_14

    #@11
    .line 1341
    invoke-virtual {p0, v4}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@14
    .line 1343
    :cond_14
    if-eqz p3, :cond_24

    #@16
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@18
    and-int/lit8 v3, v3, 0x8

    #@1a
    if-ne v3, v6, :cond_24

    #@1c
    .line 1346
    const/4 v3, 0x1

    #@1d
    invoke-virtual {p0, v3}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@20
    .line 1347
    const/4 v3, 0x4

    #@21
    invoke-virtual {p0, v3}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@24
    .line 1349
    :cond_24
    invoke-direct {p0}, Landroid/animation/LayoutTransition;->hasListeners()Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_4c

    #@2a
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@2c
    and-int/lit8 v3, v3, 0x2

    #@2e
    if-ne v3, v4, :cond_4c

    #@30
    .line 1350
    iget-object v3, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@35
    move-result-object v2

    #@36
    check-cast v2, Ljava/util/ArrayList;

    #@38
    .line 1352
    .local v2, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3b
    move-result-object v0

    #@3c
    .local v0, i$:Ljava/util/Iterator;
    :goto_3c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_4c

    #@42
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@45
    move-result-object v1

    #@46
    check-cast v1, Landroid/animation/LayoutTransition$TransitionListener;

    #@48
    .line 1353
    .local v1, listener:Landroid/animation/LayoutTransition$TransitionListener;
    invoke-interface {v1, p0, p1, p2, v5}, Landroid/animation/LayoutTransition$TransitionListener;->startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@4b
    goto :goto_3c

    #@4c
    .line 1356
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/animation/LayoutTransition$TransitionListener;
    .end local v2           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    :cond_4c
    if-eqz p3, :cond_57

    #@4e
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@50
    and-int/lit8 v3, v3, 0x8

    #@52
    if-ne v3, v6, :cond_57

    #@54
    .line 1358
    invoke-direct {p0, p1, p2, v5}, Landroid/animation/LayoutTransition;->runChangeTransition(Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@57
    .line 1360
    :cond_57
    iget v3, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@59
    and-int/lit8 v3, v3, 0x2

    #@5b
    if-ne v3, v4, :cond_a

    #@5d
    .line 1361
    invoke-direct {p0, p1, p2}, Landroid/animation/LayoutTransition;->runDisappearingTransition(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@60
    goto :goto_a
.end method

.method private runAppearingTransition(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 11
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 1133
    iget-object v5, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v5, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/animation/Animator;

    #@8
    .line 1134
    .local v1, currentAnimation:Landroid/animation/Animator;
    if-eqz v1, :cond_d

    #@a
    .line 1135
    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    #@d
    .line 1137
    :cond_d
    iget-object v5, p0, Landroid/animation/LayoutTransition;->mAppearingAnim:Landroid/animation/Animator;

    #@f
    if-nez v5, :cond_34

    #@11
    .line 1138
    invoke-direct {p0}, Landroid/animation/LayoutTransition;->hasListeners()Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_63

    #@17
    .line 1139
    iget-object v5, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@1c
    move-result-object v4

    #@1d
    check-cast v4, Ljava/util/ArrayList;

    #@1f
    .line 1141
    .local v4, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v2

    #@23
    .local v2, i$:Ljava/util/Iterator;
    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_63

    #@29
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/animation/LayoutTransition$TransitionListener;

    #@2f
    .line 1142
    .local v3, listener:Landroid/animation/LayoutTransition$TransitionListener;
    const/4 v5, 0x2

    #@30
    invoke-interface {v3, p0, p1, p2, v5}, Landroid/animation/LayoutTransition$TransitionListener;->endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@33
    goto :goto_23

    #@34
    .line 1147
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #listener:Landroid/animation/LayoutTransition$TransitionListener;
    .end local v4           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    :cond_34
    iget-object v5, p0, Landroid/animation/LayoutTransition;->mAppearingAnim:Landroid/animation/Animator;

    #@36
    invoke-virtual {v5}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    #@39
    move-result-object v0

    #@3a
    .line 1148
    .local v0, anim:Landroid/animation/Animator;
    invoke-virtual {v0, p2}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@3d
    .line 1149
    iget-wide v5, p0, Landroid/animation/LayoutTransition;->mAppearingDelay:J

    #@3f
    invoke-virtual {v0, v5, v6}, Landroid/animation/Animator;->setStartDelay(J)V

    #@42
    .line 1150
    iget-wide v5, p0, Landroid/animation/LayoutTransition;->mAppearingDuration:J

    #@44
    invoke-virtual {v0, v5, v6}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@47
    .line 1151
    instance-of v5, v0, Landroid/animation/ObjectAnimator;

    #@49
    if-eqz v5, :cond_53

    #@4b
    move-object v5, v0

    #@4c
    .line 1152
    check-cast v5, Landroid/animation/ObjectAnimator;

    #@4e
    const-wide/16 v6, 0x0

    #@50
    invoke-virtual {v5, v6, v7}, Landroid/animation/ObjectAnimator;->setCurrentPlayTime(J)V

    #@53
    .line 1154
    :cond_53
    new-instance v5, Landroid/animation/LayoutTransition$5;

    #@55
    invoke-direct {v5, p0, p2, p1}, Landroid/animation/LayoutTransition$5;-><init>(Landroid/animation/LayoutTransition;Landroid/view/View;Landroid/view/ViewGroup;)V

    #@58
    invoke-virtual {v0, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@5b
    .line 1167
    iget-object v5, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@5d
    invoke-virtual {v5, p2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    .line 1168
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@63
    .line 1169
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_63
    return-void
.end method

.method private runChangeTransition(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .registers 24
    .parameter "parent"
    .parameter "newView"
    .parameter "changeReason"

    #@0
    .prologue
    .line 716
    const/4 v5, 0x0

    #@1
    .line 717
    .local v5, baseAnimator:Landroid/animation/Animator;
    const/4 v12, 0x0

    #@2
    .line 719
    .local v12, parentAnimator:Landroid/animation/Animator;
    packed-switch p3, :pswitch_data_96

    #@5
    .line 737
    const-wide/16 v6, 0x0

    #@7
    .line 741
    .local v6, duration:J
    :goto_7
    if-nez v5, :cond_2b

    #@9
    .line 796
    :cond_9
    :goto_9
    return-void

    #@a
    .line 721
    .end local v6           #duration:J
    :pswitch_a
    move-object/from16 v0, p0

    #@c
    iget-object v5, v0, Landroid/animation/LayoutTransition;->mChangingAppearingAnim:Landroid/animation/Animator;

    #@e
    .line 722
    move-object/from16 v0, p0

    #@10
    iget-wide v6, v0, Landroid/animation/LayoutTransition;->mChangingAppearingDuration:J

    #@12
    .line 723
    .restart local v6       #duration:J
    sget-object v12, Landroid/animation/LayoutTransition;->defaultChangeIn:Landroid/animation/ObjectAnimator;

    #@14
    .line 724
    goto :goto_7

    #@15
    .line 726
    .end local v6           #duration:J
    :pswitch_15
    move-object/from16 v0, p0

    #@17
    iget-object v5, v0, Landroid/animation/LayoutTransition;->mChangingDisappearingAnim:Landroid/animation/Animator;

    #@19
    .line 727
    move-object/from16 v0, p0

    #@1b
    iget-wide v6, v0, Landroid/animation/LayoutTransition;->mChangingDisappearingDuration:J

    #@1d
    .line 728
    .restart local v6       #duration:J
    sget-object v12, Landroid/animation/LayoutTransition;->defaultChangeOut:Landroid/animation/ObjectAnimator;

    #@1f
    .line 729
    goto :goto_7

    #@20
    .line 731
    .end local v6           #duration:J
    :pswitch_20
    move-object/from16 v0, p0

    #@22
    iget-object v5, v0, Landroid/animation/LayoutTransition;->mChangingAnim:Landroid/animation/Animator;

    #@24
    .line 732
    move-object/from16 v0, p0

    #@26
    iget-wide v6, v0, Landroid/animation/LayoutTransition;->mChangingDuration:J

    #@28
    .line 733
    .restart local v6       #duration:J
    sget-object v12, Landroid/animation/LayoutTransition;->defaultChange:Landroid/animation/ObjectAnimator;

    #@2a
    .line 734
    goto :goto_7

    #@2b
    .line 746
    :cond_2b
    const-wide/16 v2, 0x0

    #@2d
    move-object/from16 v0, p0

    #@2f
    iput-wide v2, v0, Landroid/animation/LayoutTransition;->staggerDelay:J

    #@31
    .line 748
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@34
    move-result-object v18

    #@35
    .line 749
    .local v18, observer:Landroid/view/ViewTreeObserver;
    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewTreeObserver;->isAlive()Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_9

    #@3b
    .line 753
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getChildCount()I

    #@3e
    move-result v17

    #@3f
    .line 755
    .local v17, numChildren:I
    const/16 v16, 0x0

    #@41
    .local v16, i:I
    :goto_41
    move/from16 v0, v16

    #@43
    move/from16 v1, v17

    #@45
    if-ge v0, v1, :cond_5f

    #@47
    .line 756
    move-object/from16 v0, p1

    #@49
    move/from16 v1, v16

    #@4b
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@4e
    move-result-object v8

    #@4f
    .line 759
    .local v8, child:Landroid/view/View;
    move-object/from16 v0, p2

    #@51
    if-eq v8, v0, :cond_5c

    #@53
    move-object/from16 v2, p0

    #@55
    move-object/from16 v3, p1

    #@57
    move/from16 v4, p3

    #@59
    .line 760
    invoke-direct/range {v2 .. v8}, Landroid/animation/LayoutTransition;->setupChangeAnimation(Landroid/view/ViewGroup;ILandroid/animation/Animator;JLandroid/view/View;)V

    #@5c
    .line 755
    :cond_5c
    add-int/lit8 v16, v16, 0x1

    #@5e
    goto :goto_41

    #@5f
    .line 763
    .end local v8           #child:Landroid/view/View;
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-boolean v2, v0, Landroid/animation/LayoutTransition;->mAnimateParentHierarchy:Z

    #@63
    if-eqz v2, :cond_86

    #@65
    .line 764
    move-object/from16 v15, p1

    #@67
    .line 765
    .local v15, tempParent:Landroid/view/ViewGroup;
    :goto_67
    if-eqz v15, :cond_86

    #@69
    .line 766
    invoke-virtual {v15}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    #@6c
    move-result-object v19

    #@6d
    .line 767
    .local v19, parentParent:Landroid/view/ViewParent;
    move-object/from16 v0, v19

    #@6f
    instance-of v2, v0, Landroid/view/ViewGroup;

    #@71
    if-eqz v2, :cond_84

    #@73
    move-object/from16 v10, v19

    #@75
    .line 768
    check-cast v10, Landroid/view/ViewGroup;

    #@77
    move-object/from16 v9, p0

    #@79
    move/from16 v11, p3

    #@7b
    move-wide v13, v6

    #@7c
    invoke-direct/range {v9 .. v15}, Landroid/animation/LayoutTransition;->setupChangeAnimation(Landroid/view/ViewGroup;ILandroid/animation/Animator;JLandroid/view/View;)V

    #@7f
    move-object/from16 v15, v19

    #@81
    .line 770
    check-cast v15, Landroid/view/ViewGroup;

    #@83
    goto :goto_67

    #@84
    .line 772
    :cond_84
    const/4 v15, 0x0

    #@85
    goto :goto_67

    #@86
    .line 781
    .end local v15           #tempParent:Landroid/view/ViewGroup;
    .end local v19           #parentParent:Landroid/view/ViewParent;
    :cond_86
    new-instance v2, Landroid/animation/LayoutTransition$1;

    #@88
    move-object/from16 v0, p0

    #@8a
    move-object/from16 v1, p1

    #@8c
    invoke-direct {v2, v0, v1}, Landroid/animation/LayoutTransition$1;-><init>(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;)V

    #@8f
    move-object/from16 v0, v18

    #@91
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@94
    goto/16 :goto_9

    #@96
    .line 719
    :pswitch_data_96
    .packed-switch 0x2
        :pswitch_a
        :pswitch_15
        :pswitch_20
    .end packed-switch
.end method

.method private runDisappearingTransition(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 12
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 1178
    iget-object v6, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v6, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/animation/Animator;

    #@8
    .line 1179
    .local v1, currentAnimation:Landroid/animation/Animator;
    if-eqz v1, :cond_d

    #@a
    .line 1180
    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    #@d
    .line 1182
    :cond_d
    iget-object v6, p0, Landroid/animation/LayoutTransition;->mDisappearingAnim:Landroid/animation/Animator;

    #@f
    if-nez v6, :cond_34

    #@11
    .line 1183
    invoke-direct {p0}, Landroid/animation/LayoutTransition;->hasListeners()Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_67

    #@17
    .line 1184
    iget-object v6, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v6}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@1c
    move-result-object v4

    #@1d
    check-cast v4, Ljava/util/ArrayList;

    #@1f
    .line 1186
    .local v4, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v2

    #@23
    .local v2, i$:Ljava/util/Iterator;
    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_67

    #@29
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/animation/LayoutTransition$TransitionListener;

    #@2f
    .line 1187
    .local v3, listener:Landroid/animation/LayoutTransition$TransitionListener;
    const/4 v6, 0x3

    #@30
    invoke-interface {v3, p0, p1, p2, v6}, Landroid/animation/LayoutTransition$TransitionListener;->endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@33
    goto :goto_23

    #@34
    .line 1192
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #listener:Landroid/animation/LayoutTransition$TransitionListener;
    .end local v4           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    :cond_34
    iget-object v6, p0, Landroid/animation/LayoutTransition;->mDisappearingAnim:Landroid/animation/Animator;

    #@36
    invoke-virtual {v6}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    #@39
    move-result-object v0

    #@3a
    .line 1193
    .local v0, anim:Landroid/animation/Animator;
    iget-wide v6, p0, Landroid/animation/LayoutTransition;->mDisappearingDelay:J

    #@3c
    invoke-virtual {v0, v6, v7}, Landroid/animation/Animator;->setStartDelay(J)V

    #@3f
    .line 1194
    iget-wide v6, p0, Landroid/animation/LayoutTransition;->mDisappearingDuration:J

    #@41
    invoke-virtual {v0, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@44
    .line 1195
    invoke-virtual {v0, p2}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@47
    .line 1196
    invoke-virtual {p2}, Landroid/view/View;->getAlpha()F

    #@4a
    move-result v5

    #@4b
    .line 1197
    .local v5, preAnimAlpha:F
    new-instance v6, Landroid/animation/LayoutTransition$6;

    #@4d
    invoke-direct {v6, p0, p2, v5, p1}, Landroid/animation/LayoutTransition$6;-><init>(Landroid/animation/LayoutTransition;Landroid/view/View;FLandroid/view/ViewGroup;)V

    #@50
    invoke-virtual {v0, v6}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@53
    .line 1211
    instance-of v6, v0, Landroid/animation/ObjectAnimator;

    #@55
    if-eqz v6, :cond_5f

    #@57
    move-object v6, v0

    #@58
    .line 1212
    check-cast v6, Landroid/animation/ObjectAnimator;

    #@5a
    const-wide/16 v7, 0x0

    #@5c
    invoke-virtual {v6, v7, v8}, Landroid/animation/ObjectAnimator;->setCurrentPlayTime(J)V

    #@5f
    .line 1214
    :cond_5f
    iget-object v6, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@61
    invoke-virtual {v6, p2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@64
    .line 1215
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@67
    .line 1216
    .end local v0           #anim:Landroid/animation/Animator;
    .end local v5           #preAnimAlpha:F
    :cond_67
    return-void
.end method

.method private setupChangeAnimation(Landroid/view/ViewGroup;ILandroid/animation/Animator;JLandroid/view/View;)V
    .registers 19
    .parameter "parent"
    .parameter "changeReason"
    .parameter "baseAnimator"
    .parameter "duration"
    .parameter "child"

    #@0
    .prologue
    .line 830
    iget-object v2, p0, Landroid/animation/LayoutTransition;->layoutChangeListenerMap:Ljava/util/HashMap;

    #@2
    move-object/from16 v0, p6

    #@4
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 982
    :cond_a
    :goto_a
    return-void

    #@b
    .line 838
    :cond_b
    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getWidth()I

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_17

    #@11
    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getHeight()I

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_a

    #@17
    .line 843
    :cond_17
    invoke-virtual {p3}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    #@1a
    move-result-object v3

    #@1b
    .line 846
    .local v3, anim:Landroid/animation/Animator;
    move-object/from16 v0, p6

    #@1d
    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@20
    .line 850
    invoke-virtual {v3}, Landroid/animation/Animator;->setupStartValues()V

    #@23
    .line 853
    iget-object v2, p0, Landroid/animation/LayoutTransition;->pendingAnimations:Ljava/util/HashMap;

    #@25
    move-object/from16 v0, p6

    #@27
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    move-result-object v10

    #@2b
    check-cast v10, Landroid/animation/Animator;

    #@2d
    .line 854
    .local v10, currentAnimation:Landroid/animation/Animator;
    if-eqz v10, :cond_39

    #@2f
    .line 855
    invoke-virtual {v10}, Landroid/animation/Animator;->cancel()V

    #@32
    .line 856
    iget-object v2, p0, Landroid/animation/LayoutTransition;->pendingAnimations:Ljava/util/HashMap;

    #@34
    move-object/from16 v0, p6

    #@36
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@39
    .line 859
    :cond_39
    iget-object v2, p0, Landroid/animation/LayoutTransition;->pendingAnimations:Ljava/util/HashMap;

    #@3b
    move-object/from16 v0, p6

    #@3d
    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    .line 866
    const/4 v2, 0x2

    #@41
    new-array v2, v2, [F

    #@43
    fill-array-data v2, :array_86

    #@46
    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    #@49
    move-result-object v2

    #@4a
    const-wide/16 v4, 0x64

    #@4c
    add-long v4, v4, p4

    #@4e
    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@51
    move-result-object v11

    #@52
    .line 868
    .local v11, pendingAnimRemover:Landroid/animation/ValueAnimator;
    new-instance v2, Landroid/animation/LayoutTransition$2;

    #@54
    move-object/from16 v0, p6

    #@56
    invoke-direct {v2, p0, v0}, Landroid/animation/LayoutTransition$2;-><init>(Landroid/animation/LayoutTransition;Landroid/view/View;)V

    #@59
    invoke-virtual {v11, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@5c
    .line 874
    invoke-virtual {v11}, Landroid/animation/ValueAnimator;->start()V

    #@5f
    .line 878
    new-instance v1, Landroid/animation/LayoutTransition$3;

    #@61
    move-object v2, p0

    #@62
    move v4, p2

    #@63
    move-wide/from16 v5, p4

    #@65
    move-object/from16 v7, p6

    #@67
    move-object v8, p1

    #@68
    invoke-direct/range {v1 .. v8}, Landroid/animation/LayoutTransition$3;-><init>(Landroid/animation/LayoutTransition;Landroid/animation/Animator;IJLandroid/view/View;Landroid/view/ViewGroup;)V

    #@6b
    .line 941
    .local v1, listener:Landroid/view/View$OnLayoutChangeListener;
    new-instance v4, Landroid/animation/LayoutTransition$4;

    #@6d
    move-object v5, p0

    #@6e
    move-object v6, p1

    #@6f
    move-object/from16 v7, p6

    #@71
    move v8, p2

    #@72
    move-object v9, v1

    #@73
    invoke-direct/range {v4 .. v9}, Landroid/animation/LayoutTransition$4;-><init>(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;ILandroid/view/View$OnLayoutChangeListener;)V

    #@76
    invoke-virtual {v3, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@79
    .line 979
    move-object/from16 v0, p6

    #@7b
    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    #@7e
    .line 981
    iget-object v2, p0, Landroid/animation/LayoutTransition;->layoutChangeListenerMap:Ljava/util/HashMap;

    #@80
    move-object/from16 v0, p6

    #@82
    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@85
    goto :goto_a

    #@86
    .line 866
    :array_86
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method


# virtual methods
.method public addChild(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 1297
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/animation/LayoutTransition;->addChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    #@4
    .line 1298
    return-void
.end method

.method public addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 1408
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1409
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@b
    .line 1411
    :cond_b
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 1412
    return-void
.end method

.method public cancel()V
    .registers 5

    #@0
    .prologue
    .line 1055
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    #@5
    move-result v3

    #@6
    if-lez v3, :cond_2d

    #@8
    .line 1056
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@a
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Ljava/util/LinkedHashMap;

    #@10
    .line 1058
    .local v1, currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@13
    move-result-object v3

    #@14
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v2

    #@18
    .local v2, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_28

    #@1e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/animation/Animator;

    #@24
    .line 1059
    .local v0, anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    #@27
    goto :goto_18

    #@28
    .line 1061
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_28
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2a
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@2d
    .line 1063
    .end local v1           #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_2d
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@2f
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    #@32
    move-result v3

    #@33
    if-lez v3, :cond_5a

    #@35
    .line 1064
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@37
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@3a
    move-result-object v1

    #@3b
    check-cast v1, Ljava/util/LinkedHashMap;

    #@3d
    .line 1066
    .restart local v1       #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@40
    move-result-object v3

    #@41
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@44
    move-result-object v2

    #@45
    .restart local v2       #i$:Ljava/util/Iterator;
    :goto_45
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@48
    move-result v3

    #@49
    if-eqz v3, :cond_55

    #@4b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4e
    move-result-object v0

    #@4f
    check-cast v0, Landroid/animation/Animator;

    #@51
    .line 1067
    .restart local v0       #anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@54
    goto :goto_45

    #@55
    .line 1069
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_55
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@57
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@5a
    .line 1071
    .end local v1           #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_5a
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@5c
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    #@5f
    move-result v3

    #@60
    if-lez v3, :cond_87

    #@62
    .line 1072
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@64
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@67
    move-result-object v1

    #@68
    check-cast v1, Ljava/util/LinkedHashMap;

    #@6a
    .line 1074
    .restart local v1       #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@6d
    move-result-object v3

    #@6e
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@71
    move-result-object v2

    #@72
    .restart local v2       #i$:Ljava/util/Iterator;
    :goto_72
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@75
    move-result v3

    #@76
    if-eqz v3, :cond_82

    #@78
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7b
    move-result-object v0

    #@7c
    check-cast v0, Landroid/animation/Animator;

    #@7e
    .line 1075
    .restart local v0       #anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@81
    goto :goto_72

    #@82
    .line 1077
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_82
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@84
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@87
    .line 1079
    .end local v1           #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_87
    return-void
.end method

.method public cancel(I)V
    .registers 6
    .parameter "transitionType"

    #@0
    .prologue
    .line 1090
    packed-switch p1, :pswitch_data_90

    #@3
    .line 1124
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1094
    :pswitch_4
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@6
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    #@9
    move-result v3

    #@a
    if-lez v3, :cond_3

    #@c
    .line 1095
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@e
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Ljava/util/LinkedHashMap;

    #@14
    .line 1097
    .local v1, currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@17
    move-result-object v3

    #@18
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v2

    #@1c
    .local v2, i$:Ljava/util/Iterator;
    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_2c

    #@22
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Landroid/animation/Animator;

    #@28
    .line 1098
    .local v0, anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    #@2b
    goto :goto_1c

    #@2c
    .line 1100
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_2c
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2e
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@31
    goto :goto_3

    #@32
    .line 1104
    .end local v1           #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :pswitch_32
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@34
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    #@37
    move-result v3

    #@38
    if-lez v3, :cond_3

    #@3a
    .line 1105
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@3c
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@3f
    move-result-object v1

    #@40
    check-cast v1, Ljava/util/LinkedHashMap;

    #@42
    .line 1107
    .restart local v1       #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@45
    move-result-object v3

    #@46
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v2

    #@4a
    .restart local v2       #i$:Ljava/util/Iterator;
    :goto_4a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v3

    #@4e
    if-eqz v3, :cond_5a

    #@50
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v0

    #@54
    check-cast v0, Landroid/animation/Animator;

    #@56
    .line 1108
    .restart local v0       #anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@59
    goto :goto_4a

    #@5a
    .line 1110
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_5a
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@5c
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@5f
    goto :goto_3

    #@60
    .line 1114
    .end local v1           #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :pswitch_60
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@62
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    #@65
    move-result v3

    #@66
    if-lez v3, :cond_3

    #@68
    .line 1115
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@6a
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@6d
    move-result-object v1

    #@6e
    check-cast v1, Ljava/util/LinkedHashMap;

    #@70
    .line 1117
    .restart local v1       #currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@73
    move-result-object v3

    #@74
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@77
    move-result-object v2

    #@78
    .restart local v2       #i$:Ljava/util/Iterator;
    :goto_78
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@7b
    move-result v3

    #@7c
    if-eqz v3, :cond_88

    #@7e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@81
    move-result-object v0

    #@82
    check-cast v0, Landroid/animation/Animator;

    #@84
    .line 1118
    .restart local v0       #anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@87
    goto :goto_78

    #@88
    .line 1120
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_88
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@8a
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@8d
    goto/16 :goto_3

    #@8f
    .line 1090
    nop

    #@90
    :pswitch_data_90
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_32
        :pswitch_60
        :pswitch_4
    .end packed-switch
.end method

.method public disableTransitionType(I)V
    .registers 3
    .parameter "transitionType"

    #@0
    .prologue
    .line 357
    packed-switch p1, :pswitch_data_28

    #@3
    .line 374
    :goto_3
    return-void

    #@4
    .line 359
    :pswitch_4
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@6
    and-int/lit8 v0, v0, -0x2

    #@8
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@a
    goto :goto_3

    #@b
    .line 362
    :pswitch_b
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@d
    and-int/lit8 v0, v0, -0x3

    #@f
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@11
    goto :goto_3

    #@12
    .line 365
    :pswitch_12
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@14
    and-int/lit8 v0, v0, -0x5

    #@16
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@18
    goto :goto_3

    #@19
    .line 368
    :pswitch_19
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@1b
    and-int/lit8 v0, v0, -0x9

    #@1d
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@1f
    goto :goto_3

    #@20
    .line 371
    :pswitch_20
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@22
    and-int/lit8 v0, v0, -0x11

    #@24
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@26
    goto :goto_3

    #@27
    .line 357
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_12
        :pswitch_19
        :pswitch_4
        :pswitch_b
        :pswitch_20
    .end packed-switch
.end method

.method public enableTransitionType(I)V
    .registers 3
    .parameter "transitionType"

    #@0
    .prologue
    .line 330
    packed-switch p1, :pswitch_data_28

    #@3
    .line 347
    :goto_3
    return-void

    #@4
    .line 332
    :pswitch_4
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@6
    or-int/lit8 v0, v0, 0x1

    #@8
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@a
    goto :goto_3

    #@b
    .line 335
    :pswitch_b
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@d
    or-int/lit8 v0, v0, 0x2

    #@f
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@11
    goto :goto_3

    #@12
    .line 338
    :pswitch_12
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@14
    or-int/lit8 v0, v0, 0x4

    #@16
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@18
    goto :goto_3

    #@19
    .line 341
    :pswitch_19
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@1b
    or-int/lit8 v0, v0, 0x8

    #@1d
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@1f
    goto :goto_3

    #@20
    .line 344
    :pswitch_20
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@22
    or-int/lit8 v0, v0, 0x10

    #@24
    iput v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@26
    goto :goto_3

    #@27
    .line 330
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_12
        :pswitch_19
        :pswitch_4
        :pswitch_b
        :pswitch_20
    .end packed-switch
.end method

.method public endChangingAnimations()V
    .registers 5

    #@0
    .prologue
    .line 1014
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Ljava/util/LinkedHashMap;

    #@8
    .line 1016
    .local v1, currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@b
    move-result-object v3

    #@c
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v2

    #@10
    .local v2, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_23

    #@16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/animation/Animator;

    #@1c
    .line 1017
    .local v0, anim:Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@1f
    .line 1018
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@22
    goto :goto_10

    #@23
    .line 1021
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_23
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@25
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    #@28
    .line 1022
    return-void
.end method

.method public getAnimator(I)Landroid/animation/Animator;
    .registers 3
    .parameter "transitionType"

    #@0
    .prologue
    .line 683
    packed-switch p1, :pswitch_data_14

    #@3
    .line 696
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 685
    :pswitch_5
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingAnim:Landroid/animation/Animator;

    #@7
    goto :goto_4

    #@8
    .line 687
    :pswitch_8
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingAnim:Landroid/animation/Animator;

    #@a
    goto :goto_4

    #@b
    .line 689
    :pswitch_b
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mChangingAnim:Landroid/animation/Animator;

    #@d
    goto :goto_4

    #@e
    .line 691
    :pswitch_e
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mAppearingAnim:Landroid/animation/Animator;

    #@10
    goto :goto_4

    #@11
    .line 693
    :pswitch_11
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mDisappearingAnim:Landroid/animation/Animator;

    #@13
    goto :goto_4

    #@14
    .line 683
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_5
        :pswitch_8
        :pswitch_e
        :pswitch_11
        :pswitch_b
    .end packed-switch
.end method

.method public getDuration(I)J
    .registers 4
    .parameter "transitionType"

    #@0
    .prologue
    .line 502
    packed-switch p1, :pswitch_data_16

    #@3
    .line 515
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 504
    :pswitch_6
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDuration:J

    #@8
    goto :goto_5

    #@9
    .line 506
    :pswitch_9
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDuration:J

    #@b
    goto :goto_5

    #@c
    .line 508
    :pswitch_c
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDuration:J

    #@e
    goto :goto_5

    #@f
    .line 510
    :pswitch_f
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mAppearingDuration:J

    #@11
    goto :goto_5

    #@12
    .line 512
    :pswitch_12
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mDisappearingDuration:J

    #@14
    goto :goto_5

    #@15
    .line 502
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_f
        :pswitch_12
        :pswitch_c
    .end packed-switch
.end method

.method public getInterpolator(I)Landroid/animation/TimeInterpolator;
    .registers 3
    .parameter "transitionType"

    #@0
    .prologue
    .line 607
    packed-switch p1, :pswitch_data_14

    #@3
    .line 620
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 609
    :pswitch_5
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@7
    goto :goto_4

    #@8
    .line 611
    :pswitch_8
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@a
    goto :goto_4

    #@b
    .line 613
    :pswitch_b
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mChangingInterpolator:Landroid/animation/TimeInterpolator;

    #@d
    goto :goto_4

    #@e
    .line 615
    :pswitch_e
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@10
    goto :goto_4

    #@11
    .line 617
    :pswitch_11
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@13
    goto :goto_4

    #@14
    .line 607
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_5
        :pswitch_8
        :pswitch_e
        :pswitch_11
        :pswitch_b
    .end packed-switch
.end method

.method public getStagger(I)J
    .registers 4
    .parameter "transitionType"

    #@0
    .prologue
    .line 552
    packed-switch p1, :pswitch_data_10

    #@3
    .line 561
    :pswitch_3
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 554
    :pswitch_6
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingStagger:J

    #@8
    goto :goto_5

    #@9
    .line 556
    :pswitch_9
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingStagger:J

    #@b
    goto :goto_5

    #@c
    .line 558
    :pswitch_c
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingStagger:J

    #@e
    goto :goto_5

    #@f
    .line 552
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_3
        :pswitch_3
        :pswitch_c
    .end packed-switch
.end method

.method public getStartDelay(I)J
    .registers 4
    .parameter "transitionType"

    #@0
    .prologue
    .line 443
    packed-switch p1, :pswitch_data_16

    #@3
    .line 456
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 445
    :pswitch_6
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDelay:J

    #@8
    goto :goto_5

    #@9
    .line 447
    :pswitch_9
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDelay:J

    #@b
    goto :goto_5

    #@c
    .line 449
    :pswitch_c
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mChangingDelay:J

    #@e
    goto :goto_5

    #@f
    .line 451
    :pswitch_f
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mAppearingDelay:J

    #@11
    goto :goto_5

    #@12
    .line 453
    :pswitch_12
    iget-wide v0, p0, Landroid/animation/LayoutTransition;->mDisappearingDelay:J

    #@14
    goto :goto_5

    #@15
    .line 443
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_f
        :pswitch_12
        :pswitch_c
    .end packed-switch
.end method

.method public getTransitionListeners()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/animation/LayoutTransition$TransitionListener;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1431
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public hideChild(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1383
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    #@4
    .line 1384
    return-void
.end method

.method public hideChild(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .registers 5
    .parameter "parent"
    .parameter "child"
    .parameter "newVisibility"

    #@0
    .prologue
    .line 1398
    const/16 v0, 0x8

    #@2
    if-ne p3, v0, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-direct {p0, p1, p2, v0}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    #@8
    .line 1399
    return-void

    #@9
    .line 1398
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public isChangingLayout()Z
    .registers 2

    #@0
    .prologue
    .line 1033
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    #@5
    move-result v0

    #@6
    if-lez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isRunning()Z
    .registers 2

    #@0
    .prologue
    .line 1042
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    #@5
    move-result v0

    #@6
    if-gtz v0, :cond_18

    #@8
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentAppearingAnimations:Ljava/util/LinkedHashMap;

    #@a
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    #@d
    move-result v0

    #@e
    if-gtz v0, :cond_18

    #@10
    iget-object v0, p0, Landroid/animation/LayoutTransition;->currentDisappearingAnimations:Ljava/util/LinkedHashMap;

    #@12
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    #@15
    move-result v0

    #@16
    if-lez v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public isTransitionTypeEnabled(I)Z
    .registers 6
    .parameter "transitionType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 385
    packed-switch p1, :pswitch_data_36

    #@5
    move v0, v1

    #@6
    .line 397
    :cond_6
    :goto_6
    return v0

    #@7
    .line 387
    :pswitch_7
    iget v2, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@9
    and-int/lit8 v2, v2, 0x1

    #@b
    if-eq v2, v0, :cond_6

    #@d
    move v0, v1

    #@e
    goto :goto_6

    #@f
    .line 389
    :pswitch_f
    iget v2, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@11
    and-int/lit8 v2, v2, 0x2

    #@13
    const/4 v3, 0x2

    #@14
    if-eq v2, v3, :cond_6

    #@16
    move v0, v1

    #@17
    goto :goto_6

    #@18
    .line 391
    :pswitch_18
    iget v2, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@1a
    and-int/lit8 v2, v2, 0x4

    #@1c
    const/4 v3, 0x4

    #@1d
    if-eq v2, v3, :cond_6

    #@1f
    move v0, v1

    #@20
    goto :goto_6

    #@21
    .line 393
    :pswitch_21
    iget v2, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@23
    and-int/lit8 v2, v2, 0x8

    #@25
    const/16 v3, 0x8

    #@27
    if-eq v2, v3, :cond_6

    #@29
    move v0, v1

    #@2a
    goto :goto_6

    #@2b
    .line 395
    :pswitch_2b
    iget v2, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@2d
    and-int/lit8 v2, v2, 0x10

    #@2f
    const/16 v3, 0x10

    #@31
    if-eq v2, v3, :cond_6

    #@33
    move v0, v1

    #@34
    goto :goto_6

    #@35
    .line 385
    nop

    #@36
    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_18
        :pswitch_21
        :pswitch_7
        :pswitch_f
        :pswitch_2b
    .end packed-switch
.end method

.method public layoutChange(Landroid/view/ViewGroup;)V
    .registers 4
    .parameter "parent"

    #@0
    .prologue
    .line 1275
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWindowVisibility()I

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 1285
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1278
    :cond_7
    iget v0, p0, Landroid/animation/LayoutTransition;->mTransitionTypes:I

    #@9
    and-int/lit8 v0, v0, 0x10

    #@b
    const/16 v1, 0x10

    #@d
    if-ne v0, v1, :cond_6

    #@f
    invoke-virtual {p0}, Landroid/animation/LayoutTransition;->isRunning()Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_6

    #@15
    .line 1283
    const/4 v0, 0x0

    #@16
    const/4 v1, 0x4

    #@17
    invoke-direct {p0, p1, v0, v1}, Landroid/animation/LayoutTransition;->runChangeTransition(Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@1a
    goto :goto_6
.end method

.method public removeChild(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 1375
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    #@4
    .line 1376
    return-void
.end method

.method public removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 1420
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1424
    :goto_4
    return-void

    #@5
    .line 1423
    :cond_5
    iget-object v0, p0, Landroid/animation/LayoutTransition;->mListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a
    goto :goto_4
.end method

.method public setAnimateParentHierarchy(Z)V
    .registers 2
    .parameter "animateParentHierarchy"

    #@0
    .prologue
    .line 816
    iput-boolean p1, p0, Landroid/animation/LayoutTransition;->mAnimateParentHierarchy:Z

    #@2
    .line 817
    return-void
.end method

.method public setAnimator(ILandroid/animation/Animator;)V
    .registers 3
    .parameter "transitionType"
    .parameter "animator"

    #@0
    .prologue
    .line 654
    packed-switch p1, :pswitch_data_14

    #@3
    .line 671
    :goto_3
    return-void

    #@4
    .line 656
    :pswitch_4
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mChangingAppearingAnim:Landroid/animation/Animator;

    #@6
    goto :goto_3

    #@7
    .line 659
    :pswitch_7
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingAnim:Landroid/animation/Animator;

    #@9
    goto :goto_3

    #@a
    .line 662
    :pswitch_a
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mChangingAnim:Landroid/animation/Animator;

    #@c
    goto :goto_3

    #@d
    .line 665
    :pswitch_d
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mAppearingAnim:Landroid/animation/Animator;

    #@f
    goto :goto_3

    #@10
    .line 668
    :pswitch_10
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mDisappearingAnim:Landroid/animation/Animator;

    #@12
    goto :goto_3

    #@13
    .line 654
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_4
        :pswitch_7
        :pswitch_d
        :pswitch_10
        :pswitch_a
    .end packed-switch
.end method

.method public setDuration(IJ)V
    .registers 4
    .parameter "transitionType"
    .parameter "duration"

    #@0
    .prologue
    .line 471
    packed-switch p1, :pswitch_data_14

    #@3
    .line 488
    :goto_3
    return-void

    #@4
    .line 473
    :pswitch_4
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDuration:J

    #@6
    goto :goto_3

    #@7
    .line 476
    :pswitch_7
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDuration:J

    #@9
    goto :goto_3

    #@a
    .line 479
    :pswitch_a
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingDuration:J

    #@c
    goto :goto_3

    #@d
    .line 482
    :pswitch_d
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mAppearingDuration:J

    #@f
    goto :goto_3

    #@10
    .line 485
    :pswitch_10
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mDisappearingDuration:J

    #@12
    goto :goto_3

    #@13
    .line 471
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_4
        :pswitch_7
        :pswitch_d
        :pswitch_10
        :pswitch_a
    .end packed-switch
.end method

.method public setDuration(J)V
    .registers 3
    .parameter "duration"

    #@0
    .prologue
    .line 311
    iput-wide p1, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDuration:J

    #@2
    .line 312
    iput-wide p1, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDuration:J

    #@4
    .line 313
    iput-wide p1, p0, Landroid/animation/LayoutTransition;->mChangingDuration:J

    #@6
    .line 314
    iput-wide p1, p0, Landroid/animation/LayoutTransition;->mAppearingDuration:J

    #@8
    .line 315
    iput-wide p1, p0, Landroid/animation/LayoutTransition;->mDisappearingDuration:J

    #@a
    .line 316
    return-void
.end method

.method public setInterpolator(ILandroid/animation/TimeInterpolator;)V
    .registers 3
    .parameter "transitionType"
    .parameter "interpolator"

    #@0
    .prologue
    .line 576
    packed-switch p1, :pswitch_data_14

    #@3
    .line 593
    :goto_3
    return-void

    #@4
    .line 578
    :pswitch_4
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mChangingAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@6
    goto :goto_3

    #@7
    .line 581
    :pswitch_7
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@9
    goto :goto_3

    #@a
    .line 584
    :pswitch_a
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mChangingInterpolator:Landroid/animation/TimeInterpolator;

    #@c
    goto :goto_3

    #@d
    .line 587
    :pswitch_d
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mAppearingInterpolator:Landroid/animation/TimeInterpolator;

    #@f
    goto :goto_3

    #@10
    .line 590
    :pswitch_10
    iput-object p2, p0, Landroid/animation/LayoutTransition;->mDisappearingInterpolator:Landroid/animation/TimeInterpolator;

    #@12
    goto :goto_3

    #@13
    .line 576
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_4
        :pswitch_7
        :pswitch_d
        :pswitch_10
        :pswitch_a
    .end packed-switch
.end method

.method public setStagger(IJ)V
    .registers 4
    .parameter "transitionType"
    .parameter "duration"

    #@0
    .prologue
    .line 528
    packed-switch p1, :pswitch_data_e

    #@3
    .line 540
    :goto_3
    :pswitch_3
    return-void

    #@4
    .line 530
    :pswitch_4
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingAppearingStagger:J

    #@6
    goto :goto_3

    #@7
    .line 533
    :pswitch_7
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingStagger:J

    #@9
    goto :goto_3

    #@a
    .line 536
    :pswitch_a
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingStagger:J

    #@c
    goto :goto_3

    #@d
    .line 528
    nop

    #@e
    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_4
        :pswitch_7
        :pswitch_3
        :pswitch_3
        :pswitch_a
    .end packed-switch
.end method

.method public setStartDelay(IJ)V
    .registers 4
    .parameter "transitionType"
    .parameter "delay"

    #@0
    .prologue
    .line 412
    packed-switch p1, :pswitch_data_14

    #@3
    .line 429
    :goto_3
    return-void

    #@4
    .line 414
    :pswitch_4
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingAppearingDelay:J

    #@6
    goto :goto_3

    #@7
    .line 417
    :pswitch_7
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingDisappearingDelay:J

    #@9
    goto :goto_3

    #@a
    .line 420
    :pswitch_a
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mChangingDelay:J

    #@c
    goto :goto_3

    #@d
    .line 423
    :pswitch_d
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mAppearingDelay:J

    #@f
    goto :goto_3

    #@10
    .line 426
    :pswitch_10
    iput-wide p2, p0, Landroid/animation/LayoutTransition;->mDisappearingDelay:J

    #@12
    goto :goto_3

    #@13
    .line 412
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_4
        :pswitch_7
        :pswitch_d
        :pswitch_10
        :pswitch_a
    .end packed-switch
.end method

.method public showChild(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1305
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/animation/LayoutTransition;->addChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    #@4
    .line 1306
    return-void
.end method

.method public showChild(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .registers 5
    .parameter "parent"
    .parameter "child"
    .parameter "oldVisibility"

    #@0
    .prologue
    .line 1320
    const/16 v0, 0x8

    #@2
    if-ne p3, v0, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-direct {p0, p1, p2, v0}, Landroid/animation/LayoutTransition;->addChild(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    #@8
    .line 1321
    return-void

    #@9
    .line 1320
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public startChangingAnimations()V
    .registers 7

    #@0
    .prologue
    .line 995
    iget-object v3, p0, Landroid/animation/LayoutTransition;->currentChangingAnimations:Ljava/util/LinkedHashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clone()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Ljava/util/LinkedHashMap;

    #@8
    .line 997
    .local v1, currentAnimCopy:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Landroid/view/View;Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@b
    move-result-object v3

    #@c
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v2

    #@10
    .local v2, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_2c

    #@16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/animation/Animator;

    #@1c
    .line 998
    .local v0, anim:Landroid/animation/Animator;
    instance-of v3, v0, Landroid/animation/ObjectAnimator;

    #@1e
    if-eqz v3, :cond_28

    #@20
    move-object v3, v0

    #@21
    .line 999
    check-cast v3, Landroid/animation/ObjectAnimator;

    #@23
    const-wide/16 v4, 0x0

    #@25
    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setCurrentPlayTime(J)V

    #@28
    .line 1001
    :cond_28
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@2b
    goto :goto_10

    #@2c
    .line 1003
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_2c
    return-void
.end method
