.class public Landroid/animation/ArgbEvaluator;
.super Ljava/lang/Object;
.source "ArgbEvaluator.java"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 16
    .parameter "fraction"
    .parameter "startValue"
    .parameter "endValue"

    #@0
    .prologue
    .line 42
    check-cast p2, Ljava/lang/Integer;

    #@2
    .end local p2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    #@5
    move-result v8

    #@6
    .line 43
    .local v8, startInt:I
    shr-int/lit8 v10, v8, 0x18

    #@8
    and-int/lit16 v5, v10, 0xff

    #@a
    .line 44
    .local v5, startA:I
    shr-int/lit8 v10, v8, 0x10

    #@c
    and-int/lit16 v9, v10, 0xff

    #@e
    .line 45
    .local v9, startR:I
    shr-int/lit8 v10, v8, 0x8

    #@10
    and-int/lit16 v7, v10, 0xff

    #@12
    .line 46
    .local v7, startG:I
    and-int/lit16 v6, v8, 0xff

    #@14
    .line 48
    .local v6, startB:I
    check-cast p3, Ljava/lang/Integer;

    #@16
    .end local p3
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    #@19
    move-result v3

    #@1a
    .line 49
    .local v3, endInt:I
    shr-int/lit8 v10, v3, 0x18

    #@1c
    and-int/lit16 v0, v10, 0xff

    #@1e
    .line 50
    .local v0, endA:I
    shr-int/lit8 v10, v3, 0x10

    #@20
    and-int/lit16 v4, v10, 0xff

    #@22
    .line 51
    .local v4, endR:I
    shr-int/lit8 v10, v3, 0x8

    #@24
    and-int/lit16 v2, v10, 0xff

    #@26
    .line 52
    .local v2, endG:I
    and-int/lit16 v1, v3, 0xff

    #@28
    .line 54
    .local v1, endB:I
    sub-int v10, v0, v5

    #@2a
    int-to-float v10, v10

    #@2b
    mul-float/2addr v10, p1

    #@2c
    float-to-int v10, v10

    #@2d
    add-int/2addr v10, v5

    #@2e
    shl-int/lit8 v10, v10, 0x18

    #@30
    sub-int v11, v4, v9

    #@32
    int-to-float v11, v11

    #@33
    mul-float/2addr v11, p1

    #@34
    float-to-int v11, v11

    #@35
    add-int/2addr v11, v9

    #@36
    shl-int/lit8 v11, v11, 0x10

    #@38
    or-int/2addr v10, v11

    #@39
    sub-int v11, v2, v7

    #@3b
    int-to-float v11, v11

    #@3c
    mul-float/2addr v11, p1

    #@3d
    float-to-int v11, v11

    #@3e
    add-int/2addr v11, v7

    #@3f
    shl-int/lit8 v11, v11, 0x8

    #@41
    or-int/2addr v10, v11

    #@42
    sub-int v11, v1, v6

    #@44
    int-to-float v11, v11

    #@45
    mul-float/2addr v11, p1

    #@46
    float-to-int v11, v11

    #@47
    add-int/2addr v11, v6

    #@48
    or-int/2addr v10, v11

    #@49
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v10

    #@4d
    return-object v10
.end method
