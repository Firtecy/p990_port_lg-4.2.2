.class Landroid/animation/Keyframe$IntKeyframe;
.super Landroid/animation/Keyframe;
.source "Keyframe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/Keyframe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IntKeyframe"
.end annotation


# instance fields
.field mValue:I


# direct methods
.method constructor <init>(F)V
    .registers 3
    .parameter "fraction"

    #@0
    .prologue
    .line 287
    invoke-direct {p0}, Landroid/animation/Keyframe;-><init>()V

    #@3
    .line 288
    iput p1, p0, Landroid/animation/Keyframe;->mFraction:F

    #@5
    .line 289
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@7
    iput-object v0, p0, Landroid/animation/Keyframe;->mValueType:Ljava/lang/Class;

    #@9
    .line 290
    return-void
.end method

.method constructor <init>(FI)V
    .registers 4
    .parameter "fraction"
    .parameter "value"

    #@0
    .prologue
    .line 280
    invoke-direct {p0}, Landroid/animation/Keyframe;-><init>()V

    #@3
    .line 281
    iput p1, p0, Landroid/animation/Keyframe;->mFraction:F

    #@5
    .line 282
    iput p2, p0, Landroid/animation/Keyframe$IntKeyframe;->mValue:I

    #@7
    .line 283
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@9
    iput-object v0, p0, Landroid/animation/Keyframe;->mValueType:Ljava/lang/Class;

    #@b
    .line 284
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@e
    .line 285
    return-void
.end method


# virtual methods
.method public clone()Landroid/animation/Keyframe$IntKeyframe;
    .registers 4

    #@0
    .prologue
    .line 309
    iget-boolean v1, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@2
    if-eqz v1, :cond_17

    #@4
    new-instance v0, Landroid/animation/Keyframe$IntKeyframe;

    #@6
    invoke-virtual {p0}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@9
    move-result v1

    #@a
    iget v2, p0, Landroid/animation/Keyframe$IntKeyframe;->mValue:I

    #@c
    invoke-direct {v0, v1, v2}, Landroid/animation/Keyframe$IntKeyframe;-><init>(FI)V

    #@f
    .line 312
    .local v0, kfClone:Landroid/animation/Keyframe$IntKeyframe;
    :goto_f
    invoke-virtual {p0}, Landroid/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Landroid/animation/Keyframe$IntKeyframe;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@16
    .line 313
    return-object v0

    #@17
    .line 309
    .end local v0           #kfClone:Landroid/animation/Keyframe$IntKeyframe;
    :cond_17
    new-instance v0, Landroid/animation/Keyframe$IntKeyframe;

    #@19
    invoke-virtual {p0}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@1c
    move-result v1

    #@1d
    invoke-direct {v0, v1}, Landroid/animation/Keyframe$IntKeyframe;-><init>(F)V

    #@20
    goto :goto_f
.end method

.method public bridge synthetic clone()Landroid/animation/Keyframe;
    .registers 2

    #@0
    .prologue
    .line 273
    invoke-virtual {p0}, Landroid/animation/Keyframe$IntKeyframe;->clone()Landroid/animation/Keyframe$IntKeyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 273
    invoke-virtual {p0}, Landroid/animation/Keyframe$IntKeyframe;->clone()Landroid/animation/Keyframe$IntKeyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getIntValue()I
    .registers 2

    #@0
    .prologue
    .line 293
    iget v0, p0, Landroid/animation/Keyframe$IntKeyframe;->mValue:I

    #@2
    return v0
.end method

.method public getValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 297
    iget v0, p0, Landroid/animation/Keyframe$IntKeyframe;->mValue:I

    #@2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 301
    if-eqz p1, :cond_15

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v0

    #@6
    const-class v1, Ljava/lang/Integer;

    #@8
    if-ne v0, v1, :cond_15

    #@a
    .line 302
    check-cast p1, Ljava/lang/Integer;

    #@c
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/animation/Keyframe$IntKeyframe;->mValue:I

    #@12
    .line 303
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@15
    .line 305
    :cond_15
    return-void
.end method
