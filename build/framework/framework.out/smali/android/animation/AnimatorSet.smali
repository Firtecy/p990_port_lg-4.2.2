.class public final Landroid/animation/AnimatorSet;
.super Landroid/animation/Animator;
.source "AnimatorSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/AnimatorSet$Builder;,
        Landroid/animation/AnimatorSet$Node;,
        Landroid/animation/AnimatorSet$Dependency;,
        Landroid/animation/AnimatorSet$AnimatorSetListener;,
        Landroid/animation/AnimatorSet$DependencyListener;
    }
.end annotation


# instance fields
.field private mDelayAnim:Landroid/animation/ValueAnimator;

.field private mDuration:J

.field private mNeedsSort:Z

.field private mNodeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/animation/Animator;",
            "Landroid/animation/AnimatorSet$Node;",
            ">;"
        }
    .end annotation
.end field

.field private mNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/AnimatorSet$Node;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayingSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

.field private mSortedNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/AnimatorSet$Node;",
            ">;"
        }
    .end annotation
.end field

.field private mStartDelay:J

.field private mStarted:Z

.field mTerminated:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 50
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    #@5
    .line 63
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/animation/AnimatorSet;->mPlayingSet:Ljava/util/ArrayList;

    #@c
    .line 71
    new-instance v0, Ljava/util/HashMap;

    #@e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@11
    iput-object v0, p0, Landroid/animation/AnimatorSet;->mNodeMap:Ljava/util/HashMap;

    #@13
    .line 78
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@1a
    .line 85
    new-instance v0, Ljava/util/ArrayList;

    #@1c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@21
    .line 92
    const/4 v0, 0x1

    #@22
    iput-boolean v0, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@24
    .line 94
    iput-object v2, p0, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@26
    .line 103
    iput-boolean v1, p0, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@28
    .line 109
    iput-boolean v1, p0, Landroid/animation/AnimatorSet;->mStarted:Z

    #@2a
    .line 112
    const-wide/16 v0, 0x0

    #@2c
    iput-wide v0, p0, Landroid/animation/AnimatorSet;->mStartDelay:J

    #@2e
    .line 115
    iput-object v2, p0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@30
    .line 121
    const-wide/16 v0, -0x1

    #@32
    iput-wide v0, p0, Landroid/animation/AnimatorSet;->mDuration:J

    #@34
    .line 1021
    return-void
.end method

.method static synthetic access$000(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/animation/AnimatorSet;->mPlayingSet:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/animation/AnimatorSet;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/animation/AnimatorSet;->mNodeMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Landroid/animation/AnimatorSet;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Landroid/animation/AnimatorSet;->mStarted:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method private sortNodes()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 794
    iget-boolean v11, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@3
    if-eqz v11, :cond_9c

    #@5
    .line 795
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 796
    new-instance v9, Ljava/util/ArrayList;

    #@c
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 797
    .local v9, roots:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v6

    #@15
    .line 798
    .local v6, numNodes:I
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    if-ge v1, v6, :cond_32

    #@18
    .line 799
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Landroid/animation/AnimatorSet$Node;

    #@20
    .line 800
    .local v3, node:Landroid/animation/AnimatorSet$Node;
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@22
    if-eqz v11, :cond_2c

    #@24
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v11

    #@2a
    if-nez v11, :cond_2f

    #@2c
    .line 801
    :cond_2c
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 798
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_16

    #@32
    .line 804
    .end local v3           #node:Landroid/animation/AnimatorSet$Node;
    :cond_32
    new-instance v10, Ljava/util/ArrayList;

    #@34
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@37
    .line 805
    .local v10, tmpRoots:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    :goto_37
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v11

    #@3b
    if-lez v11, :cond_84

    #@3d
    .line 806
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@40
    move-result v7

    #@41
    .line 807
    .local v7, numRoots:I
    const/4 v1, 0x0

    #@42
    :goto_42
    if-ge v1, v7, :cond_7a

    #@44
    .line 808
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@47
    move-result-object v8

    #@48
    check-cast v8, Landroid/animation/AnimatorSet$Node;

    #@4a
    .line 809
    .local v8, root:Landroid/animation/AnimatorSet$Node;
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@4c
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4f
    .line 810
    iget-object v11, v8, Landroid/animation/AnimatorSet$Node;->nodeDependents:Ljava/util/ArrayList;

    #@51
    if-eqz v11, :cond_77

    #@53
    .line 811
    iget-object v11, v8, Landroid/animation/AnimatorSet$Node;->nodeDependents:Ljava/util/ArrayList;

    #@55
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@58
    move-result v5

    #@59
    .line 812
    .local v5, numDependents:I
    const/4 v2, 0x0

    #@5a
    .local v2, j:I
    :goto_5a
    if-ge v2, v5, :cond_77

    #@5c
    .line 813
    iget-object v11, v8, Landroid/animation/AnimatorSet$Node;->nodeDependents:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@61
    move-result-object v3

    #@62
    check-cast v3, Landroid/animation/AnimatorSet$Node;

    #@64
    .line 814
    .restart local v3       #node:Landroid/animation/AnimatorSet$Node;
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@66
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@69
    .line 815
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@6b
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@6e
    move-result v11

    #@6f
    if-nez v11, :cond_74

    #@71
    .line 816
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@74
    .line 812
    :cond_74
    add-int/lit8 v2, v2, 0x1

    #@76
    goto :goto_5a

    #@77
    .line 807
    .end local v2           #j:I
    .end local v3           #node:Landroid/animation/AnimatorSet$Node;
    .end local v5           #numDependents:I
    :cond_77
    add-int/lit8 v1, v1, 0x1

    #@79
    goto :goto_42

    #@7a
    .line 821
    .end local v8           #root:Landroid/animation/AnimatorSet$Node;
    :cond_7a
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    #@7d
    .line 822
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@80
    .line 823
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    #@83
    goto :goto_37

    #@84
    .line 825
    .end local v7           #numRoots:I
    :cond_84
    iput-boolean v13, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@86
    .line 826
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@88
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@8b
    move-result v11

    #@8c
    iget-object v12, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@8e
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@91
    move-result v12

    #@92
    if-eq v11, v12, :cond_ee

    #@94
    .line 827
    new-instance v11, Ljava/lang/IllegalStateException;

    #@96
    const-string v12, "Circular dependencies cannot exist in AnimatorSet"

    #@98
    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@9b
    throw v11

    #@9c
    .line 834
    .end local v1           #i:I
    .end local v6           #numNodes:I
    .end local v9           #roots:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    .end local v10           #tmpRoots:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    :cond_9c
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@9e
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@a1
    move-result v6

    #@a2
    .line 835
    .restart local v6       #numNodes:I
    const/4 v1, 0x0

    #@a3
    .restart local v1       #i:I
    :goto_a3
    if-ge v1, v6, :cond_ee

    #@a5
    .line 836
    iget-object v11, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@a7
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@aa
    move-result-object v3

    #@ab
    check-cast v3, Landroid/animation/AnimatorSet$Node;

    #@ad
    .line 837
    .restart local v3       #node:Landroid/animation/AnimatorSet$Node;
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@af
    if-eqz v11, :cond_e9

    #@b1
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@b3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@b6
    move-result v11

    #@b7
    if-lez v11, :cond_e9

    #@b9
    .line 838
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@bb
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@be
    move-result v4

    #@bf
    .line 839
    .local v4, numDependencies:I
    const/4 v2, 0x0

    #@c0
    .restart local v2       #j:I
    :goto_c0
    if-ge v2, v4, :cond_e9

    #@c2
    .line 840
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@c4
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c7
    move-result-object v0

    #@c8
    check-cast v0, Landroid/animation/AnimatorSet$Dependency;

    #@ca
    .line 841
    .local v0, dependency:Landroid/animation/AnimatorSet$Dependency;
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@cc
    if-nez v11, :cond_d5

    #@ce
    .line 842
    new-instance v11, Ljava/util/ArrayList;

    #@d0
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@d3
    iput-object v11, v3, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@d5
    .line 844
    :cond_d5
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@d7
    iget-object v12, v0, Landroid/animation/AnimatorSet$Dependency;->node:Landroid/animation/AnimatorSet$Node;

    #@d9
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@dc
    move-result v11

    #@dd
    if-nez v11, :cond_e6

    #@df
    .line 845
    iget-object v11, v3, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@e1
    iget-object v12, v0, Landroid/animation/AnimatorSet$Dependency;->node:Landroid/animation/AnimatorSet$Node;

    #@e3
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e6
    .line 839
    :cond_e6
    add-int/lit8 v2, v2, 0x1

    #@e8
    goto :goto_c0

    #@e9
    .line 851
    .end local v0           #dependency:Landroid/animation/AnimatorSet$Dependency;
    .end local v2           #j:I
    .end local v4           #numDependencies:I
    :cond_e9
    iput-boolean v13, v3, Landroid/animation/AnimatorSet$Node;->done:Z

    #@eb
    .line 835
    add-int/lit8 v1, v1, 0x1

    #@ed
    goto :goto_a3

    #@ee
    .line 854
    .end local v3           #node:Landroid/animation/AnimatorSet$Node;
    :cond_ee
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 6

    #@0
    .prologue
    .line 289
    const/4 v4, 0x1

    #@1
    iput-boolean v4, p0, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@3
    .line 290
    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->isStarted()Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_74

    #@9
    .line 291
    const/4 v3, 0x0

    #@a
    .line 292
    .local v3, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@c
    if-eqz v4, :cond_2a

    #@e
    .line 293
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    .end local v3           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    check-cast v3, Ljava/util/ArrayList;

    #@16
    .line 294
    .restart local v3       #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v0

    #@1a
    .local v0, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_2a

    #@20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    #@26
    .line 295
    .local v1, listener:Landroid/animation/Animator$AnimatorListener;
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    #@29
    goto :goto_1a

    #@2a
    .line 298
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/animation/Animator$AnimatorListener;
    :cond_2a
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@2c
    if-eqz v4, :cond_51

    #@2e
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@30
    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->isRunning()Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_51

    #@36
    .line 301
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@38
    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    #@3b
    .line 307
    :cond_3b
    if-eqz v3, :cond_71

    #@3d
    .line 308
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@40
    move-result-object v0

    #@41
    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_41
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@44
    move-result v4

    #@45
    if-eqz v4, :cond_71

    #@47
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4a
    move-result-object v1

    #@4b
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    #@4d
    .line 309
    .restart local v1       #listener:Landroid/animation/Animator$AnimatorListener;
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@50
    goto :goto_41

    #@51
    .line 302
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/animation/Animator$AnimatorListener;
    :cond_51
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@53
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@56
    move-result v4

    #@57
    if-lez v4, :cond_3b

    #@59
    .line 303
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@5b
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5e
    move-result-object v0

    #@5f
    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_5f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@62
    move-result v4

    #@63
    if-eqz v4, :cond_3b

    #@65
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@68
    move-result-object v2

    #@69
    check-cast v2, Landroid/animation/AnimatorSet$Node;

    #@6b
    .line 304
    .local v2, node:Landroid/animation/AnimatorSet$Node;
    iget-object v4, v2, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@6d
    invoke-virtual {v4}, Landroid/animation/Animator;->cancel()V

    #@70
    goto :goto_5f

    #@71
    .line 312
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #node:Landroid/animation/AnimatorSet$Node;
    :cond_71
    const/4 v4, 0x0

    #@72
    iput-boolean v4, p0, Landroid/animation/AnimatorSet;->mStarted:Z

    #@74
    .line 314
    .end local v3           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_74
    return-void
.end method

.method public bridge synthetic clone()Landroid/animation/Animator;
    .registers 2

    #@0
    .prologue
    .line 50
    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->clone()Landroid/animation/AnimatorSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public clone()Landroid/animation/AnimatorSet;
    .registers 16

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v14, 0x0

    #@2
    .line 560
    invoke-super {p0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/animation/AnimatorSet;

    #@8
    .line 569
    .local v0, anim:Landroid/animation/AnimatorSet;
    const/4 v12, 0x1

    #@9
    iput-boolean v12, v0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@b
    .line 570
    iput-boolean v13, v0, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@d
    .line 571
    iput-boolean v13, v0, Landroid/animation/AnimatorSet;->mStarted:Z

    #@f
    .line 572
    new-instance v12, Ljava/util/ArrayList;

    #@11
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v12, v0, Landroid/animation/AnimatorSet;->mPlayingSet:Ljava/util/ArrayList;

    #@16
    .line 573
    new-instance v12, Ljava/util/HashMap;

    #@18
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    #@1b
    iput-object v12, v0, Landroid/animation/AnimatorSet;->mNodeMap:Ljava/util/HashMap;

    #@1d
    .line 574
    new-instance v12, Ljava/util/ArrayList;

    #@1f
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    #@22
    iput-object v12, v0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@24
    .line 575
    new-instance v12, Ljava/util/ArrayList;

    #@26
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    #@29
    iput-object v12, v0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@2b
    .line 580
    new-instance v11, Ljava/util/HashMap;

    #@2d
    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    #@30
    .line 581
    .local v11, nodeCloneMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/animation/AnimatorSet$Node;Landroid/animation/AnimatorSet$Node;>;"
    iget-object v12, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@35
    move-result-object v5

    #@36
    :cond_36
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@39
    move-result v12

    #@3a
    if-eqz v12, :cond_9b

    #@3c
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f
    move-result-object v9

    #@40
    check-cast v9, Landroid/animation/AnimatorSet$Node;

    #@42
    .line 582
    .local v9, node:Landroid/animation/AnimatorSet$Node;
    invoke-virtual {v9}, Landroid/animation/AnimatorSet$Node;->clone()Landroid/animation/AnimatorSet$Node;

    #@45
    move-result-object v10

    #@46
    .line 583
    .local v10, nodeClone:Landroid/animation/AnimatorSet$Node;
    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    .line 584
    iget-object v12, v0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4e
    .line 585
    iget-object v12, v0, Landroid/animation/AnimatorSet;->mNodeMap:Ljava/util/HashMap;

    #@50
    iget-object v13, v10, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@52
    invoke-virtual {v12, v13, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    .line 587
    iput-object v14, v10, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@57
    .line 588
    iput-object v14, v10, Landroid/animation/AnimatorSet$Node;->tmpDependencies:Ljava/util/ArrayList;

    #@59
    .line 589
    iput-object v14, v10, Landroid/animation/AnimatorSet$Node;->nodeDependents:Ljava/util/ArrayList;

    #@5b
    .line 590
    iput-object v14, v10, Landroid/animation/AnimatorSet$Node;->nodeDependencies:Ljava/util/ArrayList;

    #@5d
    .line 593
    iget-object v12, v10, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@5f
    invoke-virtual {v12}, Landroid/animation/Animator;->getListeners()Ljava/util/ArrayList;

    #@62
    move-result-object v2

    #@63
    .line 594
    .local v2, cloneListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    if-eqz v2, :cond_36

    #@65
    .line 595
    const/4 v8, 0x0

    #@66
    .line 596
    .local v8, listenersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@69
    move-result-object v6

    #@6a
    .local v6, i$:Ljava/util/Iterator;
    :cond_6a
    :goto_6a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@6d
    move-result v12

    #@6e
    if-eqz v12, :cond_85

    #@70
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@73
    move-result-object v7

    #@74
    check-cast v7, Landroid/animation/Animator$AnimatorListener;

    #@76
    .line 597
    .local v7, listener:Landroid/animation/Animator$AnimatorListener;
    instance-of v12, v7, Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@78
    if-eqz v12, :cond_6a

    #@7a
    .line 598
    if-nez v8, :cond_81

    #@7c
    .line 599
    new-instance v8, Ljava/util/ArrayList;

    #@7e
    .end local v8           #listenersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@81
    .line 601
    .restart local v8       #listenersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_81
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@84
    goto :goto_6a

    #@85
    .line 604
    .end local v7           #listener:Landroid/animation/Animator$AnimatorListener;
    :cond_85
    if-eqz v8, :cond_36

    #@87
    .line 605
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8a
    move-result-object v6

    #@8b
    :goto_8b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@8e
    move-result v12

    #@8f
    if-eqz v12, :cond_36

    #@91
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@94
    move-result-object v7

    #@95
    check-cast v7, Landroid/animation/Animator$AnimatorListener;

    #@97
    .line 606
    .restart local v7       #listener:Landroid/animation/Animator$AnimatorListener;
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@9a
    goto :goto_8b

    #@9b
    .line 613
    .end local v2           #cloneListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #listener:Landroid/animation/Animator$AnimatorListener;
    .end local v8           #listenersToRemove:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    .end local v9           #node:Landroid/animation/AnimatorSet$Node;
    .end local v10           #nodeClone:Landroid/animation/AnimatorSet$Node;
    :cond_9b
    iget-object v12, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@9d
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a0
    move-result-object v5

    #@a1
    :cond_a1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@a4
    move-result v12

    #@a5
    if-eqz v12, :cond_dc

    #@a7
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@aa
    move-result-object v9

    #@ab
    check-cast v9, Landroid/animation/AnimatorSet$Node;

    #@ad
    .line 614
    .restart local v9       #node:Landroid/animation/AnimatorSet$Node;
    invoke-virtual {v11, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b0
    move-result-object v10

    #@b1
    check-cast v10, Landroid/animation/AnimatorSet$Node;

    #@b3
    .line 615
    .restart local v10       #nodeClone:Landroid/animation/AnimatorSet$Node;
    iget-object v12, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@b5
    if-eqz v12, :cond_a1

    #@b7
    .line 616
    iget-object v12, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@b9
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@bc
    move-result-object v6

    #@bd
    .restart local v6       #i$:Ljava/util/Iterator;
    :goto_bd
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@c0
    move-result v12

    #@c1
    if-eqz v12, :cond_a1

    #@c3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c6
    move-result-object v4

    #@c7
    check-cast v4, Landroid/animation/AnimatorSet$Dependency;

    #@c9
    .line 617
    .local v4, dependency:Landroid/animation/AnimatorSet$Dependency;
    iget-object v12, v4, Landroid/animation/AnimatorSet$Dependency;->node:Landroid/animation/AnimatorSet$Node;

    #@cb
    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ce
    move-result-object v3

    #@cf
    check-cast v3, Landroid/animation/AnimatorSet$Node;

    #@d1
    .line 618
    .local v3, clonedDependencyNode:Landroid/animation/AnimatorSet$Node;
    new-instance v1, Landroid/animation/AnimatorSet$Dependency;

    #@d3
    iget v12, v4, Landroid/animation/AnimatorSet$Dependency;->rule:I

    #@d5
    invoke-direct {v1, v3, v12}, Landroid/animation/AnimatorSet$Dependency;-><init>(Landroid/animation/AnimatorSet$Node;I)V

    #@d8
    .line 620
    .local v1, cloneDependency:Landroid/animation/AnimatorSet$Dependency;
    invoke-virtual {v10, v1}, Landroid/animation/AnimatorSet$Node;->addDependency(Landroid/animation/AnimatorSet$Dependency;)V

    #@db
    goto :goto_bd

    #@dc
    .line 625
    .end local v1           #cloneDependency:Landroid/animation/AnimatorSet$Dependency;
    .end local v3           #clonedDependencyNode:Landroid/animation/AnimatorSet$Node;
    .end local v4           #dependency:Landroid/animation/AnimatorSet$Dependency;
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v9           #node:Landroid/animation/AnimatorSet$Node;
    .end local v10           #nodeClone:Landroid/animation/AnimatorSet$Node;
    :cond_dc
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 50
    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->clone()Landroid/animation/AnimatorSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public end()V
    .registers 7

    #@0
    .prologue
    .line 324
    const/4 v4, 0x1

    #@1
    iput-boolean v4, p0, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@3
    .line 325
    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->isStarted()Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_8b

    #@9
    .line 326
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v4

    #@f
    iget-object v5, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v5

    #@15
    if-eq v4, v5, :cond_3f

    #@17
    .line 328
    invoke-direct {p0}, Landroid/animation/AnimatorSet;->sortNodes()V

    #@1a
    .line 329
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v0

    #@20
    .local v0, i$:Ljava/util/Iterator;
    :goto_20
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_3f

    #@26
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@29
    move-result-object v2

    #@2a
    check-cast v2, Landroid/animation/AnimatorSet$Node;

    #@2c
    .line 330
    .local v2, node:Landroid/animation/AnimatorSet$Node;
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@2e
    if-nez v4, :cond_37

    #@30
    .line 331
    new-instance v4, Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@32
    invoke-direct {v4, p0, p0}, Landroid/animation/AnimatorSet$AnimatorSetListener;-><init>(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet;)V

    #@35
    iput-object v4, p0, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@37
    .line 333
    :cond_37
    iget-object v4, v2, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@39
    iget-object v5, p0, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@3b
    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@3e
    goto :goto_20

    #@3f
    .line 336
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #node:Landroid/animation/AnimatorSet$Node;
    :cond_3f
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@41
    if-eqz v4, :cond_48

    #@43
    .line 337
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@45
    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    #@48
    .line 339
    :cond_48
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@4a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@4d
    move-result v4

    #@4e
    if-lez v4, :cond_68

    #@50
    .line 340
    iget-object v4, p0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@55
    move-result-object v0

    #@56
    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_56
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_68

    #@5c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f
    move-result-object v2

    #@60
    check-cast v2, Landroid/animation/AnimatorSet$Node;

    #@62
    .line 341
    .restart local v2       #node:Landroid/animation/AnimatorSet$Node;
    iget-object v4, v2, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@64
    invoke-virtual {v4}, Landroid/animation/Animator;->end()V

    #@67
    goto :goto_56

    #@68
    .line 344
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #node:Landroid/animation/AnimatorSet$Node;
    :cond_68
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@6a
    if-eqz v4, :cond_88

    #@6c
    .line 345
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@6e
    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@71
    move-result-object v3

    #@72
    check-cast v3, Ljava/util/ArrayList;

    #@74
    .line 347
    .local v3, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@77
    move-result-object v0

    #@78
    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_78
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@7b
    move-result v4

    #@7c
    if-eqz v4, :cond_88

    #@7e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@81
    move-result-object v1

    #@82
    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    #@84
    .line 348
    .local v1, listener:Landroid/animation/Animator$AnimatorListener;
    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@87
    goto :goto_78

    #@88
    .line 351
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/animation/Animator$AnimatorListener;
    .end local v3           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_88
    const/4 v4, 0x0

    #@89
    iput-boolean v4, p0, Landroid/animation/AnimatorSet;->mStarted:Z

    #@8b
    .line 353
    :cond_8b
    return-void
.end method

.method public getChildAnimations()Ljava/util/ArrayList;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 205
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 206
    .local v0, childList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    iget-object v3, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1d

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/animation/AnimatorSet$Node;

    #@17
    .line 207
    .local v2, node:Landroid/animation/AnimatorSet$Node;
    iget-object v3, v2, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@19
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    goto :goto_b

    #@1d
    .line 209
    .end local v2           #node:Landroid/animation/AnimatorSet$Node;
    :cond_1d
    return-object v0
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 407
    iget-wide v0, p0, Landroid/animation/AnimatorSet;->mDuration:J

    #@2
    return-wide v0
.end method

.method public getStartDelay()J
    .registers 3

    #@0
    .prologue
    .line 383
    iget-wide v0, p0, Landroid/animation/AnimatorSet;->mStartDelay:J

    #@2
    return-wide v0
.end method

.method public isRunning()Z
    .registers 4

    #@0
    .prologue
    .line 362
    iget-object v2, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1c

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/animation/AnimatorSet$Node;

    #@12
    .line 363
    .local v1, node:Landroid/animation/AnimatorSet$Node;
    iget-object v2, v1, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@14
    invoke-virtual {v2}, Landroid/animation/Animator;->isRunning()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_6

    #@1a
    .line 364
    const/4 v2, 0x1

    #@1b
    .line 367
    .end local v1           #node:Landroid/animation/AnimatorSet$Node;
    :goto_1b
    return v2

    #@1c
    :cond_1c
    const/4 v2, 0x0

    #@1d
    goto :goto_1b
.end method

.method public isStarted()Z
    .registers 2

    #@0
    .prologue
    .line 372
    iget-boolean v0, p0, Landroid/animation/AnimatorSet;->mStarted:Z

    #@2
    return v0
.end method

.method public play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;
    .registers 3
    .parameter "anim"

    #@0
    .prologue
    .line 273
    if-eqz p1, :cond_b

    #@2
    .line 274
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@5
    .line 275
    new-instance v0, Landroid/animation/AnimatorSet$Builder;

    #@7
    invoke-direct {v0, p0, p1}, Landroid/animation/AnimatorSet$Builder;-><init>(Landroid/animation/AnimatorSet;Landroid/animation/Animator;)V

    #@a
    .line 277
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public playSequentially(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, items:Ljava/util/List;,"Ljava/util/List<Landroid/animation/Animator;>;"
    const/4 v2, 0x1

    #@1
    .line 184
    if-eqz p1, :cond_1b

    #@3
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@6
    move-result v1

    #@7
    if-lez v1, :cond_1b

    #@9
    .line 185
    iput-boolean v2, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@b
    .line 186
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@e
    move-result v1

    #@f
    if-ne v1, v2, :cond_1c

    #@11
    .line 187
    const/4 v1, 0x0

    #@12
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/animation/Animator;

    #@18
    invoke-virtual {p0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@1b
    .line 194
    :cond_1b
    return-void

    #@1c
    .line 189
    :cond_1c
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@20
    move-result v1

    #@21
    add-int/lit8 v1, v1, -0x1

    #@23
    if-ge v0, v1, :cond_1b

    #@25
    .line 190
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Landroid/animation/Animator;

    #@2b
    invoke-virtual {p0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@2e
    move-result-object v2

    #@2f
    add-int/lit8 v1, v0, 0x1

    #@31
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Landroid/animation/Animator;

    #@37
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@3a
    .line 189
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_1d
.end method

.method public varargs playSequentially([Landroid/animation/Animator;)V
    .registers 5
    .parameter "items"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 165
    if-eqz p1, :cond_e

    #@3
    .line 166
    iput-boolean v2, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@5
    .line 167
    array-length v1, p1

    #@6
    if-ne v1, v2, :cond_f

    #@8
    .line 168
    const/4 v1, 0x0

    #@9
    aget-object v1, p1, v1

    #@b
    invoke-virtual {p0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@e
    .line 175
    :cond_e
    return-void

    #@f
    .line 170
    :cond_f
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    array-length v1, p1

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    if-ge v0, v1, :cond_e

    #@15
    .line 171
    aget-object v1, p1, v0

    #@17
    invoke-virtual {p0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@1a
    move-result-object v1

    #@1b
    add-int/lit8 v2, v0, 0x1

    #@1d
    aget-object v2, p1, v2

    #@1f
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@22
    .line 170
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_10
.end method

.method public playTogether(Ljava/util/Collection;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 145
    .local p1, items:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/animation/Animator;>;"
    if-eqz p1, :cond_27

    #@2
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    #@5
    move-result v3

    #@6
    if-lez v3, :cond_27

    #@8
    .line 146
    const/4 v3, 0x1

    #@9
    iput-boolean v3, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@b
    .line 147
    const/4 v1, 0x0

    #@c
    .line 148
    .local v1, builder:Landroid/animation/AnimatorSet$Builder;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v2

    #@10
    .local v2, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_27

    #@16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/animation/Animator;

    #@1c
    .line 149
    .local v0, anim:Landroid/animation/Animator;
    if-nez v1, :cond_23

    #@1e
    .line 150
    invoke-virtual {p0, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@21
    move-result-object v1

    #@22
    goto :goto_10

    #@23
    .line 152
    :cond_23
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@26
    goto :goto_10

    #@27
    .line 156
    .end local v0           #anim:Landroid/animation/Animator;
    .end local v1           #builder:Landroid/animation/AnimatorSet$Builder;
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_27
    return-void
.end method

.method public varargs playTogether([Landroid/animation/Animator;)V
    .registers 5
    .parameter "items"

    #@0
    .prologue
    .line 130
    if-eqz p1, :cond_18

    #@2
    .line 131
    const/4 v2, 0x1

    #@3
    iput-boolean v2, p0, Landroid/animation/AnimatorSet;->mNeedsSort:Z

    #@5
    .line 132
    const/4 v2, 0x0

    #@6
    aget-object v2, p1, v2

    #@8
    invoke-virtual {p0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@b
    move-result-object v0

    #@c
    .line 133
    .local v0, builder:Landroid/animation/AnimatorSet$Builder;
    const/4 v1, 0x1

    #@d
    .local v1, i:I
    :goto_d
    array-length v2, p1

    #@e
    if-ge v1, v2, :cond_18

    #@10
    .line 134
    aget-object v2, p1, v1

    #@12
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@15
    .line 133
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_d

    #@18
    .line 137
    .end local v0           #builder:Landroid/animation/AnimatorSet$Builder;
    .end local v1           #i:I
    :cond_18
    return-void
.end method

.method public bridge synthetic setDuration(J)Landroid/animation/Animator;
    .registers 4
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-virtual {p0, p1, p2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public setDuration(J)Landroid/animation/AnimatorSet;
    .registers 5
    .parameter "duration"

    #@0
    .prologue
    .line 420
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_e

    #@6
    .line 421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "duration must be a value of zero or greater"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 424
    :cond_e
    iput-wide p1, p0, Landroid/animation/AnimatorSet;->mDuration:J

    #@10
    .line 425
    return-object p0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .registers 5
    .parameter "interpolator"

    #@0
    .prologue
    .line 239
    iget-object v2, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_18

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/animation/AnimatorSet$Node;

    #@12
    .line 240
    .local v1, node:Landroid/animation/AnimatorSet$Node;
    iget-object v2, v1, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@14
    invoke-virtual {v2, p1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@17
    goto :goto_6

    #@18
    .line 242
    .end local v1           #node:Landroid/animation/AnimatorSet$Node;
    :cond_18
    return-void
.end method

.method public setStartDelay(J)V
    .registers 3
    .parameter "startDelay"

    #@0
    .prologue
    .line 394
    iput-wide p1, p0, Landroid/animation/AnimatorSet;->mStartDelay:J

    #@2
    .line 395
    return-void
.end method

.method public setTarget(Ljava/lang/Object;)V
    .registers 6
    .parameter "target"

    #@0
    .prologue
    .line 221
    iget-object v3, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_28

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/animation/AnimatorSet$Node;

    #@12
    .line 222
    .local v2, node:Landroid/animation/AnimatorSet$Node;
    iget-object v0, v2, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@14
    .line 223
    .local v0, animation:Landroid/animation/Animator;
    instance-of v3, v0, Landroid/animation/AnimatorSet;

    #@16
    if-eqz v3, :cond_1e

    #@18
    .line 224
    check-cast v0, Landroid/animation/AnimatorSet;

    #@1a
    .end local v0           #animation:Landroid/animation/Animator;
    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    #@1d
    goto :goto_6

    #@1e
    .line 225
    .restart local v0       #animation:Landroid/animation/Animator;
    :cond_1e
    instance-of v3, v0, Landroid/animation/ObjectAnimator;

    #@20
    if-eqz v3, :cond_6

    #@22
    .line 226
    check-cast v0, Landroid/animation/ObjectAnimator;

    #@24
    .end local v0           #animation:Landroid/animation/Animator;
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    #@27
    goto :goto_6

    #@28
    .line 229
    .end local v2           #node:Landroid/animation/AnimatorSet$Node;
    :cond_28
    return-void
.end method

.method public setupEndValues()V
    .registers 4

    #@0
    .prologue
    .line 437
    iget-object v2, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_18

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/animation/AnimatorSet$Node;

    #@12
    .line 438
    .local v1, node:Landroid/animation/AnimatorSet$Node;
    iget-object v2, v1, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@14
    invoke-virtual {v2}, Landroid/animation/Animator;->setupEndValues()V

    #@17
    goto :goto_6

    #@18
    .line 440
    .end local v1           #node:Landroid/animation/AnimatorSet$Node;
    :cond_18
    return-void
.end method

.method public setupStartValues()V
    .registers 4

    #@0
    .prologue
    .line 430
    iget-object v2, p0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_18

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/animation/AnimatorSet$Node;

    #@12
    .line 431
    .local v1, node:Landroid/animation/AnimatorSet$Node;
    iget-object v2, v1, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@14
    invoke-virtual {v2}, Landroid/animation/Animator;->setupStartValues()V

    #@17
    goto :goto_6

    #@18
    .line 433
    .end local v1           #node:Landroid/animation/AnimatorSet$Node;
    :cond_18
    return-void
.end method

.method public start()V
    .registers 21

    #@0
    .prologue
    .line 452
    const/16 v16, 0x0

    #@2
    move/from16 v0, v16

    #@4
    move-object/from16 v1, p0

    #@6
    iput-boolean v0, v1, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@8
    .line 453
    const/16 v16, 0x1

    #@a
    move/from16 v0, v16

    #@c
    move-object/from16 v1, p0

    #@e
    iput-boolean v0, v1, Landroid/animation/AnimatorSet;->mStarted:Z

    #@10
    .line 455
    move-object/from16 v0, p0

    #@12
    iget-wide v0, v0, Landroid/animation/AnimatorSet;->mDuration:J

    #@14
    move-wide/from16 v16, v0

    #@16
    const-wide/16 v18, 0x0

    #@18
    cmp-long v16, v16, v18

    #@1a
    if-ltz v16, :cond_40

    #@1c
    .line 457
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@20
    move-object/from16 v16, v0

    #@22
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v6

    #@26
    .local v6, i$:Ljava/util/Iterator;
    :goto_26
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v16

    #@2a
    if-eqz v16, :cond_40

    #@2c
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v9

    #@30
    check-cast v9, Landroid/animation/AnimatorSet$Node;

    #@32
    .line 460
    .local v9, node:Landroid/animation/AnimatorSet$Node;
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@34
    move-object/from16 v16, v0

    #@36
    move-object/from16 v0, p0

    #@38
    iget-wide v0, v0, Landroid/animation/AnimatorSet;->mDuration:J

    #@3a
    move-wide/from16 v17, v0

    #@3c
    invoke-virtual/range {v16 .. v18}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@3f
    goto :goto_26

    #@40
    .line 465
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v9           #node:Landroid/animation/AnimatorSet$Node;
    :cond_40
    invoke-direct/range {p0 .. p0}, Landroid/animation/AnimatorSet;->sortNodes()V

    #@43
    .line 467
    move-object/from16 v0, p0

    #@45
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@47
    move-object/from16 v16, v0

    #@49
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@4c
    move-result v13

    #@4d
    .line 468
    .local v13, numSortedNodes:I
    const/4 v5, 0x0

    #@4e
    .local v5, i:I
    :goto_4e
    if-ge v5, v13, :cond_9c

    #@50
    .line 469
    move-object/from16 v0, p0

    #@52
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@54
    move-object/from16 v16, v0

    #@56
    move-object/from16 v0, v16

    #@58
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5b
    move-result-object v9

    #@5c
    check-cast v9, Landroid/animation/AnimatorSet$Node;

    #@5e
    .line 471
    .restart local v9       #node:Landroid/animation/AnimatorSet$Node;
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@60
    move-object/from16 v16, v0

    #@62
    invoke-virtual/range {v16 .. v16}, Landroid/animation/Animator;->getListeners()Ljava/util/ArrayList;

    #@65
    move-result-object v14

    #@66
    .line 472
    .local v14, oldListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    if-eqz v14, :cond_99

    #@68
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    #@6b
    move-result v16

    #@6c
    if-lez v16, :cond_99

    #@6e
    .line 473
    new-instance v3, Ljava/util/ArrayList;

    #@70
    invoke-direct {v3, v14}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@73
    .line 476
    .local v3, clonedListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@76
    move-result-object v6

    #@77
    .restart local v6       #i$:Ljava/util/Iterator;
    :cond_77
    :goto_77
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@7a
    move-result v16

    #@7b
    if-eqz v16, :cond_99

    #@7d
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@80
    move-result-object v8

    #@81
    check-cast v8, Landroid/animation/Animator$AnimatorListener;

    #@83
    .line 477
    .local v8, listener:Landroid/animation/Animator$AnimatorListener;
    instance-of v0, v8, Landroid/animation/AnimatorSet$DependencyListener;

    #@85
    move/from16 v16, v0

    #@87
    if-nez v16, :cond_8f

    #@89
    instance-of v0, v8, Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@8b
    move/from16 v16, v0

    #@8d
    if-eqz v16, :cond_77

    #@8f
    .line 479
    :cond_8f
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@91
    move-object/from16 v16, v0

    #@93
    move-object/from16 v0, v16

    #@95
    invoke-virtual {v0, v8}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    #@98
    goto :goto_77

    #@99
    .line 468
    .end local v3           #clonedListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v8           #listener:Landroid/animation/Animator$AnimatorListener;
    :cond_99
    add-int/lit8 v5, v5, 0x1

    #@9b
    goto :goto_4e

    #@9c
    .line 489
    .end local v9           #node:Landroid/animation/AnimatorSet$Node;
    .end local v14           #oldListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_9c
    new-instance v10, Ljava/util/ArrayList;

    #@9e
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@a1
    .line 490
    .local v10, nodesToStart:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    const/4 v5, 0x0

    #@a2
    :goto_a2
    if-ge v5, v13, :cond_133

    #@a4
    .line 491
    move-object/from16 v0, p0

    #@a6
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mSortedNodes:Ljava/util/ArrayList;

    #@a8
    move-object/from16 v16, v0

    #@aa
    move-object/from16 v0, v16

    #@ac
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@af
    move-result-object v9

    #@b0
    check-cast v9, Landroid/animation/AnimatorSet$Node;

    #@b2
    .line 492
    .restart local v9       #node:Landroid/animation/AnimatorSet$Node;
    move-object/from16 v0, p0

    #@b4
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@b6
    move-object/from16 v16, v0

    #@b8
    if-nez v16, :cond_cb

    #@ba
    .line 493
    new-instance v16, Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@bc
    move-object/from16 v0, v16

    #@be
    move-object/from16 v1, p0

    #@c0
    move-object/from16 v2, p0

    #@c2
    invoke-direct {v0, v1, v2}, Landroid/animation/AnimatorSet$AnimatorSetListener;-><init>(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet;)V

    #@c5
    move-object/from16 v0, v16

    #@c7
    move-object/from16 v1, p0

    #@c9
    iput-object v0, v1, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@cb
    .line 495
    :cond_cb
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@cd
    move-object/from16 v16, v0

    #@cf
    if-eqz v16, :cond_db

    #@d1
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@d3
    move-object/from16 v16, v0

    #@d5
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@d8
    move-result v16

    #@d9
    if-nez v16, :cond_ee

    #@db
    .line 496
    :cond_db
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@de
    .line 506
    :goto_de
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@e0
    move-object/from16 v16, v0

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mSetListener:Landroid/animation/AnimatorSet$AnimatorSetListener;

    #@e6
    move-object/from16 v17, v0

    #@e8
    invoke-virtual/range {v16 .. v17}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@eb
    .line 490
    add-int/lit8 v5, v5, 0x1

    #@ed
    goto :goto_a2

    #@ee
    .line 498
    :cond_ee
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@f0
    move-object/from16 v16, v0

    #@f2
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@f5
    move-result v11

    #@f6
    .line 499
    .local v11, numDependencies:I
    const/4 v7, 0x0

    #@f7
    .local v7, j:I
    :goto_f7
    if-ge v7, v11, :cond_124

    #@f9
    .line 500
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@fb
    move-object/from16 v16, v0

    #@fd
    move-object/from16 v0, v16

    #@ff
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@102
    move-result-object v4

    #@103
    check-cast v4, Landroid/animation/AnimatorSet$Dependency;

    #@105
    .line 501
    .local v4, dependency:Landroid/animation/AnimatorSet$Dependency;
    iget-object v0, v4, Landroid/animation/AnimatorSet$Dependency;->node:Landroid/animation/AnimatorSet$Node;

    #@107
    move-object/from16 v16, v0

    #@109
    move-object/from16 v0, v16

    #@10b
    iget-object v0, v0, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@10d
    move-object/from16 v16, v0

    #@10f
    new-instance v17, Landroid/animation/AnimatorSet$DependencyListener;

    #@111
    iget v0, v4, Landroid/animation/AnimatorSet$Dependency;->rule:I

    #@113
    move/from16 v18, v0

    #@115
    move-object/from16 v0, v17

    #@117
    move-object/from16 v1, p0

    #@119
    move/from16 v2, v18

    #@11b
    invoke-direct {v0, v1, v9, v2}, Landroid/animation/AnimatorSet$DependencyListener;-><init>(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet$Node;I)V

    #@11e
    invoke-virtual/range {v16 .. v17}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@121
    .line 499
    add-int/lit8 v7, v7, 0x1

    #@123
    goto :goto_f7

    #@124
    .line 504
    .end local v4           #dependency:Landroid/animation/AnimatorSet$Dependency;
    :cond_124
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->dependencies:Ljava/util/ArrayList;

    #@126
    move-object/from16 v16, v0

    #@128
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@12b
    move-result-object v16

    #@12c
    check-cast v16, Ljava/util/ArrayList;

    #@12e
    move-object/from16 v0, v16

    #@130
    iput-object v0, v9, Landroid/animation/AnimatorSet$Node;->tmpDependencies:Ljava/util/ArrayList;

    #@132
    goto :goto_de

    #@133
    .line 509
    .end local v7           #j:I
    .end local v9           #node:Landroid/animation/AnimatorSet$Node;
    .end local v11           #numDependencies:I
    :cond_133
    move-object/from16 v0, p0

    #@135
    iget-wide v0, v0, Landroid/animation/AnimatorSet;->mStartDelay:J

    #@137
    move-wide/from16 v16, v0

    #@139
    const-wide/16 v18, 0x0

    #@13b
    cmp-long v16, v16, v18

    #@13d
    if-gtz v16, :cond_164

    #@13f
    .line 510
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@142
    move-result-object v6

    #@143
    .restart local v6       #i$:Ljava/util/Iterator;
    :goto_143
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@146
    move-result v16

    #@147
    if-eqz v16, :cond_1a3

    #@149
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14c
    move-result-object v9

    #@14d
    check-cast v9, Landroid/animation/AnimatorSet$Node;

    #@14f
    .line 511
    .restart local v9       #node:Landroid/animation/AnimatorSet$Node;
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@151
    move-object/from16 v16, v0

    #@153
    invoke-virtual/range {v16 .. v16}, Landroid/animation/Animator;->start()V

    #@156
    .line 512
    move-object/from16 v0, p0

    #@158
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mPlayingSet:Ljava/util/ArrayList;

    #@15a
    move-object/from16 v16, v0

    #@15c
    iget-object v0, v9, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@15e
    move-object/from16 v17, v0

    #@160
    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@163
    goto :goto_143

    #@164
    .line 515
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v9           #node:Landroid/animation/AnimatorSet$Node;
    :cond_164
    const/16 v16, 0x2

    #@166
    move/from16 v0, v16

    #@168
    new-array v0, v0, [F

    #@16a
    move-object/from16 v16, v0

    #@16c
    fill-array-data v16, :array_21a

    #@16f
    invoke-static/range {v16 .. v16}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    #@172
    move-result-object v16

    #@173
    move-object/from16 v0, v16

    #@175
    move-object/from16 v1, p0

    #@177
    iput-object v0, v1, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@179
    .line 516
    move-object/from16 v0, p0

    #@17b
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@17d
    move-object/from16 v16, v0

    #@17f
    move-object/from16 v0, p0

    #@181
    iget-wide v0, v0, Landroid/animation/AnimatorSet;->mStartDelay:J

    #@183
    move-wide/from16 v17, v0

    #@185
    invoke-virtual/range {v16 .. v18}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@188
    .line 517
    move-object/from16 v0, p0

    #@18a
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@18c
    move-object/from16 v16, v0

    #@18e
    new-instance v17, Landroid/animation/AnimatorSet$1;

    #@190
    move-object/from16 v0, v17

    #@192
    move-object/from16 v1, p0

    #@194
    invoke-direct {v0, v1, v10}, Landroid/animation/AnimatorSet$1;-><init>(Landroid/animation/AnimatorSet;Ljava/util/ArrayList;)V

    #@197
    invoke-virtual/range {v16 .. v17}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@19a
    .line 533
    move-object/from16 v0, p0

    #@19c
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mDelayAnim:Landroid/animation/ValueAnimator;

    #@19e
    move-object/from16 v16, v0

    #@1a0
    invoke-virtual/range {v16 .. v16}, Landroid/animation/ValueAnimator;->start()V

    #@1a3
    .line 535
    :cond_1a3
    move-object/from16 v0, p0

    #@1a5
    iget-object v0, v0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@1a7
    move-object/from16 v16, v0

    #@1a9
    if-eqz v16, :cond_1ce

    #@1ab
    .line 536
    move-object/from16 v0, p0

    #@1ad
    iget-object v0, v0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@1af
    move-object/from16 v16, v0

    #@1b1
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@1b4
    move-result-object v15

    #@1b5
    check-cast v15, Ljava/util/ArrayList;

    #@1b7
    .line 538
    .local v15, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@1ba
    move-result v12

    #@1bb
    .line 539
    .local v12, numListeners:I
    const/4 v5, 0x0

    #@1bc
    :goto_1bc
    if-ge v5, v12, :cond_1ce

    #@1be
    .line 540
    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c1
    move-result-object v16

    #@1c2
    check-cast v16, Landroid/animation/Animator$AnimatorListener;

    #@1c4
    move-object/from16 v0, v16

    #@1c6
    move-object/from16 v1, p0

    #@1c8
    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    #@1cb
    .line 539
    add-int/lit8 v5, v5, 0x1

    #@1cd
    goto :goto_1bc

    #@1ce
    .line 543
    .end local v12           #numListeners:I
    .end local v15           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_1ce
    move-object/from16 v0, p0

    #@1d0
    iget-object v0, v0, Landroid/animation/AnimatorSet;->mNodes:Ljava/util/ArrayList;

    #@1d2
    move-object/from16 v16, v0

    #@1d4
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@1d7
    move-result v16

    #@1d8
    if-nez v16, :cond_219

    #@1da
    move-object/from16 v0, p0

    #@1dc
    iget-wide v0, v0, Landroid/animation/AnimatorSet;->mStartDelay:J

    #@1de
    move-wide/from16 v16, v0

    #@1e0
    const-wide/16 v18, 0x0

    #@1e2
    cmp-long v16, v16, v18

    #@1e4
    if-nez v16, :cond_219

    #@1e6
    .line 546
    const/16 v16, 0x0

    #@1e8
    move/from16 v0, v16

    #@1ea
    move-object/from16 v1, p0

    #@1ec
    iput-boolean v0, v1, Landroid/animation/AnimatorSet;->mStarted:Z

    #@1ee
    .line 547
    move-object/from16 v0, p0

    #@1f0
    iget-object v0, v0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@1f2
    move-object/from16 v16, v0

    #@1f4
    if-eqz v16, :cond_219

    #@1f6
    .line 548
    move-object/from16 v0, p0

    #@1f8
    iget-object v0, v0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@1fa
    move-object/from16 v16, v0

    #@1fc
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@1ff
    move-result-object v15

    #@200
    check-cast v15, Ljava/util/ArrayList;

    #@202
    .line 550
    .restart local v15       #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@205
    move-result v12

    #@206
    .line 551
    .restart local v12       #numListeners:I
    const/4 v5, 0x0

    #@207
    :goto_207
    if-ge v5, v12, :cond_219

    #@209
    .line 552
    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20c
    move-result-object v16

    #@20d
    check-cast v16, Landroid/animation/Animator$AnimatorListener;

    #@20f
    move-object/from16 v0, v16

    #@211
    move-object/from16 v1, p0

    #@213
    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@216
    .line 551
    add-int/lit8 v5, v5, 0x1

    #@218
    goto :goto_207

    #@219
    .line 556
    .end local v12           #numListeners:I
    .end local v15           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_219
    return-void

    #@21a
    .line 515
    :array_21a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method
