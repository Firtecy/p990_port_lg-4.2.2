.class public final Landroid/animation/ObjectAnimator;
.super Landroid/animation/ValueAnimator;
.source "ObjectAnimator.java"


# static fields
.field private static final DBG:Z


# instance fields
.field private mProperty:Landroid/util/Property;

.field private mPropertyName:Ljava/lang/String;

.field private mTarget:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 135
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    #@3
    .line 136
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Landroid/util/Property;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Landroid/util/Property",
            "<TT;*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 158
    .local p1, target:Ljava/lang/Object;,"TT;"
    .local p2, property:Landroid/util/Property;,"Landroid/util/Property<TT;*>;"
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    #@3
    .line 159
    iput-object p1, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@5
    .line 160
    invoke-virtual {p0, p2}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    #@8
    .line 161
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 3
    .parameter "target"
    .parameter "propertyName"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    #@3
    .line 148
    iput-object p1, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@5
    .line 149
    invoke-virtual {p0, p2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    #@8
    .line 150
    return-void
.end method

.method public static varargs ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;
    .registers 4
    .parameter
    .parameter
    .parameter "values"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Landroid/util/Property",
            "<TT;",
            "Ljava/lang/Float;",
            ">;[F)",
            "Landroid/animation/ObjectAnimator;"
        }
    .end annotation

    #@0
    .prologue
    .line 235
    .local p0, target:Ljava/lang/Object;,"TT;"
    .local p1, property:Landroid/util/Property;,"Landroid/util/Property<TT;Ljava/lang/Float;>;"
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/ObjectAnimator;-><init>(Ljava/lang/Object;Landroid/util/Property;)V

    #@5
    .line 236
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    #@8
    .line 237
    return-object v0
.end method

.method public static varargs ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;
    .registers 4
    .parameter "target"
    .parameter "propertyName"
    .parameter "values"

    #@0
    .prologue
    .line 216
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/ObjectAnimator;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 217
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    #@8
    .line 218
    return-object v0
.end method

.method public static varargs ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;
    .registers 4
    .parameter
    .parameter
    .parameter "values"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Landroid/util/Property",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;[I)",
            "Landroid/animation/ObjectAnimator;"
        }
    .end annotation

    #@0
    .prologue
    .line 196
    .local p0, target:Ljava/lang/Object;,"TT;"
    .local p1, property:Landroid/util/Property;,"Landroid/util/Property<TT;Ljava/lang/Integer;>;"
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/ObjectAnimator;-><init>(Ljava/lang/Object;Landroid/util/Property;)V

    #@5
    .line 197
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    #@8
    .line 198
    return-object v0
.end method

.method public static varargs ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;
    .registers 4
    .parameter "target"
    .parameter "propertyName"
    .parameter "values"

    #@0
    .prologue
    .line 178
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/ObjectAnimator;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 179
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    #@8
    .line 180
    return-object v0
.end method

.method public static varargs ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Landroid/util/Property",
            "<TT;TV;>;",
            "Landroid/animation/TypeEvaluator",
            "<TV;>;[TV;)",
            "Landroid/animation/ObjectAnimator;"
        }
    .end annotation

    #@0
    .prologue
    .line 282
    .local p0, target:Ljava/lang/Object;,"TT;"
    .local p1, property:Landroid/util/Property;,"Landroid/util/Property<TT;TV;>;"
    .local p2, evaluator:Landroid/animation/TypeEvaluator;,"Landroid/animation/TypeEvaluator<TV;>;"
    .local p3, values:[Ljava/lang/Object;,"[TV;"
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/ObjectAnimator;-><init>(Ljava/lang/Object;Landroid/util/Property;)V

    #@5
    .line 283
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p3}, Landroid/animation/ObjectAnimator;->setObjectValues([Ljava/lang/Object;)V

    #@8
    .line 284
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@b
    .line 285
    return-object v0
.end method

.method public static varargs ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;
    .registers 5
    .parameter "target"
    .parameter "propertyName"
    .parameter "evaluator"
    .parameter "values"

    #@0
    .prologue
    .line 259
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/ObjectAnimator;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    #@5
    .line 260
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p3}, Landroid/animation/ObjectAnimator;->setObjectValues([Ljava/lang/Object;)V

    #@8
    .line 261
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@b
    .line 262
    return-object v0
.end method

.method public static varargs ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;
    .registers 3
    .parameter "target"
    .parameter "values"

    #@0
    .prologue
    .line 307
    new-instance v0, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    #@5
    .line 308
    .local v0, anim:Landroid/animation/ObjectAnimator;
    iput-object p0, v0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@7
    .line 309
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@a
    .line 310
    return-object v0
.end method


# virtual methods
.method animateValue(F)V
    .registers 6
    .parameter "fraction"

    #@0
    .prologue
    .line 473
    invoke-super {p0, p1}, Landroid/animation/ValueAnimator;->animateValue(F)V

    #@3
    .line 474
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@5
    array-length v1, v2

    #@6
    .line 475
    .local v1, numValues:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_15

    #@9
    .line 476
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@b
    aget-object v2, v2, v0

    #@d
    iget-object v3, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@f
    invoke-virtual {v2, v3}, Landroid/animation/PropertyValuesHolder;->setAnimatedValue(Ljava/lang/Object;)V

    #@12
    .line 475
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_7

    #@15
    .line 478
    :cond_15
    return-void
.end method

.method public bridge synthetic clone()Landroid/animation/Animator;
    .registers 2

    #@0
    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public clone()Landroid/animation/ObjectAnimator;
    .registers 2

    #@0
    .prologue
    .line 482
    invoke-super {p0}, Landroid/animation/ValueAnimator;->clone()Landroid/animation/ValueAnimator;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/animation/ObjectAnimator;

    #@6
    .line 483
    .local v0, anim:Landroid/animation/ObjectAnimator;
    return-object v0
.end method

.method public bridge synthetic clone()Landroid/animation/ValueAnimator;
    .registers 2

    #@0
    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getPropertyName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTarget()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 420
    iget-object v0, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method initAnimation()V
    .registers 5

    #@0
    .prologue
    .line 387
    iget-boolean v2, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@2
    if-nez v2, :cond_19

    #@4
    .line 390
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@6
    array-length v1, v2

    #@7
    .line 391
    .local v1, numValues:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_16

    #@a
    .line 392
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@c
    aget-object v2, v2, v0

    #@e
    iget-object v3, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@10
    invoke-virtual {v2, v3}, Landroid/animation/PropertyValuesHolder;->setupSetterAndGetter(Ljava/lang/Object;)V

    #@13
    .line 391
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_8

    #@16
    .line 394
    :cond_16
    invoke-super {p0}, Landroid/animation/ValueAnimator;->initAnimation()V

    #@19
    .line 396
    .end local v0           #i:I
    .end local v1           #numValues:I
    :cond_19
    return-void
.end method

.method public bridge synthetic setDuration(J)Landroid/animation/Animator;
    .registers 4
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-virtual {p0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public setDuration(J)Landroid/animation/ObjectAnimator;
    .registers 3
    .parameter "duration"

    #@0
    .prologue
    .line 409
    invoke-super {p0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@3
    .line 410
    return-object p0
.end method

.method public bridge synthetic setDuration(J)Landroid/animation/ValueAnimator;
    .registers 4
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-virtual {p0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public varargs setFloatValues([F)V
    .registers 5
    .parameter "values"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 330
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@8
    array-length v0, v0

    #@9
    if-nez v0, :cond_2b

    #@b
    .line 333
    :cond_b
    iget-object v0, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@d
    if-eqz v0, :cond_1d

    #@f
    .line 334
    new-array v0, v1, [Landroid/animation/PropertyValuesHolder;

    #@11
    iget-object v1, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@13
    invoke-static {v1, p1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    #@16
    move-result-object v1

    #@17
    aput-object v1, v0, v2

    #@19
    invoke-virtual {p0, v0}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@1c
    .line 341
    :goto_1c
    return-void

    #@1d
    .line 336
    :cond_1d
    new-array v0, v1, [Landroid/animation/PropertyValuesHolder;

    #@1f
    iget-object v1, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@21
    invoke-static {v1, p1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@24
    move-result-object v1

    #@25
    aput-object v1, v0, v2

    #@27
    invoke-virtual {p0, v0}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@2a
    goto :goto_1c

    #@2b
    .line 339
    :cond_2b
    invoke-super {p0, p1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    #@2e
    goto :goto_1c
.end method

.method public varargs setIntValues([I)V
    .registers 5
    .parameter "values"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 315
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@8
    array-length v0, v0

    #@9
    if-nez v0, :cond_2b

    #@b
    .line 318
    :cond_b
    iget-object v0, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@d
    if-eqz v0, :cond_1d

    #@f
    .line 319
    new-array v0, v1, [Landroid/animation/PropertyValuesHolder;

    #@11
    iget-object v1, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@13
    invoke-static {v1, p1}, Landroid/animation/PropertyValuesHolder;->ofInt(Landroid/util/Property;[I)Landroid/animation/PropertyValuesHolder;

    #@16
    move-result-object v1

    #@17
    aput-object v1, v0, v2

    #@19
    invoke-virtual {p0, v0}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@1c
    .line 326
    :goto_1c
    return-void

    #@1d
    .line 321
    :cond_1d
    new-array v0, v1, [Landroid/animation/PropertyValuesHolder;

    #@1f
    iget-object v1, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@21
    invoke-static {v1, p1}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@24
    move-result-object v1

    #@25
    aput-object v1, v0, v2

    #@27
    invoke-virtual {p0, v0}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@2a
    goto :goto_1c

    #@2b
    .line 324
    :cond_2b
    invoke-super {p0, p1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    #@2e
    goto :goto_1c
.end method

.method public varargs setObjectValues([Ljava/lang/Object;)V
    .registers 6
    .parameter "values"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 345
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@5
    if-eqz v1, :cond_c

    #@7
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@9
    array-length v1, v1

    #@a
    if-nez v1, :cond_30

    #@c
    .line 348
    :cond_c
    iget-object v1, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@e
    if-eqz v1, :cond_20

    #@10
    .line 349
    new-array v1, v2, [Landroid/animation/PropertyValuesHolder;

    #@12
    iget-object v2, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@14
    check-cast v0, Landroid/animation/TypeEvaluator;

    #@16
    invoke-static {v2, v0, p1}, Landroid/animation/PropertyValuesHolder;->ofObject(Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;

    #@19
    move-result-object v0

    #@1a
    aput-object v0, v1, v3

    #@1c
    invoke-virtual {p0, v1}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@1f
    .line 356
    :goto_1f
    return-void

    #@20
    .line 351
    :cond_20
    new-array v1, v2, [Landroid/animation/PropertyValuesHolder;

    #@22
    iget-object v2, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@24
    check-cast v0, Landroid/animation/TypeEvaluator;

    #@26
    invoke-static {v2, v0, p1}, Landroid/animation/PropertyValuesHolder;->ofObject(Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;

    #@29
    move-result-object v0

    #@2a
    aput-object v0, v1, v3

    #@2c
    invoke-virtual {p0, v1}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@2f
    goto :goto_1f

    #@30
    .line 354
    :cond_30
    invoke-super {p0, p1}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    #@33
    goto :goto_1f
.end method

.method public setProperty(Landroid/util/Property;)V
    .registers 7
    .parameter "property"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 103
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@3
    if-eqz v2, :cond_1c

    #@5
    .line 104
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@7
    aget-object v1, v2, v4

    #@9
    .line 105
    .local v1, valuesHolder:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v1}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 106
    .local v0, oldName:Ljava/lang/String;
    invoke-virtual {v1, p1}, Landroid/animation/PropertyValuesHolder;->setProperty(Landroid/util/Property;)V

    #@10
    .line 107
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@12
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 108
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@17
    iget-object v3, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@19
    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 110
    .end local v0           #oldName:Ljava/lang/String;
    .end local v1           #valuesHolder:Landroid/animation/PropertyValuesHolder;
    :cond_1c
    iget-object v2, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@1e
    if-eqz v2, :cond_26

    #@20
    .line 111
    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    iput-object v2, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@26
    .line 113
    :cond_26
    iput-object p1, p0, Landroid/animation/ObjectAnimator;->mProperty:Landroid/util/Property;

    #@28
    .line 115
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@2a
    .line 116
    return-void
.end method

.method public setPropertyName(Ljava/lang/String;)V
    .registers 6
    .parameter "propertyName"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 81
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@3
    if-eqz v2, :cond_1a

    #@5
    .line 82
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@7
    aget-object v1, v2, v3

    #@9
    .line 83
    .local v1, valuesHolder:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v1}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 84
    .local v0, oldName:Ljava/lang/String;
    invoke-virtual {v1, p1}, Landroid/animation/PropertyValuesHolder;->setPropertyName(Ljava/lang/String;)V

    #@10
    .line 85
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@12
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 86
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@17
    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 88
    .end local v0           #oldName:Ljava/lang/String;
    .end local v1           #valuesHolder:Landroid/animation/PropertyValuesHolder;
    :cond_1a
    iput-object p1, p0, Landroid/animation/ObjectAnimator;->mPropertyName:Ljava/lang/String;

    #@1c
    .line 90
    iput-boolean v3, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@1e
    .line 91
    return-void
.end method

.method public setTarget(Ljava/lang/Object;)V
    .registers 5
    .parameter "target"

    #@0
    .prologue
    .line 430
    iget-object v1, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@2
    if-eq v1, p1, :cond_16

    #@4
    .line 431
    iget-object v0, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@6
    .line 432
    .local v0, oldTarget:Ljava/lang/Object;
    iput-object p1, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@8
    .line 433
    if-eqz v0, :cond_17

    #@a
    if-eqz p1, :cond_17

    #@c
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@13
    move-result-object v2

    #@14
    if-ne v1, v2, :cond_17

    #@16
    .line 439
    .end local v0           #oldTarget:Ljava/lang/Object;
    :cond_16
    :goto_16
    return-void

    #@17
    .line 437
    .restart local v0       #oldTarget:Ljava/lang/Object;
    :cond_17
    const/4 v1, 0x0

    #@18
    iput-boolean v1, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@1a
    goto :goto_16
.end method

.method public setupEndValues()V
    .registers 5

    #@0
    .prologue
    .line 452
    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->initAnimation()V

    #@3
    .line 453
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@5
    array-length v1, v2

    #@6
    .line 454
    .local v1, numValues:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_15

    #@9
    .line 455
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@b
    aget-object v2, v2, v0

    #@d
    iget-object v3, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@f
    invoke-virtual {v2, v3}, Landroid/animation/PropertyValuesHolder;->setupEndValue(Ljava/lang/Object;)V

    #@12
    .line 454
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_7

    #@15
    .line 457
    :cond_15
    return-void
.end method

.method public setupStartValues()V
    .registers 5

    #@0
    .prologue
    .line 443
    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->initAnimation()V

    #@3
    .line 444
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@5
    array-length v1, v2

    #@6
    .line 445
    .local v1, numValues:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_15

    #@9
    .line 446
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@b
    aget-object v2, v2, v0

    #@d
    iget-object v3, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@f
    invoke-virtual {v2, v3}, Landroid/animation/PropertyValuesHolder;->setupStartValue(Ljava/lang/Object;)V

    #@12
    .line 445
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_7

    #@15
    .line 448
    :cond_15
    return-void
.end method

.method public start()V
    .registers 1

    #@0
    .prologue
    .line 370
    invoke-super {p0}, Landroid/animation/ValueAnimator;->start()V

    #@3
    .line 371
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 488
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "ObjectAnimator@"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@e
    move-result v3

    #@f
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", target "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Landroid/animation/ObjectAnimator;->mTarget:Ljava/lang/Object;

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 490
    .local v1, returnVal:Ljava/lang/String;
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@29
    if-eqz v2, :cond_53

    #@2b
    .line 491
    const/4 v0, 0x0

    #@2c
    .local v0, i:I
    :goto_2c
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@2e
    array-length v2, v2

    #@2f
    if-ge v0, v2, :cond_53

    #@31
    .line 492
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    const-string v3, "\n    "

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@42
    aget-object v3, v3, v0

    #@44
    invoke-virtual {v3}, Landroid/animation/PropertyValuesHolder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    .line 491
    add-int/lit8 v0, v0, 0x1

    #@52
    goto :goto_2c

    #@53
    .line 495
    .end local v0           #i:I
    :cond_53
    return-object v1
.end method
