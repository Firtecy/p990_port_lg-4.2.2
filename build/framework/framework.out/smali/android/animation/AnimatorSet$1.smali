.class Landroid/animation/AnimatorSet$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "AnimatorSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/animation/AnimatorSet;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field canceled:Z

.field final synthetic this$0:Landroid/animation/AnimatorSet;

.field final synthetic val$nodesToStart:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Landroid/animation/AnimatorSet;Ljava/util/ArrayList;)V
    .registers 4
    .parameter
    .parameter

    #@0
    .prologue
    .line 517
    iput-object p1, p0, Landroid/animation/AnimatorSet$1;->this$0:Landroid/animation/AnimatorSet;

    #@2
    iput-object p2, p0, Landroid/animation/AnimatorSet$1;->val$nodesToStart:Ljava/util/ArrayList;

    #@4
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    #@7
    .line 518
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/animation/AnimatorSet$1;->canceled:Z

    #@a
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter "anim"

    #@0
    .prologue
    .line 520
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/animation/AnimatorSet$1;->canceled:Z

    #@3
    .line 521
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 7
    .parameter "anim"

    #@0
    .prologue
    .line 523
    iget-boolean v3, p0, Landroid/animation/AnimatorSet$1;->canceled:Z

    #@2
    if-nez v3, :cond_28

    #@4
    .line 524
    iget-object v3, p0, Landroid/animation/AnimatorSet$1;->val$nodesToStart:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    .line 525
    .local v2, numNodes:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v2, :cond_28

    #@d
    .line 526
    iget-object v3, p0, Landroid/animation/AnimatorSet$1;->val$nodesToStart:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/animation/AnimatorSet$Node;

    #@15
    .line 527
    .local v1, node:Landroid/animation/AnimatorSet$Node;
    iget-object v3, v1, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@17
    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    #@1a
    .line 528
    iget-object v3, p0, Landroid/animation/AnimatorSet$1;->this$0:Landroid/animation/AnimatorSet;

    #@1c
    invoke-static {v3}, Landroid/animation/AnimatorSet;->access$000(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;

    #@1f
    move-result-object v3

    #@20
    iget-object v4, v1, Landroid/animation/AnimatorSet$Node;->animation:Landroid/animation/Animator;

    #@22
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 525
    add-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_b

    #@28
    .line 531
    .end local v0           #i:I
    .end local v1           #node:Landroid/animation/AnimatorSet$Node;
    .end local v2           #numNodes:I
    :cond_28
    return-void
.end method
