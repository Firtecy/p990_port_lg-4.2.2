.class Landroid/animation/LayoutTransition$1;
.super Ljava/lang/Object;
.source "LayoutTransition.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/animation/LayoutTransition;->runChangeTransition(Landroid/view/ViewGroup;Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/animation/LayoutTransition;

.field final synthetic val$parent:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 781
    iput-object p1, p0, Landroid/animation/LayoutTransition$1;->this$0:Landroid/animation/LayoutTransition;

    #@2
    iput-object p2, p0, Landroid/animation/LayoutTransition$1;->val$parent:Landroid/view/ViewGroup;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .registers 7

    #@0
    .prologue
    .line 783
    iget-object v5, p0, Landroid/animation/LayoutTransition$1;->val$parent:Landroid/view/ViewGroup;

    #@2
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@5
    move-result-object v5

    #@6
    invoke-virtual {v5, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@9
    .line 784
    iget-object v5, p0, Landroid/animation/LayoutTransition$1;->this$0:Landroid/animation/LayoutTransition;

    #@b
    invoke-static {v5}, Landroid/animation/LayoutTransition;->access$000(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@e
    move-result-object v5

    #@f
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@12
    move-result v0

    #@13
    .line 785
    .local v0, count:I
    if-lez v0, :cond_3f

    #@15
    .line 786
    iget-object v5, p0, Landroid/animation/LayoutTransition$1;->this$0:Landroid/animation/LayoutTransition;

    #@17
    invoke-static {v5}, Landroid/animation/LayoutTransition;->access$000(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@1e
    move-result-object v4

    #@1f
    .line 787
    .local v4, views:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/view/View;>;"
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v1

    #@23
    .local v1, i$:Ljava/util/Iterator;
    :goto_23
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_3f

    #@29
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/view/View;

    #@2f
    .line 788
    .local v3, view:Landroid/view/View;
    iget-object v5, p0, Landroid/animation/LayoutTransition$1;->this$0:Landroid/animation/LayoutTransition;

    #@31
    invoke-static {v5}, Landroid/animation/LayoutTransition;->access$000(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v2

    #@39
    check-cast v2, Landroid/view/View$OnLayoutChangeListener;

    #@3b
    .line 789
    .local v2, listener:Landroid/view/View$OnLayoutChangeListener;
    invoke-virtual {v3, v2}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    #@3e
    goto :goto_23

    #@3f
    .line 792
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #listener:Landroid/view/View$OnLayoutChangeListener;
    .end local v3           #view:Landroid/view/View;
    .end local v4           #views:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/view/View;>;"
    :cond_3f
    iget-object v5, p0, Landroid/animation/LayoutTransition$1;->this$0:Landroid/animation/LayoutTransition;

    #@41
    invoke-static {v5}, Landroid/animation/LayoutTransition;->access$000(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    #@48
    .line 793
    const/4 v5, 0x1

    #@49
    return v5
.end method
