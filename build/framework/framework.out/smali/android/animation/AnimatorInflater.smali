.class public Landroid/animation/AnimatorInflater;
.super Ljava/lang/Object;
.source "AnimatorInflater.java"


# static fields
.field private static final SEQUENTIALLY:I = 0x1

.field private static final TOGETHER:I = 0x0

.field private static final VALUE_TYPE_COLOR:I = 0x4

.field private static final VALUE_TYPE_CUSTOM:I = 0x5

.field private static final VALUE_TYPE_FLOAT:I = 0x0

.field private static final VALUE_TYPE_INT:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static createAnimatorFromXml(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Landroid/animation/Animator;
    .registers 5
    .parameter "c"
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 93
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {p0, p1, v0, v1, v2}, Landroid/animation/AnimatorInflater;->createAnimatorFromXml(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/animation/AnimatorSet;I)Landroid/animation/Animator;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private static createAnimatorFromXml(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/animation/AnimatorSet;I)Landroid/animation/Animator;
    .registers 22
    .parameter "c"
    .parameter "parser"
    .parameter "attrs"
    .parameter "parent"
    .parameter "sequenceOrdering"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    const/4 v4, 0x0

    #@1
    .line 101
    .local v4, anim:Landroid/animation/Animator;
    const/4 v6, 0x0

    #@2
    .line 105
    .local v6, childAnims:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@5
    move-result v7

    #@6
    .line 108
    .local v7, depth:I
    :cond_6
    :goto_6
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@9
    move-result v13

    #@a
    .local v13, type:I
    const/4 v14, 0x3

    #@b
    if-ne v13, v14, :cond_13

    #@d
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@10
    move-result v14

    #@11
    if-le v14, v7, :cond_98

    #@13
    :cond_13
    const/4 v14, 0x1

    #@14
    if-eq v13, v14, :cond_98

    #@16
    .line 110
    const/4 v14, 0x2

    #@17
    if-ne v13, v14, :cond_6

    #@19
    .line 114
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1c
    move-result-object v11

    #@1d
    .line 116
    .local v11, name:Ljava/lang/String;
    const-string/jumbo v14, "objectAnimator"

    #@20
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v14

    #@24
    if-eqz v14, :cond_3b

    #@26
    .line 117
    move-object/from16 v0, p0

    #@28
    move-object/from16 v1, p2

    #@2a
    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadObjectAnimator(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/animation/ObjectAnimator;

    #@2d
    move-result-object v4

    #@2e
    .line 132
    :goto_2e
    if-eqz p3, :cond_6

    #@30
    .line 133
    if-nez v6, :cond_37

    #@32
    .line 134
    new-instance v6, Ljava/util/ArrayList;

    #@34
    .end local v6           #childAnims:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@37
    .line 136
    .restart local v6       #childAnims:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    :cond_37
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_6

    #@3b
    .line 118
    :cond_3b
    const-string v14, "animator"

    #@3d
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v14

    #@41
    if-eqz v14, :cond_4d

    #@43
    .line 119
    const/4 v14, 0x0

    #@44
    move-object/from16 v0, p0

    #@46
    move-object/from16 v1, p2

    #@48
    invoke-static {v0, v1, v14}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    #@4b
    move-result-object v4

    #@4c
    goto :goto_2e

    #@4d
    .line 120
    :cond_4d
    const-string/jumbo v14, "set"

    #@50
    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v14

    #@54
    if-eqz v14, :cond_7b

    #@56
    .line 121
    new-instance v4, Landroid/animation/AnimatorSet;

    #@58
    .end local v4           #anim:Landroid/animation/Animator;
    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    #@5b
    .line 122
    .restart local v4       #anim:Landroid/animation/Animator;
    sget-object v14, Lcom/android/internal/R$styleable;->AnimatorSet:[I

    #@5d
    move-object/from16 v0, p0

    #@5f
    move-object/from16 v1, p2

    #@61
    invoke-virtual {v0, v1, v14}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@64
    move-result-object v3

    #@65
    .line 124
    .local v3, a:Landroid/content/res/TypedArray;
    const/4 v14, 0x0

    #@66
    const/4 v15, 0x0

    #@67
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@6a
    move-result v12

    #@6b
    .local v12, ordering:I
    move-object v14, v4

    #@6c
    .line 126
    check-cast v14, Landroid/animation/AnimatorSet;

    #@6e
    move-object/from16 v0, p0

    #@70
    move-object/from16 v1, p1

    #@72
    move-object/from16 v2, p2

    #@74
    invoke-static {v0, v1, v2, v14, v12}, Landroid/animation/AnimatorInflater;->createAnimatorFromXml(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/animation/AnimatorSet;I)Landroid/animation/Animator;

    #@77
    .line 127
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@7a
    goto :goto_2e

    #@7b
    .line 129
    .end local v3           #a:Landroid/content/res/TypedArray;
    .end local v12           #ordering:I
    :cond_7b
    new-instance v14, Ljava/lang/RuntimeException;

    #@7d
    new-instance v15, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v16, "Unknown animator name: "

    #@84
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v15

    #@88
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@8b
    move-result-object v16

    #@8c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v15

    #@90
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v15

    #@94
    invoke-direct {v14, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@97
    throw v14

    #@98
    .line 139
    .end local v11           #name:Ljava/lang/String;
    :cond_98
    if-eqz p3, :cond_c0

    #@9a
    if-eqz v6, :cond_c0

    #@9c
    .line 140
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@9f
    move-result v14

    #@a0
    new-array v5, v14, [Landroid/animation/Animator;

    #@a2
    .line 141
    .local v5, animsArray:[Landroid/animation/Animator;
    const/4 v9, 0x0

    #@a3
    .line 142
    .local v9, index:I
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a6
    move-result-object v8

    #@a7
    .local v8, i$:Ljava/util/Iterator;
    :goto_a7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@aa
    move-result v14

    #@ab
    if-eqz v14, :cond_b9

    #@ad
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b0
    move-result-object v3

    #@b1
    check-cast v3, Landroid/animation/Animator;

    #@b3
    .line 143
    .local v3, a:Landroid/animation/Animator;
    add-int/lit8 v10, v9, 0x1

    #@b5
    .end local v9           #index:I
    .local v10, index:I
    aput-object v3, v5, v9

    #@b7
    move v9, v10

    #@b8
    .end local v10           #index:I
    .restart local v9       #index:I
    goto :goto_a7

    #@b9
    .line 145
    .end local v3           #a:Landroid/animation/Animator;
    :cond_b9
    if-nez p4, :cond_c1

    #@bb
    .line 146
    move-object/from16 v0, p3

    #@bd
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    #@c0
    .line 152
    .end local v5           #animsArray:[Landroid/animation/Animator;
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #index:I
    :cond_c0
    :goto_c0
    return-object v4

    #@c1
    .line 148
    .restart local v5       #animsArray:[Landroid/animation/Animator;
    .restart local v8       #i$:Ljava/util/Iterator;
    .restart local v9       #index:I
    :cond_c1
    move-object/from16 v0, p3

    #@c3
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    #@c6
    goto :goto_c0
.end method

.method public static loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;
    .registers 7
    .parameter "context"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    const/4 v1, 0x0

    #@1
    .line 71
    .local v1, parser:Landroid/content/res/XmlResourceParser;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getAnimation(I)Landroid/content/res/XmlResourceParser;

    #@8
    move-result-object v1

    #@9
    .line 72
    invoke-static {p0, v1}, Landroid/animation/AnimatorInflater;->createAnimatorFromXml(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Landroid/animation/Animator;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_34
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_c} :catch_13
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_3b

    #@c
    move-result-object v3

    #@d
    .line 86
    if-eqz v1, :cond_12

    #@f
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    #@12
    :cond_12
    return-object v3

    #@13
    .line 73
    :catch_13
    move-exception v0

    #@14
    .line 74
    .local v0, ex:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_14
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "Can\'t load animation resource ID #0x"

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-direct {v2, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@30
    .line 77
    .local v2, rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v2, v0}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@33
    .line 78
    throw v2
    :try_end_34
    .catchall {:try_start_14 .. :try_end_34} :catchall_34

    #@34
    .line 86
    .end local v0           #ex:Lorg/xmlpull/v1/XmlPullParserException;
    .end local v2           #rnf:Landroid/content/res/Resources$NotFoundException;
    :catchall_34
    move-exception v3

    #@35
    if-eqz v1, :cond_3a

    #@37
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    #@3a
    :cond_3a
    throw v3

    #@3b
    .line 79
    :catch_3b
    move-exception v0

    #@3c
    .line 80
    .local v0, ex:Ljava/io/IOException;
    :try_start_3c
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "Can\'t load animation resource ID #0x"

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-direct {v2, v3}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    #@58
    .line 83
    .restart local v2       #rnf:Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v2, v0}, Landroid/content/res/Resources$NotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@5b
    .line 84
    throw v2
    :try_end_5c
    .catchall {:try_start_3c .. :try_end_5c} :catchall_34
.end method

.method private static loadAnimator(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .registers 27
    .parameter "context"
    .parameter "attrs"
    .parameter "anim"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    sget-object v22, Lcom/android/internal/R$styleable;->Animator:[I

    #@2
    move-object/from16 v0, p0

    #@4
    move-object/from16 v1, p1

    #@6
    move-object/from16 v2, v22

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v3

    #@c
    .line 188
    .local v3, a:Landroid/content/res/TypedArray;
    const/16 v22, 0x1

    #@e
    const/16 v23, 0x0

    #@10
    move/from16 v0, v22

    #@12
    move/from16 v1, v23

    #@14
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@17
    move-result v22

    #@18
    move/from16 v0, v22

    #@1a
    int-to-long v4, v0

    #@1b
    .line 190
    .local v4, duration:J
    const/16 v22, 0x2

    #@1d
    const/16 v23, 0x0

    #@1f
    move/from16 v0, v22

    #@21
    move/from16 v1, v23

    #@23
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@26
    move-result v22

    #@27
    move/from16 v0, v22

    #@29
    int-to-long v12, v0

    #@2a
    .line 192
    .local v12, startDelay:J
    const/16 v22, 0x7

    #@2c
    const/16 v23, 0x0

    #@2e
    move/from16 v0, v22

    #@30
    move/from16 v1, v23

    #@32
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@35
    move-result v21

    #@36
    .line 195
    .local v21, valueType:I
    if-nez p2, :cond_3d

    #@38
    .line 196
    new-instance p2, Landroid/animation/ValueAnimator;

    #@3a
    .end local p2
    invoke-direct/range {p2 .. p2}, Landroid/animation/ValueAnimator;-><init>()V

    #@3d
    .line 198
    .restart local p2
    :cond_3d
    const/4 v6, 0x0

    #@3e
    .line 200
    .local v6, evaluator:Landroid/animation/TypeEvaluator;
    const/16 v18, 0x5

    #@40
    .line 201
    .local v18, valueFromIndex:I
    const/16 v20, 0x6

    #@42
    .line 203
    .local v20, valueToIndex:I
    if-nez v21, :cond_131

    #@44
    const/4 v8, 0x1

    #@45
    .line 205
    .local v8, getFloats:Z
    :goto_45
    move/from16 v0, v18

    #@47
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@4a
    move-result-object v15

    #@4b
    .line 206
    .local v15, tvFrom:Landroid/util/TypedValue;
    if-eqz v15, :cond_134

    #@4d
    const/4 v9, 0x1

    #@4e
    .line 207
    .local v9, hasFrom:Z
    :goto_4e
    if-eqz v9, :cond_137

    #@50
    iget v7, v15, Landroid/util/TypedValue;->type:I

    #@52
    .line 208
    .local v7, fromType:I
    :goto_52
    move/from16 v0, v20

    #@54
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@57
    move-result-object v16

    #@58
    .line 209
    .local v16, tvTo:Landroid/util/TypedValue;
    if-eqz v16, :cond_13a

    #@5a
    const/4 v10, 0x1

    #@5b
    .line 210
    .local v10, hasTo:Z
    :goto_5b
    if-eqz v10, :cond_13d

    #@5d
    move-object/from16 v0, v16

    #@5f
    iget v14, v0, Landroid/util/TypedValue;->type:I

    #@61
    .line 212
    .local v14, toType:I
    :goto_61
    if-eqz v9, :cond_6f

    #@63
    const/16 v22, 0x1c

    #@65
    move/from16 v0, v22

    #@67
    if-lt v7, v0, :cond_6f

    #@69
    const/16 v22, 0x1f

    #@6b
    move/from16 v0, v22

    #@6d
    if-le v7, v0, :cond_7d

    #@6f
    :cond_6f
    if-eqz v10, :cond_8a

    #@71
    const/16 v22, 0x1c

    #@73
    move/from16 v0, v22

    #@75
    if-lt v14, v0, :cond_8a

    #@77
    const/16 v22, 0x1f

    #@79
    move/from16 v0, v22

    #@7b
    if-gt v14, v0, :cond_8a

    #@7d
    .line 217
    :cond_7d
    const/4 v8, 0x0

    #@7e
    .line 218
    new-instance v22, Landroid/animation/ArgbEvaluator;

    #@80
    invoke-direct/range {v22 .. v22}, Landroid/animation/ArgbEvaluator;-><init>()V

    #@83
    move-object/from16 v0, p2

    #@85
    move-object/from16 v1, v22

    #@87
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@8a
    .line 221
    :cond_8a
    if-eqz v8, :cond_19d

    #@8c
    .line 224
    if-eqz v9, :cond_16d

    #@8e
    .line 225
    const/16 v22, 0x5

    #@90
    move/from16 v0, v22

    #@92
    if-ne v7, v0, :cond_140

    #@94
    .line 226
    const/16 v22, 0x0

    #@96
    move/from16 v0, v18

    #@98
    move/from16 v1, v22

    #@9a
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@9d
    move-result v17

    #@9e
    .line 230
    .local v17, valueFrom:F
    :goto_9e
    if-eqz v10, :cond_158

    #@a0
    .line 231
    const/16 v22, 0x5

    #@a2
    move/from16 v0, v22

    #@a4
    if-ne v14, v0, :cond_14c

    #@a6
    .line 232
    const/16 v22, 0x0

    #@a8
    move/from16 v0, v20

    #@aa
    move/from16 v1, v22

    #@ac
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@af
    move-result v19

    #@b0
    .line 236
    .local v19, valueTo:F
    :goto_b0
    const/16 v22, 0x2

    #@b2
    move/from16 v0, v22

    #@b4
    new-array v0, v0, [F

    #@b6
    move-object/from16 v22, v0

    #@b8
    const/16 v23, 0x0

    #@ba
    aput v17, v22, v23

    #@bc
    const/16 v23, 0x1

    #@be
    aput v19, v22, v23

    #@c0
    move-object/from16 v0, p2

    #@c2
    move-object/from16 v1, v22

    #@c4
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    #@c7
    .line 288
    .end local v17           #valueFrom:F
    .end local v19           #valueTo:F
    :cond_c7
    :goto_c7
    move-object/from16 v0, p2

    #@c9
    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@cc
    .line 289
    move-object/from16 v0, p2

    #@ce
    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    #@d1
    .line 291
    const/16 v22, 0x3

    #@d3
    move/from16 v0, v22

    #@d5
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@d8
    move-result v22

    #@d9
    if-eqz v22, :cond_ee

    #@db
    .line 292
    const/16 v22, 0x3

    #@dd
    const/16 v23, 0x0

    #@df
    move/from16 v0, v22

    #@e1
    move/from16 v1, v23

    #@e3
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@e6
    move-result v22

    #@e7
    move-object/from16 v0, p2

    #@e9
    move/from16 v1, v22

    #@eb
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    #@ee
    .line 295
    :cond_ee
    const/16 v22, 0x4

    #@f0
    move/from16 v0, v22

    #@f2
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@f5
    move-result v22

    #@f6
    if-eqz v22, :cond_10b

    #@f8
    .line 296
    const/16 v22, 0x4

    #@fa
    const/16 v23, 0x1

    #@fc
    move/from16 v0, v22

    #@fe
    move/from16 v1, v23

    #@100
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@103
    move-result v22

    #@104
    move-object/from16 v0, p2

    #@106
    move/from16 v1, v22

    #@108
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    #@10b
    .line 300
    :cond_10b
    if-eqz v6, :cond_112

    #@10d
    .line 301
    move-object/from16 v0, p2

    #@10f
    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@112
    .line 304
    :cond_112
    const/16 v22, 0x0

    #@114
    const/16 v23, 0x0

    #@116
    move/from16 v0, v22

    #@118
    move/from16 v1, v23

    #@11a
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@11d
    move-result v11

    #@11e
    .line 306
    .local v11, resID:I
    if-lez v11, :cond_12d

    #@120
    .line 307
    move-object/from16 v0, p0

    #@122
    invoke-static {v0, v11}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    #@125
    move-result-object v22

    #@126
    move-object/from16 v0, p2

    #@128
    move-object/from16 v1, v22

    #@12a
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@12d
    .line 309
    :cond_12d
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@130
    .line 311
    return-object p2

    #@131
    .line 203
    .end local v7           #fromType:I
    .end local v8           #getFloats:Z
    .end local v9           #hasFrom:Z
    .end local v10           #hasTo:Z
    .end local v11           #resID:I
    .end local v14           #toType:I
    .end local v15           #tvFrom:Landroid/util/TypedValue;
    .end local v16           #tvTo:Landroid/util/TypedValue;
    :cond_131
    const/4 v8, 0x0

    #@132
    goto/16 :goto_45

    #@134
    .line 206
    .restart local v8       #getFloats:Z
    .restart local v15       #tvFrom:Landroid/util/TypedValue;
    :cond_134
    const/4 v9, 0x0

    #@135
    goto/16 :goto_4e

    #@137
    .line 207
    .restart local v9       #hasFrom:Z
    :cond_137
    const/4 v7, 0x0

    #@138
    goto/16 :goto_52

    #@13a
    .line 209
    .restart local v7       #fromType:I
    .restart local v16       #tvTo:Landroid/util/TypedValue;
    :cond_13a
    const/4 v10, 0x0

    #@13b
    goto/16 :goto_5b

    #@13d
    .line 210
    .restart local v10       #hasTo:Z
    :cond_13d
    const/4 v14, 0x0

    #@13e
    goto/16 :goto_61

    #@140
    .line 228
    .restart local v14       #toType:I
    :cond_140
    const/16 v22, 0x0

    #@142
    move/from16 v0, v18

    #@144
    move/from16 v1, v22

    #@146
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@149
    move-result v17

    #@14a
    .restart local v17       #valueFrom:F
    goto/16 :goto_9e

    #@14c
    .line 234
    :cond_14c
    const/16 v22, 0x0

    #@14e
    move/from16 v0, v20

    #@150
    move/from16 v1, v22

    #@152
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@155
    move-result v19

    #@156
    .restart local v19       #valueTo:F
    goto/16 :goto_b0

    #@158
    .line 238
    .end local v19           #valueTo:F
    :cond_158
    const/16 v22, 0x1

    #@15a
    move/from16 v0, v22

    #@15c
    new-array v0, v0, [F

    #@15e
    move-object/from16 v22, v0

    #@160
    const/16 v23, 0x0

    #@162
    aput v17, v22, v23

    #@164
    move-object/from16 v0, p2

    #@166
    move-object/from16 v1, v22

    #@168
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    #@16b
    goto/16 :goto_c7

    #@16d
    .line 241
    .end local v17           #valueFrom:F
    :cond_16d
    const/16 v22, 0x5

    #@16f
    move/from16 v0, v22

    #@171
    if-ne v14, v0, :cond_192

    #@173
    .line 242
    const/16 v22, 0x0

    #@175
    move/from16 v0, v20

    #@177
    move/from16 v1, v22

    #@179
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@17c
    move-result v19

    #@17d
    .line 246
    .restart local v19       #valueTo:F
    :goto_17d
    const/16 v22, 0x1

    #@17f
    move/from16 v0, v22

    #@181
    new-array v0, v0, [F

    #@183
    move-object/from16 v22, v0

    #@185
    const/16 v23, 0x0

    #@187
    aput v19, v22, v23

    #@189
    move-object/from16 v0, p2

    #@18b
    move-object/from16 v1, v22

    #@18d
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    #@190
    goto/16 :goto_c7

    #@192
    .line 244
    .end local v19           #valueTo:F
    :cond_192
    const/16 v22, 0x0

    #@194
    move/from16 v0, v20

    #@196
    move/from16 v1, v22

    #@198
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@19b
    move-result v19

    #@19c
    .restart local v19       #valueTo:F
    goto :goto_17d

    #@19d
    .line 251
    .end local v19           #valueTo:F
    :cond_19d
    if-eqz v9, :cond_23d

    #@19f
    .line 252
    const/16 v22, 0x5

    #@1a1
    move/from16 v0, v22

    #@1a3
    if-ne v7, v0, :cond_1e4

    #@1a5
    .line 253
    const/16 v22, 0x0

    #@1a7
    move/from16 v0, v18

    #@1a9
    move/from16 v1, v22

    #@1ab
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@1ae
    move-result v22

    #@1af
    move/from16 v0, v22

    #@1b1
    float-to-int v0, v0

    #@1b2
    move/from16 v17, v0

    #@1b4
    .line 260
    .local v17, valueFrom:I
    :goto_1b4
    if-eqz v10, :cond_228

    #@1b6
    .line 261
    const/16 v22, 0x5

    #@1b8
    move/from16 v0, v22

    #@1ba
    if-ne v14, v0, :cond_206

    #@1bc
    .line 262
    const/16 v22, 0x0

    #@1be
    move/from16 v0, v20

    #@1c0
    move/from16 v1, v22

    #@1c2
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@1c5
    move-result v22

    #@1c6
    move/from16 v0, v22

    #@1c8
    float-to-int v0, v0

    #@1c9
    move/from16 v19, v0

    #@1cb
    .line 269
    .local v19, valueTo:I
    :goto_1cb
    const/16 v22, 0x2

    #@1cd
    move/from16 v0, v22

    #@1cf
    new-array v0, v0, [I

    #@1d1
    move-object/from16 v22, v0

    #@1d3
    const/16 v23, 0x0

    #@1d5
    aput v17, v22, v23

    #@1d7
    const/16 v23, 0x1

    #@1d9
    aput v19, v22, v23

    #@1db
    move-object/from16 v0, p2

    #@1dd
    move-object/from16 v1, v22

    #@1df
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    #@1e2
    goto/16 :goto_c7

    #@1e4
    .line 254
    .end local v17           #valueFrom:I
    .end local v19           #valueTo:I
    :cond_1e4
    const/16 v22, 0x1c

    #@1e6
    move/from16 v0, v22

    #@1e8
    if-lt v7, v0, :cond_1fb

    #@1ea
    const/16 v22, 0x1f

    #@1ec
    move/from16 v0, v22

    #@1ee
    if-gt v7, v0, :cond_1fb

    #@1f0
    .line 256
    const/16 v22, 0x0

    #@1f2
    move/from16 v0, v18

    #@1f4
    move/from16 v1, v22

    #@1f6
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@1f9
    move-result v17

    #@1fa
    .restart local v17       #valueFrom:I
    goto :goto_1b4

    #@1fb
    .line 258
    .end local v17           #valueFrom:I
    :cond_1fb
    const/16 v22, 0x0

    #@1fd
    move/from16 v0, v18

    #@1ff
    move/from16 v1, v22

    #@201
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@204
    move-result v17

    #@205
    .restart local v17       #valueFrom:I
    goto :goto_1b4

    #@206
    .line 263
    :cond_206
    const/16 v22, 0x1c

    #@208
    move/from16 v0, v22

    #@20a
    if-lt v14, v0, :cond_21d

    #@20c
    const/16 v22, 0x1f

    #@20e
    move/from16 v0, v22

    #@210
    if-gt v14, v0, :cond_21d

    #@212
    .line 265
    const/16 v22, 0x0

    #@214
    move/from16 v0, v20

    #@216
    move/from16 v1, v22

    #@218
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@21b
    move-result v19

    #@21c
    .restart local v19       #valueTo:I
    goto :goto_1cb

    #@21d
    .line 267
    .end local v19           #valueTo:I
    :cond_21d
    const/16 v22, 0x0

    #@21f
    move/from16 v0, v20

    #@221
    move/from16 v1, v22

    #@223
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@226
    move-result v19

    #@227
    .restart local v19       #valueTo:I
    goto :goto_1cb

    #@228
    .line 271
    .end local v19           #valueTo:I
    :cond_228
    const/16 v22, 0x1

    #@22a
    move/from16 v0, v22

    #@22c
    new-array v0, v0, [I

    #@22e
    move-object/from16 v22, v0

    #@230
    const/16 v23, 0x0

    #@232
    aput v17, v22, v23

    #@234
    move-object/from16 v0, p2

    #@236
    move-object/from16 v1, v22

    #@238
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    #@23b
    goto/16 :goto_c7

    #@23d
    .line 274
    .end local v17           #valueFrom:I
    :cond_23d
    if-eqz v10, :cond_c7

    #@23f
    .line 275
    const/16 v22, 0x5

    #@241
    move/from16 v0, v22

    #@243
    if-ne v14, v0, :cond_269

    #@245
    .line 276
    const/16 v22, 0x0

    #@247
    move/from16 v0, v20

    #@249
    move/from16 v1, v22

    #@24b
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@24e
    move-result v22

    #@24f
    move/from16 v0, v22

    #@251
    float-to-int v0, v0

    #@252
    move/from16 v19, v0

    #@254
    .line 283
    .restart local v19       #valueTo:I
    :goto_254
    const/16 v22, 0x1

    #@256
    move/from16 v0, v22

    #@258
    new-array v0, v0, [I

    #@25a
    move-object/from16 v22, v0

    #@25c
    const/16 v23, 0x0

    #@25e
    aput v19, v22, v23

    #@260
    move-object/from16 v0, p2

    #@262
    move-object/from16 v1, v22

    #@264
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    #@267
    goto/16 :goto_c7

    #@269
    .line 277
    .end local v19           #valueTo:I
    :cond_269
    const/16 v22, 0x1c

    #@26b
    move/from16 v0, v22

    #@26d
    if-lt v14, v0, :cond_280

    #@26f
    const/16 v22, 0x1f

    #@271
    move/from16 v0, v22

    #@273
    if-gt v14, v0, :cond_280

    #@275
    .line 279
    const/16 v22, 0x0

    #@277
    move/from16 v0, v20

    #@279
    move/from16 v1, v22

    #@27b
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@27e
    move-result v19

    #@27f
    .restart local v19       #valueTo:I
    goto :goto_254

    #@280
    .line 281
    .end local v19           #valueTo:I
    :cond_280
    const/16 v22, 0x0

    #@282
    move/from16 v0, v20

    #@284
    move/from16 v1, v22

    #@286
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@289
    move-result v19

    #@28a
    .restart local v19       #valueTo:I
    goto :goto_254
.end method

.method private static loadObjectAnimator(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/animation/ObjectAnimator;
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 159
    new-instance v1, Landroid/animation/ObjectAnimator;

    #@2
    invoke-direct {v1}, Landroid/animation/ObjectAnimator;-><init>()V

    #@5
    .line 161
    .local v1, anim:Landroid/animation/ObjectAnimator;
    invoke-static {p0, p1, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    #@8
    .line 163
    sget-object v3, Lcom/android/internal/R$styleable;->PropertyAnimator:[I

    #@a
    invoke-virtual {p0, p1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@d
    move-result-object v0

    #@e
    .line 166
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x0

    #@f
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 168
    .local v2, propertyName:Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    #@16
    .line 170
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@19
    .line 172
    return-object v1
.end method
