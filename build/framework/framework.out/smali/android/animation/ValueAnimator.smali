.class public Landroid/animation/ValueAnimator;
.super Landroid/animation/Animator;
.source "ValueAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/ValueAnimator$1;,
        Landroid/animation/ValueAnimator$AnimatorUpdateListener;,
        Landroid/animation/ValueAnimator$AnimationHandler;
    }
.end annotation


# static fields
.field public static final INFINITE:I = -0x1

.field public static final RESTART:I = 0x1

.field public static final REVERSE:I = 0x2

.field static final RUNNING:I = 0x1

.field static final SEEKED:I = 0x2

.field static final STOPPED:I

.field private static sAnimationHandler:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/animation/ValueAnimator$AnimationHandler;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultInterpolator:Landroid/animation/TimeInterpolator;

.field private static sDurationScale:F


# instance fields
.field private mCurrentFraction:F

.field private mCurrentIteration:I

.field private mDelayStartTime:J

.field private mDuration:J

.field mInitialized:Z

.field private mInterpolator:Landroid/animation/TimeInterpolator;

.field private mPlayingBackwards:Z

.field mPlayingState:I

.field private mRepeatCount:I

.field private mRepeatMode:I

.field private mRunning:Z

.field mSeekTime:J

.field private mStartDelay:J

.field private mStartListenersCalled:Z

.field mStartTime:J

.field private mStarted:Z

.field private mStartedDelay:Z

.field private mUnscaledDuration:J

.field private mUnscaledStartDelay:J

.field private mUpdateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field mValues:[Landroid/animation/PropertyValuesHolder;

.field mValuesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/animation/PropertyValuesHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 56
    const/high16 v0, 0x3f80

    #@2
    sput v0, Landroid/animation/ValueAnimator;->sDurationScale:F

    #@4
    .line 86
    new-instance v0, Ljava/lang/ThreadLocal;

    #@6
    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    #@9
    sput-object v0, Landroid/animation/ValueAnimator;->sAnimationHandler:Ljava/lang/ThreadLocal;

    #@b
    .line 90
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@d
    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@10
    sput-object v0, Landroid/animation/ValueAnimator;->sDefaultInterpolator:Landroid/animation/TimeInterpolator;

    #@12
    return-void
.end method

.method public constructor <init>()V
    .registers 6

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 246
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    #@6
    .line 82
    const-wide/16 v0, -0x1

    #@8
    iput-wide v0, p0, Landroid/animation/ValueAnimator;->mSeekTime:J

    #@a
    .line 97
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@c
    .line 103
    iput v2, p0, Landroid/animation/ValueAnimator;->mCurrentIteration:I

    #@e
    .line 108
    const/4 v0, 0x0

    #@f
    iput v0, p0, Landroid/animation/ValueAnimator;->mCurrentFraction:F

    #@11
    .line 113
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mStartedDelay:Z

    #@13
    .line 129
    iput v2, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@15
    .line 139
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@17
    .line 145
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@19
    .line 152
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mStartListenersCalled:Z

    #@1b
    .line 158
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@1d
    .line 165
    const/high16 v0, 0x4396

    #@1f
    sget v1, Landroid/animation/ValueAnimator;->sDurationScale:F

    #@21
    mul-float/2addr v0, v1

    #@22
    float-to-long v0, v0

    #@23
    iput-wide v0, p0, Landroid/animation/ValueAnimator;->mDuration:J

    #@25
    .line 166
    const-wide/16 v0, 0x12c

    #@27
    iput-wide v0, p0, Landroid/animation/ValueAnimator;->mUnscaledDuration:J

    #@29
    .line 169
    iput-wide v3, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@2b
    .line 170
    iput-wide v3, p0, Landroid/animation/ValueAnimator;->mUnscaledStartDelay:J

    #@2d
    .line 174
    iput v2, p0, Landroid/animation/ValueAnimator;->mRepeatCount:I

    #@2f
    .line 181
    const/4 v0, 0x1

    #@30
    iput v0, p0, Landroid/animation/ValueAnimator;->mRepeatMode:I

    #@32
    .line 188
    sget-object v0, Landroid/animation/ValueAnimator;->sDefaultInterpolator:Landroid/animation/TimeInterpolator;

    #@34
    iput-object v0, p0, Landroid/animation/ValueAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@36
    .line 193
    const/4 v0, 0x0

    #@37
    iput-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@39
    .line 247
    return-void
.end method

.method static synthetic access$000(Landroid/animation/ValueAnimator;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 51
    iget-wide v0, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@2
    return-wide v0
.end method

.method static synthetic access$100(Landroid/animation/ValueAnimator;Landroid/animation/ValueAnimator$AnimationHandler;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/animation/ValueAnimator;->startAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/animation/ValueAnimator;J)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/animation/ValueAnimator;->delayedAnimationFrame(J)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$302(Landroid/animation/ValueAnimator;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    iput-boolean p1, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/animation/ValueAnimator;Landroid/animation/ValueAnimator$AnimationHandler;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/animation/ValueAnimator;->endAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@3
    return-void
.end method

.method public static clearAllAnimations()V
    .registers 2

    #@0
    .prologue
    .line 1238
    sget-object v1, Landroid/animation/ValueAnimator;->sAnimationHandler:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/animation/ValueAnimator$AnimationHandler;

    #@8
    .line 1239
    .local v0, handler:Landroid/animation/ValueAnimator$AnimationHandler;
    if-eqz v0, :cond_1f

    #@a
    .line 1240
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$700(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@11
    .line 1241
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$500(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@18
    .line 1242
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$600(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@1f
    .line 1244
    :cond_1f
    return-void
.end method

.method private delayedAnimationFrame(J)Z
    .registers 8
    .parameter "currentTime"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1045
    iget-boolean v3, p0, Landroid/animation/ValueAnimator;->mStartedDelay:Z

    #@3
    if-nez v3, :cond_b

    #@5
    .line 1046
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mStartedDelay:Z

    #@7
    .line 1047
    iput-wide p1, p0, Landroid/animation/ValueAnimator;->mDelayStartTime:J

    #@9
    .line 1058
    :cond_9
    const/4 v2, 0x0

    #@a
    :goto_a
    return v2

    #@b
    .line 1049
    :cond_b
    iget-wide v3, p0, Landroid/animation/ValueAnimator;->mDelayStartTime:J

    #@d
    sub-long v0, p1, v3

    #@f
    .line 1050
    .local v0, deltaTime:J
    iget-wide v3, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@11
    cmp-long v3, v0, v3

    #@13
    if-lez v3, :cond_9

    #@15
    .line 1053
    iget-wide v3, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@17
    sub-long v3, v0, v3

    #@19
    sub-long v3, p1, v3

    #@1b
    iput-wide v3, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@1d
    .line 1054
    iput v2, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@1f
    goto :goto_a
.end method

.method private endAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V
    .registers 7
    .parameter "handler"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 999
    invoke-static {p1}, Landroid/animation/ValueAnimator$AnimationHandler;->access$700(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 1000
    invoke-static {p1}, Landroid/animation/ValueAnimator$AnimationHandler;->access$500(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@f
    .line 1001
    invoke-static {p1}, Landroid/animation/ValueAnimator$AnimationHandler;->access$600(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@16
    .line 1002
    iput v4, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@18
    .line 1003
    iget-boolean v3, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@1a
    if-nez v3, :cond_20

    #@1c
    iget-boolean v3, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@1e
    if-eqz v3, :cond_46

    #@20
    :cond_20
    iget-object v3, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@22
    if-eqz v3, :cond_46

    #@24
    .line 1004
    iget-boolean v3, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@26
    if-nez v3, :cond_2b

    #@28
    .line 1006
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->notifyStartListeners()V

    #@2b
    .line 1008
    :cond_2b
    iget-object v3, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@30
    move-result-object v2

    #@31
    check-cast v2, Ljava/util/ArrayList;

    #@33
    .line 1010
    .local v2, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@36
    move-result v1

    #@37
    .line 1011
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@38
    .local v0, i:I
    :goto_38
    if-ge v0, v1, :cond_46

    #@3a
    .line 1012
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3d
    move-result-object v3

    #@3e
    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    #@40
    invoke-interface {v3, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@43
    .line 1011
    add-int/lit8 v0, v0, 0x1

    #@45
    goto :goto_38

    #@46
    .line 1015
    .end local v0           #i:I
    .end local v1           #numListeners:I
    .end local v2           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_46
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@48
    .line 1016
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@4a
    .line 1017
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mStartListenersCalled:Z

    #@4c
    .line 1018
    return-void
.end method

.method public static getCurrentAnimationsCount()I
    .registers 2

    #@0
    .prologue
    .line 1227
    sget-object v1, Landroid/animation/ValueAnimator;->sAnimationHandler:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/animation/ValueAnimator$AnimationHandler;

    #@8
    .line 1228
    .local v0, handler:Landroid/animation/ValueAnimator$AnimationHandler;
    if-eqz v0, :cond_13

    #@a
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$700(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v1

    #@12
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public static getDurationScale()F
    .registers 1

    #@0
    .prologue
    .line 238
    sget v0, Landroid/animation/ValueAnimator;->sDurationScale:F

    #@2
    return v0
.end method

.method public static getFrameDelay()J
    .registers 2

    #@0
    .prologue
    .line 684
    invoke-static {}, Landroid/view/Choreographer;->getFrameDelay()J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method private getOrCreateAnimationHandler()Landroid/animation/ValueAnimator$AnimationHandler;
    .registers 3

    #@0
    .prologue
    .line 1247
    sget-object v1, Landroid/animation/ValueAnimator;->sAnimationHandler:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/animation/ValueAnimator$AnimationHandler;

    #@8
    .line 1248
    .local v0, handler:Landroid/animation/ValueAnimator$AnimationHandler;
    if-nez v0, :cond_15

    #@a
    .line 1249
    new-instance v0, Landroid/animation/ValueAnimator$AnimationHandler;

    #@c
    .end local v0           #handler:Landroid/animation/ValueAnimator$AnimationHandler;
    const/4 v1, 0x0

    #@d
    invoke-direct {v0, v1}, Landroid/animation/ValueAnimator$AnimationHandler;-><init>(Landroid/animation/ValueAnimator$1;)V

    #@10
    .line 1250
    .restart local v0       #handler:Landroid/animation/ValueAnimator$AnimationHandler;
    sget-object v1, Landroid/animation/ValueAnimator;->sAnimationHandler:Ljava/lang/ThreadLocal;

    #@12
    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@15
    .line 1252
    :cond_15
    return-object v0
.end method

.method private notifyStartListeners()V
    .registers 5

    #@0
    .prologue
    .line 876
    iget-object v3, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v3, :cond_23

    #@4
    iget-boolean v3, p0, Landroid/animation/ValueAnimator;->mStartListenersCalled:Z

    #@6
    if-nez v3, :cond_23

    #@8
    .line 877
    iget-object v3, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Ljava/util/ArrayList;

    #@10
    .line 879
    .local v2, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v1

    #@14
    .line 880
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    if-ge v0, v1, :cond_23

    #@17
    .line 881
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v3

    #@1b
    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    #@1d
    invoke-interface {v3, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    #@20
    .line 880
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_15

    #@23
    .line 884
    .end local v0           #i:I
    .end local v1           #numListeners:I
    .end local v2           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_23
    const/4 v3, 0x1

    #@24
    iput-boolean v3, p0, Landroid/animation/ValueAnimator;->mStartListenersCalled:Z

    #@26
    .line 885
    return-void
.end method

.method public static varargs ofFloat([F)Landroid/animation/ValueAnimator;
    .registers 2
    .parameter "values"

    #@0
    .prologue
    .line 278
    new-instance v0, Landroid/animation/ValueAnimator;

    #@2
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    #@5
    .line 279
    .local v0, anim:Landroid/animation/ValueAnimator;
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    #@8
    .line 280
    return-object v0
.end method

.method public static varargs ofInt([I)Landroid/animation/ValueAnimator;
    .registers 2
    .parameter "values"

    #@0
    .prologue
    .line 261
    new-instance v0, Landroid/animation/ValueAnimator;

    #@2
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    #@5
    .line 262
    .local v0, anim:Landroid/animation/ValueAnimator;
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    #@8
    .line 263
    return-object v0
.end method

.method public static varargs ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;
    .registers 3
    .parameter "evaluator"
    .parameter "values"

    #@0
    .prologue
    .line 315
    new-instance v0, Landroid/animation/ValueAnimator;

    #@2
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    #@5
    .line 316
    .local v0, anim:Landroid/animation/ValueAnimator;
    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    #@8
    .line 317
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@b
    .line 318
    return-object v0
.end method

.method public static varargs ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;
    .registers 2
    .parameter "values"

    #@0
    .prologue
    .line 292
    new-instance v0, Landroid/animation/ValueAnimator;

    #@2
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    #@5
    .line 293
    .local v0, anim:Landroid/animation/ValueAnimator;
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@8
    .line 294
    return-object v0
.end method

.method public static setDurationScale(F)V
    .registers 1
    .parameter "durationScale"

    #@0
    .prologue
    .line 231
    sput p0, Landroid/animation/ValueAnimator;->sDurationScale:F

    #@2
    .line 232
    return-void
.end method

.method public static setFrameDelay(J)V
    .registers 2
    .parameter "frameDelay"

    #@0
    .prologue
    .line 700
    invoke-static {p0, p1}, Landroid/view/Choreographer;->setFrameDelay(J)V

    #@3
    .line 701
    return-void
.end method

.method private start(Z)V
    .registers 9
    .parameter "playBackwards"

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 901
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 902
    new-instance v1, Landroid/util/AndroidRuntimeException;

    #@c
    const-string v2, "Animators may only be run on Looper threads"

    #@e
    invoke-direct {v1, v2}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v1

    #@12
    .line 904
    :cond_12
    iput-boolean p1, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@14
    .line 905
    iput v3, p0, Landroid/animation/ValueAnimator;->mCurrentIteration:I

    #@16
    .line 906
    iput v3, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@18
    .line 907
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@1a
    .line 908
    iput-boolean v3, p0, Landroid/animation/ValueAnimator;->mStartedDelay:Z

    #@1c
    .line 909
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->getOrCreateAnimationHandler()Landroid/animation/ValueAnimator$AnimationHandler;

    #@1f
    move-result-object v0

    #@20
    .line 910
    .local v0, animationHandler:Landroid/animation/ValueAnimator$AnimationHandler;
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$500(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 911
    iget-wide v1, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@29
    cmp-long v1, v1, v5

    #@2b
    if-nez v1, :cond_37

    #@2d
    .line 913
    invoke-virtual {p0, v5, v6}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    #@30
    .line 914
    iput v3, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@32
    .line 915
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@34
    .line 916
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->notifyStartListeners()V

    #@37
    .line 918
    :cond_37
    invoke-virtual {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->start()V

    #@3a
    .line 919
    return-void
.end method

.method private startAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V
    .registers 6
    .parameter "handler"

    #@0
    .prologue
    .line 1025
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->initAnimation()V

    #@3
    .line 1026
    invoke-static {p1}, Landroid/animation/ValueAnimator$AnimationHandler;->access$700(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 1027
    iget-wide v0, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@c
    const-wide/16 v2, 0x0

    #@e
    cmp-long v0, v0, v2

    #@10
    if-lez v0, :cond_19

    #@12
    iget-object v0, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 1030
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->notifyStartListeners()V

    #@19
    .line 1032
    :cond_19
    return-void
.end method


# virtual methods
.method public addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 792
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 793
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@b
    .line 795
    :cond_b
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 796
    return-void
.end method

.method animateValue(F)V
    .registers 6
    .parameter "fraction"

    #@0
    .prologue
    .line 1157
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@2
    invoke-interface {v3, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@5
    move-result p1

    #@6
    .line 1158
    iput p1, p0, Landroid/animation/ValueAnimator;->mCurrentFraction:F

    #@8
    .line 1159
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@a
    array-length v2, v3

    #@b
    .line 1160
    .local v2, numValues:I
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    if-ge v0, v2, :cond_18

    #@e
    .line 1161
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@10
    aget-object v3, v3, v0

    #@12
    invoke-virtual {v3, p1}, Landroid/animation/PropertyValuesHolder;->calculateValue(F)V

    #@15
    .line 1160
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_c

    #@18
    .line 1163
    :cond_18
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@1a
    if-eqz v3, :cond_33

    #@1c
    .line 1164
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@21
    move-result v1

    #@22
    .line 1165
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@23
    :goto_23
    if-ge v0, v1, :cond_33

    #@25
    .line 1166
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v3

    #@2b
    check-cast v3, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@2d
    invoke-interface {v3, p0}, Landroid/animation/ValueAnimator$AnimatorUpdateListener;->onAnimationUpdate(Landroid/animation/ValueAnimator;)V

    #@30
    .line 1165
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_23

    #@33
    .line 1169
    .end local v1           #numListeners:I
    :cond_33
    return-void
.end method

.method animationFrame(J)Z
    .registers 13
    .parameter "currentTime"

    #@0
    .prologue
    const/high16 v5, 0x3f80

    #@2
    .line 1074
    const/4 v0, 0x0

    #@3
    .line 1075
    .local v0, done:Z
    iget v4, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@5
    packed-switch v4, :pswitch_data_74

    #@8
    .line 1106
    :goto_8
    return v0

    #@9
    .line 1078
    :pswitch_9
    iget-wide v6, p0, Landroid/animation/ValueAnimator;->mDuration:J

    #@b
    const-wide/16 v8, 0x0

    #@d
    cmp-long v4, v6, v8

    #@f
    if-lez v4, :cond_45

    #@11
    iget-wide v6, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@13
    sub-long v6, p1, v6

    #@15
    long-to-float v4, v6

    #@16
    iget-wide v6, p0, Landroid/animation/ValueAnimator;->mDuration:J

    #@18
    long-to-float v6, v6

    #@19
    div-float v1, v4, v6

    #@1b
    .line 1079
    .local v1, fraction:F
    :goto_1b
    cmpl-float v4, v1, v5

    #@1d
    if-ltz v4, :cond_61

    #@1f
    .line 1080
    iget v4, p0, Landroid/animation/ValueAnimator;->mCurrentIteration:I

    #@21
    iget v6, p0, Landroid/animation/ValueAnimator;->mRepeatCount:I

    #@23
    if-lt v4, v6, :cond_2a

    #@25
    iget v4, p0, Landroid/animation/ValueAnimator;->mRepeatCount:I

    #@27
    const/4 v6, -0x1

    #@28
    if-ne v4, v6, :cond_6d

    #@2a
    .line 1082
    :cond_2a
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@2c
    if-eqz v4, :cond_47

    #@2e
    .line 1083
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v3

    #@34
    .line 1084
    .local v3, numListeners:I
    const/4 v2, 0x0

    #@35
    .local v2, i:I
    :goto_35
    if-ge v2, v3, :cond_47

    #@37
    .line 1085
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@39
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    check-cast v4, Landroid/animation/Animator$AnimatorListener;

    #@3f
    invoke-interface {v4, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    #@42
    .line 1084
    add-int/lit8 v2, v2, 0x1

    #@44
    goto :goto_35

    #@45
    .end local v1           #fraction:F
    .end local v2           #i:I
    .end local v3           #numListeners:I
    :cond_45
    move v1, v5

    #@46
    .line 1078
    goto :goto_1b

    #@47
    .line 1088
    .restart local v1       #fraction:F
    :cond_47
    iget v4, p0, Landroid/animation/ValueAnimator;->mRepeatMode:I

    #@49
    const/4 v6, 0x2

    #@4a
    if-ne v4, v6, :cond_53

    #@4c
    .line 1089
    iget-boolean v4, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@4e
    if-eqz v4, :cond_6b

    #@50
    const/4 v4, 0x0

    #@51
    :goto_51
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@53
    .line 1091
    :cond_53
    iget v4, p0, Landroid/animation/ValueAnimator;->mCurrentIteration:I

    #@55
    float-to-int v6, v1

    #@56
    add-int/2addr v4, v6

    #@57
    iput v4, p0, Landroid/animation/ValueAnimator;->mCurrentIteration:I

    #@59
    .line 1092
    rem-float/2addr v1, v5

    #@5a
    .line 1093
    iget-wide v6, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@5c
    iget-wide v8, p0, Landroid/animation/ValueAnimator;->mDuration:J

    #@5e
    add-long/2addr v6, v8

    #@5f
    iput-wide v6, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@61
    .line 1099
    :cond_61
    :goto_61
    iget-boolean v4, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@63
    if-eqz v4, :cond_67

    #@65
    .line 1100
    sub-float v1, v5, v1

    #@67
    .line 1102
    :cond_67
    invoke-virtual {p0, v1}, Landroid/animation/ValueAnimator;->animateValue(F)V

    #@6a
    goto :goto_8

    #@6b
    .line 1089
    :cond_6b
    const/4 v4, 0x1

    #@6c
    goto :goto_51

    #@6d
    .line 1095
    :cond_6d
    const/4 v0, 0x1

    #@6e
    .line 1096
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    #@71
    move-result v1

    #@72
    goto :goto_61

    #@73
    .line 1075
    nop

    #@74
    :pswitch_data_74
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method public cancel()V
    .registers 6

    #@0
    .prologue
    .line 930
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->getOrCreateAnimationHandler()Landroid/animation/ValueAnimator$AnimationHandler;

    #@3
    move-result-object v0

    #@4
    .line 931
    .local v0, handler:Landroid/animation/ValueAnimator$AnimationHandler;
    iget v4, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@6
    if-nez v4, :cond_1c

    #@8
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$500(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v4, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_1c

    #@12
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$600(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_4e

    #@1c
    .line 935
    :cond_1c
    iget-boolean v4, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@1e
    if-nez v4, :cond_24

    #@20
    iget-boolean v4, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@22
    if-eqz v4, :cond_4b

    #@24
    :cond_24
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@26
    if-eqz v4, :cond_4b

    #@28
    .line 936
    iget-boolean v4, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@2a
    if-nez v4, :cond_2f

    #@2c
    .line 938
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->notifyStartListeners()V

    #@2f
    .line 940
    :cond_2f
    iget-object v4, p0, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Ljava/util/ArrayList;

    #@37
    .line 942
    .local v3, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3a
    move-result-object v1

    #@3b
    .local v1, i$:Ljava/util/Iterator;
    :goto_3b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3e
    move-result v4

    #@3f
    if-eqz v4, :cond_4b

    #@41
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@44
    move-result-object v2

    #@45
    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    #@47
    .line 943
    .local v2, listener:Landroid/animation/Animator$AnimatorListener;
    invoke-interface {v2, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    #@4a
    goto :goto_3b

    #@4b
    .line 946
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #listener:Landroid/animation/Animator$AnimatorListener;
    .end local v3           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_4b
    invoke-direct {p0, v0}, Landroid/animation/ValueAnimator;->endAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@4e
    .line 948
    :cond_4e
    return-void
.end method

.method public bridge synthetic clone()Landroid/animation/Animator;
    .registers 2

    #@0
    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->clone()Landroid/animation/ValueAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public clone()Landroid/animation/ValueAnimator;
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1173
    invoke-super {p0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    #@4
    move-result-object v0

    #@5
    check-cast v0, Landroid/animation/ValueAnimator;

    #@7
    .line 1174
    .local v0, anim:Landroid/animation/ValueAnimator;
    iget-object v7, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@9
    if-eqz v7, :cond_27

    #@b
    .line 1175
    iget-object v5, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@d
    .line 1176
    .local v5, oldListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/ValueAnimator$AnimatorUpdateListener;>;"
    new-instance v7, Ljava/util/ArrayList;

    #@f
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v7, v0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@14
    .line 1177
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v3

    #@18
    .line 1178
    .local v3, numListeners:I
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    if-ge v1, v3, :cond_27

    #@1b
    .line 1179
    iget-object v7, v0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 1178
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_19

    #@27
    .line 1182
    .end local v1           #i:I
    .end local v3           #numListeners:I
    .end local v5           #oldListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/ValueAnimator$AnimatorUpdateListener;>;"
    :cond_27
    const-wide/16 v7, -0x1

    #@29
    iput-wide v7, v0, Landroid/animation/ValueAnimator;->mSeekTime:J

    #@2b
    .line 1183
    iput-boolean v9, v0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@2d
    .line 1184
    iput v9, v0, Landroid/animation/ValueAnimator;->mCurrentIteration:I

    #@2f
    .line 1185
    iput-boolean v9, v0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@31
    .line 1186
    iput v9, v0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@33
    .line 1187
    iput-boolean v9, v0, Landroid/animation/ValueAnimator;->mStartedDelay:Z

    #@35
    .line 1188
    iget-object v6, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@37
    .line 1189
    .local v6, oldValues:[Landroid/animation/PropertyValuesHolder;
    if-eqz v6, :cond_5e

    #@39
    .line 1190
    array-length v4, v6

    #@3a
    .line 1191
    .local v4, numValues:I
    new-array v7, v4, [Landroid/animation/PropertyValuesHolder;

    #@3c
    iput-object v7, v0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@3e
    .line 1192
    new-instance v7, Ljava/util/HashMap;

    #@40
    invoke-direct {v7, v4}, Ljava/util/HashMap;-><init>(I)V

    #@43
    iput-object v7, v0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@45
    .line 1193
    const/4 v1, 0x0

    #@46
    .restart local v1       #i:I
    :goto_46
    if-ge v1, v4, :cond_5e

    #@48
    .line 1194
    aget-object v7, v6, v1

    #@4a
    invoke-virtual {v7}, Landroid/animation/PropertyValuesHolder;->clone()Landroid/animation/PropertyValuesHolder;

    #@4d
    move-result-object v2

    #@4e
    .line 1195
    .local v2, newValuesHolder:Landroid/animation/PropertyValuesHolder;
    iget-object v7, v0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@50
    aput-object v2, v7, v1

    #@52
    .line 1196
    iget-object v7, v0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@54
    invoke-virtual {v2}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    #@57
    move-result-object v8

    #@58
    invoke-virtual {v7, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 1193
    add-int/lit8 v1, v1, 0x1

    #@5d
    goto :goto_46

    #@5e
    .line 1199
    .end local v1           #i:I
    .end local v2           #newValuesHolder:Landroid/animation/PropertyValuesHolder;
    .end local v4           #numValues:I
    :cond_5e
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->clone()Landroid/animation/ValueAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method final doAnimationFrame(J)Z
    .registers 9
    .parameter "frameTime"

    #@0
    .prologue
    .line 1116
    iget v2, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@2
    if-nez v2, :cond_11

    #@4
    .line 1117
    const/4 v2, 0x1

    #@5
    iput v2, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@7
    .line 1118
    iget-wide v2, p0, Landroid/animation/ValueAnimator;->mSeekTime:J

    #@9
    const-wide/16 v4, 0x0

    #@b
    cmp-long v2, v2, v4

    #@d
    if-gez v2, :cond_1c

    #@f
    .line 1119
    iput-wide p1, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@11
    .line 1130
    :cond_11
    :goto_11
    iget-wide v2, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@13
    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@16
    move-result-wide v0

    #@17
    .line 1131
    .local v0, currentTime:J
    invoke-virtual {p0, v0, v1}, Landroid/animation/ValueAnimator;->animationFrame(J)Z

    #@1a
    move-result v2

    #@1b
    return v2

    #@1c
    .line 1121
    .end local v0           #currentTime:J
    :cond_1c
    iget-wide v2, p0, Landroid/animation/ValueAnimator;->mSeekTime:J

    #@1e
    sub-long v2, p1, v2

    #@20
    iput-wide v2, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@22
    .line 1123
    const-wide/16 v2, -0x1

    #@24
    iput-wide v2, p0, Landroid/animation/ValueAnimator;->mSeekTime:J

    #@26
    goto :goto_11
.end method

.method public end()V
    .registers 3

    #@0
    .prologue
    .line 952
    invoke-direct {p0}, Landroid/animation/ValueAnimator;->getOrCreateAnimationHandler()Landroid/animation/ValueAnimator$AnimationHandler;

    #@3
    move-result-object v0

    #@4
    .line 953
    .local v0, handler:Landroid/animation/ValueAnimator$AnimationHandler;
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$700(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_2d

    #@e
    invoke-static {v0}, Landroid/animation/ValueAnimator$AnimationHandler;->access$500(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_2d

    #@18
    .line 955
    const/4 v1, 0x0

    #@19
    iput-boolean v1, p0, Landroid/animation/ValueAnimator;->mStartedDelay:Z

    #@1b
    .line 956
    invoke-direct {p0, v0}, Landroid/animation/ValueAnimator;->startAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@1e
    .line 957
    const/4 v1, 0x1

    #@1f
    iput-boolean v1, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@21
    .line 961
    :cond_21
    :goto_21
    iget-boolean v1, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@23
    if-eqz v1, :cond_35

    #@25
    const/4 v1, 0x0

    #@26
    :goto_26
    invoke-virtual {p0, v1}, Landroid/animation/ValueAnimator;->animateValue(F)V

    #@29
    .line 962
    invoke-direct {p0, v0}, Landroid/animation/ValueAnimator;->endAnimation(Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@2c
    .line 963
    return-void

    #@2d
    .line 958
    :cond_2d
    iget-boolean v1, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@2f
    if-nez v1, :cond_21

    #@31
    .line 959
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->initAnimation()V

    #@34
    goto :goto_21

    #@35
    .line 961
    :cond_35
    const/high16 v1, 0x3f80

    #@37
    goto :goto_26
.end method

.method public getAnimatedFraction()F
    .registers 2

    #@0
    .prologue
    .line 1141
    iget v0, p0, Landroid/animation/ValueAnimator;->mCurrentFraction:F

    #@2
    return v0
.end method

.method public getAnimatedValue()Ljava/lang/Object;
    .registers 3

    #@0
    .prologue
    .line 716
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@2
    if-eqz v0, :cond_13

    #@4
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@6
    array-length v0, v0

    #@7
    if-lez v0, :cond_13

    #@9
    .line 717
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@b
    const/4 v1, 0x0

    #@c
    aget-object v0, v0, v1

    #@e
    invoke-virtual {v0}, Landroid/animation/PropertyValuesHolder;->getAnimatedValue()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    .line 720
    :goto_12
    return-object v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .parameter "propertyName"

    #@0
    .prologue
    .line 734
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/animation/PropertyValuesHolder;

    #@8
    .line 735
    .local v0, valuesHolder:Landroid/animation/PropertyValuesHolder;
    if-eqz v0, :cond_f

    #@a
    .line 736
    invoke-virtual {v0}, Landroid/animation/PropertyValuesHolder;->getAnimatedValue()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    .line 739
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getCurrentPlayTime()J
    .registers 5

    #@0
    .prologue
    .line 520
    iget-boolean v0, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    iget v0, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@6
    if-nez v0, :cond_b

    #@8
    .line 521
    :cond_8
    const-wide/16 v0, 0x0

    #@a
    .line 523
    :goto_a
    return-wide v0

    #@b
    :cond_b
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@e
    move-result-wide v0

    #@f
    iget-wide v2, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@11
    sub-long/2addr v0, v2

    #@12
    goto :goto_a
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 488
    iget-wide v0, p0, Landroid/animation/ValueAnimator;->mUnscaledDuration:J

    #@2
    return-wide v0
.end method

.method public getInterpolator()Landroid/animation/TimeInterpolator;
    .registers 2

    #@0
    .prologue
    .line 850
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@2
    return-object v0
.end method

.method public getRepeatCount()I
    .registers 2

    #@0
    .prologue
    .line 761
    iget v0, p0, Landroid/animation/ValueAnimator;->mRepeatCount:I

    #@2
    return v0
.end method

.method public getRepeatMode()I
    .registers 2

    #@0
    .prologue
    .line 781
    iget v0, p0, Landroid/animation/ValueAnimator;->mRepeatMode:I

    #@2
    return v0
.end method

.method public getStartDelay()J
    .registers 3

    #@0
    .prologue
    .line 657
    iget-wide v0, p0, Landroid/animation/ValueAnimator;->mUnscaledStartDelay:J

    #@2
    return-wide v0
.end method

.method public getValues()[Landroid/animation/PropertyValuesHolder;
    .registers 2

    #@0
    .prologue
    .line 439
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@2
    return-object v0
.end method

.method initAnimation()V
    .registers 4

    #@0
    .prologue
    .line 453
    iget-boolean v2, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@2
    if-nez v2, :cond_17

    #@4
    .line 454
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@6
    array-length v1, v2

    #@7
    .line 455
    .local v1, numValues:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_14

    #@a
    .line 456
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@c
    aget-object v2, v2, v0

    #@e
    invoke-virtual {v2}, Landroid/animation/PropertyValuesHolder;->init()V

    #@11
    .line 455
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_8

    #@14
    .line 458
    :cond_14
    const/4 v2, 0x1

    #@15
    iput-boolean v2, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@17
    .line 460
    .end local v0           #i:I
    .end local v1           #numValues:I
    :cond_17
    return-void
.end method

.method public isRunning()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 967
    iget v1, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@3
    if-eq v1, v0, :cond_9

    #@5
    iget-boolean v1, p0, Landroid/animation/ValueAnimator;->mRunning:Z

    #@7
    if-eqz v1, :cond_a

    #@9
    :cond_9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isStarted()Z
    .registers 2

    #@0
    .prologue
    .line 972
    iget-boolean v0, p0, Landroid/animation/ValueAnimator;->mStarted:Z

    #@2
    return v0
.end method

.method public removeAllUpdateListeners()V
    .registers 2

    #@0
    .prologue
    .line 802
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 807
    :goto_4
    return-void

    #@5
    .line 805
    :cond_5
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 806
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@d
    goto :goto_4
.end method

.method public removeUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 816
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 823
    :cond_4
    :goto_4
    return-void

    #@5
    .line 819
    :cond_5
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a
    .line 820
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_4

    #@12
    .line 821
    const/4 v0, 0x0

    #@13
    iput-object v0, p0, Landroid/animation/ValueAnimator;->mUpdateListeners:Ljava/util/ArrayList;

    #@15
    goto :goto_4
.end method

.method public reverse()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 983
    iget-boolean v6, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@3
    if-nez v6, :cond_1d

    #@5
    move v6, v7

    #@6
    :goto_6
    iput-boolean v6, p0, Landroid/animation/ValueAnimator;->mPlayingBackwards:Z

    #@8
    .line 984
    iget v6, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@a
    if-ne v6, v7, :cond_1f

    #@c
    .line 985
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@f
    move-result-wide v2

    #@10
    .line 986
    .local v2, currentTime:J
    iget-wide v6, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@12
    sub-long v0, v2, v6

    #@14
    .line 987
    .local v0, currentPlayTime:J
    iget-wide v6, p0, Landroid/animation/ValueAnimator;->mDuration:J

    #@16
    sub-long v4, v6, v0

    #@18
    .line 988
    .local v4, timeLeft:J
    sub-long v6, v2, v4

    #@1a
    iput-wide v6, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@1c
    .line 992
    .end local v0           #currentPlayTime:J
    .end local v2           #currentTime:J
    .end local v4           #timeLeft:J
    :goto_1c
    return-void

    #@1d
    .line 983
    :cond_1d
    const/4 v6, 0x0

    #@1e
    goto :goto_6

    #@1f
    .line 990
    :cond_1f
    invoke-direct {p0, v7}, Landroid/animation/ValueAnimator;->start(Z)V

    #@22
    goto :goto_1c
.end method

.method public setCurrentPlayTime(J)V
    .registers 7
    .parameter "playTime"

    #@0
    .prologue
    .line 502
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->initAnimation()V

    #@3
    .line 503
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@6
    move-result-wide v0

    #@7
    .line 504
    .local v0, currentTime:J
    iget v2, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@9
    const/4 v3, 0x1

    #@a
    if-eq v2, v3, :cond_11

    #@c
    .line 505
    iput-wide p1, p0, Landroid/animation/ValueAnimator;->mSeekTime:J

    #@e
    .line 506
    const/4 v2, 0x2

    #@f
    iput v2, p0, Landroid/animation/ValueAnimator;->mPlayingState:I

    #@11
    .line 508
    :cond_11
    sub-long v2, v0, p1

    #@13
    iput-wide v2, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@15
    .line 509
    invoke-virtual {p0, v0, v1}, Landroid/animation/ValueAnimator;->doAnimationFrame(J)Z

    #@18
    .line 510
    return-void
.end method

.method public bridge synthetic setDuration(J)Landroid/animation/Animator;
    .registers 4
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-virtual {p0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public setDuration(J)Landroid/animation/ValueAnimator;
    .registers 6
    .parameter "duration"

    #@0
    .prologue
    .line 473
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_1f

    #@6
    .line 474
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Animators cannot have negative duration: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 477
    :cond_1f
    iput-wide p1, p0, Landroid/animation/ValueAnimator;->mUnscaledDuration:J

    #@21
    .line 478
    long-to-float v0, p1

    #@22
    sget v1, Landroid/animation/ValueAnimator;->sDurationScale:F

    #@24
    mul-float/2addr v0, v1

    #@25
    float-to-long v0, v0

    #@26
    iput-wide v0, p0, Landroid/animation/ValueAnimator;->mDuration:J

    #@28
    .line 479
    return-object p0
.end method

.method public setEvaluator(Landroid/animation/TypeEvaluator;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 870
    if-eqz p1, :cond_13

    #@2
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@4
    if-eqz v0, :cond_13

    #@6
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@8
    array-length v0, v0

    #@9
    if-lez v0, :cond_13

    #@b
    .line 871
    iget-object v0, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@d
    const/4 v1, 0x0

    #@e
    aget-object v0, v0, v1

    #@10
    invoke-virtual {v0, p1}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@13
    .line 873
    :cond_13
    return-void
.end method

.method public varargs setFloatValues([F)V
    .registers 6
    .parameter "values"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 364
    if-eqz p1, :cond_6

    #@3
    array-length v1, p1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 375
    :cond_6
    :goto_6
    return-void

    #@7
    .line 367
    :cond_7
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@9
    if-eqz v1, :cond_10

    #@b
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@d
    array-length v1, v1

    #@e
    if-nez v1, :cond_21

    #@10
    .line 368
    :cond_10
    const/4 v1, 0x1

    #@11
    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    #@13
    const-string v2, ""

    #@15
    invoke-static {v2, p1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@18
    move-result-object v2

    #@19
    aput-object v2, v1, v3

    #@1b
    invoke-virtual {p0, v1}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@1e
    .line 374
    :goto_1e
    iput-boolean v3, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@20
    goto :goto_6

    #@21
    .line 370
    :cond_21
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@23
    aget-object v0, v1, v3

    #@25
    .line 371
    .local v0, valuesHolder:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v0, p1}, Landroid/animation/PropertyValuesHolder;->setFloatValues([F)V

    #@28
    goto :goto_1e
.end method

.method public varargs setIntValues([I)V
    .registers 6
    .parameter "values"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 336
    if-eqz p1, :cond_6

    #@3
    array-length v1, p1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 347
    :cond_6
    :goto_6
    return-void

    #@7
    .line 339
    :cond_7
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@9
    if-eqz v1, :cond_10

    #@b
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@d
    array-length v1, v1

    #@e
    if-nez v1, :cond_21

    #@10
    .line 340
    :cond_10
    const/4 v1, 0x1

    #@11
    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    #@13
    const-string v2, ""

    #@15
    invoke-static {v2, p1}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@18
    move-result-object v2

    #@19
    aput-object v2, v1, v3

    #@1b
    invoke-virtual {p0, v1}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@1e
    .line 346
    :goto_1e
    iput-boolean v3, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@20
    goto :goto_6

    #@21
    .line 342
    :cond_21
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@23
    aget-object v0, v1, v3

    #@25
    .line 343
    .local v0, valuesHolder:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v0, p1}, Landroid/animation/PropertyValuesHolder;->setIntValues([I)V

    #@28
    goto :goto_1e
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 837
    if-eqz p1, :cond_5

    #@2
    .line 838
    iput-object p1, p0, Landroid/animation/ValueAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@4
    .line 842
    :goto_4
    return-void

    #@5
    .line 840
    :cond_5
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    #@7
    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@a
    iput-object v0, p0, Landroid/animation/ValueAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@c
    goto :goto_4
.end method

.method public varargs setObjectValues([Ljava/lang/Object;)V
    .registers 7
    .parameter "values"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 396
    if-eqz p1, :cond_6

    #@3
    array-length v1, p1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 408
    :cond_6
    :goto_6
    return-void

    #@7
    .line 399
    :cond_7
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@9
    if-eqz v1, :cond_10

    #@b
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@d
    array-length v1, v1

    #@e
    if-nez v1, :cond_24

    #@10
    .line 400
    :cond_10
    const/4 v1, 0x1

    #@11
    new-array v2, v1, [Landroid/animation/PropertyValuesHolder;

    #@13
    const-string v3, ""

    #@15
    const/4 v1, 0x0

    #@16
    check-cast v1, Landroid/animation/TypeEvaluator;

    #@18
    invoke-static {v3, v1, p1}, Landroid/animation/PropertyValuesHolder;->ofObject(Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;

    #@1b
    move-result-object v1

    #@1c
    aput-object v1, v2, v4

    #@1e
    invoke-virtual {p0, v2}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@21
    .line 407
    :goto_21
    iput-boolean v4, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@23
    goto :goto_6

    #@24
    .line 403
    :cond_24
    iget-object v1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@26
    aget-object v0, v1, v4

    #@28
    .line 404
    .local v0, valuesHolder:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v0, p1}, Landroid/animation/PropertyValuesHolder;->setObjectValues([Ljava/lang/Object;)V

    #@2b
    goto :goto_21
.end method

.method public setRepeatCount(I)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 752
    iput p1, p0, Landroid/animation/ValueAnimator;->mRepeatCount:I

    #@2
    .line 753
    return-void
.end method

.method public setRepeatMode(I)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 772
    iput p1, p0, Landroid/animation/ValueAnimator;->mRepeatMode:I

    #@2
    .line 773
    return-void
.end method

.method public setStartDelay(J)V
    .registers 5
    .parameter "startDelay"

    #@0
    .prologue
    .line 667
    long-to-float v0, p1

    #@1
    sget v1, Landroid/animation/ValueAnimator;->sDurationScale:F

    #@3
    mul-float/2addr v0, v1

    #@4
    float-to-long v0, v0

    #@5
    iput-wide v0, p0, Landroid/animation/ValueAnimator;->mStartDelay:J

    #@7
    .line 668
    iput-wide p1, p0, Landroid/animation/ValueAnimator;->mUnscaledStartDelay:J

    #@9
    .line 669
    return-void
.end method

.method public varargs setValues([Landroid/animation/PropertyValuesHolder;)V
    .registers 7
    .parameter "values"

    #@0
    .prologue
    .line 419
    array-length v1, p1

    #@1
    .line 420
    .local v1, numValues:I
    iput-object p1, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@3
    .line 421
    new-instance v3, Ljava/util/HashMap;

    #@5
    invoke-direct {v3, v1}, Ljava/util/HashMap;-><init>(I)V

    #@8
    iput-object v3, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@a
    .line 422
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v1, :cond_1b

    #@d
    .line 423
    aget-object v2, p1, v0

    #@f
    .line 424
    .local v2, valuesHolder:Landroid/animation/PropertyValuesHolder;
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mValuesMap:Ljava/util/HashMap;

    #@11
    invoke-virtual {v2}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 422
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_b

    #@1b
    .line 427
    .end local v2           #valuesHolder:Landroid/animation/PropertyValuesHolder;
    :cond_1b
    const/4 v3, 0x0

    #@1c
    iput-boolean v3, p0, Landroid/animation/ValueAnimator;->mInitialized:Z

    #@1e
    .line 428
    return-void
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 923
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/animation/ValueAnimator;->start(Z)V

    #@4
    .line 924
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1257
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "ValueAnimator@"

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@e
    move-result v3

    #@f
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 1258
    .local v1, returnVal:Ljava/lang/String;
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@1d
    if-eqz v2, :cond_47

    #@1f
    .line 1259
    const/4 v0, 0x0

    #@20
    .local v0, i:I
    :goto_20
    iget-object v2, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@22
    array-length v2, v2

    #@23
    if-ge v0, v2, :cond_47

    #@25
    .line 1260
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, "\n    "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    iget-object v3, p0, Landroid/animation/ValueAnimator;->mValues:[Landroid/animation/PropertyValuesHolder;

    #@36
    aget-object v3, v3, v0

    #@38
    invoke-virtual {v3}, Landroid/animation/PropertyValuesHolder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    .line 1259
    add-int/lit8 v0, v0, 0x1

    #@46
    goto :goto_20

    #@47
    .line 1263
    .end local v0           #i:I
    :cond_47
    return-object v1
.end method
