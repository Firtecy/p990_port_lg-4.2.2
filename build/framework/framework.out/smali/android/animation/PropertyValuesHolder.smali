.class public Landroid/animation/PropertyValuesHolder;
.super Ljava/lang/Object;
.source "PropertyValuesHolder.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/PropertyValuesHolder$1;,
        Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;,
        Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;
    }
.end annotation


# static fields
.field private static DOUBLE_VARIANTS:[Ljava/lang/Class;

.field private static FLOAT_VARIANTS:[Ljava/lang/Class;

.field private static INTEGER_VARIANTS:[Ljava/lang/Class;

.field private static final sFloatEvaluator:Landroid/animation/TypeEvaluator;

.field private static final sGetterPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sIntEvaluator:Landroid/animation/TypeEvaluator;

.field private static final sSetterPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mAnimatedValue:Ljava/lang/Object;

.field private mEvaluator:Landroid/animation/TypeEvaluator;

.field private mGetter:Ljava/lang/reflect/Method;

.field mKeyframeSet:Landroid/animation/KeyframeSet;

.field protected mProperty:Landroid/util/Property;

.field final mPropertyMapLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field mPropertyName:Ljava/lang/String;

.field mSetter:Ljava/lang/reflect/Method;

.field final mTmpValueArray:[Ljava/lang/Object;

.field mValueType:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 79
    new-instance v0, Landroid/animation/IntEvaluator;

    #@7
    invoke-direct {v0}, Landroid/animation/IntEvaluator;-><init>()V

    #@a
    sput-object v0, Landroid/animation/PropertyValuesHolder;->sIntEvaluator:Landroid/animation/TypeEvaluator;

    #@c
    .line 80
    new-instance v0, Landroid/animation/FloatEvaluator;

    #@e
    invoke-direct {v0}, Landroid/animation/FloatEvaluator;-><init>()V

    #@11
    sput-object v0, Landroid/animation/PropertyValuesHolder;->sFloatEvaluator:Landroid/animation/TypeEvaluator;

    #@13
    .line 89
    const/4 v0, 0x6

    #@14
    new-array v0, v0, [Ljava/lang/Class;

    #@16
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@18
    aput-object v1, v0, v3

    #@1a
    const-class v1, Ljava/lang/Float;

    #@1c
    aput-object v1, v0, v4

    #@1e
    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    #@20
    aput-object v1, v0, v5

    #@22
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@24
    aput-object v1, v0, v6

    #@26
    const-class v1, Ljava/lang/Double;

    #@28
    aput-object v1, v0, v7

    #@2a
    const/4 v1, 0x5

    #@2b
    const-class v2, Ljava/lang/Integer;

    #@2d
    aput-object v2, v0, v1

    #@2f
    sput-object v0, Landroid/animation/PropertyValuesHolder;->FLOAT_VARIANTS:[Ljava/lang/Class;

    #@31
    .line 91
    const/4 v0, 0x6

    #@32
    new-array v0, v0, [Ljava/lang/Class;

    #@34
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@36
    aput-object v1, v0, v3

    #@38
    const-class v1, Ljava/lang/Integer;

    #@3a
    aput-object v1, v0, v4

    #@3c
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@3e
    aput-object v1, v0, v5

    #@40
    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    #@42
    aput-object v1, v0, v6

    #@44
    const-class v1, Ljava/lang/Float;

    #@46
    aput-object v1, v0, v7

    #@48
    const/4 v1, 0x5

    #@49
    const-class v2, Ljava/lang/Double;

    #@4b
    aput-object v2, v0, v1

    #@4d
    sput-object v0, Landroid/animation/PropertyValuesHolder;->INTEGER_VARIANTS:[Ljava/lang/Class;

    #@4f
    .line 93
    const/4 v0, 0x6

    #@50
    new-array v0, v0, [Ljava/lang/Class;

    #@52
    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    #@54
    aput-object v1, v0, v3

    #@56
    const-class v1, Ljava/lang/Double;

    #@58
    aput-object v1, v0, v4

    #@5a
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@5c
    aput-object v1, v0, v5

    #@5e
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@60
    aput-object v1, v0, v6

    #@62
    const-class v1, Ljava/lang/Float;

    #@64
    aput-object v1, v0, v7

    #@66
    const/4 v1, 0x5

    #@67
    const-class v2, Ljava/lang/Integer;

    #@69
    aput-object v2, v0, v1

    #@6b
    sput-object v0, Landroid/animation/PropertyValuesHolder;->DOUBLE_VARIANTS:[Ljava/lang/Class;

    #@6d
    .line 99
    new-instance v0, Ljava/util/HashMap;

    #@6f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@72
    sput-object v0, Landroid/animation/PropertyValuesHolder;->sSetterPropertyMap:Ljava/util/HashMap;

    #@74
    .line 101
    new-instance v0, Ljava/util/HashMap;

    #@76
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@79
    sput-object v0, Landroid/animation/PropertyValuesHolder;->sGetterPropertyMap:Ljava/util/HashMap;

    #@7b
    return-void
.end method

.method private constructor <init>(Landroid/util/Property;)V
    .registers 3
    .parameter "property"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 138
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 55
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mSetter:Ljava/lang/reflect/Method;

    #@6
    .line 64
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@8
    .line 75
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@a
    .line 106
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@c
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    #@f
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mPropertyMapLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@11
    .line 109
    const/4 v0, 0x1

    #@12
    new-array v0, v0, [Ljava/lang/Object;

    #@14
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mTmpValueArray:[Ljava/lang/Object;

    #@16
    .line 139
    iput-object p1, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@18
    .line 140
    if-eqz p1, :cond_20

    #@1a
    .line 141
    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@20
    .line 143
    :cond_20
    return-void
.end method

.method synthetic constructor <init>(Landroid/util/Property;Landroid/animation/PropertyValuesHolder$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/animation/PropertyValuesHolder;-><init>(Landroid/util/Property;)V

    #@3
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "propertyName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 130
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 55
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mSetter:Ljava/lang/reflect/Method;

    #@6
    .line 64
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@8
    .line 75
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@a
    .line 106
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@c
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    #@f
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mPropertyMapLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@11
    .line 109
    const/4 v0, 0x1

    #@12
    new-array v0, v0, [Ljava/lang/Object;

    #@14
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mTmpValueArray:[Ljava/lang/Object;

    #@16
    .line 131
    iput-object p1, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@18
    .line 132
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/animation/PropertyValuesHolder$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/animation/PropertyValuesHolder;-><init>(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Ljava/lang/Object;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Landroid/animation/PropertyValuesHolder;->nCallIntMethod(Ljava/lang/Object;II)V

    #@3
    return-void
.end method

.method static synthetic access$300(Ljava/lang/Class;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-static {p0, p1}, Landroid/animation/PropertyValuesHolder;->nGetIntMethod(Ljava/lang/Class;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Ljava/lang/Object;IF)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Landroid/animation/PropertyValuesHolder;->nCallFloatMethod(Ljava/lang/Object;IF)V

    #@3
    return-void
.end method

.method static synthetic access$500(Ljava/lang/Class;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-static {p0, p1}, Landroid/animation/PropertyValuesHolder;->nGetFloatMethod(Ljava/lang/Class;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "prefix"
    .parameter "propertyName"

    #@0
    .prologue
    .line 734
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 740
    .end local p0
    :cond_8
    :goto_8
    return-object p0

    #@9
    .line 738
    .restart local p0
    :cond_9
    const/4 v2, 0x0

    #@a
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v2

    #@e
    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    #@11
    move-result v0

    #@12
    .line 739
    .local v0, firstLetter:C
    const/4 v2, 0x1

    #@13
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    .line 740
    .local v1, theRest:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object p0

    #@2c
    goto :goto_8
.end method

.method private getPropertyFunction(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .registers 16
    .parameter "targetClass"
    .parameter "prefix"
    .parameter "valueType"

    #@0
    .prologue
    .line 380
    const/4 v5, 0x0

    #@1
    .line 381
    .local v5, returnVal:Ljava/lang/reflect/Method;
    iget-object v9, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@3
    invoke-static {p2, v9}, Landroid/animation/PropertyValuesHolder;->getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    .line 382
    .local v4, methodName:Ljava/lang/String;
    const/4 v0, 0x0

    #@8
    .line 383
    .local v0, args:[Ljava/lang/Class;
    if-nez p3, :cond_46

    #@a
    .line 385
    :try_start_a
    invoke-virtual {p1, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_d
    .catch Ljava/lang/NoSuchMethodException; {:try_start_a .. :try_end_d} :catch_8e

    #@d
    move-result-object v5

    #@e
    .line 416
    :cond_e
    :goto_e
    if-nez v5, :cond_44

    #@10
    .line 417
    const-string v9, "PropertyValuesHolder"

    #@12
    new-instance v10, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v11, "Method "

    #@19
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    iget-object v11, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@1f
    invoke-static {p2, v11}, Landroid/animation/PropertyValuesHolder;->getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v11

    #@23
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v10

    #@27
    const-string v11, "() with type "

    #@29
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v10

    #@2d
    iget-object v11, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@2f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v10

    #@33
    const-string v11, " not found on target class "

    #@35
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v10

    #@39
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v10

    #@3d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v10

    #@41
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    :cond_44
    move-object v6, v5

    #@45
    .line 422
    .end local v5           #returnVal:Ljava/lang/reflect/Method;
    .local v6, returnVal:Ljava/lang/reflect/Method;
    :goto_45
    return-object v6

    #@46
    .line 390
    .end local v6           #returnVal:Ljava/lang/reflect/Method;
    .restart local v5       #returnVal:Ljava/lang/reflect/Method;
    :cond_46
    const/4 v9, 0x1

    #@47
    new-array v0, v9, [Ljava/lang/Class;

    #@49
    .line 392
    iget-object v9, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@4b
    const-class v10, Ljava/lang/Float;

    #@4d
    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v9

    #@51
    if-eqz v9, :cond_67

    #@53
    .line 393
    sget-object v8, Landroid/animation/PropertyValuesHolder;->FLOAT_VARIANTS:[Ljava/lang/Class;

    #@55
    .line 402
    .local v8, typeVariants:[Ljava/lang/Class;
    :goto_55
    move-object v1, v8

    #@56
    .local v1, arr$:[Ljava/lang/Class;
    array-length v3, v1

    #@57
    .local v3, len$:I
    const/4 v2, 0x0

    #@58
    .local v2, i$:I
    :goto_58
    if-ge v2, v3, :cond_e

    #@5a
    aget-object v7, v1, v2

    #@5c
    .line 403
    .local v7, typeVariant:Ljava/lang/Class;
    const/4 v9, 0x0

    #@5d
    aput-object v7, v0, v9

    #@5f
    .line 405
    :try_start_5f
    invoke-virtual {p1, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@62
    move-result-object v5

    #@63
    .line 407
    iput-object v7, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;
    :try_end_65
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5f .. :try_end_65} :catch_8a

    #@65
    move-object v6, v5

    #@66
    .line 408
    .end local v5           #returnVal:Ljava/lang/reflect/Method;
    .restart local v6       #returnVal:Ljava/lang/reflect/Method;
    goto :goto_45

    #@67
    .line 394
    .end local v1           #arr$:[Ljava/lang/Class;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v6           #returnVal:Ljava/lang/reflect/Method;
    .end local v7           #typeVariant:Ljava/lang/Class;
    .end local v8           #typeVariants:[Ljava/lang/Class;
    .restart local v5       #returnVal:Ljava/lang/reflect/Method;
    :cond_67
    iget-object v9, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@69
    const-class v10, Ljava/lang/Integer;

    #@6b
    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v9

    #@6f
    if-eqz v9, :cond_74

    #@71
    .line 395
    sget-object v8, Landroid/animation/PropertyValuesHolder;->INTEGER_VARIANTS:[Ljava/lang/Class;

    #@73
    .restart local v8       #typeVariants:[Ljava/lang/Class;
    goto :goto_55

    #@74
    .line 396
    .end local v8           #typeVariants:[Ljava/lang/Class;
    :cond_74
    iget-object v9, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@76
    const-class v10, Ljava/lang/Double;

    #@78
    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@7b
    move-result v9

    #@7c
    if-eqz v9, :cond_81

    #@7e
    .line 397
    sget-object v8, Landroid/animation/PropertyValuesHolder;->DOUBLE_VARIANTS:[Ljava/lang/Class;

    #@80
    .restart local v8       #typeVariants:[Ljava/lang/Class;
    goto :goto_55

    #@81
    .line 399
    .end local v8           #typeVariants:[Ljava/lang/Class;
    :cond_81
    const/4 v9, 0x1

    #@82
    new-array v8, v9, [Ljava/lang/Class;

    #@84
    .line 400
    .restart local v8       #typeVariants:[Ljava/lang/Class;
    const/4 v9, 0x0

    #@85
    iget-object v10, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@87
    aput-object v10, v8, v9

    #@89
    goto :goto_55

    #@8a
    .line 409
    .restart local v1       #arr$:[Ljava/lang/Class;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v7       #typeVariant:Ljava/lang/Class;
    :catch_8a
    move-exception v9

    #@8b
    .line 402
    add-int/lit8 v2, v2, 0x1

    #@8d
    goto :goto_58

    #@8e
    .line 386
    .end local v1           #arr$:[Ljava/lang/Class;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v7           #typeVariant:Ljava/lang/Class;
    .end local v8           #typeVariants:[Ljava/lang/Class;
    :catch_8e
    move-exception v9

    #@8f
    goto/16 :goto_e
.end method

.method private static native nCallFloatMethod(Ljava/lang/Object;IF)V
.end method

.method private static native nCallIntMethod(Ljava/lang/Object;II)V
.end method

.method private static native nGetFloatMethod(Ljava/lang/Class;Ljava/lang/String;)I
.end method

.method private static native nGetIntMethod(Ljava/lang/Class;Ljava/lang/String;)I
.end method

.method public static varargs ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;
    .registers 3
    .parameter
    .parameter "values"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Property",
            "<*",
            "Ljava/lang/Float;",
            ">;[F)",
            "Landroid/animation/PropertyValuesHolder;"
        }
    .end annotation

    #@0
    .prologue
    .line 186
    .local p0, property:Landroid/util/Property;,"Landroid/util/Property<*Ljava/lang/Float;>;"
    new-instance v0, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;-><init>(Landroid/util/Property;[F)V

    #@5
    return-object v0
.end method

.method public static varargs ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;
    .registers 3
    .parameter "propertyName"
    .parameter "values"

    #@0
    .prologue
    .line 175
    new-instance v0, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;-><init>(Ljava/lang/String;[F)V

    #@5
    return-object v0
.end method

.method public static varargs ofInt(Landroid/util/Property;[I)Landroid/animation/PropertyValuesHolder;
    .registers 3
    .parameter
    .parameter "values"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Property",
            "<*",
            "Ljava/lang/Integer;",
            ">;[I)",
            "Landroid/animation/PropertyValuesHolder;"
        }
    .end annotation

    #@0
    .prologue
    .line 164
    .local p0, property:Landroid/util/Property;,"Landroid/util/Property<*Ljava/lang/Integer;>;"
    new-instance v0, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;-><init>(Landroid/util/Property;[I)V

    #@5
    return-object v0
.end method

.method public static varargs ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;
    .registers 3
    .parameter "propertyName"
    .parameter "values"

    #@0
    .prologue
    .line 153
    new-instance v0, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;-><init>(Ljava/lang/String;[I)V

    #@5
    return-object v0
.end method

.method public static varargs ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;
    .registers 5
    .parameter "property"
    .parameter "values"

    #@0
    .prologue
    .line 280
    invoke-static {p1}, Landroid/animation/KeyframeSet;->ofKeyframe([Landroid/animation/Keyframe;)Landroid/animation/KeyframeSet;

    #@3
    move-result-object v0

    #@4
    .line 281
    .local v0, keyframeSet:Landroid/animation/KeyframeSet;
    instance-of v2, v0, Landroid/animation/IntKeyframeSet;

    #@6
    if-eqz v2, :cond_10

    #@8
    .line 282
    new-instance v1, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;

    #@a
    check-cast v0, Landroid/animation/IntKeyframeSet;

    #@c
    .end local v0           #keyframeSet:Landroid/animation/KeyframeSet;
    invoke-direct {v1, p0, v0}, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;-><init>(Landroid/util/Property;Landroid/animation/IntKeyframeSet;)V

    #@f
    .line 290
    :goto_f
    return-object v1

    #@10
    .line 283
    .restart local v0       #keyframeSet:Landroid/animation/KeyframeSet;
    :cond_10
    instance-of v2, v0, Landroid/animation/FloatKeyframeSet;

    #@12
    if-eqz v2, :cond_1c

    #@14
    .line 284
    new-instance v1, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;

    #@16
    check-cast v0, Landroid/animation/FloatKeyframeSet;

    #@18
    .end local v0           #keyframeSet:Landroid/animation/KeyframeSet;
    invoke-direct {v1, p0, v0}, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;-><init>(Landroid/util/Property;Landroid/animation/FloatKeyframeSet;)V

    #@1b
    goto :goto_f

    #@1c
    .line 287
    .restart local v0       #keyframeSet:Landroid/animation/KeyframeSet;
    :cond_1c
    new-instance v1, Landroid/animation/PropertyValuesHolder;

    #@1e
    invoke-direct {v1, p0}, Landroid/animation/PropertyValuesHolder;-><init>(Landroid/util/Property;)V

    #@21
    .line 288
    .local v1, pvh:Landroid/animation/PropertyValuesHolder;
    iput-object v0, v1, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@23
    .line 289
    const/4 v2, 0x0

    #@24
    aget-object v2, p1, v2

    #@26
    invoke-virtual {v2}, Landroid/animation/Keyframe;->getType()Ljava/lang/Class;

    #@29
    move-result-object v2

    #@2a
    iput-object v2, v1, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@2c
    goto :goto_f
.end method

.method public static varargs ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;
    .registers 5
    .parameter "propertyName"
    .parameter "values"

    #@0
    .prologue
    .line 249
    invoke-static {p1}, Landroid/animation/KeyframeSet;->ofKeyframe([Landroid/animation/Keyframe;)Landroid/animation/KeyframeSet;

    #@3
    move-result-object v0

    #@4
    .line 250
    .local v0, keyframeSet:Landroid/animation/KeyframeSet;
    instance-of v2, v0, Landroid/animation/IntKeyframeSet;

    #@6
    if-eqz v2, :cond_10

    #@8
    .line 251
    new-instance v1, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;

    #@a
    check-cast v0, Landroid/animation/IntKeyframeSet;

    #@c
    .end local v0           #keyframeSet:Landroid/animation/KeyframeSet;
    invoke-direct {v1, p0, v0}, Landroid/animation/PropertyValuesHolder$IntPropertyValuesHolder;-><init>(Ljava/lang/String;Landroid/animation/IntKeyframeSet;)V

    #@f
    .line 259
    :goto_f
    return-object v1

    #@10
    .line 252
    .restart local v0       #keyframeSet:Landroid/animation/KeyframeSet;
    :cond_10
    instance-of v2, v0, Landroid/animation/FloatKeyframeSet;

    #@12
    if-eqz v2, :cond_1c

    #@14
    .line 253
    new-instance v1, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;

    #@16
    check-cast v0, Landroid/animation/FloatKeyframeSet;

    #@18
    .end local v0           #keyframeSet:Landroid/animation/KeyframeSet;
    invoke-direct {v1, p0, v0}, Landroid/animation/PropertyValuesHolder$FloatPropertyValuesHolder;-><init>(Ljava/lang/String;Landroid/animation/FloatKeyframeSet;)V

    #@1b
    goto :goto_f

    #@1c
    .line 256
    .restart local v0       #keyframeSet:Landroid/animation/KeyframeSet;
    :cond_1c
    new-instance v1, Landroid/animation/PropertyValuesHolder;

    #@1e
    invoke-direct {v1, p0}, Landroid/animation/PropertyValuesHolder;-><init>(Ljava/lang/String;)V

    #@21
    .line 257
    .local v1, pvh:Landroid/animation/PropertyValuesHolder;
    iput-object v0, v1, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@23
    .line 258
    const/4 v2, 0x0

    #@24
    aget-object v2, p1, v2

    #@26
    invoke-virtual {v2}, Landroid/animation/Keyframe;->getType()Ljava/lang/Class;

    #@29
    move-result-object v2

    #@2a
    iput-object v2, v1, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@2c
    goto :goto_f
.end method

.method public static varargs ofObject(Landroid/util/Property;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;
    .registers 4
    .parameter "property"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/Property;",
            "Landroid/animation/TypeEvaluator",
            "<TV;>;[TV;)",
            "Landroid/animation/PropertyValuesHolder;"
        }
    .end annotation

    #@0
    .prologue
    .line 223
    .local p1, evaluator:Landroid/animation/TypeEvaluator;,"Landroid/animation/TypeEvaluator<TV;>;"
    .local p2, values:[Ljava/lang/Object;,"[TV;"
    new-instance v0, Landroid/animation/PropertyValuesHolder;

    #@2
    invoke-direct {v0, p0}, Landroid/animation/PropertyValuesHolder;-><init>(Landroid/util/Property;)V

    #@5
    .line 224
    .local v0, pvh:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v0, p2}, Landroid/animation/PropertyValuesHolder;->setObjectValues([Ljava/lang/Object;)V

    #@8
    .line 225
    invoke-virtual {v0, p1}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@b
    .line 226
    return-object v0
.end method

.method public static varargs ofObject(Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/PropertyValuesHolder;
    .registers 4
    .parameter "propertyName"
    .parameter "evaluator"
    .parameter "values"

    #@0
    .prologue
    .line 203
    new-instance v0, Landroid/animation/PropertyValuesHolder;

    #@2
    invoke-direct {v0, p0}, Landroid/animation/PropertyValuesHolder;-><init>(Ljava/lang/String;)V

    #@5
    .line 204
    .local v0, pvh:Landroid/animation/PropertyValuesHolder;
    invoke-virtual {v0, p2}, Landroid/animation/PropertyValuesHolder;->setObjectValues([Ljava/lang/Object;)V

    #@8
    .line 205
    invoke-virtual {v0, p1}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@b
    .line 206
    return-object v0
.end method

.method private setupGetter(Ljava/lang/Class;)V
    .registers 5
    .parameter "targetClass"

    #@0
    .prologue
    .line 475
    sget-object v0, Landroid/animation/PropertyValuesHolder;->sGetterPropertyMap:Ljava/util/HashMap;

    #@2
    const-string v1, "get"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/animation/PropertyValuesHolder;->setupSetterOrGetter(Ljava/lang/Class;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@b
    .line 476
    return-void
.end method

.method private setupSetterOrGetter(Ljava/lang/Class;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .registers 10
    .parameter "targetClass"
    .parameter
    .parameter "prefix"
    .parameter "valueType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/Class;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    #@0
    .prologue
    .line 439
    .local p2, propertyMapMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Class;Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    const/4 v2, 0x0

    #@1
    .line 444
    .local v2, setterOrGetter:Ljava/lang/reflect/Method;
    :try_start_1
    iget-object v3, p0, Landroid/animation/PropertyValuesHolder;->mPropertyMapLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@3
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    #@a
    .line 445
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Ljava/util/HashMap;

    #@10
    .line 446
    .local v1, propertyMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    if-eqz v1, :cond_1c

    #@12
    .line 447
    iget-object v3, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@14
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    move-object v0, v3

    #@19
    check-cast v0, Ljava/lang/reflect/Method;

    #@1b
    move-object v2, v0

    #@1c
    .line 449
    :cond_1c
    if-nez v2, :cond_31

    #@1e
    .line 450
    invoke-direct {p0, p1, p3, p4}, Landroid/animation/PropertyValuesHolder;->getPropertyFunction(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@21
    move-result-object v2

    #@22
    .line 451
    if-nez v1, :cond_2c

    #@24
    .line 452
    new-instance v1, Ljava/util/HashMap;

    #@26
    .end local v1           #propertyMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@29
    .line 453
    .restart local v1       #propertyMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    invoke-virtual {p2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 455
    :cond_2c
    iget-object v3, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@2e
    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catchall {:try_start_1 .. :try_end_31} :catchall_3b

    #@31
    .line 458
    :cond_31
    iget-object v3, p0, Landroid/animation/PropertyValuesHolder;->mPropertyMapLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@33
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    #@3a
    .line 460
    return-object v2

    #@3b
    .line 458
    .end local v1           #propertyMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    :catchall_3b
    move-exception v3

    #@3c
    iget-object v4, p0, Landroid/animation/PropertyValuesHolder;->mPropertyMapLock:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    #@3e
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    #@45
    throw v3
.end method

.method private setupValue(Ljava/lang/Object;Landroid/animation/Keyframe;)V
    .registers 7
    .parameter "target"
    .parameter "kf"

    #@0
    .prologue
    .line 537
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@2
    if-eqz v2, :cond_d

    #@4
    .line 538
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@6
    invoke-virtual {v2, p1}, Landroid/util/Property;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {p2, v2}, Landroid/animation/Keyframe;->setValue(Ljava/lang/Object;)V

    #@d
    .line 541
    :cond_d
    :try_start_d
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@f
    if-nez v2, :cond_1d

    #@11
    .line 542
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@14
    move-result-object v1

    #@15
    .line 543
    .local v1, targetClass:Ljava/lang/Class;
    invoke-direct {p0, v1}, Landroid/animation/PropertyValuesHolder;->setupGetter(Ljava/lang/Class;)V

    #@18
    .line 544
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@1a
    if-nez v2, :cond_1d

    #@1c
    .line 555
    .end local v1           #targetClass:Ljava/lang/Class;
    :goto_1c
    return-void

    #@1d
    .line 549
    :cond_1d
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@1f
    const/4 v3, 0x0

    #@20
    new-array v3, v3, [Ljava/lang/Object;

    #@22
    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {p2, v2}, Landroid/animation/Keyframe;->setValue(Ljava/lang/Object;)V
    :try_end_29
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_d .. :try_end_29} :catch_2a
    .catch Ljava/lang/IllegalAccessException; {:try_start_d .. :try_end_29} :catch_35

    #@29
    goto :goto_1c

    #@2a
    .line 550
    :catch_2a
    move-exception v0

    #@2b
    .line 551
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "PropertyValuesHolder"

    #@2d
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_1c

    #@35
    .line 552
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_35
    move-exception v0

    #@36
    .line 553
    .local v0, e:Ljava/lang/IllegalAccessException;
    const-string v2, "PropertyValuesHolder"

    #@38
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_1c
.end method


# virtual methods
.method calculateValue(F)V
    .registers 3
    .parameter "fraction"

    #@0
    .prologue
    .line 660
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@2
    invoke-virtual {v0, p1}, Landroid/animation/KeyframeSet;->getValue(F)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mAnimatedValue:Ljava/lang/Object;

    #@8
    .line 661
    return-void
.end method

.method public clone()Landroid/animation/PropertyValuesHolder;
    .registers 4

    #@0
    .prologue
    .line 584
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@3
    move-result-object v1

    #@4
    check-cast v1, Landroid/animation/PropertyValuesHolder;

    #@6
    .line 585
    .local v1, newPVH:Landroid/animation/PropertyValuesHolder;
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@8
    iput-object v2, v1, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@a
    .line 586
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@c
    iput-object v2, v1, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@e
    .line 587
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@10
    invoke-virtual {v2}, Landroid/animation/KeyframeSet;->clone()Landroid/animation/KeyframeSet;

    #@13
    move-result-object v2

    #@14
    iput-object v2, v1, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@16
    .line 588
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@18
    iput-object v2, v1, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;
    :try_end_1a
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_1a} :catch_1b

    #@1a
    .line 592
    .end local v1           #newPVH:Landroid/animation/PropertyValuesHolder;
    :goto_1a
    return-object v1

    #@1b
    .line 590
    :catch_1b
    move-exception v0

    #@1c
    .line 592
    .local v0, e:Ljava/lang/CloneNotSupportedException;
    const/4 v1, 0x0

    #@1d
    goto :goto_1a
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    invoke-virtual {p0}, Landroid/animation/PropertyValuesHolder;->clone()Landroid/animation/PropertyValuesHolder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method getAnimatedValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 712
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mAnimatedValue:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getPropertyName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 703
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method init()V
    .registers 3

    #@0
    .prologue
    .line 624
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 627
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@6
    const-class v1, Ljava/lang/Integer;

    #@8
    if-ne v0, v1, :cond_1a

    #@a
    sget-object v0, Landroid/animation/PropertyValuesHolder;->sIntEvaluator:Landroid/animation/TypeEvaluator;

    #@c
    :goto_c
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@e
    .line 631
    :cond_e
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@10
    if-eqz v0, :cond_19

    #@12
    .line 634
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@14
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@16
    invoke-virtual {v0, v1}, Landroid/animation/KeyframeSet;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@19
    .line 636
    :cond_19
    return-void

    #@1a
    .line 627
    :cond_1a
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@1c
    const-class v1, Ljava/lang/Float;

    #@1e
    if-ne v0, v1, :cond_23

    #@20
    sget-object v0, Landroid/animation/PropertyValuesHolder;->sFloatEvaluator:Landroid/animation/TypeEvaluator;

    #@22
    goto :goto_c

    #@23
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_c
.end method

.method setAnimatedValue(Ljava/lang/Object;)V
    .registers 6
    .parameter "target"

    #@0
    .prologue
    .line 604
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 605
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@6
    invoke-virtual {p0}, Landroid/animation/PropertyValuesHolder;->getAnimatedValue()Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v1, p1, v2}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@d
    .line 607
    :cond_d
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mSetter:Ljava/lang/reflect/Method;

    #@f
    if-eqz v1, :cond_21

    #@11
    .line 609
    :try_start_11
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mTmpValueArray:[Ljava/lang/Object;

    #@13
    const/4 v2, 0x0

    #@14
    invoke-virtual {p0}, Landroid/animation/PropertyValuesHolder;->getAnimatedValue()Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v1, v2

    #@1a
    .line 610
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mSetter:Ljava/lang/reflect/Method;

    #@1c
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mTmpValueArray:[Ljava/lang/Object;

    #@1e
    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_21
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_11 .. :try_end_21} :catch_22
    .catch Ljava/lang/IllegalAccessException; {:try_start_11 .. :try_end_21} :catch_2d

    #@21
    .line 617
    :cond_21
    :goto_21
    return-void

    #@22
    .line 611
    :catch_22
    move-exception v0

    #@23
    .line 612
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "PropertyValuesHolder"

    #@25
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_21

    #@2d
    .line 613
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_2d
    move-exception v0

    #@2e
    .line 614
    .local v0, e:Ljava/lang/IllegalAccessException;
    const-string v1, "PropertyValuesHolder"

    #@30
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_21
.end method

.method public setEvaluator(Landroid/animation/TypeEvaluator;)V
    .registers 3
    .parameter "evaluator"

    #@0
    .prologue
    .line 649
    iput-object p1, p0, Landroid/animation/PropertyValuesHolder;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@2
    .line 650
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@4
    invoke-virtual {v0, p1}, Landroid/animation/KeyframeSet;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    #@7
    .line 651
    return-void
.end method

.method public varargs setFloatValues([F)V
    .registers 3
    .parameter "values"

    #@0
    .prologue
    .line 326
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@2
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@4
    .line 327
    invoke-static {p1}, Landroid/animation/KeyframeSet;->ofFloat([F)Landroid/animation/KeyframeSet;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@a
    .line 328
    return-void
.end method

.method public varargs setIntValues([I)V
    .registers 3
    .parameter "values"

    #@0
    .prologue
    .line 308
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@2
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@4
    .line 309
    invoke-static {p1}, Landroid/animation/KeyframeSet;->ofInt([I)Landroid/animation/KeyframeSet;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@a
    .line 310
    return-void
.end method

.method public varargs setKeyframes([Landroid/animation/Keyframe;)V
    .registers 6
    .parameter "values"

    #@0
    .prologue
    .line 336
    array-length v2, p1

    #@1
    .line 337
    .local v2, numKeyframes:I
    const/4 v3, 0x2

    #@2
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@5
    move-result v3

    #@6
    new-array v1, v3, [Landroid/animation/Keyframe;

    #@8
    .line 338
    .local v1, keyframes:[Landroid/animation/Keyframe;
    const/4 v3, 0x0

    #@9
    aget-object v3, p1, v3

    #@b
    invoke-virtual {v3}, Landroid/animation/Keyframe;->getType()Ljava/lang/Class;

    #@e
    move-result-object v3

    #@f
    iput-object v3, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@11
    .line 339
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    if-ge v0, v2, :cond_1b

    #@14
    .line 340
    aget-object v3, p1, v0

    #@16
    aput-object v3, v1, v0

    #@18
    .line 339
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_12

    #@1b
    .line 342
    :cond_1b
    new-instance v3, Landroid/animation/KeyframeSet;

    #@1d
    invoke-direct {v3, v1}, Landroid/animation/KeyframeSet;-><init>([Landroid/animation/Keyframe;)V

    #@20
    iput-object v3, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@22
    .line 343
    return-void
.end method

.method public varargs setObjectValues([Ljava/lang/Object;)V
    .registers 3
    .parameter "values"

    #@0
    .prologue
    .line 359
    const/4 v0, 0x0

    #@1
    aget-object v0, p1, v0

    #@3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@9
    .line 360
    invoke-static {p1}, Landroid/animation/KeyframeSet;->ofObject([Ljava/lang/Object;)Landroid/animation/KeyframeSet;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@f
    .line 361
    return-void
.end method

.method public setProperty(Landroid/util/Property;)V
    .registers 2
    .parameter "property"

    #@0
    .prologue
    .line 691
    iput-object p1, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@2
    .line 692
    return-void
.end method

.method public setPropertyName(Ljava/lang/String;)V
    .registers 2
    .parameter "propertyName"

    #@0
    .prologue
    .line 679
    iput-object p1, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@2
    .line 680
    return-void
.end method

.method setupEndValue(Ljava/lang/Object;)V
    .registers 4
    .parameter "target"

    #@0
    .prologue
    .line 578
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@2
    iget-object v0, v0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@4
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@6
    iget-object v1, v1, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v1

    #@c
    add-int/lit8 v1, v1, -0x1

    #@e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/animation/Keyframe;

    #@14
    invoke-direct {p0, p1, v0}, Landroid/animation/PropertyValuesHolder;->setupValue(Ljava/lang/Object;Landroid/animation/Keyframe;)V

    #@17
    .line 579
    return-void
.end method

.method setupSetter(Ljava/lang/Class;)V
    .registers 5
    .parameter "targetClass"

    #@0
    .prologue
    .line 468
    sget-object v0, Landroid/animation/PropertyValuesHolder;->sSetterPropertyMap:Ljava/util/HashMap;

    #@2
    const-string/jumbo v1, "set"

    #@5
    iget-object v2, p0, Landroid/animation/PropertyValuesHolder;->mValueType:Ljava/lang/Class;

    #@7
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/animation/PropertyValuesHolder;->setupSetterOrGetter(Ljava/lang/Class;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/animation/PropertyValuesHolder;->mSetter:Ljava/lang/reflect/Method;

    #@d
    .line 469
    return-void
.end method

.method setupSetterAndGetter(Ljava/lang/Object;)V
    .registers 10
    .parameter "target"

    #@0
    .prologue
    .line 489
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@2
    if-eqz v5, :cond_60

    #@4
    .line 492
    :try_start_4
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@6
    invoke-virtual {v5, p1}, Landroid/util/Property;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v4

    #@a
    .line 493
    .local v4, testValue:Ljava/lang/Object;
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@c
    iget-object v5, v5, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_90

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Landroid/animation/Keyframe;

    #@1e
    .line 494
    .local v2, kf:Landroid/animation/Keyframe;
    invoke-virtual {v2}, Landroid/animation/Keyframe;->hasValue()Z

    #@21
    move-result v5

    #@22
    if-nez v5, :cond_12

    #@24
    .line 495
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@26
    invoke-virtual {v5, p1}, Landroid/util/Property;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v2, v5}, Landroid/animation/Keyframe;->setValue(Ljava/lang/Object;)V
    :try_end_2d
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_2d} :catch_2e

    #@2d
    goto :goto_12

    #@2e
    .line 499
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #kf:Landroid/animation/Keyframe;
    .end local v4           #testValue:Ljava/lang/Object;
    :catch_2e
    move-exception v0

    #@2f
    .line 500
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v5, "PropertyValuesHolder"

    #@31
    new-instance v6, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v7, "No such property ("

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    iget-object v7, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@3e
    invoke-virtual {v7}, Landroid/util/Property;->getName()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    const-string v7, ") on target object "

    #@48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    const-string v7, ". Trying reflection instead"

    #@52
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v6

    #@5a
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 502
    const/4 v5, 0x0

    #@5e
    iput-object v5, p0, Landroid/animation/PropertyValuesHolder;->mProperty:Landroid/util/Property;

    #@60
    .line 505
    .end local v0           #e:Ljava/lang/ClassCastException;
    :cond_60
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@63
    move-result-object v3

    #@64
    .line 506
    .local v3, targetClass:Ljava/lang/Class;
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mSetter:Ljava/lang/reflect/Method;

    #@66
    if-nez v5, :cond_6b

    #@68
    .line 507
    invoke-virtual {p0, v3}, Landroid/animation/PropertyValuesHolder;->setupSetter(Ljava/lang/Class;)V

    #@6b
    .line 509
    :cond_6b
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@6d
    iget-object v5, v5, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@6f
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@72
    move-result-object v1

    #@73
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_73
    :goto_73
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@76
    move-result v5

    #@77
    if-eqz v5, :cond_90

    #@79
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7c
    move-result-object v2

    #@7d
    check-cast v2, Landroid/animation/Keyframe;

    #@7f
    .line 510
    .restart local v2       #kf:Landroid/animation/Keyframe;
    invoke-virtual {v2}, Landroid/animation/Keyframe;->hasValue()Z

    #@82
    move-result v5

    #@83
    if-nez v5, :cond_73

    #@85
    .line 511
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@87
    if-nez v5, :cond_91

    #@89
    .line 512
    invoke-direct {p0, v3}, Landroid/animation/PropertyValuesHolder;->setupGetter(Ljava/lang/Class;)V

    #@8c
    .line 513
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@8e
    if-nez v5, :cond_91

    #@90
    .line 527
    .end local v2           #kf:Landroid/animation/Keyframe;
    .end local v3           #targetClass:Ljava/lang/Class;
    :cond_90
    return-void

    #@91
    .line 519
    .restart local v2       #kf:Landroid/animation/Keyframe;
    .restart local v3       #targetClass:Ljava/lang/Class;
    :cond_91
    :try_start_91
    iget-object v5, p0, Landroid/animation/PropertyValuesHolder;->mGetter:Ljava/lang/reflect/Method;

    #@93
    const/4 v6, 0x0

    #@94
    new-array v6, v6, [Ljava/lang/Object;

    #@96
    invoke-virtual {v5, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@99
    move-result-object v5

    #@9a
    invoke-virtual {v2, v5}, Landroid/animation/Keyframe;->setValue(Ljava/lang/Object;)V
    :try_end_9d
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_91 .. :try_end_9d} :catch_9e
    .catch Ljava/lang/IllegalAccessException; {:try_start_91 .. :try_end_9d} :catch_a9

    #@9d
    goto :goto_73

    #@9e
    .line 520
    :catch_9e
    move-exception v0

    #@9f
    .line 521
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    const-string v5, "PropertyValuesHolder"

    #@a1
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    #@a4
    move-result-object v6

    #@a5
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    goto :goto_73

    #@a9
    .line 522
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_a9
    move-exception v0

    #@aa
    .line 523
    .local v0, e:Ljava/lang/IllegalAccessException;
    const-string v5, "PropertyValuesHolder"

    #@ac
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    #@af
    move-result-object v6

    #@b0
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    goto :goto_73
.end method

.method setupStartValue(Ljava/lang/Object;)V
    .registers 4
    .parameter "target"

    #@0
    .prologue
    .line 566
    iget-object v0, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@2
    iget-object v0, v0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/animation/Keyframe;

    #@b
    invoke-direct {p0, p1, v0}, Landroid/animation/PropertyValuesHolder;->setupValue(Ljava/lang/Object;Landroid/animation/Keyframe;)V

    #@e
    .line 567
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 717
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mPropertyName:Ljava/lang/String;

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    const-string v1, ": "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    iget-object v1, p0, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@13
    invoke-virtual {v1}, Landroid/animation/KeyframeSet;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    return-object v0
.end method
