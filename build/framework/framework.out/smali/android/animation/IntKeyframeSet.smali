.class Landroid/animation/IntKeyframeSet;
.super Landroid/animation/KeyframeSet;
.source "IntKeyframeSet.java"


# instance fields
.field private deltaValue:I

.field private firstTime:Z

.field private firstValue:I

.field private lastValue:I


# direct methods
.method public varargs constructor <init>([Landroid/animation/Keyframe$IntKeyframe;)V
    .registers 3
    .parameter "keyframes"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/animation/KeyframeSet;-><init>([Landroid/animation/Keyframe;)V

    #@3
    .line 37
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/animation/IntKeyframeSet;->firstTime:Z

    #@6
    .line 41
    return-void
.end method


# virtual methods
.method public clone()Landroid/animation/IntKeyframeSet;
    .registers 7

    #@0
    .prologue
    .line 50
    iget-object v1, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@2
    .line 51
    .local v1, keyframes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Keyframe;>;"
    iget-object v5, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v4

    #@8
    .line 52
    .local v4, numKeyframes:I
    new-array v2, v4, [Landroid/animation/Keyframe$IntKeyframe;

    #@a
    .line 53
    .local v2, newKeyframes:[Landroid/animation/Keyframe$IntKeyframe;
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v4, :cond_1e

    #@d
    .line 54
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v5

    #@11
    check-cast v5, Landroid/animation/Keyframe;

    #@13
    invoke-virtual {v5}, Landroid/animation/Keyframe;->clone()Landroid/animation/Keyframe;

    #@16
    move-result-object v5

    #@17
    check-cast v5, Landroid/animation/Keyframe$IntKeyframe;

    #@19
    aput-object v5, v2, v0

    #@1b
    .line 53
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_b

    #@1e
    .line 56
    :cond_1e
    new-instance v3, Landroid/animation/IntKeyframeSet;

    #@20
    invoke-direct {v3, v2}, Landroid/animation/IntKeyframeSet;-><init>([Landroid/animation/Keyframe$IntKeyframe;)V

    #@23
    .line 57
    .local v3, newSet:Landroid/animation/IntKeyframeSet;
    return-object v3
.end method

.method public bridge synthetic clone()Landroid/animation/KeyframeSet;
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/animation/IntKeyframeSet;->clone()Landroid/animation/IntKeyframeSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/animation/IntKeyframeSet;->clone()Landroid/animation/IntKeyframeSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getIntValue(F)I
    .registers 15
    .parameter "fraction"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 61
    iget v9, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@4
    const/4 v10, 0x2

    #@5
    if-ne v9, v10, :cond_60

    #@7
    .line 62
    iget-boolean v9, p0, Landroid/animation/IntKeyframeSet;->firstTime:Z

    #@9
    if-eqz v9, :cond_30

    #@b
    .line 63
    iput-boolean v11, p0, Landroid/animation/IntKeyframeSet;->firstTime:Z

    #@d
    .line 64
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v9

    #@13
    check-cast v9, Landroid/animation/Keyframe$IntKeyframe;

    #@15
    invoke-virtual {v9}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@18
    move-result v9

    #@19
    iput v9, p0, Landroid/animation/IntKeyframeSet;->firstValue:I

    #@1b
    .line 65
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v9

    #@21
    check-cast v9, Landroid/animation/Keyframe$IntKeyframe;

    #@23
    invoke-virtual {v9}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@26
    move-result v9

    #@27
    iput v9, p0, Landroid/animation/IntKeyframeSet;->lastValue:I

    #@29
    .line 66
    iget v9, p0, Landroid/animation/IntKeyframeSet;->lastValue:I

    #@2b
    iget v10, p0, Landroid/animation/IntKeyframeSet;->firstValue:I

    #@2d
    sub-int/2addr v9, v10

    #@2e
    iput v9, p0, Landroid/animation/IntKeyframeSet;->deltaValue:I

    #@30
    .line 68
    :cond_30
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@32
    if-eqz v9, :cond_3a

    #@34
    .line 69
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@36
    invoke-interface {v9, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@39
    move-result p1

    #@3a
    .line 71
    :cond_3a
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@3c
    if-nez v9, :cond_47

    #@3e
    .line 72
    iget v9, p0, Landroid/animation/IntKeyframeSet;->firstValue:I

    #@40
    iget v10, p0, Landroid/animation/IntKeyframeSet;->deltaValue:I

    #@42
    int-to-float v10, v10

    #@43
    mul-float/2addr v10, p1

    #@44
    float-to-int v10, v10

    #@45
    add-int/2addr v9, v10

    #@46
    .line 129
    :goto_46
    return v9

    #@47
    .line 74
    :cond_47
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@49
    iget v10, p0, Landroid/animation/IntKeyframeSet;->firstValue:I

    #@4b
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v10

    #@4f
    iget v11, p0, Landroid/animation/IntKeyframeSet;->lastValue:I

    #@51
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54
    move-result-object v11

    #@55
    invoke-interface {v9, p1, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    move-result-object v9

    #@59
    check-cast v9, Ljava/lang/Number;

    #@5b
    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    #@5e
    move-result v9

    #@5f
    goto :goto_46

    #@60
    .line 77
    :cond_60
    const/4 v9, 0x0

    #@61
    cmpg-float v9, p1, v9

    #@63
    if-gtz v9, :cond_b5

    #@65
    .line 78
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@67
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6a
    move-result-object v7

    #@6b
    check-cast v7, Landroid/animation/Keyframe$IntKeyframe;

    #@6d
    .line 79
    .local v7, prevKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@6f
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@72
    move-result-object v4

    #@73
    check-cast v4, Landroid/animation/Keyframe$IntKeyframe;

    #@75
    .line 80
    .local v4, nextKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@78
    move-result v8

    #@79
    .line 81
    .local v8, prevValue:I
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@7c
    move-result v5

    #@7d
    .line 82
    .local v5, nextValue:I
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@80
    move-result v6

    #@81
    .line 83
    .local v6, prevFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@84
    move-result v3

    #@85
    .line 84
    .local v3, nextFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@88
    move-result-object v1

    #@89
    .line 85
    .local v1, interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_8f

    #@8b
    .line 86
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@8e
    move-result p1

    #@8f
    .line 88
    :cond_8f
    sub-float v9, p1, v6

    #@91
    sub-float v10, v3, v6

    #@93
    div-float v2, v9, v10

    #@95
    .line 89
    .local v2, intervalFraction:F
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@97
    if-nez v9, :cond_a0

    #@99
    sub-int v9, v5, v8

    #@9b
    int-to-float v9, v9

    #@9c
    mul-float/2addr v9, v2

    #@9d
    float-to-int v9, v9

    #@9e
    add-int/2addr v9, v8

    #@9f
    goto :goto_46

    #@a0
    :cond_a0
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@a2
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a5
    move-result-object v10

    #@a6
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a9
    move-result-object v11

    #@aa
    invoke-interface {v9, v2, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ad
    move-result-object v9

    #@ae
    check-cast v9, Ljava/lang/Number;

    #@b0
    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    #@b3
    move-result v9

    #@b4
    goto :goto_46

    #@b5
    .line 93
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v3           #nextFraction:F
    .end local v4           #nextKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    .end local v5           #nextValue:I
    .end local v6           #prevFraction:F
    .end local v7           #prevKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    .end local v8           #prevValue:I
    :cond_b5
    const/high16 v9, 0x3f80

    #@b7
    cmpl-float v9, p1, v9

    #@b9
    if-ltz v9, :cond_115

    #@bb
    .line 94
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@bd
    iget v10, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@bf
    add-int/lit8 v10, v10, -0x2

    #@c1
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c4
    move-result-object v7

    #@c5
    check-cast v7, Landroid/animation/Keyframe$IntKeyframe;

    #@c7
    .line 95
    .restart local v7       #prevKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@c9
    iget v10, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@cb
    add-int/lit8 v10, v10, -0x1

    #@cd
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d0
    move-result-object v4

    #@d1
    check-cast v4, Landroid/animation/Keyframe$IntKeyframe;

    #@d3
    .line 96
    .restart local v4       #nextKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@d6
    move-result v8

    #@d7
    .line 97
    .restart local v8       #prevValue:I
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@da
    move-result v5

    #@db
    .line 98
    .restart local v5       #nextValue:I
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@de
    move-result v6

    #@df
    .line 99
    .restart local v6       #prevFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@e2
    move-result v3

    #@e3
    .line 100
    .restart local v3       #nextFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@e6
    move-result-object v1

    #@e7
    .line 101
    .restart local v1       #interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_ed

    #@e9
    .line 102
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@ec
    move-result p1

    #@ed
    .line 104
    :cond_ed
    sub-float v9, p1, v6

    #@ef
    sub-float v10, v3, v6

    #@f1
    div-float v2, v9, v10

    #@f3
    .line 105
    .restart local v2       #intervalFraction:F
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@f5
    if-nez v9, :cond_ff

    #@f7
    sub-int v9, v5, v8

    #@f9
    int-to-float v9, v9

    #@fa
    mul-float/2addr v9, v2

    #@fb
    float-to-int v9, v9

    #@fc
    add-int/2addr v9, v8

    #@fd
    goto/16 :goto_46

    #@ff
    :cond_ff
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@101
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@104
    move-result-object v10

    #@105
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@108
    move-result-object v11

    #@109
    invoke-interface {v9, v2, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10c
    move-result-object v9

    #@10d
    check-cast v9, Ljava/lang/Number;

    #@10f
    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    #@112
    move-result v9

    #@113
    goto/16 :goto_46

    #@115
    .line 109
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v3           #nextFraction:F
    .end local v4           #nextKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    .end local v5           #nextValue:I
    .end local v6           #prevFraction:F
    .end local v7           #prevKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    .end local v8           #prevValue:I
    :cond_115
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@117
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11a
    move-result-object v7

    #@11b
    check-cast v7, Landroid/animation/Keyframe$IntKeyframe;

    #@11d
    .line 110
    .restart local v7       #prevKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    const/4 v0, 0x1

    #@11e
    .local v0, i:I
    :goto_11e
    iget v9, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@120
    if-ge v0, v9, :cond_17b

    #@122
    .line 111
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@124
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@127
    move-result-object v4

    #@128
    check-cast v4, Landroid/animation/Keyframe$IntKeyframe;

    #@12a
    .line 112
    .restart local v4       #nextKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@12d
    move-result v9

    #@12e
    cmpg-float v9, p1, v9

    #@130
    if-gez v9, :cond_177

    #@132
    .line 113
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@135
    move-result-object v1

    #@136
    .line 114
    .restart local v1       #interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_13c

    #@138
    .line 115
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@13b
    move-result p1

    #@13c
    .line 117
    :cond_13c
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@13f
    move-result v9

    #@140
    sub-float v9, p1, v9

    #@142
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@145
    move-result v10

    #@146
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getFraction()F

    #@149
    move-result v11

    #@14a
    sub-float/2addr v10, v11

    #@14b
    div-float v2, v9, v10

    #@14d
    .line 119
    .restart local v2       #intervalFraction:F
    invoke-virtual {v7}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@150
    move-result v8

    #@151
    .line 120
    .restart local v8       #prevValue:I
    invoke-virtual {v4}, Landroid/animation/Keyframe$IntKeyframe;->getIntValue()I

    #@154
    move-result v5

    #@155
    .line 121
    .restart local v5       #nextValue:I
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@157
    if-nez v9, :cond_161

    #@159
    sub-int v9, v5, v8

    #@15b
    int-to-float v9, v9

    #@15c
    mul-float/2addr v9, v2

    #@15d
    float-to-int v9, v9

    #@15e
    add-int/2addr v9, v8

    #@15f
    goto/16 :goto_46

    #@161
    :cond_161
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@163
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@166
    move-result-object v10

    #@167
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16a
    move-result-object v11

    #@16b
    invoke-interface {v9, v2, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16e
    move-result-object v9

    #@16f
    check-cast v9, Ljava/lang/Number;

    #@171
    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    #@174
    move-result v9

    #@175
    goto/16 :goto_46

    #@177
    .line 126
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v5           #nextValue:I
    .end local v8           #prevValue:I
    :cond_177
    move-object v7, v4

    #@178
    .line 110
    add-int/lit8 v0, v0, 0x1

    #@17a
    goto :goto_11e

    #@17b
    .line 129
    .end local v4           #nextKeyframe:Landroid/animation/Keyframe$IntKeyframe;
    :cond_17b
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@17d
    iget v10, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@17f
    add-int/lit8 v10, v10, -0x1

    #@181
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@184
    move-result-object v9

    #@185
    check-cast v9, Landroid/animation/Keyframe;

    #@187
    invoke-virtual {v9}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@18a
    move-result-object v9

    #@18b
    check-cast v9, Ljava/lang/Number;

    #@18d
    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    #@190
    move-result v9

    #@191
    goto/16 :goto_46
.end method

.method public getValue(F)Ljava/lang/Object;
    .registers 3
    .parameter "fraction"

    #@0
    .prologue
    .line 45
    invoke-virtual {p0, p1}, Landroid/animation/IntKeyframeSet;->getIntValue(F)I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
