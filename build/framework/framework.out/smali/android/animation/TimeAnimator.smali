.class public Landroid/animation/TimeAnimator;
.super Landroid/animation/ValueAnimator;
.source "TimeAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/TimeAnimator$TimeListener;
    }
.end annotation


# instance fields
.field private mListener:Landroid/animation/TimeAnimator$TimeListener;

.field private mPreviousTime:J


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 10
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    #@3
    .line 13
    const-wide/16 v0, -0x1

    #@5
    iput-wide v0, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    #@7
    .line 61
    return-void
.end method


# virtual methods
.method animateValue(F)V
    .registers 2
    .parameter "fraction"

    #@0
    .prologue
    .line 45
    return-void
.end method

.method animationFrame(J)Z
    .registers 9
    .parameter "currentTime"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 23
    iget-object v0, p0, Landroid/animation/TimeAnimator;->mListener:Landroid/animation/TimeAnimator$TimeListener;

    #@4
    if-eqz v0, :cond_18

    #@6
    .line 24
    iget-wide v0, p0, Landroid/animation/ValueAnimator;->mStartTime:J

    #@8
    sub-long v2, p1, v0

    #@a
    .line 25
    .local v2, totalTime:J
    iget-wide v0, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    #@c
    cmp-long v0, v0, v4

    #@e
    if-gez v0, :cond_1a

    #@10
    .line 26
    .local v4, deltaTime:J
    :goto_10
    iput-wide p1, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    #@12
    .line 27
    iget-object v0, p0, Landroid/animation/TimeAnimator;->mListener:Landroid/animation/TimeAnimator$TimeListener;

    #@14
    move-object v1, p0

    #@15
    invoke-interface/range {v0 .. v5}, Landroid/animation/TimeAnimator$TimeListener;->onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V

    #@18
    .line 29
    .end local v2           #totalTime:J
    .end local v4           #deltaTime:J
    :cond_18
    const/4 v0, 0x0

    #@19
    return v0

    #@1a
    .line 25
    .restart local v2       #totalTime:J
    :cond_1a
    iget-wide v0, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    #@1c
    sub-long v4, p1, v0

    #@1e
    goto :goto_10
.end method

.method initAnimation()V
    .registers 1

    #@0
    .prologue
    .line 50
    return-void
.end method

.method public setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 39
    iput-object p1, p0, Landroid/animation/TimeAnimator;->mListener:Landroid/animation/TimeAnimator$TimeListener;

    #@2
    .line 40
    return-void
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    .line 17
    const-wide/16 v0, -0x1

    #@2
    iput-wide v0, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    #@4
    .line 18
    invoke-super {p0}, Landroid/animation/ValueAnimator;->start()V

    #@7
    .line 19
    return-void
.end method
