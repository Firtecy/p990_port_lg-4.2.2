.class Landroid/animation/LayoutTransition$6;
.super Landroid/animation/AnimatorListenerAdapter;
.source "LayoutTransition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/animation/LayoutTransition;->runDisappearingTransition(Landroid/view/ViewGroup;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/animation/LayoutTransition;

.field final synthetic val$child:Landroid/view/View;

.field final synthetic val$parent:Landroid/view/ViewGroup;

.field final synthetic val$preAnimAlpha:F


# direct methods
.method constructor <init>(Landroid/animation/LayoutTransition;Landroid/view/View;FLandroid/view/ViewGroup;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1197
    iput-object p1, p0, Landroid/animation/LayoutTransition$6;->this$0:Landroid/animation/LayoutTransition;

    #@2
    iput-object p2, p0, Landroid/animation/LayoutTransition$6;->val$child:Landroid/view/View;

    #@4
    iput p3, p0, Landroid/animation/LayoutTransition$6;->val$preAnimAlpha:F

    #@6
    iput-object p4, p0, Landroid/animation/LayoutTransition$6;->val$parent:Landroid/view/ViewGroup;

    #@8
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 9
    .parameter "anim"

    #@0
    .prologue
    .line 1200
    iget-object v3, p0, Landroid/animation/LayoutTransition$6;->this$0:Landroid/animation/LayoutTransition;

    #@2
    invoke-static {v3}, Landroid/animation/LayoutTransition;->access$1300(Landroid/animation/LayoutTransition;)Ljava/util/LinkedHashMap;

    #@5
    move-result-object v3

    #@6
    iget-object v4, p0, Landroid/animation/LayoutTransition$6;->val$child:Landroid/view/View;

    #@8
    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    .line 1201
    iget-object v3, p0, Landroid/animation/LayoutTransition$6;->val$child:Landroid/view/View;

    #@d
    iget v4, p0, Landroid/animation/LayoutTransition$6;->val$preAnimAlpha:F

    #@f
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    #@12
    .line 1202
    iget-object v3, p0, Landroid/animation/LayoutTransition$6;->this$0:Landroid/animation/LayoutTransition;

    #@14
    invoke-static {v3}, Landroid/animation/LayoutTransition;->access$1000(Landroid/animation/LayoutTransition;)Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_41

    #@1a
    .line 1203
    iget-object v3, p0, Landroid/animation/LayoutTransition$6;->this$0:Landroid/animation/LayoutTransition;

    #@1c
    invoke-static {v3}, Landroid/animation/LayoutTransition;->access$1100(Landroid/animation/LayoutTransition;)Ljava/util/ArrayList;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Ljava/util/ArrayList;

    #@26
    .line 1205
    .local v2, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@29
    move-result-object v0

    #@2a
    .local v0, i$:Ljava/util/Iterator;
    :goto_2a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_41

    #@30
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33
    move-result-object v1

    #@34
    check-cast v1, Landroid/animation/LayoutTransition$TransitionListener;

    #@36
    .line 1206
    .local v1, listener:Landroid/animation/LayoutTransition$TransitionListener;
    iget-object v3, p0, Landroid/animation/LayoutTransition$6;->this$0:Landroid/animation/LayoutTransition;

    #@38
    iget-object v4, p0, Landroid/animation/LayoutTransition$6;->val$parent:Landroid/view/ViewGroup;

    #@3a
    iget-object v5, p0, Landroid/animation/LayoutTransition$6;->val$child:Landroid/view/View;

    #@3c
    const/4 v6, 0x3

    #@3d
    invoke-interface {v1, v3, v4, v5, v6}, Landroid/animation/LayoutTransition$TransitionListener;->endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@40
    goto :goto_2a

    #@41
    .line 1209
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Landroid/animation/LayoutTransition$TransitionListener;
    .end local v2           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/LayoutTransition$TransitionListener;>;"
    :cond_41
    return-void
.end method
