.class Landroid/animation/Keyframe$FloatKeyframe;
.super Landroid/animation/Keyframe;
.source "Keyframe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/Keyframe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FloatKeyframe"
.end annotation


# instance fields
.field mValue:F


# direct methods
.method constructor <init>(F)V
    .registers 3
    .parameter "fraction"

    #@0
    .prologue
    .line 333
    invoke-direct {p0}, Landroid/animation/Keyframe;-><init>()V

    #@3
    .line 334
    iput p1, p0, Landroid/animation/Keyframe;->mFraction:F

    #@5
    .line 335
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@7
    iput-object v0, p0, Landroid/animation/Keyframe;->mValueType:Ljava/lang/Class;

    #@9
    .line 336
    return-void
.end method

.method constructor <init>(FF)V
    .registers 4
    .parameter "fraction"
    .parameter "value"

    #@0
    .prologue
    .line 326
    invoke-direct {p0}, Landroid/animation/Keyframe;-><init>()V

    #@3
    .line 327
    iput p1, p0, Landroid/animation/Keyframe;->mFraction:F

    #@5
    .line 328
    iput p2, p0, Landroid/animation/Keyframe$FloatKeyframe;->mValue:F

    #@7
    .line 329
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@9
    iput-object v0, p0, Landroid/animation/Keyframe;->mValueType:Ljava/lang/Class;

    #@b
    .line 330
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@e
    .line 331
    return-void
.end method


# virtual methods
.method public clone()Landroid/animation/Keyframe$FloatKeyframe;
    .registers 4

    #@0
    .prologue
    .line 355
    iget-boolean v1, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@2
    if-eqz v1, :cond_17

    #@4
    new-instance v0, Landroid/animation/Keyframe$FloatKeyframe;

    #@6
    invoke-virtual {p0}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@9
    move-result v1

    #@a
    iget v2, p0, Landroid/animation/Keyframe$FloatKeyframe;->mValue:F

    #@c
    invoke-direct {v0, v1, v2}, Landroid/animation/Keyframe$FloatKeyframe;-><init>(FF)V

    #@f
    .line 358
    .local v0, kfClone:Landroid/animation/Keyframe$FloatKeyframe;
    :goto_f
    invoke-virtual {p0}, Landroid/animation/Keyframe$FloatKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Landroid/animation/Keyframe$FloatKeyframe;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@16
    .line 359
    return-object v0

    #@17
    .line 355
    .end local v0           #kfClone:Landroid/animation/Keyframe$FloatKeyframe;
    :cond_17
    new-instance v0, Landroid/animation/Keyframe$FloatKeyframe;

    #@19
    invoke-virtual {p0}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@1c
    move-result v1

    #@1d
    invoke-direct {v0, v1}, Landroid/animation/Keyframe$FloatKeyframe;-><init>(F)V

    #@20
    goto :goto_f
.end method

.method public bridge synthetic clone()Landroid/animation/Keyframe;
    .registers 2

    #@0
    .prologue
    .line 320
    invoke-virtual {p0}, Landroid/animation/Keyframe$FloatKeyframe;->clone()Landroid/animation/Keyframe$FloatKeyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 320
    invoke-virtual {p0}, Landroid/animation/Keyframe$FloatKeyframe;->clone()Landroid/animation/Keyframe$FloatKeyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getFloatValue()F
    .registers 2

    #@0
    .prologue
    .line 339
    iget v0, p0, Landroid/animation/Keyframe$FloatKeyframe;->mValue:F

    #@2
    return v0
.end method

.method public getValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 343
    iget v0, p0, Landroid/animation/Keyframe$FloatKeyframe;->mValue:F

    #@2
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 347
    if-eqz p1, :cond_15

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v0

    #@6
    const-class v1, Ljava/lang/Float;

    #@8
    if-ne v0, v1, :cond_15

    #@a
    .line 348
    check-cast p1, Ljava/lang/Float;

    #@c
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/animation/Keyframe$FloatKeyframe;->mValue:F

    #@12
    .line 349
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@15
    .line 351
    :cond_15
    return-void
.end method
