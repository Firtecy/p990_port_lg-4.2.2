.class Landroid/animation/AnimatorSet$AnimatorSetListener;
.super Ljava/lang/Object;
.source "AnimatorSet.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimatorSetListener"
.end annotation


# instance fields
.field private mAnimatorSet:Landroid/animation/AnimatorSet;

.field final synthetic this$0:Landroid/animation/AnimatorSet;


# direct methods
.method constructor <init>(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet;)V
    .registers 3
    .parameter
    .parameter "animatorSet"

    #@0
    .prologue
    .line 720
    iput-object p1, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 721
    iput-object p2, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->mAnimatorSet:Landroid/animation/AnimatorSet;

    #@7
    .line 722
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 6
    .parameter "animation"

    #@0
    .prologue
    .line 725
    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@2
    iget-boolean v2, v2, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@4
    if-nez v2, :cond_35

    #@6
    .line 728
    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@8
    invoke-static {v2}, Landroid/animation/AnimatorSet;->access$000(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_35

    #@12
    .line 729
    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@14
    iget-object v2, v2, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@16
    if-eqz v2, :cond_35

    #@18
    .line 730
    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@1a
    iget-object v2, v2, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v1

    #@20
    .line 731
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@21
    .local v0, i:I
    :goto_21
    if-ge v0, v1, :cond_35

    #@23
    .line 732
    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@25
    iget-object v2, v2, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    #@2d
    iget-object v3, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->mAnimatorSet:Landroid/animation/AnimatorSet;

    #@2f
    invoke-interface {v2, v3}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    #@32
    .line 731
    add-int/lit8 v0, v0, 0x1

    #@34
    goto :goto_21

    #@35
    .line 737
    .end local v0           #i:I
    .end local v1           #numListeners:I
    :cond_35
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 11
    .parameter "animation"

    #@0
    .prologue
    .line 741
    invoke-virtual {p1, p0}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    #@3
    .line 742
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@5
    invoke-static {v7}, Landroid/animation/AnimatorSet;->access$000(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;

    #@8
    move-result-object v7

    #@9
    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@c
    .line 743
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->mAnimatorSet:Landroid/animation/AnimatorSet;

    #@e
    invoke-static {v7}, Landroid/animation/AnimatorSet;->access$100(Landroid/animation/AnimatorSet;)Ljava/util/HashMap;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/animation/AnimatorSet$Node;

    #@18
    .line 744
    .local v1, animNode:Landroid/animation/AnimatorSet$Node;
    const/4 v7, 0x1

    #@19
    iput-boolean v7, v1, Landroid/animation/AnimatorSet$Node;->done:Z

    #@1b
    .line 745
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@1d
    iget-boolean v7, v7, Landroid/animation/AnimatorSet;->mTerminated:Z

    #@1f
    if-nez v7, :cond_6a

    #@21
    .line 748
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->mAnimatorSet:Landroid/animation/AnimatorSet;

    #@23
    invoke-static {v7}, Landroid/animation/AnimatorSet;->access$200(Landroid/animation/AnimatorSet;)Ljava/util/ArrayList;

    #@26
    move-result-object v5

    #@27
    .line 749
    .local v5, sortedNodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    const/4 v0, 0x1

    #@28
    .line 750
    .local v0, allDone:Z
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2b
    move-result v4

    #@2c
    .line 751
    .local v4, numSortedNodes:I
    const/4 v2, 0x0

    #@2d
    .local v2, i:I
    :goto_2d
    if-ge v2, v4, :cond_3a

    #@2f
    .line 752
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@32
    move-result-object v7

    #@33
    check-cast v7, Landroid/animation/AnimatorSet$Node;

    #@35
    iget-boolean v7, v7, Landroid/animation/AnimatorSet$Node;->done:Z

    #@37
    if-nez v7, :cond_61

    #@39
    .line 753
    const/4 v0, 0x0

    #@3a
    .line 757
    :cond_3a
    if-eqz v0, :cond_6a

    #@3c
    .line 760
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@3e
    iget-object v7, v7, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@40
    if-eqz v7, :cond_64

    #@42
    .line 761
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->this$0:Landroid/animation/AnimatorSet;

    #@44
    iget-object v7, v7, Landroid/animation/Animator;->mListeners:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v7}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@49
    move-result-object v6

    #@4a
    check-cast v6, Ljava/util/ArrayList;

    #@4c
    .line 763
    .local v6, tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@4f
    move-result v3

    #@50
    .line 764
    .local v3, numListeners:I
    const/4 v2, 0x0

    #@51
    :goto_51
    if-ge v2, v3, :cond_64

    #@53
    .line 765
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@56
    move-result-object v7

    #@57
    check-cast v7, Landroid/animation/Animator$AnimatorListener;

    #@59
    iget-object v8, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->mAnimatorSet:Landroid/animation/AnimatorSet;

    #@5b
    invoke-interface {v7, v8}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@5e
    .line 764
    add-int/lit8 v2, v2, 0x1

    #@60
    goto :goto_51

    #@61
    .line 751
    .end local v3           #numListeners:I
    .end local v6           #tmpListeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_61
    add-int/lit8 v2, v2, 0x1

    #@63
    goto :goto_2d

    #@64
    .line 768
    :cond_64
    iget-object v7, p0, Landroid/animation/AnimatorSet$AnimatorSetListener;->mAnimatorSet:Landroid/animation/AnimatorSet;

    #@66
    const/4 v8, 0x0

    #@67
    invoke-static {v7, v8}, Landroid/animation/AnimatorSet;->access$302(Landroid/animation/AnimatorSet;Z)Z

    #@6a
    .line 771
    .end local v0           #allDone:Z
    .end local v2           #i:I
    .end local v4           #numSortedNodes:I
    .end local v5           #sortedNodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/AnimatorSet$Node;>;"
    :cond_6a
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 775
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 779
    return-void
.end method
