.class public abstract Landroid/animation/Keyframe;
.super Ljava/lang/Object;
.source "Keyframe.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/Keyframe$FloatKeyframe;,
        Landroid/animation/Keyframe$IntKeyframe;,
        Landroid/animation/Keyframe$ObjectKeyframe;
    }
.end annotation


# instance fields
.field mFraction:F

.field mHasValue:Z

.field private mInterpolator:Landroid/animation/TimeInterpolator;

.field mValueType:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/animation/Keyframe;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@6
    .line 59
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@9
    .line 320
    return-void
.end method

.method public static ofFloat(F)Landroid/animation/Keyframe;
    .registers 2
    .parameter "fraction"

    #@0
    .prologue
    .line 122
    new-instance v0, Landroid/animation/Keyframe$FloatKeyframe;

    #@2
    invoke-direct {v0, p0}, Landroid/animation/Keyframe$FloatKeyframe;-><init>(F)V

    #@5
    return-object v0
.end method

.method public static ofFloat(FF)Landroid/animation/Keyframe;
    .registers 3
    .parameter "fraction"
    .parameter "value"

    #@0
    .prologue
    .line 106
    new-instance v0, Landroid/animation/Keyframe$FloatKeyframe;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/Keyframe$FloatKeyframe;-><init>(FF)V

    #@5
    return-object v0
.end method

.method public static ofInt(F)Landroid/animation/Keyframe;
    .registers 2
    .parameter "fraction"

    #@0
    .prologue
    .line 90
    new-instance v0, Landroid/animation/Keyframe$IntKeyframe;

    #@2
    invoke-direct {v0, p0}, Landroid/animation/Keyframe$IntKeyframe;-><init>(F)V

    #@5
    return-object v0
.end method

.method public static ofInt(FI)Landroid/animation/Keyframe;
    .registers 3
    .parameter "fraction"
    .parameter "value"

    #@0
    .prologue
    .line 74
    new-instance v0, Landroid/animation/Keyframe$IntKeyframe;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/Keyframe$IntKeyframe;-><init>(FI)V

    #@5
    return-object v0
.end method

.method public static ofObject(F)Landroid/animation/Keyframe;
    .registers 3
    .parameter "fraction"

    #@0
    .prologue
    .line 154
    new-instance v0, Landroid/animation/Keyframe$ObjectKeyframe;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/animation/Keyframe$ObjectKeyframe;-><init>(FLjava/lang/Object;)V

    #@6
    return-object v0
.end method

.method public static ofObject(FLjava/lang/Object;)Landroid/animation/Keyframe;
    .registers 3
    .parameter "fraction"
    .parameter "value"

    #@0
    .prologue
    .line 138
    new-instance v0, Landroid/animation/Keyframe$ObjectKeyframe;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/animation/Keyframe$ObjectKeyframe;-><init>(FLjava/lang/Object;)V

    #@5
    return-object v0
.end method


# virtual methods
.method public abstract clone()Landroid/animation/Keyframe;
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-virtual {p0}, Landroid/animation/Keyframe;->clone()Landroid/animation/Keyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getFraction()F
    .registers 2

    #@0
    .prologue
    .line 189
    iget v0, p0, Landroid/animation/Keyframe;->mFraction:F

    #@2
    return v0
.end method

.method public getInterpolator()Landroid/animation/TimeInterpolator;
    .registers 2

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Landroid/animation/Keyframe;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@2
    return-object v0
.end method

.method public getType()Ljava/lang/Class;
    .registers 2

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Landroid/animation/Keyframe;->mValueType:Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method public abstract getValue()Ljava/lang/Object;
.end method

.method public hasValue()Z
    .registers 2

    #@0
    .prologue
    .line 165
    iget-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@2
    return v0
.end method

.method public setFraction(F)V
    .registers 2
    .parameter "fraction"

    #@0
    .prologue
    .line 199
    iput p1, p0, Landroid/animation/Keyframe;->mFraction:F

    #@2
    .line 200
    return-void
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .registers 2
    .parameter "interpolator"

    #@0
    .prologue
    .line 219
    iput-object p1, p0, Landroid/animation/Keyframe;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@2
    .line 220
    return-void
.end method

.method public abstract setValue(Ljava/lang/Object;)V
.end method
