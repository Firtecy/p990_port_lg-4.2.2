.class Landroid/animation/FloatKeyframeSet;
.super Landroid/animation/KeyframeSet;
.source "FloatKeyframeSet.java"


# instance fields
.field private deltaValue:F

.field private firstTime:Z

.field private firstValue:F

.field private lastValue:F


# direct methods
.method public varargs constructor <init>([Landroid/animation/Keyframe$FloatKeyframe;)V
    .registers 3
    .parameter "keyframes"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/animation/KeyframeSet;-><init>([Landroid/animation/Keyframe;)V

    #@3
    .line 37
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/animation/FloatKeyframeSet;->firstTime:Z

    #@6
    .line 41
    return-void
.end method


# virtual methods
.method public clone()Landroid/animation/FloatKeyframeSet;
    .registers 7

    #@0
    .prologue
    .line 50
    iget-object v1, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@2
    .line 51
    .local v1, keyframes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/Keyframe;>;"
    iget-object v5, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v4

    #@8
    .line 52
    .local v4, numKeyframes:I
    new-array v2, v4, [Landroid/animation/Keyframe$FloatKeyframe;

    #@a
    .line 53
    .local v2, newKeyframes:[Landroid/animation/Keyframe$FloatKeyframe;
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v4, :cond_1e

    #@d
    .line 54
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v5

    #@11
    check-cast v5, Landroid/animation/Keyframe;

    #@13
    invoke-virtual {v5}, Landroid/animation/Keyframe;->clone()Landroid/animation/Keyframe;

    #@16
    move-result-object v5

    #@17
    check-cast v5, Landroid/animation/Keyframe$FloatKeyframe;

    #@19
    aput-object v5, v2, v0

    #@1b
    .line 53
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_b

    #@1e
    .line 56
    :cond_1e
    new-instance v3, Landroid/animation/FloatKeyframeSet;

    #@20
    invoke-direct {v3, v2}, Landroid/animation/FloatKeyframeSet;-><init>([Landroid/animation/Keyframe$FloatKeyframe;)V

    #@23
    .line 57
    .local v3, newSet:Landroid/animation/FloatKeyframeSet;
    return-object v3
.end method

.method public bridge synthetic clone()Landroid/animation/KeyframeSet;
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/animation/FloatKeyframeSet;->clone()Landroid/animation/FloatKeyframeSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/animation/FloatKeyframeSet;->clone()Landroid/animation/FloatKeyframeSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getFloatValue(F)F
    .registers 15
    .parameter "fraction"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 61
    iget v9, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@4
    const/4 v10, 0x2

    #@5
    if-ne v9, v10, :cond_5e

    #@7
    .line 62
    iget-boolean v9, p0, Landroid/animation/FloatKeyframeSet;->firstTime:Z

    #@9
    if-eqz v9, :cond_30

    #@b
    .line 63
    iput-boolean v11, p0, Landroid/animation/FloatKeyframeSet;->firstTime:Z

    #@d
    .line 64
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v9

    #@13
    check-cast v9, Landroid/animation/Keyframe$FloatKeyframe;

    #@15
    invoke-virtual {v9}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@18
    move-result v9

    #@19
    iput v9, p0, Landroid/animation/FloatKeyframeSet;->firstValue:F

    #@1b
    .line 65
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v9

    #@21
    check-cast v9, Landroid/animation/Keyframe$FloatKeyframe;

    #@23
    invoke-virtual {v9}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@26
    move-result v9

    #@27
    iput v9, p0, Landroid/animation/FloatKeyframeSet;->lastValue:F

    #@29
    .line 66
    iget v9, p0, Landroid/animation/FloatKeyframeSet;->lastValue:F

    #@2b
    iget v10, p0, Landroid/animation/FloatKeyframeSet;->firstValue:F

    #@2d
    sub-float/2addr v9, v10

    #@2e
    iput v9, p0, Landroid/animation/FloatKeyframeSet;->deltaValue:F

    #@30
    .line 68
    :cond_30
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@32
    if-eqz v9, :cond_3a

    #@34
    .line 69
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@36
    invoke-interface {v9, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@39
    move-result p1

    #@3a
    .line 71
    :cond_3a
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@3c
    if-nez v9, :cond_45

    #@3e
    .line 72
    iget v9, p0, Landroid/animation/FloatKeyframeSet;->firstValue:F

    #@40
    iget v10, p0, Landroid/animation/FloatKeyframeSet;->deltaValue:F

    #@42
    mul-float/2addr v10, p1

    #@43
    add-float/2addr v9, v10

    #@44
    .line 130
    :goto_44
    return v9

    #@45
    .line 74
    :cond_45
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@47
    iget v10, p0, Landroid/animation/FloatKeyframeSet;->firstValue:F

    #@49
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4c
    move-result-object v10

    #@4d
    iget v11, p0, Landroid/animation/FloatKeyframeSet;->lastValue:F

    #@4f
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@52
    move-result-object v11

    #@53
    invoke-interface {v9, p1, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    move-result-object v9

    #@57
    check-cast v9, Ljava/lang/Number;

    #@59
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    #@5c
    move-result v9

    #@5d
    goto :goto_44

    #@5e
    .line 77
    :cond_5e
    const/4 v9, 0x0

    #@5f
    cmpg-float v9, p1, v9

    #@61
    if-gtz v9, :cond_b1

    #@63
    .line 78
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@68
    move-result-object v7

    #@69
    check-cast v7, Landroid/animation/Keyframe$FloatKeyframe;

    #@6b
    .line 79
    .local v7, prevKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@6d
    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@70
    move-result-object v4

    #@71
    check-cast v4, Landroid/animation/Keyframe$FloatKeyframe;

    #@73
    .line 80
    .local v4, nextKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@76
    move-result v8

    #@77
    .line 81
    .local v8, prevValue:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@7a
    move-result v5

    #@7b
    .line 82
    .local v5, nextValue:F
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@7e
    move-result v6

    #@7f
    .line 83
    .local v6, prevFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@82
    move-result v3

    #@83
    .line 84
    .local v3, nextFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@86
    move-result-object v1

    #@87
    .line 85
    .local v1, interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_8d

    #@89
    .line 86
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@8c
    move-result p1

    #@8d
    .line 88
    :cond_8d
    sub-float v9, p1, v6

    #@8f
    sub-float v10, v3, v6

    #@91
    div-float v2, v9, v10

    #@93
    .line 89
    .local v2, intervalFraction:F
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@95
    if-nez v9, :cond_9c

    #@97
    sub-float v9, v5, v8

    #@99
    mul-float/2addr v9, v2

    #@9a
    add-float/2addr v9, v8

    #@9b
    goto :goto_44

    #@9c
    :cond_9c
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@9e
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@a1
    move-result-object v10

    #@a2
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@a5
    move-result-object v11

    #@a6
    invoke-interface {v9, v2, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a9
    move-result-object v9

    #@aa
    check-cast v9, Ljava/lang/Number;

    #@ac
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    #@af
    move-result v9

    #@b0
    goto :goto_44

    #@b1
    .line 93
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v3           #nextFraction:F
    .end local v4           #nextKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    .end local v5           #nextValue:F
    .end local v6           #prevFraction:F
    .end local v7           #prevKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    .end local v8           #prevValue:F
    :cond_b1
    const/high16 v9, 0x3f80

    #@b3
    cmpl-float v9, p1, v9

    #@b5
    if-ltz v9, :cond_10f

    #@b7
    .line 94
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@b9
    iget v10, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@bb
    add-int/lit8 v10, v10, -0x2

    #@bd
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c0
    move-result-object v7

    #@c1
    check-cast v7, Landroid/animation/Keyframe$FloatKeyframe;

    #@c3
    .line 95
    .restart local v7       #prevKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@c5
    iget v10, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@c7
    add-int/lit8 v10, v10, -0x1

    #@c9
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@cc
    move-result-object v4

    #@cd
    check-cast v4, Landroid/animation/Keyframe$FloatKeyframe;

    #@cf
    .line 96
    .restart local v4       #nextKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@d2
    move-result v8

    #@d3
    .line 97
    .restart local v8       #prevValue:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@d6
    move-result v5

    #@d7
    .line 98
    .restart local v5       #nextValue:F
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@da
    move-result v6

    #@db
    .line 99
    .restart local v6       #prevFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@de
    move-result v3

    #@df
    .line 100
    .restart local v3       #nextFraction:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@e2
    move-result-object v1

    #@e3
    .line 101
    .restart local v1       #interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_e9

    #@e5
    .line 102
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@e8
    move-result p1

    #@e9
    .line 104
    :cond_e9
    sub-float v9, p1, v6

    #@eb
    sub-float v10, v3, v6

    #@ed
    div-float v2, v9, v10

    #@ef
    .line 105
    .restart local v2       #intervalFraction:F
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@f1
    if-nez v9, :cond_f9

    #@f3
    sub-float v9, v5, v8

    #@f5
    mul-float/2addr v9, v2

    #@f6
    add-float/2addr v9, v8

    #@f7
    goto/16 :goto_44

    #@f9
    :cond_f9
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@fb
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@fe
    move-result-object v10

    #@ff
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@102
    move-result-object v11

    #@103
    invoke-interface {v9, v2, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@106
    move-result-object v9

    #@107
    check-cast v9, Ljava/lang/Number;

    #@109
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    #@10c
    move-result v9

    #@10d
    goto/16 :goto_44

    #@10f
    .line 110
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v3           #nextFraction:F
    .end local v4           #nextKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    .end local v5           #nextValue:F
    .end local v6           #prevFraction:F
    .end local v7           #prevKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    .end local v8           #prevValue:F
    :cond_10f
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@111
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@114
    move-result-object v7

    #@115
    check-cast v7, Landroid/animation/Keyframe$FloatKeyframe;

    #@117
    .line 111
    .restart local v7       #prevKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    const/4 v0, 0x1

    #@118
    .local v0, i:I
    :goto_118
    iget v9, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@11a
    if-ge v0, v9, :cond_173

    #@11c
    .line 112
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@11e
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@121
    move-result-object v4

    #@122
    check-cast v4, Landroid/animation/Keyframe$FloatKeyframe;

    #@124
    .line 113
    .restart local v4       #nextKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@127
    move-result v9

    #@128
    cmpg-float v9, p1, v9

    #@12a
    if-gez v9, :cond_16f

    #@12c
    .line 114
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@12f
    move-result-object v1

    #@130
    .line 115
    .restart local v1       #interpolator:Landroid/animation/TimeInterpolator;
    if-eqz v1, :cond_136

    #@132
    .line 116
    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    #@135
    move-result p1

    #@136
    .line 118
    :cond_136
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@139
    move-result v9

    #@13a
    sub-float v9, p1, v9

    #@13c
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@13f
    move-result v10

    #@140
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFraction()F

    #@143
    move-result v11

    #@144
    sub-float/2addr v10, v11

    #@145
    div-float v2, v9, v10

    #@147
    .line 120
    .restart local v2       #intervalFraction:F
    invoke-virtual {v7}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@14a
    move-result v8

    #@14b
    .line 121
    .restart local v8       #prevValue:F
    invoke-virtual {v4}, Landroid/animation/Keyframe$FloatKeyframe;->getFloatValue()F

    #@14e
    move-result v5

    #@14f
    .line 122
    .restart local v5       #nextValue:F
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@151
    if-nez v9, :cond_159

    #@153
    sub-float v9, v5, v8

    #@155
    mul-float/2addr v9, v2

    #@156
    add-float/2addr v9, v8

    #@157
    goto/16 :goto_44

    #@159
    :cond_159
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mEvaluator:Landroid/animation/TypeEvaluator;

    #@15b
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@15e
    move-result-object v10

    #@15f
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@162
    move-result-object v11

    #@163
    invoke-interface {v9, v2, v10, v11}, Landroid/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@166
    move-result-object v9

    #@167
    check-cast v9, Ljava/lang/Number;

    #@169
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    #@16c
    move-result v9

    #@16d
    goto/16 :goto_44

    #@16f
    .line 127
    .end local v1           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v2           #intervalFraction:F
    .end local v5           #nextValue:F
    .end local v8           #prevValue:F
    :cond_16f
    move-object v7, v4

    #@170
    .line 111
    add-int/lit8 v0, v0, 0x1

    #@172
    goto :goto_118

    #@173
    .line 130
    .end local v4           #nextKeyframe:Landroid/animation/Keyframe$FloatKeyframe;
    :cond_173
    iget-object v9, p0, Landroid/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    #@175
    iget v10, p0, Landroid/animation/KeyframeSet;->mNumKeyframes:I

    #@177
    add-int/lit8 v10, v10, -0x1

    #@179
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17c
    move-result-object v9

    #@17d
    check-cast v9, Landroid/animation/Keyframe;

    #@17f
    invoke-virtual {v9}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@182
    move-result-object v9

    #@183
    check-cast v9, Ljava/lang/Number;

    #@185
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    #@188
    move-result v9

    #@189
    goto/16 :goto_44
.end method

.method public getValue(F)Ljava/lang/Object;
    .registers 3
    .parameter "fraction"

    #@0
    .prologue
    .line 45
    invoke-virtual {p0, p1}, Landroid/animation/FloatKeyframeSet;->getFloatValue(F)F

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
