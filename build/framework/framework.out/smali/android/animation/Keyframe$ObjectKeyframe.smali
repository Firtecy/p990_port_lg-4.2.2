.class Landroid/animation/Keyframe$ObjectKeyframe;
.super Landroid/animation/Keyframe;
.source "Keyframe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/Keyframe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ObjectKeyframe"
.end annotation


# instance fields
.field mValue:Ljava/lang/Object;


# direct methods
.method constructor <init>(FLjava/lang/Object;)V
    .registers 4
    .parameter "fraction"
    .parameter "value"

    #@0
    .prologue
    .line 246
    invoke-direct {p0}, Landroid/animation/Keyframe;-><init>()V

    #@3
    .line 247
    iput p1, p0, Landroid/animation/Keyframe;->mFraction:F

    #@5
    .line 248
    iput-object p2, p0, Landroid/animation/Keyframe$ObjectKeyframe;->mValue:Ljava/lang/Object;

    #@7
    .line 249
    if-eqz p2, :cond_17

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@c
    .line 250
    iget-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@e
    if-eqz v0, :cond_19

    #@10
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@13
    move-result-object v0

    #@14
    :goto_14
    iput-object v0, p0, Landroid/animation/Keyframe;->mValueType:Ljava/lang/Class;

    #@16
    .line 251
    return-void

    #@17
    .line 249
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_a

    #@19
    .line 250
    :cond_19
    const-class v0, Ljava/lang/Object;

    #@1b
    goto :goto_14
.end method


# virtual methods
.method public clone()Landroid/animation/Keyframe$ObjectKeyframe;
    .registers 4

    #@0
    .prologue
    .line 264
    new-instance v0, Landroid/animation/Keyframe$ObjectKeyframe;

    #@2
    invoke-virtual {p0}, Landroid/animation/Keyframe$ObjectKeyframe;->getFraction()F

    #@5
    move-result v2

    #@6
    iget-boolean v1, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@8
    if-eqz v1, :cond_17

    #@a
    iget-object v1, p0, Landroid/animation/Keyframe$ObjectKeyframe;->mValue:Ljava/lang/Object;

    #@c
    :goto_c
    invoke-direct {v0, v2, v1}, Landroid/animation/Keyframe$ObjectKeyframe;-><init>(FLjava/lang/Object;)V

    #@f
    .line 265
    .local v0, kfClone:Landroid/animation/Keyframe$ObjectKeyframe;
    invoke-virtual {p0}, Landroid/animation/Keyframe$ObjectKeyframe;->getInterpolator()Landroid/animation/TimeInterpolator;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Landroid/animation/Keyframe$ObjectKeyframe;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@16
    .line 266
    return-object v0

    #@17
    .line 264
    .end local v0           #kfClone:Landroid/animation/Keyframe$ObjectKeyframe;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_c
.end method

.method public bridge synthetic clone()Landroid/animation/Keyframe;
    .registers 2

    #@0
    .prologue
    .line 239
    invoke-virtual {p0}, Landroid/animation/Keyframe$ObjectKeyframe;->clone()Landroid/animation/Keyframe$ObjectKeyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    invoke-virtual {p0}, Landroid/animation/Keyframe$ObjectKeyframe;->clone()Landroid/animation/Keyframe$ObjectKeyframe;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/animation/Keyframe$ObjectKeyframe;->mValue:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 258
    iput-object p1, p0, Landroid/animation/Keyframe$ObjectKeyframe;->mValue:Ljava/lang/Object;

    #@2
    .line 259
    if-eqz p1, :cond_8

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    iput-boolean v0, p0, Landroid/animation/Keyframe;->mHasValue:Z

    #@7
    .line 260
    return-void

    #@8
    .line 259
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_5
.end method
