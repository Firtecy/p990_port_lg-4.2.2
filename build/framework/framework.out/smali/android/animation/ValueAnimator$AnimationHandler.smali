.class Landroid/animation/ValueAnimator$AnimationHandler;
.super Ljava/lang/Object;
.source "ValueAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimationHandler"
.end annotation


# instance fields
.field private mAnimationScheduled:Z

.field private final mAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mChoreographer:Landroid/view/Choreographer;

.field private final mDelayedAnims:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mEndingAnims:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mPendingAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mReadyAnims:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mTmpAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 556
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 537
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimations:Ljava/util/ArrayList;

    #@a
    .line 540
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mTmpAnimations:Ljava/util/ArrayList;

    #@11
    .line 543
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mPendingAnimations:Ljava/util/ArrayList;

    #@18
    .line 549
    new-instance v0, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@1f
    .line 550
    new-instance v0, Ljava/util/ArrayList;

    #@21
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@24
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mEndingAnims:Ljava/util/ArrayList;

    #@26
    .line 551
    new-instance v0, Ljava/util/ArrayList;

    #@28
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2b
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mReadyAnims:Ljava/util/ArrayList;

    #@2d
    .line 557
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mChoreographer:Landroid/view/Choreographer;

    #@33
    .line 558
    return-void
.end method

.method synthetic constructor <init>(Landroid/animation/ValueAnimator$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 535
    invoke-direct {p0}, Landroid/animation/ValueAnimator$AnimationHandler;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 535
    iget-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mPendingAnimations:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 535
    iget-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/animation/ValueAnimator$AnimationHandler;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 535
    iget-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimations:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method private doAnimationFrame(J)V
    .registers 14
    .parameter "frameTime"

    #@0
    .prologue
    .line 573
    :cond_0
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mPendingAnimations:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v7

    #@6
    if-lez v7, :cond_38

    #@8
    .line 574
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mPendingAnimations:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v7}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@d
    move-result-object v6

    #@e
    check-cast v6, Ljava/util/ArrayList;

    #@10
    .line 576
    .local v6, pendingCopy:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/ValueAnimator;>;"
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mPendingAnimations:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@15
    .line 577
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v1

    #@19
    .line 578
    .local v1, count:I
    const/4 v2, 0x0

    #@1a
    .local v2, i:I
    :goto_1a
    if-ge v2, v1, :cond_0

    #@1c
    .line 579
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/animation/ValueAnimator;

    #@22
    .line 581
    .local v0, anim:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Landroid/animation/ValueAnimator;->access$000(Landroid/animation/ValueAnimator;)J

    #@25
    move-result-wide v7

    #@26
    const-wide/16 v9, 0x0

    #@28
    cmp-long v7, v7, v9

    #@2a
    if-nez v7, :cond_32

    #@2c
    .line 582
    invoke-static {v0, p0}, Landroid/animation/ValueAnimator;->access$100(Landroid/animation/ValueAnimator;Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@2f
    .line 578
    :goto_2f
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_1a

    #@32
    .line 584
    :cond_32
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    goto :goto_2f

    #@38
    .line 590
    .end local v0           #anim:Landroid/animation/ValueAnimator;
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v6           #pendingCopy:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/ValueAnimator;>;"
    :cond_38
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@3d
    move-result v4

    #@3e
    .line 591
    .local v4, numDelayedAnims:I
    const/4 v2, 0x0

    #@3f
    .restart local v2       #i:I
    :goto_3f
    if-ge v2, v4, :cond_57

    #@41
    .line 592
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@43
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@46
    move-result-object v0

    #@47
    check-cast v0, Landroid/animation/ValueAnimator;

    #@49
    .line 593
    .restart local v0       #anim:Landroid/animation/ValueAnimator;
    invoke-static {v0, p1, p2}, Landroid/animation/ValueAnimator;->access$200(Landroid/animation/ValueAnimator;J)Z

    #@4c
    move-result v7

    #@4d
    if-eqz v7, :cond_54

    #@4f
    .line 594
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mReadyAnims:Ljava/util/ArrayList;

    #@51
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    .line 591
    :cond_54
    add-int/lit8 v2, v2, 0x1

    #@56
    goto :goto_3f

    #@57
    .line 597
    .end local v0           #anim:Landroid/animation/ValueAnimator;
    :cond_57
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mReadyAnims:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@5c
    move-result v5

    #@5d
    .line 598
    .local v5, numReadyAnims:I
    if-lez v5, :cond_7e

    #@5f
    .line 599
    const/4 v2, 0x0

    #@60
    :goto_60
    if-ge v2, v5, :cond_79

    #@62
    .line 600
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mReadyAnims:Ljava/util/ArrayList;

    #@64
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@67
    move-result-object v0

    #@68
    check-cast v0, Landroid/animation/ValueAnimator;

    #@6a
    .line 601
    .restart local v0       #anim:Landroid/animation/ValueAnimator;
    invoke-static {v0, p0}, Landroid/animation/ValueAnimator;->access$100(Landroid/animation/ValueAnimator;Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@6d
    .line 602
    const/4 v7, 0x1

    #@6e
    invoke-static {v0, v7}, Landroid/animation/ValueAnimator;->access$302(Landroid/animation/ValueAnimator;Z)Z

    #@71
    .line 603
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@73
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@76
    .line 599
    add-int/lit8 v2, v2, 0x1

    #@78
    goto :goto_60

    #@79
    .line 605
    .end local v0           #anim:Landroid/animation/ValueAnimator;
    :cond_79
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mReadyAnims:Ljava/util/ArrayList;

    #@7b
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@7e
    .line 610
    :cond_7e
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimations:Ljava/util/ArrayList;

    #@80
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@83
    move-result v3

    #@84
    .line 611
    .local v3, numAnims:I
    const/4 v2, 0x0

    #@85
    :goto_85
    if-ge v2, v3, :cond_95

    #@87
    .line 612
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mTmpAnimations:Ljava/util/ArrayList;

    #@89
    iget-object v8, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimations:Ljava/util/ArrayList;

    #@8b
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@92
    .line 611
    add-int/lit8 v2, v2, 0x1

    #@94
    goto :goto_85

    #@95
    .line 614
    :cond_95
    const/4 v2, 0x0

    #@96
    :goto_96
    if-ge v2, v3, :cond_b6

    #@98
    .line 615
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mTmpAnimations:Ljava/util/ArrayList;

    #@9a
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9d
    move-result-object v0

    #@9e
    check-cast v0, Landroid/animation/ValueAnimator;

    #@a0
    .line 616
    .restart local v0       #anim:Landroid/animation/ValueAnimator;
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimations:Ljava/util/ArrayList;

    #@a2
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@a5
    move-result v7

    #@a6
    if-eqz v7, :cond_b3

    #@a8
    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->doAnimationFrame(J)Z

    #@ab
    move-result v7

    #@ac
    if-eqz v7, :cond_b3

    #@ae
    .line 617
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mEndingAnims:Ljava/util/ArrayList;

    #@b0
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b3
    .line 614
    :cond_b3
    add-int/lit8 v2, v2, 0x1

    #@b5
    goto :goto_96

    #@b6
    .line 620
    .end local v0           #anim:Landroid/animation/ValueAnimator;
    :cond_b6
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mTmpAnimations:Ljava/util/ArrayList;

    #@b8
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@bb
    .line 621
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mEndingAnims:Ljava/util/ArrayList;

    #@bd
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@c0
    move-result v7

    #@c1
    if-lez v7, :cond_df

    #@c3
    .line 622
    const/4 v2, 0x0

    #@c4
    :goto_c4
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mEndingAnims:Ljava/util/ArrayList;

    #@c6
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@c9
    move-result v7

    #@ca
    if-ge v2, v7, :cond_da

    #@cc
    .line 623
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mEndingAnims:Ljava/util/ArrayList;

    #@ce
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d1
    move-result-object v7

    #@d2
    check-cast v7, Landroid/animation/ValueAnimator;

    #@d4
    invoke-static {v7, p0}, Landroid/animation/ValueAnimator;->access$400(Landroid/animation/ValueAnimator;Landroid/animation/ValueAnimator$AnimationHandler;)V

    #@d7
    .line 622
    add-int/lit8 v2, v2, 0x1

    #@d9
    goto :goto_c4

    #@da
    .line 625
    :cond_da
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mEndingAnims:Ljava/util/ArrayList;

    #@dc
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@df
    .line 630
    :cond_df
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimations:Ljava/util/ArrayList;

    #@e1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    #@e4
    move-result v7

    #@e5
    if-eqz v7, :cond_ef

    #@e7
    iget-object v7, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mDelayedAnims:Ljava/util/ArrayList;

    #@e9
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    #@ec
    move-result v7

    #@ed
    if-nez v7, :cond_f2

    #@ef
    .line 631
    :cond_ef
    invoke-direct {p0}, Landroid/animation/ValueAnimator$AnimationHandler;->scheduleAnimation()V

    #@f2
    .line 633
    :cond_f2
    return-void
.end method

.method private scheduleAnimation()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 643
    iget-boolean v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimationScheduled:Z

    #@3
    if-nez v0, :cond_d

    #@5
    .line 644
    iget-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mChoreographer:Landroid/view/Choreographer;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, v2, p0, v1}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@b
    .line 645
    iput-boolean v2, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimationScheduled:Z

    #@d
    .line 647
    :cond_d
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 638
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mAnimationScheduled:Z

    #@3
    .line 639
    iget-object v0, p0, Landroid/animation/ValueAnimator$AnimationHandler;->mChoreographer:Landroid/view/Choreographer;

    #@5
    invoke-virtual {v0}, Landroid/view/Choreographer;->getFrameTime()J

    #@8
    move-result-wide v0

    #@9
    invoke-direct {p0, v0, v1}, Landroid/animation/ValueAnimator$AnimationHandler;->doAnimationFrame(J)V

    #@c
    .line 640
    return-void
.end method

.method public start()V
    .registers 1

    #@0
    .prologue
    .line 564
    invoke-direct {p0}, Landroid/animation/ValueAnimator$AnimationHandler;->scheduleAnimation()V

    #@3
    .line 565
    return-void
.end method
