.class Landroid/animation/LayoutTransition$3;
.super Ljava/lang/Object;
.source "LayoutTransition.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/animation/LayoutTransition;->setupChangeAnimation(Landroid/view/ViewGroup;ILandroid/animation/Animator;JLandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/animation/LayoutTransition;

.field final synthetic val$anim:Landroid/animation/Animator;

.field final synthetic val$changeReason:I

.field final synthetic val$child:Landroid/view/View;

.field final synthetic val$duration:J

.field final synthetic val$parent:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/animation/LayoutTransition;Landroid/animation/Animator;IJLandroid/view/View;Landroid/view/ViewGroup;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 878
    iput-object p1, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@2
    iput-object p2, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@4
    iput p3, p0, Landroid/animation/LayoutTransition$3;->val$changeReason:I

    #@6
    iput-wide p4, p0, Landroid/animation/LayoutTransition$3;->val$duration:J

    #@8
    iput-object p6, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@a
    iput-object p7, p0, Landroid/animation/LayoutTransition$3;->val$parent:Landroid/view/ViewGroup;

    #@c
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@f
    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .registers 24
    .parameter "v"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "oldLeft"
    .parameter "oldTop"
    .parameter "oldRight"
    .parameter "oldBottom"

    #@0
    .prologue
    .line 883
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@2
    invoke-virtual {v10}, Landroid/animation/Animator;->setupEndValues()V

    #@5
    .line 884
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@7
    instance-of v10, v10, Landroid/animation/ValueAnimator;

    #@9
    if-eqz v10, :cond_3d

    #@b
    .line 885
    const/4 v9, 0x0

    #@c
    .line 886
    .local v9, valuesDiffer:Z
    iget-object v8, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@e
    check-cast v8, Landroid/animation/ValueAnimator;

    #@10
    .line 887
    .local v8, valueAnim:Landroid/animation/ValueAnimator;
    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    #@13
    move-result-object v2

    #@14
    .line 888
    .local v2, oldValues:[Landroid/animation/PropertyValuesHolder;
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    array-length v10, v2

    #@16
    if-ge v0, v10, :cond_3a

    #@18
    .line 889
    aget-object v5, v2, v0

    #@1a
    .line 890
    .local v5, pvh:Landroid/animation/PropertyValuesHolder;
    iget-object v1, v5, Landroid/animation/PropertyValuesHolder;->mKeyframeSet:Landroid/animation/KeyframeSet;

    #@1c
    .line 891
    .local v1, keyframeSet:Landroid/animation/KeyframeSet;
    iget-object v10, v1, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@1e
    if-eqz v10, :cond_36

    #@20
    iget-object v10, v1, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@22
    if-eqz v10, :cond_36

    #@24
    iget-object v10, v1, Landroid/animation/KeyframeSet;->mFirstKeyframe:Landroid/animation/Keyframe;

    #@26
    invoke-virtual {v10}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@29
    move-result-object v10

    #@2a
    iget-object v11, v1, Landroid/animation/KeyframeSet;->mLastKeyframe:Landroid/animation/Keyframe;

    #@2c
    invoke-virtual {v11}, Landroid/animation/Keyframe;->getValue()Ljava/lang/Object;

    #@2f
    move-result-object v11

    #@30
    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v10

    #@34
    if-nez v10, :cond_37

    #@36
    .line 895
    :cond_36
    const/4 v9, 0x1

    #@37
    .line 888
    :cond_37
    add-int/lit8 v0, v0, 0x1

    #@39
    goto :goto_15

    #@3a
    .line 898
    .end local v1           #keyframeSet:Landroid/animation/KeyframeSet;
    .end local v5           #pvh:Landroid/animation/PropertyValuesHolder;
    :cond_3a
    if-nez v9, :cond_3d

    #@3c
    .line 938
    .end local v0           #i:I
    .end local v2           #oldValues:[Landroid/animation/PropertyValuesHolder;
    .end local v8           #valueAnim:Landroid/animation/ValueAnimator;
    .end local v9           #valuesDiffer:Z
    :goto_3c
    return-void

    #@3d
    .line 903
    :cond_3d
    const-wide/16 v6, 0x0

    #@3f
    .line 904
    .local v6, startDelay:J
    iget v10, p0, Landroid/animation/LayoutTransition$3;->val$changeReason:I

    #@41
    packed-switch v10, :pswitch_data_f4

    #@44
    .line 918
    :goto_44
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@46
    invoke-virtual {v10, v6, v7}, Landroid/animation/Animator;->setStartDelay(J)V

    #@49
    .line 919
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@4b
    iget-wide v11, p0, Landroid/animation/LayoutTransition$3;->val$duration:J

    #@4d
    invoke-virtual {v10, v11, v12}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@50
    .line 921
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@52
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$900(Landroid/animation/LayoutTransition;)Ljava/util/LinkedHashMap;

    #@55
    move-result-object v10

    #@56
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@58
    invoke-virtual {v10, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    move-result-object v4

    #@5c
    check-cast v4, Landroid/animation/Animator;

    #@5e
    .line 922
    .local v4, prevAnimation:Landroid/animation/Animator;
    if-eqz v4, :cond_63

    #@60
    .line 923
    invoke-virtual {v4}, Landroid/animation/Animator;->cancel()V

    #@63
    .line 925
    :cond_63
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@65
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$100(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@68
    move-result-object v10

    #@69
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@6b
    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    move-result-object v3

    #@6f
    check-cast v3, Landroid/animation/Animator;

    #@71
    .line 926
    .local v3, pendingAnimation:Landroid/animation/Animator;
    if-eqz v3, :cond_7e

    #@73
    .line 927
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@75
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$100(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@78
    move-result-object v10

    #@79
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@7b
    invoke-virtual {v10, v11}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@7e
    .line 930
    :cond_7e
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@80
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$900(Landroid/animation/LayoutTransition;)Ljava/util/LinkedHashMap;

    #@83
    move-result-object v10

    #@84
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@86
    iget-object v12, p0, Landroid/animation/LayoutTransition$3;->val$anim:Landroid/animation/Animator;

    #@88
    invoke-virtual {v10, v11, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8b
    .line 932
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->val$parent:Landroid/view/ViewGroup;

    #@8d
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@8f
    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->requestTransitionStart(Landroid/animation/LayoutTransition;)V

    #@92
    .line 936
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@94
    invoke-virtual {v10, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    #@97
    .line 937
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@99
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$000(Landroid/animation/LayoutTransition;)Ljava/util/HashMap;

    #@9c
    move-result-object v10

    #@9d
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->val$child:Landroid/view/View;

    #@9f
    invoke-virtual {v10, v11}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a2
    goto :goto_3c

    #@a3
    .line 906
    .end local v3           #pendingAnimation:Landroid/animation/Animator;
    .end local v4           #prevAnimation:Landroid/animation/Animator;
    :pswitch_a3
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@a5
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$200(Landroid/animation/LayoutTransition;)J

    #@a8
    move-result-wide v10

    #@a9
    iget-object v12, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@ab
    invoke-static {v12}, Landroid/animation/LayoutTransition;->access$300(Landroid/animation/LayoutTransition;)J

    #@ae
    move-result-wide v12

    #@af
    add-long v6, v10, v12

    #@b1
    .line 907
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@b3
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@b5
    invoke-static {v11}, Landroid/animation/LayoutTransition;->access$400(Landroid/animation/LayoutTransition;)J

    #@b8
    move-result-wide v11

    #@b9
    invoke-static {v10, v11, v12}, Landroid/animation/LayoutTransition;->access$314(Landroid/animation/LayoutTransition;J)J

    #@bc
    goto :goto_44

    #@bd
    .line 910
    :pswitch_bd
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@bf
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$500(Landroid/animation/LayoutTransition;)J

    #@c2
    move-result-wide v10

    #@c3
    iget-object v12, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@c5
    invoke-static {v12}, Landroid/animation/LayoutTransition;->access$300(Landroid/animation/LayoutTransition;)J

    #@c8
    move-result-wide v12

    #@c9
    add-long v6, v10, v12

    #@cb
    .line 911
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@cd
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@cf
    invoke-static {v11}, Landroid/animation/LayoutTransition;->access$600(Landroid/animation/LayoutTransition;)J

    #@d2
    move-result-wide v11

    #@d3
    invoke-static {v10, v11, v12}, Landroid/animation/LayoutTransition;->access$314(Landroid/animation/LayoutTransition;J)J

    #@d6
    goto/16 :goto_44

    #@d8
    .line 914
    :pswitch_d8
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@da
    invoke-static {v10}, Landroid/animation/LayoutTransition;->access$700(Landroid/animation/LayoutTransition;)J

    #@dd
    move-result-wide v10

    #@de
    iget-object v12, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@e0
    invoke-static {v12}, Landroid/animation/LayoutTransition;->access$300(Landroid/animation/LayoutTransition;)J

    #@e3
    move-result-wide v12

    #@e4
    add-long v6, v10, v12

    #@e6
    .line 915
    iget-object v10, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@e8
    iget-object v11, p0, Landroid/animation/LayoutTransition$3;->this$0:Landroid/animation/LayoutTransition;

    #@ea
    invoke-static {v11}, Landroid/animation/LayoutTransition;->access$800(Landroid/animation/LayoutTransition;)J

    #@ed
    move-result-wide v11

    #@ee
    invoke-static {v10, v11, v12}, Landroid/animation/LayoutTransition;->access$314(Landroid/animation/LayoutTransition;J)J

    #@f1
    goto/16 :goto_44

    #@f3
    .line 904
    nop

    #@f4
    :pswitch_data_f4
    .packed-switch 0x2
        :pswitch_a3
        :pswitch_bd
        :pswitch_d8
    .end packed-switch
.end method
