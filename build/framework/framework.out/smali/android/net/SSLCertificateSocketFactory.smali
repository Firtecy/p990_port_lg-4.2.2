.class public Landroid/net/SSLCertificateSocketFactory;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "SSLCertificateSocketFactory.java"


# static fields
.field private static final HOSTNAME_VERIFIER:Ljavax/net/ssl/HostnameVerifier; = null

.field private static final INSECURE_TRUST_MANAGER:[Ljavax/net/ssl/TrustManager; = null

.field private static final TAG:Ljava/lang/String; = "SSLCertificateSocketFactory"


# instance fields
.field private final mHandshakeTimeoutMillis:I

.field private mInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private mKeyManagers:[Ljavax/net/ssl/KeyManager;

.field private mNpnProtocols:[B

.field private final mSecure:Z

.field private mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private final mSessionCache:Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;

.field private mTrustManagers:[Ljavax/net/ssl/TrustManager;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 75
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    #@3
    const/4 v1, 0x0

    #@4
    new-instance v2, Landroid/net/SSLCertificateSocketFactory$1;

    #@6
    invoke-direct {v2}, Landroid/net/SSLCertificateSocketFactory$1;-><init>()V

    #@9
    aput-object v2, v0, v1

    #@b
    sput-object v0, Landroid/net/SSLCertificateSocketFactory;->INSECURE_TRUST_MANAGER:[Ljavax/net/ssl/TrustManager;

    #@d
    .line 83
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    #@10
    move-result-object v0

    #@11
    sput-object v0, Landroid/net/SSLCertificateSocketFactory;->HOSTNAME_VERIFIER:Ljavax/net/ssl/HostnameVerifier;

    #@13
    return-void
.end method

.method public constructor <init>(I)V
    .registers 4
    .parameter "handshakeTimeoutMillis"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 99
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, v0, v1}, Landroid/net/SSLCertificateSocketFactory;-><init>(ILandroid/net/SSLSessionCache;Z)V

    #@5
    .line 100
    return-void
.end method

.method private constructor <init>(ILandroid/net/SSLSessionCache;Z)V
    .registers 5
    .parameter "handshakeTimeoutMillis"
    .parameter "cache"
    .parameter "secure"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 103
    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    #@4
    .line 86
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@6
    .line 87
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@8
    .line 88
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mTrustManagers:[Ljavax/net/ssl/TrustManager;

    #@a
    .line 89
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mKeyManagers:[Ljavax/net/ssl/KeyManager;

    #@c
    .line 90
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@e
    .line 104
    iput p1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@10
    .line 105
    if-nez p2, :cond_17

    #@12
    :goto_12
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSessionCache:Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;

    #@14
    .line 106
    iput-boolean p3, p0, Landroid/net/SSLCertificateSocketFactory;->mSecure:Z

    #@16
    .line 107
    return-void

    #@17
    .line 105
    :cond_17
    iget-object v0, p2, Landroid/net/SSLSessionCache;->mSessionCache:Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;

    #@19
    goto :goto_12
.end method

.method private static castToOpenSSLSocket(Ljava/net/Socket;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    .registers 4
    .parameter "socket"

    #@0
    .prologue
    .line 362
    instance-of v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@2
    if-nez v0, :cond_1d

    #@4
    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Socket not created by this factory: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v0

    #@1d
    .line 367
    :cond_1d
    check-cast p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@1f
    .end local p0
    return-object p0
.end method

.method public static getDefault(I)Ljavax/net/SocketFactory;
    .registers 4
    .parameter "handshakeTimeoutMillis"

    #@0
    .prologue
    .line 117
    new-instance v0, Landroid/net/SSLCertificateSocketFactory;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    invoke-direct {v0, p0, v1, v2}, Landroid/net/SSLCertificateSocketFactory;-><init>(ILandroid/net/SSLSessionCache;Z)V

    #@7
    return-object v0
.end method

.method public static getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;
    .registers 4
    .parameter "handshakeTimeoutMillis"
    .parameter "cache"

    #@0
    .prologue
    .line 130
    new-instance v0, Landroid/net/SSLCertificateSocketFactory;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {v0, p0, p1, v1}, Landroid/net/SSLCertificateSocketFactory;-><init>(ILandroid/net/SSLSessionCache;Z)V

    #@6
    return-object v0
.end method

.method private declared-synchronized getDelegate()Ljavax/net/ssl/SSLSocketFactory;
    .registers 3

    #@0
    .prologue
    .line 222
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecure:Z

    #@3
    if-eqz v0, :cond_b

    #@5
    invoke-static {}, Landroid/net/SSLCertificateSocketFactory;->isSslCheckRelaxed()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_33

    #@b
    .line 223
    :cond_b
    iget-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@d
    if-nez v0, :cond_24

    #@f
    .line 224
    iget-boolean v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecure:Z

    #@11
    if-eqz v0, :cond_28

    #@13
    .line 225
    const-string v0, "SSLCertificateSocketFactory"

    #@15
    const-string v1, "*** BYPASSING SSL SECURITY CHECKS (socket.relaxsslcheck=yes) ***"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 229
    :goto_1a
    iget-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mKeyManagers:[Ljavax/net/ssl/KeyManager;

    #@1c
    sget-object v1, Landroid/net/SSLCertificateSocketFactory;->INSECURE_TRUST_MANAGER:[Ljavax/net/ssl/TrustManager;

    #@1e
    invoke-direct {p0, v0, v1}, Landroid/net/SSLCertificateSocketFactory;->makeSocketFactory([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLSocketFactory;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@24
    .line 231
    :cond_24
    iget-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_30

    #@26
    .line 236
    :goto_26
    monitor-exit p0

    #@27
    return-object v0

    #@28
    .line 227
    :cond_28
    :try_start_28
    const-string v0, "SSLCertificateSocketFactory"

    #@2a
    const-string v1, "Bypassing SSL security checks at caller\'s request"

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catchall {:try_start_28 .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_1a

    #@30
    .line 222
    :catchall_30
    move-exception v0

    #@31
    monitor-exit p0

    #@32
    throw v0

    #@33
    .line 233
    :cond_33
    :try_start_33
    iget-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@35
    if-nez v0, :cond_41

    #@37
    .line 234
    iget-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mKeyManagers:[Ljavax/net/ssl/KeyManager;

    #@39
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mTrustManagers:[Ljavax/net/ssl/TrustManager;

    #@3b
    invoke-direct {p0, v0, v1}, Landroid/net/SSLCertificateSocketFactory;->makeSocketFactory([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLSocketFactory;

    #@3e
    move-result-object v0

    #@3f
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@41
    .line 236
    :cond_41
    iget-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_43
    .catchall {:try_start_33 .. :try_end_43} :catchall_30

    #@43
    goto :goto_26
.end method

.method public static getHttpSocketFactory(ILandroid/net/SSLSessionCache;)Lorg/apache/http/conn/ssl/SSLSocketFactory;
    .registers 5
    .parameter "handshakeTimeoutMillis"
    .parameter "cache"

    #@0
    .prologue
    .line 160
    new-instance v0, Lorg/apache/http/conn/ssl/SSLSocketFactory;

    #@2
    new-instance v1, Landroid/net/SSLCertificateSocketFactory;

    #@4
    const/4 v2, 0x1

    #@5
    invoke-direct {v1, p0, p1, v2}, Landroid/net/SSLCertificateSocketFactory;-><init>(ILandroid/net/SSLSessionCache;Z)V

    #@8
    invoke-direct {v0, v1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    #@b
    return-object v0
.end method

.method public static getInsecure(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;
    .registers 4
    .parameter "handshakeTimeoutMillis"
    .parameter "cache"

    #@0
    .prologue
    .line 146
    new-instance v0, Landroid/net/SSLCertificateSocketFactory;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, p1, v1}, Landroid/net/SSLCertificateSocketFactory;-><init>(ILandroid/net/SSLSessionCache;Z)V

    #@6
    return-object v0
.end method

.method private static isSslCheckRelaxed()Z
    .registers 2

    #@0
    .prologue
    .line 216
    const-string v0, "1"

    #@2
    const-string/jumbo v1, "ro.debuggable"

    #@5
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_21

    #@f
    const-string/jumbo v0, "yes"

    #@12
    const-string/jumbo v1, "socket.relaxsslcheck"

    #@15
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_21

    #@1f
    const/4 v0, 0x1

    #@20
    :goto_20
    return v0

    #@21
    :cond_21
    const/4 v0, 0x0

    #@22
    goto :goto_20
.end method

.method private makeSocketFactory([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLSocketFactory;
    .registers 7
    .parameter "keyManagers"
    .parameter "trustManagers"

    #@0
    .prologue
    .line 205
    :try_start_0
    new-instance v1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;

    #@2
    invoke-direct {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;-><init>()V

    #@5
    .line 206
    .local v1, sslContext:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;
    const/4 v2, 0x0

    #@6
    invoke-virtual {v1, p1, p2, v2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;->engineInit([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    #@9
    .line 207
    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;->engineGetClientSessionContext()Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Landroid/net/SSLCertificateSocketFactory;->mSessionCache:Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;

    #@f
    invoke-virtual {v2, v3}, Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;->setPersistentCache(Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;)V

    #@12
    .line 208
    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;->engineGetSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    :try_end_15
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_15} :catch_17

    #@15
    move-result-object v2

    #@16
    .line 211
    .end local v1           #sslContext:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;
    :goto_16
    return-object v2

    #@17
    .line 209
    :catch_17
    move-exception v0

    #@18
    .line 210
    .local v0, e:Ljava/security/KeyManagementException;
    const-string v2, "SSLCertificateSocketFactory"

    #@1a
    invoke-static {v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    .line 211
    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Ljavax/net/ssl/SSLSocketFactory;

    #@23
    goto :goto_16
.end method

.method static varargs toNpnProtocolsList([[B)[B
    .registers 16
    .parameter "npnProtocols"

    #@0
    .prologue
    .line 277
    array-length v12, p0

    #@1
    if-nez v12, :cond_c

    #@3
    .line 278
    new-instance v12, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v13, "npnProtocols.length == 0"

    #@8
    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v12

    #@c
    .line 280
    :cond_c
    const/4 v11, 0x0

    #@d
    .line 281
    .local v11, totalLength:I
    move-object v0, p0

    #@e
    .local v0, arr$:[[B
    array-length v5, v0

    #@f
    .local v5, len$:I
    const/4 v3, 0x0

    #@10
    .local v3, i$:I
    :goto_10
    if-ge v3, v5, :cond_3e

    #@12
    aget-object v10, v0, v3

    #@14
    .line 282
    .local v10, s:[B
    array-length v12, v10

    #@15
    if-eqz v12, :cond_1c

    #@17
    array-length v12, v10

    #@18
    const/16 v13, 0xff

    #@1a
    if-le v12, v13, :cond_37

    #@1c
    .line 283
    :cond_1c
    new-instance v12, Ljava/lang/IllegalArgumentException;

    #@1e
    new-instance v13, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string/jumbo v14, "s.length == 0 || s.length > 255: "

    #@26
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v13

    #@2a
    array-length v14, v10

    #@2b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v13

    #@2f
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v13

    #@33
    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v12

    #@37
    .line 285
    :cond_37
    array-length v12, v10

    #@38
    add-int/lit8 v12, v12, 0x1

    #@3a
    add-int/2addr v11, v12

    #@3b
    .line 281
    add-int/lit8 v3, v3, 0x1

    #@3d
    goto :goto_10

    #@3e
    .line 287
    .end local v10           #s:[B
    :cond_3e
    new-array v9, v11, [B

    #@40
    .line 288
    .local v9, result:[B
    const/4 v7, 0x0

    #@41
    .line 289
    .local v7, pos:I
    move-object v0, p0

    #@42
    array-length v5, v0

    #@43
    const/4 v3, 0x0

    #@44
    move v4, v3

    #@45
    .end local v3           #i$:I
    .local v4, i$:I
    move v8, v7

    #@46
    .end local v0           #arr$:[[B
    .end local v5           #len$:I
    .end local v7           #pos:I
    .local v8, pos:I
    :goto_46
    if-ge v4, v5, :cond_64

    #@48
    aget-object v10, v0, v4

    #@4a
    .line 290
    .restart local v10       #s:[B
    add-int/lit8 v7, v8, 0x1

    #@4c
    .end local v8           #pos:I
    .restart local v7       #pos:I
    array-length v12, v10

    #@4d
    int-to-byte v12, v12

    #@4e
    aput-byte v12, v9, v8

    #@50
    .line 291
    move-object v1, v10

    #@51
    .local v1, arr$:[B
    array-length v6, v1

    #@52
    .local v6, len$:I
    const/4 v3, 0x0

    #@53
    .end local v4           #i$:I
    .restart local v3       #i$:I
    move v8, v7

    #@54
    .end local v7           #pos:I
    .restart local v8       #pos:I
    :goto_54
    if-ge v3, v6, :cond_60

    #@56
    aget-byte v2, v1, v3

    #@58
    .line 292
    .local v2, b:B
    add-int/lit8 v7, v8, 0x1

    #@5a
    .end local v8           #pos:I
    .restart local v7       #pos:I
    aput-byte v2, v9, v8

    #@5c
    .line 291
    add-int/lit8 v3, v3, 0x1

    #@5e
    move v8, v7

    #@5f
    .end local v7           #pos:I
    .restart local v8       #pos:I
    goto :goto_54

    #@60
    .line 289
    .end local v2           #b:B
    :cond_60
    add-int/lit8 v3, v4, 0x1

    #@62
    move v4, v3

    #@63
    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_46

    #@64
    .line 295
    .end local v1           #arr$:[B
    .end local v6           #len$:I
    .end local v10           #s:[B
    :cond_64
    return-object v9
.end method

.method public static verifyHostname(Ljava/net/Socket;Ljava/lang/String;)V
    .registers 7
    .parameter "socket"
    .parameter "hostname"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 182
    instance-of v2, p0, Ljavax/net/ssl/SSLSocket;

    #@2
    if-nez v2, :cond_c

    #@4
    .line 183
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v3, "Attempt to verify non-SSL socket"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 186
    :cond_c
    invoke-static {}, Landroid/net/SSLCertificateSocketFactory;->isSslCheckRelaxed()Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_47

    #@12
    move-object v1, p0

    #@13
    .line 189
    check-cast v1, Ljavax/net/ssl/SSLSocket;

    #@15
    .line 190
    .local v1, ssl:Ljavax/net/ssl/SSLSocket;
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    #@18
    .line 192
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    #@1b
    move-result-object v0

    #@1c
    .line 193
    .local v0, session:Ljavax/net/ssl/SSLSession;
    if-nez v0, :cond_26

    #@1e
    .line 194
    new-instance v2, Ljavax/net/ssl/SSLException;

    #@20
    const-string v3, "Cannot verify SSL socket without session"

    #@22
    invoke-direct {v2, v3}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    #@25
    throw v2

    #@26
    .line 196
    :cond_26
    sget-object v2, Landroid/net/SSLCertificateSocketFactory;->HOSTNAME_VERIFIER:Ljavax/net/ssl/HostnameVerifier;

    #@28
    invoke-interface {v2, p1, v0}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    #@2b
    move-result v2

    #@2c
    if-nez v2, :cond_47

    #@2e
    .line 197
    new-instance v2, Ljavax/net/ssl/SSLPeerUnverifiedException;

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v4, "Cannot verify hostname: "

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-direct {v2, v3}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    #@46
    throw v2

    #@47
    .line 200
    .end local v0           #session:Ljavax/net/ssl/SSLSession;
    .end local v1           #ssl:Ljavax/net/ssl/SSLSocket;
    :cond_47
    return-void
.end method


# virtual methods
.method public createSocket()Ljava/net/Socket;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 397
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@a
    .line 398
    .local v0, s:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@c
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    #@f
    .line 399
    iget v1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@11
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHandshakeTimeout(I)V

    #@14
    .line 400
    return-object v0
.end method

.method public createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .registers 5
    .parameter "host"
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 462
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@a
    .line 463
    .local v0, s:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@c
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    #@f
    .line 464
    iget v1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@11
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHandshakeTimeout(I)V

    #@14
    .line 465
    iget-boolean v1, p0, Landroid/net/SSLCertificateSocketFactory;->mSecure:Z

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 466
    invoke-static {v0, p1}, Landroid/net/SSLCertificateSocketFactory;->verifyHostname(Ljava/net/Socket;Ljava/lang/String;)V

    #@1b
    .line 468
    :cond_1b
    return-object v0
.end method

.method public createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .registers 7
    .parameter "host"
    .parameter "port"
    .parameter "localAddr"
    .parameter "localPort"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 444
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@a
    .line 446
    .local v0, s:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@c
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    #@f
    .line 447
    iget v1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@11
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHandshakeTimeout(I)V

    #@14
    .line 448
    iget-boolean v1, p0, Landroid/net/SSLCertificateSocketFactory;->mSecure:Z

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 449
    invoke-static {v0, p1}, Landroid/net/SSLCertificateSocketFactory;->verifyHostname(Ljava/net/Socket;Ljava/lang/String;)V

    #@1b
    .line 451
    :cond_1b
    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .registers 5
    .parameter "addr"
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 429
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@a
    .line 430
    .local v0, s:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@c
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    #@f
    .line 431
    iget v1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@11
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHandshakeTimeout(I)V

    #@14
    .line 432
    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .registers 7
    .parameter "addr"
    .parameter "port"
    .parameter "localAddr"
    .parameter "localPort"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 413
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@a
    .line 415
    .local v0, s:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@c
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    #@f
    .line 416
    iget v1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@11
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHandshakeTimeout(I)V

    #@14
    .line 417
    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .registers 7
    .parameter "k"
    .parameter "host"
    .parameter "port"
    .parameter "close"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 378
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@a
    .line 379
    .local v0, s:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;
    iget-object v1, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@c
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setNpnProtocols([B)V

    #@f
    .line 380
    iget v1, p0, Landroid/net/SSLCertificateSocketFactory;->mHandshakeTimeoutMillis:I

    #@11
    invoke-virtual {v0, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHandshakeTimeout(I)V

    #@14
    .line 381
    iget-boolean v1, p0, Landroid/net/SSLCertificateSocketFactory;->mSecure:Z

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 382
    invoke-static {v0, p2}, Landroid/net/SSLCertificateSocketFactory;->verifyHostname(Ljava/net/Socket;Ljava/lang/String;)V

    #@1b
    .line 384
    :cond_1b
    return-object v0
.end method

.method public getDefaultCipherSuites()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 473
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getSupportedCipherSuites()[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getNpnSelectedProtocol(Ljava/net/Socket;)[B
    .registers 3
    .parameter "socket"

    #@0
    .prologue
    .line 307
    invoke-static {p1}, Landroid/net/SSLCertificateSocketFactory;->castToOpenSSLSocket(Ljava/net/Socket;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->getNpnSelectedProtocol()[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getSupportedCipherSuites()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 478
    invoke-direct {p0}, Landroid/net/SSLCertificateSocketFactory;->getDelegate()Ljavax/net/ssl/SSLSocketFactory;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getSupportedCipherSuites()[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public setHostname(Ljava/net/Socket;Ljava/lang/String;)V
    .registers 4
    .parameter "socket"
    .parameter "hostName"

    #@0
    .prologue
    .line 342
    invoke-static {p1}, Landroid/net/SSLCertificateSocketFactory;->castToOpenSSLSocket(Ljava/net/Socket;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setHostname(Ljava/lang/String;)V

    #@7
    .line 343
    return-void
.end method

.method public setKeyManagers([Ljavax/net/ssl/KeyManager;)V
    .registers 3
    .parameter "keyManagers"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 314
    iput-object p1, p0, Landroid/net/SSLCertificateSocketFactory;->mKeyManagers:[Ljavax/net/ssl/KeyManager;

    #@3
    .line 317
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@5
    .line 318
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@7
    .line 319
    return-void
.end method

.method public setNpnProtocols([[B)V
    .registers 3
    .parameter "npnProtocols"

    #@0
    .prologue
    .line 269
    invoke-static {p1}, Landroid/net/SSLCertificateSocketFactory;->toNpnProtocolsList([[B)[B

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mNpnProtocols:[B

    #@6
    .line 270
    return-void
.end method

.method public setSoWriteTimeout(Ljava/net/Socket;I)V
    .registers 4
    .parameter "socket"
    .parameter "writeTimeoutMilliseconds"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    .line 358
    invoke-static {p1}, Landroid/net/SSLCertificateSocketFactory;->castToOpenSSLSocket(Ljava/net/Socket;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setSoWriteTimeout(I)V

    #@7
    .line 359
    return-void
.end method

.method public setTrustManagers([Ljavax/net/ssl/TrustManager;)V
    .registers 3
    .parameter "trustManager"

    #@0
    .prologue
    .line 244
    iput-object p1, p0, Landroid/net/SSLCertificateSocketFactory;->mTrustManagers:[Ljavax/net/ssl/TrustManager;

    #@2
    .line 247
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/net/SSLCertificateSocketFactory;->mSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@5
    .line 250
    return-void
.end method

.method public setUseSessionTickets(Ljava/net/Socket;Z)V
    .registers 4
    .parameter "socket"
    .parameter "useSessionTickets"

    #@0
    .prologue
    .line 330
    invoke-static {p1}, Landroid/net/SSLCertificateSocketFactory;->castToOpenSSLSocket(Ljava/net/Socket;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;->setUseSessionTickets(Z)V

    #@7
    .line 331
    return-void
.end method
