.class public Landroid/net/MobileDataStateTracker;
.super Ljava/lang/Object;
.source "MobileDataStateTracker.java"

# interfaces
.implements Landroid/net/NetworkStateTracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/MobileDataStateTracker$1;,
        Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;,
        Landroid/net/MobileDataStateTracker$MdstHandler;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "MobileDataStateTracker"

.field private static final VDBG:Z


# instance fields
.field public featureset:Ljava/lang/String;

.field public isBootCompleted:Z

.field private mApnType:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDataConnectionTrackerAc:Lcom/android/internal/util/AsyncChannel;

.field private mDefaultRouteSet:Z

.field private mHandler:Landroid/os/Handler;

.field private mLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mLinkProperties:Landroid/net/LinkProperties;

.field private mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

.field private mMessenger:Landroid/os/Messenger;

.field private mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mPhoneService:Lcom/android/internal/telephony/ITelephony;

.field protected mPolicyDataEnabled:Z

.field private mPrivateDnsRouteSet:Z

.field private mTarget:Landroid/os/Handler;

.field private mTeardownRequested:Z

.field protected mUserDataEnabled:Z

.field private mylgfeature:Lcom/android/internal/telephony/LGfeature;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/android/internal/telephony/LGfeature;)V
    .registers 7
    .parameter "netType"
    .parameter "tag"
    .parameter "lgfeature"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 110
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 77
    iput-boolean v2, p0, Landroid/net/MobileDataStateTracker;->mTeardownRequested:Z

    #@7
    .line 82
    iput-boolean v2, p0, Landroid/net/MobileDataStateTracker;->mPrivateDnsRouteSet:Z

    #@9
    .line 83
    iput-boolean v2, p0, Landroid/net/MobileDataStateTracker;->mDefaultRouteSet:Z

    #@b
    .line 87
    iput-boolean v0, p0, Landroid/net/MobileDataStateTracker;->mUserDataEnabled:Z

    #@d
    .line 88
    iput-boolean v0, p0, Landroid/net/MobileDataStateTracker;->mPolicyDataEnabled:Z

    #@f
    .line 101
    const-string/jumbo v0, "ro.afwdata.LGfeatureset"

    #@12
    const-string/jumbo v1, "none"

    #@15
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/net/MobileDataStateTracker;->featureset:Ljava/lang/String;

    #@1b
    .line 102
    iput-boolean v2, p0, Landroid/net/MobileDataStateTracker;->isBootCompleted:Z

    #@1d
    .line 112
    new-instance v0, Landroid/net/NetworkInfo;

    #@1f
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@26
    move-result v1

    #@27
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkTypeName()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-direct {v0, p1, v1, p2, v2}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@32
    iput-object v0, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@34
    .line 115
    invoke-static {p1}, Landroid/net/MobileDataStateTracker;->networkTypeToApnType(I)Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    iput-object v0, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@3a
    .line 116
    iput-object p3, p0, Landroid/net/MobileDataStateTracker;->mylgfeature:Lcom/android/internal/telephony/LGfeature;

    #@3c
    .line 117
    return-void
.end method

.method static synthetic access$1000(Landroid/net/MobileDataStateTracker;)Landroid/net/LinkCapabilities;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Landroid/net/MobileDataStateTracker;Landroid/net/LinkCapabilities;)Landroid/net/LinkCapabilities;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Landroid/net/MobileDataStateTracker;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mDataConnectionTrackerAc:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Landroid/net/MobileDataStateTracker;)Landroid/os/Messenger;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mMessenger:Landroid/os/Messenger;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Landroid/net/MobileDataStateTracker;Landroid/os/Messenger;)Landroid/os/Messenger;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mMessenger:Landroid/os/Messenger;

    #@2
    return-object p1
.end method

.method static synthetic access$1200(Landroid/net/MobileDataStateTracker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/net/MobileDataStateTracker;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/MobileDataStateTracker;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/net/MobileDataStateTracker;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mTarget:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/net/MobileDataStateTracker;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Landroid/net/MobileDataStateTracker;Lcom/android/internal/telephony/PhoneConstants$DataState;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/net/MobileDataStateTracker;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/net/MobileDataStateTracker;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/net/MobileDataStateTracker;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/net/MobileDataStateTracker;)Landroid/net/LinkProperties;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Landroid/net/MobileDataStateTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private getPhoneService(Z)V
    .registers 3
    .parameter "forceRefresh"

    #@0
    .prologue
    .line 347
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1e

    #@a
    .line 348
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@c
    if-eqz v0, :cond_10

    #@e
    if-eqz p1, :cond_1d

    #@10
    .line 349
    :cond_10
    const-string/jumbo v0, "phone_msim"

    #@13
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@16
    move-result-object v0

    #@17
    invoke-static {v0}, Lcom/android/internal/telephony/msim/ITelephonyMSim$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@1d
    .line 357
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 354
    :cond_1e
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@20
    if-eqz v0, :cond_24

    #@22
    if-eqz p1, :cond_1d

    #@24
    .line 355
    :cond_24
    const-string/jumbo v0, "phone"

    #@27
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@2a
    move-result-object v0

    #@2b
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@31
    goto :goto_1d
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 753
    const-string v0, "MobileDataStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, ": "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 754
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 757
    const-string v0, "MobileDataStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, ": "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 758
    return-void
.end method

.method public static networkTypeToApnType(I)Ljava/lang/String;
    .registers 3
    .parameter "netType"

    #@0
    .prologue
    .line 680
    packed-switch p0, :pswitch_data_60

    #@3
    .line 733
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "Error mapping networkType "

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, " to apnType."

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Landroid/net/MobileDataStateTracker;->sloge(Ljava/lang/String;)V

    #@1f
    .line 734
    const/4 v0, 0x0

    #@20
    :goto_20
    return-object v0

    #@21
    .line 682
    :pswitch_21
    const-string v0, "default"

    #@23
    goto :goto_20

    #@24
    .line 684
    :pswitch_24
    const-string/jumbo v0, "mms"

    #@27
    goto :goto_20

    #@28
    .line 686
    :pswitch_28
    const-string/jumbo v0, "supl"

    #@2b
    goto :goto_20

    #@2c
    .line 688
    :pswitch_2c
    const-string v0, "dun"

    #@2e
    goto :goto_20

    #@2f
    .line 690
    :pswitch_2f
    const-string v0, "hipri"

    #@31
    goto :goto_20

    #@32
    .line 692
    :pswitch_32
    const-string v0, "fota"

    #@34
    goto :goto_20

    #@35
    .line 694
    :pswitch_35
    const-string v0, "ims"

    #@37
    goto :goto_20

    #@38
    .line 696
    :pswitch_38
    const-string v0, "cbs"

    #@3a
    goto :goto_20

    #@3b
    .line 699
    :pswitch_3b
    const-string v0, "admin"

    #@3d
    goto :goto_20

    #@3e
    .line 701
    :pswitch_3e
    const-string/jumbo v0, "vzwapp"

    #@41
    goto :goto_20

    #@42
    .line 703
    :pswitch_42
    const-string/jumbo v0, "vzw800"

    #@45
    goto :goto_20

    #@46
    .line 707
    :pswitch_46
    const-string v0, "entitlement"

    #@48
    goto :goto_20

    #@49
    .line 711
    :pswitch_49
    const-string/jumbo v0, "tethering"

    #@4c
    goto :goto_20

    #@4d
    .line 715
    :pswitch_4d
    const-string/jumbo v0, "ktmultirab1"

    #@50
    goto :goto_20

    #@51
    .line 718
    :pswitch_51
    const-string/jumbo v0, "ktmultirab2"

    #@54
    goto :goto_20

    #@55
    .line 722
    :pswitch_55
    const-string v0, "emergency"

    #@57
    goto :goto_20

    #@58
    .line 726
    :pswitch_58
    const-string v0, "bip"

    #@5a
    goto :goto_20

    #@5b
    .line 730
    :pswitch_5b
    const-string/jumbo v0, "rcs"

    #@5e
    goto :goto_20

    #@5f
    .line 680
    nop

    #@60
    :pswitch_data_60
    .packed-switch 0x0
        :pswitch_21
        :pswitch_3
        :pswitch_24
        :pswitch_28
        :pswitch_2c
        :pswitch_2f
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_32
        :pswitch_35
        :pswitch_38
        :pswitch_3
        :pswitch_3b
        :pswitch_3e
        :pswitch_46
        :pswitch_42
        :pswitch_49
        :pswitch_4d
        :pswitch_51
        :pswitch_5b
        :pswitch_58
        :pswitch_55
    .end packed-switch
.end method

.method private setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "state"
    .parameter "reason"
    .parameter "extraInfo"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 461
    new-instance v4, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string/jumbo v5, "setDetailed state, old ="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    iget-object v5, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@f
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    const-string v5, " and new state="

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-direct {p0, v4}, Landroid/net/MobileDataStateTracker;->log(Ljava/lang/String;)V

    #@28
    .line 463
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2a
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@2d
    move-result-object v4

    #@2e
    if-eq p1, v4, :cond_61

    #@30
    .line 464
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@32
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@35
    move-result-object v4

    #@36
    sget-object v5, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@38
    if-ne v4, v5, :cond_62

    #@3a
    move v2, v3

    #@3b
    .line 465
    .local v2, wasConnecting:Z
    :goto_3b
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@3d
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    .line 471
    .local v0, lastReason:Ljava/lang/String;
    if-eqz v2, :cond_4c

    #@43
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@45
    if-ne p1, v4, :cond_4c

    #@47
    if-nez p2, :cond_4c

    #@49
    if-eqz v0, :cond_4c

    #@4b
    .line 473
    move-object p2, v0

    #@4c
    .line 474
    :cond_4c
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@4e
    invoke-virtual {v4, p1, p2, p3}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@51
    .line 475
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mTarget:Landroid/os/Handler;

    #@53
    new-instance v5, Landroid/net/NetworkInfo;

    #@55
    iget-object v6, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@57
    invoke-direct {v5, v6}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@5a
    invoke-virtual {v4, v3, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5d
    move-result-object v1

    #@5e
    .line 476
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@61
    .line 478
    .end local v0           #lastReason:Ljava/lang/String;
    .end local v1           #msg:Landroid/os/Message;
    .end local v2           #wasConnecting:Z
    :cond_61
    return-void

    #@62
    .line 464
    :cond_62
    const/4 v2, 0x0

    #@63
    goto :goto_3b
.end method

.method private setEnableApn(Ljava/lang/String;Z)I
    .registers 7
    .parameter "apnType"
    .parameter "enable"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 636
    const/4 v2, 0x0

    #@2
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->getPhoneService(Z)V

    #@5
    .line 641
    const/4 v1, 0x0

    #@6
    .local v1, retry:I
    :goto_6
    const/4 v2, 0x2

    #@7
    if-ge v1, v2, :cond_1c

    #@9
    .line 642
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_61

    #@13
    .line 643
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@15
    if-nez v2, :cond_48

    #@17
    .line 644
    const-string v2, "Ignoring feature request because could not acquire MSim Phone Service"

    #@19
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@1c
    .line 675
    :cond_1c
    :goto_1c
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v3, "Could not "

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    if-eqz p2, :cond_82

    #@29
    const-string v2, "enable"

    #@2b
    :goto_2b
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " APN type \""

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, "\""

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@46
    .line 676
    const/4 v2, 0x3

    #@47
    :goto_47
    return v2

    #@48
    .line 649
    :cond_48
    if-eqz p2, :cond_51

    #@4a
    .line 650
    :try_start_4a
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@4c
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->enableApnType(Ljava/lang/String;)I

    #@4f
    move-result v2

    #@50
    goto :goto_47

    #@51
    .line 652
    :cond_51
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@53
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->disableApnType(Ljava/lang/String;)I
    :try_end_56
    .catch Landroid/os/RemoteException; {:try_start_4a .. :try_end_56} :catch_58

    #@56
    move-result v2

    #@57
    goto :goto_47

    #@58
    .line 654
    :catch_58
    move-exception v0

    #@59
    .line 655
    .local v0, e:Landroid/os/RemoteException;
    if-nez v1, :cond_5e

    #@5b
    invoke-direct {p0, v3}, Landroid/net/MobileDataStateTracker;->getPhoneService(Z)V

    #@5e
    .line 641
    :cond_5e
    :goto_5e
    add-int/lit8 v1, v1, 0x1

    #@60
    goto :goto_6

    #@61
    .line 658
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_61
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@63
    if-nez v2, :cond_6b

    #@65
    .line 659
    const-string v2, "Ignoring feature request because could not acquire PhoneService"

    #@67
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@6a
    goto :goto_1c

    #@6b
    .line 664
    :cond_6b
    if-eqz p2, :cond_74

    #@6d
    .line 665
    :try_start_6d
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@6f
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->enableApnType(Ljava/lang/String;)I

    #@72
    move-result v2

    #@73
    goto :goto_47

    #@74
    .line 667
    :cond_74
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@76
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/ITelephony;->disableApnType(Ljava/lang/String;)I
    :try_end_79
    .catch Landroid/os/RemoteException; {:try_start_6d .. :try_end_79} :catch_7b

    #@79
    move-result v2

    #@7a
    goto :goto_47

    #@7b
    .line 669
    :catch_7b
    move-exception v0

    #@7c
    .line 670
    .restart local v0       #e:Landroid/os/RemoteException;
    if-nez v1, :cond_5e

    #@7e
    invoke-direct {p0, v3}, Landroid/net/MobileDataStateTracker;->getPhoneService(Z)V

    #@81
    goto :goto_5e

    #@82
    .line 675
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_82
    const-string v2, "disable"

    #@84
    goto :goto_2b
.end method

.method private static sloge(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 761
    const-string v0, "MobileDataStateTracker"

    #@2
    invoke-static {v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 762
    return-void
.end method


# virtual methods
.method public captivePortalCheckComplete()V
    .registers 1

    #@0
    .prologue
    .line 448
    return-void
.end method

.method public defaultRouteSet(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 192
    iput-boolean p1, p0, Landroid/net/MobileDataStateTracker;->mDefaultRouteSet:Z

    #@2
    .line 193
    return-void
.end method

.method public getLinkCapabilities()Landroid/net/LinkCapabilities;
    .registers 3

    #@0
    .prologue
    .line 749
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkCapabilities;-><init>(Landroid/net/LinkCapabilities;)V

    #@7
    return-object v0
.end method

.method public getLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 742
    new-instance v0, Landroid/net/LinkProperties;

    #@2
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@7
    return-object v0
.end method

.method public getNetworkInfo()Landroid/net/NetworkInfo;
    .registers 2

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method public getTcpBufferSizesPropName()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 371
    const-string/jumbo v0, "unknown"

    #@3
    .line 372
    .local v0, networkTypeStr:Ljava/lang/String;
    new-instance v1, Landroid/telephony/TelephonyManager;

    #@5
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@7
    invoke-direct {v1, v2}, Landroid/telephony/TelephonyManager;-><init>(Landroid/content/Context;)V

    #@a
    .line 374
    .local v1, tm:Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@d
    move-result v2

    #@e
    packed-switch v2, :pswitch_data_70

    #@11
    .line 422
    :pswitch_11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string/jumbo v3, "unknown network type: "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@20
    move-result v3

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@2c
    .line 424
    :goto_2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string/jumbo v3, "net.tcp.buffersize."

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    return-object v2

    #@41
    .line 376
    :pswitch_41
    const-string v0, "gprs"

    #@43
    .line 377
    goto :goto_2c

    #@44
    .line 379
    :pswitch_44
    const-string v0, "edge"

    #@46
    .line 380
    goto :goto_2c

    #@47
    .line 383
    :pswitch_47
    const-string/jumbo v0, "umts"

    #@4a
    .line 384
    goto :goto_2c

    #@4b
    .line 386
    :pswitch_4b
    const-string v0, "hsdpa"

    #@4d
    .line 387
    goto :goto_2c

    #@4e
    .line 389
    :pswitch_4e
    const-string v0, "hsupa"

    #@50
    .line 390
    goto :goto_2c

    #@51
    .line 392
    :pswitch_51
    const-string v0, "hspa"

    #@53
    .line 393
    goto :goto_2c

    #@54
    .line 395
    :pswitch_54
    const-string v0, "hspap"

    #@56
    .line 396
    goto :goto_2c

    #@57
    .line 398
    :pswitch_57
    const-string v0, "cdma"

    #@59
    .line 399
    goto :goto_2c

    #@5a
    .line 401
    :pswitch_5a
    const-string v0, "1xrtt"

    #@5c
    .line 402
    goto :goto_2c

    #@5d
    .line 404
    :pswitch_5d
    const-string v0, "evdo"

    #@5f
    .line 405
    goto :goto_2c

    #@60
    .line 407
    :pswitch_60
    const-string v0, "evdo"

    #@62
    .line 408
    goto :goto_2c

    #@63
    .line 410
    :pswitch_63
    const-string v0, "evdo"

    #@65
    .line 411
    goto :goto_2c

    #@66
    .line 413
    :pswitch_66
    const-string v0, "iden"

    #@68
    .line 414
    goto :goto_2c

    #@69
    .line 416
    :pswitch_69
    const-string/jumbo v0, "lte"

    #@6c
    .line 417
    goto :goto_2c

    #@6d
    .line 419
    :pswitch_6d
    const-string v0, "ehrpd"

    #@6f
    .line 420
    goto :goto_2c

    #@70
    .line 374
    :pswitch_data_70
    .packed-switch 0x1
        :pswitch_41
        :pswitch_44
        :pswitch_47
        :pswitch_57
        :pswitch_5d
        :pswitch_60
        :pswitch_5a
        :pswitch_4b
        :pswitch_4e
        :pswitch_51
        :pswitch_66
        :pswitch_63
        :pswitch_69
        :pswitch_6d
        :pswitch_54
        :pswitch_11
        :pswitch_47
    .end packed-switch
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 363
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDefaultRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 188
    iget-boolean v0, p0, Landroid/net/MobileDataStateTracker;->mDefaultRouteSet:Z

    #@2
    return v0
.end method

.method public isPrivateDnsRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 176
    iget-boolean v0, p0, Landroid/net/MobileDataStateTracker;->mPrivateDnsRouteSet:Z

    #@2
    return v0
.end method

.method public isTeardownRequested()Z
    .registers 2

    #@0
    .prologue
    .line 485
    iget-boolean v0, p0, Landroid/net/MobileDataStateTracker;->mTeardownRequested:Z

    #@2
    return v0
.end method

.method public privateDnsRouteSet(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 180
    iput-boolean p1, p0, Landroid/net/MobileDataStateTracker;->mPrivateDnsRouteSet:Z

    #@2
    .line 181
    return-void
.end method

.method public reconnect()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 493
    const/4 v1, 0x0

    #@2
    .line 494
    .local v1, retValue:Z
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, v2}, Landroid/net/MobileDataStateTracker;->setTeardownRequested(Z)V

    #@6
    .line 496
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->featureset:Ljava/lang/String;

    #@8
    const-string v3, "KTBASE"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_38

    #@10
    .line 497
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "[LGE_DATA] isBootCompleted : "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    iget-boolean v3, p0, Landroid/net/MobileDataStateTracker;->isBootCompleted:Z

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@28
    .line 498
    iget-boolean v2, p0, Landroid/net/MobileDataStateTracker;->isBootCompleted:Z

    #@2a
    if-eqz v2, :cond_38

    #@2c
    .line 499
    new-instance v0, Landroid/content/Intent;

    #@2e
    const-string v2, "com.kt.CALL_PROTECTION_MENU_OFF"

    #@30
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@33
    .line 500
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@35
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@38
    .line 505
    .end local v0           #intent:Landroid/content/Intent;
    :cond_38
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@3a
    const/4 v3, 0x1

    #@3b
    invoke-direct {p0, v2, v3}, Landroid/net/MobileDataStateTracker;->setEnableApn(Ljava/lang/String;Z)I

    #@3e
    move-result v2

    #@3f
    packed-switch v2, :pswitch_data_54

    #@42
    .line 519
    const-string v2, "Error in reconnect - unexpected response."

    #@44
    invoke-direct {p0, v2}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@47
    .line 522
    :goto_47
    :pswitch_47
    return v1

    #@48
    .line 508
    :pswitch_48
    const/4 v1, 0x1

    #@49
    .line 509
    goto :goto_47

    #@4a
    .line 512
    :pswitch_4a
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@4c
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@4e
    invoke-virtual {v2, v3, v4, v4}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@51
    .line 513
    const/4 v1, 0x1

    #@52
    .line 514
    goto :goto_47

    #@53
    .line 505
    nop

    #@54
    :pswitch_data_54
    .packed-switch 0x0
        :pswitch_48
        :pswitch_4a
        :pswitch_47
        :pswitch_47
    .end packed-switch
.end method

.method public releaseWakeLock()V
    .registers 1

    #@0
    .prologue
    .line 199
    return-void
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 6
    .parameter "msger"

    #@0
    .prologue
    .line 341
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mMessenger:Landroid/os/Messenger;

    #@2
    .line 342
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@4
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@7
    .line 343
    .local v0, ac:Lcom/android/internal/util/AsyncChannel;
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@9
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mHandler:Landroid/os/Handler;

    #@b
    iget-object v3, p0, Landroid/net/MobileDataStateTracker;->mMessenger:Landroid/os/Messenger;

    #@d
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@10
    .line 344
    return-void
.end method

.method public setDependencyMet(Z)V
    .registers 7
    .parameter "met"

    #@0
    .prologue
    .line 604
    const-string v3, "apnType"

    #@2
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@4
    invoke-static {v3, v4}, Landroid/os/Bundle;->forPair(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    #@7
    move-result-object v0

    #@8
    .line 606
    .local v0, bundle:Landroid/os/Bundle;
    :try_start_8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v4, "setDependencyMet: E met="

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-direct {p0, v3}, Landroid/net/MobileDataStateTracker;->log(Ljava/lang/String;)V

    #@1f
    .line 607
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@22
    move-result-object v2

    #@23
    .line 608
    .local v2, msg:Landroid/os/Message;
    const v3, 0x4201f

    #@26
    iput v3, v2, Landroid/os/Message;->what:I

    #@28
    .line 609
    if-eqz p1, :cond_36

    #@2a
    const/4 v3, 0x1

    #@2b
    :goto_2b
    iput v3, v2, Landroid/os/Message;->arg1:I

    #@2d
    .line 610
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@30
    .line 611
    iget-object v3, p0, Landroid/net/MobileDataStateTracker;->mDataConnectionTrackerAc:Lcom/android/internal/util/AsyncChannel;

    #@32
    invoke-virtual {v3, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V
    :try_end_35
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_35} :catch_38

    #@35
    .line 616
    .end local v2           #msg:Landroid/os/Message;
    :goto_35
    return-void

    #@36
    .line 609
    .restart local v2       #msg:Landroid/os/Message;
    :cond_36
    const/4 v3, 0x0

    #@37
    goto :goto_2b

    #@38
    .line 613
    .end local v2           #msg:Landroid/os/Message;
    :catch_38
    move-exception v1

    #@39
    .line 614
    .local v1, e:Ljava/lang/NullPointerException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string/jumbo v4, "setDependencyMet: X mAc was null"

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-direct {p0, v3}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@50
    goto :goto_35
.end method

.method public setPolicyDataEnable(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 585
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v2, "setPolicyDataEnable(enabled="

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, ")"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {p0, v1}, Landroid/net/MobileDataStateTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 586
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mDataConnectionTrackerAc:Lcom/android/internal/util/AsyncChannel;

    #@1f
    .line 587
    .local v0, channel:Lcom/android/internal/util/AsyncChannel;
    if-eqz v0, :cond_54

    #@21
    .line 588
    const v2, 0x42020

    #@24
    if-eqz p1, :cond_55

    #@26
    const/4 v1, 0x1

    #@27
    :goto_27
    invoke-virtual {v0, v2, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(II)V

    #@2a
    .line 590
    iput-boolean p1, p0, Landroid/net/MobileDataStateTracker;->mPolicyDataEnabled:Z

    #@2c
    .line 592
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->featureset:Ljava/lang/String;

    #@2e
    const-string v2, "KTBASE"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v1

    #@34
    if-nez v1, :cond_4a

    #@36
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->featureset:Ljava/lang/String;

    #@38
    const-string v2, "SKTBASE"

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v1

    #@3e
    if-nez v1, :cond_4a

    #@40
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->featureset:Ljava/lang/String;

    #@42
    const-string v2, "LGTBASE"

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v1

    #@48
    if-eqz v1, :cond_54

    #@4a
    .line 593
    :cond_4a
    const-string/jumbo v1, "lge.policy_data_enable"

    #@4d
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    .line 597
    :cond_54
    return-void

    #@55
    .line 588
    :cond_55
    const/4 v1, 0x0

    #@56
    goto :goto_27
.end method

.method public setRadio(Z)Z
    .registers 9
    .parameter "turnOn"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 531
    invoke-direct {p0, v5}, Landroid/net/MobileDataStateTracker;->getPhoneService(Z)V

    #@5
    .line 536
    const/4 v3, 0x0

    #@6
    .local v3, retry:I
    :goto_6
    const/4 v6, 0x2

    #@7
    if-ge v3, v6, :cond_1c

    #@9
    .line 537
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_5e

    #@13
    .line 538
    iget-object v6, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@15
    if-nez v6, :cond_39

    #@17
    .line 539
    const-string v4, "Ignoring mobile radio request because could not acquire MSim Phone Service"

    #@19
    invoke-direct {p0, v4}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@1c
    .line 567
    :cond_1c
    :goto_1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v6, "Could not set radio power to "

    #@23
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    if-eqz p1, :cond_76

    #@29
    const-string/jumbo v4, "on"

    #@2c
    :goto_2c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-direct {p0, v4}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@37
    move v2, v5

    #@38
    .line 568
    :cond_38
    :goto_38
    return v2

    #@39
    .line 545
    :cond_39
    const/4 v2, 0x1

    #@3a
    .line 546
    .local v2, result:Z
    const/4 v1, 0x0

    #@3b
    .local v1, i:I
    :goto_3b
    :try_start_3b
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Landroid/telephony/MSimTelephonyManager;->getPhoneCount()I

    #@42
    move-result v6

    #@43
    if-ge v1, v6, :cond_38

    #@45
    .line 547
    if-eqz v2, :cond_53

    #@47
    iget-object v6, p0, Landroid/net/MobileDataStateTracker;->mMSimPhoneService:Lcom/android/internal/telephony/msim/ITelephonyMSim;

    #@49
    invoke-interface {v6, p1, v1}, Lcom/android/internal/telephony/msim/ITelephonyMSim;->setRadio(ZI)Z
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_4c} :catch_55

    #@4c
    move-result v6

    #@4d
    if-eqz v6, :cond_53

    #@4f
    move v2, v4

    #@50
    .line 546
    :goto_50
    add-int/lit8 v1, v1, 0x1

    #@52
    goto :goto_3b

    #@53
    :cond_53
    move v2, v5

    #@54
    .line 547
    goto :goto_50

    #@55
    .line 550
    :catch_55
    move-exception v0

    #@56
    .line 551
    .local v0, e:Landroid/os/RemoteException;
    if-nez v3, :cond_5b

    #@58
    invoke-direct {p0, v4}, Landroid/net/MobileDataStateTracker;->getPhoneService(Z)V

    #@5b
    .line 536
    .end local v1           #i:I
    .end local v2           #result:Z
    :cond_5b
    :goto_5b
    add-int/lit8 v3, v3, 0x1

    #@5d
    goto :goto_6

    #@5e
    .line 554
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_5e
    iget-object v6, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@60
    if-nez v6, :cond_68

    #@62
    .line 555
    const-string v4, "Ignoring mobile radio request because could not acquire PhoneService"

    #@64
    invoke-direct {p0, v4}, Landroid/net/MobileDataStateTracker;->loge(Ljava/lang/String;)V

    #@67
    goto :goto_1c

    #@68
    .line 560
    :cond_68
    :try_start_68
    iget-object v6, p0, Landroid/net/MobileDataStateTracker;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    #@6a
    invoke-interface {v6, p1}, Lcom/android/internal/telephony/ITelephony;->setRadio(Z)Z
    :try_end_6d
    .catch Landroid/os/RemoteException; {:try_start_68 .. :try_end_6d} :catch_6f

    #@6d
    move-result v2

    #@6e
    goto :goto_38

    #@6f
    .line 561
    :catch_6f
    move-exception v0

    #@70
    .line 562
    .restart local v0       #e:Landroid/os/RemoteException;
    if-nez v3, :cond_5b

    #@72
    invoke-direct {p0, v4}, Landroid/net/MobileDataStateTracker;->getPhoneService(Z)V

    #@75
    goto :goto_5b

    #@76
    .line 567
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_76
    const-string/jumbo v4, "off"

    #@79
    goto :goto_2c
.end method

.method public setTeardownRequested(Z)V
    .registers 2
    .parameter "isRequested"

    #@0
    .prologue
    .line 481
    iput-boolean p1, p0, Landroid/net/MobileDataStateTracker;->mTeardownRequested:Z

    #@2
    .line 482
    return-void
.end method

.method public setUserDataEnable(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 573
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v2, "setUserDataEnable: E enabled="

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-direct {p0, v1}, Landroid/net/MobileDataStateTracker;->log(Ljava/lang/String;)V

    #@17
    .line 574
    iget-object v0, p0, Landroid/net/MobileDataStateTracker;->mDataConnectionTrackerAc:Lcom/android/internal/util/AsyncChannel;

    #@19
    .line 575
    .local v0, channel:Lcom/android/internal/util/AsyncChannel;
    if-eqz v0, :cond_26

    #@1b
    .line 576
    const v2, 0x4201e

    #@1e
    if-eqz p1, :cond_27

    #@20
    const/4 v1, 0x1

    #@21
    :goto_21
    invoke-virtual {v0, v2, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(II)V

    #@24
    .line 578
    iput-boolean p1, p0, Landroid/net/MobileDataStateTracker;->mUserDataEnabled:Z

    #@26
    .line 581
    :cond_26
    return-void

    #@27
    .line 576
    :cond_27
    const/4 v1, 0x0

    #@28
    goto :goto_21
.end method

.method public startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 126
    iput-object p2, p0, Landroid/net/MobileDataStateTracker;->mTarget:Landroid/os/Handler;

    #@2
    .line 127
    iput-object p1, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@4
    .line 129
    new-instance v1, Landroid/net/MobileDataStateTracker$MdstHandler;

    #@6
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v1, v2, p0}, Landroid/net/MobileDataStateTracker$MdstHandler;-><init>(Landroid/os/Looper;Landroid/net/MobileDataStateTracker;)V

    #@d
    iput-object v1, p0, Landroid/net/MobileDataStateTracker;->mHandler:Landroid/os/Handler;

    #@f
    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    #@11
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@14
    .line 132
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.ANY_DATA_STATE"

    #@16
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19
    .line 133
    const-string v1, "android.intent.action.DATA_CONNECTION_FAILED"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 134
    sget-object v1, Lcom/android/internal/telephony/DctConstants;->ACTION_DATA_CONNECTION_TRACKER_MESSENGER:Ljava/lang/String;

    #@20
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 136
    iget-object v1, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@25
    new-instance v2, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;

    #@27
    const/4 v3, 0x0

    #@28
    invoke-direct {v2, p0, v3}, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;-><init>(Landroid/net/MobileDataStateTracker;Landroid/net/MobileDataStateTracker$1;)V

    #@2b
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2e
    .line 137
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@30
    iput-object v1, p0, Landroid/net/MobileDataStateTracker;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@32
    .line 138
    return-void
.end method

.method public teardown()Z
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 433
    invoke-virtual {p0, v2}, Landroid/net/MobileDataStateTracker;->setTeardownRequested(Z)V

    #@5
    .line 436
    const-string v4, "KTBASE"

    #@7
    const-string/jumbo v5, "ro.afwdata.LGfeatureset"

    #@a
    const-string/jumbo v6, "none"

    #@d
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    .line 437
    .local v1, isKTBASE:Z
    iget-boolean v4, p0, Landroid/net/MobileDataStateTracker;->isBootCompleted:Z

    #@17
    if-eqz v4, :cond_27

    #@19
    if-eqz v1, :cond_27

    #@1b
    .line 438
    new-instance v0, Landroid/content/Intent;

    #@1d
    const-string v4, "com.kt.CALL_PROTECTION_MENU_ON"

    #@1f
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@22
    .line 439
    .local v0, intent:Landroid/content/Intent;
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mContext:Landroid/content/Context;

    #@24
    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@27
    .line 442
    .end local v0           #intent:Landroid/content/Intent;
    :cond_27
    iget-object v4, p0, Landroid/net/MobileDataStateTracker;->mApnType:Ljava/lang/String;

    #@29
    invoke-direct {p0, v4, v3}, Landroid/net/MobileDataStateTracker;->setEnableApn(Ljava/lang/String;Z)I

    #@2c
    move-result v4

    #@2d
    const/4 v5, 0x3

    #@2e
    if-eq v4, v5, :cond_31

    #@30
    :goto_30
    return v2

    #@31
    :cond_31
    move v2, v3

    #@32
    goto :goto_30
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 620
    new-instance v1, Ljava/io/CharArrayWriter;

    #@2
    invoke-direct {v1}, Ljava/io/CharArrayWriter;-><init>()V

    #@5
    .line 621
    .local v1, writer:Ljava/io/CharArrayWriter;
    new-instance v0, Ljava/io/PrintWriter;

    #@7
    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@a
    .line 622
    .local v0, pw:Ljava/io/PrintWriter;
    const-string v2, "Mobile data state: "

    #@c
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f
    iget-object v2, p0, Landroid/net/MobileDataStateTracker;->mMobileDataState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@11
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@14
    .line 623
    const-string v2, "Data enabled: user="

    #@16
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    iget-boolean v2, p0, Landroid/net/MobileDataStateTracker;->mUserDataEnabled:Z

    #@1b
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Z)V

    #@1e
    .line 624
    const-string v2, ", policy="

    #@20
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    iget-boolean v2, p0, Landroid/net/MobileDataStateTracker;->mPolicyDataEnabled:Z

    #@25
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Z)V

    #@28
    .line 625
    invoke-virtual {v1}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    return-object v2
.end method
