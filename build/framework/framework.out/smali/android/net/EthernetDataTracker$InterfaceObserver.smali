.class Landroid/net/EthernetDataTracker$InterfaceObserver;
.super Landroid/net/INetworkManagementEventObserver$Stub;
.source "EthernetDataTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/EthernetDataTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InterfaceObserver"
.end annotation


# instance fields
.field private mTracker:Landroid/net/EthernetDataTracker;


# direct methods
.method constructor <init>(Landroid/net/EthernetDataTracker;)V
    .registers 2
    .parameter "tracker"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/net/INetworkManagementEventObserver$Stub;-><init>()V

    #@3
    .line 72
    iput-object p1, p0, Landroid/net/EthernetDataTracker$InterfaceObserver;->mTracker:Landroid/net/EthernetDataTracker;

    #@5
    .line 73
    return-void
.end method


# virtual methods
.method public interfaceAdded(Ljava/lang/String;)V
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/net/EthernetDataTracker$InterfaceObserver;->mTracker:Landroid/net/EthernetDataTracker;

    #@2
    invoke-static {v0, p1}, Landroid/net/EthernetDataTracker;->access$300(Landroid/net/EthernetDataTracker;Ljava/lang/String;)V

    #@5
    .line 96
    return-void
.end method

.method public interfaceClassDataActivityChanged(Ljava/lang/String;Z)V
    .registers 3
    .parameter "label"
    .parameter "active"

    #@0
    .prologue
    .line 108
    return-void
.end method

.method public interfaceLinkStateChanged(Ljava/lang/String;Z)V
    .registers 6
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 80
    invoke-static {}, Landroid/net/EthernetDataTracker;->access$000()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_4a

    #@a
    invoke-static {}, Landroid/net/EthernetDataTracker;->access$100()Z

    #@d
    move-result v0

    #@e
    if-eq v0, p2, :cond_4a

    #@10
    .line 81
    const-string v1, "Ethernet"

    #@12
    new-instance v0, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "Interface "

    #@19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v2, " link "

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    if-eqz p2, :cond_4b

    #@29
    const-string/jumbo v0, "up"

    #@2c
    :goto_2c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 82
    invoke-static {p2}, Landroid/net/EthernetDataTracker;->access$102(Z)Z

    #@3a
    .line 83
    iget-object v0, p0, Landroid/net/EthernetDataTracker$InterfaceObserver;->mTracker:Landroid/net/EthernetDataTracker;

    #@3c
    invoke-static {v0}, Landroid/net/EthernetDataTracker;->access$200(Landroid/net/EthernetDataTracker;)Landroid/net/NetworkInfo;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0, p2}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@43
    .line 86
    if-eqz p2, :cond_4e

    #@45
    .line 87
    iget-object v0, p0, Landroid/net/EthernetDataTracker$InterfaceObserver;->mTracker:Landroid/net/EthernetDataTracker;

    #@47
    invoke-virtual {v0}, Landroid/net/EthernetDataTracker;->reconnect()Z

    #@4a
    .line 92
    :cond_4a
    :goto_4a
    return-void

    #@4b
    .line 81
    :cond_4b
    const-string v0, "down"

    #@4d
    goto :goto_2c

    #@4e
    .line 89
    :cond_4e
    iget-object v0, p0, Landroid/net/EthernetDataTracker$InterfaceObserver;->mTracker:Landroid/net/EthernetDataTracker;

    #@50
    invoke-virtual {v0}, Landroid/net/EthernetDataTracker;->disconnect()V

    #@53
    goto :goto_4a
.end method

.method public interfaceRemoved(Ljava/lang/String;)V
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/net/EthernetDataTracker$InterfaceObserver;->mTracker:Landroid/net/EthernetDataTracker;

    #@2
    invoke-static {v0, p1}, Landroid/net/EthernetDataTracker;->access$400(Landroid/net/EthernetDataTracker;Ljava/lang/String;)V

    #@5
    .line 100
    return-void
.end method

.method public interfaceStatusChanged(Ljava/lang/String;Z)V
    .registers 6
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 76
    const-string v1, "Ethernet"

    #@2
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Interface status changed: "

    #@9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    if-eqz p2, :cond_22

    #@13
    const-string/jumbo v0, "up"

    #@16
    :goto_16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 77
    return-void

    #@22
    .line 76
    :cond_22
    const-string v0, "down"

    #@24
    goto :goto_16
.end method

.method public limitReached(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "limitName"
    .parameter "iface"

    #@0
    .prologue
    .line 104
    return-void
.end method
