.class public Landroid/net/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# static fields
.field public static final RESET_ALL_ADDRESSES:I = 0x3

.field public static final RESET_IPV4_ADDRESSES:I = 0x1

.field public static final RESET_IPV6_ADDRESSES:I = 0x2

.field private static final TAG:Ljava/lang/String; = "NetworkUtils"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addressTypeMatches(Ljava/net/InetAddress;Ljava/net/InetAddress;)Z
    .registers 3
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    .line 215
    instance-of v0, p0, Ljava/net/Inet4Address;

    #@2
    if-eqz v0, :cond_8

    #@4
    instance-of v0, p1, Ljava/net/Inet4Address;

    #@6
    if-nez v0, :cond_10

    #@8
    :cond_8
    instance-of v0, p0, Ljava/net/Inet6Address;

    #@a
    if-eqz v0, :cond_12

    #@c
    instance-of v0, p1, Ljava/net/Inet6Address;

    #@e
    if-eqz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public static native disableInterface(Ljava/lang/String;)I
.end method

.method public static native enableInterface(Ljava/lang/String;)I
.end method

.method public static native getDhcpError()Ljava/lang/String;
.end method

.method public static getNetworkPart(Ljava/net/InetAddress;I)Ljava/net/InetAddress;
    .registers 11
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 179
    if-nez p0, :cond_a

    #@2
    .line 180
    new-instance v6, Ljava/lang/RuntimeException;

    #@4
    const-string v7, "getNetworkPart doesn\'t accept null address"

    #@6
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@9
    throw v6

    #@a
    .line 183
    :cond_a
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    #@d
    move-result-object v0

    #@e
    .line 185
    .local v0, array:[B
    if-ltz p1, :cond_15

    #@10
    array-length v6, v0

    #@11
    mul-int/lit8 v6, v6, 0x8

    #@13
    if-le p1, v6, :cond_1d

    #@15
    .line 186
    :cond_15
    new-instance v6, Ljava/lang/RuntimeException;

    #@17
    const-string v7, "getNetworkPart - bad prefixLength"

    #@19
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v6

    #@1d
    .line 189
    :cond_1d
    div-int/lit8 v4, p1, 0x8

    #@1f
    .line 190
    .local v4, offset:I
    rem-int/lit8 v5, p1, 0x8

    #@21
    .line 191
    .local v5, reminder:I
    const/16 v6, 0xff

    #@23
    rsub-int/lit8 v7, v5, 0x8

    #@25
    shl-int/2addr v6, v7

    #@26
    int-to-byte v2, v6

    #@27
    .line 193
    .local v2, mask:B
    array-length v6, v0

    #@28
    if-ge v4, v6, :cond_30

    #@2a
    aget-byte v6, v0, v4

    #@2c
    and-int/2addr v6, v2

    #@2d
    int-to-byte v6, v6

    #@2e
    aput-byte v6, v0, v4

    #@30
    .line 195
    :cond_30
    add-int/lit8 v4, v4, 0x1

    #@32
    .line 197
    :goto_32
    array-length v6, v0

    #@33
    if-ge v4, v6, :cond_3b

    #@35
    .line 198
    const/4 v6, 0x0

    #@36
    aput-byte v6, v0, v4

    #@38
    .line 197
    add-int/lit8 v4, v4, 0x1

    #@3a
    goto :goto_32

    #@3b
    .line 201
    :cond_3b
    const/4 v3, 0x0

    #@3c
    .line 203
    .local v3, netPart:Ljava/net/InetAddress;
    :try_start_3c
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_3f
    .catch Ljava/net/UnknownHostException; {:try_start_3c .. :try_end_3f} :catch_41

    #@3f
    move-result-object v3

    #@40
    .line 207
    return-object v3

    #@41
    .line 204
    :catch_41
    move-exception v1

    #@42
    .line 205
    .local v1, e:Ljava/net/UnknownHostException;
    new-instance v6, Ljava/lang/RuntimeException;

    #@44
    new-instance v7, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v8, "getNetworkPart error - "

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v6
.end method

.method public static hexToInet6Address(Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 7
    .parameter "addrHexString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 229
    :try_start_0
    const-string v1, "%s:%s:%s:%s:%s:%s:%s:%s"

    #@2
    const/16 v2, 0x8

    #@4
    new-array v2, v2, [Ljava/lang/Object;

    #@6
    const/4 v3, 0x0

    #@7
    const/4 v4, 0x0

    #@8
    const/4 v5, 0x4

    #@9
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c
    move-result-object v4

    #@d
    aput-object v4, v2, v3

    #@f
    const/4 v3, 0x1

    #@10
    const/4 v4, 0x4

    #@11
    const/16 v5, 0x8

    #@13
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    aput-object v4, v2, v3

    #@19
    const/4 v3, 0x2

    #@1a
    const/16 v4, 0x8

    #@1c
    const/16 v5, 0xc

    #@1e
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    aput-object v4, v2, v3

    #@24
    const/4 v3, 0x3

    #@25
    const/16 v4, 0xc

    #@27
    const/16 v5, 0x10

    #@29
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    aput-object v4, v2, v3

    #@2f
    const/4 v3, 0x4

    #@30
    const/16 v4, 0x10

    #@32
    const/16 v5, 0x14

    #@34
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    aput-object v4, v2, v3

    #@3a
    const/4 v3, 0x5

    #@3b
    const/16 v4, 0x14

    #@3d
    const/16 v5, 0x18

    #@3f
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    aput-object v4, v2, v3

    #@45
    const/4 v3, 0x6

    #@46
    const/16 v4, 0x18

    #@48
    const/16 v5, 0x1c

    #@4a
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    aput-object v4, v2, v3

    #@50
    const/4 v3, 0x7

    #@51
    const/16 v4, 0x1c

    #@53
    const/16 v5, 0x20

    #@55
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    aput-object v4, v2, v3

    #@5b
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_62
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_62} :catch_64

    #@62
    move-result-object v1

    #@63
    return-object v1

    #@64
    .line 234
    :catch_64
    move-exception v0

    #@65
    .line 235
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "NetworkUtils"

    #@67
    new-instance v2, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v3, "error in hexToInet6Address("

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    const-string v3, "): "

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v2

    #@84
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 236
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@89
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    #@8c
    throw v1
.end method

.method public static inetAddressToInt(Ljava/net/InetAddress;)I
    .registers 4
    .parameter "inetAddr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    #@3
    move-result-object v0

    #@4
    .line 130
    .local v0, addr:[B
    array-length v1, v0

    #@5
    const/4 v2, 0x4

    #@6
    if-eq v1, v2, :cond_10

    #@8
    .line 131
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v2, "Not an IPv4 address"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 133
    :cond_10
    const/4 v1, 0x3

    #@11
    aget-byte v1, v0, v1

    #@13
    and-int/lit16 v1, v1, 0xff

    #@15
    shl-int/lit8 v1, v1, 0x18

    #@17
    const/4 v2, 0x2

    #@18
    aget-byte v2, v0, v2

    #@1a
    and-int/lit16 v2, v2, 0xff

    #@1c
    shl-int/lit8 v2, v2, 0x10

    #@1e
    or-int/2addr v1, v2

    #@1f
    const/4 v2, 0x1

    #@20
    aget-byte v2, v0, v2

    #@22
    and-int/lit16 v2, v2, 0xff

    #@24
    shl-int/lit8 v2, v2, 0x8

    #@26
    or-int/2addr v1, v2

    #@27
    const/4 v2, 0x0

    #@28
    aget-byte v2, v0, v2

    #@2a
    and-int/lit16 v2, v2, 0xff

    #@2c
    or-int/2addr v1, v2

    #@2d
    return v1
.end method

.method public static intToInetAddress(I)Ljava/net/InetAddress;
    .registers 5
    .parameter "hostAddress"

    #@0
    .prologue
    .line 110
    const/4 v2, 0x4

    #@1
    new-array v0, v2, [B

    #@3
    const/4 v2, 0x0

    #@4
    and-int/lit16 v3, p0, 0xff

    #@6
    int-to-byte v3, v3

    #@7
    aput-byte v3, v0, v2

    #@9
    const/4 v2, 0x1

    #@a
    shr-int/lit8 v3, p0, 0x8

    #@c
    and-int/lit16 v3, v3, 0xff

    #@e
    int-to-byte v3, v3

    #@f
    aput-byte v3, v0, v2

    #@11
    const/4 v2, 0x2

    #@12
    shr-int/lit8 v3, p0, 0x10

    #@14
    and-int/lit16 v3, v3, 0xff

    #@16
    int-to-byte v3, v3

    #@17
    aput-byte v3, v0, v2

    #@19
    const/4 v2, 0x3

    #@1a
    shr-int/lit8 v3, p0, 0x18

    #@1c
    and-int/lit16 v3, v3, 0xff

    #@1e
    int-to-byte v3, v3

    #@1f
    aput-byte v3, v0, v2

    #@21
    .line 116
    .local v0, addressBytes:[B
    :try_start_21
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_24
    .catch Ljava/net/UnknownHostException; {:try_start_21 .. :try_end_24} :catch_26

    #@24
    move-result-object v2

    #@25
    return-object v2

    #@26
    .line 117
    :catch_26
    move-exception v1

    #@27
    .line 118
    .local v1, e:Ljava/net/UnknownHostException;
    new-instance v2, Ljava/lang/AssertionError;

    #@29
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@2c
    throw v2
.end method

.method public static makeStrings(Ljava/util/Collection;)[Ljava/lang/String;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 246
    .local p0, addrs:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    #@3
    move-result v5

    #@4
    new-array v4, v5, [Ljava/lang/String;

    #@6
    .line 247
    .local v4, result:[Ljava/lang/String;
    const/4 v1, 0x0

    #@7
    .line 248
    .local v1, i:I
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v3

    #@b
    .local v3, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_21

    #@11
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Ljava/net/InetAddress;

    #@17
    .line 249
    .local v0, addr:Ljava/net/InetAddress;
    add-int/lit8 v2, v1, 0x1

    #@19
    .end local v1           #i:I
    .local v2, i:I
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    aput-object v5, v4, v1

    #@1f
    move v1, v2

    #@20
    .end local v2           #i:I
    .restart local v1       #i:I
    goto :goto_b

    #@21
    .line 251
    .end local v0           #addr:Ljava/net/InetAddress;
    :cond_21
    return-object v4
.end method

.method public static netmaskIntToPrefixLength(I)I
    .registers 2
    .parameter "netmask"

    #@0
    .prologue
    .line 157
    invoke-static {p0}, Ljava/lang/Integer;->bitCount(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 2
    .parameter "addrString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 170
    invoke-static {p0}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static prefixLengthToNetmaskInt(I)I
    .registers 4
    .parameter "prefixLength"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 144
    if-ltz p0, :cond_6

    #@2
    const/16 v1, 0x20

    #@4
    if-le p0, v1, :cond_e

    #@6
    .line 145
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Invalid prefix length (0 <= prefix <= 32)"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 147
    :cond_e
    const/4 v1, -0x1

    #@f
    rsub-int/lit8 v2, p0, 0x20

    #@11
    shl-int v0, v1, v2

    #@13
    .line 148
    .local v0, value:I
    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    #@16
    move-result v1

    #@17
    return v1
.end method

.method public static native releaseDhcpLease(Ljava/lang/String;)Z
.end method

.method public static native resetConnections(Ljava/lang/String;I)I
.end method

.method public static native runDhcp(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)Z
.end method

.method public static native runDhcpRenew(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)Z
.end method

.method public static native stopDhcp(Ljava/lang/String;)Z
.end method

.method public static trimV4AddrZeros(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "addr"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    .line 264
    if-nez p0, :cond_6

    #@4
    const/4 p0, 0x0

    #@5
    .line 279
    .end local p0
    .local v3, octets:[Ljava/lang/String;
    :cond_5
    :goto_5
    return-object p0

    #@6
    .line 265
    .end local v3           #octets:[Ljava/lang/String;
    .restart local p0
    :cond_6
    const-string v5, "\\."

    #@8
    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    .line 266
    .restart local v3       #octets:[Ljava/lang/String;
    array-length v5, v3

    #@d
    if-ne v5, v7, :cond_5

    #@f
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    #@11
    const/16 v5, 0x10

    #@13
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@16
    .line 268
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    #@17
    .line 269
    .local v4, result:Ljava/lang/String;
    const/4 v2, 0x0

    #@18
    .local v2, i:I
    :goto_18
    if-ge v2, v7, :cond_37

    #@1a
    .line 271
    :try_start_1a
    aget-object v5, v3, v2

    #@1c
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@1f
    move-result v5

    #@20
    if-gt v5, v6, :cond_5

    #@22
    .line 272
    aget-object v5, v3, v2

    #@24
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@27
    move-result v5

    #@28
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_2b
    .catch Ljava/lang/NumberFormatException; {:try_start_1a .. :try_end_2b} :catch_35

    #@2b
    .line 276
    if-ge v2, v6, :cond_32

    #@2d
    const/16 v5, 0x2e

    #@2f
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@32
    .line 269
    :cond_32
    add-int/lit8 v2, v2, 0x1

    #@34
    goto :goto_18

    #@35
    .line 273
    :catch_35
    move-exception v1

    #@36
    .line 274
    .local v1, e:Ljava/lang/NumberFormatException;
    goto :goto_5

    #@37
    .line 278
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :cond_37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    move-object p0, v4

    #@3c
    .line 279
    goto :goto_5
.end method
