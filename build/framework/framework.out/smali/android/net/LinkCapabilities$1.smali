.class final Landroid/net/LinkCapabilities$1;
.super Ljava/lang/Object;
.source "LinkCapabilities.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/LinkCapabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/LinkCapabilities;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 655
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/LinkCapabilities;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    .line 657
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    #@5
    .line 658
    .local v0, capabilities:Landroid/net/LinkCapabilities;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v2

    #@9
    .local v2, size:I
    move v3, v2

    #@a
    .line 659
    .end local v2           #size:I
    .local v3, size:I
    :goto_a
    add-int/lit8 v2, v3, -0x1

    #@c
    .end local v3           #size:I
    .restart local v2       #size:I
    if-eqz v3, :cond_23

    #@e
    .line 660
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v1

    #@12
    .line 661
    .local v1, key:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    .line 662
    .local v4, value:Ljava/lang/String;
    invoke-static {v0}, Landroid/net/LinkCapabilities;->access$000(Landroid/net/LinkCapabilities;)Ljava/util/HashMap;

    #@19
    move-result-object v5

    #@1a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move v3, v2

    #@22
    .line 663
    .end local v2           #size:I
    .restart local v3       #size:I
    goto :goto_a

    #@23
    .line 664
    .end local v1           #key:I
    .end local v3           #size:I
    .end local v4           #value:Ljava/lang/String;
    .restart local v2       #size:I
    :cond_23
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 655
    invoke-virtual {p0, p1}, Landroid/net/LinkCapabilities$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/LinkCapabilities;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/LinkCapabilities;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 668
    new-array v0, p1, [Landroid/net/LinkCapabilities;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 655
    invoke-virtual {p0, p1}, Landroid/net/LinkCapabilities$1;->newArray(I)[Landroid/net/LinkCapabilities;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
