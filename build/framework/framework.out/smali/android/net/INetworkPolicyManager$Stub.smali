.class public abstract Landroid/net/INetworkPolicyManager$Stub;
.super Landroid/os/Binder;
.source "INetworkPolicyManager.java"

# interfaces
.implements Landroid/net/INetworkPolicyManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/INetworkPolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/INetworkPolicyManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.INetworkPolicyManager"

.field static final TRANSACTION_getNetworkPolicies:I = 0x8

.field static final TRANSACTION_getNetworkQuotaInfo:I = 0xc

.field static final TRANSACTION_getRestrictBackground:I = 0xb

.field static final TRANSACTION_getUidPolicy:I = 0x2

.field static final TRANSACTION_getUidsWithPolicy:I = 0x3

.field static final TRANSACTION_isNetworkMetered:I = 0xd

.field static final TRANSACTION_isUidForeground:I = 0x4

.field static final TRANSACTION_registerListener:I = 0x5

.field static final TRANSACTION_setNetworkPolicies:I = 0x7

.field static final TRANSACTION_setRestrictBackground:I = 0xa

.field static final TRANSACTION_setUidPolicy:I = 0x1

.field static final TRANSACTION_snoozeLimit:I = 0x9

.field static final TRANSACTION_unregisterListener:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.net.INetworkPolicyManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/INetworkPolicyManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.net.INetworkPolicyManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/INetworkPolicyManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/net/INetworkPolicyManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/net/INetworkPolicyManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/INetworkPolicyManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_148

    #@5
    .line 194
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 47
    :sswitch_a
    const-string v3, "android.net.INetworkPolicyManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v3, "android.net.INetworkPolicyManager"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 56
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v1

    #@1d
    .line 57
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/INetworkPolicyManager$Stub;->setUidPolicy(II)V

    #@20
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    goto :goto_9

    #@24
    .line 63
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_24
    const-string v3, "android.net.INetworkPolicyManager"

    #@26
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v0

    #@2d
    .line 66
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->getUidPolicy(I)I

    #@30
    move-result v2

    #@31
    .line 67
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34
    .line 68
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    goto :goto_9

    #@38
    .line 73
    .end local v0           #_arg0:I
    .end local v2           #_result:I
    :sswitch_38
    const-string v3, "android.net.INetworkPolicyManager"

    #@3a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v0

    #@41
    .line 76
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->getUidsWithPolicy(I)[I

    #@44
    move-result-object v2

    #@45
    .line 77
    .local v2, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    .line 78
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    #@4b
    goto :goto_9

    #@4c
    .line 83
    .end local v0           #_arg0:I
    .end local v2           #_result:[I
    :sswitch_4c
    const-string v5, "android.net.INetworkPolicyManager"

    #@4e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@51
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v0

    #@55
    .line 86
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->isUidForeground(I)Z

    #@58
    move-result v2

    #@59
    .line 87
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    .line 88
    if-eqz v2, :cond_5f

    #@5e
    move v3, v4

    #@5f
    :cond_5f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@62
    goto :goto_9

    #@63
    .line 93
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_63
    const-string v3, "android.net.INetworkPolicyManager"

    #@65
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68
    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@6b
    move-result-object v3

    #@6c
    invoke-static {v3}, Landroid/net/INetworkPolicyListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkPolicyListener;

    #@6f
    move-result-object v0

    #@70
    .line 96
    .local v0, _arg0:Landroid/net/INetworkPolicyListener;
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->registerListener(Landroid/net/INetworkPolicyListener;)V

    #@73
    .line 97
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@76
    goto :goto_9

    #@77
    .line 102
    .end local v0           #_arg0:Landroid/net/INetworkPolicyListener;
    :sswitch_77
    const-string v3, "android.net.INetworkPolicyManager"

    #@79
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7f
    move-result-object v3

    #@80
    invoke-static {v3}, Landroid/net/INetworkPolicyListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkPolicyListener;

    #@83
    move-result-object v0

    #@84
    .line 105
    .restart local v0       #_arg0:Landroid/net/INetworkPolicyListener;
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->unregisterListener(Landroid/net/INetworkPolicyListener;)V

    #@87
    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    goto/16 :goto_9

    #@8c
    .line 111
    .end local v0           #_arg0:Landroid/net/INetworkPolicyListener;
    :sswitch_8c
    const-string v3, "android.net.INetworkPolicyManager"

    #@8e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@91
    .line 113
    sget-object v3, Landroid/net/NetworkPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    #@93
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@96
    move-result-object v0

    #@97
    check-cast v0, [Landroid/net/NetworkPolicy;

    #@99
    .line 114
    .local v0, _arg0:[Landroid/net/NetworkPolicy;
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->setNetworkPolicies([Landroid/net/NetworkPolicy;)V

    #@9c
    .line 115
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f
    goto/16 :goto_9

    #@a1
    .line 120
    .end local v0           #_arg0:[Landroid/net/NetworkPolicy;
    :sswitch_a1
    const-string v3, "android.net.INetworkPolicyManager"

    #@a3
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a6
    .line 121
    invoke-virtual {p0}, Landroid/net/INetworkPolicyManager$Stub;->getNetworkPolicies()[Landroid/net/NetworkPolicy;

    #@a9
    move-result-object v2

    #@aa
    .line 122
    .local v2, _result:[Landroid/net/NetworkPolicy;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad
    .line 123
    invoke-virtual {p3, v2, v4}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@b0
    goto/16 :goto_9

    #@b2
    .line 128
    .end local v2           #_result:[Landroid/net/NetworkPolicy;
    :sswitch_b2
    const-string v3, "android.net.INetworkPolicyManager"

    #@b4
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b7
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ba
    move-result v3

    #@bb
    if-eqz v3, :cond_cd

    #@bd
    .line 131
    sget-object v3, Landroid/net/NetworkTemplate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@bf
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c2
    move-result-object v0

    #@c3
    check-cast v0, Landroid/net/NetworkTemplate;

    #@c5
    .line 136
    .local v0, _arg0:Landroid/net/NetworkTemplate;
    :goto_c5
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->snoozeLimit(Landroid/net/NetworkTemplate;)V

    #@c8
    .line 137
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cb
    goto/16 :goto_9

    #@cd
    .line 134
    .end local v0           #_arg0:Landroid/net/NetworkTemplate;
    :cond_cd
    const/4 v0, 0x0

    #@ce
    .restart local v0       #_arg0:Landroid/net/NetworkTemplate;
    goto :goto_c5

    #@cf
    .line 142
    .end local v0           #_arg0:Landroid/net/NetworkTemplate;
    :sswitch_cf
    const-string v5, "android.net.INetworkPolicyManager"

    #@d1
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d4
    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d7
    move-result v5

    #@d8
    if-eqz v5, :cond_e3

    #@da
    move v0, v4

    #@db
    .line 145
    .local v0, _arg0:Z
    :goto_db
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->setRestrictBackground(Z)V

    #@de
    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e1
    goto/16 :goto_9

    #@e3
    .end local v0           #_arg0:Z
    :cond_e3
    move v0, v3

    #@e4
    .line 144
    goto :goto_db

    #@e5
    .line 151
    :sswitch_e5
    const-string v5, "android.net.INetworkPolicyManager"

    #@e7
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ea
    .line 152
    invoke-virtual {p0}, Landroid/net/INetworkPolicyManager$Stub;->getRestrictBackground()Z

    #@ed
    move-result v2

    #@ee
    .line 153
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f1
    .line 154
    if-eqz v2, :cond_f4

    #@f3
    move v3, v4

    #@f4
    :cond_f4
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@f7
    goto/16 :goto_9

    #@f9
    .line 159
    .end local v2           #_result:Z
    :sswitch_f9
    const-string v5, "android.net.INetworkPolicyManager"

    #@fb
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fe
    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@101
    move-result v5

    #@102
    if-eqz v5, :cond_11d

    #@104
    .line 162
    sget-object v5, Landroid/net/NetworkState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@106
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@109
    move-result-object v0

    #@10a
    check-cast v0, Landroid/net/NetworkState;

    #@10c
    .line 167
    .local v0, _arg0:Landroid/net/NetworkState;
    :goto_10c
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->getNetworkQuotaInfo(Landroid/net/NetworkState;)Landroid/net/NetworkQuotaInfo;

    #@10f
    move-result-object v2

    #@110
    .line 168
    .local v2, _result:Landroid/net/NetworkQuotaInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@113
    .line 169
    if-eqz v2, :cond_11f

    #@115
    .line 170
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@118
    .line 171
    invoke-virtual {v2, p3, v4}, Landroid/net/NetworkQuotaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 165
    .end local v0           #_arg0:Landroid/net/NetworkState;
    .end local v2           #_result:Landroid/net/NetworkQuotaInfo;
    :cond_11d
    const/4 v0, 0x0

    #@11e
    .restart local v0       #_arg0:Landroid/net/NetworkState;
    goto :goto_10c

    #@11f
    .line 174
    .restart local v2       #_result:Landroid/net/NetworkQuotaInfo;
    :cond_11f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@122
    goto/16 :goto_9

    #@124
    .line 180
    .end local v0           #_arg0:Landroid/net/NetworkState;
    .end local v2           #_result:Landroid/net/NetworkQuotaInfo;
    :sswitch_124
    const-string v5, "android.net.INetworkPolicyManager"

    #@126
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@129
    .line 182
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12c
    move-result v5

    #@12d
    if-eqz v5, :cond_146

    #@12f
    .line 183
    sget-object v5, Landroid/net/NetworkState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@131
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@134
    move-result-object v0

    #@135
    check-cast v0, Landroid/net/NetworkState;

    #@137
    .line 188
    .restart local v0       #_arg0:Landroid/net/NetworkState;
    :goto_137
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyManager$Stub;->isNetworkMetered(Landroid/net/NetworkState;)Z

    #@13a
    move-result v2

    #@13b
    .line 189
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13e
    .line 190
    if-eqz v2, :cond_141

    #@140
    move v3, v4

    #@141
    :cond_141
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@144
    goto/16 :goto_9

    #@146
    .line 186
    .end local v0           #_arg0:Landroid/net/NetworkState;
    .end local v2           #_result:Z
    :cond_146
    const/4 v0, 0x0

    #@147
    .restart local v0       #_arg0:Landroid/net/NetworkState;
    goto :goto_137

    #@148
    .line 43
    :sswitch_data_148
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_38
        0x4 -> :sswitch_4c
        0x5 -> :sswitch_63
        0x6 -> :sswitch_77
        0x7 -> :sswitch_8c
        0x8 -> :sswitch_a1
        0x9 -> :sswitch_b2
        0xa -> :sswitch_cf
        0xb -> :sswitch_e5
        0xc -> :sswitch_f9
        0xd -> :sswitch_124
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
