.class public Landroid/net/sip/SipException;
.super Ljava/lang/Exception;
.source "SipException.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    #@3
    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@3
    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .parameter "message"
    .parameter "cause"

    #@0
    .prologue
    .line 32
    instance-of v0, p2, Ljavax/sip/SipException;

    #@2
    if-eqz v0, :cond_e

    #@4
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_e

    #@a
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    #@d
    move-result-object p2

    #@e
    .end local p2
    :cond_e
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@11
    .line 36
    return-void
.end method
