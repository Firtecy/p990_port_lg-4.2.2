.class public abstract Landroid/net/sip/ISipSessionListener$Stub;
.super Landroid/os/Binder;
.source "ISipSessionListener.java"

# interfaces
.implements Landroid/net/sip/ISipSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/sip/ISipSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/sip/ISipSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.sip.ISipSessionListener"

.field static final TRANSACTION_onCallBusy:I = 0x6

.field static final TRANSACTION_onCallChangeFailed:I = 0x9

.field static final TRANSACTION_onCallEnded:I = 0x5

.field static final TRANSACTION_onCallEstablished:I = 0x4

.field static final TRANSACTION_onCallTransferring:I = 0x7

.field static final TRANSACTION_onCalling:I = 0x1

.field static final TRANSACTION_onError:I = 0x8

.field static final TRANSACTION_onRegistering:I = 0xa

.field static final TRANSACTION_onRegistrationDone:I = 0xb

.field static final TRANSACTION_onRegistrationFailed:I = 0xc

.field static final TRANSACTION_onRegistrationTimeout:I = 0xd

.field static final TRANSACTION_onRinging:I = 0x2

.field static final TRANSACTION_onRingingBack:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.net.sip.ISipSessionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSessionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.net.sip.ISipSessionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/sip/ISipSessionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/net/sip/ISipSessionListener;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/net/sip/ISipSessionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/sip/ISipSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 42
    sparse-switch p1, :sswitch_data_154

    #@4
    .line 194
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 46
    :sswitch_9
    const-string v4, "android.net.sip.ISipSessionListener"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 51
    :sswitch_f
    const-string v4, "android.net.sip.ISipSessionListener"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v4

    #@18
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@1b
    move-result-object v0

    #@1c
    .line 54
    .local v0, _arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->onCalling(Landroid/net/sip/ISipSession;)V

    #@1f
    .line 55
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22
    goto :goto_8

    #@23
    .line 60
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    :sswitch_23
    const-string v4, "android.net.sip.ISipSessionListener"

    #@25
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@2f
    move-result-object v0

    #@30
    .line 64
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_49

    #@36
    .line 65
    sget-object v4, Landroid/net/sip/SipProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@38
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3b
    move-result-object v1

    #@3c
    check-cast v1, Landroid/net/sip/SipProfile;

    #@3e
    .line 71
    .local v1, _arg1:Landroid/net/sip/SipProfile;
    :goto_3e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    .line 72
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/sip/ISipSessionListener$Stub;->onRinging(Landroid/net/sip/ISipSession;Landroid/net/sip/SipProfile;Ljava/lang/String;)V

    #@45
    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    goto :goto_8

    #@49
    .line 68
    .end local v1           #_arg1:Landroid/net/sip/SipProfile;
    .end local v2           #_arg2:Ljava/lang/String;
    :cond_49
    const/4 v1, 0x0

    #@4a
    .restart local v1       #_arg1:Landroid/net/sip/SipProfile;
    goto :goto_3e

    #@4b
    .line 78
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:Landroid/net/sip/SipProfile;
    :sswitch_4b
    const-string v4, "android.net.sip.ISipSessionListener"

    #@4d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@53
    move-result-object v4

    #@54
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@57
    move-result-object v0

    #@58
    .line 81
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->onRingingBack(Landroid/net/sip/ISipSession;)V

    #@5b
    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    goto :goto_8

    #@5f
    .line 87
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    :sswitch_5f
    const-string v4, "android.net.sip.ISipSessionListener"

    #@61
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@67
    move-result-object v4

    #@68
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@6b
    move-result-object v0

    #@6c
    .line 91
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    .line 92
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipSessionListener$Stub;->onCallEstablished(Landroid/net/sip/ISipSession;Ljava/lang/String;)V

    #@73
    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@76
    goto :goto_8

    #@77
    .line 98
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_77
    const-string v4, "android.net.sip.ISipSessionListener"

    #@79
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7f
    move-result-object v4

    #@80
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@83
    move-result-object v0

    #@84
    .line 101
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->onCallEnded(Landroid/net/sip/ISipSession;)V

    #@87
    .line 102
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    goto/16 :goto_8

    #@8c
    .line 107
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    :sswitch_8c
    const-string v4, "android.net.sip.ISipSessionListener"

    #@8e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@91
    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@94
    move-result-object v4

    #@95
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@98
    move-result-object v0

    #@99
    .line 110
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->onCallBusy(Landroid/net/sip/ISipSession;)V

    #@9c
    .line 111
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f
    goto/16 :goto_8

    #@a1
    .line 116
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    :sswitch_a1
    const-string v4, "android.net.sip.ISipSessionListener"

    #@a3
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a6
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a9
    move-result-object v4

    #@aa
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@ad
    move-result-object v0

    #@ae
    .line 120
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b1
    move-result-object v1

    #@b2
    .line 121
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipSessionListener$Stub;->onCallTransferring(Landroid/net/sip/ISipSession;Ljava/lang/String;)V

    #@b5
    .line 122
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b8
    goto/16 :goto_8

    #@ba
    .line 127
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_ba
    const-string v4, "android.net.sip.ISipSessionListener"

    #@bc
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c2
    move-result-object v4

    #@c3
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@c6
    move-result-object v0

    #@c7
    .line 131
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ca
    move-result v1

    #@cb
    .line 133
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ce
    move-result-object v2

    #@cf
    .line 134
    .restart local v2       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/sip/ISipSessionListener$Stub;->onError(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@d2
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d5
    goto/16 :goto_8

    #@d7
    .line 140
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Ljava/lang/String;
    :sswitch_d7
    const-string v4, "android.net.sip.ISipSessionListener"

    #@d9
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@df
    move-result-object v4

    #@e0
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@e3
    move-result-object v0

    #@e4
    .line 144
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e7
    move-result v1

    #@e8
    .line 146
    .restart local v1       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@eb
    move-result-object v2

    #@ec
    .line 147
    .restart local v2       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/sip/ISipSessionListener$Stub;->onCallChangeFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@ef
    .line 148
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f2
    goto/16 :goto_8

    #@f4
    .line 153
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Ljava/lang/String;
    :sswitch_f4
    const-string v4, "android.net.sip.ISipSessionListener"

    #@f6
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f9
    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@fc
    move-result-object v4

    #@fd
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@100
    move-result-object v0

    #@101
    .line 156
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->onRegistering(Landroid/net/sip/ISipSession;)V

    #@104
    .line 157
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@107
    goto/16 :goto_8

    #@109
    .line 162
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    :sswitch_109
    const-string v4, "android.net.sip.ISipSessionListener"

    #@10b
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10e
    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@111
    move-result-object v4

    #@112
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@115
    move-result-object v0

    #@116
    .line 166
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@119
    move-result v1

    #@11a
    .line 167
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipSessionListener$Stub;->onRegistrationDone(Landroid/net/sip/ISipSession;I)V

    #@11d
    .line 168
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@120
    goto/16 :goto_8

    #@122
    .line 173
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:I
    :sswitch_122
    const-string v4, "android.net.sip.ISipSessionListener"

    #@124
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@127
    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@12a
    move-result-object v4

    #@12b
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@12e
    move-result-object v0

    #@12f
    .line 177
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@132
    move-result v1

    #@133
    .line 179
    .restart local v1       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@136
    move-result-object v2

    #@137
    .line 180
    .restart local v2       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/sip/ISipSessionListener$Stub;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@13a
    .line 181
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13d
    goto/16 :goto_8

    #@13f
    .line 186
    .end local v0           #_arg0:Landroid/net/sip/ISipSession;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Ljava/lang/String;
    :sswitch_13f
    const-string v4, "android.net.sip.ISipSessionListener"

    #@141
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@144
    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@147
    move-result-object v4

    #@148
    invoke-static {v4}, Landroid/net/sip/ISipSession$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;

    #@14b
    move-result-object v0

    #@14c
    .line 189
    .restart local v0       #_arg0:Landroid/net/sip/ISipSession;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSessionListener$Stub;->onRegistrationTimeout(Landroid/net/sip/ISipSession;)V

    #@14f
    .line 190
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@152
    goto/16 :goto_8

    #@154
    .line 42
    :sswitch_data_154
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_23
        0x3 -> :sswitch_4b
        0x4 -> :sswitch_5f
        0x5 -> :sswitch_77
        0x6 -> :sswitch_8c
        0x7 -> :sswitch_a1
        0x8 -> :sswitch_ba
        0x9 -> :sswitch_d7
        0xa -> :sswitch_f4
        0xb -> :sswitch_109
        0xc -> :sswitch_122
        0xd -> :sswitch_13f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
