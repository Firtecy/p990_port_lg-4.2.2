.class public abstract Landroid/net/sip/ISipSession$Stub;
.super Landroid/os/Binder;
.source "ISipSession.java"

# interfaces
.implements Landroid/net/sip/ISipSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/sip/ISipSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/sip/ISipSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.sip.ISipSession"

.field static final TRANSACTION_answerCall:I = 0xb

.field static final TRANSACTION_changeCall:I = 0xd

.field static final TRANSACTION_endCall:I = 0xc

.field static final TRANSACTION_getCallId:I = 0x6

.field static final TRANSACTION_getLocalIp:I = 0x1

.field static final TRANSACTION_getLocalProfile:I = 0x2

.field static final TRANSACTION_getPeerProfile:I = 0x3

.field static final TRANSACTION_getState:I = 0x4

.field static final TRANSACTION_isInCall:I = 0x5

.field static final TRANSACTION_makeCall:I = 0xa

.field static final TRANSACTION_register:I = 0x8

.field static final TRANSACTION_setListener:I = 0x7

.field static final TRANSACTION_unregister:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.net.sip.ISipSession"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/sip/ISipSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.net.sip.ISipSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/sip/ISipSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/net/sip/ISipSession;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/net/sip/ISipSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/sip/ISipSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_114

    #@5
    .line 183
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 47
    :sswitch_a
    const-string v4, "android.net.sip.ISipSession"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v4, "android.net.sip.ISipSession"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->getLocalIp()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 54
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 55
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    goto :goto_9

    #@20
    .line 60
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_20
    const-string v6, "android.net.sip.ISipSession"

    #@22
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 61
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@28
    move-result-object v3

    #@29
    .line 62
    .local v3, _result:Landroid/net/sip/SipProfile;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c
    .line 63
    if-eqz v3, :cond_35

    #@2e
    .line 64
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 65
    invoke-virtual {v3, p3, v5}, Landroid/net/sip/SipProfile;->writeToParcel(Landroid/os/Parcel;I)V

    #@34
    goto :goto_9

    #@35
    .line 68
    :cond_35
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    goto :goto_9

    #@39
    .line 74
    .end local v3           #_result:Landroid/net/sip/SipProfile;
    :sswitch_39
    const-string v6, "android.net.sip.ISipSession"

    #@3b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e
    .line 75
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->getPeerProfile()Landroid/net/sip/SipProfile;

    #@41
    move-result-object v3

    #@42
    .line 76
    .restart local v3       #_result:Landroid/net/sip/SipProfile;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@45
    .line 77
    if-eqz v3, :cond_4e

    #@47
    .line 78
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@4a
    .line 79
    invoke-virtual {v3, p3, v5}, Landroid/net/sip/SipProfile;->writeToParcel(Landroid/os/Parcel;I)V

    #@4d
    goto :goto_9

    #@4e
    .line 82
    :cond_4e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@51
    goto :goto_9

    #@52
    .line 88
    .end local v3           #_result:Landroid/net/sip/SipProfile;
    :sswitch_52
    const-string v4, "android.net.sip.ISipSession"

    #@54
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@57
    .line 89
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->getState()I

    #@5a
    move-result v3

    #@5b
    .line 90
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    .line 91
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@61
    goto :goto_9

    #@62
    .line 96
    .end local v3           #_result:I
    :sswitch_62
    const-string v6, "android.net.sip.ISipSession"

    #@64
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67
    .line 97
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->isInCall()Z

    #@6a
    move-result v3

    #@6b
    .line 98
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    .line 99
    if-eqz v3, :cond_71

    #@70
    move v4, v5

    #@71
    :cond_71
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    goto :goto_9

    #@75
    .line 104
    .end local v3           #_result:Z
    :sswitch_75
    const-string v4, "android.net.sip.ISipSession"

    #@77
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a
    .line 105
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->getCallId()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    .line 106
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@81
    .line 107
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@84
    goto :goto_9

    #@85
    .line 112
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_85
    const-string v4, "android.net.sip.ISipSession"

    #@87
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8a
    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8d
    move-result-object v4

    #@8e
    invoke-static {v4}, Landroid/net/sip/ISipSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSessionListener;

    #@91
    move-result-object v0

    #@92
    .line 115
    .local v0, _arg0:Landroid/net/sip/ISipSessionListener;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSession$Stub;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@95
    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@98
    goto/16 :goto_9

    #@9a
    .line 121
    .end local v0           #_arg0:Landroid/net/sip/ISipSessionListener;
    :sswitch_9a
    const-string v4, "android.net.sip.ISipSession"

    #@9c
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9f
    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v0

    #@a3
    .line 124
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipSession$Stub;->register(I)V

    #@a6
    .line 125
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a9
    goto/16 :goto_9

    #@ab
    .line 130
    .end local v0           #_arg0:I
    :sswitch_ab
    const-string v4, "android.net.sip.ISipSession"

    #@ad
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b0
    .line 131
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->unregister()V

    #@b3
    .line 132
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    goto/16 :goto_9

    #@b8
    .line 137
    :sswitch_b8
    const-string v4, "android.net.sip.ISipSession"

    #@ba
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bd
    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c0
    move-result v4

    #@c1
    if-eqz v4, :cond_db

    #@c3
    .line 140
    sget-object v4, Landroid/net/sip/SipProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c5
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c8
    move-result-object v0

    #@c9
    check-cast v0, Landroid/net/sip/SipProfile;

    #@cb
    .line 146
    .local v0, _arg0:Landroid/net/sip/SipProfile;
    :goto_cb
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ce
    move-result-object v1

    #@cf
    .line 148
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d2
    move-result v2

    #@d3
    .line 149
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/sip/ISipSession$Stub;->makeCall(Landroid/net/sip/SipProfile;Ljava/lang/String;I)V

    #@d6
    .line 150
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d9
    goto/16 :goto_9

    #@db
    .line 143
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:I
    :cond_db
    const/4 v0, 0x0

    #@dc
    .restart local v0       #_arg0:Landroid/net/sip/SipProfile;
    goto :goto_cb

    #@dd
    .line 155
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    :sswitch_dd
    const-string v4, "android.net.sip.ISipSession"

    #@df
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e2
    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e5
    move-result-object v0

    #@e6
    .line 159
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e9
    move-result v1

    #@ea
    .line 160
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipSession$Stub;->answerCall(Ljava/lang/String;I)V

    #@ed
    .line 161
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f0
    goto/16 :goto_9

    #@f2
    .line 166
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_f2
    const-string v4, "android.net.sip.ISipSession"

    #@f4
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f7
    .line 167
    invoke-virtual {p0}, Landroid/net/sip/ISipSession$Stub;->endCall()V

    #@fa
    .line 168
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fd
    goto/16 :goto_9

    #@ff
    .line 173
    :sswitch_ff
    const-string v4, "android.net.sip.ISipSession"

    #@101
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@104
    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@107
    move-result-object v0

    #@108
    .line 177
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10b
    move-result v1

    #@10c
    .line 178
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipSession$Stub;->changeCall(Ljava/lang/String;I)V

    #@10f
    .line 179
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@112
    goto/16 :goto_9

    #@114
    .line 43
    :sswitch_data_114
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_39
        0x4 -> :sswitch_52
        0x5 -> :sswitch_62
        0x6 -> :sswitch_75
        0x7 -> :sswitch_85
        0x8 -> :sswitch_9a
        0x9 -> :sswitch_ab
        0xa -> :sswitch_b8
        0xb -> :sswitch_dd
        0xc -> :sswitch_f2
        0xd -> :sswitch_ff
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
