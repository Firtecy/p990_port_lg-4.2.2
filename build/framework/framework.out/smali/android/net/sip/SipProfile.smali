.class public Landroid/net/sip/SipProfile;
.super Ljava/lang/Object;
.source "SipProfile.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/sip/SipProfile$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/sip/SipProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_PORT:I = 0x13c4

.field private static final TCP:Ljava/lang/String; = "TCP"

.field private static final UDP:Ljava/lang/String; = "UDP"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mAddress:Ljavax/sip/address/Address;

.field private mAuthUserName:Ljava/lang/String;

.field private mAutoRegistration:Z

.field private transient mCallingUid:I

.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPort:I

.field private mProfileName:Ljava/lang/String;

.field private mProtocol:Ljava/lang/String;

.field private mProxyAddress:Ljava/lang/String;

.field private mSendKeepAlive:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 65
    new-instance v0, Landroid/net/sip/SipProfile$1;

    #@2
    invoke-direct {v0}, Landroid/net/sip/SipProfile$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/sip/SipProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 308
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 57
    const-string v0, "UDP"

    #@6
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@8
    .line 60
    const/16 v0, 0x13c4

    #@a
    iput v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@c
    .line 61
    iput-boolean v1, p0, Landroid/net/sip/SipProfile;->mSendKeepAlive:Z

    #@e
    .line 62
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/net/sip/SipProfile;->mAutoRegistration:Z

    #@11
    .line 63
    iput v1, p0, Landroid/net/sip/SipProfile;->mCallingUid:I

    #@13
    .line 309
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/sip/SipProfile$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/net/sip/SipProfile;-><init>()V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 311
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 57
    const-string v0, "UDP"

    #@7
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@9
    .line 60
    const/16 v0, 0x13c4

    #@b
    iput v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@d
    .line 61
    iput-boolean v1, p0, Landroid/net/sip/SipProfile;->mSendKeepAlive:Z

    #@f
    .line 62
    iput-boolean v2, p0, Landroid/net/sip/SipProfile;->mAutoRegistration:Z

    #@11
    .line 63
    iput v1, p0, Landroid/net/sip/SipProfile;->mCallingUid:I

    #@13
    .line 312
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Ljavax/sip/address/Address;

    #@19
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;

    #@1b
    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mProxyAddress:Ljava/lang/String;

    #@21
    .line 314
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mPassword:Ljava/lang/String;

    #@27
    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mDomain:Ljava/lang/String;

    #@2d
    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@33
    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mProfileName:Ljava/lang/String;

    #@39
    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    if-nez v0, :cond_5d

    #@3f
    move v0, v1

    #@40
    :goto_40
    iput-boolean v0, p0, Landroid/net/sip/SipProfile;->mSendKeepAlive:Z

    #@42
    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v0

    #@46
    if-nez v0, :cond_5f

    #@48
    :goto_48
    iput-boolean v1, p0, Landroid/net/sip/SipProfile;->mAutoRegistration:Z

    #@4a
    .line 320
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v0

    #@4e
    iput v0, p0, Landroid/net/sip/SipProfile;->mCallingUid:I

    #@50
    .line 321
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v0

    #@54
    iput v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@56
    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    iput-object v0, p0, Landroid/net/sip/SipProfile;->mAuthUserName:Ljava/lang/String;

    #@5c
    .line 323
    return-void

    #@5d
    :cond_5d
    move v0, v2

    #@5e
    .line 318
    goto :goto_40

    #@5f
    :cond_5f
    move v1, v2

    #@60
    .line 319
    goto :goto_48
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/net/sip/SipProfile$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/net/sip/SipProfile;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method static synthetic access$1002(Landroid/net/sip/SipProfile;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-boolean p1, p0, Landroid/net/sip/SipProfile;->mAutoRegistration:Z

    #@2
    return p1
.end method

.method static synthetic access$1102(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mPassword:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1202(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mProxyAddress:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/net/sip/SipProfile;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$302(Landroid/net/sip/SipProfile;Ljavax/sip/address/Address;)Ljavax/sip/address/Address;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Landroid/net/sip/SipProfile;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/net/sip/SipProfile;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput p1, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@2
    return p1
.end method

.method static synthetic access$502(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mDomain:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mAuthUserName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$702(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mProfileName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$800(Landroid/net/sip/SipProfile;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-object p1, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$902(Landroid/net/sip/SipProfile;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    iput-boolean p1, p0, Landroid/net/sip/SipProfile;->mSendKeepAlive:Z

    #@2
    return p1
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    #@0
    .prologue
    .line 499
    iget v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@2
    if-nez v0, :cond_8

    #@4
    const/16 v0, 0x13c4

    #@6
    iput v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@8
    .line 500
    :cond_8
    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 342
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAuthUserName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 406
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mAuthUserName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getAutoRegistration()Z
    .registers 2

    #@0
    .prologue
    .line 478
    iget-boolean v0, p0, Landroid/net/sip/SipProfile;->mAutoRegistration:Z

    #@2
    return v0
.end method

.method public getCallingUid()I
    .registers 2

    #@0
    .prologue
    .line 494
    iget v0, p0, Landroid/net/sip/SipProfile;->mCallingUid:I

    #@2
    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;

    #@2
    invoke-interface {v0}, Ljavax/sip/address/Address;->getDisplayName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 415
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mPassword:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPort()I
    .registers 2

    #@0
    .prologue
    .line 433
    iget v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@2
    return v0
.end method

.method public getProfileName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 460
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProfileName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 442
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getProxyAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 451
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProxyAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSendKeepAlive()Z
    .registers 2

    #@0
    .prologue
    .line 469
    iget-boolean v0, p0, Landroid/net/sip/SipProfile;->mSendKeepAlive:Z

    #@2
    return v0
.end method

.method public getSipAddress()Ljavax/sip/address/Address;
    .registers 2

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;

    #@2
    return-object v0
.end method

.method public getSipDomain()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 424
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mDomain:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getUri()Ljavax/sip/address/SipURI;
    .registers 2

    #@0
    .prologue
    .line 352
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;

    #@2
    invoke-interface {v0}, Ljavax/sip/address/Address;->getURI()Ljavax/sip/address/URI;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljavax/sip/address/SipURI;

    #@8
    return-object v0
.end method

.method public getUriString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProxyAddress:Ljava/lang/String;

    #@2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_2d

    #@8
    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v1, "sip:"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0}, Landroid/net/sip/SipProfile;->getUserName()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, "@"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    iget-object v1, p0, Landroid/net/sip/SipProfile;->mDomain:Ljava/lang/String;

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 367
    :goto_2c
    return-object v0

    #@2d
    :cond_2d
    invoke-virtual {p0}, Landroid/net/sip/SipProfile;->getUri()Ljavax/sip/address/SipURI;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    goto :goto_2c
.end method

.method public getUserName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 395
    invoke-virtual {p0}, Landroid/net/sip/SipProfile;->getUri()Ljavax/sip/address/SipURI;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljavax/sip/address/SipURI;->getUser()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public setCallingUid(I)V
    .registers 2
    .parameter "uid"

    #@0
    .prologue
    .line 486
    iput p1, p0, Landroid/net/sip/SipProfile;->mCallingUid:I

    #@2
    .line 487
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 327
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    #@7
    .line 328
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProxyAddress:Ljava/lang/String;

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 329
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mPassword:Ljava/lang/String;

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 330
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mDomain:Ljava/lang/String;

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 331
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProtocol:Ljava/lang/String;

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 332
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mProfileName:Ljava/lang/String;

    #@1d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 333
    iget-boolean v0, p0, Landroid/net/sip/SipProfile;->mSendKeepAlive:Z

    #@22
    if-eqz v0, :cond_3f

    #@24
    move v0, v1

    #@25
    :goto_25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 334
    iget-boolean v0, p0, Landroid/net/sip/SipProfile;->mAutoRegistration:Z

    #@2a
    if-eqz v0, :cond_41

    #@2c
    :goto_2c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 335
    iget v0, p0, Landroid/net/sip/SipProfile;->mCallingUid:I

    #@31
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 336
    iget v0, p0, Landroid/net/sip/SipProfile;->mPort:I

    #@36
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 337
    iget-object v0, p0, Landroid/net/sip/SipProfile;->mAuthUserName:Ljava/lang/String;

    #@3b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3e
    .line 338
    return-void

    #@3f
    :cond_3f
    move v0, v2

    #@40
    .line 333
    goto :goto_25

    #@41
    :cond_41
    move v1, v2

    #@42
    .line 334
    goto :goto_2c
.end method
