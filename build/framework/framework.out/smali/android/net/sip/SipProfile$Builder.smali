.class public Landroid/net/sip/SipProfile$Builder;
.super Ljava/lang/Object;
.source "SipProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/sip/SipProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAddressFactory:Ljavax/sip/address/AddressFactory;

.field private mDisplayName:Ljava/lang/String;

.field private mProfile:Landroid/net/sip/SipProfile;

.field private mProxyAddress:Ljava/lang/String;

.field private mUri:Ljavax/sip/address/SipURI;


# direct methods
.method public constructor <init>(Landroid/net/sip/SipProfile;)V
    .registers 5
    .parameter "profile"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 81
    new-instance v1, Landroid/net/sip/SipProfile;

    #@6
    invoke-direct {v1, v2}, Landroid/net/sip/SipProfile;-><init>(Landroid/net/sip/SipProfile$1;)V

    #@9
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@b
    .line 88
    :try_start_b
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    #@12
    move-result-object v1

    #@13
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;
    :try_end_15
    .catch Ljavax/sip/PeerUnavailableException; {:try_start_b .. :try_end_15} :catch_1d

    #@15
    .line 99
    if-nez p1, :cond_24

    #@17
    new-instance v1, Ljava/lang/NullPointerException;

    #@19
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@1c
    throw v1

    #@1d
    .line 90
    :catch_1d
    move-exception v0

    #@1e
    .line 91
    .local v0, e:Ljavax/sip/PeerUnavailableException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@20
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@23
    throw v1

    #@24
    .line 101
    .end local v0           #e:Ljavax/sip/PeerUnavailableException;
    :cond_24
    :try_start_24
    invoke-static {p1}, Landroid/net/sip/SipProfile;->access$200(Landroid/net/sip/SipProfile;)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Landroid/net/sip/SipProfile;

    #@2a
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;
    :try_end_2c
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_24 .. :try_end_2c} :catch_56

    #@2c
    .line 105
    iget-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@2e
    invoke-static {v1, v2}, Landroid/net/sip/SipProfile;->access$302(Landroid/net/sip/SipProfile;Ljavax/sip/address/Address;)Ljavax/sip/address/Address;

    #@31
    .line 106
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUri()Ljavax/sip/address/SipURI;

    #@34
    move-result-object v1

    #@35
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@37
    .line 107
    iget-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@39
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getPassword()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-interface {v1, v2}, Ljavax/sip/address/SipURI;->setUserPassword(Ljava/lang/String;)V

    #@40
    .line 108
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getDisplayName()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mDisplayName:Ljava/lang/String;

    #@46
    .line 109
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getProxyAddress()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    #@4c
    .line 110
    iget-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@4e
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getPort()I

    #@51
    move-result v2

    #@52
    invoke-static {v1, v2}, Landroid/net/sip/SipProfile;->access$402(Landroid/net/sip/SipProfile;I)I

    #@55
    .line 111
    return-void

    #@56
    .line 102
    :catch_56
    move-exception v0

    #@57
    .line 103
    .local v0, e:Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@59
    const-string/jumbo v2, "should not occur"

    #@5c
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@5f
    throw v1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 7
    .parameter "uriString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    new-instance v2, Landroid/net/sip/SipProfile;

    #@5
    const/4 v3, 0x0

    #@6
    invoke-direct {v2, v3}, Landroid/net/sip/SipProfile;-><init>(Landroid/net/sip/SipProfile$1;)V

    #@9
    iput-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@b
    .line 88
    :try_start_b
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    #@12
    move-result-object v2

    #@13
    iput-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;
    :try_end_15
    .catch Ljavax/sip/PeerUnavailableException; {:try_start_b .. :try_end_15} :catch_20

    #@15
    .line 120
    if-nez p1, :cond_27

    #@17
    .line 121
    new-instance v2, Ljava/lang/NullPointerException;

    #@19
    const-string/jumbo v3, "uriString cannot be null"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 90
    :catch_20
    move-exception v0

    #@21
    .line 91
    .local v0, e:Ljavax/sip/PeerUnavailableException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@23
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@26
    throw v2

    #@27
    .line 123
    .end local v0           #e:Ljavax/sip/PeerUnavailableException;
    :cond_27
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@29
    invoke-direct {p0, p1}, Landroid/net/sip/SipProfile$Builder;->fix(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-interface {v2, v3}, Ljavax/sip/address/AddressFactory;->createURI(Ljava/lang/String;)Ljavax/sip/address/URI;

    #@30
    move-result-object v1

    #@31
    .line 124
    .local v1, uri:Ljavax/sip/address/URI;
    instance-of v2, v1, Ljavax/sip/address/SipURI;

    #@33
    if-eqz v2, :cond_45

    #@35
    .line 125
    check-cast v1, Ljavax/sip/address/SipURI;

    #@37
    .end local v1           #uri:Ljavax/sip/address/URI;
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@39
    .line 129
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@3b
    iget-object v3, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@3d
    invoke-interface {v3}, Ljavax/sip/address/SipURI;->getHost()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/net/sip/SipProfile;->access$502(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@44
    .line 130
    return-void

    #@45
    .line 127
    .restart local v1       #uri:Ljavax/sip/address/URI;
    :cond_45
    new-instance v2, Ljava/text/ParseException;

    #@47
    new-instance v3, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    const-string v4, " is not a SIP URI"

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    const/4 v4, 0x0

    #@5b
    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    #@5e
    throw v2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "username"
    .parameter "serverDomain"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    new-instance v1, Landroid/net/sip/SipProfile;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-direct {v1, v2}, Landroid/net/sip/SipProfile;-><init>(Landroid/net/sip/SipProfile$1;)V

    #@9
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@b
    .line 88
    :try_start_b
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    #@12
    move-result-object v1

    #@13
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;
    :try_end_15
    .catch Ljavax/sip/PeerUnavailableException; {:try_start_b .. :try_end_15} :catch_22

    #@15
    .line 143
    if-eqz p1, :cond_19

    #@17
    if-nez p2, :cond_29

    #@19
    .line 144
    :cond_19
    new-instance v1, Ljava/lang/NullPointerException;

    #@1b
    const-string/jumbo v2, "username and serverDomain cannot be null"

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 90
    :catch_22
    move-exception v0

    #@23
    .line 91
    .local v0, e:Ljavax/sip/PeerUnavailableException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@25
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@28
    throw v1

    #@29
    .line 147
    .end local v0           #e:Ljavax/sip/PeerUnavailableException;
    :cond_29
    iget-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@2b
    invoke-interface {v1, p1, p2}, Ljavax/sip/address/AddressFactory;->createSipURI(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/address/SipURI;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@31
    .line 148
    iget-object v1, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@33
    invoke-static {v1, p2}, Landroid/net/sip/SipProfile;->access$502(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@36
    .line 149
    return-void
.end method

.method private fix(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "uriString"

    #@0
    .prologue
    .line 152
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    const-string/jumbo v1, "sip:"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_12

    #@11
    .end local p1
    :goto_11
    return-object p1

    #@12
    .restart local p1
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v1, "sip:"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object p1

    #@26
    goto :goto_11
.end method


# virtual methods
.method public build()Landroid/net/sip/SipProfile;
    .registers 7

    #@0
    .prologue
    .line 281
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@2
    iget-object v3, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@4
    invoke-interface {v3}, Ljavax/sip/address/SipURI;->getUserPassword()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    invoke-static {v2, v3}, Landroid/net/sip/SipProfile;->access$1102(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@b
    .line 282
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@d
    const/4 v3, 0x0

    #@e
    invoke-interface {v2, v3}, Ljavax/sip/address/SipURI;->setUserPassword(Ljava/lang/String;)V

    #@11
    .line 284
    :try_start_11
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    #@13
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_42

    #@19
    .line 285
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@1b
    iget-object v3, p0, Landroid/net/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    #@1d
    invoke-direct {p0, v3}, Landroid/net/sip/SipProfile$Builder;->fix(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-interface {v2, v3}, Ljavax/sip/address/AddressFactory;->createURI(Ljava/lang/String;)Ljavax/sip/address/URI;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Ljavax/sip/address/SipURI;

    #@27
    .line 287
    .local v1, uri:Ljavax/sip/address/SipURI;
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@29
    invoke-interface {v1}, Ljavax/sip/address/SipURI;->getHost()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/net/sip/SipProfile;->access$1202(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@30
    .line 296
    .end local v1           #uri:Ljavax/sip/address/SipURI;
    :cond_30
    :goto_30
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@32
    iget-object v3, p0, Landroid/net/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@34
    iget-object v4, p0, Landroid/net/sip/SipProfile$Builder;->mDisplayName:Ljava/lang/String;

    #@36
    iget-object v5, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@38
    invoke-interface {v3, v4, v5}, Ljavax/sip/address/AddressFactory;->createAddress(Ljava/lang/String;Ljavax/sip/address/URI;)Ljavax/sip/address/Address;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/net/sip/SipProfile;->access$302(Landroid/net/sip/SipProfile;Ljavax/sip/address/Address;)Ljavax/sip/address/Address;
    :try_end_3f
    .catch Ljavax/sip/InvalidArgumentException; {:try_start_11 .. :try_end_3f} :catch_71
    .catch Ljava/text/ParseException; {:try_start_11 .. :try_end_3f} :catch_78

    #@3f
    .line 304
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@41
    return-object v2

    #@42
    .line 289
    :cond_42
    :try_start_42
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@44
    invoke-static {v2}, Landroid/net/sip/SipProfile;->access$800(Landroid/net/sip/SipProfile;)Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    const-string v3, "UDP"

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v2

    #@4e
    if-nez v2, :cond_5b

    #@50
    .line 290
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@52
    iget-object v3, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@54
    invoke-static {v3}, Landroid/net/sip/SipProfile;->access$800(Landroid/net/sip/SipProfile;)Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-interface {v2, v3}, Ljavax/sip/address/SipURI;->setTransportParam(Ljava/lang/String;)V

    #@5b
    .line 292
    :cond_5b
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@5d
    invoke-static {v2}, Landroid/net/sip/SipProfile;->access$400(Landroid/net/sip/SipProfile;)I

    #@60
    move-result v2

    #@61
    const/16 v3, 0x13c4

    #@63
    if-eq v2, v3, :cond_30

    #@65
    .line 293
    iget-object v2, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@67
    iget-object v3, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@69
    invoke-static {v3}, Landroid/net/sip/SipProfile;->access$400(Landroid/net/sip/SipProfile;)I

    #@6c
    move-result v3

    #@6d
    invoke-interface {v2, v3}, Ljavax/sip/address/SipURI;->setPort(I)V
    :try_end_70
    .catch Ljavax/sip/InvalidArgumentException; {:try_start_42 .. :try_end_70} :catch_71
    .catch Ljava/text/ParseException; {:try_start_42 .. :try_end_70} :catch_78

    #@70
    goto :goto_30

    #@71
    .line 298
    :catch_71
    move-exception v0

    #@72
    .line 299
    .local v0, e:Ljavax/sip/InvalidArgumentException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@74
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@77
    throw v2

    #@78
    .line 300
    .end local v0           #e:Ljavax/sip/InvalidArgumentException;
    :catch_78
    move-exception v0

    #@79
    .line 302
    .local v0, e:Ljava/text/ParseException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@7b
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@7e
    throw v2
.end method

.method public setAuthUserName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@2
    invoke-static {v0, p1}, Landroid/net/sip/SipProfile;->access$602(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@5
    .line 165
    return-object p0
.end method

.method public setAutoRegistration(Z)Landroid/net/sip/SipProfile$Builder;
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@2
    invoke-static {v0, p1}, Landroid/net/sip/SipProfile;->access$1002(Landroid/net/sip/SipProfile;Z)Z

    #@5
    .line 271
    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;
    .registers 2
    .parameter "displayName"

    #@0
    .prologue
    .line 245
    iput-object p1, p0, Landroid/net/sip/SipProfile$Builder;->mDisplayName:Ljava/lang/String;

    #@2
    .line 246
    return-object p0
.end method

.method public setOutboundProxy(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;
    .registers 2
    .parameter "outboundProxy"

    #@0
    .prologue
    .line 234
    iput-object p1, p0, Landroid/net/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    #@2
    .line 235
    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 186
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    #@2
    invoke-interface {v0, p1}, Ljavax/sip/address/SipURI;->setUserPassword(Ljava/lang/String;)V

    #@5
    .line 187
    return-object p0
.end method

.method public setPort(I)Landroid/net/sip/SipProfile$Builder;
    .registers 5
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 198
    const v0, 0xffff

    #@3
    if-gt p1, v0, :cond_9

    #@5
    const/16 v0, 0x3e8

    #@7
    if-ge p1, v0, :cond_22

    #@9
    .line 199
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "incorrect port arugment: "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 201
    :cond_22
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@24
    invoke-static {v0, p1}, Landroid/net/sip/SipProfile;->access$402(Landroid/net/sip/SipProfile;I)I

    #@27
    .line 202
    return-object p0
.end method

.method public setProfileName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@2
    invoke-static {v0, p1}, Landroid/net/sip/SipProfile;->access$702(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@5
    .line 176
    return-object p0
.end method

.method public setProtocol(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;
    .registers 5
    .parameter "protocol"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 215
    if-nez p1, :cond_b

    #@2
    .line 216
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "protocol cannot be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 218
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@e
    move-result-object p1

    #@f
    .line 219
    const-string v0, "UDP"

    #@11
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_39

    #@17
    const-string v0, "TCP"

    #@19
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    if-nez v0, :cond_39

    #@1f
    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@21
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string/jumbo v2, "unsupported protocol: "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@38
    throw v0

    #@39
    .line 223
    :cond_39
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@3b
    invoke-static {v0, p1}, Landroid/net/sip/SipProfile;->access$802(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    .line 224
    return-object p0
.end method

.method public setSendKeepAlive(Z)Landroid/net/sip/SipProfile$Builder;
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Landroid/net/sip/SipProfile$Builder;->mProfile:Landroid/net/sip/SipProfile;

    #@2
    invoke-static {v0, p1}, Landroid/net/sip/SipProfile;->access$902(Landroid/net/sip/SipProfile;Z)Z

    #@5
    .line 258
    return-object p0
.end method
