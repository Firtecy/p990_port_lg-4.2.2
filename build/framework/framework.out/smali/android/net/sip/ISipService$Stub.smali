.class public abstract Landroid/net/sip/ISipService$Stub;
.super Landroid/os/Binder;
.source "ISipService.java"

# interfaces
.implements Landroid/net/sip/ISipService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/sip/ISipService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/sip/ISipService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.sip.ISipService"

.field static final TRANSACTION_close:I = 0x3

.field static final TRANSACTION_createSession:I = 0x7

.field static final TRANSACTION_getListOfProfiles:I = 0x9

.field static final TRANSACTION_getPendingSession:I = 0x8

.field static final TRANSACTION_isOpened:I = 0x4

.field static final TRANSACTION_isRegistered:I = 0x5

.field static final TRANSACTION_open:I = 0x1

.field static final TRANSACTION_open3:I = 0x2

.field static final TRANSACTION_setRegistrationListener:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.net.sip.ISipService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/sip/ISipService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.net.sip.ISipService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/sip/ISipService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/net/sip/ISipService;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/net/sip/ISipService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/sip/ISipService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    .line 41
    sparse-switch p1, :sswitch_data_116

    #@6
    .line 161
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@9
    move-result v5

    #@a
    :goto_a
    return v5

    #@b
    .line 45
    :sswitch_b
    const-string v4, "android.net.sip.ISipService"

    #@d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    goto :goto_a

    #@11
    .line 50
    :sswitch_11
    const-string v4, "android.net.sip.ISipService"

    #@13
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_2b

    #@1c
    .line 53
    sget-object v4, Landroid/net/sip/SipProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/net/sip/SipProfile;

    #@24
    .line 58
    .local v0, _arg0:Landroid/net/sip/SipProfile;
    :goto_24
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipService$Stub;->open(Landroid/net/sip/SipProfile;)V

    #@27
    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    goto :goto_a

    #@2b
    .line 56
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    :cond_2b
    const/4 v0, 0x0

    #@2c
    .restart local v0       #_arg0:Landroid/net/sip/SipProfile;
    goto :goto_24

    #@2d
    .line 64
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    :sswitch_2d
    const-string v4, "android.net.sip.ISipService"

    #@2f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_5d

    #@38
    .line 67
    sget-object v4, Landroid/net/sip/SipProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3a
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3d
    move-result-object v0

    #@3e
    check-cast v0, Landroid/net/sip/SipProfile;

    #@40
    .line 73
    .restart local v0       #_arg0:Landroid/net/sip/SipProfile;
    :goto_40
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v4

    #@44
    if-eqz v4, :cond_5f

    #@46
    .line 74
    sget-object v4, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@48
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4b
    move-result-object v1

    #@4c
    check-cast v1, Landroid/app/PendingIntent;

    #@4e
    .line 80
    .local v1, _arg1:Landroid/app/PendingIntent;
    :goto_4e
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@51
    move-result-object v4

    #@52
    invoke-static {v4}, Landroid/net/sip/ISipSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSessionListener;

    #@55
    move-result-object v2

    #@56
    .line 81
    .local v2, _arg2:Landroid/net/sip/ISipSessionListener;
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/sip/ISipService$Stub;->open3(Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)V

    #@59
    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    goto :goto_a

    #@5d
    .line 70
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    .end local v1           #_arg1:Landroid/app/PendingIntent;
    .end local v2           #_arg2:Landroid/net/sip/ISipSessionListener;
    :cond_5d
    const/4 v0, 0x0

    #@5e
    .restart local v0       #_arg0:Landroid/net/sip/SipProfile;
    goto :goto_40

    #@5f
    .line 77
    :cond_5f
    const/4 v1, 0x0

    #@60
    .restart local v1       #_arg1:Landroid/app/PendingIntent;
    goto :goto_4e

    #@61
    .line 87
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    .end local v1           #_arg1:Landroid/app/PendingIntent;
    :sswitch_61
    const-string v4, "android.net.sip.ISipService"

    #@63
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    .line 90
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipService$Stub;->close(Ljava/lang/String;)V

    #@6d
    .line 91
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@70
    goto :goto_a

    #@71
    .line 96
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_71
    const-string v6, "android.net.sip.ISipService"

    #@73
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@76
    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@79
    move-result-object v0

    #@7a
    .line 99
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipService$Stub;->isOpened(Ljava/lang/String;)Z

    #@7d
    move-result v3

    #@7e
    .line 100
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@81
    .line 101
    if-eqz v3, :cond_84

    #@83
    move v4, v5

    #@84
    :cond_84
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@87
    goto :goto_a

    #@88
    .line 106
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_88
    const-string v6, "android.net.sip.ISipService"

    #@8a
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8d
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    .line 109
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipService$Stub;->isRegistered(Ljava/lang/String;)Z

    #@94
    move-result v3

    #@95
    .line 110
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@98
    .line 111
    if-eqz v3, :cond_9b

    #@9a
    move v4, v5

    #@9b
    :cond_9b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@9e
    goto/16 :goto_a

    #@a0
    .line 116
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_a0
    const-string v4, "android.net.sip.ISipService"

    #@a2
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a5
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a8
    move-result-object v0

    #@a9
    .line 120
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ac
    move-result-object v4

    #@ad
    invoke-static {v4}, Landroid/net/sip/ISipSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSessionListener;

    #@b0
    move-result-object v1

    #@b1
    .line 121
    .local v1, _arg1:Landroid/net/sip/ISipSessionListener;
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipService$Stub;->setRegistrationListener(Ljava/lang/String;Landroid/net/sip/ISipSessionListener;)V

    #@b4
    .line 122
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b7
    goto/16 :goto_a

    #@b9
    .line 127
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Landroid/net/sip/ISipSessionListener;
    :sswitch_b9
    const-string v4, "android.net.sip.ISipService"

    #@bb
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@be
    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c1
    move-result v4

    #@c2
    if-eqz v4, :cond_e6

    #@c4
    .line 130
    sget-object v4, Landroid/net/sip/SipProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c6
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c9
    move-result-object v0

    #@ca
    check-cast v0, Landroid/net/sip/SipProfile;

    #@cc
    .line 136
    .local v0, _arg0:Landroid/net/sip/SipProfile;
    :goto_cc
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@cf
    move-result-object v4

    #@d0
    invoke-static {v4}, Landroid/net/sip/ISipSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/sip/ISipSessionListener;

    #@d3
    move-result-object v1

    #@d4
    .line 137
    .restart local v1       #_arg1:Landroid/net/sip/ISipSessionListener;
    invoke-virtual {p0, v0, v1}, Landroid/net/sip/ISipService$Stub;->createSession(Landroid/net/sip/SipProfile;Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;

    #@d7
    move-result-object v3

    #@d8
    .line 138
    .local v3, _result:Landroid/net/sip/ISipSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@db
    .line 139
    if-eqz v3, :cond_e8

    #@dd
    invoke-interface {v3}, Landroid/net/sip/ISipSession;->asBinder()Landroid/os/IBinder;

    #@e0
    move-result-object v4

    #@e1
    :goto_e1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@e4
    goto/16 :goto_a

    #@e6
    .line 133
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    .end local v1           #_arg1:Landroid/net/sip/ISipSessionListener;
    .end local v3           #_result:Landroid/net/sip/ISipSession;
    :cond_e6
    const/4 v0, 0x0

    #@e7
    .restart local v0       #_arg0:Landroid/net/sip/SipProfile;
    goto :goto_cc

    #@e8
    .restart local v1       #_arg1:Landroid/net/sip/ISipSessionListener;
    .restart local v3       #_result:Landroid/net/sip/ISipSession;
    :cond_e8
    move-object v4, v6

    #@e9
    .line 139
    goto :goto_e1

    #@ea
    .line 144
    .end local v0           #_arg0:Landroid/net/sip/SipProfile;
    .end local v1           #_arg1:Landroid/net/sip/ISipSessionListener;
    .end local v3           #_result:Landroid/net/sip/ISipSession;
    :sswitch_ea
    const-string v4, "android.net.sip.ISipService"

    #@ec
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ef
    .line 146
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f2
    move-result-object v0

    #@f3
    .line 147
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/sip/ISipService$Stub;->getPendingSession(Ljava/lang/String;)Landroid/net/sip/ISipSession;

    #@f6
    move-result-object v3

    #@f7
    .line 148
    .restart local v3       #_result:Landroid/net/sip/ISipSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fa
    .line 149
    if-eqz v3, :cond_100

    #@fc
    invoke-interface {v3}, Landroid/net/sip/ISipSession;->asBinder()Landroid/os/IBinder;

    #@ff
    move-result-object v6

    #@100
    :cond_100
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@103
    goto/16 :goto_a

    #@105
    .line 154
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Landroid/net/sip/ISipSession;
    :sswitch_105
    const-string v4, "android.net.sip.ISipService"

    #@107
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10a
    .line 155
    invoke-virtual {p0}, Landroid/net/sip/ISipService$Stub;->getListOfProfiles()[Landroid/net/sip/SipProfile;

    #@10d
    move-result-object v3

    #@10e
    .line 156
    .local v3, _result:[Landroid/net/sip/SipProfile;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@111
    .line 157
    invoke-virtual {p3, v3, v5}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@114
    goto/16 :goto_a

    #@116
    .line 41
    :sswitch_data_116
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_61
        0x4 -> :sswitch_71
        0x5 -> :sswitch_88
        0x6 -> :sswitch_a0
        0x7 -> :sswitch_b9
        0x8 -> :sswitch_ea
        0x9 -> :sswitch_105
        0x5f4e5446 -> :sswitch_b
    .end sparse-switch
.end method
