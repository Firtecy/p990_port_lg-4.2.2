.class public Landroid/net/sip/SipAudioCall;
.super Ljava/lang/Object;
.source "SipAudioCall.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/sip/SipAudioCall$Listener;
    }
.end annotation


# static fields
.field private static final DONT_RELEASE_SOCKET:Z = false

.field private static final RELEASE_SOCKET:Z = true

.field private static final SESSION_TIMEOUT:I = 0x5

.field private static final TAG:Ljava/lang/String; = null

.field private static final TRANSFER_TIMEOUT:I = 0xf


# instance fields
.field private mAudioGroup:Landroid/net/rtp/AudioGroup;

.field private mAudioStream:Landroid/net/rtp/AudioStream;

.field private mContext:Landroid/content/Context;

.field private mErrorCode:I

.field private mErrorMessage:Ljava/lang/String;

.field private mHold:Z

.field private mInCall:Z

.field private mListener:Landroid/net/sip/SipAudioCall$Listener;

.field private mLocalProfile:Landroid/net/sip/SipProfile;

.field private mMuted:Z

.field private mPeerSd:Ljava/lang/String;

.field private mPendingCallRequest:Landroid/net/sip/SipProfile;

.field private mSessionId:J

.field private mSipSession:Landroid/net/sip/SipSession;

.field private mTransferringSession:Landroid/net/sip/SipSession;

.field private mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private mWm:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 63
    const-class v0, Landroid/net/sip/SipAudioCall;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/sip/SipProfile;)V
    .registers 6
    .parameter "context"
    .parameter "localProfile"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 206
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@7
    move-result-wide v0

    #@8
    iput-wide v0, p0, Landroid/net/sip/SipAudioCall;->mSessionId:J

    #@a
    .line 190
    iput-boolean v2, p0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@c
    .line 191
    iput-boolean v2, p0, Landroid/net/sip/SipAudioCall;->mMuted:Z

    #@e
    .line 192
    iput-boolean v2, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@10
    .line 198
    iput v2, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@12
    .line 207
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mContext:Landroid/content/Context;

    #@14
    .line 208
    iput-object p2, p0, Landroid/net/sip/SipAudioCall;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@16
    .line 209
    const-string/jumbo v0, "wifi"

    #@19
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@1f
    iput-object v0, p0, Landroid/net/sip/SipAudioCall;->mWm:Landroid/net/wifi/WifiManager;

    #@21
    .line 210
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 62
    sget-object v0, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/sip/SipAudioCall;)Landroid/net/sip/SipAudioCall$Listener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mListener:Landroid/net/sip/SipAudioCall$Listener;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/net/sip/SipAudioCall;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@2
    return v0
.end method

.method static synthetic access$1002(Landroid/net/sip/SipAudioCall;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@2
    return p1
.end method

.method static synthetic access$1102(Landroid/net/sip/SipAudioCall;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mErrorMessage:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1200(Landroid/net/sip/SipAudioCall;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/net/sip/SipAudioCall;->onError(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/net/sip/SipAudioCall;)Landroid/net/sip/SimpleSessionDescription;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createOffer()Landroid/net/sip/SimpleSessionDescription;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/sip/SipAudioCall;)Landroid/net/sip/SipSession;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/sip/SipAudioCall;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/net/sip/SipAudioCall;Ljava/lang/String;)Landroid/net/sip/SimpleSessionDescription;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/net/sip/SipAudioCall;->createAnswer(Ljava/lang/String;)Landroid/net/sip/SimpleSessionDescription;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Landroid/net/sip/SipAudioCall;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Landroid/net/sip/SipAudioCall;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Landroid/net/sip/SipAudioCall;)Landroid/net/sip/SipSession;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mTransferringSession:Landroid/net/sip/SipSession;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Landroid/net/sip/SipAudioCall;Landroid/net/sip/SipSession;)Landroid/net/sip/SipSession;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mTransferringSession:Landroid/net/sip/SipSession;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Landroid/net/sip/SipAudioCall;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->transferToNewSession()V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/net/sip/SipAudioCall;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Landroid/net/sip/SipAudioCall;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/net/sip/SipAudioCall;->close(Z)V

    #@3
    return-void
.end method

.method private declared-synchronized close(Z)V
    .registers 4
    .parameter "closeRtp"

    #@0
    .prologue
    .line 298
    monitor-enter p0

    #@1
    if-eqz p1, :cond_7

    #@3
    const/4 v0, 0x1

    #@4
    :try_start_4
    invoke-direct {p0, v0}, Landroid/net/sip/SipAudioCall;->stopCall(Z)V

    #@7
    .line 300
    :cond_7
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@a
    .line 301
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@d
    .line 302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@10
    move-result-wide v0

    #@11
    iput-wide v0, p0, Landroid/net/sip/SipAudioCall;->mSessionId:J

    #@13
    .line 303
    const/4 v0, 0x0

    #@14
    iput v0, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@16
    .line 304
    const/4 v0, 0x0

    #@17
    iput-object v0, p0, Landroid/net/sip/SipAudioCall;->mErrorMessage:Ljava/lang/String;

    #@19
    .line 306
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@1b
    if-eqz v0, :cond_26

    #@1d
    .line 307
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@1f
    const/4 v1, 0x0

    #@20
    invoke-virtual {v0, v1}, Landroid/net/sip/SipSession;->setListener(Landroid/net/sip/SipSession$Listener;)V

    #@23
    .line 308
    const/4 v0, 0x0

    #@24
    iput-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_28

    #@26
    .line 310
    :cond_26
    monitor-exit p0

    #@27
    return-void

    #@28
    .line 298
    :catchall_28
    move-exception v0

    #@29
    monitor-exit p0

    #@2a
    throw v0
.end method

.method private createAnswer(Ljava/lang/String;)Landroid/net/sip/SimpleSessionDescription;
    .registers 24
    .parameter "offerSd"

    #@0
    .prologue
    .line 747
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v18

    #@4
    if-eqz v18, :cond_b

    #@6
    invoke-direct/range {p0 .. p0}, Landroid/net/sip/SipAudioCall;->createOffer()Landroid/net/sip/SimpleSessionDescription;

    #@9
    move-result-object v4

    #@a
    .line 803
    :cond_a
    return-object v4

    #@b
    .line 748
    :cond_b
    new-instance v14, Landroid/net/sip/SimpleSessionDescription;

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-direct {v14, v0}, Landroid/net/sip/SimpleSessionDescription;-><init>(Ljava/lang/String;)V

    #@12
    .line 750
    .local v14, offer:Landroid/net/sip/SimpleSessionDescription;
    new-instance v4, Landroid/net/sip/SimpleSessionDescription;

    #@14
    move-object/from16 v0, p0

    #@16
    iget-wide v0, v0, Landroid/net/sip/SipAudioCall;->mSessionId:J

    #@18
    move-wide/from16 v18, v0

    #@1a
    invoke-direct/range {p0 .. p0}, Landroid/net/sip/SipAudioCall;->getLocalIp()Ljava/lang/String;

    #@1d
    move-result-object v20

    #@1e
    move-wide/from16 v0, v18

    #@20
    move-object/from16 v2, v20

    #@22
    invoke-direct {v4, v0, v1, v2}, Landroid/net/sip/SimpleSessionDescription;-><init>(JLjava/lang/String;)V

    #@25
    .line 752
    .local v4, answer:Landroid/net/sip/SimpleSessionDescription;
    const/4 v7, 0x0

    #@26
    .line 753
    .local v7, codec:Landroid/net/rtp/AudioCodec;
    invoke-virtual {v14}, Landroid/net/sip/SimpleSessionDescription;->getMedia()[Landroid/net/sip/SimpleSessionDescription$Media;

    #@29
    move-result-object v5

    #@2a
    .local v5, arr$:[Landroid/net/sip/SimpleSessionDescription$Media;
    array-length v11, v5

    #@2b
    .local v11, len$:I
    const/4 v9, 0x0

    #@2c
    .local v9, i$:I
    move v10, v9

    #@2d
    .end local v5           #arr$:[Landroid/net/sip/SimpleSessionDescription$Media;
    .end local v9           #i$:I
    .end local v11           #len$:I
    .local v10, i$:I
    :goto_2d
    if-ge v10, v11, :cond_16f

    #@2f
    aget-object v13, v5, v10

    #@31
    .line 754
    .local v13, media:Landroid/net/sip/SimpleSessionDescription$Media;
    if-nez v7, :cond_143

    #@33
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getPort()I

    #@36
    move-result v18

    #@37
    if-lez v18, :cond_143

    #@39
    const-string v18, "audio"

    #@3b
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getType()Ljava/lang/String;

    #@3e
    move-result-object v19

    #@3f
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v18

    #@43
    if-eqz v18, :cond_143

    #@45
    const-string v18, "RTP/AVP"

    #@47
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getProtocol()Ljava/lang/String;

    #@4a
    move-result-object v19

    #@4b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v18

    #@4f
    if-eqz v18, :cond_143

    #@51
    .line 758
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpPayloadTypes()[I

    #@54
    move-result-object v6

    #@55
    .local v6, arr$:[I
    array-length v12, v6

    #@56
    .local v12, len$:I
    const/4 v9, 0x0

    #@57
    .end local v10           #i$:I
    .restart local v9       #i$:I
    :goto_57
    if-ge v9, v12, :cond_6d

    #@59
    aget v17, v6, v9

    #@5b
    .line 759
    .local v17, type:I
    move/from16 v0, v17

    #@5d
    invoke-virtual {v13, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpmap(I)Ljava/lang/String;

    #@60
    move-result-object v18

    #@61
    move/from16 v0, v17

    #@63
    invoke-virtual {v13, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->getFmtp(I)Ljava/lang/String;

    #@66
    move-result-object v19

    #@67
    invoke-static/range {v17 .. v19}, Landroid/net/rtp/AudioCodec;->getCodec(ILjava/lang/String;Ljava/lang/String;)Landroid/net/rtp/AudioCodec;

    #@6a
    move-result-object v7

    #@6b
    .line 761
    if-eqz v7, :cond_db

    #@6d
    .line 765
    .end local v17           #type:I
    :cond_6d
    if-eqz v7, :cond_143

    #@6f
    .line 766
    const-string v18, "audio"

    #@71
    move-object/from16 v0, p0

    #@73
    iget-object v0, v0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@75
    move-object/from16 v19, v0

    #@77
    invoke-virtual/range {v19 .. v19}, Landroid/net/rtp/AudioStream;->getLocalPort()I

    #@7a
    move-result v19

    #@7b
    const/16 v20, 0x1

    #@7d
    const-string v21, "RTP/AVP"

    #@7f
    move-object/from16 v0, v18

    #@81
    move/from16 v1, v19

    #@83
    move/from16 v2, v20

    #@85
    move-object/from16 v3, v21

    #@87
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/net/sip/SimpleSessionDescription;->newMedia(Ljava/lang/String;IILjava/lang/String;)Landroid/net/sip/SimpleSessionDescription$Media;

    #@8a
    move-result-object v15

    #@8b
    .line 768
    .local v15, reply:Landroid/net/sip/SimpleSessionDescription$Media;
    iget v0, v7, Landroid/net/rtp/AudioCodec;->type:I

    #@8d
    move/from16 v18, v0

    #@8f
    iget-object v0, v7, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@91
    move-object/from16 v19, v0

    #@93
    iget-object v0, v7, Landroid/net/rtp/AudioCodec;->fmtp:Ljava/lang/String;

    #@95
    move-object/from16 v20, v0

    #@97
    move/from16 v0, v18

    #@99
    move-object/from16 v1, v19

    #@9b
    move-object/from16 v2, v20

    #@9d
    invoke-virtual {v15, v0, v1, v2}, Landroid/net/sip/SimpleSessionDescription$Media;->setRtpPayload(ILjava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 771
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpPayloadTypes()[I

    #@a3
    move-result-object v6

    #@a4
    array-length v12, v6

    #@a5
    const/4 v9, 0x0

    #@a6
    :goto_a6
    if-ge v9, v12, :cond_df

    #@a8
    aget v17, v6, v9

    #@aa
    .line 772
    .restart local v17       #type:I
    move/from16 v0, v17

    #@ac
    invoke-virtual {v13, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpmap(I)Ljava/lang/String;

    #@af
    move-result-object v16

    #@b0
    .line 773
    .local v16, rtpmap:Ljava/lang/String;
    iget v0, v7, Landroid/net/rtp/AudioCodec;->type:I

    #@b2
    move/from16 v18, v0

    #@b4
    move/from16 v0, v17

    #@b6
    move/from16 v1, v18

    #@b8
    if-eq v0, v1, :cond_d8

    #@ba
    if-eqz v16, :cond_d8

    #@bc
    const-string/jumbo v18, "telephone-event"

    #@bf
    move-object/from16 v0, v16

    #@c1
    move-object/from16 v1, v18

    #@c3
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c6
    move-result v18

    #@c7
    if-eqz v18, :cond_d8

    #@c9
    .line 775
    move/from16 v0, v17

    #@cb
    invoke-virtual {v13, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->getFmtp(I)Ljava/lang/String;

    #@ce
    move-result-object v18

    #@cf
    move/from16 v0, v17

    #@d1
    move-object/from16 v1, v16

    #@d3
    move-object/from16 v2, v18

    #@d5
    invoke-virtual {v15, v0, v1, v2}, Landroid/net/sip/SimpleSessionDescription$Media;->setRtpPayload(ILjava/lang/String;Ljava/lang/String;)V

    #@d8
    .line 771
    :cond_d8
    add-int/lit8 v9, v9, 0x1

    #@da
    goto :goto_a6

    #@db
    .line 758
    .end local v15           #reply:Landroid/net/sip/SimpleSessionDescription$Media;
    .end local v16           #rtpmap:Ljava/lang/String;
    :cond_db
    add-int/lit8 v9, v9, 0x1

    #@dd
    goto/16 :goto_57

    #@df
    .line 781
    .end local v17           #type:I
    .restart local v15       #reply:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_df
    const-string/jumbo v18, "recvonly"

    #@e2
    move-object/from16 v0, v18

    #@e4
    invoke-virtual {v13, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@e7
    move-result-object v18

    #@e8
    if-eqz v18, :cond_fb

    #@ea
    .line 782
    const-string/jumbo v18, "sendonly"

    #@ed
    const-string v19, ""

    #@ef
    move-object/from16 v0, v18

    #@f1
    move-object/from16 v1, v19

    #@f3
    invoke-virtual {v4, v0, v1}, Landroid/net/sip/SimpleSessionDescription;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@f6
    .line 753
    .end local v6           #arr$:[I
    :cond_f6
    :goto_f6
    add-int/lit8 v9, v10, 0x1

    #@f8
    move v10, v9

    #@f9
    .end local v9           #i$:I
    .restart local v10       #i$:I
    goto/16 :goto_2d

    #@fb
    .line 783
    .end local v10           #i$:I
    .restart local v6       #arr$:[I
    .restart local v9       #i$:I
    :cond_fb
    const-string/jumbo v18, "sendonly"

    #@fe
    move-object/from16 v0, v18

    #@100
    invoke-virtual {v13, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@103
    move-result-object v18

    #@104
    if-eqz v18, :cond_113

    #@106
    .line 784
    const-string/jumbo v18, "recvonly"

    #@109
    const-string v19, ""

    #@10b
    move-object/from16 v0, v18

    #@10d
    move-object/from16 v1, v19

    #@10f
    invoke-virtual {v4, v0, v1}, Landroid/net/sip/SimpleSessionDescription;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@112
    goto :goto_f6

    #@113
    .line 785
    :cond_113
    const-string/jumbo v18, "recvonly"

    #@116
    move-object/from16 v0, v18

    #@118
    invoke-virtual {v14, v0}, Landroid/net/sip/SimpleSessionDescription;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@11b
    move-result-object v18

    #@11c
    if-eqz v18, :cond_12b

    #@11e
    .line 786
    const-string/jumbo v18, "sendonly"

    #@121
    const-string v19, ""

    #@123
    move-object/from16 v0, v18

    #@125
    move-object/from16 v1, v19

    #@127
    invoke-virtual {v4, v0, v1}, Landroid/net/sip/SimpleSessionDescription;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@12a
    goto :goto_f6

    #@12b
    .line 787
    :cond_12b
    const-string/jumbo v18, "sendonly"

    #@12e
    move-object/from16 v0, v18

    #@130
    invoke-virtual {v14, v0}, Landroid/net/sip/SimpleSessionDescription;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@133
    move-result-object v18

    #@134
    if-eqz v18, :cond_f6

    #@136
    .line 788
    const-string/jumbo v18, "recvonly"

    #@139
    const-string v19, ""

    #@13b
    move-object/from16 v0, v18

    #@13d
    move-object/from16 v1, v19

    #@13f
    invoke-virtual {v4, v0, v1}, Landroid/net/sip/SimpleSessionDescription;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@142
    goto :goto_f6

    #@143
    .line 794
    .end local v6           #arr$:[I
    .end local v9           #i$:I
    .end local v12           #len$:I
    .end local v15           #reply:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_143
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getType()Ljava/lang/String;

    #@146
    move-result-object v18

    #@147
    const/16 v19, 0x0

    #@149
    const/16 v20, 0x1

    #@14b
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getProtocol()Ljava/lang/String;

    #@14e
    move-result-object v21

    #@14f
    move-object/from16 v0, v18

    #@151
    move/from16 v1, v19

    #@153
    move/from16 v2, v20

    #@155
    move-object/from16 v3, v21

    #@157
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/net/sip/SimpleSessionDescription;->newMedia(Ljava/lang/String;IILjava/lang/String;)Landroid/net/sip/SimpleSessionDescription$Media;

    #@15a
    move-result-object v15

    #@15b
    .line 796
    .restart local v15       #reply:Landroid/net/sip/SimpleSessionDescription$Media;
    invoke-virtual {v13}, Landroid/net/sip/SimpleSessionDescription$Media;->getFormats()[Ljava/lang/String;

    #@15e
    move-result-object v6

    #@15f
    .local v6, arr$:[Ljava/lang/String;
    array-length v12, v6

    #@160
    .restart local v12       #len$:I
    const/4 v9, 0x0

    #@161
    .restart local v9       #i$:I
    :goto_161
    if-ge v9, v12, :cond_f6

    #@163
    aget-object v8, v6, v9

    #@165
    .line 797
    .local v8, format:Ljava/lang/String;
    const/16 v18, 0x0

    #@167
    move-object/from16 v0, v18

    #@169
    invoke-virtual {v15, v8, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->setFormat(Ljava/lang/String;Ljava/lang/String;)V

    #@16c
    .line 796
    add-int/lit8 v9, v9, 0x1

    #@16e
    goto :goto_161

    #@16f
    .line 800
    .end local v6           #arr$:[Ljava/lang/String;
    .end local v8           #format:Ljava/lang/String;
    .end local v9           #i$:I
    .end local v12           #len$:I
    .end local v13           #media:Landroid/net/sip/SimpleSessionDescription$Media;
    .end local v15           #reply:Landroid/net/sip/SimpleSessionDescription$Media;
    .restart local v10       #i$:I
    :cond_16f
    if-nez v7, :cond_a

    #@171
    .line 801
    new-instance v18, Ljava/lang/IllegalStateException;

    #@173
    const-string v19, "Reject SDP: no suitable codecs"

    #@175
    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@178
    throw v18
.end method

.method private createContinueOffer()Landroid/net/sip/SimpleSessionDescription;
    .registers 9

    #@0
    .prologue
    .line 813
    new-instance v3, Landroid/net/sip/SimpleSessionDescription;

    #@2
    iget-wide v4, p0, Landroid/net/sip/SipAudioCall;->mSessionId:J

    #@4
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->getLocalIp()Ljava/lang/String;

    #@7
    move-result-object v6

    #@8
    invoke-direct {v3, v4, v5, v6}, Landroid/net/sip/SimpleSessionDescription;-><init>(JLjava/lang/String;)V

    #@b
    .line 815
    .local v3, offer:Landroid/net/sip/SimpleSessionDescription;
    const-string v4, "audio"

    #@d
    iget-object v5, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@f
    invoke-virtual {v5}, Landroid/net/rtp/AudioStream;->getLocalPort()I

    #@12
    move-result v5

    #@13
    const/4 v6, 0x1

    #@14
    const-string v7, "RTP/AVP"

    #@16
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/net/sip/SimpleSessionDescription;->newMedia(Ljava/lang/String;IILjava/lang/String;)Landroid/net/sip/SimpleSessionDescription$Media;

    #@19
    move-result-object v2

    #@1a
    .line 817
    .local v2, media:Landroid/net/sip/SimpleSessionDescription$Media;
    iget-object v4, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@1c
    invoke-virtual {v4}, Landroid/net/rtp/AudioStream;->getCodec()Landroid/net/rtp/AudioCodec;

    #@1f
    move-result-object v0

    #@20
    .line 818
    .local v0, codec:Landroid/net/rtp/AudioCodec;
    iget v4, v0, Landroid/net/rtp/AudioCodec;->type:I

    #@22
    iget-object v5, v0, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@24
    iget-object v6, v0, Landroid/net/rtp/AudioCodec;->fmtp:Ljava/lang/String;

    #@26
    invoke-virtual {v2, v4, v5, v6}, Landroid/net/sip/SimpleSessionDescription$Media;->setRtpPayload(ILjava/lang/String;Ljava/lang/String;)V

    #@29
    .line 819
    iget-object v4, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@2b
    invoke-virtual {v4}, Landroid/net/rtp/AudioStream;->getDtmfType()I

    #@2e
    move-result v1

    #@2f
    .line 820
    .local v1, dtmfType:I
    const/4 v4, -0x1

    #@30
    if-eq v1, v4, :cond_3a

    #@32
    .line 821
    const-string/jumbo v4, "telephone-event/8000"

    #@35
    const-string v5, "0-15"

    #@37
    invoke-virtual {v2, v1, v4, v5}, Landroid/net/sip/SimpleSessionDescription$Media;->setRtpPayload(ILjava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 823
    :cond_3a
    return-object v3
.end method

.method private createHoldOffer()Landroid/net/sip/SimpleSessionDescription;
    .registers 4

    #@0
    .prologue
    .line 807
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createContinueOffer()Landroid/net/sip/SimpleSessionDescription;

    #@3
    move-result-object v0

    #@4
    .line 808
    .local v0, offer:Landroid/net/sip/SimpleSessionDescription;
    const-string/jumbo v1, "sendonly"

    #@7
    const-string v2, ""

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/net/sip/SimpleSessionDescription;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 809
    return-object v0
.end method

.method private createListener()Landroid/net/sip/SipSession$Listener;
    .registers 2

    #@0
    .prologue
    .line 382
    new-instance v0, Landroid/net/sip/SipAudioCall$1;

    #@2
    invoke-direct {v0, p0}, Landroid/net/sip/SipAudioCall$1;-><init>(Landroid/net/sip/SipAudioCall;)V

    #@5
    return-object v0
.end method

.method private createOffer()Landroid/net/sip/SimpleSessionDescription;
    .registers 12

    #@0
    .prologue
    .line 734
    new-instance v6, Landroid/net/sip/SimpleSessionDescription;

    #@2
    iget-wide v7, p0, Landroid/net/sip/SipAudioCall;->mSessionId:J

    #@4
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->getLocalIp()Ljava/lang/String;

    #@7
    move-result-object v9

    #@8
    invoke-direct {v6, v7, v8, v9}, Landroid/net/sip/SimpleSessionDescription;-><init>(JLjava/lang/String;)V

    #@b
    .line 736
    .local v6, offer:Landroid/net/sip/SimpleSessionDescription;
    invoke-static {}, Landroid/net/rtp/AudioCodec;->getCodecs()[Landroid/net/rtp/AudioCodec;

    #@e
    move-result-object v2

    #@f
    .line 737
    .local v2, codecs:[Landroid/net/rtp/AudioCodec;
    const-string v7, "audio"

    #@11
    iget-object v8, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@13
    invoke-virtual {v8}, Landroid/net/rtp/AudioStream;->getLocalPort()I

    #@16
    move-result v8

    #@17
    const/4 v9, 0x1

    #@18
    const-string v10, "RTP/AVP"

    #@1a
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/net/sip/SimpleSessionDescription;->newMedia(Ljava/lang/String;IILjava/lang/String;)Landroid/net/sip/SimpleSessionDescription$Media;

    #@1d
    move-result-object v5

    #@1e
    .line 739
    .local v5, media:Landroid/net/sip/SimpleSessionDescription$Media;
    invoke-static {}, Landroid/net/rtp/AudioCodec;->getCodecs()[Landroid/net/rtp/AudioCodec;

    #@21
    move-result-object v0

    #@22
    .local v0, arr$:[Landroid/net/rtp/AudioCodec;
    array-length v4, v0

    #@23
    .local v4, len$:I
    const/4 v3, 0x0

    #@24
    .local v3, i$:I
    :goto_24
    if-ge v3, v4, :cond_34

    #@26
    aget-object v1, v0, v3

    #@28
    .line 740
    .local v1, codec:Landroid/net/rtp/AudioCodec;
    iget v7, v1, Landroid/net/rtp/AudioCodec;->type:I

    #@2a
    iget-object v8, v1, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@2c
    iget-object v9, v1, Landroid/net/rtp/AudioCodec;->fmtp:Ljava/lang/String;

    #@2e
    invoke-virtual {v5, v7, v8, v9}, Landroid/net/sip/SimpleSessionDescription$Media;->setRtpPayload(ILjava/lang/String;Ljava/lang/String;)V

    #@31
    .line 739
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_24

    #@34
    .line 742
    .end local v1           #codec:Landroid/net/rtp/AudioCodec;
    :cond_34
    const/16 v7, 0x7f

    #@36
    const-string/jumbo v8, "telephone-event/8000"

    #@39
    const-string v9, "0-15"

    #@3b
    invoke-virtual {v5, v7, v8, v9}, Landroid/net/sip/SimpleSessionDescription$Media;->setRtpPayload(ILjava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 743
    return-object v6
.end method

.method private getLocalIp()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1112
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SipSession;->getLocalIp()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private getPeerProfile(Landroid/net/sip/SipSession;)Landroid/net/sip/SipProfile;
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 1124
    invoke-virtual {p1}, Landroid/net/sip/SipSession;->getPeerProfile()Landroid/net/sip/SipProfile;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private grabWifiHighPerfLock()V
    .registers 4

    #@0
    .prologue
    .line 827
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@2
    if-nez v0, :cond_24

    #@4
    .line 828
    sget-object v0, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@6
    const-string v1, "acquire wifi high perf lock"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 829
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mContext:Landroid/content/Context;

    #@d
    const-string/jumbo v1, "wifi"

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@16
    const/4 v1, 0x3

    #@17
    sget-object v2, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Landroid/net/sip/SipAudioCall;->mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@1f
    .line 832
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@21
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    #@24
    .line 834
    :cond_24
    return-void
.end method

.method private isSpeakerOn()Z
    .registers 3

    #@0
    .prologue
    .line 883
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "audio"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/AudioManager;

    #@a
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method private isWifiOn()Z
    .registers 2

    #@0
    .prologue
    .line 845
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mWm:Landroid/net/wifi/WifiManager;

    #@2
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x0

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    goto :goto_d
.end method

.method private onError(ILjava/lang/String;)V
    .registers 8
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 565
    sget-object v2, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "sip session error: "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-static {p1}, Landroid/net/sip/SipErrorCode;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, ": "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 567
    iput p1, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@29
    .line 568
    iput-object p2, p0, Landroid/net/sip/SipAudioCall;->mErrorMessage:Ljava/lang/String;

    #@2b
    .line 569
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mListener:Landroid/net/sip/SipAudioCall$Listener;

    #@2d
    .line 570
    .local v0, listener:Landroid/net/sip/SipAudioCall$Listener;
    if-eqz v0, :cond_32

    #@2f
    .line 572
    :try_start_2f
    invoke-virtual {v0, p0, p1, p2}, Landroid/net/sip/SipAudioCall$Listener;->onError(Landroid/net/sip/SipAudioCall;ILjava/lang/String;)V
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_32} :catch_43

    #@32
    .line 577
    :cond_32
    :goto_32
    monitor-enter p0

    #@33
    .line 578
    const/16 v2, -0xa

    #@35
    if-eq p1, v2, :cond_3d

    #@37
    :try_start_37
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->isInCall()Z

    #@3a
    move-result v2

    #@3b
    if-nez v2, :cond_41

    #@3d
    .line 580
    :cond_3d
    const/4 v2, 0x1

    #@3e
    invoke-direct {p0, v2}, Landroid/net/sip/SipAudioCall;->close(Z)V

    #@41
    .line 582
    :cond_41
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_37 .. :try_end_42} :catchall_5e

    #@42
    .line 583
    return-void

    #@43
    .line 573
    :catch_43
    move-exception v1

    #@44
    .line 574
    .local v1, t:Ljava/lang/Throwable;
    sget-object v2, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@46
    new-instance v3, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string/jumbo v4, "onError(): "

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_32

    #@5e
    .line 582
    .end local v1           #t:Ljava/lang/Throwable;
    :catchall_5e
    move-exception v2

    #@5f
    :try_start_5f
    monitor-exit p0
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    #@60
    throw v2
.end method

.method private releaseWifiHighPerfLock()V
    .registers 3

    #@0
    .prologue
    .line 837
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@2
    if-eqz v0, :cond_14

    #@4
    .line 838
    sget-object v0, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "release wifi high perf lock"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 839
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@e
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    #@11
    .line 840
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Landroid/net/sip/SipAudioCall;->mWifiHighPerfLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@14
    .line 842
    :cond_14
    return-void
.end method

.method private setAudioGroupMode()V
    .registers 3

    #@0
    .prologue
    .line 1084
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->getAudioGroup()Landroid/net/rtp/AudioGroup;

    #@3
    move-result-object v0

    #@4
    .line 1085
    .local v0, audioGroup:Landroid/net/rtp/AudioGroup;
    if-eqz v0, :cond_e

    #@6
    .line 1086
    iget-boolean v1, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 1087
    const/4 v1, 0x0

    #@b
    invoke-virtual {v0, v1}, Landroid/net/rtp/AudioGroup;->setMode(I)V

    #@e
    .line 1096
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1088
    :cond_f
    iget-boolean v1, p0, Landroid/net/sip/SipAudioCall;->mMuted:Z

    #@11
    if-eqz v1, :cond_18

    #@13
    .line 1089
    const/4 v1, 0x1

    #@14
    invoke-virtual {v0, v1}, Landroid/net/rtp/AudioGroup;->setMode(I)V

    #@17
    goto :goto_e

    #@18
    .line 1090
    :cond_18
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->isSpeakerOn()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_23

    #@1e
    .line 1091
    const/4 v1, 0x3

    #@1f
    invoke-virtual {v0, v1}, Landroid/net/rtp/AudioGroup;->setMode(I)V

    #@22
    goto :goto_e

    #@23
    .line 1093
    :cond_23
    const/4 v1, 0x2

    #@24
    invoke-virtual {v0, v1}, Landroid/net/rtp/AudioGroup;->setMode(I)V

    #@27
    goto :goto_e
.end method

.method private declared-synchronized startAudioInternal()V
    .registers 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    #@0
    .prologue
    .line 1000
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v15, v0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@5
    if-nez v15, :cond_1b

    #@7
    .line 1001
    sget-object v15, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@9
    const-string/jumbo v16, "startAudioInternal() mPeerSd = null"

    #@c
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1002
    new-instance v15, Ljava/lang/IllegalStateException;

    #@11
    const-string/jumbo v16, "mPeerSd = null"

    #@14
    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v15
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_18

    #@18
    .line 1000
    :catchall_18
    move-exception v15

    #@19
    monitor-exit p0

    #@1a
    throw v15

    #@1b
    .line 1005
    :cond_1b
    const/4 v15, 0x0

    #@1c
    :try_start_1c
    move-object/from16 v0, p0

    #@1e
    invoke-direct {v0, v15}, Landroid/net/sip/SipAudioCall;->stopCall(Z)V

    #@21
    .line 1006
    const/4 v15, 0x1

    #@22
    move-object/from16 v0, p0

    #@24
    iput-boolean v15, v0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@26
    .line 1009
    new-instance v11, Landroid/net/sip/SimpleSessionDescription;

    #@28
    move-object/from16 v0, p0

    #@2a
    iget-object v15, v0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@2c
    invoke-direct {v11, v15}, Landroid/net/sip/SimpleSessionDescription;-><init>(Ljava/lang/String;)V

    #@2f
    .line 1011
    .local v11, offer:Landroid/net/sip/SimpleSessionDescription;
    move-object/from16 v0, p0

    #@31
    iget-object v13, v0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@33
    .line 1012
    .local v13, stream:Landroid/net/rtp/AudioStream;
    const/4 v5, 0x0

    #@34
    .line 1013
    .local v5, codec:Landroid/net/rtp/AudioCodec;
    invoke-virtual {v11}, Landroid/net/sip/SimpleSessionDescription;->getMedia()[Landroid/net/sip/SimpleSessionDescription$Media;

    #@37
    move-result-object v2

    #@38
    .local v2, arr$:[Landroid/net/sip/SimpleSessionDescription$Media;
    array-length v8, v2

    #@39
    .local v8, len$:I
    const/4 v6, 0x0

    #@3a
    .local v6, i$:I
    move v7, v6

    #@3b
    .end local v2           #arr$:[Landroid/net/sip/SimpleSessionDescription$Media;
    .end local v6           #i$:I
    .end local v8           #len$:I
    .local v7, i$:I
    :goto_3b
    if-ge v7, v8, :cond_c7

    #@3d
    aget-object v10, v2, v7

    #@3f
    .line 1014
    .local v10, media:Landroid/net/sip/SimpleSessionDescription$Media;
    if-nez v5, :cond_10e

    #@41
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getPort()I

    #@44
    move-result v15

    #@45
    if-lez v15, :cond_10e

    #@47
    const-string v15, "audio"

    #@49
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getType()Ljava/lang/String;

    #@4c
    move-result-object v16

    #@4d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v15

    #@51
    if-eqz v15, :cond_10e

    #@53
    const-string v15, "RTP/AVP"

    #@55
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getProtocol()Ljava/lang/String;

    #@58
    move-result-object v16

    #@59
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v15

    #@5d
    if-eqz v15, :cond_10e

    #@5f
    .line 1018
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpPayloadTypes()[I

    #@62
    move-result-object v3

    #@63
    .local v3, arr$:[I
    array-length v9, v3

    #@64
    .local v9, len$:I
    const/4 v6, 0x0

    #@65
    .end local v7           #i$:I
    .restart local v6       #i$:I
    :goto_65
    if-ge v6, v9, :cond_77

    #@67
    aget v14, v3, v6

    #@69
    .line 1019
    .local v14, type:I
    invoke-virtual {v10, v14}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpmap(I)Ljava/lang/String;

    #@6c
    move-result-object v15

    #@6d
    invoke-virtual {v10, v14}, Landroid/net/sip/SimpleSessionDescription$Media;->getFmtp(I)Ljava/lang/String;

    #@70
    move-result-object v16

    #@71
    invoke-static/range {v14 .. v16}, Landroid/net/rtp/AudioCodec;->getCodec(ILjava/lang/String;Ljava/lang/String;)Landroid/net/rtp/AudioCodec;

    #@74
    move-result-object v5

    #@75
    .line 1021
    if-eqz v5, :cond_ba

    #@77
    .line 1026
    .end local v14           #type:I
    :cond_77
    if-eqz v5, :cond_10e

    #@79
    .line 1028
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getAddress()Ljava/lang/String;

    #@7c
    move-result-object v1

    #@7d
    .line 1029
    .local v1, address:Ljava/lang/String;
    if-nez v1, :cond_83

    #@7f
    .line 1030
    invoke-virtual {v11}, Landroid/net/sip/SimpleSessionDescription;->getAddress()Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    .line 1032
    :cond_83
    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@86
    move-result-object v15

    #@87
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getPort()I

    #@8a
    move-result v16

    #@8b
    move/from16 v0, v16

    #@8d
    invoke-virtual {v13, v15, v0}, Landroid/net/rtp/AudioStream;->associate(Ljava/net/InetAddress;I)V

    #@90
    .line 1035
    const/4 v15, -0x1

    #@91
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setDtmfType(I)V

    #@94
    .line 1036
    invoke-virtual {v13, v5}, Landroid/net/rtp/AudioStream;->setCodec(Landroid/net/rtp/AudioCodec;)V

    #@97
    .line 1038
    invoke-virtual {v10}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpPayloadTypes()[I

    #@9a
    move-result-object v2

    #@9b
    .end local v3           #arr$:[I
    .local v2, arr$:[I
    array-length v8, v2

    #@9c
    .end local v9           #len$:I
    .restart local v8       #len$:I
    const/4 v6, 0x0

    #@9d
    :goto_9d
    if-ge v6, v8, :cond_bd

    #@9f
    aget v14, v2, v6

    #@a1
    .line 1039
    .restart local v14       #type:I
    invoke-virtual {v10, v14}, Landroid/net/sip/SimpleSessionDescription$Media;->getRtpmap(I)Ljava/lang/String;

    #@a4
    move-result-object v12

    #@a5
    .line 1040
    .local v12, rtpmap:Ljava/lang/String;
    iget v15, v5, Landroid/net/rtp/AudioCodec;->type:I

    #@a7
    if-eq v14, v15, :cond_b7

    #@a9
    if-eqz v12, :cond_b7

    #@ab
    const-string/jumbo v15, "telephone-event"

    #@ae
    invoke-virtual {v12, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b1
    move-result v15

    #@b2
    if-eqz v15, :cond_b7

    #@b4
    .line 1042
    invoke-virtual {v13, v14}, Landroid/net/rtp/AudioStream;->setDtmfType(I)V

    #@b7
    .line 1038
    :cond_b7
    add-int/lit8 v6, v6, 0x1

    #@b9
    goto :goto_9d

    #@ba
    .line 1018
    .end local v1           #address:Ljava/lang/String;
    .end local v2           #arr$:[I
    .end local v8           #len$:I
    .end local v12           #rtpmap:Ljava/lang/String;
    .restart local v3       #arr$:[I
    .restart local v9       #len$:I
    :cond_ba
    add-int/lit8 v6, v6, 0x1

    #@bc
    goto :goto_65

    #@bd
    .line 1047
    .end local v3           #arr$:[I
    .end local v9           #len$:I
    .end local v14           #type:I
    .restart local v1       #address:Ljava/lang/String;
    .restart local v2       #arr$:[I
    .restart local v8       #len$:I
    :cond_bd
    move-object/from16 v0, p0

    #@bf
    iget-boolean v15, v0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@c1
    if-eqz v15, :cond_d1

    #@c3
    .line 1048
    const/4 v15, 0x0

    #@c4
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setMode(I)V

    #@c7
    .line 1064
    .end local v1           #address:Ljava/lang/String;
    .end local v2           #arr$:[I
    .end local v6           #i$:I
    .end local v8           #len$:I
    .end local v10           #media:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_c7
    :goto_c7
    if-nez v5, :cond_113

    #@c9
    .line 1065
    new-instance v15, Ljava/lang/IllegalStateException;

    #@cb
    const-string v16, "Reject SDP: no suitable codecs"

    #@cd
    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d0
    throw v15

    #@d1
    .line 1049
    .restart local v1       #address:Ljava/lang/String;
    .restart local v2       #arr$:[I
    .restart local v6       #i$:I
    .restart local v8       #len$:I
    .restart local v10       #media:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_d1
    const-string/jumbo v15, "recvonly"

    #@d4
    invoke-virtual {v10, v15}, Landroid/net/sip/SimpleSessionDescription$Media;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@d7
    move-result-object v15

    #@d8
    if-eqz v15, :cond_df

    #@da
    .line 1050
    const/4 v15, 0x1

    #@db
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setMode(I)V

    #@de
    goto :goto_c7

    #@df
    .line 1051
    :cond_df
    const-string/jumbo v15, "sendonly"

    #@e2
    invoke-virtual {v10, v15}, Landroid/net/sip/SimpleSessionDescription$Media;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@e5
    move-result-object v15

    #@e6
    if-eqz v15, :cond_ed

    #@e8
    .line 1052
    const/4 v15, 0x2

    #@e9
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setMode(I)V

    #@ec
    goto :goto_c7

    #@ed
    .line 1053
    :cond_ed
    const-string/jumbo v15, "recvonly"

    #@f0
    invoke-virtual {v11, v15}, Landroid/net/sip/SimpleSessionDescription;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@f3
    move-result-object v15

    #@f4
    if-eqz v15, :cond_fb

    #@f6
    .line 1054
    const/4 v15, 0x1

    #@f7
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setMode(I)V

    #@fa
    goto :goto_c7

    #@fb
    .line 1055
    :cond_fb
    const-string/jumbo v15, "sendonly"

    #@fe
    invoke-virtual {v11, v15}, Landroid/net/sip/SimpleSessionDescription;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@101
    move-result-object v15

    #@102
    if-eqz v15, :cond_109

    #@104
    .line 1056
    const/4 v15, 0x2

    #@105
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setMode(I)V

    #@108
    goto :goto_c7

    #@109
    .line 1058
    :cond_109
    const/4 v15, 0x0

    #@10a
    invoke-virtual {v13, v15}, Landroid/net/rtp/AudioStream;->setMode(I)V

    #@10d
    goto :goto_c7

    #@10e
    .line 1013
    .end local v1           #address:Ljava/lang/String;
    .end local v2           #arr$:[I
    .end local v6           #i$:I
    .end local v8           #len$:I
    :cond_10e
    add-int/lit8 v6, v7, 0x1

    #@110
    .restart local v6       #i$:I
    move v7, v6

    #@111
    .end local v6           #i$:I
    .restart local v7       #i$:I
    goto/16 :goto_3b

    #@113
    .line 1068
    .end local v7           #i$:I
    .end local v10           #media:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_113
    invoke-direct/range {p0 .. p0}, Landroid/net/sip/SipAudioCall;->isWifiOn()Z

    #@116
    move-result v15

    #@117
    if-eqz v15, :cond_11c

    #@119
    invoke-direct/range {p0 .. p0}, Landroid/net/sip/SipAudioCall;->grabWifiHighPerfLock()V

    #@11c
    .line 1071
    :cond_11c
    invoke-virtual/range {p0 .. p0}, Landroid/net/sip/SipAudioCall;->getAudioGroup()Landroid/net/rtp/AudioGroup;

    #@11f
    move-result-object v4

    #@120
    .line 1072
    .local v4, audioGroup:Landroid/net/rtp/AudioGroup;
    move-object/from16 v0, p0

    #@122
    iget-boolean v15, v0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@124
    if-eqz v15, :cond_12b

    #@126
    .line 1079
    :goto_126
    invoke-direct/range {p0 .. p0}, Landroid/net/sip/SipAudioCall;->setAudioGroupMode()V
    :try_end_129
    .catchall {:try_start_1c .. :try_end_129} :catchall_18

    #@129
    .line 1080
    monitor-exit p0

    #@12a
    return-void

    #@12b
    .line 1076
    :cond_12b
    if-nez v4, :cond_132

    #@12d
    :try_start_12d
    new-instance v4, Landroid/net/rtp/AudioGroup;

    #@12f
    .end local v4           #audioGroup:Landroid/net/rtp/AudioGroup;
    invoke-direct {v4}, Landroid/net/rtp/AudioGroup;-><init>()V

    #@132
    .line 1077
    .restart local v4       #audioGroup:Landroid/net/rtp/AudioGroup;
    :cond_132
    invoke-virtual {v13, v4}, Landroid/net/rtp/AudioStream;->join(Landroid/net/rtp/AudioGroup;)V
    :try_end_135
    .catchall {:try_start_12d .. :try_end_135} :catchall_18

    #@135
    goto :goto_126
.end method

.method private stopCall(Z)V
    .registers 5
    .parameter "releaseSocket"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1099
    sget-object v0, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@3
    const-string/jumbo v1, "stop audiocall"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1100
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->releaseWifiHighPerfLock()V

    #@c
    .line 1101
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@e
    if-eqz v0, :cond_1e

    #@10
    .line 1102
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@12
    invoke-virtual {v0, v2}, Landroid/net/rtp/AudioStream;->join(Landroid/net/rtp/AudioGroup;)V

    #@15
    .line 1104
    if-eqz p1, :cond_1e

    #@17
    .line 1105
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@19
    invoke-virtual {v0}, Landroid/net/rtp/AudioStream;->release()V

    #@1c
    .line 1106
    iput-object v2, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@1e
    .line 1109
    :cond_1e
    return-void
.end method

.method private throwSipException(Ljava/lang/Throwable;)V
    .registers 4
    .parameter "throwable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 1116
    instance-of v0, p1, Landroid/net/sip/SipException;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1117
    check-cast p1, Landroid/net/sip/SipException;

    #@6
    .end local p1
    throw p1

    #@7
    .line 1119
    .restart local p1
    :cond_7
    new-instance v0, Landroid/net/sip/SipException;

    #@9
    const-string v1, ""

    #@b
    invoke-direct {v0, v1, p1}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@e
    throw v0
.end method

.method private declared-synchronized transferToNewSession()V
    .registers 6

    #@0
    .prologue
    .line 361
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/net/sip/SipAudioCall;->mTransferringSession:Landroid/net/sip/SipSession;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_23

    #@3
    if-nez v2, :cond_7

    #@5
    .line 379
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 362
    :cond_7
    :try_start_7
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@9
    .line 363
    .local v0, origin:Landroid/net/sip/SipSession;
    iget-object v2, p0, Landroid/net/sip/SipAudioCall;->mTransferringSession:Landroid/net/sip/SipSession;

    #@b
    iput-object v2, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@d
    .line 364
    const/4 v2, 0x0

    #@e
    iput-object v2, p0, Landroid/net/sip/SipAudioCall;->mTransferringSession:Landroid/net/sip/SipSession;

    #@10
    .line 367
    iget-object v2, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@12
    if-eqz v2, :cond_26

    #@14
    .line 368
    iget-object v2, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@16
    const/4 v3, 0x0

    #@17
    invoke-virtual {v2, v3}, Landroid/net/rtp/AudioStream;->join(Landroid/net/rtp/AudioGroup;)V

    #@1a
    .line 377
    :goto_1a
    if-eqz v0, :cond_1f

    #@1c
    invoke-virtual {v0}, Landroid/net/sip/SipSession;->endCall()V

    #@1f
    .line 378
    :cond_1f
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->startAudio()V
    :try_end_22
    .catchall {:try_start_7 .. :try_end_22} :catchall_23

    #@22
    goto :goto_5

    #@23
    .line 361
    .end local v0           #origin:Landroid/net/sip/SipSession;
    :catchall_23
    move-exception v2

    #@24
    monitor-exit p0

    #@25
    throw v2

    #@26
    .line 371
    .restart local v0       #origin:Landroid/net/sip/SipSession;
    :cond_26
    :try_start_26
    new-instance v2, Landroid/net/rtp/AudioStream;

    #@28
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->getLocalIp()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v2, v3}, Landroid/net/rtp/AudioStream;-><init>(Ljava/net/InetAddress;)V

    #@33
    iput-object v2, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;
    :try_end_35
    .catchall {:try_start_26 .. :try_end_35} :catchall_23
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_35} :catch_36

    #@35
    goto :goto_1a

    #@36
    .line 373
    :catch_36
    move-exception v1

    #@37
    .line 374
    .local v1, t:Ljava/lang/Throwable;
    :try_start_37
    sget-object v2, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@39
    new-instance v3, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string/jumbo v4, "transferToNewSession(): "

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_50
    .catchall {:try_start_37 .. :try_end_50} :catchall_23

    #@50
    goto :goto_1a
.end method


# virtual methods
.method public answerCall(I)V
    .registers 5
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 698
    monitor-enter p0

    #@1
    .line 699
    :try_start_1
    iget-object v1, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@3
    if-nez v1, :cond_10

    #@5
    .line 700
    new-instance v1, Landroid/net/sip/SipException;

    #@7
    const-string v2, "No call to answer"

    #@9
    invoke-direct {v1, v2}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 709
    :catchall_d
    move-exception v1

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v1

    #@10
    .line 703
    :cond_10
    :try_start_10
    new-instance v1, Landroid/net/rtp/AudioStream;

    #@12
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->getLocalIp()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@19
    move-result-object v2

    #@1a
    invoke-direct {v1, v2}, Landroid/net/rtp/AudioStream;-><init>(Ljava/net/InetAddress;)V

    #@1d
    iput-object v1, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@1f
    .line 705
    iget-object v1, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@21
    iget-object v2, p0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@23
    invoke-direct {p0, v2}, Landroid/net/sip/SipAudioCall;->createAnswer(Ljava/lang/String;)Landroid/net/sip/SimpleSessionDescription;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Landroid/net/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v1, v2, p1}, Landroid/net/sip/SipSession;->answerCall(Ljava/lang/String;I)V
    :try_end_2e
    .catchall {:try_start_10 .. :try_end_2e} :catchall_d
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_2e} :catch_30

    #@2e
    .line 709
    :try_start_2e
    monitor-exit p0

    #@2f
    .line 710
    return-void

    #@30
    .line 706
    :catch_30
    move-exception v0

    #@31
    .line 707
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Landroid/net/sip/SipException;

    #@33
    const-string v2, "answerCall()"

    #@35
    invoke-direct {v1, v2, v0}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@38
    throw v1
    :try_end_39
    .catchall {:try_start_2e .. :try_end_39} :catchall_d
.end method

.method public attachCall(Landroid/net/sip/SipSession;Ljava/lang/String;)V
    .registers 7
    .parameter "session"
    .parameter "sessionDescription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 596
    iget-object v1, p0, Landroid/net/sip/SipAudioCall;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/net/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_10

    #@8
    .line 597
    new-instance v1, Landroid/net/sip/SipException;

    #@a
    const-string v2, "VOIP API is not supported"

    #@c
    invoke-direct {v1, v2}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 600
    :cond_10
    monitor-enter p0

    #@11
    .line 601
    :try_start_11
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@13
    .line 602
    iput-object p2, p0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@15
    .line 603
    sget-object v1, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "attachCall()"

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    iget-object v3, p0, Landroid/net/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catchall {:try_start_11 .. :try_end_2f} :catchall_44

    #@2f
    .line 605
    :try_start_2f
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createListener()Landroid/net/sip/SipSession$Listener;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {p1, v1}, Landroid/net/sip/SipSession;->setListener(Landroid/net/sip/SipSession$Listener;)V
    :try_end_36
    .catchall {:try_start_2f .. :try_end_36} :catchall_44
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_36} :catch_38

    #@36
    .line 610
    :goto_36
    :try_start_36
    monitor-exit p0

    #@37
    .line 611
    return-void

    #@38
    .line 606
    :catch_38
    move-exception v0

    #@39
    .line 607
    .local v0, e:Ljava/lang/Throwable;
    sget-object v1, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@3b
    const-string v2, "attachCall()"

    #@3d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    .line 608
    invoke-direct {p0, v0}, Landroid/net/sip/SipAudioCall;->throwSipException(Ljava/lang/Throwable;)V

    #@43
    goto :goto_36

    #@44
    .line 610
    .end local v0           #e:Ljava/lang/Throwable;
    :catchall_44
    move-exception v1

    #@45
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_36 .. :try_end_46} :catchall_44

    #@46
    throw v1
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 294
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/net/sip/SipAudioCall;->close(Z)V

    #@4
    .line 295
    return-void
.end method

.method public continueCall(I)V
    .registers 4
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 725
    monitor-enter p0

    #@1
    .line 726
    :try_start_1
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@3
    if-nez v0, :cond_7

    #@5
    monitor-exit p0

    #@6
    .line 731
    :goto_6
    return-void

    #@7
    .line 727
    :cond_7
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@9
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createContinueOffer()Landroid/net/sip/SimpleSessionDescription;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/net/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1, p1}, Landroid/net/sip/SipSession;->changeCall(Ljava/lang/String;I)V

    #@14
    .line 728
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@17
    .line 729
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->setAudioGroupMode()V

    #@1a
    .line 730
    monitor-exit p0

    #@1b
    goto :goto_6

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public endCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 653
    monitor-enter p0

    #@1
    .line 654
    const/4 v0, 0x1

    #@2
    :try_start_2
    invoke-direct {p0, v0}, Landroid/net/sip/SipAudioCall;->stopCall(Z)V

    #@5
    .line 655
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@8
    .line 658
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@a
    if-eqz v0, :cond_11

    #@c
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@e
    invoke-virtual {v0}, Landroid/net/sip/SipSession;->endCall()V

    #@11
    .line 659
    :cond_11
    monitor-exit p0

    #@12
    .line 660
    return-void

    #@13
    .line 659
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_2 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public getAudioGroup()Landroid/net/rtp/AudioGroup;
    .registers 2

    #@0
    .prologue
    .line 954
    monitor-enter p0

    #@1
    .line 955
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioGroup:Landroid/net/rtp/AudioGroup;

    #@3
    if-eqz v0, :cond_9

    #@5
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioGroup:Landroid/net/rtp/AudioGroup;

    #@7
    monitor-exit p0

    #@8
    .line 956
    :goto_8
    return-object v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@b
    if-nez v0, :cond_13

    #@d
    const/4 v0, 0x0

    #@e
    :goto_e
    monitor-exit p0

    #@f
    goto :goto_8

    #@10
    .line 957
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_10

    #@12
    throw v0

    #@13
    .line 956
    :cond_13
    :try_start_13
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@15
    invoke-virtual {v0}, Landroid/net/rtp/AudioStream;->getGroup()Landroid/net/rtp/AudioGroup;
    :try_end_18
    .catchall {:try_start_13 .. :try_end_18} :catchall_10

    #@18
    move-result-object v0

    #@19
    goto :goto_e
.end method

.method public getAudioStream()Landroid/net/rtp/AudioStream;
    .registers 2

    #@0
    .prologue
    .line 934
    monitor-enter p0

    #@1
    .line 935
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 936
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getLocalProfile()Landroid/net/sip/SipProfile;
    .registers 2

    #@0
    .prologue
    .line 318
    monitor-enter p0

    #@1
    .line 319
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 320
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getPeerProfile()Landroid/net/sip/SipProfile;
    .registers 2

    #@0
    .prologue
    .line 329
    monitor-enter p0

    #@1
    .line 330
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@3
    if-nez v0, :cond_8

    #@5
    const/4 v0, 0x0

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@a
    invoke-virtual {v0}, Landroid/net/sip/SipSession;->getPeerProfile()Landroid/net/sip/SipProfile;

    #@d
    move-result-object v0

    #@e
    goto :goto_6

    #@f
    .line 331
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public getSipSession()Landroid/net/sip/SipSession;
    .registers 2

    #@0
    .prologue
    .line 355
    monitor-enter p0

    #@1
    .line 356
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 357
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getState()I
    .registers 2

    #@0
    .prologue
    .line 341
    monitor-enter p0

    #@1
    .line 342
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@3
    if-nez v0, :cond_8

    #@5
    const/4 v0, 0x0

    #@6
    monitor-exit p0

    #@7
    .line 343
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@a
    invoke-virtual {v0}, Landroid/net/sip/SipSession;->getState()I

    #@d
    move-result v0

    #@e
    monitor-exit p0

    #@f
    goto :goto_7

    #@10
    .line 344
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public holdCall(I)V
    .registers 4
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 675
    monitor-enter p0

    #@1
    .line 676
    :try_start_1
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@3
    if-eqz v0, :cond_7

    #@5
    monitor-exit p0

    #@6
    .line 684
    :goto_6
    return-void

    #@7
    .line 677
    :cond_7
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@9
    if-nez v0, :cond_16

    #@b
    .line 678
    new-instance v0, Landroid/net/sip/SipException;

    #@d
    const-string v1, "Not in a call to hold call"

    #@f
    invoke-direct {v0, v1}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 683
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0

    #@16
    .line 680
    :cond_16
    :try_start_16
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@18
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createHoldOffer()Landroid/net/sip/SimpleSessionDescription;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Landroid/net/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1, p1}, Landroid/net/sip/SipSession;->changeCall(Ljava/lang/String;I)V

    #@23
    .line 681
    const/4 v0, 0x1

    #@24
    iput-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@26
    .line 682
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->setAudioGroupMode()V

    #@29
    .line 683
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_16 .. :try_end_2a} :catchall_13

    #@2a
    goto :goto_6
.end method

.method public isInCall()Z
    .registers 2

    #@0
    .prologue
    .line 274
    monitor-enter p0

    #@1
    .line 275
    :try_start_1
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 276
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public isMuted()Z
    .registers 2

    #@0
    .prologue
    .line 862
    monitor-enter p0

    #@1
    .line 863
    :try_start_1
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mMuted:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 864
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public isOnHold()Z
    .registers 2

    #@0
    .prologue
    .line 285
    monitor-enter p0

    #@1
    .line 286
    :try_start_1
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 287
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public makeCall(Landroid/net/sip/SipProfile;Landroid/net/sip/SipSession;I)V
    .registers 7
    .parameter "peerProfile"
    .parameter "sipSession"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 630
    iget-object v1, p0, Landroid/net/sip/SipAudioCall;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/net/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_10

    #@8
    .line 631
    new-instance v1, Landroid/net/sip/SipException;

    #@a
    const-string v2, "VOIP API is not supported"

    #@c
    invoke-direct {v1, v2}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 634
    :cond_10
    monitor-enter p0

    #@11
    .line 635
    :try_start_11
    iput-object p2, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;
    :try_end_13
    .catchall {:try_start_11 .. :try_end_13} :catchall_40

    #@13
    .line 637
    :try_start_13
    new-instance v1, Landroid/net/rtp/AudioStream;

    #@15
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->getLocalIp()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v1, v2}, Landroid/net/rtp/AudioStream;-><init>(Ljava/net/InetAddress;)V

    #@20
    iput-object v1, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@22
    .line 639
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createListener()Landroid/net/sip/SipSession$Listener;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {p2, v1}, Landroid/net/sip/SipSession;->setListener(Landroid/net/sip/SipSession$Listener;)V

    #@29
    .line 640
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->createOffer()Landroid/net/sip/SimpleSessionDescription;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Landroid/net/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {p2, p1, v1, p3}, Landroid/net/sip/SipSession;->makeCall(Landroid/net/sip/SipProfile;Ljava/lang/String;I)V
    :try_end_34
    .catchall {:try_start_13 .. :try_end_34} :catchall_40
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_34} :catch_36

    #@34
    .line 645
    :try_start_34
    monitor-exit p0

    #@35
    .line 646
    return-void

    #@36
    .line 642
    :catch_36
    move-exception v0

    #@37
    .line 643
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Landroid/net/sip/SipException;

    #@39
    const-string/jumbo v2, "makeCall()"

    #@3c
    invoke-direct {v1, v2, v0}, Landroid/net/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3f
    throw v1

    #@40
    .line 645
    .end local v0           #e:Ljava/io/IOException;
    :catchall_40
    move-exception v1

    #@41
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_34 .. :try_end_42} :catchall_40

    #@42
    throw v1
.end method

.method public sendDtmf(I)V
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 897
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/net/sip/SipAudioCall;->sendDtmf(ILandroid/os/Message;)V

    #@4
    .line 898
    return-void
.end method

.method public sendDtmf(ILandroid/os/Message;)V
    .registers 7
    .parameter "code"
    .parameter "result"

    #@0
    .prologue
    .line 911
    monitor-enter p0

    #@1
    .line 912
    :try_start_1
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->getAudioGroup()Landroid/net/rtp/AudioGroup;

    #@4
    move-result-object v0

    #@5
    .line 913
    .local v0, audioGroup:Landroid/net/rtp/AudioGroup;
    if-eqz v0, :cond_2f

    #@7
    iget-object v1, p0, Landroid/net/sip/SipAudioCall;->mSipSession:Landroid/net/sip/SipSession;

    #@9
    if-eqz v1, :cond_2f

    #@b
    const/16 v1, 0x8

    #@d
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->getState()I

    #@10
    move-result v2

    #@11
    if-ne v1, v2, :cond_2f

    #@13
    .line 915
    sget-object v1, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string/jumbo v3, "send DTMF: "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 916
    invoke-virtual {v0, p1}, Landroid/net/rtp/AudioGroup;->sendDtmf(I)V

    #@2f
    .line 918
    :cond_2f
    if-eqz p2, :cond_34

    #@31
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@34
    .line 919
    :cond_34
    monitor-exit p0

    #@35
    .line 920
    return-void

    #@36
    .line 919
    .end local v0           #audioGroup:Landroid/net/rtp/AudioGroup;
    :catchall_36
    move-exception v1

    #@37
    monitor-exit p0
    :try_end_38
    .catchall {:try_start_1 .. :try_end_38} :catchall_36

    #@38
    throw v1
.end method

.method public setAudioGroup(Landroid/net/rtp/AudioGroup;)V
    .registers 3
    .parameter "group"

    #@0
    .prologue
    .line 973
    monitor-enter p0

    #@1
    .line 974
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@3
    if-eqz v0, :cond_12

    #@5
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@7
    invoke-virtual {v0}, Landroid/net/rtp/AudioStream;->getGroup()Landroid/net/rtp/AudioGroup;

    #@a
    move-result-object v0

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 975
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mAudioStream:Landroid/net/rtp/AudioStream;

    #@f
    invoke-virtual {v0, p1}, Landroid/net/rtp/AudioStream;->join(Landroid/net/rtp/AudioGroup;)V

    #@12
    .line 977
    :cond_12
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mAudioGroup:Landroid/net/rtp/AudioGroup;

    #@14
    .line 978
    monitor-exit p0

    #@15
    .line 979
    return-void

    #@16
    .line 978
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public setListener(Landroid/net/sip/SipAudioCall$Listener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 220
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/net/sip/SipAudioCall;->setListener(Landroid/net/sip/SipAudioCall$Listener;Z)V

    #@4
    .line 221
    return-void
.end method

.method public setListener(Landroid/net/sip/SipAudioCall$Listener;Z)V
    .registers 7
    .parameter "listener"
    .parameter "callbackImmediately"

    #@0
    .prologue
    .line 234
    iput-object p1, p0, Landroid/net/sip/SipAudioCall;->mListener:Landroid/net/sip/SipAudioCall$Listener;

    #@2
    .line 236
    if-eqz p1, :cond_6

    #@4
    if-nez p2, :cond_7

    #@6
    .line 266
    :cond_6
    :goto_6
    return-void

    #@7
    .line 238
    :cond_7
    :try_start_7
    iget v2, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@9
    if-eqz v2, :cond_1d

    #@b
    .line 239
    iget v2, p0, Landroid/net/sip/SipAudioCall;->mErrorCode:I

    #@d
    iget-object v3, p0, Landroid/net/sip/SipAudioCall;->mErrorMessage:Ljava/lang/String;

    #@f
    invoke-virtual {p1, p0, v2, v3}, Landroid/net/sip/SipAudioCall$Listener;->onError(Landroid/net/sip/SipAudioCall;ILjava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_12} :catch_13

    #@12
    goto :goto_6

    #@13
    .line 263
    :catch_13
    move-exception v1

    #@14
    .line 264
    .local v1, t:Ljava/lang/Throwable;
    sget-object v2, Landroid/net/sip/SipAudioCall;->TAG:Ljava/lang/String;

    #@16
    const-string/jumbo v3, "setListener()"

    #@19
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_6

    #@1d
    .line 240
    .end local v1           #t:Ljava/lang/Throwable;
    :cond_1d
    :try_start_1d
    iget-boolean v2, p0, Landroid/net/sip/SipAudioCall;->mInCall:Z

    #@1f
    if-eqz v2, :cond_2d

    #@21
    .line 241
    iget-boolean v2, p0, Landroid/net/sip/SipAudioCall;->mHold:Z

    #@23
    if-eqz v2, :cond_29

    #@25
    .line 242
    invoke-virtual {p1, p0}, Landroid/net/sip/SipAudioCall$Listener;->onCallHeld(Landroid/net/sip/SipAudioCall;)V

    #@28
    goto :goto_6

    #@29
    .line 244
    :cond_29
    invoke-virtual {p1, p0}, Landroid/net/sip/SipAudioCall$Listener;->onCallEstablished(Landroid/net/sip/SipAudioCall;)V

    #@2c
    goto :goto_6

    #@2d
    .line 247
    :cond_2d
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->getState()I

    #@30
    move-result v0

    #@31
    .line 248
    .local v0, state:I
    packed-switch v0, :pswitch_data_4a

    #@34
    :pswitch_34
    goto :goto_6

    #@35
    .line 250
    :pswitch_35
    invoke-virtual {p1, p0}, Landroid/net/sip/SipAudioCall$Listener;->onReadyToCall(Landroid/net/sip/SipAudioCall;)V

    #@38
    goto :goto_6

    #@39
    .line 253
    :pswitch_39
    invoke-virtual {p0}, Landroid/net/sip/SipAudioCall;->getPeerProfile()Landroid/net/sip/SipProfile;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {p1, p0, v2}, Landroid/net/sip/SipAudioCall$Listener;->onRinging(Landroid/net/sip/SipAudioCall;Landroid/net/sip/SipProfile;)V

    #@40
    goto :goto_6

    #@41
    .line 256
    :pswitch_41
    invoke-virtual {p1, p0}, Landroid/net/sip/SipAudioCall$Listener;->onCalling(Landroid/net/sip/SipAudioCall;)V

    #@44
    goto :goto_6

    #@45
    .line 259
    :pswitch_45
    invoke-virtual {p1, p0}, Landroid/net/sip/SipAudioCall$Listener;->onRingingBack(Landroid/net/sip/SipAudioCall;)V
    :try_end_48
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_48} :catch_13

    #@48
    goto :goto_6

    #@49
    .line 248
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_35
        :pswitch_34
        :pswitch_34
        :pswitch_39
        :pswitch_34
        :pswitch_41
        :pswitch_45
    .end packed-switch
.end method

.method public setSpeakerMode(Z)V
    .registers 4
    .parameter "speakerMode"

    #@0
    .prologue
    .line 875
    monitor-enter p0

    #@1
    .line 876
    :try_start_1
    iget-object v0, p0, Landroid/net/sip/SipAudioCall;->mContext:Landroid/content/Context;

    #@3
    const-string v1, "audio"

    #@5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/media/AudioManager;

    #@b
    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    #@e
    .line 878
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->setAudioGroupMode()V

    #@11
    .line 879
    monitor-exit p0

    #@12
    .line 880
    return-void

    #@13
    .line 879
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public startAudio()V
    .registers 4

    #@0
    .prologue
    .line 991
    :try_start_0
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->startAudioInternal()V
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_3} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_3} :catch_e

    #@3
    .line 997
    :goto_3
    return-void

    #@4
    .line 992
    :catch_4
    move-exception v0

    #@5
    .line 993
    .local v0, e:Ljava/net/UnknownHostException;
    const/4 v1, -0x7

    #@6
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-direct {p0, v1, v2}, Landroid/net/sip/SipAudioCall;->onError(ILjava/lang/String;)V

    #@d
    goto :goto_3

    #@e
    .line 994
    .end local v0           #e:Ljava/net/UnknownHostException;
    :catch_e
    move-exception v0

    #@f
    .line 995
    .local v0, e:Ljava/lang/Throwable;
    const/4 v1, -0x4

    #@10
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-direct {p0, v1, v2}, Landroid/net/sip/SipAudioCall;->onError(ILjava/lang/String;)V

    #@17
    goto :goto_3
.end method

.method public toggleMute()V
    .registers 2

    #@0
    .prologue
    .line 850
    monitor-enter p0

    #@1
    .line 851
    :try_start_1
    iget-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mMuted:Z

    #@3
    if-nez v0, :cond_d

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    iput-boolean v0, p0, Landroid/net/sip/SipAudioCall;->mMuted:Z

    #@8
    .line 852
    invoke-direct {p0}, Landroid/net/sip/SipAudioCall;->setAudioGroupMode()V

    #@b
    .line 853
    monitor-exit p0

    #@c
    .line 854
    return-void

    #@d
    .line 851
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_6

    #@f
    .line 853
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method
