.class Landroid/net/sip/SimpleSessionDescription$Fields;
.super Ljava/lang/Object;
.source "SimpleSessionDescription.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/sip/SimpleSessionDescription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Fields"
.end annotation


# instance fields
.field private final mLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOrder:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "order"

    #@0
    .prologue
    .line 394
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 392
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@a
    .line 395
    iput-object p1, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mOrder:Ljava/lang/String;

    #@c
    .line 396
    return-void
.end method

.method static synthetic access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 390
    invoke-direct {p0, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->parse(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/StringBuilder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 390
    invoke-direct {p0, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->write(Ljava/lang/StringBuilder;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;C)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 390
    invoke-direct {p0, p1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->get(Ljava/lang/String;C)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;CLjava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 390
    invoke-direct {p0, p1, p2, p3}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@3
    return-void
.end method

.method private cut(Ljava/lang/String;C)[Ljava/lang/String;
    .registers 9
    .parameter "prefix"
    .parameter "delimiter"

    #@0
    .prologue
    .line 550
    iget-object v5, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v5

    #@6
    new-array v4, v5, [Ljava/lang/String;

    #@8
    .line 551
    .local v4, names:[Ljava/lang/String;
    const/4 v2, 0x0

    #@9
    .line 552
    .local v2, length:I
    iget-object v5, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v5

    #@13
    if-eqz v5, :cond_39

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    check-cast v3, Ljava/lang/String;

    #@1b
    .line 553
    .local v3, line:Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_f

    #@21
    .line 554
    invoke-virtual {v3, p2}, Ljava/lang/String;->indexOf(I)I

    #@24
    move-result v0

    #@25
    .line 555
    .local v0, i:I
    const/4 v5, -0x1

    #@26
    if-ne v0, v5, :cond_2c

    #@28
    .line 556
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@2b
    move-result v0

    #@2c
    .line 558
    :cond_2c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2f
    move-result v5

    #@30
    invoke-virtual {v3, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    aput-object v5, v4, v2

    #@36
    .line 559
    add-int/lit8 v2, v2, 0x1

    #@38
    goto :goto_f

    #@39
    .line 562
    .end local v0           #i:I
    .end local v3           #line:Ljava/lang/String;
    :cond_39
    invoke-static {v4, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@3c
    move-result-object v5

    #@3d
    check-cast v5, [Ljava/lang/String;

    #@3f
    return-object v5
.end method

.method private find(Ljava/lang/String;C)I
    .registers 7
    .parameter "key"
    .parameter "delimiter"

    #@0
    .prologue
    .line 569
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v1

    #@4
    .line 570
    .local v1, length:I
    iget-object v3, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v3

    #@a
    add-int/lit8 v0, v3, -0x1

    #@c
    .local v0, i:I
    :goto_c
    if-ltz v0, :cond_2c

    #@e
    .line 571
    iget-object v3, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Ljava/lang/String;

    #@16
    .line 572
    .local v2, line:Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_29

    #@1c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1f
    move-result v3

    #@20
    if-eq v3, v1, :cond_28

    #@22
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    #@25
    move-result v3

    #@26
    if-ne v3, p2, :cond_29

    #@28
    .line 577
    .end local v0           #i:I
    .end local v2           #line:Ljava/lang/String;
    :cond_28
    :goto_28
    return v0

    #@29
    .line 570
    .restart local v0       #i:I
    .restart local v2       #line:Ljava/lang/String;
    :cond_29
    add-int/lit8 v0, v0, -0x1

    #@2b
    goto :goto_c

    #@2c
    .line 577
    .end local v2           #line:Ljava/lang/String;
    :cond_2c
    const/4 v0, -0x1

    #@2d
    goto :goto_28
.end method

.method private get(Ljava/lang/String;C)Ljava/lang/String;
    .registers 7
    .parameter "key"
    .parameter "delimiter"

    #@0
    .prologue
    .line 604
    invoke-direct {p0, p1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->find(Ljava/lang/String;C)I

    #@3
    move-result v0

    #@4
    .line 605
    .local v0, index:I
    const/4 v3, -0x1

    #@5
    if-ne v0, v3, :cond_9

    #@7
    .line 606
    const/4 v3, 0x0

    #@8
    .line 610
    :goto_8
    return-object v3

    #@9
    .line 608
    :cond_9
    iget-object v3, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/lang/String;

    #@11
    .line 609
    .local v2, line:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@14
    move-result v1

    #@15
    .line 610
    .local v1, length:I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v3

    #@19
    if-ne v3, v1, :cond_1e

    #@1b
    const-string v3, ""

    #@1d
    goto :goto_8

    #@1e
    :cond_1e
    add-int/lit8 v3, v1, 0x1

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    goto :goto_8
.end method

.method private parse(Ljava/lang/String;)V
    .registers 8
    .parameter "line"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    .line 528
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    #@5
    move-result v2

    #@6
    .line 529
    .local v2, type:C
    iget-object v3, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mOrder:Ljava/lang/String;

    #@8
    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    #@b
    move-result v3

    #@c
    if-ne v3, v4, :cond_f

    #@e
    .line 544
    :goto_e
    return-void

    #@f
    .line 532
    :cond_f
    const/16 v0, 0x3d

    #@11
    .line 533
    .local v0, delimiter:C
    const-string v3, "a=rtpmap:"

    #@13
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_21

    #@19
    const-string v3, "a=fmtp:"

    #@1b
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_2f

    #@21
    .line 534
    :cond_21
    const/16 v0, 0x20

    #@23
    .line 538
    :cond_23
    :goto_23
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    #@26
    move-result v1

    #@27
    .line 539
    .local v1, i:I
    if-ne v1, v4, :cond_3a

    #@29
    .line 540
    const-string v3, ""

    #@2b
    invoke-direct {p0, p1, v0, v3}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@2e
    goto :goto_e

    #@2f
    .line 535
    .end local v1           #i:I
    :cond_2f
    const/16 v3, 0x62

    #@31
    if-eq v2, v3, :cond_37

    #@33
    const/16 v3, 0x61

    #@35
    if-ne v2, v3, :cond_23

    #@37
    .line 536
    :cond_37
    const/16 v0, 0x3a

    #@39
    goto :goto_23

    #@3a
    .line 542
    .restart local v1       #i:I
    :cond_3a
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    add-int/lit8 v4, v1, 0x1

    #@40
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-direct {p0, v3, v0, v4}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@47
    goto :goto_e
.end method

.method private set(Ljava/lang/String;CLjava/lang/String;)V
    .registers 7
    .parameter "key"
    .parameter "delimiter"
    .parameter "value"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 585
    invoke-direct {p0, p1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->find(Ljava/lang/String;C)I

    #@4
    move-result v0

    #@5
    .line 586
    .local v0, index:I
    if-eqz p3, :cond_30

    #@7
    .line 587
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_22

    #@d
    .line 588
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object p1

    #@22
    .line 590
    :cond_22
    if-ne v0, v2, :cond_2a

    #@24
    .line 591
    iget-object v1, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29
    .line 598
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 593
    :cond_2a
    iget-object v1, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@2f
    goto :goto_29

    #@30
    .line 595
    :cond_30
    if-eq v0, v2, :cond_29

    #@32
    .line 596
    iget-object v1, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@37
    goto :goto_29
.end method

.method private write(Ljava/lang/StringBuilder;)V
    .registers 8
    .parameter "buffer"

    #@0
    .prologue
    .line 514
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v4, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mOrder:Ljava/lang/String;

    #@3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@6
    move-result v4

    #@7
    if-ge v0, v4, :cond_35

    #@9
    .line 515
    iget-object v4, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mOrder:Ljava/lang/String;

    #@b
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    #@e
    move-result v3

    #@f
    .line 516
    .local v3, type:C
    iget-object v4, p0, Landroid/net/sip/SimpleSessionDescription$Fields;->mLines:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :cond_15
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_32

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Ljava/lang/String;

    #@21
    .line 517
    .local v2, line:Ljava/lang/String;
    const/4 v4, 0x0

    #@22
    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    #@25
    move-result v4

    #@26
    if-ne v4, v3, :cond_15

    #@28
    .line 518
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    const-string v5, "\r\n"

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    goto :goto_15

    #@32
    .line 514
    .end local v2           #line:Ljava/lang/String;
    :cond_32
    add-int/lit8 v0, v0, 0x1

    #@34
    goto :goto_1

    #@35
    .line 522
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #type:C
    :cond_35
    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v6, 0x2

    #@2
    .line 402
    const-string v4, "c"

    #@4
    const/16 v5, 0x3d

    #@6
    invoke-direct {p0, v4, v5}, Landroid/net/sip/SimpleSessionDescription$Fields;->get(Ljava/lang/String;C)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 403
    .local v0, address:Ljava/lang/String;
    if-nez v0, :cond_d

    #@c
    .line 411
    :cond_c
    :goto_c
    return-object v3

    #@d
    .line 406
    :cond_d
    const-string v4, " "

    #@f
    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 407
    .local v1, parts:[Ljava/lang/String;
    array-length v4, v1

    #@14
    const/4 v5, 0x3

    #@15
    if-ne v4, v5, :cond_c

    #@17
    .line 410
    aget-object v3, v1, v6

    #@19
    const/16 v4, 0x2f

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v2

    #@1f
    .line 411
    .local v2, slash:I
    if-gez v2, :cond_24

    #@21
    aget-object v3, v1, v6

    #@23
    goto :goto_c

    #@24
    :cond_24
    aget-object v3, v1, v6

    #@26
    const/4 v4, 0x0

    #@27
    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    goto :goto_c
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 501
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "a="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    const/16 v1, 0x3a

    #@15
    invoke-direct {p0, v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->get(Ljava/lang/String;C)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    return-object v0
.end method

.method public getAttributeNames()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 493
    const-string v0, "a="

    #@2
    const/16 v1, 0x3a

    #@4
    invoke-direct {p0, v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->cut(Ljava/lang/String;C)[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getBandwidth(Ljava/lang/String;)I
    .registers 6
    .parameter "type"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 471
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v3, "b="

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    const/16 v3, 0x3a

    #@16
    invoke-direct {p0, v2, v3}, Landroid/net/sip/SimpleSessionDescription$Fields;->get(Ljava/lang/String;C)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 472
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_20

    #@1c
    .line 474
    :try_start_1c
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1f
    .catch Ljava/lang/NumberFormatException; {:try_start_1c .. :try_end_1f} :catch_21

    #@1f
    move-result v1

    #@20
    .line 478
    :cond_20
    :goto_20
    return v1

    #@21
    .line 475
    :catch_21
    move-exception v2

    #@22
    .line 476
    invoke-virtual {p0, p1, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->setBandwidth(Ljava/lang/String;I)V

    #@25
    goto :goto_20
.end method

.method public getBandwidthTypes()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 463
    const-string v0, "b="

    #@2
    const/16 v1, 0x3a

    #@4
    invoke-direct {p0, v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->cut(Ljava/lang/String;C)[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getEncryptionKey()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 442
    const-string/jumbo v3, "k"

    #@4
    const/16 v4, 0x3d

    #@6
    invoke-direct {p0, v3, v4}, Landroid/net/sip/SimpleSessionDescription$Fields;->get(Ljava/lang/String;C)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 443
    .local v1, encryption:Ljava/lang/String;
    if-nez v1, :cond_d

    #@c
    .line 447
    :cond_c
    :goto_c
    return-object v2

    #@d
    .line 446
    :cond_d
    const/16 v3, 0x3a

    #@f
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    #@12
    move-result v0

    #@13
    .line 447
    .local v0, colon:I
    const/4 v3, -0x1

    #@14
    if-eq v0, v3, :cond_c

    #@16
    const/4 v2, 0x0

    #@17
    add-int/lit8 v3, v0, 0x1

    #@19
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    goto :goto_c
.end method

.method public getEncryptionMethod()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 430
    const-string/jumbo v2, "k"

    #@3
    const/16 v3, 0x3d

    #@5
    invoke-direct {p0, v2, v3}, Landroid/net/sip/SimpleSessionDescription$Fields;->get(Ljava/lang/String;C)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 431
    .local v1, encryption:Ljava/lang/String;
    if-nez v1, :cond_d

    #@b
    .line 432
    const/4 v1, 0x0

    #@c
    .line 435
    .end local v1           #encryption:Ljava/lang/String;
    :cond_c
    :goto_c
    return-object v1

    #@d
    .line 434
    .restart local v1       #encryption:Ljava/lang/String;
    :cond_d
    const/16 v2, 0x3a

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    #@12
    move-result v0

    #@13
    .line 435
    .local v0, colon:I
    const/4 v2, -0x1

    #@14
    if-eq v0, v2, :cond_c

    #@16
    const/4 v2, 0x0

    #@17
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    goto :goto_c
.end method

.method public setAddress(Ljava/lang/String;)V
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 419
    if-eqz p1, :cond_1d

    #@2
    .line 420
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const/16 v0, 0x3a

    #@9
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    #@c
    move-result v0

    #@d
    if-gez v0, :cond_25

    #@f
    const-string v0, "IN IP4 "

    #@11
    :goto_11
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object p1

    #@1d
    .line 423
    :cond_1d
    const-string v0, "c"

    #@1f
    const/16 v1, 0x3d

    #@21
    invoke-direct {p0, v0, v1, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@24
    .line 424
    return-void

    #@25
    .line 420
    :cond_25
    const-string v0, "IN IP6 "

    #@27
    goto :goto_11
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 510
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "a="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    const/16 v1, 0x3a

    #@15
    invoke-direct {p0, v0, v1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@18
    .line 511
    return-void
.end method

.method public setBandwidth(Ljava/lang/String;I)V
    .registers 6
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "b="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    const/16 v2, 0x3a

    #@15
    if-gez p2, :cond_1c

    #@17
    const/4 v0, 0x0

    #@18
    :goto_18
    invoke-direct {p0, v1, v2, v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@1b
    .line 487
    return-void

    #@1c
    .line 486
    :cond_1c
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    goto :goto_18
.end method

.method public setEncryption(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "method"
    .parameter "key"

    #@0
    .prologue
    .line 455
    const-string/jumbo v0, "k"

    #@3
    const/16 v1, 0x3d

    #@5
    if-eqz p1, :cond_9

    #@7
    if-nez p2, :cond_d

    #@9
    .end local p1
    :cond_9
    :goto_9
    invoke-direct {p0, v0, v1, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->set(Ljava/lang/String;CLjava/lang/String;)V

    #@c
    .line 457
    return-void

    #@d
    .line 455
    .restart local p1
    :cond_d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const/16 v3, 0x3a

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object p1

    #@24
    goto :goto_9
.end method
