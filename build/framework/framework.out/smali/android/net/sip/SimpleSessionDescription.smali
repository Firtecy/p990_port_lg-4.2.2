.class public Landroid/net/sip/SimpleSessionDescription;
.super Ljava/lang/Object;
.source "SimpleSessionDescription.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/sip/SimpleSessionDescription$1;,
        Landroid/net/sip/SimpleSessionDescription$Fields;,
        Landroid/net/sip/SimpleSessionDescription$Media;
    }
.end annotation


# instance fields
.field private final mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

.field private final mMedia:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/sip/SimpleSessionDescription$Media;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .registers 11
    .parameter "sessionId"
    .parameter "address"

    #@0
    .prologue
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 58
    new-instance v0, Landroid/net/sip/SimpleSessionDescription$Fields;

    #@5
    const-string/jumbo v1, "voscbtka"

    #@8
    invoke-direct {v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;-><init>(Ljava/lang/String;)V

    #@b
    iput-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@d
    .line 59
    new-instance v0, Ljava/util/ArrayList;

    #@f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mMedia:Ljava/util/ArrayList;

    #@14
    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const/16 v0, 0x3a

    #@1b
    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v0

    #@1f
    if-gez v0, :cond_86

    #@21
    const-string v0, "IN IP4 "

    #@23
    :goto_23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object p3

    #@2f
    .line 69
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@31
    const-string/jumbo v1, "v=0"

    #@34
    invoke-static {v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V

    #@37
    .line 70
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@39
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@3b
    const-string/jumbo v2, "o=- %d %d %s"

    #@3e
    const/4 v3, 0x3

    #@3f
    new-array v3, v3, [Ljava/lang/Object;

    #@41
    const/4 v4, 0x0

    #@42
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@45
    move-result-object v5

    #@46
    aput-object v5, v3, v4

    #@48
    const/4 v4, 0x1

    #@49
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4c
    move-result-wide v5

    #@4d
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@50
    move-result-object v5

    #@51
    aput-object v5, v3, v4

    #@53
    const/4 v4, 0x2

    #@54
    aput-object p3, v3, v4

    #@56
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    invoke-static {v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V

    #@5d
    .line 72
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@5f
    const-string/jumbo v1, "s=-"

    #@62
    invoke-static {v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V

    #@65
    .line 73
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@67
    const-string/jumbo v1, "t=0 0"

    #@6a
    invoke-static {v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V

    #@6d
    .line 74
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@6f
    new-instance v1, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v2, "c="

    #@76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v1

    #@7a
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    invoke-static {v0, v1}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V

    #@85
    .line 75
    return-void

    #@86
    .line 68
    :cond_86
    const-string v0, "IN IP6 "

    #@88
    goto :goto_23
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 25
    .parameter "message"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 58
    new-instance v19, Landroid/net/sip/SimpleSessionDescription$Fields;

    #@5
    const-string/jumbo v20, "voscbtka"

    #@8
    invoke-direct/range {v19 .. v20}, Landroid/net/sip/SimpleSessionDescription$Fields;-><init>(Ljava/lang/String;)V

    #@b
    move-object/from16 v0, v19

    #@d
    move-object/from16 v1, p0

    #@f
    iput-object v0, v1, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@11
    .line 59
    new-instance v19, Ljava/util/ArrayList;

    #@13
    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    #@16
    move-object/from16 v0, v19

    #@18
    move-object/from16 v1, p0

    #@1a
    iput-object v0, v1, Landroid/net/sip/SimpleSessionDescription;->mMedia:Ljava/util/ArrayList;

    #@1c
    .line 83
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1f
    move-result-object v19

    #@20
    const-string v20, " +"

    #@22
    const-string v21, " "

    #@24
    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v19

    #@28
    const-string v20, "[\r\n]+"

    #@2a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d
    move-result-object v15

    #@2e
    .line 84
    .local v15, lines:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@30
    iget-object v8, v0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@32
    .line 86
    .local v8, fields:Landroid/net/sip/SimpleSessionDescription$Fields;
    move-object v5, v15

    #@33
    .local v5, arr$:[Ljava/lang/String;
    array-length v12, v5

    #@34
    .local v12, len$:I
    const/4 v10, 0x0

    #@35
    .local v10, i$:I
    move v11, v10

    #@36
    .end local v5           #arr$:[Ljava/lang/String;
    .end local v10           #i$:I
    .end local v12           #len$:I
    .local v11, i$:I
    :goto_36
    if-ge v11, v12, :cond_f5

    #@38
    aget-object v14, v5, v11

    #@3a
    .line 88
    .local v14, line:Ljava/lang/String;
    const/16 v19, 0x1

    #@3c
    :try_start_3c
    move/from16 v0, v19

    #@3e
    invoke-virtual {v14, v0}, Ljava/lang/String;->charAt(I)C

    #@41
    move-result v19

    #@42
    const/16 v20, 0x3d

    #@44
    move/from16 v0, v19

    #@46
    move/from16 v1, v20

    #@48
    if-eq v0, v1, :cond_6c

    #@4a
    .line 89
    new-instance v19, Ljava/lang/IllegalArgumentException;

    #@4c
    invoke-direct/range {v19 .. v19}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@4f
    throw v19
    :try_end_50
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_50} :catch_50

    #@50
    .line 104
    .end local v11           #i$:I
    :catch_50
    move-exception v7

    #@51
    .line 105
    .local v7, e:Ljava/lang/Exception;
    new-instance v19, Ljava/lang/IllegalArgumentException;

    #@53
    new-instance v20, Ljava/lang/StringBuilder;

    #@55
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v21, "Invalid SDP: "

    #@5a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v20

    #@5e
    move-object/from16 v0, v20

    #@60
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v20

    #@64
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v20

    #@68
    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v19

    #@6c
    .line 91
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v11       #i$:I
    :cond_6c
    const/16 v19, 0x0

    #@6e
    :try_start_6e
    move/from16 v0, v19

    #@70
    invoke-virtual {v14, v0}, Ljava/lang/String;->charAt(I)C

    #@73
    move-result v19

    #@74
    const/16 v20, 0x6d

    #@76
    move/from16 v0, v19

    #@78
    move/from16 v1, v20

    #@7a
    if-ne v0, v1, :cond_f1

    #@7c
    .line 92
    const/16 v19, 0x2

    #@7e
    move/from16 v0, v19

    #@80
    invoke-virtual {v14, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@83
    move-result-object v19

    #@84
    const-string v20, " "

    #@86
    const/16 v21, 0x4

    #@88
    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@8b
    move-result-object v17

    #@8c
    .line 93
    .local v17, parts:[Ljava/lang/String;
    const/16 v19, 0x1

    #@8e
    aget-object v19, v17, v19

    #@90
    const-string v20, "/"

    #@92
    const/16 v21, 0x2

    #@94
    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@97
    move-result-object v18

    #@98
    .line 94
    .local v18, ports:[Ljava/lang/String;
    const/16 v19, 0x0

    #@9a
    aget-object v20, v17, v19

    #@9c
    const/16 v19, 0x0

    #@9e
    aget-object v19, v18, v19

    #@a0
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a3
    move-result v21

    #@a4
    move-object/from16 v0, v18

    #@a6
    array-length v0, v0

    #@a7
    move/from16 v19, v0

    #@a9
    const/16 v22, 0x2

    #@ab
    move/from16 v0, v19

    #@ad
    move/from16 v1, v22

    #@af
    if-ge v0, v1, :cond_e1

    #@b1
    const/16 v19, 0x1

    #@b3
    :goto_b3
    const/16 v22, 0x2

    #@b5
    aget-object v22, v17, v22

    #@b7
    move-object/from16 v0, p0

    #@b9
    move-object/from16 v1, v20

    #@bb
    move/from16 v2, v21

    #@bd
    move/from16 v3, v19

    #@bf
    move-object/from16 v4, v22

    #@c1
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/net/sip/SimpleSessionDescription;->newMedia(Ljava/lang/String;IILjava/lang/String;)Landroid/net/sip/SimpleSessionDescription$Media;

    #@c4
    move-result-object v16

    #@c5
    .line 97
    .local v16, media:Landroid/net/sip/SimpleSessionDescription$Media;
    const/16 v19, 0x3

    #@c7
    aget-object v19, v17, v19

    #@c9
    const-string v20, " "

    #@cb
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@ce
    move-result-object v6

    #@cf
    .local v6, arr$:[Ljava/lang/String;
    array-length v13, v6

    #@d0
    .local v13, len$:I
    const/4 v10, 0x0

    #@d1
    .end local v11           #i$:I
    .restart local v10       #i$:I
    :goto_d1
    if-ge v10, v13, :cond_ea

    #@d3
    aget-object v9, v6, v10

    #@d5
    .line 98
    .local v9, format:Ljava/lang/String;
    const/16 v19, 0x0

    #@d7
    move-object/from16 v0, v16

    #@d9
    move-object/from16 v1, v19

    #@db
    invoke-virtual {v0, v9, v1}, Landroid/net/sip/SimpleSessionDescription$Media;->setFormat(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    .line 97
    add-int/lit8 v10, v10, 0x1

    #@e0
    goto :goto_d1

    #@e1
    .line 94
    .end local v6           #arr$:[Ljava/lang/String;
    .end local v9           #format:Ljava/lang/String;
    .end local v10           #i$:I
    .end local v13           #len$:I
    .end local v16           #media:Landroid/net/sip/SimpleSessionDescription$Media;
    .restart local v11       #i$:I
    :cond_e1
    const/16 v19, 0x1

    #@e3
    aget-object v19, v18, v19

    #@e5
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@e8
    move-result v19

    #@e9
    goto :goto_b3

    #@ea
    .line 100
    .end local v11           #i$:I
    .restart local v6       #arr$:[Ljava/lang/String;
    .restart local v10       #i$:I
    .restart local v13       #len$:I
    .restart local v16       #media:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_ea
    move-object/from16 v8, v16

    #@ec
    .line 86
    .end local v6           #arr$:[Ljava/lang/String;
    .end local v10           #i$:I
    .end local v13           #len$:I
    .end local v16           #media:Landroid/net/sip/SimpleSessionDescription$Media;
    .end local v17           #parts:[Ljava/lang/String;
    .end local v18           #ports:[Ljava/lang/String;
    :goto_ec
    add-int/lit8 v10, v11, 0x1

    #@ee
    .restart local v10       #i$:I
    move v11, v10

    #@ef
    .end local v10           #i$:I
    .restart local v11       #i$:I
    goto/16 :goto_36

    #@f1
    .line 102
    :cond_f1
    invoke-static {v8, v14}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$000(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/String;)V
    :try_end_f4
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_f4} :catch_50

    #@f4
    goto :goto_ec

    #@f5
    .line 108
    .end local v14           #line:Ljava/lang/String;
    :cond_f5
    return-void
.end method


# virtual methods
.method public encode()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 139
    .local v0, buffer:Ljava/lang/StringBuilder;
    iget-object v3, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@7
    invoke-static {v3, v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->access$200(Landroid/net/sip/SimpleSessionDescription$Fields;Ljava/lang/StringBuilder;)V

    #@a
    .line 140
    iget-object v3, p0, Landroid/net/sip/SimpleSessionDescription;->mMedia:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v1

    #@10
    .local v1, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_20

    #@16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/net/sip/SimpleSessionDescription$Media;

    #@1c
    .line 141
    .local v2, media:Landroid/net/sip/SimpleSessionDescription$Media;
    invoke-static {v2, v0}, Landroid/net/sip/SimpleSessionDescription$Media;->access$300(Landroid/net/sip/SimpleSessionDescription$Media;Ljava/lang/StringBuilder;)V

    #@1f
    goto :goto_10

    #@20
    .line 143
    .end local v2           #media:Landroid/net/sip/SimpleSessionDescription$Media;
    :cond_20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    return-object v3
.end method

.method public getAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->getAddress()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAttributeNames()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->getAttributeNames()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getBandwidth(Ljava/lang/String;)I
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->getBandwidth(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getBandwidthTypes()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->getBandwidthTypes()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getEncryptionKey()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->getEncryptionKey()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getEncryptionMethod()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SimpleSessionDescription$Fields;->getEncryptionMethod()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMedia()[Landroid/net/sip/SimpleSessionDescription$Media;
    .registers 3

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mMedia:Ljava/util/ArrayList;

    #@2
    iget-object v1, p0, Landroid/net/sip/SimpleSessionDescription;->mMedia:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    new-array v1, v1, [Landroid/net/sip/SimpleSessionDescription$Media;

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, [Landroid/net/sip/SimpleSessionDescription$Media;

    #@10
    return-object v0
.end method

.method public newMedia(Ljava/lang/String;IILjava/lang/String;)Landroid/net/sip/SimpleSessionDescription$Media;
    .registers 11
    .parameter "type"
    .parameter "port"
    .parameter "portCount"
    .parameter "protocol"

    #@0
    .prologue
    .line 120
    new-instance v0, Landroid/net/sip/SimpleSessionDescription$Media;

    #@2
    const/4 v5, 0x0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move-object v4, p4

    #@7
    invoke-direct/range {v0 .. v5}, Landroid/net/sip/SimpleSessionDescription$Media;-><init>(Ljava/lang/String;IILjava/lang/String;Landroid/net/sip/SimpleSessionDescription$1;)V

    #@a
    .line 121
    .local v0, media:Landroid/net/sip/SimpleSessionDescription$Media;
    iget-object v1, p0, Landroid/net/sip/SimpleSessionDescription;->mMedia:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f
    .line 122
    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/sip/SimpleSessionDescription$Fields;->setAddress(Ljava/lang/String;)V

    #@5
    .line 159
    return-void
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 228
    return-void
.end method

.method public setBandwidth(Ljava/lang/String;I)V
    .registers 4
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->setBandwidth(Ljava/lang/String;I)V

    #@5
    .line 204
    return-void
.end method

.method public setEncryption(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "method"
    .parameter "key"

    #@0
    .prologue
    .line 180
    iget-object v0, p0, Landroid/net/sip/SimpleSessionDescription;->mFields:Landroid/net/sip/SimpleSessionDescription$Fields;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/net/sip/SimpleSessionDescription$Fields;->setEncryption(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 181
    return-void
.end method
