.class public Landroid/net/sip/SipErrorCode;
.super Ljava/lang/Object;
.source "SipErrorCode.java"


# static fields
.field public static final CLIENT_ERROR:I = -0x4

.field public static final CROSS_DOMAIN_AUTHENTICATION:I = -0xb

.field public static final DATA_CONNECTION_LOST:I = -0xa

.field public static final INVALID_CREDENTIALS:I = -0x8

.field public static final INVALID_REMOTE_URI:I = -0x6

.field public static final IN_PROGRESS:I = -0x9

.field public static final NO_ERROR:I = 0x0

.field public static final PEER_NOT_REACHABLE:I = -0x7

.field public static final SERVER_ERROR:I = -0x2

.field public static final SERVER_UNREACHABLE:I = -0xc

.field public static final SOCKET_ERROR:I = -0x1

.field public static final TIME_OUT:I = -0x5

.field public static final TRANSACTION_TERMINTED:I = -0x3


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 99
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 100
    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .registers 2
    .parameter "errorCode"

    #@0
    .prologue
    .line 67
    packed-switch p0, :pswitch_data_2e

    #@3
    .line 95
    const-string v0, "UNKNOWN"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 69
    :pswitch_6
    const-string v0, "NO_ERROR"

    #@8
    goto :goto_5

    #@9
    .line 71
    :pswitch_9
    const-string v0, "SOCKET_ERROR"

    #@b
    goto :goto_5

    #@c
    .line 73
    :pswitch_c
    const-string v0, "SERVER_ERROR"

    #@e
    goto :goto_5

    #@f
    .line 75
    :pswitch_f
    const-string v0, "TRANSACTION_TERMINTED"

    #@11
    goto :goto_5

    #@12
    .line 77
    :pswitch_12
    const-string v0, "CLIENT_ERROR"

    #@14
    goto :goto_5

    #@15
    .line 79
    :pswitch_15
    const-string v0, "TIME_OUT"

    #@17
    goto :goto_5

    #@18
    .line 81
    :pswitch_18
    const-string v0, "INVALID_REMOTE_URI"

    #@1a
    goto :goto_5

    #@1b
    .line 83
    :pswitch_1b
    const-string v0, "PEER_NOT_REACHABLE"

    #@1d
    goto :goto_5

    #@1e
    .line 85
    :pswitch_1e
    const-string v0, "INVALID_CREDENTIALS"

    #@20
    goto :goto_5

    #@21
    .line 87
    :pswitch_21
    const-string v0, "IN_PROGRESS"

    #@23
    goto :goto_5

    #@24
    .line 89
    :pswitch_24
    const-string v0, "DATA_CONNECTION_LOST"

    #@26
    goto :goto_5

    #@27
    .line 91
    :pswitch_27
    const-string v0, "CROSS_DOMAIN_AUTHENTICATION"

    #@29
    goto :goto_5

    #@2a
    .line 93
    :pswitch_2a
    const-string v0, "SERVER_UNREACHABLE"

    #@2c
    goto :goto_5

    #@2d
    .line 67
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch -0xc
        :pswitch_2a
        :pswitch_27
        :pswitch_24
        :pswitch_21
        :pswitch_1e
        :pswitch_1b
        :pswitch_18
        :pswitch_15
        :pswitch_12
        :pswitch_f
        :pswitch_c
        :pswitch_9
        :pswitch_6
    .end packed-switch
.end method
