.class public Landroid/net/InterfaceConfiguration;
.super Ljava/lang/Object;
.source "InterfaceConfiguration.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/InterfaceConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private static final FLAG_DOWN:Ljava/lang/String; = "down"

.field private static final FLAG_UP:Ljava/lang/String; = "up"


# instance fields
.field private mAddr:Landroid/net/LinkAddress;

.field private mFlags:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHwAddr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 139
    new-instance v0, Landroid/net/InterfaceConfiguration$1;

    #@2
    invoke-direct {v0}, Landroid/net/InterfaceConfiguration$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/InterfaceConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@9
    return-void
.end method

.method static synthetic access$002(Landroid/net/InterfaceConfiguration;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 31
    iput-object p1, p0, Landroid/net/InterfaceConfiguration;->mHwAddr:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Landroid/net/InterfaceConfiguration;Landroid/net/LinkAddress;)Landroid/net/LinkAddress;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 31
    iput-object p1, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/net/InterfaceConfiguration;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method private static validateFlag(Ljava/lang/String;)V
    .registers 4
    .parameter "flag"

    #@0
    .prologue
    .line 160
    const/16 v0, 0x20

    #@2
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    #@5
    move-result v0

    #@6
    if-ltz v0, :cond_21

    #@8
    .line 161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "flag contains space: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 163
    :cond_21
    return-void
.end method


# virtual methods
.method public clearFlag(Ljava/lang/String;)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 58
    invoke-static {p1}, Landroid/net/InterfaceConfiguration;->validateFlag(Ljava/lang/String;)V

    #@3
    .line 59
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@8
    .line 60
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getFlags()Ljava/lang/Iterable;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method public getHardwareAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mHwAddr:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLinkAddress()Landroid/net/LinkAddress;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@2
    return-object v0
.end method

.method public hasFlag(Ljava/lang/String;)Z
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 53
    invoke-static {p1}, Landroid/net/InterfaceConfiguration;->validateFlag(Ljava/lang/String;)V

    #@3
    .line 54
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public isActive()Z
    .registers 8

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 108
    :try_start_1
    const-string/jumbo v6, "up"

    #@4
    invoke-virtual {p0, v6}, Landroid/net/InterfaceConfiguration;->hasFlag(Ljava/lang/String;)Z

    #@7
    move-result v6

    #@8
    if-eqz v6, :cond_1d

    #@a
    .line 109
    iget-object v6, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@c
    invoke-virtual {v6}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6}, Ljava/net/InetAddress;->getAddress()[B

    #@13
    move-result-object v0

    #@14
    .local v0, arr$:[B
    array-length v4, v0

    #@15
    .local v4, len$:I
    const/4 v3, 0x0

    #@16
    .local v3, i$:I
    :goto_16
    if-ge v3, v4, :cond_1d

    #@18
    aget-byte v1, v0, v3
    :try_end_1a
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1a} :catch_21

    #@1a
    .line 110
    .local v1, b:B
    if-eqz v1, :cond_1e

    #@1c
    const/4 v5, 0x1

    #@1d
    .line 116
    .end local v0           #arr$:[B
    .end local v1           #b:B
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_1d
    :goto_1d
    return v5

    #@1e
    .line 109
    .restart local v0       #arr$:[B
    .restart local v1       #b:B
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_1e
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_16

    #@21
    .line 113
    .end local v0           #arr$:[B
    .end local v1           #b:B
    .end local v3           #i$:I
    .end local v4           #len$:I
    :catch_21
    move-exception v2

    #@22
    .line 114
    .local v2, e:Ljava/lang/NullPointerException;
    goto :goto_1d
.end method

.method public setFlag(Ljava/lang/String;)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 63
    invoke-static {p1}, Landroid/net/InterfaceConfiguration;->validateFlag(Ljava/lang/String;)V

    #@3
    .line 64
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@8
    .line 65
    return-void
.end method

.method public setHardwareAddress(Ljava/lang/String;)V
    .registers 2
    .parameter "hwAddr"

    #@0
    .prologue
    .line 96
    iput-object p1, p0, Landroid/net/InterfaceConfiguration;->mHwAddr:Ljava/lang/String;

    #@2
    .line 97
    return-void
.end method

.method public setInterfaceDown()V
    .registers 3

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@2
    const-string/jumbo v1, "up"

    #@5
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@8
    .line 80
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@a
    const-string v1, "down"

    #@c
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@f
    .line 81
    return-void
.end method

.method public setInterfaceUp()V
    .registers 3

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@2
    const-string v1, "down"

    #@4
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@7
    .line 72
    iget-object v0, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@9
    const-string/jumbo v1, "up"

    #@c
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@f
    .line 73
    return-void
.end method

.method public setLinkAddress(Landroid/net/LinkAddress;)V
    .registers 2
    .parameter "addr"

    #@0
    .prologue
    .line 88
    iput-object p1, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@2
    .line 89
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 42
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "mHwAddr="

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mHwAddr:Ljava/lang/String;

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    .line 43
    const-string v1, " mAddr="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@19
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 44
    const-string v1, " mFlags="

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {p0}, Landroid/net/InterfaceConfiguration;->getFlags()Ljava/lang/Iterable;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 126
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mHwAddr:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 127
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@7
    if-eqz v2, :cond_31

    #@9
    .line 128
    const/4 v2, 0x1

    #@a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@d
    .line 129
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mAddr:Landroid/net/LinkAddress;

    #@f
    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@12
    .line 133
    :goto_12
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@14
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    #@17
    move-result v2

    #@18
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 134
    iget-object v2, p0, Landroid/net/InterfaceConfiguration;->mFlags:Ljava/util/HashSet;

    #@1d
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v1

    #@21
    .local v1, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_36

    #@27
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Ljava/lang/String;

    #@2d
    .line 135
    .local v0, flag:Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@30
    goto :goto_21

    #@31
    .line 131
    .end local v0           #flag:Ljava/lang/String;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_31
    const/4 v2, 0x0

    #@32
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@35
    goto :goto_12

    #@36
    .line 137
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_36
    return-void
.end method
