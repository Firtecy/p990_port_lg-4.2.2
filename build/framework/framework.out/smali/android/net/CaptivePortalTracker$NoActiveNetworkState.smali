.class Landroid/net/CaptivePortalTracker$NoActiveNetworkState;
.super Lcom/android/internal/util/State;
.source "CaptivePortalTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/CaptivePortalTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoActiveNetworkState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/CaptivePortalTracker;


# direct methods
.method private constructor <init>(Landroid/net/CaptivePortalTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 159
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;-><init>(Landroid/net/CaptivePortalTracker;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/net/CaptivePortalTracker;->access$602(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    #@6
    .line 165
    iget-object v0, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-static {v0, v1}, Landroid/net/CaptivePortalTracker;->access$700(Landroid/net/CaptivePortalTracker;Z)V

    #@c
    .line 166
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 173
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_2c

    #@5
    .line 182
    const/4 v1, 0x0

    #@6
    .line 184
    :goto_6
    return v1

    #@7
    .line 175
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v0, Landroid/net/NetworkInfo;

    #@b
    .line 176
    .local v0, info:Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_29

    #@11
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@13
    invoke-static {v1, v0}, Landroid/net/CaptivePortalTracker;->access$800(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_29

    #@19
    .line 177
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@1b
    invoke-static {v1, v0}, Landroid/net/CaptivePortalTracker;->access$602(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    #@1e
    .line 178
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@20
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@22
    invoke-static {v2}, Landroid/net/CaptivePortalTracker;->access$900(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/net/CaptivePortalTracker;->access$1000(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V

    #@29
    .line 184
    :cond_29
    const/4 v1, 0x1

    #@2a
    goto :goto_6

    #@2b
    .line 173
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method
