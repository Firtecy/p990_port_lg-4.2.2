.class public Landroid/net/TrafficStats;
.super Ljava/lang/Object;
.source "TrafficStats.java"


# static fields
.field public static final GB_IN_BYTES:J = 0x40000000L

.field public static final KB_IN_BYTES:J = 0x400L

.field public static final MB_IN_BYTES:J = 0x100000L

.field public static final TAG_SYSTEM_BACKUP:I = -0xfd

.field public static final TAG_SYSTEM_DOWNLOAD:I = -0xff

.field public static final TAG_SYSTEM_MEDIA:I = -0xfe

.field private static final TYPE_RX_BYTES:I = 0x0

.field private static final TYPE_RX_PACKETS:I = 0x1

.field private static final TYPE_TX_BYTES:I = 0x2

.field private static final TYPE_TX_PACKETS:I = 0x3

.field public static final UID_REMOVED:I = -0x4

.field public static final UID_TETHERING:I = -0x5

.field public static final UNSUPPORTED:I = -0x1

.field private static sActiveProfilingStart:Landroid/net/NetworkStats;

.field private static sProfilingLock:Ljava/lang/Object;

.field private static sStatsService:Landroid/net/INetworkStatsService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 110
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/net/TrafficStats;->sProfilingLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static clearThreadStatsTag()V
    .registers 1

    #@0
    .prologue
    .line 137
    const/4 v0, -0x1

    #@1
    invoke-static {v0}, Lcom/android/server/NetworkManagementSocketTagger;->setThreadSocketStatsTag(I)V

    #@4
    .line 138
    return-void
.end method

.method public static clearThreadStatsUid()V
    .registers 1

    #@0
    .prologue
    .line 159
    const/4 v0, -0x1

    #@1
    invoke-static {v0}, Lcom/android/server/NetworkManagementSocketTagger;->setThreadSocketStatsUid(I)V

    #@4
    .line 160
    return-void
.end method

.method public static closeQuietly(Landroid/net/INetworkStatsSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 252
    if-eqz p0, :cond_5

    #@2
    .line 254
    :try_start_2
    invoke-interface {p0}, Landroid/net/INetworkStatsSession;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_8

    #@5
    .line 260
    :cond_5
    :goto_5
    return-void

    #@6
    .line 255
    :catch_6
    move-exception v0

    #@7
    .line 256
    .local v0, rethrown:Ljava/lang/RuntimeException;
    throw v0

    #@8
    .line 257
    .end local v0           #rethrown:Ljava/lang/RuntimeException;
    :catch_8
    move-exception v1

    #@9
    goto :goto_5
.end method

.method private static getDataLayerSnapshotForUid(Landroid/content/Context;)Landroid/net/NetworkStats;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 596
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v1

    #@4
    .line 598
    .local v1, uid:I
    :try_start_4
    invoke-static {}, Landroid/net/TrafficStats;->getStatsService()Landroid/net/INetworkStatsService;

    #@7
    move-result-object v2

    #@8
    invoke-interface {v2, v1}, Landroid/net/INetworkStatsService;->getDataLayerSnapshotForUid(I)Landroid/net/NetworkStats;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_b} :catch_d

    #@b
    move-result-object v2

    #@c
    return-object v2

    #@d
    .line 599
    :catch_d
    move-exception v0

    #@e
    .line 600
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@10
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@13
    throw v2
.end method

.method private static getMobileIfaces()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 612
    :try_start_0
    invoke-static {}, Landroid/net/TrafficStats;->getStatsService()Landroid/net/INetworkStatsService;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_1a

    #@6
    invoke-static {}, Landroid/net/TrafficStats;->getStatsService()Landroid/net/INetworkStatsService;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_23

    #@c
    invoke-static {}, Landroid/net/TrafficStats;->getStatsService()Landroid/net/INetworkStatsService;

    #@f
    move-result-object v1

    #@10
    invoke-interface {v1}, Landroid/net/INetworkStatsService;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v1}, Landroid/os/IBinder;->pingBinder()Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_23

    #@1a
    .line 614
    :cond_1a
    const/4 v1, 0x1

    #@1b
    new-array v1, v1, [Ljava/lang/String;

    #@1d
    const/4 v2, 0x0

    #@1e
    const-string v3, ""

    #@20
    aput-object v3, v1, v2

    #@22
    .line 617
    :goto_22
    return-object v1

    #@23
    :cond_23
    invoke-static {}, Landroid/net/TrafficStats;->getStatsService()Landroid/net/INetworkStatsService;

    #@26
    move-result-object v1

    #@27
    invoke-interface {v1}, Landroid/net/INetworkStatsService;->getMobileIfaces()[Ljava/lang/String;
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_2a} :catch_2c

    #@2a
    move-result-object v1

    #@2b
    goto :goto_22

    #@2c
    .line 618
    :catch_2c
    move-exception v0

    #@2d
    .line 619
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@2f
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@32
    throw v1
.end method

.method public static getMobileRxBytes()J
    .registers 8

    #@0
    .prologue
    .line 335
    const-wide/16 v4, 0x0

    #@2
    .line 337
    .local v4, total:J
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@5
    move-result-object v6

    #@6
    if-nez v6, :cond_b

    #@8
    .line 338
    const-wide/16 v4, 0x0

    #@a
    .line 347
    .local v0, arr$:[Ljava/lang/String;
    .local v1, i$:I
    .local v3, len$:I
    :cond_a
    return-wide v4

    #@b
    .line 340
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #len$:I
    :cond_b
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@10
    .restart local v3       #len$:I
    const/4 v1, 0x0

    #@11
    .restart local v1       #i$:I
    :goto_11
    if-ge v1, v3, :cond_a

    #@13
    aget-object v2, v0, v1

    #@15
    .line 341
    .local v2, iface:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v6

    #@19
    if-gtz v6, :cond_1e

    #@1b
    .line 340
    :goto_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_11

    #@1e
    .line 345
    :cond_1e
    invoke-static {v2}, Landroid/net/TrafficStats;->getRxBytes(Ljava/lang/String;)J

    #@21
    move-result-wide v6

    #@22
    add-long/2addr v4, v6

    #@23
    goto :goto_1b
.end method

.method public static getMobileRxPackets()J
    .registers 8

    #@0
    .prologue
    .line 291
    const-wide/16 v4, 0x0

    #@2
    .line 293
    .local v4, total:J
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@5
    move-result-object v6

    #@6
    if-nez v6, :cond_b

    #@8
    .line 294
    const-wide/16 v4, 0x0

    #@a
    .line 303
    .local v0, arr$:[Ljava/lang/String;
    .local v1, i$:I
    .local v3, len$:I
    :cond_a
    return-wide v4

    #@b
    .line 296
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #len$:I
    :cond_b
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@10
    .restart local v3       #len$:I
    const/4 v1, 0x0

    #@11
    .restart local v1       #i$:I
    :goto_11
    if-ge v1, v3, :cond_a

    #@13
    aget-object v2, v0, v1

    #@15
    .line 297
    .local v2, iface:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v6

    #@19
    if-gtz v6, :cond_1e

    #@1b
    .line 296
    :goto_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_11

    #@1e
    .line 301
    :cond_1e
    invoke-static {v2}, Landroid/net/TrafficStats;->getRxPackets(Ljava/lang/String;)J

    #@21
    move-result-wide v6

    #@22
    add-long/2addr v4, v6

    #@23
    goto :goto_1b
.end method

.method public static getMobileTxBytes()J
    .registers 8

    #@0
    .prologue
    .line 313
    const-wide/16 v4, 0x0

    #@2
    .line 315
    .local v4, total:J
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@5
    move-result-object v6

    #@6
    if-nez v6, :cond_b

    #@8
    .line 316
    const-wide/16 v4, 0x0

    #@a
    .line 325
    .local v0, arr$:[Ljava/lang/String;
    .local v1, i$:I
    .local v3, len$:I
    :cond_a
    return-wide v4

    #@b
    .line 318
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #len$:I
    :cond_b
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@10
    .restart local v3       #len$:I
    const/4 v1, 0x0

    #@11
    .restart local v1       #i$:I
    :goto_11
    if-ge v1, v3, :cond_a

    #@13
    aget-object v2, v0, v1

    #@15
    .line 319
    .local v2, iface:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v6

    #@19
    if-gtz v6, :cond_1e

    #@1b
    .line 318
    :goto_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_11

    #@1e
    .line 323
    :cond_1e
    invoke-static {v2}, Landroid/net/TrafficStats;->getTxBytes(Ljava/lang/String;)J

    #@21
    move-result-wide v6

    #@22
    add-long/2addr v4, v6

    #@23
    goto :goto_1b
.end method

.method public static getMobileTxPackets()J
    .registers 8

    #@0
    .prologue
    .line 269
    const-wide/16 v4, 0x0

    #@2
    .line 271
    .local v4, total:J
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@5
    move-result-object v6

    #@6
    if-nez v6, :cond_b

    #@8
    .line 272
    const-wide/16 v4, 0x0

    #@a
    .line 281
    .local v0, arr$:[Ljava/lang/String;
    .local v1, i$:I
    .local v3, len$:I
    :cond_a
    return-wide v4

    #@b
    .line 274
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #len$:I
    :cond_b
    invoke-static {}, Landroid/net/TrafficStats;->getMobileIfaces()[Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v3, v0

    #@10
    .restart local v3       #len$:I
    const/4 v1, 0x0

    #@11
    .restart local v1       #i$:I
    :goto_11
    if-ge v1, v3, :cond_a

    #@13
    aget-object v2, v0, v1

    #@15
    .line 275
    .local v2, iface:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v6

    #@19
    if-gtz v6, :cond_1e

    #@1b
    .line 274
    :goto_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_11

    #@1e
    .line 279
    :cond_1e
    invoke-static {v2}, Landroid/net/TrafficStats;->getTxPackets(Ljava/lang/String;)J

    #@21
    move-result-wide v6

    #@22
    add-long/2addr v4, v6

    #@23
    goto :goto_1b
.end method

.method public static getRxBytes(Ljava/lang/String;)J
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 391
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/net/TrafficStats;->nativeGetIfaceStat(Ljava/lang/String;I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static getRxPackets(Ljava/lang/String;)J
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 369
    const/4 v0, 0x1

    #@1
    invoke-static {p0, v0}, Landroid/net/TrafficStats;->nativeGetIfaceStat(Ljava/lang/String;I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method private static declared-synchronized getStatsService()Landroid/net/INetworkStatsService;
    .registers 2

    #@0
    .prologue
    .line 94
    const-class v1, Landroid/net/TrafficStats;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/net/TrafficStats;->sStatsService:Landroid/net/INetworkStatsService;

    #@5
    if-nez v0, :cond_14

    #@7
    .line 95
    const-string/jumbo v0, "netstats"

    #@a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/net/INetworkStatsService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsService;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/net/TrafficStats;->sStatsService:Landroid/net/INetworkStatsService;

    #@14
    .line 98
    :cond_14
    sget-object v0, Landroid/net/TrafficStats;->sStatsService:Landroid/net/INetworkStatsService;
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_18

    #@16
    monitor-exit v1

    #@17
    return-object v0

    #@18
    .line 94
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1

    #@1a
    throw v0
.end method

.method public static getThreadStatsTag()I
    .registers 1

    #@0
    .prologue
    .line 133
    invoke-static {}, Lcom/android/server/NetworkManagementSocketTagger;->getThreadSocketStatsTag()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getTotalRxBytes()J
    .registers 2

    #@0
    .prologue
    .line 431
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Landroid/net/TrafficStats;->nativeGetTotalStat(I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static getTotalRxPackets()J
    .registers 2

    #@0
    .prologue
    .line 411
    const/4 v0, 0x1

    #@1
    invoke-static {v0}, Landroid/net/TrafficStats;->nativeGetTotalStat(I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static getTotalTxBytes()J
    .registers 2

    #@0
    .prologue
    .line 421
    const/4 v0, 0x2

    #@1
    invoke-static {v0}, Landroid/net/TrafficStats;->nativeGetTotalStat(I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static getTotalTxPackets()J
    .registers 2

    #@0
    .prologue
    .line 401
    const/4 v0, 0x3

    #@1
    invoke-static {v0}, Landroid/net/TrafficStats;->nativeGetTotalStat(I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static getTxBytes(Ljava/lang/String;)J
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 380
    const/4 v0, 0x2

    #@1
    invoke-static {p0, v0}, Landroid/net/TrafficStats;->nativeGetIfaceStat(Ljava/lang/String;I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static getTxPackets(Ljava/lang/String;)J
    .registers 3
    .parameter "iface"

    #@0
    .prologue
    .line 358
    const/4 v0, 0x3

    #@1
    invoke-static {p0, v0}, Landroid/net/TrafficStats;->nativeGetIfaceStat(Ljava/lang/String;I)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static native getUidRxBytes(I)J
.end method

.method public static native getUidRxPackets(I)J
.end method

.method public static native getUidTcpRxBytes(I)J
.end method

.method public static native getUidTcpRxSegments(I)J
.end method

.method public static native getUidTcpTxBytes(I)J
.end method

.method public static native getUidTcpTxSegments(I)J
.end method

.method public static native getUidTxBytes(I)J
.end method

.method public static native getUidTxPackets(I)J
.end method

.method public static native getUidUdpRxBytes(I)J
.end method

.method public static native getUidUdpRxPackets(I)J
.end method

.method public static native getUidUdpTxBytes(I)J
.end method

.method public static native getUidUdpTxPackets(I)J
.end method

.method public static incrementOperationCount(I)V
    .registers 2
    .parameter "operationCount"

    #@0
    .prologue
    .line 229
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    #@3
    move-result v0

    #@4
    .line 230
    .local v0, tag:I
    invoke-static {v0, p0}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    #@7
    .line 231
    return-void
.end method

.method public static incrementOperationCount(II)V
    .registers 5
    .parameter "tag"
    .parameter "operationCount"

    #@0
    .prologue
    .line 241
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v1

    #@4
    .line 243
    .local v1, uid:I
    :try_start_4
    invoke-static {}, Landroid/net/TrafficStats;->getStatsService()Landroid/net/INetworkStatsService;

    #@7
    move-result-object v2

    #@8
    invoke-interface {v2, v1, p0, p1}, Landroid/net/INetworkStatsService;->incrementOperationCount(III)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_b} :catch_c

    #@b
    .line 247
    return-void

    #@c
    .line 244
    :catch_c
    move-exception v0

    #@d
    .line 245
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@f
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@12
    throw v2
.end method

.method private static native nativeGetIfaceStat(Ljava/lang/String;I)J
.end method

.method private static native nativeGetTotalStat(I)J
.end method

.method public static setThreadStatsTag(I)V
    .registers 1
    .parameter "tag"

    #@0
    .prologue
    .line 124
    invoke-static {p0}, Lcom/android/server/NetworkManagementSocketTagger;->setThreadSocketStatsTag(I)V

    #@3
    .line 125
    return-void
.end method

.method public static setThreadStatsUid(I)V
    .registers 1
    .parameter "uid"

    #@0
    .prologue
    .line 154
    invoke-static {p0}, Lcom/android/server/NetworkManagementSocketTagger;->setThreadSocketStatsUid(I)V

    #@3
    .line 155
    return-void
.end method

.method public static startDataProfiling(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 189
    sget-object v1, Landroid/net/TrafficStats;->sProfilingLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 190
    :try_start_3
    sget-object v0, Landroid/net/TrafficStats;->sActiveProfilingStart:Landroid/net/NetworkStats;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v2, "already profiling data"

    #@b
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 196
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 195
    :cond_12
    :try_start_12
    invoke-static {p0}, Landroid/net/TrafficStats;->getDataLayerSnapshotForUid(Landroid/content/Context;)Landroid/net/NetworkStats;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Landroid/net/TrafficStats;->sActiveProfilingStart:Landroid/net/NetworkStats;

    #@18
    .line 196
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_f

    #@19
    .line 197
    return-void
.end method

.method public static stopDataProfiling(Landroid/content/Context;)Landroid/net/NetworkStats;
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 207
    sget-object v3, Landroid/net/TrafficStats;->sProfilingLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 208
    :try_start_3
    sget-object v2, Landroid/net/TrafficStats;->sActiveProfilingStart:Landroid/net/NetworkStats;

    #@5
    if-nez v2, :cond_13

    #@7
    .line 209
    new-instance v2, Ljava/lang/IllegalStateException;

    #@9
    const-string/jumbo v4, "not profiling data"

    #@c
    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 218
    :catchall_10
    move-exception v2

    #@11
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v2

    #@13
    .line 213
    :cond_13
    :try_start_13
    invoke-static {p0}, Landroid/net/TrafficStats;->getDataLayerSnapshotForUid(Landroid/content/Context;)Landroid/net/NetworkStats;

    #@16
    move-result-object v1

    #@17
    .line 214
    .local v1, profilingStop:Landroid/net/NetworkStats;
    sget-object v2, Landroid/net/TrafficStats;->sActiveProfilingStart:Landroid/net/NetworkStats;

    #@19
    const/4 v4, 0x0

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-static {v1, v2, v4, v5}, Landroid/net/NetworkStats;->subtract(Landroid/net/NetworkStats;Landroid/net/NetworkStats;Landroid/net/NetworkStats$NonMonotonicObserver;Ljava/lang/Object;)Landroid/net/NetworkStats;

    #@1e
    move-result-object v0

    #@1f
    .line 216
    .local v0, profilingDelta:Landroid/net/NetworkStats;
    const/4 v2, 0x0

    #@20
    sput-object v2, Landroid/net/TrafficStats;->sActiveProfilingStart:Landroid/net/NetworkStats;

    #@22
    .line 217
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_13 .. :try_end_23} :catchall_10

    #@23
    return-object v0
.end method

.method public static tagSocket(Ljava/net/Socket;)V
    .registers 2
    .parameter "socket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    .line 172
    invoke-static {}, Ldalvik/system/SocketTagger;->get()Ldalvik/system/SocketTagger;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Ldalvik/system/SocketTagger;->tag(Ljava/net/Socket;)V

    #@7
    .line 173
    return-void
.end method

.method public static untagSocket(Ljava/net/Socket;)V
    .registers 2
    .parameter "socket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    invoke-static {}, Ldalvik/system/SocketTagger;->get()Ldalvik/system/SocketTagger;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Ldalvik/system/SocketTagger;->untag(Ljava/net/Socket;)V

    #@7
    .line 180
    return-void
.end method
