.class public Landroid/net/EthernetDataTracker;
.super Ljava/lang/Object;
.source "EthernetDataTracker.java"

# interfaces
.implements Landroid/net/NetworkStateTracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/EthernetDataTracker$InterfaceObserver;
    }
.end annotation


# static fields
.field private static final NETWORKTYPE:Ljava/lang/String; = "ETHERNET"

.field private static final TAG:Ljava/lang/String; = "Ethernet"

.field private static mIface:Ljava/lang/String;

.field private static mLinkUp:Z

.field private static sIfaceMatch:Ljava/lang/String;

.field private static sInstance:Landroid/net/EthernetDataTracker;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCsHandler:Landroid/os/Handler;

.field private mDefaultGatewayAddr:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mHwAddr:Ljava/lang/String;

.field private mInterfaceObserver:Landroid/net/EthernetDataTracker$InterfaceObserver;

.field private mLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mLinkProperties:Landroid/net/LinkProperties;

.field private mNMService:Landroid/os/INetworkManagementService;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 62
    const-string v0, ""

    #@2
    sput-object v0, Landroid/net/EthernetDataTracker;->sIfaceMatch:Ljava/lang/String;

    #@4
    .line 63
    const-string v0, ""

    #@6
    sput-object v0, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6
    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@9
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@b
    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@d
    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@10
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@12
    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@14
    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@17
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mDefaultGatewayAddr:Ljava/util/concurrent/atomic/AtomicInteger;

    #@19
    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@1b
    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@1e
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@20
    .line 112
    new-instance v0, Landroid/net/NetworkInfo;

    #@22
    const/16 v1, 0x9

    #@24
    const-string v2, "ETHERNET"

    #@26
    const-string v3, ""

    #@28
    invoke-direct {v0, v1, v4, v2, v3}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@2b
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2d
    .line 113
    new-instance v0, Landroid/net/LinkProperties;

    #@2f
    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    #@32
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@34
    .line 114
    new-instance v0, Landroid/net/LinkCapabilities;

    #@36
    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    #@39
    iput-object v0, p0, Landroid/net/EthernetDataTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@3b
    .line 115
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 41
    sget-object v0, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 41
    sget-boolean v0, Landroid/net/EthernetDataTracker;->mLinkUp:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 41
    sput-boolean p0, Landroid/net/EthernetDataTracker;->mLinkUp:Z

    #@2
    return p0
.end method

.method static synthetic access$200(Landroid/net/EthernetDataTracker;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/EthernetDataTracker;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/net/EthernetDataTracker;->interfaceAdded(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/net/EthernetDataTracker;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/net/EthernetDataTracker;->interfaceRemoved(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/net/EthernetDataTracker;)Landroid/net/LinkProperties;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Landroid/net/EthernetDataTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Landroid/net/EthernetDataTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Landroid/net/EthernetDataTracker;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mHwAddr:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/net/EthernetDataTracker;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mCsHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public static declared-synchronized getInstance()Landroid/net/EthernetDataTracker;
    .registers 2

    #@0
    .prologue
    .line 193
    const-class v1, Landroid/net/EthernetDataTracker;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Landroid/net/EthernetDataTracker;->sInstance:Landroid/net/EthernetDataTracker;

    #@5
    if-nez v0, :cond_e

    #@7
    new-instance v0, Landroid/net/EthernetDataTracker;

    #@9
    invoke-direct {v0}, Landroid/net/EthernetDataTracker;-><init>()V

    #@c
    sput-object v0, Landroid/net/EthernetDataTracker;->sInstance:Landroid/net/EthernetDataTracker;

    #@e
    .line 194
    :cond_e
    sget-object v0, Landroid/net/EthernetDataTracker;->sInstance:Landroid/net/EthernetDataTracker;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 193
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1

    #@14
    throw v0
.end method

.method private interfaceAdded(Ljava/lang/String;)V
    .registers 7
    .parameter "iface"

    #@0
    .prologue
    .line 118
    sget-object v2, Landroid/net/EthernetDataTracker;->sIfaceMatch:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 139
    :goto_8
    return-void

    #@9
    .line 121
    :cond_9
    const-string v2, "Ethernet"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Adding "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 123
    monitor-enter p0

    #@22
    .line 124
    :try_start_22
    sget-object v2, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@24
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_2f

    #@2a
    .line 125
    monitor-exit p0

    #@2b
    goto :goto_8

    #@2c
    .line 127
    :catchall_2c
    move-exception v2

    #@2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_22 .. :try_end_2e} :catchall_2c

    #@2e
    throw v2

    #@2f
    .line 126
    :cond_2f
    :try_start_2f
    sput-object p1, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@31
    .line 127
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_2c

    #@32
    .line 131
    :try_start_32
    iget-object v2, p0, Landroid/net/EthernetDataTracker;->mNMService:Landroid/os/INetworkManagementService;

    #@34
    invoke-interface {v2, p1}, Landroid/os/INetworkManagementService;->setInterfaceUp(Ljava/lang/String;)V
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_37} :catch_4a

    #@37
    .line 136
    :goto_37
    iget-object v2, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@39
    const/4 v3, 0x1

    #@3a
    invoke-virtual {v2, v3}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@3d
    .line 137
    iget-object v2, p0, Landroid/net/EthernetDataTracker;->mCsHandler:Landroid/os/Handler;

    #@3f
    const/4 v3, 0x3

    #@40
    iget-object v4, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@42
    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@45
    move-result-object v1

    #@46
    .line 138
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@49
    goto :goto_8

    #@4a
    .line 132
    .end local v1           #msg:Landroid/os/Message;
    :catch_4a
    move-exception v0

    #@4b
    .line 133
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "Ethernet"

    #@4d
    new-instance v3, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v4, "Error upping interface "

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    const-string v4, ": "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_37
.end method

.method private interfaceRemoved(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 165
    sget-object v0, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 171
    :goto_8
    return-void

    #@9
    .line 168
    :cond_9
    const-string v0, "Ethernet"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Removing "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 169
    invoke-virtual {p0}, Landroid/net/EthernetDataTracker;->disconnect()V

    #@24
    .line 170
    const-string v0, ""

    #@26
    sput-object v0, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@28
    goto :goto_8
.end method

.method private runDhcp()V
    .registers 3

    #@0
    .prologue
    .line 174
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Landroid/net/EthernetDataTracker$1;

    #@4
    invoke-direct {v1, p0}, Landroid/net/EthernetDataTracker$1;-><init>(Landroid/net/EthernetDataTracker;)V

    #@7
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    .line 189
    .local v0, dhcpThread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@d
    .line 190
    return-void
.end method


# virtual methods
.method public Clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 198
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    #@5
    throw v0
.end method

.method public captivePortalCheckComplete()V
    .registers 1

    #@0
    .prologue
    .line 283
    return-void
.end method

.method public defaultRouteSet(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 400
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 401
    return-void
.end method

.method public disconnect()V
    .registers 9

    #@0
    .prologue
    .line 143
    sget-object v4, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@2
    invoke-static {v4}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@5
    .line 145
    iget-object v4, p0, Landroid/net/EthernetDataTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@7
    invoke-virtual {v4}, Landroid/net/LinkProperties;->clear()V

    #@a
    .line 146
    iget-object v4, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@c
    const/4 v5, 0x0

    #@d
    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@10
    .line 147
    iget-object v4, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@12
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@14
    const/4 v6, 0x0

    #@15
    iget-object v7, p0, Landroid/net/EthernetDataTracker;->mHwAddr:Ljava/lang/String;

    #@17
    invoke-virtual {v4, v5, v6, v7}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 149
    iget-object v4, p0, Landroid/net/EthernetDataTracker;->mCsHandler:Landroid/os/Handler;

    #@1c
    const/4 v5, 0x3

    #@1d
    iget-object v6, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@1f
    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@22
    move-result-object v2

    #@23
    .line 150
    .local v2, msg:Landroid/os/Message;
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@26
    .line 152
    iget-object v4, p0, Landroid/net/EthernetDataTracker;->mCsHandler:Landroid/os/Handler;

    #@28
    const/4 v5, 0x1

    #@29
    iget-object v6, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2b
    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2e
    move-result-object v2

    #@2f
    .line 153
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@32
    .line 155
    const-string/jumbo v4, "network_management"

    #@35
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@38
    move-result-object v0

    #@39
    .line 156
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@3c
    move-result-object v3

    #@3d
    .line 158
    .local v3, service:Landroid/os/INetworkManagementService;
    :try_start_3d
    sget-object v4, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@3f
    invoke-interface {v3, v4}, Landroid/os/INetworkManagementService;->clearInterfaceAddresses(Ljava/lang/String;)V
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_42} :catch_43

    #@42
    .line 162
    :goto_42
    return-void

    #@43
    .line 159
    :catch_43
    move-exception v1

    #@44
    .line 160
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "Ethernet"

    #@46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v6, "Failed to clear addresses or disable ipv6"

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_42
.end method

.method public getDefaultGatewayAddr()I
    .registers 2

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mDefaultGatewayAddr:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLinkCapabilities()Landroid/net/LinkCapabilities;
    .registers 3

    #@0
    .prologue
    .line 379
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    iget-object v1, p0, Landroid/net/EthernetDataTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkCapabilities;-><init>(Landroid/net/LinkCapabilities;)V

    #@7
    return-object v0
.end method

.method public declared-synchronized getLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 369
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/net/LinkProperties;

    #@3
    iget-object v1, p0, Landroid/net/EthernetDataTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@5
    invoke-direct {v0, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    monitor-exit p0

    #@9
    return-object v0

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public declared-synchronized getNetworkInfo()Landroid/net/NetworkInfo;
    .registers 2

    #@0
    .prologue
    .line 362
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getTcpBufferSizesPropName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 408
    const-string/jumbo v0, "net.tcp.buffersize.wifi"

    #@3
    return-object v0
.end method

.method public declared-synchronized isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 297
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public isDefaultRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 393
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPrivateDnsRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 348
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isTeardownRequested()Z
    .registers 2

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public privateDnsRouteSet(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 355
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 356
    return-void
.end method

.method public reconnect()Z
    .registers 3

    #@0
    .prologue
    .line 273
    sget-boolean v0, Landroid/net/EthernetDataTracker;->mLinkUp:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 274
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@a
    .line 275
    invoke-direct {p0}, Landroid/net/EthernetDataTracker;->runDhcp()V

    #@d
    .line 277
    :cond_d
    sget-boolean v0, Landroid/net/EthernetDataTracker;->mLinkUp:Z

    #@f
    return v0
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 2
    .parameter "msger"

    #@0
    .prologue
    .line 417
    return-void
.end method

.method public setDependencyMet(Z)V
    .registers 2
    .parameter "met"

    #@0
    .prologue
    .line 413
    return-void
.end method

.method public setPolicyDataEnable(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 341
    const-string v0, "Ethernet"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "ignoring setPolicyDataEnable("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ")"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 342
    return-void
.end method

.method public setRadio(Z)Z
    .registers 3
    .parameter "turnOn"

    #@0
    .prologue
    .line 290
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setTeardownRequested(Z)V
    .registers 3
    .parameter "isRequested"

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 203
    return-void
.end method

.method public setUserDataEnable(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 336
    const-string v0, "Ethernet"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "ignoring setUserDataEnable("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ")"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 337
    return-void
.end method

.method public startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 14
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 213
    iput-object p1, p0, Landroid/net/EthernetDataTracker;->mContext:Landroid/content/Context;

    #@2
    .line 214
    iput-object p2, p0, Landroid/net/EthernetDataTracker;->mCsHandler:Landroid/os/Handler;

    #@4
    .line 217
    const-string/jumbo v8, "network_management"

    #@7
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@a
    move-result-object v1

    #@b
    .line 218
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@e
    move-result-object v8

    #@f
    iput-object v8, p0, Landroid/net/EthernetDataTracker;->mNMService:Landroid/os/INetworkManagementService;

    #@11
    .line 220
    new-instance v8, Landroid/net/EthernetDataTracker$InterfaceObserver;

    #@13
    invoke-direct {v8, p0}, Landroid/net/EthernetDataTracker$InterfaceObserver;-><init>(Landroid/net/EthernetDataTracker;)V

    #@16
    iput-object v8, p0, Landroid/net/EthernetDataTracker;->mInterfaceObserver:Landroid/net/EthernetDataTracker$InterfaceObserver;

    #@18
    .line 224
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1b
    move-result-object v8

    #@1c
    const v9, 0x1040033

    #@1f
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v8

    #@23
    sput-object v8, Landroid/net/EthernetDataTracker;->sIfaceMatch:Ljava/lang/String;

    #@25
    .line 227
    :try_start_25
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mNMService:Landroid/os/INetworkManagementService;

    #@27
    invoke-interface {v8}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    .line 228
    .local v6, ifaces:[Ljava/lang/String;
    move-object v0, v6

    #@2c
    .local v0, arr$:[Ljava/lang/String;
    array-length v7, v0

    #@2d
    .local v7, len$:I
    const/4 v4, 0x0

    #@2e
    .local v4, i$:I
    :goto_2e
    if-ge v4, v7, :cond_6f

    #@30
    aget-object v5, v0, v4

    #@32
    .line 229
    .local v5, iface:Ljava/lang/String;
    sget-object v8, Landroid/net/EthernetDataTracker;->sIfaceMatch:Ljava/lang/String;

    #@34
    invoke-virtual {v5, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@37
    move-result v8

    #@38
    if-eqz v8, :cond_77

    #@3a
    .line 230
    sput-object v5, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@3c
    .line 231
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mNMService:Landroid/os/INetworkManagementService;

    #@3e
    invoke-interface {v8, v5}, Landroid/os/INetworkManagementService;->setInterfaceUp(Ljava/lang/String;)V

    #@41
    .line 232
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mNMService:Landroid/os/INetworkManagementService;

    #@43
    invoke-interface {v8, v5}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@46
    move-result-object v2

    #@47
    .line 233
    .local v2, config:Landroid/net/InterfaceConfiguration;
    const-string/jumbo v8, "up"

    #@4a
    invoke-virtual {v2, v8}, Landroid/net/InterfaceConfiguration;->hasFlag(Ljava/lang/String;)Z

    #@4d
    move-result v8

    #@4e
    sput-boolean v8, Landroid/net/EthernetDataTracker;->mLinkUp:Z

    #@50
    .line 234
    if-eqz v2, :cond_67

    #@52
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mHwAddr:Ljava/lang/String;

    #@54
    if-nez v8, :cond_67

    #@56
    .line 235
    invoke-virtual {v2}, Landroid/net/InterfaceConfiguration;->getHardwareAddress()Ljava/lang/String;

    #@59
    move-result-object v8

    #@5a
    iput-object v8, p0, Landroid/net/EthernetDataTracker;->mHwAddr:Ljava/lang/String;

    #@5c
    .line 236
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mHwAddr:Ljava/lang/String;

    #@5e
    if-eqz v8, :cond_67

    #@60
    .line 237
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@62
    iget-object v9, p0, Landroid/net/EthernetDataTracker;->mHwAddr:Ljava/lang/String;

    #@64
    invoke-virtual {v8, v9}, Landroid/net/NetworkInfo;->setExtraInfo(Ljava/lang/String;)V

    #@67
    .line 242
    :cond_67
    sget-object v8, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@69
    invoke-static {v8}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@6c
    .line 244
    invoke-virtual {p0}, Landroid/net/EthernetDataTracker;->reconnect()Z
    :try_end_6f
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_6f} :catch_7a

    #@6f
    .line 253
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #config:Landroid/net/InterfaceConfiguration;
    .end local v4           #i$:I
    .end local v5           #iface:Ljava/lang/String;
    .end local v6           #ifaces:[Ljava/lang/String;
    .end local v7           #len$:I
    :cond_6f
    :goto_6f
    :try_start_6f
    iget-object v8, p0, Landroid/net/EthernetDataTracker;->mNMService:Landroid/os/INetworkManagementService;

    #@71
    iget-object v9, p0, Landroid/net/EthernetDataTracker;->mInterfaceObserver:Landroid/net/EthernetDataTracker$InterfaceObserver;

    #@73
    invoke-interface {v8, v9}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_76
    .catch Landroid/os/RemoteException; {:try_start_6f .. :try_end_76} :catch_94

    #@76
    .line 257
    :goto_76
    return-void

    #@77
    .line 228
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v4       #i$:I
    .restart local v5       #iface:Ljava/lang/String;
    .restart local v6       #ifaces:[Ljava/lang/String;
    .restart local v7       #len$:I
    :cond_77
    add-int/lit8 v4, v4, 0x1

    #@79
    goto :goto_2e

    #@7a
    .line 248
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #iface:Ljava/lang/String;
    .end local v6           #ifaces:[Ljava/lang/String;
    .end local v7           #len$:I
    :catch_7a
    move-exception v3

    #@7b
    .line 249
    .local v3, e:Landroid/os/RemoteException;
    const-string v8, "Ethernet"

    #@7d
    new-instance v9, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v10, "Could not get list of interfaces "

    #@84
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v9

    #@88
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v9

    #@8c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v9

    #@90
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    goto :goto_6f

    #@94
    .line 254
    .end local v3           #e:Landroid/os/RemoteException;
    :catch_94
    move-exception v3

    #@95
    .line 255
    .restart local v3       #e:Landroid/os/RemoteException;
    const-string v8, "Ethernet"

    #@97
    new-instance v9, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v10, "Could not register InterfaceObserver "

    #@9e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v9

    #@a2
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v9

    #@a6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v9

    #@aa
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto :goto_76
.end method

.method public startUsingNetworkFeature(Ljava/lang/String;II)I
    .registers 5
    .parameter "feature"
    .parameter "callingPid"
    .parameter "callingUid"

    #@0
    .prologue
    .line 314
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public stopUsingNetworkFeature(Ljava/lang/String;II)I
    .registers 5
    .parameter "feature"
    .parameter "callingPid"
    .parameter "callingUid"

    #@0
    .prologue
    .line 331
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public teardown()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 264
    iget-object v0, p0, Landroid/net/EthernetDataTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@6
    .line 265
    sget-object v0, Landroid/net/EthernetDataTracker;->mIface:Ljava/lang/String;

    #@8
    invoke-static {v0}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@b
    .line 266
    return v1
.end method
