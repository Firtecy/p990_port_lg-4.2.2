.class abstract Landroid/net/Uri$AbstractHierarchicalUri;
.super Landroid/net/Uri;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AbstractHierarchicalUri"
.end annotation


# instance fields
.field private volatile host:Ljava/lang/String;

.field private volatile port:I

.field private userInfo:Landroid/net/Uri$Part;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1029
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/net/Uri;-><init>(Landroid/net/Uri$1;)V

    #@4
    .line 1068
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->host:Ljava/lang/String;

    #@a
    .line 1094
    const/4 v0, -0x2

    #@b
    iput v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->port:I

    #@d
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1029
    invoke-direct {p0}, Landroid/net/Uri$AbstractHierarchicalUri;-><init>()V

    #@3
    return-void
.end method

.method private getUserInfoPart()Landroid/net/Uri$Part;
    .registers 2

    #@0
    .prologue
    .line 1046
    iget-object v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->userInfo:Landroid/net/Uri$Part;

    #@2
    if-nez v0, :cond_f

    #@4
    invoke-direct {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->parseUserInfo()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->userInfo:Landroid/net/Uri$Part;

    #@e
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->userInfo:Landroid/net/Uri$Part;

    #@11
    goto :goto_e
.end method

.method private parseHost()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1078
    invoke-virtual {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->getEncodedAuthority()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1079
    .local v0, authority:Ljava/lang/String;
    if-nez v0, :cond_8

    #@6
    .line 1080
    const/4 v4, 0x0

    #@7
    .line 1091
    :goto_7
    return-object v4

    #@8
    .line 1084
    :cond_8
    const/16 v4, 0x40

    #@a
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v3

    #@e
    .line 1085
    .local v3, userInfoSeparator:I
    const/16 v4, 0x3a

    #@10
    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->indexOf(II)I

    #@13
    move-result v2

    #@14
    .line 1087
    .local v2, portSeparator:I
    const/4 v4, -0x1

    #@15
    if-ne v2, v4, :cond_22

    #@17
    add-int/lit8 v4, v3, 0x1

    #@19
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 1091
    .local v1, encodedHost:Ljava/lang/String;
    :goto_1d
    invoke-static {v1}, Landroid/net/Uri$AbstractHierarchicalUri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    goto :goto_7

    #@22
    .line 1087
    .end local v1           #encodedHost:Ljava/lang/String;
    :cond_22
    add-int/lit8 v4, v3, 0x1

    #@24
    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    goto :goto_1d
.end method

.method private parsePort()I
    .registers 9

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 1103
    invoke-virtual {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->getEncodedAuthority()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 1104
    .local v0, authority:Ljava/lang/String;
    if-nez v0, :cond_8

    #@7
    .line 1122
    :cond_7
    :goto_7
    return v5

    #@8
    .line 1110
    :cond_8
    const/16 v6, 0x40

    #@a
    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v4

    #@e
    .line 1111
    .local v4, userInfoSeparator:I
    const/16 v6, 0x3a

    #@10
    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->indexOf(II)I

    #@13
    move-result v2

    #@14
    .line 1113
    .local v2, portSeparator:I
    if-eq v2, v5, :cond_7

    #@16
    .line 1117
    add-int/lit8 v6, v2, 0x1

    #@18
    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1b
    move-result-object v6

    #@1c
    invoke-static {v6}, Landroid/net/Uri$AbstractHierarchicalUri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 1119
    .local v3, portString:Ljava/lang/String;
    :try_start_20
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_23
    .catch Ljava/lang/NumberFormatException; {:try_start_20 .. :try_end_23} :catch_25

    #@23
    move-result v5

    #@24
    goto :goto_7

    #@25
    .line 1120
    :catch_25
    move-exception v1

    #@26
    .line 1121
    .local v1, e:Ljava/lang/NumberFormatException;
    invoke-static {}, Landroid/net/Uri;->access$600()Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    const-string v7, "Error parsing port string."

    #@2c
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_7
.end method

.method private parseUserInfo()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1055
    invoke-virtual {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->getEncodedAuthority()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 1056
    .local v0, authority:Ljava/lang/String;
    if-nez v0, :cond_8

    #@7
    .line 1061
    :cond_7
    :goto_7
    return-object v2

    #@8
    .line 1060
    :cond_8
    const/16 v3, 0x40

    #@a
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v1

    #@e
    .line 1061
    .local v1, end:I
    const/4 v3, -0x1

    #@f
    if-eq v1, v3, :cond_7

    #@11
    const/4 v2, 0x0

    #@12
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    goto :goto_7
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 1029
    check-cast p1, Landroid/net/Uri;

    #@2
    .end local p1
    invoke-super {p0, p1}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getEncodedUserInfo()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1051
    invoke-direct {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->getUserInfoPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getEncoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1072
    iget-object v1, p0, Landroid/net/Uri$AbstractHierarchicalUri;->host:Ljava/lang/String;

    #@2
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    if-eq v1, v2, :cond_e

    #@8
    const/4 v0, 0x1

    #@9
    .line 1073
    .local v0, cached:Z
    :goto_9
    if-eqz v0, :cond_10

    #@b
    iget-object v1, p0, Landroid/net/Uri$AbstractHierarchicalUri;->host:Ljava/lang/String;

    #@d
    :goto_d
    return-object v1

    #@e
    .line 1072
    .end local v0           #cached:Z
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_9

    #@10
    .line 1073
    .restart local v0       #cached:Z
    :cond_10
    invoke-direct {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->parseHost()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Landroid/net/Uri$AbstractHierarchicalUri;->host:Ljava/lang/String;

    #@16
    goto :goto_d
.end method

.method public getLastPathSegment()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1035
    invoke-virtual {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->getPathSegments()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    .line 1036
    .local v0, segments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v1

    #@8
    .line 1037
    .local v1, size:I
    if-nez v1, :cond_c

    #@a
    .line 1038
    const/4 v2, 0x0

    #@b
    .line 1040
    :goto_b
    return-object v2

    #@c
    :cond_c
    add-int/lit8 v2, v1, -0x1

    #@e
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Ljava/lang/String;

    #@14
    goto :goto_b
.end method

.method public getPort()I
    .registers 3

    #@0
    .prologue
    .line 1097
    iget v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->port:I

    #@2
    const/4 v1, -0x2

    #@3
    if-ne v0, v1, :cond_c

    #@5
    invoke-direct {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->parsePort()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->port:I

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    iget v0, p0, Landroid/net/Uri$AbstractHierarchicalUri;->port:I

    #@e
    goto :goto_b
.end method

.method public getUserInfo()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1065
    invoke-direct {p0}, Landroid/net/Uri$AbstractHierarchicalUri;->getUserInfoPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getDecoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
