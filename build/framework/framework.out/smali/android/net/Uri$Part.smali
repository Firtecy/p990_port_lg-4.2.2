.class Landroid/net/Uri$Part;
.super Landroid/net/Uri$AbstractPart;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Part"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/Uri$Part$EmptyPart;
    }
.end annotation


# static fields
.field static final EMPTY:Landroid/net/Uri$Part;

.field static final NULL:Landroid/net/Uri$Part;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 1993
    new-instance v0, Landroid/net/Uri$Part$EmptyPart;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/net/Uri$Part$EmptyPart;-><init>(Ljava/lang/String;)V

    #@6
    sput-object v0, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@8
    .line 1996
    new-instance v0, Landroid/net/Uri$Part$EmptyPart;

    #@a
    const-string v1, ""

    #@c
    invoke-direct {v0, v1}, Landroid/net/Uri$Part$EmptyPart;-><init>(Ljava/lang/String;)V

    #@f
    sput-object v0, Landroid/net/Uri$Part;->EMPTY:Landroid/net/Uri$Part;

    #@11
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "encoded"
    .parameter "decoded"

    #@0
    .prologue
    .line 1999
    invoke-direct {p0, p1, p2}, Landroid/net/Uri$AbstractPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 2000
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1990
    invoke-direct {p0, p1, p2}, Landroid/net/Uri$Part;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Part;
    .registers 3
    .parameter "encoded"
    .parameter "decoded"

    #@0
    .prologue
    .line 2062
    if-nez p0, :cond_5

    #@2
    .line 2063
    sget-object v0, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@4
    .line 2076
    :goto_4
    return-object v0

    #@5
    .line 2065
    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_e

    #@b
    .line 2066
    sget-object v0, Landroid/net/Uri$Part;->EMPTY:Landroid/net/Uri$Part;

    #@d
    goto :goto_4

    #@e
    .line 2069
    :cond_e
    if-nez p1, :cond_13

    #@10
    .line 2070
    sget-object v0, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@12
    goto :goto_4

    #@13
    .line 2072
    :cond_13
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_1c

    #@19
    .line 2073
    sget-object v0, Landroid/net/Uri$Part;->EMPTY:Landroid/net/Uri$Part;

    #@1b
    goto :goto_4

    #@1c
    .line 2076
    :cond_1c
    new-instance v0, Landroid/net/Uri$Part;

    #@1e
    invoke-direct {v0, p0, p1}, Landroid/net/Uri$Part;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    goto :goto_4
.end method

.method static fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;
    .registers 2
    .parameter "decoded"

    #@0
    .prologue
    .line 2049
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p0}, Landroid/net/Uri$Part;->from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Part;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method static fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;
    .registers 2
    .parameter "encoded"

    #@0
    .prologue
    .line 2040
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, v0}, Landroid/net/Uri$Part;->from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Part;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method static nonNull(Landroid/net/Uri$Part;)Landroid/net/Uri$Part;
    .registers 1
    .parameter "part"

    #@0
    .prologue
    .line 2031
    if-nez p0, :cond_4

    #@2
    sget-object p0, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@4
    .end local p0
    :cond_4
    return-object p0
.end method

.method static readFrom(Landroid/os/Parcel;)Landroid/net/Uri$Part;
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    .line 2013
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 2014
    .local v0, representation:I
    packed-switch v0, :pswitch_data_40

    #@7
    .line 2022
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Unknown representation: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 2016
    :pswitch_20
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/net/Uri$Part;->from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Part;

    #@2b
    move-result-object v1

    #@2c
    .line 2020
    :goto_2c
    return-object v1

    #@2d
    .line 2018
    :pswitch_2d
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v1}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@34
    move-result-object v1

    #@35
    goto :goto_2c

    #@36
    .line 2020
    :pswitch_36
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v1}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3d
    move-result-object v1

    #@3e
    goto :goto_2c

    #@3f
    .line 2014
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_20
        :pswitch_2d
        :pswitch_36
    .end packed-switch
.end method


# virtual methods
.method getEncoded()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2008
    iget-object v1, p0, Landroid/net/Uri$Part;->encoded:Ljava/lang/String;

    #@2
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    if-eq v1, v2, :cond_e

    #@8
    const/4 v0, 0x1

    #@9
    .line 2009
    .local v0, hasEncoded:Z
    :goto_9
    if-eqz v0, :cond_10

    #@b
    iget-object v1, p0, Landroid/net/Uri$Part;->encoded:Ljava/lang/String;

    #@d
    :goto_d
    return-object v1

    #@e
    .line 2008
    .end local v0           #hasEncoded:Z
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_9

    #@10
    .line 2009
    .restart local v0       #hasEncoded:Z
    :cond_10
    iget-object v1, p0, Landroid/net/Uri$Part;->decoded:Ljava/lang/String;

    #@12
    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Landroid/net/Uri$Part;->encoded:Ljava/lang/String;

    #@18
    goto :goto_d
.end method

.method isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 2003
    const/4 v0, 0x0

    #@1
    return v0
.end method
