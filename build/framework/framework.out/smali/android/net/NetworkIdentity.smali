.class public Landroid/net/NetworkIdentity;
.super Ljava/lang/Object;
.source "NetworkIdentity.java"


# static fields
.field public static final COMBINE_SUBTYPE_ENABLED:Z = true

.field public static final SUBTYPE_COMBINED:I = -0x1


# instance fields
.field final mNetworkId:Ljava/lang/String;

.field final mRoaming:Z

.field final mSubType:I

.field final mSubscriberId:Ljava/lang/String;

.field final mType:I


# direct methods
.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter "type"
    .parameter "subType"
    .parameter "subscriberId"
    .parameter "networkId"
    .parameter "roaming"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    iput p1, p0, Landroid/net/NetworkIdentity;->mType:I

    #@5
    .line 55
    const/4 v0, -0x1

    #@6
    iput v0, p0, Landroid/net/NetworkIdentity;->mSubType:I

    #@8
    .line 56
    iput-object p3, p0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@a
    .line 57
    iput-object p4, p0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@c
    .line 58
    iput-boolean p5, p0, Landroid/net/NetworkIdentity;->mRoaming:Z

    #@e
    .line 59
    return-void
.end method

.method public static buildNetworkIdentity(Landroid/content/Context;Landroid/net/NetworkState;)Landroid/net/NetworkIdentity;
    .registers 11
    .parameter "context"
    .parameter "state"

    #@0
    .prologue
    .line 140
    iget-object v0, p1, Landroid/net/NetworkState;->networkInfo:Landroid/net/NetworkInfo;

    #@2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@5
    move-result v1

    #@6
    .line 141
    .local v1, type:I
    iget-object v0, p1, Landroid/net/NetworkState;->networkInfo:Landroid/net/NetworkInfo;

    #@8
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    #@b
    move-result v2

    #@c
    .line 146
    .local v2, subType:I
    const/4 v3, 0x0

    #@d
    .line 147
    .local v3, subscriberId:Ljava/lang/String;
    const/4 v4, 0x0

    #@e
    .line 148
    .local v4, networkId:Ljava/lang/String;
    const/4 v5, 0x0

    #@f
    .line 150
    .local v5, roaming:Z
    invoke-static {v1}, Landroid/net/ConnectivityManager;->isNetworkTypeMobile(I)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_33

    #@15
    .line 151
    const-string/jumbo v0, "phone"

    #@18
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v7

    #@1c
    check-cast v7, Landroid/telephony/TelephonyManager;

    #@1e
    .line 153
    .local v7, telephony:Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@21
    move-result v5

    #@22
    .line 154
    iget-object v0, p1, Landroid/net/NetworkState;->subscriberId:Ljava/lang/String;

    #@24
    if-eqz v0, :cond_2e

    #@26
    .line 155
    iget-object v3, p1, Landroid/net/NetworkState;->subscriberId:Ljava/lang/String;

    #@28
    .line 171
    .end local v7           #telephony:Landroid/telephony/TelephonyManager;
    :cond_28
    :goto_28
    new-instance v0, Landroid/net/NetworkIdentity;

    #@2a
    invoke-direct/range {v0 .. v5}, Landroid/net/NetworkIdentity;-><init>(IILjava/lang/String;Ljava/lang/String;Z)V

    #@2d
    return-object v0

    #@2e
    .line 157
    .restart local v7       #telephony:Landroid/telephony/TelephonyManager;
    :cond_2e
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    goto :goto_28

    #@33
    .line 160
    .end local v7           #telephony:Landroid/telephony/TelephonyManager;
    :cond_33
    const/4 v0, 0x1

    #@34
    if-ne v1, v0, :cond_28

    #@36
    .line 161
    iget-object v0, p1, Landroid/net/NetworkState;->networkId:Ljava/lang/String;

    #@38
    if-eqz v0, :cond_3d

    #@3a
    .line 162
    iget-object v4, p1, Landroid/net/NetworkState;->networkId:Ljava/lang/String;

    #@3c
    goto :goto_28

    #@3d
    .line 164
    :cond_3d
    const-string/jumbo v0, "wifi"

    #@40
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v8

    #@44
    check-cast v8, Landroid/net/wifi/WifiManager;

    #@46
    .line 166
    .local v8, wifi:Landroid/net/wifi/WifiManager;
    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@49
    move-result-object v6

    #@4a
    .line 167
    .local v6, info:Landroid/net/wifi/WifiInfo;
    if-eqz v6, :cond_51

    #@4c
    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    :goto_50
    goto :goto_28

    #@51
    :cond_51
    const/4 v4, 0x0

    #@52
    goto :goto_50
.end method

.method public static scrubSubscriberId(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "subscriberId"

    #@0
    .prologue
    .line 125
    const-string v0, "eng"

    #@2
    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_b

    #@a
    .line 131
    .end local p0
    :goto_a
    return-object p0

    #@b
    .line 127
    .restart local p0
    :cond_b
    if-eqz p0, :cond_2f

    #@d
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const/4 v1, 0x0

    #@13
    const/4 v2, 0x6

    #@14
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@17
    move-result v3

    #@18
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    #@1b
    move-result v2

    #@1c
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    const-string v1, "..."

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object p0

    #@2e
    goto :goto_a

    #@2f
    .line 131
    :cond_2f
    const-string/jumbo p0, "null"

    #@32
    goto :goto_a
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 68
    instance-of v2, p1, Landroid/net/NetworkIdentity;

    #@3
    if-eqz v2, :cond_2f

    #@5
    move-object v0, p1

    #@6
    .line 69
    check-cast v0, Landroid/net/NetworkIdentity;

    #@8
    .line 70
    .local v0, ident:Landroid/net/NetworkIdentity;
    iget v2, p0, Landroid/net/NetworkIdentity;->mType:I

    #@a
    iget v3, v0, Landroid/net/NetworkIdentity;->mType:I

    #@c
    if-ne v2, v3, :cond_2f

    #@e
    iget v2, p0, Landroid/net/NetworkIdentity;->mSubType:I

    #@10
    iget v3, v0, Landroid/net/NetworkIdentity;->mSubType:I

    #@12
    if-ne v2, v3, :cond_2f

    #@14
    iget-boolean v2, p0, Landroid/net/NetworkIdentity;->mRoaming:Z

    #@16
    iget-boolean v3, v0, Landroid/net/NetworkIdentity;->mRoaming:Z

    #@18
    if-ne v2, v3, :cond_2f

    #@1a
    iget-object v2, p0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@1c
    iget-object v3, v0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@1e
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_2f

    #@24
    iget-object v2, p0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@26
    iget-object v3, v0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@28
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_2f

    #@2e
    const/4 v1, 0x1

    #@2f
    .line 74
    .end local v0           #ident:Landroid/net/NetworkIdentity;
    :cond_2f
    return v1
.end method

.method public getNetworkId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 118
    iget-boolean v0, p0, Landroid/net/NetworkIdentity;->mRoaming:Z

    #@2
    return v0
.end method

.method public getSubType()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Landroid/net/NetworkIdentity;->mSubType:I

    #@2
    return v0
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 102
    iget v0, p0, Landroid/net/NetworkIdentity;->mType:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 63
    const/4 v0, 0x5

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    iget v2, p0, Landroid/net/NetworkIdentity;->mType:I

    #@6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v2

    #@a
    aput-object v2, v0, v1

    #@c
    const/4 v1, 0x1

    #@d
    iget v2, p0, Landroid/net/NetworkIdentity;->mSubType:I

    #@f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v2

    #@13
    aput-object v2, v0, v1

    #@15
    const/4 v1, 0x2

    #@16
    iget-object v2, p0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@18
    aput-object v2, v0, v1

    #@1a
    const/4 v1, 0x3

    #@1b
    iget-object v2, p0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x4

    #@20
    iget-boolean v2, p0, Landroid/net/NetworkIdentity;->mRoaming:Z

    #@22
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@25
    move-result-object v2

    #@26
    aput-object v2, v0, v1

    #@28
    invoke-static {v0}, Lcom/android/internal/util/Objects;->hashCode([Ljava/lang/Object;)I

    #@2b
    move-result v0

    #@2c
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "["

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 80
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "type="

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget v2, p0, Landroid/net/NetworkIdentity;->mType:I

    #@10
    invoke-static {v2}, Landroid/net/ConnectivityManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 81
    const-string v1, ", subType="

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 83
    const-string v1, "COMBINED"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 89
    iget-object v1, p0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@23
    if-eqz v1, :cond_34

    #@25
    .line 90
    const-string v1, ", subscriberId="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget-object v2, p0, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@2d
    invoke-static {v2}, Landroid/net/NetworkIdentity;->scrubSubscriberId(Ljava/lang/String;)Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 92
    :cond_34
    iget-object v1, p0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@36
    if-eqz v1, :cond_43

    #@38
    .line 93
    const-string v1, ", networkId="

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    iget-object v2, p0, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 95
    :cond_43
    iget-boolean v1, p0, Landroid/net/NetworkIdentity;->mRoaming:Z

    #@45
    if-eqz v1, :cond_4c

    #@47
    .line 96
    const-string v1, ", ROAMING"

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    .line 98
    :cond_4c
    const-string v1, "]"

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    return-object v1
.end method
