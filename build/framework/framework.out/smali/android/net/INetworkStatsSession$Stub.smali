.class public abstract Landroid/net/INetworkStatsSession$Stub;
.super Landroid/os/Binder;
.source "INetworkStatsSession.java"

# interfaces
.implements Landroid/net/INetworkStatsSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/INetworkStatsSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/INetworkStatsSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.INetworkStatsSession"

.field static final TRANSACTION_close:I = 0x5

.field static final TRANSACTION_getHistoryForNetwork:I = 0x2

.field static final TRANSACTION_getHistoryForUid:I = 0x4

.field static final TRANSACTION_getSummaryForAllUid:I = 0x3

.field static final TRANSACTION_getSummaryForNetwork:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.net.INetworkStatsSession"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/INetworkStatsSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.net.INetworkStatsSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/INetworkStatsSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/net/INetworkStatsSession;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/net/INetworkStatsSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/INetworkStatsSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 20
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    sparse-switch p1, :sswitch_data_134

    #@3
    .line 158
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 43
    :sswitch_8
    const-string v1, "android.net.INetworkStatsSession"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 44
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 48
    :sswitch_11
    const-string v1, "android.net.INetworkStatsSession"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_48

    #@1e
    .line 51
    sget-object v1, Landroid/net/NetworkTemplate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    move-object/from16 v0, p2

    #@22
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/net/NetworkTemplate;

    #@28
    .line 57
    .local v2, _arg0:Landroid/net/NetworkTemplate;
    :goto_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@2b
    move-result-wide v3

    #@2c
    .line 59
    .local v3, _arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v5

    #@30
    .local v5, _arg2:J
    move-object v1, p0

    #@31
    .line 60
    invoke-virtual/range {v1 .. v6}, Landroid/net/INetworkStatsSession$Stub;->getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    #@34
    move-result-object v14

    #@35
    .line 61
    .local v14, _result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    .line 62
    if-eqz v14, :cond_4a

    #@3a
    .line 63
    const/4 v1, 0x1

    #@3b
    move-object/from16 v0, p3

    #@3d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 64
    const/4 v1, 0x1

    #@41
    move-object/from16 v0, p3

    #@43
    invoke-virtual {v14, v0, v1}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@46
    .line 69
    :goto_46
    const/4 v1, 0x1

    #@47
    goto :goto_7

    #@48
    .line 54
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:J
    .end local v5           #_arg2:J
    .end local v14           #_result:Landroid/net/NetworkStats;
    :cond_48
    const/4 v2, 0x0

    #@49
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    goto :goto_28

    #@4a
    .line 67
    .restart local v3       #_arg1:J
    .restart local v5       #_arg2:J
    .restart local v14       #_result:Landroid/net/NetworkStats;
    :cond_4a
    const/4 v1, 0x0

    #@4b
    move-object/from16 v0, p3

    #@4d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    goto :goto_46

    #@51
    .line 73
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:J
    .end local v5           #_arg2:J
    .end local v14           #_result:Landroid/net/NetworkStats;
    :sswitch_51
    const-string v1, "android.net.INetworkStatsSession"

    #@53
    move-object/from16 v0, p2

    #@55
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58
    .line 75
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v1

    #@5c
    if-eqz v1, :cond_83

    #@5e
    .line 76
    sget-object v1, Landroid/net/NetworkTemplate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@60
    move-object/from16 v0, p2

    #@62
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@65
    move-result-object v2

    #@66
    check-cast v2, Landroid/net/NetworkTemplate;

    #@68
    .line 82
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    :goto_68
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v3

    #@6c
    .line 83
    .local v3, _arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/net/INetworkStatsSession$Stub;->getHistoryForNetwork(Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;

    #@6f
    move-result-object v14

    #@70
    .line 84
    .local v14, _result:Landroid/net/NetworkStatsHistory;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@73
    .line 85
    if-eqz v14, :cond_85

    #@75
    .line 86
    const/4 v1, 0x1

    #@76
    move-object/from16 v0, p3

    #@78
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    .line 87
    const/4 v1, 0x1

    #@7c
    move-object/from16 v0, p3

    #@7e
    invoke-virtual {v14, v0, v1}, Landroid/net/NetworkStatsHistory;->writeToParcel(Landroid/os/Parcel;I)V

    #@81
    .line 92
    :goto_81
    const/4 v1, 0x1

    #@82
    goto :goto_7

    #@83
    .line 79
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:I
    .end local v14           #_result:Landroid/net/NetworkStatsHistory;
    :cond_83
    const/4 v2, 0x0

    #@84
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    goto :goto_68

    #@85
    .line 90
    .restart local v3       #_arg1:I
    .restart local v14       #_result:Landroid/net/NetworkStatsHistory;
    :cond_85
    const/4 v1, 0x0

    #@86
    move-object/from16 v0, p3

    #@88
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@8b
    goto :goto_81

    #@8c
    .line 96
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:I
    .end local v14           #_result:Landroid/net/NetworkStatsHistory;
    :sswitch_8c
    const-string v1, "android.net.INetworkStatsSession"

    #@8e
    move-object/from16 v0, p2

    #@90
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@93
    .line 98
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@96
    move-result v1

    #@97
    if-eqz v1, :cond_cb

    #@99
    .line 99
    sget-object v1, Landroid/net/NetworkTemplate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@9b
    move-object/from16 v0, p2

    #@9d
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a0
    move-result-object v2

    #@a1
    check-cast v2, Landroid/net/NetworkTemplate;

    #@a3
    .line 105
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    :goto_a3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@a6
    move-result-wide v3

    #@a7
    .line 107
    .local v3, _arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@aa
    move-result-wide v5

    #@ab
    .line 109
    .restart local v5       #_arg2:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v1

    #@af
    if-eqz v1, :cond_cd

    #@b1
    const/4 v7, 0x1

    #@b2
    .local v7, _arg3:Z
    :goto_b2
    move-object v1, p0

    #@b3
    .line 110
    invoke-virtual/range {v1 .. v7}, Landroid/net/INetworkStatsSession$Stub;->getSummaryForAllUid(Landroid/net/NetworkTemplate;JJZ)Landroid/net/NetworkStats;

    #@b6
    move-result-object v14

    #@b7
    .line 111
    .local v14, _result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ba
    .line 112
    if-eqz v14, :cond_cf

    #@bc
    .line 113
    const/4 v1, 0x1

    #@bd
    move-object/from16 v0, p3

    #@bf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@c2
    .line 114
    const/4 v1, 0x1

    #@c3
    move-object/from16 v0, p3

    #@c5
    invoke-virtual {v14, v0, v1}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@c8
    .line 119
    :goto_c8
    const/4 v1, 0x1

    #@c9
    goto/16 :goto_7

    #@cb
    .line 102
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:J
    .end local v5           #_arg2:J
    .end local v7           #_arg3:Z
    .end local v14           #_result:Landroid/net/NetworkStats;
    :cond_cb
    const/4 v2, 0x0

    #@cc
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    goto :goto_a3

    #@cd
    .line 109
    .restart local v3       #_arg1:J
    .restart local v5       #_arg2:J
    :cond_cd
    const/4 v7, 0x0

    #@ce
    goto :goto_b2

    #@cf
    .line 117
    .restart local v7       #_arg3:Z
    .restart local v14       #_result:Landroid/net/NetworkStats;
    :cond_cf
    const/4 v1, 0x0

    #@d0
    move-object/from16 v0, p3

    #@d2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@d5
    goto :goto_c8

    #@d6
    .line 123
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:J
    .end local v5           #_arg2:J
    .end local v7           #_arg3:Z
    .end local v14           #_result:Landroid/net/NetworkStats;
    :sswitch_d6
    const-string v1, "android.net.INetworkStatsSession"

    #@d8
    move-object/from16 v0, p2

    #@da
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dd
    .line 125
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e0
    move-result v1

    #@e1
    if-eqz v1, :cond_11a

    #@e3
    .line 126
    sget-object v1, Landroid/net/NetworkTemplate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e5
    move-object/from16 v0, p2

    #@e7
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ea
    move-result-object v2

    #@eb
    check-cast v2, Landroid/net/NetworkTemplate;

    #@ed
    .line 132
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    :goto_ed
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f0
    move-result v3

    #@f1
    .line 134
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f4
    move-result v5

    #@f5
    .line 136
    .local v5, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f8
    move-result v7

    #@f9
    .line 138
    .local v7, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@fc
    move-result v13

    #@fd
    .local v13, _arg4:I
    move-object v8, p0

    #@fe
    move-object v9, v2

    #@ff
    move v10, v3

    #@100
    move v11, v5

    #@101
    move v12, v7

    #@102
    .line 139
    invoke-virtual/range {v8 .. v13}, Landroid/net/INetworkStatsSession$Stub;->getHistoryForUid(Landroid/net/NetworkTemplate;IIII)Landroid/net/NetworkStatsHistory;

    #@105
    move-result-object v14

    #@106
    .line 140
    .local v14, _result:Landroid/net/NetworkStatsHistory;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@109
    .line 141
    if-eqz v14, :cond_11c

    #@10b
    .line 142
    const/4 v1, 0x1

    #@10c
    move-object/from16 v0, p3

    #@10e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@111
    .line 143
    const/4 v1, 0x1

    #@112
    move-object/from16 v0, p3

    #@114
    invoke-virtual {v14, v0, v1}, Landroid/net/NetworkStatsHistory;->writeToParcel(Landroid/os/Parcel;I)V

    #@117
    .line 148
    :goto_117
    const/4 v1, 0x1

    #@118
    goto/16 :goto_7

    #@11a
    .line 129
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:I
    .end local v5           #_arg2:I
    .end local v7           #_arg3:I
    .end local v13           #_arg4:I
    .end local v14           #_result:Landroid/net/NetworkStatsHistory;
    :cond_11a
    const/4 v2, 0x0

    #@11b
    .restart local v2       #_arg0:Landroid/net/NetworkTemplate;
    goto :goto_ed

    #@11c
    .line 146
    .restart local v3       #_arg1:I
    .restart local v5       #_arg2:I
    .restart local v7       #_arg3:I
    .restart local v13       #_arg4:I
    .restart local v14       #_result:Landroid/net/NetworkStatsHistory;
    :cond_11c
    const/4 v1, 0x0

    #@11d
    move-object/from16 v0, p3

    #@11f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@122
    goto :goto_117

    #@123
    .line 152
    .end local v2           #_arg0:Landroid/net/NetworkTemplate;
    .end local v3           #_arg1:I
    .end local v5           #_arg2:I
    .end local v7           #_arg3:I
    .end local v13           #_arg4:I
    .end local v14           #_result:Landroid/net/NetworkStatsHistory;
    :sswitch_123
    const-string v1, "android.net.INetworkStatsSession"

    #@125
    move-object/from16 v0, p2

    #@127
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12a
    .line 153
    invoke-virtual {p0}, Landroid/net/INetworkStatsSession$Stub;->close()V

    #@12d
    .line 154
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@130
    .line 155
    const/4 v1, 0x1

    #@131
    goto/16 :goto_7

    #@133
    .line 39
    nop

    #@134
    :sswitch_data_134
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_51
        0x3 -> :sswitch_8c
        0x4 -> :sswitch_d6
        0x5 -> :sswitch_123
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
