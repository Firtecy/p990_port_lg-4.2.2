.class Landroid/net/CaptivePortalTracker$DefaultState;
.super Lcom/android/internal/util/State;
.source "CaptivePortalTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/CaptivePortalTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/CaptivePortalTracker;


# direct methods
.method private constructor <init>(Landroid/net/CaptivePortalTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 132
    iput-object p1, p0, Landroid/net/CaptivePortalTracker$DefaultState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 132
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker$DefaultState;-><init>(Landroid/net/CaptivePortalTracker;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 1

    #@0
    .prologue
    .line 136
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 141
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_28

    #@5
    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Ignoring "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v1}, Landroid/net/CaptivePortalTracker;->access$500(Ljava/lang/String;)V

    #@1b
    .line 155
    :goto_1b
    :pswitch_1b
    const/4 v1, 0x1

    #@1c
    return v1

    #@1d
    .line 143
    :pswitch_1d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f
    check-cast v0, Landroid/net/NetworkInfo;

    #@21
    .line 146
    .local v0, info:Landroid/net/NetworkInfo;
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$DefaultState;->this$0:Landroid/net/CaptivePortalTracker;

    #@23
    invoke-static {v1, v0}, Landroid/net/CaptivePortalTracker;->access$400(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)V

    #@26
    goto :goto_1b

    #@27
    .line 141
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1b
        :pswitch_1b
    .end packed-switch
.end method
