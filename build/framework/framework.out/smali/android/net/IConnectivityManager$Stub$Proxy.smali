.class Landroid/net/IConnectivityManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IConnectivityManager.java"

# interfaces
.implements Landroid/net/IConnectivityManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/IConnectivityManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 658
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 659
    iput-object p1, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 660
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 663
    iget-object v0, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public captivePortalCheckComplete(Landroid/net/NetworkInfo;)V
    .registers 7
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1693
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1694
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1696
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1697
    if-eqz p1, :cond_29

    #@f
    .line 1698
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1699
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/net/NetworkInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1704
    :goto_17
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x37

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1705
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 1708
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1709
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1711
    return-void

    #@29
    .line 1702
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 1708
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1709
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public clearTetheringBlockNotification()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1636
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1637
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1639
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1640
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x34

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1641
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 1644
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 1645
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1647
    return-void

    #@1f
    .line 1644
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1645
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public establishVpn(Lcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;
    .registers 8
    .parameter "config"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1477
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1478
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1481
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1482
    if-eqz p1, :cond_37

    #@f
    .line 1483
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1484
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Lcom/android/internal/net/VpnConfig;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1489
    :goto_17
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x2c

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1490
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 1491
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_44

    #@28
    .line 1492
    sget-object v3, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d
    move-result-object v2

    #@2e
    check-cast v2, Landroid/os/ParcelFileDescriptor;
    :try_end_30
    .catchall {:try_start_8 .. :try_end_30} :catchall_3c

    #@30
    .line 1499
    .local v2, _result:Landroid/os/ParcelFileDescriptor;
    :goto_30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 1500
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 1502
    return-object v2

    #@37
    .line 1487
    .end local v2           #_result:Landroid/os/ParcelFileDescriptor;
    :cond_37
    const/4 v3, 0x0

    #@38
    :try_start_38
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_3c

    #@3b
    goto :goto_17

    #@3c
    .line 1499
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 1500
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3

    #@44
    .line 1495
    :cond_44
    const/4 v2, 0x0

    #@45
    .restart local v2       #_result:Landroid/os/ParcelFileDescriptor;
    goto :goto_30
.end method

.method public getActiveLinkProperties()Landroid/net/LinkProperties;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 806
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 807
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 810
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 811
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x8

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 812
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 813
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 814
    sget-object v3, Landroid/net/LinkProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/net/LinkProperties;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 821
    .local v2, _result:Landroid/net/LinkProperties;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 822
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 824
    return-object v2

    #@2d
    .line 817
    .end local v2           #_result:Landroid/net/LinkProperties;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Landroid/net/LinkProperties;
    goto :goto_26

    #@2f
    .line 821
    .end local v2           #_result:Landroid/net/LinkProperties;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 822
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public getActiveNetworkInfo()Landroid/net/NetworkInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 703
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 704
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 707
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 708
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 709
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 710
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 711
    sget-object v3, Landroid/net/NetworkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Landroid/net/NetworkInfo;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 718
    .local v2, _result:Landroid/net/NetworkInfo;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 719
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 721
    return-object v2

    #@2c
    .line 714
    .end local v2           #_result:Landroid/net/NetworkInfo;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Landroid/net/NetworkInfo;
    goto :goto_25

    #@2e
    .line 718
    .end local v2           #_result:Landroid/net/NetworkInfo;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 719
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getActiveNetworkInfoForUid(I)Landroid/net/NetworkInfo;
    .registers 8
    .parameter "uid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 725
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 726
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 729
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 730
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 731
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x4

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 732
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 733
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2f

    #@20
    .line 734
    sget-object v3, Landroid/net/NetworkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/net/NetworkInfo;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_31

    #@28
    .line 741
    .local v2, _result:Landroid/net/NetworkInfo;
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 742
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 744
    return-object v2

    #@2f
    .line 737
    .end local v2           #_result:Landroid/net/NetworkInfo;
    :cond_2f
    const/4 v2, 0x0

    #@30
    .restart local v2       #_result:Landroid/net/NetworkInfo;
    goto :goto_28

    #@31
    .line 741
    .end local v2           #_result:Landroid/net/NetworkInfo;
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 742
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getActiveNetworkQuotaInfo()Landroid/net/NetworkQuotaInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 868
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 869
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 872
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 873
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xb

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 874
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 875
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 876
    sget-object v3, Landroid/net/NetworkQuotaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/net/NetworkQuotaInfo;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 883
    .local v2, _result:Landroid/net/NetworkQuotaInfo;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 884
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 886
    return-object v2

    #@2d
    .line 879
    .end local v2           #_result:Landroid/net/NetworkQuotaInfo;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Landroid/net/NetworkQuotaInfo;
    goto :goto_26

    #@2f
    .line 883
    .end local v2           #_result:Landroid/net/NetworkQuotaInfo;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 884
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public getAllNetworkInfo()[Landroid/net/NetworkInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 771
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 772
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 775
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 776
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 777
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 778
    sget-object v3, Landroid/net/NetworkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, [Landroid/net/NetworkInfo;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_26

    #@1f
    .line 781
    .local v2, _result:[Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 782
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 784
    return-object v2

    #@26
    .line 781
    .end local v2           #_result:[Landroid/net/NetworkInfo;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 782
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getAllNetworkState()[Landroid/net/NetworkState;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 851
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 852
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 855
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 856
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xa

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 857
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 858
    sget-object v3, Landroid/net/NetworkState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, [Landroid/net/NetworkState;
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_27

    #@20
    .line 861
    .local v2, _result:[Landroid/net/NetworkState;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 862
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 864
    return-object v2

    #@27
    .line 861
    .end local v2           #_result:[Landroid/net/NetworkState;
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 862
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getGlobalProxy()Landroid/net/ProxyProperties;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1331
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1332
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1335
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1336
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x25

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1337
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1338
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 1339
    sget-object v3, Landroid/net/ProxyProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/net/ProxyProperties;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 1346
    .local v2, _result:Landroid/net/ProxyProperties;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1347
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1349
    return-object v2

    #@2d
    .line 1342
    .end local v2           #_result:Landroid/net/ProxyProperties;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Landroid/net/ProxyProperties;
    goto :goto_26

    #@2f
    .line 1346
    .end local v2           #_result:Landroid/net/ProxyProperties;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 1347
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 667
    const-string v0, "android.net.IConnectivityManager"

    #@2
    return-object v0
.end method

.method public getLastTetherError(Ljava/lang/String;)I
    .registers 8
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1124
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1125
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1128
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1129
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 1130
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x19

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1131
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 1132
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 1135
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1136
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1138
    return v2

    #@26
    .line 1135
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1136
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getLegacyVpnInfo()Lcom/android/internal/net/LegacyVpnInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1527
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1528
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1531
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1532
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x2e

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1533
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1534
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 1535
    sget-object v3, Lcom/android/internal/net/LegacyVpnInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Lcom/android/internal/net/LegacyVpnInfo;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 1542
    .local v2, _result:Lcom/android/internal/net/LegacyVpnInfo;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1543
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1545
    return-object v2

    #@2d
    .line 1538
    .end local v2           #_result:Lcom/android/internal/net/LegacyVpnInfo;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Lcom/android/internal/net/LegacyVpnInfo;
    goto :goto_26

    #@2f
    .line 1542
    .end local v2           #_result:Lcom/android/internal/net/LegacyVpnInfo;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 1543
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public getLinkProperties(I)Landroid/net/LinkProperties;
    .registers 8
    .parameter "networkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 828
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 829
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 832
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 833
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 834
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x9

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 835
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 836
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_30

    #@21
    .line 837
    sget-object v3, Landroid/net/LinkProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@23
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Landroid/net/LinkProperties;
    :try_end_29
    .catchall {:try_start_8 .. :try_end_29} :catchall_32

    #@29
    .line 844
    .local v2, _result:Landroid/net/LinkProperties;
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 845
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 847
    return-object v2

    #@30
    .line 840
    .end local v2           #_result:Landroid/net/LinkProperties;
    :cond_30
    const/4 v2, 0x0

    #@31
    .restart local v2       #_result:Landroid/net/LinkProperties;
    goto :goto_29

    #@32
    .line 844
    .end local v2           #_result:Landroid/net/LinkProperties;
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 845
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public getMobileDataEnabled()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1021
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1022
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1025
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1026
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x13

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 1027
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 1028
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 1031
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1032
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1034
    return v2

    #@27
    .line 1031
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1032
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getMobileDataEnabledByUser()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1054
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1055
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1058
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1059
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x15

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 1060
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 1061
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 1064
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1065
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1067
    return v2

    #@27
    .line 1064
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1065
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getNetworkInfo(I)Landroid/net/NetworkInfo;
    .registers 8
    .parameter "networkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 748
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 749
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 752
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 753
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 754
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x5

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 755
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 756
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2f

    #@20
    .line 757
    sget-object v3, Landroid/net/NetworkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/net/NetworkInfo;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_31

    #@28
    .line 764
    .local v2, _result:Landroid/net/NetworkInfo;
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 765
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 767
    return-object v2

    #@2f
    .line 760
    .end local v2           #_result:Landroid/net/NetworkInfo;
    :cond_2f
    const/4 v2, 0x0

    #@30
    .restart local v2       #_result:Landroid/net/NetworkInfo;
    goto :goto_28

    #@31
    .line 764
    .end local v2           #_result:Landroid/net/NetworkInfo;
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 765
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getNetworkPreference()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 686
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 687
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 690
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 691
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x2

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 692
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 693
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 696
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 697
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 699
    return v2

    #@22
    .line 696
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 697
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public getProxy()Landroid/net/ProxyProperties;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1374
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1375
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1378
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1379
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x27

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1380
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1381
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 1382
    sget-object v3, Landroid/net/ProxyProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/net/ProxyProperties;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 1389
    .local v2, _result:Landroid/net/ProxyProperties;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1390
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1392
    return-object v2

    #@2d
    .line 1385
    .end local v2           #_result:Landroid/net/ProxyProperties;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Landroid/net/ProxyProperties;
    goto :goto_26

    #@2f
    .line 1389
    .end local v2           #_result:Landroid/net/ProxyProperties;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 1390
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public getTetherableBluetoothRegexs()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1265
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1266
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1269
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1270
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x21

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1271
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1272
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1275
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1276
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1278
    return-object v2

    #@23
    .line 1275
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1276
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getTetherableIfaces()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1159
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1160
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1163
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1164
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x1b

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1165
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1166
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1169
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1172
    return-object v2

    #@23
    .line 1169
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getTetherableUsbRegexs()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1232
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1235
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1236
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x1f

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1237
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1238
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1241
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1242
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1244
    return-object v2

    #@23
    .line 1241
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1242
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getTetherableWifiRegexs()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1248
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1249
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1252
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1253
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x20

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1254
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1255
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1258
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1261
    return-object v2

    #@23
    .line 1258
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getTetheredIfacePairs()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1197
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1198
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1201
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1202
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x1d

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1203
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1204
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1207
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1208
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1210
    return-object v2

    #@23
    .line 1207
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1208
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getTetheredIfaces()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1176
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1177
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1180
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1181
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x1c

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1182
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1183
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1186
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1187
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1189
    return-object v2

    #@23
    .line 1186
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1187
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public getTetheringErroredIfaces()[Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1214
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1215
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1218
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1219
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x1e

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1220
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 1221
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result-object v2

    #@1c
    .line 1224
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1227
    return-object v2

    #@23
    .line 1224
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public handleConnectMobile()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1582
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1583
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1585
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1586
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x31

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1587
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 1590
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 1591
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1593
    return-void

    #@1f
    .line 1590
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1591
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public handleDisconnectMobile()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1568
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1569
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1571
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1572
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x30

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1573
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 1576
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 1577
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1579
    return-void

    #@1f
    .line 1576
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1577
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public isActiveNetworkMetered()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 890
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 891
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 894
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 895
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xc

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 896
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 897
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 900
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 901
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 903
    return v2

    #@27
    .line 900
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 901
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public isNetworkSupported(I)Z
    .registers 8
    .parameter "networkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 788
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 789
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 792
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 793
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 794
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x7

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 795
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 796
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 799
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 800
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 802
    return v2

    #@29
    .line 799
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 800
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public isTetheringSupported()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1142
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1143
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1146
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1147
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x1a

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 1148
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 1149
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 1152
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1155
    return v2

    #@27
    .line 1152
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public mobileDataPdpReset()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1677
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1678
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1680
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1681
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x36

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 1682
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 1685
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 1686
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1688
    return-void

    #@1f
    .line 1685
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1686
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "oldPackage"
    .parameter "newPackage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1436
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1437
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1440
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1441
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 1442
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 1443
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x2a

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 1444
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 1445
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 1448
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1449
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1451
    return v2

    #@2d
    .line 1448
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 1449
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public prepareVpnEx(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 11
    .parameter "oldPackage"
    .parameter "newPackage"
    .parameter "mvpOn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1456
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 1457
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 1460
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.net.IConnectivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 1461
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 1462
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 1463
    if-eqz p3, :cond_33

    #@17
    move v4, v2

    #@18
    :goto_18
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 1464
    iget-object v4, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v5, 0x2b

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 1465
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@26
    .line 1466
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_29
    .catchall {:try_start_a .. :try_end_29} :catchall_37

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_35

    #@2c
    .line 1469
    .local v2, _result:Z
    :goto_2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 1470
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1472
    return v2

    #@33
    .end local v2           #_result:Z
    :cond_33
    move v4, v3

    #@34
    .line 1463
    goto :goto_18

    #@35
    :cond_35
    move v2, v3

    #@36
    .line 1466
    goto :goto_2c

    #@37
    .line 1469
    :catchall_37
    move-exception v3

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 1470
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v3
.end method

.method public protectVpn(Landroid/os/ParcelFileDescriptor;)Z
    .registers 9
    .parameter "socket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1412
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 1413
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 1416
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.net.IConnectivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 1417
    if-eqz p1, :cond_31

    #@11
    .line 1418
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 1419
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 1424
    :goto_19
    iget-object v4, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v5, 0x29

    #@1d
    const/4 v6, 0x0

    #@1e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 1425
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 1426
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_a .. :try_end_27} :catchall_36

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_3e

    #@2a
    .line 1429
    .local v2, _result:Z
    :goto_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 1430
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 1432
    return v2

    #@31
    .line 1422
    .end local v2           #_result:Z
    :cond_31
    const/4 v4, 0x0

    #@32
    :try_start_32
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_36

    #@35
    goto :goto_19

    #@36
    .line 1429
    :catchall_36
    move-exception v3

    #@37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 1430
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    throw v3

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    .line 1426
    goto :goto_2a
.end method

.method public reportInetCondition(II)V
    .registers 8
    .parameter "networkType"
    .parameter "percentage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1315
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1316
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1318
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1319
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 1320
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1321
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x24

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 1322
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 1325
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 1326
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 1328
    return-void

    #@25
    .line 1325
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1326
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public requestNetworkTransitionWakelock(Ljava/lang/String;)V
    .registers 7
    .parameter "forWhom"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1300
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1301
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1303
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1304
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 1305
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x23

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1306
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 1309
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1310
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 1312
    return-void

    #@22
    .line 1309
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1310
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public requestRemRouteToHostAddress(I[B)Z
    .registers 9
    .parameter "networkType"
    .parameter "hostAddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1599
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1600
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1603
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1604
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1605
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@14
    .line 1606
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x32

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 1607
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 1608
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 1611
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1612
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1614
    return v2

    #@2d
    .line 1611
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 1612
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public requestRouteToHost(II)Z
    .registers 9
    .parameter "networkType"
    .parameter "hostAddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 983
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 984
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 987
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 988
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 989
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 990
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x11

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 991
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 992
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 995
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 996
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 998
    return v2

    #@2d
    .line 995
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 996
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public requestRouteToHostAddress(I[B)Z
    .registers 9
    .parameter "networkType"
    .parameter "hostAddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1002
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1003
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1006
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1007
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1008
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@14
    .line 1009
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x12

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 1010
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 1011
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 1014
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1015
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 1017
    return v2

    #@2d
    .line 1014
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 1015
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 7
    .parameter "msger"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1653
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1654
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1656
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1657
    if-eqz p1, :cond_29

    #@f
    .line 1658
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1659
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/os/Messenger;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1664
    :goto_17
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x35

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1665
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 1668
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1669
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1671
    return-void

    #@29
    .line 1662
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 1668
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1669
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public setDataDependency(IZ)V
    .registers 8
    .parameter "networkType"
    .parameter "met"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1396
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1397
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1399
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1400
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1401
    if-eqz p2, :cond_14

    #@13
    const/4 v2, 0x1

    #@14
    :cond_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 1402
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x28

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1403
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_29

    #@22
    .line 1406
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1407
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1409
    return-void

    #@29
    .line 1406
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 1407
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public setGlobalProxy(Landroid/net/ProxyProperties;)V
    .registers 7
    .parameter "p"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1353
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1354
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1356
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1357
    if-eqz p1, :cond_29

    #@f
    .line 1358
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1359
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/net/ProxyProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1364
    :goto_17
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x26

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1365
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 1368
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1369
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1371
    return-void

    #@29
    .line 1362
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 1368
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1369
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public setMobileDataEnabled(Z)V
    .registers 7
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1038
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1039
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1041
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1042
    if-eqz p1, :cond_11

    #@10
    const/4 v2, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1043
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v3, 0x14

    #@18
    const/4 v4, 0x0

    #@19
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 1044
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_26

    #@1f
    .line 1047
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1048
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1050
    return-void

    #@26
    .line 1047
    :catchall_26
    move-exception v2

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1048
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v2
.end method

.method public setNetworkPreference(I)V
    .registers 7
    .parameter "pref"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 671
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 672
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 674
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 675
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 676
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x1

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 677
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 680
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 683
    return-void

    #@21
    .line 680
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public setPolicyDataEnable(IZ)V
    .registers 8
    .parameter "networkType"
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1072
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1073
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1075
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1076
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1077
    if-eqz p2, :cond_14

    #@13
    const/4 v2, 0x1

    #@14
    :cond_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 1078
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x16

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1079
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_29

    #@22
    .line 1082
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1083
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1085
    return-void

    #@29
    .line 1082
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 1083
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public setRadio(IZ)Z
    .registers 10
    .parameter "networkType"
    .parameter "turnOn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 925
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 926
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 929
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.net.IConnectivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 930
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 931
    if-eqz p2, :cond_30

    #@14
    move v4, v2

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 932
    iget-object v4, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/16 v5, 0xe

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 933
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 934
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_34

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_32

    #@29
    .line 937
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 938
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 940
    return v2

    #@30
    .end local v2           #_result:Z
    :cond_30
    move v4, v3

    #@31
    .line 931
    goto :goto_15

    #@32
    :cond_32
    move v2, v3

    #@33
    .line 934
    goto :goto_29

    #@34
    .line 937
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 938
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public setRadios(Z)Z
    .registers 9
    .parameter "onOff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 907
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 908
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 911
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.net.IConnectivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 912
    if-eqz p1, :cond_2d

    #@11
    move v4, v2

    #@12
    :goto_12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 913
    iget-object v4, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v5, 0xd

    #@19
    const/4 v6, 0x0

    #@1a
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 914
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@20
    .line 915
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_23
    .catchall {:try_start_a .. :try_end_23} :catchall_31

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_2f

    #@26
    .line 918
    .local v2, _result:Z
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 919
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 921
    return v2

    #@2d
    .end local v2           #_result:Z
    :cond_2d
    move v4, v3

    #@2e
    .line 912
    goto :goto_12

    #@2f
    :cond_2f
    move v2, v3

    #@30
    .line 915
    goto :goto_26

    #@31
    .line 918
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 919
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public setUsbTethering(Z)I
    .registers 8
    .parameter "enable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1282
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1283
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1286
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v4, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1287
    if-eqz p1, :cond_11

    #@10
    const/4 v3, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1288
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x22

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 1289
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 1290
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2a

    #@22
    move-result v2

    #@23
    .line 1293
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1294
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 1296
    return v2

    #@2a
    .line 1293
    .end local v2           #_result:I
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 1294
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public showTetheringBlockNotification(I)V
    .registers 7
    .parameter "icon"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1621
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1622
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1624
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1625
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 1626
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x33

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1627
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 1630
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1631
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 1633
    return-void

    #@22
    .line 1630
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1631
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public startLegacyVpn(Lcom/android/internal/net/VpnProfile;)V
    .registers 7
    .parameter "profile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1506
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1507
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1509
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1510
    if-eqz p1, :cond_29

    #@f
    .line 1511
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1512
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/android/internal/net/VpnProfile;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 1517
    :goto_17
    iget-object v2, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x2d

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1518
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 1521
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1522
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1524
    return-void

    #@29
    .line 1515
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 1521
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1522
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public startUsingNetworkFeature(ILjava/lang/String;Landroid/os/IBinder;)I
    .registers 10
    .parameter "networkType"
    .parameter "feature"
    .parameter "binder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 944
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 945
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 948
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 949
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 950
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 951
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 952
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0xf

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 953
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 954
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2c

    #@24
    move-result v2

    #@25
    .line 957
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 958
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 960
    return v2

    #@2c
    .line 957
    .end local v2           #_result:I
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 958
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public stopUsingNetworkFeature(ILjava/lang/String;)I
    .registers 9
    .parameter "networkType"
    .parameter "feature"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 964
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 965
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 968
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 969
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 970
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 971
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v4, 0x10

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 972
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 973
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_29

    #@21
    move-result v2

    #@22
    .line 976
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 977
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 979
    return v2

    #@29
    .line 976
    .end local v2           #_result:I
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 977
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public tether(Ljava/lang/String;)I
    .registers 8
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1088
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1089
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1092
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1093
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 1094
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x17

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1095
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 1096
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 1099
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1100
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1102
    return v2

    #@26
    .line 1099
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1100
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public untether(Ljava/lang/String;)I
    .registers 8
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1106
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1107
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1110
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IConnectivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1111
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 1112
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x18

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1113
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 1114
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result v2

    #@1f
    .line 1117
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1118
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1120
    return v2

    #@26
    .line 1117
    .end local v2           #_result:I
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1118
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public updateLockdownVpn()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1549
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1550
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1553
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.net.IConnectivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1554
    iget-object v3, p0, Landroid/net/IConnectivityManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x2f

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 1555
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 1556
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 1559
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1560
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1562
    return v2

    #@27
    .line 1559
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1560
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method
