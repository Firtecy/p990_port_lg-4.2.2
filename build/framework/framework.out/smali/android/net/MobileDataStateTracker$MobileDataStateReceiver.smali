.class Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MobileDataStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/MobileDataStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MobileDataStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/MobileDataStateTracker;


# direct methods
.method private constructor <init>(Landroid/net/MobileDataStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 201
    iput-object p1, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/MobileDataStateTracker;Landroid/net/MobileDataStateTracker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 201
    invoke-direct {p0, p1}, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;-><init>(Landroid/net/MobileDataStateTracker;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 18
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 204
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v10

    #@4
    const-string v11, "android.intent.action.ANY_DATA_STATE"

    #@6
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v10

    #@a
    if-eqz v10, :cond_202

    #@c
    .line 206
    const-string v10, "apnType"

    #@e
    move-object/from16 v0, p2

    #@10
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 212
    .local v3, apnType:Ljava/lang/String;
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@16
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$200(Landroid/net/MobileDataStateTracker;)Ljava/lang/String;

    #@19
    move-result-object v10

    #@1a
    invoke-static {v3, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1d
    move-result v10

    #@1e
    if-nez v10, :cond_21

    #@20
    .line 334
    .end local v3           #apnType:Ljava/lang/String;
    :cond_20
    :goto_20
    return-void

    #@21
    .line 216
    .restart local v3       #apnType:Ljava/lang/String;
    :cond_21
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@23
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@26
    move-result-object v10

    #@27
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->getSubtype()I

    #@2a
    move-result v6

    #@2b
    .line 217
    .local v6, oldSubtype:I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@2e
    move-result-object v10

    #@2f
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@32
    move-result v5

    #@33
    .line 218
    .local v5, newSubType:I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getNetworkTypeName()Ljava/lang/String;

    #@3a
    move-result-object v9

    #@3b
    .line 219
    .local v9, subTypeName:Ljava/lang/String;
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@3d
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@40
    move-result-object v10

    #@41
    invoke-virtual {v10, v5, v9}, Landroid/net/NetworkInfo;->setSubtype(ILjava/lang/String;)V

    #@44
    .line 220
    if-eq v5, v6, :cond_67

    #@46
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@48
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@4b
    move-result-object v10

    #@4c
    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    #@4f
    move-result v10

    #@50
    if-eqz v10, :cond_67

    #@52
    .line 221
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@54
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$400(Landroid/net/MobileDataStateTracker;)Landroid/os/Handler;

    #@57
    move-result-object v10

    #@58
    const/4 v11, 0x7

    #@59
    const/4 v12, 0x0

    #@5a
    iget-object v13, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@5c
    invoke-static {v13}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@5f
    move-result-object v13

    #@60
    invoke-virtual {v10, v11, v6, v12, v13}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@63
    move-result-object v4

    #@64
    .line 223
    .local v4, msg:Landroid/os/Message;
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@67
    .line 226
    .end local v4           #msg:Landroid/os/Message;
    :cond_67
    const-class v10, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@69
    const-string/jumbo v11, "state"

    #@6c
    move-object/from16 v0, p2

    #@6e
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@71
    move-result-object v11

    #@72
    invoke-static {v10, v11}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@75
    move-result-object v8

    #@76
    check-cast v8, Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@78
    .line 228
    .local v8, state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    const-string/jumbo v10, "reason"

    #@7b
    move-object/from16 v0, p2

    #@7d
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@80
    move-result-object v7

    #@81
    .line 229
    .local v7, reason:Ljava/lang/String;
    const-string v10, "apn"

    #@83
    move-object/from16 v0, p2

    #@85
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    .line 230
    .local v2, apnName:Ljava/lang/String;
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@8b
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@8e
    move-result-object v10

    #@8f
    const-string/jumbo v11, "networkRoaming"

    #@92
    const/4 v12, 0x0

    #@93
    move-object/from16 v0, p2

    #@95
    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@98
    move-result v11

    #@99
    invoke-virtual {v10, v11}, Landroid/net/NetworkInfo;->setRoaming(Z)V

    #@9c
    .line 236
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@9e
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@a1
    move-result-object v11

    #@a2
    const-string/jumbo v10, "networkUnvailable"

    #@a5
    const/4 v12, 0x0

    #@a6
    move-object/from16 v0, p2

    #@a8
    invoke-virtual {v0, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@ab
    move-result v10

    #@ac
    if-nez v10, :cond_12c

    #@ae
    const/4 v10, 0x1

    #@af
    :goto_af
    invoke-virtual {v11, v10}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@b2
    .line 240
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@b4
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@b7
    move-result-object v10

    #@b8
    const-string/jumbo v11, "smCause"

    #@bb
    const/4 v12, 0x0

    #@bc
    move-object/from16 v0, p2

    #@be
    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@c1
    move-result v11

    #@c2
    invoke-virtual {v10, v11}, Landroid/net/NetworkInfo;->setSmCause(I)V

    #@c5
    .line 244
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@c7
    new-instance v10, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v12, "Received state="

    #@ce
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v10

    #@d2
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v10

    #@d6
    const-string v12, ", old="

    #@d8
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v10

    #@dc
    iget-object v12, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@de
    invoke-static {v12}, Landroid/net/MobileDataStateTracker;->access$500(Landroid/net/MobileDataStateTracker;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@e1
    move-result-object v12

    #@e2
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v10

    #@e6
    const-string v12, ", reason="

    #@e8
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v12

    #@ec
    if-nez v7, :cond_12e

    #@ee
    const-string v10, "(unspecified)"

    #@f0
    :goto_f0
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v10

    #@f4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v10

    #@f8
    invoke-static {v11, v10}, Landroid/net/MobileDataStateTracker;->access$600(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@fb
    .line 247
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@fd
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$500(Landroid/net/MobileDataStateTracker;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@100
    move-result-object v10

    #@101
    if-eq v10, v8, :cond_19d

    #@103
    .line 248
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@105
    invoke-static {v10, v8}, Landroid/net/MobileDataStateTracker;->access$502(Landroid/net/MobileDataStateTracker;Lcom/android/internal/telephony/PhoneConstants$DataState;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@108
    .line 249
    sget-object v10, Landroid/net/MobileDataStateTracker$1;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    #@10a
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    #@10d
    move-result v11

    #@10e
    aget v10, v10, v11

    #@110
    packed-switch v10, :pswitch_data_310

    #@113
    goto/16 :goto_20

    #@115
    .line 251
    :pswitch_115
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@117
    invoke-virtual {v10}, Landroid/net/MobileDataStateTracker;->isTeardownRequested()Z

    #@11a
    move-result v10

    #@11b
    if-eqz v10, :cond_123

    #@11d
    .line 252
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@11f
    const/4 v11, 0x0

    #@120
    invoke-virtual {v10, v11}, Landroid/net/MobileDataStateTracker;->setTeardownRequested(Z)V

    #@123
    .line 255
    :cond_123
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@125
    sget-object v11, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@127
    invoke-static {v10, v11, v7, v2}, Landroid/net/MobileDataStateTracker;->access$700(Landroid/net/MobileDataStateTracker;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@12a
    goto/16 :goto_20

    #@12c
    .line 236
    :cond_12c
    const/4 v10, 0x0

    #@12d
    goto :goto_af

    #@12e
    :cond_12e
    move-object v10, v7

    #@12f
    .line 244
    goto :goto_f0

    #@130
    .line 264
    :pswitch_130
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@132
    sget-object v11, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@134
    invoke-static {v10, v11, v7, v2}, Landroid/net/MobileDataStateTracker;->access$700(Landroid/net/MobileDataStateTracker;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@137
    goto/16 :goto_20

    #@139
    .line 267
    :pswitch_139
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@13b
    sget-object v11, Landroid/net/NetworkInfo$DetailedState;->SUSPENDED:Landroid/net/NetworkInfo$DetailedState;

    #@13d
    invoke-static {v10, v11, v7, v2}, Landroid/net/MobileDataStateTracker;->access$700(Landroid/net/MobileDataStateTracker;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@140
    goto/16 :goto_20

    #@142
    .line 270
    :pswitch_142
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@144
    const-string/jumbo v10, "linkProperties"

    #@147
    move-object/from16 v0, p2

    #@149
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@14c
    move-result-object v10

    #@14d
    check-cast v10, Landroid/net/LinkProperties;

    #@14f
    invoke-static {v11, v10}, Landroid/net/MobileDataStateTracker;->access$802(Landroid/net/MobileDataStateTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;

    #@152
    .line 272
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@154
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$800(Landroid/net/MobileDataStateTracker;)Landroid/net/LinkProperties;

    #@157
    move-result-object v10

    #@158
    if-nez v10, :cond_16b

    #@15a
    .line 273
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@15c
    const-string v11, "CONNECTED event did not supply link properties."

    #@15e
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$900(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@161
    .line 274
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@163
    new-instance v11, Landroid/net/LinkProperties;

    #@165
    invoke-direct {v11}, Landroid/net/LinkProperties;-><init>()V

    #@168
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$802(Landroid/net/MobileDataStateTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;

    #@16b
    .line 276
    :cond_16b
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@16d
    const-string/jumbo v10, "linkCapabilities"

    #@170
    move-object/from16 v0, p2

    #@172
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@175
    move-result-object v10

    #@176
    check-cast v10, Landroid/net/LinkCapabilities;

    #@178
    invoke-static {v11, v10}, Landroid/net/MobileDataStateTracker;->access$1002(Landroid/net/MobileDataStateTracker;Landroid/net/LinkCapabilities;)Landroid/net/LinkCapabilities;

    #@17b
    .line 278
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@17d
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$1000(Landroid/net/MobileDataStateTracker;)Landroid/net/LinkCapabilities;

    #@180
    move-result-object v10

    #@181
    if-nez v10, :cond_194

    #@183
    .line 279
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@185
    const-string v11, "CONNECTED event did not supply link capabilities."

    #@187
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$900(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@18a
    .line 280
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@18c
    new-instance v11, Landroid/net/LinkCapabilities;

    #@18e
    invoke-direct {v11}, Landroid/net/LinkCapabilities;-><init>()V

    #@191
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$1002(Landroid/net/MobileDataStateTracker;Landroid/net/LinkCapabilities;)Landroid/net/LinkCapabilities;

    #@194
    .line 282
    :cond_194
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@196
    sget-object v11, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@198
    invoke-static {v10, v11, v7, v2}, Landroid/net/MobileDataStateTracker;->access$700(Landroid/net/MobileDataStateTracker;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@19b
    goto/16 :goto_20

    #@19d
    .line 287
    :cond_19d
    const-string/jumbo v10, "linkPropertiesChanged"

    #@1a0
    invoke-static {v7, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@1a3
    move-result v10

    #@1a4
    if-eqz v10, :cond_20

    #@1a6
    .line 288
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1a8
    const-string/jumbo v10, "linkProperties"

    #@1ab
    move-object/from16 v0, p2

    #@1ad
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1b0
    move-result-object v10

    #@1b1
    check-cast v10, Landroid/net/LinkProperties;

    #@1b3
    invoke-static {v11, v10}, Landroid/net/MobileDataStateTracker;->access$802(Landroid/net/MobileDataStateTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;

    #@1b6
    .line 290
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1b8
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$800(Landroid/net/MobileDataStateTracker;)Landroid/net/LinkProperties;

    #@1bb
    move-result-object v10

    #@1bc
    if-nez v10, :cond_1cf

    #@1be
    .line 291
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1c0
    const-string v11, "No link property in LINK_PROPERTIES change event."

    #@1c2
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$900(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@1c5
    .line 292
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1c7
    new-instance v11, Landroid/net/LinkProperties;

    #@1c9
    invoke-direct {v11}, Landroid/net/LinkProperties;-><init>()V

    #@1cc
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$802(Landroid/net/MobileDataStateTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;

    #@1cf
    .line 295
    :cond_1cf
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1d1
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@1d4
    move-result-object v10

    #@1d5
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1d7
    invoke-static {v11}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@1da
    move-result-object v11

    #@1db
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@1de
    move-result-object v11

    #@1df
    iget-object v12, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1e1
    invoke-static {v12}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@1e4
    move-result-object v12

    #@1e5
    invoke-virtual {v12}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@1e8
    move-result-object v12

    #@1e9
    invoke-virtual {v10, v11, v7, v12}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@1ec
    .line 297
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1ee
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$400(Landroid/net/MobileDataStateTracker;)Landroid/os/Handler;

    #@1f1
    move-result-object v10

    #@1f2
    const/4 v11, 0x3

    #@1f3
    iget-object v12, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@1f5
    invoke-static {v12}, Landroid/net/MobileDataStateTracker;->access$300(Landroid/net/MobileDataStateTracker;)Landroid/net/NetworkInfo;

    #@1f8
    move-result-object v12

    #@1f9
    invoke-virtual {v10, v11, v12}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1fc
    move-result-object v4

    #@1fd
    .line 299
    .restart local v4       #msg:Landroid/os/Message;
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@200
    goto/16 :goto_20

    #@202
    .line 302
    .end local v2           #apnName:Ljava/lang/String;
    .end local v3           #apnType:Ljava/lang/String;
    .end local v4           #msg:Landroid/os/Message;
    .end local v5           #newSubType:I
    .end local v6           #oldSubtype:I
    .end local v7           #reason:Ljava/lang/String;
    .end local v8           #state:Lcom/android/internal/telephony/PhoneConstants$DataState;
    .end local v9           #subTypeName:Ljava/lang/String;
    :cond_202
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@205
    move-result-object v10

    #@206
    const-string v11, "android.intent.action.DATA_CONNECTION_FAILED"

    #@208
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20b
    move-result v10

    #@20c
    if-eqz v10, :cond_29c

    #@20e
    .line 304
    const-string v10, "apnType"

    #@210
    move-object/from16 v0, p2

    #@212
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@215
    move-result-object v3

    #@216
    .line 305
    .restart local v3       #apnType:Ljava/lang/String;
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@218
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$200(Landroid/net/MobileDataStateTracker;)Ljava/lang/String;

    #@21b
    move-result-object v10

    #@21c
    invoke-static {v3, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@21f
    move-result v10

    #@220
    if-nez v10, :cond_23e

    #@222
    .line 307
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@224
    const-string v11, "Broadcast received: ACTION_ANY_DATA_CONNECTION_FAILED ignore, mApnType=%s != received apnType=%s"

    #@226
    const/4 v12, 0x2

    #@227
    new-array v12, v12, [Ljava/lang/Object;

    #@229
    const/4 v13, 0x0

    #@22a
    iget-object v14, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@22c
    invoke-static {v14}, Landroid/net/MobileDataStateTracker;->access$200(Landroid/net/MobileDataStateTracker;)Ljava/lang/String;

    #@22f
    move-result-object v14

    #@230
    aput-object v14, v12, v13

    #@232
    const/4 v13, 0x1

    #@233
    aput-object v3, v12, v13

    #@235
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@238
    move-result-object v11

    #@239
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$600(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@23c
    goto/16 :goto_20

    #@23e
    .line 313
    :cond_23e
    const-string/jumbo v10, "reason"

    #@241
    move-object/from16 v0, p2

    #@243
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@246
    move-result-object v7

    #@247
    .line 314
    .restart local v7       #reason:Ljava/lang/String;
    const-string v10, "apn"

    #@249
    move-object/from16 v0, p2

    #@24b
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@24e
    move-result-object v2

    #@24f
    .line 316
    .restart local v2       #apnName:Ljava/lang/String;
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@251
    new-instance v10, Ljava/lang/StringBuilder;

    #@253
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@256
    const-string v12, "Received "

    #@258
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v10

    #@25c
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@25f
    move-result-object v12

    #@260
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@263
    move-result-object v10

    #@264
    const-string v12, " broadcast"

    #@266
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@269
    move-result-object v10

    #@26a
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v10

    #@26e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@271
    move-result-object v10

    #@272
    if-nez v10, :cond_282

    #@274
    const-string v10, ""

    #@276
    :goto_276
    invoke-static {v11, v10}, Landroid/net/MobileDataStateTracker;->access$600(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@279
    .line 319
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@27b
    sget-object v11, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@27d
    invoke-static {v10, v11, v7, v2}, Landroid/net/MobileDataStateTracker;->access$700(Landroid/net/MobileDataStateTracker;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@280
    goto/16 :goto_20

    #@282
    .line 316
    :cond_282
    new-instance v10, Ljava/lang/StringBuilder;

    #@284
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@287
    const-string v12, "("

    #@289
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v10

    #@28d
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@290
    move-result-object v10

    #@291
    const-string v12, ")"

    #@293
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@296
    move-result-object v10

    #@297
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29a
    move-result-object v10

    #@29b
    goto :goto_276

    #@29c
    .line 320
    .end local v2           #apnName:Ljava/lang/String;
    .end local v3           #apnType:Ljava/lang/String;
    .end local v7           #reason:Ljava/lang/String;
    :cond_29c
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@29f
    move-result-object v10

    #@2a0
    sget-object v11, Lcom/android/internal/telephony/DctConstants;->ACTION_DATA_CONNECTION_TRACKER_MESSENGER:Ljava/lang/String;

    #@2a2
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a5
    move-result v10

    #@2a6
    if-eqz v10, :cond_2d3

    #@2a8
    .line 323
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2aa
    sget-object v10, Lcom/android/internal/telephony/DctConstants;->EXTRA_MESSENGER:Ljava/lang/String;

    #@2ac
    move-object/from16 v0, p2

    #@2ae
    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@2b1
    move-result-object v10

    #@2b2
    check-cast v10, Landroid/os/Messenger;

    #@2b4
    invoke-static {v11, v10}, Landroid/net/MobileDataStateTracker;->access$1102(Landroid/net/MobileDataStateTracker;Landroid/os/Messenger;)Landroid/os/Messenger;

    #@2b7
    .line 325
    new-instance v1, Lcom/android/internal/util/AsyncChannel;

    #@2b9
    invoke-direct {v1}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@2bc
    .line 326
    .local v1, ac:Lcom/android/internal/util/AsyncChannel;
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2be
    invoke-static {v10}, Landroid/net/MobileDataStateTracker;->access$1200(Landroid/net/MobileDataStateTracker;)Landroid/content/Context;

    #@2c1
    move-result-object v10

    #@2c2
    iget-object v11, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2c4
    invoke-static {v11}, Landroid/net/MobileDataStateTracker;->access$1300(Landroid/net/MobileDataStateTracker;)Landroid/os/Handler;

    #@2c7
    move-result-object v11

    #@2c8
    iget-object v12, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2ca
    invoke-static {v12}, Landroid/net/MobileDataStateTracker;->access$1100(Landroid/net/MobileDataStateTracker;)Landroid/os/Messenger;

    #@2cd
    move-result-object v12

    #@2ce
    invoke-virtual {v1, v10, v11, v12}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@2d1
    goto/16 :goto_20

    #@2d3
    .line 328
    .end local v1           #ac:Lcom/android/internal/util/AsyncChannel;
    :cond_2d3
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2d5
    iget-object v10, v10, Landroid/net/MobileDataStateTracker;->featureset:Ljava/lang/String;

    #@2d7
    const-string v11, "KTBASE"

    #@2d9
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2dc
    move-result v10

    #@2dd
    if-eqz v10, :cond_2f2

    #@2df
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2e2
    move-result-object v10

    #@2e3
    const-string v11, "android.intent.action.BOOT_COMPLETED"

    #@2e5
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e8
    move-result v10

    #@2e9
    if-eqz v10, :cond_2f2

    #@2eb
    .line 329
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2ed
    const/4 v11, 0x1

    #@2ee
    iput-boolean v11, v10, Landroid/net/MobileDataStateTracker;->isBootCompleted:Z

    #@2f0
    goto/16 :goto_20

    #@2f2
    .line 332
    :cond_2f2
    iget-object v10, p0, Landroid/net/MobileDataStateTracker$MobileDataStateReceiver;->this$0:Landroid/net/MobileDataStateTracker;

    #@2f4
    new-instance v11, Ljava/lang/StringBuilder;

    #@2f6
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@2f9
    const-string v12, "Broadcast received: ignore "

    #@2fb
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fe
    move-result-object v11

    #@2ff
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@302
    move-result-object v12

    #@303
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@306
    move-result-object v11

    #@307
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30a
    move-result-object v11

    #@30b
    invoke-static {v10, v11}, Landroid/net/MobileDataStateTracker;->access$600(Landroid/net/MobileDataStateTracker;Ljava/lang/String;)V

    #@30e
    goto/16 :goto_20

    #@310
    .line 249
    :pswitch_data_310
    .packed-switch 0x1
        :pswitch_115
        :pswitch_130
        :pswitch_139
        :pswitch_142
    .end packed-switch
.end method
