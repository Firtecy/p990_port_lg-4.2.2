.class Landroid/net/CaptivePortalTracker$ActiveNetworkState;
.super Lcom/android/internal/util/State;
.source "CaptivePortalTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/CaptivePortalTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActiveNetworkState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/CaptivePortalTracker;


# direct methods
.method private constructor <init>(Landroid/net/CaptivePortalTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 188
    iput-object p1, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 188
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker$ActiveNetworkState;-><init>(Landroid/net/CaptivePortalTracker;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 1

    #@0
    .prologue
    .line 192
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 197
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_5e

    #@5
    .line 213
    const/4 v1, 0x0

    #@6
    .line 215
    :goto_6
    return v1

    #@7
    .line 199
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v0, Landroid/net/NetworkInfo;

    #@b
    .line 200
    .local v0, info:Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_2e

    #@11
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@14
    move-result v1

    #@15
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@17
    invoke-static {v2}, Landroid/net/CaptivePortalTracker;->access$600(Landroid/net/CaptivePortalTracker;)Landroid/net/NetworkInfo;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    #@1e
    move-result v2

    #@1f
    if-ne v1, v2, :cond_2e

    #@21
    .line 203
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@23
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@25
    invoke-static {v2}, Landroid/net/CaptivePortalTracker;->access$1100(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/net/CaptivePortalTracker;->access$1200(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V

    #@2c
    .line 215
    :cond_2c
    :goto_2c
    const/4 v1, 0x1

    #@2d
    goto :goto_6

    #@2e
    .line 204
    :cond_2e
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@31
    move-result v1

    #@32
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@34
    invoke-static {v2}, Landroid/net/CaptivePortalTracker;->access$600(Landroid/net/CaptivePortalTracker;)Landroid/net/NetworkInfo;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    #@3b
    move-result v2

    #@3c
    if-eq v1, v2, :cond_2c

    #@3e
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_2c

    #@44
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@46
    invoke-static {v1, v0}, Landroid/net/CaptivePortalTracker;->access$800(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)Z

    #@49
    move-result v1

    #@4a
    if-eqz v1, :cond_2c

    #@4c
    .line 208
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@4e
    invoke-static {v1, p1}, Landroid/net/CaptivePortalTracker;->access$1300(Landroid/net/CaptivePortalTracker;Landroid/os/Message;)V

    #@51
    .line 209
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@53
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$ActiveNetworkState;->this$0:Landroid/net/CaptivePortalTracker;

    #@55
    invoke-static {v2}, Landroid/net/CaptivePortalTracker;->access$1100(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;

    #@58
    move-result-object v2

    #@59
    invoke-static {v1, v2}, Landroid/net/CaptivePortalTracker;->access$1400(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V

    #@5c
    goto :goto_2c

    #@5d
    .line 197
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method
