.class public Landroid/net/NetworkStatsHistory$ParcelUtils;
.super Ljava/lang/Object;
.source "NetworkStatsHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/NetworkStatsHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParcelUtils"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 710
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static readLongArray(Landroid/os/Parcel;)[J
    .registers 6
    .parameter "in"

    #@0
    .prologue
    .line 712
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v1

    #@4
    .line 713
    .local v1, size:I
    const/4 v3, -0x1

    #@5
    if-ne v1, v3, :cond_9

    #@7
    const/4 v2, 0x0

    #@8
    .line 718
    :cond_8
    return-object v2

    #@9
    .line 714
    :cond_9
    new-array v2, v1, [J

    #@b
    .line 715
    .local v2, values:[J
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    array-length v3, v2

    #@d
    if-ge v0, v3, :cond_8

    #@f
    .line 716
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    #@12
    move-result-wide v3

    #@13
    aput-wide v3, v2, v0

    #@15
    .line 715
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_c
.end method

.method public static writeLongArray(Landroid/os/Parcel;[JI)V
    .registers 6
    .parameter "out"
    .parameter "values"
    .parameter "size"

    #@0
    .prologue
    .line 722
    if-nez p1, :cond_7

    #@2
    .line 723
    const/4 v1, -0x1

    #@3
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 733
    :cond_6
    return-void

    #@7
    .line 726
    :cond_7
    array-length v1, p1

    #@8
    if-le p2, v1, :cond_13

    #@a
    .line 727
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@c
    const-string/jumbo v2, "size larger than length"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1

    #@13
    .line 729
    :cond_13
    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 730
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    :goto_17
    if-ge v0, p2, :cond_6

    #@19
    .line 731
    aget-wide v1, p1, v0

    #@1b
    invoke-virtual {p0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 730
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_17
.end method
