.class Landroid/net/dhcp/DhcpDiscoverPacket;
.super Landroid/net/dhcp/DhcpPacket;
.source "DhcpDiscoverPacket.java"


# direct methods
.method constructor <init>(I[BZ)V
    .registers 12
    .parameter "transId"
    .parameter "clientMac"
    .parameter "broadcast"

    #@0
    .prologue
    .line 31
    sget-object v2, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@2
    sget-object v3, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@4
    sget-object v4, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@6
    sget-object v5, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@8
    move-object v0, p0

    #@9
    move v1, p1

    #@a
    move-object v6, p2

    #@b
    move v7, p3

    #@c
    invoke-direct/range {v0 .. v7}, Landroid/net/dhcp/DhcpPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[BZ)V

    #@f
    .line 33
    return-void
.end method


# virtual methods
.method public buildPacket(ISS)Ljava/nio/ByteBuffer;
    .registers 14
    .parameter "encap"
    .parameter "destUdp"
    .parameter "srcUdp"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 45
    const/16 v0, 0x5dc

    #@3
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@6
    move-result-object v6

    #@7
    .line 46
    .local v6, result:Ljava/nio/ByteBuffer;
    sget-object v9, Ljava/net/Inet4Address;->ALL:Ljava/net/InetAddress;

    #@9
    .line 48
    .local v9, destIp:Ljava/net/InetAddress;
    sget-object v2, Ljava/net/Inet4Address;->ALL:Ljava/net/InetAddress;

    #@b
    sget-object v3, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@d
    move-object v0, p0

    #@e
    move v1, p1

    #@f
    move v4, p2

    #@10
    move v5, p3

    #@11
    move v8, v7

    #@12
    invoke-virtual/range {v0 .. v8}, Landroid/net/dhcp/DhcpDiscoverPacket;->fillInPacket(ILjava/net/InetAddress;Ljava/net/InetAddress;SSLjava/nio/ByteBuffer;BZ)V

    #@15
    .line 50
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@18
    .line 51
    return-object v6
.end method

.method public doNextOp(Landroid/net/dhcp/DhcpStateMachine;)V
    .registers 6
    .parameter "machine"

    #@0
    .prologue
    .line 68
    iget-boolean v0, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@2
    iget v1, p0, Landroid/net/dhcp/DhcpPacket;->mTransId:I

    #@4
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@6
    iget-object v3, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@8
    invoke-interface {p1, v0, v1, v2, v3}, Landroid/net/dhcp/DhcpStateMachine;->onDiscoverReceived(ZI[B[B)V

    #@b
    .line 70
    return-void
.end method

.method finishPacket(Ljava/nio/ByteBuffer;)V
    .registers 4
    .parameter "buffer"

    #@0
    .prologue
    .line 58
    const/16 v0, 0x35

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpDiscoverPacket;->addTlv(Ljava/nio/ByteBuffer;BB)V

    #@6
    .line 59
    const/16 v0, 0x37

    #@8
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@a
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpDiscoverPacket;->addTlv(Ljava/nio/ByteBuffer;B[B)V

    #@d
    .line 60
    invoke-virtual {p0, p1}, Landroid/net/dhcp/DhcpDiscoverPacket;->addTlvEnd(Ljava/nio/ByteBuffer;)V

    #@10
    .line 61
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 36
    invoke-super {p0}, Landroid/net/dhcp/DhcpPacket;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 37
    .local v0, s:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " DISCOVER "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-boolean v1, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@15
    if-eqz v1, :cond_22

    #@17
    const-string v1, "broadcast "

    #@19
    :goto_19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    return-object v1

    #@22
    :cond_22
    const-string/jumbo v1, "unicast "

    #@25
    goto :goto_19
.end method
