.class abstract Landroid/net/dhcp/DhcpPacket;
.super Ljava/lang/Object;
.source "DhcpPacket.java"


# static fields
.field protected static final CLIENT_ID_ETHER:B = 0x1t

.field protected static final DHCP_BOOTREPLY:B = 0x2t

.field protected static final DHCP_BOOTREQUEST:B = 0x1t

.field protected static final DHCP_BROADCAST_ADDRESS:B = 0x1ct

.field static final DHCP_CLIENT:S = 0x44s

.field protected static final DHCP_CLIENT_IDENTIFIER:B = 0x3dt

.field protected static final DHCP_DNS_SERVER:B = 0x6t

.field protected static final DHCP_DOMAIN_NAME:B = 0xft

.field protected static final DHCP_HOST_NAME:B = 0xct

.field protected static final DHCP_LEASE_TIME:B = 0x33t

.field protected static final DHCP_MESSAGE:B = 0x38t

.field protected static final DHCP_MESSAGE_TYPE:B = 0x35t

.field protected static final DHCP_MESSAGE_TYPE_ACK:B = 0x5t

.field protected static final DHCP_MESSAGE_TYPE_DECLINE:B = 0x4t

.field protected static final DHCP_MESSAGE_TYPE_DISCOVER:B = 0x1t

.field protected static final DHCP_MESSAGE_TYPE_INFORM:B = 0x8t

.field protected static final DHCP_MESSAGE_TYPE_NAK:B = 0x6t

.field protected static final DHCP_MESSAGE_TYPE_OFFER:B = 0x2t

.field protected static final DHCP_MESSAGE_TYPE_REQUEST:B = 0x3t

.field protected static final DHCP_PARAMETER_LIST:B = 0x37t

.field protected static final DHCP_RENEWAL_TIME:B = 0x3at

.field protected static final DHCP_REQUESTED_IP:B = 0x32t

.field protected static final DHCP_ROUTER:B = 0x3t

.field static final DHCP_SERVER:S = 0x43s

.field protected static final DHCP_SERVER_IDENTIFIER:B = 0x36t

.field protected static final DHCP_SUBNET_MASK:B = 0x1t

.field protected static final DHCP_VENDOR_CLASS_ID:B = 0x3ct

.field public static final ENCAP_BOOTP:I = 0x2

.field public static final ENCAP_L2:I = 0x0

.field public static final ENCAP_L3:I = 0x1

.field private static final IP_FLAGS_OFFSET:S = 0x4000s

.field private static final IP_TOS_LOWDELAY:B = 0x10t

.field private static final IP_TTL:B = 0x40t

.field private static final IP_TYPE_UDP:B = 0x11t

.field private static final IP_VERSION_HEADER_LEN:B = 0x45t

.field protected static final MAX_LENGTH:I = 0x5dc

.field protected static final TAG:Ljava/lang/String; = "DhcpPacket"


# instance fields
.field protected mBroadcast:Z

.field protected mBroadcastAddress:Ljava/net/InetAddress;

.field protected final mClientIp:Ljava/net/InetAddress;

.field protected final mClientMac:[B

.field protected mDnsServers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field protected mDomainName:Ljava/lang/String;

.field protected mGateway:Ljava/net/InetAddress;

.field protected mHostName:Ljava/lang/String;

.field protected mLeaseTime:Ljava/lang/Integer;

.field protected mMessage:Ljava/lang/String;

.field private final mNextIp:Ljava/net/InetAddress;

.field private final mRelayIp:Ljava/net/InetAddress;

.field protected mRequestedIp:Ljava/net/InetAddress;

.field protected mRequestedParams:[B

.field protected mServerIdentifier:Ljava/net/InetAddress;

.field protected mSubnetMask:Ljava/net/InetAddress;

.field protected final mTransId:I

.field protected final mYourIp:Ljava/net/InetAddress;


# direct methods
.method protected constructor <init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[BZ)V
    .registers 8
    .parameter "transId"
    .parameter "clientIp"
    .parameter "yourIp"
    .parameter "nextIp"
    .parameter "relayIp"
    .parameter "clientMac"
    .parameter "broadcast"

    #@0
    .prologue
    .line 228
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 229
    iput p1, p0, Landroid/net/dhcp/DhcpPacket;->mTransId:I

    #@5
    .line 230
    iput-object p2, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@7
    .line 231
    iput-object p3, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@9
    .line 232
    iput-object p4, p0, Landroid/net/dhcp/DhcpPacket;->mNextIp:Ljava/net/InetAddress;

    #@b
    .line 233
    iput-object p5, p0, Landroid/net/dhcp/DhcpPacket;->mRelayIp:Ljava/net/InetAddress;

    #@d
    .line 234
    iput-object p6, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@f
    .line 235
    iput-boolean p7, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@11
    .line 236
    return-void
.end method

.method public static buildAckPacket(IIZLjava/net/InetAddress;Ljava/net/InetAddress;[BLjava/lang/Integer;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/util/List;Ljava/net/InetAddress;Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .registers 20
    .parameter "encap"
    .parameter "transactionId"
    .parameter "broadcast"
    .parameter "serverIpAddr"
    .parameter "clientIpAddr"
    .parameter "mac"
    .parameter "timeout"
    .parameter "netMask"
    .parameter "bcAddr"
    .parameter "gateway"
    .parameter
    .parameter "dhcpServerIdentifier"
    .parameter "domainName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "[B",
            "Ljava/lang/Integer;",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;",
            "Ljava/net/InetAddress;",
            "Ljava/lang/String;",
            ")",
            "Ljava/nio/ByteBuffer;"
        }
    .end annotation

    #@0
    .prologue
    .line 858
    .local p10, dnsServers:Ljava/util/List;,"Ljava/util/List<Ljava/net/InetAddress;>;"
    new-instance v1, Landroid/net/dhcp/DhcpAckPacket;

    #@2
    move v2, p1

    #@3
    move v3, p2

    #@4
    move-object v4, p3

    #@5
    move-object v5, p4

    #@6
    move-object v6, p5

    #@7
    invoke-direct/range {v1 .. v6}, Landroid/net/dhcp/DhcpAckPacket;-><init>(IZLjava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@a
    .line 860
    .local v1, pkt:Landroid/net/dhcp/DhcpPacket;
    move-object/from16 v0, p9

    #@c
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mGateway:Ljava/net/InetAddress;

    #@e
    .line 861
    move-object/from16 v0, p10

    #@10
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mDnsServers:Ljava/util/List;

    #@12
    .line 862
    iput-object p6, v1, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@14
    .line 863
    move-object/from16 v0, p12

    #@16
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mDomainName:Ljava/lang/String;

    #@18
    .line 864
    iput-object p7, v1, Landroid/net/dhcp/DhcpPacket;->mSubnetMask:Ljava/net/InetAddress;

    #@1a
    .line 865
    move-object/from16 v0, p11

    #@1c
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@1e
    .line 866
    iput-object p8, v1, Landroid/net/dhcp/DhcpPacket;->mBroadcastAddress:Ljava/net/InetAddress;

    #@20
    .line 867
    const/16 v2, 0x44

    #@22
    const/16 v3, 0x43

    #@24
    invoke-virtual {v1, p0, v2, v3}, Landroid/net/dhcp/DhcpAckPacket;->buildPacket(ISS)Ljava/nio/ByteBuffer;

    #@27
    move-result-object v2

    #@28
    return-object v2
.end method

.method public static buildDiscoverPacket(II[BZ[B)Ljava/nio/ByteBuffer;
    .registers 8
    .parameter "encap"
    .parameter "transactionId"
    .parameter "clientMac"
    .parameter "broadcast"
    .parameter "expectedParams"

    #@0
    .prologue
    .line 823
    new-instance v0, Landroid/net/dhcp/DhcpDiscoverPacket;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/net/dhcp/DhcpDiscoverPacket;-><init>(I[BZ)V

    #@5
    .line 825
    .local v0, pkt:Landroid/net/dhcp/DhcpPacket;
    iput-object p4, v0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@7
    .line 826
    const/16 v1, 0x43

    #@9
    const/16 v2, 0x44

    #@b
    invoke-virtual {v0, p0, v1, v2}, Landroid/net/dhcp/DhcpDiscoverPacket;->buildPacket(ISS)Ljava/nio/ByteBuffer;

    #@e
    move-result-object v1

    #@f
    return-object v1
.end method

.method public static buildNakPacket(IILjava/net/InetAddress;Ljava/net/InetAddress;[B)Ljava/nio/ByteBuffer;
    .registers 12
    .parameter "encap"
    .parameter "transactionId"
    .parameter "serverIpAddr"
    .parameter "clientIpAddr"
    .parameter "mac"

    #@0
    .prologue
    .line 875
    new-instance v0, Landroid/net/dhcp/DhcpNakPacket;

    #@2
    move v1, p1

    #@3
    move-object v2, p3

    #@4
    move-object v3, p2

    #@5
    move-object v4, p2

    #@6
    move-object v5, p2

    #@7
    move-object v6, p4

    #@8
    invoke-direct/range {v0 .. v6}, Landroid/net/dhcp/DhcpNakPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@b
    .line 877
    .local v0, pkt:Landroid/net/dhcp/DhcpPacket;
    const-string/jumbo v1, "requested address not available"

    #@e
    iput-object v1, v0, Landroid/net/dhcp/DhcpPacket;->mMessage:Ljava/lang/String;

    #@10
    .line 878
    iput-object p3, v0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@12
    .line 879
    const/16 v1, 0x44

    #@14
    const/16 v2, 0x43

    #@16
    invoke-virtual {v0, p0, v1, v2}, Landroid/net/dhcp/DhcpNakPacket;->buildPacket(ISS)Ljava/nio/ByteBuffer;

    #@19
    move-result-object v1

    #@1a
    return-object v1
.end method

.method public static buildOfferPacket(IIZLjava/net/InetAddress;Ljava/net/InetAddress;[BLjava/lang/Integer;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/util/List;Ljava/net/InetAddress;Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .registers 20
    .parameter "encap"
    .parameter "transactionId"
    .parameter "broadcast"
    .parameter "serverIpAddr"
    .parameter "clientIpAddr"
    .parameter "mac"
    .parameter "timeout"
    .parameter "netMask"
    .parameter "bcAddr"
    .parameter "gateway"
    .parameter
    .parameter "dhcpServerIdentifier"
    .parameter "domainName"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "[B",
            "Ljava/lang/Integer;",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;",
            "Ljava/net/InetAddress;",
            "Ljava/lang/String;",
            ")",
            "Ljava/nio/ByteBuffer;"
        }
    .end annotation

    #@0
    .prologue
    .line 838
    .local p10, dnsServers:Ljava/util/List;,"Ljava/util/List<Ljava/net/InetAddress;>;"
    new-instance v1, Landroid/net/dhcp/DhcpOfferPacket;

    #@2
    move v2, p1

    #@3
    move v3, p2

    #@4
    move-object v4, p3

    #@5
    move-object v5, p4

    #@6
    move-object v6, p5

    #@7
    invoke-direct/range {v1 .. v6}, Landroid/net/dhcp/DhcpOfferPacket;-><init>(IZLjava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@a
    .line 840
    .local v1, pkt:Landroid/net/dhcp/DhcpPacket;
    move-object/from16 v0, p9

    #@c
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mGateway:Ljava/net/InetAddress;

    #@e
    .line 841
    move-object/from16 v0, p10

    #@10
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mDnsServers:Ljava/util/List;

    #@12
    .line 842
    iput-object p6, v1, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@14
    .line 843
    move-object/from16 v0, p12

    #@16
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mDomainName:Ljava/lang/String;

    #@18
    .line 844
    move-object/from16 v0, p11

    #@1a
    iput-object v0, v1, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@1c
    .line 845
    iput-object p7, v1, Landroid/net/dhcp/DhcpPacket;->mSubnetMask:Ljava/net/InetAddress;

    #@1e
    .line 846
    iput-object p8, v1, Landroid/net/dhcp/DhcpPacket;->mBroadcastAddress:Ljava/net/InetAddress;

    #@20
    .line 847
    const/16 v2, 0x44

    #@22
    const/16 v3, 0x43

    #@24
    invoke-virtual {v1, p0, v2, v3}, Landroid/net/dhcp/DhcpOfferPacket;->buildPacket(ISS)Ljava/nio/ByteBuffer;

    #@27
    move-result-object v2

    #@28
    return-object v2
.end method

.method public static buildRequestPacket(IILjava/net/InetAddress;Z[BLjava/net/InetAddress;Ljava/net/InetAddress;[BLjava/lang/String;)Ljava/nio/ByteBuffer;
    .registers 13
    .parameter "encap"
    .parameter "transactionId"
    .parameter "clientIp"
    .parameter "broadcast"
    .parameter "clientMac"
    .parameter "requestedIpAddress"
    .parameter "serverIdentifier"
    .parameter "requestedParams"
    .parameter "hostName"

    #@0
    .prologue
    .line 889
    new-instance v0, Landroid/net/dhcp/DhcpRequestPacket;

    #@2
    invoke-direct {v0, p1, p2, p4, p3}, Landroid/net/dhcp/DhcpRequestPacket;-><init>(ILjava/net/InetAddress;[BZ)V

    #@5
    .line 891
    .local v0, pkt:Landroid/net/dhcp/DhcpPacket;
    iput-object p5, v0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@7
    .line 892
    iput-object p6, v0, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@9
    .line 893
    iput-object p8, v0, Landroid/net/dhcp/DhcpPacket;->mHostName:Ljava/lang/String;

    #@b
    .line 894
    iput-object p7, v0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@d
    .line 895
    const/16 v2, 0x43

    #@f
    const/16 v3, 0x44

    #@11
    invoke-virtual {v0, p0, v2, v3}, Landroid/net/dhcp/DhcpRequestPacket;->buildPacket(ISS)Ljava/nio/ByteBuffer;

    #@14
    move-result-object v1

    #@15
    .line 896
    .local v1, result:Ljava/nio/ByteBuffer;
    return-object v1
.end method

.method private checksum(Ljava/nio/ByteBuffer;III)I
    .registers 19
    .parameter "buf"
    .parameter "seed"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 377
    move/from16 v11, p2

    #@2
    .line 378
    .local v11, sum:I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@5
    move-result v4

    #@6
    .line 382
    .local v4, bufPosition:I
    move/from16 v0, p3

    #@8
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@b
    .line 383
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    #@e
    move-result-object v10

    #@f
    .line 386
    .local v10, shortBuf:Ljava/nio/ShortBuffer;
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@12
    .line 388
    sub-int v12, p4, p3

    #@14
    div-int/lit8 v12, v12, 0x2

    #@16
    new-array v9, v12, [S

    #@18
    .line 389
    .local v9, shortArray:[S
    invoke-virtual {v10, v9}, Ljava/nio/ShortBuffer;->get([S)Ljava/nio/ShortBuffer;

    #@1b
    .line 391
    move-object v2, v9

    #@1c
    .local v2, arr$:[S
    array-length v6, v2

    #@1d
    .local v6, len$:I
    const/4 v5, 0x0

    #@1e
    .local v5, i$:I
    :goto_1e
    if-ge v5, v6, :cond_2a

    #@20
    aget-short v8, v2, v5

    #@22
    .line 392
    .local v8, s:S
    invoke-direct {p0, v8}, Landroid/net/dhcp/DhcpPacket;->intAbs(S)I

    #@25
    move-result v12

    #@26
    add-int/2addr v11, v12

    #@27
    .line 391
    add-int/lit8 v5, v5, 0x1

    #@29
    goto :goto_1e

    #@2a
    .line 395
    .end local v8           #s:S
    :cond_2a
    array-length v12, v9

    #@2b
    mul-int/lit8 v12, v12, 0x2

    #@2d
    add-int p3, p3, v12

    #@2f
    .line 398
    move/from16 v0, p4

    #@31
    move/from16 v1, p3

    #@33
    if-eq v0, v1, :cond_44

    #@35
    .line 399
    move/from16 v0, p3

    #@37
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    #@3a
    move-result v12

    #@3b
    int-to-short v3, v12

    #@3c
    .line 402
    .local v3, b:S
    if-gez v3, :cond_41

    #@3e
    .line 403
    add-int/lit16 v12, v3, 0x100

    #@40
    int-to-short v3, v12

    #@41
    .line 406
    :cond_41
    mul-int/lit16 v12, v3, 0x100

    #@43
    add-int/2addr v11, v12

    #@44
    .line 409
    .end local v3           #b:S
    :cond_44
    shr-int/lit8 v12, v11, 0x10

    #@46
    const v13, 0xffff

    #@49
    and-int/2addr v12, v13

    #@4a
    const v13, 0xffff

    #@4d
    and-int/2addr v13, v11

    #@4e
    add-int v11, v12, v13

    #@50
    .line 410
    shr-int/lit8 v12, v11, 0x10

    #@52
    const v13, 0xffff

    #@55
    and-int/2addr v12, v13

    #@56
    add-int/2addr v12, v11

    #@57
    const v13, 0xffff

    #@5a
    and-int v11, v12, v13

    #@5c
    .line 411
    xor-int/lit8 v7, v11, -0x1

    #@5e
    .line 412
    .local v7, negated:I
    int-to-short v12, v7

    #@5f
    invoke-direct {p0, v12}, Landroid/net/dhcp/DhcpPacket;->intAbs(S)I

    #@62
    move-result v12

    #@63
    return v12
.end method

.method public static decodeFullPacket(Ljava/nio/ByteBuffer;I)Landroid/net/dhcp/DhcpPacket;
    .registers 62
    .parameter "packet"
    .parameter "pktType"

    #@0
    .prologue
    .line 564
    new-instance v20, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 565
    .local v20, dnsServers:Ljava/util/List;,"Ljava/util/List<Ljava/net/InetAddress;>;"
    const/16 v26, 0x0

    #@7
    .line 566
    .local v26, gateway:Ljava/net/InetAddress;
    const/16 v46, 0x0

    #@9
    .line 567
    .local v46, leaseTime:Ljava/lang/Integer;
    const/16 v53, 0x0

    #@b
    .line 568
    .local v53, serverIdentifier:Ljava/net/InetAddress;
    const/16 v48, 0x0

    #@d
    .line 569
    .local v48, netMask:Ljava/net/InetAddress;
    const/16 v47, 0x0

    #@f
    .line 570
    .local v47, message:Ljava/lang/String;
    const/16 v59, 0x0

    #@11
    .line 571
    .local v59, vendorId:Ljava/lang/String;
    const/16 v25, 0x0

    #@13
    .line 572
    .local v25, expectedParams:[B
    const/16 v28, 0x0

    #@15
    .line 573
    .local v28, hostName:Ljava/lang/String;
    const/16 v21, 0x0

    #@17
    .line 574
    .local v21, domainName:Ljava/lang/String;
    const/4 v5, 0x0

    #@18
    .line 575
    .local v5, ipSrc:Ljava/net/InetAddress;
    const/16 v34, 0x0

    #@1a
    .line 576
    .local v34, ipDst:Ljava/net/InetAddress;
    const/16 v16, 0x0

    #@1c
    .line 577
    .local v16, bcAddr:Ljava/net/InetAddress;
    const/16 v52, 0x0

    #@1e
    .line 580
    .local v52, requestedIp:Ljava/net/InetAddress;
    const/16 v19, -0x1

    #@20
    .line 582
    .local v19, dhcpType:B
    sget-object v8, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@22
    move-object/from16 v0, p0

    #@24
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@27
    .line 585
    if-nez p1, :cond_4d

    #@29
    .line 587
    const/4 v8, 0x6

    #@2a
    new-array v0, v8, [B

    #@2c
    move-object/from16 v43, v0

    #@2e
    .line 588
    .local v43, l2dst:[B
    const/4 v8, 0x6

    #@2f
    new-array v0, v8, [B

    #@31
    move-object/from16 v44, v0

    #@33
    .line 590
    .local v44, l2src:[B
    move-object/from16 v0, p0

    #@35
    move-object/from16 v1, v43

    #@37
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@3a
    .line 591
    move-object/from16 v0, p0

    #@3c
    move-object/from16 v1, v44

    #@3e
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@41
    .line 593
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@44
    move-result v45

    #@45
    .line 595
    .local v45, l2type:S
    const/16 v8, 0x800

    #@47
    move/from16 v0, v45

    #@49
    if-eq v0, v8, :cond_4d

    #@4b
    .line 596
    const/4 v2, 0x0

    #@4c
    .line 805
    .end local v43           #l2dst:[B
    .end local v44           #l2src:[B
    .end local v45           #l2type:S
    :goto_4c
    return-object v2

    #@4d
    .line 599
    :cond_4d
    if-eqz p1, :cond_54

    #@4f
    const/4 v8, 0x1

    #@50
    move/from16 v0, p1

    #@52
    if-ne v0, v8, :cond_a6

    #@54
    .line 601
    :cond_54
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@57
    move-result v41

    #@58
    .line 603
    .local v41, ipType:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@5b
    move-result v33

    #@5c
    .line 604
    .local v33, ipDiffServicesField:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@5f
    move-result v40

    #@60
    .line 605
    .local v40, ipTotalLength:S
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@63
    move-result v37

    #@64
    .line 606
    .local v37, ipIdentification:S
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@67
    move-result v35

    #@68
    .line 607
    .local v35, ipFlags:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@6b
    move-result v36

    #@6c
    .line 608
    .local v36, ipFragOffset:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@6f
    move-result v39

    #@70
    .line 609
    .local v39, ipTTL:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@73
    move-result v38

    #@74
    .line 610
    .local v38, ipProto:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@77
    move-result v32

    #@78
    .line 612
    .local v32, ipChksm:S
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@7b
    move-result-object v5

    #@7c
    .line 613
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@7f
    move-result-object v34

    #@80
    .line 615
    const/16 v8, 0x11

    #@82
    move/from16 v0, v38

    #@84
    if-eq v0, v8, :cond_88

    #@86
    .line 616
    const/4 v2, 0x0

    #@87
    goto :goto_4c

    #@88
    .line 619
    :cond_88
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@8b
    move-result v58

    #@8c
    .line 620
    .local v58, udpSrcPort:S
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@8f
    move-result v56

    #@90
    .line 621
    .local v56, udpDstPort:S
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@93
    move-result v57

    #@94
    .line 622
    .local v57, udpLen:S
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@97
    move-result v55

    #@98
    .line 624
    .local v55, udpChkSum:S
    const/16 v8, 0x43

    #@9a
    move/from16 v0, v58

    #@9c
    if-eq v0, v8, :cond_a6

    #@9e
    const/16 v8, 0x44

    #@a0
    move/from16 v0, v58

    #@a2
    if-eq v0, v8, :cond_a6

    #@a4
    .line 625
    const/4 v2, 0x0

    #@a5
    goto :goto_4c

    #@a6
    .line 629
    .end local v32           #ipChksm:S
    .end local v33           #ipDiffServicesField:B
    .end local v35           #ipFlags:B
    .end local v36           #ipFragOffset:B
    .end local v37           #ipIdentification:S
    .end local v38           #ipProto:B
    .end local v39           #ipTTL:B
    .end local v40           #ipTotalLength:S
    .end local v41           #ipType:B
    .end local v55           #udpChkSum:S
    .end local v56           #udpDstPort:S
    .end local v57           #udpLen:S
    .end local v58           #udpSrcPort:S
    :cond_a6
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@a9
    move-result v54

    #@aa
    .line 630
    .local v54, type:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@ad
    move-result v29

    #@ae
    .line 631
    .local v29, hwType:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@b1
    move-result v15

    #@b2
    .line 632
    .local v15, addrLen:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@b5
    move-result v27

    #@b6
    .line 633
    .local v27, hops:B
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    #@b9
    move-result v3

    #@ba
    .line 634
    .local v3, transactionId:I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@bd
    move-result v22

    #@be
    .line 635
    .local v22, elapsed:S
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getShort()S

    #@c1
    move-result v17

    #@c2
    .line 636
    .local v17, bootpFlags:S
    const v8, 0x8000

    #@c5
    and-int v8, v8, v17

    #@c7
    if-eqz v8, :cond_120

    #@c9
    const/4 v4, 0x1

    #@ca
    .line 637
    .local v4, broadcast:Z
    :goto_ca
    const/4 v8, 0x4

    #@cb
    new-array v0, v8, [B

    #@cd
    move-object/from16 v42, v0

    #@cf
    .line 640
    .local v42, ipv4addr:[B
    :try_start_cf
    move-object/from16 v0, p0

    #@d1
    move-object/from16 v1, v42

    #@d3
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@d6
    .line 641
    invoke-static/range {v42 .. v42}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@d9
    move-result-object v10

    #@da
    .line 642
    .local v10, clientIp:Ljava/net/InetAddress;
    move-object/from16 v0, p0

    #@dc
    move-object/from16 v1, v42

    #@de
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@e1
    .line 643
    invoke-static/range {v42 .. v42}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@e4
    move-result-object v6

    #@e5
    .line 644
    .local v6, yourIp:Ljava/net/InetAddress;
    move-object/from16 v0, p0

    #@e7
    move-object/from16 v1, v42

    #@e9
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@ec
    .line 645
    invoke-static/range {v42 .. v42}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@ef
    move-result-object v12

    #@f0
    .line 646
    .local v12, nextIp:Ljava/net/InetAddress;
    move-object/from16 v0, p0

    #@f2
    move-object/from16 v1, v42

    #@f4
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@f7
    .line 647
    invoke-static/range {v42 .. v42}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_fa
    .catch Ljava/net/UnknownHostException; {:try_start_cf .. :try_end_fa} :catch_122

    #@fa
    move-result-object v13

    #@fb
    .line 652
    .local v13, relayIp:Ljava/net/InetAddress;
    new-array v7, v15, [B

    #@fd
    .line 653
    .local v7, clientMac:[B
    move-object/from16 v0, p0

    #@ff
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@102
    .line 656
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->position()I

    #@105
    move-result v8

    #@106
    rsub-int/lit8 v9, v15, 0x10

    #@108
    add-int/2addr v8, v9

    #@109
    add-int/lit8 v8, v8, 0x40

    #@10b
    add-int/lit16 v8, v8, 0x80

    #@10d
    move-object/from16 v0, p0

    #@10f
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@112
    .line 660
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    #@115
    move-result v18

    #@116
    .line 662
    .local v18, dhcpMagicCookie:I
    const v8, 0x63825363

    #@119
    move/from16 v0, v18

    #@11b
    if-eq v0, v8, :cond_126

    #@11d
    .line 663
    const/4 v2, 0x0

    #@11e
    goto/16 :goto_4c

    #@120
    .line 636
    .end local v4           #broadcast:Z
    .end local v6           #yourIp:Ljava/net/InetAddress;
    .end local v7           #clientMac:[B
    .end local v10           #clientIp:Ljava/net/InetAddress;
    .end local v12           #nextIp:Ljava/net/InetAddress;
    .end local v13           #relayIp:Ljava/net/InetAddress;
    .end local v18           #dhcpMagicCookie:I
    .end local v42           #ipv4addr:[B
    :cond_120
    const/4 v4, 0x0

    #@121
    goto :goto_ca

    #@122
    .line 648
    .restart local v4       #broadcast:Z
    .restart local v42       #ipv4addr:[B
    :catch_122
    move-exception v23

    #@123
    .line 649
    .local v23, ex:Ljava/net/UnknownHostException;
    const/4 v2, 0x0

    #@124
    goto/16 :goto_4c

    #@126
    .line 666
    .end local v23           #ex:Ljava/net/UnknownHostException;
    .restart local v6       #yourIp:Ljava/net/InetAddress;
    .restart local v7       #clientMac:[B
    .restart local v10       #clientIp:Ljava/net/InetAddress;
    .restart local v12       #nextIp:Ljava/net/InetAddress;
    .restart local v13       #relayIp:Ljava/net/InetAddress;
    .restart local v18       #dhcpMagicCookie:I
    :cond_126
    const/16 v49, 0x1

    #@128
    .line 668
    .local v49, notFinishedOptions:Z
    :cond_128
    :goto_128
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->position()I

    #@12b
    move-result v8

    #@12c
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->limit()I

    #@12f
    move-result v9

    #@130
    if-ge v8, v9, :cond_1fa

    #@132
    if-eqz v49, :cond_1fa

    #@134
    .line 669
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@137
    move-result v51

    #@138
    .line 671
    .local v51, optionType:B
    const/4 v8, -0x1

    #@139
    move/from16 v0, v51

    #@13b
    if-ne v0, v8, :cond_140

    #@13d
    .line 672
    const/16 v49, 0x0

    #@13f
    goto :goto_128

    #@140
    .line 674
    :cond_140
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@143
    move-result v50

    #@144
    .line 675
    .local v50, optionLen:B
    const/16 v24, 0x0

    #@146
    .line 677
    .local v24, expectedLen:I
    sparse-switch v51, :sswitch_data_280

    #@149
    .line 742
    const/16 v30, 0x0

    #@14b
    .local v30, i:I
    :goto_14b
    move/from16 v0, v30

    #@14d
    move/from16 v1, v50

    #@14f
    if-ge v0, v1, :cond_15f

    #@151
    .line 743
    add-int/lit8 v24, v24, 0x1

    #@153
    .line 744
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@156
    .line 742
    add-int/lit8 v30, v30, 0x1

    #@158
    goto :goto_14b

    #@159
    .line 679
    .end local v30           #i:I
    :sswitch_159
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@15c
    move-result-object v48

    #@15d
    .line 680
    const/16 v24, 0x4

    #@15f
    .line 748
    :cond_15f
    :goto_15f
    move/from16 v0, v24

    #@161
    move/from16 v1, v50

    #@163
    if-eq v0, v1, :cond_128

    #@165
    .line 749
    const/4 v2, 0x0

    #@166
    goto/16 :goto_4c

    #@168
    .line 683
    :sswitch_168
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@16b
    move-result-object v26

    #@16c
    .line 684
    const/16 v24, 0x4

    #@16e
    .line 685
    goto :goto_15f

    #@16f
    .line 687
    :sswitch_16f
    const/16 v24, 0x0

    #@171
    .line 689
    const/16 v24, 0x0

    #@173
    :goto_173
    move/from16 v0, v24

    #@175
    move/from16 v1, v50

    #@177
    if-ge v0, v1, :cond_15f

    #@179
    .line 691
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@17c
    move-result-object v8

    #@17d
    move-object/from16 v0, v20

    #@17f
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@182
    .line 690
    add-int/lit8 v24, v24, 0x4

    #@184
    goto :goto_173

    #@185
    .line 695
    :sswitch_185
    move/from16 v24, v50

    #@187
    .line 696
    move-object/from16 v0, p0

    #@189
    move/from16 v1, v50

    #@18b
    invoke-static {v0, v1}, Landroid/net/dhcp/DhcpPacket;->readAsciiString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    #@18e
    move-result-object v28

    #@18f
    .line 697
    goto :goto_15f

    #@190
    .line 699
    :sswitch_190
    move/from16 v24, v50

    #@192
    .line 700
    move-object/from16 v0, p0

    #@194
    move/from16 v1, v50

    #@196
    invoke-static {v0, v1}, Landroid/net/dhcp/DhcpPacket;->readAsciiString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    #@199
    move-result-object v21

    #@19a
    .line 701
    goto :goto_15f

    #@19b
    .line 703
    :sswitch_19b
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@19e
    move-result-object v16

    #@19f
    .line 704
    const/16 v24, 0x4

    #@1a1
    .line 705
    goto :goto_15f

    #@1a2
    .line 707
    :sswitch_1a2
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@1a5
    move-result-object v52

    #@1a6
    .line 708
    const/16 v24, 0x4

    #@1a8
    .line 709
    goto :goto_15f

    #@1a9
    .line 711
    :sswitch_1a9
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    #@1ac
    move-result v8

    #@1ad
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b0
    move-result-object v46

    #@1b1
    .line 712
    const/16 v24, 0x4

    #@1b3
    .line 713
    goto :goto_15f

    #@1b4
    .line 715
    :sswitch_1b4
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@1b7
    move-result v19

    #@1b8
    .line 716
    const/16 v24, 0x1

    #@1ba
    .line 717
    goto :goto_15f

    #@1bb
    .line 719
    :sswitch_1bb
    invoke-static/range {p0 .. p0}, Landroid/net/dhcp/DhcpPacket;->readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;

    #@1be
    move-result-object v53

    #@1bf
    .line 720
    const/16 v24, 0x4

    #@1c1
    .line 721
    goto :goto_15f

    #@1c2
    .line 723
    :sswitch_1c2
    move/from16 v0, v50

    #@1c4
    new-array v0, v0, [B

    #@1c6
    move-object/from16 v25, v0

    #@1c8
    .line 724
    move-object/from16 v0, p0

    #@1ca
    move-object/from16 v1, v25

    #@1cc
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@1cf
    .line 725
    move/from16 v24, v50

    #@1d1
    .line 726
    goto :goto_15f

    #@1d2
    .line 728
    :sswitch_1d2
    move/from16 v24, v50

    #@1d4
    .line 729
    move-object/from16 v0, p0

    #@1d6
    move/from16 v1, v50

    #@1d8
    invoke-static {v0, v1}, Landroid/net/dhcp/DhcpPacket;->readAsciiString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    #@1db
    move-result-object v47

    #@1dc
    .line 730
    goto :goto_15f

    #@1dd
    .line 732
    :sswitch_1dd
    move/from16 v24, v50

    #@1df
    .line 733
    move-object/from16 v0, p0

    #@1e1
    move/from16 v1, v50

    #@1e3
    invoke-static {v0, v1}, Landroid/net/dhcp/DhcpPacket;->readAsciiString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    #@1e6
    move-result-object v59

    #@1e7
    .line 734
    goto/16 :goto_15f

    #@1e9
    .line 736
    :sswitch_1e9
    move/from16 v0, v50

    #@1eb
    new-array v0, v0, [B

    #@1ed
    move-object/from16 v31, v0

    #@1ef
    .line 737
    .local v31, id:[B
    move-object/from16 v0, p0

    #@1f1
    move-object/from16 v1, v31

    #@1f3
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@1f6
    .line 738
    move/from16 v24, v50

    #@1f8
    .line 739
    goto/16 :goto_15f

    #@1fa
    .line 756
    .end local v24           #expectedLen:I
    .end local v31           #id:[B
    .end local v50           #optionLen:B
    .end local v51           #optionType:B
    :cond_1fa
    packed-switch v19, :pswitch_data_2ba

    #@1fd
    .line 790
    :pswitch_1fd
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@1ff
    new-instance v9, Ljava/lang/StringBuilder;

    #@201
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@204
    const-string v11, "Unimplemented type: "

    #@206
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v9

    #@20a
    move/from16 v0, v19

    #@20c
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v9

    #@210
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@213
    move-result-object v9

    #@214
    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@217
    .line 791
    const/4 v2, 0x0

    #@218
    goto/16 :goto_4c

    #@21a
    .line 757
    :pswitch_21a
    const/4 v2, 0x0

    #@21b
    goto/16 :goto_4c

    #@21d
    .line 759
    :pswitch_21d
    new-instance v2, Landroid/net/dhcp/DhcpDiscoverPacket;

    #@21f
    invoke-direct {v2, v3, v7, v4}, Landroid/net/dhcp/DhcpDiscoverPacket;-><init>(I[BZ)V

    #@222
    .line 794
    .local v2, newPacket:Landroid/net/dhcp/DhcpPacket;
    :goto_222
    move-object/from16 v0, v16

    #@224
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mBroadcastAddress:Ljava/net/InetAddress;

    #@226
    .line 795
    move-object/from16 v0, v20

    #@228
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mDnsServers:Ljava/util/List;

    #@22a
    .line 796
    move-object/from16 v0, v21

    #@22c
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mDomainName:Ljava/lang/String;

    #@22e
    .line 797
    move-object/from16 v0, v26

    #@230
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mGateway:Ljava/net/InetAddress;

    #@232
    .line 798
    move-object/from16 v0, v28

    #@234
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mHostName:Ljava/lang/String;

    #@236
    .line 799
    move-object/from16 v0, v46

    #@238
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@23a
    .line 800
    move-object/from16 v0, v47

    #@23c
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mMessage:Ljava/lang/String;

    #@23e
    .line 801
    move-object/from16 v0, v52

    #@240
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@242
    .line 802
    move-object/from16 v0, v25

    #@244
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@246
    .line 803
    move-object/from16 v0, v53

    #@248
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@24a
    .line 804
    move-object/from16 v0, v48

    #@24c
    iput-object v0, v2, Landroid/net/dhcp/DhcpPacket;->mSubnetMask:Ljava/net/InetAddress;

    #@24e
    goto/16 :goto_4c

    #@250
    .line 763
    .end local v2           #newPacket:Landroid/net/dhcp/DhcpPacket;
    :pswitch_250
    new-instance v2, Landroid/net/dhcp/DhcpOfferPacket;

    #@252
    invoke-direct/range {v2 .. v7}, Landroid/net/dhcp/DhcpOfferPacket;-><init>(IZLjava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@255
    .line 765
    .restart local v2       #newPacket:Landroid/net/dhcp/DhcpPacket;
    goto :goto_222

    #@256
    .line 767
    .end local v2           #newPacket:Landroid/net/dhcp/DhcpPacket;
    :pswitch_256
    new-instance v2, Landroid/net/dhcp/DhcpRequestPacket;

    #@258
    invoke-direct {v2, v3, v10, v7, v4}, Landroid/net/dhcp/DhcpRequestPacket;-><init>(ILjava/net/InetAddress;[BZ)V

    #@25b
    .line 769
    .restart local v2       #newPacket:Landroid/net/dhcp/DhcpPacket;
    goto :goto_222

    #@25c
    .line 771
    .end local v2           #newPacket:Landroid/net/dhcp/DhcpPacket;
    :pswitch_25c
    new-instance v2, Landroid/net/dhcp/DhcpDeclinePacket;

    #@25e
    move-object v8, v2

    #@25f
    move v9, v3

    #@260
    move-object v11, v6

    #@261
    move-object v14, v7

    #@262
    invoke-direct/range {v8 .. v14}, Landroid/net/dhcp/DhcpDeclinePacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@265
    .line 774
    .restart local v2       #newPacket:Landroid/net/dhcp/DhcpPacket;
    goto :goto_222

    #@266
    .line 776
    .end local v2           #newPacket:Landroid/net/dhcp/DhcpPacket;
    :pswitch_266
    new-instance v2, Landroid/net/dhcp/DhcpAckPacket;

    #@268
    invoke-direct/range {v2 .. v7}, Landroid/net/dhcp/DhcpAckPacket;-><init>(IZLjava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@26b
    .line 778
    .restart local v2       #newPacket:Landroid/net/dhcp/DhcpPacket;
    goto :goto_222

    #@26c
    .line 780
    .end local v2           #newPacket:Landroid/net/dhcp/DhcpPacket;
    :pswitch_26c
    new-instance v2, Landroid/net/dhcp/DhcpNakPacket;

    #@26e
    move-object v8, v2

    #@26f
    move v9, v3

    #@270
    move-object v11, v6

    #@271
    move-object v14, v7

    #@272
    invoke-direct/range {v8 .. v14}, Landroid/net/dhcp/DhcpNakPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@275
    .line 783
    .restart local v2       #newPacket:Landroid/net/dhcp/DhcpPacket;
    goto :goto_222

    #@276
    .line 785
    .end local v2           #newPacket:Landroid/net/dhcp/DhcpPacket;
    :pswitch_276
    new-instance v2, Landroid/net/dhcp/DhcpInformPacket;

    #@278
    move-object v8, v2

    #@279
    move v9, v3

    #@27a
    move-object v11, v6

    #@27b
    move-object v14, v7

    #@27c
    invoke-direct/range {v8 .. v14}, Landroid/net/dhcp/DhcpInformPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[B)V

    #@27f
    .line 788
    .restart local v2       #newPacket:Landroid/net/dhcp/DhcpPacket;
    goto :goto_222

    #@280
    .line 677
    :sswitch_data_280
    .sparse-switch
        0x1 -> :sswitch_159
        0x3 -> :sswitch_168
        0x6 -> :sswitch_16f
        0xc -> :sswitch_185
        0xf -> :sswitch_190
        0x1c -> :sswitch_19b
        0x32 -> :sswitch_1a2
        0x33 -> :sswitch_1a9
        0x35 -> :sswitch_1b4
        0x36 -> :sswitch_1bb
        0x37 -> :sswitch_1c2
        0x38 -> :sswitch_1d2
        0x3c -> :sswitch_1dd
        0x3d -> :sswitch_1e9
    .end sparse-switch

    #@2ba
    .line 756
    :pswitch_data_2ba
    .packed-switch -0x1
        :pswitch_21a
        :pswitch_1fd
        :pswitch_21d
        :pswitch_250
        :pswitch_256
        :pswitch_25c
        :pswitch_266
        :pswitch_26c
        :pswitch_1fd
        :pswitch_276
    .end packed-switch
.end method

.method public static decodeFullPacket([BI)Landroid/net/dhcp/DhcpPacket;
    .registers 5
    .parameter "packet"
    .parameter "pktType"

    #@0
    .prologue
    .line 813
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v1

    #@4
    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@6
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@9
    move-result-object v0

    #@a
    .line 814
    .local v0, buffer:Ljava/nio/ByteBuffer;
    invoke-static {v0, p1}, Landroid/net/dhcp/DhcpPacket;->decodeFullPacket(Ljava/nio/ByteBuffer;I)Landroid/net/dhcp/DhcpPacket;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method private intAbs(S)I
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 363
    if-gez p1, :cond_7

    #@2
    .line 364
    const/high16 v1, 0x1

    #@4
    add-int v0, p1, v1

    #@6
    .line 367
    :goto_6
    return v0

    #@7
    :cond_7
    move v0, p1

    #@8
    goto :goto_6
.end method

.method public static macToString([B)Ljava/lang/String;
    .registers 6
    .parameter "mac"

    #@0
    .prologue
    .line 494
    const-string v2, ""

    #@2
    .line 496
    .local v2, macAddr:Ljava/lang/String;
    const/4 v1, 0x0

    #@3
    .local v1, i:I
    :goto_3
    array-length v3, p0

    #@4
    if-ge v1, v3, :cond_55

    #@6
    .line 497
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "0"

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    aget-byte v4, p0, v1

    #@13
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 501
    .local v0, hexString:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@2b
    move-result v4

    #@2c
    add-int/lit8 v4, v4, -0x2

    #@2e
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    .line 503
    array-length v3, p0

    #@3b
    add-int/lit8 v3, v3, -0x1

    #@3d
    if-eq v1, v3, :cond_52

    #@3f
    .line 504
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    const-string v4, ":"

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    .line 496
    :cond_52
    add-int/lit8 v1, v1, 0x1

    #@54
    goto :goto_3

    #@55
    .line 508
    .end local v0           #hexString:Ljava/lang/String;
    :cond_55
    return-object v2
.end method

.method private static readAsciiString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    .registers 7
    .parameter "buf"
    .parameter "byteCount"

    #@0
    .prologue
    .line 541
    new-array v0, p1, [B

    #@2
    .line 542
    .local v0, bytes:[B
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@5
    .line 543
    new-instance v1, Ljava/lang/String;

    #@7
    const/4 v2, 0x0

    #@8
    array-length v3, v0

    #@9
    sget-object v4, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@b
    invoke-direct {v1, v0, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@e
    return-object v1
.end method

.method private static readIpAddress(Ljava/nio/ByteBuffer;)Ljava/net/InetAddress;
    .registers 5
    .parameter "packet"

    #@0
    .prologue
    .line 522
    const/4 v2, 0x0

    #@1
    .line 523
    .local v2, result:Ljava/net/InetAddress;
    const/4 v3, 0x4

    #@2
    new-array v1, v3, [B

    #@4
    .line 524
    .local v1, ipAddr:[B
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@7
    .line 527
    :try_start_7
    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_a
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result-object v2

    #@b
    .line 534
    :goto_b
    return-object v2

    #@c
    .line 528
    :catch_c
    move-exception v0

    #@d
    .line 531
    .local v0, ex:Ljava/net/UnknownHostException;
    const/4 v2, 0x0

    #@e
    goto :goto_b
.end method


# virtual methods
.method protected addTlv(Ljava/nio/ByteBuffer;BB)V
    .registers 5
    .parameter "buf"
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 419
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@3
    .line 420
    const/4 v0, 0x1

    #@4
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@7
    .line 421
    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 422
    return-void
.end method

.method protected addTlv(Ljava/nio/ByteBuffer;BLjava/lang/Integer;)V
    .registers 5
    .parameter "buf"
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 462
    if-eqz p3, :cond_10

    #@2
    .line 463
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 464
    const/4 v0, 0x4

    #@6
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@9
    .line 465
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    #@c
    move-result v0

    #@d
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@10
    .line 467
    :cond_10
    return-void
.end method

.method protected addTlv(Ljava/nio/ByteBuffer;BLjava/lang/String;)V
    .registers 6
    .parameter "buf"
    .parameter "type"
    .parameter "str"

    #@0
    .prologue
    .line 473
    if-eqz p3, :cond_1f

    #@2
    .line 474
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 475
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@8
    move-result v1

    #@9
    int-to-byte v1, v1

    #@a
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@d
    .line 477
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@11
    move-result v1

    #@12
    if-ge v0, v1, :cond_1f

    #@14
    .line 478
    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v1

    #@18
    int-to-byte v1, v1

    #@19
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1c
    .line 477
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_e

    #@1f
    .line 481
    .end local v0           #i:I
    :cond_1f
    return-void
.end method

.method protected addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V
    .registers 5
    .parameter "buf"
    .parameter "type"
    .parameter "addr"

    #@0
    .prologue
    .line 439
    if-eqz p3, :cond_9

    #@2
    .line 440
    invoke-virtual {p3}, Ljava/net/InetAddress;->getAddress()[B

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, p1, p2, v0}, Landroid/net/dhcp/DhcpPacket;->addTlv(Ljava/nio/ByteBuffer;B[B)V

    #@9
    .line 442
    :cond_9
    return-void
.end method

.method protected addTlv(Ljava/nio/ByteBuffer;BLjava/util/List;)V
    .registers 7
    .parameter "buf"
    .parameter "type"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "B",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 448
    .local p3, addrs:Ljava/util/List;,"Ljava/util/List<Ljava/net/InetAddress;>;"
    if-eqz p3, :cond_2d

    #@2
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_2d

    #@8
    .line 449
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@b
    .line 450
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@e
    move-result v2

    #@f
    mul-int/lit8 v2, v2, 0x4

    #@11
    int-to-byte v2, v2

    #@12
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@15
    .line 452
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v1

    #@19
    .local v1, i$:Ljava/util/Iterator;
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_2d

    #@1f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Ljava/net/InetAddress;

    #@25
    .line 453
    .local v0, addr:Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@28
    move-result-object v2

    #@29
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@2c
    goto :goto_19

    #@2d
    .line 456
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_2d
    return-void
.end method

.method protected addTlv(Ljava/nio/ByteBuffer;B[B)V
    .registers 5
    .parameter "buf"
    .parameter "type"
    .parameter "payload"

    #@0
    .prologue
    .line 428
    if-eqz p3, :cond_d

    #@2
    .line 429
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 430
    array-length v0, p3

    #@6
    int-to-byte v0, v0

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 431
    invoke-virtual {p1, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@d
    .line 433
    :cond_d
    return-void
.end method

.method protected addTlvEnd(Ljava/nio/ByteBuffer;)V
    .registers 3
    .parameter "buf"

    #@0
    .prologue
    .line 487
    const/4 v0, -0x1

    #@1
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@4
    .line 488
    return-void
.end method

.method public abstract buildPacket(ISS)Ljava/nio/ByteBuffer;
.end method

.method public abstract doNextOp(Landroid/net/dhcp/DhcpStateMachine;)V
.end method

.method protected fillInPacket(ILjava/net/InetAddress;Ljava/net/InetAddress;SSLjava/nio/ByteBuffer;BZ)V
    .registers 23
    .parameter "encap"
    .parameter "destIp"
    .parameter "srcIp"
    .parameter "destUdp"
    .parameter "srcUdp"
    .parameter "buf"
    .parameter "requestCode"
    .parameter "broadcast"

    #@0
    .prologue
    .line 253
    invoke-virtual/range {p2 .. p2}, Ljava/net/InetAddress;->getAddress()[B

    #@3
    move-result-object v2

    #@4
    .line 254
    .local v2, destIpArray:[B
    invoke-virtual/range {p3 .. p3}, Ljava/net/InetAddress;->getAddress()[B

    #@7
    move-result-object v6

    #@8
    .line 255
    .local v6, srcIpArray:[B
    const/4 v5, 0x0

    #@9
    .line 256
    .local v5, ipLengthOffset:I
    const/4 v4, 0x0

    #@a
    .line 257
    .local v4, ipChecksumOffset:I
    const/4 v3, 0x0

    #@b
    .line 258
    .local v3, endIpHeader:I
    const/4 v8, 0x0

    #@c
    .line 259
    .local v8, udpHeaderOffset:I
    const/4 v10, 0x0

    #@d
    .line 260
    .local v10, udpLengthOffset:I
    const/4 v7, 0x0

    #@e
    .line 262
    .local v7, udpChecksumOffset:I
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@11
    .line 263
    sget-object v12, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@13
    move-object/from16 v0, p6

    #@15
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@18
    .line 268
    const/4 v12, 0x1

    #@19
    if-ne p1, v12, :cond_8c

    #@1b
    .line 270
    const/16 v12, 0x45

    #@1d
    move-object/from16 v0, p6

    #@1f
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@22
    .line 271
    const/16 v12, 0x10

    #@24
    move-object/from16 v0, p6

    #@26
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@29
    .line 272
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@2c
    move-result v5

    #@2d
    .line 273
    const/4 v12, 0x0

    #@2e
    move-object/from16 v0, p6

    #@30
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@33
    .line 274
    const/4 v12, 0x0

    #@34
    move-object/from16 v0, p6

    #@36
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@39
    .line 275
    const/16 v12, 0x4000

    #@3b
    move-object/from16 v0, p6

    #@3d
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@40
    .line 276
    const/16 v12, 0x40

    #@42
    move-object/from16 v0, p6

    #@44
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@47
    .line 277
    const/16 v12, 0x11

    #@49
    move-object/from16 v0, p6

    #@4b
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@4e
    .line 278
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@51
    move-result v4

    #@52
    .line 279
    const/4 v12, 0x0

    #@53
    move-object/from16 v0, p6

    #@55
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@58
    .line 281
    move-object/from16 v0, p6

    #@5a
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@5d
    .line 282
    move-object/from16 v0, p6

    #@5f
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@62
    .line 283
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@65
    move-result v3

    #@66
    .line 286
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@69
    move-result v8

    #@6a
    .line 287
    move-object/from16 v0, p6

    #@6c
    move/from16 v1, p5

    #@6e
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@71
    .line 288
    move-object/from16 v0, p6

    #@73
    move/from16 v1, p4

    #@75
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@78
    .line 289
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@7b
    move-result v10

    #@7c
    .line 290
    const/4 v12, 0x0

    #@7d
    move-object/from16 v0, p6

    #@7f
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@82
    .line 291
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@85
    move-result v7

    #@86
    .line 292
    const/4 v12, 0x0

    #@87
    move-object/from16 v0, p6

    #@89
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@8c
    .line 296
    :cond_8c
    invoke-virtual/range {p6 .. p7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@8f
    .line 297
    const/4 v12, 0x1

    #@90
    move-object/from16 v0, p6

    #@92
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@95
    .line 298
    iget-object v12, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@97
    array-length v12, v12

    #@98
    int-to-byte v12, v12

    #@99
    move-object/from16 v0, p6

    #@9b
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@9e
    .line 299
    const/4 v12, 0x0

    #@9f
    move-object/from16 v0, p6

    #@a1
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a4
    .line 300
    iget v12, p0, Landroid/net/dhcp/DhcpPacket;->mTransId:I

    #@a6
    move-object/from16 v0, p6

    #@a8
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@ab
    .line 301
    const/4 v12, 0x0

    #@ac
    move-object/from16 v0, p6

    #@ae
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@b1
    .line 303
    if-eqz p8, :cond_18b

    #@b3
    .line 304
    const/16 v12, -0x8000

    #@b5
    move-object/from16 v0, p6

    #@b7
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@ba
    .line 309
    :goto_ba
    iget-object v12, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@bc
    invoke-virtual {v12}, Ljava/net/InetAddress;->getAddress()[B

    #@bf
    move-result-object v12

    #@c0
    move-object/from16 v0, p6

    #@c2
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@c5
    .line 310
    iget-object v12, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@c7
    invoke-virtual {v12}, Ljava/net/InetAddress;->getAddress()[B

    #@ca
    move-result-object v12

    #@cb
    move-object/from16 v0, p6

    #@cd
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@d0
    .line 311
    iget-object v12, p0, Landroid/net/dhcp/DhcpPacket;->mNextIp:Ljava/net/InetAddress;

    #@d2
    invoke-virtual {v12}, Ljava/net/InetAddress;->getAddress()[B

    #@d5
    move-result-object v12

    #@d6
    move-object/from16 v0, p6

    #@d8
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@db
    .line 312
    iget-object v12, p0, Landroid/net/dhcp/DhcpPacket;->mRelayIp:Ljava/net/InetAddress;

    #@dd
    invoke-virtual {v12}, Ljava/net/InetAddress;->getAddress()[B

    #@e0
    move-result-object v12

    #@e1
    move-object/from16 v0, p6

    #@e3
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@e6
    .line 313
    iget-object v12, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@e8
    move-object/from16 v0, p6

    #@ea
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@ed
    .line 314
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@f0
    move-result v12

    #@f1
    iget-object v13, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@f3
    array-length v13, v13

    #@f4
    rsub-int/lit8 v13, v13, 0x10

    #@f6
    add-int/2addr v12, v13

    #@f7
    add-int/lit8 v12, v12, 0x40

    #@f9
    add-int/lit16 v12, v12, 0x80

    #@fb
    move-object/from16 v0, p6

    #@fd
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@100
    .line 318
    const v12, 0x63825363

    #@103
    move-object/from16 v0, p6

    #@105
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@108
    .line 319
    move-object/from16 v0, p6

    #@10a
    invoke-virtual {p0, v0}, Landroid/net/dhcp/DhcpPacket;->finishPacket(Ljava/nio/ByteBuffer;)V

    #@10d
    .line 322
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@110
    move-result v12

    #@111
    and-int/lit8 v12, v12, 0x1

    #@113
    const/4 v13, 0x1

    #@114
    if-ne v12, v13, :cond_11c

    #@116
    .line 323
    const/4 v12, 0x0

    #@117
    move-object/from16 v0, p6

    #@119
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@11c
    .line 328
    :cond_11c
    const/4 v12, 0x1

    #@11d
    if-ne p1, v12, :cond_18a

    #@11f
    .line 330
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@122
    move-result v12

    #@123
    sub-int/2addr v12, v8

    #@124
    int-to-short v9, v12

    #@125
    .line 331
    .local v9, udpLen:S
    move-object/from16 v0, p6

    #@127
    invoke-virtual {v0, v10, v9}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    #@12a
    .line 334
    const/4 v11, 0x0

    #@12b
    .line 338
    .local v11, udpSeed:I
    add-int/lit8 v12, v4, 0x2

    #@12d
    move-object/from16 v0, p6

    #@12f
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->getShort(I)S

    #@132
    move-result v12

    #@133
    invoke-direct {p0, v12}, Landroid/net/dhcp/DhcpPacket;->intAbs(S)I

    #@136
    move-result v12

    #@137
    add-int/2addr v11, v12

    #@138
    .line 339
    add-int/lit8 v12, v4, 0x4

    #@13a
    move-object/from16 v0, p6

    #@13c
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->getShort(I)S

    #@13f
    move-result v12

    #@140
    invoke-direct {p0, v12}, Landroid/net/dhcp/DhcpPacket;->intAbs(S)I

    #@143
    move-result v12

    #@144
    add-int/2addr v11, v12

    #@145
    .line 340
    add-int/lit8 v12, v4, 0x6

    #@147
    move-object/from16 v0, p6

    #@149
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->getShort(I)S

    #@14c
    move-result v12

    #@14d
    invoke-direct {p0, v12}, Landroid/net/dhcp/DhcpPacket;->intAbs(S)I

    #@150
    move-result v12

    #@151
    add-int/2addr v11, v12

    #@152
    .line 341
    add-int/lit8 v12, v4, 0x8

    #@154
    move-object/from16 v0, p6

    #@156
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->getShort(I)S

    #@159
    move-result v12

    #@15a
    invoke-direct {p0, v12}, Landroid/net/dhcp/DhcpPacket;->intAbs(S)I

    #@15d
    move-result v12

    #@15e
    add-int/2addr v11, v12

    #@15f
    .line 344
    add-int/lit8 v11, v11, 0x11

    #@161
    .line 345
    add-int/2addr v11, v9

    #@162
    .line 347
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@165
    move-result v12

    #@166
    move-object/from16 v0, p6

    #@168
    invoke-direct {p0, v0, v11, v8, v12}, Landroid/net/dhcp/DhcpPacket;->checksum(Ljava/nio/ByteBuffer;III)I

    #@16b
    move-result v12

    #@16c
    int-to-short v12, v12

    #@16d
    move-object/from16 v0, p6

    #@16f
    invoke-virtual {v0, v7, v12}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    #@172
    .line 351
    invoke-virtual/range {p6 .. p6}, Ljava/nio/ByteBuffer;->position()I

    #@175
    move-result v12

    #@176
    int-to-short v12, v12

    #@177
    move-object/from16 v0, p6

    #@179
    invoke-virtual {v0, v5, v12}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    #@17c
    .line 353
    const/4 v12, 0x0

    #@17d
    const/4 v13, 0x0

    #@17e
    move-object/from16 v0, p6

    #@180
    invoke-direct {p0, v0, v12, v13, v3}, Landroid/net/dhcp/DhcpPacket;->checksum(Ljava/nio/ByteBuffer;III)I

    #@183
    move-result v12

    #@184
    int-to-short v12, v12

    #@185
    move-object/from16 v0, p6

    #@187
    invoke-virtual {v0, v4, v12}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    #@18a
    .line 356
    .end local v9           #udpLen:S
    .end local v11           #udpSeed:I
    :cond_18a
    return-void

    #@18b
    .line 306
    :cond_18b
    const/4 v12, 0x0

    #@18c
    move-object/from16 v0, p6

    #@18e
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@191
    goto/16 :goto_ba
.end method

.method abstract finishPacket(Ljava/nio/ByteBuffer;)V
.end method

.method public getTransactionId()I
    .registers 2

    #@0
    .prologue
    .line 242
    iget v0, p0, Landroid/net/dhcp/DhcpPacket;->mTransId:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 512
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@2
    invoke-static {v1}, Landroid/net/dhcp/DhcpPacket;->macToString([B)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 514
    .local v0, macAddr:Ljava/lang/String;
    return-object v0
.end method
