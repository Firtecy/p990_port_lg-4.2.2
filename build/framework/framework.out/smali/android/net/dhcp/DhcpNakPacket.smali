.class Landroid/net/dhcp/DhcpNakPacket;
.super Landroid/net/dhcp/DhcpPacket;
.source "DhcpNakPacket.java"


# direct methods
.method constructor <init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[B)V
    .registers 15
    .parameter "transId"
    .parameter "clientIp"
    .parameter "yourIp"
    .parameter "nextIp"
    .parameter "relayIp"
    .parameter "clientMac"

    #@0
    .prologue
    .line 33
    sget-object v2, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@2
    sget-object v3, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@4
    const/4 v7, 0x0

    #@5
    move-object v0, p0

    #@6
    move v1, p1

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    move-object v6, p6

    #@a
    invoke-direct/range {v0 .. v7}, Landroid/net/dhcp/DhcpPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[BZ)V

    #@d
    .line 35
    return-void
.end method


# virtual methods
.method public buildPacket(ISS)Ljava/nio/ByteBuffer;
    .registers 13
    .parameter "encap"
    .parameter "destUdp"
    .parameter "srcUdp"

    #@0
    .prologue
    .line 46
    const/16 v0, 0x5dc

    #@2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v6

    #@6
    .line 47
    .local v6, result:Ljava/nio/ByteBuffer;
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@8
    .line 48
    .local v2, destIp:Ljava/net/InetAddress;
    iget-object v3, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@a
    .line 50
    .local v3, srcIp:Ljava/net/InetAddress;
    const/4 v7, 0x2

    #@b
    iget-boolean v8, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@d
    move-object v0, p0

    #@e
    move v1, p1

    #@f
    move v4, p2

    #@10
    move v5, p3

    #@11
    invoke-virtual/range {v0 .. v8}, Landroid/net/dhcp/DhcpNakPacket;->fillInPacket(ILjava/net/InetAddress;Ljava/net/InetAddress;SSLjava/nio/ByteBuffer;BZ)V

    #@14
    .line 52
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@17
    .line 53
    return-object v6
.end method

.method public doNextOp(Landroid/net/dhcp/DhcpStateMachine;)V
    .registers 2
    .parameter "machine"

    #@0
    .prologue
    .line 70
    invoke-interface {p1}, Landroid/net/dhcp/DhcpStateMachine;->onNakReceived()V

    #@3
    .line 71
    return-void
.end method

.method finishPacket(Ljava/nio/ByteBuffer;)V
    .registers 4
    .parameter "buffer"

    #@0
    .prologue
    .line 60
    const/16 v0, 0x35

    #@2
    const/4 v1, 0x6

    #@3
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpNakPacket;->addTlv(Ljava/nio/ByteBuffer;BB)V

    #@6
    .line 61
    const/16 v0, 0x36

    #@8
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@a
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpNakPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@d
    .line 62
    const/16 v0, 0x38

    #@f
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mMessage:Ljava/lang/String;

    #@11
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpNakPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/lang/String;)V

    #@14
    .line 63
    invoke-virtual {p0, p1}, Landroid/net/dhcp/DhcpNakPacket;->addTlvEnd(Ljava/nio/ByteBuffer;)V

    #@17
    .line 64
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 38
    invoke-super {p0}, Landroid/net/dhcp/DhcpPacket;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 39
    .local v0, s:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " NAK, reason "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mMessage:Ljava/lang/String;

    #@15
    if-nez v1, :cond_22

    #@17
    const-string v1, "(none)"

    #@19
    :goto_19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    return-object v1

    #@22
    :cond_22
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mMessage:Ljava/lang/String;

    #@24
    goto :goto_19
.end method
