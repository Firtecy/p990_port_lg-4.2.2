.class Landroid/net/dhcp/DhcpInformPacket;
.super Landroid/net/dhcp/DhcpPacket;
.source "DhcpInformPacket.java"


# direct methods
.method constructor <init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[B)V
    .registers 15
    .parameter "transId"
    .parameter "clientIp"
    .parameter "yourIp"
    .parameter "nextIp"
    .parameter "relayIp"
    .parameter "clientMac"

    #@0
    .prologue
    .line 32
    const/4 v7, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    move-object v6, p6

    #@8
    invoke-direct/range {v0 .. v7}, Landroid/net/dhcp/DhcpPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[BZ)V

    #@b
    .line 33
    return-void
.end method


# virtual methods
.method public buildPacket(ISS)Ljava/nio/ByteBuffer;
    .registers 13
    .parameter "encap"
    .parameter "destUdp"
    .parameter "srcUdp"

    #@0
    .prologue
    .line 44
    const/16 v0, 0x5dc

    #@2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v6

    #@6
    .line 46
    .local v6, result:Ljava/nio/ByteBuffer;
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@8
    iget-object v3, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@a
    const/4 v7, 0x1

    #@b
    const/4 v8, 0x0

    #@c
    move-object v0, p0

    #@d
    move v1, p1

    #@e
    move v4, p2

    #@f
    move v5, p3

    #@10
    invoke-virtual/range {v0 .. v8}, Landroid/net/dhcp/DhcpInformPacket;->fillInPacket(ILjava/net/InetAddress;Ljava/net/InetAddress;SSLjava/nio/ByteBuffer;BZ)V

    #@13
    .line 48
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@16
    .line 49
    return-object v6
.end method

.method public doNextOp(Landroid/net/dhcp/DhcpStateMachine;)V
    .registers 6
    .parameter "machine"

    #@0
    .prologue
    .line 71
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@2
    if-nez v1, :cond_10

    #@4
    iget-object v0, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@6
    .line 73
    .local v0, clientRequest:Ljava/net/InetAddress;
    :goto_6
    iget v1, p0, Landroid/net/dhcp/DhcpPacket;->mTransId:I

    #@8
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@a
    iget-object v3, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@c
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/net/dhcp/DhcpStateMachine;->onInformReceived(I[BLjava/net/InetAddress;[B)V

    #@f
    .line 75
    return-void

    #@10
    .line 71
    .end local v0           #clientRequest:Ljava/net/InetAddress;
    :cond_10
    iget-object v0, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@12
    goto :goto_6
.end method

.method finishPacket(Ljava/nio/ByteBuffer;)V
    .registers 7
    .parameter "buffer"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 56
    const/4 v1, 0x7

    #@3
    new-array v0, v1, [B

    #@5
    .line 58
    .local v0, clientId:[B
    aput-byte v4, v0, v3

    #@7
    .line 59
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@9
    const/4 v2, 0x6

    #@a
    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d
    .line 61
    const/16 v1, 0x35

    #@f
    const/4 v2, 0x3

    #@10
    invoke-virtual {p0, p1, v1, v2}, Landroid/net/dhcp/DhcpInformPacket;->addTlv(Ljava/nio/ByteBuffer;BB)V

    #@13
    .line 62
    const/16 v1, 0x37

    #@15
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@17
    invoke-virtual {p0, p1, v1, v2}, Landroid/net/dhcp/DhcpInformPacket;->addTlv(Ljava/nio/ByteBuffer;B[B)V

    #@1a
    .line 63
    invoke-virtual {p0, p1}, Landroid/net/dhcp/DhcpInformPacket;->addTlvEnd(Ljava/nio/ByteBuffer;)V

    #@1d
    .line 64
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 36
    invoke-super {p0}, Landroid/net/dhcp/DhcpPacket;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 37
    .local v0, s:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " INFORM"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    return-object v1
.end method
