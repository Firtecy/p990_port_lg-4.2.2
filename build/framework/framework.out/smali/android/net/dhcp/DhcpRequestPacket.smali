.class Landroid/net/dhcp/DhcpRequestPacket;
.super Landroid/net/dhcp/DhcpPacket;
.source "DhcpRequestPacket.java"


# direct methods
.method constructor <init>(ILjava/net/InetAddress;[BZ)V
    .registers 13
    .parameter "transId"
    .parameter "clientIp"
    .parameter "clientMac"
    .parameter "broadcast"

    #@0
    .prologue
    .line 34
    sget-object v3, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@2
    sget-object v4, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@4
    sget-object v5, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@6
    move-object v0, p0

    #@7
    move v1, p1

    #@8
    move-object v2, p2

    #@9
    move-object v6, p3

    #@a
    move v7, p4

    #@b
    invoke-direct/range {v0 .. v7}, Landroid/net/dhcp/DhcpPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[BZ)V

    #@e
    .line 36
    return-void
.end method


# virtual methods
.method public buildPacket(ISS)Ljava/nio/ByteBuffer;
    .registers 13
    .parameter "encap"
    .parameter "destUdp"
    .parameter "srcUdp"

    #@0
    .prologue
    .line 49
    const/16 v0, 0x5dc

    #@2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v6

    #@6
    .line 51
    .local v6, result:Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/net/Inet4Address;->ALL:Ljava/net/InetAddress;

    #@8
    sget-object v3, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@a
    const/4 v7, 0x1

    #@b
    iget-boolean v8, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@d
    move-object v0, p0

    #@e
    move v1, p1

    #@f
    move v4, p2

    #@10
    move v5, p3

    #@11
    invoke-virtual/range {v0 .. v8}, Landroid/net/dhcp/DhcpRequestPacket;->fillInPacket(ILjava/net/InetAddress;Ljava/net/InetAddress;SSLjava/nio/ByteBuffer;BZ)V

    #@14
    .line 53
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@17
    .line 54
    return-object v6
.end method

.method public doNextOp(Landroid/net/dhcp/DhcpStateMachine;)V
    .registers 9
    .parameter "machine"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@2
    if-nez v0, :cond_3c

    #@4
    iget-object v4, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@6
    .line 81
    .local v4, clientRequest:Ljava/net/InetAddress;
    :goto_6
    const-string v0, "DhcpPacket"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v2, "requested IP is "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " and client IP is "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mClientIp:Ljava/net/InetAddress;

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 83
    iget-boolean v1, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@2f
    iget v2, p0, Landroid/net/dhcp/DhcpPacket;->mTransId:I

    #@31
    iget-object v3, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@33
    iget-object v5, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@35
    iget-object v6, p0, Landroid/net/dhcp/DhcpPacket;->mHostName:Ljava/lang/String;

    #@37
    move-object v0, p1

    #@38
    invoke-interface/range {v0 .. v6}, Landroid/net/dhcp/DhcpStateMachine;->onRequestReceived(ZI[BLjava/net/InetAddress;[BLjava/lang/String;)V

    #@3b
    .line 85
    return-void

    #@3c
    .line 79
    .end local v4           #clientRequest:Ljava/net/InetAddress;
    :cond_3c
    iget-object v4, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@3e
    goto :goto_6
.end method

.method finishPacket(Ljava/nio/ByteBuffer;)V
    .registers 7
    .parameter "buffer"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 61
    const/4 v1, 0x7

    #@3
    new-array v0, v1, [B

    #@5
    .line 64
    .local v0, clientId:[B
    aput-byte v4, v0, v3

    #@7
    .line 65
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mClientMac:[B

    #@9
    const/4 v2, 0x6

    #@a
    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d
    .line 67
    const/16 v1, 0x35

    #@f
    const/4 v2, 0x3

    #@10
    invoke-virtual {p0, p1, v1, v2}, Landroid/net/dhcp/DhcpRequestPacket;->addTlv(Ljava/nio/ByteBuffer;BB)V

    #@13
    .line 68
    const/16 v1, 0x37

    #@15
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@17
    invoke-virtual {p0, p1, v1, v2}, Landroid/net/dhcp/DhcpRequestPacket;->addTlv(Ljava/nio/ByteBuffer;B[B)V

    #@1a
    .line 69
    const/16 v1, 0x32

    #@1c
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@1e
    invoke-virtual {p0, p1, v1, v2}, Landroid/net/dhcp/DhcpRequestPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@21
    .line 70
    const/16 v1, 0x36

    #@23
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@25
    invoke-virtual {p0, p1, v1, v2}, Landroid/net/dhcp/DhcpRequestPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@28
    .line 71
    const/16 v1, 0x3d

    #@2a
    invoke-virtual {p0, p1, v1, v0}, Landroid/net/dhcp/DhcpRequestPacket;->addTlv(Ljava/nio/ByteBuffer;B[B)V

    #@2d
    .line 72
    invoke-virtual {p0, p1}, Landroid/net/dhcp/DhcpRequestPacket;->addTlvEnd(Ljava/nio/ByteBuffer;)V

    #@30
    .line 73
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 39
    invoke-super {p0}, Landroid/net/dhcp/DhcpPacket;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 40
    .local v0, s:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " REQUEST, desired IP "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedIp:Ljava/net/InetAddress;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " from host \'"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mHostName:Ljava/lang/String;

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, "\', param list length "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@2d
    if-nez v1, :cond_39

    #@2f
    const/4 v1, 0x0

    #@30
    :goto_30
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    return-object v1

    #@39
    :cond_39
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mRequestedParams:[B

    #@3b
    array-length v1, v1

    #@3c
    goto :goto_30
.end method
