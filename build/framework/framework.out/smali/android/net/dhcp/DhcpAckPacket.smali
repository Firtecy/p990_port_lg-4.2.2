.class Landroid/net/dhcp/DhcpAckPacket;
.super Landroid/net/dhcp/DhcpPacket;
.source "DhcpAckPacket.java"


# instance fields
.field private final mSrcIp:Ljava/net/InetAddress;


# direct methods
.method constructor <init>(IZLjava/net/InetAddress;Ljava/net/InetAddress;[B)V
    .registers 14
    .parameter "transId"
    .parameter "broadcast"
    .parameter "serverAddress"
    .parameter "clientIp"
    .parameter "clientMac"

    #@0
    .prologue
    .line 36
    sget-object v2, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@2
    sget-object v5, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@4
    move-object v0, p0

    #@5
    move v1, p1

    #@6
    move-object v3, p4

    #@7
    move-object v4, p3

    #@8
    move-object v6, p5

    #@9
    move v7, p2

    #@a
    invoke-direct/range {v0 .. v7}, Landroid/net/dhcp/DhcpPacket;-><init>(ILjava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;[BZ)V

    #@d
    .line 38
    iput-boolean p2, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@f
    .line 39
    iput-object p3, p0, Landroid/net/dhcp/DhcpAckPacket;->mSrcIp:Ljava/net/InetAddress;

    #@11
    .line 40
    return-void
.end method

.method private static final getInt(Ljava/lang/Integer;)I
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 96
    if-nez p0, :cond_4

    #@2
    .line 97
    const/4 v0, 0x0

    #@3
    .line 99
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    #@7
    move-result v0

    #@8
    goto :goto_3
.end method


# virtual methods
.method public buildPacket(ISS)Ljava/nio/ByteBuffer;
    .registers 13
    .parameter "encap"
    .parameter "destUdp"
    .parameter "srcUdp"

    #@0
    .prologue
    .line 60
    const/16 v0, 0x5dc

    #@2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v6

    #@6
    .line 61
    .local v6, result:Ljava/nio/ByteBuffer;
    iget-boolean v0, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@8
    if-eqz v0, :cond_20

    #@a
    sget-object v2, Ljava/net/Inet4Address;->ALL:Ljava/net/InetAddress;

    #@c
    .line 62
    .local v2, destIp:Ljava/net/InetAddress;
    :goto_c
    iget-boolean v0, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@e
    if-eqz v0, :cond_23

    #@10
    sget-object v3, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@12
    .line 64
    .local v3, srcIp:Ljava/net/InetAddress;
    :goto_12
    const/4 v7, 0x2

    #@13
    iget-boolean v8, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcast:Z

    #@15
    move-object v0, p0

    #@16
    move v1, p1

    #@17
    move v4, p2

    #@18
    move v5, p3

    #@19
    invoke-virtual/range {v0 .. v8}, Landroid/net/dhcp/DhcpAckPacket;->fillInPacket(ILjava/net/InetAddress;Ljava/net/InetAddress;SSLjava/nio/ByteBuffer;BZ)V

    #@1c
    .line 66
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@1f
    .line 67
    return-object v6

    #@20
    .line 61
    .end local v2           #destIp:Ljava/net/InetAddress;
    .end local v3           #srcIp:Ljava/net/InetAddress;
    :cond_20
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@22
    goto :goto_c

    #@23
    .line 62
    .restart local v2       #destIp:Ljava/net/InetAddress;
    :cond_23
    iget-object v3, p0, Landroid/net/dhcp/DhcpAckPacket;->mSrcIp:Ljava/net/InetAddress;

    #@25
    goto :goto_12
.end method

.method public doNextOp(Landroid/net/dhcp/DhcpStateMachine;)V
    .registers 9
    .parameter "machine"

    #@0
    .prologue
    .line 107
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@2
    iget-object v2, p0, Landroid/net/dhcp/DhcpPacket;->mSubnetMask:Ljava/net/InetAddress;

    #@4
    iget-object v3, p0, Landroid/net/dhcp/DhcpPacket;->mGateway:Ljava/net/InetAddress;

    #@6
    iget-object v4, p0, Landroid/net/dhcp/DhcpPacket;->mDnsServers:Ljava/util/List;

    #@8
    iget-object v5, p0, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@a
    iget-object v0, p0, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@c
    invoke-static {v0}, Landroid/net/dhcp/DhcpAckPacket;->getInt(Ljava/lang/Integer;)I

    #@f
    move-result v6

    #@10
    move-object v0, p1

    #@11
    invoke-interface/range {v0 .. v6}, Landroid/net/dhcp/DhcpStateMachine;->onAckReceived(Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/net/InetAddress;Ljava/util/List;Ljava/net/InetAddress;I)V

    #@14
    .line 109
    return-void
.end method

.method finishPacket(Ljava/nio/ByteBuffer;)V
    .registers 4
    .parameter "buffer"

    #@0
    .prologue
    .line 74
    const/16 v0, 0x35

    #@2
    const/4 v1, 0x5

    #@3
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BB)V

    #@6
    .line 75
    const/16 v0, 0x36

    #@8
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mServerIdentifier:Ljava/net/InetAddress;

    #@a
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@d
    .line 76
    const/16 v0, 0x33

    #@f
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@11
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/lang/Integer;)V

    #@14
    .line 79
    iget-object v0, p0, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@16
    if-eqz v0, :cond_29

    #@18
    .line 80
    const/16 v0, 0x3a

    #@1a
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@1c
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@1f
    move-result v1

    #@20
    div-int/lit8 v1, v1, 0x2

    #@22
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/lang/Integer;)V

    #@29
    .line 84
    :cond_29
    const/4 v0, 0x1

    #@2a
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mSubnetMask:Ljava/net/InetAddress;

    #@2c
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@2f
    .line 85
    const/4 v0, 0x3

    #@30
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mGateway:Ljava/net/InetAddress;

    #@32
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@35
    .line 86
    const/16 v0, 0xf

    #@37
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mDomainName:Ljava/lang/String;

    #@39
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/lang/String;)V

    #@3c
    .line 87
    const/16 v0, 0x1c

    #@3e
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mBroadcastAddress:Ljava/net/InetAddress;

    #@40
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/net/InetAddress;)V

    #@43
    .line 88
    const/4 v0, 0x6

    #@44
    iget-object v1, p0, Landroid/net/dhcp/DhcpPacket;->mDnsServers:Ljava/util/List;

    #@46
    invoke-virtual {p0, p1, v0, v1}, Landroid/net/dhcp/DhcpAckPacket;->addTlv(Ljava/nio/ByteBuffer;BLjava/util/List;)V

    #@49
    .line 89
    invoke-virtual {p0, p1}, Landroid/net/dhcp/DhcpAckPacket;->addTlvEnd(Ljava/nio/ByteBuffer;)V

    #@4c
    .line 90
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 43
    invoke-super {p0}, Landroid/net/dhcp/DhcpPacket;->toString()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 44
    .local v3, s:Ljava/lang/String;
    const-string v1, " DNS servers: "

    #@6
    .line 46
    .local v1, dnsServers:Ljava/lang/String;
    iget-object v4, p0, Landroid/net/dhcp/DhcpPacket;->mDnsServers:Ljava/util/List;

    #@8
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v2

    #@c
    .local v2, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_34

    #@12
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Ljava/net/InetAddress;

    #@18
    .line 47
    .local v0, dnsServer:Ljava/net/InetAddress;
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v0}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, " "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    goto :goto_c

    #@34
    .line 50
    .end local v0           #dnsServer:Ljava/net/InetAddress;
    :cond_34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    const-string v5, " ACK: your new IP "

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    iget-object v5, p0, Landroid/net/dhcp/DhcpPacket;->mYourIp:Ljava/net/InetAddress;

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    const-string v5, ", netmask "

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    iget-object v5, p0, Landroid/net/dhcp/DhcpPacket;->mSubnetMask:Ljava/net/InetAddress;

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    const-string v5, ", gateway "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    iget-object v5, p0, Landroid/net/dhcp/DhcpPacket;->mGateway:Ljava/net/InetAddress;

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, ", lease time "

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    iget-object v5, p0, Landroid/net/dhcp/DhcpPacket;->mLeaseTime:Ljava/lang/Integer;

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    return-object v4
.end method
