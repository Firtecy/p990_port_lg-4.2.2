.class public final enum Landroid/net/LinkCapabilities$FlowState;
.super Ljava/lang/Enum;
.source "LinkCapabilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/LinkCapabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FlowState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/LinkCapabilities$FlowState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/LinkCapabilities$FlowState;

.field public static final enum ACTIVATED:Landroid/net/LinkCapabilities$FlowState;

.field public static final enum DISABLED:Landroid/net/LinkCapabilities$FlowState;

.field public static final enum ENABLED:Landroid/net/LinkCapabilities$FlowState;

.field public static final enum INACTIVE:Landroid/net/LinkCapabilities$FlowState;

.field public static final enum SUSPENDED:Landroid/net/LinkCapabilities$FlowState;


# instance fields
.field mState:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 177
    new-instance v0, Landroid/net/LinkCapabilities$FlowState;

    #@7
    const-string v1, "INACTIVE"

    #@9
    const-string v2, "INACTIVE"

    #@b
    invoke-direct {v0, v1, v3, v2}, Landroid/net/LinkCapabilities$FlowState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@e
    sput-object v0, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@10
    .line 178
    new-instance v0, Landroid/net/LinkCapabilities$FlowState;

    #@12
    const-string v1, "ACTIVATED"

    #@14
    const-string v2, "ACTIVATED"

    #@16
    invoke-direct {v0, v1, v4, v2}, Landroid/net/LinkCapabilities$FlowState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@19
    sput-object v0, Landroid/net/LinkCapabilities$FlowState;->ACTIVATED:Landroid/net/LinkCapabilities$FlowState;

    #@1b
    .line 179
    new-instance v0, Landroid/net/LinkCapabilities$FlowState;

    #@1d
    const-string v1, "ENABLED"

    #@1f
    const-string v2, "ENABLED"

    #@21
    invoke-direct {v0, v1, v5, v2}, Landroid/net/LinkCapabilities$FlowState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@24
    sput-object v0, Landroid/net/LinkCapabilities$FlowState;->ENABLED:Landroid/net/LinkCapabilities$FlowState;

    #@26
    .line 180
    new-instance v0, Landroid/net/LinkCapabilities$FlowState;

    #@28
    const-string v1, "SUSPENDED"

    #@2a
    const-string v2, "SUSPENDED"

    #@2c
    invoke-direct {v0, v1, v6, v2}, Landroid/net/LinkCapabilities$FlowState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@2f
    sput-object v0, Landroid/net/LinkCapabilities$FlowState;->SUSPENDED:Landroid/net/LinkCapabilities$FlowState;

    #@31
    .line 181
    new-instance v0, Landroid/net/LinkCapabilities$FlowState;

    #@33
    const-string v1, "DISABLED"

    #@35
    const-string v2, "DISABLED"

    #@37
    invoke-direct {v0, v1, v7, v2}, Landroid/net/LinkCapabilities$FlowState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@3a
    sput-object v0, Landroid/net/LinkCapabilities$FlowState;->DISABLED:Landroid/net/LinkCapabilities$FlowState;

    #@3c
    .line 176
    const/4 v0, 0x5

    #@3d
    new-array v0, v0, [Landroid/net/LinkCapabilities$FlowState;

    #@3f
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@41
    aput-object v1, v0, v3

    #@43
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->ACTIVATED:Landroid/net/LinkCapabilities$FlowState;

    #@45
    aput-object v1, v0, v4

    #@47
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->ENABLED:Landroid/net/LinkCapabilities$FlowState;

    #@49
    aput-object v1, v0, v5

    #@4b
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->SUSPENDED:Landroid/net/LinkCapabilities$FlowState;

    #@4d
    aput-object v1, v0, v6

    #@4f
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->DISABLED:Landroid/net/LinkCapabilities$FlowState;

    #@51
    aput-object v1, v0, v7

    #@53
    sput-object v0, Landroid/net/LinkCapabilities$FlowState;->$VALUES:[Landroid/net/LinkCapabilities$FlowState;

    #@55
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter "state"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 184
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/net/LinkCapabilities$FlowState;->mState:Ljava/lang/String;

    #@6
    .line 188
    iput-object p3, p0, Landroid/net/LinkCapabilities$FlowState;->mState:Ljava/lang/String;

    #@8
    .line 189
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/LinkCapabilities$FlowState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 176
    const-class v0, Landroid/net/LinkCapabilities$FlowState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/LinkCapabilities$FlowState;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/net/LinkCapabilities$FlowState;
    .registers 1

    #@0
    .prologue
    .line 176
    sget-object v0, Landroid/net/LinkCapabilities$FlowState;->$VALUES:[Landroid/net/LinkCapabilities$FlowState;

    #@2
    invoke-virtual {v0}, [Landroid/net/LinkCapabilities$FlowState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/net/LinkCapabilities$FlowState;

    #@8
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/net/LinkCapabilities$FlowState;->mState:Ljava/lang/String;

    #@2
    return-object v0
.end method
