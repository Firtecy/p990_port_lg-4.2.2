.class Landroid/net/MobileDataStateTracker$MdstHandler;
.super Landroid/os/Handler;
.source "MobileDataStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/MobileDataStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MdstHandler"
.end annotation


# instance fields
.field private mMdst:Landroid/net/MobileDataStateTracker;


# direct methods
.method constructor <init>(Landroid/os/Looper;Landroid/net/MobileDataStateTracker;)V
    .registers 3
    .parameter "looper"
    .parameter "mdst"

    #@0
    .prologue
    .line 144
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@3
    .line 145
    iput-object p2, p0, Landroid/net/MobileDataStateTracker$MdstHandler;->mMdst:Landroid/net/MobileDataStateTracker;

    #@5
    .line 146
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 150
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_1c

    #@5
    .line 172
    :cond_5
    :goto_5
    return-void

    #@6
    .line 152
    :sswitch_6
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@8
    if-nez v0, :cond_5

    #@a
    .line 156
    iget-object v1, p0, Landroid/net/MobileDataStateTracker$MdstHandler;->mMdst:Landroid/net/MobileDataStateTracker;

    #@c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e
    check-cast v0, Lcom/android/internal/util/AsyncChannel;

    #@10
    invoke-static {v1, v0}, Landroid/net/MobileDataStateTracker;->access$102(Landroid/net/MobileDataStateTracker;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@13
    goto :goto_5

    #@14
    .line 165
    :sswitch_14
    iget-object v0, p0, Landroid/net/MobileDataStateTracker$MdstHandler;->mMdst:Landroid/net/MobileDataStateTracker;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-static {v0, v1}, Landroid/net/MobileDataStateTracker;->access$102(Landroid/net/MobileDataStateTracker;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@1a
    goto :goto_5

    #@1b
    .line 150
    nop

    #@1c
    :sswitch_data_1c
    .sparse-switch
        0x11000 -> :sswitch_6
        0x11004 -> :sswitch_14
    .end sparse-switch
.end method
