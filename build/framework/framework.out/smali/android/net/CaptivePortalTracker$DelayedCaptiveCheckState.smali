.class Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;
.super Lcom/android/internal/util/State;
.source "CaptivePortalTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/CaptivePortalTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DelayedCaptiveCheckState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/CaptivePortalTracker;


# direct methods
.method private constructor <init>(Landroid/net/CaptivePortalTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 221
    iput-object p1, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 221
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;-><init>(Landroid/net/CaptivePortalTracker;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2
    iget-object v1, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@4
    const/4 v2, 0x2

    #@5
    iget-object v3, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@7
    invoke-static {v3}, Landroid/net/CaptivePortalTracker;->access$1504(Landroid/net/CaptivePortalTracker;)I

    #@a
    move-result v3

    #@b
    const/4 v4, 0x0

    #@c
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/CaptivePortalTracker;->obtainMessage(III)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    const-wide/16 v2, 0x2710

    #@12
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/CaptivePortalTracker;->sendMessageDelayed(Landroid/os/Message;J)V

    #@15
    .line 227
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 6
    .parameter "message"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 232
    iget v2, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v2, :pswitch_data_3a

    #@6
    .line 247
    const/4 v1, 0x0

    #@7
    .line 249
    :cond_7
    :goto_7
    return v1

    #@8
    .line 234
    :pswitch_8
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@a
    iget-object v3, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@c
    invoke-static {v3}, Landroid/net/CaptivePortalTracker;->access$1500(Landroid/net/CaptivePortalTracker;)I

    #@f
    move-result v3

    #@10
    if-ne v2, v3, :cond_7

    #@12
    .line 235
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@14
    iget-object v3, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@16
    invoke-static {v3}, Landroid/net/CaptivePortalTracker;->access$1600(Landroid/net/CaptivePortalTracker;)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v2, v3}, Landroid/net/CaptivePortalTracker;->access$1700(Landroid/net/CaptivePortalTracker;Ljava/lang/String;)Ljava/net/InetAddress;

    #@1d
    move-result-object v0

    #@1e
    .line 236
    .local v0, server:Ljava/net/InetAddress;
    if-eqz v0, :cond_2d

    #@20
    .line 237
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@22
    invoke-static {v2, v0}, Landroid/net/CaptivePortalTracker;->access$1800(Landroid/net/CaptivePortalTracker;Ljava/net/InetAddress;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_2d

    #@28
    .line 239
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2a
    invoke-static {v2, v1}, Landroid/net/CaptivePortalTracker;->access$700(Landroid/net/CaptivePortalTracker;Z)V

    #@2d
    .line 243
    :cond_2d
    iget-object v2, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@2f
    iget-object v3, p0, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;->this$0:Landroid/net/CaptivePortalTracker;

    #@31
    invoke-static {v3}, Landroid/net/CaptivePortalTracker;->access$1900(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;

    #@34
    move-result-object v3

    #@35
    invoke-static {v2, v3}, Landroid/net/CaptivePortalTracker;->access$2000(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V

    #@38
    goto :goto_7

    #@39
    .line 232
    nop

    #@3a
    :pswitch_data_3a
    .packed-switch 0x2
        :pswitch_8
    .end packed-switch
.end method
