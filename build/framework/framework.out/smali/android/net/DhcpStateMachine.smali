.class public Landroid/net/DhcpStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "DhcpStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/DhcpStateMachine$WaitBeforeRenewalState;,
        Landroid/net/DhcpStateMachine$RunningState;,
        Landroid/net/DhcpStateMachine$WaitBeforeStartState;,
        Landroid/net/DhcpStateMachine$StoppedState;,
        Landroid/net/DhcpStateMachine$DefaultState;,
        Landroid/net/DhcpStateMachine$DhcpAction;
    }
.end annotation


# static fields
.field private static final ACTION_DHCP_RENEW:Ljava/lang/String; = "android.net.wifi.DHCP_RENEW"

.field private static final BASE:I = 0x30000

.field public static final CMD_ON_QUIT:I = 0x30006

.field public static final CMD_POST_DHCP_ACTION:I = 0x30005

.field public static final CMD_PRE_DHCP_ACTION:I = 0x30004

.field public static final CMD_PRE_DHCP_ACTION_COMPLETE:I = 0x30007

.field public static final CMD_RENEW_DHCP:I = 0x30003

.field public static final CMD_START_DHCP:I = 0x30001

.field public static final CMD_STOP_DHCP:I = 0x30002

.field private static final DBG:Z = true

.field private static final DEFAULT_DHCP_BSSID:Ljava/lang/String; = "00:0a:eb"

.field private static final DEFAULT_DHCP_DNS1:Ljava/lang/String; = "8.8.8.8"

.field private static final DEFAULT_DHCP_DNS2:Ljava/lang/String; = "8.8.4.4"

.field private static final DEFAULT_DHCP_IPADDRESS:Ljava/lang/String; = "192.168.2."

.field private static final DEFAULT_DHCP_PREFIXLENGTH:I = 0x18

.field private static final DEFAULT_DHCP_SERVERADDRESS:Ljava/lang/String; = "192.168.2.1"

.field public static final DHCP_FAILURE:I = 0x2

.field private static final DHCP_RENEW:I = 0x0

.field public static final DHCP_SUCCESS:I = 0x1

.field private static final MIN_RENEWAL_TIME_SECS:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "DhcpStateMachine"

.field private static final WAKELOCK_TAG:Ljava/lang/String; = "DHCP"


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mController:Lcom/android/internal/util/StateMachine;

.field private mDefaultState:Lcom/android/internal/util/State;

.field private mDhcpInfo:Landroid/net/DhcpInfoInternal;

.field private mDhcpInfoCacheList:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/DhcpInfoInternal;",
            ">;"
        }
    .end annotation
.end field

.field private mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mDhcpRenewalIntent:Landroid/app/PendingIntent;

.field private mInterfaceName:Ljava/lang/String;

.field private mRegisteredForPreDhcpNotification:Z

.field private mRunningState:Lcom/android/internal/util/State;

.field private mStoppedState:Lcom/android/internal/util/State;

.field private mWaitBeforeRenewalState:Lcom/android/internal/util/State;

.field private mWaitBeforeStartState:Lcom/android/internal/util/State;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/android/internal/util/StateMachine;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "controller"
    .parameter "intf"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 161
    const-string v2, "DhcpStateMachine"

    #@3
    invoke-direct {p0, v2}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@6
    .line 126
    iput-boolean v4, p0, Landroid/net/DhcpStateMachine;->mRegisteredForPreDhcpNotification:Z

    #@8
    .line 151
    new-instance v2, Landroid/net/DhcpStateMachine$DefaultState;

    #@a
    invoke-direct {v2, p0}, Landroid/net/DhcpStateMachine$DefaultState;-><init>(Landroid/net/DhcpStateMachine;)V

    #@d
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@f
    .line 152
    new-instance v2, Landroid/net/DhcpStateMachine$StoppedState;

    #@11
    invoke-direct {v2, p0}, Landroid/net/DhcpStateMachine$StoppedState;-><init>(Landroid/net/DhcpStateMachine;)V

    #@14
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@16
    .line 153
    new-instance v2, Landroid/net/DhcpStateMachine$WaitBeforeStartState;

    #@18
    invoke-direct {v2, p0}, Landroid/net/DhcpStateMachine$WaitBeforeStartState;-><init>(Landroid/net/DhcpStateMachine;)V

    #@1b
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@1d
    .line 154
    new-instance v2, Landroid/net/DhcpStateMachine$RunningState;

    #@1f
    invoke-direct {v2, p0}, Landroid/net/DhcpStateMachine$RunningState;-><init>(Landroid/net/DhcpStateMachine;)V

    #@22
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mRunningState:Lcom/android/internal/util/State;

    #@24
    .line 155
    new-instance v2, Landroid/net/DhcpStateMachine$WaitBeforeRenewalState;

    #@26
    invoke-direct {v2, p0}, Landroid/net/DhcpStateMachine$WaitBeforeRenewalState;-><init>(Landroid/net/DhcpStateMachine;)V

    #@29
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeRenewalState:Lcom/android/internal/util/State;

    #@2b
    .line 163
    iput-object p1, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@2d
    .line 164
    iput-object p2, p0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@2f
    .line 165
    iput-object p3, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@31
    .line 167
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@33
    const-string v3, "alarm"

    #@35
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@38
    move-result-object v2

    #@39
    check-cast v2, Landroid/app/AlarmManager;

    #@3b
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mAlarmManager:Landroid/app/AlarmManager;

    #@3d
    .line 168
    new-instance v0, Landroid/content/Intent;

    #@3f
    const-string v2, "android.net.wifi.DHCP_RENEW"

    #@41
    const/4 v3, 0x0

    #@42
    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@45
    .line 169
    .local v0, dhcpRenewalIntent:Landroid/content/Intent;
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@47
    invoke-static {v2, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@4a
    move-result-object v2

    #@4b
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewalIntent:Landroid/app/PendingIntent;

    #@4d
    .line 171
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@4f
    const-string/jumbo v3, "power"

    #@52
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@55
    move-result-object v1

    #@56
    check-cast v1, Landroid/os/PowerManager;

    #@58
    .line 172
    .local v1, powerManager:Landroid/os/PowerManager;
    const/4 v2, 0x1

    #@59
    const-string v3, "DHCP"

    #@5b
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@5e
    move-result-object v2

    #@5f
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@61
    .line 173
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@63
    invoke-virtual {v2, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@66
    .line 175
    new-instance v2, Landroid/net/DhcpStateMachine$1;

    #@68
    invoke-direct {v2, p0}, Landroid/net/DhcpStateMachine$1;-><init>(Landroid/net/DhcpStateMachine;)V

    #@6b
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@6d
    .line 198
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@6f
    iget-object v3, p0, Landroid/net/DhcpStateMachine;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@71
    new-instance v4, Landroid/content/IntentFilter;

    #@73
    const-string v5, "android.net.wifi.DHCP_RENEW"

    #@75
    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@78
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@7b
    .line 200
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@7d
    invoke-virtual {p0, v2}, Landroid/net/DhcpStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@80
    .line 201
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@82
    iget-object v3, p0, Landroid/net/DhcpStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@84
    invoke-virtual {p0, v2, v3}, Landroid/net/DhcpStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@87
    .line 202
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@89
    iget-object v3, p0, Landroid/net/DhcpStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@8b
    invoke-virtual {p0, v2, v3}, Landroid/net/DhcpStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@8e
    .line 203
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mRunningState:Lcom/android/internal/util/State;

    #@90
    iget-object v3, p0, Landroid/net/DhcpStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@92
    invoke-virtual {p0, v2, v3}, Landroid/net/DhcpStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@95
    .line 204
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeRenewalState:Lcom/android/internal/util/State;

    #@97
    iget-object v3, p0, Landroid/net/DhcpStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@99
    invoke-virtual {p0, v2, v3}, Landroid/net/DhcpStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@9c
    .line 206
    iget-object v2, p0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@9e
    invoke-virtual {p0, v2}, Landroid/net/DhcpStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@a1
    .line 209
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@a3
    if-eqz v2, :cond_ab

    #@a5
    .line 210
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a8
    move-result-object v2

    #@a9
    iput-object v2, p0, Landroid/net/DhcpStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@ab
    .line 213
    :cond_ab
    return-void
.end method

.method private AddDhcpInfoCache(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)V
    .registers 10
    .parameter "key"
    .parameter "addDhcpInfo"

    #@0
    .prologue
    .line 842
    iget-object v4, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@2
    invoke-virtual {v4}, Landroid/util/LruCache;->size()I

    #@5
    move-result v2

    #@6
    .line 844
    .local v2, nCacheSize:I
    const/16 v4, 0x41

    #@8
    if-lt v2, v4, :cond_29

    #@a
    .line 845
    const-string v4, "DhcpStateMachine"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string/jumbo v6, "mDhcpInfoCacheList count is full - "

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 846
    iget-object v4, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@25
    invoke-virtual {v4}, Landroid/util/LruCache;->evictAll()V

    #@28
    .line 847
    const/4 v2, 0x0

    #@29
    .line 850
    :cond_29
    new-instance v0, Landroid/net/DhcpInfoInternal;

    #@2b
    invoke-direct {v0}, Landroid/net/DhcpInfoInternal;-><init>()V

    #@2e
    .line 851
    .local v0, RealAddDhcpInfo:Landroid/net/DhcpInfoInternal;
    iget-object v4, p2, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@30
    iput-object v4, v0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@32
    .line 852
    iget v4, p2, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@34
    iput v4, v0, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@36
    .line 854
    invoke-virtual {p2}, Landroid/net/DhcpInfoInternal;->getRoutes()Ljava/util/Collection;

    #@39
    move-result-object v4

    #@3a
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v1

    #@3e
    .local v1, i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_4e

    #@44
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v3

    #@48
    check-cast v3, Landroid/net/RouteInfo;

    #@4a
    .line 855
    .local v3, route:Landroid/net/RouteInfo;
    invoke-virtual {v0, v3}, Landroid/net/DhcpInfoInternal;->addRoute(Landroid/net/RouteInfo;)V

    #@4d
    goto :goto_3e

    #@4e
    .line 857
    .end local v3           #route:Landroid/net/RouteInfo;
    :cond_4e
    iget-object v4, p2, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@50
    iput-object v4, v0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@52
    .line 858
    iget-object v4, p2, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@54
    iput-object v4, v0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@56
    .line 859
    iget-object v4, p2, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@58
    iput-object v4, v0, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@5a
    .line 860
    iget v4, p2, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@5c
    iput v4, v0, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@5e
    .line 861
    iget-object v4, p2, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@60
    iput-object v4, v0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@62
    .line 863
    const-string v4, "DhcpStateMachine"

    #@64
    new-instance v5, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v6, "Add DhcpInfoInternal: "

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v0}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v5

    #@7b
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 865
    iget-object v4, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@80
    invoke-virtual {v4, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@83
    .line 866
    return-void
.end method

.method private AddDhcpInfoCache(Landroid/net/DhcpInfoInternal;)Z
    .registers 15
    .parameter "addDhcpInfo"

    #@0
    .prologue
    .line 747
    iget-object v10, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@2
    if-nez v10, :cond_6

    #@4
    .line 748
    const/4 v2, 0x1

    #@5
    .line 837
    :goto_5
    return v2

    #@6
    .line 752
    :cond_6
    const/4 v2, 0x0

    #@7
    .line 753
    .local v2, bShouldSendDhcpAction:Z
    const/4 v1, 0x0

    #@8
    .line 755
    .local v1, bAutoIPSetWhenDhcpRenew:Z
    iget-object v10, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@a
    const-string/jumbo v11, "wifi"

    #@d
    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v9

    #@11
    check-cast v9, Landroid/net/wifi/WifiManager;

    #@13
    .line 756
    .local v9, wifiMngr:Landroid/net/wifi/WifiManager;
    invoke-virtual {v9}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@16
    move-result-object v3

    #@17
    .line 757
    .local v3, currentWifiInfo:Landroid/net/wifi/WifiInfo;
    if-nez v3, :cond_1b

    #@19
    .line 758
    const/4 v2, 0x1

    #@1a
    goto :goto_5

    #@1b
    .line 761
    :cond_1b
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    .line 762
    .local v5, strBssid:Ljava/lang/String;
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    .line 763
    .local v7, strSsid:Ljava/lang/String;
    if-eqz v5, :cond_33

    #@25
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@28
    move-result v10

    #@29
    if-nez v10, :cond_33

    #@2b
    if-eqz v7, :cond_33

    #@2d
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    #@30
    move-result v10

    #@31
    if-eqz v10, :cond_3c

    #@33
    .line 765
    :cond_33
    const-string v10, "DhcpStateMachine"

    #@35
    const-string v11, "[bssid + ssid] hash key is null. Return."

    #@37
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 767
    const/4 v2, 0x1

    #@3b
    goto :goto_5

    #@3c
    .line 769
    :cond_3c
    new-instance v10, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v10

    #@45
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v10

    #@49
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    .line 771
    .local v4, key:Ljava/lang/String;
    const-string v10, "DhcpStateMachine"

    #@4f
    new-instance v11, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v12, "[bssid + ssid] hash key :"

    #@56
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v11

    #@5a
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v11

    #@5e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v11

    #@62
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 776
    iget-object v10, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@67
    invoke-virtual {v10, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    move-result-object v0

    #@6b
    check-cast v0, Landroid/net/DhcpInfoInternal;

    #@6d
    .line 778
    .local v0, DhcpInfoCache:Landroid/net/DhcpInfoInternal;
    if-eqz v0, :cond_12e

    #@6f
    .line 779
    invoke-virtual {v0}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    #@72
    move-result-object v6

    #@73
    .line 780
    .local v6, strDhcpInfoCache:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    #@76
    move-result-object v8

    #@77
    .line 783
    .local v8, straddDhcpInfo:Ljava/lang/String;
    iget-object v10, p1, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@79
    invoke-direct {p0, v10}, Landroid/net/DhcpStateMachine;->getIpAddressToInt(Ljava/lang/String;)I

    #@7c
    move-result v10

    #@7d
    const v11, 0xffff

    #@80
    and-int/2addr v10, v11

    #@81
    const v11, 0xfea9

    #@84
    if-ne v10, v11, :cond_109

    #@86
    .line 784
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->getDLNAEnabled()Z

    #@89
    move-result v10

    #@8a
    const/4 v11, 0x1

    #@8b
    if-eq v10, v11, :cond_8e

    #@8d
    .line 785
    const/4 v1, 0x1

    #@8e
    .line 787
    :cond_8e
    const-string v10, "DhcpStateMachine"

    #@90
    new-instance v11, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v12, "AUTOIP is not allowed for dhcp cache. bAutoIPSetWhenDhcpRenew == "

    #@97
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v11

    #@9b
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v11

    #@9f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v11

    #@a3
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 802
    :cond_a6
    :goto_a6
    const/4 v10, 0x1

    #@a7
    if-ne v2, v10, :cond_126

    #@a9
    .line 804
    const-string v10, "DhcpStateMachine"

    #@ab
    new-instance v11, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v12, "Updated DhcpInfoInternal 1: "

    #@b2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v11

    #@b6
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v11

    #@ba
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v11

    #@be
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    .line 807
    const-string v10, "DhcpStateMachine"

    #@c3
    new-instance v11, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v12, "Updated DhcpInfoInternal 2: "

    #@ca
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v11

    #@ce
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v11

    #@d2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v11

    #@d6
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 825
    .end local v6           #strDhcpInfoCache:Ljava/lang/String;
    .end local v8           #straddDhcpInfo:Ljava/lang/String;
    :goto_d9
    if-nez v1, :cond_ef

    #@db
    iget-object v10, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@dd
    if-eqz v10, :cond_ef

    #@df
    invoke-virtual {p0}, Landroid/net/DhcpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@e2
    move-result-object v10

    #@e3
    iget-object v11, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@e5
    if-eq v10, v11, :cond_ef

    #@e7
    .line 827
    const-string v10, "DhcpStateMachine"

    #@e9
    const-string v11, "Current State is not mWaitBeforeStartState. So bShouldSendDhcpAction must be true"

    #@eb
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ee
    .line 830
    const/4 v2, 0x1

    #@ef
    .line 834
    :cond_ef
    const-string v10, "DhcpStateMachine"

    #@f1
    new-instance v11, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v12, "bShouldSendDhcpAction Result: "

    #@f8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v11

    #@fc
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v11

    #@100
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v11

    #@104
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@107
    goto/16 :goto_5

    #@109
    .line 790
    .restart local v6       #strDhcpInfoCache:Ljava/lang/String;
    .restart local v8       #straddDhcpInfo:Ljava/lang/String;
    :cond_109
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10c
    move-result v10

    #@10d
    if-nez v10, :cond_a6

    #@10f
    .line 791
    iget-object v10, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@111
    invoke-virtual {v10, v4}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@114
    move-result-object v10

    #@115
    if-eqz v10, :cond_124

    #@117
    .line 793
    const-string v10, "DhcpStateMachine"

    #@119
    const-string v11, "[bssid + ssid] hash key is removed."

    #@11b
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    .line 795
    invoke-direct {p0, v0, p1}, Landroid/net/DhcpStateMachine;->setInterfaceDownUpWithDhcpInfo(Landroid/net/DhcpInfoInternal;Landroid/net/DhcpInfoInternal;)Z

    #@121
    .line 796
    invoke-direct {p0, v4, p1}, Landroid/net/DhcpStateMachine;->AddDhcpInfoCache(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)V

    #@124
    .line 798
    :cond_124
    const/4 v2, 0x1

    #@125
    goto :goto_a6

    #@126
    .line 811
    :cond_126
    const-string v10, "DhcpStateMachine"

    #@128
    const-string v11, "Don\'t need to update mDhcpInfoCacheList"

    #@12a
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    goto :goto_d9

    #@12e
    .line 817
    .end local v6           #strDhcpInfoCache:Ljava/lang/String;
    .end local v8           #straddDhcpInfo:Ljava/lang/String;
    :cond_12e
    iget-object v10, p1, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@130
    invoke-direct {p0, v10}, Landroid/net/DhcpStateMachine;->getIpAddressToInt(Ljava/lang/String;)I

    #@133
    move-result v10

    #@134
    const v11, 0xffff

    #@137
    and-int/2addr v10, v11

    #@138
    const v11, 0xfea9

    #@13b
    if-ne v10, v11, :cond_146

    #@13d
    .line 818
    const-string v10, "DhcpStateMachine"

    #@13f
    const-string v11, "AUTOIP is not allowed for dhcp cache. bShouldSendDhcpAction == TRUE"

    #@141
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@144
    .line 822
    :goto_144
    const/4 v2, 0x1

    #@145
    goto :goto_d9

    #@146
    .line 820
    :cond_146
    invoke-direct {p0, v4, p1}, Landroid/net/DhcpStateMachine;->AddDhcpInfoCache(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)V

    #@149
    goto :goto_144
.end method

.method private CheckDhcpDirectory()Z
    .registers 9

    #@0
    .prologue
    .line 595
    new-instance v1, Ljava/io/File;

    #@2
    const-string v5, "/data/misc/dhcp"

    #@4
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 596
    .local v1, dhcpDir:Ljava/io/File;
    const/4 v0, 0x1

    #@8
    .line 598
    .local v0, bRet:Z
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@b
    move-result v5

    #@c
    if-nez v5, :cond_17

    #@e
    .line 600
    const-string v5, "DhcpStateMachine"

    #@10
    const-string v6, "CheckDhcpDirectory : /data/misc/dhcp is not exist."

    #@12
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 602
    const/4 v5, 0x0

    #@16
    .line 628
    :goto_16
    return v5

    #@17
    .line 605
    :cond_17
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@1a
    move-result-object v2

    #@1b
    .line 606
    .local v2, files:[Ljava/io/File;
    const/4 v4, 0x0

    #@1c
    .line 607
    .local v4, numFiles:I
    if-eqz v2, :cond_1f

    #@1e
    .line 608
    array-length v4, v2

    #@1f
    .line 612
    :cond_1f
    const-string v5, "DhcpStateMachine"

    #@21
    new-instance v6, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v7, "CheckDhcpDirectory [Lease File Count] : "

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v6

    #@34
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 615
    const/16 v5, 0x46

    #@39
    if-le v4, v5, :cond_5c

    #@3b
    .line 616
    const/4 v0, 0x0

    #@3c
    .line 618
    const/4 v3, 0x0

    #@3d
    .local v3, i:I
    :goto_3d
    if-ge v3, v4, :cond_55

    #@3f
    .line 619
    aget-object v5, v2, v3

    #@41
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    const-string v6, ".lease2"

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@4a
    move-result v5

    #@4b
    if-eqz v5, :cond_52

    #@4d
    .line 620
    aget-object v5, v2, v3

    #@4f
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@52
    .line 618
    :cond_52
    add-int/lit8 v3, v3, 0x1

    #@54
    goto :goto_3d

    #@55
    .line 625
    :cond_55
    const-string v5, "DhcpStateMachine"

    #@57
    const-string v6, "CheckDhcpDirectory : Deleted /data/misc/dhcp/dhcpcd-*.lease2"

    #@59
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .end local v3           #i:I
    :cond_5c
    move v5, v0

    #@5d
    .line 628
    goto :goto_16
.end method

.method private CheckSustainCurrentDhcpInfoCache(Landroid/net/DhcpStateMachine$DhcpAction;)Z
    .registers 13
    .parameter "dhcpAction"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 905
    sget-object v8, Landroid/net/DhcpStateMachine$DhcpAction;->START:Landroid/net/DhcpStateMachine$DhcpAction;

    #@4
    if-eq p1, v8, :cond_f

    #@6
    .line 907
    const-string v8, "DhcpStateMachine"

    #@8
    const-string v9, "Don\'t need to check [bssid + ssid] hash key, dhcpAction != DhcpAction.START."

    #@a
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    move v1, v7

    #@e
    .line 970
    :cond_e
    :goto_e
    return v1

    #@f
    .line 912
    :cond_f
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->getSustainFailedDhcpInfoCacheDisabled()Z

    #@12
    move-result v8

    #@13
    if-ne v8, v10, :cond_1e

    #@15
    .line 914
    const-string v8, "DhcpStateMachine"

    #@17
    const-string v9, "getSustainFailedDhcpInfoCacheDisabled == true"

    #@19
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    move v1, v7

    #@1d
    .line 916
    goto :goto_e

    #@1e
    .line 919
    :cond_1e
    iget-object v8, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@20
    if-nez v8, :cond_24

    #@22
    move v1, v7

    #@23
    .line 920
    goto :goto_e

    #@24
    .line 923
    :cond_24
    iget-object v8, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@26
    const-string/jumbo v9, "wifi"

    #@29
    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2c
    move-result-object v6

    #@2d
    check-cast v6, Landroid/net/wifi/WifiManager;

    #@2f
    .line 924
    .local v6, wifiMngr:Landroid/net/wifi/WifiManager;
    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@32
    move-result-object v2

    #@33
    .line 925
    .local v2, currentWifiInfo:Landroid/net/wifi/WifiInfo;
    if-nez v2, :cond_37

    #@35
    move v1, v7

    #@36
    .line 926
    goto :goto_e

    #@37
    .line 929
    :cond_37
    const/4 v1, 0x0

    #@38
    .line 931
    .local v1, bCheckSustainCurrentDhcpInfoCache:Z
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    .line 932
    .local v4, strBssid:Ljava/lang/String;
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    .line 933
    .local v5, strSsid:Ljava/lang/String;
    if-eqz v4, :cond_50

    #@42
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@45
    move-result v8

    #@46
    if-nez v8, :cond_50

    #@48
    if-eqz v5, :cond_50

    #@4a
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@4d
    move-result v8

    #@4e
    if-eqz v8, :cond_59

    #@50
    .line 935
    :cond_50
    const-string v8, "DhcpStateMachine"

    #@52
    const-string v9, "[CheckSustainCurrentDhcpInfoCache][bssid + ssid] hash key is null. Return."

    #@54
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    move v1, v7

    #@58
    .line 937
    goto :goto_e

    #@59
    .line 939
    :cond_59
    new-instance v7, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    .line 941
    .local v3, key:Ljava/lang/String;
    const-string v7, "DhcpStateMachine"

    #@6c
    new-instance v8, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v9, "[CheckSustainCurrentDhcpInfoCache][bssid + ssid] hash key :"

    #@73
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 946
    iget-object v7, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@84
    invoke-virtual {v7, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    move-result-object v0

    #@88
    check-cast v0, Landroid/net/DhcpInfoInternal;

    #@8a
    .line 948
    .local v0, DhcpInfoCache:Landroid/net/DhcpInfoInternal;
    if-eqz v0, :cond_c3

    #@8c
    .line 950
    const-string v7, "DhcpStateMachine"

    #@8e
    const-string v8, "[CheckSustainCurrentDhcpInfoCache] bCheckSustainCurrentDhcpInfoCache set TRUE"

    #@90
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 953
    const/4 v1, 0x1

    #@94
    .line 955
    const-string v7, "DhcpStateMachine"

    #@96
    new-instance v8, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v9, "[] DHCP failed on "

    #@9d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v8

    #@a1
    iget-object v9, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@a3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    const-string v9, ": "

    #@a9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-static {}, Landroid/net/NetworkUtils;->getDhcpError()Ljava/lang/String;

    #@b0
    move-result-object v9

    #@b1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v8

    #@b5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v8

    #@b9
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 956
    iget-object v7, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@be
    invoke-static {v7}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@c1
    goto/16 :goto_e

    #@c3
    .line 959
    :cond_c3
    invoke-direct {p0, v4, v3}, Landroid/net/DhcpStateMachine;->checkSustainWithDefaultDhcpInfo(Ljava/lang/String;Ljava/lang/String;)Z

    #@c6
    move-result v7

    #@c7
    if-ne v7, v10, :cond_e

    #@c9
    .line 960
    const-string v7, "DhcpStateMachine"

    #@cb
    const-string v8, "[checkSustainWithDefaultDhcpInfo] bCheckSustainCurrentDhcpInfoCache set TRUE"

    #@cd
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    .line 962
    const/4 v1, 0x1

    #@d1
    .line 964
    const-string v7, "DhcpStateMachine"

    #@d3
    new-instance v8, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v9, "[DEFAULT] DHCP failed on "

    #@da
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v8

    #@de
    iget-object v9, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@e0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v8

    #@e4
    const-string v9, ": "

    #@e6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v8

    #@ea
    invoke-static {}, Landroid/net/NetworkUtils;->getDhcpError()Ljava/lang/String;

    #@ed
    move-result-object v9

    #@ee
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v8

    #@f2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v8

    #@f6
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    .line 965
    iget-object v7, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@fb
    invoke-static {v7}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@fe
    goto/16 :goto_e
.end method

.method private RemoveDhcpInfoCache()V
    .registers 9

    #@0
    .prologue
    .line 869
    iget-object v5, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@2
    if-nez v5, :cond_5

    #@4
    .line 901
    :cond_4
    :goto_4
    return-void

    #@5
    .line 873
    :cond_5
    iget-object v5, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@7
    const-string/jumbo v6, "wifi"

    #@a
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v4

    #@e
    check-cast v4, Landroid/net/wifi/WifiManager;

    #@10
    .line 874
    .local v4, wifiMngr:Landroid/net/wifi/WifiManager;
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@13
    move-result-object v0

    #@14
    .line 875
    .local v0, currentWifiInfo:Landroid/net/wifi/WifiInfo;
    if-eqz v0, :cond_4

    #@16
    .line 880
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    .line 881
    .local v2, strBssid:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 882
    .local v3, strSsid:Ljava/lang/String;
    if-eqz v2, :cond_2e

    #@20
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    #@23
    move-result v5

    #@24
    if-nez v5, :cond_2e

    #@26
    if-eqz v3, :cond_2e

    #@28
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_36

    #@2e
    .line 884
    :cond_2e
    const-string v5, "DhcpStateMachine"

    #@30
    const-string v6, "[bssid + ssid] hash key is null. Return."

    #@32
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_4

    #@36
    .line 888
    :cond_36
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    .line 890
    .local v1, key:Ljava/lang/String;
    const-string v5, "DhcpStateMachine"

    #@49
    new-instance v6, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v7, "[bssid + ssid] hash key :"

    #@50
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 895
    iget-object v5, p0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@61
    invoke-virtual {v5, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@64
    move-result-object v5

    #@65
    if-eqz v5, :cond_4

    #@67
    .line 897
    const-string v5, "DhcpStateMachine"

    #@69
    const-string v6, "[bssid + ssid] hash key is removed."

    #@6b
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    goto :goto_4
.end method

.method private ReplaceDhcpLeaseFileWithBackupLeaseFile()V
    .registers 8

    #@0
    .prologue
    .line 637
    const-string v4, "dhcp.ap.macaddress"

    #@2
    const-string v5, ""

    #@4
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 638
    iget-object v4, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@9
    const-string/jumbo v5, "wifi"

    #@c
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Landroid/net/wifi/WifiManager;

    #@12
    .line 639
    .local v3, wifiMngr:Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@15
    move-result-object v0

    #@16
    .line 640
    .local v0, currentWifiInfo:Landroid/net/wifi/WifiInfo;
    if-nez v0, :cond_19

    #@18
    .line 657
    :goto_18
    return-void

    #@19
    .line 644
    :cond_19
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 645
    .local v1, strBssid:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 646
    .local v2, strSsid:Ljava/lang/String;
    if-eqz v1, :cond_31

    #@23
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@26
    move-result v4

    #@27
    if-nez v4, :cond_31

    #@29
    if-eqz v2, :cond_31

    #@2b
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    #@2e
    move-result v4

    #@2f
    if-eqz v4, :cond_39

    #@31
    .line 648
    :cond_31
    const-string v4, "DhcpStateMachine"

    #@33
    const-string v5, "[bssid + ssid] is null. Return."

    #@35
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_18

    #@39
    .line 652
    :cond_39
    const-string v4, "DhcpStateMachine"

    #@3b
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v6, "dhcp.ap.macaddress = "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 654
    const-string v4, "dhcp.ap.macaddress"

    #@53
    invoke-static {v4, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    goto :goto_18
.end method

.method private StoreDhcpBackupLeaseFileWithCurrentLeaseFile()V
    .registers 3

    #@0
    .prologue
    .line 632
    const-string v0, "dhcp.ap.macaddress"

    #@2
    const-string v1, ""

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 633
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->CheckDhcpDirectory()Z

    #@a
    .line 634
    return-void
.end method

.method static synthetic access$000(Landroid/net/DhcpStateMachine;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/DhcpStateMachine;)Landroid/content/BroadcastReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/net/DhcpStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/net/DhcpStateMachine;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewalIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/net/DhcpStateMachine;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/net/DhcpStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeRenewalState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/net/DhcpStateMachine;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/net/DhcpStateMachine;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/net/DhcpStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-boolean v0, p0, Landroid/net/DhcpStateMachine;->mRegisteredForPreDhcpNotification:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/net/DhcpStateMachine;)Lcom/android/internal/util/StateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/net/DhcpStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/net/DhcpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0, p1}, Landroid/net/DhcpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/net/DhcpStateMachine;Landroid/net/DhcpStateMachine$DhcpAction;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Landroid/net/DhcpStateMachine;->runDhcp(Landroid/net/DhcpStateMachine$DhcpAction;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Landroid/net/DhcpStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mRunningState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method private checkSustainWithDefaultDhcpInfo(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 26
    .parameter "strBssid"
    .parameter "key"

    #@0
    .prologue
    .line 997
    const/4 v14, 0x0

    #@1
    .line 998
    .local v14, result:Z
    const-string v17, "00:0a:eb"

    #@3
    .line 999
    .local v17, strDefaultDhcpBssid:Ljava/lang/String;
    const/4 v15, 0x0

    #@4
    .line 1000
    .local v15, sbIpAddress:Ljava/lang/StringBuffer;
    const-string v16, "192.168.2.1"

    #@6
    .line 1001
    .local v16, serverAddress:Ljava/lang/String;
    const/16 v12, 0x18

    #@8
    .line 1005
    .local v12, prefixLength:I
    const-string v19, "dhcp.failinfo.defaultdhcp"

    #@a
    invoke-static/range {v19 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v18

    #@e
    .line 1006
    .local v18, strDefaultDhcpInfo:Ljava/lang/String;
    if-eqz v18, :cond_67

    #@10
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    #@13
    move-result v19

    #@14
    if-nez v19, :cond_67

    #@16
    .line 1008
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@19
    move-result-object v18

    #@1a
    .line 1009
    const-string v19, ","

    #@1c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1f
    move-result-object v6

    #@20
    .line 1012
    .local v6, data:[Ljava/lang/String;
    array-length v0, v6

    #@21
    move/from16 v19, v0

    #@23
    const/16 v20, 0x4

    #@25
    move/from16 v0, v19

    #@27
    move/from16 v1, v20

    #@29
    if-ne v0, v1, :cond_67

    #@2b
    .line 1014
    const-string v19, "DhcpStateMachine"

    #@2d
    const-string v20, "Set Default DhcpInfoInternal from properties"

    #@2f
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1016
    const/16 v19, 0x0

    #@34
    aget-object v19, v6, v19

    #@36
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@39
    move-result-object v19

    #@3a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@3d
    move-result-object v17

    #@3e
    .line 1018
    new-instance v15, Ljava/lang/StringBuffer;

    #@40
    .end local v15           #sbIpAddress:Ljava/lang/StringBuffer;
    const/16 v19, 0x1

    #@42
    aget-object v19, v6, v19

    #@44
    move-object/from16 v0, v19

    #@46
    invoke-direct {v15, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@49
    .line 1019
    .restart local v15       #sbIpAddress:Ljava/lang/StringBuffer;
    new-instance v19, Ljava/util/Random;

    #@4b
    invoke-direct/range {v19 .. v19}, Ljava/util/Random;-><init>()V

    #@4e
    const/16 v20, 0x4f

    #@50
    invoke-virtual/range {v19 .. v20}, Ljava/util/Random;->nextInt(I)I

    #@53
    move-result v19

    #@54
    add-int/lit8 v19, v19, 0x79

    #@56
    move/from16 v0, v19

    #@58
    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@5b
    .line 1021
    const/16 v19, 0x2

    #@5d
    aget-object v16, v6, v19

    #@5f
    .line 1023
    const/16 v19, 0x3

    #@61
    :try_start_61
    aget-object v19, v6, v19

    #@63
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_66
    .catch Ljava/lang/NumberFormatException; {:try_start_61 .. :try_end_66} :catch_190

    #@66
    move-result v12

    #@67
    .line 1029
    .end local v6           #data:[Ljava/lang/String;
    :cond_67
    :goto_67
    if-eqz p1, :cond_7d

    #@69
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    #@6c
    move-result v19

    #@6d
    if-nez v19, :cond_7d

    #@6f
    const/16 v19, 0x0

    #@71
    move-object/from16 v0, p1

    #@73
    move-object/from16 v1, v17

    #@75
    move/from16 v2, v19

    #@77
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@7a
    move-result v19

    #@7b
    if-nez v19, :cond_80

    #@7d
    .line 1030
    :cond_7d
    const/16 v19, 0x0

    #@7f
    .line 1083
    :goto_7f
    return v19

    #@80
    .line 1033
    :cond_80
    if-nez v15, :cond_9d

    #@82
    .line 1034
    new-instance v15, Ljava/lang/StringBuffer;

    #@84
    .end local v15           #sbIpAddress:Ljava/lang/StringBuffer;
    const-string v19, "192.168.2."

    #@86
    move-object/from16 v0, v19

    #@88
    invoke-direct {v15, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@8b
    .line 1035
    .restart local v15       #sbIpAddress:Ljava/lang/StringBuffer;
    new-instance v19, Ljava/util/Random;

    #@8d
    invoke-direct/range {v19 .. v19}, Ljava/util/Random;-><init>()V

    #@90
    const/16 v20, 0x4f

    #@92
    invoke-virtual/range {v19 .. v20}, Ljava/util/Random;->nextInt(I)I

    #@95
    move-result v19

    #@96
    add-int/lit8 v19, v19, 0x79

    #@98
    move/from16 v0, v19

    #@9a
    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@9d
    .line 1039
    :cond_9d
    new-instance v4, Landroid/net/DhcpInfoInternal;

    #@9f
    invoke-direct {v4}, Landroid/net/DhcpInfoInternal;-><init>()V

    #@a2
    .line 1040
    .local v4, DefaultAddDhcpInfo:Landroid/net/DhcpInfoInternal;
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@a5
    move-result-object v19

    #@a6
    move-object/from16 v0, v19

    #@a8
    iput-object v0, v4, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@aa
    .line 1041
    iput v12, v4, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@ac
    .line 1042
    const-string v19, "8.8.8.8"

    #@ae
    move-object/from16 v0, v19

    #@b0
    iput-object v0, v4, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@b2
    .line 1043
    const-string v19, "8.8.4.4"

    #@b4
    move-object/from16 v0, v19

    #@b6
    iput-object v0, v4, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@b8
    .line 1044
    move-object/from16 v0, v16

    #@ba
    iput-object v0, v4, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@bc
    .line 1045
    const v19, 0x15180

    #@bf
    move/from16 v0, v19

    #@c1
    iput v0, v4, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@c3
    .line 1047
    iget-object v0, v4, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@c5
    move-object/from16 v19, v0

    #@c7
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@ca
    move-result-object v8

    #@cb
    .line 1048
    .local v8, ia:Ljava/net/InetAddress;
    if-eqz v8, :cond_d9

    #@cd
    .line 1049
    new-instance v19, Landroid/net/RouteInfo;

    #@cf
    move-object/from16 v0, v19

    #@d1
    invoke-direct {v0, v8}, Landroid/net/RouteInfo;-><init>(Ljava/net/InetAddress;)V

    #@d4
    move-object/from16 v0, v19

    #@d6
    invoke-virtual {v4, v0}, Landroid/net/DhcpInfoInternal;->addRoute(Landroid/net/RouteInfo;)V

    #@d9
    .line 1052
    :cond_d9
    const-string/jumbo v19, "network_management"

    #@dc
    invoke-static/range {v19 .. v19}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@df
    move-result-object v5

    #@e0
    .line 1053
    .local v5, b:Landroid/os/IBinder;
    invoke-static {v5}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@e3
    move-result-object v11

    #@e4
    .line 1054
    .local v11, netd:Landroid/os/INetworkManagementService;
    if-eqz v11, :cond_188

    #@e6
    .line 1055
    new-instance v9, Landroid/net/InterfaceConfiguration;

    #@e8
    invoke-direct {v9}, Landroid/net/InterfaceConfiguration;-><init>()V

    #@eb
    .line 1056
    .local v9, ifcg:Landroid/net/InterfaceConfiguration;
    invoke-virtual {v4}, Landroid/net/DhcpInfoInternal;->makeLinkAddress()Landroid/net/LinkAddress;

    #@ee
    move-result-object v19

    #@ef
    move-object/from16 v0, v19

    #@f1
    invoke-virtual {v9, v0}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@f4
    .line 1057
    invoke-virtual {v9}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@f7
    .line 1060
    :try_start_f7
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@fb
    move-object/from16 v19, v0

    #@fd
    move-object/from16 v0, v19

    #@ff
    invoke-interface {v11, v0, v9}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@102
    .line 1061
    const-string v19, "DhcpStateMachine"

    #@104
    new-instance v20, Ljava/lang/StringBuilder;

    #@106
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v21, "checkSustainWithDefaultDhcpInfo: IP configuration succeeded: "

    #@10b
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v20

    #@10f
    invoke-virtual {v4}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    #@112
    move-result-object v21

    #@113
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v20

    #@117
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a
    move-result-object v20

    #@11b
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    .line 1064
    move-object/from16 v0, p0

    #@120
    move-object/from16 v1, p2

    #@122
    invoke-direct {v0, v1, v4}, Landroid/net/DhcpStateMachine;->AddDhcpInfoCache(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)V

    #@125
    .line 1066
    move-object/from16 v0, p0

    #@127
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@129
    move-object/from16 v19, v0

    #@12b
    const v20, 0x30005

    #@12e
    const/16 v21, 0x1

    #@130
    const/16 v22, 0x0

    #@132
    move-object/from16 v0, v19

    #@134
    move/from16 v1, v20

    #@136
    move/from16 v2, v21

    #@138
    move/from16 v3, v22

    #@13a
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@13d
    move-result-object v10

    #@13e
    .line 1067
    .local v10, msg:Landroid/os/Message;
    if-eqz v10, :cond_148

    #@140
    .line 1068
    invoke-virtual {v10}, Landroid/os/Message;->sendToTarget()V

    #@143
    .line 1072
    :goto_143
    const/4 v14, 0x1

    #@144
    .end local v9           #ifcg:Landroid/net/InterfaceConfiguration;
    .end local v10           #msg:Landroid/os/Message;
    :goto_144
    move/from16 v19, v14

    #@146
    .line 1083
    goto/16 :goto_7f

    #@148
    .line 1070
    .restart local v9       #ifcg:Landroid/net/InterfaceConfiguration;
    .restart local v10       #msg:Landroid/os/Message;
    :cond_148
    const-string v19, "DhcpStateMachine"

    #@14a
    const-string v20, "checkSustainWithDefaultDhcpInfo: msg is null"

    #@14c
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14f
    .catch Landroid/os/RemoteException; {:try_start_f7 .. :try_end_14f} :catch_150
    .catch Ljava/lang/IllegalStateException; {:try_start_f7 .. :try_end_14f} :catch_16c

    #@14f
    goto :goto_143

    #@150
    .line 1074
    .end local v10           #msg:Landroid/os/Message;
    :catch_150
    move-exception v13

    #@151
    .line 1075
    .local v13, re:Landroid/os/RemoteException;
    const-string v19, "DhcpStateMachine"

    #@153
    new-instance v20, Ljava/lang/StringBuilder;

    #@155
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    const-string v21, "checkSustainWithDefaultDhcpInfo: IP configuration failed: "

    #@15a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v20

    #@15e
    move-object/from16 v0, v20

    #@160
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v20

    #@164
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@167
    move-result-object v20

    #@168
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16b
    goto :goto_144

    #@16c
    .line 1076
    .end local v13           #re:Landroid/os/RemoteException;
    :catch_16c
    move-exception v7

    #@16d
    .line 1077
    .local v7, e:Ljava/lang/IllegalStateException;
    const-string v19, "DhcpStateMachine"

    #@16f
    new-instance v20, Ljava/lang/StringBuilder;

    #@171
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@174
    const-string v21, "checkSustainWithDefaultDhcpInfo: IP configuration failed: "

    #@176
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v20

    #@17a
    move-object/from16 v0, v20

    #@17c
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v20

    #@180
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183
    move-result-object v20

    #@184
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@187
    goto :goto_144

    #@188
    .line 1080
    .end local v7           #e:Ljava/lang/IllegalStateException;
    .end local v9           #ifcg:Landroid/net/InterfaceConfiguration;
    :cond_188
    const-string v19, "DhcpStateMachine"

    #@18a
    const-string v20, "checkSustainWithDefaultDhcpInfo: netd is null"

    #@18c
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18f
    goto :goto_144

    #@190
    .line 1024
    .end local v4           #DefaultAddDhcpInfo:Landroid/net/DhcpInfoInternal;
    .end local v5           #b:Landroid/os/IBinder;
    .end local v8           #ia:Ljava/net/InetAddress;
    .end local v11           #netd:Landroid/os/INetworkManagementService;
    .restart local v6       #data:[Ljava/lang/String;
    :catch_190
    move-exception v19

    #@191
    goto/16 :goto_67
.end method

.method private getDLNAEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 570
    const/4 v0, 0x0

    #@1
    .line 571
    .local v0, bRet:Z
    const-string v2, "dhcp.dlna.using"

    #@3
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 573
    .local v1, str:Ljava/lang/String;
    if-eqz v1, :cond_13

    #@9
    .line 574
    const-string/jumbo v2, "true"

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_13

    #@12
    .line 575
    const/4 v0, 0x1

    #@13
    .line 579
    :cond_13
    return v0
.end method

.method private getIpAddressToInt(Ljava/lang/String;)I
    .registers 5
    .parameter "addrString"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 583
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 591
    :cond_9
    :goto_9
    return v1

    #@a
    .line 587
    :cond_a
    invoke-static {p1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@d
    move-result-object v0

    #@e
    .line 588
    .local v0, addr:Ljava/net/InetAddress;
    if-eqz v0, :cond_9

    #@10
    instance-of v2, v0, Ljava/net/Inet6Address;

    #@12
    if-nez v2, :cond_9

    #@14
    .line 591
    invoke-static {v0}, Landroid/net/NetworkUtils;->inetAddressToInt(Ljava/net/InetAddress;)I

    #@17
    move-result v1

    #@18
    goto :goto_9
.end method

.method private getSustainFailedDhcpInfoCacheDisabled()Z
    .registers 4

    #@0
    .prologue
    .line 974
    const/4 v0, 0x0

    #@1
    .line 975
    .local v0, bRet:Z
    const-string v2, "dhcp.failinfo.notuse"

    #@3
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 977
    .local v1, str:Ljava/lang/String;
    if-eqz v1, :cond_12

    #@9
    .line 978
    const-string v2, "YES"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_12

    #@11
    .line 979
    const/4 v0, 0x1

    #@12
    .line 983
    :cond_12
    return v0
.end method

.method public static makeDhcpStateMachine(Landroid/content/Context;Lcom/android/internal/util/StateMachine;Ljava/lang/String;)Landroid/net/DhcpStateMachine;
    .registers 4
    .parameter "context"
    .parameter "controller"
    .parameter "intf"

    #@0
    .prologue
    .line 217
    new-instance v0, Landroid/net/DhcpStateMachine;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/net/DhcpStateMachine;-><init>(Landroid/content/Context;Lcom/android/internal/util/StateMachine;Ljava/lang/String;)V

    #@5
    .line 218
    .local v0, dsm:Landroid/net/DhcpStateMachine;
    invoke-virtual {v0}, Landroid/net/DhcpStateMachine;->start()V

    #@8
    .line 219
    return-object v0
.end method

.method private runDhcp(Landroid/net/DhcpStateMachine$DhcpAction;)Z
    .registers 14
    .parameter "dhcpAction"

    #@0
    .prologue
    .line 438
    const/4 v5, 0x0

    #@1
    .line 439
    .local v5, success:Z
    new-instance v1, Landroid/net/DhcpInfoInternal;

    #@3
    invoke-direct {v1}, Landroid/net/DhcpInfoInternal;-><init>()V

    #@6
    .line 440
    .local v1, dhcpInfoInternal:Landroid/net/DhcpInfoInternal;
    const/4 v0, 0x0

    #@7
    .line 443
    .local v0, IsWEPSec:Z
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@9
    if-eqz v6, :cond_15

    #@b
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@d
    if-eqz v6, :cond_15

    #@f
    .line 444
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@11
    invoke-interface {v6}, Lcom/lge/wifi_iface/WifiServiceExtIface;->IsWEPSecurity()Z

    #@14
    move-result v0

    #@15
    .line 451
    :cond_15
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@17
    if-eqz v6, :cond_2d

    #@19
    .line 452
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1b
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@1e
    move-result v6

    #@1f
    if-nez v6, :cond_2d

    #@21
    .line 453
    const-string v6, "DhcpStateMachine"

    #@23
    const-string v7, "DHCP Start wake lock is acquired."

    #@25
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 454
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2a
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@2d
    .line 458
    :cond_2d
    sget-object v6, Landroid/net/DhcpStateMachine$DhcpAction;->START:Landroid/net/DhcpStateMachine$DhcpAction;

    #@2f
    if-ne p1, v6, :cond_107

    #@31
    .line 460
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@33
    invoke-static {v6}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@36
    .line 461
    const-string v6, "DhcpStateMachine"

    #@38
    new-instance v7, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v8, "DHCP request on "

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    iget-object v8, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@45
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v7

    #@4d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 463
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@52
    if-eqz v6, :cond_57

    #@54
    .line 464
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->ReplaceDhcpLeaseFileWithBackupLeaseFile()V

    #@57
    .line 467
    :cond_57
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@59
    invoke-static {v6, v1}, Landroid/net/NetworkUtils;->runDhcp(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)Z

    #@5c
    move-result v5

    #@5d
    .line 468
    iput-object v1, p0, Landroid/net/DhcpStateMachine;->mDhcpInfo:Landroid/net/DhcpInfoInternal;

    #@5f
    .line 475
    :cond_5f
    :goto_5f
    if-eqz v5, :cond_143

    #@61
    .line 476
    const-string v6, "DhcpStateMachine"

    #@63
    new-instance v7, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v8, "DHCP succeeded on "

    #@6a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    iget-object v8, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@70
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v7

    #@74
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v7

    #@78
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 483
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWifiDLNA()Z

    #@7e
    move-result v6

    #@7f
    if-eqz v6, :cond_af

    #@81
    .line 484
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->getDLNAEnabled()Z

    #@84
    move-result v6

    #@85
    const/4 v7, 0x1

    #@86
    if-ne v6, v7, :cond_af

    #@88
    iget-object v6, v1, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@8a
    invoke-direct {p0, v6}, Landroid/net/DhcpStateMachine;->getIpAddressToInt(Ljava/lang/String;)I

    #@8d
    move-result v6

    #@8e
    const v7, 0xffff

    #@91
    and-int/2addr v6, v7

    #@92
    const v7, 0xfea9

    #@95
    if-ne v6, v7, :cond_af

    #@97
    .line 485
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@99
    new-instance v7, Landroid/content/Intent;

    #@9b
    const-string v8, "android.net.wifi.AUTOIP_CONNECTION_NOTIFICATION"

    #@9d
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a0
    invoke-virtual {v6, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@a3
    .line 486
    const v6, 0xa8c00

    #@a6
    iput v6, v1, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@a8
    .line 487
    const-string v6, "DhcpStateMachine"

    #@aa
    const-string v7, "AUTOIP is allowed for DLNA"

    #@ac
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    .line 492
    :cond_af
    iget v6, v1, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@b1
    int-to-long v2, v6

    #@b2
    .line 495
    .local v2, leaseDuration:J
    const-wide/16 v6, 0x0

    #@b4
    cmp-long v6, v2, v6

    #@b6
    if-ltz v6, :cond_d0

    #@b8
    .line 498
    const-wide/16 v6, 0x12c

    #@ba
    cmp-long v6, v2, v6

    #@bc
    if-gez v6, :cond_c0

    #@be
    .line 499
    const-wide/16 v2, 0x12c

    #@c0
    .line 504
    :cond_c0
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mAlarmManager:Landroid/app/AlarmManager;

    #@c2
    const/4 v7, 0x2

    #@c3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c6
    move-result-wide v8

    #@c7
    const-wide/16 v10, 0x1e0

    #@c9
    mul-long/2addr v10, v2

    #@ca
    add-long/2addr v8, v10

    #@cb
    iget-object v10, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewalIntent:Landroid/app/PendingIntent;

    #@cd
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@d0
    .line 511
    :cond_d0
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@d2
    if-eqz v6, :cond_132

    #@d4
    .line 513
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->StoreDhcpBackupLeaseFileWithCurrentLeaseFile()V

    #@d7
    .line 517
    invoke-direct {p0, v1}, Landroid/net/DhcpStateMachine;->AddDhcpInfoCache(Landroid/net/DhcpInfoInternal;)Z

    #@da
    move-result v6

    #@db
    const/4 v7, 0x1

    #@dc
    if-ne v6, v7, :cond_ee

    #@de
    .line 518
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@e0
    const v7, 0x30005

    #@e3
    const/4 v8, 0x1

    #@e4
    const/4 v9, 0x0

    #@e5
    invoke-virtual {v6, v7, v8, v9, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@e8
    move-result-object v4

    #@e9
    .line 519
    .local v4, msg:Landroid/os/Message;
    if-eqz v4, :cond_ee

    #@eb
    .line 520
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@ee
    .line 555
    .end local v2           #leaseDuration:J
    .end local v4           #msg:Landroid/os/Message;
    :cond_ee
    :goto_ee
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@f0
    if-eqz v6, :cond_106

    #@f2
    .line 556
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@f4
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@f7
    move-result v6

    #@f8
    if-eqz v6, :cond_106

    #@fa
    .line 557
    const-string v6, "DhcpStateMachine"

    #@fc
    const-string v7, "DHCP Start/Renew wake lock is released."

    #@fe
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 558
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mDhcpRenewWakeLock:Landroid/os/PowerManager$WakeLock;

    #@103
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    #@106
    .line 561
    :cond_106
    return v5

    #@107
    .line 469
    :cond_107
    sget-object v6, Landroid/net/DhcpStateMachine$DhcpAction;->RENEW:Landroid/net/DhcpStateMachine$DhcpAction;

    #@109
    if-ne p1, v6, :cond_5f

    #@10b
    .line 470
    const-string v6, "DhcpStateMachine"

    #@10d
    new-instance v7, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v8, "DHCP renewal on "

    #@114
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v7

    #@118
    iget-object v8, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@11a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v7

    #@11e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v7

    #@122
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 471
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@127
    invoke-static {v6, v1}, Landroid/net/NetworkUtils;->runDhcpRenew(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)Z

    #@12a
    move-result v5

    #@12b
    .line 472
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mDhcpInfo:Landroid/net/DhcpInfoInternal;

    #@12d
    invoke-virtual {v1, v6}, Landroid/net/DhcpInfoInternal;->updateFromDhcpRequest(Landroid/net/DhcpInfoInternal;)V

    #@130
    goto/16 :goto_5f

    #@132
    .line 525
    .restart local v2       #leaseDuration:J
    :cond_132
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@134
    const v7, 0x30005

    #@137
    const/4 v8, 0x1

    #@138
    const/4 v9, 0x0

    #@139
    invoke-virtual {v6, v7, v8, v9, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@13c
    move-result-object v4

    #@13d
    .line 526
    .restart local v4       #msg:Landroid/os/Message;
    if-eqz v4, :cond_ee

    #@13f
    .line 527
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    #@142
    goto :goto_ee

    #@143
    .line 533
    .end local v2           #leaseDuration:J
    .end local v4           #msg:Landroid/os/Message;
    :cond_143
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@145
    if-eqz v6, :cond_159

    #@147
    const-string v6, "KR"

    #@149
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@14c
    move-result-object v7

    #@14d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@150
    move-result v6

    #@151
    if-eqz v6, :cond_159

    #@153
    .line 534
    if-nez v0, :cond_159

    #@155
    .line 535
    invoke-direct {p0, p1}, Landroid/net/DhcpStateMachine;->CheckSustainCurrentDhcpInfoCache(Landroid/net/DhcpStateMachine$DhcpAction;)Z

    #@158
    move-result v5

    #@159
    .line 539
    :cond_159
    if-nez v5, :cond_ee

    #@15b
    .line 541
    const-string v6, "DhcpStateMachine"

    #@15d
    new-instance v7, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v8, "DHCP failed on "

    #@164
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v7

    #@168
    iget-object v8, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@16a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v7

    #@16e
    const-string v8, ": "

    #@170
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v7

    #@174
    invoke-static {}, Landroid/net/NetworkUtils;->getDhcpError()Ljava/lang/String;

    #@177
    move-result-object v8

    #@178
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v7

    #@17c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17f
    move-result-object v7

    #@180
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@183
    .line 543
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@185
    invoke-static {v6}, Landroid/net/NetworkUtils;->stopDhcp(Ljava/lang/String;)Z

    #@188
    .line 544
    invoke-direct {p0}, Landroid/net/DhcpStateMachine;->RemoveDhcpInfoCache()V

    #@18b
    .line 546
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@18d
    const-string/jumbo v7, "p2p"

    #@190
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@193
    move-result v6

    #@194
    const/4 v7, 0x1

    #@195
    if-ne v6, v7, :cond_19b

    #@197
    sget-object v6, Landroid/net/DhcpStateMachine$DhcpAction;->RENEW:Landroid/net/DhcpStateMachine$DhcpAction;

    #@199
    if-eq p1, v6, :cond_ee

    #@19b
    .line 547
    :cond_19b
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@19d
    const v7, 0x30005

    #@1a0
    const/4 v8, 0x2

    #@1a1
    const/4 v9, 0x0

    #@1a2
    invoke-virtual {v6, v7, v8, v9}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    #@1a5
    move-result-object v6

    #@1a6
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    #@1a9
    goto/16 :goto_ee
.end method

.method private setInterfaceDownUpWithDhcpInfo(Landroid/net/DhcpInfoInternal;Landroid/net/DhcpInfoInternal;)Z
    .registers 12
    .parameter "DhcpInfoOLD"
    .parameter "DhcpInfoNEW"

    #@0
    .prologue
    .line 1089
    const/4 v5, 0x0

    #@1
    .line 1091
    .local v5, result:Z
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@3
    if-eqz v6, :cond_22

    #@5
    invoke-virtual {p0}, Landroid/net/DhcpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@8
    move-result-object v6

    #@9
    iget-object v7, p0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@b
    if-eq v6, v7, :cond_22

    #@d
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@f
    if-eqz v6, :cond_22

    #@11
    invoke-virtual {p0}, Landroid/net/DhcpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@14
    move-result-object v6

    #@15
    iget-object v7, p0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@17
    if-eq v6, v7, :cond_22

    #@19
    .line 1094
    const-string v6, "DhcpStateMachine"

    #@1b
    const-string v7, "Don\'t need to do setInterfaceDownUpWithDhcpInfo, Current State is not mWaitBeforeStartState."

    #@1d
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1096
    const/4 v6, 0x0

    #@21
    .line 1119
    :goto_21
    return v6

    #@22
    .line 1099
    :cond_22
    const-string/jumbo v6, "network_management"

    #@25
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@28
    move-result-object v0

    #@29
    .line 1100
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@2c
    move-result-object v3

    #@2d
    .line 1101
    .local v3, netd:Landroid/os/INetworkManagementService;
    if-eqz v3, :cond_96

    #@2f
    .line 1102
    new-instance v2, Landroid/net/InterfaceConfiguration;

    #@31
    invoke-direct {v2}, Landroid/net/InterfaceConfiguration;-><init>()V

    #@34
    .line 1103
    .local v2, ifcg:Landroid/net/InterfaceConfiguration;
    invoke-virtual {p2}, Landroid/net/DhcpInfoInternal;->makeLinkAddress()Landroid/net/LinkAddress;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v2, v6}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@3b
    .line 1107
    :try_start_3b
    iget-object v6, p0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@3d
    invoke-interface {v3, v6, v2}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@40
    .line 1108
    const-string v6, "DhcpStateMachine"

    #@42
    new-instance v7, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string/jumbo v8, "setInterfaceDownUpWithDhcpInfo: IP configuration succeeded: "

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    invoke-virtual {p2}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    #@51
    move-result-object v8

    #@52
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v7

    #@56
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v7

    #@5a
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5d
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_5d} :catch_60
    .catch Ljava/lang/IllegalStateException; {:try_start_3b .. :try_end_5d} :catch_7b

    #@5d
    .line 1109
    const/4 v5, 0x1

    #@5e
    .end local v2           #ifcg:Landroid/net/InterfaceConfiguration;
    :goto_5e
    move v6, v5

    #@5f
    .line 1119
    goto :goto_21

    #@60
    .line 1110
    .restart local v2       #ifcg:Landroid/net/InterfaceConfiguration;
    :catch_60
    move-exception v4

    #@61
    .line 1111
    .local v4, re:Landroid/os/RemoteException;
    const-string v6, "DhcpStateMachine"

    #@63
    new-instance v7, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string/jumbo v8, "setInterfaceDownUpWithDhcpInfo: IP configuration failed: "

    #@6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_5e

    #@7b
    .line 1112
    .end local v4           #re:Landroid/os/RemoteException;
    :catch_7b
    move-exception v1

    #@7c
    .line 1113
    .local v1, e:Ljava/lang/IllegalStateException;
    const-string v6, "DhcpStateMachine"

    #@7e
    new-instance v7, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string/jumbo v8, "setInterfaceDownUpWithDhcpInfo: IP configuration failed: "

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v7

    #@8a
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v7

    #@8e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v7

    #@92
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    goto :goto_5e

    #@96
    .line 1116
    .end local v1           #e:Ljava/lang/IllegalStateException;
    .end local v2           #ifcg:Landroid/net/InterfaceConfiguration;
    :cond_96
    const-string v6, "DhcpStateMachine"

    #@98
    const-string/jumbo v7, "setInterfaceDownUpWithDhcpInfo: netd is null"

    #@9b
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    goto :goto_5e
.end method


# virtual methods
.method public CheckDhcpInfoCacheList(Landroid/util/LruCache;)Z
    .registers 23
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/DhcpInfoInternal;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 663
    .local p1, dhcpInfoCacheList:Landroid/util/LruCache;,"Landroid/util/LruCache<Ljava/lang/String;Landroid/net/DhcpInfoInternal;>;"
    const/4 v15, 0x0

    #@1
    .line 664
    .local v15, valid:Z
    const/4 v4, 0x0

    #@2
    .line 666
    .local v4, DhcpInfoCache:Landroid/net/DhcpInfoInternal;
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@6
    move-object/from16 v17, v0

    #@8
    if-eqz v17, :cond_3c

    #@a
    invoke-virtual/range {p0 .. p0}, Landroid/net/DhcpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@d
    move-result-object v17

    #@e
    move-object/from16 v0, p0

    #@10
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mWaitBeforeStartState:Lcom/android/internal/util/State;

    #@12
    move-object/from16 v18, v0

    #@14
    move-object/from16 v0, v17

    #@16
    move-object/from16 v1, v18

    #@18
    if-eq v0, v1, :cond_3c

    #@1a
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@1e
    move-object/from16 v17, v0

    #@20
    if-eqz v17, :cond_3c

    #@22
    invoke-virtual/range {p0 .. p0}, Landroid/net/DhcpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@25
    move-result-object v17

    #@26
    move-object/from16 v0, p0

    #@28
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mStoppedState:Lcom/android/internal/util/State;

    #@2a
    move-object/from16 v18, v0

    #@2c
    move-object/from16 v0, v17

    #@2e
    move-object/from16 v1, v18

    #@30
    if-eq v0, v1, :cond_3c

    #@32
    .line 669
    const-string v17, "DhcpStateMachine"

    #@34
    const-string v18, "Don\'t need to check [bssid + ssid] hash key, Current State is not mWaitBeforeStartState."

    #@36
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 671
    const/16 v17, 0x0

    #@3b
    .line 743
    :goto_3b
    return v17

    #@3c
    .line 674
    :cond_3c
    if-nez p1, :cond_51

    #@3e
    .line 675
    new-instance p1, Landroid/util/LruCache;

    #@40
    .end local p1           #dhcpInfoCacheList:Landroid/util/LruCache;,"Landroid/util/LruCache<Ljava/lang/String;Landroid/net/DhcpInfoInternal;>;"
    const/16 v17, 0x41

    #@42
    move-object/from16 v0, p1

    #@44
    move/from16 v1, v17

    #@46
    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    #@49
    .line 677
    .restart local p1       #dhcpInfoCacheList:Landroid/util/LruCache;,"Landroid/util/LruCache<Ljava/lang/String;Landroid/net/DhcpInfoInternal;>;"
    const-string v17, "DhcpStateMachine"

    #@4b
    const-string/jumbo v18, "setDhcpInfoCacheList Error : dhcpInfoCacheList is NULL "

    #@4e
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 680
    :cond_51
    move-object/from16 v0, p1

    #@53
    move-object/from16 v1, p0

    #@55
    iput-object v0, v1, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@57
    .line 682
    move-object/from16 v0, p0

    #@59
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@5b
    move-object/from16 v17, v0

    #@5d
    if-nez v17, :cond_69

    #@5f
    .line 684
    const-string v17, "DhcpStateMachine"

    #@61
    const-string v18, "Fatal Error! mDhcpInfoCacheList is null"

    #@63
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 686
    const/16 v17, 0x0

    #@68
    goto :goto_3b

    #@69
    .line 689
    :cond_69
    move-object/from16 v0, p0

    #@6b
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mContext:Landroid/content/Context;

    #@6d
    move-object/from16 v17, v0

    #@6f
    const-string/jumbo v18, "wifi"

    #@72
    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@75
    move-result-object v16

    #@76
    check-cast v16, Landroid/net/wifi/WifiManager;

    #@78
    .line 690
    .local v16, wifiMngr:Landroid/net/wifi/WifiManager;
    invoke-virtual/range {v16 .. v16}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@7b
    move-result-object v6

    #@7c
    .line 691
    .local v6, currentWifiInfo:Landroid/net/wifi/WifiInfo;
    if-nez v6, :cond_81

    #@7e
    .line 692
    const/16 v17, 0x0

    #@80
    goto :goto_3b

    #@81
    .line 695
    :cond_81
    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    #@84
    move-result-object v13

    #@85
    .line 696
    .local v13, strBssid:Ljava/lang/String;
    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@88
    move-result-object v14

    #@89
    .line 697
    .local v14, strSsid:Ljava/lang/String;
    if-eqz v13, :cond_99

    #@8b
    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    #@8e
    move-result v17

    #@8f
    if-nez v17, :cond_99

    #@91
    if-eqz v14, :cond_99

    #@93
    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    #@96
    move-result v17

    #@97
    if-eqz v17, :cond_9c

    #@99
    .line 698
    :cond_99
    const/16 v17, 0x0

    #@9b
    goto :goto_3b

    #@9c
    .line 701
    :cond_9c
    new-instance v17, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    move-object/from16 v0, v17

    #@a3
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v17

    #@a7
    move-object/from16 v0, v17

    #@a9
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v17

    #@ad
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v9

    #@b1
    .line 703
    .local v9, key:Ljava/lang/String;
    const-string v17, "DhcpStateMachine"

    #@b3
    new-instance v18, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v19, "[bssid + ssid] hash key :"

    #@ba
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v18

    #@be
    move-object/from16 v0, v18

    #@c0
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v18

    #@c4
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v18

    #@c8
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    .line 706
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@cf
    move-object/from16 v17, v0

    #@d1
    move-object/from16 v0, v17

    #@d3
    invoke-virtual {v0, v9}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d6
    move-result-object v4

    #@d7
    .end local v4           #DhcpInfoCache:Landroid/net/DhcpInfoInternal;
    check-cast v4, Landroid/net/DhcpInfoInternal;

    #@d9
    .line 708
    .restart local v4       #DhcpInfoCache:Landroid/net/DhcpInfoInternal;
    if-eqz v4, :cond_17b

    #@db
    .line 709
    const-string/jumbo v17, "network_management"

    #@de
    invoke-static/range {v17 .. v17}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@e1
    move-result-object v5

    #@e2
    .line 710
    .local v5, b:Landroid/os/IBinder;
    invoke-static {v5}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@e5
    move-result-object v11

    #@e6
    .line 711
    .local v11, netd:Landroid/os/INetworkManagementService;
    new-instance v8, Landroid/net/InterfaceConfiguration;

    #@e8
    invoke-direct {v8}, Landroid/net/InterfaceConfiguration;-><init>()V

    #@eb
    .line 714
    .local v8, ifcg:Landroid/net/InterfaceConfiguration;
    invoke-virtual {v4}, Landroid/net/DhcpInfoInternal;->makeLinkAddress()Landroid/net/LinkAddress;

    #@ee
    move-result-object v17

    #@ef
    move-object/from16 v0, v17

    #@f1
    invoke-virtual {v8, v0}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@f4
    .line 715
    invoke-virtual {v8}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@f7
    .line 716
    if-eqz v11, :cond_13f

    #@f9
    .line 718
    :try_start_f9
    move-object/from16 v0, p0

    #@fb
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mInterfaceName:Ljava/lang/String;

    #@fd
    move-object/from16 v17, v0

    #@ff
    move-object/from16 v0, v17

    #@101
    invoke-interface {v11, v0, v8}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@104
    .line 720
    const-string v17, "DhcpStateMachine"

    #@106
    new-instance v18, Ljava/lang/StringBuilder;

    #@108
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v19, "CheckDhcpInfoCacheList: IP configuration succeeded: "

    #@10d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v18

    #@111
    invoke-virtual {v4}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    #@114
    move-result-object v19

    #@115
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v18

    #@119
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v18

    #@11d
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@120
    .line 722
    move-object/from16 v0, p0

    #@122
    iget-object v0, v0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@124
    move-object/from16 v17, v0

    #@126
    const v18, 0x30005

    #@129
    const/16 v19, 0x1

    #@12b
    const/16 v20, 0x0

    #@12d
    move-object/from16 v0, v17

    #@12f
    move/from16 v1, v18

    #@131
    move/from16 v2, v19

    #@133
    move/from16 v3, v20

    #@135
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@138
    move-result-object v10

    #@139
    .line 723
    .local v10, msg:Landroid/os/Message;
    if-eqz v10, :cond_13e

    #@13b
    .line 724
    invoke-virtual {v10}, Landroid/os/Message;->sendToTarget()V
    :try_end_13e
    .catch Landroid/os/RemoteException; {:try_start_f9 .. :try_end_13e} :catch_143
    .catch Ljava/lang/IllegalStateException; {:try_start_f9 .. :try_end_13e} :catch_15f

    #@13e
    .line 726
    :cond_13e
    const/4 v15, 0x1

    #@13f
    .end local v5           #b:Landroid/os/IBinder;
    .end local v8           #ifcg:Landroid/net/InterfaceConfiguration;
    .end local v10           #msg:Landroid/os/Message;
    .end local v11           #netd:Landroid/os/INetworkManagementService;
    :cond_13f
    :goto_13f
    move/from16 v17, v15

    #@141
    .line 743
    goto/16 :goto_3b

    #@143
    .line 727
    .restart local v5       #b:Landroid/os/IBinder;
    .restart local v8       #ifcg:Landroid/net/InterfaceConfiguration;
    .restart local v11       #netd:Landroid/os/INetworkManagementService;
    :catch_143
    move-exception v12

    #@144
    .line 729
    .local v12, re:Landroid/os/RemoteException;
    const-string v17, "DhcpStateMachine"

    #@146
    new-instance v18, Ljava/lang/StringBuilder;

    #@148
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    const-string v19, "CheckDhcpInfoCacheList: IP configuration failed: "

    #@14d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v18

    #@151
    move-object/from16 v0, v18

    #@153
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v18

    #@157
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v18

    #@15b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    goto :goto_13f

    #@15f
    .line 731
    .end local v12           #re:Landroid/os/RemoteException;
    :catch_15f
    move-exception v7

    #@160
    .line 733
    .local v7, e:Ljava/lang/IllegalStateException;
    const-string v17, "DhcpStateMachine"

    #@162
    new-instance v18, Ljava/lang/StringBuilder;

    #@164
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@167
    const-string v19, "CheckDhcpInfoCacheList: IP configuration failed: "

    #@169
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v18

    #@16d
    move-object/from16 v0, v18

    #@16f
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v18

    #@173
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@176
    move-result-object v18

    #@177
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17a
    goto :goto_13f

    #@17b
    .line 740
    .end local v5           #b:Landroid/os/IBinder;
    .end local v7           #e:Ljava/lang/IllegalStateException;
    .end local v8           #ifcg:Landroid/net/InterfaceConfiguration;
    .end local v11           #netd:Landroid/os/INetworkManagementService;
    :cond_17b
    const-string v17, "DhcpStateMachine"

    #@17d
    new-instance v18, Ljava/lang/StringBuilder;

    #@17f
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@182
    const-string v19, "[bssid + ssid] hash key :"

    #@184
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v18

    #@188
    move-object/from16 v0, v18

    #@18a
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v18

    #@18e
    const-string v19, " is not exist."

    #@190
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v18

    #@194
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@197
    move-result-object v18

    #@198
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    goto :goto_13f
.end method

.method public doQuit()V
    .registers 1

    #@0
    .prologue
    .line 241
    invoke-virtual {p0}, Landroid/net/DhcpStateMachine;->quit()V

    #@3
    .line 242
    return-void
.end method

.method protected onQuitting()V
    .registers 3

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/net/DhcpStateMachine;->mController:Lcom/android/internal/util/StateMachine;

    #@2
    const v1, 0x30006

    #@5
    invoke-virtual {v0, v1}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@8
    .line 246
    return-void
.end method

.method public registerForPreDhcpNotification()V
    .registers 2

    #@0
    .prologue
    .line 232
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/net/DhcpStateMachine;->mRegisteredForPreDhcpNotification:Z

    #@3
    .line 233
    return-void
.end method
