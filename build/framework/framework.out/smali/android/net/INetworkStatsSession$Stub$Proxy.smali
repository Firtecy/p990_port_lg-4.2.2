.class Landroid/net/INetworkStatsSession$Stub$Proxy;
.super Ljava/lang/Object;
.source "INetworkStatsSession.java"

# interfaces
.implements Landroid/net/INetworkStatsSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/INetworkStatsSession$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 164
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 165
    iput-object p1, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 166
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public close()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 307
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 308
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 310
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.net.INetworkStatsSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 311
    iget-object v2, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x5

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 312
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 315
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 316
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 318
    return-void

    #@1e
    .line 315
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 316
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public getHistoryForNetwork(Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;
    .registers 9
    .parameter "template"
    .parameter "fields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 211
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 214
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.INetworkStatsSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 215
    if-eqz p1, :cond_39

    #@f
    .line 216
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 217
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/net/NetworkTemplate;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 222
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 223
    iget-object v3, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x2

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 224
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 225
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_46

    #@2a
    .line 226
    sget-object v3, Landroid/net/NetworkStatsHistory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2f
    move-result-object v2

    #@30
    check-cast v2, Landroid/net/NetworkStatsHistory;
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_3e

    #@32
    .line 233
    .local v2, _result:Landroid/net/NetworkStatsHistory;
    :goto_32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 236
    return-object v2

    #@39
    .line 220
    .end local v2           #_result:Landroid/net/NetworkStatsHistory;
    :cond_39
    const/4 v3, 0x0

    #@3a
    :try_start_3a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3d
    .catchall {:try_start_3a .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_17

    #@3e
    .line 233
    :catchall_3e
    move-exception v3

    #@3f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@45
    throw v3

    #@46
    .line 229
    :cond_46
    const/4 v2, 0x0

    #@47
    .restart local v2       #_result:Landroid/net/NetworkStatsHistory;
    goto :goto_32
.end method

.method public getHistoryForUid(Landroid/net/NetworkTemplate;IIII)Landroid/net/NetworkStatsHistory;
    .registers 12
    .parameter "template"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "fields"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 274
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 275
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 278
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.INetworkStatsSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 279
    if-eqz p1, :cond_42

    #@f
    .line 280
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 281
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/net/NetworkTemplate;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 286
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 287
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 288
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 289
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 290
    iget-object v3, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@25
    const/4 v4, 0x4

    #@26
    const/4 v5, 0x0

    #@27
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 292
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_4f

    #@33
    .line 293
    sget-object v3, Landroid/net/NetworkStatsHistory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@35
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@38
    move-result-object v2

    #@39
    check-cast v2, Landroid/net/NetworkStatsHistory;
    :try_end_3b
    .catchall {:try_start_8 .. :try_end_3b} :catchall_47

    #@3b
    .line 300
    .local v2, _result:Landroid/net/NetworkStatsHistory;
    :goto_3b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 301
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 303
    return-object v2

    #@42
    .line 284
    .end local v2           #_result:Landroid/net/NetworkStatsHistory;
    :cond_42
    const/4 v3, 0x0

    #@43
    :try_start_43
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_46
    .catchall {:try_start_43 .. :try_end_46} :catchall_47

    #@46
    goto :goto_17

    #@47
    .line 300
    :catchall_47
    move-exception v3

    #@48
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4b
    .line 301
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4e
    throw v3

    #@4f
    .line 296
    :cond_4f
    const/4 v2, 0x0

    #@50
    .restart local v2       #_result:Landroid/net/NetworkStatsHistory;
    goto :goto_3b
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 173
    const-string v0, "android.net.INetworkStatsSession"

    #@2
    return-object v0
.end method

.method public getSummaryForAllUid(Landroid/net/NetworkTemplate;JJZ)Landroid/net/NetworkStats;
    .registers 13
    .parameter "template"
    .parameter "start"
    .parameter "end"
    .parameter "includeTags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 241
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 242
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 245
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v5, "android.net.INetworkStatsSession"

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 246
    if-eqz p1, :cond_43

    #@11
    .line 247
    const/4 v5, 0x1

    #@12
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 248
    const/4 v5, 0x0

    #@16
    invoke-virtual {p1, v0, v5}, Landroid/net/NetworkTemplate;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 253
    :goto_19
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@1c
    .line 254
    invoke-virtual {v0, p4, p5}, Landroid/os/Parcel;->writeLong(J)V

    #@1f
    .line 255
    if-eqz p6, :cond_50

    #@21
    :goto_21
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 256
    iget-object v3, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@26
    const/4 v4, 0x3

    #@27
    const/4 v5, 0x0

    #@28
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2b
    .line 257
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2e
    .line 258
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_52

    #@34
    .line 259
    sget-object v3, Landroid/net/NetworkStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@36
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@39
    move-result-object v2

    #@3a
    check-cast v2, Landroid/net/NetworkStats;
    :try_end_3c
    .catchall {:try_start_a .. :try_end_3c} :catchall_48

    #@3c
    .line 266
    .local v2, _result:Landroid/net/NetworkStats;
    :goto_3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 267
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 269
    return-object v2

    #@43
    .line 251
    .end local v2           #_result:Landroid/net/NetworkStats;
    :cond_43
    const/4 v5, 0x0

    #@44
    :try_start_44
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_48

    #@47
    goto :goto_19

    #@48
    .line 266
    :catchall_48
    move-exception v3

    #@49
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 267
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4f
    throw v3

    #@50
    :cond_50
    move v3, v4

    #@51
    .line 255
    goto :goto_21

    #@52
    .line 262
    :cond_52
    const/4 v2, 0x0

    #@53
    .restart local v2       #_result:Landroid/net/NetworkStats;
    goto :goto_3c
.end method

.method public getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;
    .registers 12
    .parameter "template"
    .parameter "start"
    .parameter "end"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 178
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 179
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 182
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.INetworkStatsSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 183
    if-eqz p1, :cond_3c

    #@f
    .line 184
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 185
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/net/NetworkTemplate;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 190
    :goto_17
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@1a
    .line 191
    invoke-virtual {v0, p4, p5}, Landroid/os/Parcel;->writeLong(J)V

    #@1d
    .line 192
    iget-object v3, p0, Landroid/net/INetworkStatsSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/4 v4, 0x1

    #@20
    const/4 v5, 0x0

    #@21
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 193
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 194
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_49

    #@2d
    .line 195
    sget-object v3, Landroid/net/NetworkStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@32
    move-result-object v2

    #@33
    check-cast v2, Landroid/net/NetworkStats;
    :try_end_35
    .catchall {:try_start_8 .. :try_end_35} :catchall_41

    #@35
    .line 202
    .local v2, _result:Landroid/net/NetworkStats;
    :goto_35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 203
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 205
    return-object v2

    #@3c
    .line 188
    .end local v2           #_result:Landroid/net/NetworkStats;
    :cond_3c
    const/4 v3, 0x0

    #@3d
    :try_start_3d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_41

    #@40
    goto :goto_17

    #@41
    .line 202
    :catchall_41
    move-exception v3

    #@42
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 203
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@48
    throw v3

    #@49
    .line 198
    :cond_49
    const/4 v2, 0x0

    #@4a
    .restart local v2       #_result:Landroid/net/NetworkStats;
    goto :goto_35
.end method
