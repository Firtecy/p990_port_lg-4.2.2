.class Landroid/net/Uri$StringUri;
.super Landroid/net/Uri$AbstractHierarchicalUri;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StringUri"
.end annotation


# static fields
.field static final TYPE_ID:I = 0x1


# instance fields
.field private authority:Landroid/net/Uri$Part;

.field private volatile cachedFsi:I

.field private volatile cachedSsi:I

.field private fragment:Landroid/net/Uri$Part;

.field private path:Landroid/net/Uri$PathPart;

.field private query:Landroid/net/Uri$Part;

.field private volatile scheme:Ljava/lang/String;

.field private ssp:Landroid/net/Uri$Part;

.field private final uriString:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "uriString"

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 465
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v0}, Landroid/net/Uri$AbstractHierarchicalUri;-><init>(Landroid/net/Uri$1;)V

    #@5
    .line 487
    iput v1, p0, Landroid/net/Uri$StringUri;->cachedSsi:I

    #@7
    .line 497
    iput v1, p0, Landroid/net/Uri$StringUri;->cachedFsi:I

    #@9
    .line 528
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/net/Uri$StringUri;->scheme:Ljava/lang/String;

    #@f
    .line 466
    if-nez p1, :cond_1a

    #@11
    .line 467
    new-instance v0, Ljava/lang/NullPointerException;

    #@13
    const-string/jumbo v1, "uriString"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 470
    :cond_1a
    iput-object p1, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@1c
    .line 471
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/net/Uri$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 457
    invoke-direct {p0, p1}, Landroid/net/Uri$StringUri;-><init>(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private findFragmentSeparator()I
    .registers 4

    #@0
    .prologue
    .line 501
    iget v0, p0, Landroid/net/Uri$StringUri;->cachedFsi:I

    #@2
    const/4 v1, -0x2

    #@3
    if-ne v0, v1, :cond_14

    #@5
    iget-object v0, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@7
    const/16 v1, 0x23

    #@9
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@c
    move-result v2

    #@d
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/net/Uri$StringUri;->cachedFsi:I

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    iget v0, p0, Landroid/net/Uri$StringUri;->cachedFsi:I

    #@16
    goto :goto_13
.end method

.method private findSchemeSeparator()I
    .registers 3

    #@0
    .prologue
    .line 491
    iget v0, p0, Landroid/net/Uri$StringUri;->cachedSsi:I

    #@2
    const/4 v1, -0x2

    #@3
    if-ne v0, v1, :cond_10

    #@5
    iget-object v0, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@7
    const/16 v1, 0x3a

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/net/Uri$StringUri;->cachedSsi:I

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    iget v0, p0, Landroid/net/Uri$StringUri;->cachedSsi:I

    #@12
    goto :goto_f
.end method

.method private getAuthorityPart()Landroid/net/Uri$Part;
    .registers 4

    #@0
    .prologue
    .line 568
    iget-object v1, p0, Landroid/net/Uri$StringUri;->authority:Landroid/net/Uri$Part;

    #@2
    if-nez v1, :cond_15

    #@4
    .line 569
    iget-object v1, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@6
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@9
    move-result v2

    #@a
    invoke-static {v1, v2}, Landroid/net/Uri$StringUri;->parseAuthority(Ljava/lang/String;I)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 571
    .local v0, encodedAuthority:Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@11
    move-result-object v1

    #@12
    iput-object v1, p0, Landroid/net/Uri$StringUri;->authority:Landroid/net/Uri$Part;

    #@14
    .line 574
    .end local v0           #encodedAuthority:Ljava/lang/String;
    :goto_14
    return-object v1

    #@15
    :cond_15
    iget-object v1, p0, Landroid/net/Uri$StringUri;->authority:Landroid/net/Uri$Part;

    #@17
    goto :goto_14
.end method

.method private getFragmentPart()Landroid/net/Uri$Part;
    .registers 2

    #@0
    .prologue
    .line 670
    iget-object v0, p0, Landroid/net/Uri$StringUri;->fragment:Landroid/net/Uri$Part;

    #@2
    if-nez v0, :cond_f

    #@4
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->parseFragment()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/net/Uri$StringUri;->fragment:Landroid/net/Uri$Part;

    #@e
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/net/Uri$StringUri;->fragment:Landroid/net/Uri$Part;

    #@11
    goto :goto_e
.end method

.method private getPathPart()Landroid/net/Uri$PathPart;
    .registers 2

    #@0
    .prologue
    .line 588
    iget-object v0, p0, Landroid/net/Uri$StringUri;->path:Landroid/net/Uri$PathPart;

    #@2
    if-nez v0, :cond_f

    #@4
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->parsePath()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0}, Landroid/net/Uri$PathPart;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/net/Uri$StringUri;->path:Landroid/net/Uri$PathPart;

    #@e
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/net/Uri$StringUri;->path:Landroid/net/Uri$PathPart;

    #@11
    goto :goto_e
.end method

.method private getQueryPart()Landroid/net/Uri$Part;
    .registers 2

    #@0
    .prologue
    .line 633
    iget-object v0, p0, Landroid/net/Uri$StringUri;->query:Landroid/net/Uri$Part;

    #@2
    if-nez v0, :cond_f

    #@4
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->parseQuery()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/net/Uri$StringUri;->query:Landroid/net/Uri$Part;

    #@e
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/net/Uri$StringUri;->query:Landroid/net/Uri$Part;

    #@11
    goto :goto_e
.end method

.method private getSsp()Landroid/net/Uri$Part;
    .registers 2

    #@0
    .prologue
    .line 544
    iget-object v0, p0, Landroid/net/Uri$StringUri;->ssp:Landroid/net/Uri$Part;

    #@2
    if-nez v0, :cond_f

    #@4
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->parseSsp()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/net/Uri$StringUri;->ssp:Landroid/net/Uri$Part;

    #@e
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/net/Uri$StringUri;->ssp:Landroid/net/Uri$Part;

    #@11
    goto :goto_e
.end method

.method static parseAuthority(Ljava/lang/String;I)Ljava/lang/String;
    .registers 6
    .parameter "uriString"
    .parameter "ssi"

    #@0
    .prologue
    const/16 v3, 0x2f

    #@2
    .line 700
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v1

    #@6
    .line 703
    .local v1, length:I
    add-int/lit8 v2, p1, 0x2

    #@8
    if-le v1, v2, :cond_2f

    #@a
    add-int/lit8 v2, p1, 0x1

    #@c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v2

    #@10
    if-ne v2, v3, :cond_2f

    #@12
    add-int/lit8 v2, p1, 0x2

    #@14
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v2

    #@18
    if-ne v2, v3, :cond_2f

    #@1a
    .line 710
    add-int/lit8 v0, p1, 0x3

    #@1c
    .line 711
    .local v0, end:I
    :goto_1c
    if-ge v0, v1, :cond_28

    #@1e
    .line 712
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@21
    move-result v2

    #@22
    sparse-switch v2, :sswitch_data_32

    #@25
    .line 718
    add-int/lit8 v0, v0, 0x1

    #@27
    goto :goto_1c

    #@28
    .line 721
    :cond_28
    :sswitch_28
    add-int/lit8 v2, p1, 0x3

    #@2a
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    .line 723
    .end local v0           #end:I
    :goto_2e
    return-object v2

    #@2f
    :cond_2f
    const/4 v2, 0x0

    #@30
    goto :goto_2e

    #@31
    .line 712
    nop

    #@32
    :sswitch_data_32
    .sparse-switch
        0x23 -> :sswitch_28
        0x2f -> :sswitch_28
        0x3f -> :sswitch_28
    .end sparse-switch
.end method

.method private parseFragment()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 679
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findFragmentSeparator()I

    #@3
    move-result v0

    #@4
    .line 680
    .local v0, fsi:I
    const/4 v1, -0x1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v1, 0x0

    #@8
    :goto_8
    return-object v1

    #@9
    :cond_9
    iget-object v1, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@b
    add-int/lit8 v2, v0, 0x1

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    goto :goto_8
.end method

.method private parsePath()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 606
    iget-object v2, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@3
    .line 607
    .local v2, uriString:Ljava/lang/String;
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@6
    move-result v1

    #@7
    .line 610
    .local v1, ssi:I
    const/4 v4, -0x1

    #@8
    if-le v1, v4, :cond_22

    #@a
    .line 612
    add-int/lit8 v4, v1, 0x1

    #@c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@f
    move-result v5

    #@10
    if-ne v4, v5, :cond_16

    #@12
    const/4 v0, 0x1

    #@13
    .line 613
    .local v0, schemeOnly:Z
    :goto_13
    if-eqz v0, :cond_18

    #@15
    .line 627
    .end local v0           #schemeOnly:Z
    :cond_15
    :goto_15
    return-object v3

    #@16
    .line 612
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_13

    #@18
    .line 619
    .restart local v0       #schemeOnly:Z
    :cond_18
    add-int/lit8 v4, v1, 0x1

    #@1a
    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v4

    #@1e
    const/16 v5, 0x2f

    #@20
    if-ne v4, v5, :cond_15

    #@22
    .line 627
    .end local v0           #schemeOnly:Z
    :cond_22
    invoke-static {v2, v1}, Landroid/net/Uri$StringUri;->parsePath(Ljava/lang/String;I)Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    goto :goto_15
.end method

.method static parsePath(Ljava/lang/String;I)Ljava/lang/String;
    .registers 7
    .parameter "uriString"
    .parameter "ssi"

    #@0
    .prologue
    const/16 v4, 0x2f

    #@2
    .line 737
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    .line 741
    .local v0, length:I
    add-int/lit8 v3, p1, 0x2

    #@8
    if-le v0, v3, :cond_2b

    #@a
    add-int/lit8 v3, p1, 0x1

    #@c
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v3

    #@10
    if-ne v3, v4, :cond_2b

    #@12
    add-int/lit8 v3, p1, 0x2

    #@14
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v3

    #@18
    if-ne v3, v4, :cond_2b

    #@1a
    .line 745
    add-int/lit8 v2, p1, 0x3

    #@1c
    .line 746
    .local v2, pathStart:I
    :goto_1c
    if-ge v2, v0, :cond_2d

    #@1e
    .line 747
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@21
    move-result v3

    #@22
    sparse-switch v3, :sswitch_data_40

    #@25
    .line 754
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_1c

    #@28
    .line 750
    :sswitch_28
    const-string v3, ""

    #@2a
    .line 772
    :goto_2a
    return-object v3

    #@2b
    .line 758
    .end local v2           #pathStart:I
    :cond_2b
    add-int/lit8 v2, p1, 0x1

    #@2d
    .line 762
    .restart local v2       #pathStart:I
    :cond_2d
    :sswitch_2d
    move v1, v2

    #@2e
    .line 763
    .local v1, pathEnd:I
    :goto_2e
    if-ge v1, v0, :cond_3a

    #@30
    .line 764
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@33
    move-result v3

    #@34
    sparse-switch v3, :sswitch_data_4e

    #@37
    .line 769
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_2e

    #@3a
    .line 772
    :cond_3a
    :sswitch_3a
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    goto :goto_2a

    #@3f
    .line 747
    nop

    #@40
    :sswitch_data_40
    .sparse-switch
        0x23 -> :sswitch_28
        0x2f -> :sswitch_2d
        0x3f -> :sswitch_28
    .end sparse-switch

    #@4e
    .line 764
    :sswitch_data_4e
    .sparse-switch
        0x23 -> :sswitch_3a
        0x3f -> :sswitch_3a
    .end sparse-switch
.end method

.method private parseQuery()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v6, -0x1

    #@2
    .line 644
    iget-object v3, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@4
    const/16 v4, 0x3f

    #@6
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@9
    move-result v5

    #@a
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->indexOf(II)I

    #@d
    move-result v1

    #@e
    .line 645
    .local v1, qsi:I
    if-ne v1, v6, :cond_11

    #@10
    .line 660
    :cond_10
    :goto_10
    return-object v2

    #@11
    .line 649
    :cond_11
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findFragmentSeparator()I

    #@14
    move-result v0

    #@15
    .line 651
    .local v0, fsi:I
    if-ne v0, v6, :cond_20

    #@17
    .line 652
    iget-object v2, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@19
    add-int/lit8 v3, v1, 0x1

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_10

    #@20
    .line 655
    :cond_20
    if-lt v0, v1, :cond_10

    #@22
    .line 660
    iget-object v2, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@24
    add-int/lit8 v3, v1, 0x1

    #@26
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    goto :goto_10
.end method

.method private parseScheme()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 537
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@3
    move-result v0

    #@4
    .line 538
    .local v0, ssi:I
    const/4 v1, -0x1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v1, 0x0

    #@8
    :goto_8
    return-object v1

    #@9
    :cond_9
    iget-object v1, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    goto :goto_8
.end method

.method private parseSsp()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 556
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@3
    move-result v1

    #@4
    .line 557
    .local v1, ssi:I
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findFragmentSeparator()I

    #@7
    move-result v0

    #@8
    .line 560
    .local v0, fsi:I
    const/4 v2, -0x1

    #@9
    if-ne v0, v2, :cond_14

    #@b
    iget-object v2, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@d
    add-int/lit8 v3, v1, 0x1

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    :goto_13
    return-object v2

    #@14
    :cond_14
    iget-object v2, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@16
    add-int/lit8 v3, v1, 0x1

    #@18
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    goto :goto_13
.end method

.method static readFrom(Landroid/os/Parcel;)Landroid/net/Uri;
    .registers 3
    .parameter "parcel"

    #@0
    .prologue
    .line 474
    new-instance v0, Landroid/net/Uri$StringUri;

    #@2
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/net/Uri$StringUri;-><init>(Ljava/lang/String;)V

    #@9
    return-object v0
.end method


# virtual methods
.method public buildUpon()Landroid/net/Uri$Builder;
    .registers 3

    #@0
    .prologue
    .line 776
    invoke-virtual {p0}, Landroid/net/Uri$StringUri;->isHierarchical()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_34

    #@6
    .line 777
    new-instance v0, Landroid/net/Uri$Builder;

    #@8
    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    #@b
    invoke-virtual {p0}, Landroid/net/Uri$StringUri;->getScheme()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getAuthorityPart()Landroid/net/Uri$Part;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@1a
    move-result-object v0

    #@1b
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getPathPart()Landroid/net/Uri$PathPart;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Landroid/net/Uri$PathPart;)Landroid/net/Uri$Builder;

    #@22
    move-result-object v0

    #@23
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getQueryPart()Landroid/net/Uri$Part;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->query(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@2a
    move-result-object v0

    #@2b
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getFragmentPart()Landroid/net/Uri$Part;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@32
    move-result-object v0

    #@33
    .line 784
    :goto_33
    return-object v0

    #@34
    :cond_34
    new-instance v0, Landroid/net/Uri$Builder;

    #@36
    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    #@39
    invoke-virtual {p0}, Landroid/net/Uri$StringUri;->getScheme()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@40
    move-result-object v0

    #@41
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getSsp()Landroid/net/Uri$Part;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->opaquePart(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@48
    move-result-object v0

    #@49
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getFragmentPart()Landroid/net/Uri$Part;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@50
    move-result-object v0

    #@51
    goto :goto_33
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 478
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAuthority()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 582
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getAuthorityPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getDecoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getEncodedAuthority()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 578
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getAuthorityPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getEncoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getEncodedFragment()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 675
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getFragmentPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getEncoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getEncodedPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 598
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getPathPart()Landroid/net/Uri$PathPart;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$PathPart;->getEncoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getEncodedQuery()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 638
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getQueryPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getEncoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getEncodedSchemeSpecificPart()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 548
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getSsp()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getEncoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getFragment()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 684
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getFragmentPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getDecoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 594
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getPathPart()Landroid/net/Uri$PathPart;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$PathPart;->getDecoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getPathSegments()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 602
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getPathPart()Landroid/net/Uri$PathPart;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$PathPart;->getPathSegments()Landroid/net/Uri$PathSegments;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 664
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getQueryPart()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getDecoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getScheme()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 532
    iget-object v1, p0, Landroid/net/Uri$StringUri;->scheme:Ljava/lang/String;

    #@2
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    if-eq v1, v2, :cond_e

    #@8
    const/4 v0, 0x1

    #@9
    .line 533
    .local v0, cached:Z
    :goto_9
    if-eqz v0, :cond_10

    #@b
    iget-object v1, p0, Landroid/net/Uri$StringUri;->scheme:Ljava/lang/String;

    #@d
    :goto_d
    return-object v1

    #@e
    .line 532
    .end local v0           #cached:Z
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_9

    #@10
    .line 533
    .restart local v0       #cached:Z
    :cond_10
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->parseScheme()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Landroid/net/Uri$StringUri;->scheme:Ljava/lang/String;

    #@16
    goto :goto_d
.end method

.method public getSchemeSpecificPart()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 552
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->getSsp()Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri$Part;->getDecoded()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public isHierarchical()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 507
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@5
    move-result v0

    #@6
    .line 509
    .local v0, ssi:I
    const/4 v3, -0x1

    #@7
    if-ne v0, v3, :cond_a

    #@9
    .line 520
    :cond_9
    :goto_9
    return v1

    #@a
    .line 514
    :cond_a
    iget-object v3, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@f
    move-result v3

    #@10
    add-int/lit8 v4, v0, 0x1

    #@12
    if-ne v3, v4, :cond_16

    #@14
    move v1, v2

    #@15
    .line 516
    goto :goto_9

    #@16
    .line 520
    :cond_16
    iget-object v3, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@18
    add-int/lit8 v4, v0, 0x1

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v3

    #@1e
    const/16 v4, 0x2f

    #@20
    if-eq v3, v4, :cond_9

    #@22
    move v1, v2

    #@23
    goto :goto_9
.end method

.method public isRelative()Z
    .registers 3

    #@0
    .prologue
    .line 525
    invoke-direct {p0}, Landroid/net/Uri$StringUri;->findSchemeSeparator()I

    #@3
    move-result v0

    #@4
    const/4 v1, -0x1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 688
    iget-object v0, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 482
    const/4 v0, 0x1

    #@1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4
    .line 483
    iget-object v0, p0, Landroid/net/Uri$StringUri;->uriString:Ljava/lang/String;

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@9
    .line 484
    return-void
.end method
