.class final Landroid/net/ProxyProperties$1;
.super Ljava/lang/Object;
.source "ProxyProperties.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/ProxyProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/ProxyProperties;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 199
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/ProxyProperties;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    .line 201
    const/4 v1, 0x0

    #@1
    .line 202
    .local v1, host:Ljava/lang/String;
    const/4 v2, 0x0

    #@2
    .line 203
    .local v2, port:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@5
    move-result v5

    #@6
    const/4 v6, 0x1

    #@7
    if-ne v5, v6, :cond_11

    #@9
    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v2

    #@11
    .line 207
    :cond_11
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    .line 208
    .local v3, exclList:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    .line 209
    .local v4, parsedExclList:[Ljava/lang/String;
    new-instance v0, Landroid/net/ProxyProperties;

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-direct/range {v0 .. v5}, Landroid/net/ProxyProperties;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/net/ProxyProperties$1;)V

    #@1f
    .line 211
    .local v0, proxyProperties:Landroid/net/ProxyProperties;
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 199
    invoke-virtual {p0, p1}, Landroid/net/ProxyProperties$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/ProxyProperties;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/ProxyProperties;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 215
    new-array v0, p1, [Landroid/net/ProxyProperties;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 199
    invoke-virtual {p0, p1}, Landroid/net/ProxyProperties$1;->newArray(I)[Landroid/net/ProxyProperties;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
