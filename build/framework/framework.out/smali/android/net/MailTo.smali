.class public Landroid/net/MailTo;
.super Ljava/lang/Object;
.source "MailTo.java"


# static fields
.field private static final BODY:Ljava/lang/String; = "body"

.field private static final CC:Ljava/lang/String; = "cc"

.field public static final MAILTO_SCHEME:Ljava/lang/String; = "mailto:"

.field private static final SUBJECT:Ljava/lang/String; = "subject"

.field private static final TO:Ljava/lang/String; = "to"


# instance fields
.field private mHeaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 169
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 170
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@a
    .line 171
    return-void
.end method

.method public static isMailTo(Ljava/lang/String;)Z
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 51
    if-eqz p0, :cond_d

    #@2
    const-string/jumbo v0, "mailto:"

    #@5
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_d

    #@b
    .line 52
    const/4 v0, 0x1

    #@c
    .line 54
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public static parse(Ljava/lang/String;)Landroid/net/MailTo;
    .registers 18
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    if-nez p0, :cond_8

    #@2
    .line 66
    new-instance v13, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v13}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v13

    #@8
    .line 68
    :cond_8
    invoke-static/range {p0 .. p0}, Landroid/net/MailTo;->isMailTo(Ljava/lang/String;)Z

    #@b
    move-result v13

    #@c
    if-nez v13, :cond_16

    #@e
    .line 69
    new-instance v13, Landroid/net/ParseException;

    #@10
    const-string v14, "Not a mailto scheme"

    #@12
    invoke-direct {v13, v14}, Landroid/net/ParseException;-><init>(Ljava/lang/String;)V

    #@15
    throw v13

    #@16
    .line 72
    :cond_16
    const-string/jumbo v13, "mailto:"

    #@19
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@1c
    move-result v13

    #@1d
    move-object/from16 v0, p0

    #@1f
    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@22
    move-result-object v9

    #@23
    .line 73
    .local v9, noScheme:Ljava/lang/String;
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@26
    move-result-object v4

    #@27
    .line 74
    .local v4, email:Landroid/net/Uri;
    new-instance v7, Landroid/net/MailTo;

    #@29
    invoke-direct {v7}, Landroid/net/MailTo;-><init>()V

    #@2c
    .line 77
    .local v7, m:Landroid/net/MailTo;
    invoke-virtual {v4}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    #@2f
    move-result-object v12

    #@30
    .line 78
    .local v12, query:Ljava/lang/String;
    if-eqz v12, :cond_6c

    #@32
    .line 79
    const-string v13, "&"

    #@34
    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@37
    move-result-object v11

    #@38
    .line 80
    .local v11, queries:[Ljava/lang/String;
    move-object v3, v11

    #@39
    .local v3, arr$:[Ljava/lang/String;
    array-length v6, v3

    #@3a
    .local v6, len$:I
    const/4 v5, 0x0

    #@3b
    .local v5, i$:I
    :goto_3b
    if-ge v5, v6, :cond_6c

    #@3d
    aget-object v10, v3, v5

    #@3f
    .line 81
    .local v10, q:Ljava/lang/String;
    const-string v13, "="

    #@41
    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    .line 82
    .local v8, nameval:[Ljava/lang/String;
    array-length v13, v8

    #@46
    if-nez v13, :cond_4b

    #@48
    .line 80
    :goto_48
    add-int/lit8 v5, v5, 0x1

    #@4a
    goto :goto_3b

    #@4b
    .line 87
    :cond_4b
    iget-object v14, v7, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@4d
    const/4 v13, 0x0

    #@4e
    aget-object v13, v8, v13

    #@50
    invoke-static {v13}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v13

    #@54
    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@57
    move-result-object v15

    #@58
    array-length v13, v8

    #@59
    const/16 v16, 0x1

    #@5b
    move/from16 v0, v16

    #@5d
    if-le v13, v0, :cond_6a

    #@5f
    const/4 v13, 0x1

    #@60
    aget-object v13, v8, v13

    #@62
    invoke-static {v13}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@65
    move-result-object v13

    #@66
    :goto_66
    invoke-virtual {v14, v15, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    goto :goto_48

    #@6a
    :cond_6a
    const/4 v13, 0x0

    #@6b
    goto :goto_66

    #@6c
    .line 94
    .end local v3           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v8           #nameval:[Ljava/lang/String;
    .end local v10           #q:Ljava/lang/String;
    .end local v11           #queries:[Ljava/lang/String;
    :cond_6c
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    .line 95
    .local v2, address:Ljava/lang/String;
    if-eqz v2, :cond_97

    #@72
    .line 96
    invoke-virtual {v7}, Landroid/net/MailTo;->getTo()Ljava/lang/String;

    #@75
    move-result-object v1

    #@76
    .line 97
    .local v1, addr:Ljava/lang/String;
    if-eqz v1, :cond_8f

    #@78
    .line 98
    new-instance v13, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v13

    #@81
    const-string v14, ", "

    #@83
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v13

    #@87
    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v13

    #@8b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v2

    #@8f
    .line 100
    :cond_8f
    iget-object v13, v7, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@91
    const-string/jumbo v14, "to"

    #@94
    invoke-virtual {v13, v14, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@97
    .line 103
    .end local v1           #addr:Ljava/lang/String;
    :cond_97
    return-object v7
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@2
    const-string v1, "body"

    #@4
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/String;

    #@a
    return-object v0
.end method

.method public getCc()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@2
    const-string v1, "cc"

    #@4
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/String;

    #@a
    return-object v0
.end method

.method public getHeaders()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 132
    iget-object v0, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@2
    const-string/jumbo v1, "subject"

    #@5
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/String;

    #@b
    return-object v0
.end method

.method public getTo()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@2
    const-string/jumbo v1, "to"

    #@5
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/String;

    #@b
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 154
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    const-string/jumbo v3, "mailto:"

    #@5
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    .line 155
    .local v2, sb:Ljava/lang/StringBuilder;
    const/16 v3, 0x3f

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d
    .line 156
    iget-object v3, p0, Landroid/net/MailTo;->mHeaders:Ljava/util/HashMap;

    #@f
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@12
    move-result-object v3

    #@13
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v1

    #@17
    .local v1, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_48

    #@1d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Ljava/util/Map$Entry;

    #@23
    .line 157
    .local v0, header:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Ljava/lang/String;

    #@29
    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 158
    const/16 v3, 0x3d

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@35
    .line 159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@38
    move-result-object v3

    #@39
    check-cast v3, Ljava/lang/String;

    #@3b
    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 160
    const/16 v3, 0x26

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@47
    goto :goto_17

    #@48
    .line 162
    .end local v0           #header:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    return-object v3
.end method
