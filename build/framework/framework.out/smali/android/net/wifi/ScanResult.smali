.class public Landroid/net/wifi/ScanResult;
.super Ljava/lang/Object;
.source "ScanResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public BSSID:Ljava/lang/String;

.field public SSID:Ljava/lang/String;

.field public capabilities:Ljava/lang/String;

.field public frequency:I

.field public level:I

.field public timestamp:J

.field public wifiSsid:Landroid/net/wifi/WifiSsid;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 127
    new-instance v0, Landroid/net/wifi/ScanResult$1;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/ScanResult$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/ScanResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/ScanResult;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    if-eqz p1, :cond_21

    #@5
    .line 75
    iget-object v0, p1, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@7
    iput-object v0, p0, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@9
    .line 76
    iget-object v0, p1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@b
    iput-object v0, p0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@d
    .line 77
    iget-object v0, p1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@f
    iput-object v0, p0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@11
    .line 78
    iget-object v0, p1, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@13
    iput-object v0, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@15
    .line 79
    iget v0, p1, Landroid/net/wifi/ScanResult;->level:I

    #@17
    iput v0, p0, Landroid/net/wifi/ScanResult;->level:I

    #@19
    .line 80
    iget v0, p1, Landroid/net/wifi/ScanResult;->frequency:I

    #@1b
    iput v0, p0, Landroid/net/wifi/ScanResult;->frequency:I

    #@1d
    .line 81
    iget-wide v0, p1, Landroid/net/wifi/ScanResult;->timestamp:J

    #@1f
    iput-wide v0, p0, Landroid/net/wifi/ScanResult;->timestamp:J

    #@21
    .line 83
    :cond_21
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/WifiSsid;Ljava/lang/String;Ljava/lang/String;IIJ)V
    .registers 9
    .parameter "wifiSsid"
    .parameter "BSSID"
    .parameter "caps"
    .parameter "level"
    .parameter "frequency"
    .parameter "tsf"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    iput-object p1, p0, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@5
    .line 63
    if-eqz p1, :cond_18

    #@7
    invoke-virtual {p1}, Landroid/net/wifi/WifiSsid;->toString()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    :goto_b
    iput-object v0, p0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@d
    .line 64
    iput-object p2, p0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@f
    .line 65
    iput-object p3, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@11
    .line 66
    iput p4, p0, Landroid/net/wifi/ScanResult;->level:I

    #@13
    .line 67
    iput p5, p0, Landroid/net/wifi/ScanResult;->frequency:I

    #@15
    .line 68
    iput-wide p6, p0, Landroid/net/wifi/ScanResult;->timestamp:J

    #@17
    .line 69
    return-void

    #@18
    .line 63
    :cond_18
    const-string v0, "<unknown ssid>"

    #@1a
    goto :goto_b
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 108
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 87
    new-instance v1, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 88
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string v0, "<none>"

    #@7
    .line 90
    .local v0, none:Ljava/lang/String;
    const-string v2, "SSID: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c
    move-result-object v3

    #@d
    iget-object v2, p0, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@f
    if-nez v2, :cond_5c

    #@11
    const-string v2, "<unknown ssid>"

    #@13
    :goto_13
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", BSSID: "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    move-result-object v3

    #@1d
    iget-object v2, p0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@1f
    if-nez v2, :cond_5f

    #@21
    move-object v2, v0

    #@22
    :goto_22
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@25
    move-result-object v2

    #@26
    const-string v3, ", capabilities: "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    move-result-object v2

    #@2c
    iget-object v3, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@2e
    if-nez v3, :cond_62

    #@30
    .end local v0           #none:Ljava/lang/String;
    :goto_30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@33
    move-result-object v2

    #@34
    const-string v3, ", level: "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@39
    move-result-object v2

    #@3a
    iget v3, p0, Landroid/net/wifi/ScanResult;->level:I

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@3f
    move-result-object v2

    #@40
    const-string v3, ", frequency: "

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@45
    move-result-object v2

    #@46
    iget v3, p0, Landroid/net/wifi/ScanResult;->frequency:I

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@4b
    move-result-object v2

    #@4c
    const-string v3, ", timestamp: "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@51
    move-result-object v2

    #@52
    iget-wide v3, p0, Landroid/net/wifi/ScanResult;->timestamp:J

    #@54
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    #@57
    .line 103
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    return-object v2

    #@5c
    .line 90
    .restart local v0       #none:Ljava/lang/String;
    :cond_5c
    iget-object v2, p0, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@5e
    goto :goto_13

    #@5f
    :cond_5f
    iget-object v2, p0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@61
    goto :goto_22

    #@62
    :cond_62
    iget-object v0, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@64
    goto :goto_30
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@2
    if-eqz v0, :cond_27

    #@4
    .line 114
    const/4 v0, 0x1

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 115
    iget-object v0, p0, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@a
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiSsid;->writeToParcel(Landroid/os/Parcel;I)V

    #@d
    .line 119
    :goto_d
    iget-object v0, p0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 120
    iget-object v0, p0, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 121
    iget v0, p0, Landroid/net/wifi/ScanResult;->level:I

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 122
    iget v0, p0, Landroid/net/wifi/ScanResult;->frequency:I

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 123
    iget-wide v0, p0, Landroid/net/wifi/ScanResult;->timestamp:J

    #@23
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@26
    .line 124
    return-void

    #@27
    .line 117
    :cond_27
    const/4 v0, 0x0

    #@28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    goto :goto_d
.end method
