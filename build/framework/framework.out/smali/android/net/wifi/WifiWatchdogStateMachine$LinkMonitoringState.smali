.class Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;
.super Lcom/android/internal/util/State;
.source "WifiWatchdogStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiWatchdogStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LinkMonitoringState"
.end annotation


# instance fields
.field private mLastRssi:I

.field private mLastTxBad:I

.field private mLastTxGood:I

.field private mSampleCount:I

.field final synthetic this$0:Landroid/net/wifi/WifiWatchdogStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiWatchdogStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 757
    iput-object p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 767
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_e

    #@7
    invoke-virtual {p0}, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->getName()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@e
    .line 768
    :cond_e
    iput v5, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mSampleCount:I

    #@10
    .line 769
    iget-object v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@12
    new-instance v1, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@14
    iget-object v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@16
    const-wide/high16 v3, 0x3fe0

    #@18
    invoke-direct {v1, v2, v3, v4}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;-><init>(Landroid/net/wifi/WifiWatchdogStateMachine;D)V

    #@1b
    invoke-static {v0, v1}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3802(Landroid/net/wifi/WifiWatchdogStateMachine;Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@1e
    .line 770
    iget-object v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@20
    iget-object v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@22
    const v2, 0x2100b

    #@25
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@27
    invoke-static {v3}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2604(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@2a
    move-result v3

    #@2b
    invoke-virtual {v1, v2, v3, v5}, Landroid/net/wifi/WifiWatchdogStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiWatchdogStateMachine;->sendMessage(Landroid/os/Message;)V

    #@32
    .line 771
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 22
    .parameter "msg"

    #@0
    .prologue
    .line 775
    move-object/from16 v0, p1

    #@2
    iget v15, v0, Landroid/os/Message;->what:I

    #@4
    sparse-switch v15, :sswitch_data_230

    #@7
    .line 859
    const/4 v15, 0x0

    #@8
    .line 861
    :goto_8
    return v15

    #@9
    .line 777
    :sswitch_9
    move-object/from16 v0, p0

    #@b
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@d
    move-object/from16 v0, p0

    #@f
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@11
    move-object/from16 v16, v0

    #@13
    move-object/from16 v0, p1

    #@15
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@17
    move/from16 v17, v0

    #@19
    invoke-static/range {v16 .. v17}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$400(Landroid/net/wifi/WifiWatchdogStateMachine;I)I

    #@1c
    move-result v16

    #@1d
    invoke-static/range {v15 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$302(Landroid/net/wifi/WifiWatchdogStateMachine;I)I

    #@20
    .line 778
    move-object/from16 v0, p0

    #@22
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@24
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$300(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@27
    move-result v15

    #@28
    const/16 v16, 0x4

    #@2a
    move/from16 v0, v16

    #@2c
    if-gt v15, v0, :cond_30

    #@2e
    .line 861
    :cond_2e
    :goto_2e
    const/4 v15, 0x1

    #@2f
    goto :goto_8

    #@30
    .line 782
    :cond_30
    move-object/from16 v0, p0

    #@32
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@34
    move-object/from16 v0, p0

    #@36
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@38
    move-object/from16 v16, v0

    #@3a
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2100(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$OnlineWatchState;

    #@3d
    move-result-object v16

    #@3e
    invoke-static/range {v15 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3900(Landroid/net/wifi/WifiWatchdogStateMachine;Lcom/android/internal/util/IState;)V

    #@41
    goto :goto_2e

    #@42
    .line 787
    :sswitch_42
    move-object/from16 v0, p0

    #@44
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@46
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@4a
    move-object/from16 v16, v0

    #@4c
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3600(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;

    #@4f
    move-result-object v16

    #@50
    invoke-static/range {v15 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$4000(Landroid/net/wifi/WifiWatchdogStateMachine;Lcom/android/internal/util/IState;)V

    #@53
    goto :goto_2e

    #@54
    .line 791
    :sswitch_54
    move-object/from16 v0, p0

    #@56
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@58
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$500(Landroid/net/wifi/WifiWatchdogStateMachine;)Z

    #@5b
    move-result v15

    #@5c
    if-nez v15, :cond_70

    #@5e
    .line 792
    move-object/from16 v0, p0

    #@60
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@62
    move-object/from16 v0, p0

    #@64
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@66
    move-object/from16 v16, v0

    #@68
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3300(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$OnlineState;

    #@6b
    move-result-object v16

    #@6c
    invoke-static/range {v15 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$4100(Landroid/net/wifi/WifiWatchdogStateMachine;Lcom/android/internal/util/IState;)V

    #@6f
    goto :goto_2e

    #@70
    .line 793
    :cond_70
    move-object/from16 v0, p1

    #@72
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@74
    move-object/from16 v0, p0

    #@76
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@78
    move-object/from16 v16, v0

    #@7a
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2600(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@7d
    move-result v16

    #@7e
    move/from16 v0, v16

    #@80
    if-ne v15, v0, :cond_2e

    #@82
    .line 794
    move-object/from16 v0, p0

    #@84
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@86
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2800(Landroid/net/wifi/WifiWatchdogStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@89
    move-result-object v15

    #@8a
    const v16, 0x25014

    #@8d
    invoke-virtual/range {v15 .. v16}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@90
    .line 795
    move-object/from16 v0, p0

    #@92
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@94
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@98
    move-object/from16 v16, v0

    #@9a
    const v17, 0x2100b

    #@9d
    move-object/from16 v0, p0

    #@9f
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@a1
    move-object/from16 v18, v0

    #@a3
    invoke-static/range {v18 .. v18}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2604(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@a6
    move-result v18

    #@a7
    const/16 v19, 0x0

    #@a9
    invoke-virtual/range {v16 .. v19}, Landroid/net/wifi/WifiWatchdogStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@ac
    move-result-object v16

    #@ad
    const-wide/16 v17, 0x3e8

    #@af
    invoke-virtual/range {v15 .. v18}, Landroid/net/wifi/WifiWatchdogStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@b2
    goto/16 :goto_2e

    #@b4
    .line 801
    :sswitch_b4
    move-object/from16 v0, p1

    #@b6
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b8
    check-cast v6, Landroid/net/wifi/RssiPacketCountInfo;

    #@ba
    .line 802
    .local v6, info:Landroid/net/wifi/RssiPacketCountInfo;
    iget v12, v6, Landroid/net/wifi/RssiPacketCountInfo;->rssi:I

    #@bc
    .line 803
    .local v12, rssi:I
    move-object/from16 v0, p0

    #@be
    iget v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mLastRssi:I

    #@c0
    add-int/2addr v15, v12

    #@c1
    div-int/lit8 v9, v15, 0x2

    #@c3
    .line 804
    .local v9, mrssi:I
    iget v13, v6, Landroid/net/wifi/RssiPacketCountInfo;->txbad:I

    #@c5
    .line 805
    .local v13, txbad:I
    iget v14, v6, Landroid/net/wifi/RssiPacketCountInfo;->txgood:I

    #@c7
    .line 806
    .local v14, txgood:I
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@ca
    move-result v15

    #@cb
    if-eqz v15, :cond_101

    #@cd
    new-instance v15, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v16, "Fetch RSSI succeed, rssi="

    #@d4
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v15

    #@d8
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v15

    #@dc
    const-string v16, " mrssi="

    #@de
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v15

    #@e2
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v15

    #@e6
    const-string v16, " txbad="

    #@e8
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v15

    #@ec
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v15

    #@f0
    const-string v16, " txgood="

    #@f2
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v15

    #@f6
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v15

    #@fa
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v15

    #@fe
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@101
    .line 810
    :cond_101
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@104
    move-result-wide v10

    #@105
    .line 811
    .local v10, now:J
    move-object/from16 v0, p0

    #@107
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@109
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@10c
    move-result-object v15

    #@10d
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->access$4200(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)J

    #@110
    move-result-wide v15

    #@111
    sub-long v15, v10, v15

    #@113
    const-wide/16 v17, 0x7d0

    #@115
    cmp-long v15, v15, v17

    #@117
    if-gez v15, :cond_204

    #@119
    .line 814
    move-object/from16 v0, p0

    #@11b
    iget v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mLastTxBad:I

    #@11d
    sub-int v2, v13, v15

    #@11f
    .line 815
    .local v2, dbad:I
    move-object/from16 v0, p0

    #@121
    iget v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mLastTxGood:I

    #@123
    sub-int v4, v14, v15

    #@125
    .line 816
    .local v4, dgood:I
    add-int v5, v2, v4

    #@127
    .line 818
    .local v5, dtotal:I
    if-lez v5, :cond_204

    #@129
    .line 820
    int-to-double v15, v2

    #@12a
    int-to-double v0, v5

    #@12b
    move-wide/from16 v17, v0

    #@12d
    div-double v7, v15, v17

    #@12f
    .line 822
    .local v7, loss:D
    move-object/from16 v0, p0

    #@131
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@133
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@136
    move-result-object v15

    #@137
    invoke-virtual {v15, v7, v8, v5}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->update(DI)V

    #@13a
    .line 824
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@13d
    move-result v15

    #@13e
    if-eqz v15, :cond_1a7

    #@140
    .line 825
    new-instance v3, Ljava/text/DecimalFormat;

    #@142
    const-string v15, "#.##"

    #@144
    invoke-direct {v3, v15}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    #@147
    .line 826
    .local v3, df:Ljava/text/DecimalFormat;
    new-instance v15, Ljava/lang/StringBuilder;

    #@149
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@14c
    const-string v16, "Incremental loss="

    #@14e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v15

    #@152
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@155
    move-result-object v15

    #@156
    const-string v16, "/"

    #@158
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v15

    #@15c
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v15

    #@160
    const-string v16, " Current loss="

    #@162
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v15

    #@166
    move-object/from16 v0, p0

    #@168
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@16a
    move-object/from16 v16, v0

    #@16c
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@16f
    move-result-object v16

    #@170
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4300(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@173
    move-result-wide v16

    #@174
    const-wide/high16 v18, 0x4059

    #@176
    mul-double v16, v16, v18

    #@178
    move-wide/from16 v0, v16

    #@17a
    invoke-virtual {v3, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@17d
    move-result-object v16

    #@17e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v15

    #@182
    const-string v16, "% volume="

    #@184
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v15

    #@188
    move-object/from16 v0, p0

    #@18a
    iget-object v0, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@18c
    move-object/from16 v16, v0

    #@18e
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@191
    move-result-object v16

    #@192
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4400(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@195
    move-result-wide v16

    #@196
    move-wide/from16 v0, v16

    #@198
    invoke-virtual {v3, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@19b
    move-result-object v16

    #@19c
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v15

    #@1a0
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a3
    move-result-object v15

    #@1a4
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@1a7
    .line 831
    .end local v3           #df:Ljava/text/DecimalFormat;
    :cond_1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1ab
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@1ae
    move-result-object v15

    #@1af
    invoke-virtual {v15, v9, v7, v8, v5}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->updateLoss(IDI)V

    #@1b2
    .line 834
    move-object/from16 v0, p0

    #@1b4
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1b6
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@1b9
    move-result-object v15

    #@1ba
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4300(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@1bd
    move-result-wide v15

    #@1be
    const-wide/high16 v17, 0x3fe0

    #@1c0
    cmpl-double v15, v15, v17

    #@1c2
    if-lez v15, :cond_21d

    #@1c4
    move-object/from16 v0, p0

    #@1c6
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1c8
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$3800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@1cb
    move-result-object v15

    #@1cc
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4400(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@1cf
    move-result-wide v15

    #@1d0
    const-wide/high16 v17, 0x4000

    #@1d2
    cmpl-double v15, v15, v17

    #@1d4
    if-lez v15, :cond_21d

    #@1d6
    .line 836
    move-object/from16 v0, p0

    #@1d8
    iget v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mSampleCount:I

    #@1da
    add-int/lit8 v15, v15, 0x1

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iput v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mSampleCount:I

    #@1e0
    const/16 v16, 0x3

    #@1e2
    move/from16 v0, v16

    #@1e4
    if-lt v15, v0, :cond_204

    #@1e6
    .line 837
    move-object/from16 v0, p0

    #@1e8
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1ea
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@1ed
    move-result-object v15

    #@1ee
    invoke-virtual {v15, v12}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->poorLinkDetected(I)Z

    #@1f1
    move-result v15

    #@1f2
    if-eqz v15, :cond_204

    #@1f4
    .line 838
    move-object/from16 v0, p0

    #@1f6
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1f8
    const/16 v16, 0x0

    #@1fa
    invoke-static/range {v15 .. v16}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1100(Landroid/net/wifi/WifiWatchdogStateMachine;Z)V

    #@1fd
    .line 839
    move-object/from16 v0, p0

    #@1ff
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@201
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2604(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@204
    .line 847
    .end local v2           #dbad:I
    .end local v4           #dgood:I
    .end local v5           #dtotal:I
    .end local v7           #loss:D
    :cond_204
    :goto_204
    move-object/from16 v0, p0

    #@206
    iget-object v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@208
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@20b
    move-result-object v15

    #@20c
    invoke-static {v15, v10, v11}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->access$4202(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;J)J

    #@20f
    .line 848
    move-object/from16 v0, p0

    #@211
    iput v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mLastTxBad:I

    #@213
    .line 849
    move-object/from16 v0, p0

    #@215
    iput v14, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mLastTxGood:I

    #@217
    .line 850
    move-object/from16 v0, p0

    #@219
    iput v12, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mLastRssi:I

    #@21b
    goto/16 :goto_2e

    #@21d
    .line 842
    .restart local v2       #dbad:I
    .restart local v4       #dgood:I
    .restart local v5       #dtotal:I
    .restart local v7       #loss:D
    :cond_21d
    const/4 v15, 0x0

    #@21e
    move-object/from16 v0, p0

    #@220
    iput v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$LinkMonitoringState;->mSampleCount:I

    #@222
    goto :goto_204

    #@223
    .line 855
    .end local v2           #dbad:I
    .end local v4           #dgood:I
    .end local v5           #dtotal:I
    .end local v6           #info:Landroid/net/wifi/RssiPacketCountInfo;
    .end local v7           #loss:D
    .end local v9           #mrssi:I
    .end local v10           #now:J
    .end local v12           #rssi:I
    .end local v13           #txbad:I
    .end local v14           #txgood:I
    :sswitch_223
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@226
    move-result v15

    #@227
    if-eqz v15, :cond_2e

    #@229
    const-string v15, "RSSI_FETCH_FAILED"

    #@22b
    invoke-static {v15}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@22e
    goto/16 :goto_2e

    #@230
    .line 775
    :sswitch_data_230
    .sparse-switch
        0x21003 -> :sswitch_9
        0x21007 -> :sswitch_42
        0x2100b -> :sswitch_54
        0x25015 -> :sswitch_b4
        0x25016 -> :sswitch_223
    .end sparse-switch
.end method
