.class Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SupplicantStartedState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2812
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 2816
    const v1, 0xc365

    #@4
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->getName()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@b
    .line 2818
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d
    const/4 v2, 0x0

    #@e
    invoke-static {v1, v2}, Landroid/net/wifi/WifiStateMachine;->access$6402(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@11
    .line 2820
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$6500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/NetworkInfo;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, v6}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@1a
    .line 2822
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@23
    move-result-object v1

    #@24
    const v2, 0x10e000c

    #@27
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    #@2a
    move-result v0

    #@2b
    .line 2825
    .local v0, defaultInterval:I
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2d
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2f
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v2

    #@37
    const-string/jumbo v3, "wifi_supplicant_scan_interval_ms"

    #@3a
    int-to-long v4, v0

    #@3b
    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@3e
    move-result-wide v2

    #@3f
    invoke-static {v1, v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$6602(Landroid/net/wifi/WifiStateMachine;J)J

    #@42
    .line 2829
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@44
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@47
    move-result-object v1

    #@48
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4a
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$6600(Landroid/net/wifi/WifiStateMachine;)J

    #@4d
    move-result-wide v2

    #@4e
    long-to-int v2, v2

    #@4f
    div-int/lit16 v2, v2, 0x3e8

    #@51
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setScanInterval(I)V

    #@54
    .line 2831
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@56
    invoke-static {v1, v6}, Landroid/net/wifi/WifiStateMachine;->access$1102(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@59
    .line 2832
    return-void
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 2970
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$6500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/NetworkInfo;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@a
    .line 2971
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const/16 v10, 0xe

    #@2
    const/4 v6, -0x1

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    .line 2837
    iget v9, p1, Landroid/os/Message;->what:I

    #@7
    sparse-switch v9, :sswitch_data_230

    #@a
    .line 2965
    :goto_a
    return v8

    #@b
    .line 2839
    :sswitch_b
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$3500(Landroid/net/wifi/WifiStateMachine;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_20

    #@13
    .line 2840
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@15
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@17
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$6700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1a
    move-result-object v8

    #@1b
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$6800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@1e
    :cond_1e
    :goto_1e
    move v8, v7

    #@1f
    .line 2965
    goto :goto_a

    #@20
    .line 2842
    :cond_20
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@22
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@24
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$6900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@27
    move-result-object v8

    #@28
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$7000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@2b
    goto :goto_1e

    #@2c
    .line 2846
    :sswitch_2c
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2e
    const-string v9, "Connection lost, restart supplicant"

    #@30
    invoke-static {v6, v9}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@33
    .line 2847
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@35
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@38
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3a
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$3500(Landroid/net/wifi/WifiStateMachine;)Z

    #@3d
    move-result v6

    #@3e
    invoke-static {v6}, Landroid/net/wifi/WifiNative;->killSupplicant(Z)Z

    #@41
    .line 2848
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@43
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Landroid/net/wifi/WifiNative;->closeSupplicantConnection()V

    #@4a
    .line 2849
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4c
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$6500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/NetworkInfo;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v6, v8}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@53
    .line 2850
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@55
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$7100(Landroid/net/wifi/WifiStateMachine;)V

    #@58
    .line 2851
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5a
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$5800(Landroid/net/wifi/WifiStateMachine;Z)V

    #@5d
    .line 2852
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5f
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$5100(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/SupplicantStateTracker;

    #@62
    move-result-object v6

    #@63
    const v8, 0x2006f

    #@66
    invoke-virtual {v6, v8}, Landroid/net/wifi/SupplicantStateTracker;->sendMessage(I)V

    #@69
    .line 2853
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@6b
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$3500(Landroid/net/wifi/WifiStateMachine;)Z

    #@6e
    move-result v6

    #@6f
    if-eqz v6, :cond_87

    #@71
    .line 2854
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@73
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@75
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$6700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@78
    move-result-object v8

    #@79
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$7200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@7c
    .line 2858
    :goto_7c
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7e
    const v8, 0x2000b

    #@81
    const-wide/16 v9, 0x1388

    #@83
    invoke-virtual {v6, v8, v9, v10}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(IJ)V

    #@86
    goto :goto_1e

    #@87
    .line 2856
    :cond_87
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@89
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@8b
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$2000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@8e
    move-result-object v8

    #@8f
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$7300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@92
    goto :goto_7c

    #@93
    .line 2861
    :sswitch_93
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@95
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@97
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@9a
    move-result-object v9

    #@9b
    invoke-virtual {v9}, Landroid/net/wifi/WifiNative;->scanResults()Ljava/lang/String;

    #@9e
    move-result-object v9

    #@9f
    invoke-static {v6, v9}, Landroid/net/wifi/WifiStateMachine;->access$7400(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@a2
    .line 2862
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a4
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$7500(Landroid/net/wifi/WifiStateMachine;)V

    #@a7
    .line 2863
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a9
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$7602(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@ac
    goto/16 :goto_1e

    #@ae
    .line 2866
    :sswitch_ae
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b0
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {v8}, Landroid/net/wifi/WifiNative;->ping()Z

    #@b7
    move-result v2

    #@b8
    .line 2867
    .local v2, ok:Z
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ba
    iget v9, p1, Landroid/os/Message;->what:I

    #@bc
    if-eqz v2, :cond_bf

    #@be
    move v6, v7

    #@bf
    :cond_bf
    invoke-static {v8, p1, v9, v6}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@c2
    goto/16 :goto_1e

    #@c4
    .line 2870
    .end local v2           #ok:Z
    :sswitch_c4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c6
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@c8
    .line 2871
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ca
    const v8, 0x20034

    #@cd
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@cf
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@d2
    move-result-object v9

    #@d3
    invoke-virtual {v9, v0}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@d6
    move-result v9

    #@d7
    invoke-static {v6, p1, v8, v9}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@da
    goto/16 :goto_1e

    #@dc
    .line 2875
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :sswitch_dc
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@de
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@e1
    move-result-object v8

    #@e2
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@e4
    invoke-virtual {v8, v9}, Landroid/net/wifi/WifiConfigStore;->removeNetwork(I)Z

    #@e7
    move-result v2

    #@e8
    .line 2876
    .restart local v2       #ok:Z
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ea
    iget v9, p1, Landroid/os/Message;->what:I

    #@ec
    if-eqz v2, :cond_ef

    #@ee
    move v6, v7

    #@ef
    :cond_ef
    invoke-static {v8, p1, v9, v6}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@f2
    goto/16 :goto_1e

    #@f4
    .line 2879
    .end local v2           #ok:Z
    :sswitch_f4
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f6
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@f9
    move-result-object v9

    #@fa
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@fc
    iget v11, p1, Landroid/os/Message;->arg2:I

    #@fe
    if-ne v11, v7, :cond_101

    #@100
    move v8, v7

    #@101
    :cond_101
    invoke-virtual {v9, v10, v8}, Landroid/net/wifi/WifiConfigStore;->enableNetwork(IZ)Z

    #@104
    move-result v2

    #@105
    .line 2880
    .restart local v2       #ok:Z
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@107
    iget v9, p1, Landroid/os/Message;->what:I

    #@109
    if-eqz v2, :cond_10c

    #@10b
    move v6, v7

    #@10c
    :cond_10c
    invoke-static {v8, p1, v9, v6}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@10f
    goto/16 :goto_1e

    #@111
    .line 2883
    .end local v2           #ok:Z
    :sswitch_111
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@114
    move-result-wide v4

    #@115
    .line 2884
    .local v4, time:J
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@117
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$7700(Landroid/net/wifi/WifiStateMachine;)J

    #@11a
    move-result-wide v8

    #@11b
    sub-long v8, v4, v8

    #@11d
    const-wide/32 v10, 0x927c0

    #@120
    cmp-long v6, v8, v10

    #@122
    if-lez v6, :cond_1e

    #@124
    .line 2885
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@126
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@129
    move-result-object v6

    #@12a
    invoke-virtual {v6}, Landroid/net/wifi/WifiConfigStore;->enableAllNetworks()V

    #@12d
    .line 2886
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@12f
    invoke-static {v6, v4, v5}, Landroid/net/wifi/WifiStateMachine;->access$7702(Landroid/net/wifi/WifiStateMachine;J)J

    #@132
    goto/16 :goto_1e

    #@134
    .line 2890
    .end local v4           #time:J
    :sswitch_134
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@136
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@139
    move-result-object v6

    #@13a
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@13c
    invoke-virtual {v6, v9, v8}, Landroid/net/wifi/WifiConfigStore;->disableNetwork(II)Z

    #@13f
    move-result v6

    #@140
    if-ne v6, v7, :cond_14c

    #@142
    .line 2892
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@144
    const v8, 0x25013

    #@147
    invoke-static {v6, p1, v8}, Landroid/net/wifi/WifiStateMachine;->access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V

    #@14a
    goto/16 :goto_1e

    #@14c
    .line 2894
    :cond_14c
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@14e
    const v9, 0x25012

    #@151
    invoke-static {v6, p1, v9, v8}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@154
    goto/16 :goto_1e

    #@156
    .line 2899
    :sswitch_156
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@158
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@15b
    move-result-object v8

    #@15c
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15e
    check-cast v6, Ljava/lang/String;

    #@160
    invoke-virtual {v8, v6}, Landroid/net/wifi/WifiNative;->addToBlacklist(Ljava/lang/String;)Z

    #@163
    goto/16 :goto_1e

    #@165
    .line 2902
    :sswitch_165
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@167
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@16a
    move-result-object v6

    #@16b
    invoke-virtual {v6}, Landroid/net/wifi/WifiNative;->clearBlacklist()Z

    #@16e
    goto/16 :goto_1e

    #@170
    .line 2905
    :sswitch_170
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@172
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@175
    move-result-object v8

    #@176
    invoke-virtual {v8}, Landroid/net/wifi/WifiConfigStore;->saveConfig()Z

    #@179
    move-result v2

    #@17a
    .line 2906
    .restart local v2       #ok:Z
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@17c
    const v9, 0x2003a

    #@17f
    if-eqz v2, :cond_182

    #@181
    move v6, v7

    #@182
    :cond_182
    invoke-static {v8, p1, v9, v6}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@185
    .line 2909
    const-string v6, "backup"

    #@187
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@18a
    move-result-object v6

    #@18b
    invoke-static {v6}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@18e
    move-result-object v1

    #@18f
    .line 2911
    .local v1, ibm:Landroid/app/backup/IBackupManager;
    if-eqz v1, :cond_1e

    #@191
    .line 2913
    :try_start_191
    const-string v6, "com.android.providers.settings"

    #@193
    invoke-interface {v1, v6}, Landroid/app/backup/IBackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_196
    .catch Ljava/lang/Exception; {:try_start_191 .. :try_end_196} :catch_198

    #@196
    goto/16 :goto_1e

    #@198
    .line 2914
    :catch_198
    move-exception v6

    #@199
    goto/16 :goto_1e

    #@19b
    .line 2920
    .end local v1           #ibm:Landroid/app/backup/IBackupManager;
    .end local v2           #ok:Z
    :sswitch_19b
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@19d
    iget v8, p1, Landroid/os/Message;->what:I

    #@19f
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1a1
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@1a4
    move-result-object v9

    #@1a5
    invoke-virtual {v9}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@1a8
    move-result-object v9

    #@1a9
    invoke-static {v6, p1, v8, v9}, Landroid/net/wifi/WifiStateMachine;->access$1000(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@1ac
    goto/16 :goto_1e

    #@1ae
    .line 2925
    :sswitch_1ae
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1b0
    const-string v8, "Failed to start soft AP with a running supplicant"

    #@1b2
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@1b5
    .line 2926
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1b7
    invoke-static {v6, v10}, Landroid/net/wifi/WifiStateMachine;->access$2800(Landroid/net/wifi/WifiStateMachine;I)V

    #@1ba
    goto/16 :goto_1e

    #@1bc
    .line 2933
    :sswitch_1bc
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1be
    const-string v8, "Failed to start soft AP with a running supplicant"

    #@1c0
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@1c3
    .line 2934
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c5
    invoke-static {v6, v10}, Landroid/net/wifi/WifiStateMachine;->access$2800(Landroid/net/wifi/WifiStateMachine;I)V

    #@1c8
    goto/16 :goto_1e

    #@1ca
    .line 2939
    :sswitch_1ca
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1cc
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@1ce
    const/4 v10, 0x2

    #@1cf
    if-ne v9, v10, :cond_1d2

    #@1d1
    move v8, v7

    #@1d2
    :cond_1d2
    invoke-static {v6, v8}, Landroid/net/wifi/WifiStateMachine;->access$6402(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@1d5
    goto/16 :goto_1e

    #@1d7
    .line 2942
    :sswitch_1d7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d9
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@1db
    .line 2943
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1dd
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@1e0
    move-result-object v9

    #@1e1
    invoke-virtual {v9, v0}, Landroid/net/wifi/WifiConfigStore;->saveNetwork(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@1e4
    move-result-object v3

    #@1e5
    .line 2944
    .local v3, result:Landroid/net/wifi/NetworkUpdateResult;
    invoke-virtual {v3}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@1e8
    move-result v9

    #@1e9
    if-eq v9, v6, :cond_1f5

    #@1eb
    .line 2945
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1ed
    const v8, 0x25009

    #@1f0
    invoke-static {v6, p1, v8}, Landroid/net/wifi/WifiStateMachine;->access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V

    #@1f3
    goto/16 :goto_1e

    #@1f5
    .line 2947
    :cond_1f5
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1f7
    const-string v9, "Failed to save network"

    #@1f9
    invoke-static {v6, v9}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@1fc
    .line 2948
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1fe
    const v9, 0x25008

    #@201
    invoke-static {v6, p1, v9, v8}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@204
    goto/16 :goto_1e

    #@206
    .line 2953
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v3           #result:Landroid/net/wifi/NetworkUpdateResult;
    :sswitch_206
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@208
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@20b
    move-result-object v6

    #@20c
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@20e
    invoke-virtual {v6, v9}, Landroid/net/wifi/WifiConfigStore;->forgetNetwork(I)Z

    #@211
    move-result v6

    #@212
    if-eqz v6, :cond_21e

    #@214
    .line 2954
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@216
    const v8, 0x25006

    #@219
    invoke-static {v6, p1, v8}, Landroid/net/wifi/WifiStateMachine;->access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V

    #@21c
    goto/16 :goto_1e

    #@21e
    .line 2956
    :cond_21e
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@220
    const-string v9, "Failed to forget network"

    #@222
    invoke-static {v6, v9}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@225
    .line 2957
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@227
    const v9, 0x25005

    #@22a
    invoke-static {v6, p1, v9, v8}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@22d
    goto/16 :goto_1e

    #@22f
    .line 2837
    nop

    #@230
    :sswitch_data_230
    .sparse-switch
        0x2000c -> :sswitch_b
        0x20015 -> :sswitch_1ae
        0x20033 -> :sswitch_ae
        0x20034 -> :sswitch_c4
        0x20035 -> :sswitch_dc
        0x20036 -> :sswitch_f4
        0x20037 -> :sswitch_111
        0x20038 -> :sswitch_156
        0x20039 -> :sswitch_165
        0x2003a -> :sswitch_170
        0x2003b -> :sswitch_19b
        0x20048 -> :sswitch_1ca
        0x20086 -> :sswitch_1bc
        0x24002 -> :sswitch_2c
        0x24005 -> :sswitch_93
        0x25004 -> :sswitch_206
        0x25007 -> :sswitch_1d7
        0x25011 -> :sswitch_134
    .end sparse-switch
.end method
