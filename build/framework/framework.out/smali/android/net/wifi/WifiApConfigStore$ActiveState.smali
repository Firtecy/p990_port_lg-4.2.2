.class Landroid/net/wifi/WifiApConfigStore$ActiveState;
.super Lcom/android/internal/util/State;
.source "WifiApConfigStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiApConfigStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ActiveState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiApConfigStore;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiApConfigStore;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Landroid/net/wifi/WifiApConfigStore$ActiveState;->this$0:Landroid/net/wifi/WifiApConfigStore;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 137
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Landroid/net/wifi/WifiApConfigStore$ActiveState$1;

    #@4
    invoke-direct {v1, p0}, Landroid/net/wifi/WifiApConfigStore$ActiveState$1;-><init>(Landroid/net/wifi/WifiApConfigStore$ActiveState;)V

    #@7
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@d
    .line 143
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 146
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_1a

    #@5
    .line 156
    const/4 v0, 0x0

    #@6
    .line 158
    :goto_6
    return v0

    #@7
    .line 150
    :pswitch_7
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore$ActiveState;->this$0:Landroid/net/wifi/WifiApConfigStore;

    #@9
    invoke-static {v0, p1}, Landroid/net/wifi/WifiApConfigStore;->access$600(Landroid/net/wifi/WifiApConfigStore;Landroid/os/Message;)V

    #@c
    .line 158
    :goto_c
    const/4 v0, 0x1

    #@d
    goto :goto_6

    #@e
    .line 153
    :pswitch_e
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore$ActiveState;->this$0:Landroid/net/wifi/WifiApConfigStore;

    #@10
    iget-object v1, p0, Landroid/net/wifi/WifiApConfigStore$ActiveState;->this$0:Landroid/net/wifi/WifiApConfigStore;

    #@12
    invoke-static {v1}, Landroid/net/wifi/WifiApConfigStore;->access$700(Landroid/net/wifi/WifiApConfigStore;)Lcom/android/internal/util/State;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/net/wifi/WifiApConfigStore;->access$800(Landroid/net/wifi/WifiApConfigStore;Lcom/android/internal/util/IState;)V

    #@19
    goto :goto_c

    #@1a
    .line 146
    :pswitch_data_1a
    .packed-switch 0x20019
        :pswitch_7
        :pswitch_e
    .end packed-switch
.end method
