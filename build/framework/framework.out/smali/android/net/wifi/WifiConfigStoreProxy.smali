.class public Landroid/net/wifi/WifiConfigStoreProxy;
.super Ljava/lang/Object;
.source "WifiConfigStoreProxy.java"


# instance fields
.field private mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiConfigStore;)V
    .registers 2
    .parameter "wifiConfigStore"

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    iput-object p1, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@5
    .line 36
    return-void
.end method


# virtual methods
.method public addNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 43
    if-nez p1, :cond_4

    #@2
    .line 44
    const/4 v0, -0x1

    #@3
    .line 47
    :goto_3
    return v0

    #@4
    :cond_4
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@6
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@9
    move-result v0

    #@a
    goto :goto_3
.end method

.method public addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public disableNetwork(I)Z
    .registers 3
    .parameter "netId"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiConfigStore;->disableNetwork(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public disableNetwork(II)Z
    .registers 4
    .parameter "netId"
    .parameter "reason"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiConfigStore;->disableNetwork(II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public enableNetwork(IZ)Z
    .registers 4
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiConfigStore;->enableNetwork(IZ)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getConfiguredNetworks()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v0}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public saveConfig()Z
    .registers 4

    #@0
    .prologue
    .line 63
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStoreProxy;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfigStore;->saveConfig()Z

    #@5
    move-result v1

    #@6
    .line 66
    .local v1, result:Z
    if-eqz v1, :cond_19

    #@8
    .line 68
    const-string v2, "backup"

    #@a
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@11
    move-result-object v0

    #@12
    .line 70
    .local v0, ibm:Landroid/app/backup/IBackupManager;
    if-eqz v0, :cond_19

    #@14
    .line 72
    :try_start_14
    const-string v2, "com.android.providers.settings"

    #@16
    invoke-interface {v0, v2}, Landroid/app/backup/IBackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_19} :catch_1a

    #@19
    .line 79
    .end local v0           #ibm:Landroid/app/backup/IBackupManager;
    :cond_19
    :goto_19
    return v1

    #@1a
    .line 73
    .restart local v0       #ibm:Landroid/app/backup/IBackupManager;
    :catch_1a
    move-exception v2

    #@1b
    goto :goto_19
.end method
