.class public Landroid/net/wifi/WifiVZWConfigurationProxy;
.super Ljava/lang/Object;
.source "WifiVZWConfigurationProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiVZWConfigurationProxy$GroupCipher;,
        Landroid/net/wifi/WifiVZWConfigurationProxy$PairwiseCipher;,
        Landroid/net/wifi/WifiVZWConfigurationProxy$AuthAlgorithm;,
        Landroid/net/wifi/WifiVZWConfigurationProxy$Protocol;,
        Landroid/net/wifi/WifiVZWConfigurationProxy$KeyMgmt;
    }
.end annotation


# static fields
.field private static BSSIDField:Ljava/lang/reflect/Field; = null

.field private static ChannelField:Ljava/lang/reflect/Field; = null

.field private static CountryCodeField:Ljava/lang/reflect/Field; = null

.field private static INVALID_NETWORK_IDField:Ljava/lang/reflect/Field; = null

.field private static MaxscbField:Ljava/lang/reflect/Field; = null

.field private static SSIDField:Ljava/lang/reflect/Field; = null

.field private static final TAG:Ljava/lang/String; = "WifiVZWConfigurationProxy"

.field private static accept_mac_fileField:Ljava/lang/reflect/Field;

.field private static allowedAuthAlgorithmsField:Ljava/lang/reflect/Field;

.field private static allowedGroupCiphersField:Ljava/lang/reflect/Field;

.field private static allowedKeyManagementField:Ljava/lang/reflect/Field;

.field private static allowedPairwiseCiphersField:Ljava/lang/reflect/Field;

.field private static allowedProtocolsField:Ljava/lang/reflect/Field;

.field private static ap_isolateField:Ljava/lang/reflect/Field;

.field private static authModeField:Ljava/lang/reflect/Field;

.field private static c:Ljava/lang/Class;

.field private static deny_mac_fileField:Ljava/lang/reflect/Field;

.field private static encModeField:Ljava/lang/reflect/Field;

.field private static hiddenSSIDField:Ljava/lang/reflect/Field;

.field private static hw_modeField:Ljava/lang/reflect/Field;

.field private static ieee_modeField:Ljava/lang/reflect/Field;

.field private static macaddr_aclField:Ljava/lang/reflect/Field;

.field private static networkIdField:Ljava/lang/reflect/Field;

.field private static o:Ljava/lang/Object;

.field private static pariwiseField:Ljava/lang/reflect/Field;

.field private static preSharedKeyField:Ljava/lang/reflect/Field;

.field private static priorityField:Ljava/lang/reflect/Field;

.field private static secModeField:Ljava/lang/reflect/Field;

.field private static wepKeys1Field:Ljava/lang/reflect/Field;

.field private static wepKeys2Field:Ljava/lang/reflect/Field;

.field private static wepKeys3Field:Ljava/lang/reflect/Field;

.field private static wepKeys4Field:Ljava/lang/reflect/Field;

.field private static wepTxKeyIndexField:Ljava/lang/reflect/Field;


# instance fields
.field private allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

.field private allowedGroupCiphersbitset:Ljava/util/BitSet;

.field private allowedKeyManagementbitset:Ljava/util/BitSet;

.field private allowedPairwiseCiphersbitset:Ljava/util/BitSet;

.field private allowedProtocolsbitset:Ljava/util/BitSet;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 202
    new-array v2, v6, [Ljava/lang/Class;

    #@3
    .line 203
    .local v2, KeyMgmtType:[Ljava/lang/Class;
    new-array v4, v6, [Ljava/lang/Class;

    #@5
    .line 204
    .local v4, ProtocolType:[Ljava/lang/Class;
    new-array v0, v6, [Ljava/lang/Class;

    #@7
    .line 205
    .local v0, AuthAlgorithm:[Ljava/lang/Class;
    new-array v3, v6, [Ljava/lang/Class;

    #@9
    .line 206
    .local v3, PairwiseCipher:[Ljava/lang/Class;
    new-array v1, v6, [Ljava/lang/Class;

    #@b
    .line 210
    .local v1, GroupCipher:[Ljava/lang/Class;
    :try_start_b
    const-string v6, "android.net.wifi.WifiVZWConfiguration"

    #@d
    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@10
    move-result-object v6

    #@11
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->c:Ljava/lang/Class;

    #@13
    .line 211
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->c:Ljava/lang/Class;

    #@15
    invoke-virtual {v6}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@18
    move-result-object v6

    #@19
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    .line 213
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@20
    move-result-object v6

    #@21
    const-string v7, "SSID"

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@26
    move-result-object v6

    #@27
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->SSIDField:Ljava/lang/reflect/Field;

    #@29
    .line 214
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2b
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@2e
    move-result-object v6

    #@2f
    const-string/jumbo v7, "networkId"

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@35
    move-result-object v6

    #@36
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->networkIdField:Ljava/lang/reflect/Field;

    #@38
    .line 215
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@3a
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3d
    move-result-object v6

    #@3e
    const-string v7, "INVALID_NETWORK_ID"

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@43
    move-result-object v6

    #@44
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->INVALID_NETWORK_IDField:Ljava/lang/reflect/Field;

    #@46
    .line 216
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@48
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4b
    move-result-object v6

    #@4c
    const-string v7, "INVALID_NETWORK_ID"

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@51
    move-result-object v6

    #@52
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->BSSIDField:Ljava/lang/reflect/Field;

    #@54
    .line 217
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@56
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@59
    move-result-object v6

    #@5a
    const-string/jumbo v7, "preSharedKey"

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@60
    move-result-object v6

    #@61
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->preSharedKeyField:Ljava/lang/reflect/Field;

    #@63
    .line 219
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@65
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@68
    move-result-object v6

    #@69
    const-string/jumbo v7, "wepKeys1"

    #@6c
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@6f
    move-result-object v6

    #@70
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys1Field:Ljava/lang/reflect/Field;

    #@72
    .line 220
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@74
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@77
    move-result-object v6

    #@78
    const-string/jumbo v7, "wepKeys2"

    #@7b
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@7e
    move-result-object v6

    #@7f
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys2Field:Ljava/lang/reflect/Field;

    #@81
    .line 221
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@83
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@86
    move-result-object v6

    #@87
    const-string/jumbo v7, "wepKeys3"

    #@8a
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@8d
    move-result-object v6

    #@8e
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys3Field:Ljava/lang/reflect/Field;

    #@90
    .line 222
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@92
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@95
    move-result-object v6

    #@96
    const-string/jumbo v7, "wepKeys4"

    #@99
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@9c
    move-result-object v6

    #@9d
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys4Field:Ljava/lang/reflect/Field;

    #@9f
    .line 224
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@a1
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a4
    move-result-object v6

    #@a5
    const-string/jumbo v7, "wepTxKeyIndex"

    #@a8
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@ab
    move-result-object v6

    #@ac
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepTxKeyIndexField:Ljava/lang/reflect/Field;

    #@ae
    .line 225
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@b0
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b3
    move-result-object v6

    #@b4
    const-string/jumbo v7, "priority"

    #@b7
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@ba
    move-result-object v6

    #@bb
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->priorityField:Ljava/lang/reflect/Field;

    #@bd
    .line 226
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@bf
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c2
    move-result-object v6

    #@c3
    const-string v7, "hiddenSSID"

    #@c5
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@c8
    move-result-object v6

    #@c9
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->hiddenSSIDField:Ljava/lang/reflect/Field;

    #@cb
    .line 227
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@cd
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@d0
    move-result-object v6

    #@d1
    const-string v7, "allowedKeyManagement"

    #@d3
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@d6
    move-result-object v6

    #@d7
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementField:Ljava/lang/reflect/Field;

    #@d9
    .line 228
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@db
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@de
    move-result-object v6

    #@df
    const-string v7, "allowedProtocols"

    #@e1
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@e4
    move-result-object v6

    #@e5
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsField:Ljava/lang/reflect/Field;

    #@e7
    .line 229
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@e9
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@ec
    move-result-object v6

    #@ed
    const-string v7, "allowedAuthAlgorithms"

    #@ef
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@f2
    move-result-object v6

    #@f3
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsField:Ljava/lang/reflect/Field;

    #@f5
    .line 230
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f7
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@fa
    move-result-object v6

    #@fb
    const-string v7, "allowedPairwiseCiphers"

    #@fd
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@100
    move-result-object v6

    #@101
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersField:Ljava/lang/reflect/Field;

    #@103
    .line 231
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@105
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@108
    move-result-object v6

    #@109
    const-string v7, "allowedGroupCiphers"

    #@10b
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@10e
    move-result-object v6

    #@10f
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersField:Ljava/lang/reflect/Field;

    #@111
    .line 232
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@113
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@116
    move-result-object v6

    #@117
    const-string v7, "Channel"

    #@119
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@11c
    move-result-object v6

    #@11d
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->ChannelField:Ljava/lang/reflect/Field;

    #@11f
    .line 233
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@121
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@124
    move-result-object v6

    #@125
    const-string v7, "CountryCode"

    #@127
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@12a
    move-result-object v6

    #@12b
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->CountryCodeField:Ljava/lang/reflect/Field;

    #@12d
    .line 234
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@12f
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@132
    move-result-object v6

    #@133
    const-string v7, "Maxscb"

    #@135
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@138
    move-result-object v6

    #@139
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->MaxscbField:Ljava/lang/reflect/Field;

    #@13b
    .line 235
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@13d
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@140
    move-result-object v6

    #@141
    const-string v7, "hw_mode"

    #@143
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@146
    move-result-object v6

    #@147
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->hw_modeField:Ljava/lang/reflect/Field;

    #@149
    .line 236
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@14b
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@14e
    move-result-object v6

    #@14f
    const-string v7, "ap_isolate"

    #@151
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@154
    move-result-object v6

    #@155
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->ap_isolateField:Ljava/lang/reflect/Field;

    #@157
    .line 237
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@159
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@15c
    move-result-object v6

    #@15d
    const-string v7, "ieee_mode"

    #@15f
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@162
    move-result-object v6

    #@163
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->ieee_modeField:Ljava/lang/reflect/Field;

    #@165
    .line 238
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@167
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@16a
    move-result-object v6

    #@16b
    const-string/jumbo v7, "macaddr_acl"

    #@16e
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@171
    move-result-object v6

    #@172
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->macaddr_aclField:Ljava/lang/reflect/Field;

    #@174
    .line 239
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@176
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@179
    move-result-object v6

    #@17a
    const-string v7, "accept_mac_file"

    #@17c
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@17f
    move-result-object v6

    #@180
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->accept_mac_fileField:Ljava/lang/reflect/Field;

    #@182
    .line 240
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@184
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@187
    move-result-object v6

    #@188
    const-string v7, "deny_mac_file"

    #@18a
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18d
    move-result-object v6

    #@18e
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->deny_mac_fileField:Ljava/lang/reflect/Field;

    #@190
    .line 241
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@192
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@195
    move-result-object v6

    #@196
    const-string v7, "authMode"

    #@198
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19b
    move-result-object v6

    #@19c
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->authModeField:Ljava/lang/reflect/Field;

    #@19e
    .line 242
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1a0
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a3
    move-result-object v6

    #@1a4
    const-string/jumbo v7, "secMode"

    #@1a7
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@1aa
    move-result-object v6

    #@1ab
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->secModeField:Ljava/lang/reflect/Field;

    #@1ad
    .line 243
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1af
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1b2
    move-result-object v6

    #@1b3
    const-string v7, "encMode"

    #@1b5
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@1b8
    move-result-object v6

    #@1b9
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->encModeField:Ljava/lang/reflect/Field;

    #@1bb
    .line 244
    sget-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1bd
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c0
    move-result-object v6

    #@1c1
    const-string/jumbo v7, "pariwise"

    #@1c4
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@1c7
    move-result-object v6

    #@1c8
    sput-object v6, Landroid/net/wifi/WifiVZWConfigurationProxy;->pariwiseField:Ljava/lang/reflect/Field;
    :try_end_1ca
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_1ca} :catch_1cb

    #@1ca
    .line 249
    :goto_1ca
    return-void

    #@1cb
    .line 246
    :catch_1cb
    move-exception v5

    #@1cc
    .line 247
    .local v5, t:Ljava/lang/Throwable;
    const-string v6, "WifiVZWConfigurationProxy"

    #@1ce
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d3
    const-string v8, "Unable to find NetworkManagementService"

    #@1d5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v7

    #@1d9
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v7

    #@1dd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e0
    move-result-object v7

    #@1e1
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e4
    goto :goto_1ca
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 256
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    new-instance v0, Ljava/util/BitSet;

    #@5
    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@a
    .line 58
    new-instance v0, Ljava/util/BitSet;

    #@c
    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    #@f
    iput-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@11
    .line 59
    new-instance v0, Ljava/util/BitSet;

    #@13
    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    #@16
    iput-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@18
    .line 60
    new-instance v0, Ljava/util/BitSet;

    #@1a
    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@1f
    .line 61
    new-instance v0, Ljava/util/BitSet;

    #@21
    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    #@24
    iput-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@26
    .line 258
    return-void
.end method


# virtual methods
.method public clearWepWPAKey()V
    .registers 6

    #@0
    .prologue
    .line 407
    :try_start_0
    const-string v0, ""

    #@2
    .line 408
    .local v0, clearKey:Ljava/lang/String;
    const-string v2, "WifiVZWConfigurationProxy"

    #@4
    const-string v3, "clearWepWPAKey"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 410
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys1Field:Ljava/lang/reflect/Field;

    #@b
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@d
    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@10
    .line 411
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys2Field:Ljava/lang/reflect/Field;

    #@12
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@14
    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@17
    .line 412
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys3Field:Ljava/lang/reflect/Field;

    #@19
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@1e
    .line 413
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys4Field:Ljava/lang/reflect/Field;

    #@20
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@22
    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@25
    .line 414
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->preSharedKeyField:Ljava/lang/reflect/Field;

    #@27
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@29
    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2c
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2c} :catch_2d

    #@2c
    .line 420
    .end local v0           #clearKey:Ljava/lang/String;
    :goto_2c
    return-void

    #@2d
    .line 417
    :catch_2d
    move-exception v1

    #@2e
    .line 418
    .local v1, t:Ljava/lang/Throwable;
    const-string v2, "WifiVZWConfigurationProxy"

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v4, "Unable to find clearWepWPAKey "

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_2c
.end method

.method public clearallowedAuthAlgorithmsbitset()V
    .registers 5

    #@0
    .prologue
    .line 635
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    const-string v2, "clearallowedAuthAlgorithmsbitset "

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 636
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@d
    .line 637
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@13
    .line 638
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@15
    const/4 v2, 0x2

    #@16
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_19} :catch_1a

    #@19
    .line 644
    :goto_19
    return-void

    #@1a
    .line 641
    :catch_1a
    move-exception v0

    #@1b
    .line 642
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "clearallowedKeyManagementbitset "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_19
.end method

.method public clearallowedGroupCiphersbitset()V
    .registers 5

    #@0
    .prologue
    .line 716
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    const-string v2, "clearallowedGroupCiphersbitset"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 717
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@d
    .line 718
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@13
    .line 719
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@15
    const/4 v2, 0x2

    #@16
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@19
    .line 720
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V
    :try_end_1f
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_1f} :catch_20

    #@1f
    .line 726
    :goto_1f
    return-void

    #@20
    .line 723
    :catch_20
    move-exception v0

    #@21
    .line 724
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "clearallowedGroupCiphersbitset "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_1f
.end method

.method public clearallowedKeyManagementbitset()V
    .registers 5

    #@0
    .prologue
    .line 547
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    const-string v2, "clearallowedKeyManagementbitset "

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 548
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@d
    .line 549
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@13
    .line 550
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@15
    const/4 v2, 0x2

    #@16
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@19
    .line 551
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@1f
    .line 552
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@21
    const/4 v2, 0x4

    #@22
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@25
    .line 553
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@27
    const/4 v2, 0x5

    #@28
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2b} :catch_2c

    #@2b
    .line 559
    :goto_2b
    return-void

    #@2c
    .line 556
    :catch_2c
    move-exception v0

    #@2d
    .line 557
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "clearallowedKeyManagementbitset "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_2b
.end method

.method public clearallowedPairwiseCiphersbitset()V
    .registers 5

    #@0
    .prologue
    .line 675
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    const-string v2, "clearallowedPairwiseCiphersbitset "

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 676
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@d
    .line 677
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@13
    .line 678
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@15
    const/4 v2, 0x2

    #@16
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_19} :catch_1a

    #@19
    .line 684
    :goto_19
    return-void

    #@1a
    .line 681
    :catch_1a
    move-exception v0

    #@1b
    .line 682
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "clearallowedPairwiseCiphersbitset "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_19
.end method

.method public clearallowedProtocolbitset()V
    .registers 5

    #@0
    .prologue
    .line 596
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    const-string v2, "clearallowedProtocolbitset "

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 597
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    #@d
    .line 598
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_13} :catch_14

    #@13
    .line 604
    :goto_13
    return-void

    #@14
    .line 601
    :catch_14
    move-exception v0

    #@15
    .line 602
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "clearallowedProtocolbitset "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_13
.end method

.method public getAuthType()I
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    const/4 v3, 0x3

    #@2
    const/4 v2, 0x2

    #@3
    const/4 v0, 0x1

    #@4
    .line 758
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@6
    invoke-virtual {v4, v0}, Ljava/util/BitSet;->get(I)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_14

    #@c
    .line 759
    const-string v1, "WifiVZWConfigurationProxy"

    #@e
    const-string v2, "getAuthType WPA_PSK"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 771
    :goto_13
    return v0

    #@14
    .line 761
    :cond_14
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@16
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_25

    #@1c
    .line 762
    const-string v0, "WifiVZWConfigurationProxy"

    #@1e
    const-string v2, "getAuthType WPA2_PSK"

    #@20
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    move v0, v1

    #@24
    .line 763
    goto :goto_13

    #@25
    .line 764
    :cond_25
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@27
    invoke-virtual {v0, v2}, Ljava/util/BitSet;->get(I)Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_36

    #@2d
    .line 765
    const-string v0, "WifiVZWConfigurationProxy"

    #@2f
    const-string v1, "getAuthType WPA_EAP"

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    move v0, v2

    #@35
    .line 766
    goto :goto_13

    #@36
    .line 767
    :cond_36
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@38
    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_47

    #@3e
    .line 768
    const-string v0, "WifiVZWConfigurationProxy"

    #@40
    const-string v1, "getAuthType IEEE8021X"

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    move v0, v3

    #@46
    .line 769
    goto :goto_13

    #@47
    .line 771
    :cond_47
    const/4 v0, 0x0

    #@48
    goto :goto_13
.end method

.method public getBSSIDField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 309
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getBSSIDField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "BSSID"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 311
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->BSSIDField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_34} :catch_35

    #@34
    .line 315
    :goto_34
    return-object v1

    #@35
    .line 312
    :catch_35
    move-exception v0

    #@36
    .line 313
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Unable to find getBSSIDField "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 315
    const/4 v1, 0x0

    #@4f
    goto :goto_34
.end method

.method public getChannelField()I
    .registers 6

    #@0
    .prologue
    .line 776
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getChannelField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "Channel"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 779
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->ChannelField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 783
    :goto_38
    return v1

    #@39
    .line 780
    :catch_39
    move-exception v0

    #@3a
    .line 781
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getChannelField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 783
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public getINVALID_NETWORK_IDField()I
    .registers 6

    #@0
    .prologue
    .line 286
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getzField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "INVALID_NETWORK_ID"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 289
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->INVALID_NETWORK_IDField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 293
    :goto_38
    return v1

    #@39
    .line 290
    :catch_39
    move-exception v0

    #@3a
    .line 291
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getINVALID_NETWORK_IDField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 293
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public getMaxscbField()I
    .registers 6

    #@0
    .prologue
    .line 821
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getMaxscbField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "Maxscb"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 824
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->MaxscbField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 828
    :goto_38
    return v1

    #@39
    .line 825
    :catch_39
    move-exception v0

    #@3a
    .line 826
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getMaxscbField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 828
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public getSSIDField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 275
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getSSIDField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "SSID"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 277
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->SSIDField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_34} :catch_35

    #@34
    .line 281
    :goto_34
    return-object v1

    #@35
    .line 278
    :catch_35
    move-exception v0

    #@36
    .line 279
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Unable to find getSSIDField "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 281
    const/4 v1, 0x0

    #@4f
    goto :goto_34
.end method

.method public getaccept_mac_fileField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 936
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getaccept_mac_fileField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "accept_mac_file"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 940
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->accept_mac_fileField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_34} :catch_35

    #@34
    .line 944
    :goto_34
    return-object v1

    #@35
    .line 941
    :catch_35
    move-exception v0

    #@36
    .line 942
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Unable to find getaccept_mac_fileField "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 944
    const/4 v1, 0x0

    #@4f
    goto :goto_34
.end method

.method public getallowedAuthAlgorithmsField(I)Ljava/util/BitSet;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 648
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getallowedAuthAlgorithmsField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 651
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsField:Ljava/lang/reflect/Field;

    #@1a
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Ljava/util/BitSet;

    #@22
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@24
    .line 652
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@26
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@29
    .line 653
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2b} :catch_2c

    #@2b
    .line 657
    :goto_2b
    return-object v1

    #@2c
    .line 654
    :catch_2c
    move-exception v0

    #@2d
    .line 655
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Unable to find getallowedAuthAlgorithmsField "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 657
    const/4 v1, 0x0

    #@46
    goto :goto_2b
.end method

.method public getallowedGroupCiphersField(I)Ljava/util/BitSet;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 730
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getallowedGroupCiphersField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 733
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersField:Ljava/lang/reflect/Field;

    #@1a
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Ljava/util/BitSet;

    #@22
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@24
    .line 734
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@26
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@29
    .line 735
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2b} :catch_2c

    #@2b
    .line 739
    :goto_2b
    return-object v1

    #@2c
    .line 736
    :catch_2c
    move-exception v0

    #@2d
    .line 737
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Unable to find getallowedGroupCiphersField "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 739
    const/4 v1, 0x0

    #@46
    goto :goto_2b
.end method

.method public getallowedKeyManagementField(I)Ljava/util/BitSet;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 566
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getallowedKeyManagementField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 569
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementField:Ljava/lang/reflect/Field;

    #@1a
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Ljava/util/BitSet;

    #@22
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@24
    .line 572
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_26} :catch_27

    #@26
    .line 576
    :goto_26
    return-object v1

    #@27
    .line 573
    :catch_27
    move-exception v0

    #@28
    .line 574
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Unable to find getallowedKeyManagementField "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 576
    const/4 v1, 0x0

    #@41
    goto :goto_26
.end method

.method public getallowedPairwiseCiphersField(I)Ljava/util/BitSet;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 688
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getallowedPairwiseCiphersField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 691
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersField:Ljava/lang/reflect/Field;

    #@1a
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Ljava/util/BitSet;

    #@22
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@24
    .line 692
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@26
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@29
    .line 693
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2b} :catch_2c

    #@2b
    .line 697
    :goto_2b
    return-object v1

    #@2c
    .line 694
    :catch_2c
    move-exception v0

    #@2d
    .line 695
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Unable to find getallowedPairwiseCiphersField "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 697
    const/4 v1, 0x0

    #@46
    goto :goto_2b
.end method

.method public getallowedProtocolsField(I)Ljava/util/BitSet;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 608
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getallowedProtocolsField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 611
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsField:Ljava/lang/reflect/Field;

    #@1a
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Ljava/util/BitSet;

    #@22
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@24
    .line 612
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@26
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@29
    .line 613
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2b} :catch_2c

    #@2b
    .line 617
    :goto_2b
    return-object v1

    #@2c
    .line 614
    :catch_2c
    move-exception v0

    #@2d
    .line 615
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Unable to find getallowedProtocolsField "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 617
    const/4 v1, 0x0

    #@46
    goto :goto_2b
.end method

.method public getap_isolateField()I
    .registers 6

    #@0
    .prologue
    .line 867
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getap_isolateField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "ap_isolate"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 870
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->ap_isolateField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 874
    :goto_38
    return v1

    #@39
    .line 871
    :catch_39
    move-exception v0

    #@3a
    .line 872
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getap_isolateField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 874
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public getauthModeField()I
    .registers 6

    #@0
    .prologue
    .line 984
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getauthModeField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "authMode"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 987
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->authModeField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 991
    :goto_38
    return v1

    #@39
    .line 988
    :catch_39
    move-exception v0

    #@3a
    .line 989
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getauthModeField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 991
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public getcountryCodeField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 799
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getCountryCodeField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "CountryCode"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 801
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->CountryCodeField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_34} :catch_35

    #@34
    .line 805
    :goto_34
    return-object v1

    #@35
    .line 802
    :catch_35
    move-exception v0

    #@36
    .line 803
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Unable to find getCountryCodeField "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 805
    const/4 v1, 0x0

    #@4f
    goto :goto_34
.end method

.method public getdeny_mac_fileField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 960
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getdeny_mac_fileField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "deny_mac_file"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 964
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->deny_mac_fileField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_34} :catch_35

    #@34
    .line 968
    :goto_34
    return-object v1

    #@35
    .line 965
    :catch_35
    move-exception v0

    #@36
    .line 966
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Unable to find getdeny_mac_fileField "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 968
    const/4 v1, 0x0

    #@4f
    goto :goto_34
.end method

.method public getencModeField()I
    .registers 6

    #@0
    .prologue
    .line 1030
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getencModeField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "encMode"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1033
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->encModeField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 1037
    :goto_38
    return v1

    #@39
    .line 1034
    :catch_39
    move-exception v0

    #@3a
    .line 1035
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getencModeField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 1037
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public gethiddenSSIDField()Z
    .registers 6

    #@0
    .prologue
    .line 524
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "gethiddenSSIDField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "hiddenSSID"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 527
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->hiddenSSIDField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Boolean;

    #@34
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 531
    :goto_38
    return v1

    #@39
    .line 528
    :catch_39
    move-exception v0

    #@3a
    .line 529
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find gethiddenSSIDField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 531
    const/4 v1, 0x0

    #@53
    goto :goto_38
.end method

.method public gethw_modeField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 844
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "gethw_modeField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "hw_mode"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 847
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->hw_modeField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_34} :catch_35

    #@34
    .line 851
    :goto_34
    return-object v1

    #@35
    .line 848
    :catch_35
    move-exception v0

    #@36
    .line 849
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Unable to find gethw_modeField "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 851
    const/4 v1, 0x0

    #@4f
    goto :goto_34
.end method

.method public getieee_modeField()I
    .registers 6

    #@0
    .prologue
    .line 890
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getieee_modeField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string v4, "ieee_mode"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@18
    move-result-object v3

    #@19
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 893
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->ieee_modeField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/Integer;

    #@34
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_37} :catch_39

    #@37
    move-result v1

    #@38
    .line 897
    :goto_38
    return v1

    #@39
    .line 894
    :catch_39
    move-exception v0

    #@3a
    .line 895
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Unable to find getieee_modeField "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 897
    const/4 v1, -0x1

    #@53
    goto :goto_38
.end method

.method public getmacaddr_aclField()I
    .registers 6

    #@0
    .prologue
    .line 913
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getmacaddr_aclField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "macaddr_acl"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 916
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->macaddr_aclField:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/Integer;

    #@35
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_38} :catch_3a

    #@38
    move-result v1

    #@39
    .line 920
    :goto_39
    return v1

    #@3a
    .line 917
    :catch_3a
    move-exception v0

    #@3b
    .line 918
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Unable to find getmacaddr_aclField "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 920
    const/4 v1, -0x1

    #@54
    goto :goto_39
.end method

.method public getpariwiseField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1053
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getpariwiseField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "pariwise"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1057
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->pariwiseField:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 1061
    :goto_35
    return-object v1

    #@36
    .line 1058
    :catch_36
    move-exception v0

    #@37
    .line 1059
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unable to find getpariwiseField "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1061
    const/4 v1, 0x0

    #@50
    goto :goto_35
.end method

.method public getpreSharedKeyField()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 331
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getPreSharedKeyField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "preSharedKey"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 334
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->preSharedKeyField:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 338
    :goto_35
    return-object v1

    #@36
    .line 335
    :catch_36
    move-exception v0

    #@37
    .line 336
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unable to find getPreSharedKeyField "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 338
    const/4 v1, 0x0

    #@50
    goto :goto_35
.end method

.method public getpriorityField()I
    .registers 6

    #@0
    .prologue
    .line 500
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getpriorityField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "priority"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 503
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->priorityField:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/Integer;

    #@35
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_38} :catch_3a

    #@38
    move-result v1

    #@39
    .line 507
    :goto_39
    return v1

    #@3a
    .line 504
    :catch_3a
    move-exception v0

    #@3b
    .line 505
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Unable to find getpriorityField "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 507
    const/4 v1, -0x1

    #@54
    goto :goto_39
.end method

.method public getsecModeField()I
    .registers 6

    #@0
    .prologue
    .line 1007
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getsecModeField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "secMode"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1010
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->secModeField:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/Integer;

    #@35
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_38} :catch_3a

    #@38
    move-result v1

    #@39
    .line 1014
    :goto_39
    return v1

    #@3a
    .line 1011
    :catch_3a
    move-exception v0

    #@3b
    .line 1012
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Unable to find getsecModeField "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 1014
    const/4 v1, -0x1

    #@54
    goto :goto_39
.end method

.method public getwepKeysField1()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 355
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getwepKeysField1 "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "wepKeys1"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 358
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys1Field:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 363
    :goto_35
    return-object v1

    #@36
    .line 360
    :catch_36
    move-exception v0

    #@37
    .line 361
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unable to find getwepKeysField1 "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 363
    const/4 v1, 0x0

    #@50
    goto :goto_35
.end method

.method public getwepKeysField2()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 368
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getwepKeysField2 "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "wepKeys2"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 371
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys2Field:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 376
    :goto_35
    return-object v1

    #@36
    .line 373
    :catch_36
    move-exception v0

    #@37
    .line 374
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unable to find getwepKeysField2 "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 376
    const/4 v1, 0x0

    #@50
    goto :goto_35
.end method

.method public getwepKeysField3()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 381
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getwepKeysField3 "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "wepKeys3"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 384
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys3Field:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 389
    :goto_35
    return-object v1

    #@36
    .line 386
    :catch_36
    move-exception v0

    #@37
    .line 387
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unable to find getwepKeysField3 "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 389
    const/4 v1, 0x0

    #@50
    goto :goto_35
.end method

.method public getwepKeysField4()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 394
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getwepKeysField4 "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "wepKeys4"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 397
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys4Field:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/String;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 402
    :goto_35
    return-object v1

    #@36
    .line 399
    :catch_36
    move-exception v0

    #@37
    .line 400
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unable to find getwepKeysField4 "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 402
    const/4 v1, 0x0

    #@50
    goto :goto_35
.end method

.method public getwepTxKeyIndexField()I
    .registers 6

    #@0
    .prologue
    .line 477
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "getwepTxKeyIndexField "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@12
    move-result-object v3

    #@13
    const-string/jumbo v4, "wepTxKeyIndex"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 480
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepTxKeyIndexField:Ljava/lang/reflect/Field;

    #@2d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Ljava/lang/Integer;

    #@35
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_38} :catch_3a

    #@38
    move-result v1

    #@39
    .line 484
    :goto_39
    return v1

    #@3a
    .line 481
    :catch_3a
    move-exception v0

    #@3b
    .line 482
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Unable to find getwepTxKeyIndexField "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 484
    const/4 v1, -0x1

    #@54
    goto :goto_39
.end method

.method public setBSSIDField(Ljava/lang/String;)V
    .registers 6
    .parameter "bssid"

    #@0
    .prologue
    .line 320
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setSSIDField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 321
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->BSSIDField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    .line 327
    :goto_20
    return-void

    #@21
    .line 324
    :catch_21
    move-exception v0

    #@22
    .line 325
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Unable to find setBSSIDField "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20
.end method

.method public setChannelField(I)V
    .registers 6
    .parameter "channel"

    #@0
    .prologue
    .line 788
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setChannelField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 789
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->ChannelField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 795
    :goto_24
    return-void

    #@25
    .line 792
    :catch_25
    move-exception v0

    #@26
    .line 793
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setChannelField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setINVALID_NETWORK_IDField(I)V
    .registers 6
    .parameter "NETWORK_ID"

    #@0
    .prologue
    .line 298
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setSSIDField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 299
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->INVALID_NETWORK_IDField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 305
    :goto_24
    return-void

    #@25
    .line 302
    :catch_25
    move-exception v0

    #@26
    .line 303
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setINVALID_NETWORK_IDField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setMaxscbField(I)V
    .registers 6
    .parameter "maxscb"

    #@0
    .prologue
    .line 833
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setMaxscbField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 834
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->MaxscbField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 840
    :goto_24
    return-void

    #@25
    .line 837
    :catch_25
    move-exception v0

    #@26
    .line 838
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setMaxscbField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setSSIDField(Ljava/lang/String;)V
    .registers 7
    .parameter "ssid"

    #@0
    .prologue
    .line 263
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setSSIDField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 264
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->SSIDField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@20
    .line 266
    const-string v1, "WifiVZWConfigurationProxy"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string/jumbo v3, "setSSIDField2 "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    sget-object v3, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@30
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@33
    move-result-object v3

    #@34
    const-string v4, "SSID"

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@39
    move-result-object v3

    #@3a
    sget-object v4, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_4b} :catch_4c

    #@4b
    .line 271
    :goto_4b
    return-void

    #@4c
    .line 268
    :catch_4c
    move-exception v0

    #@4d
    .line 269
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@4f
    new-instance v2, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v3, "Unable to find setSSIDField "

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_4b
.end method

.method public setaccept_mac_fileField(Ljava/lang/String;)V
    .registers 6
    .parameter "accept_mac_file"

    #@0
    .prologue
    .line 949
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setaccept_mac_fileField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 950
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->accept_mac_fileField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    .line 956
    :goto_20
    return-void

    #@21
    .line 953
    :catch_21
    move-exception v0

    #@22
    .line 954
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Unable to find setaccept_mac_fileField "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20
.end method

.method public setallowedAuthAlgorithmsField(I)V
    .registers 6
    .parameter "allowedauthalgorithms"

    #@0
    .prologue
    .line 662
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearallowedAuthAlgorithmsbitset()V

    #@3
    .line 663
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v3, "setallowedAuthAlgorithmsField "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 664
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@1e
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@21
    .line 665
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsField:Ljava/lang/reflect/Field;

    #@23
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@25
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedAuthAlgorithmsbitset:Ljava/util/BitSet;

    #@27
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2a} :catch_2b

    #@2a
    .line 671
    :goto_2a
    return-void

    #@2b
    .line 668
    :catch_2b
    move-exception v0

    #@2c
    .line 669
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2e
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "Unable to find setallowedAuthAlgorithmsField "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_2a
.end method

.method public setallowedGroupCiphersField(I)V
    .registers 6
    .parameter "allowedgroupciphers"

    #@0
    .prologue
    .line 745
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearallowedGroupCiphersbitset()V

    #@3
    .line 746
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v3, "setallowedGroupCiphersField "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 747
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@1e
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@21
    .line 748
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersField:Ljava/lang/reflect/Field;

    #@23
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@25
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedGroupCiphersbitset:Ljava/util/BitSet;

    #@27
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2a} :catch_2b

    #@2a
    .line 754
    :goto_2a
    return-void

    #@2b
    .line 751
    :catch_2b
    move-exception v0

    #@2c
    .line 752
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2e
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "Unable to find setallowedGroupCiphersField "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_2a
.end method

.method public setallowedKeyManagementField(I)V
    .registers 6
    .parameter "allowedkeymanagement"

    #@0
    .prologue
    .line 581
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setpreSharedKeyField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 582
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearallowedKeyManagementbitset()V

    #@1c
    .line 583
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementField:Ljava/lang/reflect/Field;

    #@1e
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@20
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@22
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    #@25
    .line 585
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@27
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@2a
    .line 587
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementField:Ljava/lang/reflect/Field;

    #@2c
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@2e
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedKeyManagementbitset:Ljava/util/BitSet;

    #@30
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_33
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_33} :catch_34

    #@33
    .line 592
    :goto_33
    return-void

    #@34
    .line 589
    :catch_34
    move-exception v0

    #@35
    .line 590
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@37
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v3, "Unable to find setallowedKeyManagementField "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_33
.end method

.method public setallowedPairwiseCiphersField(I)V
    .registers 6
    .parameter "allowedpairwiseciphers"

    #@0
    .prologue
    .line 703
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearallowedPairwiseCiphersbitset()V

    #@3
    .line 704
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v3, "setallowedPairwiseCiphersField "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 705
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@1e
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@21
    .line 706
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersField:Ljava/lang/reflect/Field;

    #@23
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@25
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedPairwiseCiphersbitset:Ljava/util/BitSet;

    #@27
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2a} :catch_2b

    #@2a
    .line 712
    :goto_2a
    return-void

    #@2b
    .line 709
    :catch_2b
    move-exception v0

    #@2c
    .line 710
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2e
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "Unable to find setallowedPairwiseCiphersField "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_2a
.end method

.method public setallowedProtocolsField(I)V
    .registers 6
    .parameter "allowedprotocols"

    #@0
    .prologue
    .line 622
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearallowedProtocolbitset()V

    #@3
    .line 623
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v3, "setallowedProtocolsField "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 624
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@1e
    invoke-virtual {v1, p1}, Ljava/util/BitSet;->set(I)V

    #@21
    .line 625
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsField:Ljava/lang/reflect/Field;

    #@23
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@25
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfigurationProxy;->allowedProtocolsbitset:Ljava/util/BitSet;

    #@27
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2a} :catch_2b

    #@2a
    .line 631
    :goto_2a
    return-void

    #@2b
    .line 628
    :catch_2b
    move-exception v0

    #@2c
    .line 629
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@2e
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "Unable to find setallowedProtocolsField "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_2a
.end method

.method public setap_isolateField(I)V
    .registers 6
    .parameter "ap_isolate"

    #@0
    .prologue
    .line 879
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setap_isolateField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 880
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->ap_isolateField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 886
    :goto_24
    return-void

    #@25
    .line 883
    :catch_25
    move-exception v0

    #@26
    .line 884
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setap_isolateField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setauthModeField(I)V
    .registers 6
    .parameter "authMode"

    #@0
    .prologue
    .line 996
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setauthModeField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 997
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->authModeField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 1003
    :goto_24
    return-void

    #@25
    .line 1000
    :catch_25
    move-exception v0

    #@26
    .line 1001
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setauthModeField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setcountryCodeField(Ljava/lang/String;)V
    .registers 6
    .parameter "countrycode"

    #@0
    .prologue
    .line 810
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setCountryCodeField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 811
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->CountryCodeField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    .line 817
    :goto_20
    return-void

    #@21
    .line 814
    :catch_21
    move-exception v0

    #@22
    .line 815
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Unable to find setCountryCodeField "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20
.end method

.method public setdeny_mac_fileField(Ljava/lang/String;)V
    .registers 6
    .parameter "deny_mac_file"

    #@0
    .prologue
    .line 973
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setdeny_mac_fileField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 974
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->deny_mac_fileField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    .line 980
    :goto_20
    return-void

    #@21
    .line 977
    :catch_21
    move-exception v0

    #@22
    .line 978
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Unable to find setdeny_mac_fileField "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20
.end method

.method public setencModeField(I)V
    .registers 6
    .parameter "encMode"

    #@0
    .prologue
    .line 1042
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setencModeField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1043
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->encModeField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 1049
    :goto_24
    return-void

    #@25
    .line 1046
    :catch_25
    move-exception v0

    #@26
    .line 1047
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setencModeField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public sethiddenSSIDField(Z)V
    .registers 6
    .parameter "hiddenssid"

    #@0
    .prologue
    .line 536
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "sethiddenSSIDField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 537
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->hiddenSSIDField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 543
    :goto_24
    return-void

    #@25
    .line 540
    :catch_25
    move-exception v0

    #@26
    .line 541
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find sethiddenSSIDField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public sethw_modeField(Ljava/lang/String;)V
    .registers 6
    .parameter "hw_mode"

    #@0
    .prologue
    .line 856
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "sethw_modeField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 857
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->hw_modeField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    .line 863
    :goto_20
    return-void

    #@21
    .line 860
    :catch_21
    move-exception v0

    #@22
    .line 861
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Unable to find sethw_modeField "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20
.end method

.method public setieee_modeField(I)V
    .registers 6
    .parameter "ieee_mode"

    #@0
    .prologue
    .line 902
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setieee_modeField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 903
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->ieee_modeField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 909
    :goto_24
    return-void

    #@25
    .line 906
    :catch_25
    move-exception v0

    #@26
    .line 907
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setieee_modeField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setmacaddr_aclField(I)V
    .registers 6
    .parameter "macaddr_acl"

    #@0
    .prologue
    .line 925
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setmacaddr_aclField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 926
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->macaddr_aclField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 932
    :goto_24
    return-void

    #@25
    .line 929
    :catch_25
    move-exception v0

    #@26
    .line 930
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setmacaddr_aclField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setpariwiseField(Ljava/lang/String;)V
    .registers 6
    .parameter "pariwise"

    #@0
    .prologue
    .line 1066
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setpariwiseField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1067
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->pariwiseField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_21

    #@20
    .line 1073
    :goto_20
    return-void

    #@21
    .line 1070
    :catch_21
    move-exception v0

    #@22
    .line 1071
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Unable to find setpariwiseField "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_20
.end method

.method public setpreSharedKeyField(Ljava/lang/String;)V
    .registers 6
    .parameter "preSharedKey"

    #@0
    .prologue
    .line 343
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearWepWPAKey()V

    #@3
    .line 344
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v3, "setpreSharedKeyField "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 345
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->preSharedKeyField:Ljava/lang/reflect/Field;

    #@1e
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@20
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_23
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_23} :catch_24

    #@23
    .line 351
    :goto_23
    return-void

    #@24
    .line 348
    :catch_24
    move-exception v0

    #@25
    .line 349
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v3, "Unable to find setpreSharedKeyField "

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_23
.end method

.method public setpriorityField(I)V
    .registers 6
    .parameter "priority"

    #@0
    .prologue
    .line 512
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setpriorityField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 513
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->priorityField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 519
    :goto_24
    return-void

    #@25
    .line 516
    :catch_25
    move-exception v0

    #@26
    .line 517
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setpriorityField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setsecModeField(I)V
    .registers 6
    .parameter "secMode"

    #@0
    .prologue
    .line 1019
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setsecModeField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1020
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->secModeField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 1026
    :goto_24
    return-void

    #@25
    .line 1023
    :catch_25
    move-exception v0

    #@26
    .line 1024
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setsecModeField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method public setwepKeysField1(Ljava/lang/String;)V
    .registers 6
    .parameter "webkey"

    #@0
    .prologue
    .line 424
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearWepWPAKey()V

    #@3
    .line 425
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    const-string/jumbo v2, "setwepKeysField1 "

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 426
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys1Field:Ljava/lang/reflect/Field;

    #@d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_12} :catch_13

    #@12
    .line 433
    :goto_12
    return-void

    #@13
    .line 429
    :catch_13
    move-exception v0

    #@14
    .line 430
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Unable to find setwepKeysField1 "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_12
.end method

.method public setwepKeysField2(Ljava/lang/String;)V
    .registers 6
    .parameter "webkey"

    #@0
    .prologue
    .line 437
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearWepWPAKey()V

    #@3
    .line 438
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    const-string/jumbo v2, "setwepKeysField2 "

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 439
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys2Field:Ljava/lang/reflect/Field;

    #@d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_12} :catch_13

    #@12
    .line 446
    :goto_12
    return-void

    #@13
    .line 442
    :catch_13
    move-exception v0

    #@14
    .line 443
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Unable to find setwepKeysField2 "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_12
.end method

.method public setwepKeysField3(Ljava/lang/String;)V
    .registers 6
    .parameter "webkey"

    #@0
    .prologue
    .line 450
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearWepWPAKey()V

    #@3
    .line 451
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    const-string/jumbo v2, "setwepKeysField3 "

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 452
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys3Field:Ljava/lang/reflect/Field;

    #@d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_12} :catch_13

    #@12
    .line 459
    :goto_12
    return-void

    #@13
    .line 455
    :catch_13
    move-exception v0

    #@14
    .line 456
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Unable to find setwepKeysField3 "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_12
.end method

.method public setwepKeysField4(Ljava/lang/String;)V
    .registers 6
    .parameter "webkey"

    #@0
    .prologue
    .line 463
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiVZWConfigurationProxy;->clearWepWPAKey()V

    #@3
    .line 464
    const-string v1, "WifiVZWConfigurationProxy"

    #@5
    const-string/jumbo v2, "setwepKeysField4 "

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 465
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepKeys4Field:Ljava/lang/reflect/Field;

    #@d
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@f
    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_12} :catch_13

    #@12
    .line 472
    :goto_12
    return-void

    #@13
    .line 468
    :catch_13
    move-exception v0

    #@14
    .line 469
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Unable to find setwepKeysField4 "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_12
.end method

.method public setwepTxKeyIndexField(I)V
    .registers 6
    .parameter "webtxkeyindex"

    #@0
    .prologue
    .line 489
    :try_start_0
    const-string v1, "WifiVZWConfigurationProxy"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "setwepTxKeyIndexField "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 490
    sget-object v1, Landroid/net/wifi/WifiVZWConfigurationProxy;->wepTxKeyIndexField:Ljava/lang/reflect/Field;

    #@1b
    sget-object v2, Landroid/net/wifi/WifiVZWConfigurationProxy;->o:Ljava/lang/Object;

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_24} :catch_25

    #@24
    .line 496
    :goto_24
    return-void

    #@25
    .line 493
    :catch_25
    move-exception v0

    #@26
    .line 494
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "WifiVZWConfigurationProxy"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Unable to find setwepTxKeyIndexField "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method
