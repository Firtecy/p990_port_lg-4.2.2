.class public Landroid/net/wifi/WifiMonitor;
.super Ljava/lang/Object;
.source "WifiMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiMonitor$MonitorThread;
    }
.end annotation


# static fields
.field public static final AP_STA_CONNECTED_EVENT:I = 0x2402a

.field private static final AP_STA_CONNECTED_STR:Ljava/lang/String; = "AP-STA-CONNECTED"

.field public static final AP_STA_DISCONNECTED_EVENT:I = 0x24029

.field private static final AP_STA_DISCONNECTED_STR:Ljava/lang/String; = "AP-STA-DISCONNECTED"

.field public static final AUTHENTICATION_FAILURE_EVENT:I = 0x24007

.field private static final BASE:I = 0x24000

.field private static final CONFIG_AUTH_FAILURE:I = 0x12

.field private static final CONFIG_MULTIPLE_PBC_DETECTED:I = 0xc

.field private static final CONNECTED:I = 0x1

.field private static final CONNECTED_STR:Ljava/lang/String; = "CONNECTED"

.field private static final DISCONNECTED:I = 0x2

.field private static final DISCONNECTED_STR:Ljava/lang/String; = "DISCONNECTED"

.field public static final DRIVER_HUNG_EVENT:I = 0x2400c

.field private static final DRIVER_STATE:I = 0x7

.field private static final DRIVER_STATE_STR:Ljava/lang/String; = "DRIVER-STATE"

.field private static final EAP_AUTH_FAILURE_STR:Ljava/lang/String; = "EAP authentication failed"

.field private static final EAP_FAILURE:I = 0x8

.field private static final EAP_FAILURE_STR:Ljava/lang/String; = "EAP-FAILURE"

#the value of this static final field might be set in the static constructor
.field private static final EVENT_PREFIX_LEN_STR:I = 0x0

.field private static final EVENT_PREFIX_STR:Ljava/lang/String; = "CTRL-EVENT-"

.field public static final GO_STA_CONNECTED_EVENT:I = 0x2402b

.field private static final HOST_AP_EVENT_PREFIX_STR:Ljava/lang/String; = "AP"

.field public static final HS20_ANQP_3GPP_CONNECT_EVENT:I = 0x24033

.field public static final HS20_ANQP_FETCH_START_EVENT:I = 0x2402f

.field public static final HS20_ANQP_FETCH_SUCCESS_EVENT:I = 0x24030

.field public static final HS20_ANQP_RC_CONNECT_EVENT:I = 0x24034

.field public static final HS20_ANQP_RX_DATA_EVENT:I = 0x24031

.field private static final HS20_ANQP_RX_STR:Ljava/lang/String; = "HS20-ANQP-RX"

.field public static final HS20_ANQP_TLS_CONNECT_EVENT:I = 0x24035

.field public static final HS20_AP_EVENT:I = 0x2402c

.field public static final HS20_GAS_RESP_INFO_EVENT:I = 0x2402e

.field public static final HS20_NO_MATCH_EVENT:I = 0x2402d

.field private static final HS20_PREFIX_STR:Ljava/lang/String; = "HS20-"

.field public static final HS20_RX_DATA_EVENT:I = 0x24032

.field private static final INTERWORKING_3GPP_CONNECTING_STR:Ljava/lang/String; = "INTERWORKING-3GPP-CONNECTING"

.field private static final INTERWORKING_ANQP_FETCH_START_STR:Ljava/lang/String; = "INTERWORKING-ANQP-FETCH-STARTED"

.field private static final INTERWORKING_ANQP_FETCH_SUCCESS_STR:Ljava/lang/String; = "INTERWORKING-ANQP-FETCH-COMPLETED"

.field private static final INTERWORKING_ANQP_RX_STR:Ljava/lang/String; = "INTERWORKING-ANQP-RX-DATA"

.field private static final INTERWORKING_AP_STR:Ljava/lang/String; = "INTERWORKING-AP"

.field private static final INTERWORKING_GAS_RESP_INFO_STR:Ljava/lang/String; = "INTERWORKING-GAS-RESP-INFO"

.field private static final INTERWORKING_NO_MATCH_STR:Ljava/lang/String; = "INTERWORKING-NO-MATCH"

.field private static final INTERWORKING_PREFIX_STR:Ljava/lang/String; = "INTERWORKING-"

.field private static final INTERWORKING_RC_CONNECTING_STR:Ljava/lang/String; = "INTERWORKING-RC-CONNECTING"

.field private static final INTERWORKING_TLS_CONNECTING_STR:Ljava/lang/String; = "INTERWORKING-TLS-TTLS-CONNECTING"

.field private static final LINK_SPEED:I = 0x5

.field private static final LINK_SPEED_STR:Ljava/lang/String; = "LINK-SPEED"

.field private static final MAX_RECV_ERRORS:I = 0xa

.field private static final MONITOR_SOCKET_CLOSED_STR:Ljava/lang/String; = "connection closed"

.field public static final NETWORK_CONNECTION_EVENT:I = 0x24003

.field public static final NETWORK_DISCONNECTION_EVENT:I = 0x24004

.field public static final P2P_DEVICE_FOUND_EVENT:I = 0x24015

.field private static final P2P_DEVICE_FOUND_STR:Ljava/lang/String; = "P2P-DEVICE-FOUND"

.field public static final P2P_DEVICE_LOST_EVENT:I = 0x24016

.field private static final P2P_DEVICE_LOST_STR:Ljava/lang/String; = "P2P-DEVICE-LOST"

.field private static final P2P_EVENT_PREFIX_STR:Ljava/lang/String; = "P2P"

.field public static final P2P_FIND_STOPPED_EVENT:I = 0x24025

.field private static final P2P_FIND_STOPPED_STR:Ljava/lang/String; = "P2P-FIND-STOPPED"

.field public static final P2P_GO_NEGOTIATION_FAILURE_EVENT:I = 0x2401a

.field public static final P2P_GO_NEGOTIATION_REQUEST_EVENT:I = 0x24017

.field public static final P2P_GO_NEGOTIATION_SUCCESS_EVENT:I = 0x24019

.field private static final P2P_GO_NEG_FAILURE_STR:Ljava/lang/String; = "P2P-GO-NEG-FAILURE"

.field private static final P2P_GO_NEG_REQUEST_STR:Ljava/lang/String; = "P2P-GO-NEG-REQUEST"

.field private static final P2P_GO_NEG_SUCCESS_STR:Ljava/lang/String; = "P2P-GO-NEG-SUCCESS"

.field public static final P2P_GROUP_FORMATION_FAILURE_EVENT:I = 0x2401c

.field private static final P2P_GROUP_FORMATION_FAILURE_STR:Ljava/lang/String; = "P2P-GROUP-FORMATION-FAILURE"

.field public static final P2P_GROUP_FORMATION_SUCCESS_EVENT:I = 0x2401b

.field private static final P2P_GROUP_FORMATION_SUCCESS_STR:Ljava/lang/String; = "P2P-GROUP-FORMATION-SUCCESS"

.field public static final P2P_GROUP_REMOVED_EVENT:I = 0x2401e

.field private static final P2P_GROUP_REMOVED_STR:Ljava/lang/String; = "P2P-GROUP-REMOVED"

.field public static final P2P_GROUP_STARTED_EVENT:I = 0x2401d

.field private static final P2P_GROUP_STARTED_STR:Ljava/lang/String; = "P2P-GROUP-STARTED"

.field public static final P2P_INVITATION_RECEIVED_EVENT:I = 0x2401f

.field private static final P2P_INVITATION_RECEIVED_STR:Ljava/lang/String; = "P2P-INVITATION-RECEIVED"

.field public static final P2P_INVITATION_RESULT_EVENT:I = 0x24020

.field private static final P2P_INVITATION_RESULT_STR:Ljava/lang/String; = "P2P-INVITATION-RESULT"

.field public static final P2P_PROV_DISC_ENTER_PIN_EVENT:I = 0x24023

.field private static final P2P_PROV_DISC_ENTER_PIN_STR:Ljava/lang/String; = "P2P-PROV-DISC-ENTER-PIN"

.field public static final P2P_PROV_DISC_FAILURE_EVENT:I = 0x24027

.field private static final P2P_PROV_DISC_FAILURE_STR:Ljava/lang/String; = "P2P-PROV-DISC-FAILURE"

.field public static final P2P_PROV_DISC_PBC_REQ_EVENT:I = 0x24021

.field private static final P2P_PROV_DISC_PBC_REQ_STR:Ljava/lang/String; = "P2P-PROV-DISC-PBC-REQ"

.field public static final P2P_PROV_DISC_PBC_RSP_EVENT:I = 0x24022

.field private static final P2P_PROV_DISC_PBC_RSP_STR:Ljava/lang/String; = "P2P-PROV-DISC-PBC-RESP"

.field public static final P2P_PROV_DISC_SHOW_PIN_EVENT:I = 0x24024

.field private static final P2P_PROV_DISC_SHOW_PIN_STR:Ljava/lang/String; = "P2P-PROV-DISC-SHOW-PIN"

.field public static final P2P_SERV_DISC_RESP_EVENT:I = 0x24026

.field private static final P2P_SERV_DISC_RESP_STR:Ljava/lang/String; = "P2P-SERV-DISC-RESP"

.field private static final PASSWORD_MAY_BE_INCORRECT_STR:Ljava/lang/String; = "pre-shared key may be incorrect"

.field private static final REASON_TKIP_ONLY_PROHIBITED:I = 0x1

.field private static final REASON_WEP_PROHIBITED:I = 0x2

.field private static final SCAN_RESULTS:I = 0x4

.field public static final SCAN_RESULTS_EVENT:I = 0x24005

.field private static final SCAN_RESULTS_STR:Ljava/lang/String; = "SCAN-RESULTS"

.field private static final STATE_CHANGE:I = 0x3

.field private static final STATE_CHANGE_STR:Ljava/lang/String; = "STATE-CHANGE"

.field public static final SUPPLICANT_STATE_CHANGE_EVENT:I = 0x24006

.field public static final SUP_CONNECTION_EVENT:I = 0x24001

.field public static final SUP_DISCONNECTION_EVENT:I = 0x24002

.field private static final TAG:Ljava/lang/String; = "WifiMonitor"

.field private static final TERMINATING:I = 0x6

.field private static final TERMINATING_STR:Ljava/lang/String; = "TERMINATING"

.field private static final UNKNOWN:I = 0x9

.field private static final WPA_EVENT_PREFIX_STR:Ljava/lang/String; = "WPA:"

.field private static final WPA_RECV_ERROR_STR:Ljava/lang/String; = "recv error"

.field public static final WPS_FAIL_EVENT:I = 0x24009

.field private static final WPS_FAIL_PATTERN:Ljava/lang/String; = "WPS-FAIL msg=\\d+(?: config_error=(\\d+))?(?: reason=(\\d+))?"

.field private static final WPS_FAIL_STR:Ljava/lang/String; = "WPS-FAIL"

.field public static final WPS_OVERLAP_EVENT:I = 0x2400a

.field private static final WPS_OVERLAP_STR:Ljava/lang/String; = "WPS-OVERLAP-DETECTED"

.field public static final WPS_SUCCESS_EVENT:I = 0x24008

.field private static final WPS_SUCCESS_STR:Ljava/lang/String; = "WPS-SUCCESS"

.field public static final WPS_TIMEOUT_EVENT:I = 0x2400b

.field private static final WPS_TIMEOUT_STR:Ljava/lang/String; = "WPS-TIMEOUT"

.field private static mConnectedEventPattern:Ljava/util/regex/Pattern;


# instance fields
.field private mRecvErrors:I

.field private final mStateMachine:Lcom/android/internal/util/StateMachine;

.field private final mWifiNative:Landroid/net/wifi/WifiNative;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    const-string v0, "CTRL-EVENT-"

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/net/wifi/WifiMonitor;->EVENT_PREFIX_LEN_STR:I

    #@8
    .line 168
    const-string v0, "((?:[0-9a-f]{2}:){5}[0-9a-f]{2}) .* \\[id=([0-9]+) "

    #@a
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/net/wifi/WifiMonitor;->mConnectedEventPattern:Ljava/util/regex/Pattern;

    #@10
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/util/StateMachine;Landroid/net/wifi/WifiNative;)V
    .registers 4
    .parameter "wifiStateMachine"
    .parameter "wifiNative"

    #@0
    .prologue
    .line 419
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 406
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/net/wifi/WifiMonitor;->mRecvErrors:I

    #@6
    .line 417
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/net/wifi/WifiMonitor;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@9
    .line 420
    iput-object p1, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@b
    .line 421
    iput-object p2, p0, Landroid/net/wifi/WifiMonitor;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@d
    .line 427
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 428
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/net/wifi/WifiMonitor;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@17
    .line 430
    :cond_17
    return-void
.end method

.method static synthetic access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/wifi/WifiMonitor;)Landroid/net/wifi/WifiNative;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/wifi/WifiMonitor;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiMonitor;->handleLgeEvent(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$300()I
    .registers 1

    #@0
    .prologue
    .line 53
    sget v0, Landroid/net/wifi/WifiMonitor;->EVENT_PREFIX_LEN_STR:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/net/wifi/WifiMonitor;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget v0, p0, Landroid/net/wifi/WifiMonitor;->mRecvErrors:I

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/net/wifi/WifiMonitor;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    iput p1, p0, Landroid/net/wifi/WifiMonitor;->mRecvErrors:I

    #@2
    return p1
.end method

.method static synthetic access$404(Landroid/net/wifi/WifiMonitor;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget v0, p0, Landroid/net/wifi/WifiMonitor;->mRecvErrors:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/wifi/WifiMonitor;->mRecvErrors:I

    #@6
    return v0
.end method

.method static synthetic access$500(Landroid/net/wifi/WifiMonitor;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiMonitor;->getLgeEvent(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-static {p0}, Landroid/net/wifi/WifiMonitor;->nap(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/net/wifi/WifiMonitor;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiMonitor;->handleNetworkStateChange(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private getLgeEvent(Ljava/lang/String;)I
    .registers 3
    .parameter "eventName"

    #@0
    .prologue
    .line 1023
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    invoke-interface {v0, p1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getLgeEvent(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private handleLgeEvent(ILjava/lang/String;)V
    .registers 4
    .parameter "event"
    .parameter "remainder"

    #@0
    .prologue
    .line 1020
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->handleLgeEvent(ILjava/lang/String;)V

    #@5
    .line 1021
    return-void
.end method

.method private handleNetworkStateChange(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V
    .registers 8
    .parameter "newState"
    .parameter "data"

    #@0
    .prologue
    .line 953
    const/4 v0, 0x0

    #@1
    .line 954
    .local v0, BSSID:Ljava/lang/String;
    const/4 v3, -0x1

    #@2
    .line 955
    .local v3, networkId:I
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@4
    if-ne p1, v4, :cond_12

    #@6
    .line 956
    sget-object v4, Landroid/net/wifi/WifiMonitor;->mConnectedEventPattern:Ljava/util/regex/Pattern;

    #@8
    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@b
    move-result-object v2

    #@c
    .line 957
    .local v2, match:Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_16

    #@12
    .line 968
    .end local v2           #match:Ljava/util/regex/Matcher;
    :cond_12
    :goto_12
    invoke-virtual {p0, p1, v0, v3}, Landroid/net/wifi/WifiMonitor;->notifyNetworkStateChange(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;I)V

    #@15
    .line 969
    return-void

    #@16
    .line 960
    .restart local v2       #match:Ljava/util/regex/Matcher;
    :cond_16
    const/4 v4, 0x1

    #@17
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 962
    const/4 v4, 0x2

    #@1c
    :try_start_1c
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_23
    .catch Ljava/lang/NumberFormatException; {:try_start_1c .. :try_end_23} :catch_25

    #@23
    move-result v3

    #@24
    goto :goto_12

    #@25
    .line 963
    :catch_25
    move-exception v1

    #@26
    .line 964
    .local v1, e:Ljava/lang/NumberFormatException;
    const/4 v3, -0x1

    #@27
    goto :goto_12
.end method

.method private static nap(I)V
    .registers 3
    .parameter "secs"

    #@0
    .prologue
    .line 1013
    mul-int/lit16 v0, p0, 0x3e8

    #@2
    int-to-long v0, v0

    #@3
    :try_start_3
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_6} :catch_7

    #@6
    .line 1016
    :goto_6
    return-void

    #@7
    .line 1014
    :catch_7
    move-exception v0

    #@8
    goto :goto_6
.end method


# virtual methods
.method notifyNetworkStateChange(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;I)V
    .registers 8
    .parameter "newState"
    .parameter "BSSID"
    .parameter "netId"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 982
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@3
    if-ne p1, v1, :cond_14

    #@5
    .line 983
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@7
    const v2, 0x24003

    #@a
    invoke-virtual {v1, v2, p3, v3, p2}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 985
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@10
    invoke-virtual {v1, v0}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@13
    .line 991
    :goto_13
    return-void

    #@14
    .line 987
    .end local v0           #m:Landroid/os/Message;
    :cond_14
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@16
    const v2, 0x24004

    #@19
    invoke-virtual {v1, v2, p3, v3, p2}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@1c
    move-result-object v0

    #@1d
    .line 989
    .restart local v0       #m:Landroid/os/Message;
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@1f
    invoke-virtual {v1, v0}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@22
    goto :goto_13
.end method

.method notifySupplicantStateChange(ILandroid/net/wifi/WifiSsid;Ljava/lang/String;Landroid/net/wifi/SupplicantState;)V
    .registers 9
    .parameter "networkId"
    .parameter "wifiSsid"
    .parameter "BSSID"
    .parameter "newState"

    #@0
    .prologue
    .line 1003
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@2
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor;->mStateMachine:Lcom/android/internal/util/StateMachine;

    #@4
    const v2, 0x24006

    #@7
    new-instance v3, Landroid/net/wifi/StateChangeResult;

    #@9
    invoke-direct {v3, p1, p2, p3, p4}, Landroid/net/wifi/StateChangeResult;-><init>(ILandroid/net/wifi/WifiSsid;Ljava/lang/String;Landroid/net/wifi/SupplicantState;)V

    #@c
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@13
    .line 1005
    return-void
.end method

.method public startMonitoring()V
    .registers 2

    #@0
    .prologue
    .line 433
    new-instance v0, Landroid/net/wifi/WifiMonitor$MonitorThread;

    #@2
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiMonitor$MonitorThread;-><init>(Landroid/net/wifi/WifiMonitor;)V

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiMonitor$MonitorThread;->start()V

    #@8
    .line 434
    return-void
.end method
