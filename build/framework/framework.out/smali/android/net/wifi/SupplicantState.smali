.class public final enum Landroid/net/wifi/SupplicantState;
.super Ljava/lang/Enum;
.source "SupplicantState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/SupplicantState$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/wifi/SupplicantState;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/wifi/SupplicantState;

.field public static final enum ASSOCIATED:Landroid/net/wifi/SupplicantState;

.field public static final enum ASSOCIATING:Landroid/net/wifi/SupplicantState;

.field public static final enum AUTHENTICATING:Landroid/net/wifi/SupplicantState;

.field public static final enum COMPLETED:Landroid/net/wifi/SupplicantState;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/SupplicantState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISCONNECTED:Landroid/net/wifi/SupplicantState;

.field public static final enum DORMANT:Landroid/net/wifi/SupplicantState;

.field public static final enum FOUR_WAY_HANDSHAKE:Landroid/net/wifi/SupplicantState;

.field public static final enum GROUP_HANDSHAKE:Landroid/net/wifi/SupplicantState;

.field public static final enum INACTIVE:Landroid/net/wifi/SupplicantState;

.field public static final enum INTERFACE_DISABLED:Landroid/net/wifi/SupplicantState;

.field public static final enum INVALID:Landroid/net/wifi/SupplicantState;

.field public static final enum SCANNING:Landroid/net/wifi/SupplicantState;

.field public static final enum UNINITIALIZED:Landroid/net/wifi/SupplicantState;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 39
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@7
    const-string v1, "DISCONNECTED"

    #@9
    invoke-direct {v0, v1, v3}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Landroid/net/wifi/SupplicantState;->DISCONNECTED:Landroid/net/wifi/SupplicantState;

    #@e
    .line 48
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@10
    const-string v1, "INTERFACE_DISABLED"

    #@12
    invoke-direct {v0, v1, v4}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Landroid/net/wifi/SupplicantState;->INTERFACE_DISABLED:Landroid/net/wifi/SupplicantState;

    #@17
    .line 58
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@19
    const-string v1, "INACTIVE"

    #@1b
    invoke-direct {v0, v1, v5}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Landroid/net/wifi/SupplicantState;->INACTIVE:Landroid/net/wifi/SupplicantState;

    #@20
    .line 66
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@22
    const-string v1, "SCANNING"

    #@24
    invoke-direct {v0, v1, v6}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Landroid/net/wifi/SupplicantState;->SCANNING:Landroid/net/wifi/SupplicantState;

    #@29
    .line 75
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@2b
    const-string v1, "AUTHENTICATING"

    #@2d
    invoke-direct {v0, v1, v7}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Landroid/net/wifi/SupplicantState;->AUTHENTICATING:Landroid/net/wifi/SupplicantState;

    #@32
    .line 86
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@34
    const-string v1, "ASSOCIATING"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Landroid/net/wifi/SupplicantState;->ASSOCIATING:Landroid/net/wifi/SupplicantState;

    #@3c
    .line 96
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@3e
    const-string v1, "ASSOCIATED"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Landroid/net/wifi/SupplicantState;->ASSOCIATED:Landroid/net/wifi/SupplicantState;

    #@46
    .line 106
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@48
    const-string v1, "FOUR_WAY_HANDSHAKE"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Landroid/net/wifi/SupplicantState;->FOUR_WAY_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@50
    .line 116
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@52
    const-string v1, "GROUP_HANDSHAKE"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Landroid/net/wifi/SupplicantState;->GROUP_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@5b
    .line 136
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@5d
    const-string v1, "COMPLETED"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    #@66
    .line 146
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@68
    const-string v1, "DORMANT"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Landroid/net/wifi/SupplicantState;->DORMANT:Landroid/net/wifi/SupplicantState;

    #@71
    .line 155
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@73
    const-string v1, "UNINITIALIZED"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Landroid/net/wifi/SupplicantState;->UNINITIALIZED:Landroid/net/wifi/SupplicantState;

    #@7c
    .line 160
    new-instance v0, Landroid/net/wifi/SupplicantState;

    #@7e
    const-string v1, "INVALID"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/SupplicantState;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@87
    .line 33
    const/16 v0, 0xd

    #@89
    new-array v0, v0, [Landroid/net/wifi/SupplicantState;

    #@8b
    sget-object v1, Landroid/net/wifi/SupplicantState;->DISCONNECTED:Landroid/net/wifi/SupplicantState;

    #@8d
    aput-object v1, v0, v3

    #@8f
    sget-object v1, Landroid/net/wifi/SupplicantState;->INTERFACE_DISABLED:Landroid/net/wifi/SupplicantState;

    #@91
    aput-object v1, v0, v4

    #@93
    sget-object v1, Landroid/net/wifi/SupplicantState;->INACTIVE:Landroid/net/wifi/SupplicantState;

    #@95
    aput-object v1, v0, v5

    #@97
    sget-object v1, Landroid/net/wifi/SupplicantState;->SCANNING:Landroid/net/wifi/SupplicantState;

    #@99
    aput-object v1, v0, v6

    #@9b
    sget-object v1, Landroid/net/wifi/SupplicantState;->AUTHENTICATING:Landroid/net/wifi/SupplicantState;

    #@9d
    aput-object v1, v0, v7

    #@9f
    const/4 v1, 0x5

    #@a0
    sget-object v2, Landroid/net/wifi/SupplicantState;->ASSOCIATING:Landroid/net/wifi/SupplicantState;

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/4 v1, 0x6

    #@a5
    sget-object v2, Landroid/net/wifi/SupplicantState;->ASSOCIATED:Landroid/net/wifi/SupplicantState;

    #@a7
    aput-object v2, v0, v1

    #@a9
    const/4 v1, 0x7

    #@aa
    sget-object v2, Landroid/net/wifi/SupplicantState;->FOUR_WAY_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/16 v1, 0x8

    #@b0
    sget-object v2, Landroid/net/wifi/SupplicantState;->GROUP_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x9

    #@b6
    sget-object v2, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    #@b8
    aput-object v2, v0, v1

    #@ba
    const/16 v1, 0xa

    #@bc
    sget-object v2, Landroid/net/wifi/SupplicantState;->DORMANT:Landroid/net/wifi/SupplicantState;

    #@be
    aput-object v2, v0, v1

    #@c0
    const/16 v1, 0xb

    #@c2
    sget-object v2, Landroid/net/wifi/SupplicantState;->UNINITIALIZED:Landroid/net/wifi/SupplicantState;

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0xc

    #@c8
    sget-object v2, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@ca
    aput-object v2, v0, v1

    #@cc
    sput-object v0, Landroid/net/wifi/SupplicantState;->$VALUES:[Landroid/net/wifi/SupplicantState;

    #@ce
    .line 252
    new-instance v0, Landroid/net/wifi/SupplicantState$1;

    #@d0
    invoke-direct {v0}, Landroid/net/wifi/SupplicantState$1;-><init>()V

    #@d3
    sput-object v0, Landroid/net/wifi/SupplicantState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d5
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 193
    return-void
.end method

.method static isConnecting(Landroid/net/wifi/SupplicantState;)Z
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 198
    sget-object v0, Landroid/net/wifi/SupplicantState$2;->$SwitchMap$android$net$wifi$SupplicantState:[I

    #@2
    invoke-virtual {p0}, Landroid/net/wifi/SupplicantState;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_18

    #@b
    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Unknown supplicant state"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 205
    :pswitch_13
    const/4 v0, 0x1

    #@14
    .line 213
    :goto_14
    return v0

    #@15
    :pswitch_15
    const/4 v0, 0x0

    #@16
    goto :goto_14

    #@17
    .line 198
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
    .end packed-switch
.end method

.method static isDriverActive(Landroid/net/wifi/SupplicantState;)Z
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 220
    sget-object v0, Landroid/net/wifi/SupplicantState$2;->$SwitchMap$android$net$wifi$SupplicantState:[I

    #@2
    invoke-virtual {p0}, Landroid/net/wifi/SupplicantState;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_18

    #@b
    .line 237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Unknown supplicant state"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 231
    :pswitch_13
    const/4 v0, 0x1

    #@14
    .line 235
    :goto_14
    return v0

    #@15
    :pswitch_15
    const/4 v0, 0x0

    #@16
    goto :goto_14

    #@17
    .line 220
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_15
    .end packed-switch
.end method

.method public static isHandshakeState(Landroid/net/wifi/SupplicantState;)Z
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 176
    sget-object v0, Landroid/net/wifi/SupplicantState$2;->$SwitchMap$android$net$wifi$SupplicantState:[I

    #@2
    invoke-virtual {p0}, Landroid/net/wifi/SupplicantState;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_18

    #@b
    .line 193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Unknown supplicant state"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 182
    :pswitch_13
    const/4 v0, 0x1

    #@14
    .line 191
    :goto_14
    return v0

    #@15
    :pswitch_15
    const/4 v0, 0x0

    #@16
    goto :goto_14

    #@17
    .line 176
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
    .end packed-switch
.end method

.method public static isValidState(Landroid/net/wifi/SupplicantState;)Z
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 170
    sget-object v0, Landroid/net/wifi/SupplicantState;->UNINITIALIZED:Landroid/net/wifi/SupplicantState;

    #@2
    if-eq p0, v0, :cond_a

    #@4
    sget-object v0, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@6
    if-eq p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/wifi/SupplicantState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 33
    const-class v0, Landroid/net/wifi/SupplicantState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/wifi/SupplicantState;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/net/wifi/SupplicantState;
    .registers 1

    #@0
    .prologue
    .line 33
    sget-object v0, Landroid/net/wifi/SupplicantState;->$VALUES:[Landroid/net/wifi/SupplicantState;

    #@2
    invoke-virtual {v0}, [Landroid/net/wifi/SupplicantState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/net/wifi/SupplicantState;

    #@8
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 243
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 248
    invoke-virtual {p0}, Landroid/net/wifi/SupplicantState;->name()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 249
    return-void
.end method
