.class Landroid/net/wifi/WifiStateMachine$ConnectModeState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectModeState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3524
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 3528
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 3529
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 3534
    iget v9, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v9, :sswitch_data_34c

    #@7
    .line 3723
    :goto_7
    return v7

    #@8
    .line 3536
    :sswitch_8
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@a
    if-eqz v7, :cond_11

    #@c
    .line 3537
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e
    invoke-static {v7, v8}, Landroid/net/wifi/WifiStateMachine;->access$12200(Landroid/net/wifi/WifiStateMachine;Z)V

    #@11
    .line 3539
    :cond_11
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$5100(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/SupplicantStateTracker;

    #@16
    move-result-object v7

    #@17
    const v9, 0x24007

    #@1a
    invoke-virtual {v7, v9}, Landroid/net/wifi/SupplicantStateTracker;->sendMessage(I)V

    #@1d
    :cond_1d
    :goto_1d
    move v7, v8

    #@1e
    .line 3723
    goto :goto_7

    #@1f
    .line 3542
    :sswitch_1f
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@21
    invoke-static {v7, p1}, Landroid/net/wifi/WifiStateMachine;->access$8300(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)Landroid/net/wifi/SupplicantState;

    #@24
    move-result-object v4

    #@25
    .line 3545
    .local v4, state:Landroid/net/wifi/SupplicantState;
    invoke-static {v4}, Landroid/net/wifi/SupplicantState;->isDriverActive(Landroid/net/wifi/SupplicantState;)Z

    #@28
    move-result v7

    #@29
    if-nez v7, :cond_59

    #@2b
    .line 3546
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2d
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$6500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/NetworkInfo;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@34
    move-result-object v7

    #@35
    sget-object v9, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@37
    if-eq v7, v9, :cond_3e

    #@39
    .line 3547
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3b
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$7100(Landroid/net/wifi/WifiStateMachine;)V

    #@3e
    .line 3549
    :cond_3e
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@40
    const-string v9, "Detected an interface down, restart driver"

    #@42
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@45
    .line 3550
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@47
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@49
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$8500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@4c
    move-result-object v9

    #@4d
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$12300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@50
    .line 3551
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@52
    const v9, 0x2000d

    #@55
    invoke-virtual {v7, v9}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@58
    goto :goto_1d

    #@59
    .line 3560
    :cond_59
    sget-object v7, Landroid/net/wifi/SupplicantState;->DISCONNECTED:Landroid/net/wifi/SupplicantState;

    #@5b
    if-ne v4, v7, :cond_7b

    #@5d
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5f
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$6500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/NetworkInfo;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@66
    move-result-object v7

    #@67
    sget-object v9, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@69
    if-eq v7, v9, :cond_7b

    #@6b
    .line 3563
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@6d
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$7100(Landroid/net/wifi/WifiStateMachine;)V

    #@70
    .line 3564
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@72
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@74
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$9700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@77
    move-result-object v9

    #@78
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$12400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@7b
    .line 3568
    :cond_7b
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@7d
    if-eqz v7, :cond_1d

    #@7f
    .line 3569
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@81
    check-cast v5, Landroid/net/wifi/StateChangeResult;

    #@83
    .line 3570
    .local v5, stateChangeResult:Landroid/net/wifi/StateChangeResult;
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@85
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$600(Landroid/net/wifi/WifiStateMachine;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@88
    move-result-object v7

    #@89
    iget v9, v5, Landroid/net/wifi/StateChangeResult;->networkId:I

    #@8b
    invoke-interface {v7, v9, v4}, Lcom/lge/wifi_iface/WifiServiceExtIface;->updateAutoConnectProfile(ILandroid/net/wifi/SupplicantState;)V

    #@8e
    goto :goto_1d

    #@8f
    .line 3576
    .end local v4           #state:Landroid/net/wifi/SupplicantState;
    .end local v5           #stateChangeResult:Landroid/net/wifi/StateChangeResult;
    :sswitch_8f
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@91
    if-ne v9, v8, :cond_a3

    #@93
    .line 3577
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@95
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@98
    move-result-object v7

    #@99
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@9c
    .line 3578
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9e
    invoke-static {v7, v8}, Landroid/net/wifi/WifiStateMachine;->access$1702(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@a1
    goto/16 :goto_1d

    #@a3
    .line 3580
    :cond_a3
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a5
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@a8
    move-result-object v9

    #@a9
    invoke-virtual {v9}, Landroid/net/wifi/WifiNative;->reconnect()Z

    #@ac
    .line 3581
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ae
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$1702(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@b1
    goto/16 :goto_1d

    #@b3
    .line 3586
    :sswitch_b3
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b5
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@b8
    move-result-object v7

    #@b9
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@bc
    goto/16 :goto_1d

    #@be
    .line 3589
    :sswitch_be
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c0
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@c3
    move-result-object v7

    #@c4
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->reconnect()Z

    #@c7
    goto/16 :goto_1d

    #@c9
    .line 3592
    :sswitch_c9
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@cb
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@ce
    move-result-object v7

    #@cf
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->reassociate()Z

    #@d2
    goto/16 :goto_1d

    #@d4
    .line 3600
    :sswitch_d4
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@d6
    .line 3601
    .local v1, netId:I
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d8
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@da
    .line 3604
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_ea

    #@dc
    .line 3605
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@de
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@e1
    move-result-object v9

    #@e2
    invoke-virtual {v9, v0}, Landroid/net/wifi/WifiConfigStore;->saveNetwork(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@e5
    move-result-object v2

    #@e6
    .line 3606
    .local v2, result:Landroid/net/wifi/NetworkUpdateResult;
    invoke-virtual {v2}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@e9
    move-result v1

    #@ea
    .line 3609
    .end local v2           #result:Landroid/net/wifi/NetworkUpdateResult;
    :cond_ea
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ec
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@ef
    move-result-object v9

    #@f0
    invoke-virtual {v9, v1}, Landroid/net/wifi/WifiConfigStore;->selectNetwork(I)Z

    #@f3
    move-result v9

    #@f4
    if-eqz v9, :cond_12c

    #@f6
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f8
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@fb
    move-result-object v9

    #@fc
    invoke-virtual {v9}, Landroid/net/wifi/WifiNative;->reconnect()Z

    #@ff
    move-result v9

    #@100
    if-eqz v9, :cond_12c

    #@102
    .line 3612
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@104
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5100(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/SupplicantStateTracker;

    #@107
    move-result-object v9

    #@108
    const v10, 0x25001

    #@10b
    invoke-virtual {v9, v10}, Landroid/net/wifi/SupplicantStateTracker;->sendMessage(I)V

    #@10e
    .line 3613
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@110
    const v10, 0x25003

    #@113
    invoke-static {v9, p1, v10}, Landroid/net/wifi/WifiStateMachine;->access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V

    #@116
    .line 3614
    sget-boolean v9, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@118
    if-eqz v9, :cond_11f

    #@11a
    .line 3615
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11c
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$12200(Landroid/net/wifi/WifiStateMachine;Z)V

    #@11f
    .line 3618
    :cond_11f
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@121
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@123
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$12500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@126
    move-result-object v9

    #@127
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$12600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@12a
    goto/16 :goto_1d

    #@12c
    .line 3620
    :cond_12c
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@12e
    new-instance v10, Ljava/lang/StringBuilder;

    #@130
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    const-string v11, "Failed to connect config: "

    #@135
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v10

    #@139
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v10

    #@13d
    const-string v11, " netId: "

    #@13f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v10

    #@143
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@146
    move-result-object v10

    #@147
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v10

    #@14b
    invoke-static {v9, v10}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@14e
    .line 3621
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@150
    const v10, 0x25002

    #@153
    invoke-static {v9, p1, v10, v7}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@156
    goto/16 :goto_1d

    #@158
    .line 3627
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v1           #netId:I
    :sswitch_158
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15a
    check-cast v6, Landroid/net/wifi/WpsInfo;

    #@15c
    .line 3629
    .local v6, wpsInfo:Landroid/net/wifi/WpsInfo;
    iget v9, v6, Landroid/net/wifi/WpsInfo;->setup:I

    #@15e
    packed-switch v9, :pswitch_data_3a2

    #@161
    .line 3640
    new-instance v2, Landroid/net/wifi/WpsResult;

    #@163
    sget-object v9, Landroid/net/wifi/WpsResult$Status;->FAILURE:Landroid/net/wifi/WpsResult$Status;

    #@165
    invoke-direct {v2, v9}, Landroid/net/wifi/WpsResult;-><init>(Landroid/net/wifi/WpsResult$Status;)V

    #@168
    .line 3641
    .local v2, result:Landroid/net/wifi/WpsResult;
    const-string v9, "WifiStateMachine"

    #@16a
    const-string v10, "Invalid setup for WPS"

    #@16c
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16f
    .line 3644
    :goto_16f
    iget-object v9, v2, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@171
    sget-object v10, Landroid/net/wifi/WpsResult$Status;->SUCCESS:Landroid/net/wifi/WpsResult$Status;

    #@173
    if-ne v9, v10, :cond_1ab

    #@175
    .line 3645
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@177
    const v9, 0x2500b

    #@17a
    invoke-static {v7, p1, v9, v2}, Landroid/net/wifi/WifiStateMachine;->access$1000(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@17d
    .line 3646
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@17f
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@181
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$12700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@184
    move-result-object v9

    #@185
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$12800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@188
    goto/16 :goto_1d

    #@18a
    .line 3631
    .end local v2           #result:Landroid/net/wifi/WpsResult;
    :pswitch_18a
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@18c
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@18f
    move-result-object v9

    #@190
    invoke-virtual {v9, v6}, Landroid/net/wifi/WifiConfigStore;->startWpsPbc(Landroid/net/wifi/WpsInfo;)Landroid/net/wifi/WpsResult;

    #@193
    move-result-object v2

    #@194
    .line 3632
    .restart local v2       #result:Landroid/net/wifi/WpsResult;
    goto :goto_16f

    #@195
    .line 3634
    .end local v2           #result:Landroid/net/wifi/WpsResult;
    :pswitch_195
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@197
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@19a
    move-result-object v9

    #@19b
    invoke-virtual {v9, v6}, Landroid/net/wifi/WifiConfigStore;->startWpsWithPinFromAccessPoint(Landroid/net/wifi/WpsInfo;)Landroid/net/wifi/WpsResult;

    #@19e
    move-result-object v2

    #@19f
    .line 3635
    .restart local v2       #result:Landroid/net/wifi/WpsResult;
    goto :goto_16f

    #@1a0
    .line 3637
    .end local v2           #result:Landroid/net/wifi/WpsResult;
    :pswitch_1a0
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1a2
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@1a5
    move-result-object v9

    #@1a6
    invoke-virtual {v9, v6}, Landroid/net/wifi/WifiConfigStore;->startWpsWithPinFromDevice(Landroid/net/wifi/WpsInfo;)Landroid/net/wifi/WpsResult;

    #@1a9
    move-result-object v2

    #@1aa
    .line 3638
    .restart local v2       #result:Landroid/net/wifi/WpsResult;
    goto :goto_16f

    #@1ab
    .line 3648
    :cond_1ab
    const-string v9, "WifiStateMachine"

    #@1ad
    new-instance v10, Ljava/lang/StringBuilder;

    #@1af
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1b2
    const-string v11, "Failed to start WPS with config "

    #@1b4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v10

    #@1b8
    invoke-virtual {v6}, Landroid/net/wifi/WpsInfo;->toString()Ljava/lang/String;

    #@1bb
    move-result-object v11

    #@1bc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v10

    #@1c0
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c3
    move-result-object v10

    #@1c4
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c7
    .line 3649
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c9
    const v10, 0x2500c

    #@1cc
    invoke-static {v9, p1, v10, v7}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@1cf
    goto/16 :goto_1d

    #@1d1
    .line 3655
    .end local v2           #result:Landroid/net/wifi/WpsResult;
    .end local v6           #wpsInfo:Landroid/net/wifi/WpsInfo;
    :sswitch_1d1
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1d3
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@1d6
    move-result-object v9

    #@1d7
    invoke-virtual {v9, v8}, Landroid/net/wifi/WifiNative;->setScanResultHandling(I)Z

    #@1da
    goto/16 :goto_7

    #@1dc
    .line 3661
    :sswitch_1dc
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1de
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@1e0
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$5302(Landroid/net/wifi/WifiStateMachine;I)I

    #@1e3
    .line 3662
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1e5
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e7
    check-cast v7, Ljava/lang/String;

    #@1e9
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$5202(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;

    #@1ec
    .line 3664
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1ee
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$5600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiInfo;

    #@1f1
    move-result-object v7

    #@1f2
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1f4
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5200(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@1f7
    move-result-object v9

    #@1f8
    invoke-virtual {v7, v9}, Landroid/net/wifi/WifiInfo;->setBSSID(Ljava/lang/String;)V

    #@1fb
    .line 3665
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1fd
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$5600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiInfo;

    #@200
    move-result-object v7

    #@201
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@203
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5300(Landroid/net/wifi/WifiStateMachine;)I

    #@206
    move-result v9

    #@207
    invoke-virtual {v7, v9}, Landroid/net/wifi/WifiInfo;->setNetworkId(I)V

    #@20a
    .line 3666
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@20c
    if-eqz v7, :cond_213

    #@20e
    .line 3667
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@210
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$12900(Landroid/net/wifi/WifiStateMachine;)V

    #@213
    .line 3670
    :cond_213
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@215
    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    #@217
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$9300(Landroid/net/wifi/WifiStateMachine;Landroid/net/NetworkInfo$DetailedState;)V

    #@21a
    .line 3671
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@21c
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@21e
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$5200(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@221
    move-result-object v9

    #@222
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$13000(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@225
    .line 3672
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@227
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@229
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$13100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@22c
    move-result-object v9

    #@22d
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$13200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@230
    goto/16 :goto_1d

    #@232
    .line 3676
    :sswitch_232
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@234
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$7100(Landroid/net/wifi/WifiStateMachine;)V

    #@237
    .line 3677
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@239
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@23b
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$9700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@23e
    move-result-object v9

    #@23f
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$13300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@242
    goto/16 :goto_1d

    #@244
    .line 3681
    :sswitch_244
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@246
    .line 3682
    .local v3, roamingInd:I
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@248
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24a
    check-cast v7, Ljava/lang/String;

    #@24c
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$13402(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;

    #@24f
    .line 3683
    const-string v7, "WifiStateMachine"

    #@251
    new-instance v9, Ljava/lang/StringBuilder;

    #@253
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@256
    const-string v10, "[PASSPOINT][ConnectModeState] : HS20_AP_EVENT "

    #@258
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v9

    #@25c
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@25e
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@261
    move-result-object v10

    #@262
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@265
    move-result-object v9

    #@266
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@269
    move-result-object v9

    #@26a
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26d
    .line 3684
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@26f
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@271
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@274
    move-result-object v9

    #@275
    invoke-static {v7, v9, v3}, Landroid/net/wifi/WifiStateMachine;->access$13500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;I)V

    #@278
    goto/16 :goto_1d

    #@27a
    .line 3687
    .end local v3           #roamingInd:I
    :sswitch_27a
    const-string v7, "WifiStateMachine"

    #@27c
    const-string v9, " [PASSPOINT][ConnectModeState] : HS20_NO_MATCH_EVENT"

    #@27e
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@281
    goto/16 :goto_1d

    #@283
    .line 3690
    :sswitch_283
    const-string v7, "WifiStateMachine"

    #@285
    const-string v9, " [PASSPOINT][ConnectModeState] : HS20_GAS_RESP_INFO_EVENT"

    #@287
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28a
    goto/16 :goto_1d

    #@28c
    .line 3693
    :sswitch_28c
    const-string v7, "WifiStateMachine"

    #@28e
    const-string v9, " [PASSPOINT][ConnectModeState] : HS20_ANQP_FETCH_START_EVENT"

    #@290
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@293
    goto/16 :goto_1d

    #@295
    .line 3696
    :sswitch_295
    const-string v7, "WifiStateMachine"

    #@297
    const-string v9, " [PASSPOINT][ConnectModeState] : HS20_ANQP_FETCH_SUCCESS_EVENT"

    #@299
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29c
    goto/16 :goto_1d

    #@29e
    .line 3699
    :sswitch_29e
    const-string v7, "WifiStateMachine"

    #@2a0
    const-string v9, " [PASSPOINT][ConnectModeState] : HS20_ANQP_RX_DATA_EVENT"

    #@2a2
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a5
    goto/16 :goto_1d

    #@2a7
    .line 3702
    :sswitch_2a7
    const-string v7, "WifiStateMachine"

    #@2a9
    const-string v9, " [PASSPOINT][ConnectModeState] : HS20_RX_DATA_EVENT"

    #@2ab
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2ae
    goto/16 :goto_1d

    #@2b0
    .line 3705
    :sswitch_2b0
    const-string v7, "WifiStateMachine"

    #@2b2
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2b7
    const-string v10, " [PASSPOINT][ConnectModeState] : HS20_ANQP_3GPP_CONNECT_EVENT "

    #@2b9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bc
    move-result-object v9

    #@2bd
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2bf
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@2c2
    move-result-object v10

    #@2c3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c6
    move-result-object v9

    #@2c7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ca
    move-result-object v9

    #@2cb
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2ce
    .line 3706
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2d0
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d2
    check-cast v7, Ljava/lang/String;

    #@2d4
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$13402(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;

    #@2d7
    .line 3707
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2d9
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2db
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@2de
    move-result-object v9

    #@2df
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$13600(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@2e2
    goto/16 :goto_1d

    #@2e4
    .line 3710
    :sswitch_2e4
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2e6
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e8
    check-cast v7, Ljava/lang/String;

    #@2ea
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$13402(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;

    #@2ed
    .line 3711
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2ef
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2f1
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@2f4
    move-result-object v9

    #@2f5
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$13600(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@2f8
    .line 3712
    const-string v7, "WifiStateMachine"

    #@2fa
    new-instance v9, Ljava/lang/StringBuilder;

    #@2fc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2ff
    const-string v10, " [PASSPOINT][ConnectModeState] : HS20_ANQP_RC_CONNECT_EVENT "

    #@301
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@304
    move-result-object v9

    #@305
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@307
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@30a
    move-result-object v10

    #@30b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30e
    move-result-object v9

    #@30f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@312
    move-result-object v9

    #@313
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@316
    goto/16 :goto_1d

    #@318
    .line 3715
    :sswitch_318
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@31a
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@31c
    check-cast v7, Ljava/lang/String;

    #@31e
    invoke-static {v9, v7}, Landroid/net/wifi/WifiStateMachine;->access$13402(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;

    #@321
    .line 3716
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@323
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@325
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@328
    move-result-object v9

    #@329
    invoke-static {v7, v9}, Landroid/net/wifi/WifiStateMachine;->access$13600(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@32c
    .line 3717
    const-string v7, "WifiStateMachine"

    #@32e
    new-instance v9, Ljava/lang/StringBuilder;

    #@330
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@333
    const-string v10, " [PASSPOINT][ConnectModeState] : HS20_ANQP_TLS_CONNECT_EVENT "

    #@335
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@338
    move-result-object v9

    #@339
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@33b
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@33e
    move-result-object v10

    #@33f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@342
    move-result-object v9

    #@343
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@346
    move-result-object v9

    #@347
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34a
    goto/16 :goto_1d

    #@34c
    .line 3534
    :sswitch_data_34c
    .sparse-switch
        0x2004a -> :sswitch_b3
        0x2004b -> :sswitch_be
        0x2004c -> :sswitch_c9
        0x2300c -> :sswitch_8f
        0x24003 -> :sswitch_1dc
        0x24004 -> :sswitch_232
        0x24005 -> :sswitch_1d1
        0x24006 -> :sswitch_1f
        0x24007 -> :sswitch_8
        0x2402c -> :sswitch_244
        0x2402d -> :sswitch_27a
        0x2402e -> :sswitch_283
        0x2402f -> :sswitch_28c
        0x24030 -> :sswitch_295
        0x24031 -> :sswitch_29e
        0x24032 -> :sswitch_2a7
        0x24033 -> :sswitch_2b0
        0x24034 -> :sswitch_2e4
        0x24035 -> :sswitch_318
        0x25001 -> :sswitch_d4
        0x2500a -> :sswitch_158
    .end sparse-switch

    #@3a2
    .line 3629
    :pswitch_data_3a2
    .packed-switch 0x0
        :pswitch_18a
        :pswitch_1a0
        :pswitch_195
    .end packed-switch
.end method
