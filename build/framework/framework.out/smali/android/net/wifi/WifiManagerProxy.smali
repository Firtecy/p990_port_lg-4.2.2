.class public Landroid/net/wifi/WifiManagerProxy;
.super Ljava/lang/Object;
.source "WifiManagerProxy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WifiManagerProxy"

.field private static checkAndStartWifiExtMethod:Ljava/lang/reflect/Method;

.field private static getWifiNeedOnMethod:Ljava/lang/reflect/Method;

.field private static isPowerSaveModeEnabledMethod:Ljava/lang/reflect/Method;

.field private static isVZWMobileHotspotEnabledMethod:Ljava/lang/reflect/Method;

.field private static setVZWMobileHotspotMethod:Ljava/lang/reflect/Method;

.field private static setWifiNeedOnMethod:Ljava/lang/reflect/Method;

.field private static setWifiVZWApConfigurationMethod:Ljava/lang/reflect/Method;

.field private static setWifiVZWApEnabledMethod:Ljava/lang/reflect/Method;


# instance fields
.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .registers 15

    #@0
    .prologue
    const/4 v14, 0x4

    #@1
    const/4 v13, 0x3

    #@2
    const/4 v12, 0x2

    #@3
    const/4 v11, 0x1

    #@4
    const/4 v10, 0x0

    #@5
    .line 33
    new-array v0, v10, [Ljava/lang/Class;

    #@7
    .line 34
    .local v0, checkAndStartWifiExtParamType:[Ljava/lang/Class;
    const/16 v8, 0x13

    #@9
    new-array v7, v8, [Ljava/lang/Class;

    #@b
    const-class v8, Ljava/lang/String;

    #@d
    aput-object v8, v7, v10

    #@f
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@11
    aput-object v8, v7, v11

    #@13
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@15
    aput-object v8, v7, v12

    #@17
    const-class v8, Ljava/lang/String;

    #@19
    aput-object v8, v7, v13

    #@1b
    const-class v8, Ljava/lang/String;

    #@1d
    aput-object v8, v7, v14

    #@1f
    const/4 v8, 0x5

    #@20
    const-class v9, Ljava/lang/String;

    #@22
    aput-object v9, v7, v8

    #@24
    const/4 v8, 0x6

    #@25
    const-class v9, Ljava/lang/String;

    #@27
    aput-object v9, v7, v8

    #@29
    const/4 v8, 0x7

    #@2a
    const-class v9, Ljava/lang/String;

    #@2c
    aput-object v9, v7, v8

    #@2e
    const/16 v8, 0x8

    #@30
    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@32
    aput-object v9, v7, v8

    #@34
    const/16 v8, 0x9

    #@36
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@38
    aput-object v9, v7, v8

    #@3a
    const/16 v8, 0xa

    #@3c
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@3e
    aput-object v9, v7, v8

    #@40
    const/16 v8, 0xb

    #@42
    const-class v9, Ljava/lang/String;

    #@44
    aput-object v9, v7, v8

    #@46
    const/16 v8, 0xc

    #@48
    const-class v9, Ljava/lang/String;

    #@4a
    aput-object v9, v7, v8

    #@4c
    const/16 v8, 0xd

    #@4e
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@50
    aput-object v9, v7, v8

    #@52
    const/16 v8, 0xe

    #@54
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@56
    aput-object v9, v7, v8

    #@58
    const/16 v8, 0xf

    #@5a
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@5c
    aput-object v9, v7, v8

    #@5e
    const/16 v8, 0x10

    #@60
    const-class v9, Ljava/lang/String;

    #@62
    aput-object v9, v7, v8

    #@64
    const/16 v8, 0x11

    #@66
    const-class v9, Ljava/lang/String;

    #@68
    aput-object v9, v7, v8

    #@6a
    const/16 v8, 0x12

    #@6c
    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@6e
    aput-object v9, v7, v8

    #@70
    .line 42
    .local v7, setWifiVZWApEnabledParamType:[Ljava/lang/Class;
    const/16 v8, 0x12

    #@72
    new-array v6, v8, [Ljava/lang/Class;

    #@74
    const-class v8, Ljava/lang/String;

    #@76
    aput-object v8, v6, v10

    #@78
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@7a
    aput-object v8, v6, v11

    #@7c
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@7e
    aput-object v8, v6, v12

    #@80
    const-class v8, Ljava/lang/String;

    #@82
    aput-object v8, v6, v13

    #@84
    const-class v8, Ljava/lang/String;

    #@86
    aput-object v8, v6, v14

    #@88
    const/4 v8, 0x5

    #@89
    const-class v9, Ljava/lang/String;

    #@8b
    aput-object v9, v6, v8

    #@8d
    const/4 v8, 0x6

    #@8e
    const-class v9, Ljava/lang/String;

    #@90
    aput-object v9, v6, v8

    #@92
    const/4 v8, 0x7

    #@93
    const-class v9, Ljava/lang/String;

    #@95
    aput-object v9, v6, v8

    #@97
    const/16 v8, 0x8

    #@99
    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@9b
    aput-object v9, v6, v8

    #@9d
    const/16 v8, 0x9

    #@9f
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@a1
    aput-object v9, v6, v8

    #@a3
    const/16 v8, 0xa

    #@a5
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@a7
    aput-object v9, v6, v8

    #@a9
    const/16 v8, 0xb

    #@ab
    const-class v9, Ljava/lang/String;

    #@ad
    aput-object v9, v6, v8

    #@af
    const/16 v8, 0xc

    #@b1
    const-class v9, Ljava/lang/String;

    #@b3
    aput-object v9, v6, v8

    #@b5
    const/16 v8, 0xd

    #@b7
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@b9
    aput-object v9, v6, v8

    #@bb
    const/16 v8, 0xe

    #@bd
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@bf
    aput-object v9, v6, v8

    #@c1
    const/16 v8, 0xf

    #@c3
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@c5
    aput-object v9, v6, v8

    #@c7
    const/16 v8, 0x10

    #@c9
    const-class v9, Ljava/lang/String;

    #@cb
    aput-object v9, v6, v8

    #@cd
    const/16 v8, 0x11

    #@cf
    const-class v9, Ljava/lang/String;

    #@d1
    aput-object v9, v6, v8

    #@d3
    .line 49
    .local v6, setWifiVZWApConfigurationParamType:[Ljava/lang/Class;
    new-array v5, v11, [Ljava/lang/Class;

    #@d5
    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@d7
    aput-object v8, v5, v10

    #@d9
    .line 52
    .local v5, setWifiNeedOnParamType:[Ljava/lang/Class;
    new-array v1, v10, [Ljava/lang/Class;

    #@db
    .line 53
    .local v1, getWifiNeedOnParamType:[Ljava/lang/Class;
    new-array v3, v10, [Ljava/lang/Class;

    #@dd
    .line 54
    .local v3, isVZWMobileHotspotEnabledParamType:[Ljava/lang/Class;
    new-array v4, v11, [Ljava/lang/Class;

    #@df
    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@e1
    aput-object v8, v4, v10

    #@e3
    .line 57
    .local v4, setVZWMobileHotspotParamType:[Ljava/lang/Class;
    new-array v2, v10, [Ljava/lang/Class;

    #@e5
    .line 61
    .local v2, isPowerSaveModeEnabledParamType:[Ljava/lang/Class;
    :try_start_e5
    const-class v8, Landroid/net/wifi/WifiManager;

    #@e7
    const-string v9, "checkAndStartWifiExt"

    #@e9
    invoke-virtual {v8, v9, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@ec
    move-result-object v8

    #@ed
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->checkAndStartWifiExtMethod:Ljava/lang/reflect/Method;

    #@ef
    .line 63
    const-class v8, Landroid/net/wifi/WifiManager;

    #@f1
    const-string/jumbo v9, "setWifiVZWApEnabled"

    #@f4
    invoke-virtual {v8, v9, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@f7
    move-result-object v8

    #@f8
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->setWifiVZWApEnabledMethod:Ljava/lang/reflect/Method;

    #@fa
    .line 68
    const-class v8, Landroid/net/wifi/WifiManager;

    #@fc
    const-string/jumbo v9, "setWifiVZWApConfiguration"

    #@ff
    invoke-virtual {v8, v9, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@102
    move-result-object v8

    #@103
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->setWifiVZWApConfigurationMethod:Ljava/lang/reflect/Method;

    #@105
    .line 71
    const-class v8, Landroid/net/wifi/WifiManager;

    #@107
    const-string v9, "getWifiNeedOn"

    #@109
    invoke-virtual {v8, v9, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@10c
    move-result-object v8

    #@10d
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->getWifiNeedOnMethod:Ljava/lang/reflect/Method;

    #@10f
    .line 73
    const-class v8, Landroid/net/wifi/WifiManager;

    #@111
    const-string/jumbo v9, "isVZWMobileHotspotEnabled"

    #@114
    invoke-virtual {v8, v9, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@117
    move-result-object v8

    #@118
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->isVZWMobileHotspotEnabledMethod:Ljava/lang/reflect/Method;

    #@11a
    .line 76
    const-class v8, Landroid/net/wifi/WifiManager;

    #@11c
    const-string/jumbo v9, "setVZWMobileHotspot"

    #@11f
    invoke-virtual {v8, v9, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@122
    move-result-object v8

    #@123
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->setVZWMobileHotspotMethod:Ljava/lang/reflect/Method;

    #@125
    .line 78
    const-class v8, Landroid/net/wifi/WifiManager;

    #@127
    const-string v9, "isPowerSaveModeEnabled"

    #@129
    invoke-virtual {v8, v9, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@12c
    move-result-object v8

    #@12d
    sput-object v8, Landroid/net/wifi/WifiManagerProxy;->isPowerSaveModeEnabledMethod:Ljava/lang/reflect/Method;
    :try_end_12f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_e5 .. :try_end_12f} :catch_130

    #@12f
    .line 84
    :goto_12f
    return-void

    #@130
    .line 81
    :catch_130
    move-exception v8

    #@131
    goto :goto_12f
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 91
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 92
    if-eqz p1, :cond_18

    #@5
    .line 93
    const-string/jumbo v0, "wifi"

    #@8
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@e
    iput-object v0, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@10
    .line 94
    const-string v0, "WifiManagerProxy"

    #@12
    const-string v1, "WifiManagerProxy is created"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 99
    :goto_17
    return-void

    #@18
    .line 97
    :cond_18
    const-string v0, "WifiManagerProxy"

    #@1a
    const-string v1, "WifiManagerProxy is created - fail"

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_17
.end method


# virtual methods
.method public checkAndStartWifiExt()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 112
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->checkAndStartWifiExtMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_29

    #@4
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6
    if-eqz v3, :cond_29

    #@8
    .line 113
    const/4 v3, 0x0

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    .line 115
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->checkAndStartWifiExtMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v4, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@f
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_12} :catch_13
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_12} :catch_18
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_12} :catch_1d

    #@12
    .line 132
    .end local v0           #args:[Ljava/lang/Object;
    :cond_12
    :goto_12
    return-void

    #@13
    .line 117
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_13
    move-exception v2

    #@14
    .line 118
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@17
    goto :goto_12

    #@18
    .line 119
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_18
    move-exception v2

    #@19
    .line 120
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@1c
    goto :goto_12

    #@1d
    .line 121
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_1d
    move-exception v2

    #@1e
    .line 122
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@21
    move-result-object v1

    #@22
    .line 123
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@24
    if-eqz v3, :cond_12

    #@26
    .line 124
    check-cast v1, Landroid/os/RemoteException;

    #@28
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@29
    .line 131
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_29
    const-string v3, "WifiManagerProxy"

    #@2b
    const-string v4, "checkAndStartWifiExt method isn\'t implemented yet"

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_12
.end method

.method public getWifiNeedOn()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 299
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->getWifiNeedOnMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v3, :cond_31

    #@5
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@7
    if-eqz v3, :cond_31

    #@9
    .line 300
    new-array v0, v4, [Ljava/lang/Object;

    #@b
    .line 302
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->getWifiNeedOnMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v5, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@f
    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Ljava/lang/Boolean;

    #@15
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_18} :catch_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_18} :catch_20
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_18} :catch_25

    #@18
    move-result v3

    #@19
    .line 318
    .end local v0           #args:[Ljava/lang/Object;
    :goto_19
    return v3

    #@1a
    .line 303
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1a
    move-exception v2

    #@1b
    .line 304
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1e
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_1e
    :goto_1e
    move v3, v4

    #@1f
    .line 313
    goto :goto_19

    #@20
    .line 305
    :catch_20
    move-exception v2

    #@21
    .line 306
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@24
    goto :goto_1e

    #@25
    .line 307
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_25
    move-exception v2

    #@26
    .line 308
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@29
    move-result-object v1

    #@2a
    .line 309
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@2c
    if-eqz v3, :cond_1e

    #@2e
    .line 310
    check-cast v1, Landroid/os/RemoteException;

    #@30
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@31
    .line 317
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_31
    const-string v3, "WifiManagerProxy"

    #@33
    const-string v5, "getWifiNeedOn method isn\'t implemented yet"

    #@35
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    move v3, v4

    #@39
    .line 318
    goto :goto_19
.end method

.method public isPowerSaveModeEnabled()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 400
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->isPowerSaveModeEnabledMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v3, :cond_31

    #@5
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@7
    if-eqz v3, :cond_31

    #@9
    .line 401
    new-array v0, v4, [Ljava/lang/Object;

    #@b
    .line 403
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->isPowerSaveModeEnabledMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v5, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@f
    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Ljava/lang/Boolean;

    #@15
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_18} :catch_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_18} :catch_20
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_18} :catch_25

    #@18
    move-result v3

    #@19
    .line 419
    .end local v0           #args:[Ljava/lang/Object;
    :goto_19
    return v3

    #@1a
    .line 404
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1a
    move-exception v2

    #@1b
    .line 405
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1e
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_1e
    :goto_1e
    move v3, v4

    #@1f
    .line 414
    goto :goto_19

    #@20
    .line 406
    :catch_20
    move-exception v2

    #@21
    .line 407
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@24
    goto :goto_1e

    #@25
    .line 408
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_25
    move-exception v2

    #@26
    .line 409
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@29
    move-result-object v1

    #@2a
    .line 410
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@2c
    if-eqz v3, :cond_1e

    #@2e
    .line 411
    check-cast v1, Landroid/os/RemoteException;

    #@30
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@31
    .line 418
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_31
    const-string v3, "WifiManagerProxy"

    #@33
    const-string v5, "isPowerSaveModeEnabled method isn\'t implemented yet"

    #@35
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    move v3, v4

    #@39
    .line 419
    goto :goto_19
.end method

.method public isVZWMobileHotspotEnabled()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 332
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->isVZWMobileHotspotEnabledMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v3, :cond_31

    #@5
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@7
    if-eqz v3, :cond_31

    #@9
    .line 333
    new-array v0, v4, [Ljava/lang/Object;

    #@b
    .line 335
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->isVZWMobileHotspotEnabledMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v5, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@f
    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v3

    #@13
    check-cast v3, Ljava/lang/Boolean;

    #@15
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_18} :catch_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_18} :catch_20
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_18} :catch_25

    #@18
    move-result v3

    #@19
    .line 351
    .end local v0           #args:[Ljava/lang/Object;
    :goto_19
    return v3

    #@1a
    .line 336
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1a
    move-exception v2

    #@1b
    .line 337
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1e
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_1e
    :goto_1e
    move v3, v4

    #@1f
    .line 346
    goto :goto_19

    #@20
    .line 338
    :catch_20
    move-exception v2

    #@21
    .line 339
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@24
    goto :goto_1e

    #@25
    .line 340
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_25
    move-exception v2

    #@26
    .line 341
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@29
    move-result-object v1

    #@2a
    .line 342
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@2c
    if-eqz v3, :cond_1e

    #@2e
    .line 343
    check-cast v1, Landroid/os/RemoteException;

    #@30
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@31
    .line 350
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_31
    const-string v3, "WifiManagerProxy"

    #@33
    const-string/jumbo v5, "isVZWMobileHotspotEnabled method isn\'t implemented yet"

    #@36
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    move v3, v4

    #@3a
    .line 351
    goto :goto_19
.end method

.method public setVZWMobileHotspot(Z)Z
    .registers 8
    .parameter "enable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 365
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setVZWMobileHotspotMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v3, :cond_38

    #@5
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@7
    if-eqz v3, :cond_38

    #@9
    .line 366
    const/4 v3, 0x1

    #@a
    new-array v0, v3, [Ljava/lang/Object;

    #@c
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@f
    move-result-object v3

    #@10
    aput-object v3, v0, v4

    #@12
    .line 370
    .local v0, args:[Ljava/lang/Object;
    :try_start_12
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setVZWMobileHotspotMethod:Ljava/lang/reflect/Method;

    #@14
    iget-object v5, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@16
    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Ljava/lang/Boolean;

    #@1c
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_1f} :catch_21
    .catch Ljava/lang/IllegalAccessException; {:try_start_12 .. :try_end_1f} :catch_27
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_12 .. :try_end_1f} :catch_2c

    #@1f
    move-result v3

    #@20
    .line 386
    .end local v0           #args:[Ljava/lang/Object;
    :goto_20
    return v3

    #@21
    .line 371
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_21
    move-exception v2

    #@22
    .line 372
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@25
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_25
    :goto_25
    move v3, v4

    #@26
    .line 381
    goto :goto_20

    #@27
    .line 373
    :catch_27
    move-exception v2

    #@28
    .line 374
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@2b
    goto :goto_25

    #@2c
    .line 375
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_2c
    move-exception v2

    #@2d
    .line 376
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@30
    move-result-object v1

    #@31
    .line 377
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@33
    if-eqz v3, :cond_25

    #@35
    .line 378
    check-cast v1, Landroid/os/RemoteException;

    #@37
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@38
    .line 385
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_38
    const-string v3, "WifiManagerProxy"

    #@3a
    const-string/jumbo v5, "setVZWMobileHotspot method isn\'t implemented yet"

    #@3d
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    move v3, v4

    #@41
    .line 386
    goto :goto_20
.end method

.method public setWifiNeedOn(Z)Z
    .registers 8
    .parameter "enable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 264
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setWifiNeedOnMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v3, :cond_38

    #@5
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@7
    if-eqz v3, :cond_38

    #@9
    .line 265
    const/4 v3, 0x1

    #@a
    new-array v0, v3, [Ljava/lang/Object;

    #@c
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@f
    move-result-object v3

    #@10
    aput-object v3, v0, v4

    #@12
    .line 269
    .local v0, args:[Ljava/lang/Object;
    :try_start_12
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setWifiNeedOnMethod:Ljava/lang/reflect/Method;

    #@14
    iget-object v5, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@16
    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Ljava/lang/Boolean;

    #@1c
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_1f} :catch_21
    .catch Ljava/lang/IllegalAccessException; {:try_start_12 .. :try_end_1f} :catch_27
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_12 .. :try_end_1f} :catch_2c

    #@1f
    move-result v3

    #@20
    .line 285
    .end local v0           #args:[Ljava/lang/Object;
    :goto_20
    return v3

    #@21
    .line 270
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_21
    move-exception v2

    #@22
    .line 271
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@25
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_25
    :goto_25
    move v3, v4

    #@26
    .line 280
    goto :goto_20

    #@27
    .line 272
    :catch_27
    move-exception v2

    #@28
    .line 273
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@2b
    goto :goto_25

    #@2c
    .line 274
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_2c
    move-exception v2

    #@2d
    .line 275
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@30
    move-result-object v1

    #@31
    .line 276
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@33
    if-eqz v3, :cond_25

    #@35
    .line 277
    check-cast v1, Landroid/os/RemoteException;

    #@37
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@38
    .line 284
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_38
    const-string v3, "WifiManagerProxy"

    #@3a
    const-string/jumbo v5, "setWifiNeedOn method isn\'t implemented yet"

    #@3d
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    move v3, v4

    #@41
    .line 285
    goto :goto_20
.end method

.method public setWifiVZWApConfiguration(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V
    .registers 24
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 226
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setWifiVZWApConfigurationMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_78

    #@4
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6
    if-eqz v3, :cond_78

    #@8
    .line 227
    const/16 v3, 0x12

    #@a
    new-array v0, v3, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    aput-object p1, v0, v3

    #@f
    const/4 v3, 0x1

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v4

    #@14
    aput-object v4, v0, v3

    #@16
    const/4 v3, 0x2

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v4

    #@1b
    aput-object v4, v0, v3

    #@1d
    const/4 v3, 0x3

    #@1e
    aput-object p4, v0, v3

    #@20
    const/4 v3, 0x4

    #@21
    aput-object p5, v0, v3

    #@23
    const/4 v3, 0x5

    #@24
    aput-object p6, v0, v3

    #@26
    const/4 v3, 0x6

    #@27
    aput-object p7, v0, v3

    #@29
    const/4 v3, 0x7

    #@2a
    aput-object p8, v0, v3

    #@2c
    const/16 v3, 0x8

    #@2e
    invoke-static {p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@31
    move-result-object v4

    #@32
    aput-object v4, v0, v3

    #@34
    const/16 v3, 0x9

    #@36
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v4

    #@3a
    aput-object v4, v0, v3

    #@3c
    const/16 v3, 0xa

    #@3e
    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v4

    #@42
    aput-object v4, v0, v3

    #@44
    const/16 v3, 0xb

    #@46
    aput-object p12, v0, v3

    #@48
    const/16 v3, 0xc

    #@4a
    aput-object p13, v0, v3

    #@4c
    const/16 v3, 0xd

    #@4e
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v4

    #@52
    aput-object v4, v0, v3

    #@54
    const/16 v3, 0xe

    #@56
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v4

    #@5a
    aput-object v4, v0, v3

    #@5c
    const/16 v3, 0xf

    #@5e
    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61
    move-result-object v4

    #@62
    aput-object v4, v0, v3

    #@64
    const/16 v3, 0x10

    #@66
    aput-object p17, v0, v3

    #@68
    const/16 v3, 0x11

    #@6a
    aput-object p18, v0, v3

    #@6c
    .line 234
    .local v0, args:[Ljava/lang/Object;
    :try_start_6c
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setWifiVZWApConfigurationMethod:Ljava/lang/reflect/Method;

    #@6e
    iget-object v4, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@70
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_73
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6c .. :try_end_73} :catch_74
    .catch Ljava/lang/IllegalAccessException; {:try_start_6c .. :try_end_73} :catch_81
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6c .. :try_end_73} :catch_86

    #@73
    .line 250
    .end local v0           #args:[Ljava/lang/Object;
    :goto_73
    return-void

    #@74
    .line 236
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_74
    move-exception v2

    #@75
    .line 237
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@78
    .line 249
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_78
    :goto_78
    const-string v3, "WifiManagerProxy"

    #@7a
    const-string/jumbo v4, "setWifiVZWApConfiguration method isn\'t implemented yet"

    #@7d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    goto :goto_73

    #@81
    .line 238
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_81
    move-exception v2

    #@82
    .line 239
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@85
    goto :goto_78

    #@86
    .line 240
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_86
    move-exception v2

    #@87
    .line 241
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@8a
    move-result-object v1

    #@8b
    .line 242
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@8d
    if-eqz v3, :cond_78

    #@8f
    .line 243
    check-cast v1, Landroid/os/RemoteException;

    #@91
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1
.end method

.method public setWifiVZWApEnabled(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Z)Z
    .registers 25
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 148
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setWifiVZWApEnabledMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_9a

    #@4
    iget-object v3, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@6
    if-eqz v3, :cond_9a

    #@8
    .line 149
    const/16 v3, 0x13

    #@a
    new-array v0, v3, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    aput-object p1, v0, v3

    #@f
    const/4 v3, 0x1

    #@10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v4

    #@14
    aput-object v4, v0, v3

    #@16
    const/4 v3, 0x2

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v4

    #@1b
    aput-object v4, v0, v3

    #@1d
    const/4 v3, 0x3

    #@1e
    aput-object p4, v0, v3

    #@20
    const/4 v3, 0x4

    #@21
    aput-object p5, v0, v3

    #@23
    const/4 v3, 0x5

    #@24
    aput-object p6, v0, v3

    #@26
    const/4 v3, 0x6

    #@27
    aput-object p7, v0, v3

    #@29
    const/4 v3, 0x7

    #@2a
    aput-object p8, v0, v3

    #@2c
    const/16 v3, 0x8

    #@2e
    invoke-static {p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@31
    move-result-object v4

    #@32
    aput-object v4, v0, v3

    #@34
    const/16 v3, 0x9

    #@36
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v4

    #@3a
    aput-object v4, v0, v3

    #@3c
    const/16 v3, 0xa

    #@3e
    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v4

    #@42
    aput-object v4, v0, v3

    #@44
    const/16 v3, 0xb

    #@46
    aput-object p12, v0, v3

    #@48
    const/16 v3, 0xc

    #@4a
    aput-object p13, v0, v3

    #@4c
    const/16 v3, 0xd

    #@4e
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v4

    #@52
    aput-object v4, v0, v3

    #@54
    const/16 v3, 0xe

    #@56
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v4

    #@5a
    aput-object v4, v0, v3

    #@5c
    const/16 v3, 0xf

    #@5e
    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61
    move-result-object v4

    #@62
    aput-object v4, v0, v3

    #@64
    const/16 v3, 0x10

    #@66
    aput-object p17, v0, v3

    #@68
    const/16 v3, 0x11

    #@6a
    aput-object p18, v0, v3

    #@6c
    const/16 v3, 0x12

    #@6e
    invoke-static/range {p19 .. p19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@71
    move-result-object v4

    #@72
    aput-object v4, v0, v3

    #@74
    .line 156
    .local v0, args:[Ljava/lang/Object;
    :try_start_74
    sget-object v3, Landroid/net/wifi/WifiManagerProxy;->setWifiVZWApEnabledMethod:Ljava/lang/reflect/Method;

    #@76
    iget-object v4, p0, Landroid/net/wifi/WifiManagerProxy;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@78
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    move-result-object v3

    #@7c
    check-cast v3, Ljava/lang/Boolean;

    #@7e
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_81
    .catch Ljava/lang/IllegalArgumentException; {:try_start_74 .. :try_end_81} :catch_83
    .catch Ljava/lang/IllegalAccessException; {:try_start_74 .. :try_end_81} :catch_89
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_74 .. :try_end_81} :catch_8e

    #@81
    move-result v3

    #@82
    .line 172
    .end local v0           #args:[Ljava/lang/Object;
    :goto_82
    return v3

    #@83
    .line 157
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_83
    move-exception v2

    #@84
    .line 158
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@87
    .line 167
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_87
    :goto_87
    const/4 v3, 0x0

    #@88
    goto :goto_82

    #@89
    .line 159
    :catch_89
    move-exception v2

    #@8a
    .line 160
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@8d
    goto :goto_87

    #@8e
    .line 161
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_8e
    move-exception v2

    #@8f
    .line 162
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@92
    move-result-object v1

    #@93
    .line 163
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@95
    if-eqz v3, :cond_87

    #@97
    .line 164
    check-cast v1, Landroid/os/RemoteException;

    #@99
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@9a
    .line 171
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_9a
    const-string v3, "WifiManagerProxy"

    #@9c
    const-string/jumbo v4, "setWifiVZWApEnabled method isn\'t implemented yet"

    #@9f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 172
    const/4 v3, 0x0

    #@a3
    goto :goto_82
.end method
