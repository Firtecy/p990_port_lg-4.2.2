.class Landroid/net/wifi/WifiStateMachine$7;
.super Ljava/lang/Object;
.source "WifiStateMachine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/net/wifi/WifiStateMachine;->startVZWSoftApWithConfig(Landroid/net/wifi/WifiVZWConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;

.field final synthetic val$config:Landroid/net/wifi/WifiVZWConfiguration;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiVZWConfiguration;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 5035
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    iput-object p2, p0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 25

    #@0
    .prologue
    .line 5038
    :try_start_0
    const-string v1, "WifiStateMachine"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "startVZWSoftApWithConfig "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    move-object/from16 v0, p0

    #@10
    iget-object v3, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@12
    iget-object v3, v3, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 5040
    move-object/from16 v0, p0

    #@21
    iget-object v1, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@23
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;

    #@26
    move-result-object v1

    #@27
    move-object/from16 v0, p0

    #@29
    iget-object v2, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@2b
    iget-object v2, v2, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v3, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@31
    invoke-virtual {v3}, Landroid/net/wifi/WifiVZWConfiguration;->getAuthType()I

    #@34
    move-result v3

    #@35
    move-object/from16 v0, p0

    #@37
    iget-object v4, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@39
    iget v4, v4, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@3b
    move-object/from16 v0, p0

    #@3d
    iget-object v5, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@3f
    iget-object v5, v5, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@41
    move-object/from16 v0, p0

    #@43
    iget-object v6, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@45
    iget-object v6, v6, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@47
    move-object/from16 v0, p0

    #@49
    iget-object v7, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@4b
    iget-object v7, v7, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v8, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@51
    iget-object v8, v8, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v9, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@57
    iget-object v9, v9, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@59
    move-object/from16 v0, p0

    #@5b
    iget-object v10, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@5d
    iget-boolean v10, v10, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@5f
    move-object/from16 v0, p0

    #@61
    iget-object v11, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@63
    iget v11, v11, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@65
    move-object/from16 v0, p0

    #@67
    iget-object v12, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@69
    iget v12, v12, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget-object v13, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@6f
    iget-object v13, v13, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@71
    move-object/from16 v0, p0

    #@73
    iget-object v14, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@75
    iget-object v14, v14, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@77
    move-object/from16 v0, p0

    #@79
    iget-object v15, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@7b
    iget v15, v15, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@81
    move-object/from16 v16, v0

    #@83
    move-object/from16 v0, v16

    #@85
    iget v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@87
    move/from16 v16, v0

    #@89
    move-object/from16 v0, p0

    #@8b
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@8d
    move-object/from16 v17, v0

    #@8f
    move-object/from16 v0, v17

    #@91
    iget v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@93
    move/from16 v17, v0

    #@95
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@99
    move-object/from16 v18, v0

    #@9b
    move-object/from16 v0, v18

    #@9d
    iget-object v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@9f
    move-object/from16 v18, v0

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@a5
    move-object/from16 v19, v0

    #@a7
    move-object/from16 v0, v19

    #@a9
    iget-object v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@ab
    move-object/from16 v19, v0

    #@ad
    move-object/from16 v0, p0

    #@af
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b1
    move-object/from16 v20, v0

    #@b3
    invoke-static/range {v20 .. v20}, Landroid/net/wifi/WifiStateMachine;->access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@b6
    move-result-object v20

    #@b7
    const-string/jumbo v21, "wlan0"

    #@ba
    invoke-interface/range {v1 .. v21}, Landroid/os/INetworkManagementService;->startVZWAccessPoint(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_bd
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_bd} :catch_c8

    #@bd
    .line 5065
    :goto_bd
    move-object/from16 v0, p0

    #@bf
    iget-object v1, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c1
    const v2, 0x20087

    #@c4
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@c7
    .line 5066
    :goto_c7
    return-void

    #@c8
    .line 5044
    :catch_c8
    move-exception v22

    #@c9
    .line 5045
    .local v22, e:Ljava/lang/Exception;
    const-string v1, "WifiStateMachine"

    #@cb
    new-instance v2, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v3, "Exception in softap start "

    #@d2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    move-object/from16 v0, v22

    #@d8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v2

    #@e0
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 5047
    :try_start_e3
    move-object/from16 v0, p0

    #@e5
    iget-object v1, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e7
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;

    #@ea
    move-result-object v1

    #@eb
    move-object/from16 v0, p0

    #@ed
    iget-object v2, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ef
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@f2
    move-result-object v2

    #@f3
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->stopVZWAccessPoint(Ljava/lang/String;)V

    #@f6
    .line 5051
    move-object/from16 v0, p0

    #@f8
    iget-object v1, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@fa
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;

    #@fd
    move-result-object v1

    #@fe
    move-object/from16 v0, p0

    #@100
    iget-object v2, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@102
    iget-object v2, v2, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@104
    move-object/from16 v0, p0

    #@106
    iget-object v3, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@108
    invoke-virtual {v3}, Landroid/net/wifi/WifiVZWConfiguration;->getAuthType()I

    #@10b
    move-result v3

    #@10c
    move-object/from16 v0, p0

    #@10e
    iget-object v4, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@110
    iget v4, v4, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@112
    move-object/from16 v0, p0

    #@114
    iget-object v5, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@116
    iget-object v5, v5, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@118
    move-object/from16 v0, p0

    #@11a
    iget-object v6, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@11c
    iget-object v6, v6, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@11e
    move-object/from16 v0, p0

    #@120
    iget-object v7, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@122
    iget-object v7, v7, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@124
    move-object/from16 v0, p0

    #@126
    iget-object v8, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@128
    iget-object v8, v8, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-object v9, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@12e
    iget-object v9, v9, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@130
    move-object/from16 v0, p0

    #@132
    iget-object v10, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@134
    iget-boolean v10, v10, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@136
    move-object/from16 v0, p0

    #@138
    iget-object v11, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@13a
    iget v11, v11, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@13c
    move-object/from16 v0, p0

    #@13e
    iget-object v12, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@140
    iget v12, v12, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@142
    move-object/from16 v0, p0

    #@144
    iget-object v13, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@146
    iget-object v13, v13, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@148
    move-object/from16 v0, p0

    #@14a
    iget-object v14, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@14c
    iget-object v14, v14, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@14e
    move-object/from16 v0, p0

    #@150
    iget-object v15, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@152
    iget v15, v15, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@154
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@158
    move-object/from16 v16, v0

    #@15a
    move-object/from16 v0, v16

    #@15c
    iget v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@15e
    move/from16 v16, v0

    #@160
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@164
    move-object/from16 v17, v0

    #@166
    move-object/from16 v0, v17

    #@168
    iget v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@16a
    move/from16 v17, v0

    #@16c
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@170
    move-object/from16 v18, v0

    #@172
    move-object/from16 v0, v18

    #@174
    iget-object v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@176
    move-object/from16 v18, v0

    #@178
    move-object/from16 v0, p0

    #@17a
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->val$config:Landroid/net/wifi/WifiVZWConfiguration;

    #@17c
    move-object/from16 v19, v0

    #@17e
    move-object/from16 v0, v19

    #@180
    iget-object v0, v0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@182
    move-object/from16 v19, v0

    #@184
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@188
    move-object/from16 v20, v0

    #@18a
    invoke-static/range {v20 .. v20}, Landroid/net/wifi/WifiStateMachine;->access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@18d
    move-result-object v20

    #@18e
    const-string/jumbo v21, "wlan0"

    #@191
    invoke-interface/range {v1 .. v21}, Landroid/os/INetworkManagementService;->startVZWAccessPoint(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@194
    .line 5056
    move-object/from16 v0, p0

    #@196
    iget-object v1, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@198
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$9100(Landroid/net/wifi/WifiStateMachine;)V
    :try_end_19b
    .catch Ljava/lang/Exception; {:try_start_e3 .. :try_end_19b} :catch_19d

    #@19b
    goto/16 :goto_bd

    #@19d
    .line 5058
    :catch_19d
    move-exception v23

    #@19e
    .line 5059
    .local v23, e1:Ljava/lang/Exception;
    const-string v1, "WifiStateMachine"

    #@1a0
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v3, "Exception in softap re-start "

    #@1a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v2

    #@1ab
    move-object/from16 v0, v23

    #@1ad
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v2

    #@1b1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v2

    #@1b5
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b8
    .line 5060
    move-object/from16 v0, p0

    #@1ba
    iget-object v1, v0, Landroid/net/wifi/WifiStateMachine$7;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1bc
    const v2, 0x20088

    #@1bf
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@1c2
    goto/16 :goto_c7
.end method
