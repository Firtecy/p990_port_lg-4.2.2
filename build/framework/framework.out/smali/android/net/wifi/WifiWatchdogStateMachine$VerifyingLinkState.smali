.class Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;
.super Lcom/android/internal/util/State;
.source "WifiWatchdogStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiWatchdogStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VerifyingLinkState"
.end annotation


# instance fields
.field private mSampleCount:I

.field final synthetic this$0:Landroid/net/wifi/WifiWatchdogStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiWatchdogStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 618
    iput-object p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 624
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_e

    #@7
    invoke-virtual {p0}, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->getName()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@e
    .line 625
    :cond_e
    iput v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->mSampleCount:I

    #@10
    .line 626
    iget-object v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@12
    invoke-static {v0}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->newLinkDetected()V

    #@19
    .line 627
    iget-object v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1b
    iget-object v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@1d
    const v2, 0x2100b

    #@20
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@22
    invoke-static {v3}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2604(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@25
    move-result v3

    #@26
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiWatchdogStateMachine;->sendMessage(Landroid/os/Message;)V

    #@2d
    .line 628
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v10, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v5, 0x1

    #@4
    .line 632
    iget v6, p1, Landroid/os/Message;->what:I

    #@6
    sparse-switch v6, :sswitch_data_116

    #@9
    .line 684
    :goto_9
    return v4

    #@a
    .line 634
    :sswitch_a
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@c
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$200(Landroid/net/wifi/WifiWatchdogStateMachine;)V

    #@f
    .line 635
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@11
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1700(Landroid/net/wifi/WifiWatchdogStateMachine;)Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_1c

    #@17
    .line 636
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@19
    invoke-static {v4, v5}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1100(Landroid/net/wifi/WifiWatchdogStateMachine;Z)V

    #@1c
    :cond_1c
    :goto_1c
    move v4, v5

    #@1d
    .line 684
    goto :goto_9

    #@1e
    .line 641
    :sswitch_1e
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@20
    iget-object v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@22
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1900(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;

    #@25
    move-result-object v6

    #@26
    invoke-static {v4, v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2700(Landroid/net/wifi/WifiWatchdogStateMachine;Lcom/android/internal/util/IState;)V

    #@29
    goto :goto_1c

    #@2a
    .line 645
    :sswitch_2a
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@2c
    iget-object v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@2e
    invoke-static {v7}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2600(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@31
    move-result v7

    #@32
    if-ne v6, v7, :cond_1c

    #@34
    .line 646
    iget-object v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@36
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2800(Landroid/net/wifi/WifiWatchdogStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@39
    move-result-object v6

    #@3a
    const v7, 0x25014

    #@3d
    invoke-virtual {v6, v7}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@40
    .line 647
    iget-object v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@42
    iget-object v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@44
    const v8, 0x2100b

    #@47
    iget-object v9, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@49
    invoke-static {v9}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$2604(Landroid/net/wifi/WifiWatchdogStateMachine;)I

    #@4c
    move-result v9

    #@4d
    invoke-virtual {v7, v8, v9, v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@50
    move-result-object v4

    #@51
    const-wide/16 v7, 0x3e8

    #@53
    invoke-virtual {v6, v4, v7, v8}, Landroid/net/wifi/WifiWatchdogStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@56
    goto :goto_1c

    #@57
    .line 653
    :sswitch_57
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@59
    check-cast v0, Landroid/net/wifi/RssiPacketCountInfo;

    #@5b
    .line 654
    .local v0, info:Landroid/net/wifi/RssiPacketCountInfo;
    iget v1, v0, Landroid/net/wifi/RssiPacketCountInfo;->rssi:I

    #@5d
    .line 655
    .local v1, rssi:I
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@60
    move-result v6

    #@61
    if-eqz v6, :cond_79

    #@63
    new-instance v6, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v7, "Fetch RSSI succeed, rssi="

    #@6a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v6

    #@76
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@79
    .line 657
    :cond_79
    iget-object v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@7b
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@7e
    move-result-object v6

    #@7f
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->access$2900(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)J

    #@82
    move-result-wide v6

    #@83
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@86
    move-result-wide v8

    #@87
    sub-long v2, v6, v8

    #@89
    .line 658
    .local v2, time:J
    cmp-long v6, v2, v10

    #@8b
    if-gtz v6, :cond_9f

    #@8d
    .line 660
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@90
    move-result v4

    #@91
    if-eqz v4, :cond_98

    #@93
    const-string v4, "Max avoid time elapsed"

    #@95
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@98
    .line 661
    :cond_98
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@9a
    invoke-static {v4, v5}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1100(Landroid/net/wifi/WifiWatchdogStateMachine;Z)V

    #@9d
    goto/16 :goto_1c

    #@9f
    .line 663
    :cond_9f
    iget-object v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@a1
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@a4
    move-result-object v6

    #@a5
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->access$3000(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)I

    #@a8
    move-result v6

    #@a9
    if-lt v1, v6, :cond_e9

    #@ab
    .line 664
    iget v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->mSampleCount:I

    #@ad
    add-int/lit8 v4, v4, 0x1

    #@af
    iput v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->mSampleCount:I

    #@b1
    iget-object v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@b3
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@b6
    move-result-object v6

    #@b7
    invoke-static {v6}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->access$3100(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)I

    #@ba
    move-result v6

    #@bb
    if-lt v4, v6, :cond_1c

    #@bd
    .line 666
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@c0
    move-result v4

    #@c1
    if-eqz v4, :cond_d9

    #@c3
    new-instance v4, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v6, "Good link detected, rssi="

    #@ca
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v4

    #@ce
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v4

    #@d2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v4

    #@d6
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@d9
    .line 667
    :cond_d9
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@db
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1800(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;

    #@de
    move-result-object v4

    #@df
    invoke-static {v4, v10, v11}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->access$2902(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;J)J

    #@e2
    .line 668
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@e4
    invoke-static {v4, v5}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1100(Landroid/net/wifi/WifiWatchdogStateMachine;Z)V

    #@e7
    goto/16 :goto_1c

    #@e9
    .line 671
    :cond_e9
    iput v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VerifyingLinkState;->mSampleCount:I

    #@eb
    .line 672
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@ee
    move-result v4

    #@ef
    if-eqz v4, :cond_1c

    #@f1
    new-instance v4, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v6, "Link is still poor, time left="

    #@f8
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v4

    #@fc
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v4

    #@100
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v4

    #@104
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@107
    goto/16 :goto_1c

    #@109
    .line 678
    .end local v0           #info:Landroid/net/wifi/RssiPacketCountInfo;
    .end local v1           #rssi:I
    .end local v2           #time:J
    :sswitch_109
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@10c
    move-result v4

    #@10d
    if-eqz v4, :cond_1c

    #@10f
    const-string v4, "RSSI_FETCH_FAILED"

    #@111
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@114
    goto/16 :goto_1c

    #@116
    .line 632
    :sswitch_data_116
    .sparse-switch
        0x21006 -> :sswitch_a
        0x21007 -> :sswitch_1e
        0x2100b -> :sswitch_2a
        0x25015 -> :sswitch_57
        0x25016 -> :sswitch_109
    .end sparse-switch
.end method
