.class Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;
.super Ljava/lang/Object;
.source "WifiWatchdogStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiWatchdogStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BssidStatistics"
.end annotation


# instance fields
.field private final mBssid:Ljava/lang/String;

.field private mBssidAvoidTimeMax:J

.field private mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

.field private mEntriesSize:I

.field private mGoodLinkTargetCount:I

.field private mGoodLinkTargetIndex:I

.field private mGoodLinkTargetRssi:I

.field private mLastTimeGood:J

.field private mLastTimePoor:J

.field private mLastTimeSample:J

.field private mRssiBase:I

.field final synthetic this$0:Landroid/net/wifi/WifiWatchdogStateMachine;


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiWatchdogStateMachine;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter "bssid"

    #@0
    .prologue
    .line 1069
    iput-object p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1070
    iput-object p2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssid:Ljava/lang/String;

    #@7
    .line 1071
    const/16 v1, -0x69

    #@9
    iput v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@b
    .line 1072
    const/16 v1, 0x3d

    #@d
    iput v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntriesSize:I

    #@f
    .line 1073
    iget v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntriesSize:I

    #@11
    new-array v1, v1, [Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@13
    iput-object v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@15
    .line 1074
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    iget v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntriesSize:I

    #@18
    if-ge v0, v1, :cond_2b

    #@1a
    .line 1075
    iget-object v1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@1c
    new-instance v2, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@1e
    const-wide v3, 0x3fb999999999999aL

    #@23
    invoke-direct {v2, p1, v3, v4}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;-><init>(Landroid/net/wifi/WifiWatchdogStateMachine;D)V

    #@26
    aput-object v2, v1, v0

    #@28
    .line 1074
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_16

    #@2b
    .line 1076
    :cond_2b
    return-void
.end method

.method static synthetic access$2900(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 1039
    iget-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssidAvoidTimeMax:J

    #@2
    return-wide v0
.end method

.method static synthetic access$2902(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1039
    iput-wide p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssidAvoidTimeMax:J

    #@2
    return-wide p1
.end method

.method static synthetic access$3000(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1039
    iget v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@2
    return v0
.end method

.method static synthetic access$3100(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1039
    iget v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetCount:I

    #@2
    return v0
.end method

.method static synthetic access$4200(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 1039
    iget-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mLastTimeSample:J

    #@2
    return-wide v0
.end method

.method static synthetic access$4202(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1039
    iput-wide p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mLastTimeSample:J

    #@2
    return-wide p1
.end method

.method static synthetic access$4600(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1039
    iget-object v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$4702(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1039
    iput-wide p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mLastTimeGood:J

    #@2
    return-wide p1
.end method

.method static synthetic access$4802(Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1039
    iput-wide p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mLastTimePoor:J

    #@2
    return-wide p1
.end method


# virtual methods
.method public findRssiTarget(IID)I
    .registers 17
    .parameter "from"
    .parameter "to"
    .parameter "threshold"

    #@0
    .prologue
    .line 1189
    iget v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@2
    sub-int/2addr p1, v7

    #@3
    .line 1190
    iget v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@5
    sub-int/2addr p2, v7

    #@6
    .line 1191
    const/4 v2, 0x0

    #@7
    .line 1192
    .local v2, emptyCount:I
    if-ge p1, p2, :cond_96

    #@9
    const/4 v0, 0x1

    #@a
    .line 1193
    .local v0, d:I
    :goto_a
    move v3, p1

    #@b
    .local v3, i:I
    :goto_b
    if-eq v3, p2, :cond_f9

    #@d
    .line 1195
    if-ltz v3, :cond_99

    #@f
    iget v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntriesSize:I

    #@11
    if-ge v3, v7, :cond_99

    #@13
    iget-object v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@15
    aget-object v7, v7, v3

    #@17
    invoke-static {v7}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4400(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@1a
    move-result-wide v7

    #@1b
    const-wide/high16 v9, 0x3ff0

    #@1d
    cmpl-double v7, v7, v9

    #@1f
    if-lez v7, :cond_99

    #@21
    .line 1196
    const/4 v2, 0x0

    #@22
    .line 1197
    iget-object v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@24
    aget-object v7, v7, v3

    #@26
    invoke-static {v7}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4300(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@29
    move-result-wide v7

    #@2a
    cmpg-double v7, v7, p3

    #@2c
    if-gez v7, :cond_f6

    #@2e
    .line 1199
    iget v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@30
    add-int v6, v7, v3

    #@32
    .line 1200
    .local v6, rssi:I
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@35
    move-result v7

    #@36
    if-eqz v7, :cond_95

    #@38
    .line 1201
    new-instance v1, Ljava/text/DecimalFormat;

    #@3a
    const-string v7, "#.##"

    #@3c
    invoke-direct {v1, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    #@3f
    .line 1202
    .local v1, df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v8, "Scan target found: rssi="

    #@46
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v7

    #@4a
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    const-string v8, " threshold="

    #@50
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v7

    #@54
    const-wide/high16 v8, 0x4059

    #@56
    mul-double/2addr v8, p3

    #@57
    invoke-virtual {v1, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@5a
    move-result-object v8

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    const-string v8, "% value="

    #@61
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v7

    #@65
    iget-object v8, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@67
    aget-object v8, v8, v3

    #@69
    invoke-static {v8}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4300(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@6c
    move-result-wide v8

    #@6d
    const-wide/high16 v10, 0x4059

    #@6f
    mul-double/2addr v8, v10

    #@70
    invoke-virtual {v1, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@73
    move-result-object v8

    #@74
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    const-string v8, "% volume="

    #@7a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v7

    #@7e
    iget-object v8, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@80
    aget-object v8, v8, v3

    #@82
    invoke-static {v8}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4400(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@85
    move-result-wide v8

    #@86
    invoke-virtual {v1, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v7

    #@8e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v7

    #@92
    invoke-static {v7}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@95
    .line 1224
    .end local v1           #df:Ljava/text/DecimalFormat;
    .end local v6           #rssi:I
    :cond_95
    :goto_95
    return v6

    #@96
    .line 1192
    .end local v0           #d:I
    .end local v3           #i:I
    :cond_96
    const/4 v0, -0x1

    #@97
    goto/16 :goto_a

    #@99
    .line 1209
    .restart local v0       #d:I
    .restart local v3       #i:I
    :cond_99
    add-int/lit8 v2, v2, 0x1

    #@9b
    const/4 v7, 0x3

    #@9c
    if-lt v2, v7, :cond_f6

    #@9e
    .line 1211
    iget v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@a0
    add-int v6, v7, v3

    #@a2
    .line 1212
    .restart local v6       #rssi:I
    invoke-virtual {p0, v6}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->presetLoss(I)D

    #@a5
    move-result-wide v4

    #@a6
    .line 1213
    .local v4, lossPreset:D
    cmpg-double v7, v4, p3

    #@a8
    if-gez v7, :cond_f6

    #@aa
    .line 1214
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@ad
    move-result v7

    #@ae
    if-eqz v7, :cond_95

    #@b0
    .line 1215
    new-instance v1, Ljava/text/DecimalFormat;

    #@b2
    const-string v7, "#.##"

    #@b4
    invoke-direct {v1, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    #@b7
    .line 1216
    .restart local v1       #df:Ljava/text/DecimalFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v8, "Scan target found: rssi="

    #@be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v7

    #@c2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    const-string v8, " threshold="

    #@c8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v7

    #@cc
    const-wide/high16 v8, 0x4059

    #@ce
    mul-double/2addr v8, p3

    #@cf
    invoke-virtual {v1, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v7

    #@d7
    const-string v8, "% value="

    #@d9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v7

    #@dd
    const-wide/high16 v8, 0x4059

    #@df
    mul-double/2addr v8, v4

    #@e0
    invoke-virtual {v1, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@e3
    move-result-object v8

    #@e4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v7

    #@e8
    const-string v8, "% volume=preset"

    #@ea
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v7

    #@ee
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v7

    #@f2
    invoke-static {v7}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@f5
    goto :goto_95

    #@f6
    .line 1193
    .end local v1           #df:Ljava/text/DecimalFormat;
    .end local v4           #lossPreset:D
    .end local v6           #rssi:I
    :cond_f6
    add-int/2addr v3, v0

    #@f7
    goto/16 :goto_b

    #@f9
    .line 1224
    :cond_f9
    iget v7, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@fb
    add-int v6, v7, p2

    #@fd
    goto :goto_95
.end method

.method public newLinkDetected()V
    .registers 7

    #@0
    .prologue
    .line 1164
    iget-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssidAvoidTimeMax:J

    #@2
    const-wide/16 v4, 0x0

    #@4
    cmp-long v2, v2, v4

    #@6
    if-lez v2, :cond_33

    #@8
    .line 1165
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_32

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Previous avoidance still in effect, rssi="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " count="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetCount:I

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v2}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@32
    .line 1178
    :cond_32
    :goto_32
    return-void

    #@33
    .line 1171
    :cond_33
    const/16 v0, -0x69

    #@35
    .line 1172
    .local v0, from:I
    const/16 v1, -0x2d

    #@37
    .line 1173
    .local v1, to:I
    const-wide v2, 0x3fb999999999999aL

    #@3c
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->findRssiTarget(IID)I

    #@3f
    move-result v2

    #@40
    iput v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@42
    .line 1174
    const/4 v2, 0x1

    #@43
    iput v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetCount:I

    #@45
    .line 1175
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@48
    move-result-wide v2

    #@49
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5100()[Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;

    #@4c
    move-result-object v4

    #@4d
    const/4 v5, 0x0

    #@4e
    aget-object v4, v4, v5

    #@50
    iget v4, v4, Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;->TIME_MS:I

    #@52
    int-to-long v4, v4

    #@53
    add-long/2addr v2, v4

    #@54
    iput-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssidAvoidTimeMax:J

    #@56
    .line 1176
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_32

    #@5c
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "New link verifying target set, rssi="

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    iget v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@69
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    const-string v3, " count="

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    iget v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetCount:I

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-static {v2}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@80
    goto :goto_32
.end method

.method public poorLinkDetected(I)Z
    .registers 18
    .parameter "rssi"

    #@0
    .prologue
    .line 1123
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@3
    move-result v13

    #@4
    if-eqz v13, :cond_1e

    #@6
    new-instance v13, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v14, "Poor link detected, rssi="

    #@d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v13

    #@11
    move/from16 v0, p1

    #@13
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v13

    #@17
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v13

    #@1b
    invoke-static {v13}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@1e
    .line 1125
    :cond_1e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@21
    move-result-wide v8

    #@22
    .line 1126
    .local v8, now:J
    move-object/from16 v0, p0

    #@24
    iget-wide v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mLastTimeGood:J

    #@26
    sub-long v4, v8, v13

    #@28
    .line 1127
    .local v4, lastGood:J
    move-object/from16 v0, p0

    #@2a
    iget-wide v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mLastTimePoor:J

    #@2c
    sub-long v6, v8, v13

    #@2e
    .line 1131
    .local v6, lastPoor:J
    :goto_2e
    move-object/from16 v0, p0

    #@30
    iget v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@32
    if-lez v13, :cond_52

    #@34
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5000()[Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;

    #@37
    move-result-object v13

    #@38
    move-object/from16 v0, p0

    #@3a
    iget v14, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@3c
    add-int/lit8 v14, v14, -0x1

    #@3e
    aget-object v13, v13, v14

    #@40
    iget v13, v13, Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;->REDUCE_TIME_MS:I

    #@42
    int-to-long v13, v13

    #@43
    cmp-long v13, v6, v13

    #@45
    if-ltz v13, :cond_52

    #@47
    .line 1132
    move-object/from16 v0, p0

    #@49
    iget v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@4b
    add-int/lit8 v13, v13, -0x1

    #@4d
    move-object/from16 v0, p0

    #@4f
    iput v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@51
    goto :goto_2e

    #@52
    .line 1133
    :cond_52
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5000()[Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;

    #@55
    move-result-object v13

    #@56
    move-object/from16 v0, p0

    #@58
    iget v14, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@5a
    aget-object v13, v13, v14

    #@5c
    iget v13, v13, Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;->SAMPLE_COUNT:I

    #@5e
    move-object/from16 v0, p0

    #@60
    iput v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetCount:I

    #@62
    .line 1136
    add-int/lit8 v3, p1, 0x3

    #@64
    .line 1137
    .local v3, from:I
    add-int/lit8 v12, p1, 0x14

    #@66
    .line 1138
    .local v12, to:I
    const-wide v13, 0x3fb999999999999aL

    #@6b
    move-object/from16 v0, p0

    #@6d
    invoke-virtual {v0, v3, v12, v13, v14}, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->findRssiTarget(IID)I

    #@70
    move-result v13

    #@71
    move-object/from16 v0, p0

    #@73
    iput v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@75
    .line 1139
    move-object/from16 v0, p0

    #@77
    iget v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@79
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5000()[Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;

    #@7c
    move-result-object v14

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget v15, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@81
    aget-object v14, v14, v15

    #@83
    iget v14, v14, Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;->RSSI_ADJ_DBM:I

    #@85
    add-int/2addr v13, v14

    #@86
    move-object/from16 v0, p0

    #@88
    iput v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@8a
    .line 1140
    move-object/from16 v0, p0

    #@8c
    iget v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@8e
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5000()[Landroid/net/wifi/WifiWatchdogStateMachine$GoodLinkTarget;

    #@91
    move-result-object v14

    #@92
    array-length v14, v14

    #@93
    add-int/lit8 v14, v14, -0x1

    #@95
    if-ge v13, v14, :cond_a1

    #@97
    move-object/from16 v0, p0

    #@99
    iget v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@9b
    add-int/lit8 v13, v13, 0x1

    #@9d
    move-object/from16 v0, p0

    #@9f
    iput v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetIndex:I

    #@a1
    .line 1143
    :cond_a1
    const/4 v10, 0x0

    #@a2
    .local v10, p:I
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5100()[Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;

    #@a5
    move-result-object v13

    #@a6
    array-length v13, v13

    #@a7
    add-int/lit8 v11, v13, -0x1

    #@a9
    .line 1144
    .local v11, pmax:I
    :goto_a9
    if-ge v10, v11, :cond_bc

    #@ab
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5100()[Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;

    #@ae
    move-result-object v13

    #@af
    add-int/lit8 v14, v10, 0x1

    #@b1
    aget-object v13, v13, v14

    #@b3
    iget v13, v13, Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;->MIN_RSSI_DBM:I

    #@b5
    move/from16 v0, p1

    #@b7
    if-lt v0, v13, :cond_bc

    #@b9
    add-int/lit8 v10, v10, 0x1

    #@bb
    goto :goto_a9

    #@bc
    .line 1145
    :cond_bc
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$5100()[Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;

    #@bf
    move-result-object v13

    #@c0
    aget-object v13, v13, v10

    #@c2
    iget v13, v13, Landroid/net/wifi/WifiWatchdogStateMachine$MaxAvoidTime;->TIME_MS:I

    #@c4
    int-to-long v1, v13

    #@c5
    .line 1148
    .local v1, avoidMax:J
    const-wide/16 v13, 0x0

    #@c7
    cmp-long v13, v1, v13

    #@c9
    if-gtz v13, :cond_cd

    #@cb
    const/4 v13, 0x0

    #@cc
    .line 1156
    :goto_cc
    return v13

    #@cd
    .line 1151
    :cond_cd
    add-long v13, v8, v1

    #@cf
    move-object/from16 v0, p0

    #@d1
    iput-wide v13, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mBssidAvoidTimeMax:J

    #@d3
    .line 1153
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@d6
    move-result v13

    #@d7
    if-eqz v13, :cond_11f

    #@d9
    new-instance v13, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    const-string v14, "goodRssi="

    #@e0
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v13

    #@e4
    move-object/from16 v0, p0

    #@e6
    iget v14, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetRssi:I

    #@e8
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v13

    #@ec
    const-string v14, " goodCount="

    #@ee
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v13

    #@f2
    move-object/from16 v0, p0

    #@f4
    iget v14, v0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mGoodLinkTargetCount:I

    #@f6
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v13

    #@fa
    const-string v14, " lastGood="

    #@fc
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v13

    #@100
    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@103
    move-result-object v13

    #@104
    const-string v14, " lastPoor="

    #@106
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v13

    #@10a
    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v13

    #@10e
    const-string v14, " avoidMax="

    #@110
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v13

    #@114
    invoke-virtual {v13, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@117
    move-result-object v13

    #@118
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v13

    #@11c
    invoke-static {v13}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@11f
    .line 1156
    :cond_11f
    const/4 v13, 0x1

    #@120
    goto :goto_cc
.end method

.method public presetLoss(I)D
    .registers 12
    .parameter "rssi"

    #@0
    .prologue
    const/16 v9, 0x5a

    #@2
    const-wide/high16 v2, 0x3ff0

    #@4
    .line 1104
    const/16 v4, -0x5a

    #@6
    if-gt p1, v4, :cond_9

    #@8
    .line 1113
    :goto_8
    return-wide v2

    #@9
    .line 1105
    :cond_9
    if-lez p1, :cond_e

    #@b
    const-wide/16 v2, 0x0

    #@d
    goto :goto_8

    #@e
    .line 1107
    :cond_e
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$4900()[D

    #@11
    move-result-object v4

    #@12
    if-nez v4, :cond_32

    #@14
    .line 1109
    const/16 v1, 0x5a

    #@16
    .line 1110
    .local v1, size:I
    new-array v4, v9, [D

    #@18
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$4902([D)[D

    #@1b
    .line 1111
    const/4 v0, 0x0

    #@1c
    .local v0, i:I
    :goto_1c
    if-ge v0, v9, :cond_32

    #@1e
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$4900()[D

    #@21
    move-result-object v4

    #@22
    rsub-int/lit8 v5, v0, 0x5a

    #@24
    int-to-double v5, v5

    #@25
    const-wide/high16 v7, 0x3ff8

    #@27
    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    #@2a
    move-result-wide v5

    #@2b
    div-double v5, v2, v5

    #@2d
    aput-wide v5, v4, v0

    #@2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_1c

    #@32
    .line 1113
    .end local v0           #i:I
    .end local v1           #size:I
    :cond_32
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$4900()[D

    #@35
    move-result-object v2

    #@36
    neg-int v3, p1

    #@37
    aget-wide v2, v2, v3

    #@39
    goto :goto_8
.end method

.method public updateLoss(IDI)V
    .registers 12
    .parameter "rssi"
    .parameter "value"
    .parameter "volume"

    #@0
    .prologue
    .line 1086
    if-gtz p4, :cond_3

    #@2
    .line 1095
    :cond_2
    :goto_2
    return-void

    #@3
    .line 1087
    :cond_3
    iget v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mRssiBase:I

    #@5
    sub-int v1, p1, v2

    #@7
    .line 1088
    .local v1, index:I
    if-ltz v1, :cond_2

    #@9
    iget v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntriesSize:I

    #@b
    if-ge v1, v2, :cond_2

    #@d
    .line 1089
    iget-object v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@f
    aget-object v2, v2, v1

    #@11
    invoke-virtual {v2, p2, p3, p4}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->update(DI)V

    #@14
    .line 1090
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_2

    #@1a
    .line 1091
    new-instance v0, Ljava/text/DecimalFormat;

    #@1c
    const-string v2, "#.##"

    #@1e
    invoke-direct {v0, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    #@21
    .line 1092
    .local v0, df:Ljava/text/DecimalFormat;
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v3, "Cache updated: loss["

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, "]="

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@38
    aget-object v3, v3, v1

    #@3a
    invoke-static {v3}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4300(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@3d
    move-result-wide v3

    #@3e
    const-wide/high16 v5, 0x4059

    #@40
    mul-double/2addr v3, v5

    #@41
    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, "% volume="

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$BssidStatistics;->mEntries:[Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;

    #@51
    aget-object v3, v3, v1

    #@53
    invoke-static {v3}, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->access$4400(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D

    #@56
    move-result-wide v3

    #@57
    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-static {v2}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@66
    goto :goto_2
.end method
