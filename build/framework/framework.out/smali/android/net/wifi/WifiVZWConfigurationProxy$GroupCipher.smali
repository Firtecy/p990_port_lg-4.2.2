.class public Landroid/net/wifi/WifiVZWConfigurationProxy$GroupCipher;
.super Ljava/lang/Object;
.source "WifiVZWConfigurationProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiVZWConfigurationProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GroupCipher"
.end annotation


# static fields
.field public static final CCMP:I = 0x3

.field public static final TKIP:I = 0x2

.field public static final WEP104:I = 0x1

.field public static final WEP40:I = 0x0

.field public static final strings:[Ljava/lang/String; = null

.field public static final varName:Ljava/lang/String; = "group"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 191
    const/4 v0, 0x4

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "WEP40"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "WEP104"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "TKIP"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "CCMP"

    #@15
    aput-object v2, v0, v1

    #@17
    sput-object v0, Landroid/net/wifi/WifiVZWConfigurationProxy$GroupCipher;->strings:[Ljava/lang/String;

    #@19
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 174
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 175
    return-void
.end method
