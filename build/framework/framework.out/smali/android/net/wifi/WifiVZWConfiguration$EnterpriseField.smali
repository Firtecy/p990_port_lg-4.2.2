.class public Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
.super Ljava/lang/Object;
.source "WifiVZWConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiVZWConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EnterpriseField"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiVZWConfiguration;

.field private value:Ljava/lang/String;

.field private final varName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "varName"

    #@0
    .prologue
    .line 60
    iput-object p1, p0, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->this$0:Landroid/net/wifi/WifiVZWConfiguration;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 61
    iput-object p2, p0, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->varName:Ljava/lang/String;

    #@7
    .line 62
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->value:Ljava/lang/String;

    #@a
    .line 63
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;)V

    #@3
    return-void
.end method


# virtual methods
.method public setValue(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 66
    iput-object p1, p0, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->value:Ljava/lang/String;

    #@2
    .line 67
    return-void
.end method

.method public value()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->value:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public varName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->varName:Ljava/lang/String;

    #@2
    return-object v0
.end method
