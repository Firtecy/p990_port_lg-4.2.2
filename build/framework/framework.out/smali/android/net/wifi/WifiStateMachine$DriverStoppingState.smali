.class Landroid/net/wifi/WifiStateMachine$DriverStoppingState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DriverStoppingState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3403
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 3407
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 3408
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 3412
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v1, :sswitch_data_24

    #@5
    .line 3434
    const/4 v1, 0x0

    #@6
    .line 3436
    :goto_6
    return v1

    #@7
    .line 3414
    :sswitch_7
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9
    invoke-static {v1, p1}, Landroid/net/wifi/WifiStateMachine;->access$8300(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)Landroid/net/wifi/SupplicantState;

    #@c
    move-result-object v0

    #@d
    .line 3415
    .local v0, state:Landroid/net/wifi/SupplicantState;
    sget-object v1, Landroid/net/wifi/SupplicantState;->INTERFACE_DISABLED:Landroid/net/wifi/SupplicantState;

    #@f
    if-ne v0, v1, :cond_1c

    #@11
    .line 3416
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@15
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$8500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/net/wifi/WifiStateMachine;->access$11600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@1c
    .line 3436
    .end local v0           #state:Landroid/net/wifi/SupplicantState;
    :cond_1c
    :goto_1c
    const/4 v1, 0x1

    #@1d
    goto :goto_6

    #@1e
    .line 3431
    :sswitch_1e
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@20
    invoke-static {v1, p1}, Landroid/net/wifi/WifiStateMachine;->access$11700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@23
    goto :goto_1c

    #@24
    .line 3412
    :sswitch_data_24
    .sparse-switch
        0x2000d -> :sswitch_1e
        0x2000e -> :sswitch_1e
        0x20047 -> :sswitch_1e
        0x20049 -> :sswitch_1e
        0x2004a -> :sswitch_1e
        0x2004b -> :sswitch_1e
        0x2004c -> :sswitch_1e
        0x20050 -> :sswitch_1e
        0x20054 -> :sswitch_1e
        0x20055 -> :sswitch_1e
        0x2005a -> :sswitch_1e
        0x24006 -> :sswitch_7
    .end sparse-switch
.end method
