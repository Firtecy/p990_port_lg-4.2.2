.class public Landroid/net/wifi/WifiManager;
.super Ljava/lang/Object;
.source "WifiManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiManager$1;,
        Landroid/net/wifi/WifiManager$MulticastLock;,
        Landroid/net/wifi/WifiManager$WifiLock;,
        Landroid/net/wifi/WifiManager$ServiceHandler;,
        Landroid/net/wifi/WifiManager$TxPacketCountListener;,
        Landroid/net/wifi/WifiManager$WpsListener;,
        Landroid/net/wifi/WifiManager$ActionListener;
    }
.end annotation


# static fields
.field public static final ACTION_HS20_AP_EVENT:Ljava/lang/String; = "android.net.wifi.HS20_AP_EVENT"

.field public static final ACTION_HS20_TRY_CONNECTION:Ljava/lang/String; = "android.net.wifi.HS20_TRY_CONNECTION"

.field public static final ACTION_PICK_WIFI_NETWORK:Ljava/lang/String; = "android.net.wifi.PICK_WIFI_NETWORK"

.field private static final BASE:I = 0x25000

.field public static final BUSY:I = 0x2

.field public static final CANCEL_WPS:I = 0x2500e

.field public static final CANCEL_WPS_FAILED:I = 0x2500f

.field public static final CANCEL_WPS_SUCCEDED:I = 0x25010

.field public static final CHANGE_REASON_ADDED:I = 0x0

.field public static final CHANGE_REASON_CONFIG_CHANGE:I = 0x2

.field public static final CHANGE_REASON_REMOVED:I = 0x1

.field public static final CONFIGURED_NETWORKS_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

.field public static final CONNECT_NETWORK:I = 0x25001

.field public static final CONNECT_NETWORK_FAILED:I = 0x25002

.field public static final CONNECT_NETWORK_SUCCEEDED:I = 0x25003

.field public static final DATA_ACTIVITY_IN:I = 0x1

.field public static final DATA_ACTIVITY_INOUT:I = 0x3

.field public static final DATA_ACTIVITY_NONE:I = 0x0

.field public static final DATA_ACTIVITY_NOTIFICATION:I = 0x1

.field public static final DATA_ACTIVITY_OUT:I = 0x2

.field public static final DISABLE_NETWORK:I = 0x25011

.field public static final DISABLE_NETWORK_FAILED:I = 0x25012

.field public static final DISABLE_NETWORK_SUCCEEDED:I = 0x25013

.field public static final ENABLE_TRAFFIC_STATS_POLL:I = 0x2501f

.field public static final ERROR:I = 0x0

.field public static final ERROR_AUTHENTICATING:I = 0x1

.field public static final EXTRA_BSSID:Ljava/lang/String; = "bssid"

.field public static final EXTRA_CHANGE_REASON:Ljava/lang/String; = "changeReason"

.field public static final EXTRA_HS20_BSSID:Ljava/lang/String; = "bssid"

.field public static final EXTRA_HS20_RC_IND:Ljava/lang/String; = "roamingInd"

.field public static final EXTRA_HS20_SSID:Ljava/lang/String; = "ssid"

.field public static final EXTRA_LINK_CAPABILITIES:Ljava/lang/String; = "linkCapabilities"

.field public static final EXTRA_LINK_PROPERTIES:Ljava/lang/String; = "linkProperties"

.field public static final EXTRA_MULTIPLE_NETWORKS_CHANGED:Ljava/lang/String; = "multipleChanges"

.field public static final EXTRA_NETWORK_INFO:Ljava/lang/String; = "networkInfo"

.field public static final EXTRA_NEW_RSSI:Ljava/lang/String; = "newRssi"

.field public static final EXTRA_NEW_STATE:Ljava/lang/String; = "newState"

.field public static final EXTRA_PREVIOUS_WIFI_AP_STATE:Ljava/lang/String; = "previous_wifi_state"

.field public static final EXTRA_PREVIOUS_WIFI_STATE:Ljava/lang/String; = "previous_wifi_state"

.field public static final EXTRA_SUPPLICANT_CONNECTED:Ljava/lang/String; = "connected"

.field public static final EXTRA_SUPPLICANT_ERROR:Ljava/lang/String; = "supplicantError"

.field public static final EXTRA_WIFI_AP_STATE:Ljava/lang/String; = "wifi_state"

.field public static final EXTRA_WIFI_CONFIGURATION:Ljava/lang/String; = "wifiConfiguration"

.field public static final EXTRA_WIFI_INFO:Ljava/lang/String; = "wifiInfo"

.field public static final EXTRA_WIFI_STATE:Ljava/lang/String; = "wifi_state"

.field public static final FORGET_NETWORK:I = 0x25004

.field public static final FORGET_NETWORK_FAILED:I = 0x25005

.field public static final FORGET_NETWORK_SUCCEEDED:I = 0x25006

.field private static final INVALID_KEY:I = 0x0

.field public static final IN_PROGRESS:I = 0x1

.field public static final LINK_CONFIGURATION_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.LINK_CONFIGURATION_CHANGED"

.field private static final MAX_ACTIVE_LOCKS:I = 0x32

.field private static final MAX_RSSI:I = -0x37

.field private static final MIN_RSSI:I = -0x64

.field public static final NETWORK_IDS_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.NETWORK_IDS_CHANGED"

.field public static final NETWORK_STATE_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.STATE_CHANGE"

.field public static final RSSI_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.RSSI_CHANGED"

.field public static final RSSI_LEVELS:I = 0x5

.field public static final RSSI_PKTCNT_FETCH:I = 0x25014

.field public static final RSSI_PKTCNT_FETCH_FAILED:I = 0x25016

.field public static final RSSI_PKTCNT_FETCH_SUCCEEDED:I = 0x25015

.field public static final SAVE_NETWORK:I = 0x25007

.field public static final SAVE_NETWORK_FAILED:I = 0x25008

.field public static final SAVE_NETWORK_SUCCEEDED:I = 0x25009

.field public static final SCAN_RESULTS_AVAILABLE_ACTION:Ljava/lang/String; = "android.net.wifi.SCAN_RESULTS"

.field public static final START_WPS:I = 0x2500a

.field public static final START_WPS_SUCCEEDED:I = 0x2500b

.field public static final SUPPLICANT_CONNECTION_CHANGE_ACTION:Ljava/lang/String; = "android.net.wifi.supplicant.CONNECTION_CHANGE"

.field public static final SUPPLICANT_STATE_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.supplicant.STATE_CHANGE"

.field private static final TAG:Ljava/lang/String; = "WifiManager"

.field public static final TRAFFIC_STATS_POLL:I = 0x25020

.field public static final WIFI_AP_STATE_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.WIFI_AP_STATE_CHANGED"

.field public static final WIFI_AP_STATE_DISABLED:I = 0xb

.field public static final WIFI_AP_STATE_DISABLING:I = 0xa

.field public static final WIFI_AP_STATE_ENABLED:I = 0xd

.field public static final WIFI_AP_STATE_ENABLING:I = 0xc

.field public static final WIFI_AP_STATE_FAILED:I = 0xe

.field public static final WIFI_FREQUENCY_BAND_2GHZ:I = 0x2

.field public static final WIFI_FREQUENCY_BAND_5GHZ:I = 0x1

.field public static final WIFI_FREQUENCY_BAND_AUTO:I = 0x0

.field public static final WIFI_MODE_FULL:I = 0x1

.field public static final WIFI_MODE_FULL_HIGH_PERF:I = 0x3

.field public static final WIFI_MODE_SCAN_ONLY:I = 0x2

.field public static final WIFI_STATE_CHANGED_ACTION:Ljava/lang/String; = "android.net.wifi.WIFI_STATE_CHANGED"

.field public static final WIFI_STATE_DISABLED:I = 0x1

.field public static final WIFI_STATE_DISABLING:I = 0x0

.field public static final WIFI_STATE_ENABLED:I = 0x3

.field public static final WIFI_STATE_ENABLING:I = 0x2

.field public static final WIFI_STATE_UNKNOWN:I = 0x4

.field public static final WPS_AUTH_FAILURE:I = 0x6

.field public static final WPS_COMPLETED:I = 0x2500d

.field public static final WPS_FAILED:I = 0x2500c

.field public static final WPS_OVERLAP_ERROR:I = 0x3

.field public static final WPS_TIMED_OUT:I = 0x7

.field public static final WPS_TKIP_ONLY_PROHIBITED:I = 0x5

.field public static final WPS_WEP_PROHIBITED:I = 0x4

.field private static sHandlerThread:Landroid/os/HandlerThread;

.field private static sThreadRefCount:I

.field private static sThreadRefLock:Ljava/lang/Object;


# instance fields
.field private mActiveLockCount:I

.field private mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

.field private final mConnected:Ljava/util/concurrent/CountDownLatch;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/net/wifi/WifiManager$ServiceHandler;

.field private mListenerKey:I

.field private final mListenerMap:Landroid/util/SparseArray;

.field private final mListenerMapLock:Ljava/lang/Object;

.field mService:Landroid/net/wifi/IWifiManager;

.field private mWifiServiceMessenger:Landroid/os/Messenger;

.field private mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 551
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/WifiManager;->sThreadRefLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/IWifiManager;)V
    .registers 5
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 575
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 542
    iput v1, p0, Landroid/net/wifi/WifiManager;->mListenerKey:I

    #@6
    .line 543
    new-instance v0, Landroid/util/SparseArray;

    #@8
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@b
    iput-object v0, p0, Landroid/net/wifi/WifiManager;->mListenerMap:Landroid/util/SparseArray;

    #@d
    .line 544
    new-instance v0, Ljava/lang/Object;

    #@f
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@12
    iput-object v0, p0, Landroid/net/wifi/WifiManager;->mListenerMapLock:Ljava/lang/Object;

    #@14
    .line 546
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@16
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@19
    iput-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@1b
    .line 549
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@1d
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@20
    iput-object v0, p0, Landroid/net/wifi/WifiManager;->mConnected:Ljava/util/concurrent/CountDownLatch;

    #@22
    .line 576
    iput-object p1, p0, Landroid/net/wifi/WifiManager;->mContext:Landroid/content/Context;

    #@24
    .line 577
    iput-object p2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@26
    .line 578
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->init()V

    #@29
    .line 579
    return-void
.end method

.method static synthetic access$000(Landroid/net/wifi/WifiManager;I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiManager;->removeListener(I)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/wifi/WifiManager;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/net/wifi/WifiManager;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/net/wifi/WifiManager;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mConnected:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/wifi/WifiManager;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mListenerMapLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/net/wifi/WifiManager;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mListenerMap:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/net/wifi/WifiManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/net/wifi/WifiManager;->mActiveLockCount:I

    #@2
    return v0
.end method

.method static synthetic access$508(Landroid/net/wifi/WifiManager;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/net/wifi/WifiManager;->mActiveLockCount:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Landroid/net/wifi/WifiManager;->mActiveLockCount:I

    #@6
    return v0
.end method

.method static synthetic access$510(Landroid/net/wifi/WifiManager;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/net/wifi/WifiManager;->mActiveLockCount:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Landroid/net/wifi/WifiManager;->mActiveLockCount:I

    #@6
    return v0
.end method

.method private addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 662
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 664
    :goto_6
    return v1

    #@7
    .line 663
    :catch_7
    move-exception v0

    #@8
    .line 664
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@9
    goto :goto_6
.end method

.method public static calculateSignalLevel(II)I
    .registers 5
    .parameter "rssi"
    .parameter "numLevels"

    #@0
    .prologue
    .line 985
    const/16 v2, -0x64

    #@2
    if-gt p0, v2, :cond_6

    #@4
    .line 986
    const/4 v2, 0x0

    #@5
    .line 992
    :goto_5
    return v2

    #@6
    .line 987
    :cond_6
    const/16 v2, -0x37

    #@8
    if-lt p0, v2, :cond_d

    #@a
    .line 988
    add-int/lit8 v2, p1, -0x1

    #@c
    goto :goto_5

    #@d
    .line 990
    :cond_d
    const/high16 v0, 0x4234

    #@f
    .line 991
    .local v0, inputRange:F
    add-int/lit8 v2, p1, -0x1

    #@11
    int-to-float v1, v2

    #@12
    .line 992
    .local v1, outputRange:F
    add-int/lit8 v2, p0, 0x64

    #@14
    int-to-float v2, v2

    #@15
    mul-float/2addr v2, v1

    #@16
    div-float/2addr v2, v0

    #@17
    float-to-int v2, v2

    #@18
    goto :goto_5
.end method

.method public static compareSignalLevel(II)I
    .registers 3
    .parameter "rssiA"
    .parameter "rssiB"

    #@0
    .prologue
    .line 1006
    sub-int v0, p0, p1

    #@2
    return v0
.end method

.method private init()V
    .registers 6

    #@0
    .prologue
    .line 1416
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getWifiServiceMessenger()Landroid/os/Messenger;

    #@3
    move-result-object v1

    #@4
    iput-object v1, p0, Landroid/net/wifi/WifiManager;->mWifiServiceMessenger:Landroid/os/Messenger;

    #@6
    .line 1417
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mWifiServiceMessenger:Landroid/os/Messenger;

    #@8
    if-nez v1, :cond_e

    #@a
    .line 1418
    const/4 v1, 0x0

    #@b
    iput-object v1, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@d
    .line 1439
    :goto_d
    return-void

    #@e
    .line 1422
    :cond_e
    sget-object v2, Landroid/net/wifi/WifiManager;->sThreadRefLock:Ljava/lang/Object;

    #@10
    monitor-enter v2

    #@11
    .line 1423
    :try_start_11
    sget v1, Landroid/net/wifi/WifiManager;->sThreadRefCount:I

    #@13
    add-int/lit8 v1, v1, 0x1

    #@15
    sput v1, Landroid/net/wifi/WifiManager;->sThreadRefCount:I

    #@17
    const/4 v3, 0x1

    #@18
    if-ne v1, v3, :cond_28

    #@1a
    .line 1424
    new-instance v1, Landroid/os/HandlerThread;

    #@1c
    const-string v3, "WifiManager"

    #@1e
    invoke-direct {v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@21
    sput-object v1, Landroid/net/wifi/WifiManager;->sHandlerThread:Landroid/os/HandlerThread;

    #@23
    .line 1425
    sget-object v1, Landroid/net/wifi/WifiManager;->sHandlerThread:Landroid/os/HandlerThread;

    #@25
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@28
    .line 1427
    :cond_28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_11 .. :try_end_29} :catchall_4e

    #@29
    .line 1429
    new-instance v1, Landroid/net/wifi/WifiManager$ServiceHandler;

    #@2b
    sget-object v2, Landroid/net/wifi/WifiManager;->sHandlerThread:Landroid/os/HandlerThread;

    #@2d
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@30
    move-result-object v2

    #@31
    invoke-direct {v1, p0, v2}, Landroid/net/wifi/WifiManager$ServiceHandler;-><init>(Landroid/net/wifi/WifiManager;Landroid/os/Looper;)V

    #@34
    iput-object v1, p0, Landroid/net/wifi/WifiManager;->mHandler:Landroid/net/wifi/WifiManager$ServiceHandler;

    #@36
    .line 1430
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@38
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mContext:Landroid/content/Context;

    #@3a
    iget-object v3, p0, Landroid/net/wifi/WifiManager;->mHandler:Landroid/net/wifi/WifiManager$ServiceHandler;

    #@3c
    iget-object v4, p0, Landroid/net/wifi/WifiManager;->mWifiServiceMessenger:Landroid/os/Messenger;

    #@3e
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@41
    .line 1432
    :try_start_41
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mConnected:Ljava/util/concurrent/CountDownLatch;

    #@43
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_46
    .catch Ljava/lang/InterruptedException; {:try_start_41 .. :try_end_46} :catch_51

    #@46
    .line 1438
    :goto_46
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration;

    #@48
    invoke-direct {v1}, Landroid/net/wifi/WifiVZWConfiguration;-><init>()V

    #@4b
    iput-object v1, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@4d
    goto :goto_d

    #@4e
    .line 1427
    :catchall_4e
    move-exception v1

    #@4f
    :try_start_4f
    monitor-exit v2
    :try_end_50
    .catchall {:try_start_4f .. :try_end_50} :catchall_4e

    #@50
    throw v1

    #@51
    .line 1433
    :catch_51
    move-exception v0

    #@52
    .line 1434
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "WifiManager"

    #@54
    const-string v2, "interrupted wait at init"

    #@56
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_46
.end method

.method private putListener(Ljava/lang/Object;)I
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 1395
    if-nez p1, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 1403
    :goto_3
    return v0

    #@4
    .line 1397
    :cond_4
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mListenerMapLock:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 1399
    :cond_7
    :try_start_7
    iget v0, p0, Landroid/net/wifi/WifiManager;->mListenerKey:I

    #@9
    add-int/lit8 v1, v0, 0x1

    #@b
    iput v1, p0, Landroid/net/wifi/WifiManager;->mListenerKey:I

    #@d
    .line 1400
    .local v0, key:I
    if-eqz v0, :cond_7

    #@f
    .line 1401
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mListenerMap:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 1402
    monitor-exit v2

    #@15
    goto :goto_3

    #@16
    .end local v0           #key:I
    :catchall_16
    move-exception v1

    #@17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method

.method private removeListener(I)Ljava/lang/Object;
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 1407
    if-nez p1, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 1411
    :goto_3
    return-object v0

    #@4
    .line 1408
    :cond_4
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mListenerMapLock:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 1409
    :try_start_7
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mListenerMap:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    .line 1410
    .local v0, listener:Ljava/lang/Object;
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mListenerMap:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@12
    .line 1411
    monitor-exit v2

    #@13
    goto :goto_3

    #@14
    .line 1412
    .end local v0           #listener:Ljava/lang/Object;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method private validateChannel()V
    .registers 3

    #@0
    .prologue
    .line 1442
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    if-nez v0, :cond_c

    #@4
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No permission to access and change wifi or a bad initialization"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1444
    :cond_c
    return-void
.end method


# virtual methods
.method public addNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 3
    .parameter "config"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 623
    if-nez p1, :cond_4

    #@3
    .line 627
    :goto_3
    return v0

    #@4
    .line 626
    :cond_4
    iput v0, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@6
    .line 627
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiManager;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@9
    move-result v0

    #@a
    goto :goto_3
.end method

.method public addToBlacklist(Ljava/lang/String;)Z
    .registers 4
    .parameter "bssid"

    #@0
    .prologue
    .line 1139
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->addToBlacklist(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1140
    const/4 v1, 0x1

    #@6
    .line 1142
    :goto_6
    return v1

    #@7
    .line 1141
    :catch_7
    move-exception v0

    #@8
    .line 1142
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public cancelWps(Landroid/net/wifi/WifiManager$ActionListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 1572
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@3
    .line 1573
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@5
    const v1, 0x2500e

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@c
    move-result v3

    #@d
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@10
    .line 1574
    return-void
.end method

.method public captivePortalCheckComplete()V
    .registers 2

    #@0
    .prologue
    .line 2042
    :try_start_0
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v0}, Landroid/net/wifi/IWifiManager;->captivePortalCheckComplete()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 2044
    :goto_5
    return-void

    #@6
    .line 2043
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public checkAndStartWifiExt()V
    .registers 2

    #@0
    .prologue
    .line 2068
    :try_start_0
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v0}, Landroid/net/wifi/IWifiManager;->checkAndStartWifiExt()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 2072
    :goto_5
    return-void

    #@6
    .line 2070
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public clearBlacklist()Z
    .registers 3

    #@0
    .prologue
    .line 1156
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->clearBlacklist()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1157
    const/4 v1, 0x1

    #@6
    .line 1159
    :goto_6
    return v1

    #@7
    .line 1158
    :catch_7
    move-exception v0

    #@8
    .line 1159
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public connect(ILandroid/net/wifi/WifiManager$ActionListener;)V
    .registers 6
    .parameter "networkId"
    .parameter "listener"

    #@0
    .prologue
    .line 1485
    if-gez p1, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Network id cannot be negative"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1486
    :cond_a
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@d
    .line 1487
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    const v1, 0x25001

    #@12
    invoke-direct {p0, p2}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@15
    move-result v2

    #@16
    invoke-virtual {v0, v1, p1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@19
    .line 1488
    return-void
.end method

.method public connect(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V
    .registers 7
    .parameter "config"
    .parameter "listener"

    #@0
    .prologue
    .line 1463
    if-nez p1, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "config cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1464
    :cond_a
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@d
    .line 1467
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    const v1, 0x25001

    #@12
    const/4 v2, -0x1

    #@13
    invoke-direct {p0, p2}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@16
    move-result v3

    #@17
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@1a
    .line 1469
    return-void
.end method

.method public createMulticastLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    .line 1850
    new-instance v0, Landroid/net/wifi/WifiManager$MulticastLock;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, p1, v1}, Landroid/net/wifi/WifiManager$MulticastLock;-><init>(Landroid/net/wifi/WifiManager;Ljava/lang/String;Landroid/net/wifi/WifiManager$1;)V

    #@6
    return-object v0
.end method

.method public createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;
    .registers 5
    .parameter "lockType"
    .parameter "tag"

    #@0
    .prologue
    .line 1816
    new-instance v0, Landroid/net/wifi/WifiManager$WifiLock;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, p1, p2, v1}, Landroid/net/wifi/WifiManager$WifiLock;-><init>(Landroid/net/wifi/WifiManager;ILjava/lang/String;Landroid/net/wifi/WifiManager$1;)V

    #@6
    return-object v0
.end method

.method public createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;
    .registers 5
    .parameter "tag"

    #@0
    .prologue
    .line 1832
    new-instance v0, Landroid/net/wifi/WifiManager$WifiLock;

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, p0, v1, p1, v2}, Landroid/net/wifi/WifiManager$WifiLock;-><init>(Landroid/net/wifi/WifiManager;ILjava/lang/String;Landroid/net/wifi/WifiManager$1;)V

    #@7
    return-object v0
.end method

.method public disable(ILandroid/net/wifi/WifiManager$ActionListener;)V
    .registers 6
    .parameter "netId"
    .parameter "listener"

    #@0
    .prologue
    .line 1543
    if-gez p1, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Network id cannot be negative"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1544
    :cond_a
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@d
    .line 1545
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    const v1, 0x25011

    #@12
    invoke-direct {p0, p2}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@15
    move-result v2

    #@16
    invoke-virtual {v0, v1, p1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@19
    .line 1546
    return-void
.end method

.method public disableNetwork(I)Z
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 713
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->disableNetwork(I)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 715
    :goto_6
    return v1

    #@7
    .line 714
    :catch_7
    move-exception v0

    #@8
    .line 715
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public disconnect()Z
    .registers 3

    #@0
    .prologue
    .line 726
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->disconnect()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 727
    const/4 v1, 0x1

    #@6
    .line 729
    :goto_6
    return v1

    #@7
    .line 728
    :catch_7
    move-exception v0

    #@8
    .line 729
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public enableNetwork(IZ)Z
    .registers 5
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    .line 698
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/net/wifi/IWifiManager;->enableNetwork(IZ)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 700
    :goto_6
    return v1

    #@7
    .line 699
    :catch_7
    move-exception v0

    #@8
    .line 700
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 2048
    :try_start_0
    sget-object v1, Landroid/net/wifi/WifiManager;->sThreadRefLock:Ljava/lang/Object;

    #@2
    monitor-enter v1
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_20

    #@3
    .line 2049
    :try_start_3
    sget v0, Landroid/net/wifi/WifiManager;->sThreadRefCount:I

    #@5
    add-int/lit8 v0, v0, -0x1

    #@7
    sput v0, Landroid/net/wifi/WifiManager;->sThreadRefCount:I

    #@9
    if-nez v0, :cond_18

    #@b
    sget-object v0, Landroid/net/wifi/WifiManager;->sHandlerThread:Landroid/os/HandlerThread;

    #@d
    if-eqz v0, :cond_18

    #@f
    .line 2050
    sget-object v0, Landroid/net/wifi/WifiManager;->sHandlerThread:Landroid/os/HandlerThread;

    #@11
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@18
    .line 2052
    :cond_18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1d

    #@19
    .line 2054
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@1c
    .line 2056
    return-void

    #@1d
    .line 2052
    :catchall_1d
    move-exception v0

    #@1e
    :try_start_1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    :try_start_1f
    throw v0
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_20

    #@20
    .line 2054
    :catchall_20
    move-exception v0

    #@21
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@24
    throw v0
.end method

.method public forget(ILandroid/net/wifi/WifiManager$ActionListener;)V
    .registers 6
    .parameter "netId"
    .parameter "listener"

    #@0
    .prologue
    .line 1528
    if-gez p1, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Network id cannot be negative"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1529
    :cond_a
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@d
    .line 1530
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    const v1, 0x25004

    #@12
    invoke-direct {p0, p2}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@15
    move-result v2

    #@16
    invoke-virtual {v0, v1, p1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@19
    .line 1531
    return-void
.end method

.method public getConfigFile()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1612
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getConfigFile()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 1614
    :goto_6
    return-object v1

    #@7
    .line 1613
    :catch_7
    move-exception v0

    #@8
    .line 1614
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getConfiguredNetworks()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 602
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getConfiguredNetworks()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 604
    :goto_6
    return-object v1

    #@7
    .line 603
    :catch_7
    move-exception v0

    #@8
    .line 604
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getConnectionInfo()Landroid/net/wifi/WifiInfo;
    .registers 3

    #@0
    .prologue
    .line 818
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 820
    :goto_6
    return-object v1

    #@7
    .line 819
    :catch_7
    move-exception v0

    #@8
    .line 820
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getDhcpInfo()Landroid/net/DhcpInfo;
    .registers 3

    #@0
    .prologue
    .line 918
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 920
    :goto_6
    return-object v1

    #@7
    .line 919
    :catch_7
    move-exception v0

    #@8
    .line 920
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getFrequencyBand()I
    .registers 3

    #@0
    .prologue
    .line 892
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getFrequencyBand()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 894
    :goto_6
    return v1

    #@7
    .line 893
    :catch_7
    move-exception v0

    #@8
    .line 894
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@9
    goto :goto_6
.end method

.method public getScanResults()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 830
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getScanResults()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 832
    :goto_6
    return-object v1

    #@7
    .line 831
    :catch_7
    move-exception v0

    #@8
    .line 832
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getTxPacketCount(Landroid/net/wifi/WifiManager$TxPacketCountListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 970
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@3
    .line 971
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@5
    const v1, 0x25014

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@c
    move-result v3

    #@d
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@10
    .line 972
    return-void
.end method

.method public getVZWMobileHotspotSSID()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2243
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getVZWMobileHotspotSSID()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 2245
    :goto_6
    return-object v1

    #@7
    .line 2244
    :catch_7
    move-exception v0

    #@8
    .line 2245
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;
    .registers 3

    #@0
    .prologue
    .line 1066
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 1068
    :goto_6
    return-object v1

    #@7
    .line 1067
    :catch_7
    move-exception v0

    #@8
    .line 1068
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getWifiApState()I
    .registers 3

    #@0
    .prologue
    .line 1041
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getWifiApEnabledState()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1043
    :goto_6
    return v1

    #@7
    .line 1042
    :catch_7
    move-exception v0

    #@8
    .line 1043
    .local v0, e:Landroid/os/RemoteException;
    const/16 v1, 0xe

    #@a
    goto :goto_6
.end method

.method public getWifiNeedOn()Z
    .registers 3

    #@0
    .prologue
    .line 2198
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getWifiNeedOn()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 2200
    :goto_6
    return v1

    #@7
    .line 2199
    :catch_7
    move-exception v0

    #@8
    .line 2200
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getWifiServiceMessenger()Landroid/os/Messenger;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1585
    :try_start_1
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@3
    invoke-interface {v2}, Landroid/net/wifi/IWifiManager;->getWifiServiceMessenger()Landroid/os/Messenger;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_8
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_6} :catch_a

    #@6
    move-result-object v1

    #@7
    .line 1589
    :goto_7
    return-object v1

    #@8
    .line 1586
    :catch_8
    move-exception v0

    #@9
    .line 1587
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_7

    #@a
    .line 1588
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_a
    move-exception v0

    #@b
    .line 1589
    .local v0, e:Ljava/lang/SecurityException;
    goto :goto_7
.end method

.method public getWifiState()I
    .registers 3

    #@0
    .prologue
    .line 948
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getWifiEnabledState()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 950
    :goto_6
    return v1

    #@7
    .line 949
    :catch_7
    move-exception v0

    #@8
    .line 950
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x4

    #@9
    goto :goto_6
.end method

.method public getWifiStateMachineMessenger()Landroid/os/Messenger;
    .registers 3

    #@0
    .prologue
    .line 1600
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getWifiStateMachineMessenger()Landroid/os/Messenger;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 1602
    :goto_6
    return-object v1

    #@7
    .line 1601
    :catch_7
    move-exception v0

    #@8
    .line 1602
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getWifiVZWApConfiguration()Landroid/net/wifi/WifiVZWConfiguration;
    .registers 3

    #@0
    .prologue
    .line 2128
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->getWifiVZWApConfiguration()Landroid/net/wifi/WifiVZWConfiguration;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 2130
    :goto_6
    return-object v1

    #@7
    .line 2129
    :catch_7
    move-exception v0

    #@8
    .line 2130
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public initializeMulticastFiltering()Z
    .registers 3

    #@0
    .prologue
    .line 2032
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->initializeMulticastFiltering()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 2033
    const/4 v1, 0x1

    #@6
    .line 2035
    :goto_6
    return v1

    #@7
    .line 2034
    :catch_7
    move-exception v0

    #@8
    .line 2035
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isDualBandSupported()Z
    .registers 3

    #@0
    .prologue
    .line 905
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isDualBandSupported()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 907
    :goto_6
    return v1

    #@7
    .line 906
    :catch_7
    move-exception v0

    #@8
    .line 907
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isMulticastEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 2020
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isMulticastEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 2022
    :goto_6
    return v1

    #@7
    .line 2021
    :catch_7
    move-exception v0

    #@8
    .line 2022
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isVZWMobileHotspotEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 2217
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isVZWMobileHotspotEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 2219
    :goto_6
    return v1

    #@7
    .line 2218
    :catch_7
    move-exception v0

    #@8
    .line 2219
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isWifiApEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1055
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0xd

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isWifiEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 960
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x3

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public pingSupplicant()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 769
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 774
    :goto_5
    return v1

    #@6
    .line 772
    :cond_6
    :try_start_6
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@8
    invoke-interface {v2}, Landroid/net/wifi/IWifiManager;->pingSupplicant()Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    goto :goto_5

    #@d
    .line 773
    :catch_d
    move-exception v0

    #@e
    .line 774
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_5
.end method

.method public reassociate()Z
    .registers 3

    #@0
    .prologue
    .line 756
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->reassociate()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 757
    const/4 v1, 0x1

    #@6
    .line 759
    :goto_6
    return v1

    #@7
    .line 758
    :catch_7
    move-exception v0

    #@8
    .line 759
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public reconnect()Z
    .registers 3

    #@0
    .prologue
    .line 741
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->reconnect()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 742
    const/4 v1, 0x1

    #@6
    .line 744
    :goto_6
    return v1

    #@7
    .line 743
    :catch_7
    move-exception v0

    #@8
    .line 744
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public removeNetwork(I)Z
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 678
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->removeNetwork(I)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 680
    :goto_6
    return v1

    #@7
    .line 679
    :catch_7
    move-exception v0

    #@8
    .line 680
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V
    .registers 7
    .parameter "config"
    .parameter "listener"

    #@0
    .prologue
    .line 1509
    if-nez p1, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "config cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1510
    :cond_a
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@d
    .line 1511
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    const v1, 0x25007

    #@12
    const/4 v2, 0x0

    #@13
    invoke-direct {p0, p2}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@16
    move-result v3

    #@17
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@1a
    .line 1512
    return-void
.end method

.method public saveConfiguration()Z
    .registers 3

    #@0
    .prologue
    .line 847
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->saveConfiguration()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 849
    :goto_6
    return v1

    #@7
    .line 848
    :catch_7
    move-exception v0

    #@8
    .line 849
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setCountryCode(Ljava/lang/String;Z)V
    .registers 4
    .parameter "country"
    .parameter "persist"

    #@0
    .prologue
    .line 862
    :try_start_0
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/net/wifi/IWifiManager;->setCountryCode(Ljava/lang/String;Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 864
    :goto_5
    return-void

    #@6
    .line 863
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setFrequencyBand(IZ)V
    .registers 4
    .parameter "band"
    .parameter "persist"

    #@0
    .prologue
    .line 877
    :try_start_0
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/net/wifi/IWifiManager;->setFrequencyBand(IZ)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 879
    :goto_5
    return-void

    #@6
    .line 878
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setVZWMobileHotspot(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 2228
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->setVZWMobileHotspot(Z)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 2230
    :goto_6
    return v1

    #@7
    .line 2229
    :catch_7
    move-exception v0

    #@8
    .line 2230
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z
    .registers 4
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 1080
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1081
    const/4 v1, 0x1

    #@6
    .line 1083
    :goto_6
    return v1

    #@7
    .line 1082
    :catch_7
    move-exception v0

    #@8
    .line 1083
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z
    .registers 5
    .parameter "wifiConfig"
    .parameter "enabled"

    #@0
    .prologue
    .line 1023
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/net/wifi/IWifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1024
    const/4 v1, 0x1

    #@6
    .line 1026
    :goto_6
    return v1

    #@7
    .line 1025
    :catch_7
    move-exception v0

    #@8
    .line 1026
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setWifiEnabled(Z)Z
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 933
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->setWifiEnabled(Z)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 935
    :goto_6
    return v1

    #@7
    .line 934
    :catch_7
    move-exception v0

    #@8
    .line 935
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setWifiNeedOn(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 2187
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/wifi/IWifiManager;->setWifiNeedOn(Z)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 2189
    :goto_6
    return v1

    #@7
    .line 2188
    :catch_7
    move-exception v0

    #@8
    .line 2189
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setWifiVZWApConfiguration(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)Z
    .registers 23
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"

    #@0
    .prologue
    .line 2146
    :try_start_0
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@2
    iput-object p1, v2, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@4
    .line 2147
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@6
    invoke-virtual {v2, p2}, Landroid/net/wifi/WifiVZWConfiguration;->setAuthType(I)V

    #@9
    .line 2148
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@b
    iput p3, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@d
    .line 2149
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@f
    iput-object p4, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@11
    .line 2150
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@13
    iput-object p5, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@15
    .line 2151
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@17
    iput-object p6, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@19
    .line 2152
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@1b
    iput-object p7, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@1d
    .line 2153
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@1f
    iput-object p8, v2, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@21
    .line 2154
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@23
    iput-boolean p9, v2, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@25
    .line 2155
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@27
    iput p10, v2, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@29
    .line 2156
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@2b
    iput p11, v2, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@2d
    .line 2157
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@2f
    move-object/from16 v0, p12

    #@31
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@33
    .line 2158
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@35
    move-object/from16 v0, p13

    #@37
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@39
    .line 2159
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@3b
    move/from16 v0, p14

    #@3d
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@3f
    .line 2160
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@41
    move/from16 v0, p15

    #@43
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@45
    .line 2161
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@47
    move/from16 v0, p16

    #@49
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@4b
    .line 2162
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@4d
    move-object/from16 v0, p17

    #@4f
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@51
    .line 2163
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@53
    move-object/from16 v0, p18

    #@55
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@57
    .line 2165
    const-string v2, "WifiManager"

    #@59
    const-string/jumbo v3, "setWifiVZWApEnabledMethod "

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 2166
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@61
    iget-object v3, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@63
    invoke-interface {v2, v3}, Landroid/net/wifi/IWifiManager;->setWifiVZWApConfiguration(Landroid/net/wifi/WifiVZWConfiguration;)V
    :try_end_66
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_66} :catch_68

    #@66
    .line 2167
    const/4 v2, 0x1

    #@67
    .line 2169
    :goto_67
    return v2

    #@68
    .line 2168
    :catch_68
    move-exception v1

    #@69
    .line 2169
    .local v1, e:Landroid/os/RemoteException;
    const/4 v2, 0x0

    #@6a
    goto :goto_67
.end method

.method public setWifiVZWApEnabled(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Z)Z
    .registers 25
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"
    .parameter "enabled"

    #@0
    .prologue
    .line 2092
    :try_start_0
    const-string v2, "WifiManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "setWifiVZWApEnabled : "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    move/from16 v0, p19

    #@10
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2093
    if-eqz p19, :cond_76

    #@1d
    .line 2094
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@1f
    iput-object p1, v2, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@21
    .line 2095
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@23
    invoke-virtual {v2, p2}, Landroid/net/wifi/WifiVZWConfiguration;->setAuthType(I)V

    #@26
    .line 2096
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@28
    iput p3, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@2a
    .line 2097
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@2c
    iput-object p4, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@2e
    .line 2098
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@30
    iput-object p5, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@32
    .line 2099
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@34
    iput-object p6, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@36
    .line 2100
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@38
    iput-object p7, v2, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@3a
    .line 2101
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@3c
    iput-object p8, v2, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@3e
    .line 2102
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@40
    iput-boolean p9, v2, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@42
    .line 2103
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@44
    iput p10, v2, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@46
    .line 2104
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@48
    move/from16 v0, p11

    #@4a
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@4c
    .line 2105
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@4e
    move-object/from16 v0, p12

    #@50
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@52
    .line 2106
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@54
    move-object/from16 v0, p13

    #@56
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@58
    .line 2107
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@5a
    move/from16 v0, p14

    #@5c
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@5e
    .line 2108
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@60
    move/from16 v0, p15

    #@62
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@64
    .line 2109
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@66
    move/from16 v0, p16

    #@68
    iput v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@6a
    .line 2110
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@6c
    move-object/from16 v0, p17

    #@6e
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@70
    .line 2111
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@72
    move-object/from16 v0, p18

    #@74
    iput-object v0, v2, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@76
    .line 2113
    :cond_76
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@78
    iget-object v3, p0, Landroid/net/wifi/WifiManager;->mwifivzwconfig:Landroid/net/wifi/WifiVZWConfiguration;

    #@7a
    move/from16 v0, p19

    #@7c
    invoke-interface {v2, v3, v0}, Landroid/net/wifi/IWifiManager;->setWifiVZWApEnabled(Landroid/net/wifi/WifiVZWConfiguration;Z)V
    :try_end_7f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7f} :catch_81

    #@7f
    .line 2114
    const/4 v2, 0x1

    #@80
    .line 2116
    :goto_80
    return v2

    #@81
    .line 2115
    :catch_81
    move-exception v1

    #@82
    .line 2116
    .local v1, e:Landroid/os/RemoteException;
    const/4 v2, 0x0

    #@83
    goto :goto_80
.end method

.method public startScan()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 786
    :try_start_1
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@3
    const/4 v3, 0x0

    #@4
    invoke-interface {v2, v3}, Landroid/net/wifi/IWifiManager;->startScan(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_7} :catch_9

    #@7
    .line 787
    const/4 v1, 0x1

    #@8
    .line 789
    :goto_8
    return v1

    #@9
    .line 788
    :catch_9
    move-exception v0

    #@a
    .line 789
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_8
.end method

.method public startScanActive()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 805
    :try_start_1
    iget-object v2, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@3
    const/4 v3, 0x1

    #@4
    invoke-interface {v2, v3}, Landroid/net/wifi/IWifiManager;->startScan(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_7} :catch_8

    #@7
    .line 808
    :goto_7
    return v1

    #@8
    .line 807
    :catch_8
    move-exception v0

    #@9
    .line 808
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@a
    goto :goto_7
.end method

.method public startWifi()Z
    .registers 3

    #@0
    .prologue
    .line 1102
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->startWifi()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1103
    const/4 v1, 0x1

    #@6
    .line 1105
    :goto_6
    return v1

    #@7
    .line 1104
    :catch_7
    move-exception v0

    #@8
    .line 1105
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public startWps(Landroid/net/wifi/WpsInfo;Landroid/net/wifi/WifiManager$WpsListener;)V
    .registers 7
    .parameter "config"
    .parameter "listener"

    #@0
    .prologue
    .line 1558
    if-nez p1, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "config cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1559
    :cond_a
    invoke-direct {p0}, Landroid/net/wifi/WifiManager;->validateChannel()V

    #@d
    .line 1560
    iget-object v0, p0, Landroid/net/wifi/WifiManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    const v1, 0x2500a

    #@12
    const/4 v2, 0x0

    #@13
    invoke-direct {p0, p2}, Landroid/net/wifi/WifiManager;->putListener(Ljava/lang/Object;)I

    #@16
    move-result v3

    #@17
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@1a
    .line 1561
    return-void
.end method

.method public stopWifi()Z
    .registers 3

    #@0
    .prologue
    .line 1122
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2
    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->stopWifi()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1123
    const/4 v1, 0x1

    #@6
    .line 1125
    :goto_6
    return v1

    #@7
    .line 1124
    :catch_7
    move-exception v0

    #@8
    .line 1125
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public updateNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 646
    if-eqz p1, :cond_6

    #@2
    iget v0, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@4
    if-gez v0, :cond_8

    #@6
    .line 647
    :cond_6
    const/4 v0, -0x1

    #@7
    .line 649
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiManager;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method
