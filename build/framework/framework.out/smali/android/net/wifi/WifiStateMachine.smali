.class public Landroid/net/wifi/WifiStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;,
        Landroid/net/wifi/WifiStateMachine$TetheredState;,
        Landroid/net/wifi/WifiStateMachine$TetheringState;,
        Landroid/net/wifi/WifiStateMachine$SoftApStartedState;,
        Landroid/net/wifi/WifiStateMachine$SoftApStartingState;,
        Landroid/net/wifi/WifiStateMachine$WpsRunningState;,
        Landroid/net/wifi/WifiStateMachine$DisconnectedState;,
        Landroid/net/wifi/WifiStateMachine$DisconnectingState;,
        Landroid/net/wifi/WifiStateMachine$ConnectedState;,
        Landroid/net/wifi/WifiStateMachine$CaptivePortalCheckState;,
        Landroid/net/wifi/WifiStateMachine$VerifyingLinkState;,
        Landroid/net/wifi/WifiStateMachine$ObtainingIpState;,
        Landroid/net/wifi/WifiStateMachine$L2ConnectedState;,
        Landroid/net/wifi/WifiStateMachine$ConnectModeState;,
        Landroid/net/wifi/WifiStateMachine$ScanModeState;,
        Landroid/net/wifi/WifiStateMachine$DriverStoppedState;,
        Landroid/net/wifi/WifiStateMachine$DriverStoppingState;,
        Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;,
        Landroid/net/wifi/WifiStateMachine$DriverStartedState;,
        Landroid/net/wifi/WifiStateMachine$DriverStartedStateExt;,
        Landroid/net/wifi/WifiStateMachine$DriverStartingState;,
        Landroid/net/wifi/WifiStateMachine$SupplicantStoppingState;,
        Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;,
        Landroid/net/wifi/WifiStateMachine$SupplicantStartingState;,
        Landroid/net/wifi/WifiStateMachine$DriverFailedState;,
        Landroid/net/wifi/WifiStateMachine$DriverUnloadedState;,
        Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;,
        Landroid/net/wifi/WifiStateMachine$DriverLoadedState;,
        Landroid/net/wifi/WifiStateMachine$DriverLoadingState;,
        Landroid/net/wifi/WifiStateMachine$InitialState;,
        Landroid/net/wifi/WifiStateMachine$DefaultState;,
        Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    }
.end annotation


# static fields
.field private static final ACTION_DELAYED_DRIVER_STOP:Ljava/lang/String; = "com.android.server.WifiManager.action.DELAYED_DRIVER_STOP"

.field private static final ACTION_START_SCAN:Ljava/lang/String; = "com.android.server.WifiManager.action.START_SCAN"

.field static final BASE:I = 0x20000

.field private static final BSSID_STR:Ljava/lang/String; = "bssid="

.field static final CMD_ADD_OR_UPDATE_NETWORK:I = 0x20034

.field static final CMD_BLACKLIST_NETWORK:I = 0x20038

.field static final CMD_BLUETOOTH_ADAPTER_STATE_CHANGE:I = 0x2001f

.field static final CMD_CAPTIVE_CHECK_COMPLETE:I = 0x20014

.field static final CMD_CLEAR_BLACKLIST:I = 0x20039

.field static final CMD_DELAYED_STOP_DRIVER:I = 0x20012

.field public static final CMD_DISABLE_P2P_REQ:I = 0x20084

.field public static final CMD_DISABLE_P2P_RSP:I = 0x20085

.field static final CMD_DISCONNECT:I = 0x2004a

.field static final CMD_DRIVER_START_TIMED_OUT:I = 0x20013

.field static final CMD_ENABLE_ALL_NETWORKS:I = 0x20037

.field static final CMD_ENABLE_BACKGROUND_SCAN:I = 0x2005b

.field static final CMD_ENABLE_NETWORK:I = 0x20036

.field public static final CMD_ENABLE_P2P:I = 0x20083

.field static final CMD_ENABLE_RSSI_POLL:I = 0x20052

.field static final CMD_GET_CONFIGURED_NETWORKS:I = 0x2003b

.field static final CMD_LOAD_DRIVER:I = 0x20001

.field static final CMD_LOAD_DRIVER_FAILURE:I = 0x20004

.field static final CMD_LOAD_DRIVER_SUCCESS:I = 0x20003

.field static final CMD_NO_NETWORKS_PERIODIC_SCAN:I = 0x20058

.field static final CMD_PING_SUPPLICANT:I = 0x20033

.field static final CMD_REASSOCIATE:I = 0x2004c

.field static final CMD_RECONNECT:I = 0x2004b

.field static final CMD_REMOVE_NETWORK:I = 0x20035

.field static final CMD_REQUEST_AP_CONFIG:I = 0x2001b

.field static final CMD_RESET_SUPPLICANT_STATE:I = 0x2006f

.field static final CMD_RESPONSE_AP_CONFIG:I = 0x2001c

.field static final CMD_RESPONSE_VZW_AP_CONFIG:I = 0x2008d

.field static final CMD_RSSI_POLL:I = 0x20053

.field static final CMD_SAVE_CONFIG:I = 0x2003a

.field static final CMD_SET_AP_CONFIG:I = 0x20019

.field static final CMD_SET_AP_CONFIG_COMPLETED:I = 0x2001a

.field static final CMD_SET_COUNTRY_CODE:I = 0x20050

.field static final CMD_SET_FREQUENCY_BAND:I = 0x2005a

.field static final CMD_SET_HIGH_PERF_MODE:I = 0x2004d

.field static final CMD_SET_SCAN_MODE:I = 0x20048

.field static final CMD_SET_SCAN_TYPE:I = 0x20049

.field static final CMD_SET_SUSPEND_OPT_ENABLED:I = 0x20056

.field static final CMD_START_AP:I = 0x20015

.field static final CMD_START_AP_BLOCK:I = 0x20020

.field static final CMD_START_AP_FAILURE:I = 0x20017

.field static final CMD_START_AP_SUCCESS:I = 0x20016

.field static final CMD_START_DRIVER:I = 0x2000d

.field static final CMD_START_PACKET_FILTERING:I = 0x20054

.field static final CMD_START_SCAN:I = 0x20047

.field static final CMD_START_SUPPLICANT:I = 0x2000b

.field static final CMD_STATIC_IP_FAILURE:I = 0x20010

.field static final CMD_STATIC_IP_SUCCESS:I = 0x2000f

.field static final CMD_STOP_AP:I = 0x20018

.field static final CMD_STOP_DRIVER:I = 0x2000e

.field static final CMD_STOP_PACKET_FILTERING:I = 0x20055

.field static final CMD_STOP_SUPPLICANT:I = 0x2000c

.field static final CMD_STOP_SUPPLICANT_FAILED:I = 0x20011

.field static final CMD_TETHER_NOTIFICATION_TIMED_OUT:I = 0x2001e

.field static final CMD_TETHER_STATE_CHANGE:I = 0x2001d

.field static final CMD_UNLOAD_DRIVER:I = 0x20002

.field static final CMD_UNLOAD_DRIVER_FAILURE:I = 0x20006

.field static final CMD_UNLOAD_DRIVER_SUCCESS:I = 0x20005

.field private static final CONNECT_MODE:I = 0x1

.field private static final DBG:Z = false

.field private static final DEFAULT_MAX_DHCP_RETRIES:I = 0x9

.field private static final DELAYED_STOP_COUNTER:Ljava/lang/String; = "DelayedStopCounter"

.field private static final DELIMITER_STR:Ljava/lang/String; = "===="

.field public static final DHCPINFO_CACHE_SIZE:I = 0x41

.field private static final DRIVER_START_TIME_OUT_MSECS:I = 0x2710

.field private static final DRIVER_STOP_REQUEST:I = 0x0

.field private static final EVENTLOG_SUPPLICANT_STATE_CHANGED:I = 0xc367

.field private static final EVENTLOG_WIFI_EVENT_HANDLED:I = 0xc366

.field private static final EVENTLOG_WIFI_STATE_CHANGED:I = 0xc365

.field private static final FAILURE:I = -0x1

.field private static final FLAGS_STR:Ljava/lang/String; = "flags="

.field private static final FREQ_STR:Ljava/lang/String; = "freq="

.field private static final IN_ECM_STATE:I = 0x1

.field private static final LEVEL_STR:Ljava/lang/String; = "level="

.field private static final MAX_RSSI:I = 0x100

.field private static final MIN_INTERVAL_ENABLE_ALL_NETWORKS_MS:I = 0x927c0

.field private static final MIN_RSSI:I = -0xc8

.field static final MULTICAST_V4:I = 0x0

.field static final MULTICAST_V6:I = 0x1

.field private static final NETWORKTYPE:Ljava/lang/String; = "WIFI"

.field private static final NOT_IN_ECM_STATE:I = 0x0

.field private static final POLL_RSSI_INTERVAL_MSECS:I = 0xbb8

.field private static final RSSI_LEVELS_LG:I = 0x4

.field private static final SCAN_ACTIVE:I = 0x1

.field private static final SCAN_ONLY_MODE:I = 0x2

.field private static final SCAN_PASSIVE:I = 0x2

.field private static final SCAN_REQUEST:I = 0x0

.field private static final SCAN_RESULT_CACHE_SIZE:I = 0x50

.field private static final SOFTAP_IFACE:Ljava/lang/String; = "wlan0"

.field private static final SSID_STR:Ljava/lang/String; = "ssid="

.field private static final SUCCESS:I = 0x1

.field private static final SUPPLICANT_RESTART_INTERVAL_MSECS:I = 0x1388

.field private static final SUPPLICANT_RESTART_TRIES:I = 0x5

.field private static final SUSPEND_DUE_TO_DHCP:I = 0x1

.field private static final SUSPEND_DUE_TO_HIGH_PERF:I = 0x2

.field private static final SUSPEND_DUE_TO_SCREEN:I = 0x4

.field private static final TAG:Ljava/lang/String; = "WifiStateMachine"

.field private static final TETHER_NOTIFICATION_TIME_OUT_MSECS:I = 0x1388

.field private static final TSF_STR:Ljava/lang/String; = "tsf="

.field private static mStateMachineInitDone:Z

.field private static final scanResultPattern:Ljava/util/regex/Pattern;


# instance fields
.field public final CMD_REQUEST_VZW_AP_CONFIG:I

.field public final CMD_SET_VZW_AP_CONFIG:I

.field public final CMD_SET_VZW_AP_CONFIG_COMPLETED:I

.field public final CMD_START_VZW_AP:I

.field public final CMD_START_VZW_AP_FAILURE:I

.field public final CMD_START_VZW_AP_SUCCESS:I

.field public final CMD_STOP_VZW_AP:I

.field private HS20_bssid:Ljava/lang/String;

.field private HS20_ssid:Ljava/lang/String;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private final mBackgroundScanSupported:Z

.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private mBluetoothConnectionActive:Z

.field private mCaptivePortalCheckState:Lcom/android/internal/util/State;

.field private mCm:Landroid/net/ConnectivityManager;

.field private mConnectModeState:Lcom/android/internal/util/State;

.field private mConnectedState:Lcom/android/internal/util/State;

.field private mContext:Landroid/content/Context;

.field private final mDefaultFrameworkScanIntervalMs:I

.field private mDefaultState:Lcom/android/internal/util/State;

.field private mDelayedStopCounter:I

.field private final mDhcpInfoCacheList:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/DhcpInfoInternal;",
            ">;"
        }
    .end annotation
.end field

.field private mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

.field private mDhcpStateMachine:Landroid/net/DhcpStateMachine;

.field private mDisconnectedState:Lcom/android/internal/util/State;

.field private mDisconnectingState:Lcom/android/internal/util/State;

.field private mDriverFailedState:Lcom/android/internal/util/State;

.field private mDriverLoadedState:Lcom/android/internal/util/State;

.field private mDriverLoadingState:Lcom/android/internal/util/State;

.field private mDriverStartToken:I

.field private mDriverStartedState:Lcom/android/internal/util/State;

.field private mDriverStartedStateExt:Lcom/android/internal/util/State;

.field private mDriverStartingState:Lcom/android/internal/util/State;

.field private final mDriverStopDelayMs:I

.field private mDriverStopIntent:Landroid/app/PendingIntent;

.field private mDriverStoppedState:Lcom/android/internal/util/State;

.field private mDriverStoppingState:Lcom/android/internal/util/State;

.field private mDriverUnloadedState:Lcom/android/internal/util/State;

.field private mDriverUnloadingState:Lcom/android/internal/util/State;

.field private mEnableBackgroundScan:Z

.field private mEnableRssiPolling:Z

.field private mFilteringMulticastV4Packets:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mFrequencyBand:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mInDelayedStop:Z

.field private mInitialState:Lcom/android/internal/util/State;

.field private mInterfaceName:Ljava/lang/String;

.field private mIsRunning:Z

.field private mIsScanMode:Z

.field private mL2ConnectedState:Lcom/android/internal/util/State;

.field private final mLastApEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mLastBssid:Ljava/lang/String;

.field private mLastEnableAllNetworksTime:J

.field private final mLastEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mLastNetworkId:I

.field private final mLastRunningWifiUids:Landroid/os/WorkSource;

.field private mLastSignalLevel:I

.field private mLastSignalLevel_LG:I

.field private mLgeKtCm:Z

.field private mLinkProperties:Landroid/net/LinkProperties;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mNwService:Landroid/os/INetworkManagementService;

.field private mObtainingIpState:Lcom/android/internal/util/State;

.field private final mP2pConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mP2pSupported:Z

.field private mPeriodicScanToken:I

.field private final mPrimaryDeviceType:Ljava/lang/String;

.field private mReconnectCount:I

.field private mReplyChannel:Lcom/android/internal/util/AsyncChannel;

.field private mReportedRunning:Z

.field private mRssiPollToken:I

.field private final mRunningWifiUids:Landroid/os/WorkSource;

.field private mScanIntent:Landroid/app/PendingIntent;

.field private mScanModeState:Lcom/android/internal/util/State;

.field private final mScanResultCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private mScanResultIsPending:Z

.field private mScanResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenBroadcastReceived:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mSetScanActive:Z

.field private mSoftApStartedState:Lcom/android/internal/util/State;

.field private mSoftApStartingState:Lcom/android/internal/util/State;

.field private mSoftApStoppingState:Lcom/android/internal/util/State;

.field private mSupplicantRestartCount:I

.field private mSupplicantScanIntervalMs:J

.field private mSupplicantStartedState:Lcom/android/internal/util/State;

.field private mSupplicantStartingState:Lcom/android/internal/util/State;

.field private mSupplicantStateTracker:Landroid/net/wifi/SupplicantStateTracker;

.field private mSupplicantStopFailureToken:I

.field private mSupplicantStoppingState:Lcom/android/internal/util/State;

.field private mSuspendOptNeedsDisabled:I

.field private mSuspendWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mTemporarilyDisconnectWifi:Z

.field private mTetherInterfaceName:Ljava/lang/String;

.field private mTetherToken:I

.field private mTetheredState:Lcom/android/internal/util/State;

.field private mTetheringState:Lcom/android/internal/util/State;

.field private mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mVerifyingLinkState:Lcom/android/internal/util/State;

.field private mWaitForP2pDisableState:Lcom/android/internal/util/State;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

.field private mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

.field private mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

.field private mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

.field private final mWifiApState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

.field private mWifiInfo:Landroid/net/wifi/WifiInfo;

.field private mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

.field private mWifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;

.field private mWifiMonitor:Landroid/net/wifi/WifiMonitor;

.field private mWifiNative:Landroid/net/wifi/WifiNative;

.field private mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

.field private mWifiP2pChannel:Lcom/android/internal/util/AsyncChannel;

.field private mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

.field private final mWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mWpsRunningState:Lcom/android/internal/util/State;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 168
    const-string v0, "\t+"

    #@2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/net/wifi/WifiStateMachine;->scanResultPattern:Ljava/util/regex/Pattern;

    #@8
    .line 685
    const/4 v0, 0x0

    #@9
    sput-boolean v0, Landroid/net/wifi/WifiStateMachine;->mStateMachineInitDone:Z

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "wlanInterface"

    #@0
    .prologue
    .line 693
    const-string v0, "WifiStateMachine"

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@5
    .line 162
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@b
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mP2pConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@d
    .line 163
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mTemporarilyDisconnectWifi:Z

    #@10
    .line 167
    new-instance v0, Ljava/util/ArrayList;

    #@12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanResults:Ljava/util/List;

    #@17
    .line 179
    const/4 v0, -0x1

    #@18
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@1a
    .line 182
    const/4 v0, 0x0

    #@1b
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mEnableRssiPolling:Z

    #@1d
    .line 183
    const/4 v0, 0x0

    #@1e
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mEnableBackgroundScan:Z

    #@20
    .line 184
    const/4 v0, 0x0

    #@21
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mRssiPollToken:I

    #@23
    .line 185
    const/4 v0, 0x0

    #@24
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@26
    .line 186
    const/4 v0, 0x0

    #@27
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mIsScanMode:Z

    #@29
    .line 187
    const/4 v0, 0x0

    #@2a
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanResultIsPending:Z

    #@2c
    .line 189
    const/4 v0, 0x0

    #@2d
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mSetScanActive:Z

    #@2f
    .line 193
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@31
    const/4 v1, 0x0

    #@32
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@35
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScreenBroadcastReceived:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@37
    .line 195
    const/4 v0, 0x0

    #@38
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mBluetoothConnectionActive:Z

    #@3a
    .line 215
    const/4 v0, 0x0

    #@3b
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantRestartCount:I

    #@3d
    .line 217
    const/4 v0, 0x0

    #@3e
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStopFailureToken:I

    #@40
    .line 225
    const/4 v0, 0x0

    #@41
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetherToken:I

    #@43
    .line 233
    const/4 v0, 0x0

    #@44
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartToken:I

    #@46
    .line 238
    const/4 v0, 0x0

    #@47
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mPeriodicScanToken:I

    #@49
    .line 256
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@4b
    const/4 v1, 0x0

    #@4c
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@4f
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFrequencyBand:Ljava/util/concurrent/atomic/AtomicInteger;

    #@51
    .line 259
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@53
    const/4 v1, 0x1

    #@54
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@57
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFilteringMulticastV4Packets:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@59
    .line 262
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@5b
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@5e
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@60
    .line 266
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@62
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@65
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiP2pChannel:Lcom/android/internal/util/AsyncChannel;

    #@67
    .line 267
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@69
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@6c
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@6e
    .line 275
    const/4 v0, -0x1

    #@6f
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel_LG:I

    #@71
    .line 430
    const v0, 0x20086

    #@74
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_START_VZW_AP:I

    #@76
    .line 431
    const v0, 0x20087

    #@79
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_START_VZW_AP_SUCCESS:I

    #@7b
    .line 432
    const v0, 0x20088

    #@7e
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_START_VZW_AP_FAILURE:I

    #@80
    .line 433
    const v0, 0x20089

    #@83
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_STOP_VZW_AP:I

    #@85
    .line 434
    const v0, 0x2008a

    #@88
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_SET_VZW_AP_CONFIG:I

    #@8a
    .line 435
    const v0, 0x2008b

    #@8d
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_SET_VZW_AP_CONFIG_COMPLETED:I

    #@8f
    .line 436
    const v0, 0x2008c

    #@92
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->CMD_REQUEST_VZW_AP_CONFIG:I

    #@94
    .line 468
    const/4 v0, 0x0

    #@95
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@97
    .line 475
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@99
    const/4 v1, 0x1

    #@9a
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@9d
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@9f
    .line 507
    const/4 v0, 0x0

    #@a0
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mInDelayedStop:Z

    #@a2
    .line 513
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DefaultState;

    #@a4
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DefaultState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@a7
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@a9
    .line 515
    new-instance v0, Landroid/net/wifi/WifiStateMachine$InitialState;

    #@ab
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$InitialState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@ae
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mInitialState:Lcom/android/internal/util/State;

    #@b0
    .line 517
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;

    #@b2
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@b5
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadingState:Lcom/android/internal/util/State;

    #@b7
    .line 519
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverUnloadedState;

    #@b9
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverUnloadedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@bc
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadedState:Lcom/android/internal/util/State;

    #@be
    .line 521
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverFailedState;

    #@c0
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverFailedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@c3
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverFailedState:Lcom/android/internal/util/State;

    #@c5
    .line 523
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverLoadingState;

    #@c7
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverLoadingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@ca
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverLoadingState:Lcom/android/internal/util/State;

    #@cc
    .line 525
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverLoadedState;

    #@ce
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverLoadedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@d1
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverLoadedState:Lcom/android/internal/util/State;

    #@d3
    .line 527
    new-instance v0, Landroid/net/wifi/WifiStateMachine$SupplicantStartingState;

    #@d5
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$SupplicantStartingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@d8
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartingState:Lcom/android/internal/util/State;

    #@da
    .line 529
    new-instance v0, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;

    #@dc
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$SupplicantStartedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@df
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@e1
    .line 531
    new-instance v0, Landroid/net/wifi/WifiStateMachine$SupplicantStoppingState;

    #@e3
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$SupplicantStoppingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@e6
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStoppingState:Lcom/android/internal/util/State;

    #@e8
    .line 533
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;

    #@ea
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverStartingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@ed
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartingState:Lcom/android/internal/util/State;

    #@ef
    .line 535
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;

    #@f1
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverStartedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@f4
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedState:Lcom/android/internal/util/State;

    #@f6
    .line 540
    new-instance v0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;

    #@f8
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@fb
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWaitForP2pDisableState:Lcom/android/internal/util/State;

    #@fd
    .line 542
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;

    #@ff
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverStoppingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@102
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStoppingState:Lcom/android/internal/util/State;

    #@104
    .line 544
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverStoppedState;

    #@106
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverStoppedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@109
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStoppedState:Lcom/android/internal/util/State;

    #@10b
    .line 546
    new-instance v0, Landroid/net/wifi/WifiStateMachine$ScanModeState;

    #@10d
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$ScanModeState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@110
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanModeState:Lcom/android/internal/util/State;

    #@112
    .line 548
    new-instance v0, Landroid/net/wifi/WifiStateMachine$ConnectModeState;

    #@114
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$ConnectModeState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@117
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mConnectModeState:Lcom/android/internal/util/State;

    #@119
    .line 550
    new-instance v0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;

    #@11b
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@11e
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mL2ConnectedState:Lcom/android/internal/util/State;

    #@120
    .line 552
    new-instance v0, Landroid/net/wifi/WifiStateMachine$ObtainingIpState;

    #@122
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$ObtainingIpState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@125
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mObtainingIpState:Lcom/android/internal/util/State;

    #@127
    .line 554
    new-instance v0, Landroid/net/wifi/WifiStateMachine$VerifyingLinkState;

    #@129
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$VerifyingLinkState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@12c
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mVerifyingLinkState:Lcom/android/internal/util/State;

    #@12e
    .line 556
    new-instance v0, Landroid/net/wifi/WifiStateMachine$CaptivePortalCheckState;

    #@130
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$CaptivePortalCheckState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@133
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mCaptivePortalCheckState:Lcom/android/internal/util/State;

    #@135
    .line 558
    new-instance v0, Landroid/net/wifi/WifiStateMachine$ConnectedState;

    #@137
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$ConnectedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@13a
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mConnectedState:Lcom/android/internal/util/State;

    #@13c
    .line 560
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DisconnectingState;

    #@13e
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DisconnectingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@141
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectingState:Lcom/android/internal/util/State;

    #@143
    .line 562
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;

    #@145
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DisconnectedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@148
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectedState:Lcom/android/internal/util/State;

    #@14a
    .line 564
    new-instance v0, Landroid/net/wifi/WifiStateMachine$WpsRunningState;

    #@14c
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$WpsRunningState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@14f
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWpsRunningState:Lcom/android/internal/util/State;

    #@151
    .line 567
    new-instance v0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;

    #@153
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@156
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartingState:Lcom/android/internal/util/State;

    #@158
    .line 569
    new-instance v0, Landroid/net/wifi/WifiStateMachine$SoftApStartedState;

    #@15a
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$SoftApStartedState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@15d
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartedState:Lcom/android/internal/util/State;

    #@15f
    .line 571
    new-instance v0, Landroid/net/wifi/WifiStateMachine$TetheringState;

    #@161
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$TetheringState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@164
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetheringState:Lcom/android/internal/util/State;

    #@166
    .line 573
    new-instance v0, Landroid/net/wifi/WifiStateMachine$TetheredState;

    #@168
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$TetheredState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@16b
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetheredState:Lcom/android/internal/util/State;

    #@16d
    .line 575
    new-instance v0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;

    #@16f
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@172
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStoppingState:Lcom/android/internal/util/State;

    #@174
    .line 578
    new-instance v0, Landroid/net/wifi/WifiStateMachine$DriverStartedStateExt;

    #@176
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiStateMachine$DriverStartedStateExt;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@179
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedStateExt:Lcom/android/internal/util/State;

    #@17b
    .line 599
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@17d
    const/4 v1, 0x1

    #@17e
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@181
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@183
    .line 609
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@185
    const/16 v1, 0xb

    #@187
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@18a
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@18c
    .line 611
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@18e
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@191
    move-result v1

    #@192
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@195
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

    #@197
    .line 612
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@199
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@19c
    move-result v1

    #@19d
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@1a0
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastApEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

    #@1a2
    .line 626
    const/4 v0, 0x0

    #@1a3
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mIsRunning:Z

    #@1a5
    .line 631
    const/4 v0, 0x0

    #@1a6
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mReportedRunning:Z

    #@1a8
    .line 636
    new-instance v0, Landroid/os/WorkSource;

    #@1aa
    invoke-direct {v0}, Landroid/os/WorkSource;-><init>()V

    #@1ad
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@1af
    .line 641
    new-instance v0, Landroid/os/WorkSource;

    #@1b1
    invoke-direct {v0}, Landroid/os/WorkSource;-><init>()V

    #@1b4
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@1b6
    .line 676
    const/4 v0, 0x0

    #@1b7
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@1b9
    .line 688
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useLgeKtCm()Z

    #@1bc
    move-result v0

    #@1bd
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mLgeKtCm:Z

    #@1bf
    .line 695
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@1c1
    .line 696
    iput-object p2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@1c3
    .line 698
    new-instance v0, Landroid/net/NetworkInfo;

    #@1c5
    const/4 v1, 0x1

    #@1c6
    const/4 v2, 0x0

    #@1c7
    const-string v3, "WIFI"

    #@1c9
    const-string v5, ""

    #@1cb
    invoke-direct {v0, v1, v2, v3, v5}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@1ce
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@1d0
    .line 699
    const-string v0, "batteryinfo"

    #@1d2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1d5
    move-result-object v0

    #@1d6
    invoke-static {v0}, Lcom/android/internal/app/IBatteryStats$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IBatteryStats;

    #@1d9
    move-result-object v0

    #@1da
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@1dc
    .line 701
    const-string/jumbo v0, "network_management"

    #@1df
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1e2
    move-result-object v6

    #@1e3
    .line 702
    .local v6, b:Landroid/os/IBinder;
    invoke-static {v6}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@1e6
    move-result-object v0

    #@1e7
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@1e9
    .line 704
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@1eb
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1ee
    move-result-object v0

    #@1ef
    const-string v1, "android.hardware.wifi.direct"

    #@1f1
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@1f4
    move-result v0

    #@1f5
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mP2pSupported:Z

    #@1f7
    .line 707
    new-instance v0, Landroid/net/wifi/WifiNative;

    #@1f9
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@1fb
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiNative;-><init>(Ljava/lang/String;)V

    #@1fe
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@200
    .line 708
    new-instance v0, Landroid/net/wifi/WifiConfigStore;

    #@202
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@204
    invoke-direct {v0, p1, v1}, Landroid/net/wifi/WifiConfigStore;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiNative;)V

    #@207
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@209
    .line 709
    new-instance v0, Landroid/net/wifi/WifiMonitor;

    #@20b
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@20d
    invoke-direct {v0, p0, v1}, Landroid/net/wifi/WifiMonitor;-><init>(Lcom/android/internal/util/StateMachine;Landroid/net/wifi/WifiNative;)V

    #@210
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiMonitor:Landroid/net/wifi/WifiMonitor;

    #@212
    .line 710
    new-instance v0, Landroid/net/DhcpInfoInternal;

    #@214
    invoke-direct {v0}, Landroid/net/DhcpInfoInternal;-><init>()V

    #@217
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@219
    .line 711
    new-instance v0, Landroid/net/wifi/WifiInfo;

    #@21b
    invoke-direct {v0}, Landroid/net/wifi/WifiInfo;-><init>()V

    #@21e
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@220
    .line 712
    new-instance v0, Landroid/net/wifi/SupplicantStateTracker;

    #@222
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@224
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getHandler()Landroid/os/Handler;

    #@227
    move-result-object v2

    #@228
    invoke-direct {v0, p1, p0, v1, v2}, Landroid/net/wifi/SupplicantStateTracker;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiConfigStore;Landroid/os/Handler;)V

    #@22b
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStateTracker:Landroid/net/wifi/SupplicantStateTracker;

    #@22d
    .line 714
    new-instance v0, Landroid/net/LinkProperties;

    #@22f
    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    #@232
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@234
    .line 716
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getHandler()Landroid/os/Handler;

    #@237
    move-result-object v0

    #@238
    invoke-static {p1, v0}, Landroid/net/wifi/WifiApConfigStore;->makeWifiApConfigStore(Landroid/content/Context;Landroid/os/Handler;)Landroid/net/wifi/WifiApConfigStore;

    #@23b
    move-result-object v11

    #@23c
    .line 718
    .local v11, wifiApConfigStore:Landroid/net/wifi/WifiApConfigStore;
    invoke-virtual {v11}, Landroid/net/wifi/WifiApConfigStore;->loadApConfiguration()V

    #@23f
    .line 719
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@241
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@243
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getHandler()Landroid/os/Handler;

    #@246
    move-result-object v2

    #@247
    invoke-virtual {v11}, Landroid/net/wifi/WifiApConfigStore;->getMessenger()Landroid/os/Messenger;

    #@24a
    move-result-object v3

    #@24b
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/util/AsyncChannel;->connectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)I

    #@24e
    .line 721
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@250
    const/4 v1, 0x0

    #@251
    invoke-virtual {v0, v1}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@254
    .line 722
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@256
    invoke-virtual {v0}, Landroid/net/LinkProperties;->clear()V

    #@259
    .line 723
    const/4 v0, 0x0

    #@25a
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@25c
    .line 724
    const/4 v0, -0x1

    #@25d
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@25f
    .line 725
    const/4 v0, -0x1

    #@260
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@262
    .line 728
    const/4 v0, -0x1

    #@263
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel_LG:I

    #@265
    .line 732
    new-instance v0, Landroid/util/LruCache;

    #@267
    const/16 v1, 0x41

    #@269
    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    #@26c
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@26e
    .line 735
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@270
    const-string v1, "alarm"

    #@272
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@275
    move-result-object v0

    #@276
    check-cast v0, Landroid/app/AlarmManager;

    #@278
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mAlarmManager:Landroid/app/AlarmManager;

    #@27a
    .line 736
    new-instance v8, Landroid/content/Intent;

    #@27c
    const-string v0, "com.android.server.WifiManager.action.START_SCAN"

    #@27e
    const/4 v1, 0x0

    #@27f
    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@282
    .line 737
    .local v8, scanIntent:Landroid/content/Intent;
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@284
    const/4 v1, 0x0

    #@285
    const/4 v2, 0x0

    #@286
    invoke-static {v0, v1, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@289
    move-result-object v0

    #@28a
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanIntent:Landroid/app/PendingIntent;

    #@28c
    .line 739
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@28e
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@291
    move-result-object v0

    #@292
    const v1, 0x10e000e

    #@295
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@298
    move-result v0

    #@299
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultFrameworkScanIntervalMs:I

    #@29b
    .line 742
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@29d
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2a0
    move-result-object v0

    #@2a1
    const v1, 0x10e000f

    #@2a4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@2a7
    move-result v0

    #@2a8
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStopDelayMs:I

    #@2aa
    .line 745
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2ac
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2af
    move-result-object v0

    #@2b0
    const v1, 0x1110017

    #@2b3
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2b6
    move-result v0

    #@2b7
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mBackgroundScanSupported:Z

    #@2b9
    .line 748
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2bb
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2be
    move-result-object v0

    #@2bf
    const v1, 0x1040035

    #@2c2
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2c5
    move-result-object v0

    #@2c6
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mPrimaryDeviceType:Ljava/lang/String;

    #@2c8
    .line 751
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2ca
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2cc
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2cf
    move-result-object v0

    #@2d0
    const-string/jumbo v2, "wifi_suspend_optimizations_enabled"

    #@2d3
    const/4 v3, 0x1

    #@2d4
    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2d7
    move-result v0

    #@2d8
    const/4 v2, 0x1

    #@2d9
    if-ne v0, v2, :cond_497

    #@2db
    const/4 v0, 0x1

    #@2dc
    :goto_2dc
    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@2df
    .line 754
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2e1
    new-instance v1, Landroid/net/wifi/WifiStateMachine$1;

    #@2e3
    invoke-direct {v1, p0}, Landroid/net/wifi/WifiStateMachine$1;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@2e6
    new-instance v2, Landroid/content/IntentFilter;

    #@2e8
    const-string v3, "android.net.conn.TETHER_STATE_CHANGED"

    #@2ea
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2ed
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2f0
    .line 766
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2f2
    new-instance v1, Landroid/net/wifi/WifiStateMachine$2;

    #@2f4
    invoke-direct {v1, p0}, Landroid/net/wifi/WifiStateMachine$2;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@2f7
    new-instance v2, Landroid/content/IntentFilter;

    #@2f9
    const-string v3, "com.android.server.WifiManager.action.START_SCAN"

    #@2fb
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2fe
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@301
    .line 775
    new-instance v9, Landroid/content/IntentFilter;

    #@303
    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    #@306
    .line 776
    .local v9, screenFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.SCREEN_ON"

    #@308
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@30b
    .line 777
    const-string v0, "android.intent.action.SCREEN_OFF"

    #@30d
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@310
    .line 778
    new-instance v10, Landroid/net/wifi/WifiStateMachine$3;

    #@312
    invoke-direct {v10, p0}, Landroid/net/wifi/WifiStateMachine$3;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@315
    .line 790
    .local v10, screenReceiver:Landroid/content/BroadcastReceiver;
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@317
    invoke-virtual {v0, v10, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@31a
    .line 792
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@31c
    new-instance v1, Landroid/net/wifi/WifiStateMachine$4;

    #@31e
    invoke-direct {v1, p0}, Landroid/net/wifi/WifiStateMachine$4;-><init>(Landroid/net/wifi/WifiStateMachine;)V

    #@321
    new-instance v2, Landroid/content/IntentFilter;

    #@323
    const-string v3, "com.android.server.WifiManager.action.DELAYED_DRIVER_STOP"

    #@325
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@328
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@32b
    .line 802
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@32d
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@330
    move-result-object v0

    #@331
    const-string/jumbo v1, "wifi_suspend_optimizations_enabled"

    #@334
    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@337
    move-result-object v1

    #@338
    const/4 v2, 0x0

    #@339
    new-instance v3, Landroid/net/wifi/WifiStateMachine$5;

    #@33b
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getHandler()Landroid/os/Handler;

    #@33e
    move-result-object v5

    #@33f
    invoke-direct {v3, p0, v5}, Landroid/net/wifi/WifiStateMachine$5;-><init>(Landroid/net/wifi/WifiStateMachine;Landroid/os/Handler;)V

    #@342
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@345
    .line 812
    new-instance v0, Landroid/util/LruCache;

    #@347
    const/16 v1, 0x50

    #@349
    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    #@34c
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanResultCache:Landroid/util/LruCache;

    #@34e
    .line 814
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@350
    const-string/jumbo v1, "power"

    #@353
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@356
    move-result-object v7

    #@357
    check-cast v7, Landroid/os/PowerManager;

    #@359
    .line 815
    .local v7, powerManager:Landroid/os/PowerManager;
    const/4 v0, 0x1

    #@35a
    const-string v1, "WifiStateMachine"

    #@35c
    invoke-virtual {v7, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@35f
    move-result-object v0

    #@360
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@362
    .line 817
    const/4 v0, 0x1

    #@363
    const-string v1, "WifiSuspend"

    #@365
    invoke-virtual {v7, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@368
    move-result-object v0

    #@369
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendWakeLock:Landroid/os/PowerManager$WakeLock;

    #@36b
    .line 818
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendWakeLock:Landroid/os/PowerManager$WakeLock;

    #@36d
    const/4 v1, 0x0

    #@36e
    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@371
    .line 820
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@373
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@376
    .line 821
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mInitialState:Lcom/android/internal/util/State;

    #@378
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@37a
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@37d
    .line 822
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadingState:Lcom/android/internal/util/State;

    #@37f
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@381
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@384
    .line 823
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadedState:Lcom/android/internal/util/State;

    #@386
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@388
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@38b
    .line 824
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverFailedState:Lcom/android/internal/util/State;

    #@38d
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadedState:Lcom/android/internal/util/State;

    #@38f
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@392
    .line 825
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverLoadingState:Lcom/android/internal/util/State;

    #@394
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@396
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@399
    .line 826
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverLoadedState:Lcom/android/internal/util/State;

    #@39b
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@39d
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3a0
    .line 827
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartingState:Lcom/android/internal/util/State;

    #@3a2
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@3a4
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3a7
    .line 828
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@3a9
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@3ab
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3ae
    .line 829
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartingState:Lcom/android/internal/util/State;

    #@3b0
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@3b2
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3b5
    .line 831
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedStateExt:Lcom/android/internal/util/State;

    #@3b7
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@3b9
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3bc
    .line 832
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedState:Lcom/android/internal/util/State;

    #@3be
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedStateExt:Lcom/android/internal/util/State;

    #@3c0
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3c3
    .line 835
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanModeState:Lcom/android/internal/util/State;

    #@3c5
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedState:Lcom/android/internal/util/State;

    #@3c7
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3ca
    .line 836
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mConnectModeState:Lcom/android/internal/util/State;

    #@3cc
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedState:Lcom/android/internal/util/State;

    #@3ce
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3d1
    .line 837
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mL2ConnectedState:Lcom/android/internal/util/State;

    #@3d3
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mConnectModeState:Lcom/android/internal/util/State;

    #@3d5
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3d8
    .line 838
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mObtainingIpState:Lcom/android/internal/util/State;

    #@3da
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mL2ConnectedState:Lcom/android/internal/util/State;

    #@3dc
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3df
    .line 839
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mVerifyingLinkState:Lcom/android/internal/util/State;

    #@3e1
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mL2ConnectedState:Lcom/android/internal/util/State;

    #@3e3
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3e6
    .line 840
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mCaptivePortalCheckState:Lcom/android/internal/util/State;

    #@3e8
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mL2ConnectedState:Lcom/android/internal/util/State;

    #@3ea
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3ed
    .line 841
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mConnectedState:Lcom/android/internal/util/State;

    #@3ef
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mL2ConnectedState:Lcom/android/internal/util/State;

    #@3f1
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3f4
    .line 842
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectingState:Lcom/android/internal/util/State;

    #@3f6
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mConnectModeState:Lcom/android/internal/util/State;

    #@3f8
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3fb
    .line 843
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectedState:Lcom/android/internal/util/State;

    #@3fd
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mConnectModeState:Lcom/android/internal/util/State;

    #@3ff
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@402
    .line 844
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWpsRunningState:Lcom/android/internal/util/State;

    #@404
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mConnectModeState:Lcom/android/internal/util/State;

    #@406
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@409
    .line 845
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWaitForP2pDisableState:Lcom/android/internal/util/State;

    #@40b
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@40d
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@410
    .line 846
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStoppingState:Lcom/android/internal/util/State;

    #@412
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@414
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@417
    .line 847
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStoppedState:Lcom/android/internal/util/State;

    #@419
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartedState:Lcom/android/internal/util/State;

    #@41b
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@41e
    .line 848
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStoppingState:Lcom/android/internal/util/State;

    #@420
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@422
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@425
    .line 849
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartingState:Lcom/android/internal/util/State;

    #@427
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@429
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@42c
    .line 850
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartedState:Lcom/android/internal/util/State;

    #@42e
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@430
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@433
    .line 851
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetheringState:Lcom/android/internal/util/State;

    #@435
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartedState:Lcom/android/internal/util/State;

    #@437
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@43a
    .line 852
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetheredState:Lcom/android/internal/util/State;

    #@43c
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartedState:Lcom/android/internal/util/State;

    #@43e
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@441
    .line 853
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStoppingState:Lcom/android/internal/util/State;

    #@443
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultState:Lcom/android/internal/util/State;

    #@445
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@448
    .line 855
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mInitialState:Lcom/android/internal/util/State;

    #@44a
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@44d
    .line 857
    const/16 v0, 0x64

    #@44f
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->setLogRecSize(I)V

    #@452
    .line 861
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@454
    if-eqz v0, :cond_49a

    #@456
    .line 866
    invoke-static {}, Lcom/lge/wifi/WifiLgeSharedValues;->getInstance()Lcom/lge/wifi/WifiLgeSharedValues;

    #@459
    move-result-object v0

    #@45a
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@45c
    .line 867
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@45f
    move-result-object v0

    #@460
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@462
    .line 868
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@464
    if-eqz v0, :cond_493

    #@466
    .line 869
    new-instance v4, Landroid/net/wifi/WifiConfigStoreProxy;

    #@468
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@46a
    invoke-direct {v4, v0}, Landroid/net/wifi/WifiConfigStoreProxy;-><init>(Landroid/net/wifi/WifiConfigStore;)V

    #@46d
    .line 870
    .local v4, wifiConfigStoreProxy:Landroid/net/wifi/WifiConfigStoreProxy;
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@46f
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@471
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@473
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@475
    move-object v2, p0

    #@476
    invoke-interface/range {v0 .. v5}, Lcom/lge/wifi_iface/WifiServiceExtIface;->initWifiSerivceExt(Landroid/content/Context;Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiNative;Landroid/net/wifi/WifiConfigStoreProxy;Ljava/lang/String;)V

    #@479
    .line 875
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@47b
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getWifiOffDelayIfNotUsed()Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@47e
    move-result-object v0

    #@47f
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@481
    .line 880
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@483
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@485
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getWifiApDisableIfNotUsed(Ljava/lang/String;)Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@488
    move-result-object v0

    #@489
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@48b
    .line 886
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@48d
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getWifiActivationWhileCharging()Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@490
    move-result-object v0

    #@491
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@493
    .line 897
    .end local v4           #wifiConfigStoreProxy:Landroid/net/wifi/WifiConfigStoreProxy;
    :cond_493
    :goto_493
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->start()V

    #@496
    .line 898
    return-void

    #@497
    .line 751
    .end local v7           #powerManager:Landroid/os/PowerManager;
    .end local v9           #screenFilter:Landroid/content/IntentFilter;
    .end local v10           #screenReceiver:Landroid/content/BroadcastReceiver;
    :cond_497
    const/4 v0, 0x0

    #@498
    goto/16 :goto_2dc

    #@49a
    .line 889
    .restart local v7       #powerManager:Landroid/os/PowerManager;
    .restart local v9       #screenFilter:Landroid/content/IntentFilter;
    .restart local v10       #screenReceiver:Landroid/content/BroadcastReceiver;
    :cond_49a
    const/4 v0, 0x0

    #@49b
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@49d
    .line 890
    const/4 v0, 0x0

    #@49e
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@4a0
    .line 891
    const/4 v0, 0x0

    #@4a1
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@4a3
    .line 892
    const/4 v0, 0x0

    #@4a4
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@4a6
    .line 893
    const/4 v0, 0x0

    #@4a7
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@4a9
    goto :goto_493
.end method

.method private ConnectedModeStateNetworkConnectionEvent()V
    .registers 4

    #@0
    .prologue
    .line 4842
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@4
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->updateConnectionInfo(Landroid/net/wifi/WifiInfo;)V

    #@7
    .line 4843
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@9
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@b
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setKeepAlivePacket(Landroid/net/wifi/WifiInfo;)V

    #@e
    .line 4844
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@10
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@12
    invoke-virtual {v1}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@15
    move-result-object v1

    #@16
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@18
    invoke-interface {v0, v1, v2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->checkAPSecurity(Ljava/util/List;I)V

    #@1b
    .line 4845
    return-void
.end method

.method static synthetic access$000(Landroid/net/wifi/WifiStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->handleScreenStateChanged(Z)V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;ILjava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3}, Landroid/net/wifi/WifiStateMachine;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$10000(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@2
    return v0
.end method

.method static synthetic access$10100(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mSetScanActive:Z

    #@2
    return v0
.end method

.method static synthetic access$10102(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mSetScanActive:Z

    #@2
    return p1
.end method

.method static synthetic access$10200(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicInteger;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFrequencyBand:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    return-object v0
.end method

.method static synthetic access$10300(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mDelayedStopCounter:I

    #@2
    return v0
.end method

.method static synthetic access$10308(Landroid/net/wifi/WifiStateMachine;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mDelayedStopCounter:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Landroid/net/wifi/WifiStateMachine;->mDelayedStopCounter:I

    #@6
    return v0
.end method

.method static synthetic access$10400(Landroid/net/wifi/WifiStateMachine;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStopIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$10402(Landroid/net/wifi/WifiStateMachine;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStopIntent:Landroid/app/PendingIntent;

    #@2
    return-object p1
.end method

.method static synthetic access$10500(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStopDelayMs:I

    #@2
    return v0
.end method

.method static synthetic access$10600(Landroid/net/wifi/WifiStateMachine;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$10700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/IState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$10800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$10900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStoppingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mEnableRssiPolling:Z

    #@2
    return v0
.end method

.method static synthetic access$11000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1102(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mEnableRssiPolling:Z

    #@2
    return p1
.end method

.method static synthetic access$11100(Landroid/net/wifi/WifiStateMachine;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->setSuspendOptimizationsNative(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$11202(Landroid/net/wifi/WifiStateMachine;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mScanResults:Ljava/util/List;

    #@2
    return-object p1
.end method

.method static synthetic access$11300(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$11400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$11500(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$11600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$11700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$11800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$11900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mEnableBackgroundScan:Z

    #@2
    return v0
.end method

.method static synthetic access$12000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1202(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mEnableBackgroundScan:Z

    #@2
    return p1
.end method

.method static synthetic access$12100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12200(Landroid/net/wifi/WifiStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->sendShowKTPayPopup(Z)V

    #@3
    return-void
.end method

.method static synthetic access$12300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$12600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWpsRunningState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$12800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12900(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->ConnectedModeStateNetworkConnectionEvent()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/net/wifi/WifiStateMachine;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->setSuspendOptimizations(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$13000(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->sendNetworkStateChangeBroadcast(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$13100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mObtainingIpState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$13200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$13300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$13400(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->HS20_bssid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$13402(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->HS20_bssid:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$13500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->sendHS20APBroadcast(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$13600(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->sendHS20TryToConnection(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$13700(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mRssiPollToken:I

    #@2
    return v0
.end method

.method static synthetic access$13708(Landroid/net/wifi/WifiStateMachine;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mRssiPollToken:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Landroid/net/wifi/WifiStateMachine;->mRssiPollToken:I

    #@6
    return v0
.end method

.method static synthetic access$13800(Landroid/net/wifi/WifiStateMachine;)Landroid/util/LruCache;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoCacheList:Landroid/util/LruCache;

    #@2
    return-object v0
.end method

.method static synthetic access$13900(Landroid/net/wifi/WifiStateMachine;Landroid/net/DhcpInfoInternal;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->handleSuccessfulIpConfiguration(Landroid/net/DhcpInfoInternal;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/net/wifi/WifiStateMachine;)Landroid/net/DhcpStateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$14000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mVerifyingLinkState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Landroid/net/wifi/WifiStateMachine;Landroid/net/DhcpStateMachine;)Landroid/net/DhcpStateMachine;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@2
    return-object p1
.end method

.method static synthetic access$14100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$14200(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->handleFailedIpConfiguration()V

    #@3
    return-void
.end method

.method static synthetic access$14300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$14400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$14500(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$14600(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$14700(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$14800(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->configureLinkProperties()V

    #@3
    return-void
.end method

.method static synthetic access$14900(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->sendLinkConfigurationChangedBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$15000(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->fetchRssiAndLinkSpeedNative()V

    #@3
    return-void
.end method

.method static synthetic access$15100(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/RssiPacketCountInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->fetchPktcntNative(Landroid/net/wifi/RssiPacketCountInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$15200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$15300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$15400(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$15500(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$15600(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$15700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mCaptivePortalCheckState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$15800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$15900(Landroid/net/wifi/WifiStateMachine;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->startDhcpL2ConnectedState(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mP2pConnected:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method static synthetic access$16000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mConnectedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$16100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$16200(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->connectedStateEnter()V

    #@3
    return-void
.end method

.method static synthetic access$16300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$16400(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->checkAndSetConnectivityInstance()V

    #@3
    return-void
.end method

.method static synthetic access$16500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/ConnectivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@2
    return-object v0
.end method

.method static synthetic access$16600(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$16700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$16800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$16900(Landroid/net/wifi/WifiStateMachine;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mTemporarilyDisconnectWifi:Z

    #@2
    return v0
.end method

.method static synthetic access$17000(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mDefaultFrameworkScanIntervalMs:I

    #@2
    return v0
.end method

.method static synthetic access$1702(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mTemporarilyDisconnectWifi:Z

    #@2
    return p1
.end method

.method static synthetic access$17100(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mPeriodicScanToken:I

    #@2
    return v0
.end method

.method static synthetic access$17104(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mPeriodicScanToken:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mPeriodicScanToken:I

    #@6
    return v0
.end method

.method static synthetic access$17200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$17300(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$17400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$17500(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$17600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$17700(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$17800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$17900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->replyToMessage(Landroid/os/Message;I)V

    #@3
    return-void
.end method

.method static synthetic access$18000(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$18100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$18200(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiConfiguration;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->startSoftApWithConfig(Landroid/net/wifi/WifiConfiguration;)V

    #@3
    return-void
.end method

.method static synthetic access$18300(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiVZWConfiguration;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->startVZWSoftApWithConfig(Landroid/net/wifi/WifiVZWConfiguration;)V

    #@3
    return-void
.end method

.method static synthetic access$18400(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$18500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$18600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$18700(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$18800(Landroid/net/wifi/WifiStateMachine;)Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@2
    return-object v0
.end method

.method static synthetic access$18900(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    return-object v0
.end method

.method static synthetic access$19000(Landroid/net/wifi/WifiStateMachine;Ljava/util/ArrayList;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->startTethering(Ljava/util/ArrayList;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$19100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetheringState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$19200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$19300(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetherToken:I

    #@2
    return v0
.end method

.method static synthetic access$19304(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetherToken:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetherToken:I

    #@6
    return v0
.end method

.method static synthetic access$19400(Landroid/net/wifi/WifiStateMachine;Ljava/util/ArrayList;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->isWifiTethered(Ljava/util/ArrayList;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$19500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mTetheredState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$19600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$19700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$19800(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->stopTethering()V

    #@3
    return-void
.end method

.method static synthetic access$19900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStoppingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverLoadedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$20000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$20100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$20200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$20300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$20400(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/p2p/WifiP2pManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@2
    return-object v0
.end method

.method static synthetic access$2402(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/p2p/WifiP2pManager;)Landroid/net/wifi/p2p/WifiP2pManager;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@2
    return-object p1
.end method

.method static synthetic access$2500(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2600(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Landroid/net/wifi/WifiStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->setWifiState(I)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Landroid/net/wifi/WifiStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->setWifiApState(I)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$3000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverFailedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$3100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$3300(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverUnloadingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$3400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3500(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mP2pSupported:Z

    #@2
    return v0
.end method

.method static synthetic access$3600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiMonitor:Landroid/net/wifi/WifiMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$3700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStartingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$3800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSoftApStartingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@2
    return-object v0
.end method

.method static synthetic access$4000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$4200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4600(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$4700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverLoadingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$4800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4900(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mPrimaryDeviceType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$5000(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantRestartCount:I

    #@2
    return v0
.end method

.method static synthetic access$5002(Landroid/net/wifi/WifiStateMachine;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput p1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantRestartCount:I

    #@2
    return p1
.end method

.method static synthetic access$5004(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantRestartCount:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantRestartCount:I

    #@6
    return v0
.end method

.method static synthetic access$5100(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/SupplicantStateTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStateTracker:Landroid/net/wifi/SupplicantStateTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$5200(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$5202(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$5300(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@2
    return v0
.end method

.method static synthetic access$5302(Landroid/net/wifi/WifiStateMachine;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput p1, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@2
    return p1
.end method

.method static synthetic access$5402(Landroid/net/wifi/WifiStateMachine;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput p1, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@2
    return p1
.end method

.method static synthetic access$5502(Landroid/net/wifi/WifiStateMachine;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput p1, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel_LG:I

    #@2
    return p1
.end method

.method static synthetic access$5600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    return-object v0
.end method

.method static synthetic access$5800(Landroid/net/wifi/WifiStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->sendSupplicantConnectionChangedBroadcast(Z)V

    #@3
    return-void
.end method

.method static synthetic access$5900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/net/wifi/WifiStateMachine;)Lcom/lge/wifi_iface/WifiServiceExtIface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    return-object v0
.end method

.method static synthetic access$6000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6300(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$6400(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mIsScanMode:Z

    #@2
    return v0
.end method

.method static synthetic access$6402(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mIsScanMode:Z

    #@2
    return p1
.end method

.method static synthetic access$6500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$6600(Landroid/net/wifi/WifiStateMachine;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-wide v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantScanIntervalMs:J

    #@2
    return-wide v0
.end method

.method static synthetic access$6602(Landroid/net/wifi/WifiStateMachine;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-wide p1, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantScanIntervalMs:J

    #@2
    return-wide p1
.end method

.method static synthetic access$6700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWaitForP2pDisableState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$6800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStoppingState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiP2pChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$7000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7100(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->handleNetworkDisconnect()V

    #@3
    return-void
.end method

.method static synthetic access$7200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7400(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->setScanResults(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$7500(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->sendScanResultsAvailableBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$7600(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanResultIsPending:Z

    #@2
    return v0
.end method

.method static synthetic access$7602(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mScanResultIsPending:Z

    #@2
    return p1
.end method

.method static synthetic access$7700(Landroid/net/wifi/WifiStateMachine;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-wide v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastEnableAllNetworksTime:J

    #@2
    return-wide v0
.end method

.method static synthetic access$7702(Landroid/net/wifi/WifiStateMachine;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-wide p1, p0, Landroid/net/wifi/WifiStateMachine;->mLastEnableAllNetworksTime:J

    #@2
    return-wide p1
.end method

.method static synthetic access$7800(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStopFailureToken:I

    #@2
    return v0
.end method

.method static synthetic access$7804(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStopFailureToken:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStopFailureToken:I

    #@6
    return v0
.end method

.method static synthetic access$7900(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mBluetoothConnectionActive:Z

    #@2
    return v0
.end method

.method static synthetic access$8000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$802(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mBluetoothConnectionActive:Z

    #@2
    return p1
.end method

.method static synthetic access$8100(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$8200(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartToken:I

    #@2
    return v0
.end method

.method static synthetic access$8204(Landroid/net/wifi/WifiStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartToken:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStartToken:I

    #@6
    return v0
.end method

.method static synthetic access$8300(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)Landroid/net/wifi/SupplicantState;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->handleSupplicantStateChange(Landroid/os/Message;)Landroid/net/wifi/SupplicantState;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$8400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDriverStoppedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$8600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$8800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->processMessageAtDriverStartedState(Landroid/os/Message;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$8902(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mIsRunning:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3}, Landroid/net/wifi/WifiStateMachine;->replyToMessage(Landroid/os/Message;II)V

    #@3
    return-void
.end method

.method static synthetic access$9000(Landroid/net/wifi/WifiStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mInDelayedStop:Z

    #@2
    return v0
.end method

.method static synthetic access$9002(Landroid/net/wifi/WifiStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    iput-boolean p1, p0, Landroid/net/wifi/WifiStateMachine;->mInDelayedStop:Z

    #@2
    return p1
.end method

.method static synthetic access$9100(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->setCountryCode()V

    #@3
    return-void
.end method

.method static synthetic access$9200(Landroid/net/wifi/WifiStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->setFrequencyBand()V

    #@3
    return-void
.end method

.method static synthetic access$9300(Landroid/net/wifi/WifiStateMachine;Landroid/net/NetworkInfo$DetailedState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->setNetworkDetailedState(Landroid/net/NetworkInfo$DetailedState;)V

    #@3
    return-void
.end method

.method static synthetic access$9400(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFilteringMulticastV4Packets:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method static synthetic access$9500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScanModeState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$9600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$9700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectedState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$9800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$9900(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScreenBroadcastReceived:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method private checkAndSetConnectivityInstance()V
    .registers 3

    #@0
    .prologue
    .line 1437
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 1438
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "connectivity"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/net/ConnectivityManager;

    #@e
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@10
    .line 1440
    :cond_10
    return-void
.end method

.method private configureLinkProperties()V
    .registers 4

    #@0
    .prologue
    .line 1870
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    iget v1, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@4
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiConfigStore;->isUsingStaticIp(I)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    .line 1871
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@c
    iget v1, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@e
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiConfigStore;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@14
    .line 1878
    :goto_14
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@16
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@18
    invoke-virtual {v0, v1}, Landroid/net/LinkProperties;->setInterfaceName(Ljava/lang/String;)V

    #@1b
    .line 1883
    return-void

    #@1c
    .line 1873
    :cond_1c
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@1e
    monitor-enter v1

    #@1f
    .line 1874
    :try_start_1f
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@21
    invoke-virtual {v0}, Landroid/net/DhcpInfoInternal;->makeLinkProperties()Landroid/net/LinkProperties;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@27
    .line 1875
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_1f .. :try_end_28} :catchall_36

    #@28
    .line 1876
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@2a
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2c
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@2e
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiConfigStore;->getProxyProperties(I)Landroid/net/ProxyProperties;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v0, v1}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@35
    goto :goto_14

    #@36
    .line 1875
    :catchall_36
    move-exception v0

    #@37
    :try_start_37
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_36

    #@38
    throw v0
.end method

.method private connectedStateEnter()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 4880
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@3
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@5
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@8
    move-result v1

    #@9
    invoke-interface {v0, v1, v3}, Lcom/lge/wifi_iface/WifiServiceExtIface;->addWifiConnectionList(II)V

    #@c
    .line 4884
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@e
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@10
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@13
    move-result v1

    #@14
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@16
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@19
    move-result-object v2

    #@1a
    invoke-interface {v0, v1, v2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->updatePriorityNetwork(ILjava/util/List;)V

    #@1d
    .line 4887
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiStateMachine;->sendShowKTPayPopup(Z)V

    #@20
    .line 4888
    return-void
.end method

.method private fetchPktcntNative(Landroid/net/wifi/RssiPacketCountInfo;)V
    .registers 11
    .parameter "info"

    #@0
    .prologue
    .line 1849
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->pktcntPoll()Ljava/lang/String;

    #@5
    move-result-object v5

    #@6
    .line 1851
    .local v5, pktcntPoll:Ljava/lang/String;
    if-eqz v5, :cond_4e

    #@8
    .line 1852
    const-string v7, "\n"

    #@a
    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    .line 1853
    .local v4, lines:[Ljava/lang/String;
    move-object v0, v4

    #@f
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@10
    .local v2, len$:I
    const/4 v1, 0x0

    #@11
    .local v1, i$:I
    :goto_11
    if-ge v1, v2, :cond_4e

    #@13
    aget-object v3, v0, v1

    #@15
    .line 1854
    .local v3, line:Ljava/lang/String;
    const-string v7, "="

    #@17
    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    .line 1855
    .local v6, prop:[Ljava/lang/String;
    array-length v7, v6

    #@1c
    const/4 v8, 0x2

    #@1d
    if-ge v7, v8, :cond_22

    #@1f
    .line 1853
    :cond_1f
    :goto_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_11

    #@22
    .line 1857
    :cond_22
    const/4 v7, 0x0

    #@23
    :try_start_23
    aget-object v7, v6, v7

    #@25
    const-string v8, "TXGOOD"

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v7

    #@2b
    if-eqz v7, :cond_39

    #@2d
    .line 1858
    const/4 v7, 0x1

    #@2e
    aget-object v7, v6, v7

    #@30
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@33
    move-result v7

    #@34
    iput v7, p1, Landroid/net/wifi/RssiPacketCountInfo;->txgood:I

    #@36
    goto :goto_1f

    #@37
    .line 1862
    :catch_37
    move-exception v7

    #@38
    goto :goto_1f

    #@39
    .line 1859
    :cond_39
    const/4 v7, 0x0

    #@3a
    aget-object v7, v6, v7

    #@3c
    const-string v8, "TXBAD"

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_1f

    #@44
    .line 1860
    const/4 v7, 0x1

    #@45
    aget-object v7, v6, v7

    #@47
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4a
    move-result v7

    #@4b
    iput v7, p1, Landroid/net/wifi/RssiPacketCountInfo;->txbad:I
    :try_end_4d
    .catch Ljava/lang/NumberFormatException; {:try_start_23 .. :try_end_4d} :catch_37

    #@4d
    goto :goto_1f

    #@4e
    .line 1867
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #line:Ljava/lang/String;
    .end local v4           #lines:[Ljava/lang/String;
    .end local v6           #prop:[Ljava/lang/String;
    :cond_4e
    return-void
.end method

.method private fetchRssiAndLinkSpeedNative()V
    .registers 16

    #@0
    .prologue
    const/4 v14, -0x1

    #@1
    const/16 v13, -0xc8

    #@3
    .line 1774
    const/4 v6, -0x1

    #@4
    .line 1775
    .local v6, newRssi:I
    const/4 v5, -0x1

    #@5
    .line 1777
    .local v5, newLinkSpeed:I
    iget-object v11, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7
    invoke-virtual {v11}, Landroid/net/wifi/WifiNative;->signalPoll()Ljava/lang/String;

    #@a
    move-result-object v10

    #@b
    .line 1779
    .local v10, signalPoll:Ljava/lang/String;
    if-eqz v10, :cond_65

    #@d
    .line 1780
    const-string v11, "\n"

    #@f
    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 1781
    .local v4, lines:[Ljava/lang/String;
    move-object v0, v4

    #@14
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@15
    .local v2, len$:I
    const/4 v1, 0x0

    #@16
    .local v1, i$:I
    :goto_16
    if-ge v1, v2, :cond_65

    #@18
    aget-object v3, v0, v1

    #@1a
    .line 1782
    .local v3, line:Ljava/lang/String;
    const-string v11, "="

    #@1c
    invoke-virtual {v3, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    .line 1783
    .local v9, prop:[Ljava/lang/String;
    array-length v11, v9

    #@21
    const/4 v12, 0x2

    #@22
    if-ge v11, v12, :cond_27

    #@24
    .line 1781
    :cond_24
    :goto_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_16

    #@27
    .line 1785
    :cond_27
    const/4 v11, 0x0

    #@28
    :try_start_28
    aget-object v11, v9, v11

    #@2a
    const-string v12, "RSSI"

    #@2c
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v11

    #@30
    if-eqz v11, :cond_3a

    #@32
    .line 1786
    const/4 v11, 0x1

    #@33
    aget-object v11, v9, v11

    #@35
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@38
    move-result v6

    #@39
    goto :goto_24

    #@3a
    .line 1787
    :cond_3a
    const/4 v11, 0x0

    #@3b
    aget-object v11, v9, v11

    #@3d
    const-string v12, "LINKSPEED"

    #@3f
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v11

    #@43
    if-eqz v11, :cond_24

    #@45
    .line 1788
    const/4 v11, 0x1

    #@46
    aget-object v11, v9, v11

    #@48
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4b
    move-result v5

    #@4c
    .line 1791
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@4f
    move-result-object v11

    #@50
    const-string v12, "SPR"

    #@52
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v11

    #@56
    if-eqz v11, :cond_24

    #@58
    .line 1792
    const-string/jumbo v11, "wlan.linkspeed"

    #@5b
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5e
    move-result-object v12

    #@5f
    invoke-static {v11, v12}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_62
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_62} :catch_63

    #@62
    goto :goto_24

    #@63
    .line 1796
    :catch_63
    move-exception v11

    #@64
    goto :goto_24

    #@65
    .line 1802
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #line:Ljava/lang/String;
    .end local v4           #lines:[Ljava/lang/String;
    .end local v9           #prop:[Ljava/lang/String;
    :cond_65
    if-eq v6, v14, :cond_b1

    #@67
    if-ge v13, v6, :cond_b1

    #@69
    const/16 v11, 0x100

    #@6b
    if-ge v6, v11, :cond_b1

    #@6d
    .line 1806
    if-lez v6, :cond_71

    #@6f
    add-int/lit16 v6, v6, -0x100

    #@71
    .line 1807
    :cond_71
    iget-object v11, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@73
    invoke-virtual {v11, v6}, Landroid/net/wifi/WifiInfo;->setRssi(I)V

    #@76
    .line 1818
    const/4 v11, 0x5

    #@77
    invoke-static {v6, v11}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    #@7a
    move-result v7

    #@7b
    .line 1821
    .local v7, newSignalLevel:I
    const/4 v11, 0x4

    #@7c
    invoke-static {v6, v11}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    #@7f
    move-result v8

    #@80
    .line 1824
    .local v8, newSignalLevel_LG:I
    iget v11, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@82
    if-eq v7, v11, :cond_93

    #@84
    .line 1825
    invoke-direct {p0, v6}, Landroid/net/wifi/WifiStateMachine;->sendRssiChangeBroadcast(I)V

    #@87
    .line 1832
    :cond_87
    :goto_87
    iput v7, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@89
    .line 1834
    iput v8, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel_LG:I

    #@8b
    .line 1840
    .end local v7           #newSignalLevel:I
    .end local v8           #newSignalLevel_LG:I
    :goto_8b
    if-eq v5, v14, :cond_92

    #@8d
    .line 1841
    iget-object v11, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@8f
    invoke-virtual {v11, v5}, Landroid/net/wifi/WifiInfo;->setLinkSpeed(I)V

    #@92
    .line 1843
    :cond_92
    return-void

    #@93
    .line 1827
    .restart local v7       #newSignalLevel:I
    .restart local v8       #newSignalLevel_LG:I
    :cond_93
    iget v11, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel_LG:I

    #@95
    if-eq v8, v11, :cond_87

    #@97
    .line 1828
    new-instance v11, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v12, "LG_RSSI UPDATE newRssi = "

    #@9e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v11

    #@a2
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v11

    #@a6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v11

    #@aa
    invoke-direct {p0, v11}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@ad
    .line 1829
    invoke-direct {p0, v6}, Landroid/net/wifi/WifiStateMachine;->sendRssiChangeBroadcast(I)V

    #@b0
    goto :goto_87

    #@b1
    .line 1837
    .end local v7           #newSignalLevel:I
    .end local v8           #newSignalLevel_LG:I
    :cond_b1
    iget-object v11, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@b3
    invoke-virtual {v11, v13}, Landroid/net/wifi/WifiInfo;->setRssi(I)V

    #@b6
    goto :goto_8b
.end method

.method private getMaxDhcpRetries()I
    .registers 4

    #@0
    .prologue
    .line 1886
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string/jumbo v1, "wifi_max_dhcp_retry_count"

    #@9
    const/16 v2, 0x9

    #@b
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e
    move-result v0

    #@f
    return v0
.end method

.method private getNetworkDetailedState()Landroid/net/NetworkInfo$DetailedState;
    .registers 2

    #@0
    .prologue
    .line 1969
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private handleFailedIpConfiguration()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2143
    const-string v1, "IP configuration failed"

    #@3
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@6
    .line 2145
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiInfo;->setInetAddress(Ljava/net/InetAddress;)V

    #@c
    .line 2146
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@e
    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiInfo;->setMeteredHint(Z)V

    #@11
    .line 2147
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@13
    if-eqz v1, :cond_19

    #@15
    .line 2148
    const/4 v1, 0x1

    #@16
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->sendShowKTPayPopup(Z)V

    #@19
    .line 2154
    :cond_19
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->getMaxDhcpRetries()I

    #@1c
    move-result v0

    #@1d
    .line 2156
    .local v0, maxRetries:I
    if-lez v0, :cond_55

    #@1f
    iget v1, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@21
    add-int/lit8 v1, v1, 0x1

    #@23
    iput v1, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@25
    if-le v1, v0, :cond_55

    #@27
    .line 2157
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "Failed "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, " times, Disabling "

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@4b
    .line 2159
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@4d
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@4f
    const/4 v3, 0x2

    #@50
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiConfigStore;->disableNetwork(II)Z

    #@53
    .line 2161
    iput v4, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@55
    .line 2167
    :cond_55
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@57
    invoke-virtual {v1}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@5a
    .line 2168
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@5c
    invoke-virtual {v1}, Landroid/net/wifi/WifiNative;->reconnect()Z

    #@5f
    .line 2169
    return-void
.end method

.method private handleNetworkDisconnect()V
    .registers 7

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2007
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@4
    if-eqz v1, :cond_11

    #@6
    .line 2010
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->handlePostDhcpSetup()V

    #@9
    .line 2011
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@b
    const v2, 0x30002

    #@e
    invoke-virtual {v1, v2}, Landroid/net/DhcpStateMachine;->sendMessage(I)V

    #@11
    .line 2015
    :cond_11
    :try_start_11
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@13
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@15
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->clearInterfaceAddresses(Ljava/lang/String;)V

    #@18
    .line 2016
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@1a
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;
	
	const-string v2, "default"

    #@1c
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->disableIpv6(Ljava/lang/String;)V
	
	iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_1f} :catch_73

    #@1f
    .line 2022
    :goto_1f
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@21
    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiInfo;->setInetAddress(Ljava/net/InetAddress;)V

    #@24
    .line 2023
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@26
    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiInfo;->setBSSID(Ljava/lang/String;)V

    #@29
    .line 2024
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@2b
    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiInfo;->setSSID(Landroid/net/wifi/WifiSsid;)V

    #@2e
    .line 2025
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@30
    invoke-virtual {v1, v5}, Landroid/net/wifi/WifiInfo;->setNetworkId(I)V

    #@33
    .line 2026
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@35
    const/16 v2, -0xc8

    #@37
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiInfo;->setRssi(I)V

    #@3a
    .line 2027
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@3c
    invoke-virtual {v1, v5}, Landroid/net/wifi/WifiInfo;->setLinkSpeed(I)V

    #@3f
    .line 2028
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@41
    const/4 v2, 0x0

    #@42
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiInfo;->setMeteredHint(Z)V

    #@45
    .line 2030
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@47
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->setNetworkDetailedState(Landroid/net/NetworkInfo$DetailedState;)V

    #@4a
    .line 2031
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@4c
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@4e
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@50
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiConfigStore;->updateStatus(ILandroid/net/NetworkInfo$DetailedState;)V

    #@53
    .line 2034
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@55
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->sendNetworkStateChangeBroadcast(Ljava/lang/String;)V

    #@58
    .line 2037
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@5a
    invoke-virtual {v1}, Landroid/net/LinkProperties;->clear()V

    #@5d
    .line 2039
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@5f
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@61
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiConfigStore;->isUsingStaticIp(I)Z

    #@64
    move-result v1

    #@65
    if-nez v1, :cond_6e

    #@67
    .line 2040
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@69
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@6b
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiConfigStore;->clearIpConfiguration(I)V

    #@6e
    .line 2043
    :cond_6e
    iput-object v4, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@70
    .line 2044
    iput v5, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@72
    .line 2045
    return-void

    #@73
    .line 2017
    :catch_73
    move-exception v0

    #@74
    .line 2018
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v2, "Failed to clear addresses or disable ipv6"

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v1

    #@87
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@8a
    goto :goto_1f
.end method

.method private handleScreenStateChanged(Z)V
    .registers 8
    .parameter "screenOn"

    #@0
    .prologue
    const v5, 0x20056

    #@3
    const/4 v1, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 1418
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiStateMachine;->enableRssiPolling(Z)V

    #@8
    .line 1419
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mBackgroundScanSupported:Z

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 1420
    if-nez p1, :cond_2e

    #@e
    move v0, v1

    #@f
    :goto_f
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->enableBackgroundScanCommand(Z)V

    #@12
    .line 1423
    :cond_12
    if-eqz p1, :cond_17

    #@14
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->enableAllNetworks()V

    #@17
    .line 1424
    :cond_17
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@19
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_28

    #@1f
    .line 1425
    if-eqz p1, :cond_30

    #@21
    .line 1426
    invoke-virtual {p0, v5, v2, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@28
    .line 1433
    :cond_28
    :goto_28
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mScreenBroadcastReceived:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2a
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@2d
    .line 1434
    return-void

    #@2e
    :cond_2e
    move v0, v2

    #@2f
    .line 1420
    goto :goto_f

    #@30
    .line 1429
    :cond_30
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendWakeLock:Landroid/os/PowerManager$WakeLock;

    #@32
    const-wide/16 v3, 0x7d0

    #@34
    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@37
    .line 1430
    invoke-virtual {p0, v5, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@3e
    goto :goto_28
.end method

.method private handleSuccessfulIpConfiguration(Landroid/net/DhcpInfoInternal;)V
    .registers 9
    .parameter "dhcpInfoInternal"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 2090
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@3
    monitor-enter v6

    #@4
    .line 2091
    :try_start_4
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@6
    .line 2092
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_4d

    #@7
    .line 2093
    iput v5, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@9
    .line 2095
    iput v5, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel_LG:I

    #@b
    .line 2097
    const/4 v5, 0x0

    #@c
    iput v5, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@e
    .line 2100
    const/4 v0, 0x1

    #@f
    .line 2102
    .local v0, IsDhcpValid:Z
    :try_start_f
    iget-object v5, p1, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@11
    invoke-static {v5}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@14
    move-result-object v2

    #@15
    .line 2103
    .local v2, address:Ljava/net/InetAddress;
    if-eqz v2, :cond_2b

    #@17
    iget v5, p1, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@19
    if-ltz v5, :cond_2b

    #@1b
    instance-of v5, v2, Ljava/net/Inet4Address;

    #@1d
    if-eqz v5, :cond_25

    #@1f
    iget v5, p1, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@21
    const/16 v6, 0x20

    #@23
    if-gt v5, v6, :cond_2b

    #@25
    :cond_25
    iget v5, p1, Landroid/net/DhcpInfoInternal;->prefixLength:I
    :try_end_27
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_27} :catch_50

    #@27
    const/16 v6, 0x80

    #@29
    if-le v5, v6, :cond_2c

    #@2b
    .line 2106
    :cond_2b
    const/4 v0, 0x0

    #@2c
    .line 2112
    .end local v2           #address:Ljava/net/InetAddress;
    :cond_2c
    :goto_2c
    if-nez v0, :cond_53

    #@2e
    .line 2113
    new-instance v5, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v6, "dhcpinfo is not valid : "

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-direct {p0, v5}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@44
    .line 2114
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->handleFailedIpConfiguration()V

    #@47
    .line 2115
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectingState:Lcom/android/internal/util/State;

    #@49
    invoke-virtual {p0, v5}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@4c
    .line 2140
    :cond_4c
    :goto_4c
    return-void

    #@4d
    .line 2092
    .end local v0           #IsDhcpValid:Z
    :catchall_4d
    move-exception v5

    #@4e
    :try_start_4e
    monitor-exit v6
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4d

    #@4f
    throw v5

    #@50
    .line 2109
    .restart local v0       #IsDhcpValid:Z
    :catch_50
    move-exception v3

    #@51
    .line 2110
    .local v3, e:Ljava/lang/IllegalArgumentException;
    const/4 v0, 0x0

    #@52
    goto :goto_2c

    #@53
    .line 2120
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :cond_53
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@55
    iget v6, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@57
    invoke-virtual {v5, v6, p1}, Landroid/net/wifi/WifiConfigStore;->setIpConfiguration(ILandroid/net/DhcpInfoInternal;)V

    #@5a
    .line 2121
    iget-object v5, p1, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@5c
    invoke-static {v5}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@5f
    move-result-object v1

    #@60
    .line 2122
    .local v1, addr:Ljava/net/InetAddress;
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@62
    invoke-virtual {v5, v1}, Landroid/net/wifi/WifiInfo;->setInetAddress(Ljava/net/InetAddress;)V

    #@65
    .line 2123
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@67
    invoke-virtual {p1}, Landroid/net/DhcpInfoInternal;->hasMeteredHint()Z

    #@6a
    move-result v6

    #@6b
    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiInfo;->setMeteredHint(Z)V

    #@6e
    .line 2124
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->getNetworkDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@71
    move-result-object v5

    #@72
    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@74
    if-ne v5, v6, :cond_98

    #@76
    .line 2126
    invoke-virtual {p1}, Landroid/net/DhcpInfoInternal;->makeLinkProperties()Landroid/net/LinkProperties;

    #@79
    move-result-object v4

    #@7a
    .line 2127
    .local v4, linkProperties:Landroid/net/LinkProperties;
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@7c
    iget v6, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@7e
    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiConfigStore;->getProxyProperties(I)Landroid/net/ProxyProperties;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@85
    .line 2128
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@87
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->setInterfaceName(Ljava/lang/String;)V

    #@8a
    .line 2129
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@8c
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v5

    #@90
    if-nez v5, :cond_4c

    #@92
    .line 2134
    iput-object v4, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@94
    .line 2135
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->sendLinkConfigurationChangedBroadcast()V

    #@97
    goto :goto_4c

    #@98
    .line 2138
    .end local v4           #linkProperties:Landroid/net/LinkProperties;
    :cond_98
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->configureLinkProperties()V

    #@9b
    goto :goto_4c
.end method

.method private handleSupplicantStateChange(Landroid/os/Message;)Landroid/net/wifi/SupplicantState;
    .registers 6
    .parameter "message"

    #@0
    .prologue
    .line 1974
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v1, Landroid/net/wifi/StateChangeResult;

    #@4
    .line 1975
    .local v1, stateChangeResult:Landroid/net/wifi/StateChangeResult;
    iget-object v0, v1, Landroid/net/wifi/StateChangeResult;->state:Landroid/net/wifi/SupplicantState;

    #@6
    .line 1980
    .local v0, state:Landroid/net/wifi/SupplicantState;
    const v2, 0xc367

    #@9
    invoke-virtual {v0}, Landroid/net/wifi/SupplicantState;->ordinal()I

    #@c
    move-result v3

    #@d
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(II)I

    #@10
    .line 1981
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@12
    invoke-virtual {v2, v0}, Landroid/net/wifi/WifiInfo;->setSupplicantState(Landroid/net/wifi/SupplicantState;)V

    #@15
    .line 1983
    invoke-static {v0}, Landroid/net/wifi/SupplicantState;->isConnecting(Landroid/net/wifi/SupplicantState;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_3a

    #@1b
    .line 1984
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@1d
    iget v3, v1, Landroid/net/wifi/StateChangeResult;->networkId:I

    #@1f
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiInfo;->setNetworkId(I)V

    #@22
    .line 1989
    :goto_22
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@24
    iget-object v3, v1, Landroid/net/wifi/StateChangeResult;->BSSID:Ljava/lang/String;

    #@26
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiInfo;->setBSSID(Ljava/lang/String;)V

    #@29
    .line 1990
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@2b
    iget-object v3, v1, Landroid/net/wifi/StateChangeResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@2d
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiInfo;->setSSID(Landroid/net/wifi/WifiSsid;)V

    #@30
    .line 1992
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mSupplicantStateTracker:Landroid/net/wifi/SupplicantStateTracker;

    #@32
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v2, v3}, Landroid/net/wifi/SupplicantStateTracker;->sendMessage(Landroid/os/Message;)V

    #@39
    .line 1994
    return-object v0

    #@3a
    .line 1986
    :cond_3a
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@3c
    const/4 v3, -0x1

    #@3d
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiInfo;->setNetworkId(I)V

    #@40
    goto :goto_22
.end method

.method private isWifiTethered(Ljava/util/ArrayList;)Z
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1547
    .local p1, active:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->checkAndSetConnectivityInstance()V

    #@3
    .line 1549
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@5
    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    #@8
    move-result-object v6

    #@9
    .line 1550
    .local v6, wifiRegexs:[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    :cond_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v7

    #@11
    if-eqz v7, :cond_2b

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Ljava/lang/String;

    #@19
    .line 1551
    .local v3, intf:Ljava/lang/String;
    move-object v0, v6

    #@1a
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@1b
    .local v4, len$:I
    const/4 v2, 0x0

    #@1c
    .local v2, i$:I
    :goto_1c
    if-ge v2, v4, :cond_d

    #@1e
    aget-object v5, v0, v2

    #@20
    .line 1552
    .local v5, regex:Ljava/lang/String;
    invoke-virtual {v3, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_28

    #@26
    .line 1553
    const/4 v7, 0x1

    #@27
    .line 1558
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #intf:Ljava/lang/String;
    .end local v4           #len$:I
    .end local v5           #regex:Ljava/lang/String;
    :goto_27
    return v7

    #@28
    .line 1551
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #intf:Ljava/lang/String;
    .restart local v4       #len$:I
    .restart local v5       #regex:Ljava/lang/String;
    :cond_28
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_1c

    #@2b
    .line 1558
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #intf:Ljava/lang/String;
    .end local v4           #len$:I
    .end local v5           #regex:Ljava/lang/String;
    :cond_2b
    const/4 v7, 0x0

    #@2c
    goto :goto_27
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 4825
    const-string v0, "WifiStateMachine"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 4826
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 4829
    const-string v0, "WifiStateMachine"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 4830
    return-void
.end method

.method private obtainMessageWithArg2(Landroid/os/Message;)Landroid/os/Message;
    .registers 4
    .parameter "srcMsg"

    #@0
    .prologue
    .line 4819
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 4820
    .local v0, msg:Landroid/os/Message;
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@6
    iput v1, v0, Landroid/os/Message;->arg2:I

    #@8
    .line 4821
    return-object v0
.end method

.method private processMessageAtDriverStartedState(Landroid/os/Message;)Z
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 4834
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 4835
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a
    invoke-interface {v0, p1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->processMessageAtDriverStartedState(Landroid/os/Message;)Z

    #@d
    move-result v0

    #@e
    .line 4837
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method private replyToMessage(Landroid/os/Message;I)V
    .registers 5
    .parameter "msg"
    .parameter "what"

    #@0
    .prologue
    .line 4790
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 4794
    :goto_4
    return-void

    #@5
    .line 4791
    :cond_5
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessageWithArg2(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 4792
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 4793
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@d
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@10
    goto :goto_4
.end method

.method private replyToMessage(Landroid/os/Message;II)V
    .registers 6
    .parameter "msg"
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 4797
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 4802
    :goto_4
    return-void

    #@5
    .line 4798
    :cond_5
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessageWithArg2(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 4799
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 4800
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 4801
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@12
    goto :goto_4
.end method

.method private replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V
    .registers 6
    .parameter "msg"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 4805
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 4810
    :goto_4
    return-void

    #@5
    .line 4806
    :cond_5
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessageWithArg2(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 4807
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 4808
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 4809
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@f
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@12
    goto :goto_4
.end method

.method private sendHS20APBroadcast(Ljava/lang/String;I)V
    .registers 6
    .parameter "bssid"
    .parameter "roamingInd"

    #@0
    .prologue
    .line 1934
    const-string v1, "WifiStateMachine"

    #@2
    const-string v2, "[PASSPOINT]  : sendHS20APBroadcast"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1935
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "android.net.wifi.HS20_AP_EVENT"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 1936
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@13
    .line 1937
    const-string/jumbo v1, "roamingInd"

    #@16
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@19
    .line 1938
    if-eqz p1, :cond_20

    #@1b
    .line 1939
    const-string v1, "bssid"

    #@1d
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@20
    .line 1940
    :cond_20
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@22
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@24
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@27
    .line 1941
    return-void
.end method

.method private sendHS20TryToConnection(Ljava/lang/String;)V
    .registers 5
    .parameter "bssid"

    #@0
    .prologue
    .line 1944
    const-string v1, "WifiStateMachine"

    #@2
    const-string v2, "[PASSPOINT]  : sendHS20TryToConnection"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1945
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "android.net.wifi.HS20_TRY_CONNECTION"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 1946
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@13
    .line 1947
    if-eqz p1, :cond_1a

    #@15
    .line 1948
    const-string v1, "bssid"

    #@17
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1a
    .line 1949
    :cond_1a
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@1c
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1e
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@21
    .line 1950
    return-void
.end method

.method private sendLinkConfigurationChangedBroadcast()V
    .registers 5

    #@0
    .prologue
    .line 1919
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1920
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 1921
    const-string/jumbo v1, "linkProperties"

    #@f
    new-instance v2, Landroid/net/LinkProperties;

    #@11
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@13
    invoke-direct {v2, v3}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@19
    .line 1922
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@1b
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1d
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@20
    .line 1923
    return-void
.end method

.method private sendNetworkStateChangeBroadcast(Ljava/lang/String;)V
    .registers 6
    .parameter "bssid"

    #@0
    .prologue
    .line 1905
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1906
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 1907
    const-string/jumbo v1, "networkInfo"

    #@f
    new-instance v2, Landroid/net/NetworkInfo;

    #@11
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@13
    invoke-direct {v2, v3}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@19
    .line 1908
    const-string/jumbo v1, "linkProperties"

    #@1c
    new-instance v2, Landroid/net/LinkProperties;

    #@1e
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@20
    invoke-direct {v2, v3}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@26
    .line 1909
    if-eqz p1, :cond_2d

    #@28
    .line 1910
    const-string v1, "bssid"

    #@2a
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2d
    .line 1911
    :cond_2d
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2f
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@32
    move-result-object v1

    #@33
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    #@35
    if-eq v1, v2, :cond_41

    #@37
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@39
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@3c
    move-result-object v1

    #@3d
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@3f
    if-ne v1, v2, :cond_4e

    #@41
    .line 1913
    :cond_41
    const-string/jumbo v1, "wifiInfo"

    #@44
    new-instance v2, Landroid/net/wifi/WifiInfo;

    #@46
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@48
    invoke-direct {v2, v3}, Landroid/net/wifi/WifiInfo;-><init>(Landroid/net/wifi/WifiInfo;)V

    #@4b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@4e
    .line 1915
    :cond_4e
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@50
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@52
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@55
    .line 1916
    return-void
.end method

.method private sendRssiChangeBroadcast(I)V
    .registers 5
    .parameter "newRssi"

    #@0
    .prologue
    .line 1898
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1899
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 1900
    const-string/jumbo v1, "newRssi"

    #@f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@12
    .line 1901
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@14
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@16
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@19
    .line 1902
    return-void
.end method

.method private sendScanResultsAvailableBroadcast()V
    .registers 4

    #@0
    .prologue
    .line 1892
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1893
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 1894
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@e
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@10
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@13
    .line 1895
    return-void
.end method

.method private sendSetWifiApState(I)V
    .registers 3
    .parameter "wifiApState"

    #@0
    .prologue
    .line 4960
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 4961
    packed-switch p1, :pswitch_data_20

    #@7
    .line 4977
    :cond_7
    :goto_7
    return-void

    #@8
    .line 4963
    :pswitch_8
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@a
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;->beginWifiApStop()V

    #@d
    goto :goto_7

    #@e
    .line 4967
    :pswitch_e
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@10
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;->endWifiApStop()V

    #@13
    goto :goto_7

    #@14
    .line 4970
    :pswitch_14
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@16
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;->beginWifiApStart()V

    #@19
    goto :goto_7

    #@1a
    .line 4973
    :pswitch_1a
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApDisableIfNotUsed:Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;

    #@1c
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;->endWifiApStart()V

    #@1f
    goto :goto_7

    #@20
    .line 4961
    :pswitch_data_20
    .packed-switch 0xa
        :pswitch_8
        :pswitch_e
        :pswitch_14
        :pswitch_1a
        :pswitch_e
    .end packed-switch
.end method

.method private sendSetWifiState(I)V
    .registers 8
    .parameter "wifiState"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    .line 4897
    packed-switch p1, :pswitch_data_dc

    #@5
    .line 4923
    :cond_5
    :goto_5
    :pswitch_5
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiOffloading()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_ae

    #@b
    .line 4924
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@11
    .line 4925
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@13
    if-eqz v0, :cond_a5

    #@15
    .line 4926
    if-ne p1, v5, :cond_68

    #@17
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@19
    invoke-interface {v0}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->isOffloadingTimer_on()Z

    #@1c
    move-result v0

    #@1d
    if-ne v0, v4, :cond_68

    #@1f
    .line 4928
    const-string v0, "WifiStateMachine"

    #@21
    const-string v1, "Wifi Enabled ? then stop timer in WifiStateMachine.java"

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 4929
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@28
    invoke-interface {v0}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->stopWifioffloadTimer()V

    #@2b
    .line 4953
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 4900
    :pswitch_2c
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@2e
    if-eqz v0, :cond_3a

    #@30
    .line 4901
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@32
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->unregister()V

    #@35
    .line 4902
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@37
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->stopMonitoring()V

    #@3a
    .line 4904
    :cond_3a
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@3c
    if-eqz v0, :cond_5

    #@3e
    .line 4905
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@40
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiState()I

    #@43
    move-result v1

    #@44
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiApState()I

    #@47
    move-result v2

    #@48
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@4a
    invoke-virtual {v3}, Lcom/lge/wifi/WifiLgeSharedValues;->getP2pWifiState()I

    #@4d
    move-result v3

    #@4e
    invoke-interface {v0, v1, v2, v3}, Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;->register(III)V

    #@51
    goto :goto_5

    #@52
    .line 4911
    :pswitch_52
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@54
    if-eqz v0, :cond_5b

    #@56
    .line 4912
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@58
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->register()V

    #@5b
    .line 4914
    :cond_5b
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@5d
    if-eqz v0, :cond_64

    #@5f
    .line 4915
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiActivationWhileCharging:Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;

    #@61
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;->unregister()V

    #@64
    .line 4917
    :cond_64
    invoke-direct {p0, v4}, Landroid/net/wifi/WifiStateMachine;->sendShowKTPayPopup(Z)V

    #@67
    goto :goto_5

    #@68
    .line 4932
    :cond_68
    if-ne p1, v5, :cond_78

    #@6a
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@6c
    invoke-interface {v0}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->isOffloadingTimer_on()Z

    #@6f
    move-result v0

    #@70
    if-ne v0, v4, :cond_78

    #@72
    .line 4934
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@74
    invoke-interface {v0}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->disableWifioffloadTimerReminder()V

    #@77
    goto :goto_2b

    #@78
    .line 4937
    :cond_78
    if-ne p1, v4, :cond_2b

    #@7a
    .line 4938
    const-string v0, "WifiStateMachine"

    #@7c
    new-instance v1, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v2, "Wifi disabled ? then stop offloading start ("

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@89
    invoke-interface {v2}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->getWifiOffloadingStart()I

    #@8c
    move-result v2

    #@8d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    const-string v2, ")->(0)"

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v1

    #@9b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 4939
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@a0
    const/4 v1, 0x0

    #@a1
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->setWifiOffloadingStart(I)V

    #@a4
    goto :goto_2b

    #@a5
    .line 4943
    :cond_a5
    const-string v0, "WifiStateMachine"

    #@a7
    const-string v1, "WiFiOffloadingIfaceIface is null in WifiService.java"

    #@a9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    goto/16 :goto_2b

    #@ae
    .line 4948
    :cond_ae
    const/4 v0, 0x0

    #@af
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@b1
    .line 4949
    const-string v0, "WifiStateMachine"

    #@b3
    new-instance v1, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v2, "Couldn\'t get getWiFiOffloadingIfaceIface : \nuseWiFiOffloading() : "

    #@ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiOffloading()Z

    #@c1
    move-result v2

    #@c2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v1

    #@c6
    const-string v2, "\nCONFIG_LGE_WLAN_PATH : "

    #@c8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v1

    #@cc
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@ce
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v1

    #@d2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v1

    #@d6
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    goto/16 :goto_2b

    #@db
    .line 4897
    nop

    #@dc
    :pswitch_data_dc
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_5
        :pswitch_52
        :pswitch_2c
    .end packed-switch
.end method

.method private sendShowKTPayPopup(Z)V
    .registers 3
    .parameter "set"

    #@0
    .prologue
    .line 4980
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    invoke-interface {v0, p1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setShowKTPayPopup(Z)V

    #@5
    .line 4981
    return-void
.end method

.method private sendSupplicantConnectionChangedBroadcast(Z)V
    .registers 5
    .parameter "connected"

    #@0
    .prologue
    .line 1926
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1927
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 1928
    const-string v1, "connected"

    #@e
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@11
    .line 1929
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@13
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@15
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@18
    .line 1930
    return-void
.end method

.method private setCountryCode()V
    .registers 4

    #@0
    .prologue
    .line 1565
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "wifi_country_code"

    #@9
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 1567
    .local v0, countryCode:Ljava/lang/String;
    if-eqz v0, :cond_19

    #@f
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_19

    #@15
    .line 1568
    const/4 v1, 0x0

    #@16
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiStateMachine;->setCountryCode(Ljava/lang/String;Z)V

    #@19
    .line 1572
    :cond_19
    return-void
.end method

.method private setFrequencyBand()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1578
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3
    if-eqz v1, :cond_15

    #@5
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v1

    #@b
    const v2, 0x1110015

    #@e
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_15

    #@14
    .line 1586
    :goto_14
    return-void

    #@15
    .line 1583
    :cond_15
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@17
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v1

    #@1b
    const-string/jumbo v2, "wifi_frequency_band"

    #@1e
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@21
    move-result v0

    #@22
    .line 1585
    .local v0, band:I
    invoke-virtual {p0, v0, v3}, Landroid/net/wifi/WifiStateMachine;->setFrequencyBand(IZ)V

    #@25
    goto :goto_14
.end method

.method private setNetworkDetailedState(Landroid/net/NetworkInfo$DetailedState;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1963
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@5
    move-result-object v0

    #@6
    if-eq p1, v0, :cond_14

    #@8
    .line 1964
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@a
    const/4 v1, 0x0

    #@b
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@d
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, p1, v1, v2}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 1966
    :cond_14
    return-void
.end method

.method private setScanResults(Ljava/lang/String;)V
    .registers 22
    .parameter "scanResults"

    #@0
    .prologue
    .line 1692
    const-string v4, ""

    #@2
    .line 1693
    .local v4, bssid:Ljava/lang/String;
    const/4 v6, 0x0

    #@3
    .line 1694
    .local v6, level:I
    const/4 v7, 0x0

    #@4
    .line 1695
    .local v7, freq:I
    const-wide/16 v8, 0x0

    #@6
    .line 1696
    .local v8, tsf:J
    const-string v5, ""

    #@8
    .line 1697
    .local v5, flags:Ljava/lang/String;
    const/4 v3, 0x0

    #@9
    .line 1699
    .local v3, wifiSsid:Landroid/net/wifi/WifiSsid;
    if-nez p1, :cond_c

    #@b
    .line 1768
    :goto_b
    return-void

    #@c
    .line 1703
    :cond_c
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine;->mScanResultCache:Landroid/util/LruCache;

    #@10
    move-object/from16 v19, v0

    #@12
    monitor-enter v19

    #@13
    .line 1704
    :try_start_13
    new-instance v18, Ljava/util/ArrayList;

    #@15
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@18
    move-object/from16 v0, v18

    #@1a
    move-object/from16 v1, p0

    #@1c
    iput-object v0, v1, Landroid/net/wifi/WifiStateMachine;->mScanResults:Ljava/util/List;

    #@1e
    .line 1705
    const-string v18, "\n"

    #@20
    move-object/from16 v0, p1

    #@22
    move-object/from16 v1, v18

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@27
    move-result-object v16

    #@28
    .line 1707
    .local v16, lines:[Ljava/lang/String;
    move-object/from16 v10, v16

    #@2a
    .local v10, arr$:[Ljava/lang/String;
    array-length v14, v10

    #@2b
    .local v14, len$:I
    const/4 v12, 0x0

    #@2c
    .local v12, i$:I
    :goto_2c
    if-ge v12, v14, :cond_15d

    #@2e
    aget-object v15, v10, v12

    #@30
    .line 1708
    .local v15, line:Ljava/lang/String;
    const-string v18, "bssid="

    #@32
    move-object/from16 v0, v18

    #@34
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@37
    move-result v18

    #@38
    if-eqz v18, :cond_49

    #@3a
    .line 1709
    const-string v18, "bssid="

    #@3c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@3f
    move-result v18

    #@40
    move/from16 v0, v18

    #@42
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    .line 1707
    :cond_46
    :goto_46
    add-int/lit8 v12, v12, 0x1

    #@48
    goto :goto_2c

    #@49
    .line 1710
    :cond_49
    const-string v18, "freq="

    #@4b
    move-object/from16 v0, v18

    #@4d
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_50
    .catchall {:try_start_13 .. :try_end_50} :catchall_15a

    #@50
    move-result v18

    #@51
    if-eqz v18, :cond_67

    #@53
    .line 1712
    :try_start_53
    const-string v18, "freq="

    #@55
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@58
    move-result v18

    #@59
    move/from16 v0, v18

    #@5b
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@5e
    move-result-object v18

    #@5f
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_62
    .catchall {:try_start_53 .. :try_end_62} :catchall_15a
    .catch Ljava/lang/NumberFormatException; {:try_start_53 .. :try_end_62} :catch_64

    #@62
    move-result v7

    #@63
    goto :goto_46

    #@64
    .line 1713
    :catch_64
    move-exception v11

    #@65
    .line 1714
    .local v11, e:Ljava/lang/NumberFormatException;
    const/4 v7, 0x0

    #@66
    .line 1715
    goto :goto_46

    #@67
    .line 1716
    .end local v11           #e:Ljava/lang/NumberFormatException;
    :cond_67
    :try_start_67
    const-string/jumbo v18, "level="

    #@6a
    move-object/from16 v0, v18

    #@6c
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_6f
    .catchall {:try_start_67 .. :try_end_6f} :catchall_15a

    #@6f
    move-result v18

    #@70
    if-eqz v18, :cond_8b

    #@72
    .line 1718
    :try_start_72
    const-string/jumbo v18, "level="

    #@75
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@78
    move-result v18

    #@79
    move/from16 v0, v18

    #@7b
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@7e
    move-result-object v18

    #@7f
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_82
    .catchall {:try_start_72 .. :try_end_82} :catchall_15a
    .catch Ljava/lang/NumberFormatException; {:try_start_72 .. :try_end_82} :catch_88

    #@82
    move-result v6

    #@83
    .line 1722
    if-lez v6, :cond_46

    #@85
    add-int/lit16 v6, v6, -0x100

    #@87
    goto :goto_46

    #@88
    .line 1723
    :catch_88
    move-exception v11

    #@89
    .line 1724
    .restart local v11       #e:Ljava/lang/NumberFormatException;
    const/4 v6, 0x0

    #@8a
    .line 1725
    goto :goto_46

    #@8b
    .line 1726
    .end local v11           #e:Ljava/lang/NumberFormatException;
    :cond_8b
    :try_start_8b
    const-string/jumbo v18, "tsf="

    #@8e
    move-object/from16 v0, v18

    #@90
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_93
    .catchall {:try_start_8b .. :try_end_93} :catchall_15a

    #@93
    move-result v18

    #@94
    if-eqz v18, :cond_ac

    #@96
    .line 1728
    :try_start_96
    const-string/jumbo v18, "tsf="

    #@99
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@9c
    move-result v18

    #@9d
    move/from16 v0, v18

    #@9f
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@a2
    move-result-object v18

    #@a3
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_a6
    .catchall {:try_start_96 .. :try_end_a6} :catchall_15a
    .catch Ljava/lang/NumberFormatException; {:try_start_96 .. :try_end_a6} :catch_a8

    #@a6
    move-result-wide v8

    #@a7
    goto :goto_46

    #@a8
    .line 1729
    :catch_a8
    move-exception v11

    #@a9
    .line 1730
    .restart local v11       #e:Ljava/lang/NumberFormatException;
    const-wide/16 v8, 0x0

    #@ab
    .line 1731
    goto :goto_46

    #@ac
    .line 1732
    .end local v11           #e:Ljava/lang/NumberFormatException;
    :cond_ac
    :try_start_ac
    const-string v18, "flags="

    #@ae
    move-object/from16 v0, v18

    #@b0
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b3
    move-result v18

    #@b4
    if-eqz v18, :cond_c3

    #@b6
    .line 1733
    const-string v18, "flags="

    #@b8
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@bb
    move-result v18

    #@bc
    move/from16 v0, v18

    #@be
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@c1
    move-result-object v5

    #@c2
    goto :goto_46

    #@c3
    .line 1734
    :cond_c3
    const-string/jumbo v18, "ssid="

    #@c6
    move-object/from16 v0, v18

    #@c8
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@cb
    move-result v18

    #@cc
    if-eqz v18, :cond_e1

    #@ce
    .line 1735
    const-string/jumbo v18, "ssid="

    #@d1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    #@d4
    move-result v18

    #@d5
    move/from16 v0, v18

    #@d7
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@da
    move-result-object v18

    #@db
    invoke-static/range {v18 .. v18}, Landroid/net/wifi/WifiSsid;->createFromAsciiEncoded(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;

    #@de
    move-result-object v3

    #@df
    goto/16 :goto_46

    #@e1
    .line 1737
    :cond_e1
    const-string v18, "===="

    #@e3
    move-object/from16 v0, v18

    #@e5
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e8
    move-result v18

    #@e9
    if-eqz v18, :cond_46

    #@eb
    .line 1738
    if-eqz v4, :cond_139

    #@ed
    .line 1739
    if-eqz v3, :cond_143

    #@ef
    invoke-virtual {v3}, Landroid/net/wifi/WifiSsid;->toString()Ljava/lang/String;

    #@f2
    move-result-object v17

    #@f3
    .line 1740
    .local v17, ssid:Ljava/lang/String;
    :goto_f3
    new-instance v18, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    move-object/from16 v0, v18

    #@fa
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v18

    #@fe
    move-object/from16 v0, v18

    #@100
    move-object/from16 v1, v17

    #@102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v18

    #@106
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v13

    #@10a
    .line 1741
    .local v13, key:Ljava/lang/String;
    move-object/from16 v0, p0

    #@10c
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine;->mScanResultCache:Landroid/util/LruCache;

    #@10e
    move-object/from16 v18, v0

    #@110
    move-object/from16 v0, v18

    #@112
    invoke-virtual {v0, v13}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@115
    move-result-object v2

    #@116
    check-cast v2, Landroid/net/wifi/ScanResult;

    #@118
    .line 1742
    .local v2, scanResult:Landroid/net/wifi/ScanResult;
    if-eqz v2, :cond_149

    #@11a
    .line 1743
    iput v6, v2, Landroid/net/wifi/ScanResult;->level:I

    #@11c
    .line 1744
    iput-object v3, v2, Landroid/net/wifi/ScanResult;->wifiSsid:Landroid/net/wifi/WifiSsid;

    #@11e
    .line 1746
    if-eqz v3, :cond_146

    #@120
    invoke-virtual {v3}, Landroid/net/wifi/WifiSsid;->toString()Ljava/lang/String;

    #@123
    move-result-object v18

    #@124
    :goto_124
    move-object/from16 v0, v18

    #@126
    iput-object v0, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@128
    .line 1748
    iput-object v5, v2, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@12a
    .line 1749
    iput v7, v2, Landroid/net/wifi/ScanResult;->frequency:I

    #@12c
    .line 1750
    iput-wide v8, v2, Landroid/net/wifi/ScanResult;->timestamp:J

    #@12e
    .line 1757
    :goto_12e
    move-object/from16 v0, p0

    #@130
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine;->mScanResults:Ljava/util/List;

    #@132
    move-object/from16 v18, v0

    #@134
    move-object/from16 v0, v18

    #@136
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@139
    .line 1759
    .end local v2           #scanResult:Landroid/net/wifi/ScanResult;
    .end local v13           #key:Ljava/lang/String;
    .end local v17           #ssid:Ljava/lang/String;
    :cond_139
    const/4 v4, 0x0

    #@13a
    .line 1760
    const/4 v6, 0x0

    #@13b
    .line 1761
    const/4 v7, 0x0

    #@13c
    .line 1762
    const-wide/16 v8, 0x0

    #@13e
    .line 1763
    const-string v5, ""

    #@140
    .line 1764
    const/4 v3, 0x0

    #@141
    goto/16 :goto_46

    #@143
    .line 1739
    :cond_143
    const-string v17, "<unknown ssid>"

    #@145
    goto :goto_f3

    #@146
    .line 1746
    .restart local v2       #scanResult:Landroid/net/wifi/ScanResult;
    .restart local v13       #key:Ljava/lang/String;
    .restart local v17       #ssid:Ljava/lang/String;
    :cond_146
    const-string v18, "<unknown ssid>"

    #@148
    goto :goto_124

    #@149
    .line 1752
    :cond_149
    new-instance v2, Landroid/net/wifi/ScanResult;

    #@14b
    .end local v2           #scanResult:Landroid/net/wifi/ScanResult;
    invoke-direct/range {v2 .. v9}, Landroid/net/wifi/ScanResult;-><init>(Landroid/net/wifi/WifiSsid;Ljava/lang/String;Ljava/lang/String;IIJ)V

    #@14e
    .line 1755
    .restart local v2       #scanResult:Landroid/net/wifi/ScanResult;
    move-object/from16 v0, p0

    #@150
    iget-object v0, v0, Landroid/net/wifi/WifiStateMachine;->mScanResultCache:Landroid/util/LruCache;

    #@152
    move-object/from16 v18, v0

    #@154
    move-object/from16 v0, v18

    #@156
    invoke-virtual {v0, v13, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@159
    goto :goto_12e

    #@15a
    .line 1767
    .end local v2           #scanResult:Landroid/net/wifi/ScanResult;
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v12           #i$:I
    .end local v13           #key:Ljava/lang/String;
    .end local v14           #len$:I
    .end local v15           #line:Ljava/lang/String;
    .end local v16           #lines:[Ljava/lang/String;
    .end local v17           #ssid:Ljava/lang/String;
    :catchall_15a
    move-exception v18

    #@15b
    monitor-exit v19
    :try_end_15c
    .catchall {:try_start_ac .. :try_end_15c} :catchall_15a

    #@15c
    throw v18

    #@15d
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v12       #i$:I
    .restart local v14       #len$:I
    .restart local v16       #lines:[Ljava/lang/String;
    :cond_15d
    :try_start_15d
    monitor-exit v19
    :try_end_15e
    .catchall {:try_start_15d .. :try_end_15e} :catchall_15a

    #@15e
    goto/16 :goto_b
.end method

.method private setSuspendOptimizations(IZ)V
    .registers 5
    .parameter "reason"
    .parameter "enabled"

    #@0
    .prologue
    .line 1604
    if-eqz p2, :cond_a

    #@2
    .line 1605
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@4
    xor-int/lit8 v1, p1, -0x1

    #@6
    and-int/2addr v0, v1

    #@7
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@9
    .line 1610
    :goto_9
    return-void

    #@a
    .line 1607
    :cond_a
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@c
    or-int/2addr v0, p1

    #@d
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@f
    goto :goto_9
.end method

.method private setSuspendOptimizationsNative(IZ)V
    .registers 5
    .parameter "reason"
    .parameter "enabled"

    #@0
    .prologue
    .line 1590
    if-eqz p2, :cond_1c

    #@2
    .line 1591
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@4
    xor-int/lit8 v1, p1, -0x1

    #@6
    and-int/2addr v0, v1

    #@7
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@9
    .line 1593
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@b
    if-nez v0, :cond_1b

    #@d
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@f
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1b

    #@15
    .line 1594
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@17
    const/4 v1, 0x1

    #@18
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->setSuspendOptimizations(Z)Z

    #@1b
    .line 1600
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 1597
    :cond_1c
    iget v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@1e
    or-int/2addr v0, p1

    #@1f
    iput v0, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@21
    .line 1598
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@23
    const/4 v1, 0x0

    #@24
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->setSuspendOptimizations(Z)Z

    #@27
    goto :goto_1b
.end method

.method private setWifiApState(I)V
    .registers 7
    .parameter "wifiApState"

    #@0
    .prologue
    .line 1640
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v2

    #@6
    .line 1643
    .local v2, previousWifiApState:I
    const/16 v3, 0xd

    #@8
    if-ne p1, v3, :cond_3b

    #@a
    .line 1644
    :try_start_a
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@c
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->noteWifiOn()V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_45

    #@f
    .line 1653
    :cond_f
    :goto_f
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@11
    invoke-virtual {v3, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@14
    .line 1656
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@16
    if-eqz v3, :cond_1b

    #@18
    .line 1657
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->sendSetWifiApState(I)V

    #@1b
    .line 1660
    :cond_1b
    new-instance v1, Landroid/content/Intent;

    #@1d
    const-string v3, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    #@1f
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@22
    .line 1661
    .local v1, intent:Landroid/content/Intent;
    const/high16 v3, 0x800

    #@24
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@27
    .line 1662
    const-string/jumbo v3, "wifi_state"

    #@2a
    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2d
    .line 1663
    const-string/jumbo v3, "previous_wifi_state"

    #@30
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@33
    .line 1664
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@35
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@37
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3a
    .line 1665
    return-void

    #@3b
    .line 1645
    .end local v1           #intent:Landroid/content/Intent;
    :cond_3b
    const/16 v3, 0xb

    #@3d
    if-ne p1, v3, :cond_f

    #@3f
    .line 1646
    :try_start_3f
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@41
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->noteWifiOff()V
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_44} :catch_45

    #@44
    goto :goto_f

    #@45
    .line 1648
    :catch_45
    move-exception v0

    #@46
    .line 1649
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "Failed to note battery stats in wifi"

    #@48
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@4b
    goto :goto_f
.end method

.method private setWifiState(I)V
    .registers 7
    .parameter "wifiState"

    #@0
    .prologue
    .line 1613
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v2

    #@6
    .line 1616
    .local v2, previousWifiState:I
    const/4 v3, 0x3

    #@7
    if-ne p1, v3, :cond_3a

    #@9
    .line 1617
    :try_start_9
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@b
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->noteWifiOn()V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_43

    #@e
    .line 1625
    :cond_e
    :goto_e
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@10
    invoke-virtual {v3, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@13
    .line 1628
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@15
    if-eqz v3, :cond_1a

    #@17
    .line 1629
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiStateMachine;->sendSetWifiState(I)V

    #@1a
    .line 1632
    :cond_1a
    new-instance v1, Landroid/content/Intent;

    #@1c
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    #@1e
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@21
    .line 1633
    .local v1, intent:Landroid/content/Intent;
    const/high16 v3, 0x800

    #@23
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@26
    .line 1634
    const-string/jumbo v3, "wifi_state"

    #@29
    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2c
    .line 1635
    const-string/jumbo v3, "previous_wifi_state"

    #@2f
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@32
    .line 1636
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@34
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@36
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@39
    .line 1637
    return-void

    #@3a
    .line 1618
    .end local v1           #intent:Landroid/content/Intent;
    :cond_3a
    const/4 v3, 0x1

    #@3b
    if-ne p1, v3, :cond_e

    #@3d
    .line 1619
    :try_start_3d
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@3f
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->noteWifiOff()V
    :try_end_42
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_42} :catch_43

    #@42
    goto :goto_e

    #@43
    .line 1621
    :catch_43
    move-exception v0

    #@44
    .line 1622
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "Failed to note battery stats in wifi"

    #@46
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@49
    goto :goto_e
.end method

.method private startDhcpL2ConnectedState(ILjava/lang/String;)V
    .registers 6
    .parameter "networkId"
    .parameter "LastBssid"

    #@0
    .prologue
    .line 4850
    iput p1, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@2
    .line 4851
    iput-object p2, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@4
    .line 4853
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@6
    if-eqz v1, :cond_10

    #@8
    .line 4854
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@a
    const v2, 0x30002

    #@d
    invoke-virtual {v1, v2}, Landroid/net/DhcpStateMachine;->sendMessage(I)V

    #@10
    .line 4858
    :cond_10
    :try_start_10
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@12
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@14
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->clearInterfaceAddresses(Ljava/lang/String;)V

    #@17
    .line 4859
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@19
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

	const-string v2, "default"
	
    #@1b
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->disableIpv6(Ljava/lang/String;)V
	
	iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_1e} :catch_4b

    #@1e
    .line 4865
    :goto_1e
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@20
    invoke-virtual {v1}, Landroid/net/LinkProperties;->clear()V

    #@23
    .line 4867
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@25
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@27
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiConfigStore;->isUsingStaticIp(I)Z

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_34

    #@2d
    .line 4868
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2f
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@31
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiConfigStore;->clearIpConfiguration(I)V

    #@34
    .line 4871
    :cond_34
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@36
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@38
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiInfo;->setBSSID(Ljava/lang/String;)V

    #@3b
    .line 4872
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@3d
    iget v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@3f
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiInfo;->setNetworkId(I)V

    #@42
    .line 4873
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->sendLinkConfigurationChangedBroadcast()V

    #@45
    .line 4875
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mObtainingIpState:Lcom/android/internal/util/State;

    #@47
    invoke-virtual {p0, v1}, Landroid/net/wifi/WifiStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@4a
    .line 4876
    return-void

    #@4b
    .line 4860
    :catch_4b
    move-exception v0

    #@4c
    .line 4861
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v2, "Failed to clear addresses or disable ipv6"

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@62
    goto :goto_1e
.end method

.method private startSoftApWithConfig(Landroid/net/wifi/WifiConfiguration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 2179
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Landroid/net/wifi/WifiStateMachine$6;

    #@4
    invoke-direct {v1, p0, p1}, Landroid/net/wifi/WifiStateMachine$6;-><init>(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiConfiguration;)V

    #@7
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@d
    .line 2218
    return-void
.end method

.method private startTethering(Ljava/util/ArrayList;)Z
    .registers 17
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1444
    .local p1, available:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    #@1
    .line 1446
    .local v11, wifiAvailable:Z
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->checkAndSetConnectivityInstance()V

    #@4
    .line 1448
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@6
    invoke-virtual {v13}, Landroid/net/ConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    #@9
    move-result-object v12

    #@a
    .line 1450
    .local v12, wifiRegexs:[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v4

    #@e
    :cond_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v13

    #@12
    if-eqz v13, :cond_ea

    #@14
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v7

    #@18
    check-cast v7, Ljava/lang/String;

    #@1a
    .line 1451
    .local v7, intf:Ljava/lang/String;
    move-object v0, v12

    #@1b
    .local v0, arr$:[Ljava/lang/String;
    array-length v8, v0

    #@1c
    .local v8, len$:I
    const/4 v5, 0x0

    #@1d
    .local v5, i$:I
    :goto_1d
    if-ge v5, v8, :cond_e

    #@1f
    aget-object v10, v0, v5

    #@21
    .line 1452
    .local v10, regex:Ljava/lang/String;
    invoke-virtual {v7, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@24
    move-result v13

    #@25
    if-eqz v13, :cond_e6

    #@27
    .line 1454
    const/4 v6, 0x0

    #@28
    .line 1456
    .local v6, ifcg:Landroid/net/InterfaceConfiguration;
    :try_start_28
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@2a
    invoke-interface {v13, v7}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@2d
    move-result-object v6

    #@2e
    .line 1457
    if-eqz v6, :cond_8e

    #@30
    .line 1458
    const-string v3, "192.168.43.1"

    #@32
    .line 1459
    .local v3, gateway:Ljava/lang/String;
    const/16 v9, 0x18

    #@34
    .line 1460
    .local v9, maskPrefixLength:I
    sget-boolean v13, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@36
    if-eqz v13, :cond_ae

    #@38
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useMobileHotspot()Z

    #@3b
    move-result v13

    #@3c
    if-eqz v13, :cond_ae

    #@3e
    .line 1461
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@40
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@43
    move-result-object v13

    #@44
    const-string v14, "dhcp_server"

    #@46
    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@49
    move-result v1

    #@4a
    .line 1463
    .local v1, dhcp_server:I
    if-eqz v1, :cond_71

    #@4c
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@4e
    if-eqz v13, :cond_71

    #@50
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@52
    invoke-interface {v13}, Lcom/lge/wifi_iface/WifiServiceExtIface;->isTethered()Z

    #@55
    move-result v13

    #@56
    if-nez v13, :cond_71

    #@58
    .line 1464
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@5a
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5d
    move-result-object v13

    #@5e
    const-string v14, "gateway"

    #@60
    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    .line 1466
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@66
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@69
    move-result-object v13

    #@6a
    const-string/jumbo v14, "prefix_length"

    #@6d
    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@70
    move-result v9

    #@71
    .line 1491
    .end local v1           #dhcp_server:I
    :cond_71
    :goto_71
    new-instance v13, Landroid/net/LinkAddress;

    #@73
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@76
    move-result-object v14

    #@77
    invoke-direct {v13, v14, v9}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@7a
    invoke-virtual {v6, v13}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@7d
    .line 1493
    invoke-virtual {v6}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@80
    .line 1495
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@82
    invoke-interface {v13, v7, v6}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@85
    .line 1497
    sget-boolean v13, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@87
    if-nez v13, :cond_8e

    #@89
    .line 1498
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@8b
    invoke-interface {v13, v7}, Landroid/os/INetworkManagementService;->enableIpv6(Ljava/lang/String;)V
    :try_end_8e
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_8e} :catch_bf

    #@8e
    .line 1506
    .end local v3           #gateway:Ljava/lang/String;
    .end local v9           #maskPrefixLength:I
    :cond_8e
    iget-object v13, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@90
    invoke-virtual {v13, v7}, Landroid/net/ConnectivityManager;->tether(Ljava/lang/String;)I

    #@93
    move-result v13

    #@94
    if-eqz v13, :cond_e2

    #@96
    .line 1507
    new-instance v13, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v14, "Error tethering on "

    #@9d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v13

    #@a1
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v13

    #@a5
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v13

    #@a9
    invoke-direct {p0, v13}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@ac
    .line 1508
    const/4 v13, 0x0

    #@ad
    .line 1516
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v6           #ifcg:Landroid/net/InterfaceConfiguration;
    .end local v7           #intf:Ljava/lang/String;
    .end local v8           #len$:I
    .end local v10           #regex:Ljava/lang/String;
    :goto_ad
    return v13

    #@ae
    .line 1483
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v3       #gateway:Ljava/lang/String;
    .restart local v5       #i$:I
    .restart local v6       #ifcg:Landroid/net/InterfaceConfiguration;
    .restart local v7       #intf:Ljava/lang/String;
    .restart local v8       #len$:I
    .restart local v9       #maskPrefixLength:I
    .restart local v10       #regex:Ljava/lang/String;
    :cond_ae
    :try_start_ae
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@b1
    move-result v13

    #@b2
    if-eqz v13, :cond_71

    #@b4
    .line 1485
    const-string v3, "192.168.1.1"

    #@b6
    .line 1486
    const-string v13, "WifiStateMachine"

    #@b8
    const-string/jumbo v14, "startTethering wifi 1.1"

    #@bb
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_ae .. :try_end_be} :catch_bf

    #@be
    goto :goto_71

    #@bf
    .line 1501
    .end local v3           #gateway:Ljava/lang/String;
    .end local v9           #maskPrefixLength:I
    :catch_bf
    move-exception v2

    #@c0
    .line 1502
    .local v2, e:Ljava/lang/Exception;
    new-instance v13, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v14, "Error configuring interface "

    #@c7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v13

    #@cb
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v13

    #@cf
    const-string v14, ", :"

    #@d1
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v13

    #@d5
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v13

    #@d9
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v13

    #@dd
    invoke-direct {p0, v13}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@e0
    .line 1503
    const/4 v13, 0x0

    #@e1
    goto :goto_ad

    #@e2
    .line 1510
    .end local v2           #e:Ljava/lang/Exception;
    :cond_e2
    iput-object v7, p0, Landroid/net/wifi/WifiStateMachine;->mTetherInterfaceName:Ljava/lang/String;

    #@e4
    .line 1511
    const/4 v13, 0x1

    #@e5
    goto :goto_ad

    #@e6
    .line 1451
    .end local v6           #ifcg:Landroid/net/InterfaceConfiguration;
    :cond_e6
    add-int/lit8 v5, v5, 0x1

    #@e8
    goto/16 :goto_1d

    #@ea
    .line 1516
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v7           #intf:Ljava/lang/String;
    .end local v8           #len$:I
    .end local v10           #regex:Ljava/lang/String;
    :cond_ea
    const/4 v13, 0x0

    #@eb
    goto :goto_ad
.end method

.method private startVZWSoftApWithConfig(Landroid/net/wifi/WifiVZWConfiguration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 5035
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Landroid/net/wifi/WifiStateMachine$7;

    #@4
    invoke-direct {v1, p0, p1}, Landroid/net/wifi/WifiStateMachine$7;-><init>(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiVZWConfiguration;)V

    #@7
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@d
    .line 5068
    return-void
.end method

.method private stopTethering()V
    .registers 6

    #@0
    .prologue
    .line 1521
    invoke-direct {p0}, Landroid/net/wifi/WifiStateMachine;->checkAndSetConnectivityInstance()V

    #@3
    .line 1525
    const/4 v1, 0x0

    #@4
    .line 1527
    .local v1, ifcg:Landroid/net/InterfaceConfiguration;
    :try_start_4
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mTetherInterfaceName:Ljava/lang/String;

    #@8
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@b
    move-result-object v1

    #@c
    .line 1528
    if-eqz v1, :cond_2f

    #@e
    .line 1529
    new-instance v2, Landroid/net/LinkAddress;

    #@10
    const-string v3, "0.0.0.0"

    #@12
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@15
    move-result-object v3

    #@16
    const/4 v4, 0x0

    #@17
    invoke-direct {v2, v3, v4}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@1a
    invoke-virtual {v1, v2}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@1d
    .line 1531
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@1f
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mTetherInterfaceName:Ljava/lang/String;

    #@21
    invoke-interface {v2, v3, v1}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@24
    .line 1532
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@26
    if-nez v2, :cond_2f

    #@28
    .line 1533
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mNwService:Landroid/os/INetworkManagementService;

    #@2a
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mInterfaceName:Ljava/lang/String;

    #@2c
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->disableIpv6(Ljava/lang/String;)V
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_2f} :catch_3f

    #@2f
    .line 1540
    :cond_2f
    :goto_2f
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mCm:Landroid/net/ConnectivityManager;

    #@31
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mTetherInterfaceName:Ljava/lang/String;

    #@33
    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->untether(Ljava/lang/String;)I

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_3e

    #@39
    .line 1541
    const-string v2, "Untether initiate failed!"

    #@3b
    invoke-direct {p0, v2}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@3e
    .line 1543
    :cond_3e
    return-void

    #@3f
    .line 1536
    :catch_3f
    move-exception v0

    #@40
    .line 1537
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "Error resetting interface "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mTetherInterfaceName:Ljava/lang/String;

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v3, ", :"

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-direct {p0, v2}, Landroid/net/wifi/WifiStateMachine;->loge(Ljava/lang/String;)V

    #@62
    goto :goto_2f
.end method


# virtual methods
.method public addToBlacklist(Ljava/lang/String;)V
    .registers 3
    .parameter "bssid"

    #@0
    .prologue
    .line 1198
    const v0, 0x20038

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@a
    .line 1199
    return-void
.end method

.method public captivePortalCheckComplete()V
    .registers 2

    #@0
    .prologue
    .line 1062
    const v0, 0x20014

    #@3
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@a
    .line 1063
    return-void
.end method

.method public clearBlacklist()V
    .registers 2

    #@0
    .prologue
    .line 1206
    const v0, 0x20039

    #@3
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@a
    .line 1207
    return-void
.end method

.method public disconnectCommand()V
    .registers 2

    #@0
    .prologue
    .line 1104
    const v0, 0x2004a

    #@3
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@6
    .line 1105
    return-void
.end method

.method public enableAllNetworks()V
    .registers 2

    #@0
    .prologue
    .line 1218
    const v0, 0x20037

    #@3
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@6
    .line 1219
    return-void
.end method

.method public enableBackgroundScanCommand(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1214
    const v2, 0x2005b

    #@4
    if-eqz p1, :cond_f

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 1215
    return-void

    #@f
    :cond_f
    move v0, v1

    #@10
    .line 1214
    goto :goto_7
.end method

.method public enableRssiPolling(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1210
    const v2, 0x20052

    #@4
    if-eqz p1, :cond_f

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 1211
    return-void

    #@f
    :cond_f
    move v0, v1

    #@10
    .line 1210
    goto :goto_7
.end method

.method public getConfigFile()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1300
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@2
    invoke-virtual {v0}, Landroid/net/wifi/WifiConfigStore;->getConfigFile()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFrequencyBand()I
    .registers 2

    #@0
    .prologue
    .line 1293
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFrequencyBand:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMessenger()Landroid/os/Messenger;
    .registers 3

    #@0
    .prologue
    .line 905
    new-instance v0, Landroid/os/Messenger;

    #@2
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getHandler()Landroid/os/Handler;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@9
    return-object v0
.end method

.method handlePostDhcpSetup()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2081
    invoke-direct {p0, v1, v1}, Landroid/net/wifi/WifiStateMachine;->setSuspendOptimizationsNative(IZ)V

    #@4
    .line 2082
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@6
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->setPowerSave(Z)V

    #@9
    .line 2085
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@b
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@d
    const/4 v1, 0x2

    #@e
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->setBluetoothCoexistenceMode(I)Z

    #@11
    .line 2087
    return-void
.end method

.method handlePreDhcpSetup()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2048
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mBluetoothConnectionActive:Z

    #@4
    if-nez v0, :cond_d

    #@6
    .line 2066
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@8
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@a
    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiNative;->setBluetoothCoexistenceMode(I)Z

    #@d
    .line 2074
    :cond_d
    invoke-direct {p0, v3, v2}, Landroid/net/wifi/WifiStateMachine;->setSuspendOptimizationsNative(IZ)V

    #@10
    .line 2075
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@12
    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiNative;->setPowerSave(Z)V

    #@15
    .line 2076
    return-void
.end method

.method public reassociateCommand()V
    .registers 2

    #@0
    .prologue
    .line 1118
    const v0, 0x2004c

    #@3
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@6
    .line 1119
    return-void
.end method

.method public reconnectCommand()V
    .registers 2

    #@0
    .prologue
    .line 1111
    const v0, 0x2004b

    #@3
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@6
    .line 1112
    return-void
.end method

.method protected recordLogRec(Landroid/os/Message;)Z
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1385
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@4
    move-result-object v1

    #@5
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mConnectedState:Lcom/android/internal/util/State;

    #@7
    if-eq v1, v2, :cond_11

    #@9
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mDisconnectedState:Lcom/android/internal/util/State;

    #@f
    if-ne v1, v2, :cond_16

    #@11
    .line 1386
    :cond_11
    iget v1, p1, Landroid/os/Message;->what:I

    #@13
    sparse-switch v1, :sswitch_data_1e

    #@16
    .line 1399
    :cond_16
    iget v1, p1, Landroid/os/Message;->what:I

    #@18
    sparse-switch v1, :sswitch_data_40

    #@1b
    .line 1408
    const/4 v0, 0x1

    #@1c
    :sswitch_1c
    return v0

    #@1d
    .line 1386
    nop

    #@1e
    :sswitch_data_1e
    .sparse-switch
        0x20001 -> :sswitch_1c
        0x2000b -> :sswitch_1c
        0x2000d -> :sswitch_1c
        0x20037 -> :sswitch_1c
        0x20048 -> :sswitch_1c
        0x2004d -> :sswitch_1c
        0x20056 -> :sswitch_1c
        0x2005b -> :sswitch_1c
    .end sparse-switch

    #@40
    .line 1399
    :sswitch_data_40
    .sparse-switch
        0x20012 -> :sswitch_1c
        0x20047 -> :sswitch_1c
        0x20052 -> :sswitch_1c
        0x20053 -> :sswitch_1c
        0x24005 -> :sswitch_1c
        0x25014 -> :sswitch_1c
    .end sparse-switch
.end method

.method public sendBluetoothAdapterStateChange(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1307
    const v0, 0x2001f

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {p0, v0, p1, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@b
    .line 1308
    return-void
.end method

.method public setCountryCode(Ljava/lang/String;Z)V
    .registers 5
    .parameter "countryCode"
    .parameter "persist"

    #@0
    .prologue
    .line 1267
    if-eqz p2, :cond_e

    #@2
    .line 1268
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    const-string/jumbo v1, "wifi_country_code"

    #@b
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@e
    .line 1272
    :cond_e
    const v0, 0x20050

    #@11
    invoke-virtual {p0, v0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@18
    .line 1273
    return-void
.end method

.method public setDriverStart(ZZ)V
    .registers 6
    .parameter "enable"
    .parameter "ecm"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1054
    if-eqz p1, :cond_a

    #@3
    .line 1055
    const v0, 0x2000d

    #@6
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@9
    .line 1059
    :goto_9
    return-void

    #@a
    .line 1057
    :cond_a
    const v2, 0x2000e

    #@d
    if-eqz p2, :cond_18

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@17
    goto :goto_9

    #@18
    :cond_18
    move v0, v1

    #@19
    goto :goto_10
.end method

.method public setFrequencyBand(IZ)V
    .registers 5
    .parameter "band"
    .parameter "persist"

    #@0
    .prologue
    .line 1281
    if-eqz p2, :cond_e

    #@2
    .line 1282
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    const-string/jumbo v1, "wifi_frequency_band"

    #@b
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@e
    .line 1286
    :cond_e
    const v0, 0x2005a

    #@11
    const/4 v1, 0x0

    #@12
    invoke-virtual {p0, v0, p1, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@19
    .line 1287
    return-void
.end method

.method public setHighPerfModeEnabled(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1258
    const v2, 0x2004d

    #@4
    if-eqz p1, :cond_f

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 1259
    return-void

    #@f
    :cond_f
    move v0, v1

    #@10
    .line 1258
    goto :goto_7
.end method

.method public setScanOnlyMode(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    const v2, 0x20048

    #@3
    const/4 v1, 0x0

    #@4
    .line 1069
    if-eqz p1, :cond_f

    #@6
    .line 1070
    const/4 v0, 0x2

    #@7
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 1074
    :goto_e
    return-void

    #@f
    .line 1072
    :cond_f
    const/4 v0, 0x1

    #@10
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@17
    goto :goto_e
.end method

.method public setScanType(Z)V
    .registers 5
    .parameter "active"

    #@0
    .prologue
    const v2, 0x20049

    #@3
    const/4 v1, 0x0

    #@4
    .line 1080
    if-eqz p1, :cond_f

    #@6
    .line 1081
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 1085
    :goto_e
    return-void

    #@f
    .line 1083
    :cond_f
    const/4 v0, 0x2

    #@10
    invoke-virtual {p0, v2, v0, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@17
    goto :goto_e
.end method

.method public setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 971
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    const v1, 0x20019

    #@5
    invoke-virtual {v0, v1, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(ILjava/lang/Object;)V

    #@8
    .line 972
    return-void
.end method

.method public setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V
    .registers 7
    .parameter "wifiConfig"
    .parameter "enable"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 945
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastApEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@b
    .line 947
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    const-string v1, "ATT"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_46

    #@17
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    const-string v1, "US"

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_46

    #@23
    .line 948
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v0

    #@29
    const-string/jumbo v1, "tether_entitlement_check_state"

    #@2c
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2f
    move-result v0

    #@30
    if-lez v0, :cond_46

    #@32
    .line 949
    if-ne p2, v3, :cond_46

    #@34
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastApEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

    #@36
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@39
    move-result v0

    #@3a
    const/16 v1, 0x2710

    #@3c
    if-le v0, v1, :cond_46

    #@3e
    .line 951
    const-string v0, "WifiStateMachine"

    #@40
    const-string v1, "[ConnectivityService] Tethering Permission denied"

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 968
    :goto_45
    return-void

    #@46
    .line 959
    :cond_46
    if-eqz p2, :cond_5f

    #@48
    .line 961
    const v0, 0x20001

    #@4b
    const/16 v1, 0xc

    #@4d
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@54
    .line 962
    const v0, 0x20015

    #@57
    invoke-virtual {p0, v0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@5e
    goto :goto_45

    #@5f
    .line 964
    :cond_5f
    const v0, 0x20018

    #@62
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@65
    .line 966
    const v0, 0x20002

    #@68
    const/16 v1, 0xb

    #@6a
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@71
    goto :goto_45
.end method

.method public setWifiEnabled(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 929
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

    #@3
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v1

    #@7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@a
    .line 930
    if-eqz p1, :cond_1e

    #@c
    .line 932
    const v0, 0x20001

    #@f
    const/4 v1, 0x2

    #@10
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@17
    .line 933
    const v0, 0x2000b

    #@1a
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@1d
    .line 939
    :goto_1d
    return-void

    #@1e
    .line 935
    :cond_1e
    const v0, 0x2000c

    #@21
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@24
    .line 937
    const v0, 0x20002

    #@27
    const/4 v1, 0x1

    #@28
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@2f
    goto :goto_1d
.end method

.method public setWifiVZWApConfiguration(Landroid/net/wifi/WifiVZWConfiguration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 5008
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    const v1, 0x2008a

    #@5
    invoke-virtual {v0, v1, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(ILjava/lang/Object;)V

    #@8
    .line 5009
    return-void
.end method

.method public setWifiVZWApEnabled(Landroid/net/wifi/WifiVZWConfiguration;Z)V
    .registers 7
    .parameter "wifiConfig"
    .parameter "enable"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4991
    const-string v0, "WifiStateMachine"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string/jumbo v2, "setWifiVZWApEnabled! "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 4992
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastApEnableUid:Ljava/util/concurrent/atomic/AtomicInteger;

    #@1c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1f
    move-result v1

    #@20
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@23
    .line 4993
    if-eqz p2, :cond_3c

    #@25
    .line 4995
    const v0, 0x20001

    #@28
    const/16 v1, 0xc

    #@2a
    invoke-virtual {p0, v0, v1, v3}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@31
    .line 4996
    const v0, 0x20086

    #@34
    invoke-virtual {p0, v0, p1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@3b
    .line 5001
    :goto_3b
    return-void

    #@3c
    .line 4998
    :cond_3c
    const v0, 0x20089

    #@3f
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@42
    .line 4999
    const v0, 0x20002

    #@45
    const/16 v1, 0xb

    #@47
    invoke-virtual {p0, v0, v1, v3}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@4e
    goto :goto_3b
.end method

.method public startFilteringMulticastV4Packets()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1225
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFilteringMulticastV4Packets:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    const/4 v1, 0x1

    #@4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@7
    .line 1226
    const v0, 0x20054

    #@a
    invoke-virtual {p0, v0, v2, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@11
    .line 1227
    return-void
.end method

.method public startFilteringMulticastV6Packets()V
    .registers 4

    #@0
    .prologue
    .line 1241
    const v0, 0x20054

    #@3
    const/4 v1, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@c
    .line 1242
    return-void
.end method

.method public startScan(Z)V
    .registers 5
    .parameter "forceActive"

    #@0
    .prologue
    .line 921
    const v1, 0x20047

    #@3
    if-eqz p1, :cond_f

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    const/4 v2, 0x0

    #@7
    invoke-virtual {p0, v1, v0, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@e
    .line 923
    return-void

    #@f
    .line 921
    :cond_f
    const/4 v0, 0x2

    #@10
    goto :goto_6
.end method

.method public stopFilteringMulticastV4Packets()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1233
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mFilteringMulticastV4Packets:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@6
    .line 1234
    const v0, 0x20055

    #@9
    invoke-virtual {p0, v0, v1, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@10
    .line 1235
    return-void
.end method

.method public stopFilteringMulticastV6Packets()V
    .registers 4

    #@0
    .prologue
    .line 1248
    const v0, 0x20055

    #@3
    const/4 v1, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@c
    .line 1249
    return-void
.end method

.method public syncAddOrUpdateNetwork(Lcom/android/internal/util/AsyncChannel;Landroid/net/wifi/WifiConfiguration;)I
    .registers 6
    .parameter "channel"
    .parameter "config"

    #@0
    .prologue
    .line 1127
    const v2, 0x20034

    #@3
    invoke-virtual {p1, v2, p2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    .line 1128
    .local v1, resultMsg:Landroid/os/Message;
    iget v0, v1, Landroid/os/Message;->arg1:I

    #@9
    .line 1129
    .local v0, result:I
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@c
    .line 1130
    return v0
.end method

.method public syncDisableNetwork(Lcom/android/internal/util/AsyncChannel;I)Z
    .registers 8
    .parameter "channel"
    .parameter "netId"

    #@0
    .prologue
    .line 1175
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v3, :cond_1d

    #@4
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    const-string v4, "KT"

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_1d

    #@10
    .line 1176
    iget-boolean v3, p0, Landroid/net/wifi/WifiStateMachine;->mLgeKtCm:Z

    #@12
    if-nez v3, :cond_1d

    #@14
    .line 1177
    const/16 v0, -0x64

    #@16
    .line 1178
    .local v0, RELOAD_CONFIG:I
    if-ne p2, v0, :cond_1d

    #@18
    .line 1180
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@1a
    invoke-virtual {v3}, Landroid/net/wifi/WifiConfigStore;->loadConfiguredNetworks()V

    #@1d
    .line 1185
    .end local v0           #RELOAD_CONFIG:I
    :cond_1d
    const v3, 0x25011

    #@20
    invoke-virtual {p1, v3, p2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(II)Landroid/os/Message;

    #@23
    move-result-object v2

    #@24
    .line 1186
    .local v2, resultMsg:Landroid/os/Message;
    iget v3, v2, Landroid/os/Message;->arg1:I

    #@26
    const v4, 0x25012

    #@29
    if-eq v3, v4, :cond_30

    #@2b
    const/4 v1, 0x1

    #@2c
    .line 1187
    .local v1, result:Z
    :goto_2c
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    #@2f
    .line 1188
    return v1

    #@30
    .line 1186
    .end local v1           #result:Z
    :cond_30
    const/4 v1, 0x0

    #@31
    goto :goto_2c
.end method

.method public syncEnableNetwork(Lcom/android/internal/util/AsyncChannel;IZ)Z
    .registers 10
    .parameter "channel"
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1160
    const v5, 0x20036

    #@5
    if-eqz p3, :cond_16

    #@7
    move v2, v3

    #@8
    :goto_8
    invoke-virtual {p1, v5, p2, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(III)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    .line 1162
    .local v1, resultMsg:Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->arg1:I

    #@e
    const/4 v5, -0x1

    #@f
    if-eq v2, v5, :cond_18

    #@11
    move v0, v3

    #@12
    .line 1163
    .local v0, result:Z
    :goto_12
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@15
    .line 1164
    return v0

    #@16
    .end local v0           #result:Z
    .end local v1           #resultMsg:Landroid/os/Message;
    :cond_16
    move v2, v4

    #@17
    .line 1160
    goto :goto_8

    #@18
    .restart local v1       #resultMsg:Landroid/os/Message;
    :cond_18
    move v0, v4

    #@19
    .line 1162
    goto :goto_12
.end method

.method public syncGetConfiguredNetworks(Lcom/android/internal/util/AsyncChannel;)Ljava/util/List;
    .registers 5
    .parameter "channel"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/AsyncChannel;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1134
    const v2, 0x2003b

    #@3
    invoke-virtual {p1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    .line 1135
    .local v1, resultMsg:Landroid/os/Message;
    iget-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v0, Ljava/util/List;

    #@b
    .line 1136
    .local v0, result:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@e
    .line 1137
    return-object v0
.end method

.method public syncGetDhcpInfo()Landroid/net/DhcpInfo;
    .registers 3

    #@0
    .prologue
    .line 1045
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@2
    monitor-enter v1

    #@3
    .line 1046
    :try_start_3
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@5
    invoke-virtual {v0}, Landroid/net/DhcpInfoInternal;->makeDhcpInfo()Landroid/net/DhcpInfo;

    #@8
    move-result-object v0

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 1047
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public syncGetScanResultsList()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1091
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine;->mScanResultCache:Landroid/util/LruCache;

    #@2
    monitor-enter v4

    #@3
    .line 1092
    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    #@5
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@8
    .line 1093
    .local v2, scanList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mScanResults:Ljava/util/List;

    #@a
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v0

    #@e
    .local v0, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_26

    #@14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/net/wifi/ScanResult;

    #@1a
    .line 1094
    .local v1, result:Landroid/net/wifi/ScanResult;
    new-instance v3, Landroid/net/wifi/ScanResult;

    #@1c
    invoke-direct {v3, v1}, Landroid/net/wifi/ScanResult;-><init>(Landroid/net/wifi/ScanResult;)V

    #@1f
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@22
    goto :goto_e

    #@23
    .line 1097
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #result:Landroid/net/wifi/ScanResult;
    .end local v2           #scanList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :catchall_23
    move-exception v3

    #@24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v3

    #@26
    .line 1096
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v2       #scanList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_26
    :try_start_26
    monitor-exit v4
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_23

    #@27
    return-object v2
.end method

.method public syncGetWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;
    .registers 5

    #@0
    .prologue
    .line 975
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    const v3, 0x2001b

    #@5
    invoke-virtual {v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 976
    .local v0, resultMsg:Landroid/os/Message;
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    #@d
    .line 977
    .local v1, ret:Landroid/net/wifi/WifiConfiguration;
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    #@10
    .line 978
    return-object v1
.end method

.method public syncGetWifiApState()I
    .registers 2

    #@0
    .prologue
    .line 1012
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public syncGetWifiApStateByName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1019
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_1c

    #@9
    .line 1031
    const-string v0, "[invalid state]"

    #@b
    :goto_b
    return-object v0

    #@c
    .line 1021
    :pswitch_c
    const-string v0, "disabling"

    #@e
    goto :goto_b

    #@f
    .line 1023
    :pswitch_f
    const-string v0, "disabled"

    #@11
    goto :goto_b

    #@12
    .line 1025
    :pswitch_12
    const-string v0, "enabling"

    #@14
    goto :goto_b

    #@15
    .line 1027
    :pswitch_15
    const-string v0, "enabled"

    #@17
    goto :goto_b

    #@18
    .line 1029
    :pswitch_18
    const-string v0, "failed"

    #@1a
    goto :goto_b

    #@1b
    .line 1019
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0xa
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method public syncGetWifiState()I
    .registers 2

    #@0
    .prologue
    .line 985
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public syncGetWifiStateByName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 992
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_1c

    #@9
    .line 1004
    const-string v0, "[invalid state]"

    #@b
    :goto_b
    return-object v0

    #@c
    .line 994
    :pswitch_c
    const-string v0, "disabling"

    #@e
    goto :goto_b

    #@f
    .line 996
    :pswitch_f
    const-string v0, "disabled"

    #@11
    goto :goto_b

    #@12
    .line 998
    :pswitch_12
    const-string v0, "enabling"

    #@14
    goto :goto_b

    #@15
    .line 1000
    :pswitch_15
    const-string v0, "enabled"

    #@17
    goto :goto_b

    #@18
    .line 1002
    :pswitch_18
    const-string/jumbo v0, "unknown state"

    #@1b
    goto :goto_b

    #@1c
    .line 992
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method public syncGetWifiVZWApConfiguration()Landroid/net/wifi/WifiVZWConfiguration;
    .registers 5

    #@0
    .prologue
    .line 5017
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiApConfigChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    const v3, 0x2008c

    #@5
    invoke-virtual {v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 5018
    .local v0, resultMsg:Landroid/os/Message;
    if-nez v0, :cond_d

    #@b
    const/4 v1, 0x0

    #@c
    .line 5021
    :goto_c
    return-object v1

    #@d
    .line 5019
    :cond_d
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f
    check-cast v1, Landroid/net/wifi/WifiVZWConfiguration;

    #@11
    .line 5020
    .local v1, ret:Landroid/net/wifi/WifiVZWConfiguration;
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    #@14
    goto :goto_c
.end method

.method public syncPingSupplicant(Lcom/android/internal/util/AsyncChannel;)Z
    .registers 6
    .parameter "channel"

    #@0
    .prologue
    .line 911
    const v2, 0x20033

    #@3
    invoke-virtual {p1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    .line 912
    .local v1, resultMsg:Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->arg1:I

    #@9
    const/4 v3, -0x1

    #@a
    if-eq v2, v3, :cond_11

    #@c
    const/4 v0, 0x1

    #@d
    .line 913
    .local v0, result:Z
    :goto_d
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@10
    .line 914
    return v0

    #@11
    .line 912
    .end local v0           #result:Z
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_d
.end method

.method public syncRemoveNetwork(Lcom/android/internal/util/AsyncChannel;I)Z
    .registers 7
    .parameter "channel"
    .parameter "networkId"

    #@0
    .prologue
    .line 1146
    const v2, 0x20035

    #@3
    invoke-virtual {p1, v2, p2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(II)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    .line 1147
    .local v1, resultMsg:Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->arg1:I

    #@9
    const/4 v3, -0x1

    #@a
    if-eq v2, v3, :cond_11

    #@c
    const/4 v0, 0x1

    #@d
    .line 1148
    .local v0, result:Z
    :goto_d
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@10
    .line 1149
    return v0

    #@11
    .line 1147
    .end local v0           #result:Z
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_d
.end method

.method public syncRequestConnectionInfo()Landroid/net/wifi/WifiInfo;
    .registers 2

    #@0
    .prologue
    .line 1041
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@2
    return-object v0
.end method

.method public syncSaveConfig(Lcom/android/internal/util/AsyncChannel;)Z
    .registers 6
    .parameter "channel"

    #@0
    .prologue
    .line 1318
    const v2, 0x2003a

    #@3
    invoke-virtual {p1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    .line 1319
    .local v1, resultMsg:Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->arg1:I

    #@9
    const/4 v3, -0x1

    #@a
    if-eq v2, v3, :cond_11

    #@c
    const/4 v0, 0x1

    #@d
    .line 1320
    .local v0, result:Z
    :goto_d
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@10
    .line 1321
    return v0

    #@11
    .line 1319
    .end local v0           #result:Z
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_d
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1361
    new-instance v1, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 1362
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string/jumbo v2, "line.separator"

    #@8
    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1363
    .local v0, LS:Ljava/lang/String;
    const-string v2, "current HSM state: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@15
    move-result-object v3

    #@16
    invoke-interface {v3}, Lcom/android/internal/util/IState;->getName()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@21
    .line 1364
    const-string/jumbo v2, "mLinkProperties "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@27
    move-result-object v2

    #@28
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mLinkProperties:Landroid/net/LinkProperties;

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@31
    .line 1365
    const-string/jumbo v2, "mWifiInfo "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@37
    move-result-object v2

    #@38
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiInfo:Landroid/net/wifi/WifiInfo;

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@41
    .line 1366
    const-string/jumbo v2, "mDhcpInfoInternal "

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@47
    move-result-object v2

    #@48
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mDhcpInfoInternal:Landroid/net/DhcpInfoInternal;

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@51
    .line 1367
    const-string/jumbo v2, "mNetworkInfo "

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@57
    move-result-object v2

    #@58
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@61
    .line 1368
    const-string/jumbo v2, "mLastSignalLevel "

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@67
    move-result-object v2

    #@68
    iget v3, p0, Landroid/net/wifi/WifiStateMachine;->mLastSignalLevel:I

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@71
    .line 1369
    const-string/jumbo v2, "mLastBssid "

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@77
    move-result-object v2

    #@78
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mLastBssid:Ljava/lang/String;

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@81
    .line 1370
    const-string/jumbo v2, "mLastNetworkId "

    #@84
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@87
    move-result-object v2

    #@88
    iget v3, p0, Landroid/net/wifi/WifiStateMachine;->mLastNetworkId:I

    #@8a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@8d
    move-result-object v2

    #@8e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@91
    .line 1371
    const-string/jumbo v2, "mReconnectCount "

    #@94
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@97
    move-result-object v2

    #@98
    iget v3, p0, Landroid/net/wifi/WifiStateMachine;->mReconnectCount:I

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a1
    .line 1372
    const-string/jumbo v2, "mIsScanMode "

    #@a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a7
    move-result-object v2

    #@a8
    iget-boolean v3, p0, Landroid/net/wifi/WifiStateMachine;->mIsScanMode:Z

    #@aa
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    #@ad
    move-result-object v2

    #@ae
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b1
    .line 1373
    const-string/jumbo v2, "mUserWantsSuspendOpt "

    #@b4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b7
    move-result-object v2

    #@b8
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mUserWantsSuspendOpt:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@bd
    move-result-object v2

    #@be
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c1
    .line 1374
    const-string/jumbo v2, "mSuspendOptNeedsDisabled "

    #@c4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c7
    move-result-object v2

    #@c8
    iget v3, p0, Landroid/net/wifi/WifiStateMachine;->mSuspendOptNeedsDisabled:I

    #@ca
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@cd
    move-result-object v2

    #@ce
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d1
    .line 1375
    const-string v2, "Supplicant status"

    #@d3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d6
    move-result-object v2

    #@d7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@da
    move-result-object v2

    #@db
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@dd
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->status()Ljava/lang/String;

    #@e0
    move-result-object v3

    #@e1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@e8
    move-result-object v2

    #@e9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@ec
    .line 1378
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mWifiConfigStore:Landroid/net/wifi/WifiConfigStore;

    #@ee
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfigStore;->dump()Ljava/lang/String;

    #@f1
    move-result-object v2

    #@f2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@f5
    .line 1379
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@f8
    move-result-object v2

    #@f9
    return-object v2
.end method

.method public updateBatteryWorkSource(Landroid/os/WorkSource;)V
    .registers 6
    .parameter "newSource"

    #@0
    .prologue
    .line 1325
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@2
    monitor-enter v1

    #@3
    .line 1327
    if-eqz p1, :cond_a

    #@5
    .line 1328
    :try_start_5
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@7
    invoke-virtual {v0, p1}, Landroid/os/WorkSource;->set(Landroid/os/WorkSource;)V

    #@a
    .line 1330
    :cond_a
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mIsRunning:Z

    #@c
    if-eqz v0, :cond_47

    #@e
    .line 1331
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mReportedRunning:Z

    #@10
    if-eqz v0, :cond_33

    #@12
    .line 1334
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@14
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@16
    invoke-virtual {v0, v2}, Landroid/os/WorkSource;->diff(Landroid/os/WorkSource;)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_2c

    #@1c
    .line 1335
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@1e
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@20
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@22
    invoke-interface {v0, v2, v3}, Lcom/android/internal/app/IBatteryStats;->noteWifiRunningChanged(Landroid/os/WorkSource;Landroid/os/WorkSource;)V

    #@25
    .line 1337
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@27
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@29
    invoke-virtual {v0, v2}, Landroid/os/WorkSource;->set(Landroid/os/WorkSource;)V

    #@2c
    .line 1353
    :cond_2c
    :goto_2c
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2e
    invoke-virtual {v0, p1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V
    :try_end_31
    .catchall {:try_start_5 .. :try_end_31} :catchall_5b
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_31} :catch_45

    #@31
    .line 1356
    :goto_31
    :try_start_31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_5b

    #@32
    .line 1357
    return-void

    #@33
    .line 1341
    :cond_33
    :try_start_33
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@35
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@37
    invoke-interface {v0, v2}, Lcom/android/internal/app/IBatteryStats;->noteWifiRunning(Landroid/os/WorkSource;)V

    #@3a
    .line 1342
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@3c
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mRunningWifiUids:Landroid/os/WorkSource;

    #@3e
    invoke-virtual {v0, v2}, Landroid/os/WorkSource;->set(Landroid/os/WorkSource;)V

    #@41
    .line 1343
    const/4 v0, 0x1

    #@42
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mReportedRunning:Z

    #@44
    goto :goto_2c

    #@45
    .line 1354
    :catch_45
    move-exception v0

    #@46
    goto :goto_31

    #@47
    .line 1346
    :cond_47
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mReportedRunning:Z

    #@49
    if-eqz v0, :cond_2c

    #@4b
    .line 1348
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@4d
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@4f
    invoke-interface {v0, v2}, Lcom/android/internal/app/IBatteryStats;->noteWifiStopped(Landroid/os/WorkSource;)V

    #@52
    .line 1349
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine;->mLastRunningWifiUids:Landroid/os/WorkSource;

    #@54
    invoke-virtual {v0}, Landroid/os/WorkSource;->clear()V

    #@57
    .line 1350
    const/4 v0, 0x0

    #@58
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine;->mReportedRunning:Z
    :try_end_5a
    .catchall {:try_start_33 .. :try_end_5a} :catchall_5b
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_5a} :catch_45

    #@5a
    goto :goto_2c

    #@5b
    .line 1356
    :catchall_5b
    move-exception v0

    #@5c
    :try_start_5c
    monitor-exit v1
    :try_end_5d
    .catchall {:try_start_5c .. :try_end_5d} :catchall_5b

    #@5d
    throw v0
.end method
