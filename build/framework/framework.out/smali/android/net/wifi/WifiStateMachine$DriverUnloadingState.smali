.class Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DriverUnloadingState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2581
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 2585
    const v1, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->getName()Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 2587
    new-instance v0, Landroid/os/Message;

    #@c
    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    #@f
    .line 2588
    .local v0, message:Landroid/os/Message;
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$4300(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    #@18
    .line 2589
    new-instance v1, Ljava/lang/Thread;

    #@1a
    new-instance v2, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState$1;

    #@1c
    invoke-direct {v2, p0, v0}, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState$1;-><init>(Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;Landroid/os/Message;)V

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@22
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@25
    .line 2625
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 2630
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_26

    #@5
    .line 2661
    const/4 v0, 0x0

    #@6
    .line 2663
    :goto_6
    return v0

    #@7
    .line 2632
    :sswitch_7
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$2200(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@e
    move-result-object v1

    #@f
    invoke-static {v0, v1}, Landroid/net/wifi/WifiStateMachine;->access$4400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@12
    .line 2663
    :goto_12
    const/4 v0, 0x1

    #@13
    goto :goto_6

    #@14
    .line 2635
    :sswitch_14
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@16
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@18
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$3000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/net/wifi/WifiStateMachine;->access$4500(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@1f
    goto :goto_12

    #@20
    .line 2658
    :sswitch_20
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverUnloadingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@22
    invoke-static {v0, p1}, Landroid/net/wifi/WifiStateMachine;->access$4600(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@25
    goto :goto_12

    #@26
    .line 2630
    :sswitch_data_26
    .sparse-switch
        0x20001 -> :sswitch_20
        0x20002 -> :sswitch_20
        0x20005 -> :sswitch_7
        0x20006 -> :sswitch_14
        0x2000b -> :sswitch_20
        0x2000c -> :sswitch_20
        0x2000d -> :sswitch_20
        0x2000e -> :sswitch_20
        0x20015 -> :sswitch_20
        0x20018 -> :sswitch_20
        0x20048 -> :sswitch_20
        0x20049 -> :sswitch_20
        0x20050 -> :sswitch_20
        0x20054 -> :sswitch_20
        0x20055 -> :sswitch_20
        0x2005a -> :sswitch_20
        0x20086 -> :sswitch_20
        0x20089 -> :sswitch_20
    .end sparse-switch
.end method
