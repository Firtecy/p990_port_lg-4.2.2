.class public abstract Landroid/net/wifi/IWifiManager$Stub;
.super Landroid/os/Binder;
.source "IWifiManager.java"

# interfaces
.implements Landroid/net/wifi/IWifiManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/IWifiManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/IWifiManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.wifi.IWifiManager"

.field static final TRANSACTION_acquireMulticastLock:I = 0x1a

.field static final TRANSACTION_acquireWifiLock:I = 0x15

.field static final TRANSACTION_addOrUpdateNetwork:I = 0x2

.field static final TRANSACTION_addToBlacklist:I = 0x22

.field static final TRANSACTION_captivePortalCheckComplete:I = 0x27

.field static final TRANSACTION_checkAndStartWifiExt:I = 0x28

.field static final TRANSACTION_clearBlacklist:I = 0x23

.field static final TRANSACTION_disableNetwork:I = 0x5

.field static final TRANSACTION_disconnect:I = 0x9

.field static final TRANSACTION_enableNetwork:I = 0x4

.field static final TRANSACTION_getConfigFile:I = 0x26

.field static final TRANSACTION_getConfiguredNetworks:I = 0x1

.field static final TRANSACTION_getConnectionInfo:I = 0xc

.field static final TRANSACTION_getDhcpInfo:I = 0x14

.field static final TRANSACTION_getFrequencyBand:I = 0x11

.field static final TRANSACTION_getScanResults:I = 0x8

.field static final TRANSACTION_getVZWMobileHotspotSSID:I = 0x30

.field static final TRANSACTION_getWifiApConfiguration:I = 0x1e

.field static final TRANSACTION_getWifiApEnabledState:I = 0x1d

.field static final TRANSACTION_getWifiEnabledState:I = 0xe

.field static final TRANSACTION_getWifiNeedOn:I = 0x2a

.field static final TRANSACTION_getWifiServiceMessenger:I = 0x24

.field static final TRANSACTION_getWifiStateMachineMessenger:I = 0x25

.field static final TRANSACTION_getWifiVZWApConfiguration:I = 0x2c

.field static final TRANSACTION_initializeMulticastFiltering:I = 0x18

.field static final TRANSACTION_isDualBandSupported:I = 0x12

.field static final TRANSACTION_isMulticastEnabled:I = 0x19

.field static final TRANSACTION_isVZWMobileHotspotEnabled:I = 0x2e

.field static final TRANSACTION_pingSupplicant:I = 0x6

.field static final TRANSACTION_reassociate:I = 0xb

.field static final TRANSACTION_reconnect:I = 0xa

.field static final TRANSACTION_releaseMulticastLock:I = 0x1b

.field static final TRANSACTION_releaseWifiLock:I = 0x17

.field static final TRANSACTION_removeNetwork:I = 0x3

.field static final TRANSACTION_saveConfiguration:I = 0x13

.field static final TRANSACTION_setCountryCode:I = 0xf

.field static final TRANSACTION_setFrequencyBand:I = 0x10

.field static final TRANSACTION_setVZWMobileHotspot:I = 0x2f

.field static final TRANSACTION_setWifiApConfiguration:I = 0x1f

.field static final TRANSACTION_setWifiApEnabled:I = 0x1c

.field static final TRANSACTION_setWifiEnabled:I = 0xd

.field static final TRANSACTION_setWifiNeedOn:I = 0x29

.field static final TRANSACTION_setWifiVZWApConfiguration:I = 0x2d

.field static final TRANSACTION_setWifiVZWApEnabled:I = 0x2b

.field static final TRANSACTION_startScan:I = 0x7

.field static final TRANSACTION_startWifi:I = 0x20

.field static final TRANSACTION_stopWifi:I = 0x21

.field static final TRANSACTION_updateWifiLockWorkSource:I = 0x16


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.net.wifi.IWifiManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/wifi/IWifiManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.net.wifi.IWifiManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/wifi/IWifiManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/net/wifi/IWifiManager;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/net/wifi/IWifiManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/wifi/IWifiManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 44
    sparse-switch p1, :sswitch_data_448

    #@5
    .line 545
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v8

    #@9
    :goto_9
    return v8

    #@a
    .line 48
    :sswitch_a
    const-string v7, "android.net.wifi.IWifiManager"

    #@c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 53
    :sswitch_10
    const-string v7, "android.net.wifi.IWifiManager"

    #@12
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getConfiguredNetworks()Ljava/util/List;

    #@18
    move-result-object v6

    #@19
    .line 55
    .local v6, _result:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 56
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@1f
    goto :goto_9

    #@20
    .line 61
    .end local v6           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    :sswitch_20
    const-string v7, "android.net.wifi.IWifiManager"

    #@22
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_3e

    #@2b
    .line 64
    sget-object v7, Landroid/net/wifi/WifiConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@33
    .line 69
    .local v0, _arg0:Landroid/net/wifi/WifiConfiguration;
    :goto_33
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    #@36
    move-result v4

    #@37
    .line 70
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a
    .line 71
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    goto :goto_9

    #@3e
    .line 67
    .end local v0           #_arg0:Landroid/net/wifi/WifiConfiguration;
    .end local v4           #_result:I
    :cond_3e
    const/4 v0, 0x0

    #@3f
    .restart local v0       #_arg0:Landroid/net/wifi/WifiConfiguration;
    goto :goto_33

    #@40
    .line 76
    .end local v0           #_arg0:Landroid/net/wifi/WifiConfiguration;
    :sswitch_40
    const-string v9, "android.net.wifi.IWifiManager"

    #@42
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45
    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v0

    #@49
    .line 79
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->removeNetwork(I)Z

    #@4c
    move-result v4

    #@4d
    .line 80
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@50
    .line 81
    if-eqz v4, :cond_53

    #@52
    move v7, v8

    #@53
    :cond_53
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@56
    goto :goto_9

    #@57
    .line 86
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_57
    const-string v9, "android.net.wifi.IWifiManager"

    #@59
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v0

    #@60
    .line 90
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v9

    #@64
    if-eqz v9, :cond_75

    #@66
    move v1, v8

    #@67
    .line 91
    .local v1, _arg1:Z
    :goto_67
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->enableNetwork(IZ)Z

    #@6a
    move-result v4

    #@6b
    .line 92
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    .line 93
    if-eqz v4, :cond_71

    #@70
    move v7, v8

    #@71
    :cond_71
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    goto :goto_9

    #@75
    .end local v1           #_arg1:Z
    .end local v4           #_result:Z
    :cond_75
    move v1, v7

    #@76
    .line 90
    goto :goto_67

    #@77
    .line 98
    .end local v0           #_arg0:I
    :sswitch_77
    const-string v9, "android.net.wifi.IWifiManager"

    #@79
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v0

    #@80
    .line 101
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->disableNetwork(I)Z

    #@83
    move-result v4

    #@84
    .line 102
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@87
    .line 103
    if-eqz v4, :cond_8a

    #@89
    move v7, v8

    #@8a
    :cond_8a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@8d
    goto/16 :goto_9

    #@8f
    .line 108
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_8f
    const-string v9, "android.net.wifi.IWifiManager"

    #@91
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94
    .line 109
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->pingSupplicant()Z

    #@97
    move-result v4

    #@98
    .line 110
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9b
    .line 111
    if-eqz v4, :cond_9e

    #@9d
    move v7, v8

    #@9e
    :cond_9e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@a1
    goto/16 :goto_9

    #@a3
    .line 116
    .end local v4           #_result:Z
    :sswitch_a3
    const-string v9, "android.net.wifi.IWifiManager"

    #@a5
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a8
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ab
    move-result v9

    #@ac
    if-eqz v9, :cond_b7

    #@ae
    move v0, v8

    #@af
    .line 119
    .local v0, _arg0:Z
    :goto_af
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->startScan(Z)V

    #@b2
    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b5
    goto/16 :goto_9

    #@b7
    .end local v0           #_arg0:Z
    :cond_b7
    move v0, v7

    #@b8
    .line 118
    goto :goto_af

    #@b9
    .line 125
    :sswitch_b9
    const-string v7, "android.net.wifi.IWifiManager"

    #@bb
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@be
    .line 126
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getScanResults()Ljava/util/List;

    #@c1
    move-result-object v5

    #@c2
    .line 127
    .local v5, _result:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c5
    .line 128
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@c8
    goto/16 :goto_9

    #@ca
    .line 133
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :sswitch_ca
    const-string v7, "android.net.wifi.IWifiManager"

    #@cc
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cf
    .line 134
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->disconnect()V

    #@d2
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d5
    goto/16 :goto_9

    #@d7
    .line 140
    :sswitch_d7
    const-string v7, "android.net.wifi.IWifiManager"

    #@d9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 141
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->reconnect()V

    #@df
    .line 142
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e2
    goto/16 :goto_9

    #@e4
    .line 147
    :sswitch_e4
    const-string v7, "android.net.wifi.IWifiManager"

    #@e6
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e9
    .line 148
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->reassociate()V

    #@ec
    .line 149
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ef
    goto/16 :goto_9

    #@f1
    .line 154
    :sswitch_f1
    const-string v9, "android.net.wifi.IWifiManager"

    #@f3
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f6
    .line 155
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@f9
    move-result-object v4

    #@fa
    .line 156
    .local v4, _result:Landroid/net/wifi/WifiInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fd
    .line 157
    if-eqz v4, :cond_107

    #@ff
    .line 158
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@102
    .line 159
    invoke-virtual {v4, p3, v8}, Landroid/net/wifi/WifiInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@105
    goto/16 :goto_9

    #@107
    .line 162
    :cond_107
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@10a
    goto/16 :goto_9

    #@10c
    .line 168
    .end local v4           #_result:Landroid/net/wifi/WifiInfo;
    :sswitch_10c
    const-string v9, "android.net.wifi.IWifiManager"

    #@10e
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@111
    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@114
    move-result v9

    #@115
    if-eqz v9, :cond_127

    #@117
    move v0, v8

    #@118
    .line 171
    .restart local v0       #_arg0:Z
    :goto_118
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->setWifiEnabled(Z)Z

    #@11b
    move-result v4

    #@11c
    .line 172
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11f
    .line 173
    if-eqz v4, :cond_122

    #@121
    move v7, v8

    #@122
    :cond_122
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@125
    goto/16 :goto_9

    #@127
    .end local v0           #_arg0:Z
    .end local v4           #_result:Z
    :cond_127
    move v0, v7

    #@128
    .line 170
    goto :goto_118

    #@129
    .line 178
    :sswitch_129
    const-string v7, "android.net.wifi.IWifiManager"

    #@12b
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12e
    .line 179
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiEnabledState()I

    #@131
    move-result v4

    #@132
    .line 180
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@135
    .line 181
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@138
    goto/16 :goto_9

    #@13a
    .line 186
    .end local v4           #_result:I
    :sswitch_13a
    const-string v9, "android.net.wifi.IWifiManager"

    #@13c
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13f
    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@142
    move-result-object v0

    #@143
    .line 190
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@146
    move-result v9

    #@147
    if-eqz v9, :cond_152

    #@149
    move v1, v8

    #@14a
    .line 191
    .restart local v1       #_arg1:Z
    :goto_14a
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->setCountryCode(Ljava/lang/String;Z)V

    #@14d
    .line 192
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@150
    goto/16 :goto_9

    #@152
    .end local v1           #_arg1:Z
    :cond_152
    move v1, v7

    #@153
    .line 190
    goto :goto_14a

    #@154
    .line 197
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_154
    const-string v9, "android.net.wifi.IWifiManager"

    #@156
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@159
    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15c
    move-result v0

    #@15d
    .line 201
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@160
    move-result v9

    #@161
    if-eqz v9, :cond_16c

    #@163
    move v1, v8

    #@164
    .line 202
    .restart local v1       #_arg1:Z
    :goto_164
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->setFrequencyBand(IZ)V

    #@167
    .line 203
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@16a
    goto/16 :goto_9

    #@16c
    .end local v1           #_arg1:Z
    :cond_16c
    move v1, v7

    #@16d
    .line 201
    goto :goto_164

    #@16e
    .line 208
    .end local v0           #_arg0:I
    :sswitch_16e
    const-string v7, "android.net.wifi.IWifiManager"

    #@170
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173
    .line 209
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getFrequencyBand()I

    #@176
    move-result v4

    #@177
    .line 210
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17a
    .line 211
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@17d
    goto/16 :goto_9

    #@17f
    .line 216
    .end local v4           #_result:I
    :sswitch_17f
    const-string v9, "android.net.wifi.IWifiManager"

    #@181
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@184
    .line 217
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->isDualBandSupported()Z

    #@187
    move-result v4

    #@188
    .line 218
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18b
    .line 219
    if-eqz v4, :cond_18e

    #@18d
    move v7, v8

    #@18e
    :cond_18e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@191
    goto/16 :goto_9

    #@193
    .line 224
    .end local v4           #_result:Z
    :sswitch_193
    const-string v9, "android.net.wifi.IWifiManager"

    #@195
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@198
    .line 225
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->saveConfiguration()Z

    #@19b
    move-result v4

    #@19c
    .line 226
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@19f
    .line 227
    if-eqz v4, :cond_1a2

    #@1a1
    move v7, v8

    #@1a2
    :cond_1a2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1a5
    goto/16 :goto_9

    #@1a7
    .line 232
    .end local v4           #_result:Z
    :sswitch_1a7
    const-string v9, "android.net.wifi.IWifiManager"

    #@1a9
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ac
    .line 233
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getDhcpInfo()Landroid/net/DhcpInfo;

    #@1af
    move-result-object v4

    #@1b0
    .line 234
    .local v4, _result:Landroid/net/DhcpInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b3
    .line 235
    if-eqz v4, :cond_1bd

    #@1b5
    .line 236
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@1b8
    .line 237
    invoke-virtual {v4, p3, v8}, Landroid/net/DhcpInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1bb
    goto/16 :goto_9

    #@1bd
    .line 240
    :cond_1bd
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1c0
    goto/16 :goto_9

    #@1c2
    .line 246
    .end local v4           #_result:Landroid/net/DhcpInfo;
    :sswitch_1c2
    const-string v9, "android.net.wifi.IWifiManager"

    #@1c4
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c7
    .line 248
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1ca
    move-result-object v0

    #@1cb
    .line 250
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ce
    move-result v1

    #@1cf
    .line 252
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d2
    move-result-object v2

    #@1d3
    .line 254
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d6
    move-result v9

    #@1d7
    if-eqz v9, :cond_1f0

    #@1d9
    .line 255
    sget-object v9, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1db
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1de
    move-result-object v3

    #@1df
    check-cast v3, Landroid/os/WorkSource;

    #@1e1
    .line 260
    .local v3, _arg3:Landroid/os/WorkSource;
    :goto_1e1
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/net/wifi/IWifiManager$Stub;->acquireWifiLock(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;)Z

    #@1e4
    move-result v4

    #@1e5
    .line 261
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e8
    .line 262
    if-eqz v4, :cond_1eb

    #@1ea
    move v7, v8

    #@1eb
    :cond_1eb
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1ee
    goto/16 :goto_9

    #@1f0
    .line 258
    .end local v3           #_arg3:Landroid/os/WorkSource;
    .end local v4           #_result:Z
    :cond_1f0
    const/4 v3, 0x0

    #@1f1
    .restart local v3       #_arg3:Landroid/os/WorkSource;
    goto :goto_1e1

    #@1f2
    .line 267
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Ljava/lang/String;
    .end local v3           #_arg3:Landroid/os/WorkSource;
    :sswitch_1f2
    const-string v7, "android.net.wifi.IWifiManager"

    #@1f4
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f7
    .line 269
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1fa
    move-result-object v0

    #@1fb
    .line 271
    .restart local v0       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1fe
    move-result v7

    #@1ff
    if-eqz v7, :cond_211

    #@201
    .line 272
    sget-object v7, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@203
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@206
    move-result-object v1

    #@207
    check-cast v1, Landroid/os/WorkSource;

    #@209
    .line 277
    .local v1, _arg1:Landroid/os/WorkSource;
    :goto_209
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->updateWifiLockWorkSource(Landroid/os/IBinder;Landroid/os/WorkSource;)V

    #@20c
    .line 278
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20f
    goto/16 :goto_9

    #@211
    .line 275
    .end local v1           #_arg1:Landroid/os/WorkSource;
    :cond_211
    const/4 v1, 0x0

    #@212
    .restart local v1       #_arg1:Landroid/os/WorkSource;
    goto :goto_209

    #@213
    .line 283
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v1           #_arg1:Landroid/os/WorkSource;
    :sswitch_213
    const-string v9, "android.net.wifi.IWifiManager"

    #@215
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@218
    .line 285
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@21b
    move-result-object v0

    #@21c
    .line 286
    .restart local v0       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->releaseWifiLock(Landroid/os/IBinder;)Z

    #@21f
    move-result v4

    #@220
    .line 287
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@223
    .line 288
    if-eqz v4, :cond_226

    #@225
    move v7, v8

    #@226
    :cond_226
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@229
    goto/16 :goto_9

    #@22b
    .line 293
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v4           #_result:Z
    :sswitch_22b
    const-string v7, "android.net.wifi.IWifiManager"

    #@22d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@230
    .line 294
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->initializeMulticastFiltering()V

    #@233
    .line 295
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@236
    goto/16 :goto_9

    #@238
    .line 300
    :sswitch_238
    const-string v9, "android.net.wifi.IWifiManager"

    #@23a
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@23d
    .line 301
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->isMulticastEnabled()Z

    #@240
    move-result v4

    #@241
    .line 302
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@244
    .line 303
    if-eqz v4, :cond_247

    #@246
    move v7, v8

    #@247
    :cond_247
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@24a
    goto/16 :goto_9

    #@24c
    .line 308
    .end local v4           #_result:Z
    :sswitch_24c
    const-string v7, "android.net.wifi.IWifiManager"

    #@24e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@251
    .line 310
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@254
    move-result-object v0

    #@255
    .line 312
    .restart local v0       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@258
    move-result-object v1

    #@259
    .line 313
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->acquireMulticastLock(Landroid/os/IBinder;Ljava/lang/String;)V

    #@25c
    .line 314
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@25f
    goto/16 :goto_9

    #@261
    .line 319
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_261
    const-string v7, "android.net.wifi.IWifiManager"

    #@263
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@266
    .line 320
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->releaseMulticastLock()V

    #@269
    .line 321
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@26c
    goto/16 :goto_9

    #@26e
    .line 326
    :sswitch_26e
    const-string v9, "android.net.wifi.IWifiManager"

    #@270
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@273
    .line 328
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@276
    move-result v9

    #@277
    if-eqz v9, :cond_290

    #@279
    .line 329
    sget-object v9, Landroid/net/wifi/WifiConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27b
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27e
    move-result-object v0

    #@27f
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@281
    .line 335
    .local v0, _arg0:Landroid/net/wifi/WifiConfiguration;
    :goto_281
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@284
    move-result v9

    #@285
    if-eqz v9, :cond_292

    #@287
    move v1, v8

    #@288
    .line 336
    .local v1, _arg1:Z
    :goto_288
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V

    #@28b
    .line 337
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@28e
    goto/16 :goto_9

    #@290
    .line 332
    .end local v0           #_arg0:Landroid/net/wifi/WifiConfiguration;
    .end local v1           #_arg1:Z
    :cond_290
    const/4 v0, 0x0

    #@291
    .restart local v0       #_arg0:Landroid/net/wifi/WifiConfiguration;
    goto :goto_281

    #@292
    :cond_292
    move v1, v7

    #@293
    .line 335
    goto :goto_288

    #@294
    .line 342
    .end local v0           #_arg0:Landroid/net/wifi/WifiConfiguration;
    :sswitch_294
    const-string v7, "android.net.wifi.IWifiManager"

    #@296
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@299
    .line 343
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiApEnabledState()I

    #@29c
    move-result v4

    #@29d
    .line 344
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a0
    .line 345
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2a3
    goto/16 :goto_9

    #@2a5
    .line 350
    .end local v4           #_result:I
    :sswitch_2a5
    const-string v9, "android.net.wifi.IWifiManager"

    #@2a7
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2aa
    .line 351
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    #@2ad
    move-result-object v4

    #@2ae
    .line 352
    .local v4, _result:Landroid/net/wifi/WifiConfiguration;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b1
    .line 353
    if-eqz v4, :cond_2bb

    #@2b3
    .line 354
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@2b6
    .line 355
    invoke-virtual {v4, p3, v8}, Landroid/net/wifi/WifiConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    #@2b9
    goto/16 :goto_9

    #@2bb
    .line 358
    :cond_2bb
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@2be
    goto/16 :goto_9

    #@2c0
    .line 364
    .end local v4           #_result:Landroid/net/wifi/WifiConfiguration;
    :sswitch_2c0
    const-string v7, "android.net.wifi.IWifiManager"

    #@2c2
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c5
    .line 366
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c8
    move-result v7

    #@2c9
    if-eqz v7, :cond_2db

    #@2cb
    .line 367
    sget-object v7, Landroid/net/wifi/WifiConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2cd
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d0
    move-result-object v0

    #@2d1
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@2d3
    .line 372
    .restart local v0       #_arg0:Landroid/net/wifi/WifiConfiguration;
    :goto_2d3
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)V

    #@2d6
    .line 373
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d9
    goto/16 :goto_9

    #@2db
    .line 370
    .end local v0           #_arg0:Landroid/net/wifi/WifiConfiguration;
    :cond_2db
    const/4 v0, 0x0

    #@2dc
    .restart local v0       #_arg0:Landroid/net/wifi/WifiConfiguration;
    goto :goto_2d3

    #@2dd
    .line 378
    .end local v0           #_arg0:Landroid/net/wifi/WifiConfiguration;
    :sswitch_2dd
    const-string v7, "android.net.wifi.IWifiManager"

    #@2df
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e2
    .line 379
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->startWifi()V

    #@2e5
    .line 380
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e8
    goto/16 :goto_9

    #@2ea
    .line 385
    :sswitch_2ea
    const-string v7, "android.net.wifi.IWifiManager"

    #@2ec
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ef
    .line 386
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->stopWifi()V

    #@2f2
    .line 387
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f5
    goto/16 :goto_9

    #@2f7
    .line 392
    :sswitch_2f7
    const-string v7, "android.net.wifi.IWifiManager"

    #@2f9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2fc
    .line 394
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2ff
    move-result-object v0

    #@300
    .line 395
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->addToBlacklist(Ljava/lang/String;)V

    #@303
    .line 396
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@306
    goto/16 :goto_9

    #@308
    .line 401
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_308
    const-string v7, "android.net.wifi.IWifiManager"

    #@30a
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30d
    .line 402
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->clearBlacklist()V

    #@310
    .line 403
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@313
    goto/16 :goto_9

    #@315
    .line 408
    :sswitch_315
    const-string v9, "android.net.wifi.IWifiManager"

    #@317
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31a
    .line 409
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiServiceMessenger()Landroid/os/Messenger;

    #@31d
    move-result-object v4

    #@31e
    .line 410
    .local v4, _result:Landroid/os/Messenger;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@321
    .line 411
    if-eqz v4, :cond_32b

    #@323
    .line 412
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@326
    .line 413
    invoke-virtual {v4, p3, v8}, Landroid/os/Messenger;->writeToParcel(Landroid/os/Parcel;I)V

    #@329
    goto/16 :goto_9

    #@32b
    .line 416
    :cond_32b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@32e
    goto/16 :goto_9

    #@330
    .line 422
    .end local v4           #_result:Landroid/os/Messenger;
    :sswitch_330
    const-string v9, "android.net.wifi.IWifiManager"

    #@332
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@335
    .line 423
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiStateMachineMessenger()Landroid/os/Messenger;

    #@338
    move-result-object v4

    #@339
    .line 424
    .restart local v4       #_result:Landroid/os/Messenger;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@33c
    .line 425
    if-eqz v4, :cond_346

    #@33e
    .line 426
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@341
    .line 427
    invoke-virtual {v4, p3, v8}, Landroid/os/Messenger;->writeToParcel(Landroid/os/Parcel;I)V

    #@344
    goto/16 :goto_9

    #@346
    .line 430
    :cond_346
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@349
    goto/16 :goto_9

    #@34b
    .line 436
    .end local v4           #_result:Landroid/os/Messenger;
    :sswitch_34b
    const-string v7, "android.net.wifi.IWifiManager"

    #@34d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@350
    .line 437
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getConfigFile()Ljava/lang/String;

    #@353
    move-result-object v4

    #@354
    .line 438
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@357
    .line 439
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@35a
    goto/16 :goto_9

    #@35c
    .line 444
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_35c
    const-string v7, "android.net.wifi.IWifiManager"

    #@35e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@361
    .line 445
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->captivePortalCheckComplete()V

    #@364
    .line 446
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@367
    goto/16 :goto_9

    #@369
    .line 451
    :sswitch_369
    const-string v7, "android.net.wifi.IWifiManager"

    #@36b
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36e
    .line 452
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->checkAndStartWifiExt()V

    #@371
    .line 453
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@374
    goto/16 :goto_9

    #@376
    .line 458
    :sswitch_376
    const-string v9, "android.net.wifi.IWifiManager"

    #@378
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@37b
    .line 460
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37e
    move-result v9

    #@37f
    if-eqz v9, :cond_391

    #@381
    move v0, v8

    #@382
    .line 461
    .local v0, _arg0:Z
    :goto_382
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->setWifiNeedOn(Z)Z

    #@385
    move-result v4

    #@386
    .line 462
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@389
    .line 463
    if-eqz v4, :cond_38c

    #@38b
    move v7, v8

    #@38c
    :cond_38c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@38f
    goto/16 :goto_9

    #@391
    .end local v0           #_arg0:Z
    .end local v4           #_result:Z
    :cond_391
    move v0, v7

    #@392
    .line 460
    goto :goto_382

    #@393
    .line 468
    :sswitch_393
    const-string v9, "android.net.wifi.IWifiManager"

    #@395
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@398
    .line 469
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiNeedOn()Z

    #@39b
    move-result v4

    #@39c
    .line 470
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@39f
    .line 471
    if-eqz v4, :cond_3a2

    #@3a1
    move v7, v8

    #@3a2
    :cond_3a2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@3a5
    goto/16 :goto_9

    #@3a7
    .line 476
    .end local v4           #_result:Z
    :sswitch_3a7
    const-string v9, "android.net.wifi.IWifiManager"

    #@3a9
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ac
    .line 478
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3af
    move-result v9

    #@3b0
    if-eqz v9, :cond_3c9

    #@3b2
    .line 479
    sget-object v9, Landroid/net/wifi/WifiVZWConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3b4
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3b7
    move-result-object v0

    #@3b8
    check-cast v0, Landroid/net/wifi/WifiVZWConfiguration;

    #@3ba
    .line 485
    .local v0, _arg0:Landroid/net/wifi/WifiVZWConfiguration;
    :goto_3ba
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3bd
    move-result v9

    #@3be
    if-eqz v9, :cond_3cb

    #@3c0
    move v1, v8

    #@3c1
    .line 486
    .restart local v1       #_arg1:Z
    :goto_3c1
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/IWifiManager$Stub;->setWifiVZWApEnabled(Landroid/net/wifi/WifiVZWConfiguration;Z)V

    #@3c4
    .line 487
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c7
    goto/16 :goto_9

    #@3c9
    .line 482
    .end local v0           #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    .end local v1           #_arg1:Z
    :cond_3c9
    const/4 v0, 0x0

    #@3ca
    .restart local v0       #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    goto :goto_3ba

    #@3cb
    :cond_3cb
    move v1, v7

    #@3cc
    .line 485
    goto :goto_3c1

    #@3cd
    .line 492
    .end local v0           #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    :sswitch_3cd
    const-string v9, "android.net.wifi.IWifiManager"

    #@3cf
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d2
    .line 493
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getWifiVZWApConfiguration()Landroid/net/wifi/WifiVZWConfiguration;

    #@3d5
    move-result-object v4

    #@3d6
    .line 494
    .local v4, _result:Landroid/net/wifi/WifiVZWConfiguration;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d9
    .line 495
    if-eqz v4, :cond_3e3

    #@3db
    .line 496
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@3de
    .line 497
    invoke-virtual {v4, p3, v8}, Landroid/net/wifi/WifiVZWConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    #@3e1
    goto/16 :goto_9

    #@3e3
    .line 500
    :cond_3e3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@3e6
    goto/16 :goto_9

    #@3e8
    .line 506
    .end local v4           #_result:Landroid/net/wifi/WifiVZWConfiguration;
    :sswitch_3e8
    const-string v7, "android.net.wifi.IWifiManager"

    #@3ea
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ed
    .line 508
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3f0
    move-result v7

    #@3f1
    if-eqz v7, :cond_403

    #@3f3
    .line 509
    sget-object v7, Landroid/net/wifi/WifiVZWConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3f5
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3f8
    move-result-object v0

    #@3f9
    check-cast v0, Landroid/net/wifi/WifiVZWConfiguration;

    #@3fb
    .line 514
    .restart local v0       #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    :goto_3fb
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->setWifiVZWApConfiguration(Landroid/net/wifi/WifiVZWConfiguration;)V

    #@3fe
    .line 515
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@401
    goto/16 :goto_9

    #@403
    .line 512
    .end local v0           #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    :cond_403
    const/4 v0, 0x0

    #@404
    .restart local v0       #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    goto :goto_3fb

    #@405
    .line 520
    .end local v0           #_arg0:Landroid/net/wifi/WifiVZWConfiguration;
    :sswitch_405
    const-string v9, "android.net.wifi.IWifiManager"

    #@407
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40a
    .line 521
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->isVZWMobileHotspotEnabled()Z

    #@40d
    move-result v4

    #@40e
    .line 522
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@411
    .line 523
    if-eqz v4, :cond_414

    #@413
    move v7, v8

    #@414
    :cond_414
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@417
    goto/16 :goto_9

    #@419
    .line 528
    .end local v4           #_result:Z
    :sswitch_419
    const-string v9, "android.net.wifi.IWifiManager"

    #@41b
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41e
    .line 530
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@421
    move-result v9

    #@422
    if-eqz v9, :cond_434

    #@424
    move v0, v8

    #@425
    .line 531
    .local v0, _arg0:Z
    :goto_425
    invoke-virtual {p0, v0}, Landroid/net/wifi/IWifiManager$Stub;->setVZWMobileHotspot(Z)Z

    #@428
    move-result v4

    #@429
    .line 532
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@42c
    .line 533
    if-eqz v4, :cond_42f

    #@42e
    move v7, v8

    #@42f
    :cond_42f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@432
    goto/16 :goto_9

    #@434
    .end local v0           #_arg0:Z
    .end local v4           #_result:Z
    :cond_434
    move v0, v7

    #@435
    .line 530
    goto :goto_425

    #@436
    .line 538
    :sswitch_436
    const-string v7, "android.net.wifi.IWifiManager"

    #@438
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43b
    .line 539
    invoke-virtual {p0}, Landroid/net/wifi/IWifiManager$Stub;->getVZWMobileHotspotSSID()Ljava/lang/String;

    #@43e
    move-result-object v4

    #@43f
    .line 540
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@442
    .line 541
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@445
    goto/16 :goto_9

    #@447
    .line 44
    nop

    #@448
    :sswitch_data_448
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_40
        0x4 -> :sswitch_57
        0x5 -> :sswitch_77
        0x6 -> :sswitch_8f
        0x7 -> :sswitch_a3
        0x8 -> :sswitch_b9
        0x9 -> :sswitch_ca
        0xa -> :sswitch_d7
        0xb -> :sswitch_e4
        0xc -> :sswitch_f1
        0xd -> :sswitch_10c
        0xe -> :sswitch_129
        0xf -> :sswitch_13a
        0x10 -> :sswitch_154
        0x11 -> :sswitch_16e
        0x12 -> :sswitch_17f
        0x13 -> :sswitch_193
        0x14 -> :sswitch_1a7
        0x15 -> :sswitch_1c2
        0x16 -> :sswitch_1f2
        0x17 -> :sswitch_213
        0x18 -> :sswitch_22b
        0x19 -> :sswitch_238
        0x1a -> :sswitch_24c
        0x1b -> :sswitch_261
        0x1c -> :sswitch_26e
        0x1d -> :sswitch_294
        0x1e -> :sswitch_2a5
        0x1f -> :sswitch_2c0
        0x20 -> :sswitch_2dd
        0x21 -> :sswitch_2ea
        0x22 -> :sswitch_2f7
        0x23 -> :sswitch_308
        0x24 -> :sswitch_315
        0x25 -> :sswitch_330
        0x26 -> :sswitch_34b
        0x27 -> :sswitch_35c
        0x28 -> :sswitch_369
        0x29 -> :sswitch_376
        0x2a -> :sswitch_393
        0x2b -> :sswitch_3a7
        0x2c -> :sswitch_3cd
        0x2d -> :sswitch_3e8
        0x2e -> :sswitch_405
        0x2f -> :sswitch_419
        0x30 -> :sswitch_436
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
