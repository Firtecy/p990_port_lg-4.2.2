.class Landroid/net/wifi/WifiConfigStore;
.super Ljava/lang/Object;
.source "WifiConfigStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiConfigStore$1;,
        Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DNS_KEY:Ljava/lang/String; = "dns"

.field private static final EOS:Ljava/lang/String; = "eos"

.field private static final EXCLUSION_LIST_KEY:Ljava/lang/String; = "exclusionList"

.field private static final GATEWAY_KEY:Ljava/lang/String; = "gateway"

.field private static final ID_KEY:Ljava/lang/String; = "id"

.field private static final IPCONFIG_FILE_VERSION:I = 0x2

.field private static final IP_ASSIGNMENT_KEY:Ljava/lang/String; = "ipAssignment"

.field private static final LINK_ADDRESS_KEY:Ljava/lang/String; = "linkAddress"

.field private static final PROXY_HOST_KEY:Ljava/lang/String; = "proxyHost"

.field private static final PROXY_PORT_KEY:Ljava/lang/String; = "proxyPort"

.field private static final PROXY_SETTINGS_KEY:Ljava/lang/String; = "proxySettings"

.field private static final TAG:Ljava/lang/String; = "WifiConfigStore"

.field private static final ipConfigFile:Ljava/lang/String;

.field private static mLgeKtCm:Z

.field private static sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# instance fields
.field private mConfiguredNetworks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mLastPriority:I

.field private mNetworkIds:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWifiNative:Landroid/net/wifi/WifiNative;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 122
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useLgeKtCm()Z

    #@3
    move-result v0

    #@4
    sput-boolean v0, Landroid/net/wifi/WifiConfigStore;->mLgeKtCm:Z

    #@6
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "/misc/wifi/ipconfig.txt"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    sput-object v0, Landroid/net/wifi/WifiConfigStore;->ipConfigFile:Ljava/lang/String;

    #@1f
    .line 164
    const/4 v0, 0x0

    #@20
    sput-object v0, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@22
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/net/wifi/WifiNative;)V
    .registers 4
    .parameter "c"
    .parameter "wn"

    #@0
    .prologue
    .line 166
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 126
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@a
    .line 136
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@11
    .line 140
    const/4 v0, -0x1

    #@12
    iput v0, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@14
    .line 167
    iput-object p1, p0, Landroid/net/wifi/WifiConfigStore;->mContext:Landroid/content/Context;

    #@16
    .line 168
    iput-object p2, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@18
    .line 169
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 115
    sget-object v0, Landroid/net/wifi/WifiConfigStore;->ipConfigFile:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/wifi/WifiConfiguration;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 115
    invoke-static {p0}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private addIpSettingsFromConfig(Landroid/net/LinkProperties;Landroid/net/wifi/WifiConfiguration;)V
    .registers 8
    .parameter "linkProperties"
    .parameter "config"

    #@0
    .prologue
    .line 1471
    iget-object v4, p2, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@2
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@5
    move-result-object v4

    #@6
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_1a

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/net/LinkAddress;

    #@16
    .line 1472
    .local v2, linkAddr:Landroid/net/LinkAddress;
    invoke-virtual {p1, v2}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V

    #@19
    goto :goto_a

    #@1a
    .line 1474
    .end local v2           #linkAddr:Landroid/net/LinkAddress;
    :cond_1a
    iget-object v4, p2, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@1c
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@1f
    move-result-object v4

    #@20
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v1

    #@24
    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_34

    #@2a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v3

    #@2e
    check-cast v3, Landroid/net/RouteInfo;

    #@30
    .line 1475
    .local v3, route:Landroid/net/RouteInfo;
    invoke-virtual {p1, v3}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V

    #@33
    goto :goto_24

    #@34
    .line 1477
    .end local v3           #route:Landroid/net/RouteInfo;
    :cond_34
    iget-object v4, p2, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@36
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@39
    move-result-object v4

    #@3a
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v1

    #@3e
    :goto_3e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_4e

    #@44
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v0

    #@48
    check-cast v0, Ljava/net/InetAddress;

    #@4a
    .line 1478
    .local v0, dns:Ljava/net/InetAddress;
    invoke-virtual {p1, v0}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@4d
    goto :goto_3e

    #@4e
    .line 1480
    .end local v0           #dns:Ljava/net/InetAddress;
    :cond_4e
    return-void
.end method

.method private addOrUpdateNetworkNative(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;
    .registers 27
    .parameter "config"

    #@0
    .prologue
    .line 1071
    sget-boolean v22, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v22, :cond_20

    #@4
    sget-object v22, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@6
    if-eqz v22, :cond_20

    #@8
    .line 1072
    sget-object v22, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a
    move-object/from16 v0, v22

    #@c
    move-object/from16 v1, p1

    #@e
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)Z

    #@11
    move-result v22

    #@12
    if-nez v22, :cond_20

    #@14
    .line 1073
    new-instance v17, Landroid/net/wifi/NetworkUpdateResult;

    #@16
    const/16 v22, -0x1

    #@18
    move-object/from16 v0, v17

    #@1a
    move/from16 v1, v22

    #@1c
    invoke-direct {v0, v1}, Landroid/net/wifi/NetworkUpdateResult;-><init>(I)V

    #@1f
    .line 1365
    :goto_1f
    return-object v17

    #@20
    .line 1082
    :cond_20
    move-object/from16 v0, p1

    #@22
    iget v15, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@24
    .line 1083
    .local v15, netId:I
    const/16 v16, 0x0

    #@26
    .line 1085
    .local v16, newNetwork:Z
    const/16 v22, -0x1

    #@28
    move/from16 v0, v22

    #@2a
    if-ne v15, v0, :cond_46

    #@2c
    .line 1086
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@30
    move-object/from16 v22, v0

    #@32
    invoke-static/range {p1 .. p1}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@35
    move-result v23

    #@36
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v23

    #@3a
    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    move-result-object v18

    #@3e
    check-cast v18, Ljava/lang/Integer;

    #@40
    .line 1087
    .local v18, savedNetId:Ljava/lang/Integer;
    if-eqz v18, :cond_c9

    #@42
    .line 1088
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    #@45
    move-result v15

    #@46
    .line 1099
    .end local v18           #savedNetId:Ljava/lang/Integer;
    :cond_46
    const/16 v19, 0x1

    #@48
    .line 1113
    .local v19, updateFailed:Z
    move-object/from16 v0, p1

    #@4a
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@4c
    move-object/from16 v22, v0

    #@4e
    if-eqz v22, :cond_12a

    #@50
    .line 1114
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useKoreanSsid()Z

    #@53
    move-result v22

    #@54
    if-eqz v22, :cond_ed

    #@56
    sget-object v22, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@58
    if-eqz v22, :cond_ed

    #@5a
    .line 1115
    sget-object v22, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@5c
    const-string/jumbo v23, "ssid"

    #@5f
    move-object/from16 v0, p1

    #@61
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@63
    move-object/from16 v24, v0

    #@65
    move-object/from16 v0, v22

    #@67
    move-object/from16 v1, v23

    #@69
    move-object/from16 v2, v24

    #@6b
    invoke-interface {v0, v15, v1, v2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setNetworkVariableCommand(ILjava/lang/String;Ljava/lang/String;)Z

    #@6e
    move-result v22

    #@6f
    if-nez v22, :cond_12a

    #@71
    .line 1119
    new-instance v22, Ljava/lang/StringBuilder;

    #@73
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v23, "failed to set Korean SSID: "

    #@78
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v22

    #@7c
    move-object/from16 v0, p1

    #@7e
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@80
    move-object/from16 v23, v0

    #@82
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v22

    #@86
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v22

    #@8a
    move-object/from16 v0, p0

    #@8c
    move-object/from16 v1, v22

    #@8e
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@91
    .line 1326
    :goto_91
    if-eqz v19, :cond_636

    #@93
    .line 1327
    if-eqz v16, :cond_bc

    #@95
    .line 1328
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@99
    move-object/from16 v22, v0

    #@9b
    move-object/from16 v0, v22

    #@9d
    invoke-virtual {v0, v15}, Landroid/net/wifi/WifiNative;->removeNetwork(I)Z

    #@a0
    .line 1329
    new-instance v22, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v23, "Failed to set a network variable, removed network: "

    #@a7
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v22

    #@ab
    move-object/from16 v0, v22

    #@ad
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v22

    #@b1
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v22

    #@b5
    move-object/from16 v0, p0

    #@b7
    move-object/from16 v1, v22

    #@b9
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@bc
    .line 1331
    :cond_bc
    new-instance v17, Landroid/net/wifi/NetworkUpdateResult;

    #@be
    const/16 v22, -0x1

    #@c0
    move-object/from16 v0, v17

    #@c2
    move/from16 v1, v22

    #@c4
    invoke-direct {v0, v1}, Landroid/net/wifi/NetworkUpdateResult;-><init>(I)V

    #@c7
    goto/16 :goto_1f

    #@c9
    .line 1090
    .end local v19           #updateFailed:Z
    .restart local v18       #savedNetId:Ljava/lang/Integer;
    :cond_c9
    const/16 v16, 0x1

    #@cb
    .line 1091
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@cf
    move-object/from16 v22, v0

    #@d1
    invoke-virtual/range {v22 .. v22}, Landroid/net/wifi/WifiNative;->addNetwork()I

    #@d4
    move-result v15

    #@d5
    .line 1092
    if-gez v15, :cond_46

    #@d7
    .line 1093
    const-string v22, "Failed to add a network!"

    #@d9
    move-object/from16 v0, p0

    #@db
    move-object/from16 v1, v22

    #@dd
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@e0
    .line 1094
    new-instance v17, Landroid/net/wifi/NetworkUpdateResult;

    #@e2
    const/16 v22, -0x1

    #@e4
    move-object/from16 v0, v17

    #@e6
    move/from16 v1, v22

    #@e8
    invoke-direct {v0, v1}, Landroid/net/wifi/NetworkUpdateResult;-><init>(I)V

    #@eb
    goto/16 :goto_1f

    #@ed
    .line 1124
    .end local v18           #savedNetId:Ljava/lang/Integer;
    .restart local v19       #updateFailed:Z
    :cond_ed
    move-object/from16 v0, p0

    #@ef
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@f1
    move-object/from16 v22, v0

    #@f3
    const-string/jumbo v23, "ssid"

    #@f6
    move-object/from16 v0, p1

    #@f8
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@fa
    move-object/from16 v24, v0

    #@fc
    move-object/from16 v0, v22

    #@fe
    move-object/from16 v1, v23

    #@100
    move-object/from16 v2, v24

    #@102
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@105
    move-result v22

    #@106
    if-nez v22, :cond_12a

    #@108
    .line 1128
    new-instance v22, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v23, "failed to set SSID: "

    #@10f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v22

    #@113
    move-object/from16 v0, p1

    #@115
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@117
    move-object/from16 v23, v0

    #@119
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v22

    #@11d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v22

    #@121
    move-object/from16 v0, p0

    #@123
    move-object/from16 v1, v22

    #@125
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@128
    goto/16 :goto_91

    #@12a
    .line 1133
    :cond_12a
    move-object/from16 v0, p1

    #@12c
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@12e
    move-object/from16 v22, v0

    #@130
    if-eqz v22, :cond_16e

    #@132
    move-object/from16 v0, p0

    #@134
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@136
    move-object/from16 v22, v0

    #@138
    const-string v23, "bssid"

    #@13a
    move-object/from16 v0, p1

    #@13c
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@13e
    move-object/from16 v24, v0

    #@140
    move-object/from16 v0, v22

    #@142
    move-object/from16 v1, v23

    #@144
    move-object/from16 v2, v24

    #@146
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@149
    move-result v22

    #@14a
    if-nez v22, :cond_16e

    #@14c
    .line 1138
    new-instance v22, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v23, "failed to set BSSID: "

    #@153
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v22

    #@157
    move-object/from16 v0, p1

    #@159
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@15b
    move-object/from16 v23, v0

    #@15d
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v22

    #@161
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v22

    #@165
    move-object/from16 v0, p0

    #@167
    move-object/from16 v1, v22

    #@169
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@16c
    goto/16 :goto_91

    #@16e
    .line 1142
    :cond_16e
    move-object/from16 v0, p1

    #@170
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@172
    move-object/from16 v22, v0

    #@174
    sget-object v23, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@176
    move-object/from16 v0, p0

    #@178
    move-object/from16 v1, v22

    #@17a
    move-object/from16 v2, v23

    #@17c
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->makeString(Ljava/util/BitSet;[Ljava/lang/String;)Ljava/lang/String;

    #@17f
    move-result-object v5

    #@180
    .line 1144
    .local v5, allowedKeyManagementString:Ljava/lang/String;
    move-object/from16 v0, p1

    #@182
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@184
    move-object/from16 v22, v0

    #@186
    invoke-virtual/range {v22 .. v22}, Ljava/util/BitSet;->cardinality()I

    #@189
    move-result v22

    #@18a
    if-eqz v22, :cond_1bd

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@190
    move-object/from16 v22, v0

    #@192
    const-string/jumbo v23, "key_mgmt"

    #@195
    move-object/from16 v0, v22

    #@197
    move-object/from16 v1, v23

    #@199
    invoke-virtual {v0, v15, v1, v5}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@19c
    move-result v22

    #@19d
    if-nez v22, :cond_1bd

    #@19f
    .line 1149
    new-instance v22, Ljava/lang/StringBuilder;

    #@1a1
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1a4
    const-string v23, "failed to set key_mgmt: "

    #@1a6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v22

    #@1aa
    move-object/from16 v0, v22

    #@1ac
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v22

    #@1b0
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b3
    move-result-object v22

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    move-object/from16 v1, v22

    #@1b8
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@1bb
    goto/16 :goto_91

    #@1bd
    .line 1154
    :cond_1bd
    move-object/from16 v0, p1

    #@1bf
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@1c1
    move-object/from16 v22, v0

    #@1c3
    sget-object v23, Landroid/net/wifi/WifiConfiguration$Protocol;->strings:[Ljava/lang/String;

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    move-object/from16 v1, v22

    #@1c9
    move-object/from16 v2, v23

    #@1cb
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->makeString(Ljava/util/BitSet;[Ljava/lang/String;)Ljava/lang/String;

    #@1ce
    move-result-object v7

    #@1cf
    .line 1156
    .local v7, allowedProtocolsString:Ljava/lang/String;
    move-object/from16 v0, p1

    #@1d1
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@1d3
    move-object/from16 v22, v0

    #@1d5
    invoke-virtual/range {v22 .. v22}, Ljava/util/BitSet;->cardinality()I

    #@1d8
    move-result v22

    #@1d9
    if-eqz v22, :cond_20c

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@1df
    move-object/from16 v22, v0

    #@1e1
    const-string/jumbo v23, "proto"

    #@1e4
    move-object/from16 v0, v22

    #@1e6
    move-object/from16 v1, v23

    #@1e8
    invoke-virtual {v0, v15, v1, v7}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@1eb
    move-result v22

    #@1ec
    if-nez v22, :cond_20c

    #@1ee
    .line 1161
    new-instance v22, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v23, "failed to set proto: "

    #@1f5
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v22

    #@1f9
    move-object/from16 v0, v22

    #@1fb
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v22

    #@1ff
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@202
    move-result-object v22

    #@203
    move-object/from16 v0, p0

    #@205
    move-object/from16 v1, v22

    #@207
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@20a
    goto/16 :goto_91

    #@20c
    .line 1166
    :cond_20c
    move-object/from16 v0, p1

    #@20e
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@210
    move-object/from16 v22, v0

    #@212
    sget-object v23, Landroid/net/wifi/WifiConfiguration$AuthAlgorithm;->strings:[Ljava/lang/String;

    #@214
    move-object/from16 v0, p0

    #@216
    move-object/from16 v1, v22

    #@218
    move-object/from16 v2, v23

    #@21a
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->makeString(Ljava/util/BitSet;[Ljava/lang/String;)Ljava/lang/String;

    #@21d
    move-result-object v3

    #@21e
    .line 1168
    .local v3, allowedAuthAlgorithmsString:Ljava/lang/String;
    move-object/from16 v0, p1

    #@220
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@222
    move-object/from16 v22, v0

    #@224
    invoke-virtual/range {v22 .. v22}, Ljava/util/BitSet;->cardinality()I

    #@227
    move-result v22

    #@228
    if-eqz v22, :cond_25a

    #@22a
    move-object/from16 v0, p0

    #@22c
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@22e
    move-object/from16 v22, v0

    #@230
    const-string v23, "auth_alg"

    #@232
    move-object/from16 v0, v22

    #@234
    move-object/from16 v1, v23

    #@236
    invoke-virtual {v0, v15, v1, v3}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@239
    move-result v22

    #@23a
    if-nez v22, :cond_25a

    #@23c
    .line 1173
    new-instance v22, Ljava/lang/StringBuilder;

    #@23e
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@241
    const-string v23, "failed to set auth_alg: "

    #@243
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@246
    move-result-object v22

    #@247
    move-object/from16 v0, v22

    #@249
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v22

    #@24d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@250
    move-result-object v22

    #@251
    move-object/from16 v0, p0

    #@253
    move-object/from16 v1, v22

    #@255
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@258
    goto/16 :goto_91

    #@25a
    .line 1178
    :cond_25a
    move-object/from16 v0, p1

    #@25c
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@25e
    move-object/from16 v22, v0

    #@260
    sget-object v23, Landroid/net/wifi/WifiConfiguration$PairwiseCipher;->strings:[Ljava/lang/String;

    #@262
    move-object/from16 v0, p0

    #@264
    move-object/from16 v1, v22

    #@266
    move-object/from16 v2, v23

    #@268
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->makeString(Ljava/util/BitSet;[Ljava/lang/String;)Ljava/lang/String;

    #@26b
    move-result-object v6

    #@26c
    .line 1181
    .local v6, allowedPairwiseCiphersString:Ljava/lang/String;
    move-object/from16 v0, p1

    #@26e
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@270
    move-object/from16 v22, v0

    #@272
    invoke-virtual/range {v22 .. v22}, Ljava/util/BitSet;->cardinality()I

    #@275
    move-result v22

    #@276
    if-eqz v22, :cond_2a9

    #@278
    move-object/from16 v0, p0

    #@27a
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@27c
    move-object/from16 v22, v0

    #@27e
    const-string/jumbo v23, "pairwise"

    #@281
    move-object/from16 v0, v22

    #@283
    move-object/from16 v1, v23

    #@285
    invoke-virtual {v0, v15, v1, v6}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@288
    move-result v22

    #@289
    if-nez v22, :cond_2a9

    #@28b
    .line 1186
    new-instance v22, Ljava/lang/StringBuilder;

    #@28d
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@290
    const-string v23, "failed to set pairwise: "

    #@292
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@295
    move-result-object v22

    #@296
    move-object/from16 v0, v22

    #@298
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v22

    #@29c
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29f
    move-result-object v22

    #@2a0
    move-object/from16 v0, p0

    #@2a2
    move-object/from16 v1, v22

    #@2a4
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@2a7
    goto/16 :goto_91

    #@2a9
    .line 1191
    :cond_2a9
    move-object/from16 v0, p1

    #@2ab
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@2ad
    move-object/from16 v22, v0

    #@2af
    sget-object v23, Landroid/net/wifi/WifiConfiguration$GroupCipher;->strings:[Ljava/lang/String;

    #@2b1
    move-object/from16 v0, p0

    #@2b3
    move-object/from16 v1, v22

    #@2b5
    move-object/from16 v2, v23

    #@2b7
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->makeString(Ljava/util/BitSet;[Ljava/lang/String;)Ljava/lang/String;

    #@2ba
    move-result-object v4

    #@2bb
    .line 1193
    .local v4, allowedGroupCiphersString:Ljava/lang/String;
    move-object/from16 v0, p1

    #@2bd
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@2bf
    move-object/from16 v22, v0

    #@2c1
    invoke-virtual/range {v22 .. v22}, Ljava/util/BitSet;->cardinality()I

    #@2c4
    move-result v22

    #@2c5
    if-eqz v22, :cond_2f7

    #@2c7
    move-object/from16 v0, p0

    #@2c9
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2cb
    move-object/from16 v22, v0

    #@2cd
    const-string v23, "group"

    #@2cf
    move-object/from16 v0, v22

    #@2d1
    move-object/from16 v1, v23

    #@2d3
    invoke-virtual {v0, v15, v1, v4}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@2d6
    move-result v22

    #@2d7
    if-nez v22, :cond_2f7

    #@2d9
    .line 1198
    new-instance v22, Ljava/lang/StringBuilder;

    #@2db
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@2de
    const-string v23, "failed to set group: "

    #@2e0
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e3
    move-result-object v22

    #@2e4
    move-object/from16 v0, v22

    #@2e6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e9
    move-result-object v22

    #@2ea
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ed
    move-result-object v22

    #@2ee
    move-object/from16 v0, p0

    #@2f0
    move-object/from16 v1, v22

    #@2f2
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@2f5
    goto/16 :goto_91

    #@2f7
    .line 1205
    :cond_2f7
    move-object/from16 v0, p1

    #@2f9
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@2fb
    move-object/from16 v22, v0

    #@2fd
    if-eqz v22, :cond_333

    #@2ff
    move-object/from16 v0, p1

    #@301
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@303
    move-object/from16 v22, v0

    #@305
    const-string v23, "*"

    #@307
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30a
    move-result v22

    #@30b
    if-nez v22, :cond_333

    #@30d
    move-object/from16 v0, p0

    #@30f
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@311
    move-object/from16 v22, v0

    #@313
    const-string/jumbo v23, "psk"

    #@316
    move-object/from16 v0, p1

    #@318
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@31a
    move-object/from16 v24, v0

    #@31c
    move-object/from16 v0, v22

    #@31e
    move-object/from16 v1, v23

    #@320
    move-object/from16 v2, v24

    #@322
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@325
    move-result v22

    #@326
    if-nez v22, :cond_333

    #@328
    .line 1210
    const-string v22, "failed to set psk"

    #@32a
    move-object/from16 v0, p0

    #@32c
    move-object/from16 v1, v22

    #@32e
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@331
    goto/16 :goto_91

    #@333
    .line 1214
    :cond_333
    const/4 v11, 0x0

    #@334
    .line 1215
    .local v11, hasSetKey:Z
    move-object/from16 v0, p1

    #@336
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@338
    move-object/from16 v22, v0

    #@33a
    if-eqz v22, :cond_3b8

    #@33c
    .line 1216
    const/4 v12, 0x0

    #@33d
    .local v12, i:I
    :goto_33d
    move-object/from16 v0, p1

    #@33f
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@341
    move-object/from16 v22, v0

    #@343
    move-object/from16 v0, v22

    #@345
    array-length v0, v0

    #@346
    move/from16 v22, v0

    #@348
    move/from16 v0, v22

    #@34a
    if-ge v12, v0, :cond_3b8

    #@34c
    .line 1219
    move-object/from16 v0, p1

    #@34e
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@350
    move-object/from16 v22, v0

    #@352
    aget-object v22, v22, v12

    #@354
    if-eqz v22, :cond_3b5

    #@356
    move-object/from16 v0, p1

    #@358
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@35a
    move-object/from16 v22, v0

    #@35c
    aget-object v22, v22, v12

    #@35e
    const-string v23, "*"

    #@360
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@363
    move-result v22

    #@364
    if-nez v22, :cond_3b5

    #@366
    .line 1220
    move-object/from16 v0, p0

    #@368
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@36a
    move-object/from16 v22, v0

    #@36c
    sget-object v23, Landroid/net/wifi/WifiConfiguration;->wepKeyVarNames:[Ljava/lang/String;

    #@36e
    aget-object v23, v23, v12

    #@370
    move-object/from16 v0, p1

    #@372
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@374
    move-object/from16 v24, v0

    #@376
    aget-object v24, v24, v12

    #@378
    move-object/from16 v0, v22

    #@37a
    move-object/from16 v1, v23

    #@37c
    move-object/from16 v2, v24

    #@37e
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@381
    move-result v22

    #@382
    if-nez v22, :cond_3b4

    #@384
    .line 1224
    new-instance v22, Ljava/lang/StringBuilder;

    #@386
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@389
    const-string v23, "failed to set wep_key"

    #@38b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38e
    move-result-object v22

    #@38f
    move-object/from16 v0, v22

    #@391
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@394
    move-result-object v22

    #@395
    const-string v23, ": "

    #@397
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39a
    move-result-object v22

    #@39b
    move-object/from16 v0, p1

    #@39d
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@39f
    move-object/from16 v23, v0

    #@3a1
    aget-object v23, v23, v12

    #@3a3
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a6
    move-result-object v22

    #@3a7
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3aa
    move-result-object v22

    #@3ab
    move-object/from16 v0, p0

    #@3ad
    move-object/from16 v1, v22

    #@3af
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@3b2
    goto/16 :goto_91

    #@3b4
    .line 1227
    :cond_3b4
    const/4 v11, 0x1

    #@3b5
    .line 1216
    :cond_3b5
    add-int/lit8 v12, v12, 0x1

    #@3b7
    goto :goto_33d

    #@3b8
    .line 1232
    .end local v12           #i:I
    :cond_3b8
    if-eqz v11, :cond_3fb

    #@3ba
    .line 1233
    move-object/from16 v0, p0

    #@3bc
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@3be
    move-object/from16 v22, v0

    #@3c0
    const-string/jumbo v23, "wep_tx_keyidx"

    #@3c3
    move-object/from16 v0, p1

    #@3c5
    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@3c7
    move/from16 v24, v0

    #@3c9
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3cc
    move-result-object v24

    #@3cd
    move-object/from16 v0, v22

    #@3cf
    move-object/from16 v1, v23

    #@3d1
    move-object/from16 v2, v24

    #@3d3
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@3d6
    move-result v22

    #@3d7
    if-nez v22, :cond_3fb

    #@3d9
    .line 1237
    new-instance v22, Ljava/lang/StringBuilder;

    #@3db
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@3de
    const-string v23, "failed to set wep_tx_keyidx: "

    #@3e0
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e3
    move-result-object v22

    #@3e4
    move-object/from16 v0, p1

    #@3e6
    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@3e8
    move/from16 v23, v0

    #@3ea
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3ed
    move-result-object v22

    #@3ee
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f1
    move-result-object v22

    #@3f2
    move-object/from16 v0, p0

    #@3f4
    move-object/from16 v1, v22

    #@3f6
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@3f9
    goto/16 :goto_91

    #@3fb
    .line 1242
    :cond_3fb
    move-object/from16 v0, p0

    #@3fd
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@3ff
    move-object/from16 v22, v0

    #@401
    const-string/jumbo v23, "priority"

    #@404
    move-object/from16 v0, p1

    #@406
    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@408
    move/from16 v24, v0

    #@40a
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@40d
    move-result-object v24

    #@40e
    move-object/from16 v0, v22

    #@410
    move-object/from16 v1, v23

    #@412
    move-object/from16 v2, v24

    #@414
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@417
    move-result v22

    #@418
    if-nez v22, :cond_446

    #@41a
    .line 1246
    new-instance v22, Ljava/lang/StringBuilder;

    #@41c
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@41f
    move-object/from16 v0, p1

    #@421
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@423
    move-object/from16 v23, v0

    #@425
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@428
    move-result-object v22

    #@429
    const-string v23, ": failed to set priority: "

    #@42b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42e
    move-result-object v22

    #@42f
    move-object/from16 v0, p1

    #@431
    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@433
    move/from16 v23, v0

    #@435
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@438
    move-result-object v22

    #@439
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43c
    move-result-object v22

    #@43d
    move-object/from16 v0, p0

    #@43f
    move-object/from16 v1, v22

    #@441
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@444
    goto/16 :goto_91

    #@446
    .line 1252
    :cond_446
    sget-boolean v22, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@448
    if-eqz v22, :cond_4a0

    #@44a
    sget-boolean v22, Landroid/net/wifi/WifiConfigStore;->mLgeKtCm:Z

    #@44c
    if-eqz v22, :cond_4a0

    #@44e
    .line 1253
    move-object/from16 v0, p0

    #@450
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@452
    move-object/from16 v23, v0

    #@454
    const-string/jumbo v24, "scan_ssid"

    #@457
    move-object/from16 v0, p1

    #@459
    iget-boolean v0, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@45b
    move/from16 v22, v0

    #@45d
    if-eqz v22, :cond_49d

    #@45f
    const/16 v22, 0x1

    #@461
    :goto_461
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@464
    move-result-object v22

    #@465
    move-object/from16 v0, v23

    #@467
    move-object/from16 v1, v24

    #@469
    move-object/from16 v2, v22

    #@46b
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@46e
    move-result v22

    #@46f
    if-nez v22, :cond_4fa

    #@471
    .line 1258
    new-instance v22, Ljava/lang/StringBuilder;

    #@473
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@476
    move-object/from16 v0, p1

    #@478
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@47a
    move-object/from16 v23, v0

    #@47c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47f
    move-result-object v22

    #@480
    const-string v23, ": failed to set hiddenSSID: "

    #@482
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@485
    move-result-object v22

    #@486
    move-object/from16 v0, p1

    #@488
    iget-boolean v0, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@48a
    move/from16 v23, v0

    #@48c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@48f
    move-result-object v22

    #@490
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@493
    move-result-object v22

    #@494
    move-object/from16 v0, p0

    #@496
    move-object/from16 v1, v22

    #@498
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@49b
    goto/16 :goto_91

    #@49d
    .line 1253
    :cond_49d
    const/16 v22, 0x0

    #@49f
    goto :goto_461

    #@4a0
    .line 1265
    :cond_4a0
    move-object/from16 v0, p1

    #@4a2
    iget-boolean v0, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@4a4
    move/from16 v22, v0

    #@4a6
    if-eqz v22, :cond_4fa

    #@4a8
    move-object/from16 v0, p0

    #@4aa
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@4ac
    move-object/from16 v23, v0

    #@4ae
    const-string/jumbo v24, "scan_ssid"

    #@4b1
    move-object/from16 v0, p1

    #@4b3
    iget-boolean v0, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@4b5
    move/from16 v22, v0

    #@4b7
    if-eqz v22, :cond_4f7

    #@4b9
    const/16 v22, 0x1

    #@4bb
    :goto_4bb
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4be
    move-result-object v22

    #@4bf
    move-object/from16 v0, v23

    #@4c1
    move-object/from16 v1, v24

    #@4c3
    move-object/from16 v2, v22

    #@4c5
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@4c8
    move-result v22

    #@4c9
    if-nez v22, :cond_4fa

    #@4cb
    .line 1269
    new-instance v22, Ljava/lang/StringBuilder;

    #@4cd
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@4d0
    move-object/from16 v0, p1

    #@4d2
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@4d4
    move-object/from16 v23, v0

    #@4d6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d9
    move-result-object v22

    #@4da
    const-string v23, ": failed to set hiddenSSID: "

    #@4dc
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4df
    move-result-object v22

    #@4e0
    move-object/from16 v0, p1

    #@4e2
    iget-boolean v0, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@4e4
    move/from16 v23, v0

    #@4e6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e9
    move-result-object v22

    #@4ea
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4ed
    move-result-object v22

    #@4ee
    move-object/from16 v0, p0

    #@4f0
    move-object/from16 v1, v22

    #@4f2
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@4f5
    goto/16 :goto_91

    #@4f7
    .line 1265
    :cond_4f7
    const/16 v22, 0x0

    #@4f9
    goto :goto_4bb

    #@4fa
    .line 1275
    :cond_4fa
    move-object/from16 v0, p1

    #@4fc
    iget-object v8, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@4fe
    .local v8, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v14, v8

    #@4ff
    .local v14, len$:I
    const/4 v13, 0x0

    #@500
    .local v13, i$:I
    :goto_500
    if-ge v13, v14, :cond_632

    #@502
    aget-object v10, v8, v13

    #@504
    .line 1276
    .local v10, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    invoke-virtual {v10}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->varName()Ljava/lang/String;

    #@507
    move-result-object v21

    #@508
    .line 1277
    .local v21, varName:Ljava/lang/String;
    invoke-virtual {v10}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@50b
    move-result-object v20

    #@50c
    .line 1278
    .local v20, value:Ljava/lang/String;
    if-eqz v20, :cond_62e

    #@50e
    .line 1279
    move-object/from16 v0, p1

    #@510
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@512
    move-object/from16 v22, v0

    #@514
    move-object/from16 v0, v22

    #@516
    if-ne v10, v0, :cond_56a

    #@518
    .line 1284
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    #@51b
    move-result v22

    #@51c
    if-nez v22, :cond_520

    #@51e
    .line 1285
    const-string v20, "0"

    #@520
    .line 1290
    :cond_520
    :goto_520
    move-object/from16 v0, p0

    #@522
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@524
    move-object/from16 v22, v0

    #@526
    move-object/from16 v0, v22

    #@528
    move-object/from16 v1, v21

    #@52a
    move-object/from16 v2, v20

    #@52c
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@52f
    move-result v22

    #@530
    if-nez v22, :cond_586

    #@532
    .line 1294
    new-instance v22, Ljava/lang/StringBuilder;

    #@534
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@537
    move-object/from16 v0, p1

    #@539
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@53b
    move-object/from16 v23, v0

    #@53d
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@540
    move-result-object v22

    #@541
    const-string v23, ": failed to set "

    #@543
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@546
    move-result-object v22

    #@547
    move-object/from16 v0, v22

    #@549
    move-object/from16 v1, v21

    #@54b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54e
    move-result-object v22

    #@54f
    const-string v23, ": "

    #@551
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@554
    move-result-object v22

    #@555
    move-object/from16 v0, v22

    #@557
    move-object/from16 v1, v20

    #@559
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55c
    move-result-object v22

    #@55d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@560
    move-result-object v22

    #@561
    move-object/from16 v0, p0

    #@563
    move-object/from16 v1, v22

    #@565
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@568
    goto/16 :goto_91

    #@56a
    .line 1287
    :cond_56a
    move-object/from16 v0, p1

    #@56c
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@56e
    move-object/from16 v22, v0

    #@570
    move-object/from16 v0, v22

    #@572
    if-eq v10, v0, :cond_520

    #@574
    .line 1288
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    #@577
    move-result v22

    #@578
    if-nez v22, :cond_57d

    #@57a
    const-string v20, "NULL"

    #@57c
    :goto_57c
    goto :goto_520

    #@57d
    :cond_57d
    move-object/from16 v0, p0

    #@57f
    move-object/from16 v1, v20

    #@581
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@584
    move-result-object v20

    #@585
    goto :goto_57c

    #@586
    .line 1299
    :cond_586
    sget-boolean v22, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@588
    if-eqz v22, :cond_62e

    #@58a
    .line 1300
    move-object/from16 v0, p1

    #@58c
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@58e
    move-object/from16 v22, v0

    #@590
    move-object/from16 v0, v22

    #@592
    if-ne v10, v0, :cond_62e

    #@594
    const-string v22, "FAST"

    #@596
    move-object/from16 v0, v20

    #@598
    move-object/from16 v1, v22

    #@59a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59d
    move-result v22

    #@59e
    if-eqz v22, :cond_62e

    #@5a0
    .line 1302
    move-object/from16 v0, p0

    #@5a2
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@5a4
    move-object/from16 v22, v0

    #@5a6
    const-string/jumbo v23, "phase1"

    #@5a9
    const-string v24, "fast_provisioning=2"

    #@5ab
    move-object/from16 v0, p0

    #@5ad
    move-object/from16 v1, v24

    #@5af
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@5b2
    move-result-object v24

    #@5b3
    move-object/from16 v0, v22

    #@5b5
    move-object/from16 v1, v23

    #@5b7
    move-object/from16 v2, v24

    #@5b9
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@5bc
    move-result v22

    #@5bd
    if-nez v22, :cond_5e7

    #@5bf
    .line 1306
    new-instance v22, Ljava/lang/StringBuilder;

    #@5c1
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@5c4
    move-object/from16 v0, p1

    #@5c6
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@5c8
    move-object/from16 v23, v0

    #@5ca
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5cd
    move-result-object v22

    #@5ce
    const-string v23, ": failed to set phase1"

    #@5d0
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d3
    move-result-object v22

    #@5d4
    const-string v23, ": fast_provisioning=2"

    #@5d6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d9
    move-result-object v22

    #@5da
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5dd
    move-result-object v22

    #@5de
    move-object/from16 v0, p0

    #@5e0
    move-object/from16 v1, v22

    #@5e2
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@5e5
    goto/16 :goto_91

    #@5e7
    .line 1310
    :cond_5e7
    move-object/from16 v0, p0

    #@5e9
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@5eb
    move-object/from16 v22, v0

    #@5ed
    const-string/jumbo v23, "pac_file"

    #@5f0
    const-string v24, "/data/misc/wifi/fast-mschapv2.pac"

    #@5f2
    move-object/from16 v0, p0

    #@5f4
    move-object/from16 v1, v24

    #@5f6
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@5f9
    move-result-object v24

    #@5fa
    move-object/from16 v0, v22

    #@5fc
    move-object/from16 v1, v23

    #@5fe
    move-object/from16 v2, v24

    #@600
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@603
    move-result v22

    #@604
    if-nez v22, :cond_62e

    #@606
    .line 1314
    new-instance v22, Ljava/lang/StringBuilder;

    #@608
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@60b
    move-object/from16 v0, p1

    #@60d
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@60f
    move-object/from16 v23, v0

    #@611
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@614
    move-result-object v22

    #@615
    const-string v23, ": failed to set pac_file"

    #@617
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61a
    move-result-object v22

    #@61b
    const-string v23, ": /data/misc/wifi/fast-mschapv2.pac"

    #@61d
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@620
    move-result-object v22

    #@621
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@624
    move-result-object v22

    #@625
    move-object/from16 v0, p0

    #@627
    move-object/from16 v1, v22

    #@629
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@62c
    goto/16 :goto_91

    #@62e
    .line 1275
    :cond_62e
    add-int/lit8 v13, v13, 0x1

    #@630
    goto/16 :goto_500

    #@632
    .line 1323
    .end local v10           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v20           #value:Ljava/lang/String;
    .end local v21           #varName:Ljava/lang/String;
    :cond_632
    const/16 v19, 0x0

    #@634
    goto/16 :goto_91

    #@636
    .line 1341
    .end local v3           #allowedAuthAlgorithmsString:Ljava/lang/String;
    .end local v4           #allowedGroupCiphersString:Ljava/lang/String;
    .end local v5           #allowedKeyManagementString:Ljava/lang/String;
    .end local v6           #allowedPairwiseCiphersString:Ljava/lang/String;
    .end local v7           #allowedProtocolsString:Ljava/lang/String;
    .end local v8           #arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v11           #hasSetKey:Z
    .end local v13           #i$:I
    .end local v14           #len$:I
    :cond_636
    move-object/from16 v0, p0

    #@638
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@63a
    move-object/from16 v22, v0

    #@63c
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@63f
    move-result-object v23

    #@640
    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@643
    move-result-object v9

    #@644
    check-cast v9, Landroid/net/wifi/WifiConfiguration;

    #@646
    .line 1343
    .local v9, currentConfig:Landroid/net/wifi/WifiConfiguration;
    sget-boolean v22, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@648
    if-eqz v22, :cond_66a

    #@64a
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@64d
    move-result-object v22

    #@64e
    const-string v23, "KT"

    #@650
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@653
    move-result v22

    #@654
    if-eqz v22, :cond_66a

    #@656
    sget-boolean v22, Landroid/net/wifi/WifiConfigStore;->mLgeKtCm:Z

    #@658
    if-nez v22, :cond_66a

    #@65a
    .line 1345
    if-eqz v9, :cond_66a

    #@65c
    .line 1347
    move-object/from16 v0, p0

    #@65e
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@660
    move-object/from16 v22, v0

    #@662
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@665
    move-result-object v23

    #@666
    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@669
    .line 1348
    const/4 v9, 0x0

    #@66a
    .line 1352
    :cond_66a
    if-nez v9, :cond_673

    #@66c
    .line 1353
    new-instance v9, Landroid/net/wifi/WifiConfiguration;

    #@66e
    .end local v9           #currentConfig:Landroid/net/wifi/WifiConfiguration;
    invoke-direct {v9}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@671
    .line 1354
    .restart local v9       #currentConfig:Landroid/net/wifi/WifiConfiguration;
    iput v15, v9, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@673
    .line 1357
    :cond_673
    move-object/from16 v0, p0

    #@675
    invoke-direct {v0, v9}, Landroid/net/wifi/WifiConfigStore;->readNetworkVariables(Landroid/net/wifi/WifiConfiguration;)V

    #@678
    .line 1359
    move-object/from16 v0, p0

    #@67a
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@67c
    move-object/from16 v22, v0

    #@67e
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@681
    move-result-object v23

    #@682
    move-object/from16 v0, v22

    #@684
    move-object/from16 v1, v23

    #@686
    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@689
    .line 1360
    move-object/from16 v0, p0

    #@68b
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@68d
    move-object/from16 v22, v0

    #@68f
    invoke-static {v9}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@692
    move-result v23

    #@693
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@696
    move-result-object v23

    #@697
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69a
    move-result-object v24

    #@69b
    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69e
    .line 1362
    move-object/from16 v0, p0

    #@6a0
    move-object/from16 v1, p1

    #@6a2
    invoke-direct {v0, v9, v1}, Landroid/net/wifi/WifiConfigStore;->writeIpAndProxyConfigurationsOnChange(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@6a5
    move-result-object v17

    #@6a6
    .line 1363
    .local v17, result:Landroid/net/wifi/NetworkUpdateResult;
    move-object/from16 v0, v17

    #@6a8
    move/from16 v1, v16

    #@6aa
    invoke-virtual {v0, v1}, Landroid/net/wifi/NetworkUpdateResult;->setIsNewNetwork(Z)V

    #@6ad
    .line 1364
    move-object/from16 v0, v17

    #@6af
    invoke-virtual {v0, v15}, Landroid/net/wifi/NetworkUpdateResult;->setNetworkId(I)V

    #@6b2
    goto/16 :goto_1f
.end method

.method private checkRemovableNetwork(I)Z
    .registers 3
    .parameter "netId"

    #@0
    .prologue
    .line 1812
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 1813
    sget-object v0, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@6
    if-eqz v0, :cond_12

    #@8
    sget-object v0, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a
    invoke-interface {v0, p1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->checkRemovableNetwork(I)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 1819
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    .line 1816
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private static configKey(Landroid/net/wifi/WifiConfiguration;)I
    .registers 7
    .parameter "config"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 1767
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@5
    invoke-virtual {v1, v4}, Ljava/util/BitSet;->get(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_27

    #@b
    .line 1768
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    sget-object v2, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@18
    aget-object v2, v2, v4

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 1781
    .local v0, key:Ljava/lang/String;
    :goto_22
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@25
    move-result v1

    #@26
    return v1

    #@27
    .line 1769
    .end local v0           #key:Ljava/lang/String;
    :cond_27
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@29
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->get(I)Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_38

    #@2f
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@31
    const/4 v2, 0x3

    #@32
    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_5a

    #@38
    .line 1772
    :cond_38
    new-instance v1, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    sget-object v2, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@45
    aget-object v2, v2, v5

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@4d
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .restart local v0       #key:Ljava/lang/String;
    goto :goto_22

    #@5a
    .line 1775
    .end local v0           #key:Ljava/lang/String;
    :cond_5a
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@5c
    aget-object v1, v1, v3

    #@5e
    if-eqz v1, :cond_76

    #@60
    .line 1776
    new-instance v1, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    const-string v2, "WEP"

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    .restart local v0       #key:Ljava/lang/String;
    goto :goto_22

    #@76
    .line 1778
    .end local v0           #key:Ljava/lang/String;
    :cond_76
    new-instance v1, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@7d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    sget-object v2, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@83
    aget-object v2, v2, v3

    #@85
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v0

    #@8d
    .restart local v0       #key:Ljava/lang/String;
    goto :goto_22
.end method

.method private convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "string"

    #@0
    .prologue
    .line 1723
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "\""

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "\""

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 1804
    const-string v0, "WifiConfigStore"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 1805
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 1800
    const-string v0, "WifiConfigStore"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 1801
    return-void
.end method

.method private lookupString(Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7
    .parameter "string"
    .parameter "strings"

    #@0
    .prologue
    .line 1747
    array-length v1, p2

    #@1
    .line 1749
    .local v1, size:I
    const/16 v2, 0x2d

    #@3
    const/16 v3, 0x5f

    #@5
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@8
    move-result-object p1

    #@9
    .line 1751
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_18

    #@c
    .line 1752
    aget-object v2, p2, v0

    #@e
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_15

    #@14
    .line 1760
    .end local v0           #i:I
    :goto_14
    return v0

    #@15
    .line 1751
    .restart local v0       #i:I
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_a

    #@18
    .line 1758
    :cond_18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "Failed to look-up a string: "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-direct {p0, v2}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@2e
    .line 1760
    const/4 v0, -0x1

    #@2f
    goto :goto_14
.end method

.method private makeString(Ljava/util/BitSet;[Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "set"
    .parameter "strings"

    #@0
    .prologue
    .line 1727
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 1728
    .local v0, buf:Ljava/lang/StringBuffer;
    const/4 v1, -0x1

    #@6
    .line 1732
    .local v1, nextSetBit:I
    const/4 v2, 0x0

    #@7
    array-length v3, p2

    #@8
    invoke-virtual {p1, v2, v3}, Ljava/util/BitSet;->get(II)Ljava/util/BitSet;

    #@b
    move-result-object p1

    #@c
    .line 1734
    :goto_c
    add-int/lit8 v2, v1, 0x1

    #@e
    invoke-virtual {p1, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    #@11
    move-result v1

    #@12
    const/4 v2, -0x1

    #@13
    if-eq v1, v2, :cond_29

    #@15
    .line 1735
    aget-object v2, p2, v1

    #@17
    const/16 v3, 0x5f

    #@19
    const/16 v4, 0x2d

    #@1b
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@22
    move-result-object v2

    #@23
    const/16 v3, 0x20

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@28
    goto :goto_c

    #@29
    .line 1739
    :cond_29
    invoke-virtual {p1}, Ljava/util/BitSet;->cardinality()I

    #@2c
    move-result v2

    #@2d
    if-lez v2, :cond_38

    #@2f
    .line 1740
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    #@32
    move-result v2

    #@33
    add-int/lit8 v2, v2, -0x1

    #@35
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    #@38
    .line 1743
    :cond_38
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    return-object v2
.end method

.method private markAllNetworksDisabled()V
    .registers 2

    #@0
    .prologue
    .line 778
    const/4 v0, -0x1

    #@1
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiConfigStore;->markAllNetworksDisabledExcept(I)V

    #@4
    .line 779
    return-void
.end method

.method private markAllNetworksDisabledExcept(I)V
    .registers 6
    .parameter "netId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 767
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@3
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v2

    #@7
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_27

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@17
    .line 768
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_b

    #@19
    iget v2, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@1b
    if-eq v2, p1, :cond_b

    #@1d
    .line 769
    iget v2, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@1f
    if-eq v2, v3, :cond_b

    #@21
    .line 770
    iput v3, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@23
    .line 771
    const/4 v2, 0x0

    #@24
    iput v2, v0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@26
    goto :goto_b

    #@27
    .line 775
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_27
    return-void
.end method

.method private migrateOldEapTlsIfNecessary(Landroid/net/wifi/WifiConfiguration;I)V
    .registers 13
    .parameter "config"
    .parameter "netId"

    #@0
    .prologue
    .line 1673
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    const-string/jumbo v8, "private_key"

    #@5
    invoke-virtual {v7, p2, v8}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v6

    #@9
    .line 1679
    .local v6, value:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v7

    #@d
    if-eqz v7, :cond_10

    #@f
    .line 1715
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1683
    :cond_10
    invoke-direct {p0, v6}, Landroid/net/wifi/WifiConfigStore;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v6

    #@14
    .line 1684
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@17
    move-result v7

    #@18
    if-nez v7, :cond_f

    #@1a
    .line 1689
    iget-object v7, p1, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@1c
    const-string v8, "1"

    #@1e
    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@21
    .line 1690
    iget-object v7, p1, Landroid/net/wifi/WifiConfiguration;->engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@23
    const-string/jumbo v8, "keystore"

    #@26
    invoke-direct {p0, v8}, Landroid/net/wifi/WifiConfigStore;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@2d
    .line 1697
    const-string/jumbo v7, "keystore://"

    #@30
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@33
    move-result v7

    #@34
    if-eqz v7, :cond_78

    #@36
    .line 1698
    new-instance v3, Ljava/lang/String;

    #@38
    const-string/jumbo v7, "keystore://"

    #@3b
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@3e
    move-result v7

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    invoke-direct {v3, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@46
    .line 1702
    .local v3, keyName:Ljava/lang/String;
    :goto_46
    iget-object v7, p1, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@48
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiConfigStore;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v8

    #@4c
    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@4f
    .line 1705
    const/4 v7, 0x3

    #@50
    new-array v5, v7, [Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@52
    const/4 v7, 0x0

    #@53
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@55
    aput-object v8, v5, v7

    #@57
    const/4 v7, 0x1

    #@58
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@5a
    aput-object v8, v5, v7

    #@5c
    const/4 v7, 0x2

    #@5d
    iget-object v8, p1, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@5f
    aput-object v8, v5, v7

    #@61
    .line 1706
    .local v5, needsUpdate:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    move-object v0, v5

    #@62
    .local v0, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v4, v0

    #@63
    .local v4, len$:I
    const/4 v2, 0x0

    #@64
    .local v2, i$:I
    :goto_64
    if-ge v2, v4, :cond_7a

    #@66
    aget-object v1, v0, v2

    #@68
    .line 1707
    .local v1, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@6a
    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->varName()Ljava/lang/String;

    #@6d
    move-result-object v8

    #@6e
    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@71
    move-result-object v9

    #@72
    invoke-virtual {v7, p2, v8, v9}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@75
    .line 1706
    add-int/lit8 v2, v2, 0x1

    #@77
    goto :goto_64

    #@78
    .line 1700
    .end local v0           #arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v1           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v2           #i$:I
    .end local v3           #keyName:Ljava/lang/String;
    .end local v4           #len$:I
    .end local v5           #needsUpdate:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    :cond_78
    move-object v3, v6

    #@79
    .restart local v3       #keyName:Ljava/lang/String;
    goto :goto_46

    #@7a
    .line 1711
    .restart local v0       #arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .restart local v2       #i$:I
    .restart local v4       #len$:I
    .restart local v5       #needsUpdate:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    :cond_7a
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7c
    const-string/jumbo v8, "private_key"

    #@7f
    const-string v9, ""

    #@81
    invoke-direct {p0, v9}, Landroid/net/wifi/WifiConfigStore;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v9

    #@85
    invoke-virtual {v7, p2, v8, v9}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@88
    .line 1714
    invoke-virtual {p0}, Landroid/net/wifi/WifiConfigStore;->saveConfig()Z

    #@8b
    goto :goto_f
.end method

.method private readIpAndProxyConfigurations()V
    .registers 23

    #@0
    .prologue
    .line 941
    const/4 v8, 0x0

    #@1
    .line 943
    .local v8, in:Ljava/io/DataInputStream;
    :try_start_1
    new-instance v9, Ljava/io/DataInputStream;

    #@3
    new-instance v19, Ljava/io/BufferedInputStream;

    #@5
    new-instance v20, Ljava/io/FileInputStream;

    #@7
    sget-object v21, Landroid/net/wifi/WifiConfigStore;->ipConfigFile:Ljava/lang/String;

    #@9
    invoke-direct/range {v20 .. v21}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@c
    invoke-direct/range {v19 .. v20}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@f
    move-object/from16 v0, v19

    #@11
    invoke-direct {v9, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_250
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_14} :catch_256
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_14} :catch_253

    #@14
    .line 946
    .end local v8           #in:Ljava/io/DataInputStream;
    .local v9, in:Ljava/io/DataInputStream;
    :try_start_14
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@17
    move-result v18

    #@18
    .line 947
    .local v18, version:I
    const/16 v19, 0x2

    #@1a
    move/from16 v0, v18

    #@1c
    move/from16 v1, v19

    #@1e
    if-eq v0, v1, :cond_6d

    #@20
    const/16 v19, 0x1

    #@22
    move/from16 v0, v18

    #@24
    move/from16 v1, v19

    #@26
    if-eq v0, v1, :cond_6d

    #@28
    .line 948
    const-string v19, "Bad version on IP configuration file, ignore read"

    #@2a
    move-object/from16 v0, p0

    #@2c
    move-object/from16 v1, v19

    #@2e
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V
    :try_end_31
    .catchall {:try_start_14 .. :try_end_31} :catchall_18f
    .catch Ljava/io/EOFException; {:try_start_14 .. :try_end_31} :catch_e2
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_31} :catch_118

    #@31
    .line 1058
    if-eqz v9, :cond_36

    #@33
    .line 1060
    :try_start_33
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_36} :catch_24a

    #@36
    :cond_36
    :goto_36
    move-object v8, v9

    #@37
    .line 1064
    .end local v9           #in:Ljava/io/DataInputStream;
    .end local v18           #version:I
    .restart local v8       #in:Ljava/io/DataInputStream;
    :cond_37
    :goto_37
    return-void

    #@38
    .line 1001
    .end local v8           #in:Ljava/io/DataInputStream;
    .local v5, exclusionList:Ljava/lang/String;
    .local v7, id:I
    .restart local v9       #in:Ljava/io/DataInputStream;
    .local v10, ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .local v11, key:Ljava/lang/String;
    .local v13, linkProperties:Landroid/net/LinkProperties;
    .local v14, proxyHost:Ljava/lang/String;
    .local v15, proxyPort:I
    .local v17, proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .restart local v18       #version:I
    :cond_38
    :try_start_38
    const-string v19, "eos"

    #@3a
    move-object/from16 v0, v19

    #@3c
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3f
    .catchall {:try_start_38 .. :try_end_3f} :catchall_18f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_38 .. :try_end_3f} :catch_c4
    .catch Ljava/io/EOFException; {:try_start_38 .. :try_end_3f} :catch_e2
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_3f} :catch_118

    #@3f
    move-result v19

    #@40
    if-eqz v19, :cond_1de

    #@42
    .line 1011
    const/16 v19, -0x1

    #@44
    move/from16 v0, v19

    #@46
    if-eq v7, v0, :cond_6d

    #@48
    .line 1012
    :try_start_48
    move-object/from16 v0, p0

    #@4a
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@4c
    move-object/from16 v19, v0

    #@4e
    move-object/from16 v0, p0

    #@50
    iget-object v0, v0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@52
    move-object/from16 v20, v0

    #@54
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57
    move-result-object v21

    #@58
    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    move-result-object v20

    #@5c
    invoke-virtual/range {v19 .. v20}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5f
    move-result-object v2

    #@60
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@62
    .line 1015
    .local v2, config:Landroid/net/wifi/WifiConfiguration;
    if-nez v2, :cond_203

    #@64
    .line 1016
    const-string v19, "configuration found for missing network, ignored"

    #@66
    move-object/from16 v0, p0

    #@68
    move-object/from16 v1, v19

    #@6a
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@6d
    .line 953
    .end local v2           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v5           #exclusionList:Ljava/lang/String;
    .end local v7           #id:I
    .end local v10           #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .end local v11           #key:Ljava/lang/String;
    .end local v13           #linkProperties:Landroid/net/LinkProperties;
    .end local v14           #proxyHost:Ljava/lang/String;
    .end local v15           #proxyPort:I
    .end local v17           #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    :cond_6d
    :goto_6d
    :pswitch_6d
    const/4 v7, -0x1

    #@6e
    .line 954
    .restart local v7       #id:I
    sget-object v10, Landroid/net/wifi/WifiConfiguration$IpAssignment;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@70
    .line 955
    .restart local v10       #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    sget-object v17, Landroid/net/wifi/WifiConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@72
    .line 956
    .restart local v17       #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    new-instance v13, Landroid/net/LinkProperties;

    #@74
    invoke-direct {v13}, Landroid/net/LinkProperties;-><init>()V

    #@77
    .line 957
    .restart local v13       #linkProperties:Landroid/net/LinkProperties;
    const/4 v14, 0x0

    #@78
    .line 958
    .restart local v14       #proxyHost:Ljava/lang/String;
    const/4 v15, -0x1

    #@79
    .line 959
    .restart local v15       #proxyPort:I
    const/4 v5, 0x0

    #@7a
    .line 963
    .restart local v5       #exclusionList:Ljava/lang/String;
    :goto_7a
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;
    :try_end_7d
    .catchall {:try_start_48 .. :try_end_7d} :catchall_18f
    .catch Ljava/io/EOFException; {:try_start_48 .. :try_end_7d} :catch_e2
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_7d} :catch_118

    #@7d
    move-result-object v11

    #@7e
    .line 965
    .restart local v11       #key:Ljava/lang/String;
    :try_start_7e
    const-string v19, "id"

    #@80
    move-object/from16 v0, v19

    #@82
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v19

    #@86
    if-eqz v19, :cond_8d

    #@88
    .line 966
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@8b
    move-result v7

    #@8c
    goto :goto_7a

    #@8d
    .line 967
    :cond_8d
    const-string v19, "ipAssignment"

    #@8f
    move-object/from16 v0, v19

    #@91
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v19

    #@95
    if-eqz v19, :cond_a0

    #@97
    .line 968
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@9a
    move-result-object v19

    #@9b
    invoke-static/range {v19 .. v19}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@9e
    move-result-object v10

    #@9f
    goto :goto_7a

    #@a0
    .line 969
    :cond_a0
    const-string/jumbo v19, "linkAddress"

    #@a3
    move-object/from16 v0, v19

    #@a5
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v19

    #@a9
    if-eqz v19, :cond_ee

    #@ab
    .line 970
    new-instance v12, Landroid/net/LinkAddress;

    #@ad
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@b0
    move-result-object v19

    #@b1
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@b4
    move-result-object v19

    #@b5
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@b8
    move-result v20

    #@b9
    move-object/from16 v0, v19

    #@bb
    move/from16 v1, v20

    #@bd
    invoke-direct {v12, v0, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@c0
    .line 972
    .local v12, linkAddr:Landroid/net/LinkAddress;
    invoke-virtual {v13, v12}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V
    :try_end_c3
    .catchall {:try_start_7e .. :try_end_c3} :catchall_18f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7e .. :try_end_c3} :catch_c4
    .catch Ljava/io/EOFException; {:try_start_7e .. :try_end_c3} :catch_e2
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_c3} :catch_118

    #@c3
    goto :goto_7a

    #@c4
    .line 1006
    .end local v12           #linkAddr:Landroid/net/LinkAddress;
    :catch_c4
    move-exception v4

    #@c5
    .line 1007
    .local v4, e:Ljava/lang/IllegalArgumentException;
    :try_start_c5
    new-instance v19, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v20, "Ignore invalid address while reading"

    #@cc
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v19

    #@d0
    move-object/from16 v0, v19

    #@d2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v19

    #@d6
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v19

    #@da
    move-object/from16 v0, p0

    #@dc
    move-object/from16 v1, v19

    #@de
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V
    :try_end_e1
    .catchall {:try_start_c5 .. :try_end_e1} :catchall_18f
    .catch Ljava/io/EOFException; {:try_start_c5 .. :try_end_e1} :catch_e2
    .catch Ljava/io/IOException; {:try_start_c5 .. :try_end_e1} :catch_118

    #@e1
    goto :goto_7a

    #@e2
    .line 1054
    .end local v4           #e:Ljava/lang/IllegalArgumentException;
    .end local v5           #exclusionList:Ljava/lang/String;
    .end local v7           #id:I
    .end local v10           #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .end local v11           #key:Ljava/lang/String;
    .end local v13           #linkProperties:Landroid/net/LinkProperties;
    .end local v14           #proxyHost:Ljava/lang/String;
    .end local v15           #proxyPort:I
    .end local v17           #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .end local v18           #version:I
    :catch_e2
    move-exception v19

    #@e3
    move-object v8, v9

    #@e4
    .line 1058
    .end local v9           #in:Ljava/io/DataInputStream;
    .restart local v8       #in:Ljava/io/DataInputStream;
    :goto_e4
    if-eqz v8, :cond_37

    #@e6
    .line 1060
    :try_start_e6
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V
    :try_end_e9
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_e9} :catch_eb

    #@e9
    goto/16 :goto_37

    #@eb
    .line 1061
    :catch_eb
    move-exception v19

    #@ec
    goto/16 :goto_37

    #@ee
    .line 973
    .end local v8           #in:Ljava/io/DataInputStream;
    .restart local v5       #exclusionList:Ljava/lang/String;
    .restart local v7       #id:I
    .restart local v9       #in:Ljava/io/DataInputStream;
    .restart local v10       #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .restart local v11       #key:Ljava/lang/String;
    .restart local v13       #linkProperties:Landroid/net/LinkProperties;
    .restart local v14       #proxyHost:Ljava/lang/String;
    .restart local v15       #proxyPort:I
    .restart local v17       #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .restart local v18       #version:I
    :cond_ee
    :try_start_ee
    const-string v19, "gateway"

    #@f0
    move-object/from16 v0, v19

    #@f2
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v19

    #@f6
    if-eqz v19, :cond_176

    #@f8
    .line 974
    const/4 v3, 0x0

    #@f9
    .line 975
    .local v3, dest:Landroid/net/LinkAddress;
    const/4 v6, 0x0

    #@fa
    .line 976
    .local v6, gateway:Ljava/net/InetAddress;
    const/16 v19, 0x1

    #@fc
    move/from16 v0, v18

    #@fe
    move/from16 v1, v19

    #@100
    if-ne v0, v1, :cond_140

    #@102
    .line 978
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@105
    move-result-object v19

    #@106
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@109
    move-result-object v6

    #@10a
    .line 989
    :cond_10a
    :goto_10a
    new-instance v19, Landroid/net/RouteInfo;

    #@10c
    move-object/from16 v0, v19

    #@10e
    invoke-direct {v0, v3, v6}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@111
    move-object/from16 v0, v19

    #@113
    invoke-virtual {v13, v0}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V
    :try_end_116
    .catchall {:try_start_ee .. :try_end_116} :catchall_18f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_ee .. :try_end_116} :catch_c4
    .catch Ljava/io/EOFException; {:try_start_ee .. :try_end_116} :catch_e2
    .catch Ljava/io/IOException; {:try_start_ee .. :try_end_116} :catch_118

    #@116
    goto/16 :goto_7a

    #@118
    .line 1055
    .end local v3           #dest:Landroid/net/LinkAddress;
    .end local v5           #exclusionList:Ljava/lang/String;
    .end local v6           #gateway:Ljava/net/InetAddress;
    .end local v7           #id:I
    .end local v10           #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .end local v11           #key:Ljava/lang/String;
    .end local v13           #linkProperties:Landroid/net/LinkProperties;
    .end local v14           #proxyHost:Ljava/lang/String;
    .end local v15           #proxyPort:I
    .end local v17           #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .end local v18           #version:I
    :catch_118
    move-exception v4

    #@119
    move-object v8, v9

    #@11a
    .line 1056
    .end local v9           #in:Ljava/io/DataInputStream;
    .local v4, e:Ljava/io/IOException;
    .restart local v8       #in:Ljava/io/DataInputStream;
    :goto_11a
    :try_start_11a
    new-instance v19, Ljava/lang/StringBuilder;

    #@11c
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@11f
    const-string v20, "Error parsing configuration"

    #@121
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v19

    #@125
    move-object/from16 v0, v19

    #@127
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v19

    #@12b
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12e
    move-result-object v19

    #@12f
    move-object/from16 v0, p0

    #@131
    move-object/from16 v1, v19

    #@133
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V
    :try_end_136
    .catchall {:try_start_11a .. :try_end_136} :catchall_250

    #@136
    .line 1058
    if-eqz v8, :cond_37

    #@138
    .line 1060
    :try_start_138
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V
    :try_end_13b
    .catch Ljava/lang/Exception; {:try_start_138 .. :try_end_13b} :catch_13d

    #@13b
    goto/16 :goto_37

    #@13d
    .line 1061
    :catch_13d
    move-exception v19

    #@13e
    goto/16 :goto_37

    #@140
    .line 980
    .end local v4           #e:Ljava/io/IOException;
    .end local v8           #in:Ljava/io/DataInputStream;
    .restart local v3       #dest:Landroid/net/LinkAddress;
    .restart local v5       #exclusionList:Ljava/lang/String;
    .restart local v6       #gateway:Ljava/net/InetAddress;
    .restart local v7       #id:I
    .restart local v9       #in:Ljava/io/DataInputStream;
    .restart local v10       #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .restart local v11       #key:Ljava/lang/String;
    .restart local v13       #linkProperties:Landroid/net/LinkProperties;
    .restart local v14       #proxyHost:Ljava/lang/String;
    .restart local v15       #proxyPort:I
    .restart local v17       #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .restart local v18       #version:I
    :cond_140
    :try_start_140
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@143
    move-result v19

    #@144
    const/16 v20, 0x1

    #@146
    move/from16 v0, v19

    #@148
    move/from16 v1, v20

    #@14a
    if-ne v0, v1, :cond_161

    #@14c
    .line 981
    new-instance v3, Landroid/net/LinkAddress;

    #@14e
    .end local v3           #dest:Landroid/net/LinkAddress;
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@151
    move-result-object v19

    #@152
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@155
    move-result-object v19

    #@156
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@159
    move-result v20

    #@15a
    move-object/from16 v0, v19

    #@15c
    move/from16 v1, v20

    #@15e
    invoke-direct {v3, v0, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@161
    .line 985
    .restart local v3       #dest:Landroid/net/LinkAddress;
    :cond_161
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@164
    move-result v19

    #@165
    const/16 v20, 0x1

    #@167
    move/from16 v0, v19

    #@169
    move/from16 v1, v20

    #@16b
    if-ne v0, v1, :cond_10a

    #@16d
    .line 986
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@170
    move-result-object v19

    #@171
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@174
    move-result-object v6

    #@175
    goto :goto_10a

    #@176
    .line 990
    .end local v3           #dest:Landroid/net/LinkAddress;
    .end local v6           #gateway:Ljava/net/InetAddress;
    :cond_176
    const-string v19, "dns"

    #@178
    move-object/from16 v0, v19

    #@17a
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17d
    move-result v19

    #@17e
    if-eqz v19, :cond_197

    #@180
    .line 991
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@183
    move-result-object v19

    #@184
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@187
    move-result-object v19

    #@188
    move-object/from16 v0, v19

    #@18a
    invoke-virtual {v13, v0}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V
    :try_end_18d
    .catchall {:try_start_140 .. :try_end_18d} :catchall_18f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_140 .. :try_end_18d} :catch_c4
    .catch Ljava/io/EOFException; {:try_start_140 .. :try_end_18d} :catch_e2
    .catch Ljava/io/IOException; {:try_start_140 .. :try_end_18d} :catch_118

    #@18d
    goto/16 :goto_7a

    #@18f
    .line 1058
    .end local v5           #exclusionList:Ljava/lang/String;
    .end local v7           #id:I
    .end local v10           #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .end local v11           #key:Ljava/lang/String;
    .end local v13           #linkProperties:Landroid/net/LinkProperties;
    .end local v14           #proxyHost:Ljava/lang/String;
    .end local v15           #proxyPort:I
    .end local v17           #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .end local v18           #version:I
    :catchall_18f
    move-exception v19

    #@190
    move-object v8, v9

    #@191
    .end local v9           #in:Ljava/io/DataInputStream;
    .restart local v8       #in:Ljava/io/DataInputStream;
    :goto_191
    if-eqz v8, :cond_196

    #@193
    .line 1060
    :try_start_193
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V
    :try_end_196
    .catch Ljava/lang/Exception; {:try_start_193 .. :try_end_196} :catch_24d

    #@196
    .line 1061
    :cond_196
    :goto_196
    throw v19

    #@197
    .line 993
    .end local v8           #in:Ljava/io/DataInputStream;
    .restart local v5       #exclusionList:Ljava/lang/String;
    .restart local v7       #id:I
    .restart local v9       #in:Ljava/io/DataInputStream;
    .restart local v10       #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .restart local v11       #key:Ljava/lang/String;
    .restart local v13       #linkProperties:Landroid/net/LinkProperties;
    .restart local v14       #proxyHost:Ljava/lang/String;
    .restart local v15       #proxyPort:I
    .restart local v17       #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .restart local v18       #version:I
    :cond_197
    :try_start_197
    const-string/jumbo v19, "proxySettings"

    #@19a
    move-object/from16 v0, v19

    #@19c
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19f
    move-result v19

    #@1a0
    if-eqz v19, :cond_1ac

    #@1a2
    .line 994
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@1a5
    move-result-object v19

    #@1a6
    invoke-static/range {v19 .. v19}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1a9
    move-result-object v17

    #@1aa
    goto/16 :goto_7a

    #@1ac
    .line 995
    :cond_1ac
    const-string/jumbo v19, "proxyHost"

    #@1af
    move-object/from16 v0, v19

    #@1b1
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b4
    move-result v19

    #@1b5
    if-eqz v19, :cond_1bd

    #@1b7
    .line 996
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@1ba
    move-result-object v14

    #@1bb
    goto/16 :goto_7a

    #@1bd
    .line 997
    :cond_1bd
    const-string/jumbo v19, "proxyPort"

    #@1c0
    move-object/from16 v0, v19

    #@1c2
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c5
    move-result v19

    #@1c6
    if-eqz v19, :cond_1ce

    #@1c8
    .line 998
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    #@1cb
    move-result v15

    #@1cc
    goto/16 :goto_7a

    #@1ce
    .line 999
    :cond_1ce
    const-string v19, "exclusionList"

    #@1d0
    move-object/from16 v0, v19

    #@1d2
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d5
    move-result v19

    #@1d6
    if-eqz v19, :cond_38

    #@1d8
    .line 1000
    invoke-virtual {v9}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@1db
    move-result-object v5

    #@1dc
    goto/16 :goto_7a

    #@1de
    .line 1004
    :cond_1de
    new-instance v19, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v20, "Ignore unknown key "

    #@1e5
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v19

    #@1e9
    move-object/from16 v0, v19

    #@1eb
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v19

    #@1ef
    const-string/jumbo v20, "while reading"

    #@1f2
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v19

    #@1f6
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v19

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    move-object/from16 v1, v19

    #@1fe
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V
    :try_end_201
    .catchall {:try_start_197 .. :try_end_201} :catchall_18f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_197 .. :try_end_201} :catch_c4
    .catch Ljava/io/EOFException; {:try_start_197 .. :try_end_201} :catch_e2
    .catch Ljava/io/IOException; {:try_start_197 .. :try_end_201} :catch_118

    #@201
    goto/16 :goto_7a

    #@203
    .line 1018
    .restart local v2       #config:Landroid/net/wifi/WifiConfiguration;
    :cond_203
    :try_start_203
    iput-object v13, v2, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@205
    .line 1019
    sget-object v19, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$wifi$WifiConfiguration$IpAssignment:[I

    #@207
    invoke-virtual {v10}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->ordinal()I

    #@20a
    move-result v20

    #@20b
    aget v19, v19, v20

    #@20d
    packed-switch v19, :pswitch_data_25a

    #@210
    .line 1028
    const-string v19, "Ignore invalid ip assignment while reading"

    #@212
    move-object/from16 v0, p0

    #@214
    move-object/from16 v1, v19

    #@216
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@219
    .line 1032
    :goto_219
    :pswitch_219
    sget-object v19, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$wifi$WifiConfiguration$ProxySettings:[I

    #@21b
    invoke-virtual/range {v17 .. v17}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->ordinal()I

    #@21e
    move-result v20

    #@21f
    aget v19, v19, v20

    #@221
    packed-switch v19, :pswitch_data_264

    #@224
    .line 1046
    const-string v19, "Ignore invalid proxy settings while reading"

    #@226
    move-object/from16 v0, p0

    #@228
    move-object/from16 v1, v19

    #@22a
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@22d
    goto/16 :goto_6d

    #@22f
    .line 1022
    :pswitch_22f
    iput-object v10, v2, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@231
    goto :goto_219

    #@232
    .line 1034
    :pswitch_232
    move-object/from16 v0, v17

    #@234
    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@236
    .line 1035
    new-instance v16, Landroid/net/ProxyProperties;

    #@238
    move-object/from16 v0, v16

    #@23a
    invoke-direct {v0, v14, v15, v5}, Landroid/net/ProxyProperties;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@23d
    .line 1037
    .local v16, proxyProperties:Landroid/net/ProxyProperties;
    move-object/from16 v0, v16

    #@23f
    invoke-virtual {v13, v0}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@242
    goto/16 :goto_6d

    #@244
    .line 1040
    .end local v16           #proxyProperties:Landroid/net/ProxyProperties;
    :pswitch_244
    move-object/from16 v0, v17

    #@246
    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    :try_end_248
    .catchall {:try_start_203 .. :try_end_248} :catchall_18f
    .catch Ljava/io/EOFException; {:try_start_203 .. :try_end_248} :catch_e2
    .catch Ljava/io/IOException; {:try_start_203 .. :try_end_248} :catch_118

    #@248
    goto/16 :goto_6d

    #@24a
    .line 1061
    .end local v2           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v5           #exclusionList:Ljava/lang/String;
    .end local v7           #id:I
    .end local v10           #ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;
    .end local v11           #key:Ljava/lang/String;
    .end local v13           #linkProperties:Landroid/net/LinkProperties;
    .end local v14           #proxyHost:Ljava/lang/String;
    .end local v15           #proxyPort:I
    .end local v17           #proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;
    :catch_24a
    move-exception v19

    #@24b
    goto/16 :goto_36

    #@24d
    .end local v9           #in:Ljava/io/DataInputStream;
    .end local v18           #version:I
    .restart local v8       #in:Ljava/io/DataInputStream;
    :catch_24d
    move-exception v20

    #@24e
    goto/16 :goto_196

    #@250
    .line 1058
    :catchall_250
    move-exception v19

    #@251
    goto/16 :goto_191

    #@253
    .line 1055
    :catch_253
    move-exception v4

    #@254
    goto/16 :goto_11a

    #@256
    .line 1054
    :catch_256
    move-exception v19

    #@257
    goto/16 :goto_e4

    #@259
    .line 1019
    nop

    #@25a
    :pswitch_data_25a
    .packed-switch 0x1
        :pswitch_22f
        :pswitch_22f
        :pswitch_219
    .end packed-switch

    #@264
    .line 1032
    :pswitch_data_264
    .packed-switch 0x1
        :pswitch_232
        :pswitch_244
        :pswitch_6d
    .end packed-switch
.end method

.method private readNetworkVariables(Landroid/net/wifi/WifiConfiguration;)V
    .registers 15
    .parameter "config"

    #@0
    .prologue
    .line 1490
    iget v6, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@2
    .line 1491
    .local v6, netId:I
    if-gez v6, :cond_5

    #@4
    .line 1661
    :goto_4
    return-void

    #@5
    .line 1505
    :cond_5
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useKoreanSsid()Z

    #@8
    move-result v10

    #@9
    if-eqz v10, :cond_c0

    #@b
    sget-object v10, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@d
    if-eqz v10, :cond_c0

    #@f
    .line 1506
    sget-object v10, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@11
    const-string/jumbo v11, "ssid"

    #@14
    invoke-interface {v10, v6, v11}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getNetworkVariableCommand(ILjava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v9

    #@18
    .line 1513
    .local v9, value:Ljava/lang/String;
    :goto_18
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b
    move-result v10

    #@1c
    if-nez v10, :cond_cf

    #@1e
    .line 1514
    const/4 v10, 0x0

    #@1f
    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    #@22
    move-result v10

    #@23
    const/16 v11, 0x22

    #@25
    if-eq v10, v11, :cond_cb

    #@27
    .line 1515
    new-instance v10, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v11, "\""

    #@2e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v10

    #@32
    invoke-static {v9}, Landroid/net/wifi/WifiSsid;->createFromHex(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;

    #@35
    move-result-object v11

    #@36
    invoke-virtual {v11}, Landroid/net/wifi/WifiSsid;->toString()Ljava/lang/String;

    #@39
    move-result-object v11

    #@3a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v10

    #@3e
    const-string v11, "\""

    #@40
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v10

    #@44
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v10

    #@48
    iput-object v10, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@4a
    .line 1525
    :goto_4a
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@4c
    const-string v11, "bssid"

    #@4e
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v9

    #@52
    .line 1526
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@55
    move-result v10

    #@56
    if-nez v10, :cond_d4

    #@58
    .line 1527
    iput-object v9, p1, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@5a
    .line 1532
    :goto_5a
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@5c
    const-string/jumbo v11, "priority"

    #@5f
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v9

    #@63
    .line 1533
    const/4 v10, -0x1

    #@64
    iput v10, p1, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@66
    .line 1534
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@69
    move-result v10

    #@6a
    if-nez v10, :cond_72

    #@6c
    .line 1536
    :try_start_6c
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6f
    move-result v10

    #@70
    iput v10, p1, Landroid/net/wifi/WifiConfiguration;->priority:I
    :try_end_72
    .catch Ljava/lang/NumberFormatException; {:try_start_6c .. :try_end_72} :catch_227

    #@72
    .line 1541
    :cond_72
    :goto_72
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@74
    const-string/jumbo v11, "scan_ssid"

    #@77
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@7a
    move-result-object v9

    #@7b
    .line 1542
    const/4 v10, 0x0

    #@7c
    iput-boolean v10, p1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@7e
    .line 1543
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@81
    move-result v10

    #@82
    if-nez v10, :cond_8d

    #@84
    .line 1545
    :try_start_84
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@87
    move-result v10

    #@88
    if-eqz v10, :cond_d8

    #@8a
    const/4 v10, 0x1

    #@8b
    :goto_8b
    iput-boolean v10, p1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z
    :try_end_8d
    .catch Ljava/lang/NumberFormatException; {:try_start_84 .. :try_end_8d} :catch_224

    #@8d
    .line 1550
    :cond_8d
    :goto_8d
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@8f
    const-string/jumbo v11, "wep_tx_keyidx"

    #@92
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@95
    move-result-object v9

    #@96
    .line 1551
    const/4 v10, -0x1

    #@97
    iput v10, p1, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@99
    .line 1552
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9c
    move-result v10

    #@9d
    if-nez v10, :cond_a5

    #@9f
    .line 1554
    :try_start_9f
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a2
    move-result v10

    #@a3
    iput v10, p1, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I
    :try_end_a5
    .catch Ljava/lang/NumberFormatException; {:try_start_9f .. :try_end_a5} :catch_221

    #@a5
    .line 1559
    :cond_a5
    :goto_a5
    const/4 v2, 0x0

    #@a6
    .local v2, i:I
    :goto_a6
    const/4 v10, 0x4

    #@a7
    if-ge v2, v10, :cond_e0

    #@a9
    .line 1560
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@ab
    sget-object v11, Landroid/net/wifi/WifiConfiguration;->wepKeyVarNames:[Ljava/lang/String;

    #@ad
    aget-object v11, v11, v2

    #@af
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@b2
    move-result-object v9

    #@b3
    .line 1562
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b6
    move-result v10

    #@b7
    if-nez v10, :cond_da

    #@b9
    .line 1563
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@bb
    aput-object v9, v10, v2

    #@bd
    .line 1559
    :goto_bd
    add-int/lit8 v2, v2, 0x1

    #@bf
    goto :goto_a6

    #@c0
    .line 1511
    .end local v2           #i:I
    .end local v9           #value:Ljava/lang/String;
    :cond_c0
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@c2
    const-string/jumbo v11, "ssid"

    #@c5
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@c8
    move-result-object v9

    #@c9
    .restart local v9       #value:Ljava/lang/String;
    goto/16 :goto_18

    #@cb
    .line 1519
    :cond_cb
    iput-object v9, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@cd
    goto/16 :goto_4a

    #@cf
    .line 1522
    :cond_cf
    const/4 v10, 0x0

    #@d0
    iput-object v10, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@d2
    goto/16 :goto_4a

    #@d4
    .line 1529
    :cond_d4
    const/4 v10, 0x0

    #@d5
    iput-object v10, p1, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@d7
    goto :goto_5a

    #@d8
    .line 1545
    :cond_d8
    const/4 v10, 0x0

    #@d9
    goto :goto_8b

    #@da
    .line 1565
    .restart local v2       #i:I
    :cond_da
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@dc
    const/4 v11, 0x0

    #@dd
    aput-object v11, v10, v2

    #@df
    goto :goto_bd

    #@e0
    .line 1569
    :cond_e0
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@e2
    const-string/jumbo v11, "psk"

    #@e5
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@e8
    move-result-object v9

    #@e9
    .line 1570
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@ec
    move-result v10

    #@ed
    if-nez v10, :cond_11f

    #@ef
    .line 1571
    iput-object v9, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@f1
    .line 1576
    :goto_f1
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@f3
    iget v11, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@f5
    const-string/jumbo v12, "proto"

    #@f8
    invoke-virtual {v10, v11, v12}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@fb
    move-result-object v9

    #@fc
    .line 1578
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@ff
    move-result v10

    #@100
    if-nez v10, :cond_123

    #@102
    .line 1579
    const-string v10, " "

    #@104
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@107
    move-result-object v8

    #@108
    .line 1580
    .local v8, vals:[Ljava/lang/String;
    move-object v0, v8

    #@109
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@10a
    .local v5, len$:I
    const/4 v3, 0x0

    #@10b
    .local v3, i$:I
    :goto_10b
    if-ge v3, v5, :cond_123

    #@10d
    aget-object v7, v0, v3

    #@10f
    .line 1581
    .local v7, val:Ljava/lang/String;
    sget-object v10, Landroid/net/wifi/WifiConfiguration$Protocol;->strings:[Ljava/lang/String;

    #@111
    invoke-direct {p0, v7, v10}, Landroid/net/wifi/WifiConfigStore;->lookupString(Ljava/lang/String;[Ljava/lang/String;)I

    #@114
    move-result v4

    #@115
    .line 1583
    .local v4, index:I
    if-ltz v4, :cond_11c

    #@117
    .line 1584
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@119
    invoke-virtual {v10, v4}, Ljava/util/BitSet;->set(I)V

    #@11c
    .line 1580
    :cond_11c
    add-int/lit8 v3, v3, 0x1

    #@11e
    goto :goto_10b

    #@11f
    .line 1573
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #index:I
    .end local v5           #len$:I
    .end local v7           #val:Ljava/lang/String;
    .end local v8           #vals:[Ljava/lang/String;
    :cond_11f
    const/4 v10, 0x0

    #@120
    iput-object v10, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@122
    goto :goto_f1

    #@123
    .line 1589
    :cond_123
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@125
    iget v11, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@127
    const-string/jumbo v12, "key_mgmt"

    #@12a
    invoke-virtual {v10, v11, v12}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@12d
    move-result-object v9

    #@12e
    .line 1591
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@131
    move-result v10

    #@132
    if-nez v10, :cond_151

    #@134
    .line 1592
    const-string v10, " "

    #@136
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@139
    move-result-object v8

    #@13a
    .line 1593
    .restart local v8       #vals:[Ljava/lang/String;
    move-object v0, v8

    #@13b
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, v0

    #@13c
    .restart local v5       #len$:I
    const/4 v3, 0x0

    #@13d
    .restart local v3       #i$:I
    :goto_13d
    if-ge v3, v5, :cond_151

    #@13f
    aget-object v7, v0, v3

    #@141
    .line 1594
    .restart local v7       #val:Ljava/lang/String;
    sget-object v10, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@143
    invoke-direct {p0, v7, v10}, Landroid/net/wifi/WifiConfigStore;->lookupString(Ljava/lang/String;[Ljava/lang/String;)I

    #@146
    move-result v4

    #@147
    .line 1596
    .restart local v4       #index:I
    if-ltz v4, :cond_14e

    #@149
    .line 1597
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@14b
    invoke-virtual {v10, v4}, Ljava/util/BitSet;->set(I)V

    #@14e
    .line 1593
    :cond_14e
    add-int/lit8 v3, v3, 0x1

    #@150
    goto :goto_13d

    #@151
    .line 1602
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #index:I
    .end local v5           #len$:I
    .end local v7           #val:Ljava/lang/String;
    .end local v8           #vals:[Ljava/lang/String;
    :cond_151
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@153
    iget v11, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@155
    const-string v12, "auth_alg"

    #@157
    invoke-virtual {v10, v11, v12}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@15a
    move-result-object v9

    #@15b
    .line 1604
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@15e
    move-result v10

    #@15f
    if-nez v10, :cond_17e

    #@161
    .line 1605
    const-string v10, " "

    #@163
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@166
    move-result-object v8

    #@167
    .line 1606
    .restart local v8       #vals:[Ljava/lang/String;
    move-object v0, v8

    #@168
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, v0

    #@169
    .restart local v5       #len$:I
    const/4 v3, 0x0

    #@16a
    .restart local v3       #i$:I
    :goto_16a
    if-ge v3, v5, :cond_17e

    #@16c
    aget-object v7, v0, v3

    #@16e
    .line 1607
    .restart local v7       #val:Ljava/lang/String;
    sget-object v10, Landroid/net/wifi/WifiConfiguration$AuthAlgorithm;->strings:[Ljava/lang/String;

    #@170
    invoke-direct {p0, v7, v10}, Landroid/net/wifi/WifiConfigStore;->lookupString(Ljava/lang/String;[Ljava/lang/String;)I

    #@173
    move-result v4

    #@174
    .line 1609
    .restart local v4       #index:I
    if-ltz v4, :cond_17b

    #@176
    .line 1610
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@178
    invoke-virtual {v10, v4}, Ljava/util/BitSet;->set(I)V

    #@17b
    .line 1606
    :cond_17b
    add-int/lit8 v3, v3, 0x1

    #@17d
    goto :goto_16a

    #@17e
    .line 1615
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #index:I
    .end local v5           #len$:I
    .end local v7           #val:Ljava/lang/String;
    .end local v8           #vals:[Ljava/lang/String;
    :cond_17e
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@180
    iget v11, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@182
    const-string/jumbo v12, "pairwise"

    #@185
    invoke-virtual {v10, v11, v12}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@188
    move-result-object v9

    #@189
    .line 1617
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@18c
    move-result v10

    #@18d
    if-nez v10, :cond_1ac

    #@18f
    .line 1618
    const-string v10, " "

    #@191
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@194
    move-result-object v8

    #@195
    .line 1619
    .restart local v8       #vals:[Ljava/lang/String;
    move-object v0, v8

    #@196
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, v0

    #@197
    .restart local v5       #len$:I
    const/4 v3, 0x0

    #@198
    .restart local v3       #i$:I
    :goto_198
    if-ge v3, v5, :cond_1ac

    #@19a
    aget-object v7, v0, v3

    #@19c
    .line 1620
    .restart local v7       #val:Ljava/lang/String;
    sget-object v10, Landroid/net/wifi/WifiConfiguration$PairwiseCipher;->strings:[Ljava/lang/String;

    #@19e
    invoke-direct {p0, v7, v10}, Landroid/net/wifi/WifiConfigStore;->lookupString(Ljava/lang/String;[Ljava/lang/String;)I

    #@1a1
    move-result v4

    #@1a2
    .line 1622
    .restart local v4       #index:I
    if-ltz v4, :cond_1a9

    #@1a4
    .line 1623
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@1a6
    invoke-virtual {v10, v4}, Ljava/util/BitSet;->set(I)V

    #@1a9
    .line 1619
    :cond_1a9
    add-int/lit8 v3, v3, 0x1

    #@1ab
    goto :goto_198

    #@1ac
    .line 1628
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #index:I
    .end local v5           #len$:I
    .end local v7           #val:Ljava/lang/String;
    .end local v8           #vals:[Ljava/lang/String;
    :cond_1ac
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@1ae
    iget v11, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@1b0
    const-string v12, "group"

    #@1b2
    invoke-virtual {v10, v11, v12}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@1b5
    move-result-object v9

    #@1b6
    .line 1630
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b9
    move-result v10

    #@1ba
    if-nez v10, :cond_1d9

    #@1bc
    .line 1631
    const-string v10, " "

    #@1be
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1c1
    move-result-object v8

    #@1c2
    .line 1632
    .restart local v8       #vals:[Ljava/lang/String;
    move-object v0, v8

    #@1c3
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, v0

    #@1c4
    .restart local v5       #len$:I
    const/4 v3, 0x0

    #@1c5
    .restart local v3       #i$:I
    :goto_1c5
    if-ge v3, v5, :cond_1d9

    #@1c7
    aget-object v7, v0, v3

    #@1c9
    .line 1633
    .restart local v7       #val:Ljava/lang/String;
    sget-object v10, Landroid/net/wifi/WifiConfiguration$GroupCipher;->strings:[Ljava/lang/String;

    #@1cb
    invoke-direct {p0, v7, v10}, Landroid/net/wifi/WifiConfigStore;->lookupString(Ljava/lang/String;[Ljava/lang/String;)I

    #@1ce
    move-result v4

    #@1cf
    .line 1635
    .restart local v4       #index:I
    if-ltz v4, :cond_1d6

    #@1d1
    .line 1636
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@1d3
    invoke-virtual {v10, v4}, Ljava/util/BitSet;->set(I)V

    #@1d6
    .line 1632
    :cond_1d6
    add-int/lit8 v3, v3, 0x1

    #@1d8
    goto :goto_1c5

    #@1d9
    .line 1642
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #index:I
    .end local v5           #len$:I
    .end local v7           #val:Ljava/lang/String;
    .end local v8           #vals:[Ljava/lang/String;
    :cond_1d9
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@1db
    .local v0, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v5, v0

    #@1dc
    .restart local v5       #len$:I
    const/4 v3, 0x0

    #@1dd
    .restart local v3       #i$:I
    :goto_1dd
    if-ge v3, v5, :cond_21c

    #@1df
    aget-object v1, v0, v3

    #@1e1
    .line 1643
    .local v1, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    iget-object v10, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@1e3
    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->varName()Ljava/lang/String;

    #@1e6
    move-result-object v11

    #@1e7
    invoke-virtual {v10, v6, v11}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@1ea
    move-result-object v9

    #@1eb
    .line 1645
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1ee
    move-result v10

    #@1ef
    if-nez v10, :cond_203

    #@1f1
    .line 1646
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@1f3
    if-eq v1, v10, :cond_1fd

    #@1f5
    iget-object v10, p1, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@1f7
    if-eq v1, v10, :cond_1fd

    #@1f9
    .line 1647
    invoke-direct {p0, v9}, Landroid/net/wifi/WifiConfigStore;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    #@1fc
    move-result-object v9

    #@1fd
    .line 1649
    :cond_1fd
    invoke-virtual {v1, v9}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@200
    .line 1642
    :cond_200
    :goto_200
    add-int/lit8 v3, v3, 0x1

    #@202
    goto :goto_1dd

    #@203
    .line 1653
    :cond_203
    sget-boolean v10, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@205
    if-eqz v10, :cond_200

    #@207
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@20a
    move-result-object v10

    #@20b
    const-string v11, "KT"

    #@20d
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@210
    move-result v10

    #@211
    if-eqz v10, :cond_200

    #@213
    sget-boolean v10, Landroid/net/wifi/WifiConfigStore;->mLgeKtCm:Z

    #@215
    if-nez v10, :cond_200

    #@217
    .line 1655
    const/4 v10, 0x0

    #@218
    invoke-virtual {v1, v10}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@21b
    goto :goto_200

    #@21c
    .line 1660
    .end local v1           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    :cond_21c
    invoke-direct {p0, p1, v6}, Landroid/net/wifi/WifiConfigStore;->migrateOldEapTlsIfNecessary(Landroid/net/wifi/WifiConfiguration;I)V

    #@21f
    goto/16 :goto_4

    #@221
    .line 1555
    .end local v0           #arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v2           #i:I
    .end local v3           #i$:I
    .end local v5           #len$:I
    :catch_221
    move-exception v10

    #@222
    goto/16 :goto_a5

    #@224
    .line 1546
    :catch_224
    move-exception v10

    #@225
    goto/16 :goto_8d

    #@227
    .line 1537
    :catch_227
    move-exception v10

    #@228
    goto/16 :goto_72
.end method

.method private removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "string"

    #@0
    .prologue
    .line 1718
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x2

    #@5
    if-gt v0, v1, :cond_a

    #@7
    const-string v0, ""

    #@9
    .line 1719
    :goto_9
    return-object v0

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v1

    #@f
    add-int/lit8 v1, v1, -0x1

    #@11
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    goto :goto_9
.end method

.method private sendConfiguredNetworksChangedBroadcast()V
    .registers 4

    #@0
    .prologue
    .line 707
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 708
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 709
    const-string/jumbo v1, "multipleChanges"

    #@f
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@13
    .line 710
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mContext:Landroid/content/Context;

    #@15
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@17
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1a
    .line 711
    return-void
.end method

.method private sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V
    .registers 6
    .parameter "network"
    .parameter "reason"

    #@0
    .prologue
    .line 695
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 696
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 697
    const-string/jumbo v1, "multipleChanges"

    #@f
    const/4 v2, 0x0

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@13
    .line 698
    const-string/jumbo v1, "wifiConfiguration"

    #@16
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@19
    .line 699
    const-string v1, "changeReason"

    #@1b
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1e
    .line 700
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mContext:Landroid/content/Context;

    #@20
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@22
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@25
    .line 701
    return-void
.end method

.method private writeIpAndProxyConfigurations()V
    .registers 5

    #@0
    .prologue
    .line 784
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 785
    .local v2, networks:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_24

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@1b
    .line 786
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    new-instance v3, Landroid/net/wifi/WifiConfiguration;

    #@1d
    invoke-direct {v3, v0}, Landroid/net/wifi/WifiConfiguration;-><init>(Landroid/net/wifi/WifiConfiguration;)V

    #@20
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    goto :goto_f

    #@24
    .line 789
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_24
    invoke-static {v2}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->write(Ljava/util/List;)V

    #@27
    .line 790
    return-void
.end method

.method private writeIpAndProxyConfigurationsOnChange(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;
    .registers 22
    .parameter "currentConfig"
    .parameter "newConfig"

    #@0
    .prologue
    .line 1372
    const/4 v8, 0x0

    #@1
    .line 1373
    .local v8, ipChanged:Z
    const/4 v15, 0x0

    #@2
    .line 1374
    .local v15, proxyChanged:Z
    new-instance v10, Landroid/net/LinkProperties;

    #@4
    invoke-direct {v10}, Landroid/net/LinkProperties;-><init>()V

    #@7
    .line 1376
    .local v10, linkProperties:Landroid/net/LinkProperties;
    sget-object v17, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$wifi$WifiConfiguration$IpAssignment:[I

    #@9
    move-object/from16 v0, p2

    #@b
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@d
    move-object/from16 v18, v0

    #@f
    invoke-virtual/range {v18 .. v18}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->ordinal()I

    #@12
    move-result v18

    #@13
    aget v17, v17, v18

    #@15
    packed-switch v17, :pswitch_data_20c

    #@18
    .line 1411
    const-string v17, "Ignore invalid ip assignment during write"

    #@1a
    move-object/from16 v0, p0

    #@1c
    move-object/from16 v1, v17

    #@1e
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@21
    .line 1415
    :cond_21
    :goto_21
    :pswitch_21
    sget-object v17, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$wifi$WifiConfiguration$ProxySettings:[I

    #@23
    move-object/from16 v0, p2

    #@25
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@27
    move-object/from16 v18, v0

    #@29
    invoke-virtual/range {v18 .. v18}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->ordinal()I

    #@2c
    move-result v18

    #@2d
    aget v17, v17, v18

    #@2f
    packed-switch v17, :pswitch_data_216

    #@32
    .line 1435
    const-string v17, "Ignore invalid proxy configuration during write"

    #@34
    move-object/from16 v0, p0

    #@36
    move-object/from16 v1, v17

    #@38
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@3b
    .line 1439
    :cond_3b
    :goto_3b
    :pswitch_3b
    if-nez v8, :cond_163

    #@3d
    .line 1440
    move-object/from16 v0, p0

    #@3f
    move-object/from16 v1, p1

    #@41
    invoke-direct {v0, v10, v1}, Landroid/net/wifi/WifiConfigStore;->addIpSettingsFromConfig(Landroid/net/LinkProperties;Landroid/net/wifi/WifiConfiguration;)V

    #@44
    .line 1449
    :goto_44
    if-nez v15, :cond_1a6

    #@46
    .line 1450
    move-object/from16 v0, p1

    #@48
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@4a
    move-object/from16 v17, v0

    #@4c
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@4f
    move-result-object v17

    #@50
    move-object/from16 v0, v17

    #@52
    invoke-virtual {v10, v0}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@55
    .line 1460
    :cond_55
    :goto_55
    if-nez v8, :cond_59

    #@57
    if-eqz v15, :cond_6b

    #@59
    .line 1461
    :cond_59
    move-object/from16 v0, p1

    #@5b
    iput-object v10, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@5d
    .line 1462
    invoke-direct/range {p0 .. p0}, Landroid/net/wifi/WifiConfigStore;->writeIpAndProxyConfigurations()V

    #@60
    .line 1463
    const/16 v17, 0x2

    #@62
    move-object/from16 v0, p0

    #@64
    move-object/from16 v1, p1

    #@66
    move/from16 v2, v17

    #@68
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@6b
    .line 1466
    :cond_6b
    new-instance v17, Landroid/net/wifi/NetworkUpdateResult;

    #@6d
    move-object/from16 v0, v17

    #@6f
    invoke-direct {v0, v8, v15}, Landroid/net/wifi/NetworkUpdateResult;-><init>(ZZ)V

    #@72
    return-object v17

    #@73
    .line 1378
    :pswitch_73
    move-object/from16 v0, p1

    #@75
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@77
    move-object/from16 v17, v0

    #@79
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@7c
    move-result-object v5

    #@7d
    .line 1380
    .local v5, currentLinkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    move-object/from16 v0, p2

    #@7f
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@81
    move-object/from16 v17, v0

    #@83
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@86
    move-result-object v13

    #@87
    .line 1382
    .local v13, newLinkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    move-object/from16 v0, p1

    #@89
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@8b
    move-object/from16 v17, v0

    #@8d
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@90
    move-result-object v3

    #@91
    .line 1383
    .local v3, currentDnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    move-object/from16 v0, p2

    #@93
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@95
    move-object/from16 v17, v0

    #@97
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@9a
    move-result-object v11

    #@9b
    .line 1384
    .local v11, newDnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    move-object/from16 v0, p1

    #@9d
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@9f
    move-object/from16 v17, v0

    #@a1
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@a4
    move-result-object v6

    #@a5
    .line 1385
    .local v6, currentRoutes:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    move-object/from16 v0, p2

    #@a7
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@a9
    move-object/from16 v17, v0

    #@ab
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@ae
    move-result-object v14

    #@af
    .line 1387
    .local v14, newRoutes:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    #@b2
    move-result v17

    #@b3
    invoke-interface {v13}, Ljava/util/Collection;->size()I

    #@b6
    move-result v18

    #@b7
    move/from16 v0, v17

    #@b9
    move/from16 v1, v18

    #@bb
    if-ne v0, v1, :cond_c3

    #@bd
    invoke-interface {v5, v13}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@c0
    move-result v17

    #@c1
    if-nez v17, :cond_10a

    #@c3
    :cond_c3
    const/4 v9, 0x1

    #@c4
    .line 1390
    .local v9, linkAddressesDiffer:Z
    :goto_c4
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    #@c7
    move-result v17

    #@c8
    invoke-interface {v11}, Ljava/util/Collection;->size()I

    #@cb
    move-result v18

    #@cc
    move/from16 v0, v17

    #@ce
    move/from16 v1, v18

    #@d0
    if-ne v0, v1, :cond_d8

    #@d2
    invoke-interface {v3, v11}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@d5
    move-result v17

    #@d6
    if-nez v17, :cond_10c

    #@d8
    :cond_d8
    const/4 v7, 0x1

    #@d9
    .line 1392
    .local v7, dnsesDiffer:Z
    :goto_d9
    invoke-interface {v6}, Ljava/util/Collection;->size()I

    #@dc
    move-result v17

    #@dd
    invoke-interface {v14}, Ljava/util/Collection;->size()I

    #@e0
    move-result v18

    #@e1
    move/from16 v0, v17

    #@e3
    move/from16 v1, v18

    #@e5
    if-ne v0, v1, :cond_ed

    #@e7
    invoke-interface {v6, v14}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@ea
    move-result v17

    #@eb
    if-nez v17, :cond_10e

    #@ed
    :cond_ed
    const/16 v16, 0x1

    #@ef
    .line 1395
    .local v16, routesDiffer:Z
    :goto_ef
    move-object/from16 v0, p1

    #@f1
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@f3
    move-object/from16 v17, v0

    #@f5
    move-object/from16 v0, p2

    #@f7
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@f9
    move-object/from16 v18, v0

    #@fb
    move-object/from16 v0, v17

    #@fd
    move-object/from16 v1, v18

    #@ff
    if-ne v0, v1, :cond_107

    #@101
    if-nez v9, :cond_107

    #@103
    if-nez v7, :cond_107

    #@105
    if-eqz v16, :cond_21

    #@107
    .line 1399
    :cond_107
    const/4 v8, 0x1

    #@108
    goto/16 :goto_21

    #@10a
    .line 1387
    .end local v7           #dnsesDiffer:Z
    .end local v9           #linkAddressesDiffer:Z
    .end local v16           #routesDiffer:Z
    :cond_10a
    const/4 v9, 0x0

    #@10b
    goto :goto_c4

    #@10c
    .line 1390
    .restart local v9       #linkAddressesDiffer:Z
    :cond_10c
    const/4 v7, 0x0

    #@10d
    goto :goto_d9

    #@10e
    .line 1392
    .restart local v7       #dnsesDiffer:Z
    :cond_10e
    const/16 v16, 0x0

    #@110
    goto :goto_ef

    #@111
    .line 1403
    .end local v3           #currentDnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v5           #currentLinkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    .end local v6           #currentRoutes:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    .end local v7           #dnsesDiffer:Z
    .end local v9           #linkAddressesDiffer:Z
    .end local v11           #newDnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v13           #newLinkAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    .end local v14           #newRoutes:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    :pswitch_111
    move-object/from16 v0, p1

    #@113
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@115
    move-object/from16 v17, v0

    #@117
    move-object/from16 v0, p2

    #@119
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@11b
    move-object/from16 v18, v0

    #@11d
    move-object/from16 v0, v17

    #@11f
    move-object/from16 v1, v18

    #@121
    if-eq v0, v1, :cond_21

    #@123
    .line 1404
    const/4 v8, 0x1

    #@124
    goto/16 :goto_21

    #@126
    .line 1417
    :pswitch_126
    move-object/from16 v0, p2

    #@128
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@12a
    move-object/from16 v17, v0

    #@12c
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@12f
    move-result-object v12

    #@130
    .line 1418
    .local v12, newHttpProxy:Landroid/net/ProxyProperties;
    move-object/from16 v0, p1

    #@132
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@134
    move-object/from16 v17, v0

    #@136
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@139
    move-result-object v4

    #@13a
    .line 1420
    .local v4, currentHttpProxy:Landroid/net/ProxyProperties;
    if-eqz v12, :cond_147

    #@13c
    .line 1421
    invoke-virtual {v12, v4}, Landroid/net/ProxyProperties;->equals(Ljava/lang/Object;)Z

    #@13f
    move-result v17

    #@140
    if-nez v17, :cond_145

    #@142
    const/4 v15, 0x1

    #@143
    :goto_143
    goto/16 :goto_3b

    #@145
    :cond_145
    const/4 v15, 0x0

    #@146
    goto :goto_143

    #@147
    .line 1423
    :cond_147
    if-eqz v4, :cond_14c

    #@149
    const/4 v15, 0x1

    #@14a
    .line 1425
    :goto_14a
    goto/16 :goto_3b

    #@14c
    .line 1423
    :cond_14c
    const/4 v15, 0x0

    #@14d
    goto :goto_14a

    #@14e
    .line 1427
    .end local v4           #currentHttpProxy:Landroid/net/ProxyProperties;
    .end local v12           #newHttpProxy:Landroid/net/ProxyProperties;
    :pswitch_14e
    move-object/from16 v0, p1

    #@150
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@152
    move-object/from16 v17, v0

    #@154
    move-object/from16 v0, p2

    #@156
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@158
    move-object/from16 v18, v0

    #@15a
    move-object/from16 v0, v17

    #@15c
    move-object/from16 v1, v18

    #@15e
    if-eq v0, v1, :cond_3b

    #@160
    .line 1428
    const/4 v15, 0x1

    #@161
    goto/16 :goto_3b

    #@163
    .line 1442
    :cond_163
    move-object/from16 v0, p2

    #@165
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@167
    move-object/from16 v17, v0

    #@169
    move-object/from16 v0, v17

    #@16b
    move-object/from16 v1, p1

    #@16d
    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@16f
    .line 1443
    move-object/from16 v0, p0

    #@171
    move-object/from16 v1, p2

    #@173
    invoke-direct {v0, v10, v1}, Landroid/net/wifi/WifiConfigStore;->addIpSettingsFromConfig(Landroid/net/LinkProperties;Landroid/net/wifi/WifiConfiguration;)V

    #@176
    .line 1444
    new-instance v17, Ljava/lang/StringBuilder;

    #@178
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@17b
    const-string v18, "IP config changed SSID = "

    #@17d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v17

    #@181
    move-object/from16 v0, p1

    #@183
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@185
    move-object/from16 v18, v0

    #@187
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v17

    #@18b
    const-string v18, " linkProperties: "

    #@18d
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v17

    #@191
    invoke-virtual {v10}, Landroid/net/LinkProperties;->toString()Ljava/lang/String;

    #@194
    move-result-object v18

    #@195
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v17

    #@199
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19c
    move-result-object v17

    #@19d
    move-object/from16 v0, p0

    #@19f
    move-object/from16 v1, v17

    #@1a1
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->log(Ljava/lang/String;)V

    #@1a4
    goto/16 :goto_44

    #@1a6
    .line 1452
    :cond_1a6
    move-object/from16 v0, p2

    #@1a8
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1aa
    move-object/from16 v17, v0

    #@1ac
    move-object/from16 v0, v17

    #@1ae
    move-object/from16 v1, p1

    #@1b0
    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1b2
    .line 1453
    move-object/from16 v0, p2

    #@1b4
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@1b6
    move-object/from16 v17, v0

    #@1b8
    invoke-virtual/range {v17 .. v17}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@1bb
    move-result-object v17

    #@1bc
    move-object/from16 v0, v17

    #@1be
    invoke-virtual {v10, v0}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@1c1
    .line 1454
    new-instance v17, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    const-string/jumbo v18, "proxy changed SSID = "

    #@1c9
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v17

    #@1cd
    move-object/from16 v0, p1

    #@1cf
    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@1d1
    move-object/from16 v18, v0

    #@1d3
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v17

    #@1d7
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1da
    move-result-object v17

    #@1db
    move-object/from16 v0, p0

    #@1dd
    move-object/from16 v1, v17

    #@1df
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->log(Ljava/lang/String;)V

    #@1e2
    .line 1455
    invoke-virtual {v10}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@1e5
    move-result-object v17

    #@1e6
    if-eqz v17, :cond_55

    #@1e8
    .line 1456
    new-instance v17, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v18, " proxyProperties: "

    #@1ef
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v17

    #@1f3
    invoke-virtual {v10}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@1f6
    move-result-object v18

    #@1f7
    invoke-virtual/range {v18 .. v18}, Landroid/net/ProxyProperties;->toString()Ljava/lang/String;

    #@1fa
    move-result-object v18

    #@1fb
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v17

    #@1ff
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@202
    move-result-object v17

    #@203
    move-object/from16 v0, p0

    #@205
    move-object/from16 v1, v17

    #@207
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiConfigStore;->log(Ljava/lang/String;)V

    #@20a
    goto/16 :goto_55

    #@20c
    .line 1376
    :pswitch_data_20c
    .packed-switch 0x1
        :pswitch_73
        :pswitch_111
        :pswitch_21
    .end packed-switch

    #@216
    .line 1415
    :pswitch_data_216
    .packed-switch 0x1
        :pswitch_126
        :pswitch_14e
        :pswitch_3b
    .end packed-switch
.end method


# virtual methods
.method addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 5
    .parameter "config"

    #@0
    .prologue
    .line 405
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetworkNative(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@3
    move-result-object v0

    #@4
    .line 406
    .local v0, result:Landroid/net/wifi/NetworkUpdateResult;
    invoke-virtual {v0}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@7
    move-result v1

    #@8
    const/4 v2, -0x1

    #@9
    if-eq v1, v2, :cond_23

    #@b
    .line 407
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@d
    invoke-virtual {v0}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@10
    move-result v2

    #@11
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    #@1b
    iget-boolean v2, v0, Landroid/net/wifi/NetworkUpdateResult;->isNewNetwork:Z

    #@1d
    if-eqz v2, :cond_28

    #@1f
    const/4 v2, 0x0

    #@20
    :goto_20
    invoke-direct {p0, v1, v2}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@23
    .line 411
    :cond_23
    invoke-virtual {v0}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@26
    move-result v1

    #@27
    return v1

    #@28
    .line 407
    :cond_28
    const/4 v2, 0x2

    #@29
    goto :goto_20
.end method

.method clearIpConfiguration(I)V
    .registers 6
    .parameter "netId"

    #@0
    .prologue
    .line 651
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@c
    .line 652
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_22

    #@e
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@10
    if-eqz v2, :cond_22

    #@12
    .line 654
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@14
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@17
    move-result-object v1

    #@18
    .line 655
    .local v1, proxy:Landroid/net/ProxyProperties;
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@1a
    invoke-virtual {v2}, Landroid/net/LinkProperties;->clear()V

    #@1d
    .line 656
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@1f
    invoke-virtual {v2, v1}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@22
    .line 658
    .end local v1           #proxy:Landroid/net/ProxyProperties;
    :cond_22
    return-void
.end method

.method disableNetwork(I)Z
    .registers 3
    .parameter "netId"

    #@0
    .prologue
    .line 492
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/net/wifi/WifiConfigStore;->disableNetwork(II)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method disableNetwork(II)Z
    .registers 9
    .parameter "netId"
    .parameter "reason"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 502
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@3
    invoke-virtual {v3, p1}, Landroid/net/wifi/WifiNative;->disableNetwork(I)Z

    #@6
    move-result v2

    #@7
    .line 503
    .local v2, ret:Z
    const/4 v1, 0x0

    #@8
    .line 504
    .local v1, network:Landroid/net/wifi/WifiConfiguration;
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@14
    .line 506
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_3b

    #@16
    iget v3, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@18
    if-eq v3, v5, :cond_3b

    #@1a
    .line 507
    iput v5, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@1c
    .line 508
    iput p2, v0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@1e
    .line 509
    move-object v1, v0

    #@1f
    .line 511
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@21
    if-eqz v3, :cond_3b

    #@23
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    const-string v4, "LGU"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_3b

    #@2f
    .line 512
    sget-object v3, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@31
    if-eqz v3, :cond_3b

    #@33
    if-eqz p2, :cond_3b

    #@35
    .line 513
    sget-object v3, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@37
    const/4 v4, 0x0

    #@38
    invoke-interface {v3, p1, v4}, Lcom/lge/wifi_iface/WifiServiceExtIface;->addWifiConnectionList(II)V

    #@3b
    .line 518
    :cond_3b
    if-eqz v1, :cond_41

    #@3d
    .line 519
    const/4 v3, 0x2

    #@3e
    invoke-direct {p0, v1, v3}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@41
    .line 522
    :cond_41
    return v2
.end method

.method dump()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 1785
    new-instance v3, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 1786
    .local v3, sb:Ljava/lang/StringBuffer;
    const-string/jumbo v4, "line.separator"

    #@8
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1787
    .local v0, LS:Ljava/lang/String;
    const-string/jumbo v4, "mLastPriority "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@12
    move-result-object v4

    #@13
    iget v5, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    .line 1788
    const-string v4, "Configured networks "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@25
    .line 1789
    invoke-virtual {p0}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@28
    move-result-object v4

    #@29
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v2

    #@2d
    .local v2, i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_41

    #@33
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v1

    #@37
    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    #@39
    .line 1790
    .local v1, conf:Landroid/net/wifi/WifiConfiguration;
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@40
    goto :goto_2d

    #@41
    .line 1792
    .end local v1           #conf:Landroid/net/wifi/WifiConfiguration;
    :cond_41
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    return-object v4
.end method

.method enableAllNetworks()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    .line 205
    const/4 v3, 0x0

    #@3
    .line 206
    .local v3, networkEnabledStateChanged:Z
    iget-object v4, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@5
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@8
    move-result-object v4

    #@9
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v2

    #@d
    .local v2, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_be

    #@13
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@19
    .line 208
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_d

    #@1b
    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@1d
    if-ne v4, v7, :cond_d

    #@1f
    .line 212
    :try_start_1f
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const-string v5, "ATT"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_94

    #@2b
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, "US"

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_94

    #@37
    .line 213
    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@39
    if-nez v4, :cond_45

    #@3b
    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@3d
    const-string v5, "Z736563757265"

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v4

    #@43
    if-nez v4, :cond_61

    #@45
    :cond_45
    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@47
    if-ne v4, v7, :cond_53

    #@49
    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@4b
    const-string v5, "Z736563757265"

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v4

    #@51
    if-nez v4, :cond_61

    #@53
    :cond_53
    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@55
    if-ne v4, v8, :cond_94

    #@57
    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@59
    const-string v5, "attwifi"

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_94

    #@61
    :cond_61
    iget-object v4, p0, Landroid/net/wifi/WifiConfigStore;->mContext:Landroid/content/Context;

    #@63
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@66
    move-result-object v4

    #@67
    const-string/jumbo v5, "wifi_auto_connect"

    #@6a
    const/4 v6, 0x1

    #@6b
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@6e
    move-result v4

    #@6f
    if-nez v4, :cond_94

    #@71
    .line 217
    const-string v4, "WifiConfigStore"

    #@73
    const-string v5, "enableAllNetworks  SKIP  ENABLE_NETWORK of ATT hotspot when autoconnection is disabled"

    #@75
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_78
    .catch Ljava/lang/NullPointerException; {:try_start_1f .. :try_end_78} :catch_79

    #@78
    goto :goto_d

    #@79
    .line 221
    :catch_79
    move-exception v1

    #@7a
    .line 222
    .local v1, e:Ljava/lang/NullPointerException;
    const-string v4, "WifiConfigStore"

    #@7c
    new-instance v5, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "enableAllNetworks: "

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v5

    #@8f
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    goto/16 :goto_d

    #@94
    .line 227
    .end local v1           #e:Ljava/lang/NullPointerException;
    :cond_94
    iget-object v4, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@96
    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@98
    const/4 v6, 0x0

    #@99
    invoke-virtual {v4, v5, v6}, Landroid/net/wifi/WifiNative;->enableNetwork(IZ)Z

    #@9c
    move-result v4

    #@9d
    if-eqz v4, :cond_a4

    #@9f
    .line 228
    const/4 v3, 0x1

    #@a0
    .line 229
    iput v8, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@a2
    goto/16 :goto_d

    #@a4
    .line 231
    :cond_a4
    new-instance v4, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v5, "Enable network failed on "

    #@ab
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v4

    #@b9
    invoke-direct {p0, v4}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@bc
    goto/16 :goto_d

    #@be
    .line 236
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_be
    if-eqz v3, :cond_c8

    #@c0
    .line 237
    iget-object v4, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@c2
    invoke-virtual {v4}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@c5
    .line 238
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast()V

    #@c8
    .line 240
    :cond_c8
    return-void
.end method

.method enableNetwork(IZ)Z
    .registers 9
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    .line 456
    invoke-virtual {p0, p1, p2}, Landroid/net/wifi/WifiConfigStore;->enableNetworkWithoutBroadcast(IZ)Z

    #@3
    move-result v2

    #@4
    .line 457
    .local v2, ret:Z
    if-eqz p2, :cond_a

    #@6
    .line 458
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast()V

    #@9
    .line 470
    :cond_9
    :goto_9
    return v2

    #@a
    .line 460
    :cond_a
    const/4 v1, 0x0

    #@b
    .line 461
    .local v1, enabledNetwork:Landroid/net/wifi/WifiConfiguration;
    iget-object v4, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@d
    monitor-enter v4

    #@e
    .line 462
    :try_start_e
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    move-object v0, v3

    #@19
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@1b
    move-object v1, v0

    #@1c
    .line 463
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_e .. :try_end_1d} :catchall_24

    #@1d
    .line 465
    if-eqz v1, :cond_9

    #@1f
    .line 466
    const/4 v3, 0x2

    #@20
    invoke-direct {p0, v1, v3}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@23
    goto :goto_9

    #@24
    .line 463
    :catchall_24
    move-exception v3

    #@25
    :try_start_25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v3
.end method

.method enableNetworkWithoutBroadcast(IZ)Z
    .registers 7
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    .line 475
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    invoke-virtual {v2, p1, p2}, Landroid/net/wifi/WifiNative;->enableNetwork(IZ)Z

    #@5
    move-result v1

    #@6
    .line 477
    .local v1, ret:Z
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@12
    .line 478
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_17

    #@14
    const/4 v2, 0x2

    #@15
    iput v2, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@17
    .line 480
    :cond_17
    if-eqz p2, :cond_1c

    #@19
    .line 481
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiConfigStore;->markAllNetworksDisabledExcept(I)V

    #@1c
    .line 483
    :cond_1c
    return v1
.end method

.method forgetNetwork(I)Z
    .registers 9
    .parameter "netId"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 358
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@4
    if-eqz v5, :cond_23

    #@6
    .line 359
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiConfigStore;->checkRemovableNetwork(I)Z

    #@9
    move-result v5

    #@a
    if-eq v4, v5, :cond_23

    #@c
    .line 360
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "Can not remove network "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-direct {p0, v4}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@22
    .line 391
    :goto_22
    return v3

    #@23
    .line 365
    :cond_23
    iget-object v5, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@25
    invoke-virtual {v5, p1}, Landroid/net/wifi/WifiNative;->removeNetwork(I)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_a6

    #@2b
    .line 367
    iget-object v5, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@30
    move-result-object v5

    #@31
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@34
    move-result-object v1

    #@35
    .local v1, i$:Ljava/util/Iterator;
    :cond_35
    :goto_35
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_6e

    #@3b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@41
    .line 368
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_35

    #@43
    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@45
    if-ne v5, v4, :cond_35

    #@47
    .line 369
    iget-object v5, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@49
    iget v6, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@4b
    invoke-virtual {v5, v6, v3}, Landroid/net/wifi/WifiNative;->enableNetwork(IZ)Z

    #@4e
    move-result v5

    #@4f
    if-eqz v5, :cond_55

    #@51
    .line 370
    const/4 v5, 0x2

    #@52
    iput v5, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@54
    goto :goto_35

    #@55
    .line 372
    :cond_55
    new-instance v5, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v6, "Enable network failed on "

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    iget v6, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-direct {p0, v5}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@6d
    goto :goto_35

    #@6e
    .line 377
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_6e
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@70
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@73
    .line 378
    const/4 v2, 0x0

    #@74
    .line 379
    .local v2, target:Landroid/net/wifi/WifiConfiguration;
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@76
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7d
    move-result-object v0

    #@7e
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@80
    .line 380
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_9b

    #@82
    .line 381
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@84
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8b
    move-result-object v2

    #@8c
    .end local v2           #target:Landroid/net/wifi/WifiConfiguration;
    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    #@8e
    .line 382
    .restart local v2       #target:Landroid/net/wifi/WifiConfiguration;
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@90
    invoke-static {v0}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@93
    move-result v5

    #@94
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@97
    move-result-object v5

    #@98
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9b
    .line 384
    :cond_9b
    if-eqz v2, :cond_a3

    #@9d
    .line 385
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->writeIpAndProxyConfigurations()V

    #@a0
    .line 386
    invoke-direct {p0, v2, v4}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@a3
    :cond_a3
    move v3, v4

    #@a4
    .line 388
    goto/16 :goto_22

    #@a6
    .line 390
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #target:Landroid/net/wifi/WifiConfiguration;
    :cond_a6
    new-instance v4, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v5, "Failed to remove network "

    #@ad
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v4

    #@b1
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v4

    #@b9
    invoke-direct {p0, v4}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@bc
    goto/16 :goto_22
.end method

.method public getConfigFile()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1796
    sget-object v0, Landroid/net/wifi/WifiConfigStore;->ipConfigFile:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getConfiguredNetworks()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 193
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 194
    .local v2, networks:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_24

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@1b
    .line 195
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    new-instance v3, Landroid/net/wifi/WifiConfiguration;

    #@1d
    invoke-direct {v3, v0}, Landroid/net/wifi/WifiConfiguration;-><init>(Landroid/net/wifi/WifiConfiguration;)V

    #@20
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    goto :goto_f

    #@24
    .line 197
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_24
    return-object v2
.end method

.method getIpConfiguration(I)Landroid/net/DhcpInfoInternal;
    .registers 10
    .parameter "netId"

    #@0
    .prologue
    .line 608
    new-instance v0, Landroid/net/DhcpInfoInternal;

    #@2
    invoke-direct {v0}, Landroid/net/DhcpInfoInternal;-><init>()V

    #@5
    .line 609
    .local v0, dhcpInfoInternal:Landroid/net/DhcpInfoInternal;
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiConfigStore;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@8
    move-result-object v5

    #@9
    .line 611
    .local v5, linkProperties:Landroid/net/LinkProperties;
    if-eqz v5, :cond_6d

    #@b
    .line 612
    invoke-virtual {v5}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@e
    move-result-object v7

    #@f
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v3

    #@13
    .line 613
    .local v3, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/net/LinkAddress;>;"
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v7

    #@17
    if-eqz v7, :cond_6d

    #@19
    .line 614
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v4

    #@1d
    check-cast v4, Landroid/net/LinkAddress;

    #@1f
    .line 615
    .local v4, linkAddress:Landroid/net/LinkAddress;
    invoke-virtual {v4}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    iput-object v7, v0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@29
    .line 616
    invoke-virtual {v5}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@2c
    move-result-object v7

    #@2d
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@30
    move-result-object v2

    #@31
    .local v2, i$:Ljava/util/Iterator;
    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@34
    move-result v7

    #@35
    if-eqz v7, :cond_41

    #@37
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3a
    move-result-object v6

    #@3b
    check-cast v6, Landroid/net/RouteInfo;

    #@3d
    .line 617
    .local v6, route:Landroid/net/RouteInfo;
    invoke-virtual {v0, v6}, Landroid/net/DhcpInfoInternal;->addRoute(Landroid/net/RouteInfo;)V

    #@40
    goto :goto_31

    #@41
    .line 619
    .end local v6           #route:Landroid/net/RouteInfo;
    :cond_41
    invoke-virtual {v4}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@44
    move-result v7

    #@45
    iput v7, v0, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@47
    .line 620
    invoke-virtual {v5}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@4a
    move-result-object v7

    #@4b
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4e
    move-result-object v1

    #@4f
    .line 621
    .local v1, dnsIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/net/InetAddress;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v7

    #@53
    check-cast v7, Ljava/net/InetAddress;

    #@55
    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    iput-object v7, v0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@5b
    .line 622
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@5e
    move-result v7

    #@5f
    if-eqz v7, :cond_6d

    #@61
    .line 623
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@64
    move-result-object v7

    #@65
    check-cast v7, Ljava/net/InetAddress;

    #@67
    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@6a
    move-result-object v7

    #@6b
    iput-object v7, v0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@6d
    .line 627
    .end local v1           #dnsIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/net/InetAddress;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/net/LinkAddress;>;"
    .end local v4           #linkAddress:Landroid/net/LinkAddress;
    :cond_6d
    return-object v0
.end method

.method getLinkProperties(I)Landroid/net/LinkProperties;
    .registers 5
    .parameter "netId"

    #@0
    .prologue
    .line 594
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@c
    .line 595
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_16

    #@e
    new-instance v1, Landroid/net/LinkProperties;

    #@10
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@12
    invoke-direct {v1, v2}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@15
    .line 596
    :goto_15
    return-object v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method getProxyProperties(I)Landroid/net/ProxyProperties;
    .registers 5
    .parameter "netId"

    #@0
    .prologue
    .line 667
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiConfigStore;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@3
    move-result-object v0

    #@4
    .line 668
    .local v0, linkProperties:Landroid/net/LinkProperties;
    if-eqz v0, :cond_10

    #@6
    .line 669
    new-instance v1, Landroid/net/ProxyProperties;

    #@8
    invoke-virtual {v0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@b
    move-result-object v2

    #@c
    invoke-direct {v1, v2}, Landroid/net/ProxyProperties;-><init>(Landroid/net/ProxyProperties;)V

    #@f
    .line 671
    :goto_f
    return-object v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method initialize()V
    .registers 2

    #@0
    .prologue
    .line 181
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 182
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/net/wifi/WifiConfigStore;->sWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a
    .line 184
    :cond_a
    invoke-virtual {p0}, Landroid/net/wifi/WifiConfigStore;->loadConfiguredNetworks()V

    #@d
    .line 185
    invoke-virtual {p0}, Landroid/net/wifi/WifiConfigStore;->enableAllNetworks()V

    #@10
    .line 186
    return-void
.end method

.method isUsingStaticIp(I)Z
    .registers 5
    .parameter "netId"

    #@0
    .prologue
    .line 680
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@c
    .line 681
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_16

    #@e
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@10
    sget-object v2, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@12
    if-ne v1, v2, :cond_16

    #@14
    .line 682
    const/4 v1, 0x1

    #@15
    .line 684
    :goto_15
    return v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method loadConfiguredNetworks()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x2

    #@1
    const/4 v12, -0x1

    #@2
    const/4 v11, 0x3

    #@3
    const/4 v10, 0x0

    #@4
    .line 714
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@6
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->listNetworks()Ljava/lang/String;

    #@9
    move-result-object v4

    #@a
    .line 715
    .local v4, listStr:Ljava/lang/String;
    iput v10, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@c
    .line 717
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@e
    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    #@11
    .line 718
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@13
    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    #@16
    .line 720
    if-nez v4, :cond_19

    #@18
    .line 763
    :goto_18
    return-void

    #@19
    .line 723
    :cond_19
    const-string v7, "\n"

    #@1b
    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    .line 725
    .local v3, lines:[Ljava/lang/String;
    const/4 v2, 0x1

    #@20
    .local v2, i:I
    :goto_20
    array-length v7, v3

    #@21
    if-ge v2, v7, :cond_a9

    #@23
    .line 726
    aget-object v7, v3, v2

    #@25
    const-string v8, "\t"

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    .line 728
    .local v5, result:[Ljava/lang/String;
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@2d
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@30
    .line 730
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    const/4 v7, 0x0

    #@31
    :try_start_31
    aget-object v7, v5, v7

    #@33
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@36
    move-result v7

    #@37
    iput v7, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I
    :try_end_39
    .catch Ljava/lang/NumberFormatException; {:try_start_31 .. :try_end_39} :catch_93

    #@39
    .line 734
    array-length v7, v5

    #@3a
    if-le v7, v11, :cond_a6

    #@3c
    .line 735
    aget-object v7, v5, v11

    #@3e
    const-string v8, "[CURRENT]"

    #@40
    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@43
    move-result v7

    #@44
    if-eq v7, v12, :cond_95

    #@46
    .line 736
    iput v10, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@48
    .line 744
    :goto_48
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiConfigStore;->readNetworkVariables(Landroid/net/wifi/WifiConfiguration;)V

    #@4b
    .line 745
    iget v7, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@4d
    iget v8, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@4f
    if-le v7, v8, :cond_55

    #@51
    .line 746
    iget v7, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@53
    iput v7, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@55
    .line 750
    :cond_55
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@57
    if-eqz v7, :cond_72

    #@59
    .line 751
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@5b
    invoke-static {v0}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@5e
    move-result v8

    #@5f
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@66
    move-result-object v6

    #@67
    check-cast v6, Ljava/lang/Integer;

    #@69
    .line 752
    .local v6, savedNetId:Ljava/lang/Integer;
    if-eqz v6, :cond_72

    #@6b
    .line 753
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@6e
    move-result v7

    #@6f
    invoke-virtual {p0, v7}, Landroid/net/wifi/WifiConfigStore;->forgetNetwork(I)Z

    #@72
    .line 757
    .end local v6           #savedNetId:Ljava/lang/Integer;
    :cond_72
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@74
    iget v8, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@76
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v7, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7d
    .line 758
    iget-object v7, p0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@7f
    invoke-static {v0}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@82
    move-result v8

    #@83
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@86
    move-result-object v8

    #@87
    iget v9, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@89
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8c
    move-result-object v9

    #@8d
    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    .line 725
    :goto_90
    add-int/lit8 v2, v2, 0x1

    #@92
    goto :goto_20

    #@93
    .line 731
    :catch_93
    move-exception v1

    #@94
    .line 732
    .local v1, e:Ljava/lang/NumberFormatException;
    goto :goto_90

    #@95
    .line 737
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :cond_95
    aget-object v7, v5, v11

    #@97
    const-string v8, "[DISABLED]"

    #@99
    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@9c
    move-result v7

    #@9d
    if-eq v7, v12, :cond_a3

    #@9f
    .line 738
    const/4 v7, 0x1

    #@a0
    iput v7, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@a2
    goto :goto_48

    #@a3
    .line 740
    :cond_a3
    iput v13, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@a5
    goto :goto_48

    #@a6
    .line 742
    :cond_a6
    iput v13, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@a8
    goto :goto_48

    #@a9
    .line 761
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v5           #result:[Ljava/lang/String;
    :cond_a9
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->readIpAndProxyConfigurations()V

    #@ac
    .line 762
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast()V

    #@af
    goto/16 :goto_18
.end method

.method removeNetwork(I)Z
    .registers 7
    .parameter "netId"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 425
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3
    if-eqz v2, :cond_d

    #@5
    .line 426
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiConfigStore;->checkRemovableNetwork(I)Z

    #@8
    move-result v2

    #@9
    if-eq v4, v2, :cond_d

    #@b
    .line 427
    const/4 v1, 0x0

    #@c
    .line 443
    :cond_c
    :goto_c
    return v1

    #@d
    .line 431
    :cond_d
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@f
    invoke-virtual {v2, p1}, Landroid/net/wifi/WifiNative;->removeNetwork(I)Z

    #@12
    move-result v1

    #@13
    .line 432
    .local v1, ret:Z
    const/4 v0, 0x0

    #@14
    .line 433
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v1, :cond_3d

    #@16
    .line 434
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@22
    .line 435
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_3d

    #@24
    .line 436
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@26
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    move-result-object v0

    #@2e
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@30
    .line 437
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mNetworkIds:Ljava/util/HashMap;

    #@32
    invoke-static {v0}, Landroid/net/wifi/WifiConfigStore;->configKey(Landroid/net/wifi/WifiConfiguration;)I

    #@35
    move-result v3

    #@36
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 440
    :cond_3d
    if-eqz v0, :cond_c

    #@3f
    .line 441
    invoke-direct {p0, v0, v4}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@42
    goto :goto_c
.end method

.method saveConfig()Z
    .registers 2

    #@0
    .prologue
    .line 530
    iget-object v0, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    invoke-virtual {v0}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method saveNetwork(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;
    .registers 9
    .parameter "config"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v6, -0x1

    #@3
    .line 306
    if-eqz p1, :cond_d

    #@5
    iget v3, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@7
    if-ne v3, v6, :cond_13

    #@9
    iget-object v3, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@b
    if-nez v3, :cond_13

    #@d
    .line 308
    :cond_d
    new-instance v2, Landroid/net/wifi/NetworkUpdateResult;

    #@f
    invoke-direct {v2, v6}, Landroid/net/wifi/NetworkUpdateResult;-><init>(I)V

    #@12
    .line 322
    :goto_12
    return-object v2

    #@13
    .line 311
    :cond_13
    iget v3, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@15
    if-ne v3, v6, :cond_46

    #@17
    const/4 v1, 0x1

    #@18
    .line 312
    .local v1, newNetwork:Z
    :goto_18
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetworkNative(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@1b
    move-result-object v2

    #@1c
    .line 313
    .local v2, result:Landroid/net/wifi/NetworkUpdateResult;
    invoke-virtual {v2}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@1f
    move-result v0

    #@20
    .line 315
    .local v0, netId:I
    if-eqz v1, :cond_37

    #@22
    if-eq v0, v6, :cond_37

    #@24
    .line 316
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@26
    invoke-virtual {v3, v0, v4}, Landroid/net/wifi/WifiNative;->enableNetwork(IZ)Z

    #@29
    .line 317
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@2b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v3

    #@33
    check-cast v3, Landroid/net/wifi/WifiConfiguration;

    #@35
    iput v5, v3, Landroid/net/wifi/WifiConfiguration;->status:I

    #@37
    .line 319
    :cond_37
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@39
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@3c
    .line 320
    invoke-virtual {v2}, Landroid/net/wifi/NetworkUpdateResult;->isNewNetwork()Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_48

    #@42
    :goto_42
    invoke-direct {p0, p1, v4}, Landroid/net/wifi/WifiConfigStore;->sendConfiguredNetworksChangedBroadcast(Landroid/net/wifi/WifiConfiguration;I)V

    #@45
    goto :goto_12

    #@46
    .end local v0           #netId:I
    .end local v1           #newNetwork:Z
    .end local v2           #result:Landroid/net/wifi/NetworkUpdateResult;
    :cond_46
    move v1, v4

    #@47
    .line 311
    goto :goto_18

    #@48
    .restart local v0       #netId:I
    .restart local v1       #newNetwork:Z
    .restart local v2       #result:Landroid/net/wifi/NetworkUpdateResult;
    :cond_48
    move v4, v5

    #@49
    .line 320
    goto :goto_42
.end method

.method selectNetwork(I)Z
    .registers 10
    .parameter "netId"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v7, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 256
    if-ne p1, v7, :cond_6

    #@5
    .line 295
    :goto_5
    return v3

    #@6
    .line 259
    :cond_6
    iget v5, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@8
    if-eq v5, v7, :cond_11

    #@a
    iget v5, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@c
    const v6, 0xf4240

    #@f
    if-le v5, v6, :cond_33

    #@11
    .line 260
    :cond_11
    iget-object v5, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@13
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@16
    move-result-object v5

    #@17
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v1

    #@1b
    .local v1, i$:Ljava/util/Iterator;
    :cond_1b
    :goto_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_31

    #@21
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@27
    .line 261
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@29
    if-eq v5, v7, :cond_1b

    #@2b
    .line 262
    iput v3, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@2d
    .line 263
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetworkNative(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@30
    goto :goto_1b

    #@31
    .line 266
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_31
    iput v3, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@33
    .line 270
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_33
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@35
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@38
    .line 271
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    iput p1, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@3a
    .line 272
    iget v5, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@3c
    add-int/lit8 v5, v5, 0x1

    #@3e
    iput v5, p0, Landroid/net/wifi/WifiConfigStore;->mLastPriority:I

    #@40
    iput v5, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@42
    .line 275
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@44
    if-eqz v5, :cond_87

    #@46
    sget-boolean v5, Landroid/net/wifi/WifiConfigStore;->mLgeKtCm:Z

    #@48
    if-eqz v5, :cond_87

    #@4a
    .line 276
    iget-object v5, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@4c
    const-string/jumbo v6, "scan_ssid"

    #@4f
    invoke-virtual {v5, p1, v6}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    .line 277
    .local v2, value:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@56
    move-result v5

    #@57
    if-nez v5, :cond_62

    #@59
    .line 279
    :try_start_59
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5c
    move-result v5

    #@5d
    if-eqz v5, :cond_60

    #@5f
    move v3, v4

    #@60
    :cond_60
    iput-boolean v3, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z
    :try_end_62
    .catch Ljava/lang/NumberFormatException; {:try_start_59 .. :try_end_62} :catch_95

    #@62
    .line 283
    :cond_62
    :goto_62
    new-instance v3, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string/jumbo v5, "selectNetwork priority : "

    #@6a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@70
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    const-string v5, ", config.hiddenSSID : "

    #@76
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    iget-boolean v5, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@7c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v3

    #@84
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiConfigStore;->log(Ljava/lang/String;)V

    #@87
    .line 287
    .end local v2           #value:Ljava/lang/String;
    :cond_87
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiConfigStore;->addOrUpdateNetworkNative(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@8a
    .line 288
    iget-object v3, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@8c
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@8f
    .line 291
    invoke-virtual {p0, p1, v4}, Landroid/net/wifi/WifiConfigStore;->enableNetworkWithoutBroadcast(IZ)Z

    #@92
    move v3, v4

    #@93
    .line 295
    goto/16 :goto_5

    #@95
    .line 280
    .restart local v2       #value:Ljava/lang/String;
    :catch_95
    move-exception v3

    #@96
    goto :goto_62
.end method

.method setIpConfiguration(ILandroid/net/DhcpInfoInternal;)V
    .registers 7
    .parameter "netId"
    .parameter "dhcpInfo"

    #@0
    .prologue
    .line 634
    invoke-virtual {p2}, Landroid/net/DhcpInfoInternal;->makeLinkProperties()Landroid/net/LinkProperties;

    #@3
    move-result-object v1

    #@4
    .line 636
    .local v1, linkProperties:Landroid/net/LinkProperties;
    iget-object v2, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@10
    .line 637
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_21

    #@12
    .line 639
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@14
    if-eqz v2, :cond_1f

    #@16
    .line 640
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@18
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@1f
    .line 642
    :cond_1f
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@21
    .line 644
    :cond_21
    return-void
.end method

.method startWpsPbc(Landroid/net/wifi/WpsInfo;)Landroid/net/wifi/WpsResult;
    .registers 5
    .parameter "config"

    #@0
    .prologue
    .line 577
    new-instance v0, Landroid/net/wifi/WpsResult;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WpsResult;-><init>()V

    #@5
    .line 578
    .local v0, result:Landroid/net/wifi/WpsResult;
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7
    iget-object v2, p1, Landroid/net/wifi/WpsInfo;->BSSID:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->startWpsPbc(Ljava/lang/String;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_17

    #@f
    .line 580
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->markAllNetworksDisabled()V

    #@12
    .line 581
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->SUCCESS:Landroid/net/wifi/WpsResult$Status;

    #@14
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@16
    .line 586
    :goto_16
    return-object v0

    #@17
    .line 583
    :cond_17
    const-string v1, "Failed to start WPS push button configuration"

    #@19
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@1c
    .line 584
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->FAILURE:Landroid/net/wifi/WpsResult$Status;

    #@1e
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@20
    goto :goto_16
.end method

.method startWpsWithPinFromAccessPoint(Landroid/net/wifi/WpsInfo;)Landroid/net/wifi/WpsResult;
    .registers 6
    .parameter "config"

    #@0
    .prologue
    .line 540
    new-instance v0, Landroid/net/wifi/WpsResult;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WpsResult;-><init>()V

    #@5
    .line 541
    .local v0, result:Landroid/net/wifi/WpsResult;
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7
    iget-object v2, p1, Landroid/net/wifi/WpsInfo;->BSSID:Ljava/lang/String;

    #@9
    iget-object v3, p1, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@b
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiNative;->startWpsRegistrar(Ljava/lang/String;Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_19

    #@11
    .line 543
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->markAllNetworksDisabled()V

    #@14
    .line 544
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->SUCCESS:Landroid/net/wifi/WpsResult$Status;

    #@16
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@18
    .line 549
    :goto_18
    return-object v0

    #@19
    .line 546
    :cond_19
    const-string v1, "Failed to start WPS pin method configuration"

    #@1b
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@1e
    .line 547
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->FAILURE:Landroid/net/wifi/WpsResult$Status;

    #@20
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@22
    goto :goto_18
.end method

.method startWpsWithPinFromDevice(Landroid/net/wifi/WpsInfo;)Landroid/net/wifi/WpsResult;
    .registers 5
    .parameter "config"

    #@0
    .prologue
    .line 558
    new-instance v0, Landroid/net/wifi/WpsResult;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WpsResult;-><init>()V

    #@5
    .line 559
    .local v0, result:Landroid/net/wifi/WpsResult;
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7
    iget-object v2, p1, Landroid/net/wifi/WpsInfo;->BSSID:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->startWpsPinDisplay(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->pin:Ljava/lang/String;

    #@f
    .line 561
    iget-object v1, v0, Landroid/net/wifi/WpsResult;->pin:Ljava/lang/String;

    #@11
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_1f

    #@17
    .line 562
    invoke-direct {p0}, Landroid/net/wifi/WifiConfigStore;->markAllNetworksDisabled()V

    #@1a
    .line 563
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->SUCCESS:Landroid/net/wifi/WpsResult$Status;

    #@1c
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@1e
    .line 568
    :goto_1e
    return-object v0

    #@1f
    .line 565
    :cond_1f
    const-string v1, "Failed to start WPS pin method configuration"

    #@21
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiConfigStore;->loge(Ljava/lang/String;)V

    #@24
    .line 566
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->FAILURE:Landroid/net/wifi/WpsResult$Status;

    #@26
    iput-object v1, v0, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    #@28
    goto :goto_1e
.end method

.method updateStatus(ILandroid/net/NetworkInfo$DetailedState;)V
    .registers 6
    .parameter "netId"
    .parameter "state"

    #@0
    .prologue
    .line 326
    const/4 v1, -0x1

    #@1
    if-eq p1, v1, :cond_11

    #@3
    .line 327
    iget-object v1, p0, Landroid/net/wifi/WifiConfigStore;->mConfiguredNetworks:Ljava/util/HashMap;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@f
    .line 328
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-nez v0, :cond_12

    #@11
    .line 344
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_11
    :goto_11
    return-void

    #@12
    .line 329
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    :cond_12
    sget-object v1, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    #@14
    invoke-virtual {p2}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    #@17
    move-result v2

    #@18
    aget v1, v1, v2

    #@1a
    packed-switch v1, :pswitch_data_2a

    #@1d
    goto :goto_11

    #@1e
    .line 331
    :pswitch_1e
    const/4 v1, 0x0

    #@1f
    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@21
    goto :goto_11

    #@22
    .line 335
    :pswitch_22
    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@24
    if-nez v1, :cond_11

    #@26
    .line 336
    const/4 v1, 0x2

    #@27
    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@29
    goto :goto_11

    #@2a
    .line 329
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_22
    .end packed-switch
.end method
