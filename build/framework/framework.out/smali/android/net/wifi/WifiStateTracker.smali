.class public Landroid/net/wifi/WifiStateTracker;
.super Ljava/lang/Object;
.source "WifiStateTracker.java"

# interfaces
.implements Landroid/net/NetworkStateTracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiStateTracker$1;,
        Landroid/net/wifi/WifiStateTracker$WifiStateReceiver;
    }
.end annotation


# static fields
.field private static final LOGV:Z = true

.field private static final NETWORKTYPE:Ljava/lang/String; = "WIFI"

.field private static final TAG:Ljava/lang/String; = "WifiStateTracker"

.field private static wifi_NetworkType:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCsHandler:Landroid/os/Handler;

.field private mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mLastState:Landroid/net/NetworkInfo$State;

.field private mLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mLinkProperties:Landroid/net/LinkProperties;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiStateReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/net/wifi/WifiStateTracker;->wifi_NetworkType:I

    #@3
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .registers 6
    .parameter "netType"
    .parameter "networkName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@9
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@b
    .line 53
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@d
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@10
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@12
    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@14
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@17
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@19
    .line 59
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@1b
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mLastState:Landroid/net/NetworkInfo$State;

    #@1d
    .line 71
    sput p1, Landroid/net/wifi/WifiStateTracker;->wifi_NetworkType:I

    #@1f
    .line 73
    new-instance v0, Landroid/net/NetworkInfo;

    #@21
    const-string v1, ""

    #@23
    invoke-direct {v0, p1, v2, p2, v1}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@26
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@28
    .line 74
    new-instance v0, Landroid/net/LinkProperties;

    #@2a
    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    #@2d
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2f
    .line 75
    new-instance v0, Landroid/net/LinkCapabilities;

    #@31
    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    #@34
    iput-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@36
    .line 77
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@38
    invoke-virtual {v0, v2}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    #@3b
    .line 78
    invoke-virtual {p0, v2}, Landroid/net/wifi/WifiStateTracker;->setTeardownRequested(Z)V

    #@3e
    .line 79
    return-void
.end method

.method static synthetic access$100(Landroid/net/wifi/WifiStateTracker;)Landroid/net/LinkProperties;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/net/wifi/WifiStateTracker;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/net/wifi/WifiStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/net/wifi/WifiStateTracker;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Landroid/net/wifi/WifiStateTracker;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/net/wifi/WifiStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Landroid/net/wifi/WifiStateTracker;)Landroid/net/LinkCapabilities;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Landroid/net/wifi/WifiStateTracker;Landroid/net/LinkCapabilities;)Landroid/net/LinkCapabilities;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/net/wifi/WifiStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Landroid/net/wifi/WifiStateTracker;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mLastState:Landroid/net/NetworkInfo$State;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Landroid/net/wifi/WifiStateTracker;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/net/wifi/WifiStateTracker;->mLastState:Landroid/net/NetworkInfo$State;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Landroid/net/wifi/WifiStateTracker;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mCsHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method


# virtual methods
.method public captivePortalCheckComplete()V
    .registers 2

    #@0
    .prologue
    .line 138
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->captivePortalCheckComplete()V

    #@5
    .line 139
    return-void
.end method

.method public defaultRouteSet(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 223
    return-void
.end method

.method public getLinkCapabilities()Landroid/net/LinkCapabilities;
    .registers 3

    #@0
    .prologue
    .line 208
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    iget-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkCapabilities;-><init>(Landroid/net/LinkCapabilities;)V

    #@7
    return-object v0
.end method

.method public getLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 198
    new-instance v0, Landroid/net/LinkProperties;

    #@2
    iget-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@7
    return-object v0
.end method

.method public getNetworkInfo()Landroid/net/NetworkInfo;
    .registers 3

    #@0
    .prologue
    .line 191
    new-instance v0, Landroid/net/NetworkInfo;

    #@2
    iget-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@4
    invoke-direct {v0, v1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@7
    return-object v0
.end method

.method public getTcpBufferSizesPropName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 230
    const-string/jumbo v0, "net.tcp.buffersize.wifi"

    #@3
    return-object v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 160
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDefaultRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPrivateDnsRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isTeardownRequested()Z
    .registers 2

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public privateDnsRouteSet(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 185
    return-void
.end method

.method public reconnect()Z
    .registers 3

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@6
    .line 129
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@8
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startWifi()Z

    #@b
    .line 130
    const/4 v0, 0x1

    #@c
    return v0
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 2
    .parameter "msger"

    #@0
    .prologue
    .line 290
    return-void
.end method

.method public setDependencyMet(Z)V
    .registers 2
    .parameter "met"

    #@0
    .prologue
    .line 287
    return-void
.end method

.method public setPolicyDataEnable(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 171
    return-void
.end method

.method public setRadio(Z)Z
    .registers 3
    .parameter "turnOn"

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@5
    .line 147
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public setTeardownRequested(Z)V
    .registers 3
    .parameter "isRequested"

    #@0
    .prologue
    .line 83
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 84
    return-void
.end method

.method public setUserDataEnable(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 165
    const-string v0, "WifiStateTracker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "ignoring setUserDataEnable("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ")"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 166
    return-void
.end method

.method public startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 94
    iput-object p2, p0, Landroid/net/wifi/WifiStateTracker;->mCsHandler:Landroid/os/Handler;

    #@2
    .line 95
    iput-object p1, p0, Landroid/net/wifi/WifiStateTracker;->mContext:Landroid/content/Context;

    #@4
    .line 97
    iget-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mContext:Landroid/content/Context;

    #@6
    const-string/jumbo v2, "wifi"

    #@9
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/net/wifi/WifiManager;

    #@f
    iput-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@11
    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    #@13
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@16
    .line 101
    .local v0, filter:Landroid/content/IntentFilter;
    sget v1, Landroid/net/wifi/WifiStateTracker;->wifi_NetworkType:I

    #@18
    const/4 v2, 0x1

    #@19
    if-ne v1, v2, :cond_35

    #@1b
    .line 103
    const-string v1, "android.net.wifi.STATE_CHANGE"

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20
    .line 104
    const-string v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    #@22
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@25
    .line 110
    :cond_25
    :goto_25
    new-instance v1, Landroid/net/wifi/WifiStateTracker$WifiStateReceiver;

    #@27
    const/4 v2, 0x0

    #@28
    invoke-direct {v1, p0, v2}, Landroid/net/wifi/WifiStateTracker$WifiStateReceiver;-><init>(Landroid/net/wifi/WifiStateTracker;Landroid/net/wifi/WifiStateTracker$1;)V

    #@2b
    iput-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    #@2d
    .line 111
    iget-object v1, p0, Landroid/net/wifi/WifiStateTracker;->mContext:Landroid/content/Context;

    #@2f
    iget-object v2, p0, Landroid/net/wifi/WifiStateTracker;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    #@31
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@34
    .line 112
    return-void

    #@35
    .line 106
    :cond_35
    sget v1, Landroid/net/wifi/WifiStateTracker;->wifi_NetworkType:I

    #@37
    const/16 v2, 0xd

    #@39
    if-ne v1, v2, :cond_25

    #@3b
    .line 107
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@40
    goto :goto_25
.end method

.method public teardown()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 119
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@6
    .line 120
    iget-object v0, p0, Landroid/net/wifi/WifiStateTracker;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@8
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->stopWifi()Z

    #@b
    .line 121
    return v1
.end method
