.class public Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;
.super Ljava/lang/Object;
.source "WifiP2pServiceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final SERVICE_TYPE_ALL:I = 0x0

.field public static final SERVICE_TYPE_BONJOUR:I = 0x1

.field public static final SERVICE_TYPE_UPNP:I = 0x2

.field public static final SERVICE_TYPE_VENDOR_SPECIFIC:I = 0xff

.field public static final SERVICE_TYPE_WS_DISCOVERY:I = 0x3


# instance fields
.field private mQueryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 169
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo$1;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method protected constructor <init>(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 94
    .local p1, queryList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 95
    if-nez p1, :cond_e

    #@5
    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "query list cannot be null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 98
    :cond_e
    iput-object p1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@10
    .line 99
    return-void
.end method

.method static bin2HexStr([B)Ljava/lang/String;
    .registers 10
    .parameter "data"

    #@0
    .prologue
    .line 119
    new-instance v6, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 121
    .local v6, sb:Ljava/lang/StringBuffer;
    move-object v0, p0

    #@6
    .local v0, arr$:[B
    array-length v4, v0

    #@7
    .local v4, len$:I
    const/4 v3, 0x0

    #@8
    .local v3, i$:I
    :goto_8
    if-ge v3, v4, :cond_2b

    #@a
    aget-byte v1, v0, v3

    #@c
    .line 122
    .local v1, b:B
    const/4 v5, 0x0

    #@d
    .line 124
    .local v5, s:Ljava/lang/String;
    and-int/lit16 v7, v1, 0xff

    #@f
    :try_start_f
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_12} :catch_25

    #@12
    move-result-object v5

    #@13
    .line 130
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@16
    move-result v7

    #@17
    const/4 v8, 0x1

    #@18
    if-ne v7, v8, :cond_1f

    #@1a
    .line 131
    const/16 v7, 0x30

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@1f
    .line 133
    :cond_1f
    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@22
    .line 121
    add-int/lit8 v3, v3, 0x1

    #@24
    goto :goto_8

    #@25
    .line 125
    :catch_25
    move-exception v2

    #@26
    .line 126
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@29
    .line 127
    const/4 v7, 0x0

    #@2a
    .line 135
    .end local v1           #b:B
    .end local v2           #e:Ljava/lang/Exception;
    .end local v5           #s:Ljava/lang/String;
    :goto_2a
    return-object v7

    #@2b
    :cond_2b
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    goto :goto_2a
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 160
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    #@0
    .prologue
    .line 140
    if-ne p1, p0, :cond_4

    #@2
    .line 141
    const/4 v1, 0x1

    #@3
    .line 148
    :goto_3
    return v1

    #@4
    .line 143
    :cond_4
    instance-of v1, p1, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;

    #@6
    if-nez v1, :cond_a

    #@8
    .line 144
    const/4 v1, 0x0

    #@9
    goto :goto_3

    #@a
    :cond_a
    move-object v0, p1

    #@b
    .line 147
    check-cast v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;

    #@d
    .line 148
    .local v0, servInfo:Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@f
    iget-object v2, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    goto :goto_3
.end method

.method public getSupplicantQueryList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 153
    const/16 v0, 0x11

    #@2
    .line 154
    .local v0, result:I
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@4
    if-nez v1, :cond_a

    #@6
    const/4 v1, 0x0

    #@7
    :goto_7
    add-int/lit16 v0, v1, 0x20f

    #@9
    .line 155
    return v0

    #@a
    .line 154
    :cond_a
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@c
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@f
    move-result v1

    #@10
    goto :goto_7
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->mQueryList:Ljava/util/List;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@5
    .line 166
    return-void
.end method
