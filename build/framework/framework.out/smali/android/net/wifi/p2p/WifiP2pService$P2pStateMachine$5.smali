.class Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$5;
.super Ljava/lang/Object;
.source "WifiP2pService.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->notifyInvitationReceived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2203
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$5;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 5
    .parameter "ed"

    #@0
    .prologue
    .line 2206
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$5;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2
    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@7
    move-result-object v1

    #@8
    const/4 v2, -0x1

    #@9
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    #@c
    move-result-object v0

    #@d
    .line 2207
    .local v0, positiveButton:Landroid/widget/Button;
    if-eqz v0, :cond_1b

    #@f
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@12
    move-result v1

    #@13
    const/16 v2, 0x8

    #@15
    if-lt v1, v2, :cond_1c

    #@17
    const/4 v1, 0x1

    #@18
    :goto_18
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@1b
    .line 2208
    :cond_1b
    return-void

    #@1c
    .line 2207
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_18
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 2210
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 2212
    return-void
.end method
