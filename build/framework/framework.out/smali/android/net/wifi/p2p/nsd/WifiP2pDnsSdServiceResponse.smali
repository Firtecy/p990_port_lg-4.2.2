.class public Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;
.super Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
.source "WifiP2pDnsSdServiceResponse.java"


# static fields
.field private static final sVmpack:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDnsQueryName:Ljava/lang/String;

.field private mDnsType:I

.field private mInstanceName:Ljava/lang/String;

.field private final mTxtRecord:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVersion:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 81
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->sVmpack:Ljava/util/Map;

    #@7
    .line 82
    sget-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->sVmpack:Ljava/util/Map;

    #@9
    const/16 v1, 0xc

    #@b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v1

    #@f
    const-string v2, "_tcp.local."

    #@11
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 83
    sget-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->sVmpack:Ljava/util/Map;

    #@16
    const/16 v1, 0x11

    #@18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v1

    #@1c
    const-string/jumbo v2, "local."

    #@1f
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 84
    sget-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->sVmpack:Ljava/util/Map;

    #@24
    const/16 v1, 0x1c

    #@26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v1

    #@2a
    const-string v2, "_udp.local."

    #@2c
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 85
    return-void
.end method

.method protected constructor <init>(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)V
    .registers 11
    .parameter "status"
    .parameter "tranId"
    .parameter "dev"
    .parameter "data"

    #@0
    .prologue
    .line 154
    const/4 v1, 0x1

    #@1
    move-object v0, p0

    #@2
    move v2, p1

    #@3
    move v3, p2

    #@4
    move-object v4, p3

    #@5
    move-object v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;-><init>(IIILandroid/net/wifi/p2p/WifiP2pDevice;[B)V

    #@9
    .line 70
    new-instance v0, Ljava/util/HashMap;

    #@b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@e
    iput-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mTxtRecord:Ljava/util/HashMap;

    #@10
    .line 156
    invoke-direct {p0}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->parse()Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1e

    #@16
    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v1, "Malformed bonjour service response"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 159
    :cond_1e
    return-void
.end method

.method static newInstance(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;
    .registers 7
    .parameter "status"
    .parameter "transId"
    .parameter "dev"
    .parameter "data"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 297
    if-eqz p0, :cond_9

    #@3
    .line 298
    new-instance v1, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;

    #@5
    invoke-direct {v1, p0, p1, p2, v2}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;-><init>(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)V

    #@8
    .line 307
    :goto_8
    return-object v1

    #@9
    .line 302
    :cond_9
    :try_start_9
    new-instance v1, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;

    #@b
    invoke-direct {v1, p0, p1, p2, p3}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;-><init>(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)V
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    goto :goto_8

    #@f
    .line 304
    :catch_f
    move-exception v0

    #@10
    .line 305
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@13
    move-object v1, v2

    #@14
    .line 307
    goto :goto_8
.end method

.method private parse()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 176
    iget-object v5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@4
    if-nez v5, :cond_7

    #@6
    .line 213
    :goto_6
    return v3

    #@7
    .line 181
    :cond_7
    new-instance v0, Ljava/io/DataInputStream;

    #@9
    new-instance v5, Ljava/io/ByteArrayInputStream;

    #@b
    iget-object v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@d
    invoke-direct {v5, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@10
    invoke-direct {v0, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@13
    .line 183
    .local v0, dis:Ljava/io/DataInputStream;
    invoke-direct {p0, v0}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->readDnsName(Ljava/io/DataInputStream;)Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    iput-object v5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@19
    .line 184
    iget-object v5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@1b
    if-nez v5, :cond_1f

    #@1d
    move v3, v4

    #@1e
    .line 185
    goto :goto_6

    #@1f
    .line 189
    :cond_1f
    :try_start_1f
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    #@22
    move-result v5

    #@23
    iput v5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsType:I

    #@25
    .line 190
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedByte()I

    #@28
    move-result v5

    #@29
    iput v5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mVersion:I
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2b} :catch_39

    #@2b
    .line 196
    iget v5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsType:I

    #@2d
    const/16 v6, 0xc

    #@2f
    if-ne v5, v6, :cond_61

    #@31
    .line 197
    invoke-direct {p0, v0}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->readDnsName(Ljava/io/DataInputStream;)Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    .line 198
    .local v2, rData:Ljava/lang/String;
    if-nez v2, :cond_3f

    #@37
    move v3, v4

    #@38
    .line 199
    goto :goto_6

    #@39
    .line 191
    .end local v2           #rData:Ljava/lang/String;
    :catch_39
    move-exception v1

    #@3a
    .line 192
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@3d
    move v3, v4

    #@3e
    .line 193
    goto :goto_6

    #@3f
    .line 201
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #rData:Ljava/lang/String;
    :cond_3f
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@42
    move-result v5

    #@43
    iget-object v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@45
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@48
    move-result v6

    #@49
    if-gt v5, v6, :cond_4d

    #@4b
    move v3, v4

    #@4c
    .line 202
    goto :goto_6

    #@4d
    .line 205
    :cond_4d
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@50
    move-result v5

    #@51
    iget-object v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@53
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@56
    move-result v6

    #@57
    sub-int/2addr v5, v6

    #@58
    add-int/lit8 v5, v5, -0x1

    #@5a
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    iput-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mInstanceName:Ljava/lang/String;

    #@60
    goto :goto_6

    #@61
    .line 207
    .end local v2           #rData:Ljava/lang/String;
    :cond_61
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsType:I

    #@63
    const/16 v5, 0x10

    #@65
    if-ne v3, v5, :cond_6c

    #@67
    .line 208
    invoke-direct {p0, v0}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->readTxtData(Ljava/io/DataInputStream;)Z

    #@6a
    move-result v3

    #@6b
    goto :goto_6

    #@6c
    :cond_6c
    move v3, v4

    #@6d
    .line 210
    goto :goto_6
.end method

.method private readDnsName(Ljava/io/DataInputStream;)Ljava/lang/String;
    .registers 11
    .parameter "dis"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 223
    new-instance v4, Ljava/lang/StringBuffer;

    #@3
    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    #@6
    .line 226
    .local v4, sb:Ljava/lang/StringBuffer;
    new-instance v5, Ljava/util/HashMap;

    #@8
    sget-object v7, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->sVmpack:Ljava/util/Map;

    #@a
    invoke-direct {v5, v7}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@d
    .line 227
    .local v5, vmpack:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@f
    if-eqz v7, :cond_1c

    #@11
    .line 228
    const/16 v7, 0x27

    #@13
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v7

    #@17
    iget-object v8, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@19
    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 232
    :cond_1c
    :goto_1c
    :try_start_1c
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    #@1f
    move-result v2

    #@20
    .line 233
    .local v2, i:I
    if-nez v2, :cond_27

    #@22
    .line 234
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@25
    move-result-object v6

    #@26
    .line 254
    .end local v2           #i:I
    :cond_26
    :goto_26
    return-object v6

    #@27
    .line 235
    .restart local v2       #i:I
    :cond_27
    const/16 v7, 0xc0

    #@29
    if-ne v2, v7, :cond_43

    #@2b
    .line 237
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    #@2e
    move-result v7

    #@2f
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Ljava/lang/String;

    #@39
    .line 238
    .local v3, ref:Ljava/lang/String;
    if-eqz v3, :cond_26

    #@3b
    .line 242
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3e
    .line 243
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@41
    move-result-object v6

    #@42
    goto :goto_26

    #@43
    .line 245
    .end local v3           #ref:Ljava/lang/String;
    :cond_43
    new-array v0, v2, [B

    #@45
    .line 246
    .local v0, data:[B
    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    #@48
    .line 247
    new-instance v7, Ljava/lang/String;

    #@4a
    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V

    #@4d
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@50
    .line 248
    const-string v7, "."

    #@52
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_55} :catch_56

    #@55
    goto :goto_1c

    #@56
    .line 251
    .end local v0           #data:[B
    .end local v2           #i:I
    :catch_56
    move-exception v1

    #@57
    .line 252
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@5a
    goto :goto_26
.end method

.method private readTxtData(Ljava/io/DataInputStream;)Z
    .registers 11
    .parameter "dis"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 265
    :goto_2
    :try_start_2
    invoke-virtual {p1}, Ljava/io/DataInputStream;->available()I

    #@5
    move-result v6

    #@6
    if-lez v6, :cond_e

    #@8
    .line 266
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedByte()I

    #@b
    move-result v3

    #@c
    .line 267
    .local v3, len:I
    if-nez v3, :cond_10

    #@e
    .end local v3           #len:I
    :cond_e
    move v4, v5

    #@f
    .line 282
    :cond_f
    :goto_f
    return v4

    #@10
    .line 270
    .restart local v3       #len:I
    :cond_10
    new-array v0, v3, [B

    #@12
    .line 271
    .local v0, data:[B
    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    #@15
    .line 272
    new-instance v6, Ljava/lang/String;

    #@17
    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    #@1a
    const-string v7, "="

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    .line 273
    .local v2, keyVal:[Ljava/lang/String;
    array-length v6, v2

    #@21
    const/4 v7, 0x2

    #@22
    if-ne v6, v7, :cond_f

    #@24
    .line 276
    iget-object v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mTxtRecord:Ljava/util/HashMap;

    #@26
    const/4 v7, 0x0

    #@27
    aget-object v7, v2, v7

    #@29
    const/4 v8, 0x1

    #@2a
    aget-object v8, v2, v8

    #@2c
    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2f} :catch_30

    #@2f
    goto :goto_2

    #@30
    .line 279
    .end local v0           #data:[B
    .end local v2           #keyVal:[Ljava/lang/String;
    .end local v3           #len:I
    :catch_30
    move-exception v1

    #@31
    .line 280
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@34
    goto :goto_f
.end method


# virtual methods
.method public getDnsQueryName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getDnsType()I
    .registers 2

    #@0
    .prologue
    .line 100
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsType:I

    #@2
    return v0
.end method

.method public getInstanceName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mInstanceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTxtRecord()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mTxtRecord:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getVersion()I
    .registers 2

    #@0
    .prologue
    .line 108
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mVersion:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 129
    new-instance v2, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 130
    .local v2, sbuf:Ljava/lang/StringBuffer;
    const-string/jumbo v3, "serviceType:DnsSd("

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b
    move-result-object v3

    #@c
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@11
    move-result-object v3

    #@12
    const-string v4, ")"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@17
    .line 131
    const-string v3, " status:"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    move-result-object v3

    #@1d
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@1f
    invoke-static {v4}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse$Status;->toString(I)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@26
    .line 132
    const-string v3, " srcAddr:"

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2e
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@33
    .line 133
    const-string v3, " version:"

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@38
    move-result-object v3

    #@39
    const-string v4, "%02x"

    #@3b
    const/4 v5, 0x1

    #@3c
    new-array v5, v5, [Ljava/lang/Object;

    #@3e
    const/4 v6, 0x0

    #@3f
    iget v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mVersion:I

    #@41
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v7

    #@45
    aput-object v7, v5, v6

    #@47
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@4e
    .line 134
    const-string v3, " dnsName:"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@53
    move-result-object v3

    #@54
    iget-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mDnsQueryName:Ljava/lang/String;

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@59
    .line 135
    const-string v3, " TxtRecord:"

    #@5b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5e
    .line 136
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mTxtRecord:Ljava/util/HashMap;

    #@60
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@63
    move-result-object v3

    #@64
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@67
    move-result-object v0

    #@68
    .local v0, i$:Ljava/util/Iterator;
    :goto_68
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@6b
    move-result v3

    #@6c
    if-eqz v3, :cond_90

    #@6e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@71
    move-result-object v1

    #@72
    check-cast v1, Ljava/lang/String;

    #@74
    .line 137
    .local v1, key:Ljava/lang/String;
    const-string v3, " key:"

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@7d
    move-result-object v3

    #@7e
    const-string v4, " value:"

    #@80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@83
    move-result-object v4

    #@84
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mTxtRecord:Ljava/util/HashMap;

    #@86
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    move-result-object v3

    #@8a
    check-cast v3, Ljava/lang/String;

    #@8c
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@8f
    goto :goto_68

    #@90
    .line 139
    .end local v1           #key:Ljava/lang/String;
    :cond_90
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mInstanceName:Ljava/lang/String;

    #@92
    if-eqz v3, :cond_9f

    #@94
    .line 140
    const-string v3, " InsName:"

    #@96
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@99
    move-result-object v3

    #@9a
    iget-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->mInstanceName:Ljava/lang/String;

    #@9c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@9f
    .line 142
    :cond_9f
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@a2
    move-result-object v3

    #@a3
    return-object v3
.end method
