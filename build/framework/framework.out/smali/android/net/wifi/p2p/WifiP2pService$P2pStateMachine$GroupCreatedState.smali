.class Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;
.super Lcom/android/internal/util/State;
.source "WifiP2pService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GroupCreatedState"
.end annotation


# instance fields
.field final synthetic this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1592
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1595
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4
    invoke-virtual {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@b
    .line 1597
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@d
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@f
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@12
    move-result-object v0

    #@13
    iget v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@15
    if-nez v0, :cond_30

    #@17
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@19
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1b
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$10900(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_30

    #@21
    .line 1598
    const-string v0, "WifiP2pService"

    #@23
    const-string v1, "[LGE_WLAN] skip enter() already in GroupCreatedState"

    #@25
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1599
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2a
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2c
    invoke-static {v0, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$10902(Landroid/net/wifi/p2p/WifiP2pService;Z)Z

    #@2f
    .line 1621
    :goto_2f
    return-void

    #@30
    .line 1602
    :cond_30
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@32
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@34
    invoke-static {v0, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$10902(Landroid/net/wifi/p2p/WifiP2pService;Z)Z

    #@37
    .line 1604
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@39
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3b
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$3100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/NetworkInfo;

    #@3e
    move-result-object v0

    #@3f
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@41
    invoke-virtual {v0, v1, v3, v3}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@44
    .line 1606
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@46
    invoke-static {v0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;I)V

    #@49
    .line 1609
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4b
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_5c

    #@55
    .line 1610
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@57
    const-string v1, "192.168.49.1"

    #@59
    invoke-static {v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@5c
    .line 1615
    :cond_5c
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5e
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@60
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$5200(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_6b

    #@66
    .line 1616
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@68
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@6b
    .line 1619
    :cond_6b
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6d
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@6f
    const/4 v1, 0x3

    #@70
    invoke-static {v0, v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$11202(Landroid/net/wifi/p2p/WifiP2pService;I)I

    #@73
    goto :goto_2f
.end method

.method public exit()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1909
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@5
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$10900(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_13

    #@b
    .line 1910
    const-string v0, "WifiP2pService"

    #@d
    const-string v1, "[LGE_WLAN] skip exit() in GroupCreatedState 20121213"

    #@f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 1920
    :cond_12
    :goto_12
    return-void

    #@13
    .line 1914
    :cond_13
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@15
    invoke-static {v0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11302(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@18
    .line 1915
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1a
    const/4 v1, 0x3

    #@1b
    invoke-static {v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;I)V

    #@1e
    .line 1916
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@20
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$12200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@23
    .line 1917
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@25
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@27
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$3100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/NetworkInfo;

    #@2a
    move-result-object v0

    #@2b
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@2d
    invoke-virtual {v0, v1, v2, v2}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 1918
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@32
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@35
    .line 1919
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@37
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@39
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@3c
    move-result-object v0

    #@3d
    if-eqz v0, :cond_12

    #@3f
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@41
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@43
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@46
    move-result-object v0

    #@47
    const/4 v1, 0x0

    #@48
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setWifiP2pNotificationIcon(Z)V

    #@4b
    goto :goto_12
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 19
    .parameter "message"

    #@0
    .prologue
    .line 1625
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4
    new-instance v14, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual/range {p0 .. p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@c
    move-result-object v15

    #@d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v14

    #@11
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->toString()Ljava/lang/String;

    #@14
    move-result-object v15

    #@15
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v14

    #@19
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v14

    #@1d
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@20
    .line 1626
    move-object/from16 v0, p1

    #@22
    iget v13, v0, Landroid/os/Message;->what:I

    #@24
    sparse-switch v13, :sswitch_data_880

    #@27
    .line 1902
    const/4 v13, 0x0

    #@28
    .line 1904
    :goto_28
    return v13

    #@29
    .line 1629
    :sswitch_29
    move-object/from16 v0, p1

    #@2b
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d
    check-cast v5, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2f
    .line 1630
    .local v5, device:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v6, v5, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@31
    .line 1632
    .local v6, deviceAddress:Ljava/lang/String;
    move-object/from16 v0, p1

    #@33
    iget v13, v0, Landroid/os/Message;->what:I

    #@35
    const v14, 0x2402a

    #@38
    if-ne v13, v14, :cond_7c

    #@3a
    .line 1633
    move-object/from16 v0, p0

    #@3c
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3e
    new-instance v14, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    invoke-virtual/range {p0 .. p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@46
    move-result-object v15

    #@47
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v14

    #@4b
    const-string v15, " [LGE]legacy wifi connected:"

    #@4d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v14

    #@51
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v14

    #@55
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v14

    #@59
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@5c
    .line 1634
    move-object/from16 v0, p0

    #@5e
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@60
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@63
    move-result-object v13

    #@64
    new-instance v14, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@66
    invoke-direct {v14, v6}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@69
    invoke-virtual {v13, v14}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->remove(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    #@6c
    .line 1635
    move-object/from16 v0, p0

    #@6e
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@70
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@73
    move-result-object v13

    #@74
    new-instance v14, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@76
    invoke-direct {v14, v6}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@79
    invoke-virtual {v13, v14}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->update(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@7c
    .line 1638
    :cond_7c
    if-eqz v6, :cond_126

    #@7e
    .line 1639
    move-object/from16 v0, p0

    #@80
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@82
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@85
    move-result-object v13

    #@86
    if-eqz v13, :cond_a0

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@8c
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@8f
    move-result-object v13

    #@90
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@92
    invoke-virtual {v6, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@95
    move-result v13

    #@96
    if-eqz v13, :cond_a0

    #@98
    .line 1641
    move-object/from16 v0, p0

    #@9a
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@9c
    const/4 v14, 0x0

    #@9d
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11302(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@a0
    .line 1643
    :cond_a0
    move-object/from16 v0, p0

    #@a2
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@a4
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@a7
    move-result-object v13

    #@a8
    invoke-virtual {v13, v6}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@ab
    move-result-object v13

    #@ac
    if-eqz v13, :cond_11a

    #@ae
    .line 1644
    move-object/from16 v0, p0

    #@b0
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@b2
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@b5
    move-result-object v13

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@ba
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@bd
    move-result-object v14

    #@be
    invoke-virtual {v14, v6}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@c1
    move-result-object v14

    #@c2
    invoke-virtual {v13, v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->addClient(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@c5
    .line 1648
    :goto_c5
    move-object/from16 v0, p0

    #@c7
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@c9
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@cb
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@ce
    move-result-object v13

    #@cf
    if-eqz v13, :cond_df

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@d5
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d7
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@da
    move-result-object v13

    #@db
    const/4 v14, 0x1

    #@dc
    invoke-interface {v13, v14}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setWifiP2pNotificationIcon(Z)V

    #@df
    .line 1649
    :cond_df
    move-object/from16 v0, p0

    #@e1
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@e3
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@e6
    move-result-object v13

    #@e7
    const/4 v14, 0x0

    #@e8
    invoke-virtual {v13, v6, v14}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->updateStatus(Ljava/lang/String;I)V

    #@eb
    .line 1650
    move-object/from16 v0, p0

    #@ed
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@ef
    new-instance v14, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    invoke-virtual/range {p0 .. p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@f7
    move-result-object v15

    #@f8
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v14

    #@fc
    const-string v15, " ap sta connected"

    #@fe
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v14

    #@102
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v14

    #@106
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@109
    .line 1651
    move-object/from16 v0, p0

    #@10b
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@10d
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@110
    .line 1655
    :goto_110
    move-object/from16 v0, p0

    #@112
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@114
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@117
    .line 1904
    .end local v5           #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v6           #deviceAddress:Ljava/lang/String;
    :cond_117
    :goto_117
    const/4 v13, 0x1

    #@118
    goto/16 :goto_28

    #@11a
    .line 1646
    .restart local v5       #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    .restart local v6       #deviceAddress:Ljava/lang/String;
    :cond_11a
    move-object/from16 v0, p0

    #@11c
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@11e
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@121
    move-result-object v13

    #@122
    invoke-virtual {v13, v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->addClient(Ljava/lang/String;)V

    #@125
    goto :goto_c5

    #@126
    .line 1653
    :cond_126
    move-object/from16 v0, p0

    #@128
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@12a
    const-string v14, "Connect on null device address, ignore"

    #@12c
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@12f
    goto :goto_110

    #@130
    .line 1658
    .end local v5           #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v6           #deviceAddress:Ljava/lang/String;
    :sswitch_130
    move-object/from16 v0, p1

    #@132
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@134
    check-cast v5, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@136
    .line 1659
    .restart local v5       #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v6, v5, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@138
    .line 1660
    .restart local v6       #deviceAddress:Ljava/lang/String;
    if-eqz v6, :cond_289

    #@13a
    .line 1661
    move-object/from16 v0, p0

    #@13c
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@13e
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@141
    move-result-object v13

    #@142
    const/4 v14, 0x3

    #@143
    invoke-virtual {v13, v6, v14}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->updateStatus(Ljava/lang/String;I)V

    #@146
    .line 1663
    move-object/from16 v0, p0

    #@148
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@14a
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@14d
    move-result-object v13

    #@14e
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@151
    move-result-object v13

    #@152
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@155
    move-result-object v8

    #@156
    .local v8, i$:Ljava/util/Iterator;
    :cond_156
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@159
    move-result v13

    #@15a
    if-eqz v13, :cond_17f

    #@15c
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15f
    move-result-object v4

    #@160
    check-cast v4, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@162
    .line 1664
    .local v4, d:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v13, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@164
    invoke-virtual {v13, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@167
    move-result v13

    #@168
    if-eqz v13, :cond_156

    #@16a
    iget-object v13, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@16c
    const-string v14, ""

    #@16e
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@171
    move-result v13

    #@172
    if-eqz v13, :cond_156

    #@174
    .line 1665
    move-object/from16 v0, p0

    #@176
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@178
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@17b
    move-result-object v13

    #@17c
    invoke-virtual {v13, v4}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->remove(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    #@17f
    .line 1671
    .end local v4           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_17f
    move-object/from16 v0, p0

    #@181
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@183
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@186
    move-result-object v13

    #@187
    invoke-virtual {v13, v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->removeClient(Ljava/lang/String;)Z

    #@18a
    move-result v13

    #@18b
    if-eqz v13, :cond_236

    #@18d
    .line 1672
    move-object/from16 v0, p0

    #@18f
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@191
    new-instance v14, Ljava/lang/StringBuilder;

    #@193
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@196
    const-string v15, "Removed client "

    #@198
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v14

    #@19c
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v14

    #@1a0
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a3
    move-result-object v14

    #@1a4
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@1a7
    .line 1673
    move-object/from16 v0, p0

    #@1a9
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1ab
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1ae
    move-result-object v13

    #@1af
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->isClientListEmpty()Z

    #@1b2
    move-result v13

    #@1b3
    if-eqz v13, :cond_1cf

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1b9
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1bb
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@1be
    move-result-object v13

    #@1bf
    if-eqz v13, :cond_1cf

    #@1c1
    move-object/from16 v0, p0

    #@1c3
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1c5
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1c7
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@1ca
    move-result-object v13

    #@1cb
    const/4 v14, 0x0

    #@1cc
    invoke-interface {v13, v14}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setWifiP2pNotificationIcon(Z)V

    #@1cf
    .line 1674
    :cond_1cf
    move-object/from16 v0, p0

    #@1d1
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1d3
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1d5
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$5200(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@1d8
    move-result v13

    #@1d9
    if-nez v13, :cond_22e

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1df
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1e2
    move-result-object v13

    #@1e3
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->isClientListEmpty()Z

    #@1e6
    move-result v13

    #@1e7
    if-eqz v13, :cond_22e

    #@1e9
    .line 1675
    const-string v13, "WifiP2pService"

    #@1eb
    const-string v14, "Client list empty, remove non-persistent p2p group"

    #@1ed
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f0
    .line 1676
    move-object/from16 v0, p0

    #@1f2
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1f4
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@1f7
    move-result-object v13

    #@1f8
    move-object/from16 v0, p0

    #@1fa
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1fc
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1ff
    move-result-object v14

    #@200
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@203
    move-result-object v14

    #@204
    invoke-virtual {v13, v14}, Landroid/net/wifi/WifiNative;->p2pGroupRemove(Ljava/lang/String;)Z

    #@207
    .line 1689
    :cond_207
    :goto_207
    move-object/from16 v0, p0

    #@209
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@20b
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@20e
    .line 1690
    move-object/from16 v0, p0

    #@210
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@212
    new-instance v14, Ljava/lang/StringBuilder;

    #@214
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@217
    invoke-virtual/range {p0 .. p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@21a
    move-result-object v15

    #@21b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21e
    move-result-object v14

    #@21f
    const-string v15, " ap sta disconnected"

    #@221
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v14

    #@225
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@228
    move-result-object v14

    #@229
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@22c
    goto/16 :goto_117

    #@22e
    .line 1681
    :cond_22e
    move-object/from16 v0, p0

    #@230
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@232
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@235
    goto :goto_207

    #@236
    .line 1684
    :cond_236
    move-object/from16 v0, p0

    #@238
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@23a
    new-instance v14, Ljava/lang/StringBuilder;

    #@23c
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@23f
    const-string v15, "Failed to remove client "

    #@241
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@244
    move-result-object v14

    #@245
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v14

    #@249
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24c
    move-result-object v14

    #@24d
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@250
    .line 1685
    move-object/from16 v0, p0

    #@252
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@254
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@257
    move-result-object v13

    #@258
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    #@25b
    move-result-object v13

    #@25c
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@25f
    move-result-object v8

    #@260
    :goto_260
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@263
    move-result v13

    #@264
    if-eqz v13, :cond_207

    #@266
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@269
    move-result-object v1

    #@26a
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@26c
    .line 1686
    .local v1, c:Landroid/net/wifi/p2p/WifiP2pDevice;
    move-object/from16 v0, p0

    #@26e
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@270
    new-instance v14, Ljava/lang/StringBuilder;

    #@272
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@275
    const-string v15, "client "

    #@277
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v14

    #@27b
    iget-object v15, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@27d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v14

    #@281
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@284
    move-result-object v14

    #@285
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@288
    goto :goto_260

    #@289
    .line 1692
    .end local v1           #c:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v8           #i$:Ljava/util/Iterator;
    :cond_289
    move-object/from16 v0, p0

    #@28b
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@28d
    new-instance v14, Ljava/lang/StringBuilder;

    #@28f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@292
    const-string v15, "Disconnect on unknown device: "

    #@294
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@297
    move-result-object v14

    #@298
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v14

    #@29c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29f
    move-result-object v14

    #@2a0
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@2a3
    goto/16 :goto_117

    #@2a5
    .line 1696
    .end local v5           #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v6           #deviceAddress:Ljava/lang/String;
    :sswitch_2a5
    move-object/from16 v0, p1

    #@2a7
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a9
    check-cast v7, Landroid/net/DhcpInfoInternal;

    #@2ab
    .line 1697
    .local v7, dhcpInfo:Landroid/net/DhcpInfoInternal;
    move-object/from16 v0, p1

    #@2ad
    iget v13, v0, Landroid/os/Message;->arg1:I

    #@2af
    const/4 v14, 0x1

    #@2b0
    if-ne v13, v14, :cond_302

    #@2b2
    if-eqz v7, :cond_302

    #@2b4
    .line 1699
    move-object/from16 v0, p0

    #@2b6
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2b8
    new-instance v14, Ljava/lang/StringBuilder;

    #@2ba
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2bd
    const-string v15, "DhcpInfo: "

    #@2bf
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    move-result-object v14

    #@2c3
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c6
    move-result-object v14

    #@2c7
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ca
    move-result-object v14

    #@2cb
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@2ce
    .line 1700
    move-object/from16 v0, p0

    #@2d0
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2d2
    iget-object v14, v7, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@2d4
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@2d7
    .line 1701
    move-object/from16 v0, p0

    #@2d9
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2db
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@2de
    .line 1703
    move-object/from16 v0, p0

    #@2e0
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2e2
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@2e5
    move-result-object v13

    #@2e6
    move-object/from16 v0, p0

    #@2e8
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2ea
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@2ed
    move-result-object v14

    #@2ee
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@2f1
    move-result-object v14

    #@2f2
    const/4 v15, 0x1

    #@2f3
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/WifiNative;->setP2pPowerSave(Ljava/lang/String;Z)Z

    #@2f6
    .line 1705
    move-object/from16 v0, p0

    #@2f8
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2fa
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2fc
    const/4 v14, 0x0

    #@2fd
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService;->access$11202(Landroid/net/wifi/p2p/WifiP2pService;I)I

    #@300
    goto/16 :goto_117

    #@302
    .line 1706
    :cond_302
    move-object/from16 v0, p0

    #@304
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@306
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@308
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$11200(Landroid/net/wifi/p2p/WifiP2pService;)I

    #@30b
    move-result v13

    #@30c
    if-lez v13, :cond_3c4

    #@30e
    .line 1707
    move-object/from16 v0, p0

    #@310
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@312
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@314
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$11210(Landroid/net/wifi/p2p/WifiP2pService;)I

    #@317
    .line 1708
    move-object/from16 v0, p0

    #@319
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@31b
    new-instance v14, Ljava/lang/StringBuilder;

    #@31d
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@320
    const-string/jumbo v15, "stop DHCP client and restart DHCP client. counter="

    #@323
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@326
    move-result-object v14

    #@327
    move-object/from16 v0, p0

    #@329
    iget-object v15, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@32b
    iget-object v15, v15, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@32d
    invoke-static {v15}, Landroid/net/wifi/p2p/WifiP2pService;->access$11200(Landroid/net/wifi/p2p/WifiP2pService;)I

    #@330
    move-result v15

    #@331
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@334
    move-result-object v14

    #@335
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@338
    move-result-object v14

    #@339
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@33c
    .line 1710
    move-object/from16 v0, p0

    #@33e
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@340
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@343
    move-result-object v13

    #@344
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    #@347
    move-result v13

    #@348
    if-nez v13, :cond_3b8

    #@34a
    move-object/from16 v0, p0

    #@34c
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@34e
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@350
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;

    #@353
    move-result-object v13

    #@354
    if-eqz v13, :cond_3b8

    #@356
    .line 1711
    move-object/from16 v0, p0

    #@358
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@35a
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@35c
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;

    #@35f
    move-result-object v13

    #@360
    const v14, 0x30002

    #@363
    invoke-virtual {v13, v14}, Landroid/net/DhcpStateMachine;->sendMessage(I)V

    #@366
    .line 1712
    move-object/from16 v0, p0

    #@368
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@36a
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@36c
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;

    #@36f
    move-result-object v13

    #@370
    invoke-virtual {v13}, Landroid/net/DhcpStateMachine;->doQuit()V

    #@373
    .line 1713
    move-object/from16 v0, p0

    #@375
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@377
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@379
    const/4 v14, 0x0

    #@37a
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService;->access$8902(Landroid/net/wifi/p2p/WifiP2pService;Landroid/net/DhcpStateMachine;)Landroid/net/DhcpStateMachine;

    #@37d
    .line 1715
    move-object/from16 v0, p0

    #@37f
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@381
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@383
    move-object/from16 v0, p0

    #@385
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@387
    iget-object v14, v14, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@389
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@38c
    move-result-object v14

    #@38d
    move-object/from16 v0, p0

    #@38f
    iget-object v15, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@391
    move-object/from16 v0, p0

    #@393
    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@395
    move-object/from16 v16, v0

    #@397
    invoke-static/range {v16 .. v16}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@39a
    move-result-object v16

    #@39b
    invoke-virtual/range {v16 .. v16}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@39e
    move-result-object v16

    #@39f
    invoke-static/range {v14 .. v16}, Landroid/net/DhcpStateMachine;->makeDhcpStateMachine(Landroid/content/Context;Lcom/android/internal/util/StateMachine;Ljava/lang/String;)Landroid/net/DhcpStateMachine;

    #@3a2
    move-result-object v14

    #@3a3
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService;->access$8902(Landroid/net/wifi/p2p/WifiP2pService;Landroid/net/DhcpStateMachine;)Landroid/net/DhcpStateMachine;

    #@3a6
    .line 1717
    move-object/from16 v0, p0

    #@3a8
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3aa
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3ac
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;

    #@3af
    move-result-object v13

    #@3b0
    const v14, 0x30001

    #@3b3
    invoke-virtual {v13, v14}, Landroid/net/DhcpStateMachine;->sendMessage(I)V

    #@3b6
    goto/16 :goto_117

    #@3b8
    .line 1719
    :cond_3b8
    move-object/from16 v0, p0

    #@3ba
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3bc
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3be
    const/4 v14, 0x0

    #@3bf
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService;->access$11202(Landroid/net/wifi/p2p/WifiP2pService;I)I

    #@3c2
    goto/16 :goto_117

    #@3c4
    .line 1723
    :cond_3c4
    move-object/from16 v0, p0

    #@3c6
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3c8
    const-string v14, "DHCP failed"

    #@3ca
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@3cd
    .line 1724
    move-object/from16 v0, p0

    #@3cf
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3d1
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@3d4
    move-result-object v13

    #@3d5
    move-object/from16 v0, p0

    #@3d7
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3d9
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@3dc
    move-result-object v14

    #@3dd
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@3e0
    move-result-object v14

    #@3e1
    invoke-virtual {v13, v14}, Landroid/net/wifi/WifiNative;->p2pGroupRemove(Ljava/lang/String;)Z

    #@3e4
    goto/16 :goto_117

    #@3e6
    .line 1728
    .end local v7           #dhcpInfo:Landroid/net/DhcpInfoInternal;
    :sswitch_3e6
    move-object/from16 v0, p0

    #@3e8
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@3ea
    new-instance v14, Ljava/lang/StringBuilder;

    #@3ec
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@3ef
    invoke-virtual/range {p0 .. p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@3f2
    move-result-object v15

    #@3f3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f6
    move-result-object v14

    #@3f7
    const-string v15, " remove group"

    #@3f9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fc
    move-result-object v14

    #@3fd
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@400
    move-result-object v14

    #@401
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@404
    .line 1729
    move-object/from16 v0, p0

    #@406
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@408
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@40b
    move-result-object v13

    #@40c
    move-object/from16 v0, p0

    #@40e
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@410
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@413
    move-result-object v14

    #@414
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@417
    move-result-object v14

    #@418
    invoke-virtual {v13, v14}, Landroid/net/wifi/WifiNative;->p2pGroupRemove(Ljava/lang/String;)Z

    #@41b
    move-result v13

    #@41c
    if-eqz v13, :cond_43b

    #@41e
    .line 1730
    move-object/from16 v0, p0

    #@420
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@422
    move-object/from16 v0, p0

    #@424
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@426
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;

    #@429
    move-result-object v14

    #@42a
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V

    #@42d
    .line 1731
    move-object/from16 v0, p0

    #@42f
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@431
    const v14, 0x22012

    #@434
    move-object/from16 v0, p1

    #@436
    invoke-static {v13, v0, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;I)V

    #@439
    goto/16 :goto_117

    #@43b
    .line 1733
    :cond_43b
    move-object/from16 v0, p0

    #@43d
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@43f
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@442
    .line 1734
    move-object/from16 v0, p0

    #@444
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@446
    move-object/from16 v0, p0

    #@448
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@44a
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$2600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

    #@44d
    move-result-object v14

    #@44e
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V

    #@451
    .line 1735
    move-object/from16 v0, p0

    #@453
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@455
    const v14, 0x22011

    #@458
    const/4 v15, 0x0

    #@459
    move-object/from16 v0, p1

    #@45b
    invoke-static {v13, v0, v14, v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@45e
    goto/16 :goto_117

    #@460
    .line 1751
    :sswitch_460
    move-object/from16 v0, p0

    #@462
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@464
    new-instance v14, Ljava/lang/StringBuilder;

    #@466
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@469
    invoke-virtual/range {p0 .. p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->getName()Ljava/lang/String;

    #@46c
    move-result-object v15

    #@46d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@470
    move-result-object v14

    #@471
    const-string v15, " group removed"

    #@473
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@476
    move-result-object v14

    #@477
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47a
    move-result-object v14

    #@47b
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@47e
    .line 1752
    move-object/from16 v0, p0

    #@480
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@482
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@485
    .line 1753
    move-object/from16 v0, p0

    #@487
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@489
    move-object/from16 v0, p0

    #@48b
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@48d
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$2600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

    #@490
    move-result-object v14

    #@491
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V

    #@494
    goto/16 :goto_117

    #@496
    .line 1756
    :sswitch_496
    move-object/from16 v0, p1

    #@498
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@49a
    check-cast v5, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@49c
    .line 1758
    .restart local v5       #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    move-object/from16 v0, p0

    #@49e
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4a0
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@4a3
    move-result-object v13

    #@4a4
    invoke-virtual {v13, v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->contains(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    #@4a7
    move-result v13

    #@4a8
    if-eqz v13, :cond_4d2

    #@4aa
    .line 1759
    move-object/from16 v0, p0

    #@4ac
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4ae
    new-instance v14, Ljava/lang/StringBuilder;

    #@4b0
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@4b3
    const-string v15, "Add device to lost list "

    #@4b5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b8
    move-result-object v14

    #@4b9
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4bc
    move-result-object v14

    #@4bd
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c0
    move-result-object v14

    #@4c1
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@4c4
    .line 1760
    move-object/from16 v0, p0

    #@4c6
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4c8
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$7200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@4cb
    move-result-object v13

    #@4cc
    invoke-virtual {v13, v5}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->update(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@4cf
    .line 1761
    const/4 v13, 0x1

    #@4d0
    goto/16 :goto_28

    #@4d2
    .line 1764
    :cond_4d2
    move-object/from16 v0, p0

    #@4d4
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4d6
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@4d9
    move-result-object v13

    #@4da
    if-eqz v13, :cond_52c

    #@4dc
    move-object/from16 v0, p0

    #@4de
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4e0
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@4e3
    move-result-object v13

    #@4e4
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@4e6
    iget-object v14, v5, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@4e8
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4eb
    move-result v13

    #@4ec
    if-eqz v13, :cond_52c

    #@4ee
    .line 1765
    move-object/from16 v0, p0

    #@4f0
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4f2
    new-instance v14, Ljava/lang/StringBuilder;

    #@4f4
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@4f7
    const-string v15, "[LGE_PATCH]GroupCreatedState mSavedProvDiscDevice:"

    #@4f9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fc
    move-result-object v14

    #@4fd
    move-object/from16 v0, p0

    #@4ff
    iget-object v15, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@501
    invoke-static {v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@504
    move-result-object v15

    #@505
    iget-object v15, v15, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@507
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50a
    move-result-object v14

    #@50b
    const-string v15, " lost:"

    #@50d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@510
    move-result-object v14

    #@511
    iget-object v15, v5, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@513
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@516
    move-result-object v14

    #@517
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51a
    move-result-object v14

    #@51b
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@51e
    .line 1766
    move-object/from16 v0, p0

    #@520
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@522
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$7200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@525
    move-result-object v13

    #@526
    invoke-virtual {v13, v5}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->update(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@529
    .line 1767
    const/4 v13, 0x1

    #@52a
    goto/16 :goto_28

    #@52c
    .line 1771
    :cond_52c
    const/4 v13, 0x0

    #@52d
    goto/16 :goto_28

    #@52f
    .line 1773
    .end local v5           #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    :sswitch_52f
    move-object/from16 v0, p0

    #@531
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@533
    const v14, 0x22010

    #@536
    invoke-virtual {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendMessage(I)V

    #@539
    .line 1774
    move-object/from16 v0, p0

    #@53b
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@53d
    move-object/from16 v0, p1

    #@53f
    invoke-static {v13, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;)V

    #@542
    goto/16 :goto_117

    #@544
    .line 1778
    :sswitch_544
    move-object/from16 v0, p0

    #@546
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@548
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@54a
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@54d
    move-result-object v13

    #@54e
    invoke-interface {v13}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getBtWifiState()Z

    #@551
    move-result v13

    #@552
    if-eqz v13, :cond_55d

    #@554
    .line 1779
    move-object/from16 v0, p0

    #@556
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@558
    const-string v14, "BT and WiFi are connected."

    #@55a
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@55d
    .line 1782
    :cond_55d
    move-object/from16 v0, p1

    #@55f
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@561
    check-cast v2, Landroid/net/wifi/p2p/WifiP2pConfig;

    #@563
    .line 1783
    .local v2, config:Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v13, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@565
    if-eqz v13, :cond_583

    #@567
    move-object/from16 v0, p0

    #@569
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@56b
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@56e
    move-result-object v13

    #@56f
    if-eqz v13, :cond_622

    #@571
    move-object/from16 v0, p0

    #@573
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@575
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@578
    move-result-object v13

    #@579
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@57b
    iget-object v14, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@57d
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@580
    move-result v13

    #@581
    if-eqz v13, :cond_622

    #@583
    .line 1787
    :cond_583
    iget-object v13, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@585
    iget v13, v13, Landroid/net/wifi/WpsInfo;->setup:I

    #@587
    if-nez v13, :cond_5c8

    #@589
    .line 1788
    move-object/from16 v0, p0

    #@58b
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@58d
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@590
    move-result-object v13

    #@591
    move-object/from16 v0, p0

    #@593
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@595
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@598
    move-result-object v14

    #@599
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@59c
    move-result-object v14

    #@59d
    const/4 v15, 0x0

    #@59e
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/WifiNative;->startWpsPbc(Ljava/lang/String;Ljava/lang/String;)Z

    #@5a1
    .line 1807
    :cond_5a1
    :goto_5a1
    iget-object v13, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@5a3
    if-eqz v13, :cond_5ba

    #@5a5
    .line 1808
    move-object/from16 v0, p0

    #@5a7
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5a9
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@5ac
    move-result-object v13

    #@5ad
    iget-object v14, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@5af
    const/4 v15, 0x1

    #@5b0
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->updateStatus(Ljava/lang/String;I)V

    #@5b3
    .line 1809
    move-object/from16 v0, p0

    #@5b5
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5b7
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@5ba
    .line 1811
    :cond_5ba
    move-object/from16 v0, p0

    #@5bc
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5be
    const v14, 0x22009

    #@5c1
    move-object/from16 v0, p1

    #@5c3
    invoke-static {v13, v0, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;I)V

    #@5c6
    goto/16 :goto_117

    #@5c8
    .line 1790
    :cond_5c8
    iget-object v13, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@5ca
    iget-object v13, v13, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@5cc
    if-nez v13, :cond_606

    #@5ce
    .line 1791
    move-object/from16 v0, p0

    #@5d0
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5d2
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@5d5
    move-result-object v13

    #@5d6
    move-object/from16 v0, p0

    #@5d8
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5da
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@5dd
    move-result-object v14

    #@5de
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@5e1
    move-result-object v14

    #@5e2
    invoke-virtual {v13, v14}, Landroid/net/wifi/WifiNative;->startWpsPinDisplay(Ljava/lang/String;)Ljava/lang/String;

    #@5e5
    move-result-object v10

    #@5e6
    .line 1793
    .local v10, pin:Ljava/lang/String;
    :try_start_5e6
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5e9
    .line 1794
    move-object/from16 v0, p0

    #@5eb
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5ed
    invoke-static {v13, v10}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$8200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)Z

    #@5f0
    move-result v13

    #@5f1
    if-nez v13, :cond_5a1

    #@5f3
    .line 1795
    move-object/from16 v0, p0

    #@5f5
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@5f7
    iget-object v13, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@5f9
    if-eqz v13, :cond_603

    #@5fb
    iget-object v13, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@5fd
    :goto_5fd
    invoke-static {v14, v10, v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$8300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;Ljava/lang/String;)V

    #@600
    goto :goto_5a1

    #@601
    .line 1799
    :catch_601
    move-exception v13

    #@602
    goto :goto_5a1

    #@603
    .line 1795
    :cond_603
    const-string v13, "any"
    :try_end_605
    .catch Ljava/lang/NumberFormatException; {:try_start_5e6 .. :try_end_605} :catch_601

    #@605
    goto :goto_5fd

    #@606
    .line 1803
    .end local v10           #pin:Ljava/lang/String;
    :cond_606
    move-object/from16 v0, p0

    #@608
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@60a
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@60d
    move-result-object v13

    #@60e
    move-object/from16 v0, p0

    #@610
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@612
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@615
    move-result-object v14

    #@616
    invoke-virtual {v14}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@619
    move-result-object v14

    #@61a
    iget-object v15, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@61c
    iget-object v15, v15, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@61e
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/WifiNative;->startWpsPinKeypad(Ljava/lang/String;Ljava/lang/String;)Z

    #@621
    goto :goto_5a1

    #@622
    .line 1813
    :cond_622
    move-object/from16 v0, p0

    #@624
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@626
    new-instance v14, Ljava/lang/StringBuilder;

    #@628
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@62b
    const-string v15, "Inviting device : "

    #@62d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@630
    move-result-object v14

    #@631
    iget-object v15, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@633
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@636
    move-result-object v14

    #@637
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63a
    move-result-object v14

    #@63b
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@63e
    .line 1814
    move-object/from16 v0, p0

    #@640
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@642
    invoke-static {v13, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5502(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pConfig;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@645
    .line 1815
    move-object/from16 v0, p0

    #@647
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@649
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@64c
    move-result-object v13

    #@64d
    move-object/from16 v0, p0

    #@64f
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@651
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@654
    move-result-object v14

    #@655
    iget-object v15, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@657
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/WifiNative;->p2pInvite(Landroid/net/wifi/p2p/WifiP2pGroup;Ljava/lang/String;)Z

    #@65a
    move-result v13

    #@65b
    if-eqz v13, :cond_680

    #@65d
    .line 1816
    move-object/from16 v0, p0

    #@65f
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@661
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@664
    move-result-object v13

    #@665
    iget-object v14, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@667
    const/4 v15, 0x1

    #@668
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->updateStatus(Ljava/lang/String;I)V

    #@66b
    .line 1817
    move-object/from16 v0, p0

    #@66d
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@66f
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@672
    .line 1818
    move-object/from16 v0, p0

    #@674
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@676
    const v14, 0x22009

    #@679
    move-object/from16 v0, p1

    #@67b
    invoke-static {v13, v0, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;I)V

    #@67e
    goto/16 :goto_117

    #@680
    .line 1820
    :cond_680
    move-object/from16 v0, p0

    #@682
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@684
    const v14, 0x22008

    #@687
    const/4 v15, 0x0

    #@688
    move-object/from16 v0, p1

    #@68a
    invoke-static {v13, v0, v14, v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@68d
    goto/16 :goto_117

    #@68f
    .line 1827
    .end local v2           #config:Landroid/net/wifi/p2p/WifiP2pConfig;
    :sswitch_68f
    move-object/from16 v0, p1

    #@691
    iget-object v12, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@693
    check-cast v12, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@695
    .line 1828
    .local v12, status:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    move-object/from16 v0, p0

    #@697
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@699
    new-instance v14, Ljava/lang/StringBuilder;

    #@69b
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@69e
    const-string v15, "===> INVITATION RESULT EVENT : "

    #@6a0
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a3
    move-result-object v14

    #@6a4
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6a7
    move-result-object v14

    #@6a8
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6ab
    move-result-object v14

    #@6ac
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@6af
    .line 1829
    sget-object v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->SUCCESS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@6b1
    if-eq v12, v13, :cond_117

    #@6b3
    .line 1832
    sget-object v13, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN_P2P_GROUP:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@6b5
    if-ne v12, v13, :cond_117

    #@6b7
    .line 1835
    move-object/from16 v0, p0

    #@6b9
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6bb
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@6be
    move-result-object v13

    #@6bf
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@6c2
    move-result v9

    #@6c3
    .line 1836
    .local v9, netId:I
    if-ltz v9, :cond_117

    #@6c5
    .line 1837
    move-object/from16 v0, p0

    #@6c7
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6c9
    const-string v14, "Remove unknown client from the list"

    #@6cb
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@6ce
    .line 1838
    move-object/from16 v0, p0

    #@6d0
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6d2
    move-object/from16 v0, p0

    #@6d4
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6d6
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@6d9
    move-result-object v14

    #@6da
    iget-object v14, v14, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@6dc
    const/4 v15, 0x0

    #@6dd
    invoke-static {v13, v9, v14, v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$9700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;ILjava/lang/String;Z)Z

    #@6e0
    move-result v13

    #@6e1
    if-nez v13, :cond_6ec

    #@6e3
    .line 1841
    const-string v13, "WifiP2pService"

    #@6e5
    const-string v14, "Already removed the client, ignore"

    #@6e7
    invoke-static {v13, v14}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6ea
    goto/16 :goto_117

    #@6ec
    .line 1845
    :cond_6ec
    move-object/from16 v0, p0

    #@6ee
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6f0
    const v14, 0x22007

    #@6f3
    move-object/from16 v0, p0

    #@6f5
    iget-object v15, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6f7
    invoke-static {v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@6fa
    move-result-object v15

    #@6fb
    invoke-virtual {v13, v14, v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@6fe
    goto/16 :goto_117

    #@700
    .line 1852
    .end local v9           #netId:I
    .end local v12           #status:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    :sswitch_700
    move-object/from16 v0, p1

    #@702
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@704
    check-cast v11, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;

    #@706
    .line 1853
    .local v11, provDisc:Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;
    move-object/from16 v0, p0

    #@708
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@70a
    iget-object v14, v11, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;->device:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@70c
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11302(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@70f
    .line 1854
    move-object/from16 v0, p0

    #@711
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@713
    new-instance v14, Landroid/net/wifi/p2p/WifiP2pConfig;

    #@715
    invoke-direct {v14}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    #@718
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5502(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pConfig;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@71b
    .line 1855
    move-object/from16 v0, p0

    #@71d
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@71f
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@722
    move-result-object v13

    #@723
    iget-object v14, v11, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;->device:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@725
    iget-object v14, v14, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@727
    iput-object v14, v13, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@729
    .line 1856
    move-object/from16 v0, p1

    #@72b
    iget v13, v0, Landroid/os/Message;->what:I

    #@72d
    const v14, 0x24023

    #@730
    if-ne v13, v14, :cond_7a4

    #@732
    .line 1857
    move-object/from16 v0, p0

    #@734
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@736
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@739
    move-result-object v13

    #@73a
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@73c
    const/4 v14, 0x2

    #@73d
    iput v14, v13, Landroid/net/wifi/WpsInfo;->setup:I

    #@73f
    .line 1864
    :goto_73f
    const-string v13, "WifiP2pService"

    #@741
    new-instance v14, Ljava/lang/StringBuilder;

    #@743
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@746
    const-string/jumbo v15, "mGroup.isGroupOwner()"

    #@749
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74c
    move-result-object v14

    #@74d
    move-object/from16 v0, p0

    #@74f
    iget-object v15, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@751
    invoke-static {v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@754
    move-result-object v15

    #@755
    invoke-virtual {v15}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    #@758
    move-result v15

    #@759
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@75c
    move-result-object v14

    #@75d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@760
    move-result-object v14

    #@761
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@764
    .line 1865
    move-object/from16 v0, p0

    #@766
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@768
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@76b
    move-result-object v13

    #@76c
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    #@76f
    move-result v13

    #@770
    if-eqz v13, :cond_117

    #@772
    .line 1866
    const-string v13, "WifiP2pService"

    #@774
    const-string v14, "Local device is Group Owner, transiting to mUserAuthorizingJoinState"

    #@776
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@779
    .line 1867
    move-object/from16 v0, p0

    #@77b
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@77d
    move-object/from16 v0, p0

    #@77f
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@781
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@784
    move-result-object v14

    #@785
    move-object/from16 v0, p0

    #@787
    iget-object v15, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@789
    invoke-static {v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@78c
    move-result-object v15

    #@78d
    invoke-static {v13, v14, v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$6200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)Z

    #@790
    move-result v13

    #@791
    if-nez v13, :cond_117

    #@793
    .line 1868
    move-object/from16 v0, p0

    #@795
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@797
    move-object/from16 v0, p0

    #@799
    iget-object v14, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@79b
    invoke-static {v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$12000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;

    #@79e
    move-result-object v14

    #@79f
    invoke-static {v13, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$12100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V

    #@7a2
    goto/16 :goto_117

    #@7a4
    .line 1858
    :cond_7a4
    move-object/from16 v0, p1

    #@7a6
    iget v13, v0, Landroid/os/Message;->what:I

    #@7a8
    const v14, 0x24024

    #@7ab
    if-ne v13, v14, :cond_7ca

    #@7ad
    .line 1859
    move-object/from16 v0, p0

    #@7af
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@7b1
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@7b4
    move-result-object v13

    #@7b5
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@7b7
    const/4 v14, 0x1

    #@7b8
    iput v14, v13, Landroid/net/wifi/WpsInfo;->setup:I

    #@7ba
    .line 1860
    move-object/from16 v0, p0

    #@7bc
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@7be
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@7c1
    move-result-object v13

    #@7c2
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@7c4
    iget-object v14, v11, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;->pin:Ljava/lang/String;

    #@7c6
    iput-object v14, v13, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@7c8
    goto/16 :goto_73f

    #@7ca
    .line 1862
    :cond_7ca
    move-object/from16 v0, p0

    #@7cc
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@7ce
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;

    #@7d1
    move-result-object v13

    #@7d2
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@7d4
    const/4 v14, 0x0

    #@7d5
    iput v14, v13, Landroid/net/wifi/WpsInfo;->setup:I

    #@7d7
    goto/16 :goto_73f

    #@7d9
    .line 1873
    .end local v11           #provDisc:Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;
    :sswitch_7d9
    const-string v13, "WifiP2pService"

    #@7db
    const-string v14, "Duplicate group creation event notice, ignore"

    #@7dd
    invoke-static {v13, v14}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e0
    goto/16 :goto_117

    #@7e2
    .line 1877
    :sswitch_7e2
    const/4 v3, 0x0

    #@7e3
    .line 1878
    .local v3, connected:I
    move-object/from16 v0, p0

    #@7e5
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@7e7
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@7ea
    move-result-object v13

    #@7eb
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@7ee
    move-result-object v13

    #@7ef
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@7f2
    move-result-object v8

    #@7f3
    .restart local v8       #i$:Ljava/util/Iterator;
    :cond_7f3
    :goto_7f3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@7f6
    move-result v13

    #@7f7
    if-eqz v13, :cond_807

    #@7f9
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7fc
    move-result-object v4

    #@7fd
    check-cast v4, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@7ff
    .line 1879
    .restart local v4       #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget v13, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@801
    const/4 v14, 0x1

    #@802
    if-gt v13, v14, :cond_7f3

    #@804
    add-int/lit8 v3, v3, 0x1

    #@806
    goto :goto_7f3

    #@807
    .line 1881
    .end local v4           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_807
    const-string v13, "WifiP2pService"

    #@809
    new-instance v14, Ljava/lang/StringBuilder;

    #@80b
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@80e
    const-string v15, "[LGE_WLAN]DISCOVER_PEERS GroupCreatedState connected:"

    #@810
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@813
    move-result-object v14

    #@814
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@817
    move-result-object v14

    #@818
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81b
    move-result-object v14

    #@81c
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81f
    .line 1882
    if-nez v3, :cond_824

    #@821
    .line 1883
    const/4 v13, 0x0

    #@822
    goto/16 :goto_28

    #@824
    .line 1885
    :cond_824
    move-object/from16 v0, p0

    #@826
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@828
    const v14, 0x22002

    #@82b
    const/4 v15, 0x2

    #@82c
    move-object/from16 v0, p1

    #@82e
    invoke-static {v13, v0, v14, v15}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@831
    .line 1886
    move-object/from16 v0, p0

    #@833
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@835
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@838
    goto/16 :goto_117

    #@83a
    .line 1891
    .end local v3           #connected:I
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_83a
    move-object/from16 v0, p0

    #@83c
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@83e
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@841
    move-result-object v13

    #@842
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@845
    move-result-object v13

    #@846
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@849
    move-result-object v8

    #@84a
    .restart local v8       #i$:Ljava/util/Iterator;
    :cond_84a
    :goto_84a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@84d
    move-result v13

    #@84e
    if-eqz v13, :cond_871

    #@850
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@853
    move-result-object v4

    #@854
    check-cast v4, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@856
    .line 1892
    .restart local v4       #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget v13, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@858
    const/4 v14, 0x1

    #@859
    if-ne v13, v14, :cond_84a

    #@85b
    .line 1893
    const/4 v13, 0x2

    #@85c
    iput v13, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@85e
    .line 1894
    move-object/from16 v0, p0

    #@860
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@862
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@865
    .line 1895
    move-object/from16 v0, p0

    #@867
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@869
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@86c
    move-result-object v13

    #@86d
    invoke-virtual {v13}, Landroid/net/wifi/WifiNative;->cancelWps()Z

    #@870
    goto :goto_84a

    #@871
    .line 1898
    .end local v4           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_871
    move-object/from16 v0, p0

    #@873
    iget-object v13, v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@875
    const v14, 0x2200c

    #@878
    move-object/from16 v0, p1

    #@87a
    invoke-static {v13, v0, v14}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;I)V

    #@87d
    goto/16 :goto_117

    #@87f
    .line 1626
    nop

    #@880
    :sswitch_data_880
    .sparse-switch
        0x20084 -> :sswitch_52f
        0x22001 -> :sswitch_7e2
        0x22007 -> :sswitch_544
        0x2200a -> :sswitch_83a
        0x22010 -> :sswitch_3e6
        0x24016 -> :sswitch_496
        0x2401d -> :sswitch_7d9
        0x2401e -> :sswitch_460
        0x24020 -> :sswitch_68f
        0x24021 -> :sswitch_700
        0x24023 -> :sswitch_700
        0x24024 -> :sswitch_700
        0x24029 -> :sswitch_130
        0x2402a -> :sswitch_29
        0x2402b -> :sswitch_29
        0x30005 -> :sswitch_2a5
    .end sparse-switch
.end method
