.class public Landroid/net/wifi/p2p/WifiP2pGroupList;
.super Ljava/lang/Object;
.source "WifiP2pGroupList.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pGroupList;",
            ">;"
        }
    .end annotation
.end field

.field private static final CREDENTIAL_MAX_NUM:I = 0x20


# instance fields
.field private isClearCalled:Z

.field private final mGroups:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/net/wifi/p2p/WifiP2pGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final mListener:Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 221
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pGroupList$2;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pGroupList$2;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pGroupList;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 46
    invoke-direct {p0, v0, v0}, Landroid/net/wifi/p2p/WifiP2pGroupList;-><init>(Landroid/net/wifi/p2p/WifiP2pGroupList;Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;)V

    #@4
    .line 47
    return-void
.end method

.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pGroupList;Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;)V
    .registers 8
    .parameter "source"
    .parameter "listener"

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    const/4 v2, 0x0

    #@4
    iput-boolean v2, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->isClearCalled:Z

    #@6
    .line 50
    iput-object p2, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mListener:Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;

    #@8
    .line 51
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pGroupList$1;

    #@a
    const/16 v3, 0x20

    #@c
    invoke-direct {v2, p0, v3}, Landroid/net/wifi/p2p/WifiP2pGroupList$1;-><init>(Landroid/net/wifi/p2p/WifiP2pGroupList;I)V

    #@f
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@11
    .line 61
    if-eqz p1, :cond_3b

    #@13
    .line 62
    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@15
    invoke-virtual {v2}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@18
    move-result-object v2

    #@19
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@1c
    move-result-object v2

    #@1d
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v0

    #@21
    .local v0, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_3b

    #@27
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v1

    #@2b
    check-cast v1, Ljava/util/Map$Entry;

    #@2d
    .line 63
    .local v1, item:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2f
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@32
    move-result-object v3

    #@33
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v2, v3, v4}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    goto :goto_21

    #@3b
    .line 66
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #item:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    :cond_3b
    return-void
.end method

.method static synthetic access$000(Landroid/net/wifi/p2p/WifiP2pGroupList;)Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mListener:Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/wifi/p2p/WifiP2pGroupList;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->isClearCalled:Z

    #@2
    return v0
.end method


# virtual methods
.method add(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .registers 4
    .parameter "group"

    #@0
    .prologue
    .line 83
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@5
    move-result v1

    #@6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 84
    return-void
.end method

.method clear()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 108
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@4
    invoke-virtual {v2}, Landroid/util/LruCache;->size()I

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_b

    #@a
    .line 112
    :goto_a
    return v0

    #@b
    .line 109
    :cond_b
    iput-boolean v1, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->isClearCalled:Z

    #@d
    .line 110
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@f
    invoke-virtual {v2}, Landroid/util/LruCache;->evictAll()V

    #@12
    .line 111
    iput-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->isClearCalled:Z

    #@14
    move v0, v1

    #@15
    .line 112
    goto :goto_a
.end method

.method contains(I)Z
    .registers 6
    .parameter "netId"

    #@0
    .prologue
    .line 187
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2
    invoke-virtual {v3}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@9
    move-result-object v0

    #@a
    .line 188
    .local v0, groups:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_22

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1a
    .line 189
    .local v1, grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@1d
    move-result v3

    #@1e
    if-ne p1, v3, :cond_e

    #@20
    .line 190
    const/4 v3, 0x1

    #@21
    .line 193
    .end local v1           #grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    :goto_21
    return v3

    #@22
    :cond_22
    const/4 v3, 0x0

    #@23
    goto :goto_21
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 208
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getGroupList()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pGroup;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2
    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method getNetworkId(Ljava/lang/String;)I
    .registers 7
    .parameter "deviceAddress"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 125
    if-nez p1, :cond_4

    #@3
    .line 135
    :cond_3
    :goto_3
    return v3

    #@4
    .line 127
    :cond_4
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@6
    invoke-virtual {v4}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@9
    move-result-object v4

    #@a
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@d
    move-result-object v0

    #@e
    .line 128
    .local v0, groups:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v2

    #@12
    .local v2, i$:Ljava/util/Iterator;
    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_3

    #@18
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1e
    .line 129
    .local v1, grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    #@21
    move-result-object v4

    #@22
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@24
    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_12

    #@2a
    .line 131
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2c
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@2f
    move-result v4

    #@30
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v3, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    .line 132
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@3a
    move-result v3

    #@3b
    goto :goto_3
.end method

.method getNetworkId(Ljava/lang/String;Ljava/lang/String;)I
    .registers 8
    .parameter "deviceAddress"
    .parameter "ssid"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 147
    if-eqz p1, :cond_5

    #@3
    if-nez p2, :cond_6

    #@5
    .line 161
    :cond_5
    :goto_5
    return v3

    #@6
    .line 151
    :cond_6
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@8
    invoke-virtual {v4}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@b
    move-result-object v4

    #@c
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@f
    move-result-object v0

    #@10
    .line 152
    .local v0, groups:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v2

    #@14
    .local v2, i$:Ljava/util/Iterator;
    :cond_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_5

    #@1a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@20
    .line 153
    .local v1, grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    #@23
    move-result-object v4

    #@24
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@26
    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_14

    #@2c
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkName()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_14

    #@36
    .line 156
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@38
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@3b
    move-result v4

    #@3c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v3, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    .line 157
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@46
    move-result v3

    #@47
    goto :goto_5
.end method

.method getOwnerAddr(I)Ljava/lang/String;
    .registers 5
    .parameter "netId"

    #@0
    .prologue
    .line 171
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@c
    .line 172
    .local v0, grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    if-eqz v0, :cond_15

    #@e
    .line 173
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    #@11
    move-result-object v1

    #@12
    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@14
    .line 175
    :goto_14
    return-object v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method remove(I)V
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 93
    return-void
.end method

.method remove(Ljava/lang/String;)V
    .registers 3
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 101
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pGroupList;->getNetworkId(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/wifi/p2p/WifiP2pGroupList;->remove(I)V

    #@7
    .line 102
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 197
    new-instance v3, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 199
    .local v3, sbuf:Ljava/lang/StringBuffer;
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@7
    invoke-virtual {v4}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@a
    move-result-object v4

    #@b
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@e
    move-result-object v0

    #@f
    .line 200
    .local v0, groups:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v2

    #@13
    .local v2, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_29

    #@19
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1f
    .line 201
    .local v1, grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@22
    move-result-object v4

    #@23
    const-string v5, "\n"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@28
    goto :goto_13

    #@29
    .line 203
    .end local v1           #grp:Landroid/net/wifi/p2p/WifiP2pGroup;
    :cond_29
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 213
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pGroupList;->mGroups:Landroid/util/LruCache;

    #@2
    invoke-virtual {v3}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@9
    move-result-object v1

    #@a
    .line 214
    .local v1, groups:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@d
    move-result v3

    #@e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 215
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v2

    #@15
    .local v2, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_25

    #@1b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@21
    .line 216
    .local v0, group:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@24
    goto :goto_15

    #@25
    .line 218
    .end local v0           #group:Landroid/net/wifi/p2p/WifiP2pGroup;
    :cond_25
    return-void
.end method
