.class Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;
.super Landroid/os/Handler;
.source "WifiP2pManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pManager$Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "P2pHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;


# direct methods
.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 722
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@2
    .line 723
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 724
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "message"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 728
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@3
    iget v8, p1, Landroid/os/Message;->arg2:I

    #@5
    invoke-static {v7, v8}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$000(Landroid/net/wifi/p2p/WifiP2pManager$Channel;I)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 729
    .local v3, listener:Ljava/lang/Object;
    iget v7, p1, Landroid/os/Message;->what:I

    #@b
    sparse-switch v7, :sswitch_data_fe

    #@e
    .line 836
    const-string v7, "WifiP2pManager"

    #@10
    new-instance v8, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v9, "Ignored "

    #@17
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v8

    #@23
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 839
    .end local v3           #listener:Ljava/lang/Object;
    :cond_26
    :goto_26
    return-void

    #@27
    .line 731
    .restart local v3       #listener:Ljava/lang/Object;
    :sswitch_27
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@29
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$100(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;

    #@2c
    move-result-object v7

    #@2d
    if-eqz v7, :cond_26

    #@2f
    .line 732
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@31
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$100(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;

    #@34
    move-result-object v7

    #@35
    invoke-interface {v7}, Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;->onChannelDisconnected()V

    #@38
    .line 733
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@3a
    invoke-static {v7, v9}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$102(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;

    #@3d
    goto :goto_26

    #@3e
    .line 753
    :sswitch_3e
    if-eqz v3, :cond_26

    #@40
    .line 754
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    #@42
    .end local v3           #listener:Ljava/lang/Object;
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@44
    invoke-interface {v3, v7}, Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;->onFailure(I)V

    #@47
    goto :goto_26

    #@48
    .line 774
    .restart local v3       #listener:Ljava/lang/Object;
    :sswitch_48
    if-eqz v3, :cond_26

    #@4a
    .line 775
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    #@4c
    .end local v3           #listener:Ljava/lang/Object;
    invoke-interface {v3}, Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;->onSuccess()V

    #@4f
    goto :goto_26

    #@50
    .line 779
    .restart local v3       #listener:Ljava/lang/Object;
    :sswitch_50
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@52
    check-cast v4, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@54
    .line 780
    .local v4, peers:Landroid/net/wifi/p2p/WifiP2pDeviceList;
    if-eqz v3, :cond_26

    #@56
    .line 781
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    #@58
    .end local v3           #listener:Ljava/lang/Object;
    invoke-interface {v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;->onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V

    #@5b
    goto :goto_26

    #@5c
    .line 785
    .end local v4           #peers:Landroid/net/wifi/p2p/WifiP2pDeviceList;
    .restart local v3       #listener:Ljava/lang/Object;
    :sswitch_5c
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5e
    check-cast v6, Landroid/net/wifi/p2p/WifiP2pInfo;

    #@60
    .line 786
    .local v6, wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;
    if-eqz v3, :cond_26

    #@62
    .line 787
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;

    #@64
    .end local v3           #listener:Ljava/lang/Object;
    invoke-interface {v3, v6}, Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;->onConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V

    #@67
    goto :goto_26

    #@68
    .line 791
    .end local v6           #wifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;
    .restart local v3       #listener:Ljava/lang/Object;
    :sswitch_68
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6a
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@6c
    .line 792
    .local v1, group:Landroid/net/wifi/p2p/WifiP2pGroup;
    if-eqz v3, :cond_26

    #@6e
    .line 793
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;

    #@70
    .end local v3           #listener:Ljava/lang/Object;
    invoke-interface {v3, v1}, Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;->onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V

    #@73
    goto :goto_26

    #@74
    .line 797
    .end local v1           #group:Landroid/net/wifi/p2p/WifiP2pGroup;
    .restart local v3       #listener:Ljava/lang/Object;
    :sswitch_74
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    check-cast v5, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;

    #@78
    .line 798
    .local v5, resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@7a
    invoke-static {v7, v5}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$200(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;)V

    #@7d
    goto :goto_26

    #@7e
    .line 801
    .end local v5           #resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    :sswitch_7e
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@80
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@83
    move-result-object v7

    #@84
    if-eqz v7, :cond_26

    #@86
    .line 802
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@89
    move-result-object v0

    #@8a
    .line 803
    .local v0, bundle:Landroid/os/Bundle;
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@8c
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@8f
    move-result-object v9

    #@90
    const-string/jumbo v7, "wifiP2pDevice"

    #@93
    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@96
    move-result-object v7

    #@97
    check-cast v7, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@99
    const-string/jumbo v8, "wifiP2pConfig"

    #@9c
    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@9f
    move-result-object v8

    #@a0
    check-cast v8, Landroid/net/wifi/p2p/WifiP2pConfig;

    #@a2
    invoke-interface {v9, v7, v8}, Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;->onConnectionRequested(Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)V

    #@a5
    goto :goto_26

    #@a6
    .line 811
    .end local v0           #bundle:Landroid/os/Bundle;
    :sswitch_a6
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@a8
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@ab
    move-result-object v7

    #@ac
    if-eqz v7, :cond_26

    #@ae
    .line 812
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@b1
    move-result-object v0

    #@b2
    .line 813
    .restart local v0       #bundle:Landroid/os/Bundle;
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@b4
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@b7
    move-result-object v7

    #@b8
    const-string/jumbo v8, "wpsPin"

    #@bb
    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@be
    move-result-object v8

    #@bf
    invoke-interface {v7, v8}, Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;->onShowPinRequested(Ljava/lang/String;)V

    #@c2
    goto/16 :goto_26

    #@c4
    .line 818
    .end local v0           #bundle:Landroid/os/Bundle;
    :sswitch_c4
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@c6
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@c9
    move-result-object v7

    #@ca
    if-eqz v7, :cond_26

    #@cc
    .line 819
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@ce
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@d1
    move-result-object v7

    #@d2
    invoke-interface {v7}, Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;->onAttached()V

    #@d5
    goto/16 :goto_26

    #@d7
    .line 823
    :sswitch_d7
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@d9
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@dc
    move-result-object v7

    #@dd
    if-eqz v7, :cond_26

    #@df
    .line 824
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@e1
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$300(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@e4
    move-result-object v7

    #@e5
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@e7
    invoke-interface {v7, v8}, Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;->onDetached(I)V

    #@ea
    .line 825
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pManager$Channel$P2pHandler;->this$0:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@ec
    invoke-static {v7, v9}, Landroid/net/wifi/p2p/WifiP2pManager$Channel;->access$302(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;)Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;

    #@ef
    goto/16 :goto_26

    #@f1
    .line 829
    :sswitch_f1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f3
    check-cast v2, Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@f5
    .line 830
    .local v2, groups:Landroid/net/wifi/p2p/WifiP2pGroupList;
    if-eqz v3, :cond_26

    #@f7
    .line 831
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager$PersistentGroupInfoListener;

    #@f9
    .end local v3           #listener:Ljava/lang/Object;
    invoke-interface {v3, v2}, Landroid/net/wifi/p2p/WifiP2pManager$PersistentGroupInfoListener;->onPersistentGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroupList;)V

    #@fc
    goto/16 :goto_26

    #@fe
    .line 729
    :sswitch_data_fe
    .sparse-switch
        0x11004 -> :sswitch_27
        0x22002 -> :sswitch_3e
        0x22003 -> :sswitch_48
        0x22005 -> :sswitch_3e
        0x22006 -> :sswitch_48
        0x22008 -> :sswitch_3e
        0x22009 -> :sswitch_48
        0x2200b -> :sswitch_3e
        0x2200c -> :sswitch_48
        0x2200e -> :sswitch_3e
        0x2200f -> :sswitch_48
        0x22011 -> :sswitch_3e
        0x22012 -> :sswitch_48
        0x22014 -> :sswitch_50
        0x22016 -> :sswitch_5c
        0x22018 -> :sswitch_68
        0x2201d -> :sswitch_3e
        0x2201e -> :sswitch_48
        0x22020 -> :sswitch_3e
        0x22021 -> :sswitch_48
        0x22023 -> :sswitch_3e
        0x22024 -> :sswitch_48
        0x22026 -> :sswitch_3e
        0x22027 -> :sswitch_48
        0x22029 -> :sswitch_3e
        0x2202a -> :sswitch_48
        0x2202c -> :sswitch_3e
        0x2202d -> :sswitch_48
        0x2202f -> :sswitch_3e
        0x22030 -> :sswitch_48
        0x22032 -> :sswitch_74
        0x22034 -> :sswitch_3e
        0x22035 -> :sswitch_48
        0x22037 -> :sswitch_d7
        0x22038 -> :sswitch_c4
        0x22039 -> :sswitch_7e
        0x2203a -> :sswitch_a6
        0x2203c -> :sswitch_3e
        0x2203d -> :sswitch_48
        0x2203f -> :sswitch_f1
        0x22041 -> :sswitch_3e
        0x22042 -> :sswitch_48
    .end sparse-switch
.end method
