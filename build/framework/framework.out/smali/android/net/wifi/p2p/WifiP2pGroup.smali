.class public Landroid/net/wifi/p2p/WifiP2pGroup;
.super Ljava/lang/Object;
.source "WifiP2pGroup.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pGroup;",
            ">;"
        }
    .end annotation
.end field

.field public static final PERSISTENT_NET_ID:I = -0x2

.field public static final TEMPORARY_NET_ID:I = -0x1

.field private static final groupStartedPattern:Ljava/util/regex/Pattern;


# instance fields
.field private mClients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mInterface:Ljava/lang/String;

.field private mIsGroupOwner:Z

.field private mNetId:I

.field private mNetworkName:Ljava/lang/String;

.field private mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private mPassphrase:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 67
    const-string/jumbo v0, "ssid=\"(.+)\" freq=(\\d+) (?:psk=)?([0-9a-fA-F]{64})?(?:passphrase=)?(?:\"(.{0,63})\")? go_dev_addr=((?:[0-9a-f]{2}:){5}[0-9a-f]{2}) ?(\\[PERSISTENT\\])?"

    #@3
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pGroup;->groupStartedPattern:Ljava/util/regex/Pattern;

    #@9
    .line 309
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pGroup$1;

    #@b
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pGroup$1;-><init>()V

    #@e
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@a
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .registers 6
    .parameter "source"

    #@0
    .prologue
    .line 282
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    new-instance v2, Ljava/util/ArrayList;

    #@5
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@a
    .line 283
    if-eqz p1, :cond_4d

    #@c
    .line 284
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkName()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetworkName:Ljava/lang/String;

    #@12
    .line 285
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@14
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    #@17
    move-result-object v3

    #@18
    invoke-direct {v2, v3}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@1b
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@1d
    .line 286
    iget-boolean v2, p1, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@1f
    iput-boolean v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@21
    .line 287
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    #@24
    move-result-object v2

    #@25
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v1

    #@29
    .local v1, i$:Ljava/util/Iterator;
    :goto_29
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_3b

    #@2f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v0

    #@33
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@35
    .local v0, d:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@37
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_29

    #@3b
    .line 288
    .end local v0           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_3b
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getPassphrase()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mPassphrase:Ljava/lang/String;

    #@41
    .line 289
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mInterface:Ljava/lang/String;

    #@47
    .line 290
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@4a
    move-result v2

    #@4b
    iput v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@4d
    .line 292
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_4d
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 13
    .parameter "supplicantEvent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 96
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    new-instance v9, Ljava/util/ArrayList;

    #@5
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@a
    .line 98
    const-string v9, " "

    #@c
    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@f
    move-result-object v8

    #@10
    .line 100
    .local v8, tokens:[Ljava/lang/String;
    array-length v9, v8

    #@11
    const/4 v10, 0x3

    #@12
    if-ge v9, v10, :cond_1c

    #@14
    .line 101
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v10, "Malformed supplicant event"

    #@18
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v9

    #@1c
    .line 104
    :cond_1c
    const/4 v9, 0x0

    #@1d
    aget-object v9, v8, v9

    #@1f
    const-string v10, "P2P-GROUP"

    #@21
    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@24
    move-result v9

    #@25
    if-eqz v9, :cond_6d

    #@27
    .line 105
    const/4 v9, 0x1

    #@28
    aget-object v9, v8, v9

    #@2a
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mInterface:Ljava/lang/String;

    #@2c
    .line 106
    const/4 v9, 0x2

    #@2d
    aget-object v9, v8, v9

    #@2f
    const-string v10, "GO"

    #@31
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v9

    #@35
    iput-boolean v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@37
    .line 108
    sget-object v9, Landroid/net/wifi/p2p/WifiP2pGroup;->groupStartedPattern:Ljava/util/regex/Pattern;

    #@39
    invoke-virtual {v9, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@3c
    move-result-object v4

    #@3d
    .line 109
    .local v4, match:Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    #@40
    move-result v9

    #@41
    if-nez v9, :cond_44

    #@43
    .line 155
    .end local v4           #match:Ljava/util/regex/Matcher;
    :cond_43
    :goto_43
    return-void

    #@44
    .line 113
    .restart local v4       #match:Ljava/util/regex/Matcher;
    :cond_44
    const/4 v9, 0x1

    #@45
    invoke-virtual {v4, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetworkName:Ljava/lang/String;

    #@4b
    .line 117
    const/4 v9, 0x4

    #@4c
    invoke-virtual {v4, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@4f
    move-result-object v9

    #@50
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mPassphrase:Ljava/lang/String;

    #@52
    .line 118
    new-instance v9, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@54
    const/4 v10, 0x5

    #@55
    invoke-virtual {v4, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@58
    move-result-object v10

    #@59
    invoke-direct {v9, v10}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@5c
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@5e
    .line 119
    const/4 v9, 0x6

    #@5f
    invoke-virtual {v4, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@62
    move-result-object v9

    #@63
    if-eqz v9, :cond_69

    #@65
    .line 120
    const/4 v9, -0x2

    #@66
    iput v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@68
    goto :goto_43

    #@69
    .line 122
    :cond_69
    const/4 v9, -0x1

    #@6a
    iput v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@6c
    goto :goto_43

    #@6d
    .line 124
    .end local v4           #match:Ljava/util/regex/Matcher;
    :cond_6d
    const/4 v9, 0x0

    #@6e
    aget-object v9, v8, v9

    #@70
    const-string v10, "P2P-INVITATION-RECEIVED"

    #@72
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v9

    #@76
    if-eqz v9, :cond_e2

    #@78
    .line 125
    const/4 v6, 0x0

    #@79
    .line 126
    .local v6, sa:Ljava/lang/String;
    const/4 v9, -0x2

    #@7a
    iput v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@7c
    .line 127
    move-object v0, v8

    #@7d
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@7e
    .local v3, len$:I
    const/4 v2, 0x0

    #@7f
    .local v2, i$:I
    :goto_7f
    if-ge v2, v3, :cond_43

    #@81
    aget-object v7, v0, v2

    #@83
    .line 128
    .local v7, token:Ljava/lang/String;
    const-string v9, "="

    #@85
    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    .line 129
    .local v5, nameValue:[Ljava/lang/String;
    array-length v9, v5

    #@8a
    const/4 v10, 0x2

    #@8b
    if-eq v9, v10, :cond_90

    #@8d
    .line 127
    :cond_8d
    :goto_8d
    add-int/lit8 v2, v2, 0x1

    #@8f
    goto :goto_7f

    #@90
    .line 131
    :cond_90
    const/4 v9, 0x0

    #@91
    aget-object v9, v5, v9

    #@93
    const-string/jumbo v10, "sa"

    #@96
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v9

    #@9a
    if-eqz v9, :cond_af

    #@9c
    .line 132
    const/4 v9, 0x1

    #@9d
    aget-object v6, v5, v9

    #@9f
    .line 135
    new-instance v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@a1
    invoke-direct {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>()V

    #@a4
    .line 136
    .local v1, dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    const/4 v9, 0x1

    #@a5
    aget-object v9, v5, v9

    #@a7
    iput-object v9, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@a9
    .line 137
    iget-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@ab
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@ae
    goto :goto_8d

    #@af
    .line 141
    .end local v1           #dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_af
    const/4 v9, 0x0

    #@b0
    aget-object v9, v5, v9

    #@b2
    const-string v10, "go_dev_addr"

    #@b4
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b7
    move-result v9

    #@b8
    if-eqz v9, :cond_c5

    #@ba
    .line 142
    new-instance v9, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@bc
    const/4 v10, 0x1

    #@bd
    aget-object v10, v5, v10

    #@bf
    invoke-direct {v9, v10}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@c2
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@c4
    goto :goto_8d

    #@c5
    .line 146
    :cond_c5
    const/4 v9, 0x0

    #@c6
    aget-object v9, v5, v9

    #@c8
    const-string/jumbo v10, "persistent"

    #@cb
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v9

    #@cf
    if-eqz v9, :cond_8d

    #@d1
    .line 147
    new-instance v9, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@d3
    invoke-direct {v9, v6}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@d6
    iput-object v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@d8
    .line 148
    const/4 v9, 0x1

    #@d9
    aget-object v9, v5, v9

    #@db
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@de
    move-result v9

    #@df
    iput v9, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@e1
    goto :goto_8d

    #@e2
    .line 153
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v5           #nameValue:[Ljava/lang/String;
    .end local v6           #sa:Ljava/lang/String;
    .end local v7           #token:Ljava/lang/String;
    :cond_e2
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@e4
    const-string v10, "Malformed supplicant event"

    #@e6
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e9
    throw v9
.end method


# virtual methods
.method public addClient(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 197
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_19

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@12
    .line 198
    .local v0, client:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-virtual {v0, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_6

    #@18
    .line 201
    .end local v0           #client:Landroid/net/wifi/p2p/WifiP2pDevice;
    :goto_18
    return-void

    #@19
    .line 200
    :cond_19
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@1b
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1e
    goto :goto_18
.end method

.method public addClient(Ljava/lang/String;)V
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 192
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    invoke-direct {v0, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@5
    invoke-virtual {p0, v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->addClient(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@8
    .line 193
    return-void
.end method

.method public contains(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@a
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    .line 221
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 278
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getClientList()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getInterface()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 250
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mInterface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNetworkId()I
    .registers 2

    #@0
    .prologue
    .line 255
    iget v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@2
    return v0
.end method

.method public getNetworkName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetworkName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;
    .registers 2

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    return-object v0
.end method

.method public getPassphrase()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 240
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mPassphrase:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isClientListEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isGroupOwner()Z
    .registers 2

    #@0
    .prologue
    .line 177
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@2
    return v0
.end method

.method public removeClient(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@2
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public removeClient(Ljava/lang/String;)Z
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@2
    new-instance v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@4
    invoke-direct {v1, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public setInterface(Ljava/lang/String;)V
    .registers 2
    .parameter "intf"

    #@0
    .prologue
    .line 245
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mInterface:Ljava/lang/String;

    #@2
    .line 246
    return-void
.end method

.method public setIsGroupOwner(Z)V
    .registers 2
    .parameter "isGo"

    #@0
    .prologue
    .line 172
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@2
    .line 173
    return-void
.end method

.method public setNetworkId(I)V
    .registers 2
    .parameter "netId"

    #@0
    .prologue
    .line 260
    iput p1, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@2
    .line 261
    return-void
.end method

.method public setNetworkName(Ljava/lang/String;)V
    .registers 2
    .parameter "networkName"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetworkName:Ljava/lang/String;

    #@2
    .line 160
    return-void
.end method

.method public setOwner(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .registers 2
    .parameter "device"

    #@0
    .prologue
    .line 182
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    .line 183
    return-void
.end method

.method public setPassphrase(Ljava/lang/String;)V
    .registers 2
    .parameter "passphrase"

    #@0
    .prologue
    .line 231
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mPassphrase:Ljava/lang/String;

    #@2
    .line 232
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 264
    new-instance v2, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 265
    .local v2, sbuf:Ljava/lang/StringBuffer;
    const-string/jumbo v3, "network: "

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b
    move-result-object v3

    #@c
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetworkName:Ljava/lang/String;

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@11
    .line 266
    const-string v3, "\n isGO: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@16
    move-result-object v3

    #@17
    iget-boolean v4, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    #@1c
    .line 267
    const-string v3, "\n GO: "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@21
    move-result-object v3

    #@22
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@27
    .line 268
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@29
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v1

    #@2d
    .local v1, i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_43

    #@33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@39
    .line 269
    .local v0, client:Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v3, "\n Client: "

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@42
    goto :goto_2d

    #@43
    .line 271
    .end local v0           #client:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_43
    const-string v3, "\n interface: "

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@48
    move-result-object v3

    #@49
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mInterface:Ljava/lang/String;

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@4e
    .line 272
    const-string v3, "\n networkId: "

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@53
    move-result-object v3

    #@54
    iget v4, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@59
    .line 273
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 296
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetworkName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 297
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mOwner:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@7
    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@a
    .line 298
    iget-boolean v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mIsGroupOwner:Z

    #@c
    if-eqz v2, :cond_31

    #@e
    const/4 v2, 0x1

    #@f
    :goto_f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@12
    .line 299
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@14
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@17
    move-result v2

    #@18
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 300
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mClients:Ljava/util/List;

    #@1d
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v1

    #@21
    .local v1, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_33

    #@27
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2d
    .line 301
    .local v0, client:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@30
    goto :goto_21

    #@31
    .line 298
    .end local v0           #client:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_31
    const/4 v2, 0x0

    #@32
    goto :goto_f

    #@33
    .line 303
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_33
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mPassphrase:Ljava/lang/String;

    #@35
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@38
    .line 304
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mInterface:Ljava/lang/String;

    #@3a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3d
    .line 305
    iget v2, p0, Landroid/net/wifi/p2p/WifiP2pGroup;->mNetId:I

    #@3f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    .line 306
    return-void
.end method
