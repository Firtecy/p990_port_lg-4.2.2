.class public final enum Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
.super Ljava/lang/Enum;
.source "WifiP2pService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "P2pStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum BOTH_GO_INTENT_15:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum INCOMPATIBLE_PARAMETERS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum INCOMPATIBLE_PROVISIONING_METHOD:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum INFORMATION_IS_CURRENTLY_UNAVAILABLE:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum INVALID_PARAMETER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum LIMIT_REACHED:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum NO_COMMON_CHANNEL:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum PREVIOUS_PROTOCOL_ERROR:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum REJECTED_BY_USER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum SUCCESS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum UNABLE_TO_ACCOMMODATE_REQUEST:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum UNKNOWN:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

.field public static final enum UNKNOWN_P2P_GROUP:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 248
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@7
    const-string v1, "SUCCESS"

    #@9
    invoke-direct {v0, v1, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->SUCCESS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@e
    .line 251
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@10
    const-string v1, "INFORMATION_IS_CURRENTLY_UNAVAILABLE"

    #@12
    invoke-direct {v0, v1, v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INFORMATION_IS_CURRENTLY_UNAVAILABLE:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@17
    .line 254
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@19
    const-string v1, "INCOMPATIBLE_PARAMETERS"

    #@1b
    invoke-direct {v0, v1, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INCOMPATIBLE_PARAMETERS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@20
    .line 258
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@22
    const-string v1, "LIMIT_REACHED"

    #@24
    invoke-direct {v0, v1, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->LIMIT_REACHED:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@29
    .line 261
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@2b
    const-string v1, "INVALID_PARAMETER"

    #@2d
    invoke-direct {v0, v1, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INVALID_PARAMETER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@32
    .line 264
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@34
    const-string v1, "UNABLE_TO_ACCOMMODATE_REQUEST"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNABLE_TO_ACCOMMODATE_REQUEST:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@3c
    .line 267
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@3e
    const-string v1, "PREVIOUS_PROTOCOL_ERROR"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->PREVIOUS_PROTOCOL_ERROR:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@46
    .line 270
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@48
    const-string v1, "NO_COMMON_CHANNEL"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->NO_COMMON_CHANNEL:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@50
    .line 274
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@52
    const-string v1, "UNKNOWN_P2P_GROUP"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN_P2P_GROUP:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@5b
    .line 277
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@5d
    const-string v1, "BOTH_GO_INTENT_15"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->BOTH_GO_INTENT_15:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@66
    .line 280
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@68
    const-string v1, "INCOMPATIBLE_PROVISIONING_METHOD"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INCOMPATIBLE_PROVISIONING_METHOD:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@71
    .line 283
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@73
    const-string v1, "REJECTED_BY_USER"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->REJECTED_BY_USER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@7c
    .line 286
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@7e
    const-string v1, "UNKNOWN"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@87
    .line 246
    const/16 v0, 0xd

    #@89
    new-array v0, v0, [Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@8b
    sget-object v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->SUCCESS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@8d
    aput-object v1, v0, v3

    #@8f
    sget-object v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INFORMATION_IS_CURRENTLY_UNAVAILABLE:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@91
    aput-object v1, v0, v4

    #@93
    sget-object v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INCOMPATIBLE_PARAMETERS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@95
    aput-object v1, v0, v5

    #@97
    sget-object v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->LIMIT_REACHED:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@99
    aput-object v1, v0, v6

    #@9b
    sget-object v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INVALID_PARAMETER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@9d
    aput-object v1, v0, v7

    #@9f
    const/4 v1, 0x5

    #@a0
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNABLE_TO_ACCOMMODATE_REQUEST:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/4 v1, 0x6

    #@a5
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->PREVIOUS_PROTOCOL_ERROR:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@a7
    aput-object v2, v0, v1

    #@a9
    const/4 v1, 0x7

    #@aa
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->NO_COMMON_CHANNEL:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/16 v1, 0x8

    #@b0
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN_P2P_GROUP:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x9

    #@b6
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->BOTH_GO_INTENT_15:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@b8
    aput-object v2, v0, v1

    #@ba
    const/16 v1, 0xa

    #@bc
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INCOMPATIBLE_PROVISIONING_METHOD:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@be
    aput-object v2, v0, v1

    #@c0
    const/16 v1, 0xb

    #@c2
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->REJECTED_BY_USER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0xc

    #@c8
    sget-object v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@ca
    aput-object v2, v0, v1

    #@cc
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->$VALUES:[Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@ce
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(I)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    .registers 2
    .parameter "error"

    #@0
    .prologue
    .line 289
    packed-switch p0, :pswitch_data_2a

    #@3
    .line 315
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 291
    :pswitch_6
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->SUCCESS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@8
    goto :goto_5

    #@9
    .line 293
    :pswitch_9
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INFORMATION_IS_CURRENTLY_UNAVAILABLE:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@b
    goto :goto_5

    #@c
    .line 295
    :pswitch_c
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INCOMPATIBLE_PARAMETERS:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@e
    goto :goto_5

    #@f
    .line 297
    :pswitch_f
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->LIMIT_REACHED:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@11
    goto :goto_5

    #@12
    .line 299
    :pswitch_12
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INVALID_PARAMETER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@14
    goto :goto_5

    #@15
    .line 301
    :pswitch_15
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNABLE_TO_ACCOMMODATE_REQUEST:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@17
    goto :goto_5

    #@18
    .line 303
    :pswitch_18
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->PREVIOUS_PROTOCOL_ERROR:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@1a
    goto :goto_5

    #@1b
    .line 305
    :pswitch_1b
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->NO_COMMON_CHANNEL:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@1d
    goto :goto_5

    #@1e
    .line 307
    :pswitch_1e
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN_P2P_GROUP:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@20
    goto :goto_5

    #@21
    .line 309
    :pswitch_21
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->BOTH_GO_INTENT_15:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@23
    goto :goto_5

    #@24
    .line 311
    :pswitch_24
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->INCOMPATIBLE_PROVISIONING_METHOD:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@26
    goto :goto_5

    #@27
    .line 313
    :pswitch_27
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->REJECTED_BY_USER:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@29
    goto :goto_5

    #@2a
    .line 289
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 246
    const-class v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    .registers 1

    #@0
    .prologue
    .line 246
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->$VALUES:[Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@2
    invoke-virtual {v0}, [Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@8
    return-object v0
.end method
