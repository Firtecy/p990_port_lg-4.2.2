.class Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WifiP2pService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LGMDMReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/p2p/WifiP2pService;


# direct methods
.method private constructor <init>(Landroid/net/wifi/p2p/WifiP2pService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 364
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/wifi/p2p/WifiP2pService;Landroid/net/wifi/p2p/WifiP2pService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 364
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;-><init>(Landroid/net/wifi/p2p/WifiP2pService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 367
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@2
    if-eqz v3, :cond_3a

    #@4
    .line 368
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 369
    .local v1, action:Ljava/lang/String;
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@b
    move-result-object v3

    #@c
    if-eqz v3, :cond_3a

    #@e
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@11
    move-result-object v3

    #@12
    invoke-interface {v3, v1}, Lcom/lge/cappuccino/IMdm;->recevieWifiP2pIntent(Ljava/lang/String;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_3a

    #@18
    .line 371
    const-string v0, "force_wifip2p_disable"

    #@1a
    .line 372
    .local v0, FORCE_WIFIP2P_DISABLE:Ljava/lang/String;
    const-string v3, "force_wifip2p_disable"

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@20
    move-result v2

    #@21
    .line 373
    .local v2, isDisable:Z
    if-eqz v2, :cond_3b

    #@23
    .line 374
    const-string v3, "WifiP2pService"

    #@25
    const-string v4, "CAPP_MDM wifi direct policy: MDM_FORCE_DISABLE"

    #@27
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 375
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2c
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getHandler()Landroid/os/Handler;

    #@33
    move-result-object v3

    #@34
    const v4, 0x20084

    #@37
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@3a
    .line 384
    .end local v0           #FORCE_WIFIP2P_DISABLE:Ljava/lang/String;
    .end local v1           #action:Ljava/lang/String;
    .end local v2           #isDisable:Z
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 378
    .restart local v0       #FORCE_WIFIP2P_DISABLE:Ljava/lang/String;
    .restart local v1       #action:Ljava/lang/String;
    .restart local v2       #isDisable:Z
    :cond_3b
    const-string v3, "WifiP2pService"

    #@3d
    const-string v4, "CAPP_MDM wifi direct policy: MDM_FORCE_ENABLE"

    #@3f
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 379
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@44
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getHandler()Landroid/os/Handler;

    #@4b
    move-result-object v3

    #@4c
    const v4, 0x20083

    #@4f
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@52
    goto :goto_3a
.end method
