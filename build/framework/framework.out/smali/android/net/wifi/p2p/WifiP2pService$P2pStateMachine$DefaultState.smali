.class Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;
.super Lcom/android/internal/util/State;
.source "WifiP2pService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultState"
.end annotation


# instance fields
.field final synthetic this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 505
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public processMessage(Landroid/os/Message;)Z
    .registers 10
    .parameter "message"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v7, 0x2

    #@2
    .line 508
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4
    new-instance v5, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->getName()Ljava/lang/String;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {p1}, Landroid/os/Message;->toString()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-static {v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@20
    .line 509
    iget v4, p1, Landroid/os/Message;->what:I

    #@22
    sparse-switch v4, :sswitch_data_23a

    #@25
    .line 676
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@27
    new-instance v4, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v5, "Unhandled message "

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@3d
    .line 677
    const/4 v3, 0x0

    #@3e
    .line 680
    :goto_3e
    return v3

    #@3f
    .line 511
    :sswitch_3f
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@41
    if-nez v4, :cond_57

    #@43
    .line 512
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@45
    const-string v4, "Full connection with WifiStateMachine established"

    #@47
    invoke-static {v3, v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@4a
    .line 513
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@4c
    iget-object v4, v3, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@50
    check-cast v3, Lcom/android/internal/util/AsyncChannel;

    #@52
    invoke-static {v4, v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$602(Landroid/net/wifi/p2p/WifiP2pService;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@55
    .line 680
    :goto_55
    :sswitch_55
    const/4 v3, 0x1

    #@56
    goto :goto_3e

    #@57
    .line 515
    :cond_57
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@59
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v6, "Full connection failure, error = "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    invoke-static {v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@71
    .line 516
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@73
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@75
    invoke-static {v4, v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$602(Landroid/net/wifi/p2p/WifiP2pService;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@78
    goto :goto_55

    #@79
    .line 521
    :sswitch_79
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@7b
    if-ne v4, v7, :cond_8c

    #@7d
    .line 522
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@7f
    const-string v5, "Send failed, client connection lost"

    #@81
    invoke-static {v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@84
    .line 526
    :goto_84
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@86
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@88
    invoke-static {v4, v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$602(Landroid/net/wifi/p2p/WifiP2pService;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@8b
    goto :goto_55

    #@8c
    .line 524
    :cond_8c
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@8e
    new-instance v5, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v6, "Client connection lost with reason: "

    #@95
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v5

    #@99
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@9b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v5

    #@a3
    invoke-static {v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@a6
    goto :goto_84

    #@a7
    .line 530
    :sswitch_a7
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@a9
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@ac
    .line 531
    .local v0, ac:Lcom/android/internal/util/AsyncChannel;
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@ae
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@b0
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@b3
    move-result-object v3

    #@b4
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@b6
    invoke-virtual {v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getHandler()Landroid/os/Handler;

    #@b9
    move-result-object v4

    #@ba
    iget-object v5, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@bc
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@bf
    goto :goto_55

    #@c0
    .line 534
    .end local v0           #ac:Lcom/android/internal/util/AsyncChannel;
    :sswitch_c0
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@c2
    const v4, 0x22002

    #@c5
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@c8
    goto :goto_55

    #@c9
    .line 538
    :sswitch_c9
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@cb
    const v4, 0x22005

    #@ce
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@d1
    goto :goto_55

    #@d2
    .line 542
    :sswitch_d2
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@d4
    const v4, 0x2202f

    #@d7
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@da
    goto/16 :goto_55

    #@dc
    .line 546
    :sswitch_dc
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@de
    const v4, 0x22008

    #@e1
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@e4
    goto/16 :goto_55

    #@e6
    .line 550
    :sswitch_e6
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@e8
    const v4, 0x2200b

    #@eb
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@ee
    goto/16 :goto_55

    #@f0
    .line 554
    :sswitch_f0
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@f2
    const v4, 0x2200e

    #@f5
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@f8
    goto/16 :goto_55

    #@fa
    .line 558
    :sswitch_fa
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@fc
    const v4, 0x22011

    #@ff
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@102
    goto/16 :goto_55

    #@104
    .line 562
    :sswitch_104
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@106
    const v4, 0x2201d

    #@109
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@10c
    goto/16 :goto_55

    #@10e
    .line 566
    :sswitch_10e
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@110
    const v4, 0x22020

    #@113
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@116
    goto/16 :goto_55

    #@118
    .line 570
    :sswitch_118
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@11a
    const v4, 0x22023

    #@11d
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@120
    goto/16 :goto_55

    #@122
    .line 574
    :sswitch_122
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@124
    const v4, 0x22026

    #@127
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@12a
    goto/16 :goto_55

    #@12c
    .line 578
    :sswitch_12c
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@12e
    const v4, 0x22029

    #@131
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@134
    goto/16 :goto_55

    #@136
    .line 583
    :sswitch_136
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@138
    const v4, 0x2202c

    #@13b
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@13e
    goto/16 :goto_55

    #@140
    .line 588
    :sswitch_140
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@142
    const v4, 0x22034

    #@145
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@148
    goto/16 :goto_55

    #@14a
    .line 592
    :sswitch_14a
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@14c
    const v4, 0x2203b

    #@14f
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@152
    goto/16 :goto_55

    #@154
    .line 596
    :sswitch_154
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@156
    const v4, 0x22041

    #@159
    invoke-static {v3, p1, v4, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@15c
    goto/16 :goto_55

    #@15e
    .line 600
    :sswitch_15e
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@160
    const v4, 0x22014

    #@163
    new-instance v5, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@165
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@167
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@16a
    move-result-object v6

    #@16b
    invoke-direct {v5, v6}, Landroid/net/wifi/p2p/WifiP2pDeviceList;-><init>(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V

    #@16e
    invoke-static {v3, p1, v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@171
    goto/16 :goto_55

    #@173
    .line 604
    :sswitch_173
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@175
    const v4, 0x22016

    #@178
    new-instance v5, Landroid/net/wifi/p2p/WifiP2pInfo;

    #@17a
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@17c
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pInfo;

    #@17f
    move-result-object v6

    #@180
    invoke-direct {v5, v6}, Landroid/net/wifi/p2p/WifiP2pInfo;-><init>(Landroid/net/wifi/p2p/WifiP2pInfo;)V

    #@183
    invoke-static {v3, p1, v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@186
    goto/16 :goto_55

    #@188
    .line 608
    :sswitch_188
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@18a
    const v5, 0x22018

    #@18d
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@18f
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@192
    move-result-object v6

    #@193
    if-eqz v6, :cond_1a0

    #@195
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@197
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@199
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@19c
    move-result-object v6

    #@19d
    invoke-direct {v3, v6}, Landroid/net/wifi/p2p/WifiP2pGroup;-><init>(Landroid/net/wifi/p2p/WifiP2pGroup;)V

    #@1a0
    :cond_1a0
    invoke-static {v4, p1, v5, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@1a3
    goto/16 :goto_55

    #@1a5
    .line 612
    :sswitch_1a5
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1a7
    const v5, 0x2203f

    #@1aa
    new-instance v6, Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@1ac
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1ae
    invoke-static {v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@1b1
    move-result-object v7

    #@1b2
    invoke-direct {v6, v7, v3}, Landroid/net/wifi/p2p/WifiP2pGroupList;-><init>(Landroid/net/wifi/p2p/WifiP2pGroupList;Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;)V

    #@1b5
    invoke-static {v4, p1, v5, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@1b8
    goto/16 :goto_55

    #@1ba
    .line 616
    :sswitch_1ba
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1bd
    move-result-object v3

    #@1be
    const-string v4, "appPkgName"

    #@1c0
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1c3
    move-result-object v1

    #@1c4
    .line 618
    .local v1, appPkgName:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1c7
    move-result-object v3

    #@1c8
    const-string v4, "dialogResetFlag"

    #@1ca
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@1cd
    move-result v2

    #@1ce
    .line 620
    .local v2, isReset:Z
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1d0
    iget-object v4, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@1d2
    invoke-static {v3, v4, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;Ljava/lang/String;Z)Z

    #@1d5
    move-result v3

    #@1d6
    if-eqz v3, :cond_1e2

    #@1d8
    .line 621
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1da
    const v4, 0x22038

    #@1dd
    invoke-static {v3, p1, v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;I)V

    #@1e0
    goto/16 :goto_55

    #@1e2
    .line 623
    :cond_1e2
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1e4
    const v4, 0x22037

    #@1e7
    const/4 v5, 0x4

    #@1e8
    invoke-static {v3, p1, v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V

    #@1eb
    goto/16 :goto_55

    #@1ed
    .line 662
    .end local v1           #appPkgName:Ljava/lang/String;
    .end local v2           #isReset:Z
    :sswitch_1ed
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1ef
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1f1
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$600(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;

    #@1f4
    move-result-object v3

    #@1f5
    const v4, 0x20085

    #@1f8
    invoke-virtual {v3, v4}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@1fb
    goto/16 :goto_55

    #@1fd
    .line 666
    :sswitch_1fd
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@1ff
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@201
    check-cast v3, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@203
    invoke-static {v4, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1302(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@206
    .line 667
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@208
    new-instance v4, Ljava/lang/StringBuilder;

    #@20a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20d
    const-string v5, "Unexpected group creation, remove "

    #@20f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v4

    #@213
    iget-object v5, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@215
    invoke-static {v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@218
    move-result-object v5

    #@219
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v4

    #@21d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@220
    move-result-object v4

    #@221
    invoke-static {v3, v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V

    #@224
    .line 668
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@226
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;

    #@229
    move-result-object v3

    #@22a
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;->this$1:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@22c
    invoke-static {v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;

    #@22f
    move-result-object v4

    #@230
    invoke-virtual {v4}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@233
    move-result-object v4

    #@234
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiNative;->p2pGroupRemove(Ljava/lang/String;)Z

    #@237
    goto/16 :goto_55

    #@239
    .line 509
    nop

    #@23a
    :sswitch_data_23a
    .sparse-switch
        0x11000 -> :sswitch_3f
        0x11001 -> :sswitch_a7
        0x11004 -> :sswitch_79
        0x20083 -> :sswitch_55
        0x20084 -> :sswitch_1ed
        0x22001 -> :sswitch_c0
        0x22004 -> :sswitch_c9
        0x22007 -> :sswitch_dc
        0x2200a -> :sswitch_e6
        0x2200d -> :sswitch_f0
        0x22010 -> :sswitch_fa
        0x22013 -> :sswitch_15e
        0x22015 -> :sswitch_173
        0x22017 -> :sswitch_188
        0x2201c -> :sswitch_104
        0x2201f -> :sswitch_10e
        0x22022 -> :sswitch_118
        0x22025 -> :sswitch_122
        0x22028 -> :sswitch_12c
        0x2202b -> :sswitch_136
        0x2202e -> :sswitch_d2
        0x22033 -> :sswitch_140
        0x22036 -> :sswitch_1ba
        0x2203b -> :sswitch_14a
        0x2203e -> :sswitch_1a5
        0x22040 -> :sswitch_154
        0x23001 -> :sswitch_55
        0x23002 -> :sswitch_55
        0x23003 -> :sswitch_55
        0x23004 -> :sswitch_55
        0x23005 -> :sswitch_55
        0x23006 -> :sswitch_55
        0x2300d -> :sswitch_55
        0x24001 -> :sswitch_55
        0x24002 -> :sswitch_55
        0x24003 -> :sswitch_55
        0x24004 -> :sswitch_55
        0x24005 -> :sswitch_55
        0x24006 -> :sswitch_55
        0x24007 -> :sswitch_55
        0x24008 -> :sswitch_55
        0x24009 -> :sswitch_55
        0x2400a -> :sswitch_55
        0x2400b -> :sswitch_55
        0x24015 -> :sswitch_55
        0x24016 -> :sswitch_55
        0x2401c -> :sswitch_55
        0x2401d -> :sswitch_1fd
        0x2401e -> :sswitch_55
        0x24020 -> :sswitch_55
        0x24025 -> :sswitch_55
        0x24026 -> :sswitch_55
        0x24027 -> :sswitch_55
        0x30004 -> :sswitch_55
        0x30005 -> :sswitch_55
        0x30006 -> :sswitch_55
    .end sparse-switch
.end method
