.class Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "WifiP2pService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "P2pStateMachine"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;
    }
.end annotation


# instance fields
.field private mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

.field private mFrequencyConflictState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;

.field private mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

.field private mGroupCreatedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

.field private mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

.field private mGroupNegotiationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;

.field private final mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

.field private mInactiveState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

.field private mOngoingGroupRemovalState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;

.field private mP2pDisabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;

.field private mP2pDisablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;

.field private mP2pEnabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

.field private mP2pEnablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;

.field private mP2pNotSupportedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;

.field private final mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

.field private final mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

.field private mProvisionDiscoveryState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;

.field private mSavedP2pGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

.field private mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

.field private mSavedProvDiscDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private mUserAuthorizingInvitationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;

.field private mUserAuthorizingJoinState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;

.field private mWifiMonitor:Landroid/net/wifi/WifiMonitor;

.field private mWifiNative:Landroid/net/wifi/WifiNative;

.field private final mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

.field final synthetic this$0:Landroid/net/wifi/p2p/WifiP2pService;


# direct methods
.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter "name"
    .parameter "p2pSupported"

    #@0
    .prologue
    .line 479
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    .line 480
    invoke-direct {p0, p2}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@5
    .line 425
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@7
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@a
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@c
    .line 426
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;

    #@e
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@11
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pNotSupportedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;

    #@13
    .line 427
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;

    #@15
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@18
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;

    #@1a
    .line 428
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;

    #@1c
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@1f
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;

    #@21
    .line 429
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;

    #@23
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@26
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;

    #@28
    .line 430
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

    #@2a
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@2d
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

    #@2f
    .line 432
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

    #@31
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@34
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mInactiveState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

    #@36
    .line 433
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@38
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@3b
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@3d
    .line 434
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;

    #@3f
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@42
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mUserAuthorizingInvitationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;

    #@44
    .line 436
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;

    #@46
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@49
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mProvisionDiscoveryState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;

    #@4b
    .line 437
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;

    #@4d
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@50
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupNegotiationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;

    #@52
    .line 438
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;

    #@54
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@57
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mFrequencyConflictState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;

    #@59
    .line 440
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

    #@5b
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@5e
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

    #@60
    .line 441
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;

    #@62
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@65
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mUserAuthorizingJoinState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;

    #@67
    .line 442
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;

    #@69
    invoke-direct {v0, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@6c
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mOngoingGroupRemovalState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;

    #@6e
    .line 444
    new-instance v0, Landroid/net/wifi/WifiNative;

    #@70
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@72
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$200(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@75
    move-result-object v1

    #@76
    invoke-direct {v0, v1}, Landroid/net/wifi/WifiNative;-><init>(Ljava/lang/String;)V

    #@79
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7b
    .line 445
    new-instance v0, Landroid/net/wifi/WifiMonitor;

    #@7d
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@7f
    invoke-direct {v0, p0, v1}, Landroid/net/wifi/WifiMonitor;-><init>(Lcom/android/internal/util/StateMachine;Landroid/net/wifi/WifiNative;)V

    #@82
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiMonitor:Landroid/net/wifi/WifiMonitor;

    #@84
    .line 447
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@86
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pDeviceList;-><init>()V

    #@89
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@8b
    .line 456
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@8d
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pDeviceList;-><init>()V

    #@90
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@92
    .line 457
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@94
    const/4 v1, 0x0

    #@95
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$1;

    #@97
    invoke-direct {v2, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$1;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@9a
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pGroupList;-><init>(Landroid/net/wifi/p2p/WifiP2pGroupList;Landroid/net/wifi/p2p/WifiP2pGroupList$GroupDeleteListener;)V

    #@9d
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@9f
    .line 467
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pInfo;

    #@a1
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pInfo;-><init>()V

    #@a4
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@a6
    .line 482
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@a8
    invoke-virtual {p0, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@ab
    .line 483
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pNotSupportedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;

    #@ad
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@af
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@b2
    .line 484
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;

    #@b4
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@b6
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@b9
    .line 485
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;

    #@bb
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@bd
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@c0
    .line 486
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;

    #@c2
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@c4
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@c7
    .line 487
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

    #@c9
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mDefaultState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$DefaultState;

    #@cb
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@ce
    .line 488
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mInactiveState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

    #@d0
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

    #@d2
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@d5
    .line 489
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@d7
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

    #@d9
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@dc
    .line 490
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mUserAuthorizingInvitationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;

    #@de
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@e0
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@e3
    .line 491
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mProvisionDiscoveryState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;

    #@e5
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@e7
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@ea
    .line 492
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupNegotiationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;

    #@ec
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@ee
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@f1
    .line 493
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mFrequencyConflictState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;

    #@f3
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatingState;

    #@f5
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@f8
    .line 494
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

    #@fa
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnabledState;

    #@fc
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@ff
    .line 495
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mUserAuthorizingJoinState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;

    #@101
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

    #@103
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@106
    .line 496
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mOngoingGroupRemovalState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;

    #@108
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

    #@10a
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@10d
    .line 498
    if-eqz p3, :cond_115

    #@10f
    .line 499
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;

    #@111
    invoke-virtual {p0, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@114
    .line 503
    :goto_114
    return-void

    #@115
    .line 501
    :cond_115
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pNotSupportedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pNotSupportedState;

    #@117
    invoke-virtual {p0, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@11a
    goto :goto_114
.end method

.method static synthetic access$1000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@2
    return-object v0
.end method

.method static synthetic access$10000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$10100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$10200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$10300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$10400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$10500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$10600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$10800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;ILjava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2, p3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$11000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updateThisDevice(I)V

    #@3
    return-void
.end method

.method static synthetic access$11100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->setWifiP2pInfoOnGroupFormation(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$11300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedProvDiscDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$11302(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedProvDiscDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$11400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mOngoingGroupRemovalState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$OngoingGroupRemovalState;

    #@2
    return-object v0
.end method

.method static synthetic access$11500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$11600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->handleGroupRemoved()V

    #@3
    return-void
.end method

.method static synthetic access$11700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$11800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$11900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$12000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mUserAuthorizingJoinState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingJoinState;

    #@2
    return-object v0
.end method

.method static synthetic access$12100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->resetWifiP2pInfo()V

    #@3
    return-void
.end method

.method static synthetic access$12300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$12400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@2
    return-object v0
.end method

.method static synthetic access$1302(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@2
    return-object p1
.end method

.method static synthetic access$1400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pGroupList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;Ljava/lang/String;Z)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2, p3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->setDialogListenerApp(Landroid/os/Messenger;Ljava/lang/String;Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->replyToMessage(Landroid/os/Message;I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisabledState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisabledState;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiMonitor:Landroid/net/wifi/WifiMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pEnablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pEnablingState;

    #@2
    return-object v0
.end method

.method static synthetic access$2500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mInactiveState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$InactiveState;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pStateChangedBroadcast(Z)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pConnectionChangedBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$3300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->initializeP2pSettings()V

    #@3
    return-void
.end method

.method static synthetic access$3400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pPeersChangedBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$3700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mP2pDisablingState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$P2pDisablingState;

    #@2
    return-object v0
.end method

.method static synthetic access$3800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->setAndPersistDeviceName(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/WifiNative;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    return-object v0
.end method

.method static synthetic access$4000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pWfdInfo;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->setWfdInfo(Landroid/net/wifi/p2p/WifiP2pWfdInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearSupplicantServiceRequest()V

    #@3
    return-void
.end method

.method static synthetic access$4200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pDiscoveryChangedBroadcast(Z)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updateSupplicantServiceRequest()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addLocalService(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->removeLocalService(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$4700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearLocalServices(Landroid/os/Messenger;)V

    #@3
    return-void
.end method

.method static synthetic access$4800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addServiceRequest(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->removeServiceRequest(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pPersistentGroupsChangedBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$5000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Messenger;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearServiceRequests(Landroid/os/Messenger;)V

    #@3
    return-void
.end method

.method static synthetic access$5100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendServiceResponse(Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;)V

    #@3
    return-void
.end method

.method static synthetic access$5400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pConfig;Z)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->connect(Landroid/net/wifi/p2p/WifiP2pConfig;Z)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$5500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@2
    return-object v0
.end method

.method static synthetic access$5502(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pConfig;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@2
    return-object p1
.end method

.method static synthetic access$5600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mProvisionDiscoveryState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$ProvisionDiscoveryState;

    #@2
    return-object v0
.end method

.method static synthetic access$5700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$5800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupNegotiationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupNegotiationState;

    #@2
    return-object v0
.end method

.method static synthetic access$5900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendConnectNoticeToApp(Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$6300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mUserAuthorizingInvitationState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$UserAuthorizingInvitationState;

    #@2
    return-object v0
.end method

.method static synthetic access$6400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$6800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$6900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$7000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->handleGroupCreationFailure()V

    #@3
    return-void
.end method

.method static synthetic access$7100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pDeviceList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@2
    return-object v0
.end method

.method static synthetic access$7300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->notifyInvitationReceived()V

    #@3
    return-void
.end method

.method static synthetic access$7500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/p2p/WifiP2pConfig;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->p2pConnectWithPinDisplay(Landroid/net/wifi/p2p/WifiP2pConfig;)V

    #@3
    return-void
.end method

.method static synthetic access$7900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8000(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendShowPinReqToFrontApp(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$8300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->notifyInvitationSent(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$8400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$8700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updatePersistentNetworks(Z)V

    #@3
    return-void
.end method

.method static synthetic access$8800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->startDhcpServer(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/os/Message;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2, p3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->replyToMessage(Landroid/os/Message;II)V

    #@3
    return-void
.end method

.method static synthetic access$9100(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroupCreatedState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$GroupCreatedState;

    #@2
    return-object v0
.end method

.method static synthetic access$9200(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$9300(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mFrequencyConflictState:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$FrequencyConflictState;

    #@2
    return-object v0
.end method

.method static synthetic access$9400(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$9500(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$9600(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$9700(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;ILjava/lang/String;Z)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 423
    invoke-direct {p0, p1, p2, p3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->removeClientFromList(ILjava/lang/String;Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$9800(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 423
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method private addLocalService(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z
    .registers 7
    .parameter "m"
    .parameter "servInfo"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 2893
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearClientDeadChannels()V

    #@5
    .line 2894
    invoke-direct {p0, p1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@8
    move-result-object v0

    #@9
    .line 2895
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_c

    #@b
    .line 2908
    :cond_b
    :goto_b
    return v1

    #@c
    .line 2899
    :cond_c
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@f
    move-result-object v3

    #@10
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_b

    #@16
    .line 2903
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@18
    invoke-virtual {v3, p2}, Landroid/net/wifi/WifiNative;->p2pServiceAdd(Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_26

    #@1e
    .line 2904
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@21
    move-result-object v2

    #@22
    invoke-interface {v2, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@25
    goto :goto_b

    #@26
    :cond_26
    move v1, v2

    #@27
    .line 2908
    goto :goto_b
.end method

.method private addRowToDialog(Landroid/view/ViewGroup;ILjava/lang/String;)V
    .registers 9
    .parameter "group"
    .parameter "stringId"
    .parameter "value"

    #@0
    .prologue
    .line 2105
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    .line 2106
    .local v0, r:Landroid/content/res/Resources;
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@6
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@9
    move-result-object v2

    #@a
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@d
    move-result-object v2

    #@e
    const v3, 0x10900ed

    #@11
    const/4 v4, 0x0

    #@12
    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@15
    move-result-object v1

    #@16
    .line 2108
    .local v1, row:Landroid/view/View;
    const v2, 0x10203c2

    #@19
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Landroid/widget/TextView;

    #@1f
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@26
    .line 2109
    const v2, 0x10202ae

    #@29
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Landroid/widget/TextView;

    #@2f
    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@32
    .line 2110
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@35
    .line 2111
    return-void
.end method

.method private addServiceRequest(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;)Z
    .registers 7
    .parameter "m"
    .parameter "req"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2817
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearClientDeadChannels()V

    #@4
    .line 2818
    invoke-direct {p0, p1, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@7
    move-result-object v0

    #@8
    .line 2819
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_c

    #@a
    .line 2820
    const/4 v1, 0x0

    #@b
    .line 2833
    :cond_b
    :goto_b
    return v1

    #@c
    .line 2823
    :cond_c
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@e
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$12904(Landroid/net/wifi/p2p/WifiP2pService;)B

    #@11
    .line 2825
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@13
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$12900(Landroid/net/wifi/p2p/WifiP2pService;)B

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_1e

    #@19
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1b
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$12904(Landroid/net/wifi/p2p/WifiP2pService;)B

    #@1e
    .line 2826
    :cond_1e
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@20
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$12900(Landroid/net/wifi/p2p/WifiP2pService;)B

    #@23
    move-result v2

    #@24
    invoke-virtual {p2, v2}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->setTransactionId(I)V

    #@27
    .line 2827
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@2a
    move-result-object v2

    #@2b
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2d
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$12900(Landroid/net/wifi/p2p/WifiP2pService;)B

    #@30
    move-result v3

    #@31
    invoke-virtual {v2, v3, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@34
    .line 2829
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@36
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    if-eqz v2, :cond_b

    #@3c
    .line 2833
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updateSupplicantServiceRequest()Z

    #@3f
    move-result v1

    #@40
    goto :goto_b
.end method

.method private clearClientDeadChannels()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2981
    new-instance v1, Ljava/util/ArrayList;

    #@3
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 2983
    .local v1, deadClients:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Messenger;>;"
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@8
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@b
    move-result-object v6

    #@c
    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@f
    move-result-object v6

    #@10
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v3

    #@14
    .local v3, i$:Ljava/util/Iterator;
    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_46

    #@1a
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@20
    .line 2984
    .local v0, c:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@23
    move-result-object v5

    #@24
    .line 2985
    .local v5, msg:Landroid/os/Message;
    const v6, 0x22031

    #@27
    iput v6, v5, Landroid/os/Message;->what:I

    #@29
    .line 2986
    iput v7, v5, Landroid/os/Message;->arg1:I

    #@2b
    .line 2987
    iput v7, v5, Landroid/os/Message;->arg2:I

    #@2d
    .line 2988
    const/4 v6, 0x0

    #@2e
    iput-object v6, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@30
    .line 2990
    :try_start_30
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6, v5}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_37} :catch_38

    #@37
    goto :goto_14

    #@38
    .line 2991
    :catch_38
    move-exception v2

    #@39
    .line 2992
    .local v2, e:Landroid/os/RemoteException;
    const-string v6, "detect dead channel"

    #@3b
    invoke-direct {p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@3e
    .line 2993
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@45
    goto :goto_14

    #@46
    .line 2997
    .end local v0           #c:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v5           #msg:Landroid/os/Message;
    :cond_46
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v3

    #@4a
    :goto_4a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v6

    #@4e
    if-eqz v6, :cond_5a

    #@50
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v4

    #@54
    check-cast v4, Landroid/os/Messenger;

    #@56
    .line 2998
    .local v4, m:Landroid/os/Messenger;
    invoke-direct {p0, v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearClientInfo(Landroid/os/Messenger;)V

    #@59
    goto :goto_4a

    #@5a
    .line 3000
    .end local v4           #m:Landroid/os/Messenger;
    :cond_5a
    return-void
.end method

.method private clearClientInfo(Landroid/os/Messenger;)V
    .registers 2
    .parameter "m"

    #@0
    .prologue
    .line 2944
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearLocalServices(Landroid/os/Messenger;)V

    #@3
    .line 2945
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearServiceRequests(Landroid/os/Messenger;)V

    #@6
    .line 2946
    return-void
.end method

.method private clearLocalServices(Landroid/os/Messenger;)V
    .registers 7
    .parameter "m"

    #@0
    .prologue
    .line 2927
    const/4 v3, 0x0

    #@1
    invoke-direct {p0, p1, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@4
    move-result-object v0

    #@5
    .line 2928
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_8

    #@7
    .line 2941
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2932
    :cond_8
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@b
    move-result-object v3

    #@c
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v1

    #@10
    .local v1, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_22

    #@16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;

    #@1c
    .line 2933
    .local v2, servInfo:Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@1e
    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiNative;->p2pServiceDel(Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z

    #@21
    goto :goto_10

    #@22
    .line 2936
    .end local v2           #servInfo:Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;
    :cond_22
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@25
    move-result-object v3

    #@26
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@29
    .line 2937
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@30
    move-result v3

    #@31
    if-nez v3, :cond_7

    #@33
    .line 2938
    const-string/jumbo v3, "remove client information from framework"

    #@36
    invoke-direct {p0, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@39
    .line 2939
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3b
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    goto :goto_7
.end method

.method private clearServiceRequests(Landroid/os/Messenger;)V
    .registers 5
    .parameter "m"

    #@0
    .prologue
    .line 2869
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@4
    move-result-object v0

    #@5
    .line 2870
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_8

    #@7
    .line 2890
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2874
    :cond_8
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_7

    #@12
    .line 2878
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    #@19
    .line 2880
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@1c
    move-result-object v1

    #@1d
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_36

    #@23
    .line 2881
    const-string/jumbo v1, "remove channel information from framework"

    #@26
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@29
    .line 2882
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2b
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 2885
    :cond_36
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@38
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    if-eqz v1, :cond_7

    #@3e
    .line 2889
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updateSupplicantServiceRequest()Z

    #@41
    goto :goto_7
.end method

.method private clearSupplicantServiceRequest()V
    .registers 3

    #@0
    .prologue
    .line 2807
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 2811
    :goto_8
    return-void

    #@9
    .line 2809
    :cond_9
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@b
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->p2pServDiscCancelReq(Ljava/lang/String;)Z

    #@14
    .line 2810
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-static {v0, v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$6002(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    goto :goto_8
.end method

.method private connect(Landroid/net/wifi/p2p/WifiP2pConfig;Z)I
    .registers 13
    .parameter "config"
    .parameter "tryInvocation"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v7, -0x1

    #@3
    .line 2303
    if-nez p1, :cond_c

    #@5
    .line 2304
    const-string v5, "config is null"

    #@7
    invoke-direct {p0, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@a
    move v6, v7

    #@b
    .line 2384
    :cond_b
    :goto_b
    return v6

    #@c
    .line 2308
    :cond_c
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@e
    invoke-static {v8}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getBtWifiState()Z

    #@15
    move-result v8

    #@16
    if-eqz v8, :cond_1d

    #@18
    .line 2309
    const-string v8, "BT and WiFi are connected."

    #@1a
    invoke-direct {p0, v8}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@1d
    .line 2313
    :cond_1d
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@1f
    if-eqz v8, :cond_55

    #@21
    iget-object v8, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@23
    iget-object v9, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@25
    iget-object v9, v9, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v8

    #@2b
    if-eqz v8, :cond_55

    #@2d
    move v1, v5

    #@2e
    .line 2315
    .local v1, isResp:Z
    :goto_2e
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@30
    .line 2317
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@32
    iget-object v9, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@34
    invoke-virtual {v8, v9}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@37
    move-result-object v0

    #@38
    .line 2318
    .local v0, dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    if-nez v0, :cond_57

    #@3a
    .line 2319
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string/jumbo v6, "target device not found "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    iget-object v6, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v5

    #@50
    invoke-direct {p0, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@53
    move v6, v7

    #@54
    .line 2320
    goto :goto_b

    #@55
    .end local v0           #dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v1           #isResp:Z
    :cond_55
    move v1, v6

    #@56
    .line 2313
    goto :goto_2e

    #@57
    .line 2323
    .restart local v0       #dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    .restart local v1       #isResp:Z
    :cond_57
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    #@5a
    move-result v2

    #@5b
    .line 2324
    .local v2, join:Z
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@5d
    iget-object v9, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@5f
    invoke-virtual {v8, v9}, Landroid/net/wifi/WifiNative;->p2pGetSsid(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    .line 2325
    .local v4, ssid:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string/jumbo v9, "target ssid is "

    #@6b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v8

    #@6f
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v8

    #@73
    const-string v9, " join:"

    #@75
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v8

    #@81
    invoke-direct {p0, v8}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@84
    .line 2327
    if-eqz v2, :cond_a4

    #@86
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupLimit()Z

    #@89
    move-result v8

    #@8a
    if-eqz v8, :cond_a4

    #@8c
    .line 2328
    const-string/jumbo v8, "target device reaches group limit."

    #@8f
    invoke-direct {p0, v8}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@92
    .line 2332
    const/4 v2, 0x0

    #@93
    .line 2344
    :cond_93
    if-nez v2, :cond_bb

    #@95
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDevice;->isDeviceLimit()Z

    #@98
    move-result v8

    #@99
    if-eqz v8, :cond_bb

    #@9b
    .line 2345
    const-string/jumbo v5, "target device reaches the device limit."

    #@9e
    invoke-direct {p0, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@a1
    move v6, v7

    #@a2
    .line 2346
    goto/16 :goto_b

    #@a4
    .line 2333
    :cond_a4
    if-eqz v2, :cond_93

    #@a6
    .line 2334
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@a8
    iget-object v9, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@aa
    invoke-virtual {v8, v9, v4}, Landroid/net/wifi/p2p/WifiP2pGroupList;->getNetworkId(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    move-result v3

    #@ae
    .line 2335
    .local v3, netId:I
    if-ltz v3, :cond_93

    #@b0
    .line 2337
    iget-object v5, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@b2
    invoke-virtual {v5, v3}, Landroid/net/wifi/WifiNative;->p2pGroupAdd(I)Z

    #@b5
    move-result v5

    #@b6
    if-nez v5, :cond_b

    #@b8
    move v6, v7

    #@b9
    .line 2338
    goto/16 :goto_b

    #@bb
    .line 2349
    .end local v3           #netId:I
    :cond_bb
    if-nez v2, :cond_133

    #@bd
    if-eqz p2, :cond_133

    #@bf
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDevice;->isInvitationCapable()Z

    #@c2
    move-result v7

    #@c3
    if-eqz v7, :cond_133

    #@c5
    .line 2350
    const/4 v3, -0x2

    #@c6
    .line 2351
    .restart local v3       #netId:I
    iget v7, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@c8
    if-ltz v7, :cond_119

    #@ca
    .line 2352
    iget-object v7, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@cc
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@ce
    iget v9, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@d0
    invoke-virtual {v8, v9}, Landroid/net/wifi/p2p/WifiP2pGroupList;->getOwnerAddr(I)Ljava/lang/String;

    #@d3
    move-result-object v8

    #@d4
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d7
    move-result v7

    #@d8
    if-eqz v7, :cond_dc

    #@da
    .line 2353
    iget v3, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@dc
    .line 2358
    :cond_dc
    :goto_dc
    if-gez v3, :cond_e4

    #@de
    .line 2359
    iget-object v7, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@e0
    invoke-direct {p0, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getNetworkIdFromClientList(Ljava/lang/String;)I

    #@e3
    move-result v3

    #@e4
    .line 2361
    :cond_e4
    new-instance v7, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string/jumbo v8, "netId related with "

    #@ec
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v7

    #@f0
    iget-object v8, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@f2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v7

    #@f6
    const-string v8, " = "

    #@f8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v7

    #@fc
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v7

    #@100
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v7

    #@104
    invoke-direct {p0, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@107
    .line 2362
    if-ltz v3, :cond_133

    #@109
    .line 2364
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@10b
    iget-object v8, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@10d
    invoke-virtual {v7, v3, v8}, Landroid/net/wifi/WifiNative;->p2pReinvoke(ILjava/lang/String;)Z

    #@110
    move-result v7

    #@111
    if-eqz v7, :cond_122

    #@113
    .line 2366
    iget-object v5, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@115
    iput v3, v5, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@117
    goto/16 :goto_b

    #@119
    .line 2356
    :cond_119
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@11b
    iget-object v8, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@11d
    invoke-virtual {v7, v8}, Landroid/net/wifi/p2p/WifiP2pGroupList;->getNetworkId(Ljava/lang/String;)I

    #@120
    move-result v3

    #@121
    goto :goto_dc

    #@122
    .line 2369
    :cond_122
    const-string/jumbo v7, "p2pReinvoke() failed, update networks"

    #@125
    invoke-direct {p0, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@128
    .line 2370
    invoke-static {}, Landroid/net/wifi/p2p/WifiP2pService;->access$12700()Ljava/lang/Boolean;

    #@12b
    move-result-object v7

    #@12c
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    #@12f
    move-result v7

    #@130
    invoke-direct {p0, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updatePersistentNetworks(Z)V

    #@133
    .line 2377
    .end local v3           #netId:I
    :cond_133
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@135
    invoke-virtual {v7}, Landroid/net/wifi/WifiNative;->p2pStopFind()Z

    #@138
    .line 2379
    if-nez v1, :cond_13d

    #@13a
    move v6, v5

    #@13b
    .line 2380
    goto/16 :goto_b

    #@13d
    .line 2383
    :cond_13d
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->p2pConnectWithPinDisplay(Landroid/net/wifi/p2p/WifiP2pConfig;)V

    #@140
    goto/16 :goto_b
.end method

.method private getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    .registers 6
    .parameter "m"
    .parameter "createIfNotExist"

    #@0
    .prologue
    .line 3010
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@c
    .line 3012
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_26

    #@e
    if-eqz p2, :cond_26

    #@10
    .line 3013
    const-string v1, "add a new client"

    #@12
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@15
    .line 3014
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@17
    .end local v0           #clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-direct {v0, v1, p1, v2}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;-><init>(Landroid/net/wifi/p2p/WifiP2pService;Landroid/os/Messenger;Landroid/net/wifi/p2p/WifiP2pService$1;)V

    #@1d
    .line 3015
    .restart local v0       #clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1f
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    .line 3018
    :cond_26
    return-object v0
.end method

.method private getClientList(I)[Ljava/lang/String;
    .registers 5
    .parameter "netId"

    #@0
    .prologue
    .line 2419
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    const-string/jumbo v2, "p2p_client_list"

    #@5
    invoke-virtual {v1, p1, v2}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 2420
    .local v0, p2pClients:Ljava/lang/String;
    if-nez v0, :cond_d

    #@b
    .line 2421
    const/4 v1, 0x0

    #@c
    .line 2423
    :goto_c
    return-object v1

    #@d
    :cond_d
    const-string v1, " "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    goto :goto_c
.end method

.method private getDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 2482
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@2
    invoke-virtual {v1, p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@5
    move-result-object v0

    #@6
    .line 2483
    .local v0, d:Landroid/net/wifi/p2p/WifiP2pDevice;
    if-eqz v0, :cond_a

    #@8
    .line 2484
    iget-object p1, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@a
    .line 2487
    .end local p1
    :cond_a
    return-object p1
.end method

.method private getNetworkIdFromClientList(Ljava/lang/String;)I
    .registers 13
    .parameter "deviceAddress"

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    .line 2397
    if-nez p1, :cond_5

    #@3
    move v7, v9

    #@4
    .line 2410
    :cond_4
    :goto_4
    return v7

    #@5
    .line 2399
    :cond_5
    iget-object v10, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@7
    invoke-virtual {v10}, Landroid/net/wifi/p2p/WifiP2pGroupList;->getGroupList()Ljava/util/Collection;

    #@a
    move-result-object v3

    #@b
    .line 2400
    .local v3, groups:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pGroup;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v4

    #@f
    :cond_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v10

    #@13
    if-eqz v10, :cond_35

    #@15
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@1b
    .line 2401
    .local v2, group:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {v2}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    #@1e
    move-result v7

    #@1f
    .line 2402
    .local v7, netId:I
    invoke-direct {p0, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientList(I)[Ljava/lang/String;

    #@22
    move-result-object v8

    #@23
    .line 2403
    .local v8, p2pClientList:[Ljava/lang/String;
    if-eqz v8, :cond_f

    #@25
    .line 2404
    move-object v0, v8

    #@26
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@27
    .local v6, len$:I
    const/4 v5, 0x0

    #@28
    .local v5, i$:I
    :goto_28
    if-ge v5, v6, :cond_f

    #@2a
    aget-object v1, v0, v5

    #@2c
    .line 2405
    .local v1, client:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f
    move-result v10

    #@30
    if-nez v10, :cond_4

    #@32
    .line 2404
    add-int/lit8 v5, v5, 0x1

    #@34
    goto :goto_28

    #@35
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #client:Ljava/lang/String;
    .end local v2           #group:Landroid/net/wifi/p2p/WifiP2pGroup;
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v7           #netId:I
    .end local v8           #p2pClientList:[Ljava/lang/String;
    :cond_35
    move v7, v9

    #@36
    .line 2410
    goto :goto_4
.end method

.method private getPersistedDeviceName()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 2519
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v3

    #@a
    const-string/jumbo v4, "lg_device_name"

    #@d
    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 2525
    .local v1, deviceName:Ljava/lang/String;
    if-nez v1, :cond_5a

    #@13
    .line 2526
    const-string/jumbo v3, "lg_device_name DB value is null. Use original DB"

    #@16
    invoke-direct {p0, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@19
    .line 2527
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1b
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v3

    #@23
    const-string/jumbo v4, "wifi_p2p_device_name"

    #@26
    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 2529
    if-nez v1, :cond_5a

    #@2c
    .line 2531
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@2e
    .line 2532
    .local v0, defaultName:Ljava/lang/String;
    if-nez v0, :cond_59

    #@30
    .line 2534
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@32
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@39
    move-result-object v3

    #@3a
    const-string v4, "android_id"

    #@3c
    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    .line 2536
    .local v2, id:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v4, "Android_"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const/4 v4, 0x0

    #@4c
    const/4 v5, 0x4

    #@4d
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .line 2553
    .end local v0           #defaultName:Ljava/lang/String;
    .end local v2           #id:Ljava/lang/String;
    :cond_59
    :goto_59
    return-object v0

    #@5a
    :cond_5a
    move-object v0, v1

    #@5b
    goto :goto_59
.end method

.method private handleGroupCreationFailure()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2660
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@6
    move-result-object v0

    #@7
    if-eqz v0, :cond_23

    #@9
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@b
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_23

    #@15
    .line 2661
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@17
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@1e
    .line 2662
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@20
    invoke-static {v0, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$3502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@23
    .line 2665
    :cond_23
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->resetWifiP2pInfo()V

    #@26
    .line 2666
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@28
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$3100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/NetworkInfo;

    #@2b
    move-result-object v0

    #@2c
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@2e
    invoke-virtual {v0, v1, v2, v2}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 2667
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pConnectionChangedBroadcast()V

    #@34
    .line 2668
    iput-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@36
    .line 2671
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@38
    invoke-virtual {v0}, Landroid/net/wifi/WifiNative;->p2pFlush()Z

    #@3b
    .line 2672
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@3d
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@3f
    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->remove(Landroid/net/wifi/p2p/WifiP2pDeviceList;)Z

    #@42
    move-result v0

    #@43
    if-eqz v0, :cond_48

    #@45
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pPeersChangedBroadcast()V

    #@48
    .line 2673
    :cond_48
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@4a
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->clear()Z

    #@4d
    .line 2674
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4f
    invoke-static {v0, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$6002(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@52
    .line 2675
    const v0, 0x22001

    #@55
    invoke-virtual {p0, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendMessage(I)V

    #@58
    .line 2676
    return-void
.end method

.method private handleGroupRemoved()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x0

    #@2
    const/4 v9, 0x0

    #@3
    .line 2679
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@5
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    #@8
    move-result-object v2

    #@9
    .line 2680
    .local v2, devices:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    const/4 v0, 0x0

    #@a
    .line 2681
    .local v0, changed:Z
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@c
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@f
    move-result-object v6

    #@10
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v4

    #@14
    .local v4, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_36

    #@1a
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@20
    .line 2682
    .local v1, d:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-interface {v2, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v6

    #@24
    if-nez v6, :cond_32

    #@26
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@28
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6, v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v6

    #@30
    if-eqz v6, :cond_14

    #@32
    .line 2683
    :cond_32
    iput v11, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@34
    .line 2684
    const/4 v0, 0x1

    #@35
    goto :goto_14

    #@36
    .line 2688
    .end local v1           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_36
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@38
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    #@3b
    move-result v6

    #@3c
    if-eqz v6, :cond_b3

    #@3e
    .line 2689
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@40
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-direct {p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->stopDhcpServer(Ljava/lang/String;)V

    #@47
    .line 2691
    new-instance v5, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@49
    invoke-direct {v5}, Landroid/net/wifi/p2p/WifiP2pDeviceList;-><init>()V

    #@4c
    .line 2692
    .local v5, mPeersTemp:Landroid/net/wifi/p2p/WifiP2pDeviceList;
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@4e
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@51
    move-result-object v6

    #@52
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@55
    move-result-object v4

    #@56
    :cond_56
    :goto_56
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v6

    #@5a
    if-eqz v6, :cond_99

    #@5c
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f
    move-result-object v1

    #@60
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@62
    .line 2693
    .restart local v1       #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@64
    if-eqz v6, :cond_56

    #@66
    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@68
    const-string v7, ""

    #@6a
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v6

    #@6e
    if-nez v6, :cond_7a

    #@70
    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@72
    iget-object v7, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@74
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v6

    #@78
    if-eqz v6, :cond_56

    #@7a
    .line 2694
    :cond_7a
    invoke-virtual {v5, v1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->update(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@7d
    .line 2695
    const-string v6, "WifiP2pService"

    #@7f
    new-instance v7, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v8, "[LGE PATCH] REMOVE_GROUP_EVENT legacy removed:"

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v7

    #@8a
    iget-object v8, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@8c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v7

    #@90
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v7

    #@94
    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 2696
    const/4 v0, 0x1

    #@98
    goto :goto_56

    #@99
    .line 2699
    .end local v1           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_99
    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@9c
    move-result-object v6

    #@9d
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a0
    move-result-object v4

    #@a1
    :goto_a1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@a4
    move-result v6

    #@a5
    if-eqz v6, :cond_d3

    #@a7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@aa
    move-result-object v1

    #@ab
    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@ad
    .restart local v1       #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@af
    invoke-virtual {v6, v1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->remove(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    #@b2
    goto :goto_a1

    #@b3
    .line 2703
    .end local v1           #d:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v5           #mPeersTemp:Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :cond_b3
    const-string/jumbo v6, "stop DHCP client"

    #@b6
    invoke-direct {p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@b9
    .line 2704
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@bb
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;

    #@be
    move-result-object v6

    #@bf
    const v7, 0x30002

    #@c2
    invoke-virtual {v6, v7}, Landroid/net/DhcpStateMachine;->sendMessage(I)V

    #@c5
    .line 2705
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@c7
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;

    #@ca
    move-result-object v6

    #@cb
    invoke-virtual {v6}, Landroid/net/DhcpStateMachine;->doQuit()V

    #@ce
    .line 2706
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d0
    invoke-static {v6, v10}, Landroid/net/wifi/p2p/WifiP2pService;->access$8902(Landroid/net/wifi/p2p/WifiP2pService;Landroid/net/DhcpStateMachine;)Landroid/net/DhcpStateMachine;

    #@d3
    .line 2710
    :cond_d3
    :try_start_d3
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d5
    iget-object v6, v6, Landroid/net/wifi/p2p/WifiP2pService;->mNwService:Landroid/os/INetworkManagementService;

    #@d7
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@d9
    invoke-virtual {v7}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@dc
    move-result-object v7

    #@dd
    invoke-interface {v6, v7}, Landroid/os/INetworkManagementService;->clearInterfaceAddresses(Ljava/lang/String;)V
    :try_end_e0
    .catch Ljava/lang/Exception; {:try_start_d3 .. :try_end_e0} :catch_131

    #@e0
    .line 2714
    :goto_e0
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@e2
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@e5
    move-result-object v6

    #@e6
    invoke-static {v6, v11}, Landroid/net/NetworkUtils;->resetConnections(Ljava/lang/String;I)I

    #@e9
    .line 2718
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@eb
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@ed
    invoke-virtual {v7}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@f0
    move-result-object v7

    #@f1
    invoke-virtual {v6, v7, v9}, Landroid/net/wifi/WifiNative;->setP2pGroupIdle(Ljava/lang/String;I)Z

    #@f4
    .line 2720
    iput-object v10, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@f6
    .line 2721
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@f8
    invoke-virtual {v6}, Landroid/net/wifi/WifiNative;->p2pFlush()Z

    #@fb
    .line 2722
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@fd
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@ff
    invoke-virtual {v6, v7}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->remove(Landroid/net/wifi/p2p/WifiP2pDeviceList;)Z

    #@102
    move-result v6

    #@103
    if-eqz v6, :cond_108

    #@105
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pPeersChangedBroadcast()V

    #@108
    .line 2723
    :cond_108
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeersLostDuringConnection:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@10a
    invoke-virtual {v6}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->clear()Z

    #@10d
    .line 2724
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@10f
    invoke-static {v6, v10}, Landroid/net/wifi/p2p/WifiP2pService;->access$6002(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@112
    .line 2725
    if-eqz v0, :cond_117

    #@114
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pPeersChangedBroadcast()V

    #@117
    .line 2727
    :cond_117
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@119
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$10700(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@11c
    move-result v6

    #@11d
    if-eqz v6, :cond_130

    #@11f
    .line 2728
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@121
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$600(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;

    #@124
    move-result-object v6

    #@125
    const v7, 0x2300c

    #@128
    invoke-virtual {v6, v7, v9}, Lcom/android/internal/util/AsyncChannel;->sendMessage(II)V

    #@12b
    .line 2729
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@12d
    invoke-static {v6, v9}, Landroid/net/wifi/p2p/WifiP2pService;->access$10702(Landroid/net/wifi/p2p/WifiP2pService;Z)Z

    #@130
    .line 2731
    :cond_130
    return-void

    #@131
    .line 2711
    :catch_131
    move-exception v3

    #@132
    .line 2712
    .local v3, e:Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    #@134
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@137
    const-string v7, "Failed to clear addresses "

    #@139
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v6

    #@13d
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v6

    #@141
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v6

    #@145
    invoke-direct {p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@148
    goto :goto_e0
.end method

.method private initializeP2pSettings()V
    .registers 5

    #@0
    .prologue
    .line 2615
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setPersistentReconnect(Z)Z

    #@6
    .line 2616
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@8
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@b
    move-result-object v1

    #@c
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getPersistedDeviceName()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    iput-object v2, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@12
    .line 2617
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@14
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@16
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@19
    move-result-object v2

    #@1a
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@1c
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setDeviceName(Ljava/lang/String;)Z

    #@1f
    .line 2620
    const-string v1, "check postfix"

    #@21
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@24
    .line 2621
    const/4 v0, 0x0

    #@25
    .line 2622
    .local v0, postfix:Ljava/lang/String;
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@27
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2a
    move-result-object v1

    #@2b
    if-eqz v1, :cond_73

    #@2d
    .line 2623
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "before postfix = "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3a
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@3d
    move-result-object v2

    #@3e
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@4b
    .line 2624
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4d
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@50
    move-result-object v1

    #@51
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@53
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@56
    move-result-object v2

    #@57
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@59
    invoke-interface {v1, v2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->checkP2pPostfix(Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    .line 2625
    new-instance v1, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v2, "after postfix = "

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@73
    .line 2627
    :cond_73
    if-eqz v0, :cond_107

    #@75
    .line 2628
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@77
    new-instance v2, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v3, "-"

    #@7e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v2

    #@8a
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setP2pSsidPostfix(Ljava/lang/String;)Z

    #@8d
    .line 2633
    :goto_8d
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@8f
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@91
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@94
    move-result-object v2

    #@95
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    #@97
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setDeviceType(Ljava/lang/String;)Z

    #@9a
    .line 2636
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@9c
    const-string/jumbo v2, "virtual_push_button physical_display keypad"

    #@9f
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setConfigMethods(Ljava/lang/String;)Z

    #@a2
    .line 2638
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@a4
    const-string/jumbo v2, "p2p"

    #@a7
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setConcurrencyPriority(Ljava/lang/String;)Z

    #@aa
    .line 2640
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@ac
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@af
    move-result-object v1

    #@b0
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@b2
    invoke-virtual {v2}, Landroid/net/wifi/WifiNative;->p2pGetDeviceAddress()Ljava/lang/String;

    #@b5
    move-result-object v2

    #@b6
    iput-object v2, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@b8
    .line 2641
    const/4 v1, 0x3

    #@b9
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updateThisDevice(I)V

    #@bc
    .line 2642
    const-string v1, "WifiP2pService"

    #@be
    new-instance v2, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v3, "DeviceAddress: "

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@cb
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@ce
    move-result-object v3

    #@cf
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@d1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v2

    #@d5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v2

    #@d9
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 2644
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@de
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@e1
    move-result-object v1

    #@e2
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    #@e5
    .line 2645
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@e7
    invoke-virtual {v1}, Landroid/net/wifi/WifiNative;->p2pFlush()Z

    #@ea
    .line 2646
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@ec
    invoke-virtual {v1}, Landroid/net/wifi/WifiNative;->p2pServiceFlush()Z

    #@ef
    .line 2647
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@f1
    const/4 v2, 0x0

    #@f2
    invoke-static {v1, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$12902(Landroid/net/wifi/p2p/WifiP2pService;B)B

    #@f5
    .line 2648
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@f7
    const/4 v2, 0x0

    #@f8
    invoke-static {v1, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$6002(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@fb
    .line 2650
    invoke-static {}, Landroid/net/wifi/p2p/WifiP2pService;->access$12700()Ljava/lang/Boolean;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@102
    move-result v1

    #@103
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updatePersistentNetworks(Z)V

    #@106
    .line 2651
    return-void

    #@107
    .line 2630
    :cond_107
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@109
    new-instance v2, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v3, "-"

    #@110
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v2

    #@114
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@116
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@119
    move-result-object v3

    #@11a
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@11c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v2

    #@120
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v2

    #@124
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setP2pSsidPostfix(Ljava/lang/String;)Z

    #@127
    goto/16 :goto_8d
.end method

.method private isForegroundApp(Ljava/lang/String;)Z
    .registers 6
    .parameter "pkgName"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3144
    if-nez p1, :cond_4

    #@3
    .line 3151
    :cond_3
    :goto_3
    return v1

    #@4
    .line 3146
    :cond_4
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@6
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13700(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/ActivityManager;

    #@9
    move-result-object v2

    #@a
    const/4 v3, 0x1

    #@b
    invoke-virtual {v2, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    #@e
    move-result-object v0

    #@f
    .line 3147
    .local v0, tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_3

    #@15
    .line 3151
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    #@1b
    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    #@1d
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v1

    #@25
    goto :goto_3
.end method

.method private logd(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 2767
    const-string v0, "WifiP2pService"

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 2768
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 2771
    const-string v0, "WifiP2pService"

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 2772
    return-void
.end method

.method private notifyInvitationReceived()V
    .registers 13

    #@0
    .prologue
    const v11, 0x10203c0

    #@3
    const/4 v10, 0x0

    #@4
    .line 2133
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@7
    move-result-object v3

    #@8
    .line 2134
    .local v3, r:Landroid/content/res/Resources;
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@a
    iget-object v5, v6, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@c
    .line 2135
    .local v5, wps:Landroid/net/wifi/WpsInfo;
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@e
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@11
    move-result-object v6

    #@12
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@15
    move-result-object v6

    #@16
    const v7, 0x10900ec

    #@19
    const/4 v8, 0x0

    #@1a
    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1d
    move-result-object v4

    #@1e
    .line 2138
    .local v4, textEntryView:Landroid/view/View;
    const v6, 0x102033a

    #@21
    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/view/ViewGroup;

    #@27
    .line 2139
    .local v0, group:Landroid/view/ViewGroup;
    const v6, 0x104043e

    #@2a
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mSavedPeerConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    #@2c
    iget-object v7, v7, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@2e
    invoke-direct {p0, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    invoke-direct {p0, v0, v6, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addRowToDialog(Landroid/view/ViewGroup;ILjava/lang/String;)V

    #@35
    .line 2142
    const v6, 0x10203c1

    #@38
    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3b
    move-result-object v1

    #@3c
    check-cast v1, Landroid/widget/EditText;

    #@3e
    .line 2143
    .local v1, pin:Landroid/widget/EditText;
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@40
    if-eqz v6, :cond_47

    #@42
    .line 2144
    const-string v6, "DISABLE_BUBBLE_POPUP"

    #@44
    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    #@47
    .line 2147
    :cond_47
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@49
    new-instance v7, Landroid/app/AlertDialog$Builder;

    #@4b
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4d
    invoke-static {v8}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@50
    move-result-object v8

    #@51
    invoke-direct {v7, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@54
    const v8, 0x104043d

    #@57
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v8

    #@5b
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@62
    move-result-object v7

    #@63
    const v8, 0x104043a

    #@66
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v8

    #@6a
    new-instance v9, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$4;

    #@6c
    invoke-direct {v9, p0, v5, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$4;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;Landroid/net/wifi/WpsInfo;Landroid/widget/EditText;)V

    #@6f
    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@72
    move-result-object v7

    #@73
    const v8, 0x104043b

    #@76
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@79
    move-result-object v8

    #@7a
    new-instance v9, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$3;

    #@7c
    invoke-direct {v9, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$3;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@7f
    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@82
    move-result-object v7

    #@83
    new-instance v8, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$2;

    #@85
    invoke-direct {v8, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$2;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@88
    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@8f
    move-result-object v7

    #@90
    invoke-static {v6, v7}, Landroid/net/wifi/p2p/WifiP2pService;->access$3502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    #@93
    .line 2182
    iget v6, v5, Landroid/net/wifi/WpsInfo;->setup:I

    #@95
    packed-switch v6, :pswitch_data_104

    #@98
    .line 2196
    :goto_98
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@9a
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@a1
    move-result-object v6

    #@a2
    const/16 v7, 0x7d3

    #@a4
    invoke-virtual {v6, v7}, Landroid/view/Window;->setType(I)V

    #@a7
    .line 2197
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@a9
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@ac
    move-result-object v6

    #@ad
    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    #@b0
    .line 2200
    iget v6, v5, Landroid/net/wifi/WpsInfo;->setup:I

    #@b2
    const/4 v7, 0x2

    #@b3
    if-ne v6, v7, :cond_d9

    #@b5
    invoke-virtual {v4, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@b8
    move-result-object v6

    #@b9
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    #@bc
    move-result v6

    #@bd
    const/16 v7, 0x8

    #@bf
    if-eq v6, v7, :cond_d9

    #@c1
    .line 2201
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@c3
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@c6
    move-result-object v6

    #@c7
    const/4 v7, -0x1

    #@c8
    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    #@cb
    move-result-object v2

    #@cc
    .line 2202
    .local v2, positiveButton:Landroid/widget/Button;
    if-eqz v2, :cond_d1

    #@ce
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setEnabled(Z)V

    #@d1
    .line 2203
    :cond_d1
    new-instance v6, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$5;

    #@d3
    invoke-direct {v6, p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine$5;-><init>(Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;)V

    #@d6
    invoke-virtual {v1, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@d9
    .line 2216
    .end local v2           #positiveButton:Landroid/widget/Button;
    :cond_d9
    return-void

    #@da
    .line 2184
    :pswitch_da
    const-string v6, "Enter pin section visible"

    #@dc
    invoke-direct {p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@df
    .line 2185
    invoke-virtual {v4, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@e2
    move-result-object v6

    #@e3
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    #@e6
    .line 2186
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@e8
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;

    #@eb
    move-result-object v6

    #@ec
    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@ef
    move-result-object v6

    #@f0
    const/4 v7, 0x4

    #@f1
    invoke-virtual {v6, v7}, Landroid/view/Window;->setSoftInputMode(I)V

    #@f4
    goto :goto_98

    #@f5
    .line 2189
    :pswitch_f5
    const-string v6, "Shown pin section visible"

    #@f7
    invoke-direct {p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@fa
    .line 2190
    const v6, 0x1040441

    #@fd
    iget-object v7, v5, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@ff
    invoke-direct {p0, v0, v6, v7}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addRowToDialog(Landroid/view/ViewGroup;ILjava/lang/String;)V

    #@102
    goto :goto_98

    #@103
    .line 2182
    nop

    #@104
    :pswitch_data_104
    .packed-switch 0x1
        :pswitch_f5
        :pswitch_da
    .end packed-switch
.end method

.method private notifyInvitationSent(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "pin"
    .parameter "peerAddress"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2114
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@4
    move-result-object v2

    #@5
    .line 2116
    .local v2, r:Landroid/content/res/Resources;
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@7
    invoke-static {v4}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@a
    move-result-object v4

    #@b
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@e
    move-result-object v4

    #@f
    const v5, 0x10900ec

    #@12
    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@15
    move-result-object v3

    #@16
    .line 2119
    .local v3, textEntryView:Landroid/view/View;
    const v4, 0x102033a

    #@19
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/view/ViewGroup;

    #@1f
    .line 2120
    .local v1, group:Landroid/view/ViewGroup;
    const v4, 0x104043f

    #@22
    invoke-direct {p0, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-direct {p0, v1, v4, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addRowToDialog(Landroid/view/ViewGroup;ILjava/lang/String;)V

    #@29
    .line 2121
    const v4, 0x1040441

    #@2c
    invoke-direct {p0, v1, v4, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->addRowToDialog(Landroid/view/ViewGroup;ILjava/lang/String;)V

    #@2f
    .line 2123
    new-instance v4, Landroid/app/AlertDialog$Builder;

    #@31
    iget-object v5, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@33
    invoke-static {v5}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@36
    move-result-object v5

    #@37
    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@3a
    const v5, 0x104043c

    #@3d
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@48
    move-result-object v4

    #@49
    const v5, 0x104000a

    #@4c
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@57
    move-result-object v0

    #@58
    .line 2128
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@5b
    move-result-object v4

    #@5c
    const/16 v5, 0x7d3

    #@5e
    invoke-virtual {v4, v5}, Landroid/view/Window;->setType(I)V

    #@61
    .line 2129
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@64
    .line 2130
    return-void
.end method

.method private notifyP2pEnableFailure()V
    .registers 6

    #@0
    .prologue
    .line 2094
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    .line 2095
    .local v1, r:Landroid/content/res/Resources;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    #@6
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@8
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@b
    move-result-object v3

    #@c
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@f
    const v3, 0x1040435

    #@12
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@19
    move-result-object v2

    #@1a
    const v3, 0x1040437

    #@1d
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@24
    move-result-object v2

    #@25
    const v3, 0x104000a

    #@28
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    const/4 v4, 0x0

    #@2d
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@34
    move-result-object v0

    #@35
    .line 2100
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@38
    move-result-object v2

    #@39
    const/16 v3, 0x7d3

    #@3b
    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    #@3e
    .line 2101
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@41
    .line 2102
    return-void
.end method

.method private obtainMessage(Landroid/os/Message;)Landroid/os/Message;
    .registers 4
    .parameter "srcMsg"

    #@0
    .prologue
    .line 2761
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2762
    .local v0, msg:Landroid/os/Message;
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@6
    iput v1, v0, Landroid/os/Message;->arg2:I

    #@8
    .line 2763
    return-object v0
.end method

.method private p2pConnectWithPinDisplay(Landroid/net/wifi/p2p/WifiP2pConfig;)V
    .registers 6
    .parameter "config"

    #@0
    .prologue
    .line 2491
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mPeers:Landroid/net/wifi/p2p/WifiP2pDeviceList;

    #@2
    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@4
    invoke-virtual {v2, v3}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@7
    move-result-object v0

    #@8
    .line 2492
    .local v0, dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    if-nez v0, :cond_24

    #@a
    .line 2493
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string/jumbo v3, "target device is not found "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@23
    .line 2515
    :cond_23
    :goto_23
    return-void

    #@24
    .line 2497
    :cond_24
    const/4 v1, 0x0

    #@25
    .line 2498
    .local v1, pin:Ljava/lang/String;
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@27
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$6500(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_4b

    #@2d
    .line 2499
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2f
    const/4 v3, 0x1

    #@30
    invoke-virtual {v2, p1, v3}, Landroid/net/wifi/WifiNative;->p2pConnect(Landroid/net/wifi/p2p/WifiP2pConfig;Z)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 2500
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@36
    const/4 v3, 0x0

    #@37
    invoke-static {v2, v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$6502(Landroid/net/wifi/p2p/WifiP2pService;Z)Z

    #@3a
    .line 2508
    :goto_3a
    :try_start_3a
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3d
    .line 2509
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendShowPinReqToFrontApp(Ljava/lang/String;)Z

    #@40
    move-result v2

    #@41
    if-nez v2, :cond_23

    #@43
    .line 2510
    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@45
    invoke-direct {p0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->notifyInvitationSent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_48
    .catch Ljava/lang/NumberFormatException; {:try_start_3a .. :try_end_48} :catch_49

    #@48
    goto :goto_23

    #@49
    .line 2512
    :catch_49
    move-exception v2

    #@4a
    goto :goto_23

    #@4b
    .line 2502
    :cond_4b
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@4d
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    #@50
    move-result v3

    #@51
    invoke-virtual {v2, p1, v3}, Landroid/net/wifi/WifiNative;->p2pConnect(Landroid/net/wifi/p2p/WifiP2pConfig;Z)Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    goto :goto_3a
.end method

.method private removeClientFromList(ILjava/lang/String;Z)Z
    .registers 15
    .parameter "netId"
    .parameter "addr"
    .parameter "isRemovable"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 2434
    new-instance v6, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    .line 2435
    .local v6, modifiedClientList:Ljava/lang/StringBuilder;
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientList(I)[Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 2436
    .local v2, currentClientList:[Ljava/lang/String;
    const/4 v4, 0x0

    #@b
    .line 2437
    .local v4, isClientRemoved:Z
    if-eqz v2, :cond_27

    #@d
    .line 2438
    move-object v0, v2

    #@e
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@f
    .local v5, len$:I
    const/4 v3, 0x0

    #@10
    .local v3, i$:I
    :goto_10
    if-ge v3, v5, :cond_27

    #@12
    aget-object v1, v0, v3

    #@14
    .line 2439
    .local v1, client:Ljava/lang/String;
    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17
    move-result v8

    #@18
    if-nez v8, :cond_25

    #@1a
    .line 2440
    const-string v8, " "

    #@1c
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 2441
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 2438
    :goto_22
    add-int/lit8 v3, v3, 0x1

    #@24
    goto :goto_10

    #@25
    .line 2443
    :cond_25
    const/4 v4, 0x1

    #@26
    goto :goto_22

    #@27
    .line 2447
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #client:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    :cond_27
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    #@2a
    move-result v8

    #@2b
    if-nez v8, :cond_3a

    #@2d
    if-eqz p3, :cond_3a

    #@2f
    .line 2449
    const-string v8, "Remove unknown network"

    #@31
    invoke-direct {p0, v8}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@34
    .line 2450
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@36
    invoke-virtual {v8, p1}, Landroid/net/wifi/p2p/WifiP2pGroupList;->remove(I)V

    #@39
    .line 2466
    :goto_39
    return v7

    #@3a
    .line 2454
    :cond_3a
    if-nez v4, :cond_3e

    #@3c
    .line 2456
    const/4 v7, 0x0

    #@3d
    goto :goto_39

    #@3e
    .line 2459
    :cond_3e
    new-instance v8, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v9, "Modified client list: "

    #@45
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-direct {p0, v8}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@54
    .line 2460
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    #@57
    move-result v8

    #@58
    if-nez v8, :cond_5f

    #@5a
    .line 2461
    const-string v8, "\"\""

    #@5c
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    .line 2463
    :cond_5f
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@61
    const-string/jumbo v9, "p2p_client_list"

    #@64
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v10

    #@68
    invoke-virtual {v8, p1, v9, v10}, Landroid/net/wifi/WifiNative;->setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z

    #@6b
    .line 2465
    iget-object v8, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@6d
    invoke-virtual {v8}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@70
    goto :goto_39
.end method

.method private removeLocalService(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)V
    .registers 6
    .parameter "m"
    .parameter "servInfo"

    #@0
    .prologue
    .line 2912
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@4
    move-result-object v0

    #@5
    .line 2913
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_8

    #@7
    .line 2924
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2917
    :cond_8
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@a
    invoke-virtual {v1, p2}, Landroid/net/wifi/WifiNative;->p2pServiceDel(Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z

    #@d
    .line 2919
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@14
    .line 2920
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_7

    #@1e
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@21
    move-result-object v1

    #@22
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_7

    #@28
    .line 2921
    const-string/jumbo v1, "remove client information from framework"

    #@2b
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@2e
    .line 2922
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@30
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    goto :goto_7
.end method

.method private removeServiceRequest(Landroid/os/Messenger;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;)V
    .registers 8
    .parameter "m"
    .parameter "req"

    #@0
    .prologue
    .line 2837
    const/4 v3, 0x0

    #@1
    invoke-direct {p0, p1, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getClientInfo(Landroid/os/Messenger;Z)Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@4
    move-result-object v0

    #@5
    .line 2838
    .local v0, clientInfo:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    if-nez v0, :cond_8

    #@7
    .line 2865
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2844
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 2845
    .local v2, removed:Z
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@11
    move-result v3

    #@12
    if-ge v1, v3, :cond_2a

    #@14
    .line 2846
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {p2, v3}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_5f

    #@22
    .line 2847
    const/4 v2, 0x1

    #@23
    .line 2848
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->removeAt(I)V

    #@2a
    .line 2853
    :cond_2a
    if-eqz v2, :cond_7

    #@2c
    .line 2855
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_53

    #@36
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13200(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Ljava/util/List;

    #@39
    move-result-object v3

    #@3a
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@3d
    move-result v3

    #@3e
    if-nez v3, :cond_53

    #@40
    .line 2856
    const-string/jumbo v3, "remove client information from framework"

    #@43
    invoke-direct {p0, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@46
    .line 2857
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@48
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@4b
    move-result-object v3

    #@4c
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    .line 2860
    :cond_53
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@55
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    if-eqz v3, :cond_7

    #@5b
    .line 2864
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->updateSupplicantServiceRequest()Z

    #@5e
    goto :goto_7

    #@5f
    .line 2845
    :cond_5f
    add-int/lit8 v1, v1, 0x1

    #@61
    goto :goto_a
.end method

.method private replyToMessage(Landroid/os/Message;I)V
    .registers 5
    .parameter "msg"
    .parameter "what"

    #@0
    .prologue
    .line 2736
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 2740
    :goto_4
    return-void

    #@5
    .line 2737
    :cond_5
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->obtainMessage(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 2738
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 2739
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@14
    goto :goto_4
.end method

.method private replyToMessage(Landroid/os/Message;II)V
    .registers 6
    .parameter "msg"
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 2743
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 2748
    :goto_4
    return-void

    #@5
    .line 2744
    :cond_5
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->obtainMessage(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 2745
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 2746
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 2747
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@f
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@16
    goto :goto_4
.end method

.method private replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V
    .registers 6
    .parameter "msg"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2751
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 2756
    :goto_4
    return-void

    #@5
    .line 2752
    :cond_5
    invoke-direct {p0, p1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->obtainMessage(Landroid/os/Message;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 2753
    .local v0, dstMsg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@b
    .line 2754
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 2755
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@f
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@16
    goto :goto_4
.end method

.method private resetWifiP2pInfo()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2476
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@3
    iput-boolean v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    #@5
    .line 2477
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@7
    iput-boolean v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    #@9
    .line 2478
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@b
    const/4 v1, 0x0

    #@c
    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    #@e
    .line 2479
    return-void
.end method

.method private sendConnectNoticeToApp(Landroid/net/wifi/p2p/WifiP2pDevice;Landroid/net/wifi/p2p/WifiP2pConfig;)Z
    .registers 6
    .parameter "dev"
    .parameter "config"

    #@0
    .prologue
    .line 3065
    if-nez p1, :cond_9

    #@2
    .line 3066
    new-instance p1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@4
    .end local p1
    iget-object v2, p2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@6
    invoke-direct {p1, v2}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@9
    .line 3069
    .restart local p1
    :cond_9
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@b
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13600(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->isForegroundApp(Ljava/lang/String;)Z

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_20

    #@15
    .line 3070
    const-string v2, "application is NOT foreground"

    #@17
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@1a
    .line 3071
    const/4 v2, 0x4

    #@1b
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendDetachedMsg(I)V

    #@1e
    .line 3072
    const/4 v2, 0x0

    #@1f
    .line 3081
    :goto_1f
    return v2

    #@20
    .line 3075
    :cond_20
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@23
    move-result-object v1

    #@24
    .line 3076
    .local v1, msg:Landroid/os/Message;
    const v2, 0x22039

    #@27
    iput v2, v1, Landroid/os/Message;->what:I

    #@29
    .line 3077
    new-instance v0, Landroid/os/Bundle;

    #@2b
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@2e
    .line 3078
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "wifiP2pDevice"

    #@31
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@34
    .line 3079
    const-string/jumbo v2, "wifiP2pConfig"

    #@37
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@3a
    .line 3080
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@3d
    .line 3081
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendDialogMsgToFrontApp(Landroid/os/Message;)Z

    #@40
    move-result v2

    #@41
    goto :goto_1f
.end method

.method private sendDetachedMsg(I)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3026
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/os/Messenger;

    #@6
    move-result-object v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 3037
    :goto_9
    return-void

    #@a
    .line 3028
    :cond_a
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 3029
    .local v0, msg:Landroid/os/Message;
    const v1, 0x22037

    #@11
    iput v1, v0, Landroid/os/Message;->what:I

    #@13
    .line 3030
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@15
    .line 3032
    :try_start_15
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@17
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/os/Messenger;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1e} :catch_29

    #@1e
    .line 3035
    :goto_1e
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@20
    invoke-static {v1, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/os/Messenger;)Landroid/os/Messenger;

    #@23
    .line 3036
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@25
    invoke-static {v1, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13602(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@28
    goto :goto_9

    #@29
    .line 3033
    :catch_29
    move-exception v1

    #@2a
    goto :goto_1e
.end method

.method private sendDialogMsgToFrontApp(Landroid/os/Message;)Z
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3091
    :try_start_1
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/os/Messenger;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_c

    #@a
    .line 3097
    const/4 v1, 0x1

    #@b
    :goto_b
    return v1

    #@c
    .line 3092
    :catch_c
    move-exception v0

    #@d
    .line 3093
    .local v0, e:Landroid/os/RemoteException;
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@f
    invoke-static {v1, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/os/Messenger;)Landroid/os/Messenger;

    #@12
    .line 3094
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@14
    invoke-static {v1, v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13602(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@17
    .line 3095
    const/4 v1, 0x0

    #@18
    goto :goto_b
.end method

.method private sendP2pConnectionChangedBroadcast()V
    .registers 6

    #@0
    .prologue
    .line 2046
    const-string/jumbo v1, "sending p2p connection changed broadcast"

    #@3
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@6
    .line 2047
    new-instance v0, Landroid/content/Intent;

    #@8
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    #@a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 2048
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2800

    #@f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@12
    .line 2050
    const-string/jumbo v1, "wifiP2pInfo"

    #@15
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pInfo;

    #@17
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@19
    invoke-direct {v2, v3}, Landroid/net/wifi/p2p/WifiP2pInfo;-><init>(Landroid/net/wifi/p2p/WifiP2pInfo;)V

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1f
    .line 2051
    const-string/jumbo v1, "networkInfo"

    #@22
    new-instance v2, Landroid/net/NetworkInfo;

    #@24
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@26
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$3100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/NetworkInfo;

    #@29
    move-result-object v3

    #@2a
    invoke-direct {v2, v3}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@30
    .line 2052
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@32
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@35
    move-result-object v1

    #@36
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@38
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3b
    .line 2053
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3d
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$600(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;

    #@40
    move-result-object v1

    #@41
    const v2, 0x2300b

    #@44
    new-instance v3, Landroid/net/NetworkInfo;

    #@46
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@48
    invoke-static {v4}, Landroid/net/wifi/p2p/WifiP2pService;->access$3100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/NetworkInfo;

    #@4b
    move-result-object v4

    #@4c
    invoke-direct {v3, v4}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@4f
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(ILjava/lang/Object;)V

    #@52
    .line 2055
    return-void
.end method

.method private sendP2pDiscoveryChangedBroadcast(Z)V
    .registers 5
    .parameter "started"

    #@0
    .prologue
    .line 2019
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12500(Landroid/net/wifi/p2p/WifiP2pService;)Z

    #@5
    move-result v1

    #@6
    if-ne v1, p1, :cond_9

    #@8
    .line 2030
    :goto_8
    return-void

    #@9
    .line 2020
    :cond_9
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@b
    invoke-static {v1, p1}, Landroid/net/wifi/p2p/WifiP2pService;->access$12502(Landroid/net/wifi/p2p/WifiP2pService;Z)Z

    #@e
    .line 2022
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "discovery change broadcast "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@24
    .line 2024
    new-instance v0, Landroid/content/Intent;

    #@26
    const-string v1, "android.net.wifi.p2p.DISCOVERY_STATE_CHANGE"

    #@28
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2b
    .line 2025
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@2d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@30
    .line 2026
    const-string v2, "discoveryState"

    #@32
    if-eqz p1, :cond_44

    #@34
    const/4 v1, 0x2

    #@35
    :goto_35
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@38
    .line 2029
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3a
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@3d
    move-result-object v1

    #@3e
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@40
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@43
    goto :goto_8

    #@44
    .line 2026
    :cond_44
    const/4 v1, 0x1

    #@45
    goto :goto_35
.end method

.method private sendP2pPeersChangedBroadcast()V
    .registers 4

    #@0
    .prologue
    .line 2040
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2041
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 2042
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@e
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@11
    move-result-object v1

    #@12
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@14
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@17
    .line 2043
    return-void
.end method

.method private sendP2pPersistentGroupsChangedBroadcast()V
    .registers 4

    #@0
    .prologue
    .line 2058
    const-string/jumbo v1, "sending p2p persistent groups changed broadcast"

    #@3
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@6
    .line 2059
    new-instance v0, Landroid/content/Intent;

    #@8
    const-string v1, "android.net.wifi.p2p.PERSISTENT_GROUPS_CHANGED"

    #@a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 2060
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@12
    .line 2061
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@14
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@17
    move-result-object v1

    #@18
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1a
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1d
    .line 2062
    return-void
.end method

.method private sendP2pStateChangedBroadcast(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 2006
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2007
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 2008
    if-eqz p1, :cond_21

    #@e
    .line 2009
    const-string/jumbo v1, "wifi_p2p_state"

    #@11
    const/4 v2, 0x2

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@15
    .line 2015
    :goto_15
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@17
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@1a
    move-result-object v1

    #@1b
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1d
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@20
    .line 2016
    return-void

    #@21
    .line 2012
    :cond_21
    const-string/jumbo v1, "wifi_p2p_state"

    #@24
    const/4 v2, 0x1

    #@25
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@28
    goto :goto_15
.end method

.method private sendServiceResponse(Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;)V
    .registers 10
    .parameter "resp"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2954
    iget-object v5, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3
    invoke-static {v5}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@6
    move-result-object v5

    #@7
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@a
    move-result-object v5

    #@b
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v2

    #@f
    .local v2, i$:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v5

    #@13
    if-eqz v5, :cond_4f

    #@15
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@1b
    .line 2955
    .local v0, c:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {p1}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->getTransactionId()I

    #@22
    move-result v6

    #@23
    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v4

    #@27
    check-cast v4, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;

    #@29
    .line 2956
    .local v4, req:Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    if-eqz v4, :cond_f

    #@2b
    .line 2957
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@2e
    move-result-object v3

    #@2f
    .line 2958
    .local v3, msg:Landroid/os/Message;
    const v5, 0x22032

    #@32
    iput v5, v3, Landroid/os/Message;->what:I

    #@34
    .line 2959
    iput v7, v3, Landroid/os/Message;->arg1:I

    #@36
    .line 2960
    iput v7, v3, Landroid/os/Message;->arg2:I

    #@38
    .line 2961
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3a
    .line 2963
    :try_start_3a
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_41
    .catch Landroid/os/RemoteException; {:try_start_3a .. :try_end_41} :catch_42

    #@41
    goto :goto_f

    #@42
    .line 2964
    :catch_42
    move-exception v1

    #@43
    .line 2965
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "detect dead channel"

    #@45
    invoke-direct {p0, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@48
    .line 2966
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13300(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/os/Messenger;

    #@4b
    move-result-object v5

    #@4c
    invoke-direct {p0, v5}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearClientInfo(Landroid/os/Messenger;)V

    #@4f
    .line 2971
    .end local v0           #c:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v3           #msg:Landroid/os/Message;
    .end local v4           #req:Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    :cond_4f
    return-void
.end method

.method private sendShowPinReqToFrontApp(Ljava/lang/String;)Z
    .registers 5
    .parameter "pin"

    #@0
    .prologue
    .line 3045
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13600(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->isForegroundApp(Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_12

    #@c
    .line 3046
    const/4 v2, 0x4

    #@d
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendDetachedMsg(I)V

    #@10
    .line 3047
    const/4 v2, 0x0

    #@11
    .line 3054
    :goto_11
    return v2

    #@12
    .line 3049
    :cond_12
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@15
    move-result-object v1

    #@16
    .line 3050
    .local v1, msg:Landroid/os/Message;
    const v2, 0x2203a

    #@19
    iput v2, v1, Landroid/os/Message;->what:I

    #@1b
    .line 3051
    new-instance v0, Landroid/os/Bundle;

    #@1d
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@20
    .line 3052
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "wpsPin"

    #@23
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 3053
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@29
    .line 3054
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendDialogMsgToFrontApp(Landroid/os/Message;)Z

    #@2c
    move-result v2

    #@2d
    goto :goto_11
.end method

.method private sendThisDeviceChangedBroadcast()V
    .registers 5

    #@0
    .prologue
    .line 2033
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 2034
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 2035
    const-string/jumbo v1, "wifiP2pDevice"

    #@f
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@11
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@13
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@16
    move-result-object v3

    #@17
    invoke-direct {v2, v3}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1d
    .line 2036
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1f
    invoke-static {v1}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@22
    move-result-object v1

    #@23
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@25
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@28
    .line 2037
    return-void
.end method

.method private setAndPersistDeviceName(Ljava/lang/String;)Z
    .registers 7
    .parameter "devName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2557
    if-nez p1, :cond_4

    #@3
    .line 2590
    :goto_3
    return v2

    #@4
    .line 2559
    :cond_4
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@6
    invoke-virtual {v3, p1}, Landroid/net/wifi/WifiNative;->setDeviceName(Ljava/lang/String;)Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_23

    #@c
    .line 2560
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Failed to set device name "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-direct {p0, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@22
    goto :goto_3

    #@23
    .line 2564
    :cond_23
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@25
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@28
    move-result-object v2

    #@29
    iput-object p1, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@2b
    .line 2566
    const-string v2, "check postfix"

    #@2d
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@30
    .line 2567
    const/4 v1, 0x0

    #@31
    .line 2568
    .local v1, postfix:Ljava/lang/String;
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@33
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@36
    move-result-object v2

    #@37
    if-eqz v2, :cond_7f

    #@39
    .line 2569
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "before postfix = "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@46
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@49
    move-result-object v3

    #@4a
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@57
    .line 2570
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@59
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@5c
    move-result-object v2

    #@5d
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@5f
    invoke-static {v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@62
    move-result-object v3

    #@63
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@65
    invoke-interface {v2, v3}, Lcom/lge/wifi_iface/WifiServiceExtIface;->checkP2pPostfix(Ljava/lang/String;)Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    .line 2571
    new-instance v2, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v3, "after postfix = "

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v2

    #@7c
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@7f
    .line 2573
    :cond_7f
    if-eqz v1, :cond_c2

    #@81
    .line 2574
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@83
    new-instance v3, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    const-string v4, "-"

    #@8a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v3

    #@92
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiNative;->setP2pSsidPostfix(Ljava/lang/String;)Z

    #@99
    .line 2580
    :goto_99
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@9b
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@9e
    move-result-object v2

    #@9f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a2
    move-result-object v2

    #@a3
    const-string/jumbo v3, "lg_device_name"

    #@a6
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@a9
    move-result-object v0

    #@aa
    .line 2581
    .local v0, deviceName:Ljava/lang/String;
    if-eqz v0, :cond_e3

    #@ac
    .line 2582
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@ae
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@b1
    move-result-object v2

    #@b2
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b5
    move-result-object v2

    #@b6
    const-string/jumbo v3, "lg_device_name"

    #@b9
    invoke-static {v2, v3, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@bc
    .line 2589
    :goto_bc
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendThisDeviceChangedBroadcast()V

    #@bf
    .line 2590
    const/4 v2, 0x1

    #@c0
    goto/16 :goto_3

    #@c2
    .line 2576
    .end local v0           #deviceName:Ljava/lang/String;
    :cond_c2
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@c4
    new-instance v3, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v4, "-"

    #@cb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v3

    #@cf
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d1
    invoke-static {v4}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@d4
    move-result-object v4

    #@d5
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    #@d7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v3

    #@db
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v3

    #@df
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiNative;->setP2pSsidPostfix(Ljava/lang/String;)Z

    #@e2
    goto :goto_99

    #@e3
    .line 2584
    .restart local v0       #deviceName:Ljava/lang/String;
    :cond_e3
    const-string/jumbo v2, "lg_device_name DB value is null. Use original DB"

    #@e6
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@e9
    .line 2585
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@eb
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;

    #@ee
    move-result-object v2

    #@ef
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f2
    move-result-object v2

    #@f3
    const-string/jumbo v3, "wifi_p2p_device_name"

    #@f6
    invoke-static {v2, v3, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@f9
    goto :goto_bc
.end method

.method private setDialogListenerApp(Landroid/os/Messenger;Ljava/lang/String;Z)Z
    .registers 8
    .parameter "m"
    .parameter "appPkgName"
    .parameter "isReset"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v0, 0x0

    #@3
    .line 3110
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@5
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13600(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    if-eqz v2, :cond_2d

    #@b
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@d
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13600(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_2d

    #@17
    .line 3111
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@19
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13600(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->isForegroundApp(Ljava/lang/String;)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_29

    #@23
    .line 3113
    const-string v1, "application is NOT foreground"

    #@25
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@28
    .line 3134
    :cond_28
    :goto_28
    return v0

    #@29
    .line 3117
    :cond_29
    const/4 v2, 0x4

    #@2a
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendDetachedMsg(I)V

    #@2d
    .line 3120
    :cond_2d
    if-eqz p3, :cond_41

    #@2f
    .line 3121
    const-string/jumbo v0, "reset dialog listener"

    #@32
    invoke-direct {p0, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@35
    .line 3122
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@37
    invoke-static {v0, v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$13502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/os/Messenger;)Landroid/os/Messenger;

    #@3a
    .line 3123
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3c
    invoke-static {v0, v3}, Landroid/net/wifi/p2p/WifiP2pService;->access$13602(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move v0, v1

    #@40
    .line 3124
    goto :goto_28

    #@41
    .line 3127
    :cond_41
    invoke-direct {p0, p2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->isForegroundApp(Ljava/lang/String;)Z

    #@44
    move-result v2

    #@45
    if-eqz v2, :cond_28

    #@47
    .line 3131
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@49
    invoke-static {v0, p1}, Landroid/net/wifi/p2p/WifiP2pService;->access$13502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/os/Messenger;)Landroid/os/Messenger;

    #@4c
    .line 3132
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4e
    invoke-static {v0, p2}, Landroid/net/wifi/p2p/WifiP2pService;->access$13602(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@51
    .line 3133
    new-instance v0, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string/jumbo v2, "set dialog listener. app="

    #@59
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-direct {p0, v0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@68
    move v0, v1

    #@69
    .line 3134
    goto :goto_28
.end method

.method private setWfdInfo(Landroid/net/wifi/p2p/WifiP2pWfdInfo;)Z
    .registers 7
    .parameter "wfdInfo"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2596
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->isWfdEnabled()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_16

    #@8
    .line 2597
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@a
    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiNative;->setWfdEnable(Z)Z

    #@d
    move-result v0

    #@e
    .line 2604
    .local v0, success:Z
    :goto_e
    if-nez v0, :cond_2e

    #@10
    .line 2605
    const-string v1, "Failed to set wfd properties"

    #@12
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@15
    .line 2611
    :goto_15
    return v2

    #@16
    .line 2599
    .end local v0           #success:Z
    :cond_16
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@18
    invoke-virtual {v3, v1}, Landroid/net/wifi/WifiNative;->setWfdEnable(Z)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2c

    #@1e
    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@20
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->getDeviceInfoHex()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiNative;->setWfdDeviceInfo(Ljava/lang/String;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2c

    #@2a
    move v0, v1

    #@2b
    .restart local v0       #success:Z
    :goto_2b
    goto :goto_e

    #@2c
    .end local v0           #success:Z
    :cond_2c
    move v0, v2

    #@2d
    goto :goto_2b

    #@2e
    .line 2609
    .restart local v0       #success:Z
    :cond_2e
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@30
    invoke-static {v2}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@33
    move-result-object v2

    #@34
    iput-object p1, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    #@36
    .line 2610
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendThisDeviceChangedBroadcast()V

    #@39
    move v2, v1

    #@3a
    .line 2611
    goto :goto_15
.end method

.method private setWifiP2pInfoOnGroupFormation(Ljava/lang/String;)V
    .registers 4
    .parameter "serverAddress"

    #@0
    .prologue
    .line 2470
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@2
    const/4 v1, 0x1

    #@3
    iput-boolean v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    #@5
    .line 2471
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@7
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    #@9
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    #@c
    move-result v1

    #@d
    iput-boolean v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    #@f
    .line 2472
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiP2pInfo:Landroid/net/wifi/p2p/WifiP2pInfo;

    #@11
    invoke-static {p1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@14
    move-result-object v1

    #@15
    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    #@17
    .line 2473
    return-void
.end method

.method private startDhcpServer(Ljava/lang/String;)V
    .registers 7
    .parameter "intf"

    #@0
    .prologue
    .line 2065
    const/4 v1, 0x0

    #@1
    .line 2067
    .local v1, ifcg:Landroid/net/InterfaceConfiguration;
    :try_start_1
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@3
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pService;->mNwService:Landroid/os/INetworkManagementService;

    #@5
    invoke-interface {v2, p1}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@8
    move-result-object v1

    #@9
    .line 2068
    new-instance v2, Landroid/net/LinkAddress;

    #@b
    const-string v3, "192.168.49.1"

    #@d
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@10
    move-result-object v3

    #@11
    const/16 v4, 0x18

    #@13
    invoke-direct {v2, v3, v4}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@16
    invoke-virtual {v1, v2}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@19
    .line 2070
    invoke-virtual {v1}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@1c
    .line 2071
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@1e
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pService;->mNwService:Landroid/os/INetworkManagementService;

    #@20
    invoke-interface {v2, p1, v1}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@23
    .line 2073
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@25
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pService;->mNwService:Landroid/os/INetworkManagementService;

    #@27
    invoke-static {}, Landroid/net/wifi/p2p/WifiP2pService;->access$12600()[Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->startTethering([Ljava/lang/String;)V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_2e} :catch_45

    #@2e
    .line 2079
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "Started Dhcp server on "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@44
    .line 2080
    :goto_44
    return-void

    #@45
    .line 2074
    :catch_45
    move-exception v0

    #@46
    .line 2075
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "Error configuring interface "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    const-string v3, ", :"

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-direct {p0, v2}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@66
    goto :goto_44
.end method

.method private stopDhcpServer(Ljava/lang/String;)V
    .registers 5
    .parameter "intf"

    #@0
    .prologue
    .line 2084
    :try_start_0
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pService;->mNwService:Landroid/os/INetworkManagementService;

    #@4
    invoke-interface {v1}, Landroid/os/INetworkManagementService;->stopTethering()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_d

    #@7
    .line 2090
    const-string v1, "Stopped Dhcp server"

    #@9
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@c
    .line 2091
    :goto_c
    return-void

    #@d
    .line 2085
    :catch_d
    move-exception v0

    #@e
    .line 2086
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Error stopping Dhcp server"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {p0, v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->loge(Ljava/lang/String;)V

    #@24
    goto :goto_c
.end method

.method private updatePersistentNetworks(Z)V
    .registers 17
    .parameter "reload"

    #@0
    .prologue
    .line 2223
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@2
    invoke-virtual {v13}, Landroid/net/wifi/WifiNative;->listNetworks()Ljava/lang/String;

    #@5
    move-result-object v8

    #@6
    .line 2224
    .local v8, listStr:Ljava/lang/String;
    if-nez v8, :cond_9

    #@8
    .line 2290
    :cond_8
    :goto_8
    return-void

    #@9
    .line 2226
    :cond_9
    const/4 v6, 0x0

    #@a
    .line 2227
    .local v6, isSaveRequired:Z
    const-string v13, "\n"

    #@c
    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@f
    move-result-object v7

    #@10
    .line 2228
    .local v7, lines:[Ljava/lang/String;
    if-eqz v7, :cond_8

    #@12
    .line 2230
    if-eqz p1, :cond_19

    #@14
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@16
    invoke-virtual {v13}, Landroid/net/wifi/p2p/WifiP2pGroupList;->clear()Z

    #@19
    .line 2233
    :cond_19
    const/4 v5, 0x1

    #@1a
    .local v5, i:I
    :goto_1a
    array-length v13, v7

    #@1b
    if-ge v5, v13, :cond_c7

    #@1d
    .line 2234
    aget-object v13, v7, v5

    #@1f
    const-string v14, "\t"

    #@21
    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@24
    move-result-object v11

    #@25
    .line 2235
    .local v11, result:[Ljava/lang/String;
    if-eqz v11, :cond_2b

    #@27
    array-length v13, v11

    #@28
    const/4 v14, 0x4

    #@29
    if-ge v13, v14, :cond_2e

    #@2b
    .line 2233
    :cond_2b
    :goto_2b
    add-int/lit8 v5, v5, 0x1

    #@2d
    goto :goto_1a

    #@2e
    .line 2239
    :cond_2e
    const/4 v10, -0x1

    #@2f
    .line 2240
    .local v10, netId:I
    const/4 v13, 0x1

    #@30
    aget-object v12, v11, v13

    #@32
    .line 2241
    .local v12, ssid:Ljava/lang/String;
    const/4 v13, 0x2

    #@33
    aget-object v0, v11, v13

    #@35
    .line 2242
    .local v0, bssid:Ljava/lang/String;
    const/4 v13, 0x3

    #@36
    aget-object v3, v11, v13

    #@38
    .line 2244
    .local v3, flags:Ljava/lang/String;
    const/4 v13, 0x0

    #@39
    :try_start_39
    aget-object v13, v11, v13

    #@3b
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3e
    .catch Ljava/lang/NumberFormatException; {:try_start_39 .. :try_end_3e} :catch_6e

    #@3e
    move-result v10

    #@3f
    .line 2250
    const-string v13, "[CURRENT]"

    #@41
    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@44
    move-result v13

    #@45
    const/4 v14, -0x1

    #@46
    if-ne v13, v14, :cond_2b

    #@48
    .line 2253
    const-string v13, "[P2P-PERSISTENT]"

    #@4a
    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@4d
    move-result v13

    #@4e
    const/4 v14, -0x1

    #@4f
    if-ne v13, v14, :cond_73

    #@51
    .line 2258
    new-instance v13, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v14, "clean up the unused persistent group. netId="

    #@58
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v13

    #@5c
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v13

    #@60
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v13

    #@64
    invoke-direct {p0, v13}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->logd(Ljava/lang/String;)V

    #@67
    .line 2259
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@69
    invoke-virtual {v13, v10}, Landroid/net/wifi/WifiNative;->removeNetwork(I)Z

    #@6c
    .line 2260
    const/4 v6, 0x1

    #@6d
    .line 2261
    goto :goto_2b

    #@6e
    .line 2245
    :catch_6e
    move-exception v2

    #@6f
    .line 2246
    .local v2, e:Ljava/lang/NumberFormatException;
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V

    #@72
    goto :goto_2b

    #@73
    .line 2264
    .end local v2           #e:Ljava/lang/NumberFormatException;
    :cond_73
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@75
    invoke-virtual {v13, v10}, Landroid/net/wifi/p2p/WifiP2pGroupList;->contains(I)Z

    #@78
    move-result v13

    #@79
    if-nez v13, :cond_2b

    #@7b
    .line 2268
    new-instance v4, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@7d
    invoke-direct {v4}, Landroid/net/wifi/p2p/WifiP2pGroup;-><init>()V

    #@80
    .line 2269
    .local v4, group:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-virtual {v4, v10}, Landroid/net/wifi/p2p/WifiP2pGroup;->setNetworkId(I)V

    #@83
    .line 2270
    invoke-virtual {v4, v12}, Landroid/net/wifi/p2p/WifiP2pGroup;->setNetworkName(Ljava/lang/String;)V

    #@86
    .line 2271
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@88
    const-string/jumbo v14, "mode"

    #@8b
    invoke-virtual {v13, v10, v14}, Landroid/net/wifi/WifiNative;->getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;

    #@8e
    move-result-object v9

    #@8f
    .line 2272
    .local v9, mode:Ljava/lang/String;
    if-eqz v9, :cond_9d

    #@91
    const-string v13, "3"

    #@93
    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v13

    #@97
    if-eqz v13, :cond_9d

    #@99
    .line 2273
    const/4 v13, 0x1

    #@9a
    invoke-virtual {v4, v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->setIsGroupOwner(Z)V

    #@9d
    .line 2275
    :cond_9d
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@9f
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@a2
    move-result-object v13

    #@a3
    iget-object v13, v13, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@a5
    invoke-virtual {v0, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a8
    move-result v13

    #@a9
    if-eqz v13, :cond_bc

    #@ab
    .line 2276
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@ad
    invoke-static {v13}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@b0
    move-result-object v13

    #@b1
    invoke-virtual {v4, v13}, Landroid/net/wifi/p2p/WifiP2pGroup;->setOwner(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@b4
    .line 2282
    :goto_b4
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mGroups:Landroid/net/wifi/p2p/WifiP2pGroupList;

    #@b6
    invoke-virtual {v13, v4}, Landroid/net/wifi/p2p/WifiP2pGroupList;->add(Landroid/net/wifi/p2p/WifiP2pGroup;)V

    #@b9
    .line 2283
    const/4 v6, 0x1

    #@ba
    goto/16 :goto_2b

    #@bc
    .line 2278
    :cond_bc
    new-instance v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@be
    invoke-direct {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>()V

    #@c1
    .line 2279
    .local v1, device:Landroid/net/wifi/p2p/WifiP2pDevice;
    iput-object v0, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@c3
    .line 2280
    invoke-virtual {v4, v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->setOwner(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    #@c6
    goto :goto_b4

    #@c7
    .line 2286
    .end local v0           #bssid:Ljava/lang/String;
    .end local v1           #device:Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v3           #flags:Ljava/lang/String;
    .end local v4           #group:Landroid/net/wifi/p2p/WifiP2pGroup;
    .end local v9           #mode:Ljava/lang/String;
    .end local v10           #netId:I
    .end local v11           #result:[Ljava/lang/String;
    .end local v12           #ssid:Ljava/lang/String;
    :cond_c7
    if-nez p1, :cond_cb

    #@c9
    if-eqz v6, :cond_8

    #@cb
    .line 2287
    :cond_cb
    iget-object v13, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@cd
    invoke-virtual {v13}, Landroid/net/wifi/WifiNative;->saveConfig()Z

    #@d0
    .line 2288
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendP2pPersistentGroupsChangedBroadcast()V

    #@d3
    goto/16 :goto_8
.end method

.method private updateSupplicantServiceRequest()Z
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2778
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->clearSupplicantServiceRequest()V

    #@4
    .line 2780
    new-instance v4, Ljava/lang/StringBuffer;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    #@9
    .line 2781
    .local v4, sb:Ljava/lang/StringBuffer;
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@b
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@12
    move-result-object v6

    #@13
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v2

    #@17
    .local v2, i$:Ljava/util/Iterator;
    :cond_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_44

    #@1d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;

    #@23
    .line 2784
    .local v0, c:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    const/4 v1, 0x0

    #@24
    .local v1, i:I
    :goto_24
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@2b
    move-result v6

    #@2c
    if-ge v1, v6, :cond_17

    #@2e
    .line 2785
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;->access$13100(Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;)Landroid/util/SparseArray;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@35
    move-result-object v3

    #@36
    check-cast v3, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;

    #@38
    .line 2786
    .local v3, req:Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    if-eqz v3, :cond_41

    #@3a
    .line 2787
    invoke-virtual {v3}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->getSupplicantQuery()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@41
    .line 2784
    :cond_41
    add-int/lit8 v1, v1, 0x1

    #@43
    goto :goto_24

    #@44
    .line 2792
    .end local v0           #c:Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;
    .end local v1           #i:I
    .end local v3           #req:Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    :cond_44
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    #@47
    move-result v6

    #@48
    if-nez v6, :cond_4b

    #@4a
    .line 2800
    :cond_4a
    :goto_4a
    return v5

    #@4b
    .line 2796
    :cond_4b
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@4d
    iget-object v7, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->mWifiNative:Landroid/net/wifi/WifiNative;

    #@4f
    const-string v8, "00:00:00:00:00:00"

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@54
    move-result-object v9

    #@55
    invoke-virtual {v7, v8, v9}, Landroid/net/wifi/WifiNative;->p2pServDiscReq(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-static {v6, v7}, Landroid/net/wifi/p2p/WifiP2pService;->access$6002(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;

    #@5c
    .line 2797
    iget-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@5e
    invoke-static {v6}, Landroid/net/wifi/p2p/WifiP2pService;->access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;

    #@61
    move-result-object v6

    #@62
    if-eqz v6, :cond_4a

    #@64
    .line 2800
    const/4 v5, 0x1

    #@65
    goto :goto_4a
.end method

.method private updateThisDevice(I)V
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 2654
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->this$0:Landroid/net/wifi/p2p/WifiP2pService;

    #@2
    invoke-static {v0}, Landroid/net/wifi/p2p/WifiP2pService;->access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    #@5
    move-result-object v0

    #@6
    iput p1, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@8
    .line 2655
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->sendThisDeviceChangedBroadcast()V

    #@b
    .line 2656
    return-void
.end method
