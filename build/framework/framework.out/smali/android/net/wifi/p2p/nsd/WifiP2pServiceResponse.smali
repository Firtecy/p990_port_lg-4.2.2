.class public Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
.super Ljava/lang/Object;
.source "WifiP2pServiceResponse.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse$Status;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static MAX_BUF_SIZE:I


# instance fields
.field protected mData:[B

.field protected mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field protected mServiceType:I

.field protected mStatus:I

.field protected mTransId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    const/16 v0, 0x400

    #@2
    sput v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->MAX_BUF_SIZE:I

    #@4
    .line 361
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse$1;

    #@6
    invoke-direct {v0}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse$1;-><init>()V

    #@9
    sput-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b
    return-void
.end method

.method protected constructor <init>(IIILandroid/net/wifi/p2p/WifiP2pDevice;[B)V
    .registers 6
    .parameter "serviceType"
    .parameter "status"
    .parameter "transId"
    .parameter "device"
    .parameter "data"

    #@0
    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 120
    iput p1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@5
    .line 121
    iput p2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@7
    .line 122
    iput p3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mTransId:I

    #@9
    .line 123
    iput-object p4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@b
    .line 124
    iput-object p5, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@d
    .line 125
    return-void
.end method

.method private equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 321
    if-nez p1, :cond_6

    #@2
    if-nez p2, :cond_6

    #@4
    .line 322
    const/4 v0, 0x1

    #@5
    .line 326
    :goto_5
    return v0

    #@6
    .line 323
    :cond_6
    if-eqz p1, :cond_d

    #@8
    .line 324
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5

    #@d
    .line 326
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_5
.end method

.method private static hexStr2Bin(Ljava/lang/String;)[B
    .registers 7
    .parameter "hex"

    #@0
    .prologue
    .line 279
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v4

    #@4
    div-int/lit8 v3, v4, 0x2

    #@6
    .line 280
    .local v3, sz:I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@9
    move-result v4

    #@a
    div-int/lit8 v4, v4, 0x2

    #@c
    new-array v0, v4, [B

    #@e
    .line 282
    .local v0, b:[B
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v3, :cond_2c

    #@11
    .line 284
    mul-int/lit8 v4, v2, 0x2

    #@13
    mul-int/lit8 v5, v2, 0x2

    #@15
    add-int/lit8 v5, v5, 0x2

    #@17
    :try_start_17
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    const/16 v5, 0x10

    #@1d
    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@20
    move-result v4

    #@21
    int-to-byte v4, v4

    #@22
    aput-byte v4, v0, v2
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_24} :catch_27

    #@24
    .line 282
    add-int/lit8 v2, v2, 0x1

    #@26
    goto :goto_f

    #@27
    .line 285
    :catch_27
    move-exception v1

    #@28
    .line 286
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@2b
    .line 287
    const/4 v0, 0x0

    #@2c
    .line 290
    .end local v0           #b:[B
    .end local v1           #e:Ljava/lang/Exception;
    :cond_2c
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Ljava/util/List;
    .registers 16
    .parameter "supplicantEvent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 199
    new-instance v11, Ljava/util/ArrayList;

    #@2
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 200
    .local v11, respList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;>;"
    const-string v13, " "

    #@7
    invoke-virtual {p0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@a
    move-result-object v6

    #@b
    .line 201
    .local v6, args:[Ljava/lang/String;
    array-length v13, v6

    #@c
    const/4 v14, 0x4

    #@d
    if-eq v13, v14, :cond_11

    #@f
    .line 202
    const/4 v11, 0x0

    #@10
    .line 269
    .end local v11           #respList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;>;"
    :cond_10
    :goto_10
    return-object v11

    #@11
    .line 204
    .restart local v11       #respList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;>;"
    :cond_11
    new-instance v4, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@13
    invoke-direct {v4}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>()V

    #@16
    .line 205
    .local v4, dev:Landroid/net/wifi/p2p/WifiP2pDevice;
    const/4 v13, 0x1

    #@17
    aget-object v12, v6, v13

    #@19
    .line 206
    .local v12, srcAddr:Ljava/lang/String;
    iput-object v12, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@1b
    .line 208
    const/4 v13, 0x3

    #@1c
    aget-object v13, v6, v13

    #@1e
    invoke-static {v13}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->hexStr2Bin(Ljava/lang/String;)[B

    #@21
    move-result-object v7

    #@22
    .line 209
    .local v7, bin:[B
    if-nez v7, :cond_26

    #@24
    .line 210
    const/4 v11, 0x0

    #@25
    goto :goto_10

    #@26
    .line 213
    :cond_26
    new-instance v8, Ljava/io/DataInputStream;

    #@28
    new-instance v13, Ljava/io/ByteArrayInputStream;

    #@2a
    invoke-direct {v13, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@2d
    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@30
    .line 215
    .local v8, dis:Ljava/io/DataInputStream;
    :cond_30
    :goto_30
    :try_start_30
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    #@33
    move-result v13

    #@34
    if-lez v13, :cond_10

    #@36
    .line 225
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    #@39
    move-result v13

    #@3a
    and-int/lit16 v13, v13, 0xff

    #@3c
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    #@3f
    move-result v14

    #@40
    and-int/lit16 v14, v14, 0xff

    #@42
    shl-int/lit8 v14, v14, 0x8

    #@44
    add-int/2addr v13, v14

    #@45
    add-int/lit8 v10, v13, -0x3

    #@47
    .line 227
    .local v10, length:I
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    #@4a
    move-result v1

    #@4b
    .line 228
    .local v1, type:I
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    #@4e
    move-result v3

    #@4f
    .line 229
    .local v3, transId:B
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    #@52
    move-result v2

    #@53
    .line 230
    .local v2, status:I
    if-gez v10, :cond_57

    #@55
    .line 231
    const/4 v11, 0x0

    #@56
    goto :goto_10

    #@57
    .line 233
    :cond_57
    if-nez v10, :cond_71

    #@59
    .line 234
    if-nez v2, :cond_30

    #@5b
    .line 235
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;

    #@5d
    const/4 v5, 0x0

    #@5e
    invoke-direct/range {v0 .. v5}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;-><init>(IIILandroid/net/wifi/p2p/WifiP2pDevice;[B)V

    #@61
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_64} :catch_65

    #@64
    goto :goto_30

    #@65
    .line 262
    .end local v1           #type:I
    .end local v2           #status:I
    .end local v3           #transId:B
    .end local v10           #length:I
    :catch_65
    move-exception v9

    #@66
    .line 263
    .local v9, e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    #@69
    .line 266
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@6c
    move-result v13

    #@6d
    if-gtz v13, :cond_10

    #@6f
    .line 269
    const/4 v11, 0x0

    #@70
    goto :goto_10

    #@71
    .line 240
    .end local v9           #e:Ljava/io/IOException;
    .restart local v1       #type:I
    .restart local v2       #status:I
    .restart local v3       #transId:B
    .restart local v10       #length:I
    :cond_71
    :try_start_71
    sget v13, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->MAX_BUF_SIZE:I

    #@73
    if-le v10, v13, :cond_7a

    #@75
    .line 241
    int-to-long v13, v10

    #@76
    invoke-virtual {v8, v13, v14}, Ljava/io/DataInputStream;->skip(J)J

    #@79
    goto :goto_30

    #@7a
    .line 244
    :cond_7a
    new-array v5, v10, [B

    #@7c
    .line 245
    .local v5, data:[B
    invoke-virtual {v8, v5}, Ljava/io/DataInputStream;->readFully([B)V

    #@7f
    .line 248
    const/4 v13, 0x1

    #@80
    if-ne v1, v13, :cond_92

    #@82
    .line 249
    invoke-static {v2, v3, v4, v5}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;->newInstance(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceResponse;

    #@85
    move-result-object v0

    #@86
    .line 257
    .local v0, resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    :goto_86
    if-eqz v0, :cond_30

    #@88
    invoke-virtual {v0}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->getStatus()I

    #@8b
    move-result v13

    #@8c
    if-nez v13, :cond_30

    #@8e
    .line 258
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@91
    goto :goto_30

    #@92
    .line 251
    .end local v0           #resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    :cond_92
    const/4 v13, 0x2

    #@93
    if-ne v1, v13, :cond_9a

    #@95
    .line 252
    invoke-static {v2, v3, v4, v5}, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->newInstance(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;

    #@98
    move-result-object v0

    #@99
    .restart local v0       #resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    goto :goto_86

    #@9a
    .line 255
    .end local v0           #resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    :cond_9a
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;

    #@9c
    invoke-direct/range {v0 .. v5}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;-><init>(IIILandroid/net/wifi/p2p/WifiP2pDevice;[B)V
    :try_end_9f
    .catch Ljava/io/IOException; {:try_start_71 .. :try_end_9f} :catch_65

    #@9f
    .restart local v0       #resp:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    goto :goto_86
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 343
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 305
    if-ne p1, p0, :cond_5

    #@4
    .line 314
    :cond_4
    :goto_4
    return v1

    #@5
    .line 308
    :cond_5
    instance-of v3, p1, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    .line 309
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 312
    check-cast v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;

    #@e
    .line 314
    .local v0, req:Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
    iget v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@10
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@12
    if-ne v3, v4, :cond_32

    #@14
    iget v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@16
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@18
    if-ne v3, v4, :cond_32

    #@1a
    iget-object v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@1c
    iget-object v3, v3, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@1e
    iget-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@20
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@22
    invoke-direct {p0, v3, v4}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_32

    #@28
    iget-object v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@2a
    iget-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@2c
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    #@2f
    move-result v3

    #@30
    if-nez v3, :cond_4

    #@32
    :cond_32
    move v1, v2

    #@33
    goto :goto_4
.end method

.method public getRawData()[B
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@2
    return-object v0
.end method

.method public getServiceType()I
    .registers 2

    #@0
    .prologue
    .line 134
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@2
    return v0
.end method

.method public getSrcDevice()Landroid/net/wifi/p2p/WifiP2pDevice;
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    return-object v0
.end method

.method public getStatus()I
    .registers 2

    #@0
    .prologue
    .line 144
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@2
    return v0
.end method

.method public getTransactionId()I
    .registers 2

    #@0
    .prologue
    .line 154
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mTransId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 331
    const/16 v0, 0x11

    #@3
    .line 332
    .local v0, result:I
    iget v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@5
    add-int/lit16 v0, v1, 0x20f

    #@7
    .line 333
    mul-int/lit8 v1, v0, 0x1f

    #@9
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@b
    add-int v0, v1, v3

    #@d
    .line 334
    mul-int/lit8 v1, v0, 0x1f

    #@f
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mTransId:I

    #@11
    add-int v0, v1, v3

    #@13
    .line 335
    mul-int/lit8 v3, v0, 0x1f

    #@15
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@17
    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@19
    if-nez v1, :cond_27

    #@1b
    move v1, v2

    #@1c
    :goto_1c
    add-int v0, v3, v1

    #@1e
    .line 337
    mul-int/lit8 v1, v0, 0x1f

    #@20
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@22
    if-nez v3, :cond_30

    #@24
    :goto_24
    add-int v0, v1, v2

    #@26
    .line 338
    return v0

    #@27
    .line 335
    :cond_27
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@29
    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@2b
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@2e
    move-result v1

    #@2f
    goto :goto_1c

    #@30
    .line 337
    :cond_30
    iget-object v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@32
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    #@35
    move-result v2

    #@36
    goto :goto_24
.end method

.method public setSrcDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .registers 2
    .parameter "dev"

    #@0
    .prologue
    .line 181
    if-nez p1, :cond_3

    #@2
    .line 183
    :goto_2
    return-void

    #@3
    .line 182
    :cond_3
    iput-object p1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@5
    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 295
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 296
    .local v0, sbuf:Ljava/lang/StringBuffer;
    const-string/jumbo v1, "serviceType:"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b
    move-result-object v1

    #@c
    iget v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@11
    .line 297
    const-string v1, " status:"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@16
    move-result-object v1

    #@17
    iget v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@19
    invoke-static {v2}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse$Status;->toString(I)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    .line 298
    const-string v1, " srcAddr:"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@25
    move-result-object v1

    #@26
    iget-object v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@28
    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2d
    .line 299
    const-string v1, " data:"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@38
    .line 300
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 348
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 349
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 350
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mTransId:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 351
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@11
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@14
    .line 352
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@16
    if-eqz v0, :cond_1d

    #@18
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@1a
    array-length v0, v0

    #@1b
    if-nez v0, :cond_22

    #@1d
    .line 353
    :cond_1d
    const/4 v0, 0x0

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 358
    :goto_21
    return-void

    #@22
    .line 355
    :cond_22
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@24
    array-length v0, v0

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 356
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@2d
    goto :goto_21
.end method
