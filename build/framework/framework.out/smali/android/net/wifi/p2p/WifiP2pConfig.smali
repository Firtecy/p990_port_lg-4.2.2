.class public Landroid/net/wifi/p2p/WifiP2pConfig;
.super Ljava/lang/Object;
.source "WifiP2pConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pConfig;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_GROUP_OWNER_INTENT:I = 0xf

.field public static final MIN_GROUP_OWNER_INTENT:I


# instance fields
.field public deviceAddress:Ljava/lang/String;

.field public groupOwnerIntent:I

.field public netId:I

.field public wps:Landroid/net/wifi/WpsInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 136
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig$1;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@6
    .line 55
    const/4 v0, -0x2

    #@7
    iput v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@9
    .line 59
    new-instance v0, Landroid/net/wifi/WpsInfo;

    #@b
    invoke-direct {v0}, Landroid/net/wifi/WpsInfo;-><init>()V

    #@e
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@10
    .line 60
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@12
    const/4 v1, 0x0

    #@13
    iput v1, v0, Landroid/net/wifi/WpsInfo;->setup:I

    #@15
    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/p2p/WifiP2pConfig;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 118
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@6
    .line 55
    const/4 v0, -0x2

    #@7
    iput v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@9
    .line 119
    if-eqz p1, :cond_20

    #@b
    .line 120
    iget-object v0, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@d
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@f
    .line 121
    new-instance v0, Landroid/net/wifi/WpsInfo;

    #@11
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@13
    invoke-direct {v0, v1}, Landroid/net/wifi/WpsInfo;-><init>(Landroid/net/wifi/WpsInfo;)V

    #@16
    iput-object v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@18
    .line 122
    iget v0, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@1a
    iput v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@1c
    .line 123
    iget v0, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@1e
    iput v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@20
    .line 125
    :cond_20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 11
    .parameter "supplicantEvent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x2

    #@3
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 52
    const/4 v4, -0x1

    #@7
    iput v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@9
    .line 55
    const/4 v4, -0x2

    #@a
    iput v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@c
    .line 65
    const-string v4, " "

    #@e
    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 67
    .local v3, tokens:[Ljava/lang/String;
    array-length v4, v3

    #@13
    if-lt v4, v6, :cond_1f

    #@15
    aget-object v4, v3, v7

    #@17
    const-string v5, "P2P-GO-NEG-REQUEST"

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v4

    #@1d
    if-nez v4, :cond_27

    #@1f
    .line 68
    :cond_1f
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@21
    const-string v5, "Malformed supplicant event"

    #@23
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v4

    #@27
    .line 71
    :cond_27
    aget-object v4, v3, v8

    #@29
    iput-object v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@2b
    .line 72
    new-instance v4, Landroid/net/wifi/WpsInfo;

    #@2d
    invoke-direct {v4}, Landroid/net/wifi/WpsInfo;-><init>()V

    #@30
    iput-object v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@32
    .line 74
    array-length v4, v3

    #@33
    if-le v4, v6, :cond_4b

    #@35
    .line 75
    aget-object v4, v3, v6

    #@37
    const-string v5, "="

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    .line 78
    .local v2, nameVal:[Ljava/lang/String;
    const/4 v4, 0x1

    #@3e
    :try_start_3e
    aget-object v4, v2, v4

    #@40
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_43
    .catch Ljava/lang/NumberFormatException; {:try_start_3e .. :try_end_43} :catch_4c

    #@43
    move-result v0

    #@44
    .line 83
    .local v0, devPasswdId:I
    :goto_44
    packed-switch v0, :pswitch_data_5e

    #@47
    .line 97
    :pswitch_47
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@49
    iput v7, v4, Landroid/net/wifi/WpsInfo;->setup:I

    #@4b
    .line 101
    .end local v0           #devPasswdId:I
    .end local v2           #nameVal:[Ljava/lang/String;
    :cond_4b
    :goto_4b
    return-void

    #@4c
    .line 79
    .restart local v2       #nameVal:[Ljava/lang/String;
    :catch_4c
    move-exception v1

    #@4d
    .line 80
    .local v1, e:Ljava/lang/NumberFormatException;
    const/4 v0, 0x0

    #@4e
    .restart local v0       #devPasswdId:I
    goto :goto_44

    #@4f
    .line 86
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :pswitch_4f
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@51
    iput v8, v4, Landroid/net/wifi/WpsInfo;->setup:I

    #@53
    goto :goto_4b

    #@54
    .line 90
    :pswitch_54
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@56
    iput v7, v4, Landroid/net/wifi/WpsInfo;->setup:I

    #@58
    goto :goto_4b

    #@59
    .line 94
    :pswitch_59
    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@5b
    iput v6, v4, Landroid/net/wifi/WpsInfo;->setup:I

    #@5d
    goto :goto_4b

    #@5e
    .line 83
    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_4f
        :pswitch_47
        :pswitch_47
        :pswitch_54
        :pswitch_59
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 114
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 105
    .local v0, sbuf:Ljava/lang/StringBuffer;
    const-string v1, "\n address: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a
    move-result-object v1

    #@b
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@10
    .line 106
    const-string v1, "\n wps: "

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@1b
    .line 107
    const-string v1, "\n groupOwnerIntent: "

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    move-result-object v1

    #@21
    iget v2, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@26
    .line 108
    const-string v1, "\n persist: "

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    move-result-object v1

    #@2c
    iget v2, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@31
    .line 109
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 130
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@7
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@a
    .line 131
    iget v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 132
    iget v0, p0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 133
    return-void
.end method
