.class public Landroid/net/wifi/p2p/WifiP2pService;
.super Landroid/net/wifi/p2p/IWifiP2pManager$Stub;
.source "WifiP2pService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/p2p/WifiP2pService$1;,
        Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;,
        Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;,
        Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    }
.end annotation


# static fields
.field private static final BASE:I = 0x23000

.field private static final CONNECT_FAILURE:I = -0x1

.field private static final CONNECT_SUCCESS:I = 0x0

.field private static final DBG:Z = true

.field private static final DHCP_RANGE:[Ljava/lang/String; = null

.field public static final DISABLE_P2P_TIMED_OUT:I = 0x23006

.field private static final DISABLE_P2P_WAIT_TIME_MS:I = 0x1388

.field public static final DISCONNECT_WIFI_REQUEST:I = 0x2300c

.field public static final DISCONNECT_WIFI_RESPONSE:I = 0x2300d

.field private static final DISCOVER_TIMEOUT_S:I = 0x78

.field private static final DROP_WIFI_USER_ACCEPT:I = 0x23004

.field private static final DROP_WIFI_USER_REJECT:I = 0x23005

.field private static final FORM_GROUP:Ljava/lang/Boolean; = null

.field public static final GROUP_CREATING_TIMED_OUT:I = 0x23001

.field private static final GROUP_CREATING_WAIT_TIME_MS:I = 0x1d4c0

.field private static final GROUP_IDLE_TIME_S:I = 0xa

.field private static final JOIN_GROUP:Ljava/lang/Boolean; = null

.field private static final NEEDS_PROVISION_REQ:I = 0x1

.field private static final NETWORKTYPE:Ljava/lang/String; = "WIFI_P2P"

.field private static final NO_REINVOCATION:Ljava/lang/Boolean; = null

.field private static final NO_RELOAD:Ljava/lang/Boolean; = null

.field public static final P2P_CONNECTION_CHANGED:I = 0x2300b

.field private static final PEER_CONNECTION_USER_ACCEPT:I = 0x23002

.field private static final PEER_CONNECTION_USER_REJECT:I = 0x23003

.field private static final RELOAD:Ljava/lang/Boolean; = null

.field private static final SERVER_ADDRESS:Ljava/lang/String; = "192.168.49.1"

.field private static final TAG:Ljava/lang/String; = "WifiP2pService"

.field private static final TRY_REINVOCATION:Ljava/lang/Boolean;

.field private static mDisableP2pTimeoutIndex:I

.field private static mGroupCreatingTimeoutIndex:I


# instance fields
.field private mActivityMgr:Landroid/app/ActivityManager;

.field private mAutonomousGroup:Z

.field private mClientInfoList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/Messenger;",
            "Landroid/net/wifi/p2p/WifiP2pService$ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDHCPRetryCounter:I

.field private mDhcpStateMachine:Landroid/net/DhcpStateMachine;

.field private mDialogWps:Landroid/app/AlertDialog;

.field private mDiscoveryStarted:Z

.field private mForegroundAppMessenger:Landroid/os/Messenger;

.field private mForegroundAppPkgName:Ljava/lang/String;

.field private mInterface:Ljava/lang/String;

.field private mIsUserAuthorizingJoinState:Z

.field private mJoinExistingGroup:Z

.field private mJoinState:Z

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mNotification:Landroid/app/Notification;

.field mNwService:Landroid/os/INetworkManagementService;

.field private mP2pStateMachine:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

.field private final mP2pSupported:Z

.field private mReplyChannel:Lcom/android/internal/util/AsyncChannel;

.field private mServiceDiscReqId:Ljava/lang/String;

.field private mServiceTransactionId:B

.field private mTempoarilyDisconnectedWifi:Z

.field private mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private mWifiChannel:Lcom/android/internal/util/AsyncChannel;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 129
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->JOIN_GROUP:Ljava/lang/Boolean;

    #@8
    .line 130
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@b
    move-result-object v0

    #@c
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->FORM_GROUP:Ljava/lang/Boolean;

    #@e
    .line 132
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->TRY_REINVOCATION:Ljava/lang/Boolean;

    #@14
    .line 133
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17
    move-result-object v0

    #@18
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->NO_REINVOCATION:Ljava/lang/Boolean;

    #@1a
    .line 135
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->RELOAD:Ljava/lang/Boolean;

    #@20
    .line 136
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@23
    move-result-object v0

    #@24
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->NO_RELOAD:Ljava/lang/Boolean;

    #@26
    .line 144
    sput v2, Landroid/net/wifi/p2p/WifiP2pService;->mGroupCreatingTimeoutIndex:I

    #@28
    .line 147
    sput v2, Landroid/net/wifi/p2p/WifiP2pService;->mDisableP2pTimeoutIndex:I

    #@2a
    .line 235
    const/4 v0, 0x2

    #@2b
    new-array v0, v0, [Ljava/lang/String;

    #@2d
    const-string v1, "192.168.49.2"

    #@2f
    aput-object v1, v0, v2

    #@31
    const-string v1, "192.168.49.254"

    #@33
    aput-object v1, v0, v3

    #@35
    sput-object v0, Landroid/net/wifi/p2p/WifiP2pService;->DHCP_RANGE:[Ljava/lang/String;

    #@37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 320
    invoke-direct {p0}, Landroid/net/wifi/p2p/IWifiP2pManager$Stub;-><init>()V

    #@5
    .line 126
    new-instance v1, Lcom/android/internal/util/AsyncChannel;

    #@7
    invoke-direct {v1}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@a
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@c
    .line 191
    new-instance v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@e
    invoke-direct {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>()V

    #@11
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@13
    .line 208
    iput-boolean v5, p0, Landroid/net/wifi/p2p/WifiP2pService;->mTempoarilyDisconnectedWifi:Z

    #@15
    .line 211
    iput-byte v5, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceTransactionId:B

    #@17
    .line 218
    new-instance v1, Ljava/util/HashMap;

    #@19
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@1c
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mClientInfoList:Ljava/util/HashMap;

    #@1e
    .line 229
    iput-boolean v5, p0, Landroid/net/wifi/p2p/WifiP2pService;->mIsUserAuthorizingJoinState:Z

    #@20
    .line 230
    iput-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDialogWps:Landroid/app/AlertDialog;

    #@22
    .line 238
    iput v5, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDHCPRetryCounter:I

    #@24
    .line 241
    iput-boolean v5, p0, Landroid/net/wifi/p2p/WifiP2pService;->mJoinState:Z

    #@26
    .line 321
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@28
    .line 324
    const-string/jumbo v1, "p2p0"

    #@2b
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mInterface:Ljava/lang/String;

    #@2d
    .line 325
    const-string v1, "activity"

    #@2f
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Landroid/app/ActivityManager;

    #@35
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mActivityMgr:Landroid/app/ActivityManager;

    #@37
    .line 327
    new-instance v1, Landroid/net/NetworkInfo;

    #@39
    const/16 v2, 0xd

    #@3b
    const-string v3, "WIFI_P2P"

    #@3d
    const-string v4, ""

    #@3f
    invoke-direct {v1, v2, v5, v3, v4}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@42
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@44
    .line 329
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@46
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@49
    move-result-object v1

    #@4a
    const-string v2, "android.hardware.wifi.direct"

    #@4c
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@4f
    move-result v1

    #@50
    iput-boolean v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mP2pSupported:Z

    #@52
    .line 332
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@54
    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@56
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@59
    move-result-object v2

    #@5a
    const v3, 0x1040035

    #@5d
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    iput-object v2, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    #@63
    .line 335
    new-instance v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@65
    const-string v2, "WifiP2pService"

    #@67
    iget-boolean v3, p0, Landroid/net/wifi/p2p/WifiP2pService;->mP2pSupported:Z

    #@69
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;-><init>(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;Z)V

    #@6c
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mP2pStateMachine:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@6e
    .line 336
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mP2pStateMachine:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@70
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->start()V

    #@73
    .line 339
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@75
    if-eqz v1, :cond_9e

    #@77
    .line 340
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@7a
    move-result-object v1

    #@7b
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@7d
    .line 347
    :goto_7d
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@7f
    if-eqz v1, :cond_9d

    #@81
    .line 348
    new-instance v0, Landroid/content/IntentFilter;

    #@83
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@86
    .line 349
    .local v0, filterLGMDM:Landroid/content/IntentFilter;
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@89
    move-result-object v1

    #@8a
    if-eqz v1, :cond_93

    #@8c
    .line 350
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@8f
    move-result-object v1

    #@90
    invoke-interface {v1, v0}, Lcom/lge/cappuccino/IMdm;->addFilterWifiP2pServiceReceiver(Landroid/content/IntentFilter;)V

    #@93
    .line 352
    :cond_93
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@95
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;

    #@97
    invoke-direct {v2, p0, v6}, Landroid/net/wifi/p2p/WifiP2pService$LGMDMReceiver;-><init>(Landroid/net/wifi/p2p/WifiP2pService;Landroid/net/wifi/p2p/WifiP2pService$1;)V

    #@9a
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@9d
    .line 356
    .end local v0           #filterLGMDM:Landroid/content/IntentFilter;
    :cond_9d
    return-void

    #@9e
    .line 342
    :cond_9e
    iput-object v6, p0, Landroid/net/wifi/p2p/WifiP2pService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a0
    goto :goto_7d
.end method

.method static synthetic access$100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mP2pStateMachine:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$10700(Landroid/net/wifi/p2p/WifiP2pService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mTempoarilyDisconnectedWifi:Z

    #@2
    return v0
.end method

.method static synthetic access$10702(Landroid/net/wifi/p2p/WifiP2pService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mTempoarilyDisconnectedWifi:Z

    #@2
    return p1
.end method

.method static synthetic access$10900(Landroid/net/wifi/p2p/WifiP2pService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mIsUserAuthorizingJoinState:Z

    #@2
    return v0
.end method

.method static synthetic access$10902(Landroid/net/wifi/p2p/WifiP2pService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mIsUserAuthorizingJoinState:Z

    #@2
    return p1
.end method

.method static synthetic access$11200(Landroid/net/wifi/p2p/WifiP2pService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDHCPRetryCounter:I

    #@2
    return v0
.end method

.method static synthetic access$11202(Landroid/net/wifi/p2p/WifiP2pService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDHCPRetryCounter:I

    #@2
    return p1
.end method

.method static synthetic access$11210(Landroid/net/wifi/p2p/WifiP2pService;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDHCPRetryCounter:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDHCPRetryCounter:I

    #@6
    return v0
.end method

.method static synthetic access$12500(Landroid/net/wifi/p2p/WifiP2pService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDiscoveryStarted:Z

    #@2
    return v0
.end method

.method static synthetic access$12502(Landroid/net/wifi/p2p/WifiP2pService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDiscoveryStarted:Z

    #@2
    return p1
.end method

.method static synthetic access$12600()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 111
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService;->DHCP_RANGE:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$12700()Ljava/lang/Boolean;
    .registers 1

    #@0
    .prologue
    .line 111
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService;->RELOAD:Ljava/lang/Boolean;

    #@2
    return-object v0
.end method

.method static synthetic access$12800(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mClientInfoList:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$12900(Landroid/net/wifi/p2p/WifiP2pService;)B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-byte v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceTransactionId:B

    #@2
    return v0
.end method

.method static synthetic access$12902(Landroid/net/wifi/p2p/WifiP2pService;B)B
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-byte p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceTransactionId:B

    #@2
    return p1
.end method

.method static synthetic access$12904(Landroid/net/wifi/p2p/WifiP2pService;)B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-byte v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceTransactionId:B

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceTransactionId:B

    #@7
    return v0
.end method

.method static synthetic access$13000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$13500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/os/Messenger;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mForegroundAppMessenger:Landroid/os/Messenger;

    #@2
    return-object v0
.end method

.method static synthetic access$13502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/os/Messenger;)Landroid/os/Messenger;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mForegroundAppMessenger:Landroid/os/Messenger;

    #@2
    return-object p1
.end method

.method static synthetic access$13600(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mForegroundAppPkgName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$13602(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mForegroundAppPkgName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$13700(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/ActivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mActivityMgr:Landroid/app/ActivityManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1704()I
    .registers 1

    #@0
    .prologue
    .line 111
    sget v0, Landroid/net/wifi/p2p/WifiP2pService;->mDisableP2pTimeoutIndex:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    sput v0, Landroid/net/wifi/p2p/WifiP2pService;->mDisableP2pTimeoutIndex:I

    #@6
    return v0
.end method

.method static synthetic access$200(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mInterface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2100()I
    .registers 1

    #@0
    .prologue
    .line 111
    sget v0, Landroid/net/wifi/p2p/WifiP2pService;->mGroupCreatingTimeoutIndex:I

    #@2
    return v0
.end method

.method static synthetic access$2104()I
    .registers 1

    #@0
    .prologue
    .line 111
    sget v0, Landroid/net/wifi/p2p/WifiP2pService;->mGroupCreatingTimeoutIndex:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    sput v0, Landroid/net/wifi/p2p/WifiP2pService;->mGroupCreatingTimeoutIndex:I

    #@6
    return v0
.end method

.method static synthetic access$3100(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDialogWps:Landroid/app/AlertDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$3502(Landroid/net/wifi/p2p/WifiP2pService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDialogWps:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$4400(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$5200(Landroid/net/wifi/p2p/WifiP2pService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mAutonomousGroup:Z

    #@2
    return v0
.end method

.method static synthetic access$5202(Landroid/net/wifi/p2p/WifiP2pService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mAutonomousGroup:Z

    #@2
    return p1
.end method

.method static synthetic access$5300()Ljava/lang/Boolean;
    .registers 1

    #@0
    .prologue
    .line 111
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService;->TRY_REINVOCATION:Ljava/lang/Boolean;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$6000(Landroid/net/wifi/p2p/WifiP2pService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceDiscReqId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$6002(Landroid/net/wifi/p2p/WifiP2pService;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mServiceDiscReqId:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Landroid/net/wifi/p2p/WifiP2pService;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object p1
.end method

.method static synthetic access$6102(Landroid/net/wifi/p2p/WifiP2pService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mJoinExistingGroup:Z

    #@2
    return p1
.end method

.method static synthetic access$6500(Landroid/net/wifi/p2p/WifiP2pService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mJoinState:Z

    #@2
    return v0
.end method

.method static synthetic access$6502(Landroid/net/wifi/p2p/WifiP2pService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mJoinState:Z

    #@2
    return p1
.end method

.method static synthetic access$800(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$8600()Ljava/lang/Boolean;
    .registers 1

    #@0
    .prologue
    .line 111
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService;->NO_RELOAD:Ljava/lang/Boolean;

    #@2
    return-object v0
.end method

.method static synthetic access$8900(Landroid/net/wifi/p2p/WifiP2pService;)Landroid/net/DhcpStateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$8902(Landroid/net/wifi/p2p/WifiP2pService;Landroid/net/DhcpStateMachine;)Landroid/net/DhcpStateMachine;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mDhcpStateMachine:Landroid/net/DhcpStateMachine;

    #@2
    return-object p1
.end method

.method static synthetic access$9000(Landroid/net/wifi/p2p/WifiP2pService;)Lcom/lge/wifi_iface/WifiServiceExtIface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    return-object v0
.end method

.method static synthetic access$9900()Ljava/lang/Boolean;
    .registers 1

    #@0
    .prologue
    .line 111
    sget-object v0, Landroid/net/wifi/p2p/WifiP2pService;->NO_REINVOCATION:Ljava/lang/Boolean;

    #@2
    return-object v0
.end method

.method private enforceAccessPermission()V
    .registers 4

    #@0
    .prologue
    .line 389
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.ACCESS_WIFI_STATE"

    #@4
    const-string v2, "WifiP2pService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 391
    return-void
.end method

.method private enforceChangePermission()V
    .registers 4

    #@0
    .prologue
    .line 394
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CHANGE_WIFI_STATE"

    #@4
    const-string v2, "WifiP2pService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 396
    return-void
.end method


# virtual methods
.method public connectivityServiceReady()V
    .registers 3

    #@0
    .prologue
    .line 359
    const-string/jumbo v1, "network_management"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    .line 360
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@a
    move-result-object v1

    #@b
    iput-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mNwService:Landroid/os/INetworkManagementService;

    #@d
    .line 361
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 410
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_32

    #@a
    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "Permission Denial: can\'t dump WifiP2pService from from pid="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", uid="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 417
    :cond_32
    return-void
.end method

.method public getMessenger()Landroid/os/Messenger;
    .registers 3

    #@0
    .prologue
    .line 403
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService;->enforceAccessPermission()V

    #@3
    .line 404
    invoke-direct {p0}, Landroid/net/wifi/p2p/WifiP2pService;->enforceChangePermission()V

    #@6
    .line 405
    new-instance v0, Landroid/os/Messenger;

    #@8
    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pService;->mP2pStateMachine:Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;

    #@a
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pService$P2pStateMachine;->getHandler()Landroid/os/Handler;

    #@d
    move-result-object v1

    #@e
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@11
    return-object v0
.end method
