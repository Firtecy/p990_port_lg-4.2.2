.class final Landroid/net/wifi/p2p/WifiP2pInfo$1;
.super Ljava/lang/Object;
.source "WifiP2pInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/p2p/WifiP2pInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/wifi/p2p/WifiP2pInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/wifi/p2p/WifiP2pInfo;
    .registers 6
    .parameter "in"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 83
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pInfo;

    #@4
    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pInfo;-><init>()V

    #@7
    .line 84
    .local v0, info:Landroid/net/wifi/p2p/WifiP2pInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@a
    move-result v1

    #@b
    if-ne v1, v2, :cond_2a

    #@d
    move v1, v2

    #@e
    :goto_e
    iput-boolean v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    #@10
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@13
    move-result v1

    #@14
    if-ne v1, v2, :cond_17

    #@16
    move v3, v2

    #@17
    :cond_17
    iput-boolean v3, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    #@19
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@1c
    move-result v1

    #@1d
    if-ne v1, v2, :cond_29

    #@1f
    .line 88
    :try_start_1f
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@22
    move-result-object v1

    #@23
    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@26
    move-result-object v1

    #@27
    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;
    :try_end_29
    .catch Ljava/net/UnknownHostException; {:try_start_1f .. :try_end_29} :catch_2c

    #@29
    .line 91
    :cond_29
    :goto_29
    return-object v0

    #@2a
    :cond_2a
    move v1, v3

    #@2b
    .line 84
    goto :goto_e

    #@2c
    .line 89
    :catch_2c
    move-exception v1

    #@2d
    goto :goto_29
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 81
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pInfo$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/wifi/p2p/WifiP2pInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/wifi/p2p/WifiP2pInfo;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 95
    new-array v0, p1, [Landroid/net/wifi/p2p/WifiP2pInfo;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 81
    invoke-virtual {p0, p1}, Landroid/net/wifi/p2p/WifiP2pInfo$1;->newArray(I)[Landroid/net/wifi/p2p/WifiP2pInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
