.class public Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
.super Ljava/lang/Object;
.source "WifiP2pServiceRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mLength:I

.field private mProtocolType:I

.field private mQuery:Ljava/lang/String;

.field private mTransId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 263
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest$1;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(IIILjava/lang/String;)V
    .registers 5
    .parameter "serviceType"
    .parameter "length"
    .parameter "transId"
    .parameter "query"

    #@0
    .prologue
    .line 94
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 95
    iput p1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@5
    .line 96
    iput p2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@7
    .line 97
    iput p3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mTransId:I

    #@9
    .line 98
    iput-object p4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@b
    .line 99
    return-void
.end method

.method synthetic constructor <init>(IIILjava/lang/String;Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;-><init>(IIILjava/lang/String;)V

    #@3
    return-void
.end method

.method protected constructor <init>(ILjava/lang/String;)V
    .registers 4
    .parameter "protocolType"
    .parameter "query"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    invoke-direct {p0, p2}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->validateQuery(Ljava/lang/String;)V

    #@6
    .line 76
    iput p1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@8
    .line 77
    iput-object p2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@a
    .line 78
    if-eqz p2, :cond_17

    #@c
    .line 79
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@f
    move-result v0

    #@10
    div-int/lit8 v0, v0, 0x2

    #@12
    add-int/lit8 v0, v0, 0x2

    #@14
    iput v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@16
    .line 83
    :goto_16
    return-void

    #@17
    .line 81
    :cond_17
    const/4 v0, 0x2

    #@18
    iput v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@1a
    goto :goto_16
.end method

.method public static newInstance(I)Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    .registers 3
    .parameter "protocolType"

    #@0
    .prologue
    .line 209
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;-><init>(ILjava/lang/String;)V

    #@6
    return-object v0
.end method

.method public static newInstance(ILjava/lang/String;)Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    .registers 3
    .parameter "protocolType"
    .parameter "queryData"

    #@0
    .prologue
    .line 194
    new-instance v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;-><init>(ILjava/lang/String;)V

    #@5
    return-object v0
.end method

.method private validateQuery(Ljava/lang/String;)V
    .registers 11
    .parameter "query"

    #@0
    .prologue
    .line 155
    if-nez p1, :cond_3

    #@2
    .line 179
    :cond_2
    return-void

    #@3
    .line 159
    :cond_3
    const v0, 0xffff

    #@6
    .line 160
    .local v0, UNSIGNED_SHORT_MAX:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9
    move-result v6

    #@a
    rem-int/lit8 v6, v6, 0x2

    #@c
    const/4 v7, 0x1

    #@d
    if-ne v6, v7, :cond_29

    #@f
    .line 161
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v7, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string/jumbo v8, "query size is invalid. query="

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v6

    #@29
    .line 164
    :cond_29
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2c
    move-result v6

    #@2d
    div-int/lit8 v6, v6, 0x2

    #@2f
    if-le v6, v0, :cond_4f

    #@31
    .line 165
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@33
    new-instance v7, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string/jumbo v8, "query size is too large. len="

    #@3b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@42
    move-result v8

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v6

    #@4f
    .line 170
    :cond_4f
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@52
    move-result-object p1

    #@53
    .line 171
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    #@56
    move-result-object v3

    #@57
    .line 172
    .local v3, chars:[C
    move-object v1, v3

    #@58
    .local v1, arr$:[C
    array-length v5, v1

    #@59
    .local v5, len$:I
    const/4 v4, 0x0

    #@5a
    .local v4, i$:I
    :goto_5a
    if-ge v4, v5, :cond_2

    #@5c
    aget-char v2, v1, v4

    #@5e
    .line 173
    .local v2, c:C
    const/16 v6, 0x30

    #@60
    if-lt v2, v6, :cond_66

    #@62
    const/16 v6, 0x39

    #@64
    if-le v2, v6, :cond_88

    #@66
    :cond_66
    const/16 v6, 0x61

    #@68
    if-lt v2, v6, :cond_6e

    #@6a
    const/16 v6, 0x66

    #@6c
    if-le v2, v6, :cond_88

    #@6e
    .line 175
    :cond_6e
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@70
    new-instance v7, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string/jumbo v8, "query should be hex string. query="

    #@78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v7

    #@84
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@87
    throw v6

    #@88
    .line 172
    :cond_88
    add-int/lit8 v4, v4, 0x1

    #@8a
    goto :goto_5a
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 251
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 214
    if-ne p1, p0, :cond_5

    #@4
    .line 237
    :cond_4
    :goto_4
    return v1

    #@5
    .line 217
    :cond_5
    instance-of v3, p1, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    .line 218
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 221
    check-cast v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;

    #@e
    .line 227
    .local v0, req:Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;
    iget v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@10
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@12
    if-ne v3, v4, :cond_1a

    #@14
    iget v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@16
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@18
    if-eq v3, v4, :cond_1c

    #@1a
    :cond_1a
    move v1, v2

    #@1b
    .line 229
    goto :goto_4

    #@1c
    .line 232
    :cond_1c
    iget-object v3, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@1e
    if-nez v3, :cond_24

    #@20
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@22
    if-eqz v3, :cond_4

    #@24
    .line 234
    :cond_24
    iget-object v1, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@26
    if-eqz v1, :cond_31

    #@28
    .line 235
    iget-object v1, v0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@2a
    iget-object v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v1

    #@30
    goto :goto_4

    #@31
    :cond_31
    move v1, v2

    #@32
    .line 237
    goto :goto_4
.end method

.method public getSupplicantQuery()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 135
    new-instance v0, Ljava/lang/StringBuffer;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@7
    .line 137
    .local v0, sb:Ljava/lang/StringBuffer;
    const-string v1, "%02x"

    #@9
    new-array v2, v5, [Ljava/lang/Object;

    #@b
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@d
    and-int/lit16 v3, v3, 0xff

    #@f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v3

    #@13
    aput-object v3, v2, v4

    #@15
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    .line 138
    const-string v1, "%02x"

    #@1e
    new-array v2, v5, [Ljava/lang/Object;

    #@20
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@22
    shr-int/lit8 v3, v3, 0x8

    #@24
    and-int/lit16 v3, v3, 0xff

    #@26
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v3

    #@2a
    aput-object v3, v2, v4

    #@2c
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@33
    .line 139
    const-string v1, "%02x"

    #@35
    new-array v2, v5, [Ljava/lang/Object;

    #@37
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@39
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v3

    #@3d
    aput-object v3, v2, v4

    #@3f
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@46
    .line 140
    const-string v1, "%02x"

    #@48
    new-array v2, v5, [Ljava/lang/Object;

    #@4a
    iget v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mTransId:I

    #@4c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f
    move-result-object v3

    #@50
    aput-object v3, v2, v4

    #@52
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@59
    .line 141
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@5b
    if-eqz v1, :cond_62

    #@5d
    .line 142
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@62
    .line 145
    :cond_62
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    return-object v1
.end method

.method public getTransactionId()I
    .registers 2

    #@0
    .prologue
    .line 108
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mTransId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 242
    const/16 v0, 0x11

    #@2
    .line 243
    .local v0, result:I
    iget v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@4
    add-int/lit16 v0, v1, 0x20f

    #@6
    .line 244
    mul-int/lit8 v1, v0, 0x1f

    #@8
    iget v2, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@a
    add-int v0, v1, v2

    #@c
    .line 245
    mul-int/lit8 v2, v0, 0x1f

    #@e
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@10
    if-nez v1, :cond_16

    #@12
    const/4 v1, 0x0

    #@13
    :goto_13
    add-int v0, v2, v1

    #@15
    .line 246
    return v0

    #@16
    .line 245
    :cond_16
    iget-object v1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@18
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@1b
    move-result v1

    #@1c
    goto :goto_13
.end method

.method public setTransactionId(I)V
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 118
    iput p1, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mTransId:I

    #@2
    .line 119
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 256
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mProtocolType:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 257
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mLength:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 258
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mTransId:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 259
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceRequest;->mQuery:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 260
    return-void
.end method
