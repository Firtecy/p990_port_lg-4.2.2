.class public Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;
.super Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;
.source "WifiP2pUpnpServiceResponse.java"


# instance fields
.field private mUniqueServiceNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVersion:I


# direct methods
.method protected constructor <init>(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)V
    .registers 11
    .parameter "status"
    .parameter "transId"
    .parameter "dev"
    .parameter "data"

    #@0
    .prologue
    .line 82
    const/4 v1, 0x2

    #@1
    move-object v0, p0

    #@2
    move v2, p1

    #@3
    move v3, p2

    #@4
    move-object v4, p3

    #@5
    move-object v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;-><init>(IIILandroid/net/wifi/p2p/WifiP2pDevice;[B)V

    #@9
    .line 84
    invoke-direct {p0}, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->parse()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_17

    #@f
    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string v1, "Malformed upnp service response"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 87
    :cond_17
    return-void
.end method

.method static newInstance(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;
    .registers 7
    .parameter "status"
    .parameter "transId"
    .parameter "device"
    .parameter "data"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 148
    if-eqz p0, :cond_9

    #@3
    .line 149
    new-instance v1, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;

    #@5
    invoke-direct {v1, p0, p1, p2, v2}, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;-><init>(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)V

    #@8
    .line 157
    :goto_8
    return-object v1

    #@9
    .line 153
    :cond_9
    :try_start_9
    new-instance v1, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;

    #@b
    invoke-direct {v1, p0, p1, p2, p3}, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;-><init>(IILandroid/net/wifi/p2p/WifiP2pDevice;[B)V
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    goto :goto_8

    #@f
    .line 154
    :catch_f
    move-exception v0

    #@10
    .line 155
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@13
    move-object v1, v2

    #@14
    .line 157
    goto :goto_8
.end method

.method private parse()Z
    .registers 10

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 101
    iget-object v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@4
    if-nez v7, :cond_7

    #@6
    .line 116
    :cond_6
    :goto_6
    return v5

    #@7
    .line 106
    :cond_7
    iget-object v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@9
    array-length v7, v7

    #@a
    if-ge v7, v5, :cond_e

    #@c
    move v5, v6

    #@d
    .line 107
    goto :goto_6

    #@e
    .line 110
    :cond_e
    iget-object v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@10
    aget-byte v6, v7, v6

    #@12
    and-int/lit16 v6, v6, 0xff

    #@14
    iput v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mVersion:I

    #@16
    .line 111
    new-instance v6, Ljava/lang/String;

    #@18
    iget-object v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@1a
    iget-object v8, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mData:[B

    #@1c
    array-length v8, v8

    #@1d
    add-int/lit8 v8, v8, -0x1

    #@1f
    invoke-direct {v6, v7, v5, v8}, Ljava/lang/String;-><init>([BII)V

    #@22
    const-string v7, ","

    #@24
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    .line 112
    .local v4, names:[Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    #@2a
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@2d
    iput-object v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mUniqueServiceNames:Ljava/util/List;

    #@2f
    .line 113
    move-object v0, v4

    #@30
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@31
    .local v2, len$:I
    const/4 v1, 0x0

    #@32
    .local v1, i$:I
    :goto_32
    if-ge v1, v2, :cond_6

    #@34
    aget-object v3, v0, v1

    #@36
    .line 114
    .local v3, name:Ljava/lang/String;
    iget-object v6, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mUniqueServiceNames:Ljava/util/List;

    #@38
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3b
    .line 113
    add-int/lit8 v1, v1, 0x1

    #@3d
    goto :goto_32
.end method


# virtual methods
.method public getUniqueServiceNames()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mUniqueServiceNames:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getVersion()I
    .registers 2

    #@0
    .prologue
    .line 51
    iget v0, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mVersion:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 121
    new-instance v2, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 122
    .local v2, sbuf:Ljava/lang/StringBuffer;
    const-string/jumbo v3, "serviceType:UPnP("

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b
    move-result-object v3

    #@c
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mServiceType:I

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@11
    move-result-object v3

    #@12
    const-string v4, ")"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@17
    .line 123
    const-string v3, " status:"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    move-result-object v3

    #@1d
    iget v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mStatus:I

    #@1f
    invoke-static {v4}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse$Status;->toString(I)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@26
    .line 124
    const-string v3, " srcAddr:"

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, p0, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->mDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2e
    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@33
    .line 125
    const-string v3, " version:"

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@38
    move-result-object v3

    #@39
    const-string v4, "%02x"

    #@3b
    const/4 v5, 0x1

    #@3c
    new-array v5, v5, [Ljava/lang/Object;

    #@3e
    const/4 v6, 0x0

    #@3f
    iget v7, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mVersion:I

    #@41
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@44
    move-result-object v7

    #@45
    aput-object v7, v5, v6

    #@47
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@4e
    .line 126
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mUniqueServiceNames:Ljava/util/List;

    #@50
    if-eqz v3, :cond_6e

    #@52
    .line 127
    iget-object v3, p0, Landroid/net/wifi/p2p/nsd/WifiP2pUpnpServiceResponse;->mUniqueServiceNames:Ljava/util/List;

    #@54
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@57
    move-result-object v0

    #@58
    .local v0, i$:Ljava/util/Iterator;
    :goto_58
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@5b
    move-result v3

    #@5c
    if-eqz v3, :cond_6e

    #@5e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@61
    move-result-object v1

    #@62
    check-cast v1, Ljava/lang/String;

    #@64
    .line 128
    .local v1, name:Ljava/lang/String;
    const-string v3, " usn:"

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6d
    goto :goto_58

    #@6e
    .line 131
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #name:Ljava/lang/String;
    :cond_6e
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    return-object v3
.end method
