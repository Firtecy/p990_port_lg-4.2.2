.class public Landroid/net/wifi/WifiVZWConfiguration;
.super Ljava/lang/Object;
.source "WifiVZWConfiguration.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;,
        Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;,
        Landroid/net/wifi/WifiVZWConfiguration$Status;,
        Landroid/net/wifi/WifiVZWConfiguration$GroupCipher;,
        Landroid/net/wifi/WifiVZWConfiguration$PairwiseCipher;,
        Landroid/net/wifi/WifiVZWConfiguration$AuthAlgorithm;,
        Landroid/net/wifi/WifiVZWConfiguration$Protocol;,
        Landroid/net/wifi/WifiVZWConfiguration$KeyMgmt;,
        Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/WifiVZWConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public static final DISABLED_AUTH_FAILURE:I = 0x3

.field public static final DISABLED_DHCP_FAILURE:I = 0x2

.field public static final DISABLED_DNS_FAILURE:I = 0x1

.field public static final DISABLED_UNKNOWN_REASON:I = 0x0

.field private static final HOSTAPD_ACCEPT_MAC_FILE:Ljava/lang/String; = "data/hostapd/hostapd.accept"

.field private static final HOSTAPD_DENY_MAC_FILE:Ljava/lang/String; = "data/hostapd/hostapd.deny"

.field public static final INVALID_NETWORK_ID:I = -0x1

.field public static final bssidVarName:Ljava/lang/String; = "bssid"

.field public static final hiddenSSIDVarName:Ljava/lang/String; = "scan_ssid"

.field public static final priorityVarName:Ljava/lang/String; = "priority"

.field public static final pskVarName:Ljava/lang/String; = "psk"

.field public static final ssidVarName:Ljava/lang/String; = "ssid"

.field public static final wepKeyVarNames:[Ljava/lang/String; = null

.field public static final wepTxKeyIdxVarName:Ljava/lang/String; = "wep_tx_keyidx"


# instance fields
.field public BSSID:Ljava/lang/String;

.field public Channel:I

.field public CountryCode:Ljava/lang/String;

.field public Maxscb:I

.field public SSID:Ljava/lang/String;

.field public accept_mac_file:Ljava/lang/String;

.field public allowedAuthAlgorithms:Ljava/util/BitSet;

.field public allowedGroupCiphers:Ljava/util/BitSet;

.field public allowedKeyManagement:Ljava/util/BitSet;

.field public allowedPairwiseCiphers:Ljava/util/BitSet;

.field public allowedProtocols:Ljava/util/BitSet;

.field public anonymous_identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public ap_isolate:I

.field public authMode:I

.field public ca_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public client_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public deny_mac_file:Ljava/lang/String;

.field public disableReason:I

.field public eap:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public encMode:I

.field public enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public hiddenSSID:Z

.field public hw_mode:Ljava/lang/String;

.field public identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public ieee_mode:I

.field public ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

.field public linkProperties:Landroid/net/LinkProperties;

.field public macaddr_acl:I

.field public networkId:I

.field public pariwise:Ljava/lang/String;

.field public password:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public phase2:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public preSharedKey:Ljava/lang/String;

.field public priority:I

.field public private_key:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

.field public proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

.field public secMode:I

.field public status:I

.field public wepKeys1:Ljava/lang/String;

.field public wepKeys2:Ljava/lang/String;

.field public wepKeys3:Ljava/lang/String;

.field public wepKeys4:Ljava/lang/String;

.field public wepTxKeyIndex:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 43
    const/4 v0, 0x4

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string/jumbo v2, "wep_key0"

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    const-string/jumbo v2, "wep_key1"

    #@d
    aput-object v2, v0, v1

    #@f
    const/4 v1, 0x2

    #@10
    const-string/jumbo v2, "wep_key2"

    #@13
    aput-object v2, v0, v1

    #@15
    const/4 v1, 0x3

    #@16
    const-string/jumbo v2, "wep_key3"

    #@19
    aput-object v2, v0, v1

    #@1b
    sput-object v0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeyVarNames:[Ljava/lang/String;

    #@1d
    .line 796
    new-instance v0, Landroid/net/wifi/WifiVZWConfiguration$1;

    #@1f
    invoke-direct {v0}, Landroid/net/wifi/WifiVZWConfiguration$1;-><init>()V

    #@22
    sput-object v0, Landroid/net/wifi/WifiVZWConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    return-void
.end method

.method public constructor <init>()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 505
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 79
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@7
    const-string v5, "eap"

    #@9
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@c
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->eap:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@e
    .line 81
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@10
    const-string/jumbo v5, "phase2"

    #@13
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@16
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->phase2:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@18
    .line 83
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@1a
    const-string v5, "identity"

    #@1c
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@1f
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@21
    .line 85
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@23
    const-string v5, "anonymous_identity"

    #@25
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@28
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->anonymous_identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@2a
    .line 87
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@2c
    const-string/jumbo v5, "password"

    #@2f
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@32
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->password:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@34
    .line 89
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@36
    const-string v5, "client_cert"

    #@38
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@3b
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->client_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@3d
    .line 91
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@3f
    const-string/jumbo v5, "private_key"

    #@42
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@45
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->private_key:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@47
    .line 93
    new-instance v4, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@49
    const-string v5, "ca_cert"

    #@4b
    invoke-direct {v4, p0, v5, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@4e
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->ca_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@50
    .line 96
    const/16 v4, 0x8

    #@52
    new-array v4, v4, [Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@54
    iget-object v5, p0, Landroid/net/wifi/WifiVZWConfiguration;->eap:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@56
    aput-object v5, v4, v8

    #@58
    const/4 v5, 0x1

    #@59
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->phase2:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@5b
    aput-object v6, v4, v5

    #@5d
    const/4 v5, 0x2

    #@5e
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@60
    aput-object v6, v4, v5

    #@62
    const/4 v5, 0x3

    #@63
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->anonymous_identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@65
    aput-object v6, v4, v5

    #@67
    const/4 v5, 0x4

    #@68
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->password:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@6a
    aput-object v6, v4, v5

    #@6c
    const/4 v5, 0x5

    #@6d
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->client_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@6f
    aput-object v6, v4, v5

    #@71
    const/4 v5, 0x6

    #@72
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->private_key:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@74
    aput-object v6, v4, v5

    #@76
    const/4 v5, 0x7

    #@77
    iget-object v6, p0, Landroid/net/wifi/WifiVZWConfiguration;->ca_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@79
    aput-object v6, v4, v5

    #@7b
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@7d
    .line 423
    const-string v4, "data/hostapd/hostapd.accept"

    #@7f
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@81
    .line 424
    const-string v4, "data/hostapd/hostapd.deny"

    #@83
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@85
    .line 506
    const/4 v4, -0x1

    #@86
    iput v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->networkId:I

    #@88
    .line 507
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@8a
    .line 508
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->BSSID:Ljava/lang/String;

    #@8c
    .line 509
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->priority:I

    #@8e
    .line 510
    iput-boolean v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@90
    .line 511
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->disableReason:I

    #@92
    .line 512
    new-instance v4, Ljava/util/BitSet;

    #@94
    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    #@97
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@99
    .line 513
    new-instance v4, Ljava/util/BitSet;

    #@9b
    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    #@9e
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@a0
    .line 514
    new-instance v4, Ljava/util/BitSet;

    #@a2
    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    #@a5
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@a7
    .line 515
    new-instance v4, Ljava/util/BitSet;

    #@a9
    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    #@ac
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@ae
    .line 516
    new-instance v4, Ljava/util/BitSet;

    #@b0
    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    #@b3
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@b5
    .line 521
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@b7
    .line 522
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@b9
    .line 523
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@bb
    .line 524
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@bd
    .line 525
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@bf
    .local v0, arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    array-length v3, v0

    #@c0
    .local v3, len$:I
    const/4 v2, 0x0

    #@c1
    .local v2, i$:I
    :goto_c1
    if-ge v2, v3, :cond_cb

    #@c3
    aget-object v1, v0, v2

    #@c5
    .line 526
    .local v1, field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    invoke-virtual {v1, v7}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@c8
    .line 525
    add-int/lit8 v2, v2, 0x1

    #@ca
    goto :goto_c1

    #@cb
    .line 528
    .end local v1           #field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    :cond_cb
    sget-object v4, Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;->UNASSIGNED:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@cd
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@cf
    .line 529
    sget-object v4, Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@d1
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@d3
    .line 530
    new-instance v4, Landroid/net/LinkProperties;

    #@d5
    invoke-direct {v4}, Landroid/net/LinkProperties;-><init>()V

    #@d8
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@da
    .line 531
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@dc
    .line 532
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@de
    .line 533
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@e0
    .line 534
    iput-object v7, p0, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@e2
    .line 535
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@e4
    .line 536
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@e6
    .line 537
    iput v8, p0, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@e8
    .line 538
    const-string v4, "data/hostapd/hostapd.accept"

    #@ea
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@ec
    .line 539
    const-string v4, "data/hostapd/hostapd.deny"

    #@ee
    iput-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@f0
    .line 540
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/WifiVZWConfiguration;)V
    .registers 6
    .parameter "source"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 707
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 79
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@6
    const-string v2, "eap"

    #@8
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@b
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->eap:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@d
    .line 81
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@f
    const-string/jumbo v2, "phase2"

    #@12
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@15
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->phase2:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@17
    .line 83
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@19
    const-string v2, "identity"

    #@1b
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@1e
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@20
    .line 85
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@22
    const-string v2, "anonymous_identity"

    #@24
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@27
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->anonymous_identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@29
    .line 87
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@2b
    const-string/jumbo v2, "password"

    #@2e
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@31
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->password:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@33
    .line 89
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@35
    const-string v2, "client_cert"

    #@37
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@3a
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->client_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@3c
    .line 91
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@3e
    const-string/jumbo v2, "private_key"

    #@41
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@44
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->private_key:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@46
    .line 93
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@48
    const-string v2, "ca_cert"

    #@4a
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiVZWConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiVZWConfiguration$1;)V

    #@4d
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->ca_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@4f
    .line 96
    const/16 v1, 0x8

    #@51
    new-array v1, v1, [Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@53
    const/4 v2, 0x0

    #@54
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->eap:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@56
    aput-object v3, v1, v2

    #@58
    const/4 v2, 0x1

    #@59
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->phase2:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@5b
    aput-object v3, v1, v2

    #@5d
    const/4 v2, 0x2

    #@5e
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@60
    aput-object v3, v1, v2

    #@62
    const/4 v2, 0x3

    #@63
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->anonymous_identity:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@65
    aput-object v3, v1, v2

    #@67
    const/4 v2, 0x4

    #@68
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->password:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@6a
    aput-object v3, v1, v2

    #@6c
    const/4 v2, 0x5

    #@6d
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->client_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@6f
    aput-object v3, v1, v2

    #@71
    const/4 v2, 0x6

    #@72
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->private_key:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@74
    aput-object v3, v1, v2

    #@76
    const/4 v2, 0x7

    #@77
    iget-object v3, p0, Landroid/net/wifi/WifiVZWConfiguration;->ca_cert:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@79
    aput-object v3, v1, v2

    #@7b
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@7d
    .line 423
    const-string v1, "data/hostapd/hostapd.accept"

    #@7f
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@81
    .line 424
    const-string v1, "data/hostapd/hostapd.deny"

    #@83
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@85
    .line 708
    if-eqz p1, :cond_13a

    #@87
    .line 709
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->networkId:I

    #@89
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->networkId:I

    #@8b
    .line 710
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->status:I

    #@8d
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->status:I

    #@8f
    .line 711
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->disableReason:I

    #@91
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->disableReason:I

    #@93
    .line 712
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@95
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@97
    .line 713
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->BSSID:Ljava/lang/String;

    #@99
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->BSSID:Ljava/lang/String;

    #@9b
    .line 714
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@9d
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@9f
    .line 720
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@a1
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@a3
    .line 721
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@a5
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@a7
    .line 722
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@a9
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@ab
    .line 723
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@ad
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@af
    .line 725
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@b1
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@b3
    .line 726
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->priority:I

    #@b5
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->priority:I

    #@b7
    .line 727
    iget-boolean v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@b9
    iput-boolean v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@bb
    .line 728
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@bd
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@c0
    move-result-object v1

    #@c1
    check-cast v1, Ljava/util/BitSet;

    #@c3
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@c5
    .line 729
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@c7
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@ca
    move-result-object v1

    #@cb
    check-cast v1, Ljava/util/BitSet;

    #@cd
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@cf
    .line 730
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@d1
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@d4
    move-result-object v1

    #@d5
    check-cast v1, Ljava/util/BitSet;

    #@d7
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@d9
    .line 731
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@db
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@de
    move-result-object v1

    #@df
    check-cast v1, Ljava/util/BitSet;

    #@e1
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@e3
    .line 732
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@e5
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@e8
    move-result-object v1

    #@e9
    check-cast v1, Ljava/util/BitSet;

    #@eb
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@ed
    .line 734
    const/4 v0, 0x0

    #@ee
    .local v0, i:I
    :goto_ee
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@f0
    array-length v1, v1

    #@f1
    if-ge v0, v1, :cond_105

    #@f3
    .line 735
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@f5
    aget-object v1, v1, v0

    #@f7
    iget-object v2, p1, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@f9
    aget-object v2, v2, v0

    #@fb
    invoke-virtual {v2}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@fe
    move-result-object v2

    #@ff
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@102
    .line 734
    add-int/lit8 v0, v0, 0x1

    #@104
    goto :goto_ee

    #@105
    .line 737
    :cond_105
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@107
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@109
    .line 738
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@10b
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@10d
    .line 739
    new-instance v1, Landroid/net/LinkProperties;

    #@10f
    iget-object v2, p1, Landroid/net/wifi/WifiVZWConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@111
    invoke-direct {v1, v2}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@114
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@116
    .line 740
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@118
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@11a
    .line 741
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@11c
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@11e
    .line 742
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@120
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@122
    .line 743
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@124
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@126
    .line 744
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@128
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@12a
    .line 745
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@12c
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@12e
    .line 746
    iget v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@130
    iput v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@132
    .line 747
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@134
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@136
    .line 748
    iget-object v1, p1, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@138
    iput-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@13a
    .line 750
    .end local v0           #i:I
    :cond_13a
    return-void
.end method

.method static synthetic access$100(Landroid/os/Parcel;)Ljava/util/BitSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-static {p0}, Landroid/net/wifi/WifiVZWConfiguration;->readBitSet(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static readBitSet(Landroid/os/Parcel;)Ljava/util/BitSet;
    .registers 5
    .parameter "src"

    #@0
    .prologue
    .line 645
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 647
    .local v0, cardinality:I
    new-instance v2, Ljava/util/BitSet;

    #@6
    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    #@9
    .line 648
    .local v2, set:Ljava/util/BitSet;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_16

    #@c
    .line 649
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v3

    #@10
    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    #@13
    .line 648
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_a

    #@16
    .line 652
    :cond_16
    return-object v2
.end method

.method private static writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V
    .registers 4
    .parameter "dest"
    .parameter "set"

    #@0
    .prologue
    .line 656
    const/4 v0, -0x1

    #@1
    .line 658
    .local v0, nextSetBit:I
    invoke-virtual {p1}, Ljava/util/BitSet;->cardinality()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 660
    :goto_8
    add-int/lit8 v1, v0, 0x1

    #@a
    invoke-virtual {p1, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    #@d
    move-result v0

    #@e
    const/4 v1, -0x1

    #@f
    if-eq v0, v1, :cond_15

    #@11
    .line 661
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    goto :goto_8

    #@15
    .line 663
    :cond_15
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 703
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAuthType()I
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    const/4 v3, 0x3

    #@2
    const/4 v2, 0x2

    #@3
    const/4 v0, 0x1

    #@4
    .line 667
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@6
    invoke-virtual {v4, v0}, Ljava/util/BitSet;->get(I)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_d

    #@c
    .line 676
    :goto_c
    return v0

    #@d
    .line 669
    :cond_d
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@f
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_17

    #@15
    move v0, v1

    #@16
    .line 670
    goto :goto_c

    #@17
    .line 671
    :cond_17
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@19
    invoke-virtual {v0, v2}, Ljava/util/BitSet;->get(I)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_21

    #@1f
    move v0, v2

    #@20
    .line 672
    goto :goto_c

    #@21
    .line 673
    :cond_21
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@23
    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2b

    #@29
    move v0, v3

    #@2a
    .line 674
    goto :goto_c

    #@2b
    .line 676
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_c
.end method

.method public setAuthType(I)V
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 691
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    #@6
    .line 692
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    #@c
    .line 693
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@e
    const/4 v1, 0x2

    #@f
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    #@12
    .line 694
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@14
    const/4 v1, 0x3

    #@15
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    #@18
    .line 695
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@1a
    const/4 v1, 0x4

    #@1b
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    #@1e
    .line 697
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@20
    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    #@23
    .line 698
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 16

    #@0
    .prologue
    const/16 v14, 0xa

    #@2
    .line 544
    new-instance v9, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 545
    .local v9, sbuf:Ljava/lang/StringBuilder;
    iget v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->status:I

    #@9
    if-nez v11, :cond_6d

    #@b
    .line 546
    const-string v11, "* "

    #@d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 550
    :cond_10
    :goto_10
    const-string v11, "ID: "

    #@12
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v11

    #@16
    iget v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->networkId:I

    #@18
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v11

    #@1c
    const-string v12, " SSID: "

    #@1e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v11

    #@22
    iget-object v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@24
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v11

    #@28
    const-string v12, " BSSID: "

    #@2a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v11

    #@2e
    iget-object v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->BSSID:Ljava/lang/String;

    #@30
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v11

    #@34
    const-string v12, " PRIO: "

    #@36
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v11

    #@3a
    iget v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->priority:I

    #@3c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v11

    #@40
    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@43
    .line 553
    const-string v11, " KeyMgmt:"

    #@45
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 554
    const/4 v5, 0x0

    #@49
    .local v5, k:I
    :goto_49
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@4b
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@4e
    move-result v11

    #@4f
    if-ge v5, v11, :cond_8a

    #@51
    .line 555
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@53
    invoke-virtual {v11, v5}, Ljava/util/BitSet;->get(I)Z

    #@56
    move-result v11

    #@57
    if-eqz v11, :cond_6a

    #@59
    .line 556
    const-string v11, " "

    #@5b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 557
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@60
    array-length v11, v11

    #@61
    if-ge v5, v11, :cond_84

    #@63
    .line 558
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@65
    aget-object v11, v11, v5

    #@67
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 554
    :cond_6a
    :goto_6a
    add-int/lit8 v5, v5, 0x1

    #@6c
    goto :goto_49

    #@6d
    .line 547
    .end local v5           #k:I
    :cond_6d
    iget v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->status:I

    #@6f
    const/4 v12, 0x1

    #@70
    if-ne v11, v12, :cond_10

    #@72
    .line 548
    const-string v11, "- DSBLE: "

    #@74
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v11

    #@78
    iget v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->disableReason:I

    #@7a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v11

    #@7e
    const-string v12, " "

    #@80
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    goto :goto_10

    #@84
    .line 560
    .restart local v5       #k:I
    :cond_84
    const-string v11, "??"

    #@86
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    goto :goto_6a

    #@8a
    .line 564
    :cond_8a
    const-string v11, " Protocols:"

    #@8c
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    .line 565
    const/4 v7, 0x0

    #@90
    .local v7, p:I
    :goto_90
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@92
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@95
    move-result v11

    #@96
    if-ge v7, v11, :cond_ba

    #@98
    .line 566
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@9a
    invoke-virtual {v11, v7}, Ljava/util/BitSet;->get(I)Z

    #@9d
    move-result v11

    #@9e
    if-eqz v11, :cond_b1

    #@a0
    .line 567
    const-string v11, " "

    #@a2
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 568
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$Protocol;->strings:[Ljava/lang/String;

    #@a7
    array-length v11, v11

    #@a8
    if-ge v7, v11, :cond_b4

    #@aa
    .line 569
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$Protocol;->strings:[Ljava/lang/String;

    #@ac
    aget-object v11, v11, v7

    #@ae
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    .line 565
    :cond_b1
    :goto_b1
    add-int/lit8 v7, v7, 0x1

    #@b3
    goto :goto_90

    #@b4
    .line 571
    :cond_b4
    const-string v11, "??"

    #@b6
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    goto :goto_b1

    #@ba
    .line 575
    :cond_ba
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@bd
    .line 576
    const-string v11, " AuthAlgorithms:"

    #@bf
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    .line 577
    const/4 v0, 0x0

    #@c3
    .local v0, a:I
    :goto_c3
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@c5
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@c8
    move-result v11

    #@c9
    if-ge v0, v11, :cond_ed

    #@cb
    .line 578
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@cd
    invoke-virtual {v11, v0}, Ljava/util/BitSet;->get(I)Z

    #@d0
    move-result v11

    #@d1
    if-eqz v11, :cond_e4

    #@d3
    .line 579
    const-string v11, " "

    #@d5
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    .line 580
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$AuthAlgorithm;->strings:[Ljava/lang/String;

    #@da
    array-length v11, v11

    #@db
    if-ge v0, v11, :cond_e7

    #@dd
    .line 581
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$AuthAlgorithm;->strings:[Ljava/lang/String;

    #@df
    aget-object v11, v11, v0

    #@e1
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    .line 577
    :cond_e4
    :goto_e4
    add-int/lit8 v0, v0, 0x1

    #@e6
    goto :goto_c3

    #@e7
    .line 583
    :cond_e7
    const-string v11, "??"

    #@e9
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    goto :goto_e4

    #@ed
    .line 587
    :cond_ed
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@f0
    .line 588
    const-string v11, " PairwiseCiphers:"

    #@f2
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    .line 589
    const/4 v8, 0x0

    #@f6
    .local v8, pc:I
    :goto_f6
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@f8
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@fb
    move-result v11

    #@fc
    if-ge v8, v11, :cond_120

    #@fe
    .line 590
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@100
    invoke-virtual {v11, v8}, Ljava/util/BitSet;->get(I)Z

    #@103
    move-result v11

    #@104
    if-eqz v11, :cond_117

    #@106
    .line 591
    const-string v11, " "

    #@108
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    .line 592
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$PairwiseCipher;->strings:[Ljava/lang/String;

    #@10d
    array-length v11, v11

    #@10e
    if-ge v8, v11, :cond_11a

    #@110
    .line 593
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$PairwiseCipher;->strings:[Ljava/lang/String;

    #@112
    aget-object v11, v11, v8

    #@114
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    .line 589
    :cond_117
    :goto_117
    add-int/lit8 v8, v8, 0x1

    #@119
    goto :goto_f6

    #@11a
    .line 595
    :cond_11a
    const-string v11, "??"

    #@11c
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    goto :goto_117

    #@120
    .line 599
    :cond_120
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@123
    .line 600
    const-string v11, " GroupCiphers:"

    #@125
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    .line 601
    const/4 v3, 0x0

    #@129
    .local v3, gc:I
    :goto_129
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@12b
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@12e
    move-result v11

    #@12f
    if-ge v3, v11, :cond_153

    #@131
    .line 602
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@133
    invoke-virtual {v11, v3}, Ljava/util/BitSet;->get(I)Z

    #@136
    move-result v11

    #@137
    if-eqz v11, :cond_14a

    #@139
    .line 603
    const-string v11, " "

    #@13b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    .line 604
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$GroupCipher;->strings:[Ljava/lang/String;

    #@140
    array-length v11, v11

    #@141
    if-ge v3, v11, :cond_14d

    #@143
    .line 605
    sget-object v11, Landroid/net/wifi/WifiVZWConfiguration$GroupCipher;->strings:[Ljava/lang/String;

    #@145
    aget-object v11, v11, v3

    #@147
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    .line 601
    :cond_14a
    :goto_14a
    add-int/lit8 v3, v3, 0x1

    #@14c
    goto :goto_129

    #@14d
    .line 607
    :cond_14d
    const-string v11, "??"

    #@14f
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    goto :goto_14a

    #@153
    .line 611
    :cond_153
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@156
    move-result-object v11

    #@157
    const-string v12, " PSK: "

    #@159
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    .line 612
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@15e
    if-eqz v11, :cond_165

    #@160
    .line 613
    const/16 v11, 0x2a

    #@162
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@165
    .line 616
    :cond_165
    iget-object v1, p0, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@167
    .local v1, arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    array-length v6, v1

    #@168
    .local v6, len$:I
    const/4 v4, 0x0

    #@169
    .local v4, i$:I
    :goto_169
    if-ge v4, v6, :cond_19d

    #@16b
    aget-object v2, v1, v4

    #@16d
    .line 617
    .local v2, field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@170
    move-result-object v11

    #@171
    new-instance v12, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v13, " "

    #@178
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v12

    #@17c
    invoke-virtual {v2}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->varName()Ljava/lang/String;

    #@17f
    move-result-object v13

    #@180
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v12

    #@184
    const-string v13, ": "

    #@186
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v12

    #@18a
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v12

    #@18e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    .line 618
    invoke-virtual {v2}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@194
    move-result-object v10

    #@195
    .line 619
    .local v10, value:Ljava/lang/String;
    if-eqz v10, :cond_19a

    #@197
    .line 620
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    .line 616
    :cond_19a
    add-int/lit8 v4, v4, 0x1

    #@19c
    goto :goto_169

    #@19d
    .line 623
    .end local v2           #field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .end local v10           #value:Ljava/lang/String;
    :cond_19d
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a0
    .line 624
    new-instance v11, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v12, "IP assignment: "

    #@1a7
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v11

    #@1ab
    iget-object v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@1ad
    invoke-virtual {v12}, Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v12

    #@1b1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v11

    #@1b5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b8
    move-result-object v11

    #@1b9
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    .line 625
    const-string v11, "\n"

    #@1be
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    .line 626
    new-instance v11, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    const-string v12, "Proxy settings: "

    #@1c8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v11

    #@1cc
    iget-object v12, p0, Landroid/net/wifi/WifiVZWConfiguration;->proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@1ce
    invoke-virtual {v12}, Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;->toString()Ljava/lang/String;

    #@1d1
    move-result-object v12

    #@1d2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v11

    #@1d6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d9
    move-result-object v11

    #@1da
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    .line 627
    const-string v11, "\n"

    #@1df
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    .line 628
    iget-object v11, p0, Landroid/net/wifi/WifiVZWConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@1e4
    invoke-virtual {v11}, Landroid/net/LinkProperties;->toString()Ljava/lang/String;

    #@1e7
    move-result-object v11

    #@1e8
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    .line 629
    const-string v11, "\n"

    #@1ed
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    .line 631
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v11

    #@1f4
    return-object v11
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 755
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->networkId:I

    #@2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 756
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->status:I

    #@7
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 757
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->disableReason:I

    #@c
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 758
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 759
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->BSSID:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 760
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@1b
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 764
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 765
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 766
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 767
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@2f
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@32
    .line 768
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@34
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 769
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->priority:I

    #@39
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 770
    iget-boolean v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@3e
    if-eqz v4, :cond_6f

    #@40
    const/4 v4, 0x1

    #@41
    :goto_41
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@44
    .line 772
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@46
    invoke-static {p1, v4}, Landroid/net/wifi/WifiVZWConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@49
    .line 773
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@4b
    invoke-static {p1, v4}, Landroid/net/wifi/WifiVZWConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@4e
    .line 774
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@50
    invoke-static {p1, v4}, Landroid/net/wifi/WifiVZWConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@53
    .line 775
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@55
    invoke-static {p1, v4}, Landroid/net/wifi/WifiVZWConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@58
    .line 776
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@5a
    invoke-static {p1, v4}, Landroid/net/wifi/WifiVZWConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@5d
    .line 778
    iget-object v0, p0, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@5f
    .local v0, arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    array-length v3, v0

    #@60
    .local v3, len$:I
    const/4 v2, 0x0

    #@61
    .local v2, i$:I
    :goto_61
    if-ge v2, v3, :cond_71

    #@63
    aget-object v1, v0, v2

    #@65
    .line 779
    .local v1, field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    invoke-virtual {v1}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6c
    .line 778
    add-int/lit8 v2, v2, 0x1

    #@6e
    goto :goto_61

    #@6f
    .line 770
    .end local v0           #arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .end local v1           #field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_6f
    const/4 v4, 0x0

    #@70
    goto :goto_41

    #@71
    .line 781
    .restart local v0       #arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_71
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@73
    invoke-virtual {v4}, Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;->name()Ljava/lang/String;

    #@76
    move-result-object v4

    #@77
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7a
    .line 782
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@7c
    invoke-virtual {v4}, Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;->name()Ljava/lang/String;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@83
    .line 783
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@85
    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@88
    .line 784
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@8a
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@8d
    .line 785
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@8f
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@92
    .line 786
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@94
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@97
    .line 787
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@99
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@9c
    .line 788
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@9e
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@a1
    .line 789
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@a3
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@a6
    .line 790
    iget v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@a8
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@ab
    .line 791
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@ad
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b0
    .line 792
    iget-object v4, p0, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@b2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b5
    .line 793
    return-void
.end method
