.class Landroid/net/wifi/WifiManager$ServiceHandler;
.super Landroid/os/Handler;
.source "WifiManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiManager;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiManager;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 1304
    iput-object p1, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@2
    .line 1305
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 1306
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "message"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1310
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@3
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@5
    invoke-static {v3, v4}, Landroid/net/wifi/WifiManager;->access$000(Landroid/net/wifi/WifiManager;I)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    .line 1311
    .local v1, listener:Ljava/lang/Object;
    iget v3, p1, Landroid/os/Message;->what:I

    #@b
    sparse-switch v3, :sswitch_data_bc

    #@e
    .line 1391
    .end local v1           #listener:Ljava/lang/Object;
    :cond_e
    :goto_e
    :sswitch_e
    return-void

    #@f
    .line 1313
    .restart local v1       #listener:Ljava/lang/Object;
    :sswitch_f
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@11
    if-nez v3, :cond_29

    #@13
    .line 1314
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@15
    invoke-static {v3}, Landroid/net/wifi/WifiManager;->access$100(Landroid/net/wifi/WifiManager;)Lcom/android/internal/util/AsyncChannel;

    #@18
    move-result-object v3

    #@19
    const v4, 0x11001

    #@1c
    invoke-virtual {v3, v4}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@1f
    .line 1321
    :goto_1f
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@21
    invoke-static {v3}, Landroid/net/wifi/WifiManager;->access$200(Landroid/net/wifi/WifiManager;)Ljava/util/concurrent/CountDownLatch;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@28
    goto :goto_e

    #@29
    .line 1316
    :cond_29
    const-string v3, "WifiManager"

    #@2b
    const-string v4, "Failed to set up channel connection"

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1319
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@32
    invoke-static {v3, v5}, Landroid/net/wifi/WifiManager;->access$102(Landroid/net/wifi/WifiManager;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@35
    goto :goto_1f

    #@36
    .line 1327
    :sswitch_36
    const-string v3, "WifiManager"

    #@38
    const-string v4, "Channel connection lost"

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 1330
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@3f
    invoke-static {v3, v5}, Landroid/net/wifi/WifiManager;->access$102(Landroid/net/wifi/WifiManager;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;

    #@42
    .line 1331
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager$ServiceHandler;->getLooper()Landroid/os/Looper;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Landroid/os/Looper;->quit()V

    #@49
    goto :goto_e

    #@4a
    .line 1339
    :sswitch_4a
    if-eqz v1, :cond_e

    #@4c
    .line 1340
    check-cast v1, Landroid/net/wifi/WifiManager$ActionListener;

    #@4e
    .end local v1           #listener:Ljava/lang/Object;
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@50
    invoke-interface {v1, v3}, Landroid/net/wifi/WifiManager$ActionListener;->onFailure(I)V

    #@53
    goto :goto_e

    #@54
    .line 1349
    .restart local v1       #listener:Ljava/lang/Object;
    :sswitch_54
    if-eqz v1, :cond_e

    #@56
    .line 1350
    check-cast v1, Landroid/net/wifi/WifiManager$ActionListener;

    #@58
    .end local v1           #listener:Ljava/lang/Object;
    invoke-interface {v1}, Landroid/net/wifi/WifiManager$ActionListener;->onSuccess()V

    #@5b
    goto :goto_e

    #@5c
    .line 1354
    .restart local v1       #listener:Ljava/lang/Object;
    :sswitch_5c
    if-eqz v1, :cond_e

    #@5e
    .line 1355
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@60
    check-cast v2, Landroid/net/wifi/WpsResult;

    #@62
    .local v2, result:Landroid/net/wifi/WpsResult;
    move-object v3, v1

    #@63
    .line 1356
    check-cast v3, Landroid/net/wifi/WifiManager$WpsListener;

    #@65
    iget-object v4, v2, Landroid/net/wifi/WpsResult;->pin:Ljava/lang/String;

    #@67
    invoke-interface {v3, v4}, Landroid/net/wifi/WifiManager$WpsListener;->onStartSuccess(Ljava/lang/String;)V

    #@6a
    .line 1358
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@6c
    invoke-static {v3}, Landroid/net/wifi/WifiManager;->access$300(Landroid/net/wifi/WifiManager;)Ljava/lang/Object;

    #@6f
    move-result-object v4

    #@70
    monitor-enter v4

    #@71
    .line 1359
    :try_start_71
    iget-object v3, p0, Landroid/net/wifi/WifiManager$ServiceHandler;->this$0:Landroid/net/wifi/WifiManager;

    #@73
    invoke-static {v3}, Landroid/net/wifi/WifiManager;->access$400(Landroid/net/wifi/WifiManager;)Landroid/util/SparseArray;

    #@76
    move-result-object v3

    #@77
    iget v5, p1, Landroid/os/Message;->arg2:I

    #@79
    invoke-virtual {v3, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@7c
    .line 1360
    monitor-exit v4

    #@7d
    goto :goto_e

    #@7e
    :catchall_7e
    move-exception v3

    #@7f
    monitor-exit v4
    :try_end_80
    .catchall {:try_start_71 .. :try_end_80} :catchall_7e

    #@80
    throw v3

    #@81
    .line 1364
    .end local v2           #result:Landroid/net/wifi/WpsResult;
    :sswitch_81
    if-eqz v1, :cond_e

    #@83
    .line 1365
    check-cast v1, Landroid/net/wifi/WifiManager$WpsListener;

    #@85
    .end local v1           #listener:Ljava/lang/Object;
    invoke-interface {v1}, Landroid/net/wifi/WifiManager$WpsListener;->onCompletion()V

    #@88
    goto :goto_e

    #@89
    .line 1369
    .restart local v1       #listener:Ljava/lang/Object;
    :sswitch_89
    if-eqz v1, :cond_e

    #@8b
    .line 1370
    check-cast v1, Landroid/net/wifi/WifiManager$WpsListener;

    #@8d
    .end local v1           #listener:Ljava/lang/Object;
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@8f
    invoke-interface {v1, v3}, Landroid/net/wifi/WifiManager$WpsListener;->onFailure(I)V

    #@92
    goto/16 :goto_e

    #@94
    .line 1374
    .restart local v1       #listener:Ljava/lang/Object;
    :sswitch_94
    if-eqz v1, :cond_e

    #@96
    .line 1375
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@98
    check-cast v0, Landroid/net/wifi/RssiPacketCountInfo;

    #@9a
    .line 1376
    .local v0, info:Landroid/net/wifi/RssiPacketCountInfo;
    if-eqz v0, :cond_a8

    #@9c
    .line 1377
    check-cast v1, Landroid/net/wifi/WifiManager$TxPacketCountListener;

    #@9e
    .end local v1           #listener:Ljava/lang/Object;
    iget v3, v0, Landroid/net/wifi/RssiPacketCountInfo;->txgood:I

    #@a0
    iget v4, v0, Landroid/net/wifi/RssiPacketCountInfo;->txbad:I

    #@a2
    add-int/2addr v3, v4

    #@a3
    invoke-interface {v1, v3}, Landroid/net/wifi/WifiManager$TxPacketCountListener;->onSuccess(I)V

    #@a6
    goto/16 :goto_e

    #@a8
    .line 1379
    .restart local v1       #listener:Ljava/lang/Object;
    :cond_a8
    check-cast v1, Landroid/net/wifi/WifiManager$TxPacketCountListener;

    #@aa
    .end local v1           #listener:Ljava/lang/Object;
    const/4 v3, 0x0

    #@ab
    invoke-interface {v1, v3}, Landroid/net/wifi/WifiManager$TxPacketCountListener;->onFailure(I)V

    #@ae
    goto/16 :goto_e

    #@b0
    .line 1383
    .end local v0           #info:Landroid/net/wifi/RssiPacketCountInfo;
    .restart local v1       #listener:Ljava/lang/Object;
    :sswitch_b0
    if-eqz v1, :cond_e

    #@b2
    .line 1384
    check-cast v1, Landroid/net/wifi/WifiManager$TxPacketCountListener;

    #@b4
    .end local v1           #listener:Ljava/lang/Object;
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@b6
    invoke-interface {v1, v3}, Landroid/net/wifi/WifiManager$TxPacketCountListener;->onFailure(I)V

    #@b9
    goto/16 :goto_e

    #@bb
    .line 1311
    nop

    #@bc
    :sswitch_data_bc
    .sparse-switch
        0x11000 -> :sswitch_f
        0x11002 -> :sswitch_e
        0x11004 -> :sswitch_36
        0x25002 -> :sswitch_4a
        0x25003 -> :sswitch_54
        0x25005 -> :sswitch_4a
        0x25006 -> :sswitch_54
        0x25008 -> :sswitch_4a
        0x25009 -> :sswitch_54
        0x2500b -> :sswitch_5c
        0x2500c -> :sswitch_89
        0x2500d -> :sswitch_81
        0x2500f -> :sswitch_4a
        0x25010 -> :sswitch_54
        0x25012 -> :sswitch_4a
        0x25013 -> :sswitch_54
        0x25015 -> :sswitch_94
        0x25016 -> :sswitch_b0
    .end sparse-switch
.end method
