.class public Landroid/net/wifi/WifiSsid;
.super Ljava/lang/Object;
.source "WifiSsid.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/WifiSsid;",
            ">;"
        }
    .end annotation
.end field

.field private static final HEX_RADIX:I = 0x10

.field public static final NONE:Ljava/lang/String; = "<unknown ssid>"

.field private static final TAG:Ljava/lang/String; = "WifiSsid"


# instance fields
.field public octets:Ljava/io/ByteArrayOutputStream;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 352
    new-instance v0, Landroid/net/wifi/WifiSsid$1;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WifiSsid$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/wifi/WifiSsid;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@5
    const/16 v1, 0x20

    #@7
    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@c
    .line 56
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/wifi/WifiSsid$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Landroid/net/wifi/WifiSsid;-><init>()V

    #@3
    return-void
.end method

.method private static checkUniCode([BII)Z
    .registers 9
    .parameter "byte_array"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/16 v5, 0x80

    #@2
    .line 195
    const/4 v1, 0x0

    #@3
    .line 196
    .local v1, unicodeSSID:Z
    const/4 v2, 0x0

    #@4
    .line 198
    .local v2, unicode_step:I
    move v0, p1

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, p2, :cond_1e

    #@7
    aget-byte v3, p0, v0

    #@9
    const/16 v4, 0xa

    #@b
    if-eq v3, v4, :cond_1e

    #@d
    .line 199
    if-lez v2, :cond_22

    #@f
    .line 200
    aget-byte v3, p0, v0

    #@11
    and-int/lit16 v3, v3, 0xc0

    #@13
    if-ne v3, v5, :cond_1d

    #@15
    .line 201
    add-int/lit8 v2, v2, -0x1

    #@17
    .line 202
    if-nez v2, :cond_1a

    #@19
    .line 203
    const/4 v1, 0x1

    #@1a
    .line 198
    :cond_1a
    :goto_1a
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 207
    :cond_1d
    const/4 v1, 0x0

    #@1e
    .line 249
    :cond_1e
    :goto_1e
    if-eqz v2, :cond_21

    #@20
    .line 250
    const/4 v1, 0x0

    #@21
    .line 254
    :cond_21
    return v1

    #@22
    .line 211
    :cond_22
    if-gez v2, :cond_2e

    #@24
    .line 212
    aget-byte v3, p0, v0

    #@26
    and-int/lit16 v3, v3, 0xc0

    #@28
    if-ne v3, v5, :cond_2c

    #@2a
    .line 213
    const/4 v2, 0x0

    #@2b
    .line 214
    goto :goto_1a

    #@2c
    .line 216
    :cond_2c
    const/4 v1, 0x0

    #@2d
    .line 217
    goto :goto_1e

    #@2e
    .line 220
    :cond_2e
    aget-byte v3, p0, v0

    #@30
    and-int/lit16 v3, v3, 0x80

    #@32
    if-eqz v3, :cond_1a

    #@34
    .line 222
    aget-byte v3, p0, v0

    #@36
    and-int/lit16 v3, v3, 0xf0

    #@38
    const/16 v4, 0xe0

    #@3a
    if-ne v3, v4, :cond_3e

    #@3c
    .line 224
    const/4 v2, 0x2

    #@3d
    .line 225
    goto :goto_1a

    #@3e
    .line 226
    :cond_3e
    aget-byte v3, p0, v0

    #@40
    and-int/lit16 v3, v3, 0xe0

    #@42
    const/16 v4, 0xc0

    #@44
    if-ne v3, v4, :cond_48

    #@46
    .line 228
    const/4 v2, 0x1

    #@47
    .line 229
    goto :goto_1a

    #@48
    .line 230
    :cond_48
    aget-byte v3, p0, v0

    #@4a
    and-int/lit16 v3, v3, 0xf8

    #@4c
    const/16 v4, 0xf0

    #@4e
    if-ne v3, v4, :cond_52

    #@50
    .line 232
    const/4 v2, 0x3

    #@51
    .line 233
    goto :goto_1a

    #@52
    .line 234
    :cond_52
    aget-byte v3, p0, v0

    #@54
    and-int/lit16 v3, v3, 0xfc

    #@56
    const/16 v4, 0xf8

    #@58
    if-ne v3, v4, :cond_5c

    #@5a
    .line 236
    const/4 v2, 0x4

    #@5b
    .line 237
    goto :goto_1a

    #@5c
    .line 238
    :cond_5c
    aget-byte v3, p0, v0

    #@5e
    and-int/lit16 v3, v3, 0xfe

    #@60
    const/16 v4, 0xfc

    #@62
    if-ne v3, v4, :cond_66

    #@64
    .line 240
    const/4 v2, 0x5

    #@65
    .line 241
    goto :goto_1a

    #@66
    .line 243
    :cond_66
    const/4 v1, 0x0

    #@67
    .line 244
    goto :goto_1e
.end method

.method private convertToBytes(Ljava/lang/String;)V
    .registers 12
    .parameter "asciiEncoded"

    #@0
    .prologue
    const/16 v9, 0x37

    #@2
    const/16 v8, 0x30

    #@4
    const/16 v7, 0x10

    #@6
    .line 91
    const/4 v2, 0x0

    #@7
    .line 92
    .local v2, i:I
    const/4 v3, 0x0

    #@8
    .line 93
    .local v3, val:I
    :cond_8
    :goto_8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@b
    move-result v4

    #@c
    if-ge v2, v4, :cond_14a

    #@e
    .line 94
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v0

    #@12
    .line 95
    .local v0, c:C
    packed-switch v0, :pswitch_data_14c

    #@15
    .line 182
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@17
    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@1a
    .line 183
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_8

    #@1d
    .line 97
    :pswitch_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    .line 100
    :try_start_1f
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C
    :try_end_22
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1f .. :try_end_22} :catch_31

    #@22
    move-result v0

    #@23
    .line 105
    sparse-switch v0, :sswitch_data_152

    #@26
    goto :goto_8

    #@27
    .line 111
    :sswitch_27
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@29
    const/16 v5, 0x22

    #@2b
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2e
    .line 112
    add-int/lit8 v2, v2, 0x1

    #@30
    .line 113
    goto :goto_8

    #@31
    .line 101
    :catch_31
    move-exception v1

    #@32
    .line 102
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "WifiSsid"

    #@34
    new-instance v5, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v6, "convertToBytes error = "

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    const-string v6, ", length = "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4c
    move-result v6

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_8

    #@59
    .line 107
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :sswitch_59
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@5b
    const/16 v5, 0x5c

    #@5d
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@60
    .line 108
    add-int/lit8 v2, v2, 0x1

    #@62
    .line 109
    goto :goto_8

    #@63
    .line 115
    :sswitch_63
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@65
    const/16 v5, 0xa

    #@67
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@6a
    .line 116
    add-int/lit8 v2, v2, 0x1

    #@6c
    .line 117
    goto :goto_8

    #@6d
    .line 119
    :sswitch_6d
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@6f
    const/16 v5, 0xd

    #@71
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@74
    .line 120
    add-int/lit8 v2, v2, 0x1

    #@76
    .line 121
    goto :goto_8

    #@77
    .line 123
    :sswitch_77
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@79
    const/16 v5, 0x9

    #@7b
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@7e
    .line 124
    add-int/lit8 v2, v2, 0x1

    #@80
    .line 125
    goto :goto_8

    #@81
    .line 127
    :sswitch_81
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@83
    const/16 v5, 0x1b

    #@85
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@88
    .line 128
    add-int/lit8 v2, v2, 0x1

    #@8a
    .line 129
    goto/16 :goto_8

    #@8c
    .line 131
    :sswitch_8c
    add-int/lit8 v2, v2, 0x1

    #@8e
    .line 133
    add-int/lit8 v4, v2, 0x2

    #@90
    :try_start_90
    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@93
    move-result-object v4

    #@94
    const/16 v5, 0x10

    #@96
    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_99
    .catch Ljava/lang/NumberFormatException; {:try_start_90 .. :try_end_99} :catch_af
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_90 .. :try_end_99} :catch_b2

    #@99
    move-result v3

    #@9a
    .line 141
    :goto_9a
    if-gez v3, :cond_db

    #@9c
    .line 142
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@9f
    move-result v4

    #@a0
    invoke-static {v4, v7}, Ljava/lang/Character;->digit(CI)I

    #@a3
    move-result v3

    #@a4
    .line 143
    if-ltz v3, :cond_8

    #@a6
    .line 144
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@a8
    invoke-virtual {v4, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@ab
    .line 145
    add-int/lit8 v2, v2, 0x1

    #@ad
    goto/16 :goto_8

    #@af
    .line 134
    :catch_af
    move-exception v1

    #@b0
    .line 135
    .local v1, e:Ljava/lang/NumberFormatException;
    const/4 v3, -0x1

    #@b1
    .line 140
    goto :goto_9a

    #@b2
    .line 137
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :catch_b2
    move-exception v1

    #@b3
    .line 138
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "WifiSsid"

    #@b5
    new-instance v5, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v6, "convertToBytes error = "

    #@bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v5

    #@c4
    const-string v6, ", length = "

    #@c6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v5

    #@ca
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@cd
    move-result v6

    #@ce
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v5

    #@d2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v5

    #@d6
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    goto/16 :goto_8

    #@db
    .line 147
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_db
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@dd
    invoke-virtual {v4, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@e0
    .line 148
    add-int/lit8 v2, v2, 0x2

    #@e2
    .line 150
    goto/16 :goto_8

    #@e4
    .line 161
    :sswitch_e4
    :try_start_e4
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@e7
    move-result v4

    #@e8
    add-int/lit8 v3, v4, -0x30

    #@ea
    .line 162
    add-int/lit8 v2, v2, 0x1

    #@ec
    .line 163
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@ef
    move-result v4

    #@f0
    if-lt v4, v8, :cond_103

    #@f2
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@f5
    move-result v4

    #@f6
    if-gt v4, v9, :cond_103

    #@f8
    .line 164
    mul-int/lit8 v4, v3, 0x8

    #@fa
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@fd
    move-result v5

    #@fe
    add-int/2addr v4, v5

    #@ff
    add-int/lit8 v3, v4, -0x30

    #@101
    .line 165
    add-int/lit8 v2, v2, 0x1

    #@103
    .line 167
    :cond_103
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@106
    move-result v4

    #@107
    if-lt v4, v8, :cond_11a

    #@109
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@10c
    move-result v4

    #@10d
    if-gt v4, v9, :cond_11a

    #@10f
    .line 168
    mul-int/lit8 v4, v3, 0x8

    #@111
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@114
    move-result v5

    #@115
    add-int/2addr v4, v5

    #@116
    add-int/lit8 v3, v4, -0x30

    #@118
    .line 169
    add-int/lit8 v2, v2, 0x1

    #@11a
    .line 171
    :cond_11a
    iget-object v4, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@11c
    invoke-virtual {v4, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_11f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_e4 .. :try_end_11f} :catch_121

    #@11f
    goto/16 :goto_8

    #@121
    .line 172
    :catch_121
    move-exception v1

    #@122
    .line 173
    .restart local v1       #e:Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "WifiSsid"

    #@124
    new-instance v5, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v6, "convertToBytes error = "

    #@12b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v5

    #@12f
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v5

    #@133
    const-string v6, ", length = "

    #@135
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v5

    #@139
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@13c
    move-result v6

    #@13d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@140
    move-result-object v5

    #@141
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@144
    move-result-object v5

    #@145
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@148
    goto/16 :goto_8

    #@14a
    .line 187
    .end local v0           #c:C
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_14a
    return-void

    #@14b
    .line 95
    nop

    #@14c
    :pswitch_data_14c
    .packed-switch 0x5c
        :pswitch_1d
    .end packed-switch

    #@152
    .line 105
    :sswitch_data_152
    .sparse-switch
        0x22 -> :sswitch_27
        0x30 -> :sswitch_e4
        0x31 -> :sswitch_e4
        0x32 -> :sswitch_e4
        0x33 -> :sswitch_e4
        0x34 -> :sswitch_e4
        0x35 -> :sswitch_e4
        0x36 -> :sswitch_e4
        0x37 -> :sswitch_e4
        0x5c -> :sswitch_59
        0x65 -> :sswitch_81
        0x6e -> :sswitch_63
        0x72 -> :sswitch_6d
        0x74 -> :sswitch_77
        0x78 -> :sswitch_8c
    .end sparse-switch
.end method

.method public static createFromAsciiEncoded(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;
    .registers 2
    .parameter "asciiEncoded"

    #@0
    .prologue
    .line 59
    new-instance v0, Landroid/net/wifi/WifiSsid;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WifiSsid;-><init>()V

    #@5
    .line 61
    .local v0, a:Landroid/net/wifi/WifiSsid;
    if-nez p0, :cond_8

    #@7
    .line 64
    :goto_7
    return-object v0

    #@8
    .line 63
    :cond_8
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiSsid;->convertToBytes(Ljava/lang/String;)V

    #@b
    goto :goto_7
.end method

.method public static createFromHex(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;
    .registers 8
    .parameter "hexStr"

    #@0
    .prologue
    .line 68
    new-instance v0, Landroid/net/wifi/WifiSsid;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WifiSsid;-><init>()V

    #@5
    .line 69
    .local v0, a:Landroid/net/wifi/WifiSsid;
    const/4 v3, 0x0

    #@6
    .line 70
    .local v3, length:I
    if-nez p0, :cond_9

    #@8
    .line 85
    :cond_8
    return-object v0

    #@9
    .line 72
    :cond_9
    const-string v5, "0x"

    #@b
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e
    move-result v5

    #@f
    if-nez v5, :cond_19

    #@11
    const-string v5, "0X"

    #@13
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_1e

    #@19
    .line 73
    :cond_19
    const/4 v5, 0x2

    #@1a
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1d
    move-result-object p0

    #@1e
    .line 76
    :cond_1e
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@22
    move-result v5

    #@23
    add-int/lit8 v5, v5, -0x1

    #@25
    if-ge v2, v5, :cond_8

    #@27
    .line 79
    add-int/lit8 v5, v2, 0x2

    #@29
    :try_start_29
    invoke-virtual {p0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    const/16 v6, 0x10

    #@2f
    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_32
    .catch Ljava/lang/NumberFormatException; {:try_start_29 .. :try_end_32} :catch_3b

    #@32
    move-result v4

    #@33
    .line 83
    .local v4, val:I
    :goto_33
    iget-object v5, v0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@35
    invoke-virtual {v5, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@38
    .line 76
    add-int/lit8 v2, v2, 0x2

    #@3a
    goto :goto_1f

    #@3b
    .line 80
    .end local v4           #val:I
    :catch_3b
    move-exception v1

    #@3c
    .line 81
    .local v1, e:Ljava/lang/NumberFormatException;
    const/4 v4, 0x0

    #@3d
    .restart local v4       #val:I
    goto :goto_33
.end method

.method private isArrayAllZeroes([B)Z
    .registers 4
    .parameter "ssidBytes"

    #@0
    .prologue
    .line 319
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p1

    #@2
    if-ge v0, v1, :cond_d

    #@4
    .line 320
    aget-byte v1, p1, v0

    #@6
    if-eqz v1, :cond_a

    #@8
    const/4 v1, 0x0

    #@9
    .line 322
    :goto_9
    return v1

    #@a
    .line 319
    :cond_a
    add-int/lit8 v0, v0, 0x1

    #@c
    goto :goto_1

    #@d
    .line 322
    :cond_d
    const/4 v1, 0x1

    #@e
    goto :goto_9
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 342
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getHexString()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 332
    const-string v1, "0x"

    #@2
    .line 333
    .local v1, out:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/net/wifi/WifiSsid;->getOctets()[B

    #@5
    move-result-object v2

    #@6
    .line 334
    .local v2, ssidbytes:[B
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    iget-object v3, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@9
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    #@c
    move-result v3

    #@d
    if-ge v0, v3, :cond_35

    #@f
    .line 335
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, "%02x"

    #@1a
    const/4 v5, 0x1

    #@1b
    new-array v5, v5, [Ljava/lang/Object;

    #@1d
    const/4 v6, 0x0

    #@1e
    aget-byte v7, v2, v0

    #@20
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@23
    move-result-object v7

    #@24
    aput-object v7, v5, v6

    #@26
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    .line 334
    add-int/lit8 v0, v0, 0x1

    #@34
    goto :goto_7

    #@35
    .line 337
    :cond_35
    return-object v1
.end method

.method public getOctets()[B
    .registers 2

    #@0
    .prologue
    .line 327
    iget-object v0, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 259
    iget-object v7, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@3
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@6
    move-result-object v6

    #@7
    .line 263
    .local v6, ssidBytes:[B
    iget-object v7, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@9
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->size()I

    #@c
    move-result v7

    #@d
    if-lez v7, :cond_15

    #@f
    invoke-direct {p0, v6}, Landroid/net/wifi/WifiSsid;->isArrayAllZeroes([B)Z

    #@12
    move-result v7

    #@13
    if-eqz v7, :cond_18

    #@15
    :cond_15
    const-string v3, ""

    #@17
    .line 314
    :goto_17
    return-object v3

    #@18
    .line 270
    :cond_18
    const/4 v7, 0x0

    #@19
    array-length v8, v6

    #@1a
    invoke-static {v6, v7, v8}, Landroid/net/wifi/WifiSsid;->checkUniCode([BII)Z

    #@1d
    move-result v2

    #@1e
    .line 272
    .local v2, isUnicode:Z
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useKoreanSsid()Z

    #@21
    move-result v7

    #@22
    if-ne v7, v9, :cond_5d

    #@24
    .line 273
    if-eqz v2, :cond_56

    #@26
    .line 274
    const-string v7, "UTF-8"

    #@28
    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@2b
    move-result-object v0

    #@2c
    .line 286
    .local v0, charset:Ljava/nio/charset/Charset;
    :goto_2c
    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    #@2f
    move-result-object v7

    #@30
    sget-object v8, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    #@32
    invoke-virtual {v7, v8}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    #@35
    move-result-object v7

    #@36
    sget-object v8, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    #@38
    invoke-virtual {v7, v8}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    #@3b
    move-result-object v1

    #@3c
    .line 289
    .local v1, decoder:Ljava/nio/charset/CharsetDecoder;
    const/16 v7, 0x20

    #@3e
    invoke-static {v7}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    #@41
    move-result-object v4

    #@42
    .line 291
    .local v4, out:Ljava/nio/CharBuffer;
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v1, v7, v4, v9}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    #@49
    move-result-object v5

    #@4a
    .line 292
    .local v5, result:Ljava/nio/charset/CoderResult;
    invoke-virtual {v4}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    #@4d
    .line 293
    invoke-virtual {v5}, Ljava/nio/charset/CoderResult;->isError()Z

    #@50
    move-result v7

    #@51
    if-eqz v7, :cond_64

    #@53
    .line 294
    const-string v3, "<unknown ssid>"

    #@55
    goto :goto_17

    #@56
    .line 277
    .end local v0           #charset:Ljava/nio/charset/Charset;
    .end local v1           #decoder:Ljava/nio/charset/CharsetDecoder;
    .end local v4           #out:Ljava/nio/CharBuffer;
    .end local v5           #result:Ljava/nio/charset/CoderResult;
    :cond_56
    const-string v7, "KSC5601"

    #@58
    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@5b
    move-result-object v0

    #@5c
    .restart local v0       #charset:Ljava/nio/charset/Charset;
    goto :goto_2c

    #@5d
    .line 281
    .end local v0           #charset:Ljava/nio/charset/Charset;
    :cond_5d
    const-string v7, "UTF-8"

    #@5f
    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@62
    move-result-object v0

    #@63
    .restart local v0       #charset:Ljava/nio/charset/Charset;
    goto :goto_2c

    #@64
    .line 302
    .restart local v1       #decoder:Ljava/nio/charset/CharsetDecoder;
    .restart local v4       #out:Ljava/nio/CharBuffer;
    .restart local v5       #result:Ljava/nio/charset/CoderResult;
    :cond_64
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useKoreanSsid()Z

    #@67
    move-result v7

    #@68
    if-ne v7, v9, :cond_8a

    #@6a
    .line 303
    if-eqz v2, :cond_85

    #@6c
    .line 304
    new-instance v7, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    invoke-virtual {v4}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    #@74
    move-result-object v8

    #@75
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    const-string/jumbo v8, "\u200b"

    #@7c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v3

    #@84
    .local v3, newOut:Ljava/lang/String;
    goto :goto_17

    #@85
    .line 307
    .end local v3           #newOut:Ljava/lang/String;
    :cond_85
    invoke-virtual {v4}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    #@88
    move-result-object v3

    #@89
    .restart local v3       #newOut:Ljava/lang/String;
    goto :goto_17

    #@8a
    .line 311
    .end local v3           #newOut:Ljava/lang/String;
    :cond_8a
    invoke-virtual {v4}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    .restart local v3       #newOut:Ljava/lang/String;
    goto :goto_17
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    #@5
    move-result v0

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 348
    iget-object v0, p0, Landroid/net/wifi/WifiSsid;->octets:Ljava/io/ByteArrayOutputStream;

    #@b
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@e
    move-result-object v0

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@12
    .line 349
    return-void
.end method
