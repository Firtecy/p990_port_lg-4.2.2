.class Landroid/net/wifi/WifiStateMachine$DisconnectedState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisconnectedState"
.end annotation


# instance fields
.field private mAlarmEnabled:Z

.field private mFrameworkScanIntervalMs:J

.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 4147
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    .line 4148
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mAlarmEnabled:Z

    #@8
    return-void
.end method

.method private setScanAlarm(Z)V
    .registers 10
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4156
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    const-string v2, "ATT"

    #@7
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_15

    #@d
    .line 4157
    const-string v0, "WifiStateMachine"

    #@f
    const-string v1, "Disable background Scan for ATT"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 4176
    :cond_14
    :goto_14
    return-void

    #@15
    .line 4162
    :cond_15
    iget-boolean v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mAlarmEnabled:Z

    #@17
    if-eq p1, v0, :cond_14

    #@19
    .line 4163
    if-eqz p1, :cond_43

    #@1b
    .line 4164
    iget-wide v2, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mFrameworkScanIntervalMs:J

    #@1d
    const-wide/16 v6, 0x0

    #@1f
    cmp-long v0, v2, v6

    #@21
    if-lez v0, :cond_14

    #@23
    .line 4165
    iget-wide v2, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mFrameworkScanIntervalMs:J

    #@25
    const-wide/16 v6, 0x6

    #@27
    mul-long v4, v2, v6

    #@29
    .line 4166
    .local v4, mRepeatFrameworkScanIntervalMs:J
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2b
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$10600(Landroid/net/wifi/WifiStateMachine;)Landroid/app/AlarmManager;

    #@2e
    move-result-object v0

    #@2f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@32
    move-result-wide v2

    #@33
    iget-wide v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mFrameworkScanIntervalMs:J

    #@35
    add-long/2addr v2, v6

    #@36
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@38
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$16900(Landroid/net/wifi/WifiStateMachine;)Landroid/app/PendingIntent;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@3f
    .line 4170
    const/4 v0, 0x1

    #@40
    iput-boolean v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mAlarmEnabled:Z

    #@42
    goto :goto_14

    #@43
    .line 4173
    .end local v4           #mRepeatFrameworkScanIntervalMs:J
    :cond_43
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@45
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$10600(Landroid/net/wifi/WifiStateMachine;)Landroid/app/AlarmManager;

    #@48
    move-result-object v0

    #@49
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4b
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$16900(Landroid/net/wifi/WifiStateMachine;)Landroid/app/PendingIntent;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@52
    .line 4174
    iput-boolean v1, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mAlarmEnabled:Z

    #@54
    goto :goto_14
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 4181
    const v0, 0xc365

    #@4
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->getName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@b
    .line 4185
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1700(Landroid/net/wifi/WifiStateMachine;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_20

    #@13
    .line 4186
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@15
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@18
    move-result-object v0

    #@19
    const v1, 0x2300d

    #@1c
    invoke-virtual {v0, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@1f
    .line 4221
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 4190
    :cond_20
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@22
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@29
    move-result-object v0

    #@2a
    const-string/jumbo v1, "wifi_framework_scan_interval_ms"

    #@2d
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2f
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$17000(Landroid/net/wifi/WifiStateMachine;)I

    #@32
    move-result v2

    #@33
    int-to-long v2, v2

    #@34
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@37
    move-result-wide v0

    #@38
    iput-wide v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->mFrameworkScanIntervalMs:J

    #@3a
    .line 4198
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3c
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1200(Landroid/net/wifi/WifiStateMachine;)Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_8b

    #@42
    .line 4205
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@44
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$7600(Landroid/net/wifi/WifiStateMachine;)Z

    #@47
    move-result v0

    #@48
    if-nez v0, :cond_53

    #@4a
    .line 4206
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4c
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiNative;->enableBackgroundScan(Z)V

    #@53
    .line 4217
    :cond_53
    :goto_53
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@55
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1600(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5c
    move-result v0

    #@5d
    if-nez v0, :cond_1f

    #@5f
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@61
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@68
    move-result-object v0

    #@69
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@6c
    move-result v0

    #@6d
    if-nez v0, :cond_1f

    #@6f
    .line 4218
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@71
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@73
    const v2, 0x20058

    #@76
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@78
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$17104(Landroid/net/wifi/WifiStateMachine;)I

    #@7b
    move-result v3

    #@7c
    const/4 v4, 0x0

    #@7d
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@80
    move-result-object v1

    #@81
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@83
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$6600(Landroid/net/wifi/WifiStateMachine;)J

    #@86
    move-result-wide v2

    #@87
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@8a
    goto :goto_1f

    #@8b
    .line 4209
    :cond_8b
    invoke-direct {p0, v4}, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->setScanAlarm(Z)V

    #@8e
    goto :goto_53
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4320
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1200(Landroid/net/wifi/WifiStateMachine;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 4321
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->enableBackgroundScan(Z)V

    #@12
    .line 4323
    :cond_12
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->setScanAlarm(Z)V

    #@15
    .line 4324
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const v10, 0x20058

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    .line 4225
    const/4 v2, 0x1

    #@6
    .line 4226
    .local v2, ret:Z
    iget v6, p1, Landroid/os/Message;->what:I

    #@8
    sparse-switch v6, :sswitch_data_18c

    #@b
    .line 4312
    const/4 v2, 0x0

    #@c
    .line 4314
    :cond_c
    :goto_c
    :sswitch_c
    return v2

    #@d
    .line 4228
    :sswitch_d
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1600(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@12
    move-result-object v6

    #@13
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@16
    move-result v6

    #@17
    if-nez v6, :cond_c

    #@19
    .line 4229
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@1b
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1d
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$17100(Landroid/net/wifi/WifiStateMachine;)I

    #@20
    move-result v7

    #@21
    if-ne v6, v7, :cond_c

    #@23
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@25
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@2c
    move-result-object v6

    #@2d
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@30
    move-result v6

    #@31
    if-nez v6, :cond_c

    #@33
    .line 4231
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@35
    const v7, 0x20047

    #@38
    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@3b
    .line 4233
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3d
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3f
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@41
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$17104(Landroid/net/wifi/WifiStateMachine;)I

    #@44
    move-result v9

    #@45
    invoke-virtual {v7, v10, v9, v8}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@48
    move-result-object v7

    #@49
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4b
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$6600(Landroid/net/wifi/WifiStateMachine;)J

    #@4e
    move-result-wide v8

    #@4f
    const-wide/16 v10, 0xa

    #@51
    mul-long/2addr v8, v10

    #@52
    invoke-virtual {v6, v7, v8, v9}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@55
    goto :goto_c

    #@56
    .line 4242
    :sswitch_56
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@58
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5a
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5c
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$17104(Landroid/net/wifi/WifiStateMachine;)I

    #@5f
    move-result v9

    #@60
    invoke-virtual {v7, v10, v9, v8}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@63
    move-result-object v7

    #@64
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@66
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$6600(Landroid/net/wifi/WifiStateMachine;)J

    #@69
    move-result-wide v8

    #@6a
    invoke-virtual {v6, v7, v8, v9}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@6d
    .line 4244
    const/4 v2, 0x0

    #@6e
    .line 4245
    goto :goto_c

    #@6f
    .line 4247
    :sswitch_6f
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@71
    const/4 v8, 0x2

    #@72
    if-ne v6, v8, :cond_c

    #@74
    .line 4248
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@76
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@79
    move-result-object v6

    #@7a
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@7c
    invoke-virtual {v6, v8}, Landroid/net/wifi/WifiNative;->setScanResultHandling(I)Z

    #@7f
    .line 4250
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@81
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@88
    .line 4251
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@8a
    invoke-static {v6, v7}, Landroid/net/wifi/WifiStateMachine;->access$6402(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@8d
    .line 4252
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@8f
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@91
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$9500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@94
    move-result-object v7

    #@95
    invoke-static {v6, v7}, Landroid/net/wifi/WifiStateMachine;->access$17200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@98
    goto/16 :goto_c

    #@9a
    .line 4256
    :sswitch_9a
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9c
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@9e
    if-ne v6, v7, :cond_ba

    #@a0
    move v6, v7

    #@a1
    :goto_a1
    invoke-static {v9, v6}, Landroid/net/wifi/WifiStateMachine;->access$1202(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@a4
    .line 4257
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a6
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1200(Landroid/net/wifi/WifiStateMachine;)Z

    #@a9
    move-result v6

    #@aa
    if-eqz v6, :cond_bc

    #@ac
    .line 4258
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ae
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@b1
    move-result-object v6

    #@b2
    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiNative;->enableBackgroundScan(Z)V

    #@b5
    .line 4259
    invoke-direct {p0, v8}, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->setScanAlarm(Z)V

    #@b8
    goto/16 :goto_c

    #@ba
    :cond_ba
    move v6, v8

    #@bb
    .line 4256
    goto :goto_a1

    #@bc
    .line 4261
    :cond_bc
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@be
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@c1
    move-result-object v6

    #@c2
    invoke-virtual {v6, v8}, Landroid/net/wifi/WifiNative;->enableBackgroundScan(Z)V

    #@c5
    .line 4262
    invoke-direct {p0, v7}, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->setScanAlarm(Z)V

    #@c8
    goto/16 :goto_c

    #@ca
    .line 4269
    :sswitch_ca
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@cc
    check-cast v5, Landroid/net/wifi/StateChangeResult;

    #@ce
    .line 4270
    .local v5, stateChangeResult:Landroid/net/wifi/StateChangeResult;
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d0
    iget-object v7, v5, Landroid/net/wifi/StateChangeResult;->state:Landroid/net/wifi/SupplicantState;

    #@d2
    invoke-static {v7}, Landroid/net/wifi/WifiInfo;->getDetailedStateOf(Landroid/net/wifi/SupplicantState;)Landroid/net/NetworkInfo$DetailedState;

    #@d5
    move-result-object v7

    #@d6
    invoke-static {v6, v7}, Landroid/net/wifi/WifiStateMachine;->access$9300(Landroid/net/wifi/WifiStateMachine;Landroid/net/NetworkInfo$DetailedState;)V

    #@d9
    .line 4272
    const/4 v2, 0x0

    #@da
    .line 4273
    goto/16 :goto_c

    #@dc
    .line 4276
    .end local v5           #stateChangeResult:Landroid/net/wifi/StateChangeResult;
    :sswitch_dc
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@de
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1200(Landroid/net/wifi/WifiStateMachine;)Z

    #@e1
    move-result v6

    #@e2
    if-eqz v6, :cond_ed

    #@e4
    .line 4277
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e6
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@e9
    move-result-object v6

    #@ea
    invoke-virtual {v6, v8}, Landroid/net/wifi/WifiNative;->enableBackgroundScan(Z)V

    #@ed
    .line 4280
    :cond_ed
    const/4 v2, 0x0

    #@ee
    .line 4281
    goto/16 :goto_c

    #@f0
    .line 4284
    :sswitch_f0
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f2
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1200(Landroid/net/wifi/WifiStateMachine;)Z

    #@f5
    move-result v6

    #@f6
    if-eqz v6, :cond_109

    #@f8
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@fa
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$7600(Landroid/net/wifi/WifiStateMachine;)Z

    #@fd
    move-result v6

    #@fe
    if-eqz v6, :cond_109

    #@100
    .line 4285
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@102
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@105
    move-result-object v6

    #@106
    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiNative;->enableBackgroundScan(Z)V

    #@109
    .line 4288
    :cond_109
    const/4 v2, 0x0

    #@10a
    .line 4289
    goto/16 :goto_c

    #@10c
    .line 4291
    :sswitch_10c
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10e
    check-cast v1, Landroid/net/NetworkInfo;

    #@110
    .line 4292
    .local v1, info:Landroid/net/NetworkInfo;
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@112
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1600(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@115
    move-result-object v6

    #@116
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@119
    move-result v7

    #@11a
    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@11d
    .line 4293
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11f
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1600(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@122
    move-result-object v6

    #@123
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@126
    move-result v6

    #@127
    if-eqz v6, :cond_163

    #@129
    .line 4294
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@12b
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@12e
    move-result-object v6

    #@12f
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@132
    move-result-object v6

    #@133
    const v7, 0x10e000d

    #@136
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    #@139
    move-result v0

    #@13a
    .line 4296
    .local v0, defaultInterval:I
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13c
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@13f
    move-result-object v6

    #@140
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@143
    move-result-object v6

    #@144
    const-string/jumbo v7, "wifi_scan_interval_p2p_connected_ms"

    #@147
    int-to-long v8, v0

    #@148
    invoke-static {v6, v7, v8, v9}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@14b
    move-result-wide v3

    #@14c
    .line 4299
    .local v3, scanIntervalMs:J
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@14e
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@151
    move-result-object v6

    #@152
    long-to-int v7, v3

    #@153
    div-int/lit16 v7, v7, 0x3e8

    #@155
    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiNative;->setScanInterval(I)V

    #@158
    .line 4309
    .end local v0           #defaultInterval:I
    .end local v1           #info:Landroid/net/NetworkInfo;
    .end local v3           #scanIntervalMs:J
    :cond_158
    :goto_158
    :sswitch_158
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@15a
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$1700(Landroid/net/wifi/WifiStateMachine;)Z

    #@15d
    move-result v6

    #@15e
    if-nez v6, :cond_c

    #@160
    const/4 v2, 0x0

    #@161
    goto/16 :goto_c

    #@163
    .line 4300
    .restart local v1       #info:Landroid/net/NetworkInfo;
    :cond_163
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@165
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@168
    move-result-object v6

    #@169
    invoke-virtual {v6}, Landroid/net/wifi/WifiConfigStore;->getConfiguredNetworks()Ljava/util/List;

    #@16c
    move-result-object v6

    #@16d
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@170
    move-result v6

    #@171
    if-nez v6, :cond_158

    #@173
    .line 4302
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@175
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@177
    iget-object v9, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@179
    invoke-static {v9}, Landroid/net/wifi/WifiStateMachine;->access$17104(Landroid/net/wifi/WifiStateMachine;)I

    #@17c
    move-result v9

    #@17d
    invoke-virtual {v7, v10, v9, v8}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@180
    move-result-object v7

    #@181
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$DisconnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@183
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$6600(Landroid/net/wifi/WifiStateMachine;)J

    #@186
    move-result-wide v8

    #@187
    invoke-virtual {v6, v7, v8, v9}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@18a
    goto :goto_158

    #@18b
    .line 4226
    nop

    #@18c
    :sswitch_data_18c
    .sparse-switch
        0x20035 -> :sswitch_56
        0x20047 -> :sswitch_dc
        0x20048 -> :sswitch_6f
        0x2004b -> :sswitch_158
        0x2004c -> :sswitch_158
        0x20058 -> :sswitch_d
        0x2005b -> :sswitch_9a
        0x2300b -> :sswitch_10c
        0x24004 -> :sswitch_c
        0x24005 -> :sswitch_f0
        0x24006 -> :sswitch_ca
        0x25004 -> :sswitch_56
    .end sparse-switch
.end method
