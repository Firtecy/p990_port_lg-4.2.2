.class Landroid/net/wifi/WifiApConfigStore;
.super Lcom/android/internal/util/StateMachine;
.source "WifiApConfigStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiApConfigStore$ActiveState;,
        Landroid/net/wifi/WifiApConfigStore$InactiveState;,
        Landroid/net/wifi/WifiApConfigStore$DefaultState;
    }
.end annotation


# static fields
.field private static final AP_CONFIG_FILE:Ljava/lang/String; = null

.field private static final AP_CONFIG_FILE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WifiApConfigStore"


# instance fields
.field private mActiveState:Lcom/android/internal/util/State;

.field private mContext:Landroid/content/Context;

.field private mDefaultState:Lcom/android/internal/util/State;

.field private mInactiveState:Lcom/android/internal/util/State;

.field private mReplyChannel:Lcom/android/internal/util/AsyncChannel;

.field private mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "/misc/wifi/softap.conf"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    sput-object v0, Landroid/net/wifi/WifiApConfigStore;->AP_CONFIG_FILE:Ljava/lang/String;

    #@19
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 5
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 73
    const-string v0, "WifiApConfigStore"

    #@2
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v1

    #@6
    invoke-direct {p0, v0, v1}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;Landroid/os/Looper;)V

    #@9
    .line 64
    new-instance v0, Landroid/net/wifi/WifiApConfigStore$DefaultState;

    #@b
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiApConfigStore$DefaultState;-><init>(Landroid/net/wifi/WifiApConfigStore;)V

    #@e
    iput-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mDefaultState:Lcom/android/internal/util/State;

    #@10
    .line 65
    new-instance v0, Landroid/net/wifi/WifiApConfigStore$InactiveState;

    #@12
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiApConfigStore$InactiveState;-><init>(Landroid/net/wifi/WifiApConfigStore;)V

    #@15
    iput-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mInactiveState:Lcom/android/internal/util/State;

    #@17
    .line 66
    new-instance v0, Landroid/net/wifi/WifiApConfigStore$ActiveState;

    #@19
    invoke-direct {v0, p0}, Landroid/net/wifi/WifiApConfigStore$ActiveState;-><init>(Landroid/net/wifi/WifiApConfigStore;)V

    #@1c
    iput-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mActiveState:Lcom/android/internal/util/State;

    #@1e
    .line 68
    const/4 v0, 0x0

    #@1f
    iput-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    #@21
    .line 69
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@23
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@26
    iput-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@28
    .line 75
    iput-object p1, p0, Landroid/net/wifi/WifiApConfigStore;->mContext:Landroid/content/Context;

    #@2a
    .line 76
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mDefaultState:Lcom/android/internal/util/State;

    #@2c
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiApConfigStore;->addState(Lcom/android/internal/util/State;)V

    #@2f
    .line 77
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mInactiveState:Lcom/android/internal/util/State;

    #@31
    iget-object v1, p0, Landroid/net/wifi/WifiApConfigStore;->mDefaultState:Lcom/android/internal/util/State;

    #@33
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiApConfigStore;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@36
    .line 78
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mActiveState:Lcom/android/internal/util/State;

    #@38
    iget-object v1, p0, Landroid/net/wifi/WifiApConfigStore;->mDefaultState:Lcom/android/internal/util/State;

    #@3a
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiApConfigStore;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@3d
    .line 80
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mInactiveState:Lcom/android/internal/util/State;

    #@3f
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiApConfigStore;->setInitialState(Lcom/android/internal/util/State;)V

    #@42
    .line 83
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@44
    if-eqz v0, :cond_4c

    #@46
    .line 84
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@49
    move-result-object v0

    #@4a
    iput-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@4c
    .line 87
    :cond_4c
    return-void
.end method

.method static synthetic access$000(Landroid/net/wifi/WifiApConfigStore;)Lcom/lge/wifi_iface/WifiServiceExtIface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/wifi/WifiApConfigStore;)Landroid/net/wifi/WifiConfiguration;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/net/wifi/WifiApConfigStore;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-object p1, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/net/wifi/WifiApConfigStore;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mReplyChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/wifi/WifiApConfigStore;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mActiveState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/net/wifi/WifiApConfigStore;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiApConfigStore;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/net/wifi/WifiApConfigStore;Landroid/net/wifi/WifiConfiguration;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiApConfigStore;->writeApConfiguration(Landroid/net/wifi/WifiConfiguration;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/net/wifi/WifiApConfigStore;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiApConfigStore;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/net/wifi/WifiApConfigStore;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/net/wifi/WifiApConfigStore;->mInactiveState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/net/wifi/WifiApConfigStore;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiApConfigStore;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method public static makeWifiApConfigStore(Landroid/content/Context;Landroid/os/Handler;)Landroid/net/wifi/WifiApConfigStore;
    .registers 3
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 90
    new-instance v0, Landroid/net/wifi/WifiApConfigStore;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/net/wifi/WifiApConfigStore;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@5
    .line 91
    .local v0, s:Landroid/net/wifi/WifiApConfigStore;
    invoke-virtual {v0}, Landroid/net/wifi/WifiApConfigStore;->start()V

    #@8
    .line 92
    return-object v0
.end method

.method private setDefaultApConfiguration()V
    .registers 9

    #@0
    .prologue
    .line 226
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    #@2
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@5
    .line 227
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    iget-object v5, p0, Landroid/net/wifi/WifiApConfigStore;->mContext:Landroid/content/Context;

    #@7
    const v6, 0x1040434

    #@a
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@d
    move-result-object v5

    #@e
    iput-object v5, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@10
    .line 228
    iget-object v5, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@12
    const/4 v6, 0x4

    #@13
    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    #@16
    .line 229
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 232
    .local v3, randomUUID:Ljava/lang/String;
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    const-string v6, "TMO"

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_7b

    #@2a
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    const-string v6, "US"

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v5

    #@34
    if-eqz v5, :cond_7b

    #@36
    .line 233
    const-string v5, ""

    #@38
    iput-object v5, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@3a
    .line 246
    :goto_3a
    const-string v5, "WifiApConfigStore"

    #@3c
    new-instance v6, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v7, "$$$, yeongsu.wu, randomUUID = "

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 248
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@54
    if-eqz v5, :cond_74

    #@56
    iget-object v5, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@58
    if-eqz v5, :cond_74

    #@5a
    .line 249
    iget-object v5, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@5c
    iget-object v6, p0, Landroid/net/wifi/WifiApConfigStore;->mContext:Landroid/content/Context;

    #@5e
    invoke-interface {v5, v6, v0}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setSoftApRename(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@61
    move-result-object v5

    #@62
    iput-object v5, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@64
    .line 251
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->setDefaultMobileHotspotUS()Z

    #@67
    move-result v5

    #@68
    if-eqz v5, :cond_74

    #@6a
    .line 252
    iget-object v5, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@6c
    invoke-interface {v5, v0}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setSoftApDefaultKey(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    .line 253
    .local v1, pKey:Ljava/lang/String;
    if-eqz v1, :cond_74

    #@72
    .line 254
    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@74
    .line 260
    .end local v1           #pKey:Ljava/lang/String;
    :cond_74
    const v5, 0x20019

    #@77
    invoke-virtual {p0, v5, v0}, Landroid/net/wifi/WifiApConfigStore;->sendMessage(ILjava/lang/Object;)V

    #@7a
    .line 261
    return-void

    #@7b
    .line 234
    :cond_7b
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    const-string v6, "TEL"

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@84
    move-result v5

    #@85
    if-eqz v5, :cond_af

    #@87
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, "AU"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@90
    move-result v5

    #@91
    if-eqz v5, :cond_af

    #@93
    .line 236
    new-instance v2, Ljava/util/Random;

    #@95
    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    #@98
    .line 237
    .local v2, random:Ljava/util/Random;
    const v5, 0x55d4a80

    #@9b
    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    #@9e
    move-result v5

    #@9f
    const v6, 0x989680

    #@a2
    add-int/2addr v5, v6

    #@a3
    const v6, 0x5f5e100

    #@a6
    rem-int v4, v5, v6

    #@a8
    .line 239
    .local v4, random_8digit:I
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@ab
    move-result-object v5

    #@ac
    iput-object v5, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@ae
    goto :goto_3a

    #@af
    .line 243
    .end local v2           #random:Ljava/util/Random;
    .end local v4           #random_8digit:I
    :cond_af
    new-instance v5, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const/4 v6, 0x0

    #@b5
    const/16 v7, 0x8

    #@b7
    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ba
    move-result-object v6

    #@bb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v5

    #@bf
    const/16 v6, 0x9

    #@c1
    const/16 v7, 0xd

    #@c3
    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c6
    move-result-object v6

    #@c7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v5

    #@cb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v5

    #@cf
    iput-object v5, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@d1
    goto/16 :goto_3a
.end method

.method private writeApConfiguration(Landroid/net/wifi/WifiConfiguration;)V
    .registers 9
    .parameter "config"

    #@0
    .prologue
    .line 198
    const/4 v2, 0x0

    #@1
    .line 200
    .local v2, out:Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/DataOutputStream;

    #@3
    new-instance v4, Ljava/io/BufferedOutputStream;

    #@5
    new-instance v5, Ljava/io/FileOutputStream;

    #@7
    sget-object v6, Landroid/net/wifi/WifiApConfigStore;->AP_CONFIG_FILE:Ljava/lang/String;

    #@9
    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@c
    invoke-direct {v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@f
    invoke-direct {v3, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_51
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_12} :catch_30

    #@12
    .line 203
    .end local v2           #out:Ljava/io/DataOutputStream;
    .local v3, out:Ljava/io/DataOutputStream;
    const/4 v4, 0x1

    #@13
    :try_start_13
    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@16
    .line 204
    iget-object v4, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@18
    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1b
    .line 205
    invoke-virtual {p1}, Landroid/net/wifi/WifiConfiguration;->getAuthType()I

    #@1e
    move-result v0

    #@1f
    .line 206
    .local v0, authType:I
    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@22
    .line 207
    if-eqz v0, :cond_29

    #@24
    .line 208
    iget-object v4, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@26
    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_29
    .catchall {:try_start_13 .. :try_end_29} :catchall_5c
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_29} :catch_5f

    #@29
    .line 213
    :cond_29
    if-eqz v3, :cond_2e

    #@2b
    .line 215
    :try_start_2b
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2e} :catch_5a

    #@2e
    :cond_2e
    :goto_2e
    move-object v2, v3

    #@2f
    .line 219
    .end local v0           #authType:I
    .end local v3           #out:Ljava/io/DataOutputStream;
    .restart local v2       #out:Ljava/io/DataOutputStream;
    :cond_2f
    :goto_2f
    return-void

    #@30
    .line 210
    :catch_30
    move-exception v1

    #@31
    .line 211
    .local v1, e:Ljava/io/IOException;
    :goto_31
    :try_start_31
    const-string v4, "WifiApConfigStore"

    #@33
    new-instance v5, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v6, "Error writing hotspot configuration"

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_31 .. :try_end_49} :catchall_51

    #@49
    .line 213
    if-eqz v2, :cond_2f

    #@4b
    .line 215
    :try_start_4b
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_2f

    #@4f
    .line 216
    :catch_4f
    move-exception v4

    #@50
    goto :goto_2f

    #@51
    .line 213
    .end local v1           #e:Ljava/io/IOException;
    :catchall_51
    move-exception v4

    #@52
    :goto_52
    if-eqz v2, :cond_57

    #@54
    .line 215
    :try_start_54
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_57} :catch_58

    #@57
    .line 213
    :cond_57
    :goto_57
    throw v4

    #@58
    .line 216
    :catch_58
    move-exception v5

    #@59
    goto :goto_57

    #@5a
    .end local v2           #out:Ljava/io/DataOutputStream;
    .restart local v0       #authType:I
    .restart local v3       #out:Ljava/io/DataOutputStream;
    :catch_5a
    move-exception v4

    #@5b
    goto :goto_2e

    #@5c
    .line 213
    .end local v0           #authType:I
    :catchall_5c
    move-exception v4

    #@5d
    move-object v2, v3

    #@5e
    .end local v3           #out:Ljava/io/DataOutputStream;
    .restart local v2       #out:Ljava/io/DataOutputStream;
    goto :goto_52

    #@5f
    .line 210
    .end local v2           #out:Ljava/io/DataOutputStream;
    .restart local v3       #out:Ljava/io/DataOutputStream;
    :catch_5f
    move-exception v1

    #@60
    move-object v2, v3

    #@61
    .end local v3           #out:Ljava/io/DataOutputStream;
    .restart local v2       #out:Ljava/io/DataOutputStream;
    goto :goto_31
.end method


# virtual methods
.method getMessenger()Landroid/os/Messenger;
    .registers 3

    #@0
    .prologue
    .line 194
    new-instance v0, Landroid/os/Messenger;

    #@2
    invoke-virtual {p0}, Landroid/net/wifi/WifiApConfigStore;->getHandler()Landroid/os/Handler;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@9
    return-object v0
.end method

.method loadApConfiguration()V
    .registers 10

    #@0
    .prologue
    .line 163
    const/4 v3, 0x0

    #@1
    .line 165
    .local v3, in:Ljava/io/DataInputStream;
    :try_start_1
    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    #@3
    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@6
    .line 166
    .local v1, config:Landroid/net/wifi/WifiConfiguration;
    new-instance v4, Ljava/io/DataInputStream;

    #@8
    new-instance v6, Ljava/io/BufferedInputStream;

    #@a
    new-instance v7, Ljava/io/FileInputStream;

    #@c
    sget-object v8, Landroid/net/wifi/WifiApConfigStore;->AP_CONFIG_FILE:Ljava/lang/String;

    #@e
    invoke-direct {v7, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@11
    invoke-direct {v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@14
    invoke-direct {v4, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_5b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_17} :catch_4f

    #@17
    .line 169
    .end local v3           #in:Ljava/io/DataInputStream;
    .local v4, in:Ljava/io/DataInputStream;
    :try_start_17
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@1a
    move-result v5

    #@1b
    .line 170
    .local v5, version:I
    const/4 v6, 0x1

    #@1c
    if-eq v5, v6, :cond_2f

    #@1e
    .line 171
    const-string v6, "WifiApConfigStore"

    #@20
    const-string v7, "Bad version on hotspot configuration file, set defaults"

    #@22
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 172
    invoke-direct {p0}, Landroid/net/wifi/WifiApConfigStore;->setDefaultApConfiguration()V
    :try_end_28
    .catchall {:try_start_17 .. :try_end_28} :catchall_68
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_28} :catch_6b

    #@28
    .line 185
    if-eqz v4, :cond_2d

    #@2a
    .line 187
    :try_start_2a
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2d} :catch_64

    #@2d
    :cond_2d
    :goto_2d
    move-object v3, v4

    #@2e
    .line 191
    .end local v1           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v4           #in:Ljava/io/DataInputStream;
    .end local v5           #version:I
    .restart local v3       #in:Ljava/io/DataInputStream;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 175
    .end local v3           #in:Ljava/io/DataInputStream;
    .restart local v1       #config:Landroid/net/wifi/WifiConfiguration;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v5       #version:I
    :cond_2f
    :try_start_2f
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@32
    move-result-object v6

    #@33
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@35
    .line 176
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    #@38
    move-result v0

    #@39
    .line 177
    .local v0, authType:I
    iget-object v6, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@3b
    invoke-virtual {v6, v0}, Ljava/util/BitSet;->set(I)V

    #@3e
    .line 178
    if-eqz v0, :cond_46

    #@40
    .line 179
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@46
    .line 181
    :cond_46
    iput-object v1, p0, Landroid/net/wifi/WifiApConfigStore;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;
    :try_end_48
    .catchall {:try_start_2f .. :try_end_48} :catchall_68
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_48} :catch_6b

    #@48
    .line 185
    if-eqz v4, :cond_4d

    #@4a
    .line 187
    :try_start_4a
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_66

    #@4d
    :cond_4d
    :goto_4d
    move-object v3, v4

    #@4e
    .line 190
    .end local v4           #in:Ljava/io/DataInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    goto :goto_2e

    #@4f
    .line 182
    .end local v0           #authType:I
    .end local v1           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v5           #version:I
    :catch_4f
    move-exception v2

    #@50
    .line 183
    .local v2, ignore:Ljava/io/IOException;
    :goto_50
    :try_start_50
    invoke-direct {p0}, Landroid/net/wifi/WifiApConfigStore;->setDefaultApConfiguration()V
    :try_end_53
    .catchall {:try_start_50 .. :try_end_53} :catchall_5b

    #@53
    .line 185
    if-eqz v3, :cond_2e

    #@55
    .line 187
    :try_start_55
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_58} :catch_59

    #@58
    goto :goto_2e

    #@59
    .line 188
    :catch_59
    move-exception v6

    #@5a
    goto :goto_2e

    #@5b
    .line 185
    .end local v2           #ignore:Ljava/io/IOException;
    :catchall_5b
    move-exception v6

    #@5c
    :goto_5c
    if-eqz v3, :cond_61

    #@5e
    .line 187
    :try_start_5e
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_61} :catch_62

    #@61
    .line 185
    :cond_61
    :goto_61
    throw v6

    #@62
    .line 188
    :catch_62
    move-exception v7

    #@63
    goto :goto_61

    #@64
    .end local v3           #in:Ljava/io/DataInputStream;
    .restart local v1       #config:Landroid/net/wifi/WifiConfiguration;
    .restart local v4       #in:Ljava/io/DataInputStream;
    .restart local v5       #version:I
    :catch_64
    move-exception v6

    #@65
    goto :goto_2d

    #@66
    .restart local v0       #authType:I
    :catch_66
    move-exception v6

    #@67
    goto :goto_4d

    #@68
    .line 185
    .end local v0           #authType:I
    .end local v5           #version:I
    :catchall_68
    move-exception v6

    #@69
    move-object v3, v4

    #@6a
    .end local v4           #in:Ljava/io/DataInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    goto :goto_5c

    #@6b
    .line 182
    .end local v3           #in:Ljava/io/DataInputStream;
    .restart local v4       #in:Ljava/io/DataInputStream;
    :catch_6b
    move-exception v2

    #@6c
    move-object v3, v4

    #@6d
    .end local v4           #in:Ljava/io/DataInputStream;
    .restart local v3       #in:Ljava/io/DataInputStream;
    goto :goto_50
.end method
