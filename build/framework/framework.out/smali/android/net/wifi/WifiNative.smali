.class public Landroid/net/wifi/WifiNative;
.super Ljava/lang/Object;
.source "WifiNative.java"


# static fields
.field static final BLUETOOTH_COEXISTENCE_MODE_DISABLED:I = 0x1

.field static final BLUETOOTH_COEXISTENCE_MODE_ENABLED:I = 0x0

.field static final BLUETOOTH_COEXISTENCE_MODE_SENSE:I = 0x2

.field private static final DBG:Z = false

.field private static final DEFAULT_GROUP_OWNER_INTENT:I = 0x5


# instance fields
.field mInterface:Ljava/lang/String;

.field private mSuspendOptEnabled:Z

.field private final mTAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    const-string v0, ""

    #@5
    iput-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@7
    .line 52
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/net/wifi/WifiNative;->mSuspendOptEnabled:Z

    #@a
    .line 83
    iput-object p1, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@c
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v1, "WifiNative-"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/net/wifi/WifiNative;->mTAG:Ljava/lang/String;

    #@21
    .line 85
    return-void
.end method

.method private native closeSupplicantConnection(Ljava/lang/String;)V
.end method

.method private native connectToSupplicant(Ljava/lang/String;)Z
.end method

.method private doBooleanCommand(Ljava/lang/String;)Z
    .registers 3
    .parameter "command"

    #@0
    .prologue
    .line 100
    const-string/jumbo v0, "psk"

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_9

    #@9
    .line 103
    :cond_9
    iget-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@b
    invoke-direct {p0, v0, p1}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    return v0
.end method

.method private native doBooleanCommand(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private doIntCommand(Ljava/lang/String;)I
    .registers 3
    .parameter "command"

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0, p1}, Landroid/net/wifi/WifiNative;->doIntCommand(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private native doIntCommand(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private doStringCommand(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "command"

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0, p1}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private native doStringCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native isDriverLoaded()Z
.end method

.method public static native killSupplicant(Z)Z
.end method

.method public static native loadDriver()Z
.end method

.method private p2pGetParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "deviceAddress"
    .parameter "key"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 773
    if-nez p1, :cond_4

    #@3
    .line 787
    :cond_3
    :goto_3
    return-object v7

    #@4
    .line 775
    :cond_4
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiNative;->p2pPeer(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v4

    #@8
    .line 776
    .local v4, peerInfo:Ljava/lang/String;
    if-eqz v4, :cond_3

    #@a
    .line 777
    const-string v8, "\n"

    #@c
    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@f
    move-result-object v6

    #@10
    .line 779
    .local v6, tokens:[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    const-string v9, "="

    #@1b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object p2

    #@23
    .line 780
    move-object v0, v6

    #@24
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@25
    .local v2, len$:I
    const/4 v1, 0x0

    #@26
    .local v1, i$:I
    :goto_26
    if-ge v1, v2, :cond_3

    #@28
    aget-object v5, v0, v1

    #@2a
    .line 781
    .local v5, token:Ljava/lang/String;
    invoke-virtual {v5, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2d
    move-result v8

    #@2e
    if-eqz v8, :cond_3e

    #@30
    .line 782
    const-string v8, "="

    #@32
    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    .line 783
    .local v3, nameValue:[Ljava/lang/String;
    array-length v8, v3

    #@37
    const/4 v9, 0x2

    #@38
    if-ne v8, v9, :cond_3

    #@3a
    .line 784
    const/4 v7, 0x1

    #@3b
    aget-object v7, v3, v7

    #@3d
    goto :goto_3

    #@3e
    .line 780
    .end local v3           #nameValue:[Ljava/lang/String;
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_26
.end method

.method public static native startSupplicant(Z)Z
.end method

.method public static native unloadDriver()Z
.end method

.method private native waitForEvent(Ljava/lang/String;)Ljava/lang/String;
.end method


# virtual methods
.method public addNetwork()I
    .registers 2

    #@0
    .prologue
    .line 148
    const-string v0, "ADD_NETWORK"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doIntCommand(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public addToBlacklist(Ljava/lang/String;)Z
    .registers 4
    .parameter "bssid"

    #@0
    .prologue
    .line 392
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 393
    :goto_7
    return v0

    #@8
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "BLACKLIST "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1e
    move-result v0

    #@1f
    goto :goto_7
.end method

.method public cancelWps()Z
    .registers 2

    #@0
    .prologue
    .line 494
    const-string v0, "WPS_CANCEL"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clearBlacklist()Z
    .registers 2

    #@0
    .prologue
    .line 397
    const-string v0, "BLACKLIST clear"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public closeSupplicantConnection()V
    .registers 2

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->closeSupplicantConnection(Ljava/lang/String;)V

    #@5
    .line 93
    return-void
.end method

.method public connectToSupplicant()Z
    .registers 2

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->connectToSupplicant(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public disableNetwork(I)Z
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DISABLE_NETWORK "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public disconnect()Z
    .registers 2

    #@0
    .prologue
    .line 186
    const-string v0, "DISCONNECT"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public enableBackgroundScan(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 415
    if-eqz p1, :cond_8

    #@2
    .line 416
    const-string v0, "SET pno 1"

    #@4
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@7
    .line 420
    :goto_7
    return-void

    #@8
    .line 418
    :cond_8
    const-string v0, "SET pno 0"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    goto :goto_7
.end method

.method public enableNetwork(IZ)Z
    .registers 5
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    .line 166
    if-eqz p2, :cond_1a

    #@2
    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "SELECT_NETWORK "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@18
    move-result v0

    #@19
    .line 169
    :goto_19
    return v0

    #@1a
    :cond_1a
    new-instance v0, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v1, "ENABLE_NETWORK "

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@30
    move-result v0

    #@31
    goto :goto_19
.end method

.method public getBand()I
    .registers 7

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 337
    const-string v4, "DRIVER GETBAND"

    #@3
    invoke-direct {p0, v4}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 338
    .local v1, ret:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v4

    #@b
    if-nez v4, :cond_1e

    #@d
    .line 340
    const-string v4, " "

    #@f
    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 342
    .local v2, tokens:[Ljava/lang/String;
    :try_start_13
    array-length v4, v2

    #@14
    const/4 v5, 0x2

    #@15
    if-ne v4, v5, :cond_1e

    #@17
    const/4 v4, 0x1

    #@18
    aget-object v4, v2, v4

    #@1a
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1d
    .catch Ljava/lang/NumberFormatException; {:try_start_13 .. :try_end_1d} :catch_1f

    #@1d
    move-result v3

    #@1e
    .line 347
    .end local v2           #tokens:[Ljava/lang/String;
    :cond_1e
    :goto_1e
    return v3

    #@1f
    .line 343
    .restart local v2       #tokens:[Ljava/lang/String;
    :catch_1f
    move-exception v0

    #@20
    .line 344
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_1e
.end method

.method public getGroupCapability(Ljava/lang/String;)I
    .registers 13
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 748
    const/4 v2, 0x0

    #@1
    .line 749
    .local v2, gc:I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v9

    #@5
    if-eqz v9, :cond_8

    #@7
    .line 765
    .end local v2           #gc:I
    :cond_7
    :goto_7
    return v2

    #@8
    .line 750
    .restart local v2       #gc:I
    :cond_8
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiNative;->p2pPeer(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v6

    #@c
    .line 751
    .local v6, peerInfo:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v9

    #@10
    if-nez v9, :cond_7

    #@12
    .line 753
    const-string v9, "\n"

    #@14
    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@17
    move-result-object v8

    #@18
    .line 754
    .local v8, tokens:[Ljava/lang/String;
    move-object v0, v8

    #@19
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@1a
    .local v4, len$:I
    const/4 v3, 0x0

    #@1b
    .local v3, i$:I
    :goto_1b
    if-ge v3, v4, :cond_7

    #@1d
    aget-object v7, v0, v3

    #@1f
    .line 755
    .local v7, token:Ljava/lang/String;
    const-string v9, "group_capab="

    #@21
    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@24
    move-result v9

    #@25
    if-eqz v9, :cond_3f

    #@27
    .line 756
    const-string v9, "="

    #@29
    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    .line 757
    .local v5, nameValue:[Ljava/lang/String;
    array-length v9, v5

    #@2e
    const/4 v10, 0x2

    #@2f
    if-ne v9, v10, :cond_7

    #@31
    .line 759
    const/4 v9, 0x1

    #@32
    :try_start_32
    aget-object v9, v5, v9

    #@34
    invoke-static {v9}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@37
    move-result-object v9

    #@38
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I
    :try_end_3b
    .catch Ljava/lang/NumberFormatException; {:try_start_32 .. :try_end_3b} :catch_3d

    #@3b
    move-result v2

    #@3c
    goto :goto_7

    #@3d
    .line 760
    :catch_3d
    move-exception v1

    #@3e
    .line 761
    .local v1, e:Ljava/lang/NumberFormatException;
    goto :goto_7

    #@3f
    .line 754
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v5           #nameValue:[Ljava/lang/String;
    :cond_3f
    add-int/lit8 v3, v3, 0x1

    #@41
    goto :goto_1b
.end method

.method public getMacAddress()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 195
    const-string v2, "DRIVER MACADDR"

    #@2
    invoke-direct {p0, v2}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 196
    .local v0, ret:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_1a

    #@c
    .line 197
    const-string v2, " = "

    #@e
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 198
    .local v1, tokens:[Ljava/lang/String;
    array-length v2, v1

    #@13
    const/4 v3, 0x2

    #@14
    if-ne v2, v3, :cond_1a

    #@16
    const/4 v2, 0x1

    #@17
    aget-object v2, v1, v2

    #@19
    .line 200
    .end local v1           #tokens:[Ljava/lang/String;
    :goto_19
    return-object v2

    #@1a
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_19
.end method

.method public getNetworkVariable(ILjava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "netId"
    .parameter "name"

    #@0
    .prologue
    .line 157
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 158
    :goto_7
    return-object v0

    #@8
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "GET_NETWORK "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    goto :goto_7
.end method

.method public listNetworks()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 144
    const-string v0, "LIST_NETWORKS"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public p2pCancelConnect()Z
    .registers 2

    #@0
    .prologue
    .line 668
    const-string v0, "P2P_CANCEL"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public p2pConnect(Landroid/net/wifi/p2p/WifiP2pConfig;Z)Ljava/lang/String;
    .registers 11
    .parameter "config"
    .parameter "joinExistingGroup"

    #@0
    .prologue
    .line 613
    if-nez p1, :cond_4

    #@2
    const/4 v6, 0x0

    #@3
    .line 664
    :goto_3
    return-object v6

    #@4
    .line 614
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 615
    .local v0, args:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@b
    .line 616
    .local v5, wps:Landroid/net/wifi/WpsInfo;
    iget-object v6, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@d
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@10
    .line 618
    iget v6, v5, Landroid/net/wifi/WpsInfo;->setup:I

    #@12
    packed-switch v6, :pswitch_data_b6

    #@15
    .line 641
    :goto_15
    iget v6, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    #@17
    const/4 v7, -0x2

    #@18
    if-ne v6, v7, :cond_20

    #@1a
    .line 642
    const-string/jumbo v6, "persistent"

    #@1d
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@20
    .line 645
    :cond_20
    if-eqz p2, :cond_8b

    #@22
    .line 646
    const-string/jumbo v6, "join"

    #@25
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@28
    .line 661
    :goto_28
    const-string v1, "P2P_CONNECT "

    #@2a
    .line 662
    .local v1, command:Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v3

    #@2e
    .local v3, i$:Ljava/util/Iterator;
    :goto_2e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_b0

    #@34
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@37
    move-result-object v4

    #@38
    check-cast v4, Ljava/lang/String;

    #@3a
    .local v4, s:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    const-string v7, " "

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    goto :goto_2e

    #@52
    .line 620
    .end local v1           #command:Ljava/lang/String;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #s:Ljava/lang/String;
    :pswitch_52
    const-string/jumbo v6, "pbc"

    #@55
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@58
    goto :goto_15

    #@59
    .line 623
    :pswitch_59
    iget-object v6, v5, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@5b
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5e
    move-result v6

    #@5f
    if-eqz v6, :cond_6d

    #@61
    .line 624
    const-string/jumbo v6, "pin"

    #@64
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@67
    .line 628
    :goto_67
    const-string v6, "display"

    #@69
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@6c
    goto :goto_15

    #@6d
    .line 626
    :cond_6d
    iget-object v6, v5, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@6f
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@72
    goto :goto_67

    #@73
    .line 631
    :pswitch_73
    iget-object v6, v5, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@75
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@78
    .line 632
    const-string/jumbo v6, "keypad"

    #@7b
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@7e
    goto :goto_15

    #@7f
    .line 635
    :pswitch_7f
    iget-object v6, v5, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    #@81
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@84
    .line 636
    const-string/jumbo v6, "label"

    #@87
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@8a
    goto :goto_15

    #@8b
    .line 650
    :cond_8b
    iget v2, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    #@8d
    .line 652
    .local v2, groupOwnerIntent:I
    if-ltz v2, :cond_93

    #@8f
    const/16 v6, 0xf

    #@91
    if-le v2, v6, :cond_94

    #@93
    .line 653
    :cond_93
    const/4 v2, 0x5

    #@94
    .line 655
    :cond_94
    const/4 v6, 0x5

    #@95
    if-le v2, v6, :cond_98

    #@97
    .line 656
    const/4 v2, 0x5

    #@98
    .line 658
    :cond_98
    new-instance v6, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v7, "go_intent="

    #@9f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v6

    #@a3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@ae
    goto/16 :goto_28

    #@b0
    .line 664
    .end local v2           #groupOwnerIntent:I
    .restart local v1       #command:Ljava/lang/String;
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_b0
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@b3
    move-result-object v6

    #@b4
    goto/16 :goto_3

    #@b6
    .line 618
    :pswitch_data_b6
    .packed-switch 0x0
        :pswitch_52
        :pswitch_59
        :pswitch_73
        :pswitch_7f
    .end packed-switch
.end method

.method public p2pFind()Z
    .registers 2

    #@0
    .prologue
    .line 581
    const-string v0, "P2P_FIND"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public p2pFind(I)Z
    .registers 4
    .parameter "timeout"

    #@0
    .prologue
    .line 585
    if-gtz p1, :cond_7

    #@2
    .line 586
    invoke-virtual {p0}, Landroid/net/wifi/WifiNative;->p2pFind()Z

    #@5
    move-result v0

    #@6
    .line 588
    :goto_6
    return v0

    #@7
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v1, "P2P_FIND "

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    goto :goto_6
.end method

.method public p2pFlush()Z
    .registers 2

    #@0
    .prologue
    .line 607
    const-string v0, "P2P_FLUSH"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public p2pGetDeviceAddress()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    .line 733
    invoke-virtual {p0}, Landroid/net/wifi/WifiNative;->status()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 734
    .local v4, status:Ljava/lang/String;
    if-nez v4, :cond_9

    #@6
    const-string v7, ""

    #@8
    .line 744
    :goto_8
    return-object v7

    #@9
    .line 736
    :cond_9
    const-string v7, "\n"

    #@b
    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e
    move-result-object v6

    #@f
    .line 737
    .local v6, tokens:[Ljava/lang/String;
    move-object v0, v6

    #@10
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@11
    .local v2, len$:I
    const/4 v1, 0x0

    #@12
    .local v1, i$:I
    :goto_12
    if-ge v1, v2, :cond_29

    #@14
    aget-object v5, v0, v1

    #@16
    .line 738
    .local v5, token:Ljava/lang/String;
    const-string/jumbo v7, "p2p_device_address="

    #@19
    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1c
    move-result v7

    #@1d
    if-eqz v7, :cond_30

    #@1f
    .line 739
    const-string v7, "="

    #@21
    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    .line 740
    .local v3, nameValue:[Ljava/lang/String;
    array-length v7, v3

    #@26
    const/4 v8, 0x2

    #@27
    if-eq v7, v8, :cond_2c

    #@29
    .line 744
    .end local v3           #nameValue:[Ljava/lang/String;
    .end local v5           #token:Ljava/lang/String;
    :cond_29
    const-string v7, ""

    #@2b
    goto :goto_8

    #@2c
    .line 741
    .restart local v3       #nameValue:[Ljava/lang/String;
    .restart local v5       #token:Ljava/lang/String;
    :cond_2c
    const/4 v7, 0x1

    #@2d
    aget-object v7, v3, v7

    #@2f
    goto :goto_8

    #@30
    .line 737
    .end local v3           #nameValue:[Ljava/lang/String;
    :cond_30
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_12
.end method

.method public p2pGetSsid(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 729
    const-string/jumbo v0, "oper_ssid"

    #@3
    invoke-direct {p0, p1, v0}, Landroid/net/wifi/WifiNative;->p2pGetParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public p2pGroupAdd(I)Z
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "P2P_GROUP_ADD persistent="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public p2pGroupAdd(Z)Z
    .registers 3
    .parameter "persistent"

    #@0
    .prologue
    .line 690
    if-eqz p1, :cond_9

    #@2
    .line 691
    const-string v0, "P2P_GROUP_ADD persistent"

    #@4
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    .line 693
    :goto_8
    return v0

    #@9
    :cond_9
    const-string v0, "P2P_GROUP_ADD"

    #@b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    goto :goto_8
.end method

.method public p2pGroupRemove(Ljava/lang/String;)Z
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 701
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 702
    :goto_7
    return v0

    #@8
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "P2P_GROUP_REMOVE "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1e
    move-result v0

    #@1f
    goto :goto_7
.end method

.method public p2pInvite(Landroid/net/wifi/p2p/WifiP2pGroup;Ljava/lang/String;)Z
    .registers 5
    .parameter "group"
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 711
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 716
    :goto_7
    return v0

    #@8
    .line 713
    :cond_8
    if-nez p1, :cond_22

    #@a
    .line 714
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "P2P_INVITE peer="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@20
    move-result v0

    #@21
    goto :goto_7

    #@22
    .line 716
    :cond_22
    new-instance v0, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v1, "P2P_INVITE group="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getInterface()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " peer="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, " go_dev_addr="

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    #@48
    move-result-object v1

    #@49
    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@56
    move-result v0

    #@57
    goto :goto_7
.end method

.method public p2pListen()Z
    .registers 2

    #@0
    .prologue
    .line 596
    const-string v0, "P2P_LISTEN"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public p2pListen(I)Z
    .registers 4
    .parameter "timeout"

    #@0
    .prologue
    .line 600
    if-gtz p1, :cond_7

    #@2
    .line 601
    invoke-virtual {p0}, Landroid/net/wifi/WifiNative;->p2pListen()Z

    #@5
    move-result v0

    #@6
    .line 603
    :goto_6
    return v0

    #@7
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v1, "P2P_LISTEN "

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    goto :goto_6
.end method

.method public p2pPeer(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "P2P_PEER "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method public p2pProvisionDiscovery(Landroid/net/wifi/p2p/WifiP2pConfig;)Z
    .registers 4
    .parameter "config"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 672
    if-nez p1, :cond_4

    #@3
    .line 686
    :goto_3
    return v0

    #@4
    .line 674
    :cond_4
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    #@6
    iget v1, v1, Landroid/net/wifi/WpsInfo;->setup:I

    #@8
    packed-switch v1, :pswitch_data_6c

    #@b
    goto :goto_3

    #@c
    .line 676
    :pswitch_c
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v1, "P2P_PROV_DISC "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " pbc"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@2a
    move-result v0

    #@2b
    goto :goto_3

    #@2c
    .line 679
    :pswitch_2c
    new-instance v0, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v1, "P2P_PROV_DISC "

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    const-string v1, " keypad"

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@4a
    move-result v0

    #@4b
    goto :goto_3

    #@4c
    .line 682
    :pswitch_4c
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v1, "P2P_PROV_DISC "

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    const-string v1, " display"

    #@5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v0

    #@63
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@6a
    move-result v0

    #@6b
    goto :goto_3

    #@6c
    .line 674
    :pswitch_data_6c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_2c
        :pswitch_4c
    .end packed-switch
.end method

.method public p2pReinvoke(ILjava/lang/String;)Z
    .registers 5
    .parameter "netId"
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 723
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    if-gez p1, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 725
    :goto_9
    return v0

    #@a
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "P2P_INVITE persistent="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, " peer="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@2a
    move-result v0

    #@2b
    goto :goto_9
.end method

.method public p2pReject(Ljava/lang/String;)Z
    .registers 4
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 706
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "P2P_REJECT "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public p2pServDiscCancelReq(Ljava/lang/String;)Z
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 861
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "P2P_SERV_DISC_CANCEL_REQ "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public p2pServDiscReq(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "addr"
    .parameter "query"

    #@0
    .prologue
    .line 853
    const-string v0, "P2P_SERV_DISC_REQ"

    #@2
    .line 854
    .local v0, command:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    const-string v2, " "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 855
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, " "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 857
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    return-object v1
.end method

.method public p2pServiceAdd(Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z
    .registers 7
    .parameter "servInfo"

    #@0
    .prologue
    .line 811
    invoke-virtual {p1}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->getSupplicantQueryList()Ljava/util/List;

    #@3
    move-result-object v3

    #@4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_35

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Ljava/lang/String;

    #@14
    .line 812
    .local v2, s:Ljava/lang/String;
    const-string v0, "P2P_SERVICE_ADD"

    #@16
    .line 813
    .local v0, command:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, " "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 814
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@30
    move-result v3

    #@31
    if-nez v3, :cond_8

    #@33
    .line 815
    const/4 v3, 0x0

    #@34
    .line 818
    .end local v0           #command:Ljava/lang/String;
    .end local v2           #s:Ljava/lang/String;
    :goto_34
    return v3

    #@35
    :cond_35
    const/4 v3, 0x1

    #@36
    goto :goto_34
.end method

.method public p2pServiceDel(Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;)Z
    .registers 10
    .parameter "servInfo"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 826
    invoke-virtual {p1}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;->getSupplicantQueryList()Ljava/util/List;

    #@5
    move-result-object v6

    #@6
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v2

    #@a
    .local v2, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_7d

    #@10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/lang/String;

    #@16
    .line 827
    .local v3, s:Ljava/lang/String;
    const-string v0, "P2P_SERVICE_DEL "

    #@18
    .line 829
    .local v0, command:Ljava/lang/String;
    const-string v6, " "

    #@1a
    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    .line 830
    .local v1, data:[Ljava/lang/String;
    array-length v6, v1

    #@1f
    const/4 v7, 0x2

    #@20
    if-ge v6, v7, :cond_23

    #@22
    .line 845
    .end local v0           #command:Ljava/lang/String;
    .end local v1           #data:[Ljava/lang/String;
    .end local v3           #s:Ljava/lang/String;
    :cond_22
    :goto_22
    return v4

    #@23
    .line 833
    .restart local v0       #command:Ljava/lang/String;
    .restart local v1       #data:[Ljava/lang/String;
    .restart local v3       #s:Ljava/lang/String;
    :cond_23
    const-string/jumbo v6, "upnp"

    #@26
    aget-object v7, v1, v4

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_46

    #@2e
    .line 834
    new-instance v6, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    .line 841
    :goto_3f
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@42
    move-result v6

    #@43
    if-nez v6, :cond_a

    #@45
    goto :goto_22

    #@46
    .line 835
    :cond_46
    const-string v6, "bonjour"

    #@48
    aget-object v7, v1, v4

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v6

    #@4e
    if-eqz v6, :cond_22

    #@50
    .line 836
    new-instance v6, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    aget-object v7, v1, v4

    #@5b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    .line 837
    new-instance v6, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    const-string v7, " "

    #@6e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    aget-object v7, v1, v5

    #@74
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    goto :goto_3f

    #@7d
    .end local v0           #command:Ljava/lang/String;
    .end local v1           #data:[Ljava/lang/String;
    .end local v3           #s:Ljava/lang/String;
    :cond_7d
    move v4, v5

    #@7e
    .line 845
    goto :goto_22
.end method

.method public p2pServiceFlush()Z
    .registers 2

    #@0
    .prologue
    .line 849
    const-string v0, "P2P_SERVICE_FLUSH"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public p2pStopFind()Z
    .registers 2

    #@0
    .prologue
    .line 592
    const-string v0, "P2P_STOP_FIND"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public ping()Z
    .registers 3

    #@0
    .prologue
    .line 117
    const-string v1, "PING"

    #@2
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 118
    .local v0, pong:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@8
    const-string v1, "PONG"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    const/4 v1, 0x1

    #@11
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method public pktcntPoll()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 441
    const-string v0, "PKTCNT_POLL"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public reassociate()Z
    .registers 2

    #@0
    .prologue
    .line 182
    const-string v0, "REASSOCIATE"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public reconnect()Z
    .registers 2

    #@0
    .prologue
    .line 178
    const-string v0, "RECONNECT"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public removeNetwork(I)Z
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "REMOVE_NETWORK "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public saveConfig()Z
    .registers 2

    #@0
    .prologue
    .line 384
    const-string v0, "AP_SCAN 1"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    const-string v0, "SAVE_CONFIG"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public scan()Z
    .registers 2

    #@0
    .prologue
    .line 122
    const-string v0, "SCAN"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public scanResults()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 218
    const-string v0, "BSS RANGE=ALL MASK=0x1986"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public setBand(I)Z
    .registers 4
    .parameter "band"

    #@0
    .prologue
    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DRIVER SETBAND "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setBluetoothCoexistenceMode(I)Z
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DRIVER BTCOEXMODE "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setBluetoothCoexistenceScanMode(Z)Z
    .registers 3
    .parameter "setCoexScanMode"

    #@0
    .prologue
    .line 375
    if-eqz p1, :cond_9

    #@2
    .line 376
    const-string v0, "DRIVER BTCOEXSCAN-START"

    #@4
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    .line 378
    :goto_8
    return v0

    #@9
    :cond_9
    const-string v0, "DRIVER BTCOEXSCAN-STOP"

    #@b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    goto :goto_8
.end method

.method public setConcurrencyPriority(Ljava/lang/String;)Z
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 577
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "P2P_SET conc_pref "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setConfigMethods(Ljava/lang/String;)Z
    .registers 4
    .parameter "cfg"

    #@0
    .prologue
    .line 521
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET config_methods "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setCountryCode(Ljava/lang/String;)Z
    .registers 4
    .parameter "countryCode"

    #@0
    .prologue
    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DRIVER COUNTRY "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setDeviceName(Ljava/lang/String;)Z
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 513
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET device_name "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setDeviceType(Ljava/lang/String;)Z
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET device_type "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setL2RoamingTrigger(III)Z
    .registers 6
    .parameter "threshold"
    .parameter "delta"
    .parameter "scanperiod"

    #@0
    .prologue
    .line 499
    const/16 v0, -0x64

    #@2
    if-le p1, v0, :cond_1c

    #@4
    if-gez p1, :cond_1c

    #@6
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "DRIVER SETROAMTRIGGER "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1c
    .line 500
    :cond_1c
    if-lez p2, :cond_34

    #@1e
    new-instance v0, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v1, "DRIVER SETROAMDELTA "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@34
    .line 501
    :cond_34
    if-lez p3, :cond_4c

    #@36
    new-instance v0, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v1, "DRIVER SETROAMSCANPERIOD "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@4c
    .line 503
    :cond_4c
    const/4 v0, 0x1

    #@4d
    return v0
.end method

.method public setManufacturer(Ljava/lang/String;)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET manufacturer "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setModelName(Ljava/lang/String;)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET model_name "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setModelNumber(Ljava/lang/String;)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 533
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET model_number "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setNetworkVariable(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "netId"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 152
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 153
    :goto_d
    return v0

    #@e
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v1, "SET_NETWORK "

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v1, " "

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@38
    move-result v0

    #@39
    goto :goto_d
.end method

.method public setP2pGroupIdle(Ljava/lang/String;I)Z
    .registers 5
    .parameter "iface"
    .parameter "time"

    #@0
    .prologue
    .line 545
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET interface="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " p2p_group_idle "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@20
    move-result v0

    #@21
    return v0
.end method

.method public setP2pPowerSave(Ljava/lang/String;Z)Z
    .registers 5
    .parameter "iface"
    .parameter "enabled"

    #@0
    .prologue
    .line 557
    if-eqz p2, :cond_20

    #@2
    .line 558
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "P2P_SET interface="

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " ps 1"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1e
    move-result v0

    #@1f
    .line 560
    :goto_1f
    return v0

    #@20
    :cond_20
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, "P2P_SET interface="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, " ps 0"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@3c
    move-result v0

    #@3d
    goto :goto_1f
.end method

.method public setP2pSsidPostfix(Ljava/lang/String;)Z
    .registers 4
    .parameter "postfix"

    #@0
    .prologue
    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET p2p_ssid_postfix "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setPersistentReconnect(Z)Z
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 508
    if-ne p1, v0, :cond_1b

    #@3
    .line 509
    .local v0, value:I
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "SET persistent_reconnect "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@19
    move-result v1

    #@1a
    return v1

    #@1b
    .line 508
    .end local v0           #value:I
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_3
.end method

.method public setPowerSave(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 549
    if-eqz p1, :cond_8

    #@2
    .line 550
    const-string v0, "SET ps 1"

    #@4
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@7
    .line 554
    :goto_7
    return-void

    #@8
    .line 552
    :cond_8
    const-string v0, "SET ps 0"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    goto :goto_7
.end method

.method public setScanInterval(I)V
    .registers 4
    .parameter "scanInterval"

    #@0
    .prologue
    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SCAN_INTERVAL "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    .line 424
    return-void
.end method

.method public setScanMode(Z)Z
    .registers 3
    .parameter "setActive"

    #@0
    .prologue
    .line 126
    if-eqz p1, :cond_9

    #@2
    .line 127
    const-string v0, "DRIVER SCAN-ACTIVE"

    #@4
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    .line 129
    :goto_8
    return v0

    #@9
    :cond_9
    const-string v0, "DRIVER SCAN-PASSIVE"

    #@b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    goto :goto_8
.end method

.method public setScanResultHandling(I)Z
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "AP_SCAN "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setSerialNumber(Ljava/lang/String;)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET serial_number "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setSuspendOptimizations(Z)Z
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 401
    iget-boolean v0, p0, Landroid/net/wifi/WifiNative;->mSuspendOptEnabled:Z

    #@2
    if-ne v0, p1, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 406
    :goto_5
    return v0

    #@6
    .line 402
    :cond_6
    iput-boolean p1, p0, Landroid/net/wifi/WifiNative;->mSuspendOptEnabled:Z

    #@8
    .line 403
    if-eqz p1, :cond_11

    #@a
    .line 404
    const-string v0, "DRIVER SETSUSPENDMODE 1"

    #@c
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    goto :goto_5

    #@11
    .line 406
    :cond_11
    const-string v0, "DRIVER SETSUSPENDMODE 0"

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    goto :goto_5
.end method

.method public setWfdDeviceInfo(Ljava/lang/String;)Z
    .registers 4
    .parameter "hex"

    #@0
    .prologue
    .line 569
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "WFD_SUBELEM_SET 0 "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setWfdEnable(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 565
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SET wifi_display "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    if-eqz p1, :cond_1c

    #@d
    const-string v0, "1"

    #@f
    :goto_f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1a
    move-result v0

    #@1b
    return v0

    #@1c
    :cond_1c
    const-string v0, "0"

    #@1e
    goto :goto_f
.end method

.method public signalPoll()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 433
    const-string v0, "SIGNAL_POLL"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public startDriver()Z
    .registers 2

    #@0
    .prologue
    .line 268
    const-string v0, "DRIVER START"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public startFilteringMulticastV4Packets()Z
    .registers 2

    #@0
    .prologue
    .line 301
    const-string v0, "DRIVER RXFILTER-STOP"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    const-string v0, "DRIVER RXFILTER-REMOVE 2"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1a

    #@10
    const-string v0, "DRIVER RXFILTER-START"

    #@12
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public startFilteringMulticastV6Packets()Z
    .registers 2

    #@0
    .prologue
    .line 321
    const-string v0, "DRIVER RXFILTER-STOP"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    const-string v0, "DRIVER RXFILTER-REMOVE 3"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1a

    #@10
    const-string v0, "DRIVER RXFILTER-START"

    #@12
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public startWpsPbc(Ljava/lang/String;)Z
    .registers 4
    .parameter "bssid"

    #@0
    .prologue
    .line 445
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 446
    const-string v0, "WPS_PBC"

    #@8
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    .line 448
    :goto_c
    return v0

    #@d
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v1, "WPS_PBC "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@23
    move-result v0

    #@24
    goto :goto_c
.end method

.method public startWpsPbc(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "iface"
    .parameter "bssid"

    #@0
    .prologue
    .line 453
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1e

    #@6
    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "WPS_PBC interface="

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1c
    move-result v0

    #@1d
    .line 456
    :goto_1d
    return v0

    #@1e
    :cond_1e
    new-instance v0, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v1, "WPS_PBC interface="

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, " "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@3e
    move-result v0

    #@3f
    goto :goto_1d
.end method

.method public startWpsPinDisplay(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "bssid"

    #@0
    .prologue
    .line 472
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 473
    const-string v0, "WPS_PIN any"

    #@8
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 475
    :goto_c
    return-object v0

    #@d
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v1, "WPS_PIN "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    goto :goto_c
.end method

.method public startWpsPinDisplay(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "iface"
    .parameter "bssid"

    #@0
    .prologue
    .line 480
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_24

    #@6
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "WPS_PIN interface="

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, " any"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 483
    :goto_23
    return-object v0

    #@24
    :cond_24
    new-instance v0, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v1, "WPS_PIN interface="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v1, " "

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    goto :goto_23
.end method

.method public startWpsPinKeypad(Ljava/lang/String;)Z
    .registers 4
    .parameter "pin"

    #@0
    .prologue
    .line 461
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 462
    :goto_7
    return v0

    #@8
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "WPS_PIN any "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@1e
    move-result v0

    #@1f
    goto :goto_7
.end method

.method public startWpsPinKeypad(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "iface"
    .parameter "pin"

    #@0
    .prologue
    .line 466
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 467
    :goto_7
    return v0

    #@8
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "WPS_PIN interface="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " any "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    goto :goto_7
.end method

.method public startWpsRegistrar(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "bssid"
    .parameter "pin"

    #@0
    .prologue
    .line 489
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 490
    :goto_d
    return v0

    #@e
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v1, "WPS_REG "

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@2e
    move-result v0

    #@2f
    goto :goto_d
.end method

.method public status()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 190
    const-string v0, "STATUS"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doStringCommand(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public stopDriver()Z
    .registers 2

    #@0
    .prologue
    .line 272
    const-string v0, "DRIVER STOP"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public stopFilteringMulticastV4Packets()Z
    .registers 2

    #@0
    .prologue
    .line 311
    const-string v0, "DRIVER RXFILTER-STOP"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    const-string v0, "DRIVER RXFILTER-ADD 2"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1a

    #@10
    const-string v0, "DRIVER RXFILTER-START"

    #@12
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public stopFilteringMulticastV6Packets()Z
    .registers 2

    #@0
    .prologue
    .line 331
    const-string v0, "DRIVER RXFILTER-STOP"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    const-string v0, "DRIVER RXFILTER-ADD 3"

    #@a
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1a

    #@10
    const-string v0, "DRIVER RXFILTER-START"

    #@12
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public stopSupplicant()Z
    .registers 2

    #@0
    .prologue
    .line 140
    const-string v0, "TERMINATE"

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->doBooleanCommand(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public waitForEvent()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Landroid/net/wifi/WifiNative;->mInterface:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0}, Landroid/net/wifi/WifiNative;->waitForEvent(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static native setMode(I)Z
.end method