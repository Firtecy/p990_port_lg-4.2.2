.class Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;
.super Lcom/android/internal/util/State;
.source "WifiWatchdogStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiWatchdogStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WatchdogDisabledState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiWatchdogStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiWatchdogStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 499
    iput-object p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 2

    #@0
    .prologue
    .line 502
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    invoke-virtual {p0}, Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;->getName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    invoke-static {v0}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@d
    .line 503
    :cond_d
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 507
    iget v3, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v3, :pswitch_data_4a

    #@6
    .line 527
    :goto_6
    const/4 v2, 0x0

    #@7
    :cond_7
    :goto_7
    return v2

    #@8
    .line 509
    :pswitch_8
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@a
    invoke-static {v3}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$800(Landroid/net/wifi/WifiWatchdogStateMachine;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_7

    #@10
    .line 510
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@12
    iget-object v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@14
    invoke-static {v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$900(Landroid/net/wifi/WifiWatchdogStateMachine;)Landroid/net/wifi/WifiWatchdogStateMachine$NotConnectedState;

    #@17
    move-result-object v4

    #@18
    invoke-static {v3, v4}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1000(Landroid/net/wifi/WifiWatchdogStateMachine;Lcom/android/internal/util/IState;)V

    #@1b
    goto :goto_7

    #@1c
    .line 513
    :pswitch_1c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e
    check-cast v0, Landroid/content/Intent;

    #@20
    .line 514
    .local v0, intent:Landroid/content/Intent;
    const-string/jumbo v3, "networkInfo"

    #@23
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/net/NetworkInfo;

    #@29
    .line 517
    .local v1, networkInfo:Landroid/net/NetworkInfo;
    sget-object v3, Landroid/net/wifi/WifiWatchdogStateMachine$4;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    #@2b
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    #@32
    move-result v4

    #@33
    aget v3, v3, v4

    #@35
    packed-switch v3, :pswitch_data_52

    #@38
    goto :goto_6

    #@39
    .line 519
    :pswitch_39
    invoke-static {}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$000()Z

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_44

    #@3f
    const-string v3, "Watchdog disabled, verify link"

    #@41
    invoke-static {v3}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$100(Ljava/lang/String;)V

    #@44
    .line 520
    :cond_44
    iget-object v3, p0, Landroid/net/wifi/WifiWatchdogStateMachine$WatchdogDisabledState;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@46
    invoke-static {v3, v2}, Landroid/net/wifi/WifiWatchdogStateMachine;->access$1100(Landroid/net/wifi/WifiWatchdogStateMachine;Z)V

    #@49
    goto :goto_6

    #@4a
    .line 507
    :pswitch_data_4a
    .packed-switch 0x21001
        :pswitch_8
        :pswitch_1c
    .end packed-switch

    #@52
    .line 517
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_39
    .end packed-switch
.end method
