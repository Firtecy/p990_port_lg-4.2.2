.class Landroid/net/wifi/WifiMonitor$MonitorThread;
.super Ljava/lang/Thread;
.source "WifiMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MonitorThread"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiMonitor;


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiMonitor;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 437
    iput-object p1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@2
    .line 438
    const-string v0, "WifiMonitor"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 439
    return-void
.end method

.method private connectToSupplicant()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 586
    const/4 v0, 0x0

    #@2
    .line 589
    .local v0, connectTries:I
    :goto_2
    iget-object v3, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@4
    invoke-static {v3}, Landroid/net/wifi/WifiMonitor;->access$100(Landroid/net/wifi/WifiMonitor;)Landroid/net/wifi/WifiNative;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->connectToSupplicant()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_f

    #@e
    .line 598
    :goto_e
    return v2

    #@f
    .line 592
    :cond_f
    add-int/lit8 v1, v0, 0x1

    #@11
    .end local v0           #connectTries:I
    .local v1, connectTries:I
    const/4 v3, 0x5

    #@12
    if-ge v0, v3, :cond_19

    #@14
    .line 593
    invoke-static {v2}, Landroid/net/wifi/WifiMonitor;->access$600(I)V

    #@17
    move v0, v1

    #@18
    .end local v1           #connectTries:I
    .restart local v0       #connectTries:I
    goto :goto_2

    #@19
    .line 598
    .end local v0           #connectTries:I
    .restart local v1       #connectTries:I
    :cond_19
    const/4 v2, 0x0

    #@1a
    move v0, v1

    #@1b
    .end local v1           #connectTries:I
    .restart local v0       #connectTries:I
    goto :goto_e
.end method

.method private handleDriverEvent(Ljava/lang/String;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 602
    if-nez p1, :cond_3

    #@2
    .line 608
    :cond_2
    :goto_2
    return-void

    #@3
    .line 605
    :cond_3
    const-string v0, "HANGED"

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_2

    #@b
    .line 606
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@d
    invoke-static {v0}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@10
    move-result-object v0

    #@11
    const v1, 0x2400c

    #@14
    invoke-virtual {v0, v1}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@17
    goto :goto_2
.end method

.method private handleHostApEvents(Ljava/lang/String;)V
    .registers 7
    .parameter "dataString"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 879
    const-string v1, " "

    #@4
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 881
    .local v0, tokens:[Ljava/lang/String;
    aget-object v1, v0, v4

    #@a
    const-string v2, "AP-STA-CONNECTED"

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_47

    #@12
    .line 883
    const-string v1, " p2p_dev_addr="

    #@14
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@17
    move-result v1

    #@18
    if-le v1, v3, :cond_35

    #@1a
    const-string/jumbo v1, "p2p_dev_addr=00:00:00:00:00:00"

    #@1d
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@20
    move-result v1

    #@21
    if-ne v1, v3, :cond_35

    #@23
    .line 884
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@25
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@28
    move-result-object v1

    #@29
    const v2, 0x2402b

    #@2c
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2e
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@31
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@34
    .line 892
    :cond_34
    :goto_34
    return-void

    #@35
    .line 887
    :cond_35
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@37
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@3a
    move-result-object v1

    #@3b
    const v2, 0x2402a

    #@3e
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@40
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@43
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@46
    goto :goto_34

    #@47
    .line 889
    :cond_47
    aget-object v1, v0, v4

    #@49
    const-string v2, "AP-STA-DISCONNECTED"

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_34

    #@51
    .line 890
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@53
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@56
    move-result-object v1

    #@57
    const v2, 0x24029

    #@5a
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@5c
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@5f
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@62
    goto :goto_34
.end method

.method private handleP2pEvents(Ljava/lang/String;)V
    .registers 6
    .parameter "dataString"

    #@0
    .prologue
    .line 825
    const-string v1, "P2P-DEVICE-FOUND"

    #@2
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_1a

    #@8
    .line 826
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@a
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@d
    move-result-object v1

    #@e
    const v2, 0x24015

    #@11
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@13
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@16
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@19
    .line 873
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 827
    :cond_1a
    const-string v1, "P2P-DEVICE-LOST"

    #@1c
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_34

    #@22
    .line 828
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@24
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@27
    move-result-object v1

    #@28
    const v2, 0x24016

    #@2b
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@2d
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>(Ljava/lang/String;)V

    #@30
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@33
    goto :goto_19

    #@34
    .line 829
    :cond_34
    const-string v1, "P2P-FIND-STOPPED"

    #@36
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@39
    move-result v1

    #@3a
    if-eqz v1, :cond_49

    #@3c
    .line 830
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@3e
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@41
    move-result-object v1

    #@42
    const v2, 0x24025

    #@45
    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@48
    goto :goto_19

    #@49
    .line 831
    :cond_49
    const-string v1, "P2P-GO-NEG-REQUEST"

    #@4b
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_63

    #@51
    .line 832
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@53
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@56
    move-result-object v1

    #@57
    const v2, 0x24017

    #@5a
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pConfig;

    #@5c
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>(Ljava/lang/String;)V

    #@5f
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@62
    goto :goto_19

    #@63
    .line 834
    :cond_63
    const-string v1, "P2P-GO-NEG-SUCCESS"

    #@65
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@68
    move-result v1

    #@69
    if-eqz v1, :cond_78

    #@6b
    .line 835
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@6d
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@70
    move-result-object v1

    #@71
    const v2, 0x24019

    #@74
    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@77
    goto :goto_19

    #@78
    .line 836
    :cond_78
    const-string v1, "P2P-GO-NEG-FAILURE"

    #@7a
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7d
    move-result v1

    #@7e
    if-eqz v1, :cond_91

    #@80
    .line 837
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@82
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@85
    move-result-object v1

    #@86
    const v2, 0x2401a

    #@89
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiMonitor$MonitorThread;->p2pError(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@90
    goto :goto_19

    #@91
    .line 838
    :cond_91
    const-string v1, "P2P-GROUP-FORMATION-SUCCESS"

    #@93
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@96
    move-result v1

    #@97
    if-eqz v1, :cond_a7

    #@99
    .line 839
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@9b
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@9e
    move-result-object v1

    #@9f
    const v2, 0x2401b

    #@a2
    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@a5
    goto/16 :goto_19

    #@a7
    .line 840
    :cond_a7
    const-string v1, "P2P-GROUP-FORMATION-FAILURE"

    #@a9
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@ac
    move-result v1

    #@ad
    if-eqz v1, :cond_c1

    #@af
    .line 841
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@b1
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@b4
    move-result-object v1

    #@b5
    const v2, 0x2401c

    #@b8
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiMonitor$MonitorThread;->p2pError(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@bb
    move-result-object v3

    #@bc
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@bf
    goto/16 :goto_19

    #@c1
    .line 842
    :cond_c1
    const-string v1, "P2P-GROUP-STARTED"

    #@c3
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c6
    move-result v1

    #@c7
    if-eqz v1, :cond_dc

    #@c9
    .line 843
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@cb
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@ce
    move-result-object v1

    #@cf
    const v2, 0x2401d

    #@d2
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@d4
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pGroup;-><init>(Ljava/lang/String;)V

    #@d7
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@da
    goto/16 :goto_19

    #@dc
    .line 844
    :cond_dc
    const-string v1, "P2P-GROUP-REMOVED"

    #@de
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e1
    move-result v1

    #@e2
    if-eqz v1, :cond_f7

    #@e4
    .line 845
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@e6
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@e9
    move-result-object v1

    #@ea
    const v2, 0x2401e

    #@ed
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@ef
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pGroup;-><init>(Ljava/lang/String;)V

    #@f2
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@f5
    goto/16 :goto_19

    #@f7
    .line 846
    :cond_f7
    const-string v1, "P2P-INVITATION-RECEIVED"

    #@f9
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@fc
    move-result v1

    #@fd
    if-eqz v1, :cond_112

    #@ff
    .line 847
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@101
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@104
    move-result-object v1

    #@105
    const v2, 0x2401f

    #@108
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pGroup;

    #@10a
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pGroup;-><init>(Ljava/lang/String;)V

    #@10d
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@110
    goto/16 :goto_19

    #@112
    .line 849
    :cond_112
    const-string v1, "P2P-INVITATION-RESULT"

    #@114
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@117
    move-result v1

    #@118
    if-eqz v1, :cond_12c

    #@11a
    .line 850
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@11c
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@11f
    move-result-object v1

    #@120
    const v2, 0x24020

    #@123
    invoke-direct {p0, p1}, Landroid/net/wifi/WifiMonitor$MonitorThread;->p2pError(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@126
    move-result-object v3

    #@127
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@12a
    goto/16 :goto_19

    #@12c
    .line 851
    :cond_12c
    const-string v1, "P2P-PROV-DISC-PBC-REQ"

    #@12e
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@131
    move-result v1

    #@132
    if-eqz v1, :cond_147

    #@134
    .line 852
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@136
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@139
    move-result-object v1

    #@13a
    const v2, 0x24021

    #@13d
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;

    #@13f
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;-><init>(Ljava/lang/String;)V

    #@142
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@145
    goto/16 :goto_19

    #@147
    .line 854
    :cond_147
    const-string v1, "P2P-PROV-DISC-PBC-RESP"

    #@149
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@14c
    move-result v1

    #@14d
    if-eqz v1, :cond_162

    #@14f
    .line 855
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@151
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@154
    move-result-object v1

    #@155
    const v2, 0x24022

    #@158
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;

    #@15a
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;-><init>(Ljava/lang/String;)V

    #@15d
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@160
    goto/16 :goto_19

    #@162
    .line 857
    :cond_162
    const-string v1, "P2P-PROV-DISC-ENTER-PIN"

    #@164
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@167
    move-result v1

    #@168
    if-eqz v1, :cond_17d

    #@16a
    .line 858
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@16c
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@16f
    move-result-object v1

    #@170
    const v2, 0x24023

    #@173
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;

    #@175
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;-><init>(Ljava/lang/String;)V

    #@178
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@17b
    goto/16 :goto_19

    #@17d
    .line 860
    :cond_17d
    const-string v1, "P2P-PROV-DISC-SHOW-PIN"

    #@17f
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@182
    move-result v1

    #@183
    if-eqz v1, :cond_198

    #@185
    .line 861
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@187
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@18a
    move-result-object v1

    #@18b
    const v2, 0x24024

    #@18e
    new-instance v3, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;

    #@190
    invoke-direct {v3, p1}, Landroid/net/wifi/p2p/WifiP2pProvDiscEvent;-><init>(Ljava/lang/String;)V

    #@193
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@196
    goto/16 :goto_19

    #@198
    .line 863
    :cond_198
    const-string v1, "P2P-PROV-DISC-FAILURE"

    #@19a
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@19d
    move-result v1

    #@19e
    if-eqz v1, :cond_1ae

    #@1a0
    .line 864
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1a2
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@1a5
    move-result-object v1

    #@1a6
    const v2, 0x24027

    #@1a9
    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@1ac
    goto/16 :goto_19

    #@1ae
    .line 865
    :cond_1ae
    const-string v1, "P2P-SERV-DISC-RESP"

    #@1b0
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1b3
    move-result v1

    #@1b4
    if-eqz v1, :cond_19

    #@1b6
    .line 866
    invoke-static {p1}, Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;->newInstance(Ljava/lang/String;)Ljava/util/List;

    #@1b9
    move-result-object v0

    #@1ba
    .line 867
    .local v0, list:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/p2p/nsd/WifiP2pServiceResponse;>;"
    if-eqz v0, :cond_1ca

    #@1bc
    .line 868
    iget-object v1, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1be
    invoke-static {v1}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@1c1
    move-result-object v1

    #@1c2
    const v2, 0x24026

    #@1c5
    invoke-virtual {v1, v2, v0}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@1c8
    goto/16 :goto_19

    #@1ca
    .line 870
    :cond_1ca
    const-string v1, "WifiMonitor"

    #@1cc
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ce
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d1
    const-string v3, "Null service resp "

    #@1d3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v2

    #@1d7
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v2

    #@1db
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1de
    move-result-object v2

    #@1df
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e2
    goto/16 :goto_19
.end method

.method private handleSupplicantStateChange(Ljava/lang/String;)V
    .registers 22
    .parameter "dataString"

    #@0
    .prologue
    .line 900
    const/16 v16, 0x0

    #@2
    .line 901
    .local v16, wifiSsid:Landroid/net/wifi/WifiSsid;
    const-string v17, "SSID="

    #@4
    move-object/from16 v0, p1

    #@6
    move-object/from16 v1, v17

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@b
    move-result v7

    #@c
    .line 902
    .local v7, index:I
    const/16 v17, -0x1

    #@e
    move/from16 v0, v17

    #@10
    if-eq v7, v0, :cond_20

    #@12
    .line 903
    add-int/lit8 v17, v7, 0x5

    #@14
    move-object/from16 v0, p1

    #@16
    move/from16 v1, v17

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1b
    move-result-object v17

    #@1c
    invoke-static/range {v17 .. v17}, Landroid/net/wifi/WifiSsid;->createFromAsciiEncoded(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;

    #@1f
    move-result-object v16

    #@20
    .line 906
    :cond_20
    const-string v17, " "

    #@22
    move-object/from16 v0, p1

    #@24
    move-object/from16 v1, v17

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    .line 908
    .local v4, dataTokens:[Ljava/lang/String;
    const/4 v2, 0x0

    #@2b
    .line 909
    .local v2, BSSID:Ljava/lang/String;
    const/4 v10, -0x1

    #@2c
    .line 910
    .local v10, networkId:I
    const/4 v11, -0x1

    #@2d
    .line 911
    .local v11, newState:I
    move-object v3, v4

    #@2e
    .local v3, arr$:[Ljava/lang/String;
    array-length v8, v3

    #@2f
    .local v8, len$:I
    const/4 v6, 0x0

    #@30
    .local v6, i$:I
    :goto_30
    if-ge v6, v8, :cond_82

    #@32
    aget-object v14, v3, v6

    #@34
    .line 912
    .local v14, token:Ljava/lang/String;
    const-string v17, "="

    #@36
    move-object/from16 v0, v17

    #@38
    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3b
    move-result-object v9

    #@3c
    .line 913
    .local v9, nameValue:[Ljava/lang/String;
    array-length v0, v9

    #@3d
    move/from16 v17, v0

    #@3f
    const/16 v18, 0x2

    #@41
    move/from16 v0, v17

    #@43
    move/from16 v1, v18

    #@45
    if-eq v0, v1, :cond_4a

    #@47
    .line 911
    :cond_47
    :goto_47
    add-int/lit8 v6, v6, 0x1

    #@49
    goto :goto_30

    #@4a
    .line 917
    :cond_4a
    const/16 v17, 0x0

    #@4c
    aget-object v17, v9, v17

    #@4e
    const-string v18, "BSSID"

    #@50
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v17

    #@54
    if-eqz v17, :cond_5b

    #@56
    .line 918
    const/16 v17, 0x1

    #@58
    aget-object v2, v9, v17

    #@5a
    .line 919
    goto :goto_47

    #@5b
    .line 924
    :cond_5b
    const/16 v17, 0x1

    #@5d
    :try_start_5d
    aget-object v17, v9, v17

    #@5f
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_62
    .catch Ljava/lang/NumberFormatException; {:try_start_5d .. :try_end_62} :catch_71

    #@62
    move-result v15

    #@63
    .line 929
    .local v15, value:I
    const/16 v17, 0x0

    #@65
    aget-object v17, v9, v17

    #@67
    const-string v18, "id"

    #@69
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v17

    #@6d
    if-eqz v17, :cond_73

    #@6f
    .line 930
    move v10, v15

    #@70
    goto :goto_47

    #@71
    .line 925
    .end local v15           #value:I
    :catch_71
    move-exception v5

    #@72
    .line 926
    .local v5, e:Ljava/lang/NumberFormatException;
    goto :goto_47

    #@73
    .line 931
    .end local v5           #e:Ljava/lang/NumberFormatException;
    .restart local v15       #value:I
    :cond_73
    const/16 v17, 0x0

    #@75
    aget-object v17, v9, v17

    #@77
    const-string/jumbo v18, "state"

    #@7a
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v17

    #@7e
    if-eqz v17, :cond_47

    #@80
    .line 932
    move v11, v15

    #@81
    goto :goto_47

    #@82
    .line 936
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v14           #token:Ljava/lang/String;
    .end local v15           #value:I
    :cond_82
    const/16 v17, -0x1

    #@84
    move/from16 v0, v17

    #@86
    if-ne v11, v0, :cond_89

    #@88
    .line 949
    .end local v3           #arr$:[Ljava/lang/String;
    :goto_88
    return-void

    #@89
    .line 938
    .restart local v3       #arr$:[Ljava/lang/String;
    :cond_89
    sget-object v12, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@8b
    .line 939
    .local v12, newSupplicantState:Landroid/net/wifi/SupplicantState;
    invoke-static {}, Landroid/net/wifi/SupplicantState;->values()[Landroid/net/wifi/SupplicantState;

    #@8e
    move-result-object v3

    #@8f
    .local v3, arr$:[Landroid/net/wifi/SupplicantState;
    array-length v8, v3

    #@90
    const/4 v6, 0x0

    #@91
    :goto_91
    if-ge v6, v8, :cond_9e

    #@93
    aget-object v13, v3, v6

    #@95
    .line 940
    .local v13, state:Landroid/net/wifi/SupplicantState;
    invoke-virtual {v13}, Landroid/net/wifi/SupplicantState;->ordinal()I

    #@98
    move-result v17

    #@99
    move/from16 v0, v17

    #@9b
    if-ne v0, v11, :cond_cc

    #@9d
    .line 941
    move-object v12, v13

    #@9e
    .line 945
    .end local v13           #state:Landroid/net/wifi/SupplicantState;
    :cond_9e
    sget-object v17, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@a0
    move-object/from16 v0, v17

    #@a2
    if-ne v12, v0, :cond_be

    #@a4
    .line 946
    const-string v17, "WifiMonitor"

    #@a6
    new-instance v18, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v19, "Invalid supplicant state: "

    #@ad
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v18

    #@b1
    move-object/from16 v0, v18

    #@b3
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v18

    #@b7
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v18

    #@bb
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@be
    .line 948
    :cond_be
    move-object/from16 v0, p0

    #@c0
    iget-object v0, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@c2
    move-object/from16 v17, v0

    #@c4
    move-object/from16 v0, v17

    #@c6
    move-object/from16 v1, v16

    #@c8
    invoke-virtual {v0, v10, v1, v2, v12}, Landroid/net/wifi/WifiMonitor;->notifySupplicantStateChange(ILandroid/net/wifi/WifiSsid;Ljava/lang/String;Landroid/net/wifi/SupplicantState;)V

    #@cb
    goto :goto_88

    #@cc
    .line 939
    .restart local v13       #state:Landroid/net/wifi/SupplicantState;
    :cond_cc
    add-int/lit8 v6, v6, 0x1

    #@ce
    goto :goto_91
.end method

.method private handleWpsFailEvent(Ljava/lang/String;)V
    .registers 11
    .parameter "dataString"

    #@0
    .prologue
    const v8, 0x24009

    #@3
    const/4 v7, 0x0

    #@4
    .line 765
    const-string v4, "WPS-FAIL msg=\\d+(?: config_error=(\\d+))?(?: reason=(\\d+))?"

    #@6
    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@9
    move-result-object v2

    #@a
    .line 766
    .local v2, p:Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@d
    move-result-object v1

    #@e
    .line 767
    .local v1, match:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_30

    #@14
    .line 768
    const/4 v4, 0x1

    #@15
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 769
    .local v0, cfgErr:Ljava/lang/String;
    const/4 v4, 0x2

    #@1a
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 771
    .local v3, reason:Ljava/lang/String;
    if-eqz v3, :cond_27

    #@20
    .line 772
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23
    move-result v4

    #@24
    packed-switch v4, :pswitch_data_98

    #@27
    .line 783
    :cond_27
    if-eqz v0, :cond_30

    #@29
    .line 784
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2c
    move-result v4

    #@2d
    sparse-switch v4, :sswitch_data_a0

    #@30
    .line 797
    .end local v0           #cfgErr:Ljava/lang/String;
    .end local v3           #reason:Ljava/lang/String;
    :cond_30
    iget-object v4, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@32
    invoke-static {v4}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@35
    move-result-object v4

    #@36
    iget-object v5, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@38
    invoke-static {v5}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5, v8, v7, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v4, v5}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@43
    .line 799
    :goto_43
    return-void

    #@44
    .line 774
    .restart local v0       #cfgErr:Ljava/lang/String;
    .restart local v3       #reason:Ljava/lang/String;
    :pswitch_44
    iget-object v4, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@46
    invoke-static {v4}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@49
    move-result-object v4

    #@4a
    iget-object v5, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@4c
    invoke-static {v5}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@4f
    move-result-object v5

    #@50
    const/4 v6, 0x5

    #@51
    invoke-virtual {v5, v8, v6, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v4, v5}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@58
    goto :goto_43

    #@59
    .line 778
    :pswitch_59
    iget-object v4, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@5b
    invoke-static {v4}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@5e
    move-result-object v4

    #@5f
    iget-object v5, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@61
    invoke-static {v5}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@64
    move-result-object v5

    #@65
    const/4 v6, 0x4

    #@66
    invoke-virtual {v5, v8, v6, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v4, v5}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@6d
    goto :goto_43

    #@6e
    .line 786
    :sswitch_6e
    iget-object v4, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@70
    invoke-static {v4}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@73
    move-result-object v4

    #@74
    iget-object v5, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@76
    invoke-static {v5}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@79
    move-result-object v5

    #@7a
    const/4 v6, 0x6

    #@7b
    invoke-virtual {v5, v8, v6, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v4, v5}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@82
    goto :goto_43

    #@83
    .line 790
    :sswitch_83
    iget-object v4, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@85
    invoke-static {v4}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@88
    move-result-object v4

    #@89
    iget-object v5, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@8b
    invoke-static {v5}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@8e
    move-result-object v5

    #@8f
    const/4 v6, 0x3

    #@90
    invoke-virtual {v5, v8, v6, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {v4, v5}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@97
    goto :goto_43

    #@98
    .line 772
    :pswitch_data_98
    .packed-switch 0x1
        :pswitch_44
        :pswitch_59
    .end packed-switch

    #@a0
    .line 784
    :sswitch_data_a0
    .sparse-switch
        0xc -> :sswitch_83
        0x12 -> :sswitch_6e
    .end sparse-switch
.end method

.method private p2pError(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    .registers 10
    .parameter "dataString"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 803
    sget-object v1, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->UNKNOWN:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@4
    .line 804
    .local v1, err:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    const-string v4, " "

    #@6
    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 805
    .local v3, tokens:[Ljava/lang/String;
    array-length v4, v3

    #@b
    if-ge v4, v7, :cond_f

    #@d
    move-object v4, v1

    #@e
    .line 818
    :goto_e
    return-object v4

    #@f
    .line 806
    :cond_f
    aget-object v4, v3, v6

    #@11
    const-string v5, "="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    .line 807
    .local v2, nameValue:[Ljava/lang/String;
    array-length v4, v2

    #@18
    if-eq v4, v7, :cond_1c

    #@1a
    move-object v4, v1

    #@1b
    goto :goto_e

    #@1c
    .line 810
    :cond_1c
    aget-object v4, v2, v6

    #@1e
    const-string v5, "FREQ_CONFLICT"

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_29

    #@26
    .line 811
    sget-object v4, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->NO_COMMON_CHANNEL:Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;

    #@28
    goto :goto_e

    #@29
    .line 814
    :cond_29
    const/4 v4, 0x1

    #@2a
    :try_start_2a
    aget-object v4, v2, v4

    #@2c
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2f
    move-result v4

    #@30
    invoke-static {v4}, Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;->valueOf(I)Landroid/net/wifi/p2p/WifiP2pService$P2pStatus;
    :try_end_33
    .catch Ljava/lang/NumberFormatException; {:try_start_2a .. :try_end_33} :catch_36

    #@33
    move-result-object v1

    #@34
    :goto_34
    move-object v4, v1

    #@35
    .line 818
    goto :goto_e

    #@36
    .line 815
    :catch_36
    move-exception v0

    #@37
    .line 816
    .local v0, e:Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    #@3a
    goto :goto_34
.end method


# virtual methods
.method handleEvent(ILjava/lang/String;)V
    .registers 5
    .parameter "event"
    .parameter "remainder"

    #@0
    .prologue
    .line 746
    packed-switch p1, :pswitch_data_22

    #@3
    .line 762
    :goto_3
    :pswitch_3
    return-void

    #@4
    .line 748
    :pswitch_4
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@6
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@8
    invoke-static {v0, v1, p2}, Landroid/net/wifi/WifiMonitor;->access$700(Landroid/net/wifi/WifiMonitor;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@b
    goto :goto_3

    #@c
    .line 752
    :pswitch_c
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@e
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@10
    invoke-static {v0, v1, p2}, Landroid/net/wifi/WifiMonitor;->access$700(Landroid/net/wifi/WifiMonitor;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@13
    goto :goto_3

    #@14
    .line 756
    :pswitch_14
    iget-object v0, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@16
    invoke-static {v0}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@19
    move-result-object v0

    #@1a
    const v1, 0x24005

    #@1d
    invoke-virtual {v0, v1}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@20
    goto :goto_3

    #@21
    .line 746
    nop

    #@22
    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_c
        :pswitch_4
        :pswitch_3
        :pswitch_14
    .end packed-switch
.end method

.method handleInterworkingEvent(Ljava/lang/String;)V
    .registers 18
    .parameter "dataString"

    #@0
    .prologue
    .line 613
    const-string v13, " "

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7
    move-result-object v4

    #@8
    .line 614
    .local v4, dataTokens:[Ljava/lang/String;
    const/4 v1, 0x0

    #@9
    .line 615
    .local v1, BSSID:Ljava/lang/String;
    const/4 v2, 0x0

    #@a
    .line 616
    .local v2, SSID:Ljava/lang/String;
    move-object v3, v4

    #@b
    .local v3, arr$:[Ljava/lang/String;
    array-length v7, v3

    #@c
    .local v7, len$:I
    const/4 v6, 0x0

    #@d
    .local v6, i$:I
    :goto_d
    if-ge v6, v7, :cond_38

    #@f
    aget-object v11, v3, v6

    #@11
    .line 617
    .local v11, token:Ljava/lang/String;
    const-string v13, "="

    #@13
    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    .line 619
    .local v9, nameValue:[Ljava/lang/String;
    const-string/jumbo v13, "ssid"

    #@1a
    const/4 v14, 0x0

    #@1b
    aget-object v14, v9, v14

    #@1d
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v13

    #@21
    if-eqz v13, :cond_29

    #@23
    .line 620
    const/4 v13, 0x1

    #@24
    aget-object v2, v9, v13

    #@26
    .line 616
    :cond_26
    :goto_26
    add-int/lit8 v6, v6, 0x1

    #@28
    goto :goto_d

    #@29
    .line 624
    :cond_29
    const-string v13, "bssid"

    #@2b
    const/4 v14, 0x0

    #@2c
    aget-object v14, v9, v14

    #@2e
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v13

    #@32
    if-eqz v13, :cond_26

    #@34
    .line 625
    const/4 v13, 0x1

    #@35
    aget-object v1, v9, v13

    #@37
    .line 626
    goto :goto_26

    #@38
    .line 630
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_38
    const-string v13, "INTERWORKING-AP"

    #@3a
    move-object/from16 v0, p1

    #@3c
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3f
    move-result v13

    #@40
    if-eqz v13, :cond_d5

    #@42
    .line 633
    const/4 v10, 0x0

    #@43
    .line 634
    .local v10, roamingInd:I
    const/4 v12, 0x0

    #@44
    .line 636
    .local v12, typeInterworking:Ljava/lang/String;
    move-object v3, v4

    #@45
    array-length v7, v3

    #@46
    const/4 v6, 0x0

    #@47
    :goto_47
    if-ge v6, v7, :cond_60

    #@49
    aget-object v11, v3, v6

    #@4b
    .line 637
    .restart local v11       #token:Ljava/lang/String;
    const-string v13, "="

    #@4d
    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@50
    move-result-object v9

    #@51
    .line 639
    .restart local v9       #nameValue:[Ljava/lang/String;
    const-string/jumbo v13, "type"

    #@54
    const/4 v14, 0x0

    #@55
    aget-object v14, v9, v14

    #@57
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v13

    #@5b
    if-eqz v13, :cond_c0

    #@5d
    .line 640
    const/4 v13, 0x1

    #@5e
    aget-object v12, v9, v13

    #@60
    .line 645
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_60
    const-string v13, "home"

    #@62
    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v13

    #@66
    if-eqz v13, :cond_c3

    #@68
    .line 646
    const/4 v10, 0x1

    #@69
    .line 647
    const-string v13, "WifiMonitor"

    #@6b
    const-string v14, "[PASSPOINT] \'home\' networking connected"

    #@6d
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 653
    :cond_70
    :goto_70
    move-object/from16 v0, p0

    #@72
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@74
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@77
    move-result-object v13

    #@78
    const v14, 0x2402c

    #@7b
    const/4 v15, 0x0

    #@7c
    invoke-virtual {v13, v14, v10, v15, v2}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@7f
    move-result-object v8

    #@80
    .line 655
    .local v8, m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@82
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@84
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@87
    move-result-object v13

    #@88
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@8b
    .line 656
    const-string v13, "WifiMonitor"

    #@8d
    new-instance v14, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v15, "[PASSPOINT]  bssid ="

    #@94
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v14

    #@98
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v14

    #@9c
    const-string v15, "SSID ="

    #@9e
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v14

    #@a2
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v14

    #@a6
    const-string v15, " data="

    #@a8
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v14

    #@ac
    move-object/from16 v0, p1

    #@ae
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v14

    #@b2
    const-string v15, ""

    #@b4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v14

    #@b8
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v14

    #@bc
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 735
    .end local v8           #m:Landroid/os/Message;
    .end local v10           #roamingInd:I
    .end local v12           #typeInterworking:Ljava/lang/String;
    :cond_bf
    :goto_bf
    return-void

    #@c0
    .line 636
    .restart local v9       #nameValue:[Ljava/lang/String;
    .restart local v10       #roamingInd:I
    .restart local v11       #token:Ljava/lang/String;
    .restart local v12       #typeInterworking:Ljava/lang/String;
    :cond_c0
    add-int/lit8 v6, v6, 0x1

    #@c2
    goto :goto_47

    #@c3
    .line 648
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_c3
    const-string/jumbo v13, "roaming"

    #@c6
    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v13

    #@ca
    if-eqz v13, :cond_70

    #@cc
    .line 649
    const-string v13, "WifiMonitor"

    #@ce
    const-string v14, "[PASSPOINT] \'roaming\' networking connected"

    #@d0
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d3
    .line 650
    const/4 v10, 0x2

    #@d4
    goto :goto_70

    #@d5
    .line 657
    .end local v10           #roamingInd:I
    .end local v12           #typeInterworking:Ljava/lang/String;
    :cond_d5
    const-string v13, "INTERWORKING-NO-MATCH"

    #@d7
    move-object/from16 v0, p1

    #@d9
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@dc
    move-result v13

    #@dd
    if-eqz v13, :cond_12e

    #@df
    .line 662
    move-object/from16 v0, p0

    #@e1
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@e3
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@e6
    move-result-object v13

    #@e7
    const v14, 0x2402d

    #@ea
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@ed
    move-result-object v8

    #@ee
    .line 663
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@f0
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@f2
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@f5
    move-result-object v13

    #@f6
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@f9
    .line 664
    const-string v13, "WifiMonitor"

    #@fb
    new-instance v14, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v15, "[PASSPOINT]  bssid ="

    #@102
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v14

    #@106
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v14

    #@10a
    const-string v15, "SSID ="

    #@10c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v14

    #@110
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v14

    #@114
    const-string v15, " data="

    #@116
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v14

    #@11a
    move-object/from16 v0, p1

    #@11c
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v14

    #@120
    const-string v15, ""

    #@122
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v14

    #@126
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@129
    move-result-object v14

    #@12a
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    goto :goto_bf

    #@12e
    .line 665
    .end local v8           #m:Landroid/os/Message;
    :cond_12e
    const-string v13, "INTERWORKING-GAS-RESP-INFO"

    #@130
    move-object/from16 v0, p1

    #@132
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@135
    move-result v13

    #@136
    if-eqz v13, :cond_188

    #@138
    .line 668
    move-object/from16 v0, p0

    #@13a
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@13c
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@13f
    move-result-object v13

    #@140
    const v14, 0x2402e

    #@143
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@146
    move-result-object v8

    #@147
    .line 669
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@149
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@14b
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@14e
    move-result-object v13

    #@14f
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@152
    .line 670
    const-string v13, "WifiMonitor"

    #@154
    new-instance v14, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v15, "[PASSPOINT]  bssid ="

    #@15b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v14

    #@15f
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v14

    #@163
    const-string v15, "SSID ="

    #@165
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v14

    #@169
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v14

    #@16d
    const-string v15, " data="

    #@16f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v14

    #@173
    move-object/from16 v0, p1

    #@175
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v14

    #@179
    const-string v15, ""

    #@17b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v14

    #@17f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v14

    #@183
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@186
    goto/16 :goto_bf

    #@188
    .line 671
    .end local v8           #m:Landroid/os/Message;
    :cond_188
    const-string v13, "INTERWORKING-ANQP-FETCH-STARTED"

    #@18a
    move-object/from16 v0, p1

    #@18c
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@18f
    move-result v13

    #@190
    if-eqz v13, :cond_1e2

    #@192
    .line 674
    move-object/from16 v0, p0

    #@194
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@196
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@199
    move-result-object v13

    #@19a
    const v14, 0x2402f

    #@19d
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1a0
    move-result-object v8

    #@1a1
    .line 675
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@1a3
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1a5
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@1a8
    move-result-object v13

    #@1a9
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@1ac
    .line 676
    const-string v13, "WifiMonitor"

    #@1ae
    new-instance v14, Ljava/lang/StringBuilder;

    #@1b0
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1b3
    const-string v15, "[PASSPOINT]  bssid ="

    #@1b5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v14

    #@1b9
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v14

    #@1bd
    const-string v15, "SSID ="

    #@1bf
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v14

    #@1c3
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v14

    #@1c7
    const-string v15, " data="

    #@1c9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v14

    #@1cd
    move-object/from16 v0, p1

    #@1cf
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v14

    #@1d3
    const-string v15, ""

    #@1d5
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v14

    #@1d9
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v14

    #@1dd
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e0
    goto/16 :goto_bf

    #@1e2
    .line 677
    .end local v8           #m:Landroid/os/Message;
    :cond_1e2
    const-string v13, "INTERWORKING-ANQP-FETCH-COMPLETED"

    #@1e4
    move-object/from16 v0, p1

    #@1e6
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1e9
    move-result v13

    #@1ea
    if-eqz v13, :cond_23c

    #@1ec
    .line 680
    move-object/from16 v0, p0

    #@1ee
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1f0
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@1f3
    move-result-object v13

    #@1f4
    const v14, 0x24030

    #@1f7
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1fa
    move-result-object v8

    #@1fb
    .line 681
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@1fd
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1ff
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@202
    move-result-object v13

    #@203
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@206
    .line 682
    const-string v13, "WifiMonitor"

    #@208
    new-instance v14, Ljava/lang/StringBuilder;

    #@20a
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@20d
    const-string v15, "[PASSPOINT]  bssid ="

    #@20f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v14

    #@213
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v14

    #@217
    const-string v15, "SSID ="

    #@219
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v14

    #@21d
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v14

    #@221
    const-string v15, " data="

    #@223
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v14

    #@227
    move-object/from16 v0, p1

    #@229
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v14

    #@22d
    const-string v15, ""

    #@22f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@232
    move-result-object v14

    #@233
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@236
    move-result-object v14

    #@237
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23a
    goto/16 :goto_bf

    #@23c
    .line 683
    .end local v8           #m:Landroid/os/Message;
    :cond_23c
    const-string v13, "INTERWORKING-ANQP-RX-DATA"

    #@23e
    move-object/from16 v0, p1

    #@240
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@243
    move-result v13

    #@244
    if-eqz v13, :cond_296

    #@246
    .line 685
    move-object/from16 v0, p0

    #@248
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@24a
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@24d
    move-result-object v13

    #@24e
    const v14, 0x24031

    #@251
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@254
    move-result-object v8

    #@255
    .line 686
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@257
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@259
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@25c
    move-result-object v13

    #@25d
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@260
    .line 687
    const-string v13, "WifiMonitor"

    #@262
    new-instance v14, Ljava/lang/StringBuilder;

    #@264
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@267
    const-string v15, "[PASSPOINT]  bssid ="

    #@269
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26c
    move-result-object v14

    #@26d
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@270
    move-result-object v14

    #@271
    const-string v15, "SSID ="

    #@273
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@276
    move-result-object v14

    #@277
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v14

    #@27b
    const-string v15, " data="

    #@27d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v14

    #@281
    move-object/from16 v0, p1

    #@283
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@286
    move-result-object v14

    #@287
    const-string v15, ""

    #@289
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v14

    #@28d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@290
    move-result-object v14

    #@291
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@294
    goto/16 :goto_bf

    #@296
    .line 688
    .end local v8           #m:Landroid/os/Message;
    :cond_296
    const-string v13, "HS20-ANQP-RX"

    #@298
    move-object/from16 v0, p1

    #@29a
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@29d
    move-result v13

    #@29e
    if-eqz v13, :cond_2bc

    #@2a0
    .line 690
    move-object/from16 v0, p0

    #@2a2
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@2a4
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@2a7
    move-result-object v13

    #@2a8
    const v14, 0x24032

    #@2ab
    invoke-virtual {v13, v14}, Lcom/android/internal/util/StateMachine;->obtainMessage(I)Landroid/os/Message;

    #@2ae
    move-result-object v8

    #@2af
    .line 691
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@2b1
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@2b3
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@2b6
    move-result-object v13

    #@2b7
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@2ba
    goto/16 :goto_bf

    #@2bc
    .line 692
    .end local v8           #m:Landroid/os/Message;
    :cond_2bc
    const-string v13, "INTERWORKING-3GPP-CONNECTING"

    #@2be
    move-object/from16 v0, p1

    #@2c0
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2c3
    move-result v13

    #@2c4
    if-eqz v13, :cond_33f

    #@2c6
    .line 693
    const/4 v5, 0x0

    #@2c7
    .line 695
    .local v5, eap_method:Ljava/lang/String;
    move-object v3, v4

    #@2c8
    array-length v7, v3

    #@2c9
    const/4 v6, 0x0

    #@2ca
    :goto_2ca
    if-ge v6, v7, :cond_2e2

    #@2cc
    aget-object v11, v3, v6

    #@2ce
    .line 696
    .restart local v11       #token:Ljava/lang/String;
    const-string v13, "="

    #@2d0
    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2d3
    move-result-object v9

    #@2d4
    .line 698
    .restart local v9       #nameValue:[Ljava/lang/String;
    const-string v13, "eap_method"

    #@2d6
    const/4 v14, 0x0

    #@2d7
    aget-object v14, v9, v14

    #@2d9
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2dc
    move-result v13

    #@2dd
    if-eqz v13, :cond_33c

    #@2df
    .line 699
    const/4 v13, 0x1

    #@2e0
    aget-object v5, v9, v13

    #@2e2
    .line 703
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_2e2
    move-object/from16 v0, p0

    #@2e4
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@2e6
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@2e9
    move-result-object v13

    #@2ea
    const v14, 0x24033

    #@2ed
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2f0
    move-result-object v8

    #@2f1
    .line 704
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@2f3
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@2f5
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@2f8
    move-result-object v13

    #@2f9
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@2fc
    .line 705
    const-string v13, "WifiMonitor"

    #@2fe
    new-instance v14, Ljava/lang/StringBuilder;

    #@300
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@303
    const-string v15, "[PASSPOINT]  bssid ="

    #@305
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@308
    move-result-object v14

    #@309
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30c
    move-result-object v14

    #@30d
    const-string v15, "SSID ="

    #@30f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v14

    #@313
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@316
    move-result-object v14

    #@317
    const-string v15, "eap_method ="

    #@319
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31c
    move-result-object v14

    #@31d
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@320
    move-result-object v14

    #@321
    const-string v15, " data="

    #@323
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@326
    move-result-object v14

    #@327
    move-object/from16 v0, p1

    #@329
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32c
    move-result-object v14

    #@32d
    const-string v15, ""

    #@32f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@332
    move-result-object v14

    #@333
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@336
    move-result-object v14

    #@337
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33a
    goto/16 :goto_bf

    #@33c
    .line 695
    .end local v8           #m:Landroid/os/Message;
    .restart local v9       #nameValue:[Ljava/lang/String;
    .restart local v11       #token:Ljava/lang/String;
    :cond_33c
    add-int/lit8 v6, v6, 0x1

    #@33e
    goto :goto_2ca

    #@33f
    .line 706
    .end local v5           #eap_method:Ljava/lang/String;
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_33f
    const-string v13, "INTERWORKING-RC-CONNECTING"

    #@341
    move-object/from16 v0, p1

    #@343
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@346
    move-result v13

    #@347
    if-eqz v13, :cond_3c2

    #@349
    .line 707
    const/4 v5, 0x0

    #@34a
    .line 709
    .restart local v5       #eap_method:Ljava/lang/String;
    move-object v3, v4

    #@34b
    array-length v7, v3

    #@34c
    const/4 v6, 0x0

    #@34d
    :goto_34d
    if-ge v6, v7, :cond_365

    #@34f
    aget-object v11, v3, v6

    #@351
    .line 710
    .restart local v11       #token:Ljava/lang/String;
    const-string v13, "="

    #@353
    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@356
    move-result-object v9

    #@357
    .line 712
    .restart local v9       #nameValue:[Ljava/lang/String;
    const-string v13, "eap_method"

    #@359
    const/4 v14, 0x0

    #@35a
    aget-object v14, v9, v14

    #@35c
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35f
    move-result v13

    #@360
    if-eqz v13, :cond_3bf

    #@362
    .line 713
    const/4 v13, 0x1

    #@363
    aget-object v5, v9, v13

    #@365
    .line 717
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_365
    move-object/from16 v0, p0

    #@367
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@369
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@36c
    move-result-object v13

    #@36d
    const v14, 0x24034

    #@370
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@373
    move-result-object v8

    #@374
    .line 718
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@376
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@378
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@37b
    move-result-object v13

    #@37c
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@37f
    .line 719
    const-string v13, "WifiMonitor"

    #@381
    new-instance v14, Ljava/lang/StringBuilder;

    #@383
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@386
    const-string v15, "[PASSPOINT]  bssid ="

    #@388
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38b
    move-result-object v14

    #@38c
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38f
    move-result-object v14

    #@390
    const-string v15, "SSID ="

    #@392
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@395
    move-result-object v14

    #@396
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@399
    move-result-object v14

    #@39a
    const-string v15, "eap_method ="

    #@39c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39f
    move-result-object v14

    #@3a0
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a3
    move-result-object v14

    #@3a4
    const-string v15, " data="

    #@3a6
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a9
    move-result-object v14

    #@3aa
    move-object/from16 v0, p1

    #@3ac
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3af
    move-result-object v14

    #@3b0
    const-string v15, ""

    #@3b2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b5
    move-result-object v14

    #@3b6
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b9
    move-result-object v14

    #@3ba
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3bd
    goto/16 :goto_bf

    #@3bf
    .line 709
    .end local v8           #m:Landroid/os/Message;
    .restart local v9       #nameValue:[Ljava/lang/String;
    .restart local v11       #token:Ljava/lang/String;
    :cond_3bf
    add-int/lit8 v6, v6, 0x1

    #@3c1
    goto :goto_34d

    #@3c2
    .line 720
    .end local v5           #eap_method:Ljava/lang/String;
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_3c2
    const-string v13, "INTERWORKING-TLS-TTLS-CONNECTING"

    #@3c4
    move-object/from16 v0, p1

    #@3c6
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3c9
    move-result v13

    #@3ca
    if-eqz v13, :cond_bf

    #@3cc
    .line 721
    const/4 v5, 0x0

    #@3cd
    .line 723
    .restart local v5       #eap_method:Ljava/lang/String;
    move-object v3, v4

    #@3ce
    array-length v7, v3

    #@3cf
    const/4 v6, 0x0

    #@3d0
    :goto_3d0
    if-ge v6, v7, :cond_3e8

    #@3d2
    aget-object v11, v3, v6

    #@3d4
    .line 724
    .restart local v11       #token:Ljava/lang/String;
    const-string v13, "="

    #@3d6
    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3d9
    move-result-object v9

    #@3da
    .line 726
    .restart local v9       #nameValue:[Ljava/lang/String;
    const-string v13, "eap_method"

    #@3dc
    const/4 v14, 0x0

    #@3dd
    aget-object v14, v9, v14

    #@3df
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e2
    move-result v13

    #@3e3
    if-eqz v13, :cond_442

    #@3e5
    .line 727
    const/4 v13, 0x1

    #@3e6
    aget-object v5, v9, v13

    #@3e8
    .line 731
    .end local v9           #nameValue:[Ljava/lang/String;
    .end local v11           #token:Ljava/lang/String;
    :cond_3e8
    move-object/from16 v0, p0

    #@3ea
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@3ec
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@3ef
    move-result-object v13

    #@3f0
    const v14, 0x24035

    #@3f3
    invoke-virtual {v13, v14, v1}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@3f6
    move-result-object v8

    #@3f7
    .line 732
    .restart local v8       #m:Landroid/os/Message;
    move-object/from16 v0, p0

    #@3f9
    iget-object v13, v0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@3fb
    invoke-static {v13}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@3fe
    move-result-object v13

    #@3ff
    invoke-virtual {v13, v8}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    #@402
    .line 733
    const-string v13, "WifiMonitor"

    #@404
    new-instance v14, Ljava/lang/StringBuilder;

    #@406
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@409
    const-string v15, "[PASSPOINT]  bssid ="

    #@40b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40e
    move-result-object v14

    #@40f
    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@412
    move-result-object v14

    #@413
    const-string v15, "SSID ="

    #@415
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@418
    move-result-object v14

    #@419
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41c
    move-result-object v14

    #@41d
    const-string v15, "eap_method ="

    #@41f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@422
    move-result-object v14

    #@423
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@426
    move-result-object v14

    #@427
    const-string v15, " data="

    #@429
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42c
    move-result-object v14

    #@42d
    move-object/from16 v0, p1

    #@42f
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@432
    move-result-object v14

    #@433
    const-string v15, ""

    #@435
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@438
    move-result-object v14

    #@439
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43c
    move-result-object v14

    #@43d
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@440
    goto/16 :goto_bf

    #@442
    .line 723
    .end local v8           #m:Landroid/os/Message;
    .restart local v9       #nameValue:[Ljava/lang/String;
    .restart local v11       #token:Ljava/lang/String;
    :cond_442
    add-int/lit8 v6, v6, 0x1

    #@444
    goto :goto_3d0
.end method

.method public run()V
    .registers 15

    #@0
    .prologue
    const/16 v13, 0x8

    #@2
    const/4 v12, 0x7

    #@3
    const/4 v11, 0x3

    #@4
    const/4 v10, 0x0

    #@5
    const/4 v9, -0x1

    #@6
    .line 443
    invoke-direct {p0}, Landroid/net/wifi/WifiMonitor$MonitorThread;->connectToSupplicant()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_48

    #@c
    .line 446
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@e
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@11
    move-result-object v6

    #@12
    const v7, 0x24001

    #@15
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@18
    .line 454
    :cond_18
    :goto_18
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1a
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$100(Landroid/net/wifi/WifiMonitor;)Landroid/net/wifi/WifiNative;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6}, Landroid/net/wifi/WifiNative;->waitForEvent()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    .line 460
    .local v3, eventStr:Ljava/lang/String;
    const-string v6, "CTRL-EVENT-"

    #@24
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@27
    move-result v6

    #@28
    if-nez v6, :cond_dd

    #@2a
    .line 461
    const-string v6, "WPA:"

    #@2c
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2f
    move-result v6

    #@30
    if-eqz v6, :cond_55

    #@32
    const-string/jumbo v6, "pre-shared key may be incorrect"

    #@35
    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@38
    move-result v6

    #@39
    if-lez v6, :cond_55

    #@3b
    .line 463
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@3d
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@40
    move-result-object v6

    #@41
    const v7, 0x24007

    #@44
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@47
    goto :goto_18

    #@48
    .line 448
    .end local v3           #eventStr:Ljava/lang/String;
    :cond_48
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@4a
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@4d
    move-result-object v6

    #@4e
    const v7, 0x24002

    #@51
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@54
    .line 583
    :goto_54
    return-void

    #@55
    .line 464
    .restart local v3       #eventStr:Ljava/lang/String;
    :cond_55
    const-string v6, "WPS-SUCCESS"

    #@57
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5a
    move-result v6

    #@5b
    if-eqz v6, :cond_77

    #@5d
    .line 465
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@5f
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@62
    move-result-object v6

    #@63
    const v7, 0x24008

    #@66
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@69
    .line 467
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@6b
    if-eqz v6, :cond_18

    #@6d
    .line 468
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@6f
    const v7, 0x24008

    #@72
    const/4 v8, 0x0

    #@73
    invoke-static {v6, v7, v8}, Landroid/net/wifi/WifiMonitor;->access$200(Landroid/net/wifi/WifiMonitor;ILjava/lang/String;)V

    #@76
    goto :goto_18

    #@77
    .line 471
    :cond_77
    const-string v6, "WPS-FAIL"

    #@79
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7c
    move-result v6

    #@7d
    if-eqz v6, :cond_83

    #@7f
    .line 472
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleWpsFailEvent(Ljava/lang/String;)V

    #@82
    goto :goto_18

    #@83
    .line 473
    :cond_83
    const-string v6, "WPS-OVERLAP-DETECTED"

    #@85
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@88
    move-result v6

    #@89
    if-eqz v6, :cond_98

    #@8b
    .line 474
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@8d
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@90
    move-result-object v6

    #@91
    const v7, 0x2400a

    #@94
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@97
    goto :goto_18

    #@98
    .line 475
    :cond_98
    const-string v6, "WPS-TIMEOUT"

    #@9a
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@9d
    move-result v6

    #@9e
    if-eqz v6, :cond_ae

    #@a0
    .line 476
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@a2
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@a5
    move-result-object v6

    #@a6
    const v7, 0x2400b

    #@a9
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@ac
    goto/16 :goto_18

    #@ae
    .line 477
    :cond_ae
    const-string v6, "P2P"

    #@b0
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b3
    move-result v6

    #@b4
    if-eqz v6, :cond_bb

    #@b6
    .line 478
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleP2pEvents(Ljava/lang/String;)V

    #@b9
    goto/16 :goto_18

    #@bb
    .line 479
    :cond_bb
    const-string v6, "AP"

    #@bd
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c0
    move-result v6

    #@c1
    if-eqz v6, :cond_c8

    #@c3
    .line 480
    invoke-direct {p0, v3}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleHostApEvents(Ljava/lang/String;)V

    #@c6
    goto/16 :goto_18

    #@c8
    .line 483
    :cond_c8
    const-string v6, "INTERWORKING-"

    #@ca
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@cd
    move-result v6

    #@ce
    if-nez v6, :cond_d8

    #@d0
    const-string v6, "HS20-"

    #@d2
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d5
    move-result v6

    #@d6
    if-eqz v6, :cond_18

    #@d8
    .line 485
    :cond_d8
    invoke-virtual {p0, v3}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleInterworkingEvent(Ljava/lang/String;)V

    #@db
    goto/16 :goto_18

    #@dd
    .line 491
    :cond_dd
    invoke-static {}, Landroid/net/wifi/WifiMonitor;->access$300()I

    #@e0
    move-result v6

    #@e1
    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@e4
    move-result-object v2

    #@e5
    .line 492
    .local v2, eventName:Ljava/lang/String;
    const/16 v6, 0x20

    #@e7
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    #@ea
    move-result v5

    #@eb
    .line 493
    .local v5, nameEnd:I
    if-eq v5, v9, :cond_f1

    #@ed
    .line 494
    invoke-virtual {v2, v10, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@f0
    move-result-object v2

    #@f1
    .line 495
    :cond_f1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@f4
    move-result v6

    #@f5
    if-eqz v6, :cond_18

    #@f7
    .line 503
    const-string v6, "CONNECTED"

    #@f9
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fc
    move-result v6

    #@fd
    if-eqz v6, :cond_11b

    #@ff
    .line 504
    const/4 v0, 0x1

    #@100
    .line 526
    .local v0, event:I
    :goto_100
    move-object v1, v3

    #@101
    .line 527
    .local v1, eventData:Ljava/lang/String;
    if-eq v0, v12, :cond_106

    #@103
    const/4 v6, 0x5

    #@104
    if-ne v0, v6, :cond_165

    #@106
    .line 528
    :cond_106
    const-string v6, " "

    #@108
    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@10b
    move-result-object v6

    #@10c
    const/4 v7, 0x1

    #@10d
    aget-object v1, v6, v7

    #@10f
    .line 541
    :cond_10f
    :goto_10f
    if-ne v0, v11, :cond_187

    #@111
    .line 542
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleSupplicantStateChange(Ljava/lang/String;)V

    #@114
    .line 581
    :cond_114
    :goto_114
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@116
    invoke-static {v6, v10}, Landroid/net/wifi/WifiMonitor;->access$402(Landroid/net/wifi/WifiMonitor;I)I

    #@119
    goto/16 :goto_18

    #@11b
    .line 505
    .end local v0           #event:I
    .end local v1           #eventData:Ljava/lang/String;
    :cond_11b
    const-string v6, "DISCONNECTED"

    #@11d
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@120
    move-result v6

    #@121
    if-eqz v6, :cond_125

    #@123
    .line 506
    const/4 v0, 0x2

    #@124
    .restart local v0       #event:I
    goto :goto_100

    #@125
    .line 507
    .end local v0           #event:I
    :cond_125
    const-string v6, "STATE-CHANGE"

    #@127
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12a
    move-result v6

    #@12b
    if-eqz v6, :cond_12f

    #@12d
    .line 508
    const/4 v0, 0x3

    #@12e
    .restart local v0       #event:I
    goto :goto_100

    #@12f
    .line 509
    .end local v0           #event:I
    :cond_12f
    const-string v6, "SCAN-RESULTS"

    #@131
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@134
    move-result v6

    #@135
    if-eqz v6, :cond_139

    #@137
    .line 510
    const/4 v0, 0x4

    #@138
    .restart local v0       #event:I
    goto :goto_100

    #@139
    .line 511
    .end local v0           #event:I
    :cond_139
    const-string v6, "LINK-SPEED"

    #@13b
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13e
    move-result v6

    #@13f
    if-eqz v6, :cond_143

    #@141
    .line 512
    const/4 v0, 0x5

    #@142
    .restart local v0       #event:I
    goto :goto_100

    #@143
    .line 513
    .end local v0           #event:I
    :cond_143
    const-string v6, "TERMINATING"

    #@145
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@148
    move-result v6

    #@149
    if-eqz v6, :cond_14d

    #@14b
    .line 514
    const/4 v0, 0x6

    #@14c
    .restart local v0       #event:I
    goto :goto_100

    #@14d
    .line 515
    .end local v0           #event:I
    :cond_14d
    const-string v6, "DRIVER-STATE"

    #@14f
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152
    move-result v6

    #@153
    if-eqz v6, :cond_157

    #@155
    .line 516
    const/4 v0, 0x7

    #@156
    .restart local v0       #event:I
    goto :goto_100

    #@157
    .line 517
    .end local v0           #event:I
    :cond_157
    const-string v6, "EAP-FAILURE"

    #@159
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15c
    move-result v6

    #@15d
    if-eqz v6, :cond_162

    #@15f
    .line 519
    const/16 v0, 0x8

    #@161
    .restart local v0       #event:I
    goto :goto_100

    #@162
    .line 523
    .end local v0           #event:I
    :cond_162
    const/16 v0, 0x9

    #@164
    .restart local v0       #event:I
    goto :goto_100

    #@165
    .line 529
    .restart local v1       #eventData:Ljava/lang/String;
    :cond_165
    if-eq v0, v11, :cond_169

    #@167
    if-ne v0, v13, :cond_178

    #@169
    .line 530
    :cond_169
    const-string v6, " "

    #@16b
    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@16e
    move-result v4

    #@16f
    .line 531
    .local v4, ind:I
    if-eq v4, v9, :cond_10f

    #@171
    .line 532
    add-int/lit8 v6, v4, 0x1

    #@173
    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@176
    move-result-object v1

    #@177
    goto :goto_10f

    #@178
    .line 535
    .end local v4           #ind:I
    :cond_178
    const-string v6, " - "

    #@17a
    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@17d
    move-result v4

    #@17e
    .line 536
    .restart local v4       #ind:I
    if-eq v4, v9, :cond_10f

    #@180
    .line 537
    add-int/lit8 v6, v4, 0x3

    #@182
    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@185
    move-result-object v1

    #@186
    goto :goto_10f

    #@187
    .line 543
    .end local v4           #ind:I
    :cond_187
    if-ne v0, v12, :cond_18d

    #@189
    .line 544
    invoke-direct {p0, v1}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleDriverEvent(Ljava/lang/String;)V

    #@18c
    goto :goto_114

    #@18d
    .line 545
    :cond_18d
    const/4 v6, 0x6

    #@18e
    if-ne v0, v6, :cond_1d2

    #@190
    .line 550
    const-string/jumbo v6, "recv error"

    #@193
    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@196
    move-result v6

    #@197
    if-eqz v6, :cond_1a3

    #@199
    .line 551
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@19b
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$404(Landroid/net/wifi/WifiMonitor;)I

    #@19e
    move-result v6

    #@19f
    const/16 v7, 0xa

    #@1a1
    if-le v6, v7, :cond_1b1

    #@1a3
    .line 562
    :cond_1a3
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1a5
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@1a8
    move-result-object v6

    #@1a9
    const v7, 0x24002

    #@1ac
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@1af
    goto/16 :goto_54

    #@1b1
    .line 556
    :cond_1b1
    const-string v6, "WifiMonitor"

    #@1b3
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1b8
    const-string/jumbo v8, "too many recv errors, continue mRecvErrors"

    #@1bb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v7

    #@1bf
    iget-object v8, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1c1
    invoke-static {v8}, Landroid/net/wifi/WifiMonitor;->access$400(Landroid/net/wifi/WifiMonitor;)I

    #@1c4
    move-result v8

    #@1c5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v7

    #@1c9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cc
    move-result-object v7

    #@1cd
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d0
    goto/16 :goto_18

    #@1d2
    .line 564
    :cond_1d2
    if-ne v0, v13, :cond_1f9

    #@1d4
    .line 565
    const-string v6, "EAP authentication failed"

    #@1d6
    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1d9
    move-result v6

    #@1da
    if-eqz v6, :cond_1e8

    #@1dc
    .line 566
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1de
    invoke-static {v6}, Landroid/net/wifi/WifiMonitor;->access$000(Landroid/net/wifi/WifiMonitor;)Lcom/android/internal/util/StateMachine;

    #@1e1
    move-result-object v6

    #@1e2
    const v7, 0x24007

    #@1e5
    invoke-virtual {v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    #@1e8
    .line 569
    :cond_1e8
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@1ea
    if-eqz v6, :cond_114

    #@1ec
    .line 570
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1ee
    iget-object v7, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1f0
    invoke-static {v7, v2}, Landroid/net/wifi/WifiMonitor;->access$500(Landroid/net/wifi/WifiMonitor;Ljava/lang/String;)I

    #@1f3
    move-result v7

    #@1f4
    invoke-static {v6, v7, v1}, Landroid/net/wifi/WifiMonitor;->access$200(Landroid/net/wifi/WifiMonitor;ILjava/lang/String;)V

    #@1f7
    goto/16 :goto_114

    #@1f9
    .line 575
    :cond_1f9
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@1fb
    if-eqz v6, :cond_208

    #@1fd
    .line 576
    iget-object v6, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@1ff
    iget-object v7, p0, Landroid/net/wifi/WifiMonitor$MonitorThread;->this$0:Landroid/net/wifi/WifiMonitor;

    #@201
    invoke-static {v7, v2}, Landroid/net/wifi/WifiMonitor;->access$500(Landroid/net/wifi/WifiMonitor;Ljava/lang/String;)I

    #@204
    move-result v7

    #@205
    invoke-static {v6, v7, v1}, Landroid/net/wifi/WifiMonitor;->access$200(Landroid/net/wifi/WifiMonitor;ILjava/lang/String;)V

    #@208
    .line 579
    :cond_208
    invoke-virtual {p0, v0, v1}, Landroid/net/wifi/WifiMonitor$MonitorThread;->handleEvent(ILjava/lang/String;)V

    #@20b
    goto/16 :goto_114
.end method
