.class public Landroid/net/wifi/NetworkManagementServiceProxy;
.super Ljava/lang/Object;
.source "NetworkManagementServiceProxy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NetworkManagementServiceProxy"

.field private static getAssociatedIpHostnameWithMacMethod:Ljava/lang/reflect/Method;

.field private static getAssociatedIpWithMacMethod:Ljava/lang/reflect/Method;

.field private static getStationListMethod:Ljava/lang/reflect/Method;

.field private static runClearNatRuleMethod:Ljava/lang/reflect/Method;

.field private static runClearPortFilterRuleMethod:Ljava/lang/reflect/Method;

.field private static runClearPortForwardRuleMethod:Ljava/lang/reflect/Method;

.field private static runSetNatForwardRuleMethod:Ljava/lang/reflect/Method;

.field private static runSetPortFilterRuleMethod:Ljava/lang/reflect/Method;

.field private static runSetPortForwardRuleMethod:Ljava/lang/reflect/Method;

.field private static setSoftapMaxClientsMethod:Ljava/lang/reflect/Method;

.field private static startTetheringWithFullOptionMethod:Ljava/lang/reflect/Method;

.field private static startVZWAccessPointMethod:Ljava/lang/reflect/Method;

.field private static stopVZWAccessPointMethod:Ljava/lang/reflect/Method;


# instance fields
.field private final mNwService:Landroid/os/INetworkManagementService;


# direct methods
.method static constructor <clinit>()V
    .registers 19

    #@0
    .prologue
    .line 42
    const/16 v16, 0x0

    #@2
    move/from16 v0, v16

    #@4
    new-array v4, v0, [Ljava/lang/Class;

    #@6
    .line 43
    .local v4, getStationListMethodParamType:[Ljava/lang/Class;
    const/16 v16, 0x1

    #@8
    move/from16 v0, v16

    #@a
    new-array v12, v0, [Ljava/lang/Class;

    #@c
    const/16 v16, 0x0

    #@e
    const-class v17, [Ljava/lang/String;

    #@10
    aput-object v17, v12, v16

    #@12
    .line 46
    .local v12, startTetheringWithFullOptionParamType:[Ljava/lang/Class;
    const/16 v16, 0x1

    #@14
    move/from16 v0, v16

    #@16
    new-array v2, v0, [Ljava/lang/Class;

    #@18
    const/16 v16, 0x0

    #@1a
    const-class v17, Ljava/lang/String;

    #@1c
    aput-object v17, v2, v16

    #@1e
    .line 49
    .local v2, getAssociatedIpHostnameWithMacParamType:[Ljava/lang/Class;
    const/16 v16, 0x1

    #@20
    move/from16 v0, v16

    #@22
    new-array v8, v0, [Ljava/lang/Class;

    #@24
    const/16 v16, 0x0

    #@26
    const-class v17, Ljava/lang/String;

    #@28
    aput-object v17, v8, v16

    #@2a
    .line 52
    .local v8, runSetNatForwardRuleParamType:[Ljava/lang/Class;
    const/16 v16, 0x0

    #@2c
    move/from16 v0, v16

    #@2e
    new-array v5, v0, [Ljava/lang/Class;

    #@30
    .line 53
    .local v5, runClearNatRuleParamType:[Ljava/lang/Class;
    const/16 v16, 0x2

    #@32
    move/from16 v0, v16

    #@34
    new-array v9, v0, [Ljava/lang/Class;

    #@36
    const/16 v16, 0x0

    #@38
    const-class v17, Ljava/lang/String;

    #@3a
    aput-object v17, v9, v16

    #@3c
    const/16 v16, 0x1

    #@3e
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@40
    aput-object v17, v9, v16

    #@42
    .line 56
    .local v9, runSetPortFilterRuleParamType:[Ljava/lang/Class;
    const/16 v16, 0x0

    #@44
    move/from16 v0, v16

    #@46
    new-array v6, v0, [Ljava/lang/Class;

    #@48
    .line 57
    .local v6, runClearPortFilterRuleParamType:[Ljava/lang/Class;
    const/16 v16, 0x2

    #@4a
    move/from16 v0, v16

    #@4c
    new-array v10, v0, [Ljava/lang/Class;

    #@4e
    const/16 v16, 0x0

    #@50
    const-class v17, Ljava/lang/String;

    #@52
    aput-object v17, v10, v16

    #@54
    const/16 v16, 0x1

    #@56
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@58
    aput-object v17, v10, v16

    #@5a
    .line 60
    .local v10, runSetPortForwardRuleParamType:[Ljava/lang/Class;
    const/16 v16, 0x0

    #@5c
    move/from16 v0, v16

    #@5e
    new-array v7, v0, [Ljava/lang/Class;

    #@60
    .line 64
    .local v7, runClearPortForwardRuleParamType:[Ljava/lang/Class;
    const/16 v16, 0x14

    #@62
    move/from16 v0, v16

    #@64
    new-array v13, v0, [Ljava/lang/Class;

    #@66
    const/16 v16, 0x0

    #@68
    const-class v17, Ljava/lang/String;

    #@6a
    aput-object v17, v13, v16

    #@6c
    const/16 v16, 0x1

    #@6e
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@70
    aput-object v17, v13, v16

    #@72
    const/16 v16, 0x2

    #@74
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@76
    aput-object v17, v13, v16

    #@78
    const/16 v16, 0x3

    #@7a
    const-class v17, Ljava/lang/String;

    #@7c
    aput-object v17, v13, v16

    #@7e
    const/16 v16, 0x4

    #@80
    const-class v17, Ljava/lang/String;

    #@82
    aput-object v17, v13, v16

    #@84
    const/16 v16, 0x5

    #@86
    const-class v17, Ljava/lang/String;

    #@88
    aput-object v17, v13, v16

    #@8a
    const/16 v16, 0x6

    #@8c
    const-class v17, Ljava/lang/String;

    #@8e
    aput-object v17, v13, v16

    #@90
    const/16 v16, 0x7

    #@92
    const-class v17, Ljava/lang/String;

    #@94
    aput-object v17, v13, v16

    #@96
    const/16 v16, 0x8

    #@98
    sget-object v17, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@9a
    aput-object v17, v13, v16

    #@9c
    const/16 v16, 0x9

    #@9e
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@a0
    aput-object v17, v13, v16

    #@a2
    const/16 v16, 0xa

    #@a4
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@a6
    aput-object v17, v13, v16

    #@a8
    const/16 v16, 0xb

    #@aa
    const-class v17, Ljava/lang/String;

    #@ac
    aput-object v17, v13, v16

    #@ae
    const/16 v16, 0xc

    #@b0
    const-class v17, Ljava/lang/String;

    #@b2
    aput-object v17, v13, v16

    #@b4
    const/16 v16, 0xd

    #@b6
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@b8
    aput-object v17, v13, v16

    #@ba
    const/16 v16, 0xe

    #@bc
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@be
    aput-object v17, v13, v16

    #@c0
    const/16 v16, 0xf

    #@c2
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@c4
    aput-object v17, v13, v16

    #@c6
    const/16 v16, 0x10

    #@c8
    const-class v17, Ljava/lang/String;

    #@ca
    aput-object v17, v13, v16

    #@cc
    const/16 v16, 0x11

    #@ce
    const-class v17, Ljava/lang/String;

    #@d0
    aput-object v17, v13, v16

    #@d2
    const/16 v16, 0x12

    #@d4
    const-class v17, Ljava/lang/String;

    #@d6
    aput-object v17, v13, v16

    #@d8
    const/16 v16, 0x13

    #@da
    const-class v17, Ljava/lang/String;

    #@dc
    aput-object v17, v13, v16

    #@de
    .line 70
    .local v13, startVZWAccessPointParamType:[Ljava/lang/Class;
    const/16 v16, 0x1

    #@e0
    move/from16 v0, v16

    #@e2
    new-array v14, v0, [Ljava/lang/Class;

    #@e4
    const/16 v16, 0x0

    #@e6
    const-class v17, Ljava/lang/String;

    #@e8
    aput-object v17, v14, v16

    #@ea
    .line 73
    .local v14, stopVZWAccessPointParamType:[Ljava/lang/Class;
    const/16 v16, 0x1

    #@ec
    move/from16 v0, v16

    #@ee
    new-array v3, v0, [Ljava/lang/Class;

    #@f0
    const/16 v16, 0x0

    #@f2
    const-class v17, Ljava/lang/String;

    #@f4
    aput-object v17, v3, v16

    #@f6
    .line 76
    .local v3, getAssociatedIpWithMacParamType:[Ljava/lang/Class;
    const/16 v16, 0x1

    #@f8
    move/from16 v0, v16

    #@fa
    new-array v11, v0, [Ljava/lang/Class;

    #@fc
    const/16 v16, 0x0

    #@fe
    sget-object v17, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@100
    aput-object v17, v11, v16

    #@102
    .line 82
    .local v11, setSoftapMaxClientsParamType:[Ljava/lang/Class;
    :try_start_102
    const-string v16, "com.android.server.NetworkManagementService"

    #@104
    invoke-static/range {v16 .. v16}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@107
    move-result-object v1

    #@108
    .line 90
    .local v1, c:Ljava/lang/Class;
    const-string v16, "getStationList"

    #@10a
    move-object/from16 v0, v16

    #@10c
    invoke-virtual {v1, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@10f
    move-result-object v16

    #@110
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->getStationListMethod:Ljava/lang/reflect/Method;

    #@112
    .line 92
    const-string/jumbo v16, "startTetheringWithFullOption"

    #@115
    move-object/from16 v0, v16

    #@117
    invoke-virtual {v1, v0, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@11a
    move-result-object v16

    #@11b
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->startTetheringWithFullOptionMethod:Ljava/lang/reflect/Method;

    #@11d
    .line 94
    const-string v16, "getAssociatedIpHostnameWithMac"

    #@11f
    move-object/from16 v0, v16

    #@121
    invoke-virtual {v1, v0, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@124
    move-result-object v16

    #@125
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->getAssociatedIpHostnameWithMacMethod:Ljava/lang/reflect/Method;

    #@127
    .line 96
    const-string/jumbo v16, "runSetNatForwardRule"

    #@12a
    move-object/from16 v0, v16

    #@12c
    invoke-virtual {v1, v0, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@12f
    move-result-object v16

    #@130
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetNatForwardRuleMethod:Ljava/lang/reflect/Method;

    #@132
    .line 98
    const-string/jumbo v16, "runClearNatRule"

    #@135
    move-object/from16 v0, v16

    #@137
    invoke-virtual {v1, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@13a
    move-result-object v16

    #@13b
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearNatRuleMethod:Ljava/lang/reflect/Method;

    #@13d
    .line 100
    const-string/jumbo v16, "runSetPortFilterRule"

    #@140
    move-object/from16 v0, v16

    #@142
    invoke-virtual {v1, v0, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@145
    move-result-object v16

    #@146
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetPortFilterRuleMethod:Ljava/lang/reflect/Method;

    #@148
    .line 102
    const-string/jumbo v16, "runClearPortFilterRule"

    #@14b
    move-object/from16 v0, v16

    #@14d
    invoke-virtual {v1, v0, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@150
    move-result-object v16

    #@151
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearPortFilterRuleMethod:Ljava/lang/reflect/Method;

    #@153
    .line 104
    const-string/jumbo v16, "runSetPortForwardRule"

    #@156
    move-object/from16 v0, v16

    #@158
    invoke-virtual {v1, v0, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@15b
    move-result-object v16

    #@15c
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetPortForwardRuleMethod:Ljava/lang/reflect/Method;

    #@15e
    .line 106
    const-string/jumbo v16, "runClearPortForwardRule"

    #@161
    move-object/from16 v0, v16

    #@163
    invoke-virtual {v1, v0, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@166
    move-result-object v16

    #@167
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearPortForwardRuleMethod:Ljava/lang/reflect/Method;

    #@169
    .line 110
    const-string/jumbo v16, "startVZWAccessPoint"

    #@16c
    move-object/from16 v0, v16

    #@16e
    invoke-virtual {v1, v0, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@171
    move-result-object v16

    #@172
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->startVZWAccessPointMethod:Ljava/lang/reflect/Method;

    #@174
    .line 112
    const-string/jumbo v16, "stopVZWAccessPoint"

    #@177
    move-object/from16 v0, v16

    #@179
    invoke-virtual {v1, v0, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@17c
    move-result-object v16

    #@17d
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->stopVZWAccessPointMethod:Ljava/lang/reflect/Method;

    #@17f
    .line 114
    const-string v16, "getAssociatedIpWithMac"

    #@181
    move-object/from16 v0, v16

    #@183
    invoke-virtual {v1, v0, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@186
    move-result-object v16

    #@187
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->getAssociatedIpWithMacMethod:Ljava/lang/reflect/Method;

    #@189
    .line 116
    const-string/jumbo v16, "setSoftapMaxClients"

    #@18c
    move-object/from16 v0, v16

    #@18e
    invoke-virtual {v1, v0, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@191
    move-result-object v16

    #@192
    sput-object v16, Landroid/net/wifi/NetworkManagementServiceProxy;->setSoftapMaxClientsMethod:Ljava/lang/reflect/Method;
    :try_end_194
    .catch Ljava/lang/NoSuchMethodException; {:try_start_102 .. :try_end_194} :catch_1b1
    .catch Ljava/lang/Throwable; {:try_start_102 .. :try_end_194} :catch_195

    #@194
    .line 124
    .end local v1           #c:Ljava/lang/Class;
    :goto_194
    return-void

    #@195
    .line 121
    :catch_195
    move-exception v15

    #@196
    .line 122
    .local v15, t:Ljava/lang/Throwable;
    const-string v16, "NetworkManagementServiceProxy"

    #@198
    new-instance v17, Ljava/lang/StringBuilder;

    #@19a
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@19d
    const-string v18, "Unable to find NetworkManagementService"

    #@19f
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v17

    #@1a3
    move-object/from16 v0, v17

    #@1a5
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v17

    #@1a9
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v17

    #@1ad
    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    goto :goto_194

    #@1b1
    .line 119
    .end local v15           #t:Ljava/lang/Throwable;
    :catch_1b1
    move-exception v16

    #@1b2
    goto :goto_194
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 131
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 132
    const-string/jumbo v1, "network_management"

    #@6
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 133
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@10
    .line 134
    return-void
.end method

.method private convertQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "s"

    #@0
    .prologue
    const/16 v4, 0x22

    #@2
    .line 639
    if-nez p1, :cond_5

    #@4
    .line 643
    .end local p1
    :goto_4
    return-object p1

    #@5
    .restart local p1
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, "\\\\"

    #@10
    const-string v2, "\\\\\\\\"

    #@12
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    const-string v2, "\""

    #@18
    const-string v3, "\\\\\""

    #@1a
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object p1

    #@2a
    goto :goto_4
.end method

.method private static getVZWSecurityType(Landroid/net/wifi/WifiVZWConfigurationProxy;)Ljava/lang/String;
    .registers 2
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 655
    const-string/jumbo v0, "open"

    #@3
    return-object v0
.end method

.method private isAllowalldevices()Z
    .registers 2

    #@0
    .prologue
    .line 635
    const/4 v0, 0x0

    #@1
    return v0
.end method


# virtual methods
.method public getAssociatedIpHostnameWithMac(Ljava/lang/String;)[Ljava/lang/String;
    .registers 8
    .parameter "mac"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 182
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->getAssociatedIpHostnameWithMacMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v3, :cond_3a

    #@5
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@7
    if-eqz v3, :cond_3a

    #@9
    .line 183
    const/4 v3, 0x1

    #@a
    new-array v0, v3, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    aput-object p1, v0, v3

    #@f
    .line 187
    .local v0, args:[Ljava/lang/Object;
    :try_start_f
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->getAssociatedIpHostnameWithMacMethod:Ljava/lang/reflect/Method;

    #@11
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@13
    invoke-virtual {v3, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    check-cast v3, [Ljava/lang/String;

    #@19
    check-cast v3, [Ljava/lang/String;
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_1b} :catch_1c
    .catch Ljava/lang/IllegalAccessException; {:try_start_f .. :try_end_1b} :catch_22
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_f .. :try_end_1b} :catch_27

    #@1b
    .line 207
    .end local v0           #args:[Ljava/lang/Object;
    :goto_1b
    return-object v3

    #@1c
    .line 188
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1c
    move-exception v2

    #@1d
    .line 189
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@20
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_20
    :goto_20
    move-object v3, v4

    #@21
    .line 201
    goto :goto_1b

    #@22
    .line 190
    :catch_22
    move-exception v2

    #@23
    .line 191
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@26
    goto :goto_20

    #@27
    .line 192
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_27
    move-exception v2

    #@28
    .line 193
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@2b
    move-result-object v1

    #@2c
    .line 194
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v3, v1, Landroid/os/RemoteException;

    #@2e
    if-eqz v3, :cond_33

    #@30
    .line 195
    check-cast v1, Landroid/os/RemoteException;

    #@32
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@33
    .line 197
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_33
    instance-of v3, v1, Ljava/lang/IllegalStateException;

    #@35
    if-eqz v3, :cond_20

    #@37
    .line 198
    check-cast v1, Ljava/lang/IllegalStateException;

    #@39
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@3a
    .line 205
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_3a
    const-string v3, "NetworkManagementServiceProxy"

    #@3c
    const-string v5, "getAssociatedIpHostnameWithMac method isn\'t implemented yet"

    #@3e
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    move-object v3, v4

    #@42
    .line 207
    goto :goto_1b
.end method

.method public getAssociatedIpWithMac(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "mac"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 559
    sget-object v5, Landroid/net/wifi/NetworkManagementServiceProxy;->getAssociatedIpWithMacMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v5, :cond_38

    #@5
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@7
    if-eqz v5, :cond_38

    #@9
    .line 560
    const/4 v5, 0x1

    #@a
    new-array v0, v5, [Ljava/lang/Object;

    #@c
    const/4 v5, 0x0

    #@d
    aput-object p1, v0, v5

    #@f
    .line 564
    .local v0, args:[Ljava/lang/Object;
    :try_start_f
    sget-object v5, Landroid/net/wifi/NetworkManagementServiceProxy;->getAssociatedIpWithMacMethod:Ljava/lang/reflect/Method;

    #@11
    iget-object v6, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@13
    invoke-virtual {v5, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    .line 565
    .local v3, obj:Ljava/lang/Object;
    check-cast v3, Ljava/lang/String;
    :try_end_19
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_19} :catch_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_f .. :try_end_19} :catch_20
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_f .. :try_end_19} :catch_25

    #@19
    .line 584
    .end local v0           #args:[Ljava/lang/Object;
    .end local v3           #obj:Ljava/lang/Object;
    :goto_19
    return-object v3

    #@1a
    .line 566
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1a
    move-exception v2

    #@1b
    .line 567
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1e
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_1e
    :goto_1e
    move-object v3, v4

    #@1f
    .line 579
    goto :goto_19

    #@20
    .line 568
    :catch_20
    move-exception v2

    #@21
    .line 569
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@24
    goto :goto_1e

    #@25
    .line 570
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_25
    move-exception v2

    #@26
    .line 571
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@29
    move-result-object v1

    #@2a
    .line 572
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v5, v1, Landroid/os/RemoteException;

    #@2c
    if-eqz v5, :cond_31

    #@2e
    .line 573
    check-cast v1, Landroid/os/RemoteException;

    #@30
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@31
    .line 575
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_31
    instance-of v5, v1, Ljava/lang/IllegalStateException;

    #@33
    if-eqz v5, :cond_1e

    #@35
    .line 576
    check-cast v1, Ljava/lang/IllegalStateException;

    #@37
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@38
    .line 583
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_38
    const-string v5, "NetworkManagementServiceProxy"

    #@3a
    const-string v6, "getAssociatedIpWithMac method isn\'t implemented yet"

    #@3c
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    move-object v3, v4

    #@40
    .line 584
    goto :goto_19
.end method

.method public getStationList()[Ljava/lang/String;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 396
    sget-object v5, Landroid/net/wifi/NetworkManagementServiceProxy;->getStationListMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v5, :cond_37

    #@5
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@7
    if-eqz v5, :cond_37

    #@9
    .line 397
    const/4 v5, 0x0

    #@a
    new-array v0, v5, [Ljava/lang/Object;

    #@c
    .line 399
    .local v0, args:[Ljava/lang/Object;
    :try_start_c
    sget-object v5, Landroid/net/wifi/NetworkManagementServiceProxy;->getStationListMethod:Ljava/lang/reflect/Method;

    #@e
    iget-object v6, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@10
    invoke-virtual {v5, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    .line 400
    .local v3, obj:Ljava/lang/Object;
    check-cast v3, [Ljava/lang/String;

    #@16
    .end local v3           #obj:Ljava/lang/Object;
    check-cast v3, [Ljava/lang/String;
    :try_end_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_18} :catch_19
    .catch Ljava/lang/IllegalAccessException; {:try_start_c .. :try_end_18} :catch_1f
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_c .. :try_end_18} :catch_24

    #@18
    .line 419
    .end local v0           #args:[Ljava/lang/Object;
    :goto_18
    return-object v3

    #@19
    .line 401
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_19
    move-exception v2

    #@1a
    .line 402
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1d
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :cond_1d
    :goto_1d
    move-object v3, v4

    #@1e
    .line 414
    goto :goto_18

    #@1f
    .line 403
    :catch_1f
    move-exception v2

    #@20
    .line 404
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@23
    goto :goto_1d

    #@24
    .line 405
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_24
    move-exception v2

    #@25
    .line 406
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@28
    move-result-object v1

    #@29
    .line 407
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v5, v1, Landroid/os/RemoteException;

    #@2b
    if-eqz v5, :cond_30

    #@2d
    .line 408
    check-cast v1, Landroid/os/RemoteException;

    #@2f
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@30
    .line 410
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_30
    instance-of v5, v1, Ljava/lang/IllegalStateException;

    #@32
    if-eqz v5, :cond_1d

    #@34
    .line 411
    check-cast v1, Ljava/lang/IllegalStateException;

    #@36
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@37
    .line 418
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_37
    const-string v5, "NetworkManagementServiceProxy"

    #@39
    const-string v6, "getStationList method isn\'t implemented yet"

    #@3b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    move-object v3, v4

    #@3f
    .line 419
    goto :goto_18
.end method

.method public runClearNatRule()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 248
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearNatRuleMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_23

    #@4
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v3, :cond_23

    #@8
    .line 249
    const/4 v3, 0x0

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    .line 251
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearNatRuleMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@f
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_12} :catch_14
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_12} :catch_19
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_12} :catch_1e

    #@12
    move-result-object v2

    #@13
    .line 265
    .end local v0           #args:[Ljava/lang/Object;
    :goto_13
    return-void

    #@14
    .line 253
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_14
    move-exception v1

    #@15
    .line 254
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@18
    goto :goto_13

    #@19
    .line 255
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_19
    move-exception v1

    #@1a
    .line 256
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@1d
    goto :goto_13

    #@1e
    .line 257
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_1e
    move-exception v1

    #@1f
    .line 258
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@22
    goto :goto_13

    #@23
    .line 264
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_23
    const-string v3, "NetworkManagementServiceProxy"

    #@25
    const-string/jumbo v4, "runClearNatRule method isn\'t implemented yet"

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_13
.end method

.method public runClearPortFilterRule()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 306
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearPortFilterRuleMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_23

    #@4
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v3, :cond_23

    #@8
    .line 307
    const/4 v3, 0x0

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    .line 309
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearPortFilterRuleMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@f
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_12} :catch_14
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_12} :catch_19
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_12} :catch_1e

    #@12
    move-result-object v2

    #@13
    .line 323
    .end local v0           #args:[Ljava/lang/Object;
    :goto_13
    return-void

    #@14
    .line 311
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_14
    move-exception v1

    #@15
    .line 312
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@18
    goto :goto_13

    #@19
    .line 313
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_19
    move-exception v1

    #@1a
    .line 314
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@1d
    goto :goto_13

    #@1e
    .line 315
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_1e
    move-exception v1

    #@1f
    .line 316
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@22
    goto :goto_13

    #@23
    .line 322
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_23
    const-string v3, "NetworkManagementServiceProxy"

    #@25
    const-string/jumbo v4, "runClearPortFilterRule method isn\'t implemented yet"

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_13
.end method

.method public runClearPortForwardRule()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 365
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearPortForwardRuleMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_23

    #@4
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v3, :cond_23

    #@8
    .line 366
    const/4 v3, 0x0

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    .line 368
    .local v0, args:[Ljava/lang/Object;
    :try_start_b
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runClearPortForwardRuleMethod:Ljava/lang/reflect/Method;

    #@d
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@f
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_12} :catch_14
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_12} :catch_19
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_12} :catch_1e

    #@12
    move-result-object v2

    #@13
    .line 382
    .end local v0           #args:[Ljava/lang/Object;
    :goto_13
    return-void

    #@14
    .line 370
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_14
    move-exception v1

    #@15
    .line 371
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@18
    goto :goto_13

    #@19
    .line 372
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_19
    move-exception v1

    #@1a
    .line 373
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@1d
    goto :goto_13

    #@1e
    .line 374
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_1e
    move-exception v1

    #@1f
    .line 375
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@22
    goto :goto_13

    #@23
    .line 381
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_23
    const-string v3, "NetworkManagementServiceProxy"

    #@25
    const-string/jumbo v4, "runClearPortForwardRule method isn\'t implemented yet"

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_13
.end method

.method public runSetNatForwardRule(Ljava/lang/String;)V
    .registers 7
    .parameter "iptablescmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 218
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetNatForwardRuleMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_26

    #@4
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v3, :cond_26

    #@8
    .line 219
    const/4 v3, 0x1

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    const/4 v3, 0x0

    #@c
    aput-object p1, v0, v3

    #@e
    .line 223
    .local v0, args:[Ljava/lang/Object;
    :try_start_e
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetNatForwardRuleMethod:Ljava/lang/reflect/Method;

    #@10
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@12
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_15} :catch_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_e .. :try_end_15} :catch_1c
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_e .. :try_end_15} :catch_21

    #@15
    move-result-object v2

    #@16
    .line 237
    .end local v0           #args:[Ljava/lang/Object;
    :goto_16
    return-void

    #@17
    .line 225
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_17
    move-exception v1

    #@18
    .line 226
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1b
    goto :goto_16

    #@1c
    .line 227
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_1c
    move-exception v1

    #@1d
    .line 228
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@20
    goto :goto_16

    #@21
    .line 229
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_21
    move-exception v1

    #@22
    .line 230
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@25
    goto :goto_16

    #@26
    .line 236
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_26
    const-string v3, "NetworkManagementServiceProxy"

    #@28
    const-string/jumbo v4, "runSetNatForwardRule method isn\'t implemented yet"

    #@2b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    goto :goto_16
.end method

.method public runSetPortFilterRule(Ljava/lang/String;I)V
    .registers 8
    .parameter "iptablescmd"
    .parameter "addORdel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 276
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetPortFilterRuleMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_2d

    #@4
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v3, :cond_2d

    #@8
    .line 277
    const/4 v3, 0x2

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    const/4 v3, 0x0

    #@c
    aput-object p1, v0, v3

    #@e
    const/4 v3, 0x1

    #@f
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v4

    #@13
    aput-object v4, v0, v3

    #@15
    .line 281
    .local v0, args:[Ljava/lang/Object;
    :try_start_15
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetPortFilterRuleMethod:Ljava/lang/reflect/Method;

    #@17
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@19
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_15 .. :try_end_1c} :catch_1e
    .catch Ljava/lang/IllegalAccessException; {:try_start_15 .. :try_end_1c} :catch_23
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_15 .. :try_end_1c} :catch_28

    #@1c
    move-result-object v2

    #@1d
    .line 295
    .end local v0           #args:[Ljava/lang/Object;
    :goto_1d
    return-void

    #@1e
    .line 283
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1e
    move-exception v1

    #@1f
    .line 284
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@22
    goto :goto_1d

    #@23
    .line 285
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_23
    move-exception v1

    #@24
    .line 286
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@27
    goto :goto_1d

    #@28
    .line 287
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_28
    move-exception v1

    #@29
    .line 288
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@2c
    goto :goto_1d

    #@2d
    .line 294
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_2d
    const-string v3, "NetworkManagementServiceProxy"

    #@2f
    const-string/jumbo v4, "runSetPortFilterRule method isn\'t implemented yet"

    #@32
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_1d
.end method

.method public runSetPortForwardRule(Ljava/lang/String;I)V
    .registers 8
    .parameter "iptablescmd"
    .parameter "addORdel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 335
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetPortForwardRuleMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v3, :cond_2d

    #@4
    iget-object v3, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v3, :cond_2d

    #@8
    .line 336
    const/4 v3, 0x2

    #@9
    new-array v0, v3, [Ljava/lang/Object;

    #@b
    const/4 v3, 0x0

    #@c
    aput-object p1, v0, v3

    #@e
    const/4 v3, 0x1

    #@f
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v4

    #@13
    aput-object v4, v0, v3

    #@15
    .line 340
    .local v0, args:[Ljava/lang/Object;
    :try_start_15
    sget-object v3, Landroid/net/wifi/NetworkManagementServiceProxy;->runSetPortForwardRuleMethod:Ljava/lang/reflect/Method;

    #@17
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@19
    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_15 .. :try_end_1c} :catch_1e
    .catch Ljava/lang/IllegalAccessException; {:try_start_15 .. :try_end_1c} :catch_23
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_15 .. :try_end_1c} :catch_28

    #@1c
    move-result-object v2

    #@1d
    .line 354
    .end local v0           #args:[Ljava/lang/Object;
    :goto_1d
    return-void

    #@1e
    .line 342
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_1e
    move-exception v1

    #@1f
    .line 343
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@22
    goto :goto_1d

    #@23
    .line 344
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_23
    move-exception v1

    #@24
    .line 345
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@27
    goto :goto_1d

    #@28
    .line 346
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_28
    move-exception v1

    #@29
    .line 347
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@2c
    goto :goto_1d

    #@2d
    .line 353
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_2d
    const-string v3, "NetworkManagementServiceProxy"

    #@2f
    const-string/jumbo v4, "runSetPortForwardRule method isn\'t implemented yet"

    #@32
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_1d
.end method

.method public setSoftapMaxClients(I)Z
    .registers 9
    .parameter "num"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 598
    sget-object v5, Landroid/net/wifi/NetworkManagementServiceProxy;->setSoftapMaxClientsMethod:Ljava/lang/reflect/Method;

    #@3
    if-eqz v5, :cond_3e

    #@5
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@7
    if-eqz v5, :cond_3e

    #@9
    .line 599
    const/4 v5, 0x1

    #@a
    new-array v0, v5, [Ljava/lang/Object;

    #@c
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v5

    #@10
    aput-object v5, v0, v4

    #@12
    .line 603
    .local v0, args:[Ljava/lang/Object;
    :try_start_12
    sget-object v5, Landroid/net/wifi/NetworkManagementServiceProxy;->setSoftapMaxClientsMethod:Ljava/lang/reflect/Method;

    #@14
    iget-object v6, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@16
    invoke-virtual {v5, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    .line 604
    .local v3, obj:Ljava/lang/Object;
    check-cast v3, Ljava/lang/Boolean;

    #@1c
    .end local v3           #obj:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_1f} :catch_21
    .catch Ljava/lang/IllegalAccessException; {:try_start_12 .. :try_end_1f} :catch_26
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_12 .. :try_end_1f} :catch_2b

    #@1f
    move-result v4

    #@20
    .line 623
    .end local v0           #args:[Ljava/lang/Object;
    :cond_20
    :goto_20
    return v4

    #@21
    .line 605
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_21
    move-exception v2

    #@22
    .line 606
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@25
    goto :goto_20

    #@26
    .line 607
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_26
    move-exception v2

    #@27
    .line 608
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@2a
    goto :goto_20

    #@2b
    .line 609
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_2b
    move-exception v2

    #@2c
    .line 610
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@2f
    move-result-object v1

    #@30
    .line 611
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v5, v1, Landroid/os/RemoteException;

    #@32
    if-eqz v5, :cond_37

    #@34
    .line 612
    check-cast v1, Landroid/os/RemoteException;

    #@36
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@37
    .line 614
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_37
    instance-of v5, v1, Ljava/lang/IllegalStateException;

    #@39
    if-eqz v5, :cond_20

    #@3b
    .line 615
    check-cast v1, Ljava/lang/IllegalStateException;

    #@3d
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@3e
    .line 622
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_3e
    const-string v5, "NetworkManagementServiceProxy"

    #@40
    const-string/jumbo v6, "setSoftapMaxClients method isn\'t implemented yet"

    #@43
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_20
.end method

.method public startTetheringWithFullOption([Ljava/lang/String;)V
    .registers 8
    .parameter "dhcpOption"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 145
    sget-object v4, Landroid/net/wifi/NetworkManagementServiceProxy;->startTetheringWithFullOptionMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v4, :cond_34

    #@4
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v4, :cond_34

    #@8
    .line 146
    const/4 v4, 0x1

    #@9
    new-array v0, v4, [Ljava/lang/Object;

    #@b
    const/4 v4, 0x0

    #@c
    aput-object p1, v0, v4

    #@e
    .line 150
    .local v0, args:[Ljava/lang/Object;
    :try_start_e
    sget-object v4, Landroid/net/wifi/NetworkManagementServiceProxy;->startTetheringWithFullOptionMethod:Ljava/lang/reflect/Method;

    #@10
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@12
    invoke-virtual {v4, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_15} :catch_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_e .. :try_end_15} :catch_1c
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_e .. :try_end_15} :catch_21

    #@15
    move-result-object v3

    #@16
    .line 170
    .end local v0           #args:[Ljava/lang/Object;
    :cond_16
    :goto_16
    return-void

    #@17
    .line 152
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_17
    move-exception v2

    #@18
    .line 153
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1b
    goto :goto_16

    #@1c
    .line 154
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_1c
    move-exception v2

    #@1d
    .line 155
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@20
    goto :goto_16

    #@21
    .line 156
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_21
    move-exception v2

    #@22
    .line 157
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@25
    move-result-object v1

    #@26
    .line 158
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v4, v1, Landroid/os/RemoteException;

    #@28
    if-eqz v4, :cond_2d

    #@2a
    .line 159
    check-cast v1, Landroid/os/RemoteException;

    #@2c
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@2d
    .line 161
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_2d
    instance-of v4, v1, Ljava/lang/IllegalStateException;

    #@2f
    if-eqz v4, :cond_16

    #@31
    .line 162
    check-cast v1, Ljava/lang/IllegalStateException;

    #@33
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@34
    .line 169
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_34
    const-string v4, "NetworkManagementServiceProxy"

    #@36
    const-string/jumbo v5, "startTetheringWithFullOption method isn\'t implemented yet"

    #@39
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_16
.end method

.method public startVZWAccessPoint(Landroid/net/wifi/WifiVZWConfigurationProxy;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "wifiConfig"
    .parameter "wlanIface"
    .parameter "softapIface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 476
    sget-object v4, Landroid/net/wifi/NetworkManagementServiceProxy;->startVZWAccessPointMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v4, :cond_e2

    #@4
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v4, :cond_e2

    #@8
    .line 477
    const/16 v4, 0x14

    #@a
    new-array v0, v4, [Ljava/lang/Object;

    #@c
    const/4 v4, 0x0

    #@d
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getSSIDField()Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    aput-object v5, v0, v4

    #@13
    const/4 v4, 0x1

    #@14
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getAuthType()I

    #@17
    move-result v5

    #@18
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v5

    #@1c
    aput-object v5, v0, v4

    #@1e
    const/4 v4, 0x2

    #@1f
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getwepTxKeyIndexField()I

    #@22
    move-result v5

    #@23
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v5

    #@27
    aput-object v5, v0, v4

    #@29
    const/4 v4, 0x3

    #@2a
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getwepKeysField1()Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    aput-object v5, v0, v4

    #@30
    const/4 v4, 0x4

    #@31
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getwepKeysField2()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    aput-object v5, v0, v4

    #@37
    const/4 v4, 0x5

    #@38
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getwepKeysField3()Ljava/lang/String;

    #@3b
    move-result-object v5

    #@3c
    aput-object v5, v0, v4

    #@3e
    const/4 v4, 0x6

    #@3f
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getwepKeysField4()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    aput-object v5, v0, v4

    #@45
    const/4 v4, 0x7

    #@46
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getpreSharedKeyField()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    aput-object v5, v0, v4

    #@4c
    const/16 v4, 0x8

    #@4e
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->gethiddenSSIDField()Z

    #@51
    move-result v5

    #@52
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@55
    move-result-object v5

    #@56
    aput-object v5, v0, v4

    #@58
    const/16 v4, 0x9

    #@5a
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getChannelField()I

    #@5d
    move-result v5

    #@5e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61
    move-result-object v5

    #@62
    aput-object v5, v0, v4

    #@64
    const/16 v4, 0xa

    #@66
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getMaxscbField()I

    #@69
    move-result v5

    #@6a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d
    move-result-object v5

    #@6e
    aput-object v5, v0, v4

    #@70
    const/16 v4, 0xb

    #@72
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->gethw_modeField()Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    aput-object v5, v0, v4

    #@78
    const/16 v4, 0xc

    #@7a
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getcountryCodeField()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    aput-object v5, v0, v4

    #@80
    const/16 v4, 0xd

    #@82
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getap_isolateField()I

    #@85
    move-result v5

    #@86
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@89
    move-result-object v5

    #@8a
    aput-object v5, v0, v4

    #@8c
    const/16 v4, 0xe

    #@8e
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getieee_modeField()I

    #@91
    move-result v5

    #@92
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@95
    move-result-object v5

    #@96
    aput-object v5, v0, v4

    #@98
    const/16 v4, 0xf

    #@9a
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getmacaddr_aclField()I

    #@9d
    move-result v5

    #@9e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a1
    move-result-object v5

    #@a2
    aput-object v5, v0, v4

    #@a4
    const/16 v4, 0x10

    #@a6
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getaccept_mac_fileField()Ljava/lang/String;

    #@a9
    move-result-object v5

    #@aa
    aput-object v5, v0, v4

    #@ac
    const/16 v4, 0x11

    #@ae
    invoke-virtual {p1}, Landroid/net/wifi/WifiVZWConfigurationProxy;->getdeny_mac_fileField()Ljava/lang/String;

    #@b1
    move-result-object v5

    #@b2
    aput-object v5, v0, v4

    #@b4
    const/16 v4, 0x12

    #@b6
    aput-object p2, v0, v4

    #@b8
    const/16 v4, 0x13

    #@ba
    aput-object p3, v0, v4

    #@bc
    .line 486
    .local v0, args:[Ljava/lang/Object;
    :try_start_bc
    sget-object v4, Landroid/net/wifi/NetworkManagementServiceProxy;->startVZWAccessPointMethod:Ljava/lang/reflect/Method;

    #@be
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@c0
    invoke-virtual {v4, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_bc .. :try_end_c3} :catch_c5
    .catch Ljava/lang/IllegalAccessException; {:try_start_bc .. :try_end_c3} :catch_ca
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_bc .. :try_end_c3} :catch_cf

    #@c3
    move-result-object v3

    #@c4
    .line 506
    .end local v0           #args:[Ljava/lang/Object;
    :cond_c4
    :goto_c4
    return-void

    #@c5
    .line 488
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_c5
    move-exception v2

    #@c6
    .line 489
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@c9
    goto :goto_c4

    #@ca
    .line 490
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_ca
    move-exception v2

    #@cb
    .line 491
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@ce
    goto :goto_c4

    #@cf
    .line 492
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_cf
    move-exception v2

    #@d0
    .line 493
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@d3
    move-result-object v1

    #@d4
    .line 494
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v4, v1, Landroid/os/RemoteException;

    #@d6
    if-eqz v4, :cond_db

    #@d8
    .line 495
    check-cast v1, Landroid/os/RemoteException;

    #@da
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@db
    .line 497
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_db
    instance-of v4, v1, Ljava/lang/IllegalStateException;

    #@dd
    if-eqz v4, :cond_c4

    #@df
    .line 498
    check-cast v1, Ljava/lang/IllegalStateException;

    #@e1
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@e2
    .line 505
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_e2
    const-string v4, "NetworkManagementServiceProxy"

    #@e4
    const-string/jumbo v5, "startVZWAccessPoint method isn\'t implemented yet"

    #@e7
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    goto :goto_c4
.end method

.method public stopVZWAccessPoint(Ljava/lang/String;)V
    .registers 8
    .parameter "wlanIface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 520
    sget-object v4, Landroid/net/wifi/NetworkManagementServiceProxy;->stopVZWAccessPointMethod:Ljava/lang/reflect/Method;

    #@2
    if-eqz v4, :cond_34

    #@4
    iget-object v4, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@6
    if-eqz v4, :cond_34

    #@8
    .line 521
    const/4 v4, 0x1

    #@9
    new-array v0, v4, [Ljava/lang/Object;

    #@b
    const/4 v4, 0x0

    #@c
    aput-object p1, v0, v4

    #@e
    .line 525
    .local v0, args:[Ljava/lang/Object;
    :try_start_e
    sget-object v4, Landroid/net/wifi/NetworkManagementServiceProxy;->stopVZWAccessPointMethod:Ljava/lang/reflect/Method;

    #@10
    iget-object v5, p0, Landroid/net/wifi/NetworkManagementServiceProxy;->mNwService:Landroid/os/INetworkManagementService;

    #@12
    invoke-virtual {v4, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_15} :catch_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_e .. :try_end_15} :catch_1c
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_e .. :try_end_15} :catch_21

    #@15
    move-result-object v3

    #@16
    .line 545
    .end local v0           #args:[Ljava/lang/Object;
    :cond_16
    :goto_16
    return-void

    #@17
    .line 527
    .restart local v0       #args:[Ljava/lang/Object;
    :catch_17
    move-exception v2

    #@18
    .line 528
    .local v2, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@1b
    goto :goto_16

    #@1c
    .line 529
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_1c
    move-exception v2

    #@1d
    .line 530
    .local v2, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@20
    goto :goto_16

    #@21
    .line 531
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_21
    move-exception v2

    #@22
    .line 532
    .local v2, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@25
    move-result-object v1

    #@26
    .line 533
    .local v1, cause:Ljava/lang/Throwable;
    instance-of v4, v1, Landroid/os/RemoteException;

    #@28
    if-eqz v4, :cond_2d

    #@2a
    .line 534
    check-cast v1, Landroid/os/RemoteException;

    #@2c
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@2d
    .line 536
    .restart local v1       #cause:Ljava/lang/Throwable;
    :cond_2d
    instance-of v4, v1, Ljava/lang/IllegalStateException;

    #@2f
    if-eqz v4, :cond_16

    #@31
    .line 537
    check-cast v1, Ljava/lang/IllegalStateException;

    #@33
    .end local v1           #cause:Ljava/lang/Throwable;
    throw v1

    #@34
    .line 544
    .end local v0           #args:[Ljava/lang/Object;
    .end local v2           #e:Ljava/lang/reflect/InvocationTargetException;
    :cond_34
    const-string v4, "NetworkManagementServiceProxy"

    #@36
    const-string/jumbo v5, "stopVZWAccessPointPoint method isn\'t implemented yet"

    #@39
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_16
.end method
