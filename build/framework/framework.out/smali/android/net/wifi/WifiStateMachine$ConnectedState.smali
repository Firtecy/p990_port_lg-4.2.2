.class Landroid/net/wifi/WifiStateMachine$ConnectedState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectedState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4037
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 4041
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$ConnectedState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 4042
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 4043
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@10
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$16200(Landroid/net/wifi/WifiStateMachine;)V

    #@13
    .line 4045
    :cond_13
    return-void
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 4111
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$16400(Landroid/net/wifi/WifiStateMachine;)V

    #@5
    .line 4112
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$16500(Landroid/net/wifi/WifiStateMachine;)Landroid/net/ConnectivityManager;

    #@a
    move-result-object v0

    #@b
    const-string v1, "WifiStateMachine"

    #@d
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->requestNetworkTransitionWakelock(Ljava/lang/String;)Z

    #@10
    .line 4113
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "message"

    #@0
    .prologue
    .line 4049
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v2, :sswitch_data_de

    #@5
    .line 4104
    const/4 v2, 0x0

    #@6
    .line 4106
    :goto_6
    return v2

    #@7
    .line 4053
    :sswitch_7
    :try_start_7
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->disableIpv6(Ljava/lang/String;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_16} :catch_46
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_16} :catch_60

    #@16
    .line 4060
    :goto_16
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@18
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@1a
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$9300(Landroid/net/wifi/WifiStateMachine;Landroid/net/NetworkInfo$DetailedState;)V

    #@1d
    .line 4061
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1f
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@22
    move-result-object v2

    #@23
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@25
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$5300(Landroid/net/wifi/WifiStateMachine;)I

    #@28
    move-result v3

    #@29
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@2b
    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/WifiConfigStore;->updateStatus(ILandroid/net/NetworkInfo$DetailedState;)V

    #@2e
    .line 4062
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@30
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@32
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$5200(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$13000(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@39
    .line 4064
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3b
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3d
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$14000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$16300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@44
    .line 4106
    :cond_44
    :goto_44
    const/4 v2, 0x1

    #@45
    goto :goto_6

    #@46
    .line 4054
    :catch_46
    move-exception v1

    #@47
    .line 4055
    .local v1, re:Landroid/os/RemoteException;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@49
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "Failed to disable IPv6: "

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@5f
    goto :goto_16

    #@60
    .line 4056
    .end local v1           #re:Landroid/os/RemoteException;
    :catch_60
    move-exception v0

    #@61
    .line 4057
    .local v0, e:Ljava/lang/IllegalStateException;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@63
    new-instance v3, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v4, "Failed to disable IPv6: "

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@79
    goto :goto_16

    #@7a
    .line 4067
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :sswitch_7a
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@7c
    if-eqz v2, :cond_44

    #@7e
    .line 4068
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@80
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@82
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@84
    check-cast v2, Ljava/lang/String;

    #@86
    invoke-static {v3, v4, v2}, Landroid/net/wifi/WifiStateMachine;->access$15900(Landroid/net/wifi/WifiStateMachine;ILjava/lang/String;)V

    #@89
    goto :goto_44

    #@8a
    .line 4073
    :sswitch_8a
    const-string v2, "WifiStateMachine"

    #@8c
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_AP_EVENT"

    #@8e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    goto :goto_44

    #@92
    .line 4076
    :sswitch_92
    const-string v2, "WifiStateMachine"

    #@94
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_NO_MATCH_EVENT"

    #@96
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    goto :goto_44

    #@9a
    .line 4079
    :sswitch_9a
    const-string v2, "WifiStateMachine"

    #@9c
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_GAS_RESP_INFO_EVENT"

    #@9e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto :goto_44

    #@a2
    .line 4082
    :sswitch_a2
    const-string v2, "WifiStateMachine"

    #@a4
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_ANQP_FETCH_START_EVENT"

    #@a6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    goto :goto_44

    #@aa
    .line 4085
    :sswitch_aa
    const-string v2, "WifiStateMachine"

    #@ac
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_ANQP_FETCH_SUCCESS_EVENT"

    #@ae
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    goto :goto_44

    #@b2
    .line 4088
    :sswitch_b2
    const-string v2, "WifiStateMachine"

    #@b4
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_ANQP_RX_DATA_EVENT"

    #@b6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b9
    goto :goto_44

    #@ba
    .line 4091
    :sswitch_ba
    const-string v2, "WifiStateMachine"

    #@bc
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_RX_DATA_EVENT"

    #@be
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    goto :goto_44

    #@c2
    .line 4094
    :sswitch_c2
    const-string v2, "WifiStateMachine"

    #@c4
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_ANQP_3GPP_CONNECT_EVENT"

    #@c6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c9
    goto/16 :goto_44

    #@cb
    .line 4097
    :sswitch_cb
    const-string v2, "WifiStateMachine"

    #@cd
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_ANQP_RC_CONNECT_EVENT"

    #@cf
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    goto/16 :goto_44

    #@d4
    .line 4100
    :sswitch_d4
    const-string v2, "WifiStateMachine"

    #@d6
    const-string v3, " [PASSPOINT][ConnectedState] : HS20_ANQP_TLS_CONNECT_EVENT"

    #@d8
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    goto/16 :goto_44

    #@dd
    .line 4049
    nop

    #@de
    :sswitch_data_de
    .sparse-switch
        0x21015 -> :sswitch_7
        0x24003 -> :sswitch_7a
        0x2402c -> :sswitch_8a
        0x2402d -> :sswitch_92
        0x2402e -> :sswitch_9a
        0x2402f -> :sswitch_a2
        0x24030 -> :sswitch_aa
        0x24031 -> :sswitch_b2
        0x24032 -> :sswitch_ba
        0x24033 -> :sswitch_c2
        0x24034 -> :sswitch_cb
        0x24035 -> :sswitch_d4
    .end sparse-switch
.end method
