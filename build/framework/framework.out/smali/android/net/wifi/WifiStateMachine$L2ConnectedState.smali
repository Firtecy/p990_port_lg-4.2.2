.class Landroid/net/wifi/WifiStateMachine$L2ConnectedState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "L2ConnectedState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3727
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    .line 3731
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 3732
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$13708(Landroid/net/wifi/WifiStateMachine;)I

    #@f
    .line 3733
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1100(Landroid/net/wifi/WifiStateMachine;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_2c

    #@17
    .line 3734
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@19
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1b
    const v2, 0x20053

    #@1e
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@20
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$13700(Landroid/net/wifi/WifiStateMachine;)I

    #@23
    move-result v3

    #@24
    const/4 v4, 0x0

    #@25
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@2c
    .line 3736
    :cond_2c
    return-void
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 3900
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$7600(Landroid/net/wifi/WifiStateMachine;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 3901
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@d
    move-result-object v0

    #@e
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiNative;->setScanResultHandling(I)Z

    #@12
    .line 3903
    :cond_12
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const-wide/16 v10, 0xbb8

    #@2
    const v9, 0x20053

    #@5
    const/4 v7, 0x2

    #@6
    const/4 v5, 0x1

    #@7
    const/4 v6, 0x0

    #@8
    .line 3741
    iget v4, p1, Landroid/os/Message;->what:I

    #@a
    sparse-switch v4, :sswitch_data_214

    #@d
    .line 3892
    :cond_d
    :goto_d
    return v6

    #@e
    .line 3743
    :sswitch_e
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@10
    invoke-virtual {v4}, Landroid/net/wifi/WifiStateMachine;->handlePreDhcpSetup()V

    #@13
    .line 3745
    const-string v4, "WifiStateMachine"

    #@15
    const-string v6, "CheckDhcpInfoCacheList : DhcpStateMachine.CMD_PRE_DHCP_ACTION[ConnectingState]"

    #@17
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 3748
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1400(Landroid/net/wifi/WifiStateMachine;)Landroid/net/DhcpStateMachine;

    #@1f
    move-result-object v4

    #@20
    if-eqz v4, :cond_3d

    #@22
    .line 3749
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@24
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1400(Landroid/net/wifi/WifiStateMachine;)Landroid/net/DhcpStateMachine;

    #@27
    move-result-object v4

    #@28
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2a
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$13800(Landroid/net/wifi/WifiStateMachine;)Landroid/util/LruCache;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v4, v6}, Landroid/net/DhcpStateMachine;->CheckDhcpInfoCacheList(Landroid/util/LruCache;)Z

    #@31
    .line 3751
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@33
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1400(Landroid/net/wifi/WifiStateMachine;)Landroid/net/DhcpStateMachine;

    #@36
    move-result-object v4

    #@37
    const v6, 0x30007

    #@3a
    invoke-virtual {v4, v6}, Landroid/net/DhcpStateMachine;->sendMessage(I)V

    #@3d
    :cond_3d
    :goto_3d
    :sswitch_3d
    move v6, v5

    #@3e
    .line 3892
    goto :goto_d

    #@3f
    .line 3755
    :sswitch_3f
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@41
    invoke-virtual {v4}, Landroid/net/wifi/WifiStateMachine;->handlePostDhcpSetup()V

    #@44
    .line 3756
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@46
    if-ne v4, v5, :cond_5d

    #@48
    .line 3758
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4a
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4c
    check-cast v4, Landroid/net/DhcpInfoInternal;

    #@4e
    invoke-static {v6, v4}, Landroid/net/wifi/WifiStateMachine;->access$13900(Landroid/net/wifi/WifiStateMachine;Landroid/net/DhcpInfoInternal;)V

    #@51
    .line 3759
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@53
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@55
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$14000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@58
    move-result-object v6

    #@59
    invoke-static {v4, v6}, Landroid/net/wifi/WifiStateMachine;->access$14100(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@5c
    goto :goto_3d

    #@5d
    .line 3760
    :cond_5d
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@5f
    if-ne v4, v7, :cond_3d

    #@61
    .line 3762
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@63
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$14200(Landroid/net/wifi/WifiStateMachine;)V

    #@66
    .line 3763
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@68
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@6a
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$12500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@6d
    move-result-object v6

    #@6e
    invoke-static {v4, v6}, Landroid/net/wifi/WifiStateMachine;->access$14300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@71
    goto :goto_3d

    #@72
    .line 3767
    :sswitch_72
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@74
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@7b
    .line 3768
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7d
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7f
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$12500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@82
    move-result-object v6

    #@83
    invoke-static {v4, v6}, Landroid/net/wifi/WifiStateMachine;->access$14400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@86
    goto :goto_3d

    #@87
    .line 3771
    :sswitch_87
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@89
    if-ne v4, v5, :cond_3d

    #@8b
    .line 3772
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@8d
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@94
    .line 3773
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@96
    invoke-static {v4, v5}, Landroid/net/wifi/WifiStateMachine;->access$1702(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@99
    .line 3774
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9b
    iget-object v6, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9d
    invoke-static {v6}, Landroid/net/wifi/WifiStateMachine;->access$12500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@a0
    move-result-object v6

    #@a1
    invoke-static {v4, v6}, Landroid/net/wifi/WifiStateMachine;->access$14500(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@a4
    goto :goto_3d

    #@a5
    .line 3778
    :sswitch_a5
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@a7
    if-ne v4, v7, :cond_3d

    #@a9
    .line 3779
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ab
    const v6, 0x2004a

    #@ae
    invoke-virtual {v4, v6}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@b1
    .line 3780
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b3
    invoke-static {v4, p1}, Landroid/net/wifi/WifiStateMachine;->access$14600(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@b6
    goto :goto_3d

    #@b7
    .line 3790
    :sswitch_b7
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b9
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@bc
    move-result-object v4

    #@bd
    invoke-virtual {v4, v7}, Landroid/net/wifi/WifiNative;->setScanResultHandling(I)Z

    #@c0
    goto/16 :goto_d

    #@c2
    .line 3796
    :sswitch_c2
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@c4
    .line 3797
    .local v2, netId:I
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c6
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$5600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiInfo;

    #@c9
    move-result-object v4

    #@ca
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@cd
    move-result v4

    #@ce
    if-ne v4, v2, :cond_d

    #@d0
    goto/16 :goto_3d

    #@d2
    .line 3802
    .end local v2           #netId:I
    :sswitch_d2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d4
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@d6
    .line 3803
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d8
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$5700(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiConfigStore;

    #@db
    move-result-object v4

    #@dc
    invoke-virtual {v4, v0}, Landroid/net/wifi/WifiConfigStore;->saveNetwork(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/NetworkUpdateResult;

    #@df
    move-result-object v3

    #@e0
    .line 3804
    .local v3, result:Landroid/net/wifi/NetworkUpdateResult;
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e2
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$5600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiInfo;

    #@e5
    move-result-object v4

    #@e6
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    #@e9
    move-result v4

    #@ea
    invoke-virtual {v3}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@ed
    move-result v7

    #@ee
    if-ne v4, v7, :cond_11f

    #@f0
    .line 3805
    invoke-virtual {v3}, Landroid/net/wifi/NetworkUpdateResult;->hasIpChanged()Z

    #@f3
    move-result v4

    #@f4
    if-eqz v4, :cond_108

    #@f6
    .line 3806
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f8
    const-string v7, "Reconfiguring IP on connection"

    #@fa
    invoke-static {v4, v7}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@fd
    .line 3807
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ff
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@101
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$13100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@104
    move-result-object v7

    #@105
    invoke-static {v4, v7}, Landroid/net/wifi/WifiStateMachine;->access$14700(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@108
    .line 3809
    :cond_108
    invoke-virtual {v3}, Landroid/net/wifi/NetworkUpdateResult;->hasProxyChanged()Z

    #@10b
    move-result v4

    #@10c
    if-eqz v4, :cond_11f

    #@10e
    .line 3810
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@110
    const-string v7, "Reconfiguring proxy on connection"

    #@112
    invoke-static {v4, v7}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@115
    .line 3811
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@117
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$14800(Landroid/net/wifi/WifiStateMachine;)V

    #@11a
    .line 3812
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11c
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$14900(Landroid/net/wifi/WifiStateMachine;)V

    #@11f
    .line 3816
    :cond_11f
    invoke-virtual {v3}, Landroid/net/wifi/NetworkUpdateResult;->getNetworkId()I

    #@122
    move-result v4

    #@123
    const/4 v7, -0x1

    #@124
    if-eq v4, v7, :cond_130

    #@126
    .line 3817
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@128
    const v6, 0x25009

    #@12b
    invoke-static {v4, p1, v6}, Landroid/net/wifi/WifiStateMachine;->access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V

    #@12e
    goto/16 :goto_3d

    #@130
    .line 3819
    :cond_130
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@132
    const-string v7, "Failed to save network"

    #@134
    invoke-static {v4, v7}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@137
    .line 3820
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@139
    const v7, 0x25008

    #@13c
    invoke-static {v4, p1, v7, v6}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@13f
    goto/16 :goto_3d

    #@141
    .line 3828
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v3           #result:Landroid/net/wifi/NetworkUpdateResult;
    :sswitch_141
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@143
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@145
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$13700(Landroid/net/wifi/WifiStateMachine;)I

    #@148
    move-result v7

    #@149
    if-ne v4, v7, :cond_3d

    #@14b
    .line 3830
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@14d
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$15000(Landroid/net/wifi/WifiStateMachine;)V

    #@150
    .line 3831
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@152
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@154
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@156
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$13700(Landroid/net/wifi/WifiStateMachine;)I

    #@159
    move-result v8

    #@15a
    invoke-virtual {v7, v9, v8, v6}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@15d
    move-result-object v6

    #@15e
    invoke-virtual {v4, v6, v10, v11}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@161
    goto/16 :goto_3d

    #@163
    .line 3838
    :sswitch_163
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@165
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@167
    if-ne v4, v5, :cond_192

    #@169
    move v4, v5

    #@16a
    :goto_16a
    invoke-static {v7, v4}, Landroid/net/wifi/WifiStateMachine;->access$1102(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@16d
    .line 3839
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@16f
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$13708(Landroid/net/wifi/WifiStateMachine;)I

    #@172
    .line 3840
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@174
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$1100(Landroid/net/wifi/WifiStateMachine;)Z

    #@177
    move-result v4

    #@178
    if-eqz v4, :cond_3d

    #@17a
    .line 3842
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@17c
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$15000(Landroid/net/wifi/WifiStateMachine;)V

    #@17f
    .line 3843
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@181
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@183
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@185
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$13700(Landroid/net/wifi/WifiStateMachine;)I

    #@188
    move-result v8

    #@189
    invoke-virtual {v7, v9, v8, v6}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@18c
    move-result-object v6

    #@18d
    invoke-virtual {v4, v6, v10, v11}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@190
    goto/16 :goto_3d

    #@192
    :cond_192
    move v4, v6

    #@193
    .line 3838
    goto :goto_16a

    #@194
    .line 3848
    :sswitch_194
    new-instance v1, Landroid/net/wifi/RssiPacketCountInfo;

    #@196
    invoke-direct {v1}, Landroid/net/wifi/RssiPacketCountInfo;-><init>()V

    #@199
    .line 3849
    .local v1, info:Landroid/net/wifi/RssiPacketCountInfo;
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@19b
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$15000(Landroid/net/wifi/WifiStateMachine;)V

    #@19e
    .line 3850
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1a0
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$5600(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiInfo;

    #@1a3
    move-result-object v4

    #@1a4
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getRssi()I

    #@1a7
    move-result v4

    #@1a8
    iput v4, v1, Landroid/net/wifi/RssiPacketCountInfo;->rssi:I

    #@1aa
    .line 3851
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1ac
    invoke-static {v4, v1}, Landroid/net/wifi/WifiStateMachine;->access$15100(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/RssiPacketCountInfo;)V

    #@1af
    .line 3852
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$L2ConnectedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1b1
    const v6, 0x25015

    #@1b4
    invoke-static {v4, p1, v6, v1}, Landroid/net/wifi/WifiStateMachine;->access$1000(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@1b7
    goto/16 :goto_3d

    #@1b9
    .line 3857
    .end local v1           #info:Landroid/net/wifi/RssiPacketCountInfo;
    :sswitch_1b9
    const-string v4, "WifiStateMachine"

    #@1bb
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_AP_EVENT"

    #@1bd
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c0
    goto/16 :goto_3d

    #@1c2
    .line 3860
    :sswitch_1c2
    const-string v4, "WifiStateMachine"

    #@1c4
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_NO_MATCH_EVENT"

    #@1c6
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c9
    goto/16 :goto_3d

    #@1cb
    .line 3863
    :sswitch_1cb
    const-string v4, "WifiStateMachine"

    #@1cd
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_GAS_RESP_INFO_EVENT"

    #@1cf
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d2
    goto/16 :goto_3d

    #@1d4
    .line 3866
    :sswitch_1d4
    const-string v4, "WifiStateMachine"

    #@1d6
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_ANQP_FETCH_START_EVENT"

    #@1d8
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1db
    goto/16 :goto_3d

    #@1dd
    .line 3869
    :sswitch_1dd
    const-string v4, "WifiStateMachine"

    #@1df
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_ANQP_FETCH_SUCCESS_EVENT"

    #@1e1
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e4
    goto/16 :goto_3d

    #@1e6
    .line 3872
    :sswitch_1e6
    const-string v4, "WifiStateMachine"

    #@1e8
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_ANQP_RX_DATA_EVENT"

    #@1ea
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ed
    goto/16 :goto_3d

    #@1ef
    .line 3875
    :sswitch_1ef
    const-string v4, "WifiStateMachine"

    #@1f1
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_RX_DATA_EVENT"

    #@1f3
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f6
    goto/16 :goto_3d

    #@1f8
    .line 3878
    :sswitch_1f8
    const-string v4, "WifiStateMachine"

    #@1fa
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_ANQP_3GPP_CONNECT_EVENT"

    #@1fc
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ff
    goto/16 :goto_3d

    #@201
    .line 3881
    :sswitch_201
    const-string v4, "WifiStateMachine"

    #@203
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_ANQP_RC_CONNECT_EVENT"

    #@205
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@208
    goto/16 :goto_3d

    #@20a
    .line 3884
    :sswitch_20a
    const-string v4, "WifiStateMachine"

    #@20c
    const-string v6, " [PASSPOINT][L2ConnectedState] : HS20_ANQP_TLS_CONNECT_EVENT"

    #@20e
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@211
    goto/16 :goto_3d

    #@213
    .line 3741
    nop

    #@214
    :sswitch_data_214
    .sparse-switch
        0x20047 -> :sswitch_b7
        0x20048 -> :sswitch_a5
        0x2004a -> :sswitch_72
        0x20052 -> :sswitch_163
        0x20053 -> :sswitch_141
        0x2300c -> :sswitch_87
        0x24003 -> :sswitch_3d
        0x2402c -> :sswitch_1b9
        0x2402d -> :sswitch_1c2
        0x2402e -> :sswitch_1cb
        0x2402f -> :sswitch_1d4
        0x24030 -> :sswitch_1dd
        0x24031 -> :sswitch_1e6
        0x24032 -> :sswitch_1ef
        0x24033 -> :sswitch_1f8
        0x24034 -> :sswitch_201
        0x24035 -> :sswitch_20a
        0x25001 -> :sswitch_c2
        0x25007 -> :sswitch_d2
        0x25014 -> :sswitch_194
        0x30004 -> :sswitch_e
        0x30005 -> :sswitch_3f
    .end sparse-switch
.end method
