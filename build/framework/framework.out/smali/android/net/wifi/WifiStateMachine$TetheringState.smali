.class Landroid/net/wifi/WifiStateMachine$TetheringState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TetheringState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4612
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    .line 4616
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$TetheringState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 4619
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e
    const v2, 0x2001e

    #@11
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$19304(Landroid/net/wifi/WifiStateMachine;)I

    #@16
    move-result v3

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@1b
    move-result-object v1

    #@1c
    const-wide/16 v2, 0x1388

    #@1e
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@21
    .line 4621
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "message"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 4625
    iget v3, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v3, :sswitch_data_42

    #@7
    move v1, v2

    #@8
    .line 4664
    :cond_8
    :goto_8
    return v1

    #@9
    .line 4627
    :sswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    check-cast v0, Landroid/net/wifi/WifiStateMachine$TetherStateChange;

    #@d
    .line 4628
    .local v0, stateChange:Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f
    iget-object v3, v0, Landroid/net/wifi/WifiStateMachine$TetherStateChange;->active:Ljava/util/ArrayList;

    #@11
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$19400(Landroid/net/wifi/WifiStateMachine;Ljava/util/ArrayList;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_8

    #@17
    .line 4629
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@19
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1b
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$19500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$19600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@22
    goto :goto_8

    #@23
    .line 4633
    .end local v0           #stateChange:Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    :sswitch_23
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@25
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@27
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$19300(Landroid/net/wifi/WifiStateMachine;)I

    #@2a
    move-result v4

    #@2b
    if-ne v3, v4, :cond_8

    #@2d
    .line 4634
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2f
    const-string v4, "Failed to get tether update, shutdown soft access point"

    #@31
    invoke-static {v3, v4}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@34
    .line 4635
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@36
    const/4 v4, 0x0

    #@37
    invoke-virtual {v3, v4, v2}, Landroid/net/wifi/WifiStateMachine;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V

    #@3a
    goto :goto_8

    #@3b
    .line 4659
    :sswitch_3b
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$TetheringState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3d
    invoke-static {v2, p1}, Landroid/net/wifi/WifiStateMachine;->access$19700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@40
    goto :goto_8

    #@41
    .line 4625
    nop

    #@42
    :sswitch_data_42
    .sparse-switch
        0x20001 -> :sswitch_3b
        0x20002 -> :sswitch_3b
        0x2000b -> :sswitch_3b
        0x2000c -> :sswitch_3b
        0x2000d -> :sswitch_3b
        0x2000e -> :sswitch_3b
        0x20015 -> :sswitch_3b
        0x20018 -> :sswitch_3b
        0x2001d -> :sswitch_9
        0x2001e -> :sswitch_23
        0x20048 -> :sswitch_3b
        0x20049 -> :sswitch_3b
        0x20050 -> :sswitch_3b
        0x20054 -> :sswitch_3b
        0x20055 -> :sswitch_3b
        0x2005a -> :sswitch_3b
        0x20086 -> :sswitch_3b
        0x20089 -> :sswitch_3b
    .end sparse-switch
.end method
