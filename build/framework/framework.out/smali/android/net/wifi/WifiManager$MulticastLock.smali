.class public Landroid/net/wifi/WifiManager$MulticastLock;
.super Ljava/lang/Object;
.source "WifiManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MulticastLock"
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mHeld:Z

.field private mRefCount:I

.field private mRefCounted:Z

.field private mTag:Ljava/lang/String;

.field final synthetic this$0:Landroid/net/wifi/WifiManager;


# direct methods
.method private constructor <init>(Landroid/net/wifi/WifiManager;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1868
    iput-object p1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 1869
    iput-object p2, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mTag:Ljava/lang/String;

    #@8
    .line 1870
    new-instance v0, Landroid/os/Binder;

    #@a
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@d
    iput-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mBinder:Landroid/os/IBinder;

    #@f
    .line 1871
    iput v1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I

    #@11
    .line 1872
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCounted:Z

    #@14
    .line 1873
    iput-boolean v1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z

    #@16
    .line 1874
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/wifi/WifiManager;Ljava/lang/String;Landroid/net/wifi/WifiManager$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 1861
    invoke-direct {p0, p1, p2}, Landroid/net/wifi/WifiManager$MulticastLock;-><init>(Landroid/net/wifi/WifiManager;Ljava/lang/String;)V

    #@3
    return-void
.end method


# virtual methods
.method public acquire()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1897
    iget-object v1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mBinder:Landroid/os/IBinder;

    #@3
    monitor-enter v1

    #@4
    .line 1898
    :try_start_4
    iget-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCounted:Z

    #@6
    if-eqz v0, :cond_40

    #@8
    iget v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I

    #@a
    add-int/lit8 v0, v0, 0x1

    #@c
    iput v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_4c

    #@e
    if-ne v0, v2, :cond_3e

    #@10
    .line 1900
    :goto_10
    :try_start_10
    iget-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@12
    iget-object v0, v0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@14
    iget-object v2, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mBinder:Landroid/os/IBinder;

    #@16
    iget-object v3, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mTag:Ljava/lang/String;

    #@18
    invoke-interface {v0, v2, v3}, Landroid/net/wifi/IWifiManager;->acquireMulticastLock(Landroid/os/IBinder;Ljava/lang/String;)V

    #@1b
    .line 1901
    iget-object v2, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@1d
    monitor-enter v2
    :try_end_1e
    .catchall {:try_start_10 .. :try_end_1e} :catchall_4c
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_1e} :catch_3a

    #@1e
    .line 1902
    :try_start_1e
    iget-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@20
    invoke-static {v0}, Landroid/net/wifi/WifiManager;->access$500(Landroid/net/wifi/WifiManager;)I

    #@23
    move-result v0

    #@24
    const/16 v3, 0x32

    #@26
    if-lt v0, v3, :cond_45

    #@28
    .line 1903
    iget-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@2a
    iget-object v0, v0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@2c
    invoke-interface {v0}, Landroid/net/wifi/IWifiManager;->releaseMulticastLock()V

    #@2f
    .line 1904
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@31
    const-string v3, "Exceeded maximum number of wifi locks"

    #@33
    invoke-direct {v0, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@36
    throw v0

    #@37
    .line 1908
    :catchall_37
    move-exception v0

    #@38
    monitor-exit v2
    :try_end_39
    .catchall {:try_start_1e .. :try_end_39} :catchall_37

    #@39
    :try_start_39
    throw v0
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_4c
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_3a} :catch_3a

    #@3a
    .line 1909
    :catch_3a
    move-exception v0

    #@3b
    .line 1911
    :goto_3b
    const/4 v0, 0x1

    #@3c
    :try_start_3c
    iput-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z

    #@3e
    .line 1913
    :cond_3e
    monitor-exit v1

    #@3f
    .line 1914
    return-void

    #@40
    .line 1898
    :cond_40
    iget-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_4c

    #@42
    if-nez v0, :cond_3e

    #@44
    goto :goto_10

    #@45
    .line 1907
    :cond_45
    :try_start_45
    iget-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@47
    invoke-static {v0}, Landroid/net/wifi/WifiManager;->access$508(Landroid/net/wifi/WifiManager;)I

    #@4a
    .line 1908
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_45 .. :try_end_4b} :catchall_37

    #@4b
    goto :goto_3b

    #@4c
    .line 1913
    :catchall_4c
    move-exception v0

    #@4d
    :try_start_4d
    monitor-exit v1
    :try_end_4e
    .catchall {:try_start_4d .. :try_end_4e} :catchall_4c

    #@4e
    throw v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 2005
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@3
    .line 2006
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/wifi/WifiManager$MulticastLock;->setReferenceCounted(Z)V

    #@7
    .line 2007
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager$MulticastLock;->release()V

    #@a
    .line 2008
    return-void
.end method

.method public isHeld()Z
    .registers 3

    #@0
    .prologue
    .line 1984
    iget-object v1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mBinder:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 1985
    :try_start_3
    iget-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 1986
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public release()V
    .registers 5

    #@0
    .prologue
    .line 1941
    iget-object v1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mBinder:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 1942
    :try_start_3
    iget-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCounted:Z

    #@5
    if-eqz v0, :cond_44

    #@7
    iget v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I

    #@9
    add-int/lit8 v0, v0, -0x1

    #@b
    iput v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_41

    #@d
    if-nez v0, :cond_22

    #@f
    .line 1944
    :goto_f
    :try_start_f
    iget-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@11
    iget-object v0, v0, Landroid/net/wifi/WifiManager;->mService:Landroid/net/wifi/IWifiManager;

    #@13
    invoke-interface {v0}, Landroid/net/wifi/IWifiManager;->releaseMulticastLock()V

    #@16
    .line 1945
    iget-object v2, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@18
    monitor-enter v2
    :try_end_19
    .catchall {:try_start_f .. :try_end_19} :catchall_41
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_19} :catch_4c

    #@19
    .line 1946
    :try_start_19
    iget-object v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->this$0:Landroid/net/wifi/WifiManager;

    #@1b
    invoke-static {v0}, Landroid/net/wifi/WifiManager;->access$510(Landroid/net/wifi/WifiManager;)I

    #@1e
    .line 1947
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_19 .. :try_end_1f} :catchall_49

    #@1f
    .line 1950
    :goto_1f
    const/4 v0, 0x0

    #@20
    :try_start_20
    iput-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z

    #@22
    .line 1952
    :cond_22
    iget v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I

    #@24
    if-gez v0, :cond_4e

    #@26
    .line 1953
    new-instance v0, Ljava/lang/RuntimeException;

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "MulticastLock under-locked "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    iget-object v3, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mTag:Ljava/lang/String;

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@40
    throw v0

    #@41
    .line 1956
    :catchall_41
    move-exception v0

    #@42
    monitor-exit v1
    :try_end_43
    .catchall {:try_start_20 .. :try_end_43} :catchall_41

    #@43
    throw v0

    #@44
    .line 1942
    :cond_44
    :try_start_44
    iget-boolean v0, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z
    :try_end_46
    .catchall {:try_start_44 .. :try_end_46} :catchall_41

    #@46
    if-eqz v0, :cond_22

    #@48
    goto :goto_f

    #@49
    .line 1947
    :catchall_49
    move-exception v0

    #@4a
    :try_start_4a
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_49

    #@4b
    :try_start_4b
    throw v0
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_41
    .catch Landroid/os/RemoteException; {:try_start_4b .. :try_end_4c} :catch_4c

    #@4c
    .line 1948
    :catch_4c
    move-exception v0

    #@4d
    goto :goto_1f

    #@4e
    .line 1956
    :cond_4e
    :try_start_4e
    monitor-exit v1
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_41

    #@4f
    .line 1957
    return-void
.end method

.method public setReferenceCounted(Z)V
    .registers 2
    .parameter "refCounted"

    #@0
    .prologue
    .line 1975
    iput-boolean p1, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCounted:Z

    #@2
    .line 1976
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 1991
    iget-object v4, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mBinder:Landroid/os/IBinder;

    #@2
    monitor-enter v4

    #@3
    .line 1992
    :try_start_3
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@6
    move-result v3

    #@7
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 1993
    .local v0, s1:Ljava/lang/String;
    iget-boolean v3, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mHeld:Z

    #@d
    if-eqz v3, :cond_54

    #@f
    const-string v1, "held; "

    #@11
    .line 1994
    .local v1, s2:Ljava/lang/String;
    :goto_11
    iget-boolean v3, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCounted:Z

    #@13
    if-eqz v3, :cond_57

    #@15
    .line 1995
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string/jumbo v5, "refcounted: refcount = "

    #@1d
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    iget v5, p0, Landroid/net/wifi/WifiManager$MulticastLock;->mRefCount:I

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    .line 1999
    .local v2, s3:Ljava/lang/String;
    :goto_2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v5, "MulticastLock{ "

    #@32
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v5, "; "

    #@3c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    const-string v5, " }"

    #@4a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    monitor-exit v4

    #@53
    return-object v3

    #@54
    .line 1993
    .end local v1           #s2:Ljava/lang/String;
    .end local v2           #s3:Ljava/lang/String;
    :cond_54
    const-string v1, ""

    #@56
    goto :goto_11

    #@57
    .line 1997
    .restart local v1       #s2:Ljava/lang/String;
    :cond_57
    const-string/jumbo v2, "not refcounted"

    #@5a
    .restart local v2       #s3:Ljava/lang/String;
    goto :goto_2b

    #@5b
    .line 2000
    .end local v0           #s1:Ljava/lang/String;
    .end local v1           #s2:Ljava/lang/String;
    .end local v2           #s3:Ljava/lang/String;
    :catchall_5b
    move-exception v3

    #@5c
    monitor-exit v4
    :try_end_5d
    .catchall {:try_start_3 .. :try_end_5d} :catchall_5b

    #@5d
    throw v3
.end method
