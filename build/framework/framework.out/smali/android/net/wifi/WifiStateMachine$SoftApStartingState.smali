.class Landroid/net/wifi/WifiStateMachine$SoftApStartingState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SoftApStartingState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4426
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    .line 4430
    const v2, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->getName()Ljava/lang/String;

    #@6
    move-result-object v3

    #@7
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 4432
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$18000(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    .line 4433
    .local v1, message:Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->what:I

    #@12
    const v3, 0x20015

    #@15
    if-ne v2, v3, :cond_3c

    #@17
    .line 4434
    iget-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@1b
    .line 4436
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-nez v0, :cond_2a

    #@1d
    .line 4437
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1f
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$18100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@22
    move-result-object v2

    #@23
    const v3, 0x2001b

    #@26
    invoke-virtual {v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@29
    .line 4467
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :goto_29
    return-void

    #@2a
    .line 4439
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    :cond_2a
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2c
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$18100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@2f
    move-result-object v2

    #@30
    const v3, 0x20019

    #@33
    invoke-virtual {v2, v3, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(ILjava/lang/Object;)V

    #@36
    .line 4440
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@38
    invoke-static {v2, v0}, Landroid/net/wifi/WifiStateMachine;->access$18200(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiConfiguration;)V

    #@3b
    goto :goto_29

    #@3c
    .line 4444
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :cond_3c
    iget v2, v1, Landroid/os/Message;->what:I

    #@3e
    const v3, 0x20086

    #@41
    if-ne v2, v3, :cond_7d

    #@43
    .line 4446
    iget-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@45
    check-cast v0, Landroid/net/wifi/WifiVZWConfiguration;

    #@47
    .line 4447
    .local v0, config:Landroid/net/wifi/WifiVZWConfiguration;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@49
    const-string v3, "--CMD_START_VZW_AP--"

    #@4b
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@4e
    .line 4449
    if-nez v0, :cond_64

    #@50
    .line 4454
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@52
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$18100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@55
    move-result-object v2

    #@56
    const v3, 0x2008c

    #@59
    invoke-virtual {v2, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@5c
    .line 4455
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5e
    const-string v3, "--CMD_REQUEST_VZW_AP_CONFIG--"

    #@60
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@63
    goto :goto_29

    #@64
    .line 4457
    :cond_64
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@66
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$18100(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@69
    move-result-object v2

    #@6a
    const v3, 0x2008a

    #@6d
    invoke-virtual {v2, v3, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(ILjava/lang/Object;)V

    #@70
    .line 4458
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@72
    invoke-static {v2, v0}, Landroid/net/wifi/WifiStateMachine;->access$18300(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiVZWConfiguration;)V

    #@75
    .line 4459
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@77
    const-string v3, "--CMD_START_VZW_AP--"

    #@79
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@7c
    goto :goto_29

    #@7d
    .line 4465
    .end local v0           #config:Landroid/net/wifi/WifiVZWConfiguration;
    :cond_7d
    new-instance v2, Ljava/lang/RuntimeException;

    #@7f
    new-instance v3, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v4, "Illegal transition to SoftApStartingState: "

    #@86
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v3

    #@92
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@95
    throw v2
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 9
    .parameter "message"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4471
    iget v3, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v3, :sswitch_data_6a

    #@6
    .line 4535
    :goto_6
    return v2

    #@7
    .line 4494
    :sswitch_7
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9
    invoke-static {v2, p1}, Landroid/net/wifi/WifiStateMachine;->access$18400(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@c
    .line 4535
    :goto_c
    const/4 v2, 0x1

    #@d
    goto :goto_6

    #@e
    .line 4497
    :sswitch_e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10
    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    #@12
    .line 4498
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_1a

    #@14
    .line 4499
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@16
    invoke-static {v2, v0}, Landroid/net/wifi/WifiStateMachine;->access$18200(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiConfiguration;)V

    #@19
    goto :goto_c

    #@1a
    .line 4501
    :cond_1a
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c
    const-string v3, "Softap config is null!"

    #@1e
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@21
    .line 4502
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@23
    const v3, 0x20017

    #@26
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@29
    goto :goto_c

    #@2a
    .line 4507
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    :sswitch_2a
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    check-cast v1, Landroid/net/wifi/WifiVZWConfiguration;

    #@2e
    .line 4508
    .local v1, mconfig:Landroid/net/wifi/WifiVZWConfiguration;
    if-eqz v1, :cond_36

    #@30
    .line 4509
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@32
    invoke-static {v2, v1}, Landroid/net/wifi/WifiStateMachine;->access$18300(Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiVZWConfiguration;)V

    #@35
    goto :goto_c

    #@36
    .line 4512
    :cond_36
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@38
    const-string v3, "Softap VZW config is null!"

    #@3a
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$4100(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@3d
    .line 4513
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3f
    const v3, 0x20088

    #@42
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessage(I)V

    #@45
    goto :goto_c

    #@46
    .line 4522
    .end local v1           #mconfig:Landroid/net/wifi/WifiVZWConfiguration;
    :sswitch_46
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@48
    const/16 v3, 0xd

    #@4a
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$2800(Landroid/net/wifi/WifiStateMachine;I)V

    #@4d
    .line 4523
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4f
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@51
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$18500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@54
    move-result-object v3

    #@55
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$18600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@58
    goto :goto_c

    #@59
    .line 4530
    :sswitch_59
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5b
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$SoftApStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5d
    const v5, 0x20002

    #@60
    const/16 v6, 0xe

    #@62
    invoke-virtual {v4, v5, v6, v2}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@69
    goto :goto_c

    #@6a
    .line 4471
    :sswitch_data_6a
    .sparse-switch
        0x20001 -> :sswitch_7
        0x20002 -> :sswitch_7
        0x2000b -> :sswitch_7
        0x2000c -> :sswitch_7
        0x2000d -> :sswitch_7
        0x2000e -> :sswitch_7
        0x20015 -> :sswitch_7
        0x20016 -> :sswitch_46
        0x20017 -> :sswitch_59
        0x20018 -> :sswitch_7
        0x2001c -> :sswitch_e
        0x2001d -> :sswitch_7
        0x20048 -> :sswitch_7
        0x20049 -> :sswitch_7
        0x20050 -> :sswitch_7
        0x20054 -> :sswitch_7
        0x20055 -> :sswitch_7
        0x2005a -> :sswitch_7
        0x20086 -> :sswitch_7
        0x20087 -> :sswitch_46
        0x20088 -> :sswitch_59
        0x20089 -> :sswitch_7
        0x2008d -> :sswitch_2a
    .end sparse-switch
.end method
