.class public Landroid/net/wifi/WifiConfiguration;
.super Ljava/lang/Object;
.source "WifiConfiguration.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/wifi/WifiConfiguration$ProxySettings;,
        Landroid/net/wifi/WifiConfiguration$IpAssignment;,
        Landroid/net/wifi/WifiConfiguration$Status;,
        Landroid/net/wifi/WifiConfiguration$GroupCipher;,
        Landroid/net/wifi/WifiConfiguration$PairwiseCipher;,
        Landroid/net/wifi/WifiConfiguration$AuthAlgorithm;,
        Landroid/net/wifi/WifiConfiguration$Protocol;,
        Landroid/net/wifi/WifiConfiguration$KeyMgmt;,
        Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public static final DISABLED_AUTH_FAILURE:I = 0x3

.field public static final DISABLED_DHCP_FAILURE:I = 0x2

.field public static final DISABLED_DNS_FAILURE:I = 0x1

.field public static final DISABLED_UNKNOWN_REASON:I = 0x0

.field public static final ENGINE_DISABLE:Ljava/lang/String; = "0"

.field public static final ENGINE_ENABLE:Ljava/lang/String; = "1"

.field public static final INVALID_NETWORK_ID:I = -0x1

.field public static final KEYSTORE_ENGINE_ID:Ljava/lang/String; = "keystore"

.field public static final KEYSTORE_URI:Ljava/lang/String; = "keystore://"

.field public static final OLD_PRIVATE_KEY_NAME:Ljava/lang/String; = "private_key"

.field public static final bssidVarName:Ljava/lang/String; = "bssid"

.field public static final hiddenSSIDVarName:Ljava/lang/String; = "scan_ssid"

.field public static final priorityVarName:Ljava/lang/String; = "priority"

.field public static final pskVarName:Ljava/lang/String; = "psk"

.field public static final ssidVarName:Ljava/lang/String; = "ssid"

.field public static final wepKeyVarNames:[Ljava/lang/String; = null

.field public static final wepTxKeyIdxVarName:Ljava/lang/String; = "wep_tx_keyidx"


# instance fields
.field public BSSID:Ljava/lang/String;

.field public SSID:Ljava/lang/String;

.field public allowedAuthAlgorithms:Ljava/util/BitSet;

.field public allowedGroupCiphers:Ljava/util/BitSet;

.field public allowedKeyManagement:Ljava/util/BitSet;

.field public allowedPairwiseCiphers:Ljava/util/BitSet;

.field public allowedProtocols:Ljava/util/BitSet;

.field public anonymous_identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public ca_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public client_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public disableReason:I

.field public eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public hiddenSSID:Z

.field public identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

.field public key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public linkProperties:Landroid/net/LinkProperties;

.field public networkId:I

.field public password:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public phase1:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public phase2:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

.field public preSharedKey:Ljava/lang/String;

.field public priority:I

.field public proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

.field public status:I

.field public wepKeys:[Ljava/lang/String;

.field public wepTxKeyIndex:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 72
    const/4 v0, 0x4

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string/jumbo v2, "wep_key0"

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    const-string/jumbo v2, "wep_key1"

    #@d
    aput-object v2, v0, v1

    #@f
    const/4 v1, 0x2

    #@10
    const-string/jumbo v2, "wep_key2"

    #@13
    aput-object v2, v0, v1

    #@15
    const/4 v1, 0x3

    #@16
    const-string/jumbo v2, "wep_key3"

    #@19
    aput-object v2, v0, v1

    #@1b
    sput-object v0, Landroid/net/wifi/WifiConfiguration;->wepKeyVarNames:[Ljava/lang/String;

    #@1d
    .line 680
    new-instance v0, Landroid/net/wifi/WifiConfiguration$1;

    #@1f
    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration$1;-><init>()V

    #@22
    sput-object v0, Landroid/net/wifi/WifiConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    return-void
.end method

.method public constructor <init>()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, 0x0

    #@3
    .line 430
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 106
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@8
    const-string v6, "eap"

    #@a
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@d
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@f
    .line 108
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@11
    const-string/jumbo v6, "phase2"

    #@14
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@17
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->phase2:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@19
    .line 110
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@1b
    const-string v6, "identity"

    #@1d
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@20
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@22
    .line 112
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@24
    const-string v6, "anonymous_identity"

    #@26
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@29
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->anonymous_identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@2b
    .line 114
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@2d
    const-string/jumbo v6, "password"

    #@30
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@33
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->password:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@35
    .line 116
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@37
    const-string v6, "client_cert"

    #@39
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@3c
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->client_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@3e
    .line 118
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@40
    const-string v6, "engine"

    #@42
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@45
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@47
    .line 120
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@49
    const-string v6, "engine_id"

    #@4b
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@4e
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@50
    .line 122
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@52
    const-string/jumbo v6, "key_id"

    #@55
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@58
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@5a
    .line 124
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@5c
    const-string v6, "ca_cert"

    #@5e
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@61
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->ca_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@63
    .line 129
    new-instance v5, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@65
    const-string/jumbo v6, "phase1"

    #@68
    invoke-direct {v5, p0, v6, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@6b
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->phase1:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@6d
    .line 135
    const/16 v5, 0xb

    #@6f
    new-array v5, v5, [Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@71
    iget-object v6, p0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@73
    aput-object v6, v5, v9

    #@75
    const/4 v6, 0x1

    #@76
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->phase2:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@78
    aput-object v7, v5, v6

    #@7a
    const/4 v6, 0x2

    #@7b
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@7d
    aput-object v7, v5, v6

    #@7f
    const/4 v6, 0x3

    #@80
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->anonymous_identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@82
    aput-object v7, v5, v6

    #@84
    iget-object v6, p0, Landroid/net/wifi/WifiConfiguration;->password:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@86
    aput-object v6, v5, v10

    #@88
    const/4 v6, 0x5

    #@89
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->client_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@8b
    aput-object v7, v5, v6

    #@8d
    const/4 v6, 0x6

    #@8e
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@90
    aput-object v7, v5, v6

    #@92
    const/4 v6, 0x7

    #@93
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@95
    aput-object v7, v5, v6

    #@97
    const/16 v6, 0x8

    #@99
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@9b
    aput-object v7, v5, v6

    #@9d
    const/16 v6, 0x9

    #@9f
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->ca_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@a1
    aput-object v7, v5, v6

    #@a3
    const/16 v6, 0xa

    #@a5
    iget-object v7, p0, Landroid/net/wifi/WifiConfiguration;->phase1:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@a7
    aput-object v7, v5, v6

    #@a9
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@ab
    .line 431
    const/4 v5, -0x1

    #@ac
    iput v5, p0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@ae
    .line 432
    iput-object v8, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@b0
    .line 433
    iput-object v8, p0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@b2
    .line 434
    iput v9, p0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@b4
    .line 435
    iput-boolean v9, p0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@b6
    .line 436
    iput v9, p0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@b8
    .line 437
    new-instance v5, Ljava/util/BitSet;

    #@ba
    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    #@bd
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@bf
    .line 438
    new-instance v5, Ljava/util/BitSet;

    #@c1
    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    #@c4
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@c6
    .line 439
    new-instance v5, Ljava/util/BitSet;

    #@c8
    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    #@cb
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@cd
    .line 440
    new-instance v5, Ljava/util/BitSet;

    #@cf
    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    #@d2
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@d4
    .line 441
    new-instance v5, Ljava/util/BitSet;

    #@d6
    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    #@d9
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@db
    .line 442
    new-array v5, v10, [Ljava/lang/String;

    #@dd
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@df
    .line 443
    const/4 v2, 0x0

    #@e0
    .local v2, i:I
    :goto_e0
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@e2
    array-length v5, v5

    #@e3
    if-ge v2, v5, :cond_ec

    #@e5
    .line 444
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@e7
    aput-object v8, v5, v2

    #@e9
    .line 443
    add-int/lit8 v2, v2, 0x1

    #@eb
    goto :goto_e0

    #@ec
    .line 445
    :cond_ec
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@ee
    .local v0, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v4, v0

    #@ef
    .local v4, len$:I
    const/4 v3, 0x0

    #@f0
    .local v3, i$:I
    :goto_f0
    if-ge v3, v4, :cond_fa

    #@f2
    aget-object v1, v0, v3

    #@f4
    .line 446
    .local v1, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    invoke-virtual {v1, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@f7
    .line 445
    add-int/lit8 v3, v3, 0x1

    #@f9
    goto :goto_f0

    #@fa
    .line 448
    .end local v1           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    :cond_fa
    sget-object v5, Landroid/net/wifi/WifiConfiguration$IpAssignment;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@fc
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@fe
    .line 449
    sget-object v5, Landroid/net/wifi/WifiConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@100
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@102
    .line 450
    new-instance v5, Landroid/net/LinkProperties;

    #@104
    invoke-direct {v5}, Landroid/net/LinkProperties;-><init>()V

    #@107
    iput-object v5, p0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@109
    .line 451
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/WifiConfiguration;)V
    .registers 7
    .parameter "source"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v3, 0x0

    #@2
    .line 620
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 106
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@7
    const-string v2, "eap"

    #@9
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@c
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@e
    .line 108
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@10
    const-string/jumbo v2, "phase2"

    #@13
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@16
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->phase2:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@18
    .line 110
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@1a
    const-string v2, "identity"

    #@1c
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@1f
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@21
    .line 112
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@23
    const-string v2, "anonymous_identity"

    #@25
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@28
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->anonymous_identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@2a
    .line 114
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@2c
    const-string/jumbo v2, "password"

    #@2f
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@32
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->password:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@34
    .line 116
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@36
    const-string v2, "client_cert"

    #@38
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@3b
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->client_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@3d
    .line 118
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@3f
    const-string v2, "engine"

    #@41
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@44
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@46
    .line 120
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@48
    const-string v2, "engine_id"

    #@4a
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@4d
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@4f
    .line 122
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@51
    const-string/jumbo v2, "key_id"

    #@54
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@57
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@59
    .line 124
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@5b
    const-string v2, "ca_cert"

    #@5d
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@60
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->ca_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@62
    .line 129
    new-instance v1, Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@64
    const-string/jumbo v2, "phase1"

    #@67
    invoke-direct {v1, p0, v2, v3}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;-><init>(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;Landroid/net/wifi/WifiConfiguration$1;)V

    #@6a
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->phase1:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@6c
    .line 135
    const/16 v1, 0xb

    #@6e
    new-array v1, v1, [Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@70
    const/4 v2, 0x0

    #@71
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@73
    aput-object v3, v1, v2

    #@75
    const/4 v2, 0x1

    #@76
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->phase2:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@78
    aput-object v3, v1, v2

    #@7a
    const/4 v2, 0x2

    #@7b
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@7d
    aput-object v3, v1, v2

    #@7f
    const/4 v2, 0x3

    #@80
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->anonymous_identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@82
    aput-object v3, v1, v2

    #@84
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->password:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@86
    aput-object v2, v1, v4

    #@88
    const/4 v2, 0x5

    #@89
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->client_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@8b
    aput-object v3, v1, v2

    #@8d
    const/4 v2, 0x6

    #@8e
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->engine:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@90
    aput-object v3, v1, v2

    #@92
    const/4 v2, 0x7

    #@93
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->engine_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@95
    aput-object v3, v1, v2

    #@97
    const/16 v2, 0x8

    #@99
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@9b
    aput-object v3, v1, v2

    #@9d
    const/16 v2, 0x9

    #@9f
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->ca_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@a1
    aput-object v3, v1, v2

    #@a3
    const/16 v2, 0xa

    #@a5
    iget-object v3, p0, Landroid/net/wifi/WifiConfiguration;->phase1:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@a7
    aput-object v3, v1, v2

    #@a9
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@ab
    .line 621
    if-eqz p1, :cond_141

    #@ad
    .line 622
    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@af
    iput v1, p0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@b1
    .line 623
    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->status:I

    #@b3
    iput v1, p0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@b5
    .line 624
    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@b7
    iput v1, p0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@b9
    .line 625
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@bb
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@bd
    .line 626
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@bf
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@c1
    .line 627
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@c3
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@c5
    .line 629
    new-array v1, v4, [Ljava/lang/String;

    #@c7
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@c9
    .line 630
    const/4 v0, 0x0

    #@ca
    .local v0, i:I
    :goto_ca
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@cc
    array-length v1, v1

    #@cd
    if-ge v0, v1, :cond_da

    #@cf
    .line 631
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@d1
    iget-object v2, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@d3
    aget-object v2, v2, v0

    #@d5
    aput-object v2, v1, v0

    #@d7
    .line 630
    add-int/lit8 v0, v0, 0x1

    #@d9
    goto :goto_ca

    #@da
    .line 633
    :cond_da
    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@dc
    iput v1, p0, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@de
    .line 634
    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@e0
    iput v1, p0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@e2
    .line 635
    iget-boolean v1, p1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@e4
    iput-boolean v1, p0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@e6
    .line 636
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@e8
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@eb
    move-result-object v1

    #@ec
    check-cast v1, Ljava/util/BitSet;

    #@ee
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@f0
    .line 637
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@f2
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@f5
    move-result-object v1

    #@f6
    check-cast v1, Ljava/util/BitSet;

    #@f8
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@fa
    .line 638
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@fc
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@ff
    move-result-object v1

    #@100
    check-cast v1, Ljava/util/BitSet;

    #@102
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@104
    .line 639
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@106
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@109
    move-result-object v1

    #@10a
    check-cast v1, Ljava/util/BitSet;

    #@10c
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@10e
    .line 640
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@110
    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    #@113
    move-result-object v1

    #@114
    check-cast v1, Ljava/util/BitSet;

    #@116
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@118
    .line 642
    const/4 v0, 0x0

    #@119
    :goto_119
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@11b
    array-length v1, v1

    #@11c
    if-ge v0, v1, :cond_130

    #@11e
    .line 643
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@120
    aget-object v1, v1, v0

    #@122
    iget-object v2, p1, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@124
    aget-object v2, v2, v0

    #@126
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@129
    move-result-object v2

    #@12a
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@12d
    .line 642
    add-int/lit8 v0, v0, 0x1

    #@12f
    goto :goto_119

    #@130
    .line 645
    :cond_130
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@132
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@134
    .line 646
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@136
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@138
    .line 647
    new-instance v1, Landroid/net/LinkProperties;

    #@13a
    iget-object v2, p1, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@13c
    invoke-direct {v1, v2}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@13f
    iput-object v1, p0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@141
    .line 649
    .end local v0           #i:I
    :cond_141
    return-void
.end method

.method static synthetic access$100(Landroid/os/Parcel;)Ljava/util/BitSet;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    invoke-static {p0}, Landroid/net/wifi/WifiConfiguration;->readBitSet(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static readBitSet(Landroid/os/Parcel;)Ljava/util/BitSet;
    .registers 5
    .parameter "src"

    #@0
    .prologue
    .line 577
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 579
    .local v0, cardinality:I
    new-instance v2, Ljava/util/BitSet;

    #@6
    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    #@9
    .line 580
    .local v2, set:Ljava/util/BitSet;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_16

    #@c
    .line 581
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v3

    #@10
    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    #@13
    .line 580
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_a

    #@16
    .line 583
    :cond_16
    return-object v2
.end method

.method private static writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V
    .registers 4
    .parameter "dest"
    .parameter "set"

    #@0
    .prologue
    .line 587
    const/4 v0, -0x1

    #@1
    .line 589
    .local v0, nextSetBit:I
    invoke-virtual {p1}, Ljava/util/BitSet;->cardinality()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 591
    :goto_8
    add-int/lit8 v1, v0, 0x1

    #@a
    invoke-virtual {p1, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    #@d
    move-result v0

    #@e
    const/4 v1, -0x1

    #@f
    if-eq v0, v1, :cond_15

    #@11
    .line 592
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    goto :goto_8

    #@15
    .line 593
    :cond_15
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 616
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAuthType()I
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v1, 0x4

    #@2
    const/4 v3, 0x3

    #@3
    const/4 v2, 0x2

    #@4
    const/4 v0, 0x1

    #@5
    .line 600
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@7
    invoke-virtual {v5, v0}, Ljava/util/BitSet;->get(I)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_e

    #@d
    .line 611
    :goto_d
    return v0

    #@e
    .line 602
    :cond_e
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@10
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_18

    #@16
    move v0, v1

    #@17
    .line 603
    goto :goto_d

    #@18
    .line 604
    :cond_18
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@1a
    invoke-virtual {v0, v2}, Ljava/util/BitSet;->get(I)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_22

    #@20
    move v0, v2

    #@21
    .line 605
    goto :goto_d

    #@22
    .line 606
    :cond_22
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@24
    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_2c

    #@2a
    move v0, v3

    #@2b
    .line 607
    goto :goto_d

    #@2c
    .line 608
    :cond_2c
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@2e
    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_36

    #@34
    move v0, v4

    #@35
    .line 609
    goto :goto_d

    #@36
    .line 611
    :cond_36
    const/4 v0, 0x0

    #@37
    goto :goto_d
.end method

.method public getPrintableSsid()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/16 v4, 0x22

    #@5
    .line 557
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@7
    if-nez v2, :cond_c

    #@9
    const-string v2, ""

    #@b
    .line 573
    :goto_b
    return-object v2

    #@c
    .line 558
    :cond_c
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@e
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@11
    move-result v0

    #@12
    .line 559
    .local v0, length:I
    if-le v0, v7, :cond_2f

    #@14
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@16
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    #@19
    move-result v2

    #@1a
    if-ne v2, v4, :cond_2f

    #@1c
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@1e
    add-int/lit8 v3, v0, -0x1

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    #@23
    move-result v2

    #@24
    if-ne v2, v4, :cond_2f

    #@26
    .line 560
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@28
    add-int/lit8 v3, v0, -0x1

    #@2a
    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    goto :goto_b

    #@2f
    .line 567
    :cond_2f
    const/4 v2, 0x3

    #@30
    if-le v0, v2, :cond_5f

    #@32
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@34
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    #@37
    move-result v2

    #@38
    const/16 v3, 0x50

    #@3a
    if-ne v2, v3, :cond_5f

    #@3c
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@3e
    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    #@41
    move-result v2

    #@42
    if-ne v2, v4, :cond_5f

    #@44
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@46
    add-int/lit8 v3, v0, -0x1

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    #@4b
    move-result v2

    #@4c
    if-ne v2, v4, :cond_5f

    #@4e
    .line 569
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@50
    add-int/lit8 v3, v0, -0x1

    #@52
    invoke-virtual {v2, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-static {v2}, Landroid/net/wifi/WifiSsid;->createFromAsciiEncoded(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;

    #@59
    move-result-object v1

    #@5a
    .line 571
    .local v1, wifiSsid:Landroid/net/wifi/WifiSsid;
    invoke-virtual {v1}, Landroid/net/wifi/WifiSsid;->toString()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    goto :goto_b

    #@5f
    .line 573
    .end local v1           #wifiSsid:Landroid/net/wifi/WifiSsid;
    :cond_5f
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@61
    goto :goto_b
.end method

.method public toString()Ljava/lang/String;
    .registers 16

    #@0
    .prologue
    const/16 v14, 0xa

    #@2
    .line 455
    new-instance v9, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 456
    .local v9, sbuf:Ljava/lang/StringBuilder;
    iget v11, p0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@9
    if-nez v11, :cond_6d

    #@b
    .line 457
    const-string v11, "* "

    #@d
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 461
    :cond_10
    :goto_10
    const-string v11, "ID: "

    #@12
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v11

    #@16
    iget v12, p0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@18
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v11

    #@1c
    const-string v12, " SSID: "

    #@1e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v11

    #@22
    iget-object v12, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@24
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v11

    #@28
    const-string v12, " BSSID: "

    #@2a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v11

    #@2e
    iget-object v12, p0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@30
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v11

    #@34
    const-string v12, " PRIO: "

    #@36
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v11

    #@3a
    iget v12, p0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@3c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v11

    #@40
    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@43
    .line 464
    const-string v11, " KeyMgmt:"

    #@45
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 465
    const/4 v5, 0x0

    #@49
    .local v5, k:I
    :goto_49
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@4b
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@4e
    move-result v11

    #@4f
    if-ge v5, v11, :cond_8a

    #@51
    .line 466
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@53
    invoke-virtual {v11, v5}, Ljava/util/BitSet;->get(I)Z

    #@56
    move-result v11

    #@57
    if-eqz v11, :cond_6a

    #@59
    .line 467
    const-string v11, " "

    #@5b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 468
    sget-object v11, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@60
    array-length v11, v11

    #@61
    if-ge v5, v11, :cond_84

    #@63
    .line 469
    sget-object v11, Landroid/net/wifi/WifiConfiguration$KeyMgmt;->strings:[Ljava/lang/String;

    #@65
    aget-object v11, v11, v5

    #@67
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 465
    :cond_6a
    :goto_6a
    add-int/lit8 v5, v5, 0x1

    #@6c
    goto :goto_49

    #@6d
    .line 458
    .end local v5           #k:I
    :cond_6d
    iget v11, p0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@6f
    const/4 v12, 0x1

    #@70
    if-ne v11, v12, :cond_10

    #@72
    .line 459
    const-string v11, "- DSBLE: "

    #@74
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v11

    #@78
    iget v12, p0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@7a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v11

    #@7e
    const-string v12, " "

    #@80
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    goto :goto_10

    #@84
    .line 471
    .restart local v5       #k:I
    :cond_84
    const-string v11, "??"

    #@86
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    goto :goto_6a

    #@8a
    .line 475
    :cond_8a
    const-string v11, " Protocols:"

    #@8c
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    .line 476
    const/4 v7, 0x0

    #@90
    .local v7, p:I
    :goto_90
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@92
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@95
    move-result v11

    #@96
    if-ge v7, v11, :cond_ba

    #@98
    .line 477
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@9a
    invoke-virtual {v11, v7}, Ljava/util/BitSet;->get(I)Z

    #@9d
    move-result v11

    #@9e
    if-eqz v11, :cond_b1

    #@a0
    .line 478
    const-string v11, " "

    #@a2
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 479
    sget-object v11, Landroid/net/wifi/WifiConfiguration$Protocol;->strings:[Ljava/lang/String;

    #@a7
    array-length v11, v11

    #@a8
    if-ge v7, v11, :cond_b4

    #@aa
    .line 480
    sget-object v11, Landroid/net/wifi/WifiConfiguration$Protocol;->strings:[Ljava/lang/String;

    #@ac
    aget-object v11, v11, v7

    #@ae
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    .line 476
    :cond_b1
    :goto_b1
    add-int/lit8 v7, v7, 0x1

    #@b3
    goto :goto_90

    #@b4
    .line 482
    :cond_b4
    const-string v11, "??"

    #@b6
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    goto :goto_b1

    #@ba
    .line 486
    :cond_ba
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@bd
    .line 487
    const-string v11, " AuthAlgorithms:"

    #@bf
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    .line 488
    const/4 v0, 0x0

    #@c3
    .local v0, a:I
    :goto_c3
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@c5
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@c8
    move-result v11

    #@c9
    if-ge v0, v11, :cond_ed

    #@cb
    .line 489
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@cd
    invoke-virtual {v11, v0}, Ljava/util/BitSet;->get(I)Z

    #@d0
    move-result v11

    #@d1
    if-eqz v11, :cond_e4

    #@d3
    .line 490
    const-string v11, " "

    #@d5
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    .line 491
    sget-object v11, Landroid/net/wifi/WifiConfiguration$AuthAlgorithm;->strings:[Ljava/lang/String;

    #@da
    array-length v11, v11

    #@db
    if-ge v0, v11, :cond_e7

    #@dd
    .line 492
    sget-object v11, Landroid/net/wifi/WifiConfiguration$AuthAlgorithm;->strings:[Ljava/lang/String;

    #@df
    aget-object v11, v11, v0

    #@e1
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    .line 488
    :cond_e4
    :goto_e4
    add-int/lit8 v0, v0, 0x1

    #@e6
    goto :goto_c3

    #@e7
    .line 494
    :cond_e7
    const-string v11, "??"

    #@e9
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    goto :goto_e4

    #@ed
    .line 498
    :cond_ed
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@f0
    .line 499
    const-string v11, " PairwiseCiphers:"

    #@f2
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    .line 500
    const/4 v8, 0x0

    #@f6
    .local v8, pc:I
    :goto_f6
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@f8
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@fb
    move-result v11

    #@fc
    if-ge v8, v11, :cond_120

    #@fe
    .line 501
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@100
    invoke-virtual {v11, v8}, Ljava/util/BitSet;->get(I)Z

    #@103
    move-result v11

    #@104
    if-eqz v11, :cond_117

    #@106
    .line 502
    const-string v11, " "

    #@108
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    .line 503
    sget-object v11, Landroid/net/wifi/WifiConfiguration$PairwiseCipher;->strings:[Ljava/lang/String;

    #@10d
    array-length v11, v11

    #@10e
    if-ge v8, v11, :cond_11a

    #@110
    .line 504
    sget-object v11, Landroid/net/wifi/WifiConfiguration$PairwiseCipher;->strings:[Ljava/lang/String;

    #@112
    aget-object v11, v11, v8

    #@114
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    .line 500
    :cond_117
    :goto_117
    add-int/lit8 v8, v8, 0x1

    #@119
    goto :goto_f6

    #@11a
    .line 506
    :cond_11a
    const-string v11, "??"

    #@11c
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    goto :goto_117

    #@120
    .line 510
    :cond_120
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@123
    .line 511
    const-string v11, " GroupCiphers:"

    #@125
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    .line 512
    const/4 v3, 0x0

    #@129
    .local v3, gc:I
    :goto_129
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@12b
    invoke-virtual {v11}, Ljava/util/BitSet;->size()I

    #@12e
    move-result v11

    #@12f
    if-ge v3, v11, :cond_153

    #@131
    .line 513
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@133
    invoke-virtual {v11, v3}, Ljava/util/BitSet;->get(I)Z

    #@136
    move-result v11

    #@137
    if-eqz v11, :cond_14a

    #@139
    .line 514
    const-string v11, " "

    #@13b
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    .line 515
    sget-object v11, Landroid/net/wifi/WifiConfiguration$GroupCipher;->strings:[Ljava/lang/String;

    #@140
    array-length v11, v11

    #@141
    if-ge v3, v11, :cond_14d

    #@143
    .line 516
    sget-object v11, Landroid/net/wifi/WifiConfiguration$GroupCipher;->strings:[Ljava/lang/String;

    #@145
    aget-object v11, v11, v3

    #@147
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    .line 512
    :cond_14a
    :goto_14a
    add-int/lit8 v3, v3, 0x1

    #@14c
    goto :goto_129

    #@14d
    .line 518
    :cond_14d
    const-string v11, "??"

    #@14f
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    goto :goto_14a

    #@153
    .line 522
    :cond_153
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@156
    move-result-object v11

    #@157
    const-string v12, " PSK: "

    #@159
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    .line 523
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@15e
    if-eqz v11, :cond_165

    #@160
    .line 524
    const/16 v11, 0x2a

    #@162
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@165
    .line 527
    :cond_165
    iget-object v1, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@167
    .local v1, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v6, v1

    #@168
    .local v6, len$:I
    const/4 v4, 0x0

    #@169
    .local v4, i$:I
    :goto_169
    if-ge v4, v6, :cond_19d

    #@16b
    aget-object v2, v1, v4

    #@16d
    .line 528
    .local v2, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@170
    move-result-object v11

    #@171
    new-instance v12, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v13, " "

    #@178
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v12

    #@17c
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->varName()Ljava/lang/String;

    #@17f
    move-result-object v13

    #@180
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v12

    #@184
    const-string v13, ": "

    #@186
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v12

    #@18a
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v12

    #@18e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    .line 529
    invoke-virtual {v2}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@194
    move-result-object v10

    #@195
    .line 530
    .local v10, value:Ljava/lang/String;
    if-eqz v10, :cond_19a

    #@197
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    .line 527
    :cond_19a
    add-int/lit8 v4, v4, 0x1

    #@19c
    goto :goto_169

    #@19d
    .line 532
    .end local v2           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v10           #value:Ljava/lang/String;
    :cond_19d
    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a0
    .line 533
    new-instance v11, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v12, "IP assignment: "

    #@1a7
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v11

    #@1ab
    iget-object v12, p0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@1ad
    invoke-virtual {v12}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v12

    #@1b1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v11

    #@1b5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b8
    move-result-object v11

    #@1b9
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    .line 534
    const-string v11, "\n"

    #@1be
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    .line 535
    new-instance v11, Ljava/lang/StringBuilder;

    #@1c3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1c6
    const-string v12, "Proxy settings: "

    #@1c8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v11

    #@1cc
    iget-object v12, p0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1ce
    invoke-virtual {v12}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->toString()Ljava/lang/String;

    #@1d1
    move-result-object v12

    #@1d2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v11

    #@1d6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d9
    move-result-object v11

    #@1da
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    .line 536
    const-string v11, "\n"

    #@1df
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    .line 537
    iget-object v11, p0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@1e4
    invoke-virtual {v11}, Landroid/net/LinkProperties;->toString()Ljava/lang/String;

    #@1e7
    move-result-object v11

    #@1e8
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    .line 538
    const-string v11, "\n"

    #@1ed
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    .line 540
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f3
    move-result-object v11

    #@1f4
    return-object v11
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 653
    iget v5, p0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@2
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 654
    iget v5, p0, Landroid/net/wifi/WifiConfiguration;->status:I

    #@7
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 655
    iget v5, p0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@c
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 656
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 657
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 658
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@1b
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 659
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@20
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@21
    .local v3, len$:I
    const/4 v2, 0x0

    #@22
    .local v2, i$:I
    :goto_22
    if-ge v2, v3, :cond_2c

    #@24
    aget-object v4, v0, v2

    #@26
    .line 660
    .local v4, wepKey:Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@29
    .line 659
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_22

    #@2c
    .line 661
    .end local v4           #wepKey:Ljava/lang/String;
    :cond_2c
    iget v5, p0, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@2e
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 662
    iget v5, p0, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@33
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@36
    .line 663
    iget-boolean v5, p0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@38
    if-eqz v5, :cond_69

    #@3a
    const/4 v5, 0x1

    #@3b
    :goto_3b
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 665
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@40
    invoke-static {p1, v5}, Landroid/net/wifi/WifiConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@43
    .line 666
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@45
    invoke-static {p1, v5}, Landroid/net/wifi/WifiConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@48
    .line 667
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@4a
    invoke-static {p1, v5}, Landroid/net/wifi/WifiConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@4d
    .line 668
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@4f
    invoke-static {p1, v5}, Landroid/net/wifi/WifiConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@52
    .line 669
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@54
    invoke-static {p1, v5}, Landroid/net/wifi/WifiConfiguration;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    #@57
    .line 671
    iget-object v0, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@59
    .local v0, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v3, v0

    #@5a
    const/4 v2, 0x0

    #@5b
    :goto_5b
    if-ge v2, v3, :cond_6b

    #@5d
    aget-object v1, v0, v2

    #@5f
    .line 672
    .local v1, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->value()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@66
    .line 671
    add-int/lit8 v2, v2, 0x1

    #@68
    goto :goto_5b

    #@69
    .line 663
    .end local v1           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .local v0, arr$:[Ljava/lang/String;
    :cond_69
    const/4 v5, 0x0

    #@6a
    goto :goto_3b

    #@6b
    .line 674
    .local v0, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    :cond_6b
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@6d
    invoke-virtual {v5}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->name()Ljava/lang/String;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@74
    .line 675
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@76
    invoke-virtual {v5}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->name()Ljava/lang/String;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7d
    .line 676
    iget-object v5, p0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@7f
    invoke-virtual {p1, v5, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@82
    .line 677
    return-void
.end method
