.class public final enum Landroid/net/wifi/WifiConfiguration$ProxySettings;
.super Ljava/lang/Enum;
.source "WifiConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProxySettings"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/wifi/WifiConfiguration$ProxySettings;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/wifi/WifiConfiguration$ProxySettings;

.field public static final enum NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

.field public static final enum STATIC:Landroid/net/wifi/WifiConfiguration$ProxySettings;

.field public static final enum UNASSIGNED:Landroid/net/wifi/WifiConfiguration$ProxySettings;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 413
    new-instance v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@5
    const-string v1, "NONE"

    #@7
    invoke-direct {v0, v1, v2}, Landroid/net/wifi/WifiConfiguration$ProxySettings;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;->NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@c
    .line 416
    new-instance v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@e
    const-string v1, "STATIC"

    #@10
    invoke-direct {v0, v1, v3}, Landroid/net/wifi/WifiConfiguration$ProxySettings;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;->STATIC:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@15
    .line 419
    new-instance v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@17
    const-string v1, "UNASSIGNED"

    #@19
    invoke-direct {v0, v1, v4}, Landroid/net/wifi/WifiConfiguration$ProxySettings;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1e
    .line 410
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@21
    sget-object v1, Landroid/net/wifi/WifiConfiguration$ProxySettings;->NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Landroid/net/wifi/WifiConfiguration$ProxySettings;->STATIC:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Landroid/net/wifi/WifiConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;->$VALUES:[Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 410
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 410
    const-class v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/net/wifi/WifiConfiguration$ProxySettings;
    .registers 1

    #@0
    .prologue
    .line 410
    sget-object v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;->$VALUES:[Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@2
    invoke-virtual {v0}, [Landroid/net/wifi/WifiConfiguration$ProxySettings;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@8
    return-object v0
.end method
