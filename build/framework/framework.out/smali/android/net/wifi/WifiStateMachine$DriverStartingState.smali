.class Landroid/net/wifi/WifiStateMachine$DriverStartingState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DriverStartingState"
.end annotation


# instance fields
.field private mTries:I

.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3054
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    .line 3059
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 3061
    const/4 v0, 0x1

    #@b
    iput v0, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->mTries:I

    #@d
    .line 3063
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11
    const v2, 0x20013

    #@14
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@16
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$8204(Landroid/net/wifi/WifiStateMachine;)I

    #@19
    move-result v3

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@1e
    move-result-object v1

    #@1f
    const-wide/16 v2, 0x2710

    #@21
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@24
    .line 3065
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 8
    .parameter "message"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3069
    iget v2, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v2, :sswitch_data_9a

    #@6
    .line 3119
    :goto_6
    return v1

    #@7
    .line 3071
    :sswitch_7
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9
    invoke-static {v1, p1}, Landroid/net/wifi/WifiStateMachine;->access$8300(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)Landroid/net/wifi/SupplicantState;

    #@c
    move-result-object v0

    #@d
    .line 3076
    .local v0, state:Landroid/net/wifi/SupplicantState;
    invoke-static {v0}, Landroid/net/wifi/SupplicantState;->isDriverActive(Landroid/net/wifi/SupplicantState;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_1e

    #@13
    .line 3077
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@15
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@17
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$5900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/net/wifi/WifiStateMachine;->access$8400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@1e
    .line 3119
    .end local v0           #state:Landroid/net/wifi/SupplicantState;
    :cond_1e
    :goto_1e
    const/4 v1, 0x1

    #@1f
    goto :goto_6

    #@20
    .line 3081
    :sswitch_20
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@22
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@24
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$8200(Landroid/net/wifi/WifiStateMachine;)I

    #@27
    move-result v3

    #@28
    if-ne v2, v3, :cond_1e

    #@2a
    .line 3082
    iget v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->mTries:I

    #@2c
    const/4 v3, 0x2

    #@2d
    if-lt v2, v3, :cond_55

    #@2f
    .line 3083
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Failed to start driver after "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    iget v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->mTries:I

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-static {v1, v2}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@49
    .line 3084
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4b
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4d
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$8500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@50
    move-result-object v2

    #@51
    invoke-static {v1, v2}, Landroid/net/wifi/WifiStateMachine;->access$8600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@54
    goto :goto_1e

    #@55
    .line 3086
    :cond_55
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@57
    const-string v3, "Driver start failed, retrying"

    #@59
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@5c
    .line 3087
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5e
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$2600(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@65
    .line 3088
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@67
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Landroid/net/wifi/WifiNative;->startDriver()Z

    #@6e
    .line 3089
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@70
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$2600(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@77
    .line 3091
    iget v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->mTries:I

    #@79
    add-int/lit8 v2, v2, 0x1

    #@7b
    iput v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->mTries:I

    #@7d
    .line 3093
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7f
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@81
    const v4, 0x20013

    #@84
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@86
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$8204(Landroid/net/wifi/WifiStateMachine;)I

    #@89
    move-result v5

    #@8a
    invoke-virtual {v3, v4, v5, v1}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@8d
    move-result-object v1

    #@8e
    const-wide/16 v3, 0x2710

    #@90
    invoke-virtual {v2, v1, v3, v4}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@93
    goto :goto_1e

    #@94
    .line 3114
    :sswitch_94
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@96
    invoke-static {v1, p1}, Landroid/net/wifi/WifiStateMachine;->access$8700(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@99
    goto :goto_1e

    #@9a
    .line 3069
    :sswitch_data_9a
    .sparse-switch
        0x2000d -> :sswitch_94
        0x2000e -> :sswitch_94
        0x20013 -> :sswitch_20
        0x20047 -> :sswitch_94
        0x20049 -> :sswitch_94
        0x2004a -> :sswitch_94
        0x2004b -> :sswitch_94
        0x2004c -> :sswitch_94
        0x20050 -> :sswitch_94
        0x20054 -> :sswitch_94
        0x20055 -> :sswitch_94
        0x2005a -> :sswitch_94
        0x24003 -> :sswitch_94
        0x24004 -> :sswitch_94
        0x24006 -> :sswitch_7
        0x24007 -> :sswitch_94
        0x2400a -> :sswitch_94
    .end sparse-switch
.end method
