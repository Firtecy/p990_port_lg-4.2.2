.class Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;
.super Ljava/lang/Object;
.source "WifiWatchdogStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiWatchdogStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeWeightedEMA"
.end annotation


# instance fields
.field private final mAlpha:D

.field private mProduct:D

.field private mValue:D

.field private mVolume:D

.field final synthetic this$0:Landroid/net/wifi/WifiWatchdogStateMachine;


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiWatchdogStateMachine;D)V
    .registers 6
    .parameter
    .parameter "coefficient"

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 1019
    iput-object p1, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->this$0:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 1020
    iput-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mValue:D

    #@9
    .line 1021
    iput-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mVolume:D

    #@b
    .line 1022
    iput-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mProduct:D

    #@d
    .line 1023
    iput-wide p2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mAlpha:D

    #@f
    .line 1024
    return-void
.end method

.method static synthetic access$4300(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 1013
    iget-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mValue:D

    #@2
    return-wide v0
.end method

.method static synthetic access$4400(Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;)D
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 1013
    iget-wide v0, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mVolume:D

    #@2
    return-wide v0
.end method


# virtual methods
.method public update(DI)V
    .registers 14
    .parameter "newValue"
    .parameter "newVolume"

    #@0
    .prologue
    const-wide/high16 v8, 0x3ff0

    #@2
    .line 1027
    if-gtz p3, :cond_5

    #@4
    .line 1033
    :goto_4
    return-void

    #@5
    .line 1029
    :cond_5
    int-to-double v2, p3

    #@6
    mul-double v0, p1, v2

    #@8
    .line 1030
    .local v0, newProduct:D
    iget-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mAlpha:D

    #@a
    mul-double/2addr v2, v0

    #@b
    iget-wide v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mAlpha:D

    #@d
    sub-double v4, v8, v4

    #@f
    iget-wide v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mProduct:D

    #@11
    mul-double/2addr v4, v6

    #@12
    add-double/2addr v2, v4

    #@13
    iput-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mProduct:D

    #@15
    .line 1031
    iget-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mAlpha:D

    #@17
    int-to-double v4, p3

    #@18
    mul-double/2addr v2, v4

    #@19
    iget-wide v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mAlpha:D

    #@1b
    sub-double v4, v8, v4

    #@1d
    iget-wide v6, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mVolume:D

    #@1f
    mul-double/2addr v4, v6

    #@20
    add-double/2addr v2, v4

    #@21
    iput-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mVolume:D

    #@23
    .line 1032
    iget-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mProduct:D

    #@25
    iget-wide v4, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mVolume:D

    #@27
    div-double/2addr v2, v4

    #@28
    iput-wide v2, p0, Landroid/net/wifi/WifiWatchdogStateMachine$VolumeWeightedEMA;->mValue:D

    #@2a
    goto :goto_4
.end method
