.class public Landroid/net/wifi/WifiInfo;
.super Ljava/lang/Object;
.source "WifiInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/wifi/WifiInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final LINK_SPEED_UNITS:Ljava/lang/String; = "Mbps"

.field private static final TAG:Ljava/lang/String; = "WifiInfo"

.field private static final stateMap:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Landroid/net/wifi/SupplicantState;",
            "Landroid/net/NetworkInfo$DetailedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBSSID:Ljava/lang/String;

.field private mHiddenSSID:Z

.field private mIpAddress:Ljava/net/InetAddress;

.field private mLinkSpeed:I

.field private mMacAddress:Ljava/lang/String;

.field private mMeteredHint:Z

.field private mNetworkId:I

.field private mRssi:I

.field private mSupplicantState:Landroid/net/wifi/SupplicantState;

.field private mWifiSsid:Landroid/net/wifi/WifiSsid;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 41
    new-instance v0, Ljava/util/EnumMap;

    #@2
    const-class v1, Landroid/net/wifi/SupplicantState;

    #@4
    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    #@7
    sput-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@9
    .line 45
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@b
    sget-object v1, Landroid/net/wifi/SupplicantState;->DISCONNECTED:Landroid/net/wifi/SupplicantState;

    #@d
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@f
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    .line 46
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@14
    sget-object v1, Landroid/net/wifi/SupplicantState;->INTERFACE_DISABLED:Landroid/net/wifi/SupplicantState;

    #@16
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 47
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@1d
    sget-object v1, Landroid/net/wifi/SupplicantState;->INACTIVE:Landroid/net/wifi/SupplicantState;

    #@1f
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@21
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 48
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@26
    sget-object v1, Landroid/net/wifi/SupplicantState;->SCANNING:Landroid/net/wifi/SupplicantState;

    #@28
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    #@2a
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 49
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@2f
    sget-object v1, Landroid/net/wifi/SupplicantState;->AUTHENTICATING:Landroid/net/wifi/SupplicantState;

    #@31
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@33
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 50
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@38
    sget-object v1, Landroid/net/wifi/SupplicantState;->ASSOCIATING:Landroid/net/wifi/SupplicantState;

    #@3a
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 51
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@41
    sget-object v1, Landroid/net/wifi/SupplicantState;->ASSOCIATED:Landroid/net/wifi/SupplicantState;

    #@43
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@45
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 52
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@4a
    sget-object v1, Landroid/net/wifi/SupplicantState;->FOUR_WAY_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@4c
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    #@4e
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    .line 53
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@53
    sget-object v1, Landroid/net/wifi/SupplicantState;->GROUP_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@55
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    #@57
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    .line 54
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@5c
    sget-object v1, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    #@5e
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    #@60
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    .line 55
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@65
    sget-object v1, Landroid/net/wifi/SupplicantState;->DORMANT:Landroid/net/wifi/SupplicantState;

    #@67
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@69
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@6c
    .line 56
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@6e
    sget-object v1, Landroid/net/wifi/SupplicantState;->UNINITIALIZED:Landroid/net/wifi/SupplicantState;

    #@70
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@72
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@75
    .line 57
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@77
    sget-object v1, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@79
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@7b
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@7e
    .line 339
    new-instance v0, Landroid/net/wifi/WifiInfo$1;

    #@80
    invoke-direct {v0}, Landroid/net/wifi/WifiInfo$1;-><init>()V

    #@83
    sput-object v0, Landroid/net/wifi/WifiInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@85
    return-void
.end method

.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 82
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@7
    .line 83
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@9
    .line 84
    iput v1, p0, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@b
    .line 85
    sget-object v0, Landroid/net/wifi/SupplicantState;->UNINITIALIZED:Landroid/net/wifi/SupplicantState;

    #@d
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@f
    .line 86
    const/16 v0, -0x270f

    #@11
    iput v0, p0, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@13
    .line 87
    iput v1, p0, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@15
    .line 88
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Landroid/net/wifi/WifiInfo;->mHiddenSSID:Z

    #@18
    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/net/wifi/WifiInfo;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 95
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 96
    if-eqz p1, :cond_2d

    #@5
    .line 97
    iget-object v0, p1, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@7
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@9
    .line 98
    iget-object v0, p1, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@b
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@d
    .line 99
    iget-object v0, p1, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@f
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@11
    .line 100
    iget v0, p1, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@13
    iput v0, p0, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@15
    .line 101
    iget-boolean v0, p1, Landroid/net/wifi/WifiInfo;->mHiddenSSID:Z

    #@17
    iput-boolean v0, p0, Landroid/net/wifi/WifiInfo;->mHiddenSSID:Z

    #@19
    .line 102
    iget v0, p1, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@1b
    iput v0, p0, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@1d
    .line 103
    iget v0, p1, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@1f
    iput v0, p0, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@21
    .line 104
    iget-object v0, p1, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@23
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@25
    .line 105
    iget-object v0, p1, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@27
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@29
    .line 106
    iget-boolean v0, p1, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@2b
    iput-boolean v0, p0, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@2d
    .line 108
    :cond_2d
    return-void
.end method

.method static synthetic access$002(Landroid/net/wifi/WifiInfo;Landroid/net/wifi/WifiSsid;)Landroid/net/wifi/WifiSsid;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Landroid/net/wifi/WifiInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$202(Landroid/net/wifi/WifiInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$302(Landroid/net/wifi/WifiInfo;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-boolean p1, p0, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@2
    return p1
.end method

.method static synthetic access$402(Landroid/net/wifi/WifiInfo;Landroid/net/wifi/SupplicantState;)Landroid/net/wifi/SupplicantState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@2
    return-object p1
.end method

.method public static getDetailedStateOf(Landroid/net/wifi/SupplicantState;)Landroid/net/NetworkInfo$DetailedState;
    .registers 2
    .parameter "suppState"

    #@0
    .prologue
    .line 257
    sget-object v0, Landroid/net/wifi/WifiInfo;->stateMap:Ljava/util/EnumMap;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/NetworkInfo$DetailedState;

    #@8
    return-object v0
.end method

.method public static removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "string"

    #@0
    .prologue
    const/16 v3, 0x22

    #@2
    const/4 v2, 0x1

    #@3
    .line 284
    if-nez p0, :cond_7

    #@5
    const/4 p0, 0x0

    #@6
    .line 289
    .end local p0
    .local v0, length:I
    :cond_6
    :goto_6
    return-object p0

    #@7
    .line 285
    .end local v0           #length:I
    .restart local p0
    :cond_7
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@a
    move-result v0

    #@b
    .line 286
    .restart local v0       #length:I
    if-le v0, v2, :cond_6

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v1

    #@12
    if-ne v1, v3, :cond_6

    #@14
    add-int/lit8 v1, v0, -0x1

    #@16
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@19
    move-result v1

    #@1a
    if-ne v1, v3, :cond_6

    #@1c
    .line 287
    add-int/lit8 v1, v0, -0x1

    #@1e
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@21
    move-result-object p0

    #@22
    goto :goto_6
.end method

.method static valueOf(Ljava/lang/String;)Landroid/net/wifi/SupplicantState;
    .registers 3
    .parameter "stateName"

    #@0
    .prologue
    .line 271
    const-string v1, "4WAY_HANDSHAKE"

    #@2
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_b

    #@8
    .line 272
    sget-object v1, Landroid/net/wifi/SupplicantState;->FOUR_WAY_HANDSHAKE:Landroid/net/wifi/SupplicantState;

    #@a
    .line 277
    :goto_a
    return-object v1

    #@b
    .line 275
    :cond_b
    :try_start_b
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-static {v1}, Landroid/net/wifi/SupplicantState;->valueOf(Ljava/lang/String;)Landroid/net/wifi/SupplicantState;
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    goto :goto_a

    #@14
    .line 276
    :catch_14
    move-exception v0

    #@15
    .line 277
    .local v0, e:Ljava/lang/IllegalArgumentException;
    sget-object v1, Landroid/net/wifi/SupplicantState;->INVALID:Landroid/net/wifi/SupplicantState;

    #@17
    goto :goto_a
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 312
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getBSSID()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHiddenSSID()Z
    .registers 2

    #@0
    .prologue
    .line 243
    iget-boolean v0, p0, Landroid/net/wifi/WifiInfo;->mHiddenSSID:Z

    #@2
    return v0
.end method

.method public getIpAddress()I
    .registers 2

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@6
    instance-of v0, v0, Ljava/net/Inet6Address;

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 235
    :goto_b
    return v0

    #@c
    :cond_c
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@e
    invoke-static {v0}, Landroid/net/NetworkUtils;->inetAddressToInt(Ljava/net/InetAddress;)I

    #@11
    move-result v0

    #@12
    goto :goto_b
.end method

.method public getLinkSpeed()I
    .registers 2

    #@0
    .prologue
    .line 173
    iget v0, p0, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@2
    return v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMeteredHint()Z
    .registers 2

    #@0
    .prologue
    .line 199
    iget-boolean v0, p0, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@2
    return v0
.end method

.method public getNetworkId()I
    .registers 2

    #@0
    .prologue
    .line 213
    iget v0, p0, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@2
    return v0
.end method

.method public getRssi()I
    .registers 2

    #@0
    .prologue
    .line 160
    iget v0, p0, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@2
    return v0
.end method

.method public getSSID()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 124
    iget-object v1, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@2
    if-eqz v1, :cond_31

    #@4
    .line 125
    iget-object v1, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@6
    invoke-virtual {v1}, Landroid/net/wifi/WifiSsid;->toString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 126
    .local v0, unicode:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_2a

    #@10
    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "\""

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "\""

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 132
    .end local v0           #unicode:Ljava/lang/String;
    :goto_29
    return-object v1

    #@2a
    .line 129
    .restart local v0       #unicode:Ljava/lang/String;
    :cond_2a
    iget-object v1, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@2c
    invoke-virtual {v1}, Landroid/net/wifi/WifiSsid;->getHexString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    goto :goto_29

    #@31
    .line 132
    .end local v0           #unicode:Ljava/lang/String;
    :cond_31
    const-string v1, "<unknown ssid>"

    #@33
    goto :goto_29
.end method

.method public getSupplicantState()Landroid/net/wifi/SupplicantState;
    .registers 2

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@2
    return-object v0
.end method

.method public getWifiSsid()Landroid/net/wifi/WifiSsid;
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@2
    return-object v0
.end method

.method setBSSID(Ljava/lang/String;)V
    .registers 2
    .parameter "BSSID"

    #@0
    .prologue
    .line 141
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@2
    .line 142
    return-void
.end method

.method public setHiddenSSID(Z)V
    .registers 2
    .parameter "hiddenSSID"

    #@0
    .prologue
    .line 248
    iput-boolean p1, p0, Landroid/net/wifi/WifiInfo;->mHiddenSSID:Z

    #@2
    .line 249
    return-void
.end method

.method setInetAddress(Ljava/net/InetAddress;)V
    .registers 2
    .parameter "address"

    #@0
    .prologue
    .line 230
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@2
    .line 231
    return-void
.end method

.method setLinkSpeed(I)V
    .registers 2
    .parameter "linkSpeed"

    #@0
    .prologue
    .line 177
    iput p1, p0, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@2
    .line 178
    return-void
.end method

.method setMacAddress(Ljava/lang/String;)V
    .registers 2
    .parameter "macAddress"

    #@0
    .prologue
    .line 185
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@2
    .line 186
    return-void
.end method

.method public setMeteredHint(Z)V
    .registers 2
    .parameter "meteredHint"

    #@0
    .prologue
    .line 194
    iput-boolean p1, p0, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@2
    .line 195
    return-void
.end method

.method setNetworkId(I)V
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 203
    iput p1, p0, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@2
    .line 204
    return-void
.end method

.method setRssi(I)V
    .registers 2
    .parameter "rssi"

    #@0
    .prologue
    .line 164
    iput p1, p0, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@2
    .line 165
    return-void
.end method

.method setSSID(Landroid/net/wifi/WifiSsid;)V
    .registers 3
    .parameter "wifiSsid"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@2
    .line 113
    const/4 v0, 0x0

    #@3
    iput-boolean v0, p0, Landroid/net/wifi/WifiInfo;->mHiddenSSID:Z

    #@5
    .line 114
    return-void
.end method

.method setSupplicantState(Landroid/net/wifi/SupplicantState;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 226
    iput-object p1, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@2
    .line 227
    return-void
.end method

.method setSupplicantState(Ljava/lang/String;)V
    .registers 3
    .parameter "stateName"

    #@0
    .prologue
    .line 267
    invoke-static {p1}, Landroid/net/wifi/WifiInfo;->valueOf(Ljava/lang/String;)Landroid/net/wifi/SupplicantState;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@6
    .line 268
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 294
    new-instance v1, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 295
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string v0, "<none>"

    #@7
    .line 297
    .local v0, none:Ljava/lang/String;
    const-string v2, "SSID: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c
    move-result-object v3

    #@d
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@f
    if-nez v2, :cond_77

    #@11
    const-string v2, "<unknown ssid>"

    #@13
    :goto_13
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", BSSID: "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1c
    move-result-object v3

    #@1d
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@1f
    if-nez v2, :cond_7a

    #@21
    move-object v2, v0

    #@22
    :goto_22
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@25
    move-result-object v2

    #@26
    const-string v3, ", MAC: "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2b
    move-result-object v3

    #@2c
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@2e
    if-nez v2, :cond_7d

    #@30
    move-object v2, v0

    #@31
    :goto_31
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@34
    move-result-object v2

    #@35
    const-string v3, ", Supplicant state: "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3a
    move-result-object v2

    #@3b
    iget-object v3, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@3d
    if-nez v3, :cond_80

    #@3f
    .end local v0           #none:Ljava/lang/String;
    :goto_3f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    #@42
    move-result-object v2

    #@43
    const-string v3, ", RSSI: "

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@48
    move-result-object v2

    #@49
    iget v3, p0, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@4e
    move-result-object v2

    #@4f
    const-string v3, ", Link speed: "

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@54
    move-result-object v2

    #@55
    iget v3, p0, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@5a
    move-result-object v2

    #@5b
    const-string v3, ", Net ID: "

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@60
    move-result-object v2

    #@61
    iget v3, p0, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@66
    move-result-object v2

    #@67
    const-string v3, ", Metered hint: "

    #@69
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6c
    move-result-object v2

    #@6d
    iget-boolean v3, p0, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    #@72
    .line 307
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    return-object v2

    #@77
    .line 297
    .restart local v0       #none:Ljava/lang/String;
    :cond_77
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@79
    goto :goto_13

    #@7a
    :cond_7a
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@7c
    goto :goto_22

    #@7d
    :cond_7d
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@7f
    goto :goto_31

    #@80
    :cond_80
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@82
    goto :goto_3f
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 317
    iget v2, p0, Landroid/net/wifi/WifiInfo;->mNetworkId:I

    #@4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 318
    iget v2, p0, Landroid/net/wifi/WifiInfo;->mRssi:I

    #@9
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 319
    iget v2, p0, Landroid/net/wifi/WifiInfo;->mLinkSpeed:I

    #@e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 320
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@13
    if-eqz v2, :cond_44

    #@15
    .line 321
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@18
    .line 322
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mIpAddress:Ljava/net/InetAddress;

    #@1a
    invoke-virtual {v2}, Ljava/net/InetAddress;->getAddress()[B

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@21
    .line 326
    :goto_21
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@23
    if-eqz v2, :cond_48

    #@25
    .line 327
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 328
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mWifiSsid:Landroid/net/wifi/WifiSsid;

    #@2a
    invoke-virtual {v2, p1, p2}, Landroid/net/wifi/WifiSsid;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 332
    :goto_2d
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mBSSID:Ljava/lang/String;

    #@2f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@32
    .line 333
    iget-object v2, p0, Landroid/net/wifi/WifiInfo;->mMacAddress:Ljava/lang/String;

    #@34
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 334
    iget-boolean v2, p0, Landroid/net/wifi/WifiInfo;->mMeteredHint:Z

    #@39
    if-eqz v2, :cond_4c

    #@3b
    :goto_3b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 335
    iget-object v0, p0, Landroid/net/wifi/WifiInfo;->mSupplicantState:Landroid/net/wifi/SupplicantState;

    #@40
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/SupplicantState;->writeToParcel(Landroid/os/Parcel;I)V

    #@43
    .line 336
    return-void

    #@44
    .line 324
    :cond_44
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    #@47
    goto :goto_21

    #@48
    .line 330
    :cond_48
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4b
    goto :goto_2d

    #@4c
    :cond_4c
    move v0, v1

    #@4d
    .line 334
    goto :goto_3b
.end method
