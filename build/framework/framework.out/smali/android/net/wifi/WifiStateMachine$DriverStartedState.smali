.class Landroid/net/wifi/WifiStateMachine$DriverStartedState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DriverStartedState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3132
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 3136
    const v3, 0xc365

    #@5
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->getName()Ljava/lang/String;

    #@8
    move-result-object v4

    #@9
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@c
    .line 3138
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e
    invoke-static {v3, v2}, Landroid/net/wifi/WifiStateMachine;->access$8902(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@11
    .line 3139
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13
    invoke-static {v3, v1}, Landroid/net/wifi/WifiStateMachine;->access$9002(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@16
    .line 3140
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@18
    const/4 v4, 0x0

    #@19
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiStateMachine;->updateBatteryWorkSource(Landroid/os/WorkSource;)V

    #@1c
    .line 3147
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1e
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@21
    move-result-object v3

    #@22
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@24
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$800(Landroid/net/wifi/WifiStateMachine;)Z

    #@27
    move-result v4

    #@28
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiNative;->setBluetoothCoexistenceScanMode(Z)Z

    #@2b
    .line 3149
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2d
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$9100(Landroid/net/wifi/WifiStateMachine;)V

    #@30
    .line 3151
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@32
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$9200(Landroid/net/wifi/WifiStateMachine;)V

    #@35
    .line 3153
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@37
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@39
    invoke-static {v3, v4}, Landroid/net/wifi/WifiStateMachine;->access$9300(Landroid/net/wifi/WifiStateMachine;Landroid/net/NetworkInfo$DetailedState;)V

    #@3c
    .line 3156
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3e
    if-eqz v3, :cond_c6

    #@40
    .line 3157
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@42
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->startFilteringMulticastV6Packets()Z

    #@49
    .line 3162
    :goto_49
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4b
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$9400(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_d1

    #@55
    .line 3163
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@57
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->startFilteringMulticastV4Packets()Z

    #@5e
    .line 3168
    :goto_5e
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@60
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$6400(Landroid/net/wifi/WifiStateMachine;)Z

    #@63
    move-result v3

    #@64
    if-eqz v3, :cond_db

    #@66
    .line 3169
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@68
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@6b
    move-result-object v3

    #@6c
    const/4 v4, 0x2

    #@6d
    invoke-virtual {v3, v4}, Landroid/net/wifi/WifiNative;->setScanResultHandling(I)Z

    #@70
    .line 3170
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@72
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@79
    .line 3171
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7b
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7d
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$9500(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@80
    move-result-object v4

    #@81
    invoke-static {v3, v4}, Landroid/net/wifi/WifiStateMachine;->access$9600(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@84
    .line 3183
    :goto_84
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@86
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$9900(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@8d
    move-result v3

    #@8e
    if-nez v3, :cond_102

    #@90
    .line 3184
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@92
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@95
    move-result-object v1

    #@96
    const-string/jumbo v3, "power"

    #@99
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9c
    move-result-object v0

    #@9d
    check-cast v0, Landroid/os/PowerManager;

    #@9f
    .line 3186
    .local v0, powerManager:Landroid/os/PowerManager;
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a1
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    #@a4
    move-result v3

    #@a5
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$000(Landroid/net/wifi/WifiStateMachine;Z)V

    #@a8
    .line 3192
    .end local v0           #powerManager:Landroid/os/PowerManager;
    :goto_a8
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@aa
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiNative;->setPowerSave(Z)V

    #@b1
    .line 3194
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b3
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$3500(Landroid/net/wifi/WifiStateMachine;)Z

    #@b6
    move-result v1

    #@b7
    if-eqz v1, :cond_c5

    #@b9
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@bb
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@be
    move-result-object v1

    #@bf
    const v2, 0x20083

    #@c2
    invoke-virtual {v1, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@c5
    .line 3195
    :cond_c5
    return-void

    #@c6
    .line 3159
    :cond_c6
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c8
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@cb
    move-result-object v3

    #@cc
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->stopFilteringMulticastV6Packets()Z

    #@cf
    goto/16 :goto_49

    #@d1
    .line 3165
    :cond_d1
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d3
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@d6
    move-result-object v3

    #@d7
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->stopFilteringMulticastV4Packets()Z

    #@da
    goto :goto_5e

    #@db
    .line 3173
    :cond_db
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@dd
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@e0
    move-result-object v3

    #@e1
    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiNative;->setScanResultHandling(I)Z

    #@e4
    .line 3174
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e6
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@e9
    move-result-object v3

    #@ea
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->reconnect()Z

    #@ed
    .line 3178
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ef
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@f2
    move-result-object v3

    #@f3
    invoke-virtual {v3}, Landroid/net/wifi/WifiNative;->status()Ljava/lang/String;

    #@f6
    .line 3179
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f8
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@fa
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$9700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@fd
    move-result-object v4

    #@fe
    invoke-static {v3, v4}, Landroid/net/wifi/WifiStateMachine;->access$9800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@101
    goto :goto_84

    #@102
    .line 3189
    :cond_102
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@104
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@107
    move-result-object v3

    #@108
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@10a
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$10000(Landroid/net/wifi/WifiStateMachine;)I

    #@10d
    move-result v4

    #@10e
    if-nez v4, :cond_11d

    #@110
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@112
    invoke-static {v4}, Landroid/net/wifi/WifiStateMachine;->access$200(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@115
    move-result-object v4

    #@116
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@119
    move-result v4

    #@11a
    if-eqz v4, :cond_11d

    #@11c
    move v1, v2

    #@11d
    :cond_11d
    invoke-virtual {v3, v1}, Landroid/net/wifi/WifiNative;->setSuspendOptimizations(Z)Z

    #@120
    goto :goto_a8
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 3339
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/net/wifi/WifiStateMachine;->access$8902(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@6
    .line 3340
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiStateMachine;->updateBatteryWorkSource(Landroid/os/WorkSource;)V

    #@c
    .line 3341
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e
    new-instance v1, Ljava/util/ArrayList;

    #@10
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@13
    invoke-static {v0, v1}, Landroid/net/wifi/WifiStateMachine;->access$11202(Landroid/net/wifi/WifiStateMachine;Ljava/util/List;)Ljava/util/List;

    #@16
    .line 3342
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x2

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    .line 3199
    iget v7, p1, Landroid/os/Message;->what:I

    #@6
    sparse-switch v7, :sswitch_data_260

    #@9
    .line 3334
    :goto_9
    return v5

    #@a
    .line 3201
    :sswitch_a
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@e
    if-ne v8, v6, :cond_11

    #@10
    move v5, v6

    #@11
    :cond_11
    invoke-static {v7, v5}, Landroid/net/wifi/WifiStateMachine;->access$10102(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@14
    .line 3202
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@16
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@19
    move-result-object v5

    #@1a
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10100(Landroid/net/wifi/WifiStateMachine;)Z

    #@1f
    move-result v7

    #@20
    invoke-virtual {v5, v7}, Landroid/net/wifi/WifiNative;->setScanMode(Z)Z

    #@23
    :cond_23
    :goto_23
    move v5, v6

    #@24
    .line 3334
    goto :goto_9

    #@25
    .line 3205
    :sswitch_25
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@27
    if-ne v7, v6, :cond_65

    #@29
    move v3, v6

    #@2a
    .line 3206
    .local v3, forceActive:Z
    :goto_2a
    if-eqz v3, :cond_3d

    #@2c
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2e
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$10100(Landroid/net/wifi/WifiStateMachine;)Z

    #@31
    move-result v5

    #@32
    if-nez v5, :cond_3d

    #@34
    .line 3207
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@36
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, v3}, Landroid/net/wifi/WifiNative;->setScanMode(Z)Z

    #@3d
    .line 3209
    :cond_3d
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3f
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->scan()Z

    #@46
    .line 3210
    if-eqz v3, :cond_5f

    #@48
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4a
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$10100(Landroid/net/wifi/WifiStateMachine;)Z

    #@4d
    move-result v5

    #@4e
    if-nez v5, :cond_5f

    #@50
    .line 3211
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@52
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@55
    move-result-object v5

    #@56
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@58
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10100(Landroid/net/wifi/WifiStateMachine;)Z

    #@5b
    move-result v7

    #@5c
    invoke-virtual {v5, v7}, Landroid/net/wifi/WifiNative;->setScanMode(Z)Z

    #@5f
    .line 3213
    :cond_5f
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@61
    invoke-static {v5, v6}, Landroid/net/wifi/WifiStateMachine;->access$7602(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@64
    goto :goto_23

    #@65
    .end local v3           #forceActive:Z
    :cond_65
    move v3, v5

    #@66
    .line 3205
    goto :goto_2a

    #@67
    .line 3216
    :sswitch_67
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69
    check-cast v1, Ljava/lang/String;

    #@6b
    .line 3218
    .local v1, country:Ljava/lang/String;
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@6d
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v5, v7}, Landroid/net/wifi/WifiNative;->setCountryCode(Ljava/lang/String;)Z

    #@78
    move-result v5

    #@79
    if-nez v5, :cond_23

    #@7b
    .line 3219
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@7d
    new-instance v7, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v8, "Failed to set country code "

    #@84
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v7

    #@88
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    invoke-static {v5, v7}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@93
    goto :goto_23

    #@94
    .line 3223
    .end local v1           #country:Ljava/lang/String;
    :sswitch_94
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@96
    .line 3225
    .local v0, band:I
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@98
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@9b
    move-result-object v5

    #@9c
    invoke-virtual {v5, v0}, Landroid/net/wifi/WifiNative;->setBand(I)Z

    #@9f
    move-result v5

    #@a0
    if-eqz v5, :cond_ad

    #@a2
    .line 3226
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a4
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$10200(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicInteger;

    #@a7
    move-result-object v5

    #@a8
    invoke-virtual {v5, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@ab
    goto/16 :goto_23

    #@ad
    .line 3232
    :cond_ad
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@af
    new-instance v7, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v8, "Failed to set frequency band "

    #@b6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v7

    #@ba
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v7

    #@be
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v7

    #@c2
    invoke-static {v5, v7}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@c5
    goto/16 :goto_23

    #@c7
    .line 3236
    .end local v0           #band:I
    :sswitch_c7
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c9
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@cb
    if-eqz v8, :cond_ce

    #@cd
    move v5, v6

    #@ce
    :cond_ce
    invoke-static {v7, v5}, Landroid/net/wifi/WifiStateMachine;->access$802(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@d1
    .line 3238
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d3
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@d6
    move-result-object v5

    #@d7
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d9
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$800(Landroid/net/wifi/WifiStateMachine;)Z

    #@dc
    move-result v7

    #@dd
    invoke-virtual {v5, v7}, Landroid/net/wifi/WifiNative;->setBluetoothCoexistenceScanMode(Z)Z

    #@e0
    goto/16 :goto_23

    #@e2
    .line 3241
    :sswitch_e2
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@e4
    .line 3244
    .local v4, mode:I
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e6
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$9000(Landroid/net/wifi/WifiStateMachine;)Z

    #@e9
    move-result v7

    #@ea
    if-eqz v7, :cond_ee

    #@ec
    if-ne v4, v6, :cond_23

    #@ee
    .line 3248
    :cond_ee
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f0
    invoke-static {v7, v6}, Landroid/net/wifi/WifiStateMachine;->access$9002(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@f3
    .line 3249
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f5
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10308(Landroid/net/wifi/WifiStateMachine;)I

    #@f8
    .line 3252
    if-ne v4, v6, :cond_110

    #@fa
    .line 3254
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@fc
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@fe
    const v9, 0x20012

    #@101
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@103
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$10300(Landroid/net/wifi/WifiStateMachine;)I

    #@106
    move-result v10

    #@107
    invoke-virtual {v8, v9, v10, v5}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@10a
    move-result-object v5

    #@10b
    invoke-virtual {v7, v5}, Landroid/net/wifi/WifiStateMachine;->sendMessage(Landroid/os/Message;)V

    #@10e
    goto/16 :goto_23

    #@110
    .line 3257
    :cond_110
    new-instance v2, Landroid/content/Intent;

    #@112
    const-string v7, "com.android.server.WifiManager.action.DELAYED_DRIVER_STOP"

    #@114
    const/4 v8, 0x0

    #@115
    invoke-direct {v2, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@118
    .line 3258
    .local v2, driverStopIntent:Landroid/content/Intent;
    const-string v7, "DelayedStopCounter"

    #@11a
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11c
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$10300(Landroid/net/wifi/WifiStateMachine;)I

    #@11f
    move-result v8

    #@120
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@123
    .line 3259
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@125
    iget-object v8, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@127
    invoke-static {v8}, Landroid/net/wifi/WifiStateMachine;->access$100(Landroid/net/wifi/WifiStateMachine;)Landroid/content/Context;

    #@12a
    move-result-object v8

    #@12b
    const/high16 v9, 0x800

    #@12d
    invoke-static {v8, v5, v2, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@130
    move-result-object v8

    #@131
    invoke-static {v7, v8}, Landroid/net/wifi/WifiStateMachine;->access$10402(Landroid/net/wifi/WifiStateMachine;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    #@134
    .line 3263
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@136
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10600(Landroid/net/wifi/WifiStateMachine;)Landroid/app/AlarmManager;

    #@139
    move-result-object v7

    #@13a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@13d
    move-result-wide v8

    #@13e
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@140
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$10500(Landroid/net/wifi/WifiStateMachine;)I

    #@143
    move-result v10

    #@144
    int-to-long v10, v10

    #@145
    add-long/2addr v8, v10

    #@146
    iget-object v10, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@148
    invoke-static {v10}, Landroid/net/wifi/WifiStateMachine;->access$10400(Landroid/net/wifi/WifiStateMachine;)Landroid/app/PendingIntent;

    #@14b
    move-result-object v10

    #@14c
    invoke-virtual {v7, v5, v8, v9, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@14f
    goto/16 :goto_23

    #@151
    .line 3268
    .end local v2           #driverStopIntent:Landroid/content/Intent;
    .end local v4           #mode:I
    :sswitch_151
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@153
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$9000(Landroid/net/wifi/WifiStateMachine;)Z

    #@156
    move-result v7

    #@157
    if-eqz v7, :cond_23

    #@159
    .line 3269
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@15b
    invoke-static {v7, v5}, Landroid/net/wifi/WifiStateMachine;->access$9002(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@15e
    .line 3270
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@160
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$10308(Landroid/net/wifi/WifiStateMachine;)I

    #@163
    .line 3271
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@165
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$10600(Landroid/net/wifi/WifiStateMachine;)Landroid/app/AlarmManager;

    #@168
    move-result-object v5

    #@169
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@16b
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10400(Landroid/net/wifi/WifiStateMachine;)Landroid/app/PendingIntent;

    #@16e
    move-result-object v7

    #@16f
    invoke-virtual {v5, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@172
    goto/16 :goto_23

    #@174
    .line 3277
    :sswitch_174
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@176
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@178
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10300(Landroid/net/wifi/WifiStateMachine;)I

    #@17b
    move-result v7

    #@17c
    if-ne v5, v7, :cond_23

    #@17e
    .line 3278
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@180
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$10700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/IState;

    #@183
    move-result-object v5

    #@184
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@186
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$9700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@189
    move-result-object v7

    #@18a
    if-eq v5, v7, :cond_1ca

    #@18c
    .line 3279
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@18e
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@191
    move-result-object v5

    #@192
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@195
    .line 3280
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@197
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$7100(Landroid/net/wifi/WifiStateMachine;)V

    #@19a
    .line 3289
    :cond_19a
    :goto_19a
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@19c
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$2600(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@19f
    move-result-object v5

    #@1a0
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@1a3
    .line 3290
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1a5
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->stopDriver()Z

    #@1ac
    .line 3291
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1ae
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$2600(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@1b1
    move-result-object v5

    #@1b2
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1b5
    .line 3292
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1b7
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$3500(Landroid/net/wifi/WifiStateMachine;)Z

    #@1ba
    move-result v5

    #@1bb
    if-eqz v5, :cond_1d8

    #@1bd
    .line 3293
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1bf
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1c1
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$6700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1c4
    move-result-object v7

    #@1c5
    invoke-static {v5, v7}, Landroid/net/wifi/WifiStateMachine;->access$10800(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@1c8
    goto/16 :goto_23

    #@1ca
    .line 3286
    :cond_1ca
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@1cc
    if-eqz v5, :cond_19a

    #@1ce
    .line 3287
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1d0
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@1d3
    move-result-object v5

    #@1d4
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->disconnect()Z

    #@1d7
    goto :goto_19a

    #@1d8
    .line 3295
    :cond_1d8
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1da
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1dc
    invoke-static {v7}, Landroid/net/wifi/WifiStateMachine;->access$10900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1df
    move-result-object v7

    #@1e0
    invoke-static {v5, v7}, Landroid/net/wifi/WifiStateMachine;->access$11000(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@1e3
    goto/16 :goto_23

    #@1e5
    .line 3299
    :sswitch_1e5
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@1e7
    if-ne v5, v6, :cond_1f4

    #@1e9
    .line 3300
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1eb
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@1ee
    move-result-object v5

    #@1ef
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->startFilteringMulticastV6Packets()Z

    #@1f2
    goto/16 :goto_23

    #@1f4
    .line 3301
    :cond_1f4
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@1f6
    if-nez v5, :cond_203

    #@1f8
    .line 3302
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1fa
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@1fd
    move-result-object v5

    #@1fe
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->startFilteringMulticastV4Packets()Z

    #@201
    goto/16 :goto_23

    #@203
    .line 3304
    :cond_203
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@205
    const-string v7, "Illegal arugments to CMD_START_PACKET_FILTERING"

    #@207
    invoke-static {v5, v7}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@20a
    goto/16 :goto_23

    #@20c
    .line 3308
    :sswitch_20c
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@20e
    if-ne v5, v6, :cond_21b

    #@210
    .line 3309
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@212
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@215
    move-result-object v5

    #@216
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->stopFilteringMulticastV6Packets()Z

    #@219
    goto/16 :goto_23

    #@21b
    .line 3310
    :cond_21b
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@21d
    if-nez v5, :cond_22a

    #@21f
    .line 3311
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@221
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1900(Landroid/net/wifi/WifiStateMachine;)Landroid/net/wifi/WifiNative;

    #@224
    move-result-object v5

    #@225
    invoke-virtual {v5}, Landroid/net/wifi/WifiNative;->stopFilteringMulticastV4Packets()Z

    #@228
    goto/16 :goto_23

    #@22a
    .line 3313
    :cond_22a
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@22c
    const-string v7, "Illegal arugments to CMD_STOP_PACKET_FILTERING"

    #@22e
    invoke-static {v5, v7}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@231
    goto/16 :goto_23

    #@233
    .line 3317
    :sswitch_233
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@235
    if-ne v7, v6, :cond_247

    #@237
    .line 3318
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@239
    invoke-static {v5, v9, v6}, Landroid/net/wifi/WifiStateMachine;->access$11100(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@23c
    .line 3319
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@23e
    invoke-static {v5}, Landroid/net/wifi/WifiStateMachine;->access$1500(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@241
    move-result-object v5

    #@242
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@245
    goto/16 :goto_23

    #@247
    .line 3321
    :cond_247
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@249
    invoke-static {v7, v9, v5}, Landroid/net/wifi/WifiStateMachine;->access$11100(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@24c
    goto/16 :goto_23

    #@24e
    .line 3325
    :sswitch_24e
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@250
    if-ne v7, v6, :cond_259

    #@252
    .line 3326
    iget-object v7, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@254
    invoke-static {v7, v8, v5}, Landroid/net/wifi/WifiStateMachine;->access$11100(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@257
    goto/16 :goto_23

    #@259
    .line 3328
    :cond_259
    iget-object v5, p0, Landroid/net/wifi/WifiStateMachine$DriverStartedState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@25b
    invoke-static {v5, v8, v6}, Landroid/net/wifi/WifiStateMachine;->access$11100(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@25e
    goto/16 :goto_23

    #@260
    .line 3199
    :sswitch_data_260
    .sparse-switch
        0x2000d -> :sswitch_151
        0x2000e -> :sswitch_e2
        0x20012 -> :sswitch_174
        0x2001f -> :sswitch_c7
        0x20047 -> :sswitch_25
        0x20049 -> :sswitch_a
        0x2004d -> :sswitch_24e
        0x20050 -> :sswitch_67
        0x20054 -> :sswitch_1e5
        0x20055 -> :sswitch_20c
        0x20056 -> :sswitch_233
        0x2005a -> :sswitch_94
    .end sparse-switch
.end method
