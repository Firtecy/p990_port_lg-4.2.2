.class Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;
.super Ljava/lang/Object;
.source "WifiConfigStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiConfigStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DelayedDiskWrite"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DelayedDiskWrite"

.field private static sDiskWriteHandler:Landroid/os/Handler;

.field private static sDiskWriteHandlerThread:Landroid/os/HandlerThread;

.field private static sWriteSequence:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 797
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 792
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 792
    invoke-static {p0}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->onWriteCalled(Ljava/util/List;)V

    #@3
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 935
    const-string v0, "DelayedDiskWrite"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 936
    return-void
.end method

.method private static onWriteCalled(Ljava/util/List;)V
    .registers 18
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 821
    .local p0, networks:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    const/4 v9, 0x0

    #@1
    .line 823
    .local v9, out:Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v10, Ljava/io/DataOutputStream;

    #@3
    new-instance v14, Ljava/io/BufferedOutputStream;

    #@5
    new-instance v15, Ljava/io/FileOutputStream;

    #@7
    invoke-static {}, Landroid/net/wifi/WifiConfigStore;->access$100()Ljava/lang/String;

    #@a
    move-result-object v16

    #@b
    invoke-direct/range {v15 .. v16}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@e
    invoke-direct {v14, v15}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@11
    invoke-direct {v10, v14}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_216
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_14} :catch_219

    #@14
    .line 826
    .end local v9           #out:Ljava/io/DataOutputStream;
    .local v10, out:Ljava/io/DataOutputStream;
    const/4 v14, 0x2

    #@15
    :try_start_15
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@18
    .line 828
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v4

    #@1c
    :goto_1c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v14

    #@20
    if-eqz v14, :cond_1e2

    #@22
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Landroid/net/wifi/WifiConfiguration;
    :try_end_28
    .catchall {:try_start_15 .. :try_end_28} :catchall_e5
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_28} :catch_63

    #@28
    .line 829
    .local v0, config:Landroid/net/wifi/WifiConfiguration;
    const/4 v13, 0x0

    #@29
    .line 832
    .local v13, writeToFile:Z
    :try_start_29
    iget-object v8, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@2b
    .line 833
    .local v8, linkProperties:Landroid/net/LinkProperties;
    sget-object v14, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$wifi$WifiConfiguration$IpAssignment:[I

    #@2d
    iget-object v15, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@2f
    invoke-virtual {v15}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->ordinal()I

    #@32
    move-result v15

    #@33
    aget v14, v14, v15

    #@35
    packed-switch v14, :pswitch_data_21c

    #@38
    .line 874
    const-string v14, "Ignore invalid ip assignment while writing"

    #@3a
    invoke-static {v14}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->loge(Ljava/lang/String;)V

    #@3d
    .line 878
    :goto_3d
    :pswitch_3d
    sget-object v14, Landroid/net/wifi/WifiConfigStore$1;->$SwitchMap$android$net$wifi$WifiConfiguration$ProxySettings:[I

    #@3f
    iget-object v15, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@41
    invoke-virtual {v15}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->ordinal()I

    #@44
    move-result v15

    #@45
    aget v14, v14, v15

    #@47
    packed-switch v14, :pswitch_data_226

    #@4a
    .line 901
    const-string v14, "Ignthisore invalid proxy settings while writing"

    #@4c
    invoke-static {v14}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->loge(Ljava/lang/String;)V

    #@4f
    .line 904
    :goto_4f
    :pswitch_4f
    if-eqz v13, :cond_5d

    #@51
    .line 905
    const-string v14, "id"

    #@53
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@56
    .line 906
    invoke-static {v0}, Landroid/net/wifi/WifiConfigStore;->access$200(Landroid/net/wifi/WifiConfiguration;)I

    #@59
    move-result v14

    #@5a
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_5d
    .catchall {:try_start_29 .. :try_end_5d} :catchall_e5
    .catch Ljava/lang/NullPointerException; {:try_start_29 .. :try_end_5d} :catch_c6
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_5d} :catch_63

    #@5d
    .line 911
    .end local v8           #linkProperties:Landroid/net/LinkProperties;
    :cond_5d
    :goto_5d
    :try_start_5d
    const-string v14, "eos"

    #@5f
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_62
    .catchall {:try_start_5d .. :try_end_62} :catchall_e5
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_62} :catch_63

    #@62
    goto :goto_1c

    #@63
    .line 914
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v13           #writeToFile:Z
    :catch_63
    move-exception v2

    #@64
    move-object v9, v10

    #@65
    .line 915
    .end local v10           #out:Ljava/io/DataOutputStream;
    .local v2, e:Ljava/io/IOException;
    .restart local v9       #out:Ljava/io/DataOutputStream;
    :goto_65
    :try_start_65
    const-string v14, "Error writing data file"

    #@67
    invoke-static {v14}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->loge(Ljava/lang/String;)V
    :try_end_6a
    .catchall {:try_start_65 .. :try_end_6a} :catchall_216

    #@6a
    .line 917
    if-eqz v9, :cond_6f

    #@6c
    .line 919
    :try_start_6c
    invoke-virtual {v9}, Ljava/io/DataOutputStream;->close()V
    :try_end_6f
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_6f} :catch_210

    #@6f
    .line 924
    :cond_6f
    :goto_6f
    const-class v15, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;

    #@71
    monitor-enter v15

    #@72
    .line 925
    :try_start_72
    sget v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@74
    add-int/lit8 v14, v14, -0x1

    #@76
    sput v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@78
    if-nez v14, :cond_89

    #@7a
    .line 926
    sget-object v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@7c
    invoke-virtual {v14}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@7f
    move-result-object v14

    #@80
    invoke-virtual {v14}, Landroid/os/Looper;->quit()V

    #@83
    .line 927
    const/4 v14, 0x0

    #@84
    sput-object v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@86
    .line 928
    const/4 v14, 0x0

    #@87
    sput-object v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandlerThread:Landroid/os/HandlerThread;

    #@89
    .line 930
    :cond_89
    monitor-exit v15
    :try_end_8a
    .catchall {:try_start_72 .. :try_end_8a} :catchall_208

    #@8a
    .line 932
    .end local v2           #e:Ljava/io/IOException;
    :goto_8a
    return-void

    #@8b
    .line 835
    .end local v9           #out:Ljava/io/DataOutputStream;
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    .restart local v8       #linkProperties:Landroid/net/LinkProperties;
    .restart local v10       #out:Ljava/io/DataOutputStream;
    .restart local v13       #writeToFile:Z
    :pswitch_8b
    :try_start_8b
    const-string v14, "ipAssignment"

    #@8d
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@90
    .line 836
    iget-object v14, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@92
    invoke-virtual {v14}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->toString()Ljava/lang/String;

    #@95
    move-result-object v14

    #@96
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@99
    .line 837
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@9c
    move-result-object v14

    #@9d
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a0
    move-result-object v5

    #@a1
    .local v5, i$:Ljava/util/Iterator;
    :goto_a1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@a4
    move-result v14

    #@a5
    if-eqz v14, :cond_10a

    #@a7
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@aa
    move-result-object v7

    #@ab
    check-cast v7, Landroid/net/LinkAddress;

    #@ad
    .line 838
    .local v7, linkAddr:Landroid/net/LinkAddress;
    const-string/jumbo v14, "linkAddress"

    #@b0
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@b3
    .line 839
    invoke-virtual {v7}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@b6
    move-result-object v14

    #@b7
    invoke-virtual {v14}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@ba
    move-result-object v14

    #@bb
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@be
    .line 840
    invoke-virtual {v7}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@c1
    move-result v14

    #@c2
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_c5
    .catchall {:try_start_8b .. :try_end_c5} :catchall_e5
    .catch Ljava/lang/NullPointerException; {:try_start_8b .. :try_end_c5} :catch_c6
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_c5} :catch_63

    #@c5
    goto :goto_a1

    #@c6
    .line 908
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v7           #linkAddr:Landroid/net/LinkAddress;
    .end local v8           #linkProperties:Landroid/net/LinkProperties;
    :catch_c6
    move-exception v2

    #@c7
    .line 909
    .local v2, e:Ljava/lang/NullPointerException;
    :try_start_c7
    new-instance v14, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v15, "Failure in writing "

    #@ce
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v14

    #@d2
    iget-object v15, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@d4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v14

    #@d8
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v14

    #@dc
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v14

    #@e0
    invoke-static {v14}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->loge(Ljava/lang/String;)V
    :try_end_e3
    .catchall {:try_start_c7 .. :try_end_e3} :catchall_e5
    .catch Ljava/io/IOException; {:try_start_c7 .. :try_end_e3} :catch_63

    #@e3
    goto/16 :goto_5d

    #@e5
    .line 917
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v2           #e:Ljava/lang/NullPointerException;
    .end local v13           #writeToFile:Z
    :catchall_e5
    move-exception v14

    #@e6
    move-object v9, v10

    #@e7
    .end local v10           #out:Ljava/io/DataOutputStream;
    .restart local v9       #out:Ljava/io/DataOutputStream;
    :goto_e7
    if-eqz v9, :cond_ec

    #@e9
    .line 919
    :try_start_e9
    invoke-virtual {v9}, Ljava/io/DataOutputStream;->close()V
    :try_end_ec
    .catch Ljava/lang/Exception; {:try_start_e9 .. :try_end_ec} :catch_213

    #@ec
    .line 924
    :cond_ec
    :goto_ec
    const-class v15, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;

    #@ee
    monitor-enter v15

    #@ef
    .line 925
    :try_start_ef
    sget v16, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@f1
    add-int/lit8 v16, v16, -0x1

    #@f3
    sput v16, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@f5
    if-nez v16, :cond_108

    #@f7
    .line 926
    sget-object v16, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@f9
    invoke-virtual/range {v16 .. v16}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@fc
    move-result-object v16

    #@fd
    invoke-virtual/range {v16 .. v16}, Landroid/os/Looper;->quit()V

    #@100
    .line 927
    const/16 v16, 0x0

    #@102
    sput-object v16, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@104
    .line 928
    const/16 v16, 0x0

    #@106
    sput-object v16, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandlerThread:Landroid/os/HandlerThread;

    #@108
    .line 930
    :cond_108
    monitor-exit v15
    :try_end_109
    .catchall {:try_start_ef .. :try_end_109} :catchall_20b

    #@109
    throw v14

    #@10a
    .line 842
    .end local v9           #out:Ljava/io/DataOutputStream;
    .restart local v0       #config:Landroid/net/wifi/WifiConfiguration;
    .restart local v5       #i$:Ljava/util/Iterator;
    .restart local v8       #linkProperties:Landroid/net/LinkProperties;
    .restart local v10       #out:Ljava/io/DataOutputStream;
    .restart local v13       #writeToFile:Z
    :cond_10a
    :try_start_10a
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@10d
    move-result-object v14

    #@10e
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@111
    move-result-object v5

    #@112
    :goto_112
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@115
    move-result v14

    #@116
    if-eqz v14, :cond_15f

    #@118
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11b
    move-result-object v12

    #@11c
    check-cast v12, Landroid/net/RouteInfo;

    #@11e
    .line 843
    .local v12, route:Landroid/net/RouteInfo;
    const-string v14, "gateway"

    #@120
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@123
    .line 844
    invoke-virtual {v12}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@126
    move-result-object v1

    #@127
    .line 845
    .local v1, dest:Landroid/net/LinkAddress;
    if-eqz v1, :cond_155

    #@129
    .line 846
    const/4 v14, 0x1

    #@12a
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@12d
    .line 847
    invoke-virtual {v1}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@130
    move-result-object v14

    #@131
    invoke-virtual {v14}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@134
    move-result-object v14

    #@135
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@138
    .line 848
    invoke-virtual {v1}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@13b
    move-result v14

    #@13c
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@13f
    .line 852
    :goto_13f
    invoke-virtual {v12}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@142
    move-result-object v14

    #@143
    if-eqz v14, :cond_15a

    #@145
    .line 853
    const/4 v14, 0x1

    #@146
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@149
    .line 854
    invoke-virtual {v12}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@14c
    move-result-object v14

    #@14d
    invoke-virtual {v14}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@150
    move-result-object v14

    #@151
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@154
    goto :goto_112

    #@155
    .line 850
    :cond_155
    const/4 v14, 0x0

    #@156
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@159
    goto :goto_13f

    #@15a
    .line 856
    :cond_15a
    const/4 v14, 0x0

    #@15b
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@15e
    goto :goto_112

    #@15f
    .line 859
    .end local v1           #dest:Landroid/net/LinkAddress;
    .end local v12           #route:Landroid/net/RouteInfo;
    :cond_15f
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@162
    move-result-object v14

    #@163
    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@166
    move-result-object v5

    #@167
    :goto_167
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@16a
    move-result v14

    #@16b
    if-eqz v14, :cond_180

    #@16d
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@170
    move-result-object v6

    #@171
    check-cast v6, Ljava/net/InetAddress;

    #@173
    .line 860
    .local v6, inetAddr:Ljava/net/InetAddress;
    const-string v14, "dns"

    #@175
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@178
    .line 861
    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@17b
    move-result-object v14

    #@17c
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@17f
    goto :goto_167

    #@180
    .line 863
    .end local v6           #inetAddr:Ljava/net/InetAddress;
    :cond_180
    const/4 v13, 0x1

    #@181
    .line 864
    goto/16 :goto_3d

    #@183
    .line 866
    .end local v5           #i$:Ljava/util/Iterator;
    :pswitch_183
    const-string v14, "ipAssignment"

    #@185
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@188
    .line 867
    iget-object v14, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@18a
    invoke-virtual {v14}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->toString()Ljava/lang/String;

    #@18d
    move-result-object v14

    #@18e
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@191
    .line 868
    const/4 v13, 0x1

    #@192
    .line 869
    goto/16 :goto_3d

    #@194
    .line 880
    :pswitch_194
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@197
    move-result-object v11

    #@198
    .line 881
    .local v11, proxyProperties:Landroid/net/ProxyProperties;
    invoke-virtual {v11}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@19b
    move-result-object v3

    #@19c
    .line 882
    .local v3, exclusionList:Ljava/lang/String;
    const-string/jumbo v14, "proxySettings"

    #@19f
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1a2
    .line 883
    iget-object v14, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1a4
    invoke-virtual {v14}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v14

    #@1a8
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1ab
    .line 884
    const-string/jumbo v14, "proxyHost"

    #@1ae
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1b1
    .line 885
    invoke-virtual {v11}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@1b4
    move-result-object v14

    #@1b5
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1b8
    .line 886
    const-string/jumbo v14, "proxyPort"

    #@1bb
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1be
    .line 887
    invoke-virtual {v11}, Landroid/net/ProxyProperties;->getPort()I

    #@1c1
    move-result v14

    #@1c2
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@1c5
    .line 888
    const-string v14, "exclusionList"

    #@1c7
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1ca
    .line 889
    invoke-virtual {v10, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1cd
    .line 890
    const/4 v13, 0x1

    #@1ce
    .line 891
    goto/16 :goto_4f

    #@1d0
    .line 893
    .end local v3           #exclusionList:Ljava/lang/String;
    .end local v11           #proxyProperties:Landroid/net/ProxyProperties;
    :pswitch_1d0
    const-string/jumbo v14, "proxySettings"

    #@1d3
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@1d6
    .line 894
    iget-object v14, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@1d8
    invoke-virtual {v14}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->toString()Ljava/lang/String;

    #@1db
    move-result-object v14

    #@1dc
    invoke-virtual {v10, v14}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V
    :try_end_1df
    .catchall {:try_start_10a .. :try_end_1df} :catchall_e5
    .catch Ljava/lang/NullPointerException; {:try_start_10a .. :try_end_1df} :catch_c6
    .catch Ljava/io/IOException; {:try_start_10a .. :try_end_1df} :catch_63

    #@1df
    .line 895
    const/4 v13, 0x1

    #@1e0
    .line 896
    goto/16 :goto_4f

    #@1e2
    .line 917
    .end local v0           #config:Landroid/net/wifi/WifiConfiguration;
    .end local v8           #linkProperties:Landroid/net/LinkProperties;
    .end local v13           #writeToFile:Z
    :cond_1e2
    if-eqz v10, :cond_1e7

    #@1e4
    .line 919
    :try_start_1e4
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V
    :try_end_1e7
    .catch Ljava/lang/Exception; {:try_start_1e4 .. :try_end_1e7} :catch_20e

    #@1e7
    .line 924
    :cond_1e7
    :goto_1e7
    const-class v15, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;

    #@1e9
    monitor-enter v15

    #@1ea
    .line 925
    :try_start_1ea
    sget v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@1ec
    add-int/lit8 v14, v14, -0x1

    #@1ee
    sput v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@1f0
    if-nez v14, :cond_201

    #@1f2
    .line 926
    sget-object v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@1f4
    invoke-virtual {v14}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@1f7
    move-result-object v14

    #@1f8
    invoke-virtual {v14}, Landroid/os/Looper;->quit()V

    #@1fb
    .line 927
    const/4 v14, 0x0

    #@1fc
    sput-object v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@1fe
    .line 928
    const/4 v14, 0x0

    #@1ff
    sput-object v14, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandlerThread:Landroid/os/HandlerThread;

    #@201
    .line 930
    :cond_201
    monitor-exit v15

    #@202
    move-object v9, v10

    #@203
    .line 931
    .end local v10           #out:Ljava/io/DataOutputStream;
    .restart local v9       #out:Ljava/io/DataOutputStream;
    goto/16 :goto_8a

    #@205
    .line 930
    .end local v9           #out:Ljava/io/DataOutputStream;
    .restart local v10       #out:Ljava/io/DataOutputStream;
    :catchall_205
    move-exception v14

    #@206
    monitor-exit v15
    :try_end_207
    .catchall {:try_start_1ea .. :try_end_207} :catchall_205

    #@207
    throw v14

    #@208
    .end local v10           #out:Ljava/io/DataOutputStream;
    .local v2, e:Ljava/io/IOException;
    .restart local v9       #out:Ljava/io/DataOutputStream;
    :catchall_208
    move-exception v14

    #@209
    :try_start_209
    monitor-exit v15
    :try_end_20a
    .catchall {:try_start_209 .. :try_end_20a} :catchall_208

    #@20a
    throw v14

    #@20b
    .end local v2           #e:Ljava/io/IOException;
    :catchall_20b
    move-exception v14

    #@20c
    :try_start_20c
    monitor-exit v15
    :try_end_20d
    .catchall {:try_start_20c .. :try_end_20d} :catchall_20b

    #@20d
    throw v14

    #@20e
    .line 920
    .end local v9           #out:Ljava/io/DataOutputStream;
    .restart local v10       #out:Ljava/io/DataOutputStream;
    :catch_20e
    move-exception v14

    #@20f
    goto :goto_1e7

    #@210
    .end local v10           #out:Ljava/io/DataOutputStream;
    .restart local v2       #e:Ljava/io/IOException;
    .restart local v9       #out:Ljava/io/DataOutputStream;
    :catch_210
    move-exception v14

    #@211
    goto/16 :goto_6f

    #@213
    .end local v2           #e:Ljava/io/IOException;
    :catch_213
    move-exception v15

    #@214
    goto/16 :goto_ec

    #@216
    .line 917
    :catchall_216
    move-exception v14

    #@217
    goto/16 :goto_e7

    #@219
    .line 914
    :catch_219
    move-exception v2

    #@21a
    goto/16 :goto_65

    #@21c
    .line 833
    :pswitch_data_21c
    .packed-switch 0x1
        :pswitch_8b
        :pswitch_183
        :pswitch_3d
    .end packed-switch

    #@226
    .line 878
    :pswitch_data_226
    .packed-switch 0x1
        :pswitch_194
        :pswitch_1d0
        :pswitch_4f
    .end packed-switch
.end method

.method static write(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 803
    .local p0, networks:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    const-class v1, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;

    #@2
    monitor-enter v1

    #@3
    .line 804
    :try_start_3
    sget v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@5
    add-int/lit8 v0, v0, 0x1

    #@7
    sput v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sWriteSequence:I

    #@9
    const/4 v2, 0x1

    #@a
    if-ne v0, v2, :cond_27

    #@c
    .line 805
    new-instance v0, Landroid/os/HandlerThread;

    #@e
    const-string v2, "WifiConfigThread"

    #@10
    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@13
    sput-object v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandlerThread:Landroid/os/HandlerThread;

    #@15
    .line 806
    sget-object v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandlerThread:Landroid/os/HandlerThread;

    #@17
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@1a
    .line 807
    new-instance v0, Landroid/os/Handler;

    #@1c
    sget-object v2, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandlerThread:Landroid/os/HandlerThread;

    #@1e
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@21
    move-result-object v2

    #@22
    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@25
    sput-object v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@27
    .line 809
    :cond_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_33

    #@28
    .line 811
    sget-object v0, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite;->sDiskWriteHandler:Landroid/os/Handler;

    #@2a
    new-instance v1, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite$1;

    #@2c
    invoke-direct {v1, p0}, Landroid/net/wifi/WifiConfigStore$DelayedDiskWrite$1;-><init>(Ljava/util/List;)V

    #@2f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@32
    .line 817
    return-void

    #@33
    .line 809
    :catchall_33
    move-exception v0

    #@34
    :try_start_34
    monitor-exit v1
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_33

    #@35
    throw v0
.end method
