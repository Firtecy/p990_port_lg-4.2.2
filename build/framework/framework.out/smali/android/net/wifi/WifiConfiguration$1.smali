.class final Landroid/net/wifi/WifiConfiguration$1;
.super Ljava/lang/Object;
.source "WifiConfiguration.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/wifi/WifiConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 681
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/wifi/WifiConfiguration;
    .registers 10
    .parameter "in"

    #@0
    .prologue
    .line 683
    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    #@2
    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    #@5
    .line 684
    .local v1, config:Landroid/net/wifi/WifiConfiguration;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v6

    #@9
    iput v6, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@b
    .line 685
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v6

    #@f
    iput v6, v1, Landroid/net/wifi/WifiConfiguration;->status:I

    #@11
    .line 686
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@14
    move-result v6

    #@15
    iput v6, v1, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    #@17
    .line 687
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@1d
    .line 688
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v6

    #@21
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    #@23
    .line 689
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    #@29
    .line 690
    const/4 v3, 0x0

    #@2a
    .local v3, i:I
    :goto_2a
    iget-object v6, v1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@2c
    array-length v6, v6

    #@2d
    if-ge v3, v6, :cond_3a

    #@2f
    .line 691
    iget-object v6, v1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    #@31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    aput-object v7, v6, v3

    #@37
    .line 690
    add-int/lit8 v3, v3, 0x1

    #@39
    goto :goto_2a

    #@3a
    .line 692
    :cond_3a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v6

    #@3e
    iput v6, v1, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    #@40
    .line 693
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v6

    #@44
    iput v6, v1, Landroid/net/wifi/WifiConfiguration;->priority:I

    #@46
    .line 694
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v6

    #@4a
    if-eqz v6, :cond_7f

    #@4c
    const/4 v6, 0x1

    #@4d
    :goto_4d
    iput-boolean v6, v1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    #@4f
    .line 695
    invoke-static {p1}, Landroid/net/wifi/WifiConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@52
    move-result-object v6

    #@53
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@55
    .line 696
    invoke-static {p1}, Landroid/net/wifi/WifiConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@58
    move-result-object v6

    #@59
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@5b
    .line 697
    invoke-static {p1}, Landroid/net/wifi/WifiConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@5e
    move-result-object v6

    #@5f
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@61
    .line 698
    invoke-static {p1}, Landroid/net/wifi/WifiConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@64
    move-result-object v6

    #@65
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@67
    .line 699
    invoke-static {p1}, Landroid/net/wifi/WifiConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@6a
    move-result-object v6

    #@6b
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@6d
    .line 701
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    #@6f
    .local v0, arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    array-length v5, v0

    #@70
    .local v5, len$:I
    const/4 v4, 0x0

    #@71
    .local v4, i$:I
    :goto_71
    if-ge v4, v5, :cond_81

    #@73
    aget-object v2, v0, v4

    #@75
    .line 702
    .local v2, field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v2, v6}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@7c
    .line 701
    add-int/lit8 v4, v4, 0x1

    #@7e
    goto :goto_71

    #@7f
    .line 694
    .end local v0           #arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v2           #field:Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_7f
    const/4 v6, 0x0

    #@80
    goto :goto_4d

    #@81
    .line 705
    .restart local v0       #arr$:[Landroid/net/wifi/WifiConfiguration$EnterpriseField;
    .restart local v4       #i$:I
    .restart local v5       #len$:I
    :cond_81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@84
    move-result-object v6

    #@85
    invoke-static {v6}, Landroid/net/wifi/WifiConfiguration$IpAssignment;->valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@88
    move-result-object v6

    #@89
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    #@8b
    .line 706
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8e
    move-result-object v6

    #@8f
    invoke-static {v6}, Landroid/net/wifi/WifiConfiguration$ProxySettings;->valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@92
    move-result-object v6

    #@93
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    #@95
    .line 707
    const/4 v6, 0x0

    #@96
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@99
    move-result-object v6

    #@9a
    check-cast v6, Landroid/net/LinkProperties;

    #@9c
    iput-object v6, v1, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@9e
    .line 708
    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 681
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/wifi/WifiConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/wifi/WifiConfiguration;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 712
    new-array v0, p1, [Landroid/net/wifi/WifiConfiguration;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 681
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiConfiguration$1;->newArray(I)[Landroid/net/wifi/WifiConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
