.class Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WaitForP2pDisableState"
.end annotation


# instance fields
.field private mTransitionToState:Lcom/android/internal/util/State;

.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3345
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 3350
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 3351
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$11300(Landroid/net/wifi/WifiStateMachine;)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    iget v0, v0, Landroid/os/Message;->what:I

    #@12
    sparse-switch v0, :sswitch_data_46

    #@15
    .line 3362
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@17
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$10900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->mTransitionToState:Lcom/android/internal/util/State;

    #@1d
    .line 3365
    :goto_1d
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1f
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@22
    move-result-object v0

    #@23
    const v1, 0x20084

    #@26
    invoke-virtual {v0, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@29
    .line 3366
    return-void

    #@2a
    .line 3353
    :sswitch_2a
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2c
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$2000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@2f
    move-result-object v0

    #@30
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->mTransitionToState:Lcom/android/internal/util/State;

    #@32
    goto :goto_1d

    #@33
    .line 3356
    :sswitch_33
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@35
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$10900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->mTransitionToState:Lcom/android/internal/util/State;

    #@3b
    goto :goto_1d

    #@3c
    .line 3359
    :sswitch_3c
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3e
    invoke-static {v0}, Landroid/net/wifi/WifiStateMachine;->access$6900(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->mTransitionToState:Lcom/android/internal/util/State;

    #@44
    goto :goto_1d

    #@45
    .line 3351
    nop

    #@46
    :sswitch_data_46
    .sparse-switch
        0x2000c -> :sswitch_3c
        0x20012 -> :sswitch_33
        0x24002 -> :sswitch_2a
    .end sparse-switch
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 3370
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_16

    #@5
    .line 3397
    const/4 v0, 0x0

    #@6
    .line 3399
    :goto_6
    return v0

    #@7
    .line 3372
    :sswitch_7
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@9
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->mTransitionToState:Lcom/android/internal/util/State;

    #@b
    invoke-static {v0, v1}, Landroid/net/wifi/WifiStateMachine;->access$11400(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@e
    .line 3399
    :goto_e
    const/4 v0, 0x1

    #@f
    goto :goto_6

    #@10
    .line 3394
    :sswitch_10
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$WaitForP2pDisableState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@12
    invoke-static {v0, p1}, Landroid/net/wifi/WifiStateMachine;->access$11500(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@15
    goto :goto_e

    #@16
    .line 3370
    :sswitch_data_16
    .sparse-switch
        0x20001 -> :sswitch_10
        0x20002 -> :sswitch_10
        0x2000b -> :sswitch_10
        0x2000c -> :sswitch_10
        0x2000d -> :sswitch_10
        0x2000e -> :sswitch_10
        0x20015 -> :sswitch_10
        0x20018 -> :sswitch_10
        0x20047 -> :sswitch_10
        0x20048 -> :sswitch_10
        0x20049 -> :sswitch_10
        0x2004a -> :sswitch_10
        0x2004b -> :sswitch_10
        0x2004c -> :sswitch_10
        0x20050 -> :sswitch_10
        0x20054 -> :sswitch_10
        0x20055 -> :sswitch_10
        0x2005a -> :sswitch_10
        0x20085 -> :sswitch_7
        0x24006 -> :sswitch_10
    .end sparse-switch
.end method
