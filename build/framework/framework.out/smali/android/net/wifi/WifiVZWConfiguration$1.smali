.class final Landroid/net/wifi/WifiVZWConfiguration$1;
.super Ljava/lang/Object;
.source "WifiVZWConfiguration.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiVZWConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/wifi/WifiVZWConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 797
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/wifi/WifiVZWConfiguration;
    .registers 8
    .parameter "in"

    #@0
    .prologue
    .line 800
    new-instance v1, Landroid/net/wifi/WifiVZWConfiguration;

    #@2
    invoke-direct {v1}, Landroid/net/wifi/WifiVZWConfiguration;-><init>()V

    #@5
    .line 801
    .local v1, config:Landroid/net/wifi/WifiVZWConfiguration;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v5

    #@9
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->networkId:I

    #@b
    .line 802
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v5

    #@f
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->status:I

    #@11
    .line 803
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@14
    move-result v5

    #@15
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->disableReason:I

    #@17
    .line 804
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->SSID:Ljava/lang/String;

    #@1d
    .line 805
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->BSSID:Ljava/lang/String;

    #@23
    .line 806
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->preSharedKey:Ljava/lang/String;

    #@29
    .line 810
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys1:Ljava/lang/String;

    #@2f
    .line 811
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys2:Ljava/lang/String;

    #@35
    .line 812
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys3:Ljava/lang/String;

    #@3b
    .line 813
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->wepKeys4:Ljava/lang/String;

    #@41
    .line 814
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v5

    #@45
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->wepTxKeyIndex:I

    #@47
    .line 815
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4a
    move-result v5

    #@4b
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->priority:I

    #@4d
    .line 816
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v5

    #@51
    if-eqz v5, :cond_86

    #@53
    const/4 v5, 0x1

    #@54
    :goto_54
    iput-boolean v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->hiddenSSID:Z

    #@56
    .line 817
    invoke-static {p1}, Landroid/net/wifi/WifiVZWConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@59
    move-result-object v5

    #@5a
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    #@5c
    .line 818
    invoke-static {p1}, Landroid/net/wifi/WifiVZWConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@5f
    move-result-object v5

    #@60
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->allowedProtocols:Ljava/util/BitSet;

    #@62
    .line 819
    invoke-static {p1}, Landroid/net/wifi/WifiVZWConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@65
    move-result-object v5

    #@66
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    #@68
    .line 820
    invoke-static {p1}, Landroid/net/wifi/WifiVZWConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@6b
    move-result-object v5

    #@6c
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    #@6e
    .line 821
    invoke-static {p1}, Landroid/net/wifi/WifiVZWConfiguration;->access$100(Landroid/os/Parcel;)Ljava/util/BitSet;

    #@71
    move-result-object v5

    #@72
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    #@74
    .line 823
    iget-object v0, v1, Landroid/net/wifi/WifiVZWConfiguration;->enterpriseFields:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;

    #@76
    .local v0, arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    array-length v4, v0

    #@77
    .local v4, len$:I
    const/4 v3, 0x0

    #@78
    .local v3, i$:I
    :goto_78
    if-ge v3, v4, :cond_88

    #@7a
    aget-object v2, v0, v3

    #@7c
    .line 824
    .local v2, field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v2, v5}, Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    #@83
    .line 823
    add-int/lit8 v3, v3, 0x1

    #@85
    goto :goto_78

    #@86
    .line 816
    .end local v0           #arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .end local v2           #field:Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_86
    const/4 v5, 0x0

    #@87
    goto :goto_54

    #@88
    .line 827
    .restart local v0       #arr$:[Landroid/net/wifi/WifiVZWConfiguration$EnterpriseField;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_88
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-static {v5}, Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;->valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@8f
    move-result-object v5

    #@90
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->ipAssignment:Landroid/net/wifi/WifiVZWConfiguration$IpAssignment;

    #@92
    .line 828
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@95
    move-result-object v5

    #@96
    invoke-static {v5}, Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;->valueOf(Ljava/lang/String;)Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@99
    move-result-object v5

    #@9a
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->proxySettings:Landroid/net/wifi/WifiVZWConfiguration$ProxySettings;

    #@9c
    .line 829
    const/4 v5, 0x0

    #@9d
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@a0
    move-result-object v5

    #@a1
    check-cast v5, Landroid/net/LinkProperties;

    #@a3
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->linkProperties:Landroid/net/LinkProperties;

    #@a5
    .line 830
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v5

    #@a9
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->Channel:I

    #@ab
    .line 831
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ae
    move-result-object v5

    #@af
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->CountryCode:Ljava/lang/String;

    #@b1
    .line 832
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b4
    move-result v5

    #@b5
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->Maxscb:I

    #@b7
    .line 833
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ba
    move-result-object v5

    #@bb
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->hw_mode:Ljava/lang/String;

    #@bd
    .line 834
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c0
    move-result v5

    #@c1
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->ap_isolate:I

    #@c3
    .line 835
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c6
    move-result v5

    #@c7
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->ieee_mode:I

    #@c9
    .line 836
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@cc
    move-result v5

    #@cd
    iput v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->macaddr_acl:I

    #@cf
    .line 837
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d2
    move-result-object v5

    #@d3
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->accept_mac_file:Ljava/lang/String;

    #@d5
    .line 838
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d8
    move-result-object v5

    #@d9
    iput-object v5, v1, Landroid/net/wifi/WifiVZWConfiguration;->deny_mac_file:Ljava/lang/String;

    #@db
    .line 839
    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 797
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiVZWConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/wifi/WifiVZWConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/wifi/WifiVZWConfiguration;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 844
    new-array v0, p1, [Landroid/net/wifi/WifiVZWConfiguration;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 797
    invoke-virtual {p0, p1}, Landroid/net/wifi/WifiVZWConfiguration$1;->newArray(I)[Landroid/net/wifi/WifiVZWConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
