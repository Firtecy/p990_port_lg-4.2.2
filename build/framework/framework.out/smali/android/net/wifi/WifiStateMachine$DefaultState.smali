.class Landroid/net/wifi/WifiStateMachine$DefaultState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2224
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public processMessage(Landroid/os/Message;)Z
    .registers 9
    .parameter "message"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v6, 0x4

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v5, 0x2

    #@4
    const/4 v2, 0x1

    #@5
    .line 2228
    iget v4, p1, Landroid/os/Message;->what:I

    #@7
    sparse-switch v4, :sswitch_data_14e

    #@a
    .line 2380
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Error! unhandled message"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@22
    .line 2383
    :goto_22
    :sswitch_22
    return v2

    #@23
    .line 2230
    :sswitch_23
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@25
    if-nez v1, :cond_34

    #@27
    .line 2231
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@29
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$700(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/AsyncChannel;

    #@2c
    move-result-object v1

    #@2d
    const v3, 0x11001

    #@30
    invoke-virtual {v1, v3}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@33
    goto :goto_22

    #@34
    .line 2233
    :cond_34
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "WifiP2pService connection failure, error="

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@4e
    goto :goto_22

    #@4f
    .line 2237
    :sswitch_4f
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@51
    new-instance v3, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v4, "WifiP2pService channel lost, message.arg1 ="

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@69
    goto :goto_22

    #@6a
    .line 2242
    :sswitch_6a
    iget-object v4, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@6c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@6e
    if-eqz v1, :cond_75

    #@70
    move v1, v2

    #@71
    :goto_71
    invoke-static {v4, v1}, Landroid/net/wifi/WifiStateMachine;->access$802(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@74
    goto :goto_22

    #@75
    :cond_75
    move v1, v3

    #@76
    goto :goto_71

    #@77
    .line 2251
    :sswitch_77
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@79
    iget v3, p1, Landroid/os/Message;->what:I

    #@7b
    const/4 v4, -0x1

    #@7c
    invoke-static {v1, p1, v3, v4}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@7f
    goto :goto_22

    #@80
    .line 2254
    :sswitch_80
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@82
    iget v4, p1, Landroid/os/Message;->what:I

    #@84
    check-cast v1, Ljava/util/List;

    #@86
    invoke-static {v3, p1, v4, v1}, Landroid/net/wifi/WifiStateMachine;->access$1000(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;ILjava/lang/Object;)V

    #@89
    goto :goto_22

    #@8a
    .line 2257
    :sswitch_8a
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@8c
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@8e
    if-ne v4, v2, :cond_91

    #@90
    move v3, v2

    #@91
    :cond_91
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$1102(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@94
    goto :goto_22

    #@95
    .line 2260
    :sswitch_95
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@97
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@99
    if-ne v4, v2, :cond_9c

    #@9b
    move v3, v2

    #@9c
    :cond_9c
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$1202(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@9f
    goto :goto_22

    #@a0
    .line 2263
    :sswitch_a0
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@a2
    if-ne v1, v2, :cond_ab

    #@a4
    .line 2264
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@a6
    invoke-static {v1, v5, v3}, Landroid/net/wifi/WifiStateMachine;->access$1300(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@a9
    goto/16 :goto_22

    #@ab
    .line 2266
    :cond_ab
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ad
    invoke-static {v1, v5, v2}, Landroid/net/wifi/WifiStateMachine;->access$1300(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@b0
    goto/16 :goto_22

    #@b2
    .line 2329
    :sswitch_b2
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@b4
    invoke-static {v3, v1}, Landroid/net/wifi/WifiStateMachine;->access$1402(Landroid/net/wifi/WifiStateMachine;Landroid/net/DhcpStateMachine;)Landroid/net/DhcpStateMachine;

    #@b7
    goto/16 :goto_22

    #@b9
    .line 2332
    :sswitch_b9
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@bb
    if-ne v1, v2, :cond_cd

    #@bd
    .line 2333
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@bf
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$1500(Landroid/net/wifi/WifiStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@c2
    move-result-object v1

    #@c3
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@c6
    .line 2334
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c8
    invoke-static {v1, v6, v2}, Landroid/net/wifi/WifiStateMachine;->access$1300(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@cb
    goto/16 :goto_22

    #@cd
    .line 2336
    :cond_cd
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@cf
    invoke-static {v1, v6, v3}, Landroid/net/wifi/WifiStateMachine;->access$1300(Landroid/net/wifi/WifiStateMachine;IZ)V

    #@d2
    goto/16 :goto_22

    #@d4
    .line 2340
    :sswitch_d4
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d6
    invoke-virtual {v1, v3}, Landroid/net/wifi/WifiStateMachine;->setWifiEnabled(Z)V

    #@d9
    .line 2341
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@db
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiStateMachine;->setWifiEnabled(Z)V

    #@de
    goto/16 :goto_22

    #@e0
    .line 2344
    :sswitch_e0
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e2
    const v3, 0x25002

    #@e5
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@e8
    goto/16 :goto_22

    #@ea
    .line 2348
    :sswitch_ea
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@ec
    const v3, 0x25005

    #@ef
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@f2
    goto/16 :goto_22

    #@f4
    .line 2352
    :sswitch_f4
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@f6
    const v3, 0x25008

    #@f9
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@fc
    goto/16 :goto_22

    #@fe
    .line 2356
    :sswitch_fe
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@100
    const v3, 0x2500c

    #@103
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@106
    goto/16 :goto_22

    #@108
    .line 2360
    :sswitch_108
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@10a
    const v3, 0x2500f

    #@10d
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@110
    goto/16 :goto_22

    #@112
    .line 2364
    :sswitch_112
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@114
    const v3, 0x25012

    #@117
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@11a
    goto/16 :goto_22

    #@11c
    .line 2368
    :sswitch_11c
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@11e
    const v3, 0x25016

    #@121
    invoke-static {v1, p1, v3, v5}, Landroid/net/wifi/WifiStateMachine;->access$900(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;II)V

    #@124
    goto/16 :goto_22

    #@126
    .line 2372
    :sswitch_126
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@128
    check-cast v0, Landroid/net/NetworkInfo;

    #@12a
    .line 2373
    .local v0, info:Landroid/net/NetworkInfo;
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@12c
    invoke-static {v1}, Landroid/net/wifi/WifiStateMachine;->access$1600(Landroid/net/wifi/WifiStateMachine;)Ljava/util/concurrent/atomic/AtomicBoolean;

    #@12f
    move-result-object v1

    #@130
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    #@133
    move-result v3

    #@134
    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@137
    goto/16 :goto_22

    #@139
    .line 2376
    .end local v0           #info:Landroid/net/NetworkInfo;
    :sswitch_139
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13b
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@13d
    if-ne v4, v2, :cond_140

    #@13f
    move v3, v2

    #@140
    :cond_140
    invoke-static {v1, v3}, Landroid/net/wifi/WifiStateMachine;->access$1702(Landroid/net/wifi/WifiStateMachine;Z)Z

    #@143
    .line 2377
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$DefaultState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@145
    const v3, 0x2300d

    #@148
    invoke-static {v1, p1, v3}, Landroid/net/wifi/WifiStateMachine;->access$1800(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;I)V

    #@14b
    goto/16 :goto_22

    #@14d
    .line 2228
    nop

    #@14e
    :sswitch_data_14e
    .sparse-switch
        0x11000 -> :sswitch_23
        0x11004 -> :sswitch_4f
        0x20001 -> :sswitch_22
        0x20002 -> :sswitch_22
        0x2000b -> :sswitch_22
        0x2000c -> :sswitch_22
        0x2000d -> :sswitch_22
        0x2000e -> :sswitch_22
        0x20011 -> :sswitch_22
        0x20012 -> :sswitch_22
        0x20013 -> :sswitch_22
        0x20014 -> :sswitch_22
        0x20015 -> :sswitch_22
        0x20016 -> :sswitch_22
        0x20017 -> :sswitch_22
        0x20018 -> :sswitch_22
        0x20019 -> :sswitch_22
        0x2001a -> :sswitch_22
        0x2001b -> :sswitch_22
        0x2001c -> :sswitch_22
        0x2001d -> :sswitch_22
        0x2001e -> :sswitch_22
        0x2001f -> :sswitch_6a
        0x20033 -> :sswitch_77
        0x20034 -> :sswitch_77
        0x20035 -> :sswitch_77
        0x20036 -> :sswitch_77
        0x20037 -> :sswitch_22
        0x20038 -> :sswitch_22
        0x20039 -> :sswitch_22
        0x2003a -> :sswitch_77
        0x2003b -> :sswitch_80
        0x20047 -> :sswitch_22
        0x20048 -> :sswitch_22
        0x20049 -> :sswitch_22
        0x2004a -> :sswitch_22
        0x2004b -> :sswitch_22
        0x2004c -> :sswitch_22
        0x2004d -> :sswitch_a0
        0x20050 -> :sswitch_22
        0x20052 -> :sswitch_8a
        0x20053 -> :sswitch_22
        0x20056 -> :sswitch_b9
        0x20058 -> :sswitch_22
        0x2005a -> :sswitch_22
        0x2005b -> :sswitch_95
        0x20086 -> :sswitch_22
        0x20087 -> :sswitch_22
        0x20088 -> :sswitch_22
        0x20089 -> :sswitch_22
        0x2008a -> :sswitch_22
        0x2008b -> :sswitch_22
        0x2008c -> :sswitch_22
        0x2008d -> :sswitch_22
        0x21015 -> :sswitch_22
        0x21016 -> :sswitch_22
        0x2300b -> :sswitch_126
        0x2300c -> :sswitch_139
        0x24001 -> :sswitch_22
        0x24002 -> :sswitch_22
        0x24003 -> :sswitch_22
        0x24004 -> :sswitch_22
        0x24005 -> :sswitch_22
        0x24006 -> :sswitch_22
        0x24007 -> :sswitch_22
        0x2400a -> :sswitch_22
        0x2400c -> :sswitch_d4
        0x25001 -> :sswitch_e0
        0x25004 -> :sswitch_ea
        0x25007 -> :sswitch_f4
        0x2500a -> :sswitch_fe
        0x2500e -> :sswitch_108
        0x25011 -> :sswitch_112
        0x25014 -> :sswitch_11c
        0x30004 -> :sswitch_22
        0x30005 -> :sswitch_22
        0x30006 -> :sswitch_b2
    .end sparse-switch
.end method
