.class Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;
.super Lcom/android/internal/util/State;
.source "WifiStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SoftApStoppingState"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/wifi/WifiStateMachine;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4718
    iput-object p1, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 6

    #@0
    .prologue
    .line 4722
    const v0, 0xc365

    #@3
    invoke-virtual {p0}, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->getName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@a
    .line 4725
    iget-object v0, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@c
    iget-object v1, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@e
    const v2, 0x2001e

    #@11
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@13
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$19304(Landroid/net/wifi/WifiStateMachine;)I

    #@16
    move-result v3

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/WifiStateMachine;->obtainMessage(III)Landroid/os/Message;

    #@1b
    move-result-object v1

    #@1c
    const-wide/16 v2, 0x1388

    #@1e
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/WifiStateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    #@21
    .line 4728
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 6
    .parameter "message"

    #@0
    .prologue
    .line 4732
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v2, :sswitch_data_76

    #@5
    .line 4781
    const/4 v2, 0x0

    #@6
    .line 4783
    :goto_6
    return v2

    #@7
    .line 4734
    :sswitch_7
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v1, Landroid/net/wifi/WifiStateMachine$TetherStateChange;

    #@b
    .line 4737
    .local v1, stateChange:Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@d
    iget-object v3, v1, Landroid/net/wifi/WifiStateMachine$TetherStateChange;->active:Ljava/util/ArrayList;

    #@f
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$19400(Landroid/net/wifi/WifiStateMachine;Ljava/util/ArrayList;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_17

    #@15
    .line 4783
    .end local v1           #stateChange:Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    :cond_15
    :goto_15
    const/4 v2, 0x1

    #@16
    goto :goto_6

    #@17
    .line 4740
    .restart local v1       #stateChange:Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    :cond_17
    :try_start_17
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@19
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@1f
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->stopAccessPoint(Ljava/lang/String;)V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_26} :catch_32

    #@26
    .line 4744
    :goto_26
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@28
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@2a
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$2000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$20200(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@31
    goto :goto_15

    #@32
    .line 4741
    :catch_32
    move-exception v0

    #@33
    .line 4742
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@35
    const-string v3, "Exception in stopAccessPoint()"

    #@37
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@3a
    goto :goto_26

    #@3b
    .line 4747
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #stateChange:Landroid/net/wifi/WifiStateMachine$TetherStateChange;
    :sswitch_3b
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@3d
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@3f
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$19300(Landroid/net/wifi/WifiStateMachine;)I

    #@42
    move-result v3

    #@43
    if-ne v2, v3, :cond_15

    #@45
    .line 4748
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@47
    const-string v3, "Failed to get tether update, force stop access point"

    #@49
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@4c
    .line 4750
    :try_start_4c
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@4e
    invoke-static {v2}, Landroid/net/wifi/WifiStateMachine;->access$400(Landroid/net/wifi/WifiStateMachine;)Landroid/os/INetworkManagementService;

    #@51
    move-result-object v2

    #@52
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@54
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$300(Landroid/net/wifi/WifiStateMachine;)Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->stopAccessPoint(Ljava/lang/String;)V
    :try_end_5b
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_5b} :catch_67

    #@5b
    .line 4754
    :goto_5b
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5d
    iget-object v3, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@5f
    invoke-static {v3}, Landroid/net/wifi/WifiStateMachine;->access$2000(Landroid/net/wifi/WifiStateMachine;)Lcom/android/internal/util/State;

    #@62
    move-result-object v3

    #@63
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$20300(Landroid/net/wifi/WifiStateMachine;Lcom/android/internal/util/IState;)V

    #@66
    goto :goto_15

    #@67
    .line 4751
    :catch_67
    move-exception v0

    #@68
    .line 4752
    .restart local v0       #e:Ljava/lang/Exception;
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@6a
    const-string v3, "Exception in stopAccessPoint()"

    #@6c
    invoke-static {v2, v3}, Landroid/net/wifi/WifiStateMachine;->access$500(Landroid/net/wifi/WifiStateMachine;Ljava/lang/String;)V

    #@6f
    goto :goto_5b

    #@70
    .line 4778
    .end local v0           #e:Ljava/lang/Exception;
    :sswitch_70
    iget-object v2, p0, Landroid/net/wifi/WifiStateMachine$SoftApStoppingState;->this$0:Landroid/net/wifi/WifiStateMachine;

    #@72
    invoke-static {v2, p1}, Landroid/net/wifi/WifiStateMachine;->access$20400(Landroid/net/wifi/WifiStateMachine;Landroid/os/Message;)V

    #@75
    goto :goto_15

    #@76
    .line 4732
    :sswitch_data_76
    .sparse-switch
        0x20001 -> :sswitch_70
        0x20002 -> :sswitch_70
        0x2000b -> :sswitch_70
        0x2000c -> :sswitch_70
        0x2000d -> :sswitch_70
        0x2000e -> :sswitch_70
        0x20015 -> :sswitch_70
        0x20018 -> :sswitch_70
        0x2001d -> :sswitch_7
        0x2001e -> :sswitch_3b
        0x20048 -> :sswitch_70
        0x20049 -> :sswitch_70
        0x20050 -> :sswitch_70
        0x20054 -> :sswitch_70
        0x20055 -> :sswitch_70
        0x2005a -> :sswitch_70
        0x20086 -> :sswitch_70
        0x20089 -> :sswitch_70
    .end sparse-switch
.end method
