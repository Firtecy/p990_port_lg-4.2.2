.class public Landroid/net/NetworkStats;
.super Ljava/lang/Object;
.source "NetworkStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/NetworkStats$NonMonotonicObserver;,
        Landroid/net/NetworkStats$Entry;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/NetworkStats;",
            ">;"
        }
    .end annotation
.end field

.field public static final IFACE_ALL:Ljava/lang/String; = null

.field public static final SET_ALL:I = -0x1

.field public static final SET_DEFAULT:I = 0x0

.field public static final SET_FOREGROUND:I = 0x1

.field public static final TAG_NONE:I = 0x0

.field public static final UID_ALL:I = -0x1


# instance fields
.field private final elapsedRealtime:J

.field private iface:[Ljava/lang/String;

.field private operations:[J

.field private rxBytes:[J

.field private rxPackets:[J

.field private set:[I

.field private size:I

.field private tag:[I

.field private txBytes:[J

.field private txPackets:[J

.field private uid:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 44
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@3
    .line 680
    new-instance v0, Landroid/net/NetworkStats$1;

    #@5
    invoke-direct {v0}, Landroid/net/NetworkStats$1;-><init>()V

    #@8
    sput-object v0, Landroid/net/NetworkStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a
    return-void
.end method

.method public constructor <init>(JI)V
    .registers 5
    .parameter "elapsedRealtime"
    .parameter "initialSize"

    #@0
    .prologue
    .line 140
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 141
    iput-wide p1, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@5
    .line 142
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/net/NetworkStats;->size:I

    #@8
    .line 143
    new-array v0, p3, [Ljava/lang/String;

    #@a
    iput-object v0, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@c
    .line 144
    new-array v0, p3, [I

    #@e
    iput-object v0, p0, Landroid/net/NetworkStats;->uid:[I

    #@10
    .line 145
    new-array v0, p3, [I

    #@12
    iput-object v0, p0, Landroid/net/NetworkStats;->set:[I

    #@14
    .line 146
    new-array v0, p3, [I

    #@16
    iput-object v0, p0, Landroid/net/NetworkStats;->tag:[I

    #@18
    .line 147
    new-array v0, p3, [J

    #@1a
    iput-object v0, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@1c
    .line 148
    new-array v0, p3, [J

    #@1e
    iput-object v0, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@20
    .line 149
    new-array v0, p3, [J

    #@22
    iput-object v0, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@24
    .line 150
    new-array v0, p3, [J

    #@26
    iput-object v0, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@28
    .line 151
    new-array v0, p3, [J

    #@2a
    iput-object v0, p0, Landroid/net/NetworkStats;->operations:[J

    #@2c
    .line 152
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    #@0
    .prologue
    .line 154
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@6
    move-result-wide v0

    #@7
    iput-wide v0, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@9
    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/net/NetworkStats;->size:I

    #@f
    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@15
    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/net/NetworkStats;->uid:[I

    #@1b
    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/net/NetworkStats;->set:[I

    #@21
    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/net/NetworkStats;->tag:[I

    #@27
    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@2d
    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@33
    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@39
    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    #@3c
    move-result-object v0

    #@3d
    iput-object v0, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@3f
    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Landroid/net/NetworkStats;->operations:[J

    #@45
    .line 166
    return-void
.end method

.method private getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;IZ)Landroid/net/NetworkStats$Entry;
    .registers 16
    .parameter "recycle"
    .parameter
    .parameter "limitUid"
    .parameter "includeTags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/NetworkStats$Entry;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;IZ)",
            "Landroid/net/NetworkStats$Entry;"
        }
    .end annotation

    #@0
    .prologue
    .local p2, limitIface:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v5, 0x1

    #@1
    const/4 v10, -0x1

    #@2
    const/4 v4, 0x0

    #@3
    const-wide/16 v7, 0x0

    #@5
    .line 451
    if-eqz p1, :cond_46

    #@7
    move-object v0, p1

    #@8
    .line 453
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    :goto_8
    sget-object v6, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@a
    iput-object v6, v0, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@c
    .line 454
    iput p3, v0, Landroid/net/NetworkStats$Entry;->uid:I

    #@e
    .line 455
    iput v10, v0, Landroid/net/NetworkStats$Entry;->set:I

    #@10
    .line 456
    iput v4, v0, Landroid/net/NetworkStats$Entry;->tag:I

    #@12
    .line 457
    iput-wide v7, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@14
    .line 458
    iput-wide v7, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@16
    .line 459
    iput-wide v7, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@18
    .line 460
    iput-wide v7, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@1a
    .line 461
    iput-wide v7, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@1c
    .line 463
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    iget v6, p0, Landroid/net/NetworkStats;->size:I

    #@1f
    if-ge v1, v6, :cond_7e

    #@21
    .line 464
    if-eq p3, v10, :cond_29

    #@23
    iget-object v6, p0, Landroid/net/NetworkStats;->uid:[I

    #@25
    aget v6, v6, v1

    #@27
    if-ne p3, v6, :cond_4c

    #@29
    :cond_29
    move v3, v5

    #@2a
    .line 465
    .local v3, matchesUid:Z
    :goto_2a
    if-eqz p2, :cond_36

    #@2c
    iget-object v6, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@2e
    aget-object v6, v6, v1

    #@30
    invoke-virtual {p2, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@33
    move-result v6

    #@34
    if-eqz v6, :cond_4e

    #@36
    :cond_36
    move v2, v5

    #@37
    .line 467
    .local v2, matchesIface:Z
    :goto_37
    if-eqz v3, :cond_43

    #@39
    if-eqz v2, :cond_43

    #@3b
    .line 469
    iget-object v6, p0, Landroid/net/NetworkStats;->tag:[I

    #@3d
    aget v6, v6, v1

    #@3f
    if-eqz v6, :cond_50

    #@41
    if-nez p4, :cond_50

    #@43
    .line 463
    :cond_43
    :goto_43
    add-int/lit8 v1, v1, 0x1

    #@45
    goto :goto_1d

    #@46
    .line 451
    .end local v0           #entry:Landroid/net/NetworkStats$Entry;
    .end local v1           #i:I
    .end local v2           #matchesIface:Z
    .end local v3           #matchesUid:Z
    :cond_46
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@48
    invoke-direct {v0}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@4b
    goto :goto_8

    #@4c
    .restart local v0       #entry:Landroid/net/NetworkStats$Entry;
    .restart local v1       #i:I
    :cond_4c
    move v3, v4

    #@4d
    .line 464
    goto :goto_2a

    #@4e
    .restart local v3       #matchesUid:Z
    :cond_4e
    move v2, v4

    #@4f
    .line 465
    goto :goto_37

    #@50
    .line 471
    .restart local v2       #matchesIface:Z
    :cond_50
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@52
    iget-object v8, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@54
    aget-wide v8, v8, v1

    #@56
    add-long/2addr v6, v8

    #@57
    iput-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@59
    .line 472
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@5b
    iget-object v8, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@5d
    aget-wide v8, v8, v1

    #@5f
    add-long/2addr v6, v8

    #@60
    iput-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@62
    .line 473
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@64
    iget-object v8, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@66
    aget-wide v8, v8, v1

    #@68
    add-long/2addr v6, v8

    #@69
    iput-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@6b
    .line 474
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@6d
    iget-object v8, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@6f
    aget-wide v8, v8, v1

    #@71
    add-long/2addr v6, v8

    #@72
    iput-wide v6, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@74
    .line 475
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@76
    iget-object v8, p0, Landroid/net/NetworkStats;->operations:[J

    #@78
    aget-wide v8, v8, v1

    #@7a
    add-long/2addr v6, v8

    #@7b
    iput-wide v6, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@7d
    goto :goto_43

    #@7e
    .line 478
    .end local v2           #matchesIface:Z
    .end local v3           #matchesUid:Z
    :cond_7e
    return-object v0
.end method

.method public static setToString(I)Ljava/lang/String;
    .registers 2
    .parameter "set"

    #@0
    .prologue
    .line 649
    packed-switch p0, :pswitch_data_10

    #@3
    .line 657
    const-string v0, "UNKNOWN"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 651
    :pswitch_6
    const-string v0, "ALL"

    #@8
    goto :goto_5

    #@9
    .line 653
    :pswitch_9
    const-string v0, "DEFAULT"

    #@b
    goto :goto_5

    #@c
    .line 655
    :pswitch_c
    const-string v0, "FOREGROUND"

    #@e
    goto :goto_5

    #@f
    .line 649
    nop

    #@10
    :pswitch_data_10
    .packed-switch -0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
    .end packed-switch
.end method

.method public static subtract(Landroid/net/NetworkStats;Landroid/net/NetworkStats;Landroid/net/NetworkStats$NonMonotonicObserver;Ljava/lang/Object;)Landroid/net/NetworkStats;
    .registers 17
    .parameter "left"
    .parameter "right"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/net/NetworkStats;",
            "Landroid/net/NetworkStats;",
            "Landroid/net/NetworkStats$NonMonotonicObserver",
            "<TC;>;TC;)",
            "Landroid/net/NetworkStats;"
        }
    .end annotation

    #@0
    .prologue
    .line 500
    .local p2, observer:Landroid/net/NetworkStats$NonMonotonicObserver;,"Landroid/net/NetworkStats$NonMonotonicObserver<TC;>;"
    .local p3, cookie:Ljava/lang/Object;,"TC;"
    iget-wide v0, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@2
    iget-wide v2, p1, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@4
    sub-long v9, v0, v2

    #@6
    .line 501
    .local v9, deltaRealtime:J
    const-wide/16 v0, 0x0

    #@8
    cmp-long v0, v9, v0

    #@a
    if-gez v0, :cond_1a

    #@c
    .line 502
    if-eqz p2, :cond_18

    #@e
    .line 503
    const/4 v2, -0x1

    #@f
    const/4 v4, -0x1

    #@10
    move-object v0, p2

    #@11
    move-object v1, p0

    #@12
    move-object v3, p1

    #@13
    move-object/from16 v5, p3

    #@15
    invoke-interface/range {v0 .. v5}, Landroid/net/NetworkStats$NonMonotonicObserver;->foundNonMonotonic(Landroid/net/NetworkStats;ILandroid/net/NetworkStats;ILjava/lang/Object;)V

    #@18
    .line 505
    :cond_18
    const-wide/16 v9, 0x0

    #@1a
    .line 509
    :cond_1a
    new-instance v11, Landroid/net/NetworkStats$Entry;

    #@1c
    invoke-direct {v11}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@1f
    .line 510
    .local v11, entry:Landroid/net/NetworkStats$Entry;
    new-instance v12, Landroid/net/NetworkStats;

    #@21
    iget v0, p0, Landroid/net/NetworkStats;->size:I

    #@23
    invoke-direct {v12, v9, v10, v0}, Landroid/net/NetworkStats;-><init>(JI)V

    #@26
    .line 511
    .local v12, result:Landroid/net/NetworkStats;
    const/4 v5, 0x0

    #@27
    .local v5, i:I
    :goto_27
    iget v0, p0, Landroid/net/NetworkStats;->size:I

    #@29
    if-ge v5, v0, :cond_114

    #@2b
    .line 512
    iget-object v0, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@2d
    aget-object v0, v0, v5

    #@2f
    iput-object v0, v11, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@31
    .line 513
    iget-object v0, p0, Landroid/net/NetworkStats;->uid:[I

    #@33
    aget v0, v0, v5

    #@35
    iput v0, v11, Landroid/net/NetworkStats$Entry;->uid:I

    #@37
    .line 514
    iget-object v0, p0, Landroid/net/NetworkStats;->set:[I

    #@39
    aget v0, v0, v5

    #@3b
    iput v0, v11, Landroid/net/NetworkStats$Entry;->set:I

    #@3d
    .line 515
    iget-object v0, p0, Landroid/net/NetworkStats;->tag:[I

    #@3f
    aget v0, v0, v5

    #@41
    iput v0, v11, Landroid/net/NetworkStats$Entry;->tag:I

    #@43
    .line 518
    iget-object v1, v11, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@45
    iget v2, v11, Landroid/net/NetworkStats$Entry;->uid:I

    #@47
    iget v3, v11, Landroid/net/NetworkStats$Entry;->set:I

    #@49
    iget v4, v11, Landroid/net/NetworkStats$Entry;->tag:I

    #@4b
    move-object v0, p1

    #@4c
    invoke-virtual/range {v0 .. v5}, Landroid/net/NetworkStats;->findIndexHinted(Ljava/lang/String;IIII)I

    #@4f
    move-result v7

    #@50
    .line 519
    .local v7, j:I
    const/4 v0, -0x1

    #@51
    if-ne v7, v0, :cond_77

    #@53
    .line 521
    iget-object v0, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@55
    aget-wide v0, v0, v5

    #@57
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@59
    .line 522
    iget-object v0, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@5b
    aget-wide v0, v0, v5

    #@5d
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@5f
    .line 523
    iget-object v0, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@61
    aget-wide v0, v0, v5

    #@63
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@65
    .line 524
    iget-object v0, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@67
    aget-wide v0, v0, v5

    #@69
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@6b
    .line 525
    iget-object v0, p0, Landroid/net/NetworkStats;->operations:[J

    #@6d
    aget-wide v0, v0, v5

    #@6f
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->operations:J

    #@71
    .line 547
    :cond_71
    :goto_71
    invoke-virtual {v12, v11}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@74
    .line 511
    add-int/lit8 v5, v5, 0x1

    #@76
    goto :goto_27

    #@77
    .line 528
    :cond_77
    iget-object v0, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@79
    aget-wide v0, v0, v5

    #@7b
    iget-object v2, p1, Landroid/net/NetworkStats;->rxBytes:[J

    #@7d
    aget-wide v2, v2, v7

    #@7f
    sub-long/2addr v0, v2

    #@80
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@82
    .line 529
    iget-object v0, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@84
    aget-wide v0, v0, v5

    #@86
    iget-object v2, p1, Landroid/net/NetworkStats;->rxPackets:[J

    #@88
    aget-wide v2, v2, v7

    #@8a
    sub-long/2addr v0, v2

    #@8b
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@8d
    .line 530
    iget-object v0, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@8f
    aget-wide v0, v0, v5

    #@91
    iget-object v2, p1, Landroid/net/NetworkStats;->txBytes:[J

    #@93
    aget-wide v2, v2, v7

    #@95
    sub-long/2addr v0, v2

    #@96
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@98
    .line 531
    iget-object v0, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@9a
    aget-wide v0, v0, v5

    #@9c
    iget-object v2, p1, Landroid/net/NetworkStats;->txPackets:[J

    #@9e
    aget-wide v2, v2, v7

    #@a0
    sub-long/2addr v0, v2

    #@a1
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@a3
    .line 532
    iget-object v0, p0, Landroid/net/NetworkStats;->operations:[J

    #@a5
    aget-wide v0, v0, v5

    #@a7
    iget-object v2, p1, Landroid/net/NetworkStats;->operations:[J

    #@a9
    aget-wide v2, v2, v7

    #@ab
    sub-long/2addr v0, v2

    #@ac
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->operations:J

    #@ae
    .line 534
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@b0
    const-wide/16 v2, 0x0

    #@b2
    cmp-long v0, v0, v2

    #@b4
    if-ltz v0, :cond_d6

    #@b6
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@b8
    const-wide/16 v2, 0x0

    #@ba
    cmp-long v0, v0, v2

    #@bc
    if-ltz v0, :cond_d6

    #@be
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@c0
    const-wide/16 v2, 0x0

    #@c2
    cmp-long v0, v0, v2

    #@c4
    if-ltz v0, :cond_d6

    #@c6
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@c8
    const-wide/16 v2, 0x0

    #@ca
    cmp-long v0, v0, v2

    #@cc
    if-ltz v0, :cond_d6

    #@ce
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->operations:J

    #@d0
    const-wide/16 v2, 0x0

    #@d2
    cmp-long v0, v0, v2

    #@d4
    if-gez v0, :cond_71

    #@d6
    .line 536
    :cond_d6
    if-eqz p2, :cond_e0

    #@d8
    move-object v3, p2

    #@d9
    move-object v4, p0

    #@da
    move-object v6, p1

    #@db
    move-object/from16 v8, p3

    #@dd
    .line 537
    invoke-interface/range {v3 .. v8}, Landroid/net/NetworkStats$NonMonotonicObserver;->foundNonMonotonic(Landroid/net/NetworkStats;ILandroid/net/NetworkStats;ILjava/lang/Object;)V

    #@e0
    .line 539
    :cond_e0
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@e2
    const-wide/16 v2, 0x0

    #@e4
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@e7
    move-result-wide v0

    #@e8
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@ea
    .line 540
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@ec
    const-wide/16 v2, 0x0

    #@ee
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@f1
    move-result-wide v0

    #@f2
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@f4
    .line 541
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@f6
    const-wide/16 v2, 0x0

    #@f8
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@fb
    move-result-wide v0

    #@fc
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@fe
    .line 542
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@100
    const-wide/16 v2, 0x0

    #@102
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@105
    move-result-wide v0

    #@106
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@108
    .line 543
    iget-wide v0, v11, Landroid/net/NetworkStats$Entry;->operations:J

    #@10a
    const-wide/16 v2, 0x0

    #@10c
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    #@10f
    move-result-wide v0

    #@110
    iput-wide v0, v11, Landroid/net/NetworkStats$Entry;->operations:J

    #@112
    goto/16 :goto_71

    #@114
    .line 550
    .end local v7           #j:I
    :cond_114
    return-object v12
.end method

.method public static tagToString(I)Ljava/lang/String;
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 665
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "0x"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method


# virtual methods
.method public addIfaceValues(Ljava/lang/String;JJJJ)Landroid/net/NetworkStats;
    .registers 25
    .parameter "iface"
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"

    #@0
    .prologue
    .line 197
    const/4 v2, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    const-wide/16 v13, 0x0

    #@5
    move-object v0, p0

    #@6
    move-object/from16 v1, p1

    #@8
    move-wide/from16 v5, p2

    #@a
    move-wide/from16 v7, p4

    #@c
    move-wide/from16 v9, p6

    #@e
    move-wide/from16 v11, p8

    #@10
    invoke-virtual/range {v0 .. v14}, Landroid/net/NetworkStats;->addValues(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method public addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;
    .registers 7
    .parameter "entry"

    #@0
    .prologue
    .line 213
    iget v1, p0, Landroid/net/NetworkStats;->size:I

    #@2
    iget-object v2, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@4
    array-length v2, v2

    #@5
    if-lt v1, v2, :cond_5e

    #@7
    .line 214
    iget-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@9
    array-length v1, v1

    #@a
    const/16 v2, 0xa

    #@c
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@f
    move-result v1

    #@10
    mul-int/lit8 v1, v1, 0x3

    #@12
    div-int/lit8 v0, v1, 0x2

    #@14
    .line 215
    .local v0, newLength:I
    iget-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@16
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, [Ljava/lang/String;

    #@1c
    iput-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@1e
    .line 216
    iget-object v1, p0, Landroid/net/NetworkStats;->uid:[I

    #@20
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    #@23
    move-result-object v1

    #@24
    iput-object v1, p0, Landroid/net/NetworkStats;->uid:[I

    #@26
    .line 217
    iget-object v1, p0, Landroid/net/NetworkStats;->set:[I

    #@28
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    #@2b
    move-result-object v1

    #@2c
    iput-object v1, p0, Landroid/net/NetworkStats;->set:[I

    #@2e
    .line 218
    iget-object v1, p0, Landroid/net/NetworkStats;->tag:[I

    #@30
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Landroid/net/NetworkStats;->tag:[I

    #@36
    .line 219
    iget-object v1, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@38
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    #@3b
    move-result-object v1

    #@3c
    iput-object v1, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@3e
    .line 220
    iget-object v1, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@40
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    #@43
    move-result-object v1

    #@44
    iput-object v1, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@46
    .line 221
    iget-object v1, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@48
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    #@4b
    move-result-object v1

    #@4c
    iput-object v1, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@4e
    .line 222
    iget-object v1, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@50
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    #@53
    move-result-object v1

    #@54
    iput-object v1, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@56
    .line 223
    iget-object v1, p0, Landroid/net/NetworkStats;->operations:[J

    #@58
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    #@5b
    move-result-object v1

    #@5c
    iput-object v1, p0, Landroid/net/NetworkStats;->operations:[J

    #@5e
    .line 226
    .end local v0           #newLength:I
    :cond_5e
    iget-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@60
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@62
    iget-object v3, p1, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@64
    aput-object v3, v1, v2

    #@66
    .line 227
    iget-object v1, p0, Landroid/net/NetworkStats;->uid:[I

    #@68
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@6a
    iget v3, p1, Landroid/net/NetworkStats$Entry;->uid:I

    #@6c
    aput v3, v1, v2

    #@6e
    .line 228
    iget-object v1, p0, Landroid/net/NetworkStats;->set:[I

    #@70
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@72
    iget v3, p1, Landroid/net/NetworkStats$Entry;->set:I

    #@74
    aput v3, v1, v2

    #@76
    .line 229
    iget-object v1, p0, Landroid/net/NetworkStats;->tag:[I

    #@78
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@7a
    iget v3, p1, Landroid/net/NetworkStats$Entry;->tag:I

    #@7c
    aput v3, v1, v2

    #@7e
    .line 230
    iget-object v1, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@80
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@82
    iget-wide v3, p1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@84
    aput-wide v3, v1, v2

    #@86
    .line 231
    iget-object v1, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@88
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@8a
    iget-wide v3, p1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@8c
    aput-wide v3, v1, v2

    #@8e
    .line 232
    iget-object v1, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@90
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@92
    iget-wide v3, p1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@94
    aput-wide v3, v1, v2

    #@96
    .line 233
    iget-object v1, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@98
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@9a
    iget-wide v3, p1, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@9c
    aput-wide v3, v1, v2

    #@9e
    .line 234
    iget-object v1, p0, Landroid/net/NetworkStats;->operations:[J

    #@a0
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@a2
    iget-wide v3, p1, Landroid/net/NetworkStats$Entry;->operations:J

    #@a4
    aput-wide v3, v1, v2

    #@a6
    .line 235
    iget v1, p0, Landroid/net/NetworkStats;->size:I

    #@a8
    add-int/lit8 v1, v1, 0x1

    #@aa
    iput v1, p0, Landroid/net/NetworkStats;->size:I

    #@ac
    .line 237
    return-object p0
.end method

.method public addValues(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;
    .registers 30
    .parameter "iface"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"
    .parameter "operations"

    #@0
    .prologue
    .line 204
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@2
    move-object/from16 v1, p1

    #@4
    move/from16 v2, p2

    #@6
    move/from16 v3, p3

    #@8
    move/from16 v4, p4

    #@a
    move-wide/from16 v5, p5

    #@c
    move-wide/from16 v7, p7

    #@e
    move-wide/from16 v9, p9

    #@10
    move-wide/from16 v11, p11

    #@12
    move-wide/from16 v13, p13

    #@14
    invoke-direct/range {v0 .. v14}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@17
    invoke-virtual {p0, v0}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public clone()Landroid/net/NetworkStats;
    .registers 7

    #@0
    .prologue
    .line 185
    new-instance v0, Landroid/net/NetworkStats;

    #@2
    iget-wide v3, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@4
    iget v5, p0, Landroid/net/NetworkStats;->size:I

    #@6
    invoke-direct {v0, v3, v4, v5}, Landroid/net/NetworkStats;-><init>(JI)V

    #@9
    .line 186
    .local v0, clone:Landroid/net/NetworkStats;
    const/4 v1, 0x0

    #@a
    .line 187
    .local v1, entry:Landroid/net/NetworkStats$Entry;
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    iget v3, p0, Landroid/net/NetworkStats;->size:I

    #@d
    if-ge v2, v3, :cond_19

    #@f
    .line 188
    invoke-virtual {p0, v2, v1}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@12
    move-result-object v1

    #@13
    .line 189
    invoke-virtual {v0, v1}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@16
    .line 187
    add-int/lit8 v2, v2, 0x1

    #@18
    goto :goto_b

    #@19
    .line 191
    :cond_19
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/net/NetworkStats;->clone()Landroid/net/NetworkStats;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public combineAllValues(Landroid/net/NetworkStats;)V
    .registers 5
    .parameter "another"

    #@0
    .prologue
    .line 315
    const/4 v0, 0x0

    #@1
    .line 316
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget v2, p1, Landroid/net/NetworkStats;->size:I

    #@4
    if-ge v1, v2, :cond_10

    #@6
    .line 317
    invoke-virtual {p1, v1, v0}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@9
    move-result-object v0

    #@a
    .line 318
    invoke-virtual {p0, v0}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@d
    .line 316
    add-int/lit8 v1, v1, 0x1

    #@f
    goto :goto_2

    #@10
    .line 320
    :cond_10
    return-void
.end method

.method public combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;
    .registers 8
    .parameter "entry"

    #@0
    .prologue
    .line 297
    iget-object v1, p1, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@2
    iget v2, p1, Landroid/net/NetworkStats$Entry;->uid:I

    #@4
    iget v3, p1, Landroid/net/NetworkStats$Entry;->set:I

    #@6
    iget v4, p1, Landroid/net/NetworkStats$Entry;->tag:I

    #@8
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/net/NetworkStats;->findIndex(Ljava/lang/String;III)I

    #@b
    move-result v0

    #@c
    .line 298
    .local v0, i:I
    const/4 v1, -0x1

    #@d
    if-ne v0, v1, :cond_13

    #@f
    .line 300
    invoke-virtual {p0, p1}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@12
    .line 308
    :goto_12
    return-object p0

    #@13
    .line 302
    :cond_13
    iget-object v1, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@15
    aget-wide v2, v1, v0

    #@17
    iget-wide v4, p1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@19
    add-long/2addr v2, v4

    #@1a
    aput-wide v2, v1, v0

    #@1c
    .line 303
    iget-object v1, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@1e
    aget-wide v2, v1, v0

    #@20
    iget-wide v4, p1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@22
    add-long/2addr v2, v4

    #@23
    aput-wide v2, v1, v0

    #@25
    .line 304
    iget-object v1, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@27
    aget-wide v2, v1, v0

    #@29
    iget-wide v4, p1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@2b
    add-long/2addr v2, v4

    #@2c
    aput-wide v2, v1, v0

    #@2e
    .line 305
    iget-object v1, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@30
    aget-wide v2, v1, v0

    #@32
    iget-wide v4, p1, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@34
    add-long/2addr v2, v4

    #@35
    aput-wide v2, v1, v0

    #@37
    .line 306
    iget-object v1, p0, Landroid/net/NetworkStats;->operations:[J

    #@39
    aget-wide v2, v1, v0

    #@3b
    iget-wide v4, p1, Landroid/net/NetworkStats$Entry;->operations:J

    #@3d
    add-long/2addr v2, v4

    #@3e
    aput-wide v2, v1, v0

    #@40
    goto :goto_12
.end method

.method public combineValues(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;
    .registers 30
    .parameter "iface"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"
    .parameter "operations"

    #@0
    .prologue
    .line 287
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@2
    move-object/from16 v1, p1

    #@4
    move/from16 v2, p2

    #@6
    move/from16 v3, p3

    #@8
    move/from16 v4, p4

    #@a
    move-wide/from16 v5, p5

    #@c
    move-wide/from16 v7, p7

    #@e
    move-wide/from16 v9, p9

    #@10
    move-wide/from16 v11, p11

    #@12
    move-wide/from16 v13, p13

    #@14
    invoke-direct/range {v0 .. v14}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@17
    invoke-virtual {p0, v0}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public combineValues(Ljava/lang/String;IIJJJJJ)Landroid/net/NetworkStats;
    .registers 29
    .parameter "iface"
    .parameter "uid"
    .parameter "tag"
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"
    .parameter "operations"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 281
    const/4 v3, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object/from16 v1, p1

    #@4
    move/from16 v2, p2

    #@6
    move/from16 v4, p3

    #@8
    move-wide/from16 v5, p4

    #@a
    move-wide/from16 v7, p6

    #@c
    move-wide/from16 v9, p8

    #@e
    move-wide/from16 v11, p10

    #@10
    move-wide/from16 v13, p12

    #@12
    invoke-virtual/range {v0 .. v14}, Landroid/net/NetworkStats;->combineValues(Ljava/lang/String;IIIJJJJJ)Landroid/net/NetworkStats;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 677
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "prefix"
    .parameter "pw"

    #@0
    .prologue
    .line 628
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    .line 629
    const-string v1, "NetworkStats: elapsedRealtime="

    #@5
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-wide v1, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@a
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->println(J)V

    #@d
    .line 630
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    iget v1, p0, Landroid/net/NetworkStats;->size:I

    #@10
    if-ge v0, v1, :cond_9a

    #@12
    .line 631
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    .line 632
    const-string v1, "  ["

    #@17
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1d
    const-string v1, "]"

    #@1f
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    .line 633
    const-string v1, " iface="

    #@24
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27
    iget-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@29
    aget-object v1, v1, v0

    #@2b
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    .line 634
    const-string v1, " uid="

    #@30
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    iget-object v1, p0, Landroid/net/NetworkStats;->uid:[I

    #@35
    aget v1, v1, v0

    #@37
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@3a
    .line 635
    const-string v1, " set="

    #@3c
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    iget-object v1, p0, Landroid/net/NetworkStats;->set:[I

    #@41
    aget v1, v1, v0

    #@43
    invoke-static {v1}, Landroid/net/NetworkStats;->setToString(I)Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    .line 636
    const-string v1, " tag="

    #@4c
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4f
    iget-object v1, p0, Landroid/net/NetworkStats;->tag:[I

    #@51
    aget v1, v1, v0

    #@53
    invoke-static {v1}, Landroid/net/NetworkStats;->tagToString(I)Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5a
    .line 637
    const-string v1, " rxBytes="

    #@5c
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5f
    iget-object v1, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@61
    aget-wide v1, v1, v0

    #@63
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@66
    .line 638
    const-string v1, " rxPackets="

    #@68
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6b
    iget-object v1, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@6d
    aget-wide v1, v1, v0

    #@6f
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@72
    .line 639
    const-string v1, " txBytes="

    #@74
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@77
    iget-object v1, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@79
    aget-wide v1, v1, v0

    #@7b
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@7e
    .line 640
    const-string v1, " txPackets="

    #@80
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@83
    iget-object v1, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@85
    aget-wide v1, v1, v0

    #@87
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@8a
    .line 641
    const-string v1, " operations="

    #@8c
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f
    iget-object v1, p0, Landroid/net/NetworkStats;->operations:[J

    #@91
    aget-wide v1, v1, v0

    #@93
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->println(J)V

    #@96
    .line 630
    add-int/lit8 v0, v0, 0x1

    #@98
    goto/16 :goto_e

    #@9a
    .line 643
    :cond_9a
    return-void
.end method

.method public findIndex(Ljava/lang/String;III)I
    .registers 7
    .parameter "iface"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"

    #@0
    .prologue
    .line 326
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/net/NetworkStats;->size:I

    #@3
    if-ge v0, v1, :cond_25

    #@5
    .line 327
    iget-object v1, p0, Landroid/net/NetworkStats;->uid:[I

    #@7
    aget v1, v1, v0

    #@9
    if-ne p2, v1, :cond_22

    #@b
    iget-object v1, p0, Landroid/net/NetworkStats;->set:[I

    #@d
    aget v1, v1, v0

    #@f
    if-ne p3, v1, :cond_22

    #@11
    iget-object v1, p0, Landroid/net/NetworkStats;->tag:[I

    #@13
    aget v1, v1, v0

    #@15
    if-ne p4, v1, :cond_22

    #@17
    iget-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@19
    aget-object v1, v1, v0

    #@1b
    invoke-static {p1, v1}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_22

    #@21
    .line 332
    .end local v0           #i:I
    :goto_21
    return v0

    #@22
    .line 326
    .restart local v0       #i:I
    :cond_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_1

    #@25
    .line 332
    :cond_25
    const/4 v0, -0x1

    #@26
    goto :goto_21
.end method

.method public findIndexHinted(Ljava/lang/String;IIII)I
    .registers 11
    .parameter "iface"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "hintIndex"

    #@0
    .prologue
    .line 341
    const/4 v2, 0x0

    #@1
    .local v2, offset:I
    :goto_1
    iget v3, p0, Landroid/net/NetworkStats;->size:I

    #@3
    if-ge v2, v3, :cond_3c

    #@5
    .line 342
    div-int/lit8 v0, v2, 0x2

    #@7
    .line 346
    .local v0, halfOffset:I
    rem-int/lit8 v3, v2, 0x2

    #@9
    if-nez v3, :cond_2e

    #@b
    .line 347
    add-int v3, p5, v0

    #@d
    iget v4, p0, Landroid/net/NetworkStats;->size:I

    #@f
    rem-int v1, v3, v4

    #@11
    .line 352
    .local v1, i:I
    :goto_11
    iget-object v3, p0, Landroid/net/NetworkStats;->uid:[I

    #@13
    aget v3, v3, v1

    #@15
    if-ne p2, v3, :cond_39

    #@17
    iget-object v3, p0, Landroid/net/NetworkStats;->set:[I

    #@19
    aget v3, v3, v1

    #@1b
    if-ne p3, v3, :cond_39

    #@1d
    iget-object v3, p0, Landroid/net/NetworkStats;->tag:[I

    #@1f
    aget v3, v3, v1

    #@21
    if-ne p4, v3, :cond_39

    #@23
    iget-object v3, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@25
    aget-object v3, v3, v1

    #@27
    invoke-static {p1, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_39

    #@2d
    .line 357
    .end local v0           #halfOffset:I
    .end local v1           #i:I
    :goto_2d
    return v1

    #@2e
    .line 349
    .restart local v0       #halfOffset:I
    :cond_2e
    iget v3, p0, Landroid/net/NetworkStats;->size:I

    #@30
    add-int/2addr v3, p5

    #@31
    sub-int/2addr v3, v0

    #@32
    add-int/lit8 v3, v3, -0x1

    #@34
    iget v4, p0, Landroid/net/NetworkStats;->size:I

    #@36
    rem-int v1, v3, v4

    #@38
    .restart local v1       #i:I
    goto :goto_11

    #@39
    .line 341
    :cond_39
    add-int/lit8 v2, v2, 0x1

    #@3b
    goto :goto_1

    #@3c
    .line 357
    .end local v0           #halfOffset:I
    .end local v1           #i:I
    :cond_3c
    const/4 v1, -0x1

    #@3d
    goto :goto_2d
.end method

.method public getElapsedRealtime()J
    .registers 3

    #@0
    .prologue
    .line 258
    iget-wide v0, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@2
    return-wide v0
.end method

.method public getElapsedRealtimeAge()J
    .registers 5

    #@0
    .prologue
    .line 266
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iget-wide v2, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@6
    sub-long/2addr v0, v2

    #@7
    return-wide v0
.end method

.method public getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;
    .registers 5
    .parameter "recycle"

    #@0
    .prologue
    .line 419
    const/4 v0, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    const/4 v2, 0x0

    #@3
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;IZ)Landroid/net/NetworkStats$Entry;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getTotal(Landroid/net/NetworkStats$Entry;I)Landroid/net/NetworkStats$Entry;
    .registers 5
    .parameter "recycle"
    .parameter "limitUid"

    #@0
    .prologue
    .line 427
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, p2, v1}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;IZ)Landroid/net/NetworkStats$Entry;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;)Landroid/net/NetworkStats$Entry;
    .registers 5
    .parameter "recycle"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/NetworkStats$Entry;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/NetworkStats$Entry;"
        }
    .end annotation

    #@0
    .prologue
    .line 435
    .local p2, limitIface:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v0, -0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;IZ)Landroid/net/NetworkStats$Entry;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTotalBytes()J
    .registers 6

    #@0
    .prologue
    .line 411
    const/4 v1, 0x0

    #@1
    invoke-virtual {p0, v1}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@4
    move-result-object v0

    #@5
    .line 412
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    iget-wide v1, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@7
    iget-wide v3, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@9
    add-long/2addr v1, v3

    #@a
    return-wide v1
.end method

.method public getTotalIncludingTags(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;
    .registers 5
    .parameter "recycle"

    #@0
    .prologue
    .line 439
    const/4 v0, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    const/4 v2, 0x1

    #@3
    invoke-direct {p0, p1, v0, v1, v2}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;IZ)Landroid/net/NetworkStats$Entry;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getUniqueIfaces()[Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 380
    new-instance v3, Ljava/util/HashSet;

    #@2
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 381
    .local v3, ifaces:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v0, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@7
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@8
    .local v4, len$:I
    const/4 v1, 0x0

    #@9
    .local v1, i$:I
    :goto_9
    if-ge v1, v4, :cond_17

    #@b
    aget-object v2, v0, v1

    #@d
    .line 382
    .local v2, iface:Ljava/lang/String;
    sget-object v5, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@f
    if-eq v2, v5, :cond_14

    #@11
    .line 383
    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@14
    .line 381
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_9

    #@17
    .line 386
    .end local v2           #iface:Ljava/lang/String;
    :cond_17
    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    #@1a
    move-result v5

    #@1b
    new-array v5, v5, [Ljava/lang/String;

    #@1d
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@20
    move-result-object v5

    #@21
    check-cast v5, [Ljava/lang/String;

    #@23
    return-object v5
.end method

.method public getUniqueUids()[I
    .registers 10

    #@0
    .prologue
    .line 393
    new-instance v7, Landroid/util/SparseBooleanArray;

    #@2
    invoke-direct {v7}, Landroid/util/SparseBooleanArray;-><init>()V

    #@5
    .line 394
    .local v7, uids:Landroid/util/SparseBooleanArray;
    iget-object v0, p0, Landroid/net/NetworkStats;->uid:[I

    #@7
    .local v0, arr$:[I
    array-length v3, v0

    #@8
    .local v3, len$:I
    const/4 v2, 0x0

    #@9
    .local v2, i$:I
    :goto_9
    if-ge v2, v3, :cond_14

    #@b
    aget v6, v0, v2

    #@d
    .line 395
    .local v6, uid:I
    const/4 v8, 0x1

    #@e
    invoke-virtual {v7, v6, v8}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@11
    .line 394
    add-int/lit8 v2, v2, 0x1

    #@13
    goto :goto_9

    #@14
    .line 398
    .end local v6           #uid:I
    :cond_14
    invoke-virtual {v7}, Landroid/util/SparseBooleanArray;->size()I

    #@17
    move-result v5

    #@18
    .line 399
    .local v5, size:I
    new-array v4, v5, [I

    #@1a
    .line 400
    .local v4, result:[I
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    if-ge v1, v5, :cond_26

    #@1d
    .line 401
    invoke-virtual {v7, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@20
    move-result v8

    #@21
    aput v8, v4, v1

    #@23
    .line 400
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_1b

    #@26
    .line 403
    :cond_26
    return-object v4
.end method

.method public getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;
    .registers 6
    .parameter "i"
    .parameter "recycle"

    #@0
    .prologue
    .line 244
    if-eqz p2, :cond_3a

    #@2
    move-object v0, p2

    #@3
    .line 245
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    :goto_3
    iget-object v1, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@5
    aget-object v1, v1, p1

    #@7
    iput-object v1, v0, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@9
    .line 246
    iget-object v1, p0, Landroid/net/NetworkStats;->uid:[I

    #@b
    aget v1, v1, p1

    #@d
    iput v1, v0, Landroid/net/NetworkStats$Entry;->uid:I

    #@f
    .line 247
    iget-object v1, p0, Landroid/net/NetworkStats;->set:[I

    #@11
    aget v1, v1, p1

    #@13
    iput v1, v0, Landroid/net/NetworkStats$Entry;->set:I

    #@15
    .line 248
    iget-object v1, p0, Landroid/net/NetworkStats;->tag:[I

    #@17
    aget v1, v1, p1

    #@19
    iput v1, v0, Landroid/net/NetworkStats$Entry;->tag:I

    #@1b
    .line 249
    iget-object v1, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@1d
    aget-wide v1, v1, p1

    #@1f
    iput-wide v1, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@21
    .line 250
    iget-object v1, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@23
    aget-wide v1, v1, p1

    #@25
    iput-wide v1, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@27
    .line 251
    iget-object v1, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@29
    aget-wide v1, v1, p1

    #@2b
    iput-wide v1, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@2d
    .line 252
    iget-object v1, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@2f
    aget-wide v1, v1, p1

    #@31
    iput-wide v1, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@33
    .line 253
    iget-object v1, p0, Landroid/net/NetworkStats;->operations:[J

    #@35
    aget-wide v1, v1, p1

    #@37
    iput-wide v1, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@39
    .line 254
    return-object v0

    #@3a
    .line 244
    .end local v0           #entry:Landroid/net/NetworkStats$Entry;
    :cond_3a
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@3c
    invoke-direct {v0}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@3f
    goto :goto_3
.end method

.method public groupedByIface()Landroid/net/NetworkStats;
    .registers 8

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 558
    new-instance v2, Landroid/net/NetworkStats;

    #@3
    iget-wide v3, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@5
    const/16 v5, 0xa

    #@7
    invoke-direct {v2, v3, v4, v5}, Landroid/net/NetworkStats;-><init>(JI)V

    #@a
    .line 560
    .local v2, stats:Landroid/net/NetworkStats;
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@c
    invoke-direct {v0}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@f
    .line 561
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    iput v6, v0, Landroid/net/NetworkStats$Entry;->uid:I

    #@11
    .line 562
    iput v6, v0, Landroid/net/NetworkStats$Entry;->set:I

    #@13
    .line 563
    const/4 v3, 0x0

    #@14
    iput v3, v0, Landroid/net/NetworkStats$Entry;->tag:I

    #@16
    .line 564
    const-wide/16 v3, 0x0

    #@18
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@1a
    .line 566
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    iget v3, p0, Landroid/net/NetworkStats;->size:I

    #@1d
    if-ge v1, v3, :cond_4a

    #@1f
    .line 568
    iget-object v3, p0, Landroid/net/NetworkStats;->tag:[I

    #@21
    aget v3, v3, v1

    #@23
    if-eqz v3, :cond_28

    #@25
    .line 566
    :goto_25
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_1b

    #@28
    .line 570
    :cond_28
    iget-object v3, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@2a
    aget-object v3, v3, v1

    #@2c
    iput-object v3, v0, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@2e
    .line 571
    iget-object v3, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@30
    aget-wide v3, v3, v1

    #@32
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@34
    .line 572
    iget-object v3, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@36
    aget-wide v3, v3, v1

    #@38
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@3a
    .line 573
    iget-object v3, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@3c
    aget-wide v3, v3, v1

    #@3e
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@40
    .line 574
    iget-object v3, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@42
    aget-wide v3, v3, v1

    #@44
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@46
    .line 575
    invoke-virtual {v2, v0}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@49
    goto :goto_25

    #@4a
    .line 578
    :cond_4a
    return-object v2
.end method

.method public groupedByUid()Landroid/net/NetworkStats;
    .registers 7

    #@0
    .prologue
    .line 586
    new-instance v2, Landroid/net/NetworkStats;

    #@2
    iget-wide v3, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@4
    const/16 v5, 0xa

    #@6
    invoke-direct {v2, v3, v4, v5}, Landroid/net/NetworkStats;-><init>(JI)V

    #@9
    .line 588
    .local v2, stats:Landroid/net/NetworkStats;
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@b
    invoke-direct {v0}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@e
    .line 589
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    sget-object v3, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@10
    iput-object v3, v0, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@12
    .line 590
    const/4 v3, -0x1

    #@13
    iput v3, v0, Landroid/net/NetworkStats$Entry;->set:I

    #@15
    .line 591
    const/4 v3, 0x0

    #@16
    iput v3, v0, Landroid/net/NetworkStats$Entry;->tag:I

    #@18
    .line 593
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    iget v3, p0, Landroid/net/NetworkStats;->size:I

    #@1b
    if-ge v1, v3, :cond_4e

    #@1d
    .line 595
    iget-object v3, p0, Landroid/net/NetworkStats;->tag:[I

    #@1f
    aget v3, v3, v1

    #@21
    if-eqz v3, :cond_26

    #@23
    .line 593
    :goto_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_19

    #@26
    .line 597
    :cond_26
    iget-object v3, p0, Landroid/net/NetworkStats;->uid:[I

    #@28
    aget v3, v3, v1

    #@2a
    iput v3, v0, Landroid/net/NetworkStats$Entry;->uid:I

    #@2c
    .line 598
    iget-object v3, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@2e
    aget-wide v3, v3, v1

    #@30
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@32
    .line 599
    iget-object v3, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@34
    aget-wide v3, v3, v1

    #@36
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@38
    .line 600
    iget-object v3, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@3a
    aget-wide v3, v3, v1

    #@3c
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@3e
    .line 601
    iget-object v3, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@40
    aget-wide v3, v3, v1

    #@42
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@44
    .line 602
    iget-object v3, p0, Landroid/net/NetworkStats;->operations:[J

    #@46
    aget-wide v3, v3, v1

    #@48
    iput-wide v3, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@4a
    .line 603
    invoke-virtual {v2, v0}, Landroid/net/NetworkStats;->combineValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@4d
    goto :goto_23

    #@4e
    .line 606
    :cond_4e
    return-object v2
.end method

.method public internalSize()I
    .registers 2

    #@0
    .prologue
    .line 275
    iget-object v0, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@2
    array-length v0, v0

    #@3
    return v0
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 270
    iget v0, p0, Landroid/net/NetworkStats;->size:I

    #@2
    return v0
.end method

.method public spliceOperationsFrom(Landroid/net/NetworkStats;)V
    .registers 8
    .parameter "stats"

    #@0
    .prologue
    .line 366
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v2, p0, Landroid/net/NetworkStats;->size:I

    #@3
    if-ge v0, v2, :cond_2e

    #@5
    .line 367
    iget-object v2, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@7
    aget-object v2, v2, v0

    #@9
    iget-object v3, p0, Landroid/net/NetworkStats;->uid:[I

    #@b
    aget v3, v3, v0

    #@d
    iget-object v4, p0, Landroid/net/NetworkStats;->set:[I

    #@f
    aget v4, v4, v0

    #@11
    iget-object v5, p0, Landroid/net/NetworkStats;->tag:[I

    #@13
    aget v5, v5, v0

    #@15
    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/net/NetworkStats;->findIndex(Ljava/lang/String;III)I

    #@18
    move-result v1

    #@19
    .line 368
    .local v1, j:I
    const/4 v2, -0x1

    #@1a
    if-ne v1, v2, :cond_25

    #@1c
    .line 369
    iget-object v2, p0, Landroid/net/NetworkStats;->operations:[J

    #@1e
    const-wide/16 v3, 0x0

    #@20
    aput-wide v3, v2, v0

    #@22
    .line 366
    :goto_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_1

    #@25
    .line 371
    :cond_25
    iget-object v2, p0, Landroid/net/NetworkStats;->operations:[J

    #@27
    iget-object v3, p1, Landroid/net/NetworkStats;->operations:[J

    #@29
    aget-wide v3, v3, v1

    #@2b
    aput-wide v3, v2, v0

    #@2d
    goto :goto_22

    #@2e
    .line 374
    .end local v1           #j:I
    :cond_2e
    return-void
.end method

.method public subtract(Landroid/net/NetworkStats;)Landroid/net/NetworkStats;
    .registers 3
    .parameter "right"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 487
    invoke-static {p0, p1, v0, v0}, Landroid/net/NetworkStats;->subtract(Landroid/net/NetworkStats;Landroid/net/NetworkStats;Landroid/net/NetworkStats$NonMonotonicObserver;Ljava/lang/Object;)Landroid/net/NetworkStats;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 670
    new-instance v0, Ljava/io/CharArrayWriter;

    #@2
    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    #@5
    .line 671
    .local v0, writer:Ljava/io/CharArrayWriter;
    const-string v1, ""

    #@7
    new-instance v2, Ljava/io/PrintWriter;

    #@9
    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@c
    invoke-virtual {p0, v1, v2}, Landroid/net/NetworkStats;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@f
    .line 672
    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method

.method public withoutUids([I)Landroid/net/NetworkStats;
    .registers 8
    .parameter "uids"

    #@0
    .prologue
    .line 614
    new-instance v2, Landroid/net/NetworkStats;

    #@2
    iget-wide v3, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@4
    const/16 v5, 0xa

    #@6
    invoke-direct {v2, v3, v4, v5}, Landroid/net/NetworkStats;-><init>(JI)V

    #@9
    .line 616
    .local v2, stats:Landroid/net/NetworkStats;
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@b
    invoke-direct {v0}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@e
    .line 617
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    iget v3, p0, Landroid/net/NetworkStats;->size:I

    #@11
    if-ge v1, v3, :cond_25

    #@13
    .line 618
    invoke-virtual {p0, v1, v0}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@16
    move-result-object v0

    #@17
    .line 619
    iget v3, v0, Landroid/net/NetworkStats$Entry;->uid:I

    #@19
    invoke-static {p1, v3}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_22

    #@1f
    .line 620
    invoke-virtual {v2, v0}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@22
    .line 617
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_f

    #@25
    .line 624
    :cond_25
    return-object v2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 170
    iget-wide v0, p0, Landroid/net/NetworkStats;->elapsedRealtime:J

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 171
    iget v0, p0, Landroid/net/NetworkStats;->size:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 172
    iget-object v0, p0, Landroid/net/NetworkStats;->iface:[Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@f
    .line 173
    iget-object v0, p0, Landroid/net/NetworkStats;->uid:[I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@14
    .line 174
    iget-object v0, p0, Landroid/net/NetworkStats;->set:[I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@19
    .line 175
    iget-object v0, p0, Landroid/net/NetworkStats;->tag:[I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1e
    .line 176
    iget-object v0, p0, Landroid/net/NetworkStats;->rxBytes:[J

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    #@23
    .line 177
    iget-object v0, p0, Landroid/net/NetworkStats;->rxPackets:[J

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    #@28
    .line 178
    iget-object v0, p0, Landroid/net/NetworkStats;->txBytes:[J

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    #@2d
    .line 179
    iget-object v0, p0, Landroid/net/NetworkStats;->txPackets:[J

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    #@32
    .line 180
    iget-object v0, p0, Landroid/net/NetworkStats;->operations:[J

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    #@37
    .line 181
    return-void
.end method
