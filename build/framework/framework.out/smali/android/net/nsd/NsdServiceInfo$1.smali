.class final Landroid/net/nsd/NsdServiceInfo$1;
.super Ljava/lang/Object;
.source "NsdServiceInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/nsd/NsdServiceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/nsd/NsdServiceInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 132
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/nsd/NsdServiceInfo;
    .registers 5
    .parameter "in"

    #@0
    .prologue
    .line 134
    new-instance v0, Landroid/net/nsd/NsdServiceInfo;

    #@2
    invoke-direct {v0}, Landroid/net/nsd/NsdServiceInfo;-><init>()V

    #@5
    .line 135
    .local v0, info:Landroid/net/nsd/NsdServiceInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-static {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->access$002(Landroid/net/nsd/NsdServiceInfo;Ljava/lang/String;)Ljava/lang/String;

    #@c
    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-static {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->access$102(Landroid/net/nsd/NsdServiceInfo;Ljava/lang/String;)Ljava/lang/String;

    #@13
    .line 137
    const/4 v1, 0x0

    #@14
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/net/nsd/DnsSdTxtRecord;

    #@1a
    invoke-static {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->access$202(Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/DnsSdTxtRecord;)Landroid/net/nsd/DnsSdTxtRecord;

    #@1d
    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@20
    move-result v1

    #@21
    const/4 v2, 0x1

    #@22
    if-ne v1, v2, :cond_2f

    #@24
    .line 141
    :try_start_24
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@27
    move-result-object v1

    #@28
    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->access$302(Landroid/net/nsd/NsdServiceInfo;Ljava/net/InetAddress;)Ljava/net/InetAddress;
    :try_end_2f
    .catch Ljava/net/UnknownHostException; {:try_start_24 .. :try_end_2f} :catch_37

    #@2f
    .line 145
    :cond_2f
    :goto_2f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v1

    #@33
    invoke-static {v0, v1}, Landroid/net/nsd/NsdServiceInfo;->access$402(Landroid/net/nsd/NsdServiceInfo;I)I

    #@36
    .line 146
    return-object v0

    #@37
    .line 142
    :catch_37
    move-exception v1

    #@38
    goto :goto_2f
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 132
    invoke-virtual {p0, p1}, Landroid/net/nsd/NsdServiceInfo$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/nsd/NsdServiceInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/nsd/NsdServiceInfo;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 150
    new-array v0, p1, [Landroid/net/nsd/NsdServiceInfo;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 132
    invoke-virtual {p0, p1}, Landroid/net/nsd/NsdServiceInfo$1;->newArray(I)[Landroid/net/nsd/NsdServiceInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
