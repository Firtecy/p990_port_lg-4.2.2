.class public final Landroid/net/nsd/NsdManager;
.super Ljava/lang/Object;
.source "NsdManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/nsd/NsdManager$ServiceHandler;,
        Landroid/net/nsd/NsdManager$ResolveListener;,
        Landroid/net/nsd/NsdManager$RegistrationListener;,
        Landroid/net/nsd/NsdManager$DiscoveryListener;
    }
.end annotation


# static fields
.field public static final ACTION_NSD_STATE_CHANGED:Ljava/lang/String; = "android.net.nsd.STATE_CHANGED"

.field private static final BASE:I = 0x60000

.field public static final DISABLE:I = 0x60019

.field public static final DISCOVER_SERVICES:I = 0x60001

.field public static final DISCOVER_SERVICES_FAILED:I = 0x60003

.field public static final DISCOVER_SERVICES_STARTED:I = 0x60002

.field public static final ENABLE:I = 0x60018

.field public static final EXTRA_NSD_STATE:Ljava/lang/String; = "nsd_state"

.field public static final FAILURE_ALREADY_ACTIVE:I = 0x3

.field public static final FAILURE_INTERNAL_ERROR:I = 0x0

.field public static final FAILURE_MAX_LIMIT:I = 0x4

.field private static final INVALID_LISTENER_KEY:I = 0x0

.field public static final NATIVE_DAEMON_EVENT:I = 0x6001a

.field public static final NSD_STATE_DISABLED:I = 0x1

.field public static final NSD_STATE_ENABLED:I = 0x2

.field public static final PROTOCOL_DNS_SD:I = 0x1

.field public static final REGISTER_SERVICE:I = 0x60009

.field public static final REGISTER_SERVICE_FAILED:I = 0x6000a

.field public static final REGISTER_SERVICE_SUCCEEDED:I = 0x6000b

.field public static final RESOLVE_SERVICE:I = 0x60012

.field public static final RESOLVE_SERVICE_FAILED:I = 0x60013

.field public static final RESOLVE_SERVICE_SUCCEEDED:I = 0x60014

.field public static final SERVICE_FOUND:I = 0x60004

.field public static final SERVICE_LOST:I = 0x60005

.field public static final STOP_DISCOVERY:I = 0x60006

.field public static final STOP_DISCOVERY_FAILED:I = 0x60007

.field public static final STOP_DISCOVERY_SUCCEEDED:I = 0x60008

.field private static final TAG:Ljava/lang/String; = "NsdManager"

.field public static final UNREGISTER_SERVICE:I = 0x6000c

.field public static final UNREGISTER_SERVICE_FAILED:I = 0x6000d

.field public static final UNREGISTER_SERVICE_SUCCEEDED:I = 0x6000e


# instance fields
.field private final mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

.field private final mConnected:Ljava/util/concurrent/CountDownLatch;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/net/nsd/NsdManager$ServiceHandler;

.field private mListenerKey:I

.field private final mListenerMap:Landroid/util/SparseArray;

.field private final mMapLock:Ljava/lang/Object;

.field mService:Landroid/net/nsd/INsdManager;

.field private final mServiceMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/net/nsd/NsdServiceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/nsd/INsdManager;)V
    .registers 5
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 216
    iput v1, p0, Landroid/net/nsd/NsdManager;->mListenerKey:I

    #@6
    .line 217
    new-instance v0, Landroid/util/SparseArray;

    #@8
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@b
    iput-object v0, p0, Landroid/net/nsd/NsdManager;->mListenerMap:Landroid/util/SparseArray;

    #@d
    .line 218
    new-instance v0, Landroid/util/SparseArray;

    #@f
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@12
    iput-object v0, p0, Landroid/net/nsd/NsdManager;->mServiceMap:Landroid/util/SparseArray;

    #@14
    .line 219
    new-instance v0, Ljava/lang/Object;

    #@16
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@19
    iput-object v0, p0, Landroid/net/nsd/NsdManager;->mMapLock:Ljava/lang/Object;

    #@1b
    .line 221
    new-instance v0, Lcom/android/internal/util/AsyncChannel;

    #@1d
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@20
    iput-object v0, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@22
    .line 223
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    #@24
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@27
    iput-object v0, p0, Landroid/net/nsd/NsdManager;->mConnected:Ljava/util/concurrent/CountDownLatch;

    #@29
    .line 234
    iput-object p2, p0, Landroid/net/nsd/NsdManager;->mService:Landroid/net/nsd/INsdManager;

    #@2b
    .line 235
    iput-object p1, p0, Landroid/net/nsd/NsdManager;->mContext:Landroid/content/Context;

    #@2d
    .line 236
    invoke-direct {p0}, Landroid/net/nsd/NsdManager;->init()V

    #@30
    .line 237
    return-void
.end method

.method static synthetic access$000(Landroid/net/nsd/NsdManager;I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1}, Landroid/net/nsd/NsdManager;->getListener(I)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/nsd/NsdManager;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/nsd/NsdManager;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mConnected:Ljava/util/concurrent/CountDownLatch;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1}, Landroid/net/nsd/NsdManager;->getNsdService(I)Landroid/net/nsd/NsdServiceInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Landroid/net/nsd/NsdManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 123
    invoke-direct {p0, p1}, Landroid/net/nsd/NsdManager;->removeListener(I)V

    #@3
    return-void
.end method

.method private getListener(I)Ljava/lang/Object;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 394
    if-nez p1, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 396
    :goto_3
    return-object v0

    #@4
    .line 395
    :cond_4
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mMapLock:Ljava/lang/Object;

    #@6
    monitor-enter v1

    #@7
    .line 396
    :try_start_7
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mListenerMap:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    monitor-exit v1

    #@e
    goto :goto_3

    #@f
    .line 397
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method private getListenerKey(Ljava/lang/Object;)I
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 415
    iget-object v2, p0, Landroid/net/nsd/NsdManager;->mMapLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 416
    :try_start_3
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mListenerMap:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    #@8
    move-result v0

    #@9
    .line 417
    .local v0, valueIndex:I
    const/4 v1, -0x1

    #@a
    if-eq v0, v1, :cond_14

    #@c
    .line 418
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mListenerMap:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@11
    move-result v1

    #@12
    monitor-exit v2

    #@13
    .line 421
    :goto_13
    return v1

    #@14
    .line 420
    :cond_14
    monitor-exit v2

    #@15
    .line 421
    const/4 v1, 0x0

    #@16
    goto :goto_13

    #@17
    .line 420
    .end local v0           #valueIndex:I
    :catchall_17
    move-exception v1

    #@18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v1
.end method

.method private getMessenger()Landroid/os/Messenger;
    .registers 3

    #@0
    .prologue
    .line 593
    :try_start_0
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mService:Landroid/net/nsd/INsdManager;

    #@2
    invoke-interface {v1}, Landroid/net/nsd/INsdManager;->getMessenger()Landroid/os/Messenger;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 595
    :goto_6
    return-object v1

    #@7
    .line 594
    :catch_7
    move-exception v0

    #@8
    .line 595
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method private getNsdService(I)Landroid/net/nsd/NsdServiceInfo;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 401
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mMapLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 402
    :try_start_3
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mServiceMap:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/net/nsd/NsdServiceInfo;

    #@b
    monitor-exit v1

    #@c
    return-object v0

    #@d
    .line 403
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method private init()V
    .registers 7

    #@0
    .prologue
    .line 429
    invoke-direct {p0}, Landroid/net/nsd/NsdManager;->getMessenger()Landroid/os/Messenger;

    #@3
    move-result-object v1

    #@4
    .line 430
    .local v1, messenger:Landroid/os/Messenger;
    if-nez v1, :cond_e

    #@6
    new-instance v3, Ljava/lang/RuntimeException;

    #@8
    const-string v4, "Failed to initialize"

    #@a
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v3

    #@e
    .line 431
    :cond_e
    new-instance v2, Landroid/os/HandlerThread;

    #@10
    const-string v3, "NsdManager"

    #@12
    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@15
    .line 432
    .local v2, t:Landroid/os/HandlerThread;
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    #@18
    .line 433
    new-instance v3, Landroid/net/nsd/NsdManager$ServiceHandler;

    #@1a
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@1d
    move-result-object v4

    #@1e
    invoke-direct {v3, p0, v4}, Landroid/net/nsd/NsdManager$ServiceHandler;-><init>(Landroid/net/nsd/NsdManager;Landroid/os/Looper;)V

    #@21
    iput-object v3, p0, Landroid/net/nsd/NsdManager;->mHandler:Landroid/net/nsd/NsdManager$ServiceHandler;

    #@23
    .line 434
    iget-object v3, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@25
    iget-object v4, p0, Landroid/net/nsd/NsdManager;->mContext:Landroid/content/Context;

    #@27
    iget-object v5, p0, Landroid/net/nsd/NsdManager;->mHandler:Landroid/net/nsd/NsdManager$ServiceHandler;

    #@29
    invoke-virtual {v3, v4, v5, v1}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@2c
    .line 436
    :try_start_2c
    iget-object v3, p0, Landroid/net/nsd/NsdManager;->mConnected:Ljava/util/concurrent/CountDownLatch;

    #@2e
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_31
    .catch Ljava/lang/InterruptedException; {:try_start_2c .. :try_end_31} :catch_32

    #@31
    .line 440
    :goto_31
    return-void

    #@32
    .line 437
    :catch_32
    move-exception v0

    #@33
    .line 438
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v3, "NsdManager"

    #@35
    const-string v4, "interrupted wait at init"

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_31
.end method

.method private putListener(Ljava/lang/Object;Landroid/net/nsd/NsdServiceInfo;)I
    .registers 6
    .parameter "listener"
    .parameter "s"

    #@0
    .prologue
    .line 381
    if-nez p1, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 390
    :goto_3
    return v0

    #@4
    .line 383
    :cond_4
    iget-object v2, p0, Landroid/net/nsd/NsdManager;->mMapLock:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 385
    :cond_7
    :try_start_7
    iget v0, p0, Landroid/net/nsd/NsdManager;->mListenerKey:I

    #@9
    add-int/lit8 v1, v0, 0x1

    #@b
    iput v1, p0, Landroid/net/nsd/NsdManager;->mListenerKey:I

    #@d
    .line 386
    .local v0, key:I
    if-eqz v0, :cond_7

    #@f
    .line 387
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mListenerMap:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 388
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mServiceMap:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v1, v0, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@19
    .line 389
    monitor-exit v2

    #@1a
    goto :goto_3

    #@1b
    .end local v0           #key:I
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_7 .. :try_end_1d} :catchall_1b

    #@1d
    throw v1
.end method

.method private removeListener(I)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 407
    if-nez p1, :cond_3

    #@2
    .line 412
    :goto_2
    return-void

    #@3
    .line 408
    :cond_3
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mMapLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 409
    :try_start_6
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mListenerMap:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@b
    .line 410
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mServiceMap:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@10
    .line 411
    monitor-exit v1

    #@11
    goto :goto_2

    #@12
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_6 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method


# virtual methods
.method public discoverServices(Ljava/lang/String;ILandroid/net/nsd/NsdManager$DiscoveryListener;)V
    .registers 9
    .parameter "serviceType"
    .parameter "protocolType"
    .parameter "listener"

    #@0
    .prologue
    .line 519
    if-nez p3, :cond_b

    #@2
    .line 520
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "listener cannot be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 522
    :cond_b
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_19

    #@11
    .line 523
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v2, "Service type cannot be empty"

    #@15
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v1

    #@19
    .line 526
    :cond_19
    const/4 v1, 0x1

    #@1a
    if-eq p2, v1, :cond_24

    #@1c
    .line 527
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1e
    const-string v2, "Unsupported protocol"

    #@20
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 530
    :cond_24
    new-instance v0, Landroid/net/nsd/NsdServiceInfo;

    #@26
    invoke-direct {v0}, Landroid/net/nsd/NsdServiceInfo;-><init>()V

    #@29
    .line 531
    .local v0, s:Landroid/net/nsd/NsdServiceInfo;
    invoke-virtual {v0, p1}, Landroid/net/nsd/NsdServiceInfo;->setServiceType(Ljava/lang/String;)V

    #@2c
    .line 532
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@2e
    const v2, 0x60001

    #@31
    const/4 v3, 0x0

    #@32
    invoke-direct {p0, p3, v0}, Landroid/net/nsd/NsdManager;->putListener(Ljava/lang/Object;Landroid/net/nsd/NsdServiceInfo;)I

    #@35
    move-result v4

    #@36
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@39
    .line 533
    return-void
.end method

.method public registerService(Landroid/net/nsd/NsdServiceInfo;ILandroid/net/nsd/NsdManager$RegistrationListener;)V
    .registers 8
    .parameter "serviceInfo"
    .parameter "protocolType"
    .parameter "listener"

    #@0
    .prologue
    .line 457
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 459
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "Service name or type cannot be empty"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 461
    :cond_1c
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getPort()I

    #@1f
    move-result v0

    #@20
    if-gtz v0, :cond_2a

    #@22
    .line 462
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v1, "Invalid port number"

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 464
    :cond_2a
    if-nez p3, :cond_35

    #@2c
    .line 465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2e
    const-string/jumbo v1, "listener cannot be null"

    #@31
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@34
    throw v0

    #@35
    .line 467
    :cond_35
    const/4 v0, 0x1

    #@36
    if-eq p2, v0, :cond_40

    #@38
    .line 468
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@3a
    const-string v1, "Unsupported protocol"

    #@3c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v0

    #@40
    .line 470
    :cond_40
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@42
    const v1, 0x60009

    #@45
    const/4 v2, 0x0

    #@46
    invoke-direct {p0, p3, p1}, Landroid/net/nsd/NsdManager;->putListener(Ljava/lang/Object;Landroid/net/nsd/NsdServiceInfo;)I

    #@49
    move-result v3

    #@4a
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@4d
    .line 472
    return-void
.end method

.method public resolveService(Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$ResolveListener;)V
    .registers 7
    .parameter "serviceInfo"
    .parameter "listener"

    #@0
    .prologue
    .line 567
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 569
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "Service name or type cannot be empty"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 571
    :cond_1c
    if-nez p2, :cond_27

    #@1e
    .line 572
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string/jumbo v1, "listener cannot be null"

    #@23
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 574
    :cond_27
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@29
    const v1, 0x60012

    #@2c
    const/4 v2, 0x0

    #@2d
    invoke-direct {p0, p2, p1}, Landroid/net/nsd/NsdManager;->putListener(Ljava/lang/Object;Landroid/net/nsd/NsdServiceInfo;)I

    #@30
    move-result v3

    #@31
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(IIILjava/lang/Object;)V

    #@34
    .line 576
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 581
    :try_start_0
    iget-object v0, p0, Landroid/net/nsd/NsdManager;->mService:Landroid/net/nsd/INsdManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/nsd/INsdManager;->setEnabled(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 583
    :goto_5
    return-void

    #@6
    .line 582
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public stopServiceDiscovery(Landroid/net/nsd/NsdManager$DiscoveryListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 548
    invoke-direct {p0, p1}, Landroid/net/nsd/NsdManager;->getListenerKey(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    .line 549
    .local v0, id:I
    if-nez v0, :cond_f

    #@6
    .line 550
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v2, "service discovery not active on listener"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 552
    :cond_f
    if-nez p1, :cond_1a

    #@11
    .line 553
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string/jumbo v2, "listener cannot be null"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 555
    :cond_1a
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@1c
    const v2, 0x60006

    #@1f
    const/4 v3, 0x0

    #@20
    invoke-virtual {v1, v2, v3, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@23
    .line 556
    return-void
.end method

.method public unregisterService(Landroid/net/nsd/NsdManager$RegistrationListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 484
    invoke-direct {p0, p1}, Landroid/net/nsd/NsdManager;->getListenerKey(Ljava/lang/Object;)I

    #@3
    move-result v0

    #@4
    .line 485
    .local v0, id:I
    if-nez v0, :cond_f

    #@6
    .line 486
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v2, "listener not registered"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 488
    :cond_f
    if-nez p1, :cond_1a

    #@11
    .line 489
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string/jumbo v2, "listener cannot be null"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 491
    :cond_1a
    iget-object v1, p0, Landroid/net/nsd/NsdManager;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    #@1c
    const v2, 0x6000c

    #@1f
    const/4 v3, 0x0

    #@20
    invoke-virtual {v1, v2, v3, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(III)V

    #@23
    .line 492
    return-void
.end method
