.class Landroid/net/nsd/NsdManager$ServiceHandler;
.super Landroid/os/Handler;
.source "NsdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/nsd/NsdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/nsd/NsdManager;


# direct methods
.method constructor <init>(Landroid/net/nsd/NsdManager;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 298
    iput-object p1, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@2
    .line 299
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 300
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "message"

    #@0
    .prologue
    .line 304
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@2
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@4
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$000(Landroid/net/nsd/NsdManager;I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    .line 305
    .local v0, listener:Ljava/lang/Object;
    const/4 v1, 0x1

    #@9
    .line 306
    .local v1, listenerRemove:Z
    iget v3, p1, Landroid/os/Message;->what:I

    #@b
    sparse-switch v3, :sswitch_data_108

    #@e
    .line 371
    const-string v3, "NsdManager"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "Ignored "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 374
    .end local v0           #listener:Ljava/lang/Object;
    :goto_26
    :sswitch_26
    if-eqz v1, :cond_2f

    #@28
    .line 375
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@2a
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@2c
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$400(Landroid/net/nsd/NsdManager;I)V

    #@2f
    .line 377
    :cond_2f
    return-void

    #@30
    .line 308
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_30
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@32
    invoke-static {v3}, Landroid/net/nsd/NsdManager;->access$100(Landroid/net/nsd/NsdManager;)Lcom/android/internal/util/AsyncChannel;

    #@35
    move-result-object v3

    #@36
    const v4, 0x11001

    #@39
    invoke-virtual {v3, v4}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    #@3c
    .line 309
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@3e
    invoke-static {v3}, Landroid/net/nsd/NsdManager;->access$200(Landroid/net/nsd/NsdManager;)Ljava/util/concurrent/CountDownLatch;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@45
    goto :goto_26

    #@46
    .line 315
    :sswitch_46
    const-string v3, "NsdManager"

    #@48
    const-string v4, "Channel lost"

    #@4a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_26

    #@4e
    .line 318
    :sswitch_4e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@50
    check-cast v3, Landroid/net/nsd/NsdServiceInfo;

    #@52
    invoke-virtual {v3}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    .line 319
    .local v2, s:Ljava/lang/String;
    check-cast v0, Landroid/net/nsd/NsdManager$DiscoveryListener;

    #@58
    .end local v0           #listener:Ljava/lang/Object;
    invoke-interface {v0, v2}, Landroid/net/nsd/NsdManager$DiscoveryListener;->onDiscoveryStarted(Ljava/lang/String;)V

    #@5b
    .line 321
    const/4 v1, 0x0

    #@5c
    .line 322
    goto :goto_26

    #@5d
    .line 324
    .end local v2           #s:Ljava/lang/String;
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_5d
    check-cast v0, Landroid/net/nsd/NsdManager$DiscoveryListener;

    #@5f
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@61
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@63
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {v3}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@6d
    invoke-interface {v0, v3, v4}, Landroid/net/nsd/NsdManager$DiscoveryListener;->onStartDiscoveryFailed(Ljava/lang/String;I)V

    #@70
    goto :goto_26

    #@71
    .line 328
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_71
    check-cast v0, Landroid/net/nsd/NsdManager$DiscoveryListener;

    #@73
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@75
    check-cast v3, Landroid/net/nsd/NsdServiceInfo;

    #@77
    invoke-interface {v0, v3}, Landroid/net/nsd/NsdManager$DiscoveryListener;->onServiceFound(Landroid/net/nsd/NsdServiceInfo;)V

    #@7a
    .line 330
    const/4 v1, 0x0

    #@7b
    .line 331
    goto :goto_26

    #@7c
    .line 333
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_7c
    check-cast v0, Landroid/net/nsd/NsdManager$DiscoveryListener;

    #@7e
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@80
    check-cast v3, Landroid/net/nsd/NsdServiceInfo;

    #@82
    invoke-interface {v0, v3}, Landroid/net/nsd/NsdManager$DiscoveryListener;->onServiceLost(Landroid/net/nsd/NsdServiceInfo;)V

    #@85
    .line 335
    const/4 v1, 0x0

    #@86
    .line 336
    goto :goto_26

    #@87
    .line 338
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_87
    check-cast v0, Landroid/net/nsd/NsdManager$DiscoveryListener;

    #@89
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@8b
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@8d
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@90
    move-result-object v3

    #@91
    invoke-virtual {v3}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@94
    move-result-object v3

    #@95
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@97
    invoke-interface {v0, v3, v4}, Landroid/net/nsd/NsdManager$DiscoveryListener;->onStopDiscoveryFailed(Ljava/lang/String;I)V

    #@9a
    goto :goto_26

    #@9b
    .line 342
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_9b
    check-cast v0, Landroid/net/nsd/NsdManager$DiscoveryListener;

    #@9d
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@9f
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@a1
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    invoke-interface {v0, v3}, Landroid/net/nsd/NsdManager$DiscoveryListener;->onDiscoveryStopped(Ljava/lang/String;)V

    #@ac
    goto/16 :goto_26

    #@ae
    .line 346
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_ae
    check-cast v0, Landroid/net/nsd/NsdManager$RegistrationListener;

    #@b0
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@b2
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@b4
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@b7
    move-result-object v3

    #@b8
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@ba
    invoke-interface {v0, v3, v4}, Landroid/net/nsd/NsdManager$RegistrationListener;->onRegistrationFailed(Landroid/net/nsd/NsdServiceInfo;I)V

    #@bd
    goto/16 :goto_26

    #@bf
    .line 350
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_bf
    check-cast v0, Landroid/net/nsd/NsdManager$RegistrationListener;

    #@c1
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c3
    check-cast v3, Landroid/net/nsd/NsdServiceInfo;

    #@c5
    invoke-interface {v0, v3}, Landroid/net/nsd/NsdManager$RegistrationListener;->onServiceRegistered(Landroid/net/nsd/NsdServiceInfo;)V

    #@c8
    .line 353
    const/4 v1, 0x0

    #@c9
    .line 354
    goto/16 :goto_26

    #@cb
    .line 356
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_cb
    check-cast v0, Landroid/net/nsd/NsdManager$RegistrationListener;

    #@cd
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@cf
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@d1
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@d4
    move-result-object v3

    #@d5
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@d7
    invoke-interface {v0, v3, v4}, Landroid/net/nsd/NsdManager$RegistrationListener;->onUnregistrationFailed(Landroid/net/nsd/NsdServiceInfo;I)V

    #@da
    goto/16 :goto_26

    #@dc
    .line 360
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_dc
    check-cast v0, Landroid/net/nsd/NsdManager$RegistrationListener;

    #@de
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@e0
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@e2
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@e5
    move-result-object v3

    #@e6
    invoke-interface {v0, v3}, Landroid/net/nsd/NsdManager$RegistrationListener;->onServiceUnregistered(Landroid/net/nsd/NsdServiceInfo;)V

    #@e9
    goto/16 :goto_26

    #@eb
    .line 364
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_eb
    check-cast v0, Landroid/net/nsd/NsdManager$ResolveListener;

    #@ed
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p0, Landroid/net/nsd/NsdManager$ServiceHandler;->this$0:Landroid/net/nsd/NsdManager;

    #@ef
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@f1
    invoke-static {v3, v4}, Landroid/net/nsd/NsdManager;->access$300(Landroid/net/nsd/NsdManager;I)Landroid/net/nsd/NsdServiceInfo;

    #@f4
    move-result-object v3

    #@f5
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@f7
    invoke-interface {v0, v3, v4}, Landroid/net/nsd/NsdManager$ResolveListener;->onResolveFailed(Landroid/net/nsd/NsdServiceInfo;I)V

    #@fa
    goto/16 :goto_26

    #@fc
    .line 368
    .restart local v0       #listener:Ljava/lang/Object;
    :sswitch_fc
    check-cast v0, Landroid/net/nsd/NsdManager$ResolveListener;

    #@fe
    .end local v0           #listener:Ljava/lang/Object;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@100
    check-cast v3, Landroid/net/nsd/NsdServiceInfo;

    #@102
    invoke-interface {v0, v3}, Landroid/net/nsd/NsdManager$ResolveListener;->onServiceResolved(Landroid/net/nsd/NsdServiceInfo;)V

    #@105
    goto/16 :goto_26

    #@107
    .line 306
    nop

    #@108
    :sswitch_data_108
    .sparse-switch
        0x11000 -> :sswitch_30
        0x11002 -> :sswitch_26
        0x11004 -> :sswitch_46
        0x60002 -> :sswitch_4e
        0x60003 -> :sswitch_5d
        0x60004 -> :sswitch_71
        0x60005 -> :sswitch_7c
        0x60007 -> :sswitch_87
        0x60008 -> :sswitch_9b
        0x6000a -> :sswitch_ae
        0x6000b -> :sswitch_bf
        0x6000d -> :sswitch_cb
        0x6000e -> :sswitch_dc
        0x60013 -> :sswitch_eb
        0x60014 -> :sswitch_fc
    .end sparse-switch
.end method
