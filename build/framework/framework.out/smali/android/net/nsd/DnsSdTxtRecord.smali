.class public Landroid/net/nsd/DnsSdTxtRecord;
.super Ljava/lang/Object;
.source "DnsSdTxtRecord.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/nsd/DnsSdTxtRecord;",
            ">;"
        }
    .end annotation
.end field

.field private static final mSeperator:B = 0x3dt


# instance fields
.field private mData:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 313
    new-instance v0, Landroid/net/nsd/DnsSdTxtRecord$1;

    #@2
    invoke-direct {v0}, Landroid/net/nsd/DnsSdTxtRecord$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/nsd/DnsSdTxtRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    const/4 v0, 0x0

    #@4
    new-array v0, v0, [B

    #@6
    iput-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@8
    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/net/nsd/DnsSdTxtRecord;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 59
    if-eqz p1, :cond_15

    #@5
    iget-object v0, p1, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@7
    if-eqz v0, :cond_15

    #@9
    .line 60
    iget-object v0, p1, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@b
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, [B

    #@11
    check-cast v0, [B

    #@13
    iput-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@15
    .line 62
    :cond_15
    return-void
.end method

.method public constructor <init>([B)V
    .registers 3
    .parameter "data"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, [B

    #@9
    check-cast v0, [B

    #@b
    iput-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@d
    .line 55
    return-void
.end method

.method static synthetic access$000(Landroid/net/nsd/DnsSdTxtRecord;)[B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2
    return-object v0
.end method

.method private getKey(I)Ljava/lang/String;
    .registers 9
    .parameter "index"

    #@0
    .prologue
    .line 195
    const/4 v2, 0x0

    #@1
    .line 197
    .local v2, avStart:I
    const/4 v3, 0x0

    #@2
    .local v3, i:I
    :goto_2
    if-ge v3, p1, :cond_13

    #@4
    iget-object v4, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@6
    array-length v4, v4

    #@7
    if-ge v2, v4, :cond_13

    #@9
    .line 198
    iget-object v4, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@b
    aget-byte v4, v4, v2

    #@d
    add-int/lit8 v4, v4, 0x1

    #@f
    add-int/2addr v2, v4

    #@10
    .line 197
    add-int/lit8 v3, v3, 0x1

    #@12
    goto :goto_2

    #@13
    .line 201
    :cond_13
    iget-object v4, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@15
    array-length v4, v4

    #@16
    if-ge v2, v4, :cond_39

    #@18
    .line 202
    iget-object v4, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@1a
    aget-byte v1, v4, v2

    #@1c
    .line 203
    .local v1, avLen:I
    const/4 v0, 0x0

    #@1d
    .line 205
    .local v0, aLen:I
    const/4 v0, 0x0

    #@1e
    :goto_1e
    if-ge v0, v1, :cond_2c

    #@20
    .line 206
    iget-object v4, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@22
    add-int v5, v2, v0

    #@24
    add-int/lit8 v5, v5, 0x1

    #@26
    aget-byte v4, v4, v5

    #@28
    const/16 v5, 0x3d

    #@2a
    if-ne v4, v5, :cond_36

    #@2c
    .line 208
    :cond_2c
    new-instance v4, Ljava/lang/String;

    #@2e
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@30
    add-int/lit8 v6, v2, 0x1

    #@32
    invoke-direct {v4, v5, v6, v0}, Ljava/lang/String;-><init>([BII)V

    #@35
    .line 210
    .end local v0           #aLen:I
    .end local v1           #avLen:I
    :goto_35
    return-object v4

    #@36
    .line 205
    .restart local v0       #aLen:I
    .restart local v1       #avLen:I
    :cond_36
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_1e

    #@39
    .line 210
    .end local v0           #aLen:I
    .end local v1           #avLen:I
    :cond_39
    const/4 v4, 0x0

    #@3a
    goto :goto_35
.end method

.method private getValue(I)[B
    .registers 11
    .parameter "index"

    #@0
    .prologue
    .line 219
    const/4 v2, 0x0

    #@1
    .line 220
    .local v2, avStart:I
    const/4 v4, 0x0

    #@2
    .line 222
    .local v4, value:[B
    const/4 v3, 0x0

    #@3
    .local v3, i:I
    :goto_3
    if-ge v3, p1, :cond_14

    #@5
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@7
    array-length v5, v5

    #@8
    if-ge v2, v5, :cond_14

    #@a
    .line 223
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@c
    aget-byte v5, v5, v2

    #@e
    add-int/lit8 v5, v5, 0x1

    #@10
    add-int/2addr v2, v5

    #@11
    .line 222
    add-int/lit8 v3, v3, 0x1

    #@13
    goto :goto_3

    #@14
    .line 226
    :cond_14
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@16
    array-length v5, v5

    #@17
    if-ge v2, v5, :cond_41

    #@19
    .line 227
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@1b
    aget-byte v1, v5, v2

    #@1d
    .line 228
    .local v1, avLen:I
    const/4 v0, 0x0

    #@1e
    .line 230
    .local v0, aLen:I
    const/4 v0, 0x0

    #@1f
    :goto_1f
    if-ge v0, v1, :cond_41

    #@21
    .line 231
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@23
    add-int v6, v2, v0

    #@25
    add-int/lit8 v6, v6, 0x1

    #@27
    aget-byte v5, v5, v6

    #@29
    const/16 v6, 0x3d

    #@2b
    if-ne v5, v6, :cond_42

    #@2d
    .line 232
    sub-int v5, v1, v0

    #@2f
    add-int/lit8 v5, v5, -0x1

    #@31
    new-array v4, v5, [B

    #@33
    .line 233
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@35
    add-int v6, v2, v0

    #@37
    add-int/lit8 v6, v6, 0x2

    #@39
    const/4 v7, 0x0

    #@3a
    sub-int v8, v1, v0

    #@3c
    add-int/lit8 v8, v8, -0x1

    #@3e
    invoke-static {v5, v6, v4, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@41
    .line 238
    .end local v0           #aLen:I
    .end local v1           #avLen:I
    :cond_41
    return-object v4

    #@42
    .line 230
    .restart local v0       #aLen:I
    .restart local v1       #avLen:I
    :cond_42
    add-int/lit8 v0, v0, 0x1

    #@44
    goto :goto_1f
.end method

.method private getValue(Ljava/lang/String;)[B
    .registers 5
    .parameter "forKey"

    #@0
    .prologue
    .line 247
    const/4 v1, 0x0

    #@1
    .line 250
    .local v1, s:Ljava/lang/String;
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    invoke-direct {p0, v0}, Landroid/net/nsd/DnsSdTxtRecord;->getKey(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_16

    #@8
    .line 251
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_13

    #@e
    .line 252
    invoke-direct {p0, v0}, Landroid/net/nsd/DnsSdTxtRecord;->getValue(I)[B

    #@11
    move-result-object v2

    #@12
    .line 256
    :goto_12
    return-object v2

    #@13
    .line 250
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_2

    #@16
    .line 256
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_12
.end method

.method private getValueAsString(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 242
    invoke-direct {p0, p1}, Landroid/net/nsd/DnsSdTxtRecord;->getValue(I)[B

    #@3
    move-result-object v0

    #@4
    .line 243
    .local v0, value:[B
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Ljava/lang/String;

    #@8
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method private insert([B[BI)V
    .registers 15
    .parameter "keyBytes"
    .parameter "value"
    .parameter "index"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 169
    iget-object v4, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@3
    .line 170
    .local v4, oldBytes:[B
    if-eqz p2, :cond_1b

    #@5
    array-length v6, p2

    #@6
    .line 171
    .local v6, valLen:I
    :goto_6
    const/4 v2, 0x0

    #@7
    .line 174
    .local v2, insertion:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, p3, :cond_1d

    #@a
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@c
    array-length v8, v8

    #@d
    if-ge v2, v8, :cond_1d

    #@f
    .line 175
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@11
    aget-byte v8, v8, v2

    #@13
    add-int/lit8 v8, v8, 0x1

    #@15
    and-int/lit16 v8, v8, 0xff

    #@17
    add-int/2addr v2, v8

    #@18
    .line 174
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_8

    #@1b
    .end local v1           #i:I
    .end local v2           #insertion:I
    .end local v6           #valLen:I
    :cond_1b
    move v6, v7

    #@1c
    .line 170
    goto :goto_6

    #@1d
    .line 178
    .restart local v1       #i:I
    .restart local v2       #insertion:I
    .restart local v6       #valLen:I
    :cond_1d
    array-length v8, p1

    #@1e
    add-int v9, v8, v6

    #@20
    if-eqz p2, :cond_5f

    #@22
    const/4 v8, 0x1

    #@23
    :goto_23
    add-int v0, v9, v8

    #@25
    .line 179
    .local v0, avLen:I
    array-length v8, v4

    #@26
    add-int/2addr v8, v0

    #@27
    add-int/lit8 v3, v8, 0x1

    #@29
    .line 181
    .local v3, newLen:I
    new-array v8, v3, [B

    #@2b
    iput-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2d
    .line 182
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2f
    invoke-static {v4, v7, v8, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@32
    .line 183
    array-length v8, v4

    #@33
    sub-int v5, v8, v2

    #@35
    .line 184
    .local v5, secondHalfLen:I
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@37
    sub-int v9, v3, v5

    #@39
    invoke-static {v4, v2, v8, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3c
    .line 185
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@3e
    int-to-byte v9, v0

    #@3f
    aput-byte v9, v8, v2

    #@41
    .line 186
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@43
    add-int/lit8 v9, v2, 0x1

    #@45
    array-length v10, p1

    #@46
    invoke-static {p1, v7, v8, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@49
    .line 187
    if-eqz p2, :cond_5e

    #@4b
    .line 188
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@4d
    add-int/lit8 v9, v2, 0x1

    #@4f
    array-length v10, p1

    #@50
    add-int/2addr v9, v10

    #@51
    const/16 v10, 0x3d

    #@53
    aput-byte v10, v8, v9

    #@55
    .line 189
    iget-object v8, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@57
    array-length v9, p1

    #@58
    add-int/2addr v9, v2

    #@59
    add-int/lit8 v9, v9, 0x2

    #@5b
    invoke-static {p2, v7, v8, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5e
    .line 191
    :cond_5e
    return-void

    #@5f
    .end local v0           #avLen:I
    .end local v3           #newLen:I
    .end local v5           #secondHalfLen:I
    :cond_5f
    move v8, v7

    #@60
    .line 178
    goto :goto_23
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 151
    const/4 v1, 0x0

    #@1
    .line 152
    .local v1, s:Ljava/lang/String;
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    invoke-direct {p0, v0}, Landroid/net/nsd/DnsSdTxtRecord;->getKey(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_13

    #@8
    .line 153
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_10

    #@e
    const/4 v2, 0x1

    #@f
    .line 155
    :goto_f
    return v2

    #@10
    .line 152
    :cond_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_2

    #@13
    .line 155
    :cond_13
    const/4 v2, 0x0

    #@14
    goto :goto_f
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 304
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    #@0
    .prologue
    .line 286
    if-ne p1, p0, :cond_4

    #@2
    .line 287
    const/4 v1, 0x1

    #@3
    .line 294
    :goto_3
    return v1

    #@4
    .line 289
    :cond_4
    instance-of v1, p1, Landroid/net/nsd/DnsSdTxtRecord;

    #@6
    if-nez v1, :cond_a

    #@8
    .line 290
    const/4 v1, 0x0

    #@9
    goto :goto_3

    #@a
    :cond_a
    move-object v0, p1

    #@b
    .line 293
    check-cast v0, Landroid/net/nsd/DnsSdTxtRecord;

    #@d
    .line 294
    .local v0, record:Landroid/net/nsd/DnsSdTxtRecord;
    iget-object v1, v0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@f
    iget-object v2, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@11
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    #@14
    move-result v1

    #@15
    goto :goto_3
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 113
    invoke-direct {p0, p1}, Landroid/net/nsd/DnsSdTxtRecord;->getValue(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    .line 114
    .local v0, val:[B
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Ljava/lang/String;

    #@8
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public getRawData()[B
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [B

    #@8
    check-cast v0, [B

    #@a
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public keyCount()I
    .registers 4

    #@0
    .prologue
    .line 142
    const/4 v0, 0x0

    #@1
    .line 143
    .local v0, count:I
    const/4 v1, 0x0

    #@2
    .local v1, nextKey:I
    :goto_2
    iget-object v2, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@4
    array-length v2, v2

    #@5
    if-ge v1, v2, :cond_13

    #@7
    .line 144
    iget-object v2, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@9
    aget-byte v2, v2, v1

    #@b
    add-int/lit8 v2, v2, 0x1

    #@d
    and-int/lit16 v2, v2, 0xff

    #@f
    add-int/2addr v1, v2

    #@10
    .line 143
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_2

    #@13
    .line 146
    :cond_13
    return v0
.end method

.method public remove(Ljava/lang/String;)I
    .registers 11
    .parameter "key"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 119
    const/4 v1, 0x0

    #@2
    .line 121
    .local v1, avStart:I
    const/4 v2, 0x0

    #@3
    .local v2, i:I
    :goto_3
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@5
    array-length v5, v5

    #@6
    if-ge v1, v5, :cond_60

    #@8
    .line 122
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@a
    aget-byte v0, v5, v1

    #@c
    .line 123
    .local v0, avLen:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@f
    move-result v5

    #@10
    if-gt v5, v0, :cond_58

    #@12
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@15
    move-result v5

    #@16
    if-eq v5, v0, :cond_27

    #@18
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@1a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1d
    move-result v6

    #@1e
    add-int/2addr v6, v1

    #@1f
    add-int/lit8 v6, v6, 0x1

    #@21
    aget-byte v5, v5, v6

    #@23
    const/16 v6, 0x3d

    #@25
    if-ne v5, v6, :cond_58

    #@27
    .line 125
    :cond_27
    new-instance v4, Ljava/lang/String;

    #@29
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2b
    add-int/lit8 v6, v1, 0x1

    #@2d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@30
    move-result v7

    #@31
    invoke-direct {v4, v5, v6, v7}, Ljava/lang/String;-><init>([BII)V

    #@34
    .line 126
    .local v4, s:Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@37
    move-result v5

    #@38
    if-nez v5, :cond_58

    #@3a
    .line 127
    iget-object v3, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@3c
    .line 128
    .local v3, oldBytes:[B
    array-length v5, v3

    #@3d
    sub-int/2addr v5, v0

    #@3e
    add-int/lit8 v5, v5, -0x1

    #@40
    new-array v5, v5, [B

    #@42
    iput-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@44
    .line 129
    iget-object v5, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@46
    invoke-static {v3, v8, v5, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@49
    .line 130
    add-int v5, v1, v0

    #@4b
    add-int/lit8 v5, v5, 0x1

    #@4d
    iget-object v6, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@4f
    array-length v7, v3

    #@50
    sub-int/2addr v7, v1

    #@51
    sub-int/2addr v7, v0

    #@52
    add-int/lit8 v7, v7, -0x1

    #@54
    invoke-static {v3, v5, v6, v1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@57
    .line 137
    .end local v0           #avLen:I
    .end local v2           #i:I
    .end local v3           #oldBytes:[B
    .end local v4           #s:Ljava/lang/String;
    :goto_57
    return v2

    #@58
    .line 135
    .restart local v0       #avLen:I
    .restart local v2       #i:I
    :cond_58
    add-int/lit8 v5, v0, 0x1

    #@5a
    and-int/lit16 v5, v5, 0xff

    #@5c
    add-int/2addr v1, v5

    #@5d
    .line 121
    add-int/lit8 v2, v2, 0x1

    #@5f
    goto :goto_3

    #@60
    .line 137
    .end local v0           #avLen:I
    :cond_60
    const/4 v2, -0x1

    #@61
    goto :goto_57
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 74
    if-eqz p2, :cond_1f

    #@2
    .line 75
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@5
    move-result-object v4

    #@6
    .line 76
    .local v4, valBytes:[B
    array-length v5, v4

    #@7
    .line 83
    .local v5, valLen:I
    :goto_7
    :try_start_7
    const-string v6, "US-ASCII"

    #@9
    invoke-virtual {p1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_c} :catch_22

    #@c
    move-result-object v3

    #@d
    .line 89
    .local v3, keyBytes:[B
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    array-length v6, v3

    #@f
    if-ge v2, v6, :cond_2f

    #@11
    .line 90
    aget-byte v6, v3, v2

    #@13
    const/16 v7, 0x3d

    #@15
    if-ne v6, v7, :cond_2c

    #@17
    .line 91
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v7, "= is not a valid character in key"

    #@1b
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v6

    #@1f
    .line 78
    .end local v2           #i:I
    .end local v3           #keyBytes:[B
    .end local v4           #valBytes:[B
    .end local v5           #valLen:I
    :cond_1f
    const/4 v4, 0x0

    #@20
    .line 79
    .restart local v4       #valBytes:[B
    const/4 v5, 0x0

    #@21
    .restart local v5       #valLen:I
    goto :goto_7

    #@22
    .line 85
    :catch_22
    move-exception v1

    #@23
    .line 86
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@25
    const-string/jumbo v7, "key should be US-ASCII"

    #@28
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v6

    #@2c
    .line 89
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v2       #i:I
    .restart local v3       #keyBytes:[B
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_e

    #@2f
    .line 95
    :cond_2f
    array-length v6, v3

    #@30
    add-int/2addr v6, v5

    #@31
    const/16 v7, 0xff

    #@33
    if-lt v6, v7, :cond_3d

    #@35
    .line 96
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@37
    const-string v7, "Key and Value length cannot exceed 255 bytes"

    #@39
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v6

    #@3d
    .line 99
    :cond_3d
    invoke-virtual {p0, p1}, Landroid/net/nsd/DnsSdTxtRecord;->remove(Ljava/lang/String;)I

    #@40
    move-result v0

    #@41
    .line 100
    .local v0, currentLoc:I
    const/4 v6, -0x1

    #@42
    if-ne v0, v6, :cond_48

    #@44
    .line 101
    invoke-virtual {p0}, Landroid/net/nsd/DnsSdTxtRecord;->keyCount()I

    #@47
    move-result v0

    #@48
    .line 103
    :cond_48
    invoke-direct {p0, v3, v4, v0}, Landroid/net/nsd/DnsSdTxtRecord;->insert([B[BI)V

    #@4b
    .line 104
    return-void
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 160
    iget-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2
    array-length v0, v0

    #@3
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 267
    const/4 v3, 0x0

    #@1
    .line 269
    .local v3, result:Ljava/lang/String;
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    invoke-direct {p0, v2}, Landroid/net/nsd/DnsSdTxtRecord;->getKey(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .local v0, a:Ljava/lang/String;
    if-eqz v0, :cond_73

    #@8
    .line 270
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v6, "{"

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 271
    .local v1, av:Ljava/lang/String;
    invoke-direct {p0, v2}, Landroid/net/nsd/DnsSdTxtRecord;->getValueAsString(I)Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    .line 272
    .local v4, val:Ljava/lang/String;
    if-eqz v4, :cond_46

    #@22
    .line 273
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    const-string v6, "="

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    const-string/jumbo v6, "}"

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    .line 276
    :goto_40
    if-nez v3, :cond_5b

    #@42
    .line 277
    move-object v3, v1

    #@43
    .line 269
    :goto_43
    add-int/lit8 v2, v2, 0x1

    #@45
    goto :goto_2

    #@46
    .line 275
    :cond_46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    const-string/jumbo v6, "}"

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    goto :goto_40

    #@5b
    .line 279
    :cond_5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    const-string v6, ", "

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    goto :goto_43

    #@73
    .line 281
    .end local v1           #av:Ljava/lang/String;
    .end local v4           #val:Ljava/lang/String;
    :cond_73
    if-eqz v3, :cond_76

    #@75
    .end local v3           #result:Ljava/lang/String;
    :goto_75
    return-object v3

    #@76
    .restart local v3       #result:Ljava/lang/String;
    :cond_76
    const-string v3, ""

    #@78
    goto :goto_75
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 309
    iget-object v0, p0, Landroid/net/nsd/DnsSdTxtRecord;->mData:[B

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@5
    .line 310
    return-void
.end method
