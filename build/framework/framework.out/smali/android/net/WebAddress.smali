.class public Landroid/net/WebAddress;
.super Ljava/lang/Object;
.source "WebAddress.java"


# static fields
.field static final MATCH_GROUP_AUTHORITY:I = 0x2

.field static final MATCH_GROUP_HOST:I = 0x3

.field static final MATCH_GROUP_PATH:I = 0x5

.field static final MATCH_GROUP_PORT:I = 0x4

.field static final MATCH_GROUP_SCHEME:I = 0x1

.field static sAddressPattern:Ljava/util/regex/Pattern;


# instance fields
.field private mAuthInfo:Ljava/lang/String;

.field private mHost:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mPort:I

.field private mScheme:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 54
    const-string v0, "(?:(http|https|file)\\:\\/\\/)?(?:([-A-Za-z0-9$_.+!*\'(),;?&=]+(?:\\:[-A-Za-z0-9$_.+!*\'(),;?&=]+)?)@)?([a-zA-Z0-9\u00a0-\ud7ff\uf900-\ufdcf\ufdf0-\uffef%_-][a-zA-Z0-9\u00a0-\ud7ff\uf900-\ufdcf\ufdf0-\uffef%_\\.-]*|\\[[0-9a-fA-F:\\.]+\\])?(?:\\:([0-9]*))?(\\/?[^#]*)?.*"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Landroid/net/WebAddress;->sAddressPattern:Ljava/util/regex/Pattern;

    #@9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 9
    .parameter "address"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/ParseException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v6, 0x1bb

    #@2
    const/4 v5, -0x1

    #@3
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 64
    if-nez p1, :cond_e

    #@8
    .line 65
    new-instance v3, Ljava/lang/NullPointerException;

    #@a
    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    #@d
    throw v3

    #@e
    .line 70
    :cond_e
    const-string v3, ""

    #@10
    iput-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@12
    .line 71
    const-string v3, ""

    #@14
    iput-object v3, p0, Landroid/net/WebAddress;->mHost:Ljava/lang/String;

    #@16
    .line 72
    iput v5, p0, Landroid/net/WebAddress;->mPort:I

    #@18
    .line 73
    const-string v3, "/"

    #@1a
    iput-object v3, p0, Landroid/net/WebAddress;->mPath:Ljava/lang/String;

    #@1c
    .line 74
    const-string v3, ""

    #@1e
    iput-object v3, p0, Landroid/net/WebAddress;->mAuthInfo:Ljava/lang/String;

    #@20
    .line 76
    sget-object v3, Landroid/net/WebAddress;->sAddressPattern:Ljava/util/regex/Pattern;

    #@22
    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@25
    move-result-object v1

    #@26
    .line 78
    .local v1, m:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_b6

    #@2c
    .line 79
    const/4 v3, 0x1

    #@2d
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 80
    .local v2, t:Ljava/lang/String;
    if-eqz v2, :cond_39

    #@33
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    iput-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@39
    .line 81
    :cond_39
    const/4 v3, 0x2

    #@3a
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    .line 82
    if-eqz v2, :cond_42

    #@40
    iput-object v2, p0, Landroid/net/WebAddress;->mAuthInfo:Ljava/lang/String;

    #@42
    .line 83
    :cond_42
    const/4 v3, 0x3

    #@43
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    .line 84
    if-eqz v2, :cond_4b

    #@49
    iput-object v2, p0, Landroid/net/WebAddress;->mHost:Ljava/lang/String;

    #@4b
    .line 85
    :cond_4b
    const/4 v3, 0x4

    #@4c
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    .line 86
    if-eqz v2, :cond_5e

    #@52
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@55
    move-result v3

    #@56
    if-lez v3, :cond_5e

    #@58
    .line 89
    :try_start_58
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5b
    move-result v3

    #@5c
    iput v3, p0, Landroid/net/WebAddress;->mPort:I
    :try_end_5e
    .catch Ljava/lang/NumberFormatException; {:try_start_58 .. :try_end_5e} :catch_97

    #@5e
    .line 94
    :cond_5e
    const/4 v3, 0x5

    #@5f
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    .line 95
    if-eqz v2, :cond_76

    #@65
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@68
    move-result v3

    #@69
    if-lez v3, :cond_76

    #@6b
    .line 98
    const/4 v3, 0x0

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    #@6f
    move-result v3

    #@70
    const/16 v4, 0x2f

    #@72
    if-ne v3, v4, :cond_a0

    #@74
    .line 99
    iput-object v2, p0, Landroid/net/WebAddress;->mPath:Ljava/lang/String;

    #@76
    .line 112
    :cond_76
    :goto_76
    iget v3, p0, Landroid/net/WebAddress;->mPort:I

    #@78
    if-ne v3, v6, :cond_be

    #@7a
    iget-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@7c
    const-string v4, ""

    #@7e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v3

    #@82
    if-eqz v3, :cond_be

    #@84
    .line 113
    const-string v3, "https"

    #@86
    iput-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@88
    .line 120
    :cond_88
    :goto_88
    iget-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@8a
    const-string v4, ""

    #@8c
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v3

    #@90
    if-eqz v3, :cond_96

    #@92
    const-string v3, "http"

    #@94
    iput-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@96
    .line 121
    :cond_96
    return-void

    #@97
    .line 90
    :catch_97
    move-exception v0

    #@98
    .line 91
    .local v0, ex:Ljava/lang/NumberFormatException;
    new-instance v3, Landroid/net/ParseException;

    #@9a
    const-string v4, "Bad port"

    #@9c
    invoke-direct {v3, v4}, Landroid/net/ParseException;-><init>(Ljava/lang/String;)V

    #@9f
    throw v3

    #@a0
    .line 101
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    :cond_a0
    new-instance v3, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v4, "/"

    #@a7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v3

    #@b3
    iput-object v3, p0, Landroid/net/WebAddress;->mPath:Ljava/lang/String;

    #@b5
    goto :goto_76

    #@b6
    .line 107
    .end local v2           #t:Ljava/lang/String;
    :cond_b6
    new-instance v3, Landroid/net/ParseException;

    #@b8
    const-string v4, "Bad address"

    #@ba
    invoke-direct {v3, v4}, Landroid/net/ParseException;-><init>(Ljava/lang/String;)V

    #@bd
    throw v3

    #@be
    .line 114
    .restart local v2       #t:Ljava/lang/String;
    :cond_be
    iget v3, p0, Landroid/net/WebAddress;->mPort:I

    #@c0
    if-ne v3, v5, :cond_88

    #@c2
    .line 115
    iget-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@c4
    const-string v4, "https"

    #@c6
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v3

    #@ca
    if-eqz v3, :cond_cf

    #@cc
    .line 116
    iput v6, p0, Landroid/net/WebAddress;->mPort:I

    #@ce
    goto :goto_88

    #@cf
    .line 118
    :cond_cf
    const/16 v3, 0x50

    #@d1
    iput v3, p0, Landroid/net/WebAddress;->mPort:I

    #@d3
    goto :goto_88
.end method


# virtual methods
.method public getAuthInfo()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Landroid/net/WebAddress;->mAuthInfo:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/net/WebAddress;->mHost:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/net/WebAddress;->mPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPort()I
    .registers 2

    #@0
    .prologue
    .line 159
    iget v0, p0, Landroid/net/WebAddress;->mPort:I

    #@2
    return v0
.end method

.method public getScheme()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public setAuthInfo(Ljava/lang/String;)V
    .registers 2
    .parameter "authInfo"

    #@0
    .prologue
    .line 171
    iput-object p1, p0, Landroid/net/WebAddress;->mAuthInfo:Ljava/lang/String;

    #@2
    .line 172
    return-void
.end method

.method public setHost(Ljava/lang/String;)V
    .registers 2
    .parameter "host"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Landroid/net/WebAddress;->mHost:Ljava/lang/String;

    #@2
    .line 148
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .registers 2
    .parameter "path"

    #@0
    .prologue
    .line 163
    iput-object p1, p0, Landroid/net/WebAddress;->mPath:Ljava/lang/String;

    #@2
    .line 164
    return-void
.end method

.method public setPort(I)V
    .registers 2
    .parameter "port"

    #@0
    .prologue
    .line 155
    iput p1, p0, Landroid/net/WebAddress;->mPort:I

    #@2
    .line 156
    return-void
.end method

.method public setScheme(Ljava/lang/String;)V
    .registers 2
    .parameter "scheme"

    #@0
    .prologue
    .line 139
    iput-object p1, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@2
    .line 140
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 125
    const-string v1, ""

    #@2
    .line 126
    .local v1, port:Ljava/lang/String;
    iget v2, p0, Landroid/net/WebAddress;->mPort:I

    #@4
    const/16 v3, 0x1bb

    #@6
    if-eq v2, v3, :cond_12

    #@8
    iget-object v2, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@a
    const-string v3, "https"

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_22

    #@12
    :cond_12
    iget v2, p0, Landroid/net/WebAddress;->mPort:I

    #@14
    const/16 v3, 0x50

    #@16
    if-eq v2, v3, :cond_3b

    #@18
    iget-object v2, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@1a
    const-string v3, "http"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_3b

    #@22
    .line 128
    :cond_22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, ":"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget v3, p0, Landroid/net/WebAddress;->mPort:I

    #@2f
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    .line 130
    :cond_3b
    const-string v0, ""

    #@3d
    .line 131
    .local v0, authInfo:Ljava/lang/String;
    iget-object v2, p0, Landroid/net/WebAddress;->mAuthInfo:Ljava/lang/String;

    #@3f
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@42
    move-result v2

    #@43
    if-lez v2, :cond_5a

    #@45
    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    iget-object v3, p0, Landroid/net/WebAddress;->mAuthInfo:Ljava/lang/String;

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    const-string v3, "@"

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    .line 135
    :cond_5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    iget-object v3, p0, Landroid/net/WebAddress;->mScheme:Ljava/lang/String;

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    const-string v3, "://"

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    iget-object v3, p0, Landroid/net/WebAddress;->mHost:Ljava/lang/String;

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    iget-object v3, p0, Landroid/net/WebAddress;->mPath:Ljava/lang/String;

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    return-object v2
.end method
