.class public Landroid/net/DhcpInfoInternal;
.super Ljava/lang/Object;
.source "DhcpInfoInternal.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DhcpInfoInternal"


# instance fields
.field public dns1:Ljava/lang/String;

.field public dns2:Ljava/lang/String;

.field public domainName:Ljava/lang/String;

.field public ipAddress:Ljava/lang/String;

.field public leaseDuration:I

.field private mRoutes:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field public prefixLength:I

.field public serverAddress:Ljava/lang/String;

.field public vendorInfo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@a
    .line 55
    return-void
.end method

.method private convertToInt(Ljava/lang/String;)I
    .registers 4
    .parameter "addr"

    #@0
    .prologue
    .line 66
    if-eqz p1, :cond_10

    #@2
    .line 68
    :try_start_2
    invoke-static {p1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@5
    move-result-object v0

    #@6
    .line 69
    .local v0, inetAddress:Ljava/net/InetAddress;
    instance-of v1, v0, Ljava/net/Inet4Address;

    #@8
    if-eqz v1, :cond_10

    #@a
    .line 70
    invoke-static {v0}, Landroid/net/NetworkUtils;->inetAddressToInt(Ljava/net/InetAddress;)I
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 74
    .end local v0           #inetAddress:Ljava/net/InetAddress;
    :goto_e
    return v1

    #@f
    .line 72
    :catch_f
    move-exception v1

    #@10
    .line 74
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method


# virtual methods
.method public addRoute(Landroid/net/RouteInfo;)V
    .registers 3
    .parameter "routeInfo"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@5
    .line 59
    return-void
.end method

.method public getRoutes()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public hasMeteredHint()Z
    .registers 3

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/net/DhcpInfoInternal;->vendorInfo:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 173
    iget-object v0, p0, Landroid/net/DhcpInfoInternal;->vendorInfo:Ljava/lang/String;

    #@6
    const-string v1, "ANDROID_METERED"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@b
    move-result v0

    #@c
    .line 175
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public makeDhcpInfo()Landroid/net/DhcpInfo;
    .registers 6

    #@0
    .prologue
    .line 78
    new-instance v2, Landroid/net/DhcpInfo;

    #@2
    invoke-direct {v2}, Landroid/net/DhcpInfo;-><init>()V

    #@5
    .line 79
    .local v2, info:Landroid/net/DhcpInfo;
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@7
    invoke-direct {p0, v4}, Landroid/net/DhcpInfoInternal;->convertToInt(Ljava/lang/String;)I

    #@a
    move-result v4

    #@b
    iput v4, v2, Landroid/net/DhcpInfo;->ipAddress:I

    #@d
    .line 80
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@f
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :cond_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_33

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/net/RouteInfo;

    #@1f
    .line 81
    .local v3, route:Landroid/net/RouteInfo;
    invoke-virtual {v3}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_13

    #@25
    .line 82
    invoke-virtual {v3}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-direct {p0, v4}, Landroid/net/DhcpInfoInternal;->convertToInt(Ljava/lang/String;)I

    #@30
    move-result v4

    #@31
    iput v4, v2, Landroid/net/DhcpInfo;->gateway:I

    #@33
    .line 87
    .end local v3           #route:Landroid/net/RouteInfo;
    :cond_33
    :try_start_33
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@35
    invoke-static {v4}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@38
    move-result-object v1

    #@39
    .line 88
    .local v1, inetAddress:Ljava/net/InetAddress;
    iget v4, p0, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@3b
    invoke-static {v4}, Landroid/net/NetworkUtils;->prefixLengthToNetmaskInt(I)I

    #@3e
    move-result v4

    #@3f
    iput v4, v2, Landroid/net/DhcpInfo;->netmask:I
    :try_end_41
    .catch Ljava/lang/IllegalArgumentException; {:try_start_33 .. :try_end_41} :catch_62

    #@41
    .line 90
    .end local v1           #inetAddress:Ljava/net/InetAddress;
    :goto_41
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@43
    invoke-direct {p0, v4}, Landroid/net/DhcpInfoInternal;->convertToInt(Ljava/lang/String;)I

    #@46
    move-result v4

    #@47
    iput v4, v2, Landroid/net/DhcpInfo;->dns1:I

    #@49
    .line 91
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@4b
    invoke-direct {p0, v4}, Landroid/net/DhcpInfoInternal;->convertToInt(Ljava/lang/String;)I

    #@4e
    move-result v4

    #@4f
    iput v4, v2, Landroid/net/DhcpInfo;->dns2:I

    #@51
    .line 92
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@53
    invoke-direct {p0, v4}, Landroid/net/DhcpInfoInternal;->convertToInt(Ljava/lang/String;)I

    #@56
    move-result v4

    #@57
    iput v4, v2, Landroid/net/DhcpInfo;->serverAddress:I

    #@59
    .line 93
    iget v4, p0, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@5b
    iput v4, v2, Landroid/net/DhcpInfo;->leaseDuration:I

    #@5d
    .line 94
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@5f
    iput-object v4, v2, Landroid/net/DhcpInfo;->domainName:Ljava/lang/String;

    #@61
    .line 95
    return-object v2

    #@62
    .line 89
    :catch_62
    move-exception v4

    #@63
    goto :goto_41
.end method

.method public makeLinkAddress()Landroid/net/LinkAddress;
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 99
    iget-object v2, p0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_12

    #@9
    .line 100
    const-string v2, "DhcpInfoInternal"

    #@b
    const-string/jumbo v3, "makeLinkAddress with empty ipAddress"

    #@e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 110
    :goto_11
    return-object v1

    #@12
    .line 106
    :cond_12
    :try_start_12
    new-instance v2, Landroid/net/LinkAddress;

    #@14
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@16
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@19
    move-result-object v3

    #@1a
    iget v4, p0, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@1c
    invoke-direct {v2, v3, v4}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V
    :try_end_1f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_1f} :catch_21

    #@1f
    move-object v1, v2

    #@20
    goto :goto_11

    #@21
    .line 108
    :catch_21
    move-exception v0

    #@22
    .line 109
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@25
    goto :goto_11
.end method

.method public makeLinkProperties()Landroid/net/LinkProperties;
    .registers 6

    #@0
    .prologue
    .line 117
    new-instance v1, Landroid/net/LinkProperties;

    #@2
    invoke-direct {v1}, Landroid/net/LinkProperties;-><init>()V

    #@5
    .line 118
    .local v1, p:Landroid/net/LinkProperties;
    invoke-virtual {p0}, Landroid/net/DhcpInfoInternal;->makeLinkAddress()Landroid/net/LinkAddress;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v3}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V

    #@c
    .line 119
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@e
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_22

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Landroid/net/RouteInfo;

    #@1e
    .line 120
    .local v2, route:Landroid/net/RouteInfo;
    invoke-virtual {v1, v2}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V

    #@21
    goto :goto_12

    #@22
    .line 123
    .end local v2           #route:Landroid/net/RouteInfo;
    :cond_22
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@24
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@27
    move-result v3

    #@28
    if-nez v3, :cond_52

    #@2a
    .line 124
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@2c
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v1, v3}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@33
    .line 128
    :goto_33
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@35
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@38
    move-result v3

    #@39
    if-nez v3, :cond_5b

    #@3b
    .line 129
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@3d
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v1, v3}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    #@44
    .line 133
    :goto_44
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@46
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@49
    move-result v3

    #@4a
    if-nez v3, :cond_64

    #@4c
    .line 134
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@4e
    invoke-virtual {v1, v3}, Landroid/net/LinkProperties;->setDomainName(Ljava/lang/String;)V

    #@51
    .line 138
    :goto_51
    return-object v1

    #@52
    .line 126
    :cond_52
    const-string v3, "DhcpInfoInternal"

    #@54
    const-string/jumbo v4, "makeLinkProperties with empty dns1!"

    #@57
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_33

    #@5b
    .line 131
    :cond_5b
    const-string v3, "DhcpInfoInternal"

    #@5d
    const-string/jumbo v4, "makeLinkProperties with empty dns2!"

    #@60
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_44

    #@64
    .line 136
    :cond_64
    const-string v3, "DhcpInfoInternal"

    #@66
    const-string/jumbo v4, "makeLinkProperties with empty domainName!"

    #@69
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    goto :goto_51
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 180
    const-string v2, ""

    #@2
    .line 181
    .local v2, routeString:Ljava/lang/String;
    iget-object v3, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@4
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v0

    #@8
    .local v0, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_30

    #@e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/net/RouteInfo;

    #@14
    .local v1, route:Landroid/net/RouteInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v1}, Landroid/net/RouteInfo;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, " | "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    goto :goto_8

    #@30
    .line 182
    .end local v1           #route:Landroid/net/RouteInfo;
    :cond_30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v4, "addr: "

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->ipAddress:Ljava/lang/String;

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string v4, "/"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    iget v4, p0, Landroid/net/DhcpInfoInternal;->prefixLength:I

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    const-string v4, " mRoutes: "

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    const-string v4, " dns: "

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    const-string v4, ","

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    const-string v4, " dhcpServer: "

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->serverAddress:Ljava/lang/String;

    #@77
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    const-string v4, " leaseDuration: "

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    iget v4, p0, Landroid/net/DhcpInfoInternal;->leaseDuration:I

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    const-string v4, " domainName: "

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    iget-object v4, p0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@8f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v3

    #@93
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v3

    #@97
    return-object v3
.end method

.method public updateFromDhcpRequest(Landroid/net/DhcpInfoInternal;)V
    .registers 5
    .parameter "orig"

    #@0
    .prologue
    .line 146
    if-nez p1, :cond_3

    #@2
    .line 165
    :cond_2
    :goto_2
    return-void

    #@3
    .line 148
    :cond_3
    iget-object v2, p0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@5
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_f

    #@b
    .line 149
    iget-object v2, p1, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@d
    iput-object v2, p0, Landroid/net/DhcpInfoInternal;->dns1:Ljava/lang/String;

    #@f
    .line 152
    :cond_f
    iget-object v2, p0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@11
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_1b

    #@17
    .line 153
    iget-object v2, p1, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@19
    iput-object v2, p0, Landroid/net/DhcpInfoInternal;->dns2:Ljava/lang/String;

    #@1b
    .line 156
    :cond_1b
    iget-object v2, p0, Landroid/net/DhcpInfoInternal;->mRoutes:Ljava/util/Collection;

    #@1d
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_3b

    #@23
    .line 157
    invoke-virtual {p1}, Landroid/net/DhcpInfoInternal;->getRoutes()Ljava/util/Collection;

    #@26
    move-result-object v2

    #@27
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v0

    #@2b
    .local v0, i$:Ljava/util/Iterator;
    :goto_2b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_3b

    #@31
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Landroid/net/RouteInfo;

    #@37
    .line 158
    .local v1, route:Landroid/net/RouteInfo;
    invoke-virtual {p0, v1}, Landroid/net/DhcpInfoInternal;->addRoute(Landroid/net/RouteInfo;)V

    #@3a
    goto :goto_2b

    #@3b
    .line 162
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #route:Landroid/net/RouteInfo;
    :cond_3b
    iget-object v2, p0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@3d
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_2

    #@43
    .line 163
    iget-object v2, p1, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@45
    iput-object v2, p0, Landroid/net/DhcpInfoInternal;->domainName:Ljava/lang/String;

    #@47
    goto :goto_2
.end method
