.class public abstract Landroid/net/INetworkManagementEventObserverEx$Stub;
.super Landroid/os/Binder;
.source "INetworkManagementEventObserverEx.java"

# interfaces
.implements Landroid/net/INetworkManagementEventObserverEx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/INetworkManagementEventObserverEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/INetworkManagementEventObserverEx$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.INetworkManagementEventObserverEx"

.field static final TRANSACTION_DnsFailed:I = 0x2

.field static final TRANSACTION_interfaceLinkStateChanged:I = 0x1

.field static final TRANSACTION_interfaceThrottleStateChanged:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.net.INetworkManagementEventObserverEx"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/INetworkManagementEventObserverEx$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/INetworkManagementEventObserverEx;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.net.INetworkManagementEventObserverEx"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/INetworkManagementEventObserverEx;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/net/INetworkManagementEventObserverEx;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/net/INetworkManagementEventObserverEx$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/INetworkManagementEventObserverEx$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_50

    #@4
    .line 84
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 47
    :sswitch_9
    const-string v3, "android.net.INetworkManagementEventObserverEx"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v3, "android.net.INetworkManagementEventObserverEx"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 56
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_26

    #@1e
    move v1, v2

    #@1f
    .line 57
    .local v1, _arg1:Z
    :goto_1f
    invoke-virtual {p0, v0, v1}, Landroid/net/INetworkManagementEventObserverEx$Stub;->interfaceLinkStateChanged(Ljava/lang/String;Z)V

    #@22
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@25
    goto :goto_8

    #@26
    .line 56
    .end local v1           #_arg1:Z
    :cond_26
    const/4 v1, 0x0

    #@27
    goto :goto_1f

    #@28
    .line 63
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_28
    const-string v3, "android.net.INetworkManagementEventObserverEx"

    #@2a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    .line 67
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v1

    #@35
    .line 68
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/INetworkManagementEventObserverEx$Stub;->DnsFailed(Ljava/lang/String;I)V

    #@38
    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    goto :goto_8

    #@3c
    .line 74
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_3c
    const-string v3, "android.net.INetworkManagementEventObserverEx"

    #@3e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    .line 78
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v1

    #@49
    .line 79
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/INetworkManagementEventObserverEx$Stub;->interfaceThrottleStateChanged(Ljava/lang/String;I)V

    #@4c
    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f
    goto :goto_8

    #@50
    .line 43
    :sswitch_data_50
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_3c
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
