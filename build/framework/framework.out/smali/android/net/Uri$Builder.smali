.class public final Landroid/net/Uri$Builder;
.super Ljava/lang/Object;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private authority:Landroid/net/Uri$Part;

.field private fragment:Landroid/net/Uri$Part;

.field private opaquePart:Landroid/net/Uri$Part;

.field private path:Landroid/net/Uri$PathPart;

.field private query:Landroid/net/Uri$Part;

.field private scheme:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1326
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private hasSchemeOrAuthority()Z
    .registers 3

    #@0
    .prologue
    .line 1538
    iget-object v0, p0, Landroid/net/Uri$Builder;->scheme:Ljava/lang/String;

    #@2
    if-nez v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/net/Uri$Builder;->authority:Landroid/net/Uri$Part;

    #@6
    if-eqz v0, :cond_10

    #@8
    iget-object v0, p0, Landroid/net/Uri$Builder;->authority:Landroid/net/Uri$Part;

    #@a
    sget-object v1, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@c
    if-eq v0, v1, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method


# virtual methods
.method public appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "newSegment"

    #@0
    .prologue
    .line 1425
    iget-object v0, p0, Landroid/net/Uri$Builder;->path:Landroid/net/Uri$PathPart;

    #@2
    invoke-static {v0, p1}, Landroid/net/Uri$PathPart;->appendEncodedSegment(Landroid/net/Uri$PathPart;Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->path(Landroid/net/Uri$PathPart;)Landroid/net/Uri$Builder;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "newSegment"

    #@0
    .prologue
    .line 1418
    iget-object v0, p0, Landroid/net/Uri$Builder;->path:Landroid/net/Uri$PathPart;

    #@2
    invoke-static {v0, p1}, Landroid/net/Uri$PathPart;->appendDecodedSegment(Landroid/net/Uri$PathPart;Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->path(Landroid/net/Uri$PathPart;)Landroid/net/Uri$Builder;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 8
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1478
    iput-object v4, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@3
    .line 1480
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    invoke-static {p1, v4}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    const-string v3, "="

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p2, v4}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    .line 1483
    .local v0, encodedParameter:Ljava/lang/String;
    iget-object v2, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@24
    if-nez v2, :cond_2d

    #@26
    .line 1484
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@29
    move-result-object v2

    #@2a
    iput-object v2, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@2c
    .line 1495
    :goto_2c
    return-object p0

    #@2d
    .line 1488
    :cond_2d
    iget-object v2, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@2f
    invoke-virtual {v2}, Landroid/net/Uri$Part;->getEncoded()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    .line 1489
    .local v1, oldQuery:Ljava/lang/String;
    if-eqz v1, :cond_3b

    #@35
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@38
    move-result v2

    #@39
    if-nez v2, :cond_42

    #@3b
    .line 1490
    :cond_3b
    invoke-static {v0}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3e
    move-result-object v2

    #@3f
    iput-object v2, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@41
    goto :goto_2c

    #@42
    .line 1492
    :cond_42
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, "&"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    invoke-static {v2}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@5c
    move-result-object v2

    #@5d
    iput-object v2, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@5f
    goto :goto_2c
.end method

.method authority(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "authority"

    #@0
    .prologue
    .line 1363
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@3
    .line 1365
    iput-object p1, p0, Landroid/net/Uri$Builder;->authority:Landroid/net/Uri$Part;

    #@5
    .line 1366
    return-object p0
.end method

.method public authority(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "authority"

    #@0
    .prologue
    .line 1373
    invoke-static {p1}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->authority(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public build()Landroid/net/Uri;
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1512
    iget-object v0, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@3
    if-eqz v0, :cond_1d

    #@5
    .line 1513
    iget-object v0, p0, Landroid/net/Uri$Builder;->scheme:Ljava/lang/String;

    #@7
    if-nez v0, :cond_11

    #@9
    .line 1514
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@b
    const-string v1, "An opaque URI must have a scheme."

    #@d
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 1518
    :cond_11
    new-instance v0, Landroid/net/Uri$OpaqueUri;

    #@13
    iget-object v1, p0, Landroid/net/Uri$Builder;->scheme:Ljava/lang/String;

    #@15
    iget-object v2, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@17
    iget-object v4, p0, Landroid/net/Uri$Builder;->fragment:Landroid/net/Uri$Part;

    #@19
    invoke-direct {v0, v1, v2, v4, v6}, Landroid/net/Uri$OpaqueUri;-><init>(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$Part;Landroid/net/Uri$1;)V

    #@1c
    .line 1532
    :goto_1c
    return-object v0

    #@1d
    .line 1521
    :cond_1d
    iget-object v3, p0, Landroid/net/Uri$Builder;->path:Landroid/net/Uri$PathPart;

    #@1f
    .line 1522
    .local v3, path:Landroid/net/Uri$PathPart;
    if-eqz v3, :cond_25

    #@21
    sget-object v0, Landroid/net/Uri$PathPart;->NULL:Landroid/net/Uri$PathPart;

    #@23
    if-ne v3, v0, :cond_35

    #@25
    .line 1523
    :cond_25
    sget-object v3, Landroid/net/Uri$PathPart;->EMPTY:Landroid/net/Uri$PathPart;

    #@27
    .line 1532
    :cond_27
    :goto_27
    new-instance v0, Landroid/net/Uri$HierarchicalUri;

    #@29
    iget-object v1, p0, Landroid/net/Uri$Builder;->scheme:Ljava/lang/String;

    #@2b
    iget-object v2, p0, Landroid/net/Uri$Builder;->authority:Landroid/net/Uri$Part;

    #@2d
    iget-object v4, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@2f
    iget-object v5, p0, Landroid/net/Uri$Builder;->fragment:Landroid/net/Uri$Part;

    #@31
    invoke-direct/range {v0 .. v6}, Landroid/net/Uri$HierarchicalUri;-><init>(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$PathPart;Landroid/net/Uri$Part;Landroid/net/Uri$Part;Landroid/net/Uri$1;)V

    #@34
    goto :goto_1c

    #@35
    .line 1527
    :cond_35
    invoke-direct {p0}, Landroid/net/Uri$Builder;->hasSchemeOrAuthority()Z

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_27

    #@3b
    .line 1528
    invoke-static {v3}, Landroid/net/Uri$PathPart;->makeAbsolute(Landroid/net/Uri$PathPart;)Landroid/net/Uri$PathPart;

    #@3e
    move-result-object v3

    #@3f
    goto :goto_27
.end method

.method public clearQuery()Landroid/net/Uri$Builder;
    .registers 2

    #@0
    .prologue
    .line 1502
    const/4 v0, 0x0

    #@1
    check-cast v0, Landroid/net/Uri$Part;

    #@3
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->query(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "authority"

    #@0
    .prologue
    .line 1380
    invoke-static {p1}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->authority(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "fragment"

    #@0
    .prologue
    .line 1466
    invoke-static {p1}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->fragment(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public encodedOpaquePart(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "opaquePart"

    #@0
    .prologue
    .line 1358
    invoke-static {p1}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->opaquePart(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 1411
    invoke-static {p1}, Landroid/net/Uri$PathPart;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->path(Landroid/net/Uri$PathPart;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "query"

    #@0
    .prologue
    .line 1447
    invoke-static {p1}, Landroid/net/Uri$Part;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->query(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method fragment(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;
    .registers 2
    .parameter "fragment"

    #@0
    .prologue
    .line 1451
    iput-object p1, p0, Landroid/net/Uri$Builder;->fragment:Landroid/net/Uri$Part;

    #@2
    .line 1452
    return-object p0
.end method

.method public fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "fragment"

    #@0
    .prologue
    .line 1459
    invoke-static {p1}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->fragment(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method opaquePart(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;
    .registers 2
    .parameter "opaquePart"

    #@0
    .prologue
    .line 1339
    iput-object p1, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@2
    .line 1340
    return-object p0
.end method

.method public opaquePart(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "opaquePart"

    #@0
    .prologue
    .line 1349
    invoke-static {p1}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->opaquePart(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method path(Landroid/net/Uri$PathPart;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 1385
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@3
    .line 1387
    iput-object p1, p0, Landroid/net/Uri$Builder;->path:Landroid/net/Uri$PathPart;

    #@5
    .line 1388
    return-object p0
.end method

.method public path(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 1400
    invoke-static {p1}, Landroid/net/Uri$PathPart;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->path(Landroid/net/Uri$PathPart;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method query(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "query"

    #@0
    .prologue
    .line 1430
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/net/Uri$Builder;->opaquePart:Landroid/net/Uri$Part;

    #@3
    .line 1432
    iput-object p1, p0, Landroid/net/Uri$Builder;->query:Landroid/net/Uri$Part;

    #@5
    .line 1433
    return-object p0
.end method

.method public query(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 3
    .parameter "query"

    #@0
    .prologue
    .line 1440
    invoke-static {p1}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->query(Landroid/net/Uri$Part;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .registers 2
    .parameter "scheme"

    #@0
    .prologue
    .line 1334
    iput-object p1, p0, Landroid/net/Uri$Builder;->scheme:Ljava/lang/String;

    #@2
    .line 1335
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1545
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
