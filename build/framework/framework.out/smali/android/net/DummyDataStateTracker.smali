.class public Landroid/net/DummyDataStateTracker;
.super Ljava/lang/Object;
.source "DummyDataStateTracker.java"

# interfaces
.implements Landroid/net/NetworkStateTracker;


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "DummyDataStateTracker"

.field private static final VDBG:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDefaultRouteSet:Z

.field private mIsDefaultOrHipri:Z

.field private mLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mLinkProperties:Landroid/net/LinkProperties;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mPrivateDnsRouteSet:Z

.field private mTarget:Landroid/os/Handler;

.field private mTeardownRequested:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 4
    .parameter "netType"
    .parameter "tag"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 41
    iput-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mTeardownRequested:Z

    #@6
    .line 46
    iput-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mPrivateDnsRouteSet:Z

    #@8
    .line 47
    iput-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mDefaultRouteSet:Z

    #@a
    .line 51
    iput-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mIsDefaultOrHipri:Z

    #@c
    .line 59
    new-instance v0, Landroid/net/NetworkInfo;

    #@e
    invoke-direct {v0, p1}, Landroid/net/NetworkInfo;-><init>(I)V

    #@11
    iput-object v0, p0, Landroid/net/DummyDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@13
    .line 60
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 210
    const-string v0, "DummyDataStateTracker"

    #@2
    invoke-static {v0, p0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 211
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 214
    const-string v0, "DummyDataStateTracker"

    #@2
    invoke-static {v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 215
    return-void
.end method

.method private setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "state"
    .parameter "reason"
    .parameter "extraInfo"

    #@0
    .prologue
    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v2, "setDetailed state, old ="

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    iget-object v2, p0, Landroid/net/DummyDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@e
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, " and new state="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v1}, Landroid/net/DummyDataStateTracker;->log(Ljava/lang/String;)V

    #@27
    .line 142
    iget-object v1, p0, Landroid/net/DummyDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@29
    invoke-virtual {v1, p1, p2, p3}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 143
    iget-object v1, p0, Landroid/net/DummyDataStateTracker;->mTarget:Landroid/os/Handler;

    #@2e
    const/4 v2, 0x1

    #@2f
    iget-object v3, p0, Landroid/net/DummyDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@31
    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@34
    move-result-object v0

    #@35
    .line 144
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@38
    .line 145
    return-void
.end method


# virtual methods
.method public captivePortalCheckComplete()V
    .registers 1

    #@0
    .prologue
    .line 127
    return-void
.end method

.method public defaultRouteSet(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 90
    iput-boolean p1, p0, Landroid/net/DummyDataStateTracker;->mDefaultRouteSet:Z

    #@2
    .line 91
    return-void
.end method

.method public getLinkCapabilities()Landroid/net/LinkCapabilities;
    .registers 3

    #@0
    .prologue
    .line 202
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    iget-object v1, p0, Landroid/net/DummyDataStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkCapabilities;-><init>(Landroid/net/LinkCapabilities;)V

    #@7
    return-object v0
.end method

.method public getLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 195
    new-instance v0, Landroid/net/LinkProperties;

    #@2
    iget-object v1, p0, Landroid/net/DummyDataStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@7
    return-object v0
.end method

.method public getNetworkInfo()Landroid/net/NetworkInfo;
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/net/DummyDataStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method public getTcpBufferSizesPropName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 111
    const-string/jumbo v0, "net.tcp.buffersize.unknown"

    #@3
    return-object v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 103
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isDefaultRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mDefaultRouteSet:Z

    #@2
    return v0
.end method

.method public isPrivateDnsRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 74
    iget-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mPrivateDnsRouteSet:Z

    #@2
    return v0
.end method

.method public isTeardownRequested()Z
    .registers 2

    #@0
    .prologue
    .line 152
    iget-boolean v0, p0, Landroid/net/DummyDataStateTracker;->mTeardownRequested:Z

    #@2
    return v0
.end method

.method public privateDnsRouteSet(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 78
    iput-boolean p1, p0, Landroid/net/DummyDataStateTracker;->mPrivateDnsRouteSet:Z

    #@2
    .line 79
    return-void
.end method

.method public reconnect()Z
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 160
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@3
    const-string v1, "enabled"

    #@5
    invoke-direct {p0, v0, v1, v2}, Landroid/net/DummyDataStateTracker;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 161
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@a
    const-string v1, "enabled"

    #@c
    invoke-direct {p0, v0, v1, v2}, Landroid/net/DummyDataStateTracker;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 162
    const/4 v0, 0x0

    #@10
    invoke-virtual {p0, v0}, Landroid/net/DummyDataStateTracker;->setTeardownRequested(Z)V

    #@13
    .line 163
    const/4 v0, 0x1

    #@14
    return v0
.end method

.method public releaseWakeLock()V
    .registers 1

    #@0
    .prologue
    .line 97
    return-void
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 2
    .parameter "msger"

    #@0
    .prologue
    .line 219
    return-void
.end method

.method public setDependencyMet(Z)V
    .registers 2
    .parameter "met"

    #@0
    .prologue
    .line 207
    return-void
.end method

.method public setPolicyDataEnable(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 183
    return-void
.end method

.method public setRadio(Z)Z
    .registers 3
    .parameter "turnOn"

    #@0
    .prologue
    .line 172
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setTeardownRequested(Z)V
    .registers 2
    .parameter "isRequested"

    #@0
    .prologue
    .line 148
    iput-boolean p1, p0, Landroid/net/DummyDataStateTracker;->mTeardownRequested:Z

    #@2
    .line 149
    return-void
.end method

.method public setUserDataEnable(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 178
    return-void
.end method

.method public startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 69
    iput-object p2, p0, Landroid/net/DummyDataStateTracker;->mTarget:Landroid/os/Handler;

    #@2
    .line 70
    iput-object p1, p0, Landroid/net/DummyDataStateTracker;->mContext:Landroid/content/Context;

    #@4
    .line 71
    return-void
.end method

.method public teardown()Z
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 120
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@3
    const-string v1, "disabled"

    #@5
    invoke-direct {p0, v0, v1, v2}, Landroid/net/DummyDataStateTracker;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 121
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@a
    const-string v1, "disabled"

    #@c
    invoke-direct {p0, v0, v1, v2}, Landroid/net/DummyDataStateTracker;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 122
    const/4 v0, 0x1

    #@10
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    const-string v1, "Dummy data state: none, dummy!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@7
    .line 188
    .local v0, sb:Ljava/lang/StringBuffer;
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method
