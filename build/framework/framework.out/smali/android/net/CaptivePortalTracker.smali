.class public Landroid/net/CaptivePortalTracker;
.super Lcom/android/internal/util/StateMachine;
.source "CaptivePortalTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;,
        Landroid/net/CaptivePortalTracker$ActiveNetworkState;,
        Landroid/net/CaptivePortalTracker$NoActiveNetworkState;,
        Landroid/net/CaptivePortalTracker$DefaultState;
    }
.end annotation


# static fields
.field private static final CMD_CONNECTIVITY_CHANGE:I = 0x1

.field private static final CMD_DELAYED_CAPTIVE_CHECK:I = 0x2

.field private static final CMD_DETECT_PORTAL:I = 0x0

.field private static final DBG:Z = false

.field private static final DEFAULT_SERVER:Ljava/lang/String; = "clients3.google.com"

.field private static final DELAYED_CHECK_INTERVAL_MS:I = 0x2710

.field private static final NOTIFICATION_ID:Ljava/lang/String; = "CaptivePortal.Notification"

.field private static final SOCKET_TIMEOUT_MS:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "CaptivePortalTracker"


# instance fields
.field private mActiveNetworkState:Lcom/android/internal/util/State;

.field private mConnService:Landroid/net/IConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mDefaultState:Lcom/android/internal/util/State;

.field private mDelayedCaptiveCheckState:Lcom/android/internal/util/State;

.field private mDelayedCheckToken:I

.field private mIsCaptivePortalCheckEnabled:Z

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mNoActiveNetworkState:Lcom/android/internal/util/State;

.field private mNotificationShown:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mServer:Ljava/lang/String;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/net/IConnectivityManager;)V
    .registers 8
    .parameter "context"
    .parameter "cs"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 85
    const-string v1, "CaptivePortalTracker"

    #@5
    invoke-direct {p0, v1}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@8
    .line 64
    iput-boolean v3, p0, Landroid/net/CaptivePortalTracker;->mNotificationShown:Z

    #@a
    .line 65
    iput-boolean v3, p0, Landroid/net/CaptivePortalTracker;->mIsCaptivePortalCheckEnabled:Z

    #@c
    .line 77
    iput v3, p0, Landroid/net/CaptivePortalTracker;->mDelayedCheckToken:I

    #@e
    .line 79
    new-instance v1, Landroid/net/CaptivePortalTracker$DefaultState;

    #@10
    invoke-direct {v1, p0, v4}, Landroid/net/CaptivePortalTracker$DefaultState;-><init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V

    #@13
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mDefaultState:Lcom/android/internal/util/State;

    #@15
    .line 80
    new-instance v1, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;

    #@17
    invoke-direct {v1, p0, v4}, Landroid/net/CaptivePortalTracker$NoActiveNetworkState;-><init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V

    #@1a
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mNoActiveNetworkState:Lcom/android/internal/util/State;

    #@1c
    .line 81
    new-instance v1, Landroid/net/CaptivePortalTracker$ActiveNetworkState;

    #@1e
    invoke-direct {v1, p0, v4}, Landroid/net/CaptivePortalTracker$ActiveNetworkState;-><init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V

    #@21
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mActiveNetworkState:Lcom/android/internal/util/State;

    #@23
    .line 82
    new-instance v1, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;

    #@25
    invoke-direct {v1, p0, v4}, Landroid/net/CaptivePortalTracker$DelayedCaptiveCheckState;-><init>(Landroid/net/CaptivePortalTracker;Landroid/net/CaptivePortalTracker$1;)V

    #@28
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mDelayedCaptiveCheckState:Lcom/android/internal/util/State;

    #@2a
    .line 109
    new-instance v1, Landroid/net/CaptivePortalTracker$1;

    #@2c
    invoke-direct {v1, p0}, Landroid/net/CaptivePortalTracker$1;-><init>(Landroid/net/CaptivePortalTracker;)V

    #@2f
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mReceiver:Landroid/content/BroadcastReceiver;

    #@31
    .line 87
    iput-object p1, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@33
    .line 88
    iput-object p2, p0, Landroid/net/CaptivePortalTracker;->mConnService:Landroid/net/IConnectivityManager;

    #@35
    .line 89
    const-string/jumbo v1, "phone"

    #@38
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3b
    move-result-object v1

    #@3c
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@3e
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@40
    .line 91
    new-instance v0, Landroid/content/IntentFilter;

    #@42
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@45
    .line 92
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    #@47
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4a
    .line 93
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@4c
    iget-object v4, p0, Landroid/net/CaptivePortalTracker;->mReceiver:Landroid/content/BroadcastReceiver;

    #@4e
    invoke-virtual {v1, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@51
    .line 95
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@53
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@56
    move-result-object v1

    #@57
    const-string v4, "captive_portal_server"

    #@59
    invoke-static {v1, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mServer:Ljava/lang/String;

    #@5f
    .line 97
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mServer:Ljava/lang/String;

    #@61
    if-nez v1, :cond_67

    #@63
    const-string v1, "clients3.google.com"

    #@65
    iput-object v1, p0, Landroid/net/CaptivePortalTracker;->mServer:Ljava/lang/String;

    #@67
    .line 99
    :cond_67
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@69
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6c
    move-result-object v1

    #@6d
    const-string v4, "captive_portal_detection_enabled"

    #@6f
    invoke-static {v1, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@72
    move-result v1

    #@73
    if-ne v1, v2, :cond_98

    #@75
    move v1, v2

    #@76
    :goto_76
    iput-boolean v1, p0, Landroid/net/CaptivePortalTracker;->mIsCaptivePortalCheckEnabled:Z

    #@78
    .line 102
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mDefaultState:Lcom/android/internal/util/State;

    #@7a
    invoke-virtual {p0, v1}, Landroid/net/CaptivePortalTracker;->addState(Lcom/android/internal/util/State;)V

    #@7d
    .line 103
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mNoActiveNetworkState:Lcom/android/internal/util/State;

    #@7f
    iget-object v2, p0, Landroid/net/CaptivePortalTracker;->mDefaultState:Lcom/android/internal/util/State;

    #@81
    invoke-virtual {p0, v1, v2}, Landroid/net/CaptivePortalTracker;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@84
    .line 104
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mActiveNetworkState:Lcom/android/internal/util/State;

    #@86
    iget-object v2, p0, Landroid/net/CaptivePortalTracker;->mDefaultState:Lcom/android/internal/util/State;

    #@88
    invoke-virtual {p0, v1, v2}, Landroid/net/CaptivePortalTracker;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@8b
    .line 105
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mDelayedCaptiveCheckState:Lcom/android/internal/util/State;

    #@8d
    iget-object v2, p0, Landroid/net/CaptivePortalTracker;->mActiveNetworkState:Lcom/android/internal/util/State;

    #@8f
    invoke-virtual {p0, v1, v2}, Landroid/net/CaptivePortalTracker;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@92
    .line 106
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mNoActiveNetworkState:Lcom/android/internal/util/State;

    #@94
    invoke-virtual {p0, v1}, Landroid/net/CaptivePortalTracker;->setInitialState(Lcom/android/internal/util/State;)V

    #@97
    .line 107
    return-void

    #@98
    :cond_98
    move v1, v3

    #@99
    .line 99
    goto :goto_76
.end method

.method static synthetic access$1000(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Landroid/net/CaptivePortalTracker;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/CaptivePortalTracker;->mNoActiveNetworkState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Landroid/net/CaptivePortalTracker;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/net/CaptivePortalTracker;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Landroid/net/CaptivePortalTracker;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Landroid/net/CaptivePortalTracker;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/net/CaptivePortalTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget v0, p0, Landroid/net/CaptivePortalTracker;->mDelayedCheckToken:I

    #@2
    return v0
.end method

.method static synthetic access$1504(Landroid/net/CaptivePortalTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget v0, p0, Landroid/net/CaptivePortalTracker;->mDelayedCheckToken:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/net/CaptivePortalTracker;->mDelayedCheckToken:I

    #@6
    return v0
.end method

.method static synthetic access$1600(Landroid/net/CaptivePortalTracker;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/CaptivePortalTracker;->mServer:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/net/CaptivePortalTracker;Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker;->lookupHost(Ljava/lang/String;)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1800(Landroid/net/CaptivePortalTracker;Ljava/net/InetAddress;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker;->isCaptivePortal(Ljava/net/InetAddress;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1900(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/CaptivePortalTracker;->mActiveNetworkState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Landroid/net/CaptivePortalTracker;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Landroid/net/CaptivePortalTracker;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker;->notifyPortalCheckComplete(Landroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-static {p0}, Landroid/net/CaptivePortalTracker;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/net/CaptivePortalTracker;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/CaptivePortalTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    iput-object p1, p0, Landroid/net/CaptivePortalTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Landroid/net/CaptivePortalTracker;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker;->setNotificationVisible(Z)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/net/CaptivePortalTracker;Landroid/net/NetworkInfo;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/net/CaptivePortalTracker;->isActiveNetwork(Landroid/net/NetworkInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Landroid/net/CaptivePortalTracker;)Lcom/android/internal/util/State;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/net/CaptivePortalTracker;->mDelayedCaptiveCheckState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method private isActiveNetwork(Landroid/net/NetworkInfo;)Z
    .registers 6
    .parameter "info"

    #@0
    .prologue
    .line 267
    :try_start_0
    iget-object v2, p0, Landroid/net/CaptivePortalTracker;->mConnService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v2}, Landroid/net/IConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@5
    move-result-object v0

    #@6
    .line 268
    .local v0, active:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_18

    #@8
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    #@b
    move-result v2

    #@c
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_14

    #@f
    move-result v3

    #@10
    if-ne v2, v3, :cond_18

    #@12
    .line 269
    const/4 v2, 0x1

    #@13
    .line 274
    .end local v0           #active:Landroid/net/NetworkInfo;
    :goto_13
    return v2

    #@14
    .line 271
    :catch_14
    move-exception v1

    #@15
    .line 272
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@18
    .line 274
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_18
    const/4 v2, 0x0

    #@19
    goto :goto_13
.end method

.method private isCaptivePortal(Ljava/net/InetAddress;)Z
    .registers 9
    .parameter "server"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 281
    const/4 v3, 0x0

    #@2
    .line 282
    .local v3, urlConnection:Ljava/net/HttpURLConnection;
    iget-boolean v4, p0, Landroid/net/CaptivePortalTracker;->mIsCaptivePortalCheckEnabled:Z

    #@4
    if-nez v4, :cond_7

    #@6
    .line 298
    :cond_6
    :goto_6
    return v5

    #@7
    .line 284
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v6, "http://"

    #@e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v6, "/generate_204"

    #@1c
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    iput-object v4, p0, Landroid/net/CaptivePortalTracker;->mUrl:Ljava/lang/String;

    #@26
    .line 287
    :try_start_26
    new-instance v2, Ljava/net/URL;

    #@28
    iget-object v4, p0, Landroid/net/CaptivePortalTracker;->mUrl:Ljava/lang/String;

    #@2a
    invoke-direct {v2, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@2d
    .line 288
    .local v2, url:Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    #@30
    move-result-object v4

    #@31
    move-object v0, v4

    #@32
    check-cast v0, Ljava/net/HttpURLConnection;

    #@34
    move-object v3, v0

    #@35
    .line 289
    const/4 v4, 0x0

    #@36
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    #@39
    .line 290
    const/16 v4, 0x2710

    #@3b
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    #@3e
    .line 291
    const/16 v4, 0x2710

    #@40
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    #@43
    .line 292
    const/4 v4, 0x0

    #@44
    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    #@47
    .line 293
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    #@4a
    .line 295
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_4d
    .catchall {:try_start_26 .. :try_end_4d} :catchall_63
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_4d} :catch_5c

    #@4d
    move-result v4

    #@4e
    const/16 v6, 0xcc

    #@50
    if-eq v4, v6, :cond_5a

    #@52
    const/4 v4, 0x1

    #@53
    .line 300
    :goto_53
    if-eqz v3, :cond_58

    #@55
    .line 301
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    #@58
    :cond_58
    move v5, v4

    #@59
    .line 295
    goto :goto_6

    #@5a
    :cond_5a
    move v4, v5

    #@5b
    goto :goto_53

    #@5c
    .line 296
    .end local v2           #url:Ljava/net/URL;
    :catch_5c
    move-exception v1

    #@5d
    .line 300
    .local v1, e:Ljava/io/IOException;
    if-eqz v3, :cond_6

    #@5f
    .line 301
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    #@62
    goto :goto_6

    #@63
    .line 300
    .end local v1           #e:Ljava/io/IOException;
    :catchall_63
    move-exception v4

    #@64
    if-eqz v3, :cond_69

    #@66
    .line 301
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    #@69
    .line 300
    :cond_69
    throw v4
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 375
    const-string v0, "CaptivePortalTracker"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 376
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 379
    const-string v0, "CaptivePortalTracker"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 380
    return-void
.end method

.method private lookupHost(Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 10
    .parameter "hostname"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 309
    :try_start_1
    invoke-static {p1}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_4} :catch_11

    #@4
    move-result-object v4

    #@5
    .line 314
    .local v4, inetAddress:[Ljava/net/InetAddress;
    move-object v1, v4

    #@6
    .local v1, arr$:[Ljava/net/InetAddress;
    array-length v5, v1

    #@7
    .local v5, len$:I
    const/4 v3, 0x0

    #@8
    .local v3, i$:I
    :goto_8
    if-ge v3, v5, :cond_17

    #@a
    aget-object v0, v1, v3

    #@c
    .line 315
    .local v0, a:Ljava/net/InetAddress;
    instance-of v7, v0, Ljava/net/Inet4Address;

    #@e
    if-eqz v7, :cond_14

    #@10
    .line 317
    .end local v0           #a:Ljava/net/InetAddress;
    .end local v1           #arr$:[Ljava/net/InetAddress;
    .end local v3           #i$:I
    .end local v4           #inetAddress:[Ljava/net/InetAddress;
    .end local v5           #len$:I
    :goto_10
    return-object v0

    #@11
    .line 310
    :catch_11
    move-exception v2

    #@12
    .local v2, e:Ljava/net/UnknownHostException;
    move-object v0, v6

    #@13
    .line 311
    goto :goto_10

    #@14
    .line 314
    .end local v2           #e:Ljava/net/UnknownHostException;
    .restart local v0       #a:Ljava/net/InetAddress;
    .restart local v1       #arr$:[Ljava/net/InetAddress;
    .restart local v3       #i$:I
    .restart local v4       #inetAddress:[Ljava/net/InetAddress;
    .restart local v5       #len$:I
    :cond_14
    add-int/lit8 v3, v3, 0x1

    #@16
    goto :goto_8

    #@17
    .end local v0           #a:Ljava/net/InetAddress;
    :cond_17
    move-object v0, v6

    #@18
    .line 317
    goto :goto_10
.end method

.method public static makeCaptivePortalTracker(Landroid/content/Context;Landroid/net/IConnectivityManager;)Landroid/net/CaptivePortalTracker;
    .registers 3
    .parameter "context"
    .parameter "cs"

    #@0
    .prologue
    .line 123
    new-instance v0, Landroid/net/CaptivePortalTracker;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/net/CaptivePortalTracker;-><init>(Landroid/content/Context;Landroid/net/IConnectivityManager;)V

    #@5
    .line 124
    .local v0, captivePortal:Landroid/net/CaptivePortalTracker;
    invoke-virtual {v0}, Landroid/net/CaptivePortalTracker;->start()V

    #@8
    .line 125
    return-object v0
.end method

.method private notifyPortalCheckComplete(Landroid/net/NetworkInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 254
    if-nez p1, :cond_9

    #@2
    .line 255
    const-string/jumbo v1, "notifyPortalCheckComplete on null"

    #@5
    invoke-static {v1}, Landroid/net/CaptivePortalTracker;->loge(Ljava/lang/String;)V

    #@8
    .line 263
    :goto_8
    return-void

    #@9
    .line 259
    :cond_9
    :try_start_9
    iget-object v1, p0, Landroid/net/CaptivePortalTracker;->mConnService:Landroid/net/IConnectivityManager;

    #@b
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->captivePortalCheckComplete(Landroid/net/NetworkInfo;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_f

    #@e
    goto :goto_8

    #@f
    .line 260
    :catch_f
    move-exception v0

    #@10
    .line 261
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@13
    goto :goto_8
.end method

.method private setNotificationVisible(Z)V
    .registers 15
    .parameter "visible"

    #@0
    .prologue
    const v12, 0x1040431

    #@3
    const v9, 0x1040430

    #@6
    const/4 v11, 0x1

    #@7
    const/4 v10, 0x0

    #@8
    .line 322
    if-nez p1, :cond_f

    #@a
    iget-boolean v7, p0, Landroid/net/CaptivePortalTracker;->mNotificationShown:Z

    #@c
    if-nez v7, :cond_f

    #@e
    .line 372
    :goto_e
    return-void

    #@f
    .line 326
    :cond_f
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@12
    move-result-object v5

    #@13
    .line 327
    .local v5, r:Landroid/content/res/Resources;
    iget-object v7, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@15
    const-string/jumbo v8, "notification"

    #@18
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v4

    #@1c
    check-cast v4, Landroid/app/NotificationManager;

    #@1e
    .line 330
    .local v4, notificationManager:Landroid/app/NotificationManager;
    if-eqz p1, :cond_b7

    #@20
    .line 334
    iget-object v7, p0, Landroid/net/CaptivePortalTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@22
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    #@25
    move-result v7

    #@26
    packed-switch v7, :pswitch_data_be

    #@29
    .line 349
    new-array v7, v11, [Ljava/lang/Object;

    #@2b
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v8

    #@2f
    aput-object v8, v7, v10

    #@31
    invoke-virtual {v5, v9, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    .line 350
    .local v6, title:Ljava/lang/CharSequence;
    new-array v7, v11, [Ljava/lang/Object;

    #@37
    iget-object v8, p0, Landroid/net/CaptivePortalTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@39
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@3c
    move-result-object v8

    #@3d
    aput-object v8, v7, v10

    #@3f
    invoke-virtual {v5, v12, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    .line 352
    .local v0, details:Ljava/lang/CharSequence;
    const v1, 0x1080529

    #@46
    .line 356
    .local v1, icon:I
    :goto_46
    new-instance v3, Landroid/app/Notification;

    #@48
    invoke-direct {v3}, Landroid/app/Notification;-><init>()V

    #@4b
    .line 357
    .local v3, notification:Landroid/app/Notification;
    const-wide/16 v7, 0x0

    #@4d
    iput-wide v7, v3, Landroid/app/Notification;->when:J

    #@4f
    .line 358
    iput v1, v3, Landroid/app/Notification;->icon:I

    #@51
    .line 359
    const/16 v7, 0x10

    #@53
    iput v7, v3, Landroid/app/Notification;->flags:I

    #@55
    .line 360
    new-instance v2, Landroid/content/Intent;

    #@57
    const-string v7, "android.intent.action.VIEW"

    #@59
    iget-object v8, p0, Landroid/net/CaptivePortalTracker;->mUrl:Ljava/lang/String;

    #@5b
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5e
    move-result-object v8

    #@5f
    invoke-direct {v2, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@62
    .line 361
    .local v2, intent:Landroid/content/Intent;
    const/high16 v7, 0x1040

    #@64
    invoke-virtual {v2, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@67
    .line 363
    iget-object v7, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@69
    invoke-static {v7, v10, v2, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@6c
    move-result-object v7

    #@6d
    iput-object v7, v3, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@6f
    .line 364
    iput-object v6, v3, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@71
    .line 365
    iget-object v7, p0, Landroid/net/CaptivePortalTracker;->mContext:Landroid/content/Context;

    #@73
    iget-object v8, v3, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@75
    invoke-virtual {v3, v7, v6, v0, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@78
    .line 367
    const-string v7, "CaptivePortal.Notification"

    #@7a
    invoke-virtual {v4, v7, v11, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    #@7d
    .line 371
    .end local v0           #details:Ljava/lang/CharSequence;
    .end local v1           #icon:I
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #notification:Landroid/app/Notification;
    .end local v6           #title:Ljava/lang/CharSequence;
    :goto_7d
    iput-boolean p1, p0, Landroid/net/CaptivePortalTracker;->mNotificationShown:Z

    #@7f
    goto :goto_e

    #@80
    .line 336
    :pswitch_80
    const v7, 0x104042f

    #@83
    new-array v8, v11, [Ljava/lang/Object;

    #@85
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@88
    move-result-object v9

    #@89
    aput-object v9, v8, v10

    #@8b
    invoke-virtual {v5, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@8e
    move-result-object v6

    #@8f
    .line 337
    .restart local v6       #title:Ljava/lang/CharSequence;
    new-array v7, v11, [Ljava/lang/Object;

    #@91
    iget-object v8, p0, Landroid/net/CaptivePortalTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@93
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    #@96
    move-result-object v8

    #@97
    aput-object v8, v7, v10

    #@99
    invoke-virtual {v5, v12, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@9c
    move-result-object v0

    #@9d
    .line 339
    .restart local v0       #details:Ljava/lang/CharSequence;
    const v1, 0x108052d

    #@a0
    .line 340
    .restart local v1       #icon:I
    goto :goto_46

    #@a1
    .line 342
    .end local v0           #details:Ljava/lang/CharSequence;
    .end local v1           #icon:I
    .end local v6           #title:Ljava/lang/CharSequence;
    :pswitch_a1
    new-array v7, v11, [Ljava/lang/Object;

    #@a3
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a6
    move-result-object v8

    #@a7
    aput-object v8, v7, v10

    #@a9
    invoke-virtual {v5, v9, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@ac
    move-result-object v6

    #@ad
    .line 345
    .restart local v6       #title:Ljava/lang/CharSequence;
    iget-object v7, p0, Landroid/net/CaptivePortalTracker;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@af
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    #@b2
    move-result-object v0

    #@b3
    .line 346
    .restart local v0       #details:Ljava/lang/CharSequence;
    const v1, 0x1080529

    #@b6
    .line 347
    .restart local v1       #icon:I
    goto :goto_46

    #@b7
    .line 369
    .end local v0           #details:Ljava/lang/CharSequence;
    .end local v1           #icon:I
    .end local v6           #title:Ljava/lang/CharSequence;
    :cond_b7
    const-string v7, "CaptivePortal.Notification"

    #@b9
    invoke-virtual {v4, v7, v11}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    #@bc
    goto :goto_7d

    #@bd
    .line 334
    nop

    #@be
    :pswitch_data_be
    .packed-switch 0x0
        :pswitch_a1
        :pswitch_80
    .end packed-switch
.end method


# virtual methods
.method public detectCaptivePortal(Landroid/net/NetworkInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 129
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/net/CaptivePortalTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {p0, v0}, Landroid/net/CaptivePortalTracker;->sendMessage(Landroid/os/Message;)V

    #@8
    .line 130
    return-void
.end method
