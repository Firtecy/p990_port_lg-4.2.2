.class Landroid/net/Uri$PathPart;
.super Landroid/net/Uri$AbstractPart;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PathPart"
.end annotation


# static fields
.field static final EMPTY:Landroid/net/Uri$PathPart;

.field static final NULL:Landroid/net/Uri$PathPart;


# instance fields
.field private pathSegments:Landroid/net/Uri$PathSegments;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2098
    new-instance v0, Landroid/net/Uri$PathPart;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/net/Uri$PathPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    sput-object v0, Landroid/net/Uri$PathPart;->NULL:Landroid/net/Uri$PathPart;

    #@8
    .line 2101
    new-instance v0, Landroid/net/Uri$PathPart;

    #@a
    const-string v1, ""

    #@c
    const-string v2, ""

    #@e
    invoke-direct {v0, v1, v2}, Landroid/net/Uri$PathPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    sput-object v0, Landroid/net/Uri$PathPart;->EMPTY:Landroid/net/Uri$PathPart;

    #@13
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "encoded"
    .parameter "decoded"

    #@0
    .prologue
    .line 2104
    invoke-direct {p0, p1, p2}, Landroid/net/Uri$AbstractPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 2105
    return-void
.end method

.method static appendDecodedSegment(Landroid/net/Uri$PathPart;Ljava/lang/String;)Landroid/net/Uri$PathPart;
    .registers 4
    .parameter "oldPart"
    .parameter "decoded"

    #@0
    .prologue
    .line 2191
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 2194
    .local v0, encoded:Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/net/Uri$PathPart;->appendEncodedSegment(Landroid/net/Uri$PathPart;Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method static appendEncodedSegment(Landroid/net/Uri$PathPart;Ljava/lang/String;)Landroid/net/Uri$PathPart;
    .registers 7
    .parameter "oldPart"
    .parameter "newSegment"

    #@0
    .prologue
    .line 2165
    if-nez p0, :cond_1a

    #@2
    .line 2167
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "/"

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v3}, Landroid/net/Uri$PathPart;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@18
    move-result-object v3

    #@19
    .line 2187
    :goto_19
    return-object v3

    #@1a
    .line 2170
    :cond_1a
    invoke-virtual {p0}, Landroid/net/Uri$PathPart;->getEncoded()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    .line 2172
    .local v1, oldPath:Ljava/lang/String;
    if-nez v1, :cond_22

    #@20
    .line 2173
    const-string v1, ""

    #@22
    .line 2176
    :cond_22
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@25
    move-result v2

    #@26
    .line 2178
    .local v2, oldPathLength:I
    if-nez v2, :cond_40

    #@28
    .line 2180
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "/"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .line 2187
    .local v0, newPath:Ljava/lang/String;
    :goto_3b
    invoke-static {v0}, Landroid/net/Uri$PathPart;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@3e
    move-result-object v3

    #@3f
    goto :goto_19

    #@40
    .line 2181
    .end local v0           #newPath:Ljava/lang/String;
    :cond_40
    add-int/lit8 v3, v2, -0x1

    #@42
    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    #@45
    move-result v3

    #@46
    const/16 v4, 0x2f

    #@48
    if-ne v3, v4, :cond_5c

    #@4a
    .line 2182
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    .restart local v0       #newPath:Ljava/lang/String;
    goto :goto_3b

    #@5c
    .line 2184
    .end local v0           #newPath:Ljava/lang/String;
    :cond_5c
    new-instance v3, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    const-string v4, "/"

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v0

    #@73
    .restart local v0       #newPath:Ljava/lang/String;
    goto :goto_3b
.end method

.method static from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$PathPart;
    .registers 3
    .parameter "encoded"
    .parameter "decoded"

    #@0
    .prologue
    .line 2236
    if-nez p0, :cond_5

    #@2
    .line 2237
    sget-object v0, Landroid/net/Uri$PathPart;->NULL:Landroid/net/Uri$PathPart;

    #@4
    .line 2244
    :goto_4
    return-object v0

    #@5
    .line 2240
    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_e

    #@b
    .line 2241
    sget-object v0, Landroid/net/Uri$PathPart;->EMPTY:Landroid/net/Uri$PathPart;

    #@d
    goto :goto_4

    #@e
    .line 2244
    :cond_e
    new-instance v0, Landroid/net/Uri$PathPart;

    #@10
    invoke-direct {v0, p0, p1}, Landroid/net/Uri$PathPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    goto :goto_4
.end method

.method static fromDecoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;
    .registers 2
    .parameter "decoded"

    #@0
    .prologue
    .line 2226
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p0}, Landroid/net/Uri$PathPart;->from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method static fromEncoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;
    .registers 2
    .parameter "encoded"

    #@0
    .prologue
    .line 2217
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, v0}, Landroid/net/Uri$PathPart;->from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method static makeAbsolute(Landroid/net/Uri$PathPart;)Landroid/net/Uri$PathPart;
    .registers 9
    .parameter "oldPart"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2253
    iget-object v6, p0, Landroid/net/Uri$PathPart;->encoded:Ljava/lang/String;

    #@4
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@7
    move-result-object v7

    #@8
    if-eq v6, v7, :cond_20

    #@a
    move v1, v0

    #@b
    .line 2257
    .local v1, encodedCached:Z
    :goto_b
    if-eqz v1, :cond_22

    #@d
    iget-object v4, p0, Landroid/net/Uri$PathPart;->encoded:Ljava/lang/String;

    #@f
    .line 2259
    .local v4, oldPath:Ljava/lang/String;
    :goto_f
    if-eqz v4, :cond_1f

    #@11
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_1f

    #@17
    const-string v6, "/"

    #@19
    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_25

    #@1f
    .line 2275
    .end local p0
    :cond_1f
    :goto_1f
    return-object p0

    #@20
    .end local v1           #encodedCached:Z
    .end local v4           #oldPath:Ljava/lang/String;
    .restart local p0
    :cond_20
    move v1, v5

    #@21
    .line 2253
    goto :goto_b

    #@22
    .line 2257
    .restart local v1       #encodedCached:Z
    :cond_22
    iget-object v4, p0, Landroid/net/Uri$PathPart;->decoded:Ljava/lang/String;

    #@24
    goto :goto_f

    #@25
    .line 2265
    .restart local v4       #oldPath:Ljava/lang/String;
    :cond_25
    if-eqz v1, :cond_61

    #@27
    new-instance v6, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v7, "/"

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    iget-object v7, p0, Landroid/net/Uri$PathPart;->encoded:Ljava/lang/String;

    #@34
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    .line 2270
    .local v3, newEncoded:Ljava/lang/String;
    :goto_3c
    iget-object v6, p0, Landroid/net/Uri$PathPart;->decoded:Ljava/lang/String;

    #@3e
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    if-eq v6, v7, :cond_66

    #@44
    .line 2271
    .local v0, decodedCached:Z
    :goto_44
    if-eqz v0, :cond_68

    #@46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v6, "/"

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    iget-object v6, p0, Landroid/net/Uri$PathPart;->decoded:Ljava/lang/String;

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    .line 2275
    .local v2, newDecoded:Ljava/lang/String;
    :goto_5b
    new-instance p0, Landroid/net/Uri$PathPart;

    #@5d
    .end local p0
    invoke-direct {p0, v3, v2}, Landroid/net/Uri$PathPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    goto :goto_1f

    #@61
    .line 2265
    .end local v0           #decodedCached:Z
    .end local v2           #newDecoded:Ljava/lang/String;
    .end local v3           #newEncoded:Ljava/lang/String;
    .restart local p0
    :cond_61
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    goto :goto_3c

    #@66
    .restart local v3       #newEncoded:Ljava/lang/String;
    :cond_66
    move v0, v5

    #@67
    .line 2270
    goto :goto_44

    #@68
    .line 2271
    .restart local v0       #decodedCached:Z
    :cond_68
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    goto :goto_5b
.end method

.method static readFrom(Landroid/os/Parcel;)Landroid/net/Uri$PathPart;
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    .line 2198
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 2199
    .local v0, representation:I
    packed-switch v0, :pswitch_data_40

    #@7
    .line 2207
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Bad representation: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 2201
    :pswitch_20
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/net/Uri$PathPart;->from(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@2b
    move-result-object v1

    #@2c
    .line 2205
    :goto_2c
    return-object v1

    #@2d
    .line 2203
    :pswitch_2d
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v1}, Landroid/net/Uri$PathPart;->fromEncoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@34
    move-result-object v1

    #@35
    goto :goto_2c

    #@36
    .line 2205
    :pswitch_36
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v1}, Landroid/net/Uri$PathPart;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@3d
    move-result-object v1

    #@3e
    goto :goto_2c

    #@3f
    .line 2199
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_20
        :pswitch_2d
        :pswitch_36
    .end packed-switch
.end method


# virtual methods
.method getEncoded()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2109
    iget-object v1, p0, Landroid/net/Uri$PathPart;->encoded:Ljava/lang/String;

    #@2
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    if-eq v1, v2, :cond_e

    #@8
    const/4 v0, 0x1

    #@9
    .line 2112
    .local v0, hasEncoded:Z
    :goto_9
    if-eqz v0, :cond_10

    #@b
    iget-object v1, p0, Landroid/net/Uri$PathPart;->encoded:Ljava/lang/String;

    #@d
    :goto_d
    return-object v1

    #@e
    .line 2109
    .end local v0           #hasEncoded:Z
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_9

    #@10
    .line 2112
    .restart local v0       #hasEncoded:Z
    :cond_10
    iget-object v1, p0, Landroid/net/Uri$PathPart;->decoded:Ljava/lang/String;

    #@12
    const-string v2, "/"

    #@14
    invoke-static {v1, v2}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    iput-object v1, p0, Landroid/net/Uri$PathPart;->encoded:Ljava/lang/String;

    #@1a
    goto :goto_d
.end method

.method getPathSegments()Landroid/net/Uri$PathSegments;
    .registers 7

    #@0
    .prologue
    .line 2128
    iget-object v5, p0, Landroid/net/Uri$PathPart;->pathSegments:Landroid/net/Uri$PathSegments;

    #@2
    if-eqz v5, :cond_7

    #@4
    .line 2129
    iget-object v5, p0, Landroid/net/Uri$PathPart;->pathSegments:Landroid/net/Uri$PathSegments;

    #@6
    .line 2157
    :goto_6
    return-object v5

    #@7
    .line 2132
    :cond_7
    invoke-virtual {p0}, Landroid/net/Uri$PathPart;->getEncoded()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    .line 2133
    .local v2, path:Ljava/lang/String;
    if-nez v2, :cond_12

    #@d
    .line 2134
    sget-object v5, Landroid/net/Uri$PathSegments;->EMPTY:Landroid/net/Uri$PathSegments;

    #@f
    iput-object v5, p0, Landroid/net/Uri$PathPart;->pathSegments:Landroid/net/Uri$PathSegments;

    #@11
    goto :goto_6

    #@12
    .line 2137
    :cond_12
    new-instance v4, Landroid/net/Uri$PathSegmentsBuilder;

    #@14
    invoke-direct {v4}, Landroid/net/Uri$PathSegmentsBuilder;-><init>()V

    #@17
    .line 2139
    .local v4, segmentBuilder:Landroid/net/Uri$PathSegmentsBuilder;
    const/4 v3, 0x0

    #@18
    .line 2141
    .local v3, previous:I
    :goto_18
    const/16 v5, 0x2f

    #@1a
    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->indexOf(II)I

    #@1d
    move-result v0

    #@1e
    .local v0, current:I
    const/4 v5, -0x1

    #@1f
    if-le v0, v5, :cond_31

    #@21
    .line 2144
    if-ge v3, v0, :cond_2e

    #@23
    .line 2145
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    .line 2147
    .local v1, decodedSegment:Ljava/lang/String;
    invoke-virtual {v4, v1}, Landroid/net/Uri$PathSegmentsBuilder;->add(Ljava/lang/String;)V

    #@2e
    .line 2149
    .end local v1           #decodedSegment:Ljava/lang/String;
    :cond_2e
    add-int/lit8 v3, v0, 0x1

    #@30
    goto :goto_18

    #@31
    .line 2153
    :cond_31
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@34
    move-result v5

    #@35
    if-ge v3, v5, :cond_42

    #@37
    .line 2154
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v4, v5}, Landroid/net/Uri$PathSegmentsBuilder;->add(Ljava/lang/String;)V

    #@42
    .line 2157
    :cond_42
    invoke-virtual {v4}, Landroid/net/Uri$PathSegmentsBuilder;->build()Landroid/net/Uri$PathSegments;

    #@45
    move-result-object v5

    #@46
    iput-object v5, p0, Landroid/net/Uri$PathPart;->pathSegments:Landroid/net/Uri$PathSegments;

    #@48
    goto :goto_6
.end method
