.class public Landroid/net/LinkProperties;
.super Ljava/lang/Object;
.source "LinkProperties.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/LinkProperties$CompareResult;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/LinkProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDnses:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field private mDomainName:Ljava/lang/String;

.field private mHttpProxy:Landroid/net/ProxyProperties;

.field mIfaceName:Ljava/lang/String;

.field private mLinkAddresses:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/net/LinkAddress;",
            ">;"
        }
    .end annotation
.end field

.field private mRoutes:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 409
    new-instance v0, Landroid/net/LinkProperties$1;

    #@2
    invoke-direct {v0}, Landroid/net/LinkProperties$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/LinkProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@a
    .line 56
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@11
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@18
    .line 77
    invoke-virtual {p0}, Landroid/net/LinkProperties;->clear()V

    #@1b
    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/net/LinkProperties;)V
    .registers 8
    .parameter "source"

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    new-instance v4, Ljava/util/ArrayList;

    #@5
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v4, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@a
    .line 56
    new-instance v4, Ljava/util/ArrayList;

    #@c
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v4, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@11
    .line 57
    new-instance v4, Ljava/util/ArrayList;

    #@13
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v4, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@18
    .line 82
    if-eqz p1, :cond_7b

    #@1a
    .line 83
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    iput-object v4, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@20
    .line 84
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@23
    move-result-object v4

    #@24
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v1

    #@28
    .local v1, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_3a

    #@2e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31
    move-result-object v2

    #@32
    check-cast v2, Landroid/net/LinkAddress;

    #@34
    .local v2, l:Landroid/net/LinkAddress;
    iget-object v4, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@36
    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@39
    goto :goto_28

    #@3a
    .line 85
    .end local v2           #l:Landroid/net/LinkAddress;
    :cond_3a
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@3d
    move-result-object v4

    #@3e
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@41
    move-result-object v1

    #@42
    :goto_42
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@45
    move-result v4

    #@46
    if-eqz v4, :cond_54

    #@48
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4b
    move-result-object v0

    #@4c
    check-cast v0, Ljava/net/InetAddress;

    #@4e
    .local v0, i:Ljava/net/InetAddress;
    iget-object v4, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@50
    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@53
    goto :goto_42

    #@54
    .line 86
    .end local v0           #i:Ljava/net/InetAddress;
    :cond_54
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@57
    move-result-object v4

    #@58
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@5b
    move-result-object v1

    #@5c
    :goto_5c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_6e

    #@62
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@65
    move-result-object v3

    #@66
    check-cast v3, Landroid/net/RouteInfo;

    #@68
    .local v3, r:Landroid/net/RouteInfo;
    iget-object v4, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@6a
    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@6d
    goto :goto_5c

    #@6e
    .line 87
    .end local v3           #r:Landroid/net/RouteInfo;
    :cond_6e
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@71
    move-result-object v4

    #@72
    if-nez v4, :cond_7c

    #@74
    const/4 v4, 0x0

    #@75
    :goto_75
    iput-object v4, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@77
    .line 89
    iget-object v4, p1, Landroid/net/LinkProperties;->mDomainName:Ljava/lang/String;

    #@79
    iput-object v4, p0, Landroid/net/LinkProperties;->mDomainName:Ljava/lang/String;

    #@7b
    .line 91
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_7b
    return-void

    #@7c
    .line 87
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_7c
    new-instance v4, Landroid/net/ProxyProperties;

    #@7e
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@81
    move-result-object v5

    #@82
    invoke-direct {v4, v5}, Landroid/net/ProxyProperties;-><init>(Landroid/net/ProxyProperties;)V

    #@85
    goto :goto_75
.end method


# virtual methods
.method public addDns(Ljava/net/InetAddress;)V
    .registers 3
    .parameter "dns"

    #@0
    .prologue
    .line 118
    if-eqz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@4
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@7
    .line 119
    :cond_7
    return-void
.end method

.method public addLinkAddress(Landroid/net/LinkAddress;)V
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 110
    if-eqz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@4
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@7
    .line 111
    :cond_7
    return-void
.end method

.method public addRoute(Landroid/net/RouteInfo;)V
    .registers 3
    .parameter "route"

    #@0
    .prologue
    .line 126
    if-eqz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@4
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@7
    .line 127
    :cond_7
    return-void
.end method

.method public clear()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 147
    iput-object v1, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@3
    .line 148
    iget-object v0, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@5
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    #@8
    .line 149
    iget-object v0, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@a
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    #@d
    .line 150
    iget-object v0, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@f
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    #@12
    .line 151
    iput-object v1, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@14
    .line 152
    iput-object v1, p0, Landroid/net/LinkProperties;->mDomainName:Ljava/lang/String;

    #@16
    .line 153
    return-void
.end method

.method public compareAddresses(Landroid/net/LinkProperties;)Landroid/net/LinkProperties$CompareResult;
    .registers 7
    .parameter "target"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/LinkProperties;",
            ")",
            "Landroid/net/LinkProperties$CompareResult",
            "<",
            "Landroid/net/LinkAddress;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 287
    new-instance v2, Landroid/net/LinkProperties$CompareResult;

    #@2
    invoke-direct {v2}, Landroid/net/LinkProperties$CompareResult;-><init>()V

    #@5
    .line 288
    .local v2, result:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/LinkAddress;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@7
    iget-object v4, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@9
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@c
    iput-object v3, v2, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@e
    .line 289
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@10
    invoke-interface {v3}, Ljava/util/Collection;->clear()V

    #@13
    .line 290
    if-eqz p1, :cond_37

    #@15
    .line 291
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@18
    move-result-object v3

    #@19
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v0

    #@1d
    .local v0, i$:Ljava/util/Iterator;
    :cond_1d
    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_37

    #@23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/net/LinkAddress;

    #@29
    .line 292
    .local v1, newAddress:Landroid/net/LinkAddress;
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@2b
    invoke-interface {v3, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@2e
    move-result v3

    #@2f
    if-nez v3, :cond_1d

    #@31
    .line 293
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@33
    invoke-interface {v3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_1d

    #@37
    .line 297
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #newAddress:Landroid/net/LinkAddress;
    :cond_37
    return-object v2
.end method

.method public compareDnses(Landroid/net/LinkProperties;)Landroid/net/LinkProperties$CompareResult;
    .registers 7
    .parameter "target"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/LinkProperties;",
            ")",
            "Landroid/net/LinkProperties$CompareResult",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 317
    new-instance v2, Landroid/net/LinkProperties$CompareResult;

    #@2
    invoke-direct {v2}, Landroid/net/LinkProperties$CompareResult;-><init>()V

    #@5
    .line 319
    .local v2, result:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Ljava/net/InetAddress;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@7
    iget-object v4, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@9
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@c
    iput-object v3, v2, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@e
    .line 320
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@10
    invoke-interface {v3}, Ljava/util/Collection;->clear()V

    #@13
    .line 321
    if-eqz p1, :cond_37

    #@15
    .line 322
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@18
    move-result-object v3

    #@19
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v0

    #@1d
    .local v0, i$:Ljava/util/Iterator;
    :cond_1d
    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_37

    #@23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Ljava/net/InetAddress;

    #@29
    .line 323
    .local v1, newAddress:Ljava/net/InetAddress;
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@2b
    invoke-interface {v3, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@2e
    move-result v3

    #@2f
    if-nez v3, :cond_1d

    #@31
    .line 324
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@33
    invoke-interface {v3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_1d

    #@37
    .line 328
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #newAddress:Ljava/net/InetAddress;
    :cond_37
    return-object v2
.end method

.method public compareRoutes(Landroid/net/LinkProperties;)Landroid/net/LinkProperties$CompareResult;
    .registers 7
    .parameter "target"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/LinkProperties;",
            ")",
            "Landroid/net/LinkProperties$CompareResult",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 347
    new-instance v2, Landroid/net/LinkProperties$CompareResult;

    #@2
    invoke-direct {v2}, Landroid/net/LinkProperties$CompareResult;-><init>()V

    #@5
    .line 349
    .local v2, result:Landroid/net/LinkProperties$CompareResult;,"Landroid/net/LinkProperties$CompareResult<Landroid/net/RouteInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@7
    iget-object v4, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@9
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@c
    iput-object v3, v2, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@e
    .line 350
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@10
    invoke-interface {v3}, Ljava/util/Collection;->clear()V

    #@13
    .line 351
    if-eqz p1, :cond_37

    #@15
    .line 352
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@18
    move-result-object v3

    #@19
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v0

    #@1d
    .local v0, i$:Ljava/util/Iterator;
    :cond_1d
    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_37

    #@23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/net/RouteInfo;

    #@29
    .line 353
    .local v1, r:Landroid/net/RouteInfo;
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->removed:Ljava/util/Collection;

    #@2b
    invoke-interface {v3, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@2e
    move-result v3

    #@2f
    if-nez v3, :cond_1d

    #@31
    .line 354
    iget-object v3, v2, Landroid/net/LinkProperties$CompareResult;->added:Ljava/util/Collection;

    #@33
    invoke-interface {v3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_1d

    #@37
    .line 358
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #r:Landroid/net/RouteInfo;
    :cond_37
    return-object v2
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 160
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 257
    if-ne p0, p1, :cond_5

    #@4
    .line 263
    :cond_4
    :goto_4
    return v1

    #@5
    .line 259
    :cond_5
    instance-of v3, p1, Landroid/net/LinkProperties;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 261
    check-cast v0, Landroid/net/LinkProperties;

    #@e
    .line 263
    .local v0, target:Landroid/net/LinkProperties;
    invoke-virtual {p0, v0}, Landroid/net/LinkProperties;->isIdenticalInterfaceName(Landroid/net/LinkProperties;)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_2c

    #@14
    invoke-virtual {p0, v0}, Landroid/net/LinkProperties;->isIdenticalAddresses(Landroid/net/LinkProperties;)Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_2c

    #@1a
    invoke-virtual {p0, v0}, Landroid/net/LinkProperties;->isIdenticalDnses(Landroid/net/LinkProperties;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2c

    #@20
    invoke-virtual {p0, v0}, Landroid/net/LinkProperties;->isIdenticalRoutes(Landroid/net/LinkProperties;)Z

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_2c

    #@26
    invoke-virtual {p0, v0}, Landroid/net/LinkProperties;->isIdenticalHttpProxy(Landroid/net/LinkProperties;)Z

    #@29
    move-result v3

    #@2a
    if-nez v3, :cond_4

    #@2c
    :cond_2c
    move v1, v2

    #@2d
    goto :goto_4
.end method

.method public getAddresses()Ljava/util/Collection;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 103
    .local v0, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    iget-object v3, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@7
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1f

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/net/LinkAddress;

    #@17
    .line 104
    .local v2, linkAddress:Landroid/net/LinkAddress;
    invoke-virtual {v2}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@1a
    move-result-object v3

    #@1b
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@1e
    goto :goto_b

    #@1f
    .line 106
    .end local v2           #linkAddress:Landroid/net/LinkAddress;
    :cond_1f
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@22
    move-result-object v3

    #@23
    return-object v3
.end method

.method public getDnses()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDomainName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/net/LinkProperties;->mDomainName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHttpProxy()Landroid/net/ProxyProperties;
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@2
    return-object v0
.end method

.method public getInterfaceName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLinkAddresses()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/LinkAddress;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getRoutes()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 369
    iget-object v1, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@3
    if-nez v1, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v1, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@8
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@b
    move-result v1

    #@c
    iget-object v2, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@e
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    #@11
    move-result v2

    #@12
    mul-int/lit8 v2, v2, 0x1f

    #@14
    add-int/2addr v1, v2

    #@15
    iget-object v2, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@17
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    #@1a
    move-result v2

    #@1b
    mul-int/lit8 v2, v2, 0x25

    #@1d
    add-int/2addr v1, v2

    #@1e
    iget-object v2, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@20
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    #@23
    move-result v2

    #@24
    mul-int/lit8 v2, v2, 0x29

    #@26
    add-int/2addr v1, v2

    #@27
    iget-object v2, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@29
    if-nez v2, :cond_2d

    #@2b
    :goto_2b
    add-int/2addr v0, v1

    #@2c
    goto :goto_5

    #@2d
    :cond_2d
    iget-object v0, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@2f
    invoke-virtual {v0}, Landroid/net/ProxyProperties;->hashCode()I

    #@32
    move-result v0

    #@33
    goto :goto_2b
.end method

.method public isIdenticalAddresses(Landroid/net/LinkProperties;)Z
    .registers 6
    .parameter "target"

    #@0
    .prologue
    .line 200
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@3
    move-result-object v1

    #@4
    .line 201
    .local v1, targetAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@7
    move-result-object v0

    #@8
    .line 202
    .local v0, sourceAddresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    #@b
    move-result v2

    #@c
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@f
    move-result v3

    #@10
    if-ne v2, v3, :cond_17

    #@12
    invoke-interface {v0, v1}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@15
    move-result v2

    #@16
    :goto_16
    return v2

    #@17
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_16
.end method

.method public isIdenticalDnses(Landroid/net/LinkProperties;)Z
    .registers 5
    .parameter "target"

    #@0
    .prologue
    .line 213
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@3
    move-result-object v0

    #@4
    .line 214
    .local v0, targetDnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    iget-object v1, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@6
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@9
    move-result v1

    #@a
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    #@d
    move-result v2

    #@e
    if-ne v1, v2, :cond_17

    #@10
    iget-object v1, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@12
    invoke-interface {v1, v0}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@15
    move-result v1

    #@16
    :goto_16
    return v1

    #@17
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_16
.end method

.method public isIdenticalHttpProxy(Landroid/net/LinkProperties;)Z
    .registers 4
    .parameter "target"

    #@0
    .prologue
    .line 237
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@3
    move-result-object v0

    #@4
    if-nez v0, :cond_10

    #@6
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@9
    move-result-object v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d

    #@10
    :cond_10
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyProperties;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Landroid/net/ProxyProperties;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    goto :goto_d
.end method

.method public isIdenticalInterfaceName(Landroid/net/LinkProperties;)Z
    .registers 4
    .parameter "target"

    #@0
    .prologue
    .line 190
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public isIdenticalRoutes(Landroid/net/LinkProperties;)Z
    .registers 5
    .parameter "target"

    #@0
    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@3
    move-result-object v0

    #@4
    .line 226
    .local v0, targetRoutes:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    iget-object v1, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@6
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@9
    move-result v1

    #@a
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    #@d
    move-result v2

    #@e
    if-ne v1, v2, :cond_17

    #@10
    iget-object v1, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@12
    invoke-interface {v1, v0}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    #@15
    move-result v1

    #@16
    :goto_16
    return v1

    #@17
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_16
.end method

.method public isIpv4Connected()Z
    .registers 7

    #@0
    .prologue
    .line 448
    const/4 v4, 0x0

    #@1
    .line 449
    .local v4, ret:Z
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@4
    move-result-object v1

    #@5
    .line 451
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v2

    #@9
    .local v2, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_35

    #@f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Ljava/net/InetAddress;

    #@15
    .line 452
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet4Address;

    #@17
    if-eqz v5, :cond_9

    #@19
    move-object v3, v0

    #@1a
    .line 453
    check-cast v3, Ljava/net/Inet4Address;

    #@1c
    .line 454
    .local v3, i4addr:Ljava/net/Inet4Address;
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isAnyLocalAddress()Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_9

    #@22
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isLinkLocalAddress()Z

    #@25
    move-result v5

    #@26
    if-nez v5, :cond_9

    #@28
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isLoopbackAddress()Z

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_9

    #@2e
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isMulticastAddress()Z

    #@31
    move-result v5

    #@32
    if-nez v5, :cond_9

    #@34
    .line 456
    const/4 v4, 0x1

    #@35
    .line 461
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i4addr:Ljava/net/Inet4Address;
    :cond_35
    return v4
.end method

.method public isIpv6Connected()Z
    .registers 7

    #@0
    .prologue
    .line 465
    const/4 v4, 0x0

    #@1
    .line 466
    .local v4, ret:Z
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@4
    move-result-object v1

    #@5
    .line 468
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v2

    #@9
    .local v2, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_35

    #@f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Ljava/net/InetAddress;

    #@15
    .line 469
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet6Address;

    #@17
    if-eqz v5, :cond_9

    #@19
    move-object v3, v0

    #@1a
    .line 470
    check-cast v3, Ljava/net/Inet6Address;

    #@1c
    .line 471
    .local v3, i6addr:Ljava/net/Inet6Address;
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isAnyLocalAddress()Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_9

    #@22
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isLinkLocalAddress()Z

    #@25
    move-result v5

    #@26
    if-nez v5, :cond_9

    #@28
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isLoopbackAddress()Z

    #@2b
    move-result v5

    #@2c
    if-nez v5, :cond_9

    #@2e
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isMulticastAddress()Z

    #@31
    move-result v5

    #@32
    if-nez v5, :cond_9

    #@34
    .line 473
    const/4 v4, 0x1

    #@35
    .line 478
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i6addr:Ljava/net/Inet6Address;
    :cond_35
    return v4
.end method

.method public setDomainName(Ljava/lang/String;)V
    .registers 2
    .parameter "domainName"

    #@0
    .prologue
    .line 140
    iput-object p1, p0, Landroid/net/LinkProperties;->mDomainName:Ljava/lang/String;

    #@2
    .line 141
    return-void
.end method

.method public setHttpProxy(Landroid/net/ProxyProperties;)V
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 133
    iput-object p1, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@2
    .line 134
    return-void
.end method

.method public setInterfaceName(Ljava/lang/String;)V
    .registers 2
    .parameter "iface"

    #@0
    .prologue
    .line 94
    iput-object p1, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@2
    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 11

    #@0
    .prologue
    .line 165
    iget-object v8, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@2
    if-nez v8, :cond_36

    #@4
    const-string v3, ""

    #@6
    .line 167
    .local v3, ifaceName:Ljava/lang/String;
    :goto_6
    const-string v4, "LinkAddresses: ["

    #@8
    .line 168
    .local v4, linkAddresses:Ljava/lang/String;
    iget-object v8, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@a
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v8

    #@12
    if-eqz v8, :cond_52

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/net/LinkAddress;

    #@1a
    .local v0, addr:Landroid/net/LinkAddress;
    new-instance v8, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v0}, Landroid/net/LinkAddress;->toString()Ljava/lang/String;

    #@26
    move-result-object v9

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    const-string v9, ","

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    goto :goto_e

    #@36
    .line 165
    .end local v0           #addr:Landroid/net/LinkAddress;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #ifaceName:Ljava/lang/String;
    .end local v4           #linkAddresses:Ljava/lang/String;
    :cond_36
    new-instance v8, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v9, "InterfaceName: "

    #@3d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    iget-object v9, p0, Landroid/net/LinkProperties;->mIfaceName:Ljava/lang/String;

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    const-string v9, " "

    #@49
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    goto :goto_6

    #@52
    .line 169
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #ifaceName:Ljava/lang/String;
    .restart local v4       #linkAddresses:Ljava/lang/String;
    :cond_52
    new-instance v8, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v8

    #@5b
    const-string v9, "] "

    #@5d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    .line 171
    const-string v1, "DnsAddresses: ["

    #@67
    .line 172
    .local v1, dns:Ljava/lang/String;
    iget-object v8, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@69
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@6c
    move-result-object v2

    #@6d
    :goto_6d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@70
    move-result v8

    #@71
    if-eqz v8, :cond_95

    #@73
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@76
    move-result-object v0

    #@77
    check-cast v0, Ljava/net/InetAddress;

    #@79
    .local v0, addr:Ljava/net/InetAddress;
    new-instance v8, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@85
    move-result-object v9

    #@86
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v8

    #@8a
    const-string v9, ","

    #@8c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v1

    #@94
    goto :goto_6d

    #@95
    .line 173
    .end local v0           #addr:Ljava/net/InetAddress;
    :cond_95
    new-instance v8, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v8

    #@9e
    const-string v9, "] "

    #@a0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v8

    #@a4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v1

    #@a8
    .line 175
    const-string v7, "Routes: ["

    #@aa
    .line 176
    .local v7, routes:Ljava/lang/String;
    iget-object v8, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@ac
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@af
    move-result-object v2

    #@b0
    :goto_b0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@b3
    move-result v8

    #@b4
    if-eqz v8, :cond_d8

    #@b6
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b9
    move-result-object v6

    #@ba
    check-cast v6, Landroid/net/RouteInfo;

    #@bc
    .local v6, route:Landroid/net/RouteInfo;
    new-instance v8, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v8

    #@c5
    invoke-virtual {v6}, Landroid/net/RouteInfo;->toString()Ljava/lang/String;

    #@c8
    move-result-object v9

    #@c9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v8

    #@cd
    const-string v9, ","

    #@cf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v7

    #@d7
    goto :goto_b0

    #@d8
    .line 177
    .end local v6           #route:Landroid/net/RouteInfo;
    :cond_d8
    new-instance v8, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v8

    #@e1
    const-string v9, "] "

    #@e3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v8

    #@e7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v7

    #@eb
    .line 178
    iget-object v8, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@ed
    if-nez v8, :cond_10f

    #@ef
    const-string v5, ""

    #@f1
    .line 180
    .local v5, proxy:Ljava/lang/String;
    :goto_f1
    new-instance v8, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v8

    #@fa
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v8

    #@fe
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v8

    #@102
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v8

    #@106
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v8

    #@10a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v8

    #@10e
    return-object v8

    #@10f
    .line 178
    .end local v5           #proxy:Ljava/lang/String;
    :cond_10f
    new-instance v8, Ljava/lang/StringBuilder;

    #@111
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    const-string v9, "HttpProxy: "

    #@116
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v8

    #@11a
    iget-object v9, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@11c
    invoke-virtual {v9}, Landroid/net/ProxyProperties;->toString()Ljava/lang/String;

    #@11f
    move-result-object v9

    #@120
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v8

    #@124
    const-string v9, " "

    #@126
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v8

    #@12a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v5

    #@12e
    goto :goto_f1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 381
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 382
    iget-object v4, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@9
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    #@c
    move-result v4

    #@d
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 383
    iget-object v4, p0, Landroid/net/LinkProperties;->mLinkAddresses:Ljava/util/Collection;

    #@12
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_26

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Landroid/net/LinkAddress;

    #@22
    .line 384
    .local v2, linkAddress:Landroid/net/LinkAddress;
    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@25
    goto :goto_16

    #@26
    .line 387
    .end local v2           #linkAddress:Landroid/net/LinkAddress;
    :cond_26
    iget-object v4, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@28
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    #@2b
    move-result v4

    #@2c
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 388
    iget-object v4, p0, Landroid/net/LinkProperties;->mDnses:Ljava/util/Collection;

    #@31
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@34
    move-result-object v1

    #@35
    :goto_35
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@38
    move-result v4

    #@39
    if-eqz v4, :cond_49

    #@3b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Ljava/net/InetAddress;

    #@41
    .line 389
    .local v0, d:Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@44
    move-result-object v4

    #@45
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@48
    goto :goto_35

    #@49
    .line 392
    .end local v0           #d:Ljava/net/InetAddress;
    :cond_49
    iget-object v4, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@4b
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    #@4e
    move-result v4

    #@4f
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    .line 393
    iget-object v4, p0, Landroid/net/LinkProperties;->mRoutes:Ljava/util/Collection;

    #@54
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@57
    move-result-object v1

    #@58
    :goto_58
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@5b
    move-result v4

    #@5c
    if-eqz v4, :cond_68

    #@5e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@61
    move-result-object v3

    #@62
    check-cast v3, Landroid/net/RouteInfo;

    #@64
    .line 394
    .local v3, route:Landroid/net/RouteInfo;
    invoke-virtual {p1, v3, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@67
    goto :goto_58

    #@68
    .line 397
    .end local v3           #route:Landroid/net/RouteInfo;
    :cond_68
    iget-object v4, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@6a
    if-eqz v4, :cond_76

    #@6c
    .line 398
    const/4 v4, 0x1

    #@6d
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeByte(B)V

    #@70
    .line 399
    iget-object v4, p0, Landroid/net/LinkProperties;->mHttpProxy:Landroid/net/ProxyProperties;

    #@72
    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@75
    .line 403
    :goto_75
    return-void

    #@76
    .line 401
    :cond_76
    const/4 v4, 0x0

    #@77
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeByte(B)V

    #@7a
    goto :goto_75
.end method
