.class public Landroid/net/LocalSocket;
.super Ljava/lang/Object;
.source "LocalSocket.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private impl:Landroid/net/LocalSocketImpl;

.field private volatile implCreated:Z

.field private isBound:Z

.field private isConnected:Z

.field private localAddress:Landroid/net/LocalSocketAddress;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 42
    new-instance v0, Landroid/net/LocalSocketImpl;

    #@3
    invoke-direct {v0}, Landroid/net/LocalSocketImpl;-><init>()V

    #@6
    invoke-direct {p0, v0}, Landroid/net/LocalSocket;-><init>(Landroid/net/LocalSocketImpl;)V

    #@9
    .line 43
    iput-boolean v1, p0, Landroid/net/LocalSocket;->isBound:Z

    #@b
    .line 44
    iput-boolean v1, p0, Landroid/net/LocalSocket;->isConnected:Z

    #@d
    .line 45
    return-void
.end method

.method constructor <init>(Landroid/net/LocalSocketImpl;)V
    .registers 3
    .parameter "impl"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 61
    iput-object p1, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@6
    .line 62
    iput-boolean v0, p0, Landroid/net/LocalSocket;->isConnected:Z

    #@8
    .line 63
    iput-boolean v0, p0, Landroid/net/LocalSocket;->isBound:Z

    #@a
    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/io/FileDescriptor;)V
    .registers 4
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 51
    new-instance v0, Landroid/net/LocalSocketImpl;

    #@3
    invoke-direct {v0, p1}, Landroid/net/LocalSocketImpl;-><init>(Ljava/io/FileDescriptor;)V

    #@6
    invoke-direct {p0, v0}, Landroid/net/LocalSocket;-><init>(Landroid/net/LocalSocketImpl;)V

    #@9
    .line 52
    iput-boolean v1, p0, Landroid/net/LocalSocket;->isBound:Z

    #@b
    .line 53
    iput-boolean v1, p0, Landroid/net/LocalSocket;->isConnected:Z

    #@d
    .line 54
    return-void
.end method

.method private implCreateIfNeeded()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    iget-boolean v0, p0, Landroid/net/LocalSocket;->implCreated:Z

    #@2
    if-nez v0, :cond_13

    #@4
    .line 81
    monitor-enter p0

    #@5
    .line 82
    :try_start_5
    iget-boolean v0, p0, Landroid/net/LocalSocket;->implCreated:Z
    :try_end_7
    .catchall {:try_start_5 .. :try_end_7} :catchall_19

    #@7
    if-nez v0, :cond_12

    #@9
    .line 84
    :try_start_9
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@b
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/net/LocalSocketImpl;->create(Z)V
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_14

    #@f
    .line 86
    const/4 v0, 0x1

    #@10
    :try_start_10
    iput-boolean v0, p0, Landroid/net/LocalSocket;->implCreated:Z

    #@12
    .line 89
    :cond_12
    monitor-exit p0

    #@13
    .line 91
    :cond_13
    return-void

    #@14
    .line 86
    :catchall_14
    move-exception v0

    #@15
    const/4 v1, 0x1

    #@16
    iput-boolean v1, p0, Landroid/net/LocalSocket;->implCreated:Z

    #@18
    throw v0

    #@19
    .line 89
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_10 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method


# virtual methods
.method public bind(Landroid/net/LocalSocketAddress;)V
    .registers 4
    .parameter "bindpoint"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 122
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@3
    .line 124
    monitor-enter p0

    #@4
    .line 125
    :try_start_4
    iget-boolean v0, p0, Landroid/net/LocalSocket;->isBound:Z

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 126
    new-instance v0, Ljava/io/IOException;

    #@a
    const-string v1, "already bound"

    #@c
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 132
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_10

    #@12
    throw v0

    #@13
    .line 129
    :cond_13
    :try_start_13
    iput-object p1, p0, Landroid/net/LocalSocket;->localAddress:Landroid/net/LocalSocketAddress;

    #@15
    .line 130
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@17
    iget-object v1, p0, Landroid/net/LocalSocket;->localAddress:Landroid/net/LocalSocketAddress;

    #@19
    invoke-virtual {v0, v1}, Landroid/net/LocalSocketImpl;->bind(Landroid/net/LocalSocketAddress;)V

    #@1c
    .line 131
    const/4 v0, 0x1

    #@1d
    iput-boolean v0, p0, Landroid/net/LocalSocket;->isBound:Z

    #@1f
    .line 132
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_13 .. :try_end_20} :catchall_10

    #@20
    .line 133
    return-void
.end method

.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 173
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@3
    .line 174
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@5
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->close()V

    #@8
    .line 175
    return-void
.end method

.method public connect(Landroid/net/LocalSocketAddress;)V
    .registers 4
    .parameter "endpoint"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 102
    monitor-enter p0

    #@1
    .line 103
    :try_start_1
    iget-boolean v0, p0, Landroid/net/LocalSocket;->isConnected:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 104
    new-instance v0, Ljava/io/IOException;

    #@7
    const-string v1, "already connected"

    #@9
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 111
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0

    #@10
    .line 107
    :cond_10
    :try_start_10
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@13
    .line 108
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@15
    const/4 v1, 0x0

    #@16
    invoke-virtual {v0, p1, v1}, Landroid/net/LocalSocketImpl;->connect(Landroid/net/LocalSocketAddress;I)V

    #@19
    .line 109
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Landroid/net/LocalSocket;->isConnected:Z

    #@1c
    .line 110
    const/4 v0, 0x1

    #@1d
    iput-boolean v0, p0, Landroid/net/LocalSocket;->isBound:Z

    #@1f
    .line 111
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_10 .. :try_end_20} :catchall_d

    #@20
    .line 112
    return-void
.end method

.method public connect(Landroid/net/LocalSocketAddress;I)V
    .registers 4
    .parameter "endpoint"
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 254
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getAncillaryFileDescriptors()[Ljava/io/FileDescriptor;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 280
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->getAncillaryFileDescriptors()[Ljava/io/FileDescriptor;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFileDescriptor()Ljava/io/FileDescriptor;
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 151
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@3
    .line 152
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@5
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->getInputStream()Ljava/io/InputStream;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getLocalSocketAddress()Landroid/net/LocalSocketAddress;
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/net/LocalSocket;->localAddress:Landroid/net/LocalSocketAddress;

    #@2
    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 162
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@3
    .line 163
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@5
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->getOutputStream()Ljava/io/OutputStream;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getPeerCredentials()Landroid/net/Credentials;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->getPeerCredentials()Landroid/net/Credentials;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getReceiveBufferSize()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    const/16 v1, 0x1002

    #@4
    invoke-virtual {v0, v1}, Landroid/net/LocalSocketImpl;->getOption(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/Integer;

    #@a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getRemoteSocketAddress()Landroid/net/LocalSocketAddress;
    .registers 2

    #@0
    .prologue
    .line 223
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getSendBufferSize()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    const/16 v1, 0x1001

    #@4
    invoke-virtual {v0, v1}, Landroid/net/LocalSocketImpl;->getOption(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/Integer;

    #@a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getSoTimeout()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    const/16 v1, 0x1006

    #@4
    invoke-virtual {v0, v1}, Landroid/net/LocalSocketImpl;->getOption(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/Integer;

    #@a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public declared-synchronized isBound()Z
    .registers 2

    #@0
    .prologue
    .line 238
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/net/LocalSocket;->isBound:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public isClosed()Z
    .registers 2

    #@0
    .prologue
    .line 233
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 228
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/net/LocalSocket;->isConnected:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public isInputShutdown()Z
    .registers 2

    #@0
    .prologue
    .line 248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public isOutputShutdown()Z
    .registers 2

    #@0
    .prologue
    .line 243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public setFileDescriptorsForSend([Ljava/io/FileDescriptor;)V
    .registers 3
    .parameter "fds"

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    invoke-virtual {v0, p1}, Landroid/net/LocalSocketImpl;->setFileDescriptorsForSend([Ljava/io/FileDescriptor;)V

    #@5
    .line 267
    return-void
.end method

.method public setReceiveBufferSize(I)V
    .registers 5
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    const/16 v1, 0x1002

    #@4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/net/LocalSocketImpl;->setOption(ILjava/lang/Object;)V

    #@b
    .line 199
    return-void
.end method

.method public setSendBufferSize(I)V
    .registers 5
    .parameter "n"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    const/16 v1, 0x1001

    #@4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/net/LocalSocketImpl;->setOption(ILjava/lang/Object;)V

    #@b
    .line 215
    return-void
.end method

.method public setSoTimeout(I)V
    .registers 5
    .parameter "n"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@2
    const/16 v1, 0x1006

    #@4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/net/LocalSocketImpl;->setOption(ILjava/lang/Object;)V

    #@b
    .line 207
    return-void
.end method

.method public shutdownInput()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@3
    .line 184
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@5
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->shutdownInput()V

    #@8
    .line 185
    return-void
.end method

.method public shutdownOutput()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 193
    invoke-direct {p0}, Landroid/net/LocalSocket;->implCreateIfNeeded()V

    #@3
    .line 194
    iget-object v0, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@5
    invoke-virtual {v0}, Landroid/net/LocalSocketImpl;->shutdownOutput()V

    #@8
    .line 195
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, " impl:"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Landroid/net/LocalSocket;->impl:Landroid/net/LocalSocketImpl;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method
