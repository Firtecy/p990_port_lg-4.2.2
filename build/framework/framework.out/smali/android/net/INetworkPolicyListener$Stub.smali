.class public abstract Landroid/net/INetworkPolicyListener$Stub;
.super Landroid/os/Binder;
.source "INetworkPolicyListener.java"

# interfaces
.implements Landroid/net/INetworkPolicyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/INetworkPolicyListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/INetworkPolicyListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.INetworkPolicyListener"

.field static final TRANSACTION_onMeteredIfacesChanged:I = 0x2

.field static final TRANSACTION_onRestrictBackgroundChanged:I = 0x3

.field static final TRANSACTION_onUidRulesChanged:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.net.INetworkPolicyListener"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/INetworkPolicyListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/INetworkPolicyListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.net.INetworkPolicyListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/INetworkPolicyListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/net/INetworkPolicyListener;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/net/INetworkPolicyListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/INetworkPolicyListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 39
    sparse-switch p1, :sswitch_data_40

    #@4
    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 43
    :sswitch_9
    const-string v3, "android.net.INetworkPolicyListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 48
    :sswitch_f
    const-string v3, "android.net.INetworkPolicyListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 52
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 53
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/INetworkPolicyListener$Stub;->onUidRulesChanged(II)V

    #@1f
    goto :goto_8

    #@20
    .line 58
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_20
    const-string v3, "android.net.INetworkPolicyListener"

    #@22
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 61
    .local v0, _arg0:[Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyListener$Stub;->onMeteredIfacesChanged([Ljava/lang/String;)V

    #@2c
    goto :goto_8

    #@2d
    .line 66
    .end local v0           #_arg0:[Ljava/lang/String;
    :sswitch_2d
    const-string v3, "android.net.INetworkPolicyListener"

    #@2f
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_3d

    #@38
    move v0, v2

    #@39
    .line 69
    .local v0, _arg0:Z
    :goto_39
    invoke-virtual {p0, v0}, Landroid/net/INetworkPolicyListener$Stub;->onRestrictBackgroundChanged(Z)V

    #@3c
    goto :goto_8

    #@3d
    .line 68
    .end local v0           #_arg0:Z
    :cond_3d
    const/4 v0, 0x0

    #@3e
    goto :goto_39

    #@3f
    .line 39
    nop

    #@40
    :sswitch_data_40
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_20
        0x3 -> :sswitch_2d
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
