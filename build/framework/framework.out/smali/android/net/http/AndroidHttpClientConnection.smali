.class public Landroid/net/http/AndroidHttpClientConnection;
.super Ljava/lang/Object;
.source "AndroidHttpClientConnection.java"

# interfaces
.implements Lorg/apache/http/HttpInetConnection;
.implements Lorg/apache/http/HttpConnection;


# instance fields
.field private final entityserializer:Lorg/apache/http/impl/entity/EntitySerializer;

.field private inbuffer:Lorg/apache/http/io/SessionInputBuffer;

.field private maxHeaderCount:I

.field private maxLineLength:I

.field private metrics:Lorg/apache/http/impl/HttpConnectionMetricsImpl;

.field private volatile open:Z

.field private outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

.field private requestWriter:Lorg/apache/http/io/HttpMessageWriter;

.field private socket:Ljava/net/Socket;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 70
    iput-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@6
    .line 71
    iput-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

    #@8
    .line 78
    iput-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->requestWriter:Lorg/apache/http/io/HttpMessageWriter;

    #@a
    .line 79
    iput-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->metrics:Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    #@c
    .line 81
    iput-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@e
    .line 84
    new-instance v0, Lorg/apache/http/impl/entity/EntitySerializer;

    #@10
    new-instance v1, Lorg/apache/http/impl/entity/StrictContentLengthStrategy;

    #@12
    invoke-direct {v1}, Lorg/apache/http/impl/entity/StrictContentLengthStrategy;-><init>()V

    #@15
    invoke-direct {v0, v1}, Lorg/apache/http/impl/entity/EntitySerializer;-><init>(Lorg/apache/http/entity/ContentLengthStrategy;)V

    #@18
    iput-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->entityserializer:Lorg/apache/http/impl/entity/EntitySerializer;

    #@1a
    .line 86
    return-void
.end method

.method private assertNotOpen()V
    .registers 3

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 147
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Connection is already open"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 149
    :cond_c
    return-void
.end method

.method private assertOpen()V
    .registers 3

    #@0
    .prologue
    .line 152
    iget-boolean v0, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 153
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Connection is not open"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 155
    :cond_c
    return-void
.end method

.method private determineLength(Landroid/net/http/Headers;)J
    .registers 10
    .parameter "headers"

    #@0
    .prologue
    const-wide/16 v4, -0x1

    #@2
    .line 417
    invoke-virtual {p1}, Landroid/net/http/Headers;->getTransferEncoding()J

    #@5
    move-result-wide v2

    #@6
    .line 420
    .local v2, transferEncoding:J
    const-wide/16 v6, 0x0

    #@8
    cmp-long v6, v2, v6

    #@a
    if-gez v6, :cond_d

    #@c
    .line 427
    .end local v2           #transferEncoding:J
    :goto_c
    return-wide v2

    #@d
    .line 423
    .restart local v2       #transferEncoding:J
    :cond_d
    invoke-virtual {p1}, Landroid/net/http/Headers;->getContentLength()J

    #@10
    move-result-wide v0

    #@11
    .line 424
    .local v0, contentlen:J
    cmp-long v6, v0, v4

    #@13
    if-lez v6, :cond_17

    #@15
    move-wide v2, v0

    #@16
    .line 425
    goto :goto_c

    #@17
    :cond_17
    move-wide v2, v4

    #@18
    .line 427
    goto :goto_c
.end method


# virtual methods
.method public bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V
    .registers 9
    .parameter "socket"
    .parameter "params"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, -0x1

    #@2
    .line 97
    if-nez p1, :cond_c

    #@4
    .line 98
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v3, "Socket may not be null"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 100
    :cond_c
    if-nez p2, :cond_16

    #@e
    .line 101
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v3, "HTTP parameters may not be null"

    #@12
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2

    #@16
    .line 103
    :cond_16
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertNotOpen()V

    #@19
    .line 104
    invoke-static {p2}, Lorg/apache/http/params/HttpConnectionParams;->getTcpNoDelay(Lorg/apache/http/params/HttpParams;)Z

    #@1c
    move-result v2

    #@1d
    invoke-virtual {p1, v2}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    #@20
    .line 105
    invoke-static {p2}, Lorg/apache/http/params/HttpConnectionParams;->getSoTimeout(Lorg/apache/http/params/HttpParams;)I

    #@23
    move-result v2

    #@24
    invoke-virtual {p1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    #@27
    .line 107
    invoke-static {p2}, Lorg/apache/http/params/HttpConnectionParams;->getLinger(Lorg/apache/http/params/HttpParams;)I

    #@2a
    move-result v1

    #@2b
    .line 108
    .local v1, linger:I
    if-ltz v1, :cond_33

    #@2d
    .line 109
    if-lez v1, :cond_77

    #@2f
    move v2, v3

    #@30
    :goto_30
    invoke-virtual {p1, v2, v1}, Ljava/net/Socket;->setSoLinger(ZI)V

    #@33
    .line 111
    :cond_33
    iput-object p1, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@35
    .line 113
    invoke-static {p2}, Lorg/apache/http/params/HttpConnectionParams;->getSocketBufferSize(Lorg/apache/http/params/HttpParams;)I

    #@38
    move-result v0

    #@39
    .line 114
    .local v0, buffersize:I
    new-instance v2, Lorg/apache/http/impl/io/SocketInputBuffer;

    #@3b
    invoke-direct {v2, p1, v0, p2}, Lorg/apache/http/impl/io/SocketInputBuffer;-><init>(Ljava/net/Socket;ILorg/apache/http/params/HttpParams;)V

    #@3e
    iput-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@40
    .line 115
    new-instance v2, Lorg/apache/http/impl/io/SocketOutputBuffer;

    #@42
    invoke-direct {v2, p1, v0, p2}, Lorg/apache/http/impl/io/SocketOutputBuffer;-><init>(Ljava/net/Socket;ILorg/apache/http/params/HttpParams;)V

    #@45
    iput-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

    #@47
    .line 117
    const-string v2, "http.connection.max-header-count"

    #@49
    invoke-interface {p2, v2, v4}, Lorg/apache/http/params/HttpParams;->getIntParameter(Ljava/lang/String;I)I

    #@4c
    move-result v2

    #@4d
    iput v2, p0, Landroid/net/http/AndroidHttpClientConnection;->maxHeaderCount:I

    #@4f
    .line 119
    const-string v2, "http.connection.max-line-length"

    #@51
    invoke-interface {p2, v2, v4}, Lorg/apache/http/params/HttpParams;->getIntParameter(Ljava/lang/String;I)I

    #@54
    move-result v2

    #@55
    iput v2, p0, Landroid/net/http/AndroidHttpClientConnection;->maxLineLength:I

    #@57
    .line 122
    new-instance v2, Lorg/apache/http/impl/io/HttpRequestWriter;

    #@59
    iget-object v4, p0, Landroid/net/http/AndroidHttpClientConnection;->outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

    #@5b
    const/4 v5, 0x0

    #@5c
    invoke-direct {v2, v4, v5, p2}, Lorg/apache/http/impl/io/HttpRequestWriter;-><init>(Lorg/apache/http/io/SessionOutputBuffer;Lorg/apache/http/message/LineFormatter;Lorg/apache/http/params/HttpParams;)V

    #@5f
    iput-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->requestWriter:Lorg/apache/http/io/HttpMessageWriter;

    #@61
    .line 124
    new-instance v2, Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    #@63
    iget-object v4, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@65
    invoke-interface {v4}, Lorg/apache/http/io/SessionInputBuffer;->getMetrics()Lorg/apache/http/io/HttpTransportMetrics;

    #@68
    move-result-object v4

    #@69
    iget-object v5, p0, Landroid/net/http/AndroidHttpClientConnection;->outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

    #@6b
    invoke-interface {v5}, Lorg/apache/http/io/SessionOutputBuffer;->getMetrics()Lorg/apache/http/io/HttpTransportMetrics;

    #@6e
    move-result-object v5

    #@6f
    invoke-direct {v2, v4, v5}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;-><init>(Lorg/apache/http/io/HttpTransportMetrics;Lorg/apache/http/io/HttpTransportMetrics;)V

    #@72
    iput-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->metrics:Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    #@74
    .line 128
    iput-boolean v3, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@76
    .line 129
    return-void

    #@77
    .line 109
    .end local v0           #buffersize:I
    :cond_77
    const/4 v2, 0x0

    #@78
    goto :goto_30
.end method

.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 228
    iget-boolean v0, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 246
    :goto_4
    return-void

    #@5
    .line 231
    :cond_5
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@8
    .line 232
    invoke-virtual {p0}, Landroid/net/http/AndroidHttpClientConnection;->doFlush()V

    #@b
    .line 235
    :try_start_b
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@d
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_1f
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_b .. :try_end_10} :catch_1b

    #@10
    .line 239
    :goto_10
    :try_start_10
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@12
    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_15} :catch_1d
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_10 .. :try_end_15} :catch_1b

    #@15
    .line 245
    :goto_15
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@17
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    #@1a
    goto :goto_4

    #@1b
    .line 242
    :catch_1b
    move-exception v0

    #@1c
    goto :goto_15

    #@1d
    .line 240
    :catch_1d
    move-exception v0

    #@1e
    goto :goto_15

    #@1f
    .line 236
    :catch_1f
    move-exception v0

    #@20
    goto :goto_10
.end method

.method protected doFlush()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 286
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

    #@2
    invoke-interface {v0}, Lorg/apache/http/io/SessionOutputBuffer;->flush()V

    #@5
    .line 287
    return-void
.end method

.method public flush()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 290
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@3
    .line 291
    invoke-virtual {p0}, Landroid/net/http/AndroidHttpClientConnection;->doFlush()V

    #@6
    .line 292
    return-void
.end method

.method public getLocalAddress()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 164
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@6
    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    #@9
    move-result-object v0

    #@a
    .line 166
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getLocalPort()I
    .registers 2

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 172
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@6
    invoke-virtual {v0}, Ljava/net/Socket;->getLocalPort()I

    #@9
    move-result v0

    #@a
    .line 174
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public getMetrics()Lorg/apache/http/HttpConnectionMetrics;
    .registers 2

    #@0
    .prologue
    .line 462
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->metrics:Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    #@2
    return-object v0
.end method

.method public getRemoteAddress()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 179
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 180
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@6
    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    #@9
    move-result-object v0

    #@a
    .line 182
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getRemotePort()I
    .registers 2

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 188
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@6
    invoke-virtual {v0}, Ljava/net/Socket;->getPort()I

    #@9
    move-result v0

    #@a
    .line 190
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public getSocketTimeout()I
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 208
    iget-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@3
    if-eqz v2, :cond_b

    #@5
    .line 210
    :try_start_5
    iget-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@7
    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_a
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 215
    :cond_b
    :goto_b
    return v1

    #@c
    .line 211
    :catch_c
    move-exception v0

    #@d
    .line 212
    .local v0, ignore:Ljava/net/SocketException;
    goto :goto_b
.end method

.method public isOpen()Z
    .registers 2

    #@0
    .prologue
    .line 159
    iget-boolean v0, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@6
    if-eqz v0, :cond_12

    #@8
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@a
    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public isStale()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 448
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@4
    .line 450
    :try_start_4
    iget-object v2, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@6
    const/4 v3, 0x1

    #@7
    invoke-interface {v2, v3}, Lorg/apache/http/io/SessionInputBuffer;->isDataAvailable(I)Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_a} :catch_c

    #@a
    .line 451
    const/4 v1, 0x0

    #@b
    .line 453
    :goto_b
    return v1

    #@c
    .line 452
    :catch_c
    move-exception v0

    #@d
    .line 453
    .local v0, ex:Ljava/io/IOException;
    goto :goto_b
.end method

.method public parseResponseHeader(Landroid/net/http/Headers;)Lorg/apache/http/StatusLine;
    .registers 16
    .parameter "headers"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 304
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@3
    .line 306
    new-instance v1, Lorg/apache/http/util/CharArrayBuffer;

    #@5
    const/16 v10, 0x40

    #@7
    invoke-direct {v1, v10}, Lorg/apache/http/util/CharArrayBuffer;-><init>(I)V

    #@a
    .line 308
    .local v1, current:Lorg/apache/http/util/CharArrayBuffer;
    iget-object v10, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@c
    invoke-interface {v10, v1}, Lorg/apache/http/io/SessionInputBuffer;->readLine(Lorg/apache/http/util/CharArrayBuffer;)I

    #@f
    move-result v10

    #@10
    const/4 v11, -0x1

    #@11
    if-ne v10, v11, :cond_1b

    #@13
    .line 309
    new-instance v10, Lorg/apache/http/NoHttpResponseException;

    #@15
    const-string v11, "The target server failed to respond"

    #@17
    invoke-direct {v10, v11}, Lorg/apache/http/NoHttpResponseException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v10

    #@1b
    .line 313
    :cond_1b
    sget-object v10, Lorg/apache/http/message/BasicLineParser;->DEFAULT:Lorg/apache/http/message/BasicLineParser;

    #@1d
    new-instance v11, Lorg/apache/http/message/ParserCursor;

    #@1f
    const/4 v12, 0x0

    #@20
    invoke-virtual {v1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@23
    move-result v13

    #@24
    invoke-direct {v11, v12, v13}, Lorg/apache/http/message/ParserCursor;-><init>(II)V

    #@27
    invoke-virtual {v10, v1, v11}, Lorg/apache/http/message/BasicLineParser;->parseStatusLine(Lorg/apache/http/util/CharArrayBuffer;Lorg/apache/http/message/ParserCursor;)Lorg/apache/http/StatusLine;

    #@2a
    move-result-object v9

    #@2b
    .line 317
    .local v9, statusline:Lorg/apache/http/StatusLine;
    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    #@2e
    move-result v8

    #@2f
    .line 320
    .local v8, statusCode:I
    const/4 v6, 0x0

    #@30
    .line 321
    .local v6, previous:Lorg/apache/http/util/CharArrayBuffer;
    const/4 v3, 0x0

    #@31
    .line 323
    .local v3, headerNumber:I
    :cond_31
    if-nez v1, :cond_59

    #@33
    .line 324
    new-instance v1, Lorg/apache/http/util/CharArrayBuffer;

    #@35
    .end local v1           #current:Lorg/apache/http/util/CharArrayBuffer;
    const/16 v10, 0x40

    #@37
    invoke-direct {v1, v10}, Lorg/apache/http/util/CharArrayBuffer;-><init>(I)V

    #@3a
    .line 329
    .restart local v1       #current:Lorg/apache/http/util/CharArrayBuffer;
    :goto_3a
    iget-object v10, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@3c
    invoke-interface {v10, v1}, Lorg/apache/http/io/SessionInputBuffer;->readLine(Lorg/apache/http/util/CharArrayBuffer;)I

    #@3f
    move-result v4

    #@40
    .line 330
    .local v4, l:I
    const/4 v10, -0x1

    #@41
    if-eq v4, v10, :cond_4a

    #@43
    invoke-virtual {v1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@46
    move-result v10

    #@47
    const/4 v11, 0x1

    #@48
    if-ge v10, v11, :cond_5d

    #@4a
    .line 370
    :cond_4a
    if-eqz v6, :cond_4f

    #@4c
    .line 371
    invoke-virtual {p1, v6}, Landroid/net/http/Headers;->parseHeader(Lorg/apache/http/util/CharArrayBuffer;)V

    #@4f
    .line 374
    :cond_4f
    const/16 v10, 0xc8

    #@51
    if-lt v8, v10, :cond_58

    #@53
    .line 375
    iget-object v10, p0, Landroid/net/http/AndroidHttpClientConnection;->metrics:Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    #@55
    invoke-virtual {v10}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;->incrementResponseCount()V

    #@58
    .line 377
    :cond_58
    return-object v9

    #@59
    .line 327
    .end local v4           #l:I
    :cond_59
    invoke-virtual {v1}, Lorg/apache/http/util/CharArrayBuffer;->clear()V

    #@5c
    goto :goto_3a

    #@5d
    .line 337
    .restart local v4       #l:I
    :cond_5d
    const/4 v10, 0x0

    #@5e
    invoke-virtual {v1, v10}, Lorg/apache/http/util/CharArrayBuffer;->charAt(I)C

    #@61
    move-result v2

    #@62
    .line 338
    .local v2, first:C
    const/16 v10, 0x20

    #@64
    if-eq v2, v10, :cond_6a

    #@66
    const/16 v10, 0x9

    #@68
    if-ne v2, v10, :cond_bb

    #@6a
    :cond_6a
    if-eqz v6, :cond_bb

    #@6c
    .line 341
    const/4 v7, 0x0

    #@6d
    .line 342
    .local v7, start:I
    invoke-virtual {v1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@70
    move-result v5

    #@71
    .line 343
    .local v5, length:I
    :goto_71
    if-ge v7, v5, :cond_7f

    #@73
    .line 344
    invoke-virtual {v1, v7}, Lorg/apache/http/util/CharArrayBuffer;->charAt(I)C

    #@76
    move-result v0

    #@77
    .line 345
    .local v0, ch:C
    const/16 v10, 0x20

    #@79
    if-eq v0, v10, :cond_9b

    #@7b
    const/16 v10, 0x9

    #@7d
    if-eq v0, v10, :cond_9b

    #@7f
    .line 350
    .end local v0           #ch:C
    :cond_7f
    iget v10, p0, Landroid/net/http/AndroidHttpClientConnection;->maxLineLength:I

    #@81
    if-lez v10, :cond_9e

    #@83
    invoke-virtual {v6}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@86
    move-result v10

    #@87
    add-int/lit8 v10, v10, 0x1

    #@89
    invoke-virtual {v1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@8c
    move-result v11

    #@8d
    add-int/2addr v10, v11

    #@8e
    sub-int/2addr v10, v7

    #@8f
    iget v11, p0, Landroid/net/http/AndroidHttpClientConnection;->maxLineLength:I

    #@91
    if-le v10, v11, :cond_9e

    #@93
    .line 353
    new-instance v10, Ljava/io/IOException;

    #@95
    const-string v11, "Maximum line length limit exceeded"

    #@97
    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@9a
    throw v10

    #@9b
    .line 348
    .restart local v0       #ch:C
    :cond_9b
    add-int/lit8 v7, v7, 0x1

    #@9d
    .line 349
    goto :goto_71

    #@9e
    .line 355
    .end local v0           #ch:C
    :cond_9e
    const/16 v10, 0x20

    #@a0
    invoke-virtual {v6, v10}, Lorg/apache/http/util/CharArrayBuffer;->append(C)V

    #@a3
    .line 356
    invoke-virtual {v1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@a6
    move-result v10

    #@a7
    sub-int/2addr v10, v7

    #@a8
    invoke-virtual {v6, v1, v7, v10}, Lorg/apache/http/util/CharArrayBuffer;->append(Lorg/apache/http/util/CharArrayBuffer;II)V

    #@ab
    .line 365
    .end local v5           #length:I
    .end local v7           #start:I
    :goto_ab
    iget v10, p0, Landroid/net/http/AndroidHttpClientConnection;->maxHeaderCount:I

    #@ad
    if-lez v10, :cond_31

    #@af
    iget v10, p0, Landroid/net/http/AndroidHttpClientConnection;->maxHeaderCount:I

    #@b1
    if-lt v3, v10, :cond_31

    #@b3
    .line 366
    new-instance v10, Ljava/io/IOException;

    #@b5
    const-string v11, "Maximum header count exceeded"

    #@b7
    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@ba
    throw v10

    #@bb
    .line 358
    :cond_bb
    if-eqz v6, :cond_c0

    #@bd
    .line 359
    invoke-virtual {p1, v6}, Landroid/net/http/Headers;->parseHeader(Lorg/apache/http/util/CharArrayBuffer;)V

    #@c0
    .line 361
    :cond_c0
    add-int/lit8 v3, v3, 0x1

    #@c2
    .line 362
    move-object v6, v1

    #@c3
    .line 363
    const/4 v1, 0x0

    #@c4
    goto :goto_ab
.end method

.method public receiveResponseEntity(Landroid/net/http/Headers;)Lorg/apache/http/HttpEntity;
    .registers 12
    .parameter "headers"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const-wide/16 v7, -0x1

    #@3
    .line 386
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@6
    .line 387
    new-instance v2, Lorg/apache/http/entity/BasicHttpEntity;

    #@8
    invoke-direct {v2}, Lorg/apache/http/entity/BasicHttpEntity;-><init>()V

    #@b
    .line 389
    .local v2, entity:Lorg/apache/http/entity/BasicHttpEntity;
    invoke-direct {p0, p1}, Landroid/net/http/AndroidHttpClientConnection;->determineLength(Landroid/net/http/Headers;)J

    #@e
    move-result-wide v3

    #@f
    .line 390
    .local v3, len:J
    const-wide/16 v5, -0x2

    #@11
    cmp-long v5, v3, v5

    #@13
    if-nez v5, :cond_39

    #@15
    .line 391
    const/4 v5, 0x1

    #@16
    invoke-virtual {v2, v5}, Lorg/apache/http/entity/BasicHttpEntity;->setChunked(Z)V

    #@19
    .line 392
    invoke-virtual {v2, v7, v8}, Lorg/apache/http/entity/BasicHttpEntity;->setContentLength(J)V

    #@1c
    .line 393
    new-instance v5, Lorg/apache/http/impl/io/ChunkedInputStream;

    #@1e
    iget-object v6, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@20
    invoke-direct {v5, v6}, Lorg/apache/http/impl/io/ChunkedInputStream;-><init>(Lorg/apache/http/io/SessionInputBuffer;)V

    #@23
    invoke-virtual {v2, v5}, Lorg/apache/http/entity/BasicHttpEntity;->setContent(Ljava/io/InputStream;)V

    #@26
    .line 404
    :goto_26
    invoke-virtual {p1}, Landroid/net/http/Headers;->getContentType()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 405
    .local v1, contentTypeHeader:Ljava/lang/String;
    if-eqz v1, :cond_2f

    #@2c
    .line 406
    invoke-virtual {v2, v1}, Lorg/apache/http/entity/BasicHttpEntity;->setContentType(Ljava/lang/String;)V

    #@2f
    .line 408
    :cond_2f
    invoke-virtual {p1}, Landroid/net/http/Headers;->getContentEncoding()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    .line 409
    .local v0, contentEncodingHeader:Ljava/lang/String;
    if-eqz v0, :cond_38

    #@35
    .line 410
    invoke-virtual {v2, v0}, Lorg/apache/http/entity/BasicHttpEntity;->setContentEncoding(Ljava/lang/String;)V

    #@38
    .line 413
    :cond_38
    return-object v2

    #@39
    .line 394
    .end local v0           #contentEncodingHeader:Ljava/lang/String;
    .end local v1           #contentTypeHeader:Ljava/lang/String;
    :cond_39
    cmp-long v5, v3, v7

    #@3b
    if-nez v5, :cond_4e

    #@3d
    .line 395
    invoke-virtual {v2, v9}, Lorg/apache/http/entity/BasicHttpEntity;->setChunked(Z)V

    #@40
    .line 396
    invoke-virtual {v2, v7, v8}, Lorg/apache/http/entity/BasicHttpEntity;->setContentLength(J)V

    #@43
    .line 397
    new-instance v5, Lorg/apache/http/impl/io/IdentityInputStream;

    #@45
    iget-object v6, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@47
    invoke-direct {v5, v6}, Lorg/apache/http/impl/io/IdentityInputStream;-><init>(Lorg/apache/http/io/SessionInputBuffer;)V

    #@4a
    invoke-virtual {v2, v5}, Lorg/apache/http/entity/BasicHttpEntity;->setContent(Ljava/io/InputStream;)V

    #@4d
    goto :goto_26

    #@4e
    .line 399
    :cond_4e
    invoke-virtual {v2, v9}, Lorg/apache/http/entity/BasicHttpEntity;->setChunked(Z)V

    #@51
    .line 400
    invoke-virtual {v2, v3, v4}, Lorg/apache/http/entity/BasicHttpEntity;->setContentLength(J)V

    #@54
    .line 401
    new-instance v5, Lorg/apache/http/impl/io/ContentLengthInputStream;

    #@56
    iget-object v6, p0, Landroid/net/http/AndroidHttpClientConnection;->inbuffer:Lorg/apache/http/io/SessionInputBuffer;

    #@58
    invoke-direct {v5, v6, v3, v4}, Lorg/apache/http/impl/io/ContentLengthInputStream;-><init>(Lorg/apache/http/io/SessionInputBuffer;J)V

    #@5b
    invoke-virtual {v2, v5}, Lorg/apache/http/entity/BasicHttpEntity;->setContent(Ljava/io/InputStream;)V

    #@5e
    goto :goto_26
.end method

.method public sendRequestEntity(Lorg/apache/http/HttpEntityEnclosingRequest;)V
    .registers 5
    .parameter "request"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 272
    if-nez p1, :cond_a

    #@2
    .line 273
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "HTTP request may not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 275
    :cond_a
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@d
    .line 276
    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    #@10
    move-result-object v0

    #@11
    if-nez v0, :cond_14

    #@13
    .line 283
    :goto_13
    return-void

    #@14
    .line 279
    :cond_14
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->entityserializer:Lorg/apache/http/impl/entity/EntitySerializer;

    #@16
    iget-object v1, p0, Landroid/net/http/AndroidHttpClientConnection;->outbuffer:Lorg/apache/http/io/SessionOutputBuffer;

    #@18
    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v0, v1, p1, v2}, Lorg/apache/http/impl/entity/EntitySerializer;->serialize(Lorg/apache/http/io/SessionOutputBuffer;Lorg/apache/http/HttpMessage;Lorg/apache/http/HttpEntity;)V

    #@1f
    goto :goto_13
.end method

.method public sendRequestHeader(Lorg/apache/http/HttpRequest;)V
    .registers 4
    .parameter "request"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 256
    if-nez p1, :cond_a

    #@2
    .line 257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "HTTP request may not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 259
    :cond_a
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@d
    .line 260
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->requestWriter:Lorg/apache/http/io/HttpMessageWriter;

    #@f
    invoke-interface {v0, p1}, Lorg/apache/http/io/HttpMessageWriter;->write(Lorg/apache/http/HttpMessage;)V

    #@12
    .line 261
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->metrics:Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    #@14
    invoke-virtual {v0}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;->incrementRequestCount()V

    #@17
    .line 262
    return-void
.end method

.method public setSocketTimeout(I)V
    .registers 3
    .parameter "timeout"

    #@0
    .prologue
    .line 195
    invoke-direct {p0}, Landroid/net/http/AndroidHttpClientConnection;->assertOpen()V

    #@3
    .line 196
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 198
    :try_start_7
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@9
    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_c
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_c} :catch_d

    #@c
    .line 205
    :cond_c
    :goto_c
    return-void

    #@d
    .line 199
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method public shutdown()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 220
    const/4 v1, 0x0

    #@1
    iput-boolean v1, p0, Landroid/net/http/AndroidHttpClientConnection;->open:Z

    #@3
    .line 221
    iget-object v0, p0, Landroid/net/http/AndroidHttpClientConnection;->socket:Ljava/net/Socket;

    #@5
    .line 222
    .local v0, tmpsocket:Ljava/net/Socket;
    if-eqz v0, :cond_a

    #@7
    .line 223
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    #@a
    .line 225
    :cond_a
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 134
    .local v0, buffer:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "["

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 135
    invoke-virtual {p0}, Landroid/net/http/AndroidHttpClientConnection;->isOpen()Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_2d

    #@1c
    .line 136
    invoke-virtual {p0}, Landroid/net/http/AndroidHttpClientConnection;->getRemotePort()I

    #@1f
    move-result v1

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    .line 140
    :goto_23
    const-string v1, "]"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    return-object v1

    #@2d
    .line 138
    :cond_2d
    const-string v1, "closed"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    goto :goto_23
.end method
