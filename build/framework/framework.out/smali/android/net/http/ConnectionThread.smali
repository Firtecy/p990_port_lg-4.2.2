.class Landroid/net/http/ConnectionThread;
.super Ljava/lang/Thread;
.source "ConnectionThread.java"


# static fields
.field static final WAIT_TICK:I = 0x3e8

.field static final WAIT_TIMEOUT:I = 0x1388


# instance fields
.field mConnection:Landroid/net/http/Connection;

.field private mConnectionManager:Landroid/net/http/RequestQueue$ConnectionManager;

.field private mContext:Landroid/content/Context;

.field mCurrentThreadTime:J

.field private mId:I

.field private mRequestFeeder:Landroid/net/http/RequestFeeder;

.field private volatile mRunning:Z

.field mTotalThreadTime:J

.field private mWaiting:Z


# direct methods
.method constructor <init>(Landroid/content/Context;ILandroid/net/http/RequestQueue$ConnectionManager;Landroid/net/http/RequestFeeder;)V
    .registers 7
    .parameter "context"
    .parameter "id"
    .parameter "connectionManager"
    .parameter "requestFeeder"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@3
    .line 39
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/net/http/ConnectionThread;->mRunning:Z

    #@6
    .line 52
    iput-object p1, p0, Landroid/net/http/ConnectionThread;->mContext:Landroid/content/Context;

    #@8
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "http"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p0, v0}, Landroid/net/http/ConnectionThread;->setName(Ljava/lang/String;)V

    #@1e
    .line 54
    iput p2, p0, Landroid/net/http/ConnectionThread;->mId:I

    #@20
    .line 55
    iput-object p3, p0, Landroid/net/http/ConnectionThread;->mConnectionManager:Landroid/net/http/RequestQueue$ConnectionManager;

    #@22
    .line 56
    iput-object p4, p0, Landroid/net/http/ConnectionThread;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@24
    .line 57
    return-void
.end method


# virtual methods
.method requestStop()V
    .registers 3

    #@0
    .prologue
    .line 60
    iget-object v1, p0, Landroid/net/http/ConnectionThread;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@2
    monitor-enter v1

    #@3
    .line 61
    const/4 v0, 0x0

    #@4
    :try_start_4
    iput-boolean v0, p0, Landroid/net/http/ConnectionThread;->mRunning:Z

    #@6
    .line 62
    iget-object v0, p0, Landroid/net/http/ConnectionThread;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@8
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@b
    .line 63
    monitor-exit v1

    #@c
    .line 64
    return-void

    #@d
    .line 63
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const-wide/16 v7, 0x0

    #@3
    .line 71
    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    #@6
    .line 79
    iput-wide v7, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@8
    .line 80
    iput-wide v7, p0, Landroid/net/http/ConnectionThread;->mTotalThreadTime:J

    #@a
    .line 82
    :cond_a
    :goto_a
    iget-boolean v3, p0, Landroid/net/http/ConnectionThread;->mRunning:Z

    #@c
    if-eqz v3, :cond_8b

    #@e
    .line 83
    iget-wide v3, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@10
    const-wide/16 v5, -0x1

    #@12
    cmp-long v3, v3, v5

    #@14
    if-nez v3, :cond_1c

    #@16
    .line 84
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    #@19
    move-result-wide v3

    #@1a
    iput-wide v3, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@1c
    .line 90
    :cond_1c
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@1e
    invoke-interface {v3}, Landroid/net/http/RequestFeeder;->getRequest()Landroid/net/http/Request;

    #@21
    move-result-object v0

    #@22
    .line 93
    .local v0, request:Landroid/net/http/Request;
    if-nez v0, :cond_43

    #@24
    .line 94
    iget-object v4, p0, Landroid/net/http/ConnectionThread;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@26
    monitor-enter v4

    #@27
    .line 96
    const/4 v3, 0x1

    #@28
    :try_start_28
    iput-boolean v3, p0, Landroid/net/http/ConnectionThread;->mWaiting:Z
    :try_end_2a
    .catchall {:try_start_28 .. :try_end_2a} :catchall_40

    #@2a
    .line 98
    :try_start_2a
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@2c
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_2a .. :try_end_2f} :catch_8c

    #@2f
    .line 101
    :goto_2f
    const/4 v3, 0x0

    #@30
    :try_start_30
    iput-boolean v3, p0, Landroid/net/http/ConnectionThread;->mWaiting:Z

    #@32
    .line 102
    iget-wide v5, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@34
    cmp-long v3, v5, v7

    #@36
    if-eqz v3, :cond_3e

    #@38
    .line 103
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    #@3b
    move-result-wide v5

    #@3c
    iput-wide v5, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@3e
    .line 106
    :cond_3e
    monitor-exit v4

    #@3f
    goto :goto_a

    #@40
    :catchall_40
    move-exception v3

    #@41
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_30 .. :try_end_42} :catchall_40

    #@42
    throw v3

    #@43
    .line 111
    :cond_43
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mConnectionManager:Landroid/net/http/RequestQueue$ConnectionManager;

    #@45
    iget-object v4, p0, Landroid/net/http/ConnectionThread;->mContext:Landroid/content/Context;

    #@47
    iget-object v5, v0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@49
    invoke-interface {v3, v4, v5}, Landroid/net/http/RequestQueue$ConnectionManager;->getConnection(Landroid/content/Context;Lorg/apache/http/HttpHost;)Landroid/net/http/Connection;

    #@4c
    move-result-object v3

    #@4d
    iput-object v3, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@4f
    .line 113
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@51
    invoke-virtual {v3, v0}, Landroid/net/http/Connection;->processRequests(Landroid/net/http/Request;)V

    #@54
    .line 114
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@56
    invoke-virtual {v3}, Landroid/net/http/Connection;->getCanPersist()Z

    #@59
    move-result v3

    #@5a
    if-eqz v3, :cond_85

    #@5c
    .line 115
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mConnectionManager:Landroid/net/http/RequestQueue$ConnectionManager;

    #@5e
    iget-object v4, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@60
    invoke-interface {v3, v4}, Landroid/net/http/RequestQueue$ConnectionManager;->recycleConnection(Landroid/net/http/Connection;)Z

    #@63
    move-result v3

    #@64
    if-nez v3, :cond_6b

    #@66
    .line 116
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@68
    invoke-virtual {v3}, Landroid/net/http/Connection;->closeConnection()V

    #@6b
    .line 121
    :cond_6b
    :goto_6b
    const/4 v3, 0x0

    #@6c
    iput-object v3, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@6e
    .line 123
    iget-wide v3, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@70
    cmp-long v3, v3, v7

    #@72
    if-lez v3, :cond_a

    #@74
    .line 124
    iget-wide v1, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@76
    .line 125
    .local v1, start:J
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    #@79
    move-result-wide v3

    #@7a
    iput-wide v3, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@7c
    .line 126
    iget-wide v3, p0, Landroid/net/http/ConnectionThread;->mTotalThreadTime:J

    #@7e
    iget-wide v5, p0, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@80
    sub-long/2addr v5, v1

    #@81
    add-long/2addr v3, v5

    #@82
    iput-wide v3, p0, Landroid/net/http/ConnectionThread;->mTotalThreadTime:J

    #@84
    goto :goto_a

    #@85
    .line 119
    .end local v1           #start:J
    :cond_85
    iget-object v3, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@87
    invoke-virtual {v3}, Landroid/net/http/Connection;->closeConnection()V

    #@8a
    goto :goto_6b

    #@8b
    .line 131
    .end local v0           #request:Landroid/net/http/Request;
    :cond_8b
    return-void

    #@8c
    .line 99
    .restart local v0       #request:Landroid/net/http/Request;
    :catch_8c
    move-exception v3

    #@8d
    goto :goto_2f
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 134
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@3
    if-nez v2, :cond_39

    #@5
    const-string v1, ""

    #@7
    .line 135
    .local v1, con:Ljava/lang/String;
    :goto_7
    iget-boolean v2, p0, Landroid/net/http/ConnectionThread;->mWaiting:Z

    #@9
    if-eqz v2, :cond_40

    #@b
    const-string/jumbo v0, "w"

    #@e
    .line 136
    .local v0, active:Ljava/lang/String;
    :goto_e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "cid "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget v3, p0, Landroid/net/http/ConnectionThread;->mId:I

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_36
    .catchall {:try_start_1 .. :try_end_36} :catchall_43

    #@36
    move-result-object v2

    #@37
    monitor-exit p0

    #@38
    return-object v2

    #@39
    .line 134
    .end local v0           #active:Ljava/lang/String;
    .end local v1           #con:Ljava/lang/String;
    :cond_39
    :try_start_39
    iget-object v2, p0, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@3b
    invoke-virtual {v2}, Landroid/net/http/Connection;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    goto :goto_7

    #@40
    .line 135
    .restart local v1       #con:Ljava/lang/String;
    :cond_40
    const-string v0, "a"
    :try_end_42
    .catchall {:try_start_39 .. :try_end_42} :catchall_43

    #@42
    goto :goto_e

    #@43
    .line 134
    .end local v1           #con:Ljava/lang/String;
    :catchall_43
    move-exception v2

    #@44
    monitor-exit p0

    #@45
    throw v2
.end method
