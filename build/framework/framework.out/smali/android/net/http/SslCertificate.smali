.class public Landroid/net/http/SslCertificate;
.super Ljava/lang/Object;
.source "SslCertificate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/http/SslCertificate$DName;
    }
.end annotation


# static fields
.field private static ISO_8601_DATE_FORMAT:Ljava/lang/String; = null

.field private static final ISSUED_BY:Ljava/lang/String; = "issued-by"

.field private static final ISSUED_TO:Ljava/lang/String; = "issued-to"

.field private static final VALID_NOT_AFTER:Ljava/lang/String; = "valid-not-after"

.field private static final VALID_NOT_BEFORE:Ljava/lang/String; = "valid-not-before"

.field private static final X509_CERTIFICATE:Ljava/lang/String; = "x509-certificate"


# instance fields
.field private final mIssuedBy:Landroid/net/http/SslCertificate$DName;

.field private final mIssuedTo:Landroid/net/http/SslCertificate$DName;

.field private final mValidNotAfter:Ljava/util/Date;

.field private final mValidNotBefore:Ljava/util/Date;

.field private final mX509Certificate:Ljava/security/cert/X509Certificate;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 50
    const-string/jumbo v0, "yyyy-MM-dd HH:mm:ssZ"

    #@3
    sput-object v0, Landroid/net/http/SslCertificate;->ISO_8601_DATE_FORMAT:Ljava/lang/String;

    #@5
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "issuedTo"
    .parameter "issuedBy"
    .parameter "validNotBefore"
    .parameter "validNotAfter"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 156
    invoke-static {p3}, Landroid/net/http/SslCertificate;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    #@3
    move-result-object v3

    #@4
    invoke-static {p4}, Landroid/net/http/SslCertificate;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    #@7
    move-result-object v4

    #@8
    const/4 v5, 0x0

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v2, p2

    #@c
    invoke-direct/range {v0 .. v5}, Landroid/net/http/SslCertificate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V

    #@f
    .line 157
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V
    .registers 11
    .parameter "issuedTo"
    .parameter "issuedBy"
    .parameter "validNotBefore"
    .parameter "validNotAfter"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 170
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/net/http/SslCertificate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V

    #@9
    .line 171
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V
    .registers 7
    .parameter "issuedTo"
    .parameter "issuedBy"
    .parameter "validNotBefore"
    .parameter "validNotAfter"
    .parameter "x509Certificate"

    #@0
    .prologue
    .line 188
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 189
    new-instance v0, Landroid/net/http/SslCertificate$DName;

    #@5
    invoke-direct {v0, p0, p1}, Landroid/net/http/SslCertificate$DName;-><init>(Landroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@8
    iput-object v0, p0, Landroid/net/http/SslCertificate;->mIssuedTo:Landroid/net/http/SslCertificate$DName;

    #@a
    .line 190
    new-instance v0, Landroid/net/http/SslCertificate$DName;

    #@c
    invoke-direct {v0, p0, p2}, Landroid/net/http/SslCertificate$DName;-><init>(Landroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@f
    iput-object v0, p0, Landroid/net/http/SslCertificate;->mIssuedBy:Landroid/net/http/SslCertificate$DName;

    #@11
    .line 191
    invoke-static {p3}, Landroid/net/http/SslCertificate;->cloneDate(Ljava/util/Date;)Ljava/util/Date;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/net/http/SslCertificate;->mValidNotBefore:Ljava/util/Date;

    #@17
    .line 192
    invoke-static {p4}, Landroid/net/http/SslCertificate;->cloneDate(Ljava/util/Date;)Ljava/util/Date;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/net/http/SslCertificate;->mValidNotAfter:Ljava/util/Date;

    #@1d
    .line 193
    iput-object p5, p0, Landroid/net/http/SslCertificate;->mX509Certificate:Ljava/security/cert/X509Certificate;

    #@1f
    .line 194
    return-void
.end method

.method public constructor <init>(Ljava/security/cert/X509Certificate;)V
    .registers 8
    .parameter "certificate"

    #@0
    .prologue
    .line 178
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/security/Principal;->getName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getIssuerDN()Ljava/security/Principal;

    #@b
    move-result-object v0

    #@c
    invoke-interface {v0}, Ljava/security/Principal;->getName()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getNotBefore()Ljava/util/Date;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getNotAfter()Ljava/util/Date;

    #@17
    move-result-object v4

    #@18
    move-object v0, p0

    #@19
    move-object v5, p1

    #@1a
    invoke-direct/range {v0 .. v5}, Landroid/net/http/SslCertificate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V

    #@1d
    .line 183
    return-void
.end method

.method private static cloneDate(Ljava/util/Date;)Ljava/util/Date;
    .registers 2
    .parameter "date"

    #@0
    .prologue
    .line 329
    if-nez p0, :cond_4

    #@2
    .line 330
    const/4 v0, 0x0

    #@3
    .line 332
    :goto_3
    return-object v0

    #@4
    :cond_4
    invoke-virtual {p0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/util/Date;

    #@a
    goto :goto_3
.end method

.method private static final fingerprint([B)Ljava/lang/String;
    .registers 6
    .parameter "bytes"

    #@0
    .prologue
    .line 282
    if-nez p0, :cond_5

    #@2
    .line 283
    const-string v3, ""

    #@4
    .line 293
    :goto_4
    return-object v3

    #@5
    .line 285
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    .line 286
    .local v2, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    array-length v3, p0

    #@c
    if-ge v1, v3, :cond_21

    #@e
    .line 287
    aget-byte v0, p0, v1

    #@10
    .line 288
    .local v0, b:B
    const/4 v3, 0x1

    #@11
    invoke-static {v2, v0, v3}, Ljava/lang/IntegralToString;->appendByteAsHex(Ljava/lang/StringBuilder;BZ)Ljava/lang/StringBuilder;

    #@14
    .line 289
    add-int/lit8 v3, v1, 0x1

    #@16
    array-length v4, p0

    #@17
    if-eq v3, v4, :cond_1e

    #@19
    .line 290
    const/16 v3, 0x3a

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    .line 286
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_b

    #@21
    .line 293
    .end local v0           #b:B
    :cond_21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    goto :goto_4
.end method

.method private formatCertificateDate(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "certificateDate"

    #@0
    .prologue
    .line 489
    if-nez p2, :cond_5

    #@2
    .line 490
    const-string v0, ""

    #@4
    .line 492
    :goto_4
    return-object v0

    #@5
    :cond_5
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    goto :goto_4
.end method

.method private static formatDate(Ljava/util/Date;)Ljava/lang/String;
    .registers 3
    .parameter "date"

    #@0
    .prologue
    .line 319
    if-nez p0, :cond_5

    #@2
    .line 320
    const-string v0, ""

    #@4
    .line 322
    :goto_4
    return-object v0

    #@5
    :cond_5
    new-instance v0, Ljava/text/SimpleDateFormat;

    #@7
    sget-object v1, Landroid/net/http/SslCertificate;->ISO_8601_DATE_FORMAT:Ljava/lang/String;

    #@9
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@c
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    goto :goto_4
.end method

.method private static getDigest(Ljava/security/cert/X509Certificate;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "x509Certificate"
    .parameter "algorithm"

    #@0
    .prologue
    .line 266
    if-nez p0, :cond_5

    #@2
    .line 267
    const-string v4, ""

    #@4
    .line 277
    :goto_4
    return-object v4

    #@5
    .line 270
    :cond_5
    :try_start_5
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    #@8
    move-result-object v0

    #@9
    .line 271
    .local v0, bytes:[B
    invoke-static {p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@c
    move-result-object v3

    #@d
    .line 272
    .local v3, md:Ljava/security/MessageDigest;
    invoke-virtual {v3, v0}, Ljava/security/MessageDigest;->digest([B)[B

    #@10
    move-result-object v1

    #@11
    .line 273
    .local v1, digest:[B
    invoke-static {v1}, Landroid/net/http/SslCertificate;->fingerprint([B)Ljava/lang/String;
    :try_end_14
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_5 .. :try_end_14} :catch_16
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_14} :catch_1a

    #@14
    move-result-object v4

    #@15
    goto :goto_4

    #@16
    .line 274
    .end local v0           #bytes:[B
    .end local v1           #digest:[B
    .end local v3           #md:Ljava/security/MessageDigest;
    :catch_16
    move-exception v2

    #@17
    .line 275
    .local v2, ignored:Ljava/security/cert/CertificateEncodingException;
    const-string v4, ""

    #@19
    goto :goto_4

    #@1a
    .line 276
    .end local v2           #ignored:Ljava/security/cert/CertificateEncodingException;
    :catch_1a
    move-exception v2

    #@1b
    .line 277
    .local v2, ignored:Ljava/security/NoSuchAlgorithmException;
    const-string v4, ""

    #@1d
    goto :goto_4
.end method

.method private static getSerialNumber(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .registers 3
    .parameter "x509Certificate"

    #@0
    .prologue
    .line 252
    if-nez p0, :cond_5

    #@2
    .line 253
    const-string v1, ""

    #@4
    .line 259
    :goto_4
    return-object v1

    #@5
    .line 255
    :cond_5
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    #@8
    move-result-object v0

    #@9
    .line 256
    .local v0, serialNumber:Ljava/math/BigInteger;
    if-nez v0, :cond_e

    #@b
    .line 257
    const-string v1, ""

    #@d
    goto :goto_4

    #@e
    .line 259
    :cond_e
    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    #@11
    move-result-object v1

    #@12
    invoke-static {v1}, Landroid/net/http/SslCertificate;->fingerprint([B)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    goto :goto_4
.end method

.method private static parseDate(Ljava/lang/String;)Ljava/util/Date;
    .registers 4
    .parameter "string"

    #@0
    .prologue
    .line 309
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    #@2
    sget-object v2, Landroid/net/http/SslCertificate;->ISO_8601_DATE_FORMAT:Ljava/lang/String;

    #@4
    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@7
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_a
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_a} :catch_c

    #@a
    move-result-object v1

    #@b
    .line 311
    :goto_b
    return-object v1

    #@c
    .line 310
    :catch_c
    move-exception v0

    #@d
    .line 311
    .local v0, e:Ljava/text/ParseException;
    const/4 v1, 0x0

    #@e
    goto :goto_b
.end method

.method public static restoreState(Landroid/os/Bundle;)Landroid/net/http/SslCertificate;
    .registers 12
    .parameter "bundle"

    #@0
    .prologue
    .line 120
    if-nez p0, :cond_4

    #@2
    .line 121
    const/4 v1, 0x0

    #@3
    .line 136
    :goto_3
    return-object v1

    #@4
    .line 124
    :cond_4
    const-string/jumbo v1, "x509-certificate"

    #@7
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    #@a
    move-result-object v7

    #@b
    .line 125
    .local v7, bytes:[B
    if-nez v7, :cond_38

    #@d
    .line 126
    const/4 v6, 0x0

    #@e
    .line 136
    .local v6, x509Certificate:Ljava/security/cert/X509Certificate;
    :goto_e
    new-instance v1, Landroid/net/http/SslCertificate;

    #@10
    const-string/jumbo v2, "issued-to"

    #@13
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    const-string/jumbo v3, "issued-by"

    #@1a
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    const-string/jumbo v4, "valid-not-before"

    #@21
    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-static {v4}, Landroid/net/http/SslCertificate;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    #@28
    move-result-object v4

    #@29
    const-string/jumbo v5, "valid-not-after"

    #@2c
    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v5}, Landroid/net/http/SslCertificate;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    #@33
    move-result-object v5

    #@34
    invoke-direct/range {v1 .. v6}, Landroid/net/http/SslCertificate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V

    #@37
    goto :goto_3

    #@38
    .line 129
    .end local v6           #x509Certificate:Ljava/security/cert/X509Certificate;
    :cond_38
    :try_start_38
    const-string v1, "X.509"

    #@3a
    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@3d
    move-result-object v9

    #@3e
    .line 130
    .local v9, certFactory:Ljava/security/cert/CertificateFactory;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    #@40
    invoke-direct {v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@43
    invoke-virtual {v9, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@46
    move-result-object v8

    #@47
    .line 131
    .local v8, cert:Ljava/security/cert/Certificate;
    move-object v0, v8

    #@48
    check-cast v0, Ljava/security/cert/X509Certificate;

    #@4a
    move-object v6, v0
    :try_end_4b
    .catch Ljava/security/cert/CertificateException; {:try_start_38 .. :try_end_4b} :catch_4c

    #@4b
    .restart local v6       #x509Certificate:Ljava/security/cert/X509Certificate;
    goto :goto_e

    #@4c
    .line 132
    .end local v6           #x509Certificate:Ljava/security/cert/X509Certificate;
    .end local v8           #cert:Ljava/security/cert/Certificate;
    .end local v9           #certFactory:Ljava/security/cert/CertificateFactory;
    :catch_4c
    move-exception v10

    #@4d
    .line 133
    .local v10, e:Ljava/security/cert/CertificateException;
    const/4 v6, 0x0

    #@4e
    .restart local v6       #x509Certificate:Ljava/security/cert/X509Certificate;
    goto :goto_e
.end method

.method public static saveState(Landroid/net/http/SslCertificate;)Landroid/os/Bundle;
    .registers 5
    .parameter "certificate"

    #@0
    .prologue
    .line 96
    if-nez p0, :cond_4

    #@2
    .line 97
    const/4 v0, 0x0

    #@3
    .line 111
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 99
    :cond_4
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    .line 100
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "issued-to"

    #@c
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Landroid/net/http/SslCertificate$DName;->getDName()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 101
    const-string/jumbo v2, "issued-by"

    #@1a
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getIssuedBy()Landroid/net/http/SslCertificate$DName;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Landroid/net/http/SslCertificate$DName;->getDName()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 102
    const-string/jumbo v2, "valid-not-before"

    #@28
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getValidNotBefore()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 103
    const-string/jumbo v2, "valid-not-after"

    #@32
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getValidNotAfter()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 104
    iget-object v1, p0, Landroid/net/http/SslCertificate;->mX509Certificate:Ljava/security/cert/X509Certificate;

    #@3b
    .line 105
    .local v1, x509Certificate:Ljava/security/cert/X509Certificate;
    if-eqz v1, :cond_3

    #@3d
    .line 107
    :try_start_3d
    const-string/jumbo v2, "x509-certificate"

    #@40
    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_47
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_3d .. :try_end_47} :catch_48

    #@47
    goto :goto_3

    #@48
    .line 108
    :catch_48
    move-exception v2

    #@49
    goto :goto_3
.end method


# virtual methods
.method public getIssuedBy()Landroid/net/http/SslCertificate$DName;
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/net/http/SslCertificate;->mIssuedBy:Landroid/net/http/SslCertificate$DName;

    #@2
    return-object v0
.end method

.method public getIssuedTo()Landroid/net/http/SslCertificate$DName;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/net/http/SslCertificate;->mIssuedTo:Landroid/net/http/SslCertificate$DName;

    #@2
    return-object v0
.end method

.method public getValidNotAfter()Ljava/lang/String;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/net/http/SslCertificate;->mValidNotAfter:Ljava/util/Date;

    #@2
    invoke-static {v0}, Landroid/net/http/SslCertificate;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getValidNotAfterDate()Ljava/util/Date;
    .registers 2

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/net/http/SslCertificate;->mValidNotAfter:Ljava/util/Date;

    #@2
    invoke-static {v0}, Landroid/net/http/SslCertificate;->cloneDate(Ljava/util/Date;)Ljava/util/Date;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getValidNotBefore()Ljava/lang/String;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Landroid/net/http/SslCertificate;->mValidNotBefore:Ljava/util/Date;

    #@2
    invoke-static {v0}, Landroid/net/http/SslCertificate;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getValidNotBeforeDate()Ljava/util/Date;
    .registers 2

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/net/http/SslCertificate;->mValidNotBefore:Ljava/util/Date;

    #@2
    invoke-static {v0}, Landroid/net/http/SslCertificate;->cloneDate(Ljava/util/Date;)Ljava/util/Date;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public inflateCertificateView(Landroid/content/Context;)Landroid/view/View;
    .registers 11
    .parameter "context"

    #@0
    .prologue
    .line 434
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@3
    move-result-object v2

    #@4
    .line 436
    .local v2, factory:Landroid/view/LayoutInflater;
    const v6, 0x10900cf

    #@7
    const/4 v7, 0x0

    #@8
    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 440
    .local v0, certificateView:Landroid/view/View;
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    #@f
    move-result-object v5

    #@10
    .line 441
    .local v5, issuedTo:Landroid/net/http/SslCertificate$DName;
    if-eqz v5, :cond_42

    #@12
    .line 442
    const v6, 0x102038c

    #@15
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v6

    #@19
    check-cast v6, Landroid/widget/TextView;

    #@1b
    invoke-virtual {v5}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@22
    .line 444
    const v6, 0x102038e

    #@25
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@28
    move-result-object v6

    #@29
    check-cast v6, Landroid/widget/TextView;

    #@2b
    invoke-virtual {v5}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@32
    .line 446
    const v6, 0x1020390

    #@35
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@38
    move-result-object v6

    #@39
    check-cast v6, Landroid/widget/TextView;

    #@3b
    invoke-virtual {v5}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@42
    .line 450
    :cond_42
    const v6, 0x1020392

    #@45
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@48
    move-result-object v6

    #@49
    check-cast v6, Landroid/widget/TextView;

    #@4b
    iget-object v7, p0, Landroid/net/http/SslCertificate;->mX509Certificate:Ljava/security/cert/X509Certificate;

    #@4d
    invoke-static {v7}, Landroid/net/http/SslCertificate;->getSerialNumber(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@54
    .line 454
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getIssuedBy()Landroid/net/http/SslCertificate$DName;

    #@57
    move-result-object v3

    #@58
    .line 455
    .local v3, issuedBy:Landroid/net/http/SslCertificate$DName;
    if-eqz v3, :cond_8a

    #@5a
    .line 456
    const v6, 0x1020394

    #@5d
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@60
    move-result-object v6

    #@61
    check-cast v6, Landroid/widget/TextView;

    #@63
    invoke-virtual {v3}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    #@66
    move-result-object v7

    #@67
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@6a
    .line 458
    const v6, 0x1020396

    #@6d
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@70
    move-result-object v6

    #@71
    check-cast v6, Landroid/widget/TextView;

    #@73
    invoke-virtual {v3}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@7a
    .line 460
    const v6, 0x1020398

    #@7d
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@80
    move-result-object v6

    #@81
    check-cast v6, Landroid/widget/TextView;

    #@83
    invoke-virtual {v3}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    #@86
    move-result-object v7

    #@87
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@8a
    .line 465
    :cond_8a
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getValidNotBeforeDate()Ljava/util/Date;

    #@8d
    move-result-object v6

    #@8e
    invoke-direct {p0, p1, v6}, Landroid/net/http/SslCertificate;->formatCertificateDate(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    .line 466
    .local v4, issuedOn:Ljava/lang/String;
    const v6, 0x102039b

    #@95
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@98
    move-result-object v6

    #@99
    check-cast v6, Landroid/widget/TextView;

    #@9b
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@9e
    .line 470
    invoke-virtual {p0}, Landroid/net/http/SslCertificate;->getValidNotAfterDate()Ljava/util/Date;

    #@a1
    move-result-object v6

    #@a2
    invoke-direct {p0, p1, v6}, Landroid/net/http/SslCertificate;->formatCertificateDate(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;

    #@a5
    move-result-object v1

    #@a6
    .line 471
    .local v1, expiresOn:Ljava/lang/String;
    const v6, 0x102039d

    #@a9
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@ac
    move-result-object v6

    #@ad
    check-cast v6, Landroid/widget/TextView;

    #@af
    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b2
    .line 475
    const v6, 0x10203a0

    #@b5
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@b8
    move-result-object v6

    #@b9
    check-cast v6, Landroid/widget/TextView;

    #@bb
    iget-object v7, p0, Landroid/net/http/SslCertificate;->mX509Certificate:Ljava/security/cert/X509Certificate;

    #@bd
    const-string v8, "SHA256"

    #@bf
    invoke-static {v7, v8}, Landroid/net/http/SslCertificate;->getDigest(Ljava/security/cert/X509Certificate;Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v7

    #@c3
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@c6
    .line 477
    const v6, 0x10203a2

    #@c9
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@cc
    move-result-object v6

    #@cd
    check-cast v6, Landroid/widget/TextView;

    #@cf
    iget-object v7, p0, Landroid/net/http/SslCertificate;->mX509Certificate:Ljava/security/cert/X509Certificate;

    #@d1
    const-string v8, "SHA1"

    #@d3
    invoke-static {v7, v8}, Landroid/net/http/SslCertificate;->getDigest(Ljava/security/cert/X509Certificate;Ljava/lang/String;)Ljava/lang/String;

    #@d6
    move-result-object v7

    #@d7
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@da
    .line 480
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Issued to: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/net/http/SslCertificate;->mIssuedTo:Landroid/net/http/SslCertificate$DName;

    #@d
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getDName()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ";\n"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    const-string v1, "Issued by: "

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    iget-object v1, p0, Landroid/net/http/SslCertificate;->mIssuedBy:Landroid/net/http/SslCertificate$DName;

    #@23
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getDName()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, ";\n"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    return-object v0
.end method
