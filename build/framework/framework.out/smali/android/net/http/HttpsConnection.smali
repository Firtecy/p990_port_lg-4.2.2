.class public Landroid/net/http/HttpsConnection;
.super Landroid/net/http/Connection;
.source "HttpsConnection.java"


# static fields
.field private static mSslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;


# instance fields
.field private mAborted:Z

.field private mProxyHost:Lorg/apache/http/HttpHost;

.field private mSuspendLock:Ljava/lang/Object;

.field private mSuspended:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 59
    sput-object v0, Landroid/net/http/HttpsConnection;->mSslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@3
    .line 65
    invoke-static {v0}, Landroid/net/http/HttpsConnection;->initializeEngine(Ljava/io/File;)V

    #@6
    .line 66
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V
    .registers 7
    .parameter "context"
    .parameter "host"
    .parameter "proxy"
    .parameter "requestFeeder"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 143
    invoke-direct {p0, p1, p2, p4}, Landroid/net/http/Connection;-><init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V

    #@4
    .line 121
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/net/http/HttpsConnection;->mSuspendLock:Ljava/lang/Object;

    #@b
    .line 127
    iput-boolean v1, p0, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@d
    .line 133
    iput-boolean v1, p0, Landroid/net/http/HttpsConnection;->mAborted:Z

    #@f
    .line 144
    iput-object p3, p0, Landroid/net/http/HttpsConnection;->mProxyHost:Lorg/apache/http/HttpHost;

    #@11
    .line 145
    return-void
.end method

.method private static declared-synchronized getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .registers 2

    #@0
    .prologue
    .line 115
    const-class v0, Landroid/net/http/HttpsConnection;

    #@2
    monitor-enter v0

    #@3
    :try_start_3
    sget-object v1, Landroid/net/http/HttpsConnection;->mSslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    #@5
    monitor-exit v0

    #@6
    return-object v1

    #@7
    :catchall_7
    move-exception v1

    #@8
    monitor-exit v0

    #@9
    throw v1
.end method

.method public static initializeEngine(Ljava/io/File;)V
    .registers 8
    .parameter "sessionDir"

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    .line 76
    .local v0, cache:Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;
    if-eqz p0, :cond_25

    #@3
    .line 77
    :try_start_3
    const-string v4, "HttpsConnection"

    #@5
    new-instance v5, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v6, "Caching SSL sessions in "

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    const-string v6, "."

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 79
    invoke-static {p0}, Lorg/apache/harmony/xnet/provider/jsse/FileClientSessionCache;->usingDirectory(Ljava/io/File;)Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;

    #@24
    move-result-object v0

    #@25
    .line 82
    :cond_25
    new-instance v2, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;

    #@27
    invoke-direct {v2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;-><init>()V

    #@2a
    .line 85
    .local v2, sslContext:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;
    const/4 v4, 0x1

    #@2b
    new-array v3, v4, [Ljavax/net/ssl/TrustManager;

    #@2d
    const/4 v4, 0x0

    #@2e
    new-instance v5, Landroid/net/http/HttpsConnection$1;

    #@30
    invoke-direct {v5}, Landroid/net/http/HttpsConnection$1;-><init>()V

    #@33
    aput-object v5, v3, v4

    #@35
    .line 101
    .local v3, trustManagers:[Ljavax/net/ssl/TrustManager;
    const/4 v4, 0x0

    #@36
    const/4 v5, 0x0

    #@37
    invoke-virtual {v2, v4, v3, v5}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;->engineInit([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    #@3a
    .line 102
    invoke-virtual {v2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;->engineGetClientSessionContext()Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, v0}, Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;->setPersistentCache(Lorg/apache/harmony/xnet/provider/jsse/SSLClientSessionCache;)V

    #@41
    .line 104
    const-class v5, Landroid/net/http/HttpsConnection;

    #@43
    monitor-enter v5
    :try_end_44
    .catch Ljava/security/KeyManagementException; {:try_start_3 .. :try_end_44} :catch_4f
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_44} :catch_56

    #@44
    .line 105
    :try_start_44
    invoke-virtual {v2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;->engineGetSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    #@47
    move-result-object v4

    #@48
    sput-object v4, Landroid/net/http/HttpsConnection;->mSslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    #@4a
    .line 106
    monitor-exit v5

    #@4b
    .line 112
    return-void

    #@4c
    .line 106
    :catchall_4c
    move-exception v4

    #@4d
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_44 .. :try_end_4e} :catchall_4c

    #@4e
    :try_start_4e
    throw v4
    :try_end_4f
    .catch Ljava/security/KeyManagementException; {:try_start_4e .. :try_end_4f} :catch_4f
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_4f} :catch_56

    #@4f
    .line 107
    .end local v2           #sslContext:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLContextImpl;
    .end local v3           #trustManagers:[Ljavax/net/ssl/TrustManager;
    :catch_4f
    move-exception v1

    #@50
    .line 108
    .local v1, e:Ljava/security/KeyManagementException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@52
    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@55
    throw v4

    #@56
    .line 109
    .end local v1           #e:Ljava/security/KeyManagementException;
    :catch_56
    move-exception v1

    #@57
    .line 110
    .local v1, e:Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@59
    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@5c
    throw v4
.end method


# virtual methods
.method closeConnection()V
    .registers 3

    #@0
    .prologue
    .line 382
    iget-boolean v1, p0, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@2
    if-eqz v1, :cond_8

    #@4
    .line 384
    const/4 v1, 0x0

    #@5
    invoke-virtual {p0, v1}, Landroid/net/http/HttpsConnection;->restartConnection(Z)V

    #@8
    .line 388
    :cond_8
    :try_start_8
    iget-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@a
    if-eqz v1, :cond_19

    #@c
    iget-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@e
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClientConnection;->isOpen()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_19

    #@14
    .line 389
    iget-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@16
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClientConnection;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_19} :catch_1a

    #@19
    .line 397
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 391
    :catch_1a
    move-exception v0

    #@1b
    .line 395
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@1e
    goto :goto_19
.end method

.method getScheme()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 419
    const-string v0, "https"

    #@2
    return-object v0
.end method

.method openConnection(Landroid/net/http/Request;)Landroid/net/http/AndroidHttpClientConnection;
    .registers 32
    .parameter "req"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 164
    const/16 v22, 0x0

    #@2
    .line 166
    .local v22, sslSock:Ljavax/net/ssl/SSLSocket;
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Landroid/net/http/HttpsConnection;->mProxyHost:Lorg/apache/http/HttpHost;

    #@6
    move-object/from16 v26, v0

    #@8
    if-eqz v26, :cond_1ef

    #@a
    .line 174
    const/16 v17, 0x0

    #@c
    .line 175
    .local v17, proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    const/16 v20, 0x0

    #@e
    .line 177
    .local v20, proxySock:Ljava/net/Socket;
    :try_start_e
    new-instance v21, Ljava/net/Socket;

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Landroid/net/http/HttpsConnection;->mProxyHost:Lorg/apache/http/HttpHost;

    #@14
    move-object/from16 v26, v0

    #@16
    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@19
    move-result-object v26

    #@1a
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Landroid/net/http/HttpsConnection;->mProxyHost:Lorg/apache/http/HttpHost;

    #@1e
    move-object/from16 v27, v0

    #@20
    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/HttpHost;->getPort()I

    #@23
    move-result v27

    #@24
    move-object/from16 v0, v21

    #@26
    move-object/from16 v1, v26

    #@28
    move/from16 v2, v27

    #@2a
    invoke-direct {v0, v1, v2}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_2d} :catch_b3

    #@2d
    .line 180
    .end local v20           #proxySock:Ljava/net/Socket;
    .local v21, proxySock:Ljava/net/Socket;
    const v26, 0xea60

    #@30
    :try_start_30
    move-object/from16 v0, v21

    #@32
    move/from16 v1, v26

    #@34
    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    #@37
    .line 182
    new-instance v18, Landroid/net/http/AndroidHttpClientConnection;

    #@39
    invoke-direct/range {v18 .. v18}, Landroid/net/http/AndroidHttpClientConnection;-><init>()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_3c} :catch_2aa

    #@3c
    .line 183
    .end local v17           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .local v18, proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    :try_start_3c
    new-instance v16, Lorg/apache/http/params/BasicHttpParams;

    #@3e
    invoke-direct/range {v16 .. v16}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    #@41
    .line 184
    .local v16, params:Lorg/apache/http/params/HttpParams;
    const/16 v26, 0x2000

    #@43
    move-object/from16 v0, v16

    #@45
    move/from16 v1, v26

    #@47
    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    #@4a
    .line 186
    move-object/from16 v0, v18

    #@4c
    move-object/from16 v1, v21

    #@4e
    move-object/from16 v2, v16

    #@50
    invoke-virtual {v0, v1, v2}, Landroid/net/http/AndroidHttpClientConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V
    :try_end_53
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_53} :catch_2af

    #@53
    .line 201
    const/16 v24, 0x0

    #@55
    .line 202
    .local v24, statusLine:Lorg/apache/http/StatusLine;
    const/16 v23, 0x0

    #@57
    .line 203
    .local v23, statusCode:I
    new-instance v13, Landroid/net/http/Headers;

    #@59
    invoke-direct {v13}, Landroid/net/http/Headers;-><init>()V

    #@5c
    .line 205
    .local v13, headers:Landroid/net/http/Headers;
    :try_start_5c
    new-instance v19, Lorg/apache/http/message/BasicHttpRequest;

    #@5e
    const-string v26, "CONNECT"

    #@60
    move-object/from16 v0, p0

    #@62
    iget-object v0, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@64
    move-object/from16 v27, v0

    #@66
    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/HttpHost;->toHostString()Ljava/lang/String;

    #@69
    move-result-object v27

    #@6a
    move-object/from16 v0, v19

    #@6c
    move-object/from16 v1, v26

    #@6e
    move-object/from16 v2, v27

    #@70
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHttpRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 211
    .local v19, proxyReq:Lorg/apache/http/message/BasicHttpRequest;
    move-object/from16 v0, p1

    #@75
    iget-object v0, v0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@77
    move-object/from16 v26, v0

    #@79
    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/message/BasicHttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    #@7c
    move-result-object v5

    #@7d
    .local v5, arr$:[Lorg/apache/http/Header;
    array-length v15, v5

    #@7e
    .local v15, len$:I
    const/4 v14, 0x0

    #@7f
    .local v14, i$:I
    :goto_7f
    if-ge v14, v15, :cond_c9

    #@81
    aget-object v11, v5, v14

    #@83
    .line 212
    .local v11, h:Lorg/apache/http/Header;
    invoke-interface {v11}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    #@86
    move-result-object v26

    #@87
    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@8a
    move-result-object v12

    #@8b
    .line 213
    .local v12, headerName:Ljava/lang/String;
    const-string/jumbo v26, "proxy"

    #@8e
    move-object/from16 v0, v26

    #@90
    invoke-virtual {v12, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@93
    move-result v26

    #@94
    if-nez v26, :cond_ab

    #@96
    const-string/jumbo v26, "keep-alive"

    #@99
    move-object/from16 v0, v26

    #@9b
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9e
    move-result v26

    #@9f
    if-nez v26, :cond_ab

    #@a1
    const-string v26, "host"

    #@a3
    move-object/from16 v0, v26

    #@a5
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v26

    #@a9
    if-eqz v26, :cond_b0

    #@ab
    .line 215
    :cond_ab
    move-object/from16 v0, v19

    #@ad
    invoke-virtual {v0, v11}, Lorg/apache/http/message/BasicHttpRequest;->addHeader(Lorg/apache/http/Header;)V
    :try_end_b0
    .catch Lorg/apache/http/ParseException; {:try_start_5c .. :try_end_b0} :catch_16a
    .catch Lorg/apache/http/HttpException; {:try_start_5c .. :try_end_b0} :catch_17b
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_b0} :catch_18c

    #@b0
    .line 211
    :cond_b0
    add-int/lit8 v14, v14, 0x1

    #@b2
    goto :goto_7f

    #@b3
    .line 187
    .end local v5           #arr$:[Lorg/apache/http/Header;
    .end local v11           #h:Lorg/apache/http/Header;
    .end local v12           #headerName:Ljava/lang/String;
    .end local v13           #headers:Landroid/net/http/Headers;
    .end local v14           #i$:I
    .end local v15           #len$:I
    .end local v16           #params:Lorg/apache/http/params/HttpParams;
    .end local v18           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .end local v19           #proxyReq:Lorg/apache/http/message/BasicHttpRequest;
    .end local v21           #proxySock:Ljava/net/Socket;
    .end local v23           #statusCode:I
    .end local v24           #statusLine:Lorg/apache/http/StatusLine;
    .restart local v17       #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .restart local v20       #proxySock:Ljava/net/Socket;
    :catch_b3
    move-exception v8

    #@b4
    .line 188
    .local v8, e:Ljava/io/IOException;
    :goto_b4
    if-eqz v17, :cond_b9

    #@b6
    .line 189
    invoke-virtual/range {v17 .. v17}, Landroid/net/http/AndroidHttpClientConnection;->close()V

    #@b9
    .line 192
    :cond_b9
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@bc
    move-result-object v10

    #@bd
    .line 193
    .local v10, errorMessage:Ljava/lang/String;
    if-nez v10, :cond_c1

    #@bf
    .line 194
    const-string v10, "failed to establish a connection to the proxy"

    #@c1
    .line 198
    :cond_c1
    new-instance v26, Ljava/io/IOException;

    #@c3
    move-object/from16 v0, v26

    #@c5
    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c8
    throw v26

    #@c9
    .line 219
    .end local v8           #e:Ljava/io/IOException;
    .end local v10           #errorMessage:Ljava/lang/String;
    .end local v17           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .end local v20           #proxySock:Ljava/net/Socket;
    .restart local v5       #arr$:[Lorg/apache/http/Header;
    .restart local v13       #headers:Landroid/net/http/Headers;
    .restart local v14       #i$:I
    .restart local v15       #len$:I
    .restart local v16       #params:Lorg/apache/http/params/HttpParams;
    .restart local v18       #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .restart local v19       #proxyReq:Lorg/apache/http/message/BasicHttpRequest;
    .restart local v21       #proxySock:Ljava/net/Socket;
    .restart local v23       #statusCode:I
    .restart local v24       #statusLine:Lorg/apache/http/StatusLine;
    :cond_c9
    :try_start_c9
    invoke-virtual/range {v18 .. v19}, Landroid/net/http/AndroidHttpClientConnection;->sendRequestHeader(Lorg/apache/http/HttpRequest;)V

    #@cc
    .line 220
    invoke-virtual/range {v18 .. v18}, Landroid/net/http/AndroidHttpClientConnection;->flush()V

    #@cf
    .line 227
    :cond_cf
    move-object/from16 v0, v18

    #@d1
    invoke-virtual {v0, v13}, Landroid/net/http/AndroidHttpClientConnection;->parseResponseHeader(Landroid/net/http/Headers;)Lorg/apache/http/StatusLine;

    #@d4
    move-result-object v24

    #@d5
    .line 228
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_d8
    .catch Lorg/apache/http/ParseException; {:try_start_c9 .. :try_end_d8} :catch_16a
    .catch Lorg/apache/http/HttpException; {:try_start_c9 .. :try_end_d8} :catch_17b
    .catch Ljava/io/IOException; {:try_start_c9 .. :try_end_d8} :catch_18c

    #@d8
    move-result v23

    #@d9
    .line 229
    const/16 v26, 0xc8

    #@db
    move/from16 v0, v23

    #@dd
    move/from16 v1, v26

    #@df
    if-lt v0, v1, :cond_cf

    #@e1
    .line 256
    const/16 v26, 0xc8

    #@e3
    move/from16 v0, v23

    #@e5
    move/from16 v1, v26

    #@e7
    if-ne v0, v1, :cond_1b3

    #@e9
    .line 258
    :try_start_e9
    invoke-static {}, Landroid/net/http/HttpsConnection;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    #@ec
    move-result-object v26

    #@ed
    move-object/from16 v0, p0

    #@ef
    iget-object v0, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@f1
    move-object/from16 v27, v0

    #@f3
    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@f6
    move-result-object v27

    #@f7
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@fb
    move-object/from16 v28, v0

    #@fd
    invoke-virtual/range {v28 .. v28}, Lorg/apache/http/HttpHost;->getPort()I

    #@100
    move-result v28

    #@101
    const/16 v29, 0x1

    #@103
    move-object/from16 v0, v26

    #@105
    move-object/from16 v1, v21

    #@107
    move-object/from16 v2, v27

    #@109
    move/from16 v3, v28

    #@10b
    move/from16 v4, v29

    #@10d
    invoke-virtual {v0, v1, v2, v3, v4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    #@110
    move-result-object v26

    #@111
    move-object/from16 v0, v26

    #@113
    check-cast v0, Ljavax/net/ssl/SSLSocket;

    #@115
    move-object/from16 v22, v0
    :try_end_117
    .catch Ljava/io/IOException; {:try_start_e9 .. :try_end_117} :catch_19d

    #@117
    .line 310
    .end local v5           #arr$:[Lorg/apache/http/Header;
    .end local v13           #headers:Landroid/net/http/Headers;
    .end local v14           #i$:I
    .end local v15           #len$:I
    .end local v16           #params:Lorg/apache/http/params/HttpParams;
    .end local v18           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .end local v19           #proxyReq:Lorg/apache/http/message/BasicHttpRequest;
    .end local v21           #proxySock:Ljava/net/Socket;
    .end local v23           #statusCode:I
    .end local v24           #statusLine:Lorg/apache/http/StatusLine;
    :goto_117
    invoke-static {}, Landroid/net/http/CertificateChainValidator;->getInstance()Landroid/net/http/CertificateChainValidator;

    #@11a
    move-result-object v26

    #@11b
    move-object/from16 v0, p0

    #@11d
    iget-object v0, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@11f
    move-object/from16 v27, v0

    #@121
    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@124
    move-result-object v27

    #@125
    move-object/from16 v0, v26

    #@127
    move-object/from16 v1, p0

    #@129
    move-object/from16 v2, v22

    #@12b
    move-object/from16 v3, v27

    #@12d
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/http/CertificateChainValidator;->doHandshakeAndValidateServerCertificates(Landroid/net/http/HttpsConnection;Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)Landroid/net/http/SslError;

    #@130
    move-result-object v9

    #@131
    .line 314
    .local v9, error:Landroid/net/http/SslError;
    if-eqz v9, :cond_288

    #@133
    .line 320
    move-object/from16 v0, p0

    #@135
    iget-object v0, v0, Landroid/net/http/HttpsConnection;->mSuspendLock:Ljava/lang/Object;

    #@137
    move-object/from16 v27, v0

    #@139
    monitor-enter v27

    #@13a
    .line 321
    const/16 v26, 0x1

    #@13c
    :try_start_13c
    move/from16 v0, v26

    #@13e
    move-object/from16 v1, p0

    #@140
    iput-boolean v0, v1, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@142
    .line 322
    monitor-exit v27
    :try_end_143
    .catchall {:try_start_13c .. :try_end_143} :catchall_233

    #@143
    .line 324
    invoke-virtual/range {p1 .. p1}, Landroid/net/http/Request;->getEventHandler()Landroid/net/http/EventHandler;

    #@146
    move-result-object v26

    #@147
    move-object/from16 v0, v26

    #@149
    invoke-interface {v0, v9}, Landroid/net/http/EventHandler;->handleSslErrorRequest(Landroid/net/http/SslError;)Z

    #@14c
    move-result v6

    #@14d
    .line 325
    .local v6, canHandle:Z
    if-nez v6, :cond_236

    #@14f
    .line 326
    new-instance v26, Ljava/io/IOException;

    #@151
    new-instance v27, Ljava/lang/StringBuilder;

    #@153
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@156
    const-string v28, "failed to handle "

    #@158
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v27

    #@15c
    move-object/from16 v0, v27

    #@15e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v27

    #@162
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165
    move-result-object v27

    #@166
    invoke-direct/range {v26 .. v27}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@169
    throw v26

    #@16a
    .line 230
    .end local v6           #canHandle:Z
    .end local v9           #error:Landroid/net/http/SslError;
    .restart local v13       #headers:Landroid/net/http/Headers;
    .restart local v16       #params:Lorg/apache/http/params/HttpParams;
    .restart local v18       #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .restart local v21       #proxySock:Ljava/net/Socket;
    .restart local v23       #statusCode:I
    .restart local v24       #statusLine:Lorg/apache/http/StatusLine;
    :catch_16a
    move-exception v8

    #@16b
    .line 231
    .local v8, e:Lorg/apache/http/ParseException;
    invoke-virtual {v8}, Lorg/apache/http/ParseException;->getMessage()Ljava/lang/String;

    #@16e
    move-result-object v10

    #@16f
    .line 232
    .restart local v10       #errorMessage:Ljava/lang/String;
    if-nez v10, :cond_173

    #@171
    .line 233
    const-string v10, "failed to send a CONNECT request"

    #@173
    .line 237
    :cond_173
    new-instance v26, Ljava/io/IOException;

    #@175
    move-object/from16 v0, v26

    #@177
    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@17a
    throw v26

    #@17b
    .line 238
    .end local v8           #e:Lorg/apache/http/ParseException;
    .end local v10           #errorMessage:Ljava/lang/String;
    :catch_17b
    move-exception v8

    #@17c
    .line 239
    .local v8, e:Lorg/apache/http/HttpException;
    invoke-virtual {v8}, Lorg/apache/http/HttpException;->getMessage()Ljava/lang/String;

    #@17f
    move-result-object v10

    #@180
    .line 240
    .restart local v10       #errorMessage:Ljava/lang/String;
    if-nez v10, :cond_184

    #@182
    .line 241
    const-string v10, "failed to send a CONNECT request"

    #@184
    .line 245
    :cond_184
    new-instance v26, Ljava/io/IOException;

    #@186
    move-object/from16 v0, v26

    #@188
    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@18b
    throw v26

    #@18c
    .line 246
    .end local v8           #e:Lorg/apache/http/HttpException;
    .end local v10           #errorMessage:Ljava/lang/String;
    :catch_18c
    move-exception v8

    #@18d
    .line 247
    .local v8, e:Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@190
    move-result-object v10

    #@191
    .line 248
    .restart local v10       #errorMessage:Ljava/lang/String;
    if-nez v10, :cond_195

    #@193
    .line 249
    const-string v10, "failed to send a CONNECT request"

    #@195
    .line 253
    :cond_195
    new-instance v26, Ljava/io/IOException;

    #@197
    move-object/from16 v0, v26

    #@199
    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@19c
    throw v26

    #@19d
    .line 260
    .end local v8           #e:Ljava/io/IOException;
    .end local v10           #errorMessage:Ljava/lang/String;
    .restart local v5       #arr$:[Lorg/apache/http/Header;
    .restart local v14       #i$:I
    .restart local v15       #len$:I
    .restart local v19       #proxyReq:Lorg/apache/http/message/BasicHttpRequest;
    :catch_19d
    move-exception v8

    #@19e
    .line 261
    .restart local v8       #e:Ljava/io/IOException;
    if-eqz v22, :cond_1a3

    #@1a0
    .line 262
    #Replaced unresolvable odex instruction with a throw
    throw v22
    #invoke-virtual-quick/range {v22 .. v22}, vtable@0xd

    #@1a3
    .line 265
    :cond_1a3
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@1a6
    move-result-object v10

    #@1a7
    .line 266
    .restart local v10       #errorMessage:Ljava/lang/String;
    if-nez v10, :cond_1ab

    #@1a9
    .line 267
    const-string v10, "failed to create an SSL socket"

    #@1ab
    .line 270
    :cond_1ab
    new-instance v26, Ljava/io/IOException;

    #@1ad
    move-object/from16 v0, v26

    #@1af
    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1b2
    throw v26

    #@1b3
    .line 274
    .end local v8           #e:Ljava/io/IOException;
    .end local v10           #errorMessage:Ljava/lang/String;
    :cond_1b3
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getProtocolVersion()Lorg/apache/http/ProtocolVersion;

    #@1b6
    move-result-object v25

    #@1b7
    .line 276
    .local v25, version:Lorg/apache/http/ProtocolVersion;
    move-object/from16 v0, p1

    #@1b9
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@1bb
    move-object/from16 v26, v0

    #@1bd
    invoke-virtual/range {v25 .. v25}, Lorg/apache/http/ProtocolVersion;->getMajor()I

    #@1c0
    move-result v27

    #@1c1
    invoke-virtual/range {v25 .. v25}, Lorg/apache/http/ProtocolVersion;->getMinor()I

    #@1c4
    move-result v28

    #@1c5
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    #@1c8
    move-result-object v29

    #@1c9
    move-object/from16 v0, v26

    #@1cb
    move/from16 v1, v27

    #@1cd
    move/from16 v2, v28

    #@1cf
    move/from16 v3, v23

    #@1d1
    move-object/from16 v4, v29

    #@1d3
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/net/http/EventHandler;->status(IIILjava/lang/String;)V

    #@1d6
    .line 280
    move-object/from16 v0, p1

    #@1d8
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@1da
    move-object/from16 v26, v0

    #@1dc
    move-object/from16 v0, v26

    #@1de
    invoke-interface {v0, v13}, Landroid/net/http/EventHandler;->headers(Landroid/net/http/Headers;)V

    #@1e1
    .line 281
    move-object/from16 v0, p1

    #@1e3
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@1e5
    move-object/from16 v26, v0

    #@1e7
    invoke-interface/range {v26 .. v26}, Landroid/net/http/EventHandler;->endData()V

    #@1ea
    .line 283
    invoke-virtual/range {v18 .. v18}, Landroid/net/http/AndroidHttpClientConnection;->close()V

    #@1ed
    .line 287
    const/4 v7, 0x0

    #@1ee
    .line 368
    .end local v5           #arr$:[Lorg/apache/http/Header;
    .end local v13           #headers:Landroid/net/http/Headers;
    .end local v14           #i$:I
    .end local v15           #len$:I
    .end local v16           #params:Lorg/apache/http/params/HttpParams;
    .end local v18           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .end local v19           #proxyReq:Lorg/apache/http/message/BasicHttpRequest;
    .end local v21           #proxySock:Ljava/net/Socket;
    .end local v23           #statusCode:I
    .end local v24           #statusLine:Lorg/apache/http/StatusLine;
    .end local v25           #version:Lorg/apache/http/ProtocolVersion;
    :goto_1ee
    return-object v7

    #@1ef
    .line 292
    :cond_1ef
    :try_start_1ef
    invoke-static {}, Landroid/net/http/HttpsConnection;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    #@1f2
    move-result-object v26

    #@1f3
    move-object/from16 v0, p0

    #@1f5
    iget-object v0, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@1f7
    move-object/from16 v27, v0

    #@1f9
    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@1fc
    move-result-object v27

    #@1fd
    move-object/from16 v0, p0

    #@1ff
    iget-object v0, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@201
    move-object/from16 v28, v0

    #@203
    invoke-virtual/range {v28 .. v28}, Lorg/apache/http/HttpHost;->getPort()I

    #@206
    move-result v28

    #@207
    invoke-virtual/range {v26 .. v28}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    #@20a
    move-result-object v26

    #@20b
    move-object/from16 v0, v26

    #@20d
    check-cast v0, Ljavax/net/ssl/SSLSocket;

    #@20f
    move-object/from16 v22, v0

    #@211
    .line 294
    const v26, 0xea60

    #@214
    move-object/from16 v0, v22

    #@216
    move/from16 v1, v26

    #@218
    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V
    :try_end_21b
    .catch Ljava/io/IOException; {:try_start_1ef .. :try_end_21b} :catch_21d

    #@21b
    goto/16 :goto_117

    #@21d
    .line 295
    :catch_21d
    move-exception v8

    #@21e
    .line 296
    .restart local v8       #e:Ljava/io/IOException;
    if-eqz v22, :cond_223

    #@220
    .line 297
    invoke-virtual/range {v22 .. v22}, Ljavax/net/ssl/SSLSocket;->close()V

    #@223
    .line 300
    :cond_223
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@226
    move-result-object v10

    #@227
    .line 301
    .restart local v10       #errorMessage:Ljava/lang/String;
    if-nez v10, :cond_22b

    #@229
    .line 302
    const-string v10, "failed to create an SSL socket"

    #@22b
    .line 305
    :cond_22b
    new-instance v26, Ljava/io/IOException;

    #@22d
    move-object/from16 v0, v26

    #@22f
    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@232
    throw v26

    #@233
    .line 322
    .end local v8           #e:Ljava/io/IOException;
    .end local v10           #errorMessage:Ljava/lang/String;
    .restart local v9       #error:Landroid/net/http/SslError;
    :catchall_233
    move-exception v26

    #@234
    :try_start_234
    monitor-exit v27
    :try_end_235
    .catchall {:try_start_234 .. :try_end_235} :catchall_233

    #@235
    throw v26

    #@236
    .line 328
    .restart local v6       #canHandle:Z
    :cond_236
    move-object/from16 v0, p0

    #@238
    iget-object v0, v0, Landroid/net/http/HttpsConnection;->mSuspendLock:Ljava/lang/Object;

    #@23a
    move-object/from16 v27, v0

    #@23c
    monitor-enter v27

    #@23d
    .line 329
    :try_start_23d
    move-object/from16 v0, p0

    #@23f
    iget-boolean v0, v0, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@241
    move/from16 v26, v0
    :try_end_243
    .catchall {:try_start_23d .. :try_end_243} :catchall_284

    #@243
    if-eqz v26, :cond_26d

    #@245
    .line 337
    :try_start_245
    move-object/from16 v0, p0

    #@247
    iget-object v0, v0, Landroid/net/http/HttpsConnection;->mSuspendLock:Ljava/lang/Object;

    #@249
    move-object/from16 v26, v0

    #@24b
    const-wide/32 v28, 0x927c0

    #@24e
    move-object/from16 v0, v26

    #@250
    move-wide/from16 v1, v28

    #@252
    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V

    #@255
    .line 338
    move-object/from16 v0, p0

    #@257
    iget-boolean v0, v0, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@259
    move/from16 v26, v0

    #@25b
    if-eqz v26, :cond_26d

    #@25d
    .line 342
    const/16 v26, 0x0

    #@25f
    move/from16 v0, v26

    #@261
    move-object/from16 v1, p0

    #@263
    iput-boolean v0, v1, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@265
    .line 343
    const/16 v26, 0x1

    #@267
    move/from16 v0, v26

    #@269
    move-object/from16 v1, p0

    #@26b
    iput-boolean v0, v1, Landroid/net/http/HttpsConnection;->mAborted:Z
    :try_end_26d
    .catchall {:try_start_245 .. :try_end_26d} :catchall_284
    .catch Ljava/lang/InterruptedException; {:try_start_245 .. :try_end_26d} :catch_2a8

    #@26d
    .line 353
    :cond_26d
    :goto_26d
    :try_start_26d
    move-object/from16 v0, p0

    #@26f
    iget-boolean v0, v0, Landroid/net/http/HttpsConnection;->mAborted:Z

    #@271
    move/from16 v26, v0

    #@273
    if-eqz v26, :cond_287

    #@275
    .line 356
    invoke-virtual/range {v22 .. v22}, Ljavax/net/ssl/SSLSocket;->close()V

    #@278
    .line 357
    new-instance v26, Landroid/net/http/SSLConnectionClosedByUserException;

    #@27a
    const-string v28, "connection closed by the user"

    #@27c
    move-object/from16 v0, v26

    #@27e
    move-object/from16 v1, v28

    #@280
    invoke-direct {v0, v1}, Landroid/net/http/SSLConnectionClosedByUserException;-><init>(Ljava/lang/String;)V

    #@283
    throw v26

    #@284
    .line 359
    :catchall_284
    move-exception v26

    #@285
    monitor-exit v27
    :try_end_286
    .catchall {:try_start_26d .. :try_end_286} :catchall_284

    #@286
    throw v26

    #@287
    :cond_287
    :try_start_287
    monitor-exit v27
    :try_end_288
    .catchall {:try_start_287 .. :try_end_288} :catchall_284

    #@288
    .line 363
    .end local v6           #canHandle:Z
    :cond_288
    new-instance v7, Landroid/net/http/AndroidHttpClientConnection;

    #@28a
    invoke-direct {v7}, Landroid/net/http/AndroidHttpClientConnection;-><init>()V

    #@28d
    .line 364
    .local v7, conn:Landroid/net/http/AndroidHttpClientConnection;
    new-instance v16, Lorg/apache/http/params/BasicHttpParams;

    #@28f
    invoke-direct/range {v16 .. v16}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    #@292
    .line 365
    .local v16, params:Lorg/apache/http/params/BasicHttpParams;
    const-string v26, "http.socket.buffer-size"

    #@294
    const/16 v27, 0x2000

    #@296
    move-object/from16 v0, v16

    #@298
    move-object/from16 v1, v26

    #@29a
    move/from16 v2, v27

    #@29c
    invoke-virtual {v0, v1, v2}, Lorg/apache/http/params/BasicHttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    #@29f
    .line 366
    move-object/from16 v0, v22

    #@2a1
    move-object/from16 v1, v16

    #@2a3
    invoke-virtual {v7, v0, v1}, Landroid/net/http/AndroidHttpClientConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    #@2a6
    goto/16 :goto_1ee

    #@2a8
    .line 349
    .end local v7           #conn:Landroid/net/http/AndroidHttpClientConnection;
    .end local v16           #params:Lorg/apache/http/params/BasicHttpParams;
    .restart local v6       #canHandle:Z
    :catch_2a8
    move-exception v26

    #@2a9
    goto :goto_26d

    #@2aa
    .line 187
    .end local v6           #canHandle:Z
    .end local v9           #error:Landroid/net/http/SslError;
    .restart local v17       #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .restart local v21       #proxySock:Ljava/net/Socket;
    :catch_2aa
    move-exception v8

    #@2ab
    move-object/from16 v20, v21

    #@2ad
    .end local v21           #proxySock:Ljava/net/Socket;
    .restart local v20       #proxySock:Ljava/net/Socket;
    goto/16 :goto_b4

    #@2af
    .end local v17           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .end local v20           #proxySock:Ljava/net/Socket;
    .restart local v18       #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .restart local v21       #proxySock:Ljava/net/Socket;
    :catch_2af
    move-exception v8

    #@2b0
    move-object/from16 v20, v21

    #@2b2
    .end local v21           #proxySock:Ljava/net/Socket;
    .restart local v20       #proxySock:Ljava/net/Socket;
    move-object/from16 v17, v18

    #@2b4
    .end local v18           #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    .restart local v17       #proxyConnection:Landroid/net/http/AndroidHttpClientConnection;
    goto/16 :goto_b4
.end method

.method restartConnection(Z)V
    .registers 5
    .parameter "proceed"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 408
    iget-object v1, p0, Landroid/net/http/HttpsConnection;->mSuspendLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 409
    :try_start_4
    iget-boolean v2, p0, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@6
    if-eqz v2, :cond_15

    #@8
    .line 410
    const/4 v2, 0x0

    #@9
    iput-boolean v2, p0, Landroid/net/http/HttpsConnection;->mSuspended:Z

    #@b
    .line 411
    if-nez p1, :cond_e

    #@d
    const/4 v0, 0x1

    #@e
    :cond_e
    iput-boolean v0, p0, Landroid/net/http/HttpsConnection;->mAborted:Z

    #@10
    .line 412
    iget-object v0, p0, Landroid/net/http/HttpsConnection;->mSuspendLock:Ljava/lang/Object;

    #@12
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@15
    .line 414
    :cond_15
    monitor-exit v1

    #@16
    .line 415
    return-void

    #@17
    .line 414
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method setCertificate(Landroid/net/http/SslCertificate;)V
    .registers 2
    .parameter "certificate"

    #@0
    .prologue
    .line 153
    iput-object p1, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@2
    .line 154
    return-void
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 54
    invoke-super {p0}, Landroid/net/http/Connection;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
