.class public Landroid/net/http/RequestHandle;
.super Ljava/lang/Object;
.source "RequestHandle.java"


# static fields
.field private static final AUTHORIZATION_HEADER:Ljava/lang/String; = "Authorization"

.field public static final MAX_REDIRECT_COUNT:I = 0x10

.field private static final PROXY_AUTHORIZATION_HEADER:Ljava/lang/String; = "Proxy-Authorization"


# instance fields
.field private mBodyLength:I

.field private mBodyProvider:Ljava/io/InputStream;

.field private mConnection:Landroid/net/http/Connection;

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMethod:Ljava/lang/String;

.field private mRedirectCount:I

.field private mRequest:Landroid/net/http/Request;

.field private mRequestQueue:Landroid/net/http/RequestQueue;

.field private mUri:Landroid/net/WebAddress;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/http/RequestQueue;Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Ljava/io/InputStream;ILandroid/net/http/Request;)V
    .registers 10
    .parameter "requestQueue"
    .parameter "url"
    .parameter "uri"
    .parameter "method"
    .parameter
    .parameter "bodyProvider"
    .parameter "bodyLength"
    .parameter "request"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/http/RequestQueue;",
            "Ljava/lang/String;",
            "Landroid/net/WebAddress;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/InputStream;",
            "I",
            "Landroid/net/http/Request;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 64
    .local p5, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/net/http/RequestHandle;->mRedirectCount:I

    #@6
    .line 66
    if-nez p5, :cond_d

    #@8
    .line 67
    new-instance p5, Ljava/util/HashMap;

    #@a
    .end local p5           #headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p5}, Ljava/util/HashMap;-><init>()V

    #@d
    .line 69
    .restart local p5       #headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_d
    iput-object p5, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@f
    .line 70
    iput-object p6, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@11
    .line 71
    iput p7, p0, Landroid/net/http/RequestHandle;->mBodyLength:I

    #@13
    .line 72
    if-nez p4, :cond_17

    #@15
    const-string p4, "GET"

    #@17
    .end local p4
    :cond_17
    iput-object p4, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@19
    .line 74
    iput-object p2, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@1b
    .line 75
    iput-object p3, p0, Landroid/net/http/RequestHandle;->mUri:Landroid/net/WebAddress;

    #@1d
    .line 77
    iput-object p1, p0, Landroid/net/http/RequestHandle;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@1f
    .line 79
    iput-object p8, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@21
    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/net/http/RequestQueue;Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Ljava/io/InputStream;ILandroid/net/http/Request;Landroid/net/http/Connection;)V
    .registers 10
    .parameter "requestQueue"
    .parameter "url"
    .parameter "uri"
    .parameter "method"
    .parameter
    .parameter "bodyProvider"
    .parameter "bodyLength"
    .parameter "request"
    .parameter "conn"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/http/RequestQueue;",
            "Ljava/lang/String;",
            "Landroid/net/WebAddress;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/InputStream;",
            "I",
            "Landroid/net/http/Request;",
            "Landroid/net/http/Connection;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 90
    .local p5, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p8}, Landroid/net/http/RequestHandle;-><init>(Landroid/net/http/RequestQueue;Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Ljava/io/InputStream;ILandroid/net/http/Request;)V

    #@3
    .line 92
    iput-object p9, p0, Landroid/net/http/RequestHandle;->mConnection:Landroid/net/http/Connection;

    #@5
    .line 93
    return-void
.end method

.method private H(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "param"

    #@0
    .prologue
    .line 380
    if-eqz p1, :cond_1e

    #@2
    .line 382
    :try_start_2
    const-string v3, "MD5"

    #@4
    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@7
    move-result-object v2

    #@8
    .line 384
    .local v2, md5:Ljava/security/MessageDigest;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->digest([B)[B

    #@f
    move-result-object v0

    #@10
    .line 385
    .local v0, d:[B
    if-eqz v0, :cond_1e

    #@12
    .line 386
    invoke-direct {p0, v0}, Landroid/net/http/RequestHandle;->bufferToHex([B)Ljava/lang/String;
    :try_end_15
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_15} :catch_17

    #@15
    move-result-object v3

    #@16
    .line 393
    .end local v0           #d:[B
    .end local v2           #md5:Ljava/security/MessageDigest;
    :goto_16
    return-object v3

    #@17
    .line 388
    :catch_17
    move-exception v1

    #@18
    .line 389
    .local v1, e:Ljava/security/NoSuchAlgorithmException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@1a
    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@1d
    throw v3

    #@1e
    .line 393
    .end local v1           #e:Ljava/security/NoSuchAlgorithmException;
    :cond_1e
    const/4 v3, 0x0

    #@1f
    goto :goto_16
.end method

.method private KD(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "secret"
    .parameter "data"

    #@0
    .prologue
    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, ":"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-direct {p0, v0}, Landroid/net/http/RequestHandle;->H(Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public static authorizationHeader(Z)Ljava/lang/String;
    .registers 2
    .parameter "isProxy"

    #@0
    .prologue
    .line 342
    if-nez p0, :cond_5

    #@2
    .line 343
    const-string v0, "Authorization"

    #@4
    .line 345
    :goto_4
    return-object v0

    #@5
    :cond_5
    const-string v0, "Proxy-Authorization"

    #@7
    goto :goto_4
.end method

.method private bufferToHex([B)Ljava/lang/String;
    .registers 9
    .parameter "buffer"

    #@0
    .prologue
    .line 400
    const/16 v6, 0x10

    #@2
    new-array v2, v6, [C

    #@4
    fill-array-data v2, :array_3a

    #@7
    .line 403
    .local v2, hexChars:[C
    if-eqz p1, :cond_37

    #@9
    .line 404
    array-length v5, p1

    #@a
    .line 405
    .local v5, length:I
    if-lez v5, :cond_34

    #@c
    .line 406
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    mul-int/lit8 v6, v5, 0x2

    #@10
    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@13
    .line 408
    .local v1, hex:Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    #@14
    .local v3, i:I
    :goto_14
    if-ge v3, v5, :cond_2f

    #@16
    .line 409
    aget-byte v6, p1, v3

    #@18
    and-int/lit8 v6, v6, 0xf

    #@1a
    int-to-byte v4, v6

    #@1b
    .line 410
    .local v4, l:B
    aget-byte v6, p1, v3

    #@1d
    and-int/lit16 v6, v6, 0xf0

    #@1f
    shr-int/lit8 v6, v6, 0x4

    #@21
    int-to-byte v0, v6

    #@22
    .line 412
    .local v0, h:B
    aget-char v6, v2, v0

    #@24
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@27
    .line 413
    aget-char v6, v2, v4

    #@29
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    .line 408
    add-int/lit8 v3, v3, 0x1

    #@2e
    goto :goto_14

    #@2f
    .line 416
    .end local v0           #h:B
    .end local v4           #l:B
    :cond_2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v6

    #@33
    .line 422
    .end local v1           #hex:Ljava/lang/StringBuilder;
    .end local v3           #i:I
    .end local v5           #length:I
    :goto_33
    return-object v6

    #@34
    .line 418
    .restart local v5       #length:I
    :cond_34
    const-string v6, ""

    #@36
    goto :goto_33

    #@37
    .line 422
    .end local v5           #length:I
    :cond_37
    const/4 v6, 0x0

    #@38
    goto :goto_33

    #@39
    .line 400
    nop

    #@3a
    :array_3a
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x61t 0x0t
        0x62t 0x0t
        0x63t 0x0t
        0x64t 0x0t
        0x65t 0x0t
        0x66t 0x0t
    .end array-data
.end method

.method public static computeBasicAuthResponse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 276
    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    #@3
    .line 277
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    #@6
    .line 280
    new-instance v0, Ljava/lang/String;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const/16 v2, 0x3a

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@22
    move-result-object v1

    #@23
    invoke-static {v1}, Lorg/apache/commons/codec/binary/Base64;->encodeBase64([B)[B

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    #@2a
    return-object v0
.end method

.method private computeCnonce()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 429
    new-instance v1, Ljava/util/Random;

    #@2
    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    #@5
    .line 430
    .local v1, rand:Ljava/util/Random;
    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    #@8
    move-result v0

    #@9
    .line 431
    .local v0, nextInt:I
    const/high16 v2, -0x8000

    #@b
    if-ne v0, v2, :cond_17

    #@d
    const v0, 0x7fffffff

    #@10
    .line 433
    :goto_10
    const/16 v2, 0x10

    #@12
    invoke-static {v0, v2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    return-object v2

    #@17
    .line 431
    :cond_17
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@1a
    move-result v0

    #@1b
    goto :goto_10
.end method

.method private computeDigest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "A1"
    .parameter "A2"
    .parameter "nonce"
    .parameter "QOP"
    .parameter "nc"
    .parameter "cnonce"

    #@0
    .prologue
    .line 358
    if-nez p4, :cond_26

    #@2
    .line 359
    invoke-direct {p0, p1}, Landroid/net/http/RequestHandle;->H(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ":"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-direct {p0, p2}, Landroid/net/http/RequestHandle;->H(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {p0, v0, v1}, Landroid/net/http/RequestHandle;->KD(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    .line 366
    :goto_25
    return-object v0

    #@26
    .line 361
    :cond_26
    const-string v0, "auth"

    #@28
    invoke-virtual {p4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_70

    #@2e
    .line 362
    invoke-direct {p0, p1}, Landroid/net/http/RequestHandle;->H(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    const-string v2, ":"

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, ":"

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, ":"

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    const-string v2, ":"

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-direct {p0, p2}, Landroid/net/http/RequestHandle;->H(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v1

    #@6b
    invoke-direct {p0, v0, v1}, Landroid/net/http/RequestHandle;->KD(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6e
    move-result-object v0

    #@6f
    goto :goto_25

    #@70
    .line 366
    :cond_70
    const/4 v0, 0x0

    #@71
    goto :goto_25
.end method

.method private computeDigestAuthResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 18
    .parameter "username"
    .parameter "password"
    .parameter "realm"
    .parameter "nonce"
    .parameter "QOP"
    .parameter "algorithm"
    .parameter "opaque"

    #@0
    .prologue
    .line 304
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    #@3
    .line 305
    invoke-static {p2}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    #@6
    .line 306
    invoke-static {p3}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    #@9
    .line 308
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v4, ":"

    #@14
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v4, ":"

    #@1e
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    .line 309
    .local v2, A1:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@31
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v4, ":"

    #@37
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@3d
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    .line 312
    .local v3, A2:Ljava/lang/String;
    const-string v6, "00000001"

    #@47
    .line 313
    .local v6, nc:Ljava/lang/String;
    invoke-direct {p0}, Landroid/net/http/RequestHandle;->computeCnonce()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    .local v7, cnonce:Ljava/lang/String;
    move-object v1, p0

    #@4c
    move-object v4, p4

    #@4d
    move-object v5, p5

    #@4e
    .line 314
    invoke-direct/range {v1 .. v7}, Landroid/net/http/RequestHandle;->computeDigest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@51
    move-result-object v8

    #@52
    .line 316
    .local v8, digest:Ljava/lang/String;
    const-string v9, ""

    #@54
    .line 317
    .local v9, response:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    const-string/jumbo v4, "username="

    #@60
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    invoke-direct {p0, p1}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    const-string v4, ", "

    #@6e
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v9

    #@76
    .line 318
    new-instance v1, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    const-string/jumbo v4, "realm="

    #@82
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v1

    #@86
    invoke-direct {p0, p3}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v1

    #@8e
    const-string v4, ", "

    #@90
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v1

    #@94
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v9

    #@98
    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v1

    #@a1
    const-string/jumbo v4, "nonce="

    #@a4
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v1

    #@a8
    invoke-direct {p0, p4}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v1

    #@b0
    const-string v4, ", "

    #@b2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v9

    #@ba
    .line 320
    new-instance v1, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v1

    #@c3
    const-string/jumbo v4, "uri="

    #@c6
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v1

    #@ca
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@cc
    invoke-direct {p0, v4}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@cf
    move-result-object v4

    #@d0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v1

    #@d4
    const-string v4, ", "

    #@d6
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v1

    #@da
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v9

    #@de
    .line 321
    new-instance v1, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v1

    #@e7
    const-string/jumbo v4, "response="

    #@ea
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    invoke-direct {p0, v8}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@f1
    move-result-object v4

    #@f2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v1

    #@f6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v9

    #@fa
    .line 323
    if-eqz p7, :cond_119

    #@fc
    .line 324
    new-instance v1, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v1

    #@105
    const-string v4, ", opaque="

    #@107
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v1

    #@10b
    move-object/from16 v0, p7

    #@10d
    invoke-direct {p0, v0}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@110
    move-result-object v4

    #@111
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v1

    #@115
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v9

    #@119
    .line 327
    :cond_119
    if-eqz p6, :cond_134

    #@11b
    .line 328
    new-instance v1, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v1

    #@124
    const-string v4, ", algorithm="

    #@126
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v1

    #@12a
    move-object/from16 v0, p6

    #@12c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v1

    #@130
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@133
    move-result-object v9

    #@134
    .line 331
    :cond_134
    if-eqz p5, :cond_165

    #@136
    .line 332
    new-instance v1, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v1

    #@13f
    const-string v4, ", qop="

    #@141
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v1

    #@145
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v1

    #@149
    const-string v4, ", nc="

    #@14b
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v1

    #@14f
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v1

    #@153
    const-string v4, ", cnonce="

    #@155
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v1

    #@159
    invoke-direct {p0, v7}, Landroid/net/http/RequestHandle;->doubleQuote(Ljava/lang/String;)Ljava/lang/String;

    #@15c
    move-result-object v4

    #@15d
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v1

    #@161
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@164
    move-result-object v9

    #@165
    .line 335
    :cond_165
    return-object v9
.end method

.method private createAndQueueNewRequest()V
    .registers 10

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mConnection:Landroid/net/http/Connection;

    #@2
    if-eqz v0, :cond_26

    #@4
    .line 453
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@6
    iget-object v1, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@8
    iget-object v2, p0, Landroid/net/http/RequestHandle;->mUri:Landroid/net/WebAddress;

    #@a
    iget-object v3, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@c
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@e
    iget-object v5, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@10
    iget-object v5, v5, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@12
    iget-object v6, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@14
    iget v7, p0, Landroid/net/http/RequestHandle;->mBodyLength:I

    #@16
    invoke-virtual/range {v0 .. v7}, Landroid/net/http/RequestQueue;->queueSynchronousRequest(Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;

    #@19
    move-result-object v8

    #@1a
    .line 456
    .local v8, newHandle:Landroid/net/http/RequestHandle;
    iget-object v0, v8, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@1c
    iput-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@1e
    .line 457
    iget-object v0, v8, Landroid/net/http/RequestHandle;->mConnection:Landroid/net/http/Connection;

    #@20
    iput-object v0, p0, Landroid/net/http/RequestHandle;->mConnection:Landroid/net/http/Connection;

    #@22
    .line 458
    invoke-virtual {v8}, Landroid/net/http/RequestHandle;->processRequest()V

    #@25
    .line 465
    .end local v8           #newHandle:Landroid/net/http/RequestHandle;
    :goto_25
    return-void

    #@26
    .line 461
    :cond_26
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequestQueue:Landroid/net/http/RequestQueue;

    #@28
    iget-object v1, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@2a
    iget-object v2, p0, Landroid/net/http/RequestHandle;->mUri:Landroid/net/WebAddress;

    #@2c
    iget-object v3, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@2e
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@30
    iget-object v5, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@32
    iget-object v5, v5, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@34
    iget-object v6, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@36
    iget v7, p0, Landroid/net/http/RequestHandle;->mBodyLength:I

    #@38
    invoke-virtual/range {v0 .. v7}, Landroid/net/http/RequestQueue;->queueRequest(Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;

    #@3b
    move-result-object v0

    #@3c
    iget-object v0, v0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@3e
    iput-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@40
    goto :goto_25
.end method

.method private doubleQuote(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "param"

    #@0
    .prologue
    .line 440
    if-eqz p1, :cond_1c

    #@2
    .line 441
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "\""

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "\""

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 444
    :goto_1b
    return-object v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method private setupAuthResponse()V
    .registers 2

    #@0
    .prologue
    .line 256
    :try_start_0
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@6
    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_d

    #@9
    .line 262
    :cond_9
    :goto_9
    invoke-direct {p0}, Landroid/net/http/RequestHandle;->createAndQueueNewRequest()V

    #@c
    .line 263
    return-void

    #@d
    .line 257
    :catch_d
    move-exception v0

    #@e
    goto :goto_9
.end method


# virtual methods
.method public cancel()V
    .registers 2

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 100
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@6
    invoke-virtual {v0}, Landroid/net/http/Request;->cancel()V

    #@9
    .line 102
    :cond_9
    return-void
.end method

.method public getMethod()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRedirectCount()I
    .registers 2

    #@0
    .prologue
    .line 132
    iget v0, p0, Landroid/net/http/RequestHandle;->mRedirectCount:I

    #@2
    return v0
.end method

.method public handleSslErrorResponse(Z)V
    .registers 3
    .parameter "proceed"

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 120
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@6
    invoke-virtual {v0, p1}, Landroid/net/http/Request;->handleSslErrorResponse(Z)V

    #@9
    .line 122
    :cond_9
    return-void
.end method

.method public isRedirectMax()Z
    .registers 3

    #@0
    .prologue
    .line 128
    iget v0, p0, Landroid/net/http/RequestHandle;->mRedirectCount:I

    #@2
    const/16 v1, 0x10

    #@4
    if-lt v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public pauseRequest(Z)V
    .registers 3
    .parameter "pause"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 110
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@6
    invoke-virtual {v0, p1}, Landroid/net/http/Request;->setLoadingPaused(Z)V

    #@9
    .line 112
    :cond_9
    return-void
.end method

.method public processRequest()V
    .registers 3

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mConnection:Landroid/net/http/Connection;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 289
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mConnection:Landroid/net/http/Connection;

    #@6
    iget-object v1, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@8
    invoke-virtual {v0, v1}, Landroid/net/http/Connection;->processRequests(Landroid/net/http/Request;)V

    #@b
    .line 291
    :cond_b
    return-void
.end method

.method public setRedirectCount(I)V
    .registers 2
    .parameter "count"

    #@0
    .prologue
    .line 136
    iput p1, p0, Landroid/net/http/RequestHandle;->mRedirectCount:I

    #@2
    .line 137
    return-void
.end method

.method public setupBasicAuthResponse(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "isProxy"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 225
    invoke-static {p2, p3}, Landroid/net/http/RequestHandle;->computeBasicAuthResponse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 229
    .local v0, response:Ljava/lang/String;
    iget-object v1, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@6
    invoke-static {p1}, Landroid/net/http/RequestHandle;->authorizationHeader(Z)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Basic "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 230
    invoke-direct {p0}, Landroid/net/http/RequestHandle;->setupAuthResponse()V

    #@23
    .line 231
    return-void
.end method

.method public setupDigestAuthResponse(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 18
    .parameter "isProxy"
    .parameter "username"
    .parameter "password"
    .parameter "realm"
    .parameter "nonce"
    .parameter "QOP"
    .parameter "algorithm"
    .parameter "opaque"

    #@0
    .prologue
    .line 245
    move-object v0, p0

    #@1
    move-object v1, p2

    #@2
    move-object v2, p3

    #@3
    move-object v3, p4

    #@4
    move-object v4, p5

    #@5
    move-object v5, p6

    #@6
    move-object/from16 v6, p7

    #@8
    move-object/from16 v7, p8

    #@a
    invoke-direct/range {v0 .. v7}, Landroid/net/http/RequestHandle;->computeDigestAuthResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v8

    #@e
    .line 250
    .local v8, response:Ljava/lang/String;
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@10
    invoke-static {p1}, Landroid/net/http/RequestHandle;->authorizationHeader(Z)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Digest "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 251
    invoke-direct {p0}, Landroid/net/http/RequestHandle;->setupAuthResponse()V

    #@2d
    .line 252
    return-void
.end method

.method public setupRedirect(Ljava/lang/String;ILjava/util/Map;)Z
    .registers 11
    .parameter "redirectTo"
    .parameter "statusCode"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p3, cacheHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 156
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@3
    const-string v5, "Authorization"

    #@5
    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 157
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@a
    const-string v5, "Proxy-Authorization"

    #@c
    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 159
    iget v4, p0, Landroid/net/http/RequestHandle;->mRedirectCount:I

    #@11
    add-int/lit8 v4, v4, 0x1

    #@13
    iput v4, p0, Landroid/net/http/RequestHandle;->mRedirectCount:I

    #@15
    const/16 v5, 0x10

    #@17
    if-ne v4, v5, :cond_24

    #@19
    .line 164
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@1b
    const/16 v5, -0x9

    #@1d
    const v6, 0x10400e7

    #@20
    invoke-virtual {v4, v5, v6}, Landroid/net/http/Request;->error(II)V

    #@23
    .line 218
    :goto_23
    return v3

    #@24
    .line 169
    :cond_24
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@26
    const-string v5, "https:"

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_3d

    #@2e
    const-string v4, "http:"

    #@30
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_3d

    #@36
    .line 174
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@38
    const-string v5, "Referer"

    #@3a
    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 177
    :cond_3d
    iput-object p1, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@3f
    .line 179
    :try_start_3f
    new-instance v4, Landroid/net/WebAddress;

    #@41
    iget-object v5, p0, Landroid/net/http/RequestHandle;->mUrl:Ljava/lang/String;

    #@43
    invoke-direct {v4, v5}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    #@46
    iput-object v4, p0, Landroid/net/http/RequestHandle;->mUri:Landroid/net/WebAddress;
    :try_end_48
    .catch Landroid/net/ParseException; {:try_start_3f .. :try_end_48} :catch_95

    #@48
    .line 185
    :goto_48
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@4a
    const-string v5, "Cookie"

    #@4c
    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    .line 186
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    #@52
    move-result-object v4

    #@53
    iget-object v5, p0, Landroid/net/http/RequestHandle;->mUri:Landroid/net/WebAddress;

    #@55
    invoke-virtual {v4, v5}, Landroid/webkit/CookieManager;->getCookie(Landroid/net/WebAddress;)Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .line 187
    .local v0, cookie:Ljava/lang/String;
    if-eqz v0, :cond_68

    #@5b
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@5e
    move-result v4

    #@5f
    if-lez v4, :cond_68

    #@61
    .line 188
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@63
    const-string v5, "Cookie"

    #@65
    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    .line 191
    :cond_68
    const/16 v4, 0x12e

    #@6a
    if-eq p2, v4, :cond_70

    #@6c
    const/16 v4, 0x12f

    #@6e
    if-ne p2, v4, :cond_7e

    #@70
    :cond_70
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@72
    const-string v5, "POST"

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v4

    #@78
    if-eqz v4, :cond_7e

    #@7a
    .line 195
    const-string v4, "GET"

    #@7c
    iput-object v4, p0, Landroid/net/http/RequestHandle;->mMethod:Ljava/lang/String;

    #@7e
    .line 199
    :cond_7e
    const/16 v4, 0x133

    #@80
    if-ne p2, v4, :cond_9c

    #@82
    .line 201
    :try_start_82
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@84
    if-eqz v4, :cond_8b

    #@86
    iget-object v4, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@88
    invoke-virtual {v4}, Ljava/io/InputStream;->reset()V
    :try_end_8b
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_8b} :catch_9a

    #@8b
    .line 215
    :cond_8b
    :goto_8b
    iget-object v3, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@8d
    invoke-interface {v3, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    #@90
    .line 217
    invoke-direct {p0}, Landroid/net/http/RequestHandle;->createAndQueueNewRequest()V

    #@93
    .line 218
    const/4 v3, 0x1

    #@94
    goto :goto_23

    #@95
    .line 180
    .end local v0           #cookie:Ljava/lang/String;
    :catch_95
    move-exception v1

    #@96
    .line 181
    .local v1, e:Landroid/net/ParseException;
    invoke-virtual {v1}, Landroid/net/ParseException;->printStackTrace()V

    #@99
    goto :goto_48

    #@9a
    .line 202
    .end local v1           #e:Landroid/net/ParseException;
    .restart local v0       #cookie:Ljava/lang/String;
    :catch_9a
    move-exception v2

    #@9b
    .line 206
    .local v2, ex:Ljava/io/IOException;
    goto :goto_23

    #@9c
    .line 210
    .end local v2           #ex:Ljava/io/IOException;
    :cond_9c
    iget-object v3, p0, Landroid/net/http/RequestHandle;->mHeaders:Ljava/util/Map;

    #@9e
    const-string v4, "Content-Type"

    #@a0
    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a3
    .line 211
    const/4 v3, 0x0

    #@a4
    iput-object v3, p0, Landroid/net/http/RequestHandle;->mBodyProvider:Ljava/io/InputStream;

    #@a6
    goto :goto_8b
.end method

.method public waitUntilComplete()V
    .registers 2

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Landroid/net/http/RequestHandle;->mRequest:Landroid/net/http/Request;

    #@2
    invoke-virtual {v0}, Landroid/net/http/Request;->waitUntilComplete()V

    #@5
    .line 285
    return-void
.end method
