.class Landroid/net/http/RequestQueue$ActivePool;
.super Ljava/lang/Object;
.source "RequestQueue.java"

# interfaces
.implements Landroid/net/http/RequestQueue$ConnectionManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/http/RequestQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ActivePool"
.end annotation


# instance fields
.field private mConnectionCount:I

.field mIdleCache:Landroid/net/http/IdleCache;

.field mThreads:[Landroid/net/http/ConnectionThread;

.field private mTotalConnection:I

.field private mTotalRequest:I

.field final synthetic this$0:Landroid/net/http/RequestQueue;


# direct methods
.method constructor <init>(Landroid/net/http/RequestQueue;I)V
    .registers 7
    .parameter
    .parameter "connectionCount"

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 81
    new-instance v1, Landroid/net/http/IdleCache;

    #@7
    invoke-direct {v1}, Landroid/net/http/IdleCache;-><init>()V

    #@a
    iput-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mIdleCache:Landroid/net/http/IdleCache;

    #@c
    .line 82
    iput p2, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@e
    .line 83
    iget v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@10
    new-array v1, v1, [Landroid/net/http/ConnectionThread;

    #@12
    iput-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@14
    .line 85
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    iget v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@17
    if-ge v0, v1, :cond_29

    #@19
    .line 86
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@1b
    new-instance v2, Landroid/net/http/ConnectionThread;

    #@1d
    invoke-static {p1}, Landroid/net/http/RequestQueue;->access$000(Landroid/net/http/RequestQueue;)Landroid/content/Context;

    #@20
    move-result-object v3

    #@21
    invoke-direct {v2, v3, v0, p0, p1}, Landroid/net/http/ConnectionThread;-><init>(Landroid/content/Context;ILandroid/net/http/RequestQueue$ConnectionManager;Landroid/net/http/RequestFeeder;)V

    #@24
    aput-object v2, v1, v0

    #@26
    .line 85
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_15

    #@29
    .line 89
    :cond_29
    return-void
.end method

.method static synthetic access$408(Landroid/net/http/RequestQueue$ActivePool;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget v0, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalRequest:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalRequest:I

    #@6
    return v0
.end method


# virtual methods
.method disablePersistence()V
    .registers 4

    #@0
    .prologue
    .line 150
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget v2, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@3
    if-ge v1, v2, :cond_14

    #@5
    .line 151
    iget-object v2, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@7
    aget-object v2, v2, v1

    #@9
    iget-object v0, v2, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@b
    .line 152
    .local v0, connection:Landroid/net/http/Connection;
    if-eqz v0, :cond_11

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0, v2}, Landroid/net/http/Connection;->setCanPersist(Z)V

    #@11
    .line 150
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_1

    #@14
    .line 154
    .end local v0           #connection:Landroid/net/http/Connection;
    :cond_14
    iget-object v2, p0, Landroid/net/http/RequestQueue$ActivePool;->mIdleCache:Landroid/net/http/IdleCache;

    #@16
    invoke-virtual {v2}, Landroid/net/http/IdleCache;->clear()V

    #@19
    .line 155
    return-void
.end method

.method public getConnection(Landroid/content/Context;Lorg/apache/http/HttpHost;)Landroid/net/http/Connection;
    .registers 7
    .parameter "context"
    .parameter "host"

    #@0
    .prologue
    .line 174
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@2
    invoke-static {v1, p2}, Landroid/net/http/RequestQueue;->access$200(Landroid/net/http/RequestQueue;Lorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;

    #@5
    move-result-object p2

    #@6
    .line 175
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mIdleCache:Landroid/net/http/IdleCache;

    #@8
    invoke-virtual {v1, p2}, Landroid/net/http/IdleCache;->getConnection(Lorg/apache/http/HttpHost;)Landroid/net/http/Connection;

    #@b
    move-result-object v0

    #@c
    .line 176
    .local v0, con:Landroid/net/http/Connection;
    if-nez v0, :cond_26

    #@e
    .line 177
    iget v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalConnection:I

    #@10
    add-int/lit8 v1, v1, 0x1

    #@12
    iput v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalConnection:I

    #@14
    .line 178
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@16
    invoke-static {v1}, Landroid/net/http/RequestQueue;->access$000(Landroid/net/http/RequestQueue;)Landroid/content/Context;

    #@19
    move-result-object v1

    #@1a
    iget-object v2, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@1c
    invoke-static {v2}, Landroid/net/http/RequestQueue;->access$100(Landroid/net/http/RequestQueue;)Lorg/apache/http/HttpHost;

    #@1f
    move-result-object v2

    #@20
    iget-object v3, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@22
    invoke-static {v1, p2, v2, v3}, Landroid/net/http/Connection;->getConnection(Landroid/content/Context;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)Landroid/net/http/Connection;

    #@25
    move-result-object v0

    #@26
    .line 181
    :cond_26
    return-object v0
.end method

.method public getProxyHost()Lorg/apache/http/HttpHost;
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@2
    invoke-static {v0}, Landroid/net/http/RequestQueue;->access$100(Landroid/net/http/RequestQueue;)Lorg/apache/http/HttpHost;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getThread(Lorg/apache/http/HttpHost;)Landroid/net/http/ConnectionThread;
    .registers 7
    .parameter "host"

    #@0
    .prologue
    .line 161
    iget-object v4, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@2
    monitor-enter v4

    #@3
    .line 162
    const/4 v2, 0x0

    #@4
    .local v2, i:I
    :goto_4
    :try_start_4
    iget-object v3, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@6
    array-length v3, v3

    #@7
    if-ge v2, v3, :cond_1e

    #@9
    .line 163
    iget-object v3, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@b
    aget-object v1, v3, v2

    #@d
    .line 164
    .local v1, ct:Landroid/net/http/ConnectionThread;
    iget-object v0, v1, Landroid/net/http/ConnectionThread;->mConnection:Landroid/net/http/Connection;

    #@f
    .line 165
    .local v0, connection:Landroid/net/http/Connection;
    if-eqz v0, :cond_1b

    #@11
    iget-object v3, v0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@13
    invoke-virtual {v3, p1}, Lorg/apache/http/HttpHost;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_1b

    #@19
    .line 166
    monitor-exit v4

    #@1a
    .line 170
    .end local v0           #connection:Landroid/net/http/Connection;
    .end local v1           #ct:Landroid/net/http/ConnectionThread;
    :goto_1a
    return-object v1

    #@1b
    .line 162
    .restart local v0       #connection:Landroid/net/http/Connection;
    .restart local v1       #ct:Landroid/net/http/ConnectionThread;
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_4

    #@1e
    .line 169
    .end local v0           #connection:Landroid/net/http/Connection;
    .end local v1           #ct:Landroid/net/http/ConnectionThread;
    :cond_1e
    monitor-exit v4

    #@1f
    .line 170
    const/4 v1, 0x0

    #@20
    goto :goto_1a

    #@21
    .line 169
    :catchall_21
    move-exception v3

    #@22
    monitor-exit v4
    :try_end_23
    .catchall {:try_start_4 .. :try_end_23} :catchall_21

    #@23
    throw v3
.end method

.method logState()V
    .registers 5

    #@0
    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 135
    .local v0, dump:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    iget v2, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@8
    if-ge v1, v2, :cond_27

    #@a
    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    iget-object v3, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@11
    aget-object v3, v3, v1

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "\n"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 135
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_6

    #@27
    .line 138
    :cond_27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v2}, Landroid/net/http/HttpLog;->v(Ljava/lang/String;)V

    #@2e
    .line 139
    return-void
.end method

.method public recycleConnection(Landroid/net/http/Connection;)Z
    .registers 4
    .parameter "connection"

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/net/http/RequestQueue$ActivePool;->mIdleCache:Landroid/net/http/IdleCache;

    #@2
    invoke-virtual {p1}, Landroid/net/http/Connection;->getHost()Lorg/apache/http/HttpHost;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p1}, Landroid/net/http/IdleCache;->cacheConnection(Lorg/apache/http/HttpHost;Landroid/net/http/Connection;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method shutdown()V
    .registers 3

    #@0
    .prologue
    .line 98
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@3
    if-ge v0, v1, :cond_f

    #@5
    .line 99
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@7
    aget-object v1, v1, v0

    #@9
    invoke-virtual {v1}, Landroid/net/http/ConnectionThread;->requestStop()V

    #@c
    .line 98
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_1

    #@f
    .line 101
    :cond_f
    return-void
.end method

.method startConnectionThread()V
    .registers 3

    #@0
    .prologue
    .line 104
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@2
    monitor-enter v1

    #@3
    .line 105
    :try_start_3
    iget-object v0, p0, Landroid/net/http/RequestQueue$ActivePool;->this$0:Landroid/net/http/RequestQueue;

    #@5
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@8
    .line 106
    monitor-exit v1

    #@9
    .line 107
    return-void

    #@a
    .line 106
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public startTiming()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 110
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget v2, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@4
    if-ge v0, v2, :cond_15

    #@6
    .line 111
    iget-object v2, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@8
    aget-object v1, v2, v0

    #@a
    .line 112
    .local v1, rt:Landroid/net/http/ConnectionThread;
    const-wide/16 v2, -0x1

    #@c
    iput-wide v2, v1, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@e
    .line 113
    const-wide/16 v2, 0x0

    #@10
    iput-wide v2, v1, Landroid/net/http/ConnectionThread;->mTotalThreadTime:J

    #@12
    .line 110
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_2

    #@15
    .line 115
    .end local v1           #rt:Landroid/net/http/ConnectionThread;
    :cond_15
    iput v4, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalRequest:I

    #@17
    .line 116
    iput v4, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalConnection:I

    #@19
    .line 117
    return-void
.end method

.method startup()V
    .registers 3

    #@0
    .prologue
    .line 92
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@3
    if-ge v0, v1, :cond_f

    #@5
    .line 93
    iget-object v1, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@7
    aget-object v1, v1, v0

    #@9
    invoke-virtual {v1}, Landroid/net/http/ConnectionThread;->start()V

    #@c
    .line 92
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_1

    #@f
    .line 95
    :cond_f
    return-void
.end method

.method public stopTiming()V
    .registers 8

    #@0
    .prologue
    .line 120
    const/4 v2, 0x0

    #@1
    .line 121
    .local v2, totalTime:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget v3, p0, Landroid/net/http/RequestQueue$ActivePool;->mConnectionCount:I

    #@4
    if-ge v0, v3, :cond_1e

    #@6
    .line 122
    iget-object v3, p0, Landroid/net/http/RequestQueue$ActivePool;->mThreads:[Landroid/net/http/ConnectionThread;

    #@8
    aget-object v1, v3, v0

    #@a
    .line 123
    .local v1, rt:Landroid/net/http/ConnectionThread;
    iget-wide v3, v1, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@c
    const-wide/16 v5, -0x1

    #@e
    cmp-long v3, v3, v5

    #@10
    if-eqz v3, :cond_17

    #@12
    .line 124
    int-to-long v3, v2

    #@13
    iget-wide v5, v1, Landroid/net/http/ConnectionThread;->mTotalThreadTime:J

    #@15
    add-long/2addr v3, v5

    #@16
    long-to-int v2, v3

    #@17
    .line 126
    :cond_17
    const-wide/16 v3, 0x0

    #@19
    iput-wide v3, v1, Landroid/net/http/ConnectionThread;->mCurrentThreadTime:J

    #@1b
    .line 121
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_2

    #@1e
    .line 128
    .end local v1           #rt:Landroid/net/http/ConnectionThread;
    :cond_1e
    const-string v3, "Http"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Http thread used "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, " ms "

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " for "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    iget v5, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalRequest:I

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    const-string v5, " requests and "

    #@43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    iget v5, p0, Landroid/net/http/RequestQueue$ActivePool;->mTotalConnection:I

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, " new connections"

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 131
    return-void
.end method
