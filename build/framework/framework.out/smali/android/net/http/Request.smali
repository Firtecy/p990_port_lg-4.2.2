.class Landroid/net/http/Request;
.super Ljava/lang/Object;
.source "Request.java"


# static fields
.field private static final ACCEPT_ENCODING_HEADER:Ljava/lang/String; = "Accept-Encoding"

.field private static final CONTENT_LENGTH_HEADER:Ljava/lang/String; = "content-length"

.field private static final HOST_HEADER:Ljava/lang/String; = "Host"

.field private static requestContentProcessor:Lorg/apache/http/protocol/RequestContent;


# instance fields
.field private mBodyLength:I

.field private mBodyProvider:Ljava/io/InputStream;

.field volatile mCancelled:Z

.field private final mClientResource:Ljava/lang/Object;

.field private mConnection:Landroid/net/http/Connection;

.field mEventHandler:Landroid/net/http/EventHandler;

.field mFailCount:I

.field mHost:Lorg/apache/http/HttpHost;

.field mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

.field private mLoadingPaused:Z

.field mPath:Ljava/lang/String;

.field mProxyHost:Lorg/apache/http/HttpHost;

.field private mReceivedBytes:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 97
    new-instance v0, Lorg/apache/http/protocol/RequestContent;

    #@2
    invoke-direct {v0}, Lorg/apache/http/protocol/RequestContent;-><init>()V

    #@5
    sput-object v0, Landroid/net/http/Request;->requestContentProcessor:Lorg/apache/http/protocol/RequestContent;

    #@7
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Ljava/lang/String;Ljava/io/InputStream;ILandroid/net/http/EventHandler;Ljava/util/Map;)V
    .registers 11
    .parameter "method"
    .parameter "host"
    .parameter "proxyHost"
    .parameter "path"
    .parameter "bodyProvider"
    .parameter "bodyLength"
    .parameter "eventHandler"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpHost;",
            "Ljava/lang/String;",
            "Ljava/io/InputStream;",
            "I",
            "Landroid/net/http/EventHandler;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p8, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@1
    .line 114
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 72
    iput-boolean v1, p0, Landroid/net/http/Request;->mCancelled:Z

    #@6
    .line 74
    iput v1, p0, Landroid/net/http/Request;->mFailCount:I

    #@8
    .line 78
    iput v1, p0, Landroid/net/http/Request;->mReceivedBytes:I

    #@a
    .line 88
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v0, p0, Landroid/net/http/Request;->mClientResource:Ljava/lang/Object;

    #@11
    .line 91
    iput-boolean v1, p0, Landroid/net/http/Request;->mLoadingPaused:Z

    #@13
    .line 115
    iput-object p7, p0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@15
    .line 116
    iput-object p2, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@17
    .line 117
    iput-object p3, p0, Landroid/net/http/Request;->mProxyHost:Lorg/apache/http/HttpHost;

    #@19
    .line 118
    iput-object p4, p0, Landroid/net/http/Request;->mPath:Ljava/lang/String;

    #@1b
    .line 119
    iput-object p5, p0, Landroid/net/http/Request;->mBodyProvider:Ljava/io/InputStream;

    #@1d
    .line 120
    iput p6, p0, Landroid/net/http/Request;->mBodyLength:I

    #@1f
    .line 122
    if-nez p5, :cond_48

    #@21
    const-string v0, "POST"

    #@23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_48

    #@29
    .line 123
    new-instance v0, Lorg/apache/http/message/BasicHttpRequest;

    #@2b
    invoke-virtual {p0}, Landroid/net/http/Request;->getUri()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-direct {v0, p1, v1}, Lorg/apache/http/message/BasicHttpRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    iput-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@34
    .line 134
    :cond_34
    :goto_34
    const-string v0, "Host"

    #@36
    invoke-virtual {p0}, Landroid/net/http/Request;->getHostPort()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {p0, v0, v1}, Landroid/net/http/Request;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    .line 139
    const-string v0, "Accept-Encoding"

    #@3f
    const-string v1, "gzip"

    #@41
    invoke-virtual {p0, v0, v1}, Landroid/net/http/Request;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    .line 140
    invoke-virtual {p0, p8}, Landroid/net/http/Request;->addHeaders(Ljava/util/Map;)V

    #@47
    .line 141
    return-void

    #@48
    .line 125
    :cond_48
    new-instance v0, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;

    #@4a
    invoke-virtual {p0}, Landroid/net/http/Request;->getUri()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-direct {v0, p1, v1}, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    iput-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@53
    .line 130
    if-eqz p5, :cond_34

    #@55
    .line 131
    invoke-direct {p0, p5, p6}, Landroid/net/http/Request;->setBodyProvider(Ljava/io/InputStream;I)V

    #@58
    goto :goto_34
.end method

.method private static canResponseHaveBody(Lorg/apache/http/HttpRequest;I)Z
    .registers 5
    .parameter "request"
    .parameter "status"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 478
    const-string v1, "HEAD"

    #@3
    invoke-interface {p0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    #@6
    move-result-object v2

    #@7
    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 481
    :cond_11
    :goto_11
    return v0

    #@12
    :cond_12
    const/16 v1, 0xc8

    #@14
    if-lt p1, v1, :cond_11

    #@16
    const/16 v1, 0xcc

    #@18
    if-eq p1, v1, :cond_11

    #@1a
    const/16 v1, 0x130

    #@1c
    if-eq p1, v1, :cond_11

    #@1e
    const/4 v0, 0x1

    #@1f
    goto :goto_11
.end method

.method private setBodyProvider(Ljava/io/InputStream;I)V
    .registers 7
    .parameter "bodyProvider"
    .parameter "bodyLength"

    #@0
    .prologue
    .line 495
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 496
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "bodyProvider must support mark()"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 500
    :cond_e
    const v0, 0x7fffffff

    #@11
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->mark(I)V

    #@14
    .line 502
    iget-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@16
    check-cast v0, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;

    #@18
    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    #@1a
    int-to-long v2, p2

    #@1b
    invoke-direct {v1, p1, v2, v3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    #@1e
    invoke-virtual {v0, v1}, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    #@21
    .line 504
    return-void
.end method


# virtual methods
.method addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 173
    if-nez p1, :cond_d

    #@2
    .line 174
    const-string v0, "Null http header name"

    #@4
    .line 175
    .local v0, damage:Ljava/lang/String;
    invoke-static {v0}, Landroid/net/http/HttpLog;->e(Ljava/lang/String;)V

    #@7
    .line 176
    new-instance v1, Ljava/lang/NullPointerException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 178
    .end local v0           #damage:Ljava/lang/String;
    :cond_d
    if-eqz p2, :cond_15

    #@f
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_37

    #@15
    .line 179
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Null or empty value for header \""

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, "\""

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    .line 180
    .restart local v0       #damage:Ljava/lang/String;
    invoke-static {v0}, Landroid/net/http/HttpLog;->e(Ljava/lang/String;)V

    #@31
    .line 181
    new-instance v1, Ljava/lang/RuntimeException;

    #@33
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1

    #@37
    .line 183
    .end local v0           #damage:Ljava/lang/String;
    :cond_37
    iget-object v1, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@39
    invoke-virtual {v1, p1, p2}, Lorg/apache/http/message/BasicHttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 184
    return-void
.end method

.method addHeaders(Ljava/util/Map;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 191
    .local p1, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_3

    #@2
    .line 201
    :cond_2
    return-void

    #@3
    .line 196
    :cond_3
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@6
    move-result-object v2

    #@7
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 197
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_2

    #@11
    .line 198
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Ljava/util/Map$Entry;

    #@17
    .line 199
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Ljava/lang/String;

    #@1d
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Ljava/lang/String;

    #@23
    invoke-virtual {p0, v2, v3}, Landroid/net/http/Request;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    goto :goto_b
.end method

.method declared-synchronized cancel()V
    .registers 2

    #@0
    .prologue
    .line 374
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/net/http/Request;->mLoadingPaused:Z

    #@4
    .line 375
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    #@7
    .line 377
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/net/http/Request;->mCancelled:Z

    #@a
    .line 378
    iget-object v0, p0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 379
    iget-object v0, p0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@10
    invoke-virtual {v0}, Landroid/net/http/Connection;->cancel()V
    :try_end_13
    .catchall {:try_start_2 .. :try_end_13} :catchall_15

    #@13
    .line 381
    :cond_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 374
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0

    #@17
    throw v0
.end method

.method complete()V
    .registers 3

    #@0
    .prologue
    .line 457
    iget-object v1, p0, Landroid/net/http/Request;->mClientResource:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 458
    :try_start_3
    iget-object v0, p0, Landroid/net/http/Request;->mClientResource:Ljava/lang/Object;

    #@5
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@8
    .line 459
    monitor-exit v1

    #@9
    .line 460
    return-void

    #@a
    .line 459
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method error(II)V
    .registers 5
    .parameter "errorId"
    .parameter "resourceId"

    #@0
    .prologue
    .line 523
    iget-object v0, p0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@2
    iget-object v1, p0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@4
    iget-object v1, v1, Landroid/net/http/Connection;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v1, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-interface {v0, p1, v1}, Landroid/net/http/EventHandler;->error(ILjava/lang/String;)V

    #@11
    .line 527
    return-void
.end method

.method getEventHandler()Landroid/net/http/EventHandler;
    .registers 2

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@2
    return-object v0
.end method

.method getHostPort()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 384
    iget-object v2, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@2
    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 385
    .local v1, myScheme:Ljava/lang/String;
    iget-object v2, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@8
    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getPort()I

    #@b
    move-result v0

    #@c
    .line 388
    .local v0, myPort:I
    const/16 v2, 0x50

    #@e
    if-eq v0, v2, :cond_18

    #@10
    const-string v2, "http"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_24

    #@18
    :cond_18
    const/16 v2, 0x1bb

    #@1a
    if-eq v0, v2, :cond_2b

    #@1c
    const-string v2, "https"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_2b

    #@24
    .line 390
    :cond_24
    iget-object v2, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@26
    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->toHostString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    .line 392
    :goto_2a
    return-object v2

    #@2b
    :cond_2b
    iget-object v2, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@2d
    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    goto :goto_2a
.end method

.method getUri()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 397
    iget-object v0, p0, Landroid/net/http/Request;->mProxyHost:Lorg/apache/http/HttpHost;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@6
    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v1, "https"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 399
    :cond_12
    iget-object v0, p0, Landroid/net/http/Request;->mPath:Ljava/lang/String;

    #@14
    .line 401
    :goto_14
    return-object v0

    #@15
    :cond_15
    new-instance v0, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    iget-object v1, p0, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@1c
    invoke-virtual {v1}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    const-string v1, "://"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p0}, Landroid/net/http/Request;->getHostPort()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    iget-object v1, p0, Landroid/net/http/Request;->mPath:Ljava/lang/String;

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    goto :goto_14
.end method

.method public handleSslErrorResponse(Z)V
    .registers 4
    .parameter "proceed"

    #@0
    .prologue
    .line 512
    iget-object v1, p0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@2
    check-cast v1, Landroid/net/http/HttpsConnection;

    #@4
    move-object v0, v1

    #@5
    check-cast v0, Landroid/net/http/HttpsConnection;

    #@7
    .line 513
    .local v0, connection:Landroid/net/http/HttpsConnection;
    if-eqz v0, :cond_c

    #@9
    .line 514
    invoke-virtual {v0, p1}, Landroid/net/http/HttpsConnection;->restartConnection(Z)V

    #@c
    .line 516
    :cond_c
    return-void
.end method

.method readResponse(Landroid/net/http/AndroidHttpClientConnection;)V
    .registers 27
    .parameter "httpClientConnection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 245
    move-object/from16 v0, p0

    #@2
    iget-boolean v0, v0, Landroid/net/http/Request;->mCancelled:Z

    #@4
    move/from16 v21, v0

    #@6
    if-eqz v21, :cond_9

    #@8
    .line 359
    :goto_8
    return-void

    #@9
    .line 247
    :cond_9
    const/16 v18, 0x0

    #@b
    .line 248
    .local v18, statusLine:Lorg/apache/http/StatusLine;
    const/4 v10, 0x0

    #@c
    .line 249
    .local v10, hasBody:Z
    invoke-virtual/range {p1 .. p1}, Landroid/net/http/AndroidHttpClientConnection;->flush()V

    #@f
    .line 250
    const/16 v17, 0x0

    #@11
    .line 252
    .local v17, statusCode:I
    new-instance v11, Landroid/net/http/Headers;

    #@13
    invoke-direct {v11}, Landroid/net/http/Headers;-><init>()V

    #@16
    .line 254
    .local v11, header:Landroid/net/http/Headers;
    :cond_16
    move-object/from16 v0, p1

    #@18
    invoke-virtual {v0, v11}, Landroid/net/http/AndroidHttpClientConnection;->parseResponseHeader(Landroid/net/http/Headers;)Lorg/apache/http/StatusLine;

    #@1b
    move-result-object v18

    #@1c
    .line 255
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getStatusCode()I

    #@1f
    move-result v17

    #@20
    .line 256
    const/16 v21, 0xc8

    #@22
    move/from16 v0, v17

    #@24
    move/from16 v1, v21

    #@26
    if-lt v0, v1, :cond_16

    #@28
    .line 261
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getProtocolVersion()Lorg/apache/http/ProtocolVersion;

    #@2b
    move-result-object v20

    #@2c
    .line 262
    .local v20, v:Lorg/apache/http/ProtocolVersion;
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@30
    move-object/from16 v21, v0

    #@32
    invoke-virtual/range {v20 .. v20}, Lorg/apache/http/ProtocolVersion;->getMajor()I

    #@35
    move-result v22

    #@36
    invoke-virtual/range {v20 .. v20}, Lorg/apache/http/ProtocolVersion;->getMinor()I

    #@39
    move-result v23

    #@3a
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    #@3d
    move-result-object v24

    #@3e
    move-object/from16 v0, v21

    #@40
    move/from16 v1, v22

    #@42
    move/from16 v2, v23

    #@44
    move/from16 v3, v17

    #@46
    move-object/from16 v4, v24

    #@48
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/net/http/EventHandler;->status(IIILjava/lang/String;)V

    #@4b
    .line 264
    move-object/from16 v0, p0

    #@4d
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@4f
    move-object/from16 v21, v0

    #@51
    move-object/from16 v0, v21

    #@53
    invoke-interface {v0, v11}, Landroid/net/http/EventHandler;->headers(Landroid/net/http/Headers;)V

    #@56
    .line 265
    const/4 v9, 0x0

    #@57
    .line 266
    .local v9, entity:Lorg/apache/http/HttpEntity;
    move-object/from16 v0, p0

    #@59
    iget-object v0, v0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@5b
    move-object/from16 v21, v0

    #@5d
    move-object/from16 v0, v21

    #@5f
    move/from16 v1, v17

    #@61
    invoke-static {v0, v1}, Landroid/net/http/Request;->canResponseHaveBody(Lorg/apache/http/HttpRequest;I)Z

    #@64
    move-result v10

    #@65
    .line 268
    if-eqz v10, :cond_6d

    #@67
    .line 269
    move-object/from16 v0, p1

    #@69
    invoke-virtual {v0, v11}, Landroid/net/http/AndroidHttpClientConnection;->receiveResponseEntity(Landroid/net/http/Headers;)Lorg/apache/http/HttpEntity;

    #@6c
    move-result-object v9

    #@6d
    .line 273
    :cond_6d
    const-string v21, "bytes"

    #@6f
    invoke-virtual {v11}, Landroid/net/http/Headers;->getAcceptRanges()Ljava/lang/String;

    #@72
    move-result-object v22

    #@73
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@76
    move-result v19

    #@77
    .line 276
    .local v19, supportPartialContent:Z
    if-eqz v9, :cond_f0

    #@79
    .line 277
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    #@7c
    move-result-object v12

    #@7d
    .line 280
    .local v12, is:Ljava/io/InputStream;
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    #@80
    move-result-object v6

    #@81
    .line 281
    .local v6, contentEncoding:Lorg/apache/http/Header;
    const/4 v15, 0x0

    #@82
    .line 282
    .local v15, nis:Ljava/io/InputStream;
    const/4 v5, 0x0

    #@83
    .line 283
    .local v5, buf:[B
    const/4 v7, 0x0

    #@84
    .line 285
    .local v7, count:I
    if-eqz v6, :cond_115

    #@86
    :try_start_86
    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    #@89
    move-result-object v21

    #@8a
    const-string v22, "gzip"

    #@8c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v21

    #@90
    if-eqz v21, :cond_115

    #@92
    .line 287
    new-instance v16, Ljava/util/zip/GZIPInputStream;

    #@94
    move-object/from16 v0, v16

    #@96
    invoke-direct {v0, v12}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    #@99
    .end local v15           #nis:Ljava/io/InputStream;
    .local v16, nis:Ljava/io/InputStream;
    move-object/from16 v15, v16

    #@9b
    .line 294
    .end local v16           #nis:Ljava/io/InputStream;
    .restart local v15       #nis:Ljava/io/InputStream;
    :goto_9b
    move-object/from16 v0, p0

    #@9d
    iget-object v0, v0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@9f
    move-object/from16 v21, v0

    #@a1
    invoke-virtual/range {v21 .. v21}, Landroid/net/http/Connection;->getBuf()[B

    #@a4
    move-result-object v5

    #@a5
    .line 295
    const/4 v13, 0x0

    #@a6
    .line 296
    .local v13, len:I
    array-length v0, v5

    #@a7
    move/from16 v21, v0

    #@a9
    div-int/lit8 v14, v21, 0x2

    #@ab
    .line 297
    .local v14, lowWater:I
    :cond_ab
    :goto_ab
    const/16 v21, -0x1

    #@ad
    move/from16 v0, v21

    #@af
    if-eq v13, v0, :cond_150

    #@b1
    .line 298
    monitor-enter p0
    :try_end_b2
    .catchall {:try_start_86 .. :try_end_b2} :catchall_177
    .catch Ljava/io/EOFException; {:try_start_86 .. :try_end_b2} :catch_dd
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_b2} :catch_156

    #@b2
    .line 299
    :goto_b2
    :try_start_b2
    move-object/from16 v0, p0

    #@b4
    iget-boolean v0, v0, Landroid/net/http/Request;->mLoadingPaused:Z

    #@b6
    move/from16 v21, v0
    :try_end_b8
    .catchall {:try_start_b2 .. :try_end_b8} :catchall_da

    #@b8
    if-eqz v21, :cond_117

    #@ba
    .line 305
    :try_start_ba
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->wait()V
    :try_end_bd
    .catchall {:try_start_ba .. :try_end_bd} :catchall_da
    .catch Ljava/lang/InterruptedException; {:try_start_ba .. :try_end_bd} :catch_be

    #@bd
    goto :goto_b2

    #@be
    .line 306
    :catch_be
    move-exception v8

    #@bf
    .line 307
    .local v8, e:Ljava/lang/InterruptedException;
    :try_start_bf
    new-instance v21, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v22, "Interrupted exception whilst network thread paused at WebCore\'s request. "

    #@c6
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v21

    #@ca
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    #@cd
    move-result-object v22

    #@ce
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v21

    #@d2
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v21

    #@d6
    invoke-static/range {v21 .. v21}, Landroid/net/http/HttpLog;->e(Ljava/lang/String;)V

    #@d9
    goto :goto_b2

    #@da
    .line 312
    .end local v8           #e:Ljava/lang/InterruptedException;
    :catchall_da
    move-exception v21

    #@db
    monitor-exit p0
    :try_end_dc
    .catchall {:try_start_bf .. :try_end_dc} :catchall_da

    #@dc
    :try_start_dc
    throw v21
    :try_end_dd
    .catchall {:try_start_dc .. :try_end_dd} :catchall_177
    .catch Ljava/io/EOFException; {:try_start_dc .. :try_end_dd} :catch_dd
    .catch Ljava/io/IOException; {:try_start_dc .. :try_end_dd} :catch_156

    #@dd
    .line 326
    .end local v13           #len:I
    .end local v14           #lowWater:I
    :catch_dd
    move-exception v8

    #@de
    .line 330
    .local v8, e:Ljava/io/EOFException;
    if-lez v7, :cond_eb

    #@e0
    .line 332
    :try_start_e0
    move-object/from16 v0, p0

    #@e2
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@e4
    move-object/from16 v21, v0

    #@e6
    move-object/from16 v0, v21

    #@e8
    invoke-interface {v0, v5, v7}, Landroid/net/http/EventHandler;->data([BI)V
    :try_end_eb
    .catchall {:try_start_e0 .. :try_end_eb} :catchall_177

    #@eb
    .line 347
    :cond_eb
    if-eqz v15, :cond_f0

    #@ed
    .line 348
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    #@f0
    .line 352
    .end local v5           #buf:[B
    .end local v6           #contentEncoding:Lorg/apache/http/Header;
    .end local v7           #count:I
    .end local v8           #e:Ljava/io/EOFException;
    .end local v12           #is:Ljava/io/InputStream;
    .end local v15           #nis:Ljava/io/InputStream;
    :cond_f0
    :goto_f0
    move-object/from16 v0, p0

    #@f2
    iget-object v0, v0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@f4
    move-object/from16 v21, v0

    #@f6
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getProtocolVersion()Lorg/apache/http/ProtocolVersion;

    #@f9
    move-result-object v22

    #@fa
    invoke-virtual {v11}, Landroid/net/http/Headers;->getConnectionType()I

    #@fd
    move-result v23

    #@fe
    move-object/from16 v0, v21

    #@100
    move-object/from16 v1, v22

    #@102
    move/from16 v2, v23

    #@104
    invoke-virtual {v0, v9, v1, v2}, Landroid/net/http/Connection;->setCanPersist(Lorg/apache/http/HttpEntity;Lorg/apache/http/ProtocolVersion;I)V

    #@107
    .line 354
    move-object/from16 v0, p0

    #@109
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@10b
    move-object/from16 v21, v0

    #@10d
    invoke-interface/range {v21 .. v21}, Landroid/net/http/EventHandler;->endData()V

    #@110
    .line 355
    invoke-virtual/range {p0 .. p0}, Landroid/net/http/Request;->complete()V

    #@113
    goto/16 :goto_8

    #@115
    .line 289
    .restart local v5       #buf:[B
    .restart local v6       #contentEncoding:Lorg/apache/http/Header;
    .restart local v7       #count:I
    .restart local v12       #is:Ljava/io/InputStream;
    .restart local v15       #nis:Ljava/io/InputStream;
    :cond_115
    move-object v15, v12

    #@116
    goto :goto_9b

    #@117
    .line 312
    .restart local v13       #len:I
    .restart local v14       #lowWater:I
    :cond_117
    :try_start_117
    monitor-exit p0
    :try_end_118
    .catchall {:try_start_117 .. :try_end_118} :catchall_da

    #@118
    .line 314
    :try_start_118
    array-length v0, v5

    #@119
    move/from16 v21, v0

    #@11b
    sub-int v21, v21, v7

    #@11d
    move/from16 v0, v21

    #@11f
    invoke-virtual {v15, v5, v7, v0}, Ljava/io/InputStream;->read([BII)I

    #@122
    move-result v13

    #@123
    .line 316
    const/16 v21, -0x1

    #@125
    move/from16 v0, v21

    #@127
    if-eq v13, v0, :cond_13a

    #@129
    .line 317
    add-int/2addr v7, v13

    #@12a
    .line 318
    if-eqz v19, :cond_13a

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget v0, v0, Landroid/net/http/Request;->mReceivedBytes:I

    #@130
    move/from16 v21, v0

    #@132
    add-int v21, v21, v13

    #@134
    move/from16 v0, v21

    #@136
    move-object/from16 v1, p0

    #@138
    iput v0, v1, Landroid/net/http/Request;->mReceivedBytes:I

    #@13a
    .line 320
    :cond_13a
    const/16 v21, -0x1

    #@13c
    move/from16 v0, v21

    #@13e
    if-eq v13, v0, :cond_142

    #@140
    if-lt v7, v14, :cond_ab

    #@142
    .line 322
    :cond_142
    move-object/from16 v0, p0

    #@144
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@146
    move-object/from16 v21, v0

    #@148
    move-object/from16 v0, v21

    #@14a
    invoke-interface {v0, v5, v7}, Landroid/net/http/EventHandler;->data([BI)V
    :try_end_14d
    .catchall {:try_start_118 .. :try_end_14d} :catchall_177
    .catch Ljava/io/EOFException; {:try_start_118 .. :try_end_14d} :catch_dd
    .catch Ljava/io/IOException; {:try_start_118 .. :try_end_14d} :catch_156

    #@14d
    .line 323
    const/4 v7, 0x0

    #@14e
    goto/16 :goto_ab

    #@150
    .line 347
    :cond_150
    if-eqz v15, :cond_f0

    #@152
    .line 348
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    #@155
    goto :goto_f0

    #@156
    .line 335
    .end local v13           #len:I
    .end local v14           #lowWater:I
    :catch_156
    move-exception v8

    #@157
    .line 337
    .local v8, e:Ljava/io/IOException;
    const/16 v21, 0xc8

    #@159
    move/from16 v0, v17

    #@15b
    move/from16 v1, v21

    #@15d
    if-eq v0, v1, :cond_167

    #@15f
    const/16 v21, 0xce

    #@161
    move/from16 v0, v17

    #@163
    move/from16 v1, v21

    #@165
    if-ne v0, v1, :cond_17e

    #@167
    .line 339
    :cond_167
    if-eqz v19, :cond_176

    #@169
    if-lez v7, :cond_176

    #@16b
    .line 342
    :try_start_16b
    move-object/from16 v0, p0

    #@16d
    iget-object v0, v0, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@16f
    move-object/from16 v21, v0

    #@171
    move-object/from16 v0, v21

    #@173
    invoke-interface {v0, v5, v7}, Landroid/net/http/EventHandler;->data([BI)V

    #@176
    .line 344
    :cond_176
    throw v8
    :try_end_177
    .catchall {:try_start_16b .. :try_end_177} :catchall_177

    #@177
    .line 347
    .end local v8           #e:Ljava/io/IOException;
    :catchall_177
    move-exception v21

    #@178
    if-eqz v15, :cond_17d

    #@17a
    .line 348
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    #@17d
    :cond_17d
    throw v21

    #@17e
    .line 347
    .restart local v8       #e:Ljava/io/IOException;
    :cond_17e
    if-eqz v15, :cond_f0

    #@180
    .line 348
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    #@183
    goto/16 :goto_f0
.end method

.method reset()V
    .registers 5

    #@0
    .prologue
    .line 418
    iget-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@2
    const-string v1, "content-length"

    #@4
    invoke-virtual {v0, v1}, Lorg/apache/http/message/BasicHttpRequest;->removeHeaders(Ljava/lang/String;)V

    #@7
    .line 420
    iget-object v0, p0, Landroid/net/http/Request;->mBodyProvider:Ljava/io/InputStream;

    #@9
    if-eqz v0, :cond_17

    #@b
    .line 422
    :try_start_b
    iget-object v0, p0, Landroid/net/http/Request;->mBodyProvider:Ljava/io/InputStream;

    #@d
    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_59

    #@10
    .line 428
    :goto_10
    iget-object v0, p0, Landroid/net/http/Request;->mBodyProvider:Ljava/io/InputStream;

    #@12
    iget v1, p0, Landroid/net/http/Request;->mBodyLength:I

    #@14
    invoke-direct {p0, v0, v1}, Landroid/net/http/Request;->setBodyProvider(Ljava/io/InputStream;I)V

    #@17
    .line 431
    :cond_17
    iget v0, p0, Landroid/net/http/Request;->mReceivedBytes:I

    #@19
    if-lez v0, :cond_58

    #@1b
    .line 433
    const/4 v0, 0x0

    #@1c
    iput v0, p0, Landroid/net/http/Request;->mFailCount:I

    #@1e
    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v1, "*** Request.reset() to range:"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget v1, p0, Landroid/net/http/Request;->mReceivedBytes:I

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    invoke-static {v0}, Landroid/net/http/HttpLog;->v(Ljava/lang/String;)V

    #@36
    .line 437
    iget-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@38
    const-string v1, "Range"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "bytes="

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    iget v3, p0, Landroid/net/http/Request;->mReceivedBytes:I

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, "-"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v0, v1, v2}, Lorg/apache/http/message/BasicHttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@58
    .line 439
    :cond_58
    return-void

    #@59
    .line 423
    :catch_59
    move-exception v0

    #@5a
    goto :goto_10
.end method

.method sendRequest(Landroid/net/http/AndroidHttpClientConnection;)V
    .registers 5
    .parameter "httpClientConnection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 209
    iget-boolean v0, p0, Landroid/net/http/Request;->mCancelled:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 234
    :cond_4
    :goto_4
    return-void

    #@5
    .line 223
    :cond_5
    sget-object v0, Landroid/net/http/Request;->requestContentProcessor:Lorg/apache/http/protocol/RequestContent;

    #@7
    iget-object v1, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@9
    iget-object v2, p0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@b
    invoke-virtual {v2}, Landroid/net/http/Connection;->getHttpContext()Lorg/apache/http/protocol/HttpContext;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v0, v1, v2}, Lorg/apache/http/protocol/RequestContent;->process(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V

    #@12
    .line 225
    iget-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@14
    invoke-virtual {p1, v0}, Landroid/net/http/AndroidHttpClientConnection;->sendRequestHeader(Lorg/apache/http/HttpRequest;)V

    #@17
    .line 226
    iget-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@19
    instance-of v0, v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    #@1b
    if-eqz v0, :cond_4

    #@1d
    .line 227
    iget-object v0, p0, Landroid/net/http/Request;->mHttpRequest:Lorg/apache/http/message/BasicHttpRequest;

    #@1f
    check-cast v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    #@21
    invoke-virtual {p1, v0}, Landroid/net/http/AndroidHttpClientConnection;->sendRequestEntity(Lorg/apache/http/HttpEntityEnclosingRequest;)V

    #@24
    goto :goto_4
.end method

.method setConnection(Landroid/net/http/Connection;)V
    .registers 2
    .parameter "connection"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Landroid/net/http/Request;->mConnection:Landroid/net/http/Connection;

    #@2
    .line 160
    return-void
.end method

.method declared-synchronized setLoadingPaused(Z)V
    .registers 3
    .parameter "pause"

    #@0
    .prologue
    .line 147
    monitor-enter p0

    #@1
    :try_start_1
    iput-boolean p1, p0, Landroid/net/http/Request;->mLoadingPaused:Z

    #@3
    .line 150
    iget-boolean v0, p0, Landroid/net/http/Request;->mLoadingPaused:Z

    #@5
    if-nez v0, :cond_a

    #@7
    .line 151
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 153
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 147
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Landroid/net/http/Request;->mPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method waitUntilComplete()V
    .registers 3

    #@0
    .prologue
    .line 446
    iget-object v1, p0, Landroid/net/http/Request;->mClientResource:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 449
    :try_start_3
    iget-object v0, p0, Landroid/net/http/Request;->mClientResource:Ljava/lang/Object;

    #@5
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_8} :catch_d

    #@8
    .line 453
    :goto_8
    :try_start_8
    monitor-exit v1

    #@9
    .line 454
    return-void

    #@a
    .line 453
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_8 .. :try_end_c} :catchall_a

    #@c
    throw v0

    #@d
    .line 451
    :catch_d
    move-exception v0

    #@e
    goto :goto_8
.end method
