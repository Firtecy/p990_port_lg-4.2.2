.class Landroid/net/http/IdleCache$IdleReaper;
.super Ljava/lang/Thread;
.source "IdleCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/http/IdleCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleReaper"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/http/IdleCache;


# direct methods
.method private constructor <init>(Landroid/net/http/IdleCache;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 144
    iput-object p1, p0, Landroid/net/http/IdleCache$IdleReaper;->this$0:Landroid/net/http/IdleCache;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/http/IdleCache;Landroid/net/http/IdleCache$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 144
    invoke-direct {p0, p1}, Landroid/net/http/IdleCache$IdleReaper;-><init>(Landroid/net/http/IdleCache;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 147
    const/4 v0, 0x0

    #@1
    .line 149
    .local v0, check:I
    const-string v1, "IdleReaper"

    #@3
    invoke-virtual {p0, v1}, Landroid/net/http/IdleCache$IdleReaper;->setName(Ljava/lang/String;)V

    #@6
    .line 150
    const/16 v1, 0xa

    #@8
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@b
    .line 152
    iget-object v2, p0, Landroid/net/http/IdleCache$IdleReaper;->this$0:Landroid/net/http/IdleCache;

    #@d
    monitor-enter v2

    #@e
    .line 153
    :goto_e
    const/4 v1, 0x5

    #@f
    if-ge v0, v1, :cond_2d

    #@11
    .line 155
    :try_start_11
    iget-object v1, p0, Landroid/net/http/IdleCache$IdleReaper;->this$0:Landroid/net/http/IdleCache;

    #@13
    const-wide/16 v3, 0x7d0

    #@15
    invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_2a
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_18} :catch_35

    #@18
    .line 158
    :goto_18
    :try_start_18
    iget-object v1, p0, Landroid/net/http/IdleCache$IdleReaper;->this$0:Landroid/net/http/IdleCache;

    #@1a
    invoke-static {v1}, Landroid/net/http/IdleCache;->access$100(Landroid/net/http/IdleCache;)I

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_23

    #@20
    .line 159
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_e

    #@23
    .line 161
    :cond_23
    const/4 v0, 0x0

    #@24
    .line 162
    iget-object v1, p0, Landroid/net/http/IdleCache$IdleReaper;->this$0:Landroid/net/http/IdleCache;

    #@26
    invoke-static {v1}, Landroid/net/http/IdleCache;->access$200(Landroid/net/http/IdleCache;)V

    #@29
    goto :goto_e

    #@2a
    .line 166
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_18 .. :try_end_2c} :catchall_2a

    #@2c
    throw v1

    #@2d
    .line 165
    :cond_2d
    :try_start_2d
    iget-object v1, p0, Landroid/net/http/IdleCache$IdleReaper;->this$0:Landroid/net/http/IdleCache;

    #@2f
    const/4 v3, 0x0

    #@30
    invoke-static {v1, v3}, Landroid/net/http/IdleCache;->access$302(Landroid/net/http/IdleCache;Landroid/net/http/IdleCache$IdleReaper;)Landroid/net/http/IdleCache$IdleReaper;

    #@33
    .line 166
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_2d .. :try_end_34} :catchall_2a

    #@34
    .line 173
    return-void

    #@35
    .line 156
    :catch_35
    move-exception v1

    #@36
    goto :goto_18
.end method
