.class public Landroid/net/http/CertificateChainValidator;
.super Ljava/lang/Object;
.source "CertificateChainValidator.java"


# static fields
.field private static final sInstance:Landroid/net/http/CertificateChainValidator;

.field private static final sVerifier:Ljavax/net/ssl/DefaultHostnameVerifier;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 45
    new-instance v0, Landroid/net/http/CertificateChainValidator;

    #@2
    invoke-direct {v0}, Landroid/net/http/CertificateChainValidator;-><init>()V

    #@5
    sput-object v0, Landroid/net/http/CertificateChainValidator;->sInstance:Landroid/net/http/CertificateChainValidator;

    #@7
    .line 48
    new-instance v0, Ljavax/net/ssl/DefaultHostnameVerifier;

    #@9
    invoke-direct {v0}, Ljavax/net/ssl/DefaultHostnameVerifier;-><init>()V

    #@c
    sput-object v0, Landroid/net/http/CertificateChainValidator;->sVerifier:Ljavax/net/ssl/DefaultHostnameVerifier;

    #@e
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private closeSocketThrowException(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V
    .registers 5
    .parameter "socket"
    .parameter "errorMessage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    if-eqz p1, :cond_e

    #@2
    .line 203
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    #@5
    move-result-object v0

    #@6
    .line 204
    .local v0, session:Ljavax/net/ssl/SSLSession;
    if-eqz v0, :cond_b

    #@8
    .line 205
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->invalidate()V

    #@b
    .line 208
    :cond_b
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->close()V

    #@e
    .line 211
    .end local v0           #session:Ljavax/net/ssl/SSLSession;
    :cond_e
    new-instance v1, Ljavax/net/ssl/SSLHandshakeException;

    #@10
    invoke-direct {v1, p2}, Ljavax/net/ssl/SSLHandshakeException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1
.end method

.method private closeSocketThrowException(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "socket"
    .parameter "errorMessage"
    .parameter "defaultErrorMessage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 192
    if-eqz p2, :cond_6

    #@2
    .end local p2
    :goto_2
    invoke-direct {p0, p1, p2}, Landroid/net/http/CertificateChainValidator;->closeSocketThrowException(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    #@5
    .line 194
    return-void

    #@6
    .restart local p2
    :cond_6
    move-object p2, p3

    #@7
    .line 192
    goto :goto_2
.end method

.method public static getInstance()Landroid/net/http/CertificateChainValidator;
    .registers 1

    #@0
    .prologue
    .line 55
    sget-object v0, Landroid/net/http/CertificateChainValidator;->sInstance:Landroid/net/http/CertificateChainValidator;

    #@2
    return-object v0
.end method

.method public static handleTrustStorageUpdate()V
    .registers 4

    #@0
    .prologue
    .line 134
    :try_start_0
    invoke-static {}, Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;->getDefaultTrustManager()Ljavax/net/ssl/X509TrustManager;

    #@3
    move-result-object v2

    #@4
    .line 135
    .local v2, x509TrustManager:Ljavax/net/ssl/X509TrustManager;
    instance-of v3, v2, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@6
    if-eqz v3, :cond_f

    #@8
    .line 136
    move-object v0, v2

    #@9
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@b
    move-object v1, v0

    #@c
    .line 137
    .local v1, trustManager:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;
    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;->handleTrustStorageUpdate()V
    :try_end_f
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_f} :catch_10

    #@f
    .line 141
    .end local v1           #trustManager:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;
    :cond_f
    :goto_f
    return-void

    #@10
    .line 139
    :catch_10
    move-exception v3

    #@11
    goto :goto_f
.end method

.method public static verifyServerCertificates([[BLjava/lang/String;Ljava/lang/String;)Landroid/net/http/SslError;
    .registers 7
    .parameter "certChain"
    .parameter "domain"
    .parameter "authType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 115
    if-eqz p0, :cond_5

    #@2
    array-length v2, p0

    #@3
    if-nez v2, :cond_d

    #@5
    .line 116
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v3, "bad certificate chain"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 119
    :cond_d
    array-length v2, p0

    #@e
    new-array v1, v2, [Ljava/security/cert/X509Certificate;

    #@10
    .line 121
    .local v1, serverCertificates:[Ljava/security/cert/X509Certificate;
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    array-length v2, p0

    #@12
    if-ge v0, v2, :cond_20

    #@14
    .line 122
    new-instance v2, Lorg/apache/harmony/security/provider/cert/X509CertImpl;

    #@16
    aget-object v3, p0, v0

    #@18
    invoke-direct {v2, v3}, Lorg/apache/harmony/security/provider/cert/X509CertImpl;-><init>([B)V

    #@1b
    aput-object v2, v1, v0

    #@1d
    .line 121
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_11

    #@20
    .line 125
    :cond_20
    invoke-static {v1, p1, p2}, Landroid/net/http/CertificateChainValidator;->verifyServerDomainAndCertificates([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Landroid/net/http/SslError;

    #@23
    move-result-object v2

    #@24
    return-object v2
.end method

.method private static verifyServerDomainAndCertificates([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Landroid/net/http/SslError;
    .registers 11
    .parameter "chain"
    .parameter "domain"
    .parameter "authType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 155
    aget-object v1, p0, v4

    #@3
    .line 156
    .local v1, currCertificate:Ljava/security/cert/X509Certificate;
    if-nez v1, :cond_d

    #@5
    .line 157
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v7, "certificate for this site is null"

    #@9
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v6

    #@d
    .line 160
    :cond_d
    if-eqz p1, :cond_1e

    #@f
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@12
    move-result v6

    #@13
    if-nez v6, :cond_1e

    #@15
    sget-object v6, Landroid/net/http/CertificateChainValidator;->sVerifier:Ljavax/net/ssl/DefaultHostnameVerifier;

    #@17
    invoke-virtual {v6, p1, v1}, Ljavax/net/ssl/DefaultHostnameVerifier;->verify(Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_1e

    #@1d
    const/4 v4, 0x1

    #@1e
    .line 163
    .local v4, valid:Z
    :cond_1e
    if-nez v4, :cond_27

    #@20
    .line 167
    new-instance v6, Landroid/net/http/SslError;

    #@22
    const/4 v7, 0x2

    #@23
    invoke-direct {v6, v7, v1}, Landroid/net/http/SslError;-><init>(ILjava/security/cert/X509Certificate;)V

    #@26
    .line 184
    :goto_26
    return-object v6

    #@27
    .line 171
    :cond_27
    :try_start_27
    invoke-static {}, Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;->getDefaultTrustManager()Ljavax/net/ssl/X509TrustManager;

    #@2a
    move-result-object v5

    #@2b
    .line 172
    .local v5, x509TrustManager:Ljavax/net/ssl/X509TrustManager;
    instance-of v6, v5, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@2d
    if-eqz v6, :cond_38

    #@2f
    .line 173
    move-object v0, v5

    #@30
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@32
    move-object v3, v0

    #@33
    .line 174
    .local v3, trustManager:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;
    invoke-virtual {v3, p0, p2, p1}, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    #@36
    .line 178
    .end local v3           #trustManager:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;
    :goto_36
    const/4 v6, 0x0

    #@37
    goto :goto_26

    #@38
    .line 176
    :cond_38
    invoke-interface {v5, p0, p2}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    :try_end_3b
    .catch Ljava/security/GeneralSecurityException; {:try_start_27 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_36

    #@3c
    .line 179
    .end local v5           #x509TrustManager:Ljavax/net/ssl/X509TrustManager;
    :catch_3c
    move-exception v2

    #@3d
    .line 184
    .local v2, e:Ljava/security/GeneralSecurityException;
    new-instance v6, Landroid/net/http/SslError;

    #@3f
    const/4 v7, 0x3

    #@40
    invoke-direct {v6, v7, v1}, Landroid/net/http/SslError;-><init>(ILjava/security/cert/X509Certificate;)V

    #@43
    goto :goto_26
.end method


# virtual methods
.method public doHandshakeAndValidateServerCertificates(Landroid/net/http/HttpsConnection;Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)Landroid/net/http/SslError;
    .registers 9
    .parameter "connection"
    .parameter "sslSocket"
    .parameter "domain"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 78
    invoke-virtual {p2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    #@4
    move-result-object v1

    #@5
    .line 79
    .local v1, sslSession:Ljavax/net/ssl/SSLSession;
    invoke-interface {v1}, Ljavax/net/ssl/SSLSession;->isValid()Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_10

    #@b
    .line 80
    const-string v2, "failed to perform SSL handshake"

    #@d
    invoke-direct {p0, p2, v2}, Landroid/net/http/CertificateChainValidator;->closeSocketThrowException(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    #@10
    .line 84
    :cond_10
    invoke-virtual {p2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    #@13
    move-result-object v2

    #@14
    invoke-interface {v2}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    #@17
    move-result-object v0

    #@18
    .line 87
    .local v0, peerCertificates:[Ljava/security/cert/Certificate;
    if-eqz v0, :cond_1d

    #@1a
    array-length v2, v0

    #@1b
    if-nez v2, :cond_2d

    #@1d
    .line 88
    :cond_1d
    const-string v2, "failed to retrieve peer certificates"

    #@1f
    invoke-direct {p0, p2, v2}, Landroid/net/http/CertificateChainValidator;->closeSocketThrowException(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    #@22
    .line 100
    :cond_22
    :goto_22
    check-cast v0, [Ljava/security/cert/X509Certificate;

    #@24
    .end local v0           #peerCertificates:[Ljava/security/cert/Certificate;
    check-cast v0, [Ljava/security/cert/X509Certificate;

    #@26
    const-string v2, "RSA"

    #@28
    invoke-static {v0, p3, v2}, Landroid/net/http/CertificateChainValidator;->verifyServerDomainAndCertificates([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Landroid/net/http/SslError;

    #@2b
    move-result-object v2

    #@2c
    return-object v2

    #@2d
    .line 92
    .restart local v0       #peerCertificates:[Ljava/security/cert/Certificate;
    :cond_2d
    if-eqz p1, :cond_22

    #@2f
    .line 93
    aget-object v2, v0, v4

    #@31
    if-eqz v2, :cond_22

    #@33
    .line 94
    new-instance v3, Landroid/net/http/SslCertificate;

    #@35
    aget-object v2, v0, v4

    #@37
    check-cast v2, Ljava/security/cert/X509Certificate;

    #@39
    invoke-direct {v3, v2}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    #@3c
    invoke-virtual {p1, v3}, Landroid/net/http/HttpsConnection;->setCertificate(Landroid/net/http/SslCertificate;)V

    #@3f
    goto :goto_22
.end method
