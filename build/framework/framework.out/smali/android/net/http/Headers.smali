.class public final Landroid/net/http/Headers;
.super Ljava/lang/Object;
.source "Headers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/http/Headers$HeaderCallback;
    }
.end annotation


# static fields
.field public static final ACCEPT_RANGES:Ljava/lang/String; = "accept-ranges"

.field public static final CACHE_CONTROL:Ljava/lang/String; = "cache-control"

.field public static final CONN_CLOSE:I = 0x1

.field public static final CONN_DIRECTIVE:Ljava/lang/String; = "connection"

.field public static final CONN_KEEP_ALIVE:I = 0x2

.field public static final CONTENT_DISPOSITION:Ljava/lang/String; = "content-disposition"

.field public static final CONTENT_ENCODING:Ljava/lang/String; = "content-encoding"

.field public static final CONTENT_LEN:Ljava/lang/String; = "content-length"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "content-type"

.field public static final ETAG:Ljava/lang/String; = "etag"

.field public static final EXPIRES:Ljava/lang/String; = "expires"

.field private static final HASH_ACCEPT_RANGES:I = 0x53476b3b

.field private static final HASH_CACHE_CONTROL:I = -0xc71a9ee

.field private static final HASH_CONN_DIRECTIVE:I = -0x2e3b8122

.field private static final HASH_CONTENT_DISPOSITION:I = -0x4b88f79d

.field private static final HASH_CONTENT_ENCODING:I = 0x7ce07427

.field private static final HASH_CONTENT_LEN:I = -0x4384d946

.field private static final HASH_CONTENT_TYPE:I = 0x2ed4600e

.field private static final HASH_ETAG:I = 0x2fa915

.field private static final HASH_EXPIRES:I = -0x4e0958cc

.field private static final HASH_LAST_MODIFIED:I = 0x8f17c20

.field private static final HASH_LOCATION:I = 0x714f9fb5

.field private static final HASH_PRAGMA:I = -0x3a6d1ac4

.field private static final HASH_PROXY_AUTHENTICATE:I = -0x11fc9c2c

.field private static final HASH_PROXY_CONNECTION:I = 0x110aef9d

.field private static final HASH_REFRESH:I = 0x40b292db

.field private static final HASH_SET_COOKIE:I = 0x49be662f

.field private static final HASH_TRANSFER_ENCODING:I = 0x4bf6b0f5

.field private static final HASH_WWW_AUTHENTICATE:I = -0xe7c74b5

.field private static final HASH_X_PERMITTED_CROSS_DOMAIN_POLICIES:I = -0x5034229e

.field private static final HEADER_COUNT:I = 0x13

.field private static final IDX_ACCEPT_RANGES:I = 0xa

.field private static final IDX_CACHE_CONTROL:I = 0xc

.field private static final IDX_CONN_DIRECTIVE:I = 0x4

.field private static final IDX_CONTENT_DISPOSITION:I = 0x9

.field private static final IDX_CONTENT_ENCODING:I = 0x3

.field private static final IDX_CONTENT_LEN:I = 0x1

.field private static final IDX_CONTENT_TYPE:I = 0x2

.field private static final IDX_ETAG:I = 0xe

.field private static final IDX_EXPIRES:I = 0xb

.field private static final IDX_LAST_MODIFIED:I = 0xd

.field private static final IDX_LOCATION:I = 0x5

.field private static final IDX_PRAGMA:I = 0x10

.field private static final IDX_PROXY_AUTHENTICATE:I = 0x8

.field private static final IDX_PROXY_CONNECTION:I = 0x6

.field private static final IDX_REFRESH:I = 0x11

.field private static final IDX_SET_COOKIE:I = 0xf

.field private static final IDX_TRANSFER_ENCODING:I = 0x0

.field private static final IDX_WWW_AUTHENTICATE:I = 0x7

.field private static final IDX_X_PERMITTED_CROSS_DOMAIN_POLICIES:I = 0x12

.field public static final LAST_MODIFIED:Ljava/lang/String; = "last-modified"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field private static final LOGTAG:Ljava/lang/String; = "Http"

.field public static final NO_CONN_TYPE:I = 0x0

.field public static final NO_CONTENT_LENGTH:J = -0x1L

.field public static final NO_TRANSFER_ENCODING:J = 0x0L

.field public static final PRAGMA:Ljava/lang/String; = "pragma"

.field public static final PROXY_AUTHENTICATE:Ljava/lang/String; = "proxy-authenticate"

.field public static final PROXY_CONNECTION:Ljava/lang/String; = "proxy-connection"

.field public static final REFRESH:Ljava/lang/String; = "refresh"

.field public static final SET_COOKIE:Ljava/lang/String; = "set-cookie"

.field public static final TRANSFER_ENCODING:Ljava/lang/String; = "transfer-encoding"

.field public static final WWW_AUTHENTICATE:Ljava/lang/String; = "www-authenticate"

.field public static final X_PERMITTED_CROSS_DOMAIN_POLICIES:Ljava/lang/String; = "x-permitted-cross-domain-policies"

.field private static final sHeaderNames:[Ljava/lang/String;


# instance fields
.field private connectionType:I

.field private contentLength:J

.field private cookies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraHeaderNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraHeaderValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaders:[Ljava/lang/String;

.field private transferEncoding:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 128
    const/16 v0, 0x13

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    const/4 v1, 0x0

    #@5
    const-string/jumbo v2, "transfer-encoding"

    #@8
    aput-object v2, v0, v1

    #@a
    const/4 v1, 0x1

    #@b
    const-string v2, "content-length"

    #@d
    aput-object v2, v0, v1

    #@f
    const/4 v1, 0x2

    #@10
    const-string v2, "content-type"

    #@12
    aput-object v2, v0, v1

    #@14
    const/4 v1, 0x3

    #@15
    const-string v2, "content-encoding"

    #@17
    aput-object v2, v0, v1

    #@19
    const/4 v1, 0x4

    #@1a
    const-string v2, "connection"

    #@1c
    aput-object v2, v0, v1

    #@1e
    const/4 v1, 0x5

    #@1f
    const-string/jumbo v2, "location"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x6

    #@25
    const-string/jumbo v2, "proxy-connection"

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x7

    #@2b
    const-string/jumbo v2, "www-authenticate"

    #@2e
    aput-object v2, v0, v1

    #@30
    const/16 v1, 0x8

    #@32
    const-string/jumbo v2, "proxy-authenticate"

    #@35
    aput-object v2, v0, v1

    #@37
    const/16 v1, 0x9

    #@39
    const-string v2, "content-disposition"

    #@3b
    aput-object v2, v0, v1

    #@3d
    const/16 v1, 0xa

    #@3f
    const-string v2, "accept-ranges"

    #@41
    aput-object v2, v0, v1

    #@43
    const/16 v1, 0xb

    #@45
    const-string v2, "expires"

    #@47
    aput-object v2, v0, v1

    #@49
    const/16 v1, 0xc

    #@4b
    const-string v2, "cache-control"

    #@4d
    aput-object v2, v0, v1

    #@4f
    const/16 v1, 0xd

    #@51
    const-string/jumbo v2, "last-modified"

    #@54
    aput-object v2, v0, v1

    #@56
    const/16 v1, 0xe

    #@58
    const-string v2, "etag"

    #@5a
    aput-object v2, v0, v1

    #@5c
    const/16 v1, 0xf

    #@5e
    const-string/jumbo v2, "set-cookie"

    #@61
    aput-object v2, v0, v1

    #@63
    const/16 v1, 0x10

    #@65
    const-string/jumbo v2, "pragma"

    #@68
    aput-object v2, v0, v1

    #@6a
    const/16 v1, 0x11

    #@6c
    const-string/jumbo v2, "refresh"

    #@6f
    aput-object v2, v0, v1

    #@71
    const/16 v1, 0x12

    #@73
    const-string/jumbo v2, "x-permitted-cross-domain-policies"

    #@76
    aput-object v2, v0, v1

    #@78
    sput-object v0, Landroid/net/http/Headers;->sHeaderNames:[Ljava/lang/String;

    #@7a
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    .line 154
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 125
    new-instance v0, Ljava/util/ArrayList;

    #@6
    const/4 v1, 0x2

    #@7
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/net/http/Headers;->cookies:Ljava/util/ArrayList;

    #@c
    .line 127
    const/16 v0, 0x13

    #@e
    new-array v0, v0, [Ljava/lang/String;

    #@10
    iput-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@12
    .line 151
    new-instance v0, Ljava/util/ArrayList;

    #@14
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@17
    iput-object v0, p0, Landroid/net/http/Headers;->mExtraHeaderNames:Ljava/util/ArrayList;

    #@19
    .line 152
    new-instance v0, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@1e
    iput-object v0, p0, Landroid/net/http/Headers;->mExtraHeaderValues:Ljava/util/ArrayList;

    #@20
    .line 155
    const-wide/16 v0, 0x0

    #@22
    iput-wide v0, p0, Landroid/net/http/Headers;->transferEncoding:J

    #@24
    .line 156
    const-wide/16 v0, -0x1

    #@26
    iput-wide v0, p0, Landroid/net/http/Headers;->contentLength:J

    #@28
    .line 157
    const/4 v0, 0x0

    #@29
    iput v0, p0, Landroid/net/http/Headers;->connectionType:I

    #@2b
    .line 158
    return-void
.end method

.method private setConnectionType(Lorg/apache/http/util/CharArrayBuffer;I)V
    .registers 4
    .parameter "buffer"
    .parameter "pos"

    #@0
    .prologue
    .line 462
    const-string v0, "Close"

    #@2
    invoke-static {p1, p2, v0}, Landroid/net/http/CharArrayBuffers;->containsIgnoreCaseTrimmed(Lorg/apache/http/util/CharArrayBuffer;ILjava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_c

    #@8
    .line 464
    const/4 v0, 0x1

    #@9
    iput v0, p0, Landroid/net/http/Headers;->connectionType:I

    #@b
    .line 469
    :cond_b
    :goto_b
    return-void

    #@c
    .line 465
    :cond_c
    const-string v0, "Keep-Alive"

    #@e
    invoke-static {p1, p2, v0}, Landroid/net/http/CharArrayBuffers;->containsIgnoreCaseTrimmed(Lorg/apache/http/util/CharArrayBuffer;ILjava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_b

    #@14
    .line 467
    const/4 v0, 0x2

    #@15
    iput v0, p0, Landroid/net/http/Headers;->connectionType:I

    #@17
    goto :goto_b
.end method


# virtual methods
.method public getAcceptRanges()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 348
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xa

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getCacheControl()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xc

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getConnectionType()I
    .registers 2

    #@0
    .prologue
    .line 320
    iget v0, p0, Landroid/net/http/Headers;->connectionType:I

    #@2
    return v0
.end method

.method public getContentDisposition()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 344
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x9

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getContentEncoding()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x3

    #@3
    aget-object v0, v0, v1

    #@5
    return-object v0
.end method

.method public getContentLength()J
    .registers 3

    #@0
    .prologue
    .line 316
    iget-wide v0, p0, Landroid/net/http/Headers;->contentLength:J

    #@2
    return-wide v0
.end method

.method public getContentType()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 324
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x2

    #@3
    aget-object v0, v0, v1

    #@5
    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xe

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getExpires()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 352
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xb

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getHeaders(Landroid/net/http/Headers$HeaderCallback;)V
    .registers 7
    .parameter "hcb"

    #@0
    .prologue
    .line 443
    const/4 v2, 0x0

    #@1
    .local v2, i:I
    :goto_1
    const/16 v3, 0x13

    #@3
    if-ge v2, v3, :cond_15

    #@5
    .line 444
    iget-object v3, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@7
    aget-object v1, v3, v2

    #@9
    .line 445
    .local v1, h:Ljava/lang/String;
    if-eqz v1, :cond_12

    #@b
    .line 446
    sget-object v3, Landroid/net/http/Headers;->sHeaderNames:[Ljava/lang/String;

    #@d
    aget-object v3, v3, v2

    #@f
    invoke-interface {p1, v3, v1}, Landroid/net/http/Headers$HeaderCallback;->header(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 443
    :cond_12
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_1

    #@15
    .line 449
    .end local v1           #h:Ljava/lang/String;
    :cond_15
    iget-object v3, p0, Landroid/net/http/Headers;->mExtraHeaderNames:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v0

    #@1b
    .line 450
    .local v0, extraLen:I
    const/4 v2, 0x0

    #@1c
    :goto_1c
    if-ge v2, v0, :cond_34

    #@1e
    .line 455
    iget-object v3, p0, Landroid/net/http/Headers;->mExtraHeaderNames:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v3

    #@24
    check-cast v3, Ljava/lang/String;

    #@26
    iget-object v4, p0, Landroid/net/http/Headers;->mExtraHeaderValues:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v4

    #@2c
    check-cast v4, Ljava/lang/String;

    #@2e
    invoke-interface {p1, v3, v4}, Landroid/net/http/Headers$HeaderCallback;->header(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 450
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_1c

    #@34
    .line 459
    :cond_34
    return-void
.end method

.method public getLastModified()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 360
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xd

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 332
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x5

    #@3
    aget-object v0, v0, v1

    #@5
    return-object v0
.end method

.method public getPragma()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x10

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getProxyAuthenticate()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 340
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x8

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getRefresh()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 376
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x11

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public getSetCookie()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 368
    iget-object v0, p0, Landroid/net/http/Headers;->cookies:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getTransferEncoding()J
    .registers 3

    #@0
    .prologue
    .line 312
    iget-wide v0, p0, Landroid/net/http/Headers;->transferEncoding:J

    #@2
    return-wide v0
.end method

.method public getWwwAuthenticate()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 336
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x7

    #@3
    aget-object v0, v0, v1

    #@5
    return-object v0
.end method

.method public getXPermittedCrossDomainPolicies()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 380
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x12

    #@4
    aget-object v0, v0, v1

    #@6
    return-object v0
.end method

.method public parseHeader(Lorg/apache/http/util/CharArrayBuffer;)V
    .registers 13
    .parameter "buffer"

    #@0
    .prologue
    const-wide/16 v9, -0x1

    #@2
    const/4 v6, 0x0

    #@3
    const/16 v8, 0xc

    #@5
    .line 161
    const/16 v5, 0x3a

    #@7
    invoke-static {p1, v5}, Landroid/net/http/CharArrayBuffers;->setLowercaseIndexOf(Lorg/apache/http/util/CharArrayBuffer;I)I

    #@a
    move-result v3

    #@b
    .line 162
    .local v3, pos:I
    const/4 v5, -0x1

    #@c
    if-ne v3, v5, :cond_f

    #@e
    .line 309
    :cond_e
    :goto_e
    return-void

    #@f
    .line 165
    :cond_f
    invoke-virtual {p1, v6, v3}, Lorg/apache/http/util/CharArrayBuffer;->substringTrimmed(II)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 166
    .local v2, name:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_e

    #@19
    .line 169
    add-int/lit8 v3, v3, 0x1

    #@1b
    .line 171
    invoke-virtual {p1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@1e
    move-result v5

    #@1f
    invoke-virtual {p1, v3, v5}, Lorg/apache/http/util/CharArrayBuffer;->substringTrimmed(II)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    .line 176
    .local v4, val:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    #@26
    move-result v5

    #@27
    sparse-switch v5, :sswitch_data_1d8

    #@2a
    .line 306
    iget-object v5, p0, Landroid/net/http/Headers;->mExtraHeaderNames:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 307
    iget-object v5, p0, Landroid/net/http/Headers;->mExtraHeaderValues:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_e

    #@35
    .line 178
    :sswitch_35
    const-string/jumbo v5, "transfer-encoding"

    #@38
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_e

    #@3e
    .line 179
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@40
    aput-object v4, v5, v6

    #@42
    .line 180
    sget-object v5, Lorg/apache/http/message/BasicHeaderValueParser;->DEFAULT:Lorg/apache/http/message/BasicHeaderValueParser;

    #@44
    new-instance v6, Lorg/apache/http/message/ParserCursor;

    #@46
    invoke-virtual {p1}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@49
    move-result v7

    #@4a
    invoke-direct {v6, v3, v7}, Lorg/apache/http/message/ParserCursor;-><init>(II)V

    #@4d
    invoke-virtual {v5, p1, v6}, Lorg/apache/http/message/BasicHeaderValueParser;->parseElements(Lorg/apache/http/util/CharArrayBuffer;Lorg/apache/http/message/ParserCursor;)[Lorg/apache/http/HeaderElement;

    #@50
    move-result-object v0

    #@51
    .line 185
    .local v0, encodings:[Lorg/apache/http/HeaderElement;
    array-length v1, v0

    #@52
    .line 186
    .local v1, len:I
    const-string v5, "identity"

    #@54
    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@57
    move-result v5

    #@58
    if-eqz v5, :cond_5d

    #@5a
    .line 187
    iput-wide v9, p0, Landroid/net/http/Headers;->transferEncoding:J

    #@5c
    goto :goto_e

    #@5d
    .line 188
    :cond_5d
    if-lez v1, :cond_74

    #@5f
    const-string v5, "chunked"

    #@61
    add-int/lit8 v6, v1, -0x1

    #@63
    aget-object v6, v0, v6

    #@65
    invoke-interface {v6}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@6c
    move-result v5

    #@6d
    if-eqz v5, :cond_74

    #@6f
    .line 191
    const-wide/16 v5, -0x2

    #@71
    iput-wide v5, p0, Landroid/net/http/Headers;->transferEncoding:J

    #@73
    goto :goto_e

    #@74
    .line 193
    :cond_74
    iput-wide v9, p0, Landroid/net/http/Headers;->transferEncoding:J

    #@76
    goto :goto_e

    #@77
    .line 198
    .end local v0           #encodings:[Lorg/apache/http/HeaderElement;
    .end local v1           #len:I
    :sswitch_77
    const-string v5, "content-length"

    #@79
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v5

    #@7d
    if-eqz v5, :cond_e

    #@7f
    .line 199
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@81
    const/4 v6, 0x1

    #@82
    aput-object v4, v5, v6

    #@84
    .line 201
    :try_start_84
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@87
    move-result-wide v5

    #@88
    iput-wide v5, p0, Landroid/net/http/Headers;->contentLength:J
    :try_end_8a
    .catch Ljava/lang/NumberFormatException; {:try_start_84 .. :try_end_8a} :catch_8b

    #@8a
    goto :goto_e

    #@8b
    .line 202
    :catch_8b
    move-exception v5

    #@8c
    goto :goto_e

    #@8d
    .line 211
    :sswitch_8d
    const-string v5, "content-type"

    #@8f
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v5

    #@93
    if-eqz v5, :cond_e

    #@95
    .line 212
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@97
    const/4 v6, 0x2

    #@98
    aput-object v4, v5, v6

    #@9a
    goto/16 :goto_e

    #@9c
    .line 216
    :sswitch_9c
    const-string v5, "content-encoding"

    #@9e
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v5

    #@a2
    if-eqz v5, :cond_e

    #@a4
    .line 217
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@a6
    const/4 v6, 0x3

    #@a7
    aput-object v4, v5, v6

    #@a9
    goto/16 :goto_e

    #@ab
    .line 221
    :sswitch_ab
    const-string v5, "connection"

    #@ad
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v5

    #@b1
    if-eqz v5, :cond_e

    #@b3
    .line 222
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@b5
    const/4 v6, 0x4

    #@b6
    aput-object v4, v5, v6

    #@b8
    .line 223
    invoke-direct {p0, p1, v3}, Landroid/net/http/Headers;->setConnectionType(Lorg/apache/http/util/CharArrayBuffer;I)V

    #@bb
    goto/16 :goto_e

    #@bd
    .line 227
    :sswitch_bd
    const-string/jumbo v5, "location"

    #@c0
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c3
    move-result v5

    #@c4
    if-eqz v5, :cond_e

    #@c6
    .line 228
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@c8
    const/4 v6, 0x5

    #@c9
    aput-object v4, v5, v6

    #@cb
    goto/16 :goto_e

    #@cd
    .line 232
    :sswitch_cd
    const-string/jumbo v5, "proxy-connection"

    #@d0
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d3
    move-result v5

    #@d4
    if-eqz v5, :cond_e

    #@d6
    .line 233
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@d8
    const/4 v6, 0x6

    #@d9
    aput-object v4, v5, v6

    #@db
    .line 234
    invoke-direct {p0, p1, v3}, Landroid/net/http/Headers;->setConnectionType(Lorg/apache/http/util/CharArrayBuffer;I)V

    #@de
    goto/16 :goto_e

    #@e0
    .line 238
    :sswitch_e0
    const-string/jumbo v5, "www-authenticate"

    #@e3
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e6
    move-result v5

    #@e7
    if-eqz v5, :cond_e

    #@e9
    .line 239
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@eb
    const/4 v6, 0x7

    #@ec
    aput-object v4, v5, v6

    #@ee
    goto/16 :goto_e

    #@f0
    .line 243
    :sswitch_f0
    const-string/jumbo v5, "proxy-authenticate"

    #@f3
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f6
    move-result v5

    #@f7
    if-eqz v5, :cond_e

    #@f9
    .line 244
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@fb
    const/16 v6, 0x8

    #@fd
    aput-object v4, v5, v6

    #@ff
    goto/16 :goto_e

    #@101
    .line 248
    :sswitch_101
    const-string v5, "content-disposition"

    #@103
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@106
    move-result v5

    #@107
    if-eqz v5, :cond_e

    #@109
    .line 249
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@10b
    const/16 v6, 0x9

    #@10d
    aput-object v4, v5, v6

    #@10f
    goto/16 :goto_e

    #@111
    .line 253
    :sswitch_111
    const-string v5, "accept-ranges"

    #@113
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@116
    move-result v5

    #@117
    if-eqz v5, :cond_e

    #@119
    .line 254
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@11b
    const/16 v6, 0xa

    #@11d
    aput-object v4, v5, v6

    #@11f
    goto/16 :goto_e

    #@121
    .line 258
    :sswitch_121
    const-string v5, "expires"

    #@123
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@126
    move-result v5

    #@127
    if-eqz v5, :cond_e

    #@129
    .line 259
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@12b
    const/16 v6, 0xb

    #@12d
    aput-object v4, v5, v6

    #@12f
    goto/16 :goto_e

    #@131
    .line 263
    :sswitch_131
    const-string v5, "cache-control"

    #@133
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@136
    move-result v5

    #@137
    if-eqz v5, :cond_e

    #@139
    .line 266
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@13b
    aget-object v5, v5, v8

    #@13d
    if-eqz v5, :cond_168

    #@13f
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@141
    aget-object v5, v5, v8

    #@143
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@146
    move-result v5

    #@147
    if-lez v5, :cond_168

    #@149
    .line 268
    new-instance v5, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    iget-object v6, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@150
    aget-object v7, v6, v8

    #@152
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v5

    #@156
    const/16 v7, 0x2c

    #@158
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v5

    #@15c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v5

    #@160
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@163
    move-result-object v5

    #@164
    aput-object v5, v6, v8

    #@166
    goto/16 :goto_e

    #@168
    .line 270
    :cond_168
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@16a
    aput-object v4, v5, v8

    #@16c
    goto/16 :goto_e

    #@16e
    .line 275
    :sswitch_16e
    const-string/jumbo v5, "last-modified"

    #@171
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@174
    move-result v5

    #@175
    if-eqz v5, :cond_e

    #@177
    .line 276
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@179
    const/16 v6, 0xd

    #@17b
    aput-object v4, v5, v6

    #@17d
    goto/16 :goto_e

    #@17f
    .line 280
    :sswitch_17f
    const-string v5, "etag"

    #@181
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@184
    move-result v5

    #@185
    if-eqz v5, :cond_e

    #@187
    .line 281
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@189
    const/16 v6, 0xe

    #@18b
    aput-object v4, v5, v6

    #@18d
    goto/16 :goto_e

    #@18f
    .line 285
    :sswitch_18f
    const-string/jumbo v5, "set-cookie"

    #@192
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@195
    move-result v5

    #@196
    if-eqz v5, :cond_e

    #@198
    .line 286
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@19a
    const/16 v6, 0xf

    #@19c
    aput-object v4, v5, v6

    #@19e
    .line 287
    iget-object v5, p0, Landroid/net/http/Headers;->cookies:Ljava/util/ArrayList;

    #@1a0
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a3
    goto/16 :goto_e

    #@1a5
    .line 291
    :sswitch_1a5
    const-string/jumbo v5, "pragma"

    #@1a8
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ab
    move-result v5

    #@1ac
    if-eqz v5, :cond_e

    #@1ae
    .line 292
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@1b0
    const/16 v6, 0x10

    #@1b2
    aput-object v4, v5, v6

    #@1b4
    goto/16 :goto_e

    #@1b6
    .line 296
    :sswitch_1b6
    const-string/jumbo v5, "refresh"

    #@1b9
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1bc
    move-result v5

    #@1bd
    if-eqz v5, :cond_e

    #@1bf
    .line 297
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@1c1
    const/16 v6, 0x11

    #@1c3
    aput-object v4, v5, v6

    #@1c5
    goto/16 :goto_e

    #@1c7
    .line 301
    :sswitch_1c7
    const-string/jumbo v5, "x-permitted-cross-domain-policies"

    #@1ca
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cd
    move-result v5

    #@1ce
    if-eqz v5, :cond_e

    #@1d0
    .line 302
    iget-object v5, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@1d2
    const/16 v6, 0x12

    #@1d4
    aput-object v4, v5, v6

    #@1d6
    goto/16 :goto_e

    #@1d8
    .line 176
    :sswitch_data_1d8
    .sparse-switch
        -0x5034229e -> :sswitch_1c7
        -0x4e0958cc -> :sswitch_121
        -0x4b88f79d -> :sswitch_101
        -0x4384d946 -> :sswitch_77
        -0x3a6d1ac4 -> :sswitch_1a5
        -0x2e3b8122 -> :sswitch_ab
        -0x11fc9c2c -> :sswitch_f0
        -0xe7c74b5 -> :sswitch_e0
        -0xc71a9ee -> :sswitch_131
        0x2fa915 -> :sswitch_17f
        0x8f17c20 -> :sswitch_16e
        0x110aef9d -> :sswitch_cd
        0x2ed4600e -> :sswitch_8d
        0x40b292db -> :sswitch_1b6
        0x49be662f -> :sswitch_18f
        0x4bf6b0f5 -> :sswitch_35
        0x53476b3b -> :sswitch_111
        0x714f9fb5 -> :sswitch_bd
        0x7ce07427 -> :sswitch_9c
    .end sparse-switch
.end method

.method public setAcceptRanges(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 412
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xa

    #@4
    aput-object p1, v0, v1

    #@6
    .line 413
    return-void
.end method

.method public setCacheControl(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 420
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xc

    #@4
    aput-object p1, v0, v1

    #@6
    .line 421
    return-void
.end method

.method public setContentDisposition(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x9

    #@4
    aput-object p1, v0, v1

    #@6
    .line 409
    return-void
.end method

.method public setContentEncoding(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x3

    #@3
    aput-object p1, v0, v1

    #@5
    .line 393
    return-void
.end method

.method public setContentLength(J)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 384
    iput-wide p1, p0, Landroid/net/http/Headers;->contentLength:J

    #@2
    .line 385
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 388
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x2

    #@3
    aput-object p1, v0, v1

    #@5
    .line 389
    return-void
.end method

.method public setEtag(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xe

    #@4
    aput-object p1, v0, v1

    #@6
    .line 429
    return-void
.end method

.method public setExpires(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 416
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xb

    #@4
    aput-object p1, v0, v1

    #@6
    .line 417
    return-void
.end method

.method public setLastModified(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 424
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0xd

    #@4
    aput-object p1, v0, v1

    #@6
    .line 425
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 396
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x5

    #@3
    aput-object p1, v0, v1

    #@5
    .line 397
    return-void
.end method

.method public setProxyAuthenticate(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 404
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x8

    #@4
    aput-object p1, v0, v1

    #@6
    .line 405
    return-void
.end method

.method public setWwwAuthenticate(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 400
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/4 v1, 0x7

    #@3
    aput-object p1, v0, v1

    #@5
    .line 401
    return-void
.end method

.method public setXPermittedCrossDomainPolicies(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 432
    iget-object v0, p0, Landroid/net/http/Headers;->mHeaders:[Ljava/lang/String;

    #@2
    const/16 v1, 0x12

    #@4
    aput-object p1, v0, v1

    #@6
    .line 433
    return-void
.end method
