.class public final Landroid/net/http/AndroidHttpClient;
.super Ljava/lang/Object;
.source "AndroidHttpClient.java"

# interfaces
.implements Lorg/apache/http/client/HttpClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/http/AndroidHttpClient$CurlLogger;,
        Landroid/net/http/AndroidHttpClient$LoggingConfiguration;
    }
.end annotation


# static fields
.field public static DEFAULT_SYNC_MIN_GZIP_BYTES:J = 0x0L

.field private static final SOCKET_OPERATION_TIMEOUT:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "AndroidHttpClient"

.field private static final sThreadCheckInterceptor:Lorg/apache/http/HttpRequestInterceptor;

.field private static textContentTypes:[Ljava/lang/String;


# instance fields
.field private volatile curlConfiguration:Landroid/net/http/AndroidHttpClient$LoggingConfiguration;

.field private final delegate:Lorg/apache/http/client/HttpClient;

.field private mLeakedException:Ljava/lang/RuntimeException;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 81
    const-wide/16 v0, 0x100

    #@2
    sput-wide v0, Landroid/net/http/AndroidHttpClient;->DEFAULT_SYNC_MIN_GZIP_BYTES:J

    #@4
    .line 88
    const/4 v0, 0x3

    #@5
    new-array v0, v0, [Ljava/lang/String;

    #@7
    const/4 v1, 0x0

    #@8
    const-string/jumbo v2, "text/"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x1

    #@e
    const-string v2, "application/xml"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x2

    #@13
    const-string v2, "application/json"

    #@15
    aput-object v2, v0, v1

    #@17
    sput-object v0, Landroid/net/http/AndroidHttpClient;->textContentTypes:[Ljava/lang/String;

    #@19
    .line 95
    new-instance v0, Landroid/net/http/AndroidHttpClient$1;

    #@1b
    invoke-direct {v0}, Landroid/net/http/AndroidHttpClient$1;-><init>()V

    #@1e
    sput-object v0, Landroid/net/http/AndroidHttpClient;->sThreadCheckInterceptor:Lorg/apache/http/HttpRequestInterceptor;

    #@20
    return-void
.end method

.method private constructor <init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .registers 5
    .parameter "ccm"
    .parameter "params"

    #@0
    .prologue
    .line 161
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    #@5
    const-string v1, "AndroidHttpClient created and never closed"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@a
    iput-object v0, p0, Landroid/net/http/AndroidHttpClient;->mLeakedException:Ljava/lang/RuntimeException;

    #@c
    .line 162
    new-instance v0, Landroid/net/http/AndroidHttpClient$2;

    #@e
    invoke-direct {v0, p0, p1, p2}, Landroid/net/http/AndroidHttpClient$2;-><init>(Landroid/net/http/AndroidHttpClient;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    #@11
    iput-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@13
    .line 190
    return-void
.end method

.method static synthetic access$000()Lorg/apache/http/HttpRequestInterceptor;
    .registers 1

    #@0
    .prologue
    .line 78
    sget-object v0, Landroid/net/http/AndroidHttpClient;->sThreadCheckInterceptor:Lorg/apache/http/HttpRequestInterceptor;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient$LoggingConfiguration;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->curlConfiguration:Landroid/net/http/AndroidHttpClient$LoggingConfiguration;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lorg/apache/http/client/methods/HttpUriRequest;Z)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 78
    invoke-static {p0, p1}, Landroid/net/http/AndroidHttpClient;->toCurl(Lorg/apache/http/client/methods/HttpUriRequest;Z)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;
    .registers 9
    .parameter "data"
    .parameter "resolver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 304
    array-length v3, p0

    #@1
    int-to-long v3, v3

    #@2
    invoke-static {p1}, Landroid/net/http/AndroidHttpClient;->getMinGzipSize(Landroid/content/ContentResolver;)J

    #@5
    move-result-wide v5

    #@6
    cmp-long v3, v3, v5

    #@8
    if-gez v3, :cond_10

    #@a
    .line 305
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    #@c
    invoke-direct {v1, p0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    #@f
    .line 314
    .local v1, entity:Lorg/apache/http/entity/AbstractHttpEntity;
    :goto_f
    return-object v1

    #@10
    .line 307
    .end local v1           #entity:Lorg/apache/http/entity/AbstractHttpEntity;
    :cond_10
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@12
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@15
    .line 308
    .local v0, arr:Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    #@17
    invoke-direct {v2, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@1a
    .line 309
    .local v2, zipper:Ljava/io/OutputStream;
    invoke-virtual {v2, p0}, Ljava/io/OutputStream;->write([B)V

    #@1d
    .line 310
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    #@20
    .line 311
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    #@22
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@25
    move-result-object v3

    #@26
    invoke-direct {v1, v3}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    #@29
    .line 312
    .restart local v1       #entity:Lorg/apache/http/entity/AbstractHttpEntity;
    const-string v3, "gzip"

    #@2b
    invoke-virtual {v1, v3}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentEncoding(Ljava/lang/String;)V

    #@2e
    goto :goto_f
.end method

.method public static getMinGzipSize(Landroid/content/ContentResolver;)J
    .registers 3
    .parameter "resolver"

    #@0
    .prologue
    .line 322
    sget-wide v0, Landroid/net/http/AndroidHttpClient;->DEFAULT_SYNC_MIN_GZIP_BYTES:J

    #@2
    return-wide v0
.end method

.method public static getUngzippedContent(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    .registers 6
    .parameter "entity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    #@3
    move-result-object v2

    #@4
    .line 222
    .local v2, responseStream:Ljava/io/InputStream;
    if-nez v2, :cond_8

    #@6
    move-object v3, v2

    #@7
    .line 229
    .end local v2           #responseStream:Ljava/io/InputStream;
    .local v3, responseStream:Ljava/io/InputStream;
    :goto_7
    return-object v3

    #@8
    .line 223
    .end local v3           #responseStream:Ljava/io/InputStream;
    .restart local v2       #responseStream:Ljava/io/InputStream;
    :cond_8
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    #@b
    move-result-object v1

    #@c
    .line 224
    .local v1, header:Lorg/apache/http/Header;
    if-nez v1, :cond_10

    #@e
    move-object v3, v2

    #@f
    .end local v2           #responseStream:Ljava/io/InputStream;
    .restart local v3       #responseStream:Ljava/io/InputStream;
    goto :goto_7

    #@10
    .line 225
    .end local v3           #responseStream:Ljava/io/InputStream;
    .restart local v2       #responseStream:Ljava/io/InputStream;
    :cond_10
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 226
    .local v0, contentEncoding:Ljava/lang/String;
    if-nez v0, :cond_18

    #@16
    move-object v3, v2

    #@17
    .end local v2           #responseStream:Ljava/io/InputStream;
    .restart local v3       #responseStream:Ljava/io/InputStream;
    goto :goto_7

    #@18
    .line 227
    .end local v3           #responseStream:Ljava/io/InputStream;
    .restart local v2       #responseStream:Ljava/io/InputStream;
    :cond_18
    const-string v4, "gzip"

    #@1a
    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_26

    #@20
    new-instance v3, Ljava/util/zip/GZIPInputStream;

    #@22
    invoke-direct {v3, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    #@25
    .end local v2           #responseStream:Ljava/io/InputStream;
    .restart local v3       #responseStream:Ljava/io/InputStream;
    move-object v2, v3

    #@26
    .end local v3           #responseStream:Ljava/io/InputStream;
    .restart local v2       #responseStream:Ljava/io/InputStream;
    :cond_26
    move-object v3, v2

    #@27
    .line 229
    .end local v2           #responseStream:Ljava/io/InputStream;
    .restart local v3       #responseStream:Ljava/io/InputStream;
    goto :goto_7
.end method

.method private static isBinaryContent(Lorg/apache/http/client/methods/HttpUriRequest;)Z
    .registers 13
    .parameter "request"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 465
    const-string v10, "content-encoding"

    #@3
    invoke-interface {p0, v10}, Lorg/apache/http/client/methods/HttpUriRequest;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    #@6
    move-result-object v4

    #@7
    .line 466
    .local v4, headers:[Lorg/apache/http/Header;
    if-eqz v4, :cond_20

    #@9
    .line 467
    move-object v0, v4

    #@a
    .local v0, arr$:[Lorg/apache/http/Header;
    array-length v7, v0

    #@b
    .local v7, len$:I
    const/4 v5, 0x0

    #@c
    .local v5, i$:I
    :goto_c
    if-ge v5, v7, :cond_20

    #@e
    aget-object v3, v0, v5

    #@10
    .line 468
    .local v3, header:Lorg/apache/http/Header;
    const-string v10, "gzip"

    #@12
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    #@15
    move-result-object v11

    #@16
    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@19
    move-result v10

    #@1a
    if-eqz v10, :cond_1d

    #@1c
    .line 484
    .end local v0           #arr$:[Lorg/apache/http/Header;
    .end local v3           #header:Lorg/apache/http/Header;
    .end local v5           #i$:I
    .end local v7           #len$:I
    :cond_1c
    :goto_1c
    return v9

    #@1d
    .line 467
    .restart local v0       #arr$:[Lorg/apache/http/Header;
    .restart local v3       #header:Lorg/apache/http/Header;
    .restart local v5       #i$:I
    .restart local v7       #len$:I
    :cond_1d
    add-int/lit8 v5, v5, 0x1

    #@1f
    goto :goto_c

    #@20
    .line 474
    .end local v0           #arr$:[Lorg/apache/http/Header;
    .end local v3           #header:Lorg/apache/http/Header;
    .end local v5           #i$:I
    .end local v7           #len$:I
    :cond_20
    const-string v10, "content-type"

    #@22
    invoke-interface {p0, v10}, Lorg/apache/http/client/methods/HttpUriRequest;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    #@25
    move-result-object v4

    #@26
    .line 475
    if-eqz v4, :cond_1c

    #@28
    .line 476
    move-object v0, v4

    #@29
    .restart local v0       #arr$:[Lorg/apache/http/Header;
    array-length v7, v0

    #@2a
    .restart local v7       #len$:I
    const/4 v5, 0x0

    #@2b
    .restart local v5       #i$:I
    move v6, v5

    #@2c
    .end local v0           #arr$:[Lorg/apache/http/Header;
    .end local v5           #i$:I
    .end local v7           #len$:I
    .local v6, i$:I
    :goto_2c
    if-ge v6, v7, :cond_1c

    #@2e
    aget-object v3, v0, v6

    #@30
    .line 477
    .restart local v3       #header:Lorg/apache/http/Header;
    sget-object v1, Landroid/net/http/AndroidHttpClient;->textContentTypes:[Ljava/lang/String;

    #@32
    .local v1, arr$:[Ljava/lang/String;
    array-length v8, v1

    #@33
    .local v8, len$:I
    const/4 v5, 0x0

    #@34
    .end local v6           #i$:I
    .restart local v5       #i$:I
    :goto_34
    if-ge v5, v8, :cond_47

    #@36
    aget-object v2, v1, v5

    #@38
    .line 478
    .local v2, contentType:Ljava/lang/String;
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    #@3b
    move-result-object v10

    #@3c
    invoke-virtual {v10, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3f
    move-result v10

    #@40
    if-eqz v10, :cond_44

    #@42
    .line 479
    const/4 v9, 0x0

    #@43
    goto :goto_1c

    #@44
    .line 477
    :cond_44
    add-int/lit8 v5, v5, 0x1

    #@46
    goto :goto_34

    #@47
    .line 476
    .end local v2           #contentType:Ljava/lang/String;
    :cond_47
    add-int/lit8 v5, v6, 0x1

    #@49
    move v6, v5

    #@4a
    .end local v5           #i$:I
    .restart local v6       #i$:I
    goto :goto_2c
.end method

.method public static modifyRequestToAcceptGzipResponse(Lorg/apache/http/HttpRequest;)V
    .registers 3
    .parameter "request"

    #@0
    .prologue
    .line 208
    const-string v0, "Accept-Encoding"

    #@2
    const-string v1, "gzip"

    #@4
    invoke-interface {p0, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 209
    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;
    .registers 2
    .parameter "userAgent"

    #@0
    .prologue
    .line 153
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;
    .registers 11
    .parameter "userAgent"
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const v8, 0xea60

    #@4
    .line 113
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    #@6
    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    #@9
    .line 117
    .local v1, params:Lorg/apache/http/params/HttpParams;
    invoke-static {v1, v5}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    #@c
    .line 119
    invoke-static {v1, v8}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    #@f
    .line 120
    invoke-static {v1, v8}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    #@12
    .line 121
    const/16 v4, 0x2000

    #@14
    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    #@17
    .line 125
    invoke-static {v1, v5}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    #@1a
    .line 128
    if-nez p1, :cond_50

    #@1c
    const/4 v3, 0x0

    #@1d
    .line 131
    .local v3, sessionCache:Landroid/net/SSLSessionCache;
    :goto_1d
    invoke-static {v1, p0}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    #@20
    .line 132
    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    #@22
    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    #@25
    .line 133
    .local v2, schemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    #@27
    const-string v5, "http"

    #@29
    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    #@2c
    move-result-object v6

    #@2d
    const/16 v7, 0x50

    #@2f
    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    #@32
    invoke-virtual {v2, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    #@35
    .line 135
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    #@37
    const-string v5, "https"

    #@39
    invoke-static {v8, v3}, Landroid/net/SSLCertificateSocketFactory;->getHttpSocketFactory(ILandroid/net/SSLSessionCache;)Lorg/apache/http/conn/ssl/SSLSocketFactory;

    #@3c
    move-result-object v6

    #@3d
    const/16 v7, 0x1bb

    #@3f
    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    #@42
    invoke-virtual {v2, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    #@45
    .line 139
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    #@47
    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    #@4a
    .line 144
    .local v0, manager:Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v4, Landroid/net/http/AndroidHttpClient;

    #@4c
    invoke-direct {v4, v0, v1}, Landroid/net/http/AndroidHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    #@4f
    return-object v4

    #@50
    .line 128
    .end local v0           #manager:Lorg/apache/http/conn/ClientConnectionManager;
    .end local v2           #schemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v3           #sessionCache:Landroid/net/SSLSessionCache;
    :cond_50
    new-instance v3, Landroid/net/SSLSessionCache;

    #@52
    invoke-direct {v3, p1}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V

    #@55
    goto :goto_1d
.end method

.method public static parseDate(Ljava/lang/String;)J
    .registers 3
    .parameter "dateString"

    #@0
    .prologue
    .line 502
    invoke-static {p0}, Lcom/android/internal/http/HttpDateTime;->parse(Ljava/lang/String;)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method private static toCurl(Lorg/apache/http/client/methods/HttpUriRequest;Z)Ljava/lang/String;
    .registers 19
    .parameter "request"
    .parameter "logAuthToken"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 404
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 406
    .local v3, builder:Ljava/lang/StringBuilder;
    const-string v13, "curl "

    #@7
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 408
    invoke-interface/range {p0 .. p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    #@d
    move-result-object v1

    #@e
    .local v1, arr$:[Lorg/apache/http/Header;
    array-length v9, v1

    #@f
    .local v9, len$:I
    const/4 v8, 0x0

    #@10
    .local v8, i$:I
    :goto_10
    if-ge v8, v9, :cond_47

    #@12
    aget-object v7, v1, v8

    #@14
    .line 409
    .local v7, header:Lorg/apache/http/Header;
    if-nez p1, :cond_31

    #@16
    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    #@19
    move-result-object v13

    #@1a
    const-string v14, "Authorization"

    #@1c
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v13

    #@20
    if-nez v13, :cond_2e

    #@22
    invoke-interface {v7}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    #@25
    move-result-object v13

    #@26
    const-string v14, "Cookie"

    #@28
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v13

    #@2c
    if-eqz v13, :cond_31

    #@2e
    .line 408
    :cond_2e
    :goto_2e
    add-int/lit8 v8, v8, 0x1

    #@30
    goto :goto_10

    #@31
    .line 414
    :cond_31
    const-string v13, "--header \""

    #@33
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 415
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@39
    move-result-object v13

    #@3a
    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@3d
    move-result-object v13

    #@3e
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    .line 416
    const-string v13, "\" "

    #@43
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    goto :goto_2e

    #@47
    .line 419
    .end local v7           #header:Lorg/apache/http/Header;
    :cond_47
    invoke-interface/range {p0 .. p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    #@4a
    move-result-object v12

    #@4b
    .line 424
    .local v12, uri:Ljava/net/URI;
    move-object/from16 v0, p0

    #@4d
    instance-of v13, v0, Lorg/apache/http/impl/client/RequestWrapper;

    #@4f
    if-eqz v13, :cond_63

    #@51
    move-object/from16 v13, p0

    #@53
    .line 425
    check-cast v13, Lorg/apache/http/impl/client/RequestWrapper;

    #@55
    invoke-virtual {v13}, Lorg/apache/http/impl/client/RequestWrapper;->getOriginal()Lorg/apache/http/HttpRequest;

    #@58
    move-result-object v10

    #@59
    .line 426
    .local v10, original:Lorg/apache/http/HttpRequest;
    instance-of v13, v10, Lorg/apache/http/client/methods/HttpUriRequest;

    #@5b
    if-eqz v13, :cond_63

    #@5d
    .line 427
    check-cast v10, Lorg/apache/http/client/methods/HttpUriRequest;

    #@5f
    .end local v10           #original:Lorg/apache/http/HttpRequest;
    invoke-interface {v10}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    #@62
    move-result-object v12

    #@63
    .line 431
    :cond_63
    const-string v13, "\""

    #@65
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    .line 432
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    .line 433
    const-string v13, "\""

    #@6d
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    .line 435
    move-object/from16 v0, p0

    #@72
    instance-of v13, v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    #@74
    if-eqz v13, :cond_c9

    #@76
    move-object/from16 v5, p0

    #@78
    .line 436
    check-cast v5, Lorg/apache/http/HttpEntityEnclosingRequest;

    #@7a
    .line 438
    .local v5, entityRequest:Lorg/apache/http/HttpEntityEnclosingRequest;
    invoke-interface {v5}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    #@7d
    move-result-object v4

    #@7e
    .line 439
    .local v4, entity:Lorg/apache/http/HttpEntity;
    if-eqz v4, :cond_c9

    #@80
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    #@83
    move-result v13

    #@84
    if-eqz v13, :cond_c9

    #@86
    .line 440
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContentLength()J

    #@89
    move-result-wide v13

    #@8a
    const-wide/16 v15, 0x400

    #@8c
    cmp-long v13, v13, v15

    #@8e
    if-gez v13, :cond_e2

    #@90
    .line 441
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    #@92
    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@95
    .line 442
    .local v11, stream:Ljava/io/ByteArrayOutputStream;
    invoke-interface {v4, v11}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    #@98
    .line 444
    invoke-static/range {p0 .. p0}, Landroid/net/http/AndroidHttpClient;->isBinaryContent(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    #@9b
    move-result v13

    #@9c
    if-eqz v13, :cond_ce

    #@9e
    .line 445
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@a1
    move-result-object v13

    #@a2
    const/4 v14, 0x2

    #@a3
    invoke-static {v13, v14}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    #@a6
    move-result-object v2

    #@a7
    .line 446
    .local v2, base64:Ljava/lang/String;
    const/4 v13, 0x0

    #@a8
    new-instance v14, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v15, "echo \'"

    #@af
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v14

    #@b3
    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v14

    #@b7
    const-string v15, "\' | base64 -d > /tmp/$$.bin; "

    #@b9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v14

    #@bd
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v14

    #@c1
    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    .line 447
    const-string v13, " --data-binary @/tmp/$$.bin"

    #@c6
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    .line 460
    .end local v2           #base64:Ljava/lang/String;
    .end local v4           #entity:Lorg/apache/http/HttpEntity;
    .end local v5           #entityRequest:Lorg/apache/http/HttpEntityEnclosingRequest;
    .end local v11           #stream:Ljava/io/ByteArrayOutputStream;
    :cond_c9
    :goto_c9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v13

    #@cd
    return-object v13

    #@ce
    .line 449
    .restart local v4       #entity:Lorg/apache/http/HttpEntity;
    .restart local v5       #entityRequest:Lorg/apache/http/HttpEntityEnclosingRequest;
    .restart local v11       #stream:Ljava/io/ByteArrayOutputStream;
    :cond_ce
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    #@d1
    move-result-object v6

    #@d2
    .line 450
    .local v6, entityString:Ljava/lang/String;
    const-string v13, " --data-ascii \""

    #@d4
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v13

    #@d8
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v13

    #@dc
    const-string v14, "\""

    #@de
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    goto :goto_c9

    #@e2
    .line 455
    .end local v6           #entityString:Ljava/lang/String;
    .end local v11           #stream:Ljava/io/ByteArrayOutputStream;
    :cond_e2
    const-string v13, " [TOO MUCH DATA TO INCLUDE]"

    #@e4
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    goto :goto_c9
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->mLeakedException:Ljava/lang/RuntimeException;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 238
    invoke-virtual {p0}, Landroid/net/http/AndroidHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    #@b
    .line 239
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/net/http/AndroidHttpClient;->mLeakedException:Ljava/lang/RuntimeException;

    #@e
    .line 241
    :cond_e
    return-void
.end method

.method public disableCurlLogging()V
    .registers 2

    #@0
    .prologue
    .line 380
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/net/http/AndroidHttpClient;->curlConfiguration:Landroid/net/http/AndroidHttpClient$LoggingConfiguration;

    #@3
    .line 381
    return-void
.end method

.method public enableCurlLogging(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "level"

    #@0
    .prologue
    .line 365
    if-nez p1, :cond_b

    #@2
    .line 366
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "name"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 368
    :cond_b
    const/4 v0, 0x2

    #@c
    if-lt p2, v0, :cond_11

    #@e
    const/4 v0, 0x7

    #@f
    if-le p2, v0, :cond_19

    #@11
    .line 369
    :cond_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v1, "Level is out of range [2..7]"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 373
    :cond_19
    new-instance v0, Landroid/net/http/AndroidHttpClient$LoggingConfiguration;

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-direct {v0, p1, p2, v1}, Landroid/net/http/AndroidHttpClient$LoggingConfiguration;-><init>(Ljava/lang/String;ILandroid/net/http/AndroidHttpClient$1;)V

    #@1f
    iput-object v0, p0, Landroid/net/http/AndroidHttpClient;->curlConfiguration:Landroid/net/http/AndroidHttpClient$LoggingConfiguration;

    #@21
    .line 374
    return-void
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .registers 5
    .parameter "target"
    .parameter "request"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    .local p3, responseHandler:Lorg/apache/http/client/ResponseHandler;,"Lorg/apache/http/client/ResponseHandler<+TT;>;"
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .registers 6
    .parameter "target"
    .parameter "request"
    .parameter
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    #@0
    .prologue
    .line 291
    .local p3, responseHandler:Lorg/apache/http/client/ResponseHandler;,"Lorg/apache/http/client/ResponseHandler<+TT;>;"
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2, p3, p4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .registers 4
    .parameter "request"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    #@0
    .prologue
    .line 273
    .local p2, responseHandler:Lorg/apache/http/client/ResponseHandler;,"Lorg/apache/http/client/ResponseHandler<+TT;>;"
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .registers 5
    .parameter "request"
    .parameter
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    #@0
    .prologue
    .line 279
    .local p2, responseHandler:Lorg/apache/http/client/ResponseHandler;,"Lorg/apache/http/client/ResponseHandler<+TT;>;"
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .registers 4
    .parameter "target"
    .parameter "request"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .registers 5
    .parameter "target"
    .parameter "request"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .registers 3
    .parameter "request"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .registers 4
    .parameter "request"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0, p1, p2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 194
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@3
    .line 195
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->mLeakedException:Ljava/lang/RuntimeException;

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 196
    const-string v0, "AndroidHttpClient"

    #@9
    const-string v1, "Leak found"

    #@b
    iget-object v2, p0, Landroid/net/http/AndroidHttpClient;->mLeakedException:Ljava/lang/RuntimeException;

    #@d
    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    .line 197
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/net/http/AndroidHttpClient;->mLeakedException:Ljava/lang/RuntimeException;

    #@13
    .line 199
    :cond_13
    return-void
.end method

.method public getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    .registers 2

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getParams()Lorg/apache/http/params/HttpParams;
    .registers 2

    #@0
    .prologue
    .line 244
    iget-object v0, p0, Landroid/net/http/AndroidHttpClient;->delegate:Lorg/apache/http/client/HttpClient;

    #@2
    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
