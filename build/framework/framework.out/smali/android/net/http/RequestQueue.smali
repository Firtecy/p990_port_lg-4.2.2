.class public Landroid/net/http/RequestQueue;
.super Ljava/lang/Object;
.source "RequestQueue.java"

# interfaces
.implements Landroid/net/http/RequestFeeder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/http/RequestQueue$ConnectionManager;,
        Landroid/net/http/RequestQueue$SyncFeeder;,
        Landroid/net/http/RequestQueue$ActivePool;
    }
.end annotation


# static fields
.field private static final CONNECTION_COUNT:I = 0x4


# instance fields
.field private final mActivePool:Landroid/net/http/RequestQueue$ActivePool;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private final mPending:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/http/HttpHost;",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/net/http/Request;",
            ">;>;"
        }
    .end annotation
.end field

.field private mProxyChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mProxyHost:Lorg/apache/http/HttpHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 199
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, p1, v0}, Landroid/net/http/RequestQueue;-><init>(Landroid/content/Context;I)V

    #@4
    .line 200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "connectionCount"

    #@0
    .prologue
    .line 212
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@6
    .line 213
    iput-object p1, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@8
    .line 215
    new-instance v0, Ljava/util/LinkedHashMap;

    #@a
    const/16 v1, 0x20

    #@c
    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    #@f
    iput-object v0, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@11
    .line 217
    new-instance v0, Landroid/net/http/RequestQueue$ActivePool;

    #@13
    invoke-direct {v0, p0, p2}, Landroid/net/http/RequestQueue$ActivePool;-><init>(Landroid/net/http/RequestQueue;I)V

    #@16
    iput-object v0, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@18
    .line 218
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@1a
    invoke-virtual {v0}, Landroid/net/http/RequestQueue$ActivePool;->startup()V

    #@1d
    .line 220
    const-string v0, "connectivity"

    #@1f
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Landroid/net/ConnectivityManager;

    #@25
    iput-object v0, p0, Landroid/net/http/RequestQueue;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@27
    .line 222
    return-void
.end method

.method static synthetic access$000(Landroid/net/http/RequestQueue;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/net/http/RequestQueue;)Lorg/apache/http/HttpHost;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/net/http/RequestQueue;Lorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/net/http/RequestQueue;->determineHost(Lorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Landroid/net/http/RequestQueue;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Landroid/net/http/RequestQueue;->setProxyConfig()V

    #@3
    return-void
.end method

.method private determineHost(Lorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;
    .registers 4
    .parameter "host"

    #@0
    .prologue
    .line 404
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@2
    if-eqz v0, :cond_10

    #@4
    const-string v0, "https"

    #@6
    invoke-virtual {p1}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_11

    #@10
    .end local p1
    :cond_10
    :goto_10
    return-object p1

    #@11
    .restart local p1
    :cond_11
    iget-object p1, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@13
    goto :goto_10
.end method

.method private removeFirst(Ljava/util/LinkedHashMap;)Landroid/net/http/Request;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/http/HttpHost;",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/net/http/Request;",
            ">;>;)",
            "Landroid/net/http/Request;"
        }
    .end annotation

    #@0
    .prologue
    .line 525
    .local p1, requestQueue:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    const/4 v3, 0x0

    #@1
    .line 526
    .local v3, ret:Landroid/net/http/Request;
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    #@4
    move-result-object v4

    #@5
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .line 527
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_2e

    #@f
    .line 528
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Ljava/util/Map$Entry;

    #@15
    .line 529
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Ljava/util/LinkedList;

    #@1b
    .line 530
    .local v2, reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    .end local v3           #ret:Landroid/net/http/Request;
    check-cast v3, Landroid/net/http/Request;

    #@21
    .line 531
    .restart local v3       #ret:Landroid/net/http/Request;
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_2e

    #@27
    .line 532
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {p1, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    .line 535
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    .end local v2           #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :cond_2e
    return-object v3
.end method

.method private declared-synchronized setProxyConfig()V
    .registers 6

    #@0
    .prologue
    .line 263
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/net/http/RequestQueue;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@3
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@6
    move-result-object v1

    #@7
    .line 264
    .local v1, info:Landroid/net/NetworkInfo;
    if-eqz v1, :cond_15

    #@9
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    #@c
    move-result v2

    #@d
    const/4 v3, 0x1

    #@e
    if-ne v2, v3, :cond_15

    #@10
    .line 265
    const/4 v2, 0x0

    #@11
    iput-object v2, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_21

    #@13
    .line 276
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 267
    :cond_15
    :try_start_15
    iget-object v2, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@17
    invoke-static {v2}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 269
    .local v0, host:Ljava/lang/String;
    if-nez v0, :cond_24

    #@1d
    .line 270
    const/4 v2, 0x0

    #@1e
    iput-object v2, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;
    :try_end_20
    .catchall {:try_start_15 .. :try_end_20} :catchall_21

    #@20
    goto :goto_13

    #@21
    .line 263
    .end local v0           #host:Ljava/lang/String;
    .end local v1           #info:Landroid/net/NetworkInfo;
    :catchall_21
    move-exception v2

    #@22
    monitor-exit p0

    #@23
    throw v2

    #@24
    .line 272
    .restart local v0       #host:Ljava/lang/String;
    .restart local v1       #info:Landroid/net/NetworkInfo;
    :cond_24
    :try_start_24
    iget-object v2, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@26
    invoke-virtual {v2}, Landroid/net/http/RequestQueue$ActivePool;->disablePersistence()V

    #@29
    .line 273
    new-instance v2, Lorg/apache/http/HttpHost;

    #@2b
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@2d
    invoke-static {v3}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    #@30
    move-result v3

    #@31
    const-string v4, "http"

    #@33
    invoke-direct {v2, v0, v3, v4}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@36
    iput-object v2, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;
    :try_end_38
    .catchall {:try_start_24 .. :try_end_38} :catchall_21

    #@38
    goto :goto_13
.end method


# virtual methods
.method public declared-synchronized disablePlatformNotifications()V
    .registers 3

    #@0
    .prologue
    .line 252
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mProxyChangeReceiver:Landroid/content/BroadcastReceiver;

    #@3
    if-eqz v0, :cond_f

    #@5
    .line 253
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@7
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mProxyChangeReceiver:Landroid/content/BroadcastReceiver;

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@c
    .line 254
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Landroid/net/http/RequestQueue;->mProxyChangeReceiver:Landroid/content/BroadcastReceiver;
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    #@f
    .line 256
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 252
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method declared-synchronized dump()V
    .registers 13

    #@0
    .prologue
    .line 420
    monitor-enter p0

    #@1
    :try_start_1
    const-string v10, "dump()"

    #@3
    invoke-static {v10}, Landroid/net/http/HttpLog;->v(Ljava/lang/String;)V

    #@6
    .line 421
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    .line 422
    .local v2, dump:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@c
    .line 427
    .local v0, count:I
    iget-object v10, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@e
    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->isEmpty()Z

    #@11
    move-result v10

    #@12
    if-nez v10, :cond_9c

    #@14
    .line 428
    iget-object v10, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@16
    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    #@19
    move-result-object v10

    #@1a
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v5

    #@1e
    .local v5, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;>;"
    move v1, v0

    #@1f
    .line 429
    .end local v0           #count:I
    .local v1, count:I
    :goto_1f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v10

    #@23
    if-eqz v10, :cond_9b

    #@25
    .line 430
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Ljava/util/Map$Entry;

    #@2b
    .line 431
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2e
    move-result-object v10

    #@2f
    check-cast v10, Lorg/apache/http/HttpHost;

    #@31
    invoke-virtual {v10}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 432
    .local v4, hostName:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@37
    new-instance v10, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string/jumbo v11, "p"

    #@3f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v10

    #@43
    add-int/lit8 v0, v1, 0x1

    #@45
    .end local v1           #count:I
    .restart local v0       #count:I
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v10

    #@49
    const-string v11, " "

    #@4b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v10

    #@4f
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v10

    #@53
    const-string v11, " "

    #@55
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v10

    #@59
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v10

    #@5d
    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@60
    .line 434
    .local v6, line:Ljava/lang/StringBuilder;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@63
    move-result-object v8

    #@64
    check-cast v8, Ljava/util/LinkedList;

    #@66
    .line 435
    .local v8, reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    const/4 v10, 0x0

    #@67
    invoke-virtual {v8, v10}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    #@6a
    move-result-object v7

    #@6b
    .line 436
    .local v7, reqIter:Ljava/util/ListIterator;
    :goto_6b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@6e
    move-result v10

    #@6f
    if-eqz v10, :cond_91

    #@71
    .line 437
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@74
    move-result-object v9

    #@75
    check-cast v9, Landroid/net/http/Request;

    #@77
    .line 438
    .local v9, request:Landroid/net/http/Request;
    new-instance v10, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v10

    #@80
    const-string v11, " "

    #@82
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v10

    #@86
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v10

    #@8a
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_8d
    .catchall {:try_start_1 .. :try_end_8d} :catchall_8e

    #@8d
    goto :goto_6b

    #@8e
    .line 420
    .end local v0           #count:I
    .end local v2           #dump:Ljava/lang/StringBuilder;
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    .end local v4           #hostName:Ljava/lang/String;
    .end local v5           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;>;"
    .end local v6           #line:Ljava/lang/StringBuilder;
    .end local v7           #reqIter:Ljava/util/ListIterator;
    .end local v8           #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    .end local v9           #request:Landroid/net/http/Request;
    :catchall_8e
    move-exception v10

    #@8f
    monitor-exit p0

    #@90
    throw v10

    #@91
    .line 440
    .restart local v0       #count:I
    .restart local v2       #dump:Ljava/lang/StringBuilder;
    .restart local v3       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    .restart local v4       #hostName:Ljava/lang/String;
    .restart local v5       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;>;"
    .restart local v6       #line:Ljava/lang/StringBuilder;
    .restart local v7       #reqIter:Ljava/util/ListIterator;
    .restart local v8       #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :cond_91
    :try_start_91
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@94
    .line 441
    const-string v10, "\n"

    #@96
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move v1, v0

    #@9a
    .line 442
    .end local v0           #count:I
    .restart local v1       #count:I
    goto :goto_1f

    #@9b
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;"
    .end local v4           #hostName:Ljava/lang/String;
    .end local v6           #line:Ljava/lang/StringBuilder;
    .end local v7           #reqIter:Ljava/util/ListIterator;
    .end local v8           #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :cond_9b
    move v0, v1

    #@9c
    .line 444
    .end local v1           #count:I
    .end local v5           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/http/HttpHost;Ljava/util/LinkedList<Landroid/net/http/Request;>;>;>;"
    .restart local v0       #count:I
    :cond_9c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v10

    #@a0
    invoke-static {v10}, Landroid/net/http/HttpLog;->v(Ljava/lang/String;)V
    :try_end_a3
    .catchall {:try_start_91 .. :try_end_a3} :catchall_8e

    #@a3
    .line 445
    monitor-exit p0

    #@a4
    return-void
.end method

.method public declared-synchronized enablePlatformNotifications()V
    .registers 5

    #@0
    .prologue
    .line 230
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mProxyChangeReceiver:Landroid/content/BroadcastReceiver;

    #@3
    if-nez v0, :cond_1a

    #@5
    .line 231
    new-instance v0, Landroid/net/http/RequestQueue$1;

    #@7
    invoke-direct {v0, p0}, Landroid/net/http/RequestQueue$1;-><init>(Landroid/net/http/RequestQueue;)V

    #@a
    iput-object v0, p0, Landroid/net/http/RequestQueue;->mProxyChangeReceiver:Landroid/content/BroadcastReceiver;

    #@c
    .line 238
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@e
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mProxyChangeReceiver:Landroid/content/BroadcastReceiver;

    #@10
    new-instance v2, Landroid/content/IntentFilter;

    #@12
    const-string v3, "android.intent.action.PROXY_CHANGE"

    #@14
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1a
    .line 242
    :cond_1a
    invoke-direct {p0}, Landroid/net/http/RequestQueue;->setProxyConfig()V
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1f

    #@1d
    .line 243
    monitor-exit p0

    #@1e
    return-void

    #@1f
    .line 230
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit p0

    #@21
    throw v0
.end method

.method public getProxyHost()Lorg/apache/http/HttpHost;
    .registers 2

    #@0
    .prologue
    .line 283
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@2
    return-object v0
.end method

.method public declared-synchronized getRequest()Landroid/net/http/Request;
    .registers 3

    #@0
    .prologue
    .line 451
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .line 453
    .local v0, ret:Landroid/net/http/Request;
    :try_start_2
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@4
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_10

    #@a
    .line 454
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@c
    invoke-direct {p0, v1}, Landroid/net/http/RequestQueue;->removeFirst(Ljava/util/LinkedHashMap;)Landroid/net/http/Request;
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_12

    #@f
    move-result-object v0

    #@10
    .line 457
    :cond_10
    monitor-exit p0

    #@11
    return-object v0

    #@12
    .line 451
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0

    #@14
    throw v1
.end method

.method public declared-synchronized getRequest(Lorg/apache/http/HttpHost;)Landroid/net/http/Request;
    .registers 6
    .parameter "host"

    #@0
    .prologue
    .line 464
    monitor-enter p0

    #@1
    const/4 v2, 0x0

    #@2
    .line 466
    .local v2, ret:Landroid/net/http/Request;
    :try_start_2
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@4
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_25

    #@a
    .line 467
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@c
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Ljava/util/LinkedList;

    #@12
    .line 468
    .local v1, reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    move-object v0, v3

    #@17
    check-cast v0, Landroid/net/http/Request;

    #@19
    move-object v2, v0

    #@1a
    .line 469
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_25

    #@20
    .line 470
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@22
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catchall {:try_start_2 .. :try_end_25} :catchall_27

    #@25
    .line 474
    .end local v1           #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :cond_25
    monitor-exit p0

    #@26
    return-object v2

    #@27
    .line 464
    :catchall_27
    move-exception v3

    #@28
    monitor-exit p0

    #@29
    throw v3
.end method

.method public declared-synchronized haveRequest(Lorg/apache/http/HttpHost;)Z
    .registers 3
    .parameter "host"

    #@0
    .prologue
    .line 481
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public queueRequest(Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;
    .registers 20
    .parameter "url"
    .parameter "uri"
    .parameter "method"
    .parameter
    .parameter "eventHandler"
    .parameter "bodyProvider"
    .parameter "bodyLength"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/WebAddress;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/http/EventHandler;",
            "Ljava/io/InputStream;",
            "I)",
            "Landroid/net/http/RequestHandle;"
        }
    .end annotation

    #@0
    .prologue
    .line 324
    .local p4, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p5, :cond_7

    #@2
    .line 325
    new-instance p5, Landroid/net/http/LoggingEventHandler;

    #@4
    .end local p5
    invoke-direct/range {p5 .. p5}, Landroid/net/http/LoggingEventHandler;-><init>()V

    #@7
    .line 330
    .restart local p5
    :cond_7
    new-instance v2, Lorg/apache/http/HttpHost;

    #@9
    invoke-virtual {p2}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p2}, Landroid/net/WebAddress;->getPort()I

    #@10
    move-result v3

    #@11
    invoke-virtual {p2}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-direct {v2, v1, v3, v4}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@18
    .line 333
    .local v2, httpHost:Lorg/apache/http/HttpHost;
    new-instance v0, Landroid/net/http/Request;

    #@1a
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@1c
    invoke-virtual {p2}, Landroid/net/WebAddress;->getPath()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    move-object v1, p3

    #@21
    move-object/from16 v5, p6

    #@23
    move/from16 v6, p7

    #@25
    move-object/from16 v7, p5

    #@27
    move-object/from16 v8, p4

    #@29
    invoke-direct/range {v0 .. v8}, Landroid/net/http/Request;-><init>(Ljava/lang/String;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Ljava/lang/String;Ljava/io/InputStream;ILandroid/net/http/EventHandler;Ljava/util/Map;)V

    #@2c
    .line 336
    .local v0, req:Landroid/net/http/Request;
    const/4 v1, 0x0

    #@2d
    invoke-virtual {p0, v0, v1}, Landroid/net/http/RequestQueue;->queueRequest(Landroid/net/http/Request;Z)V

    #@30
    .line 338
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@32
    invoke-static {v1}, Landroid/net/http/RequestQueue$ActivePool;->access$408(Landroid/net/http/RequestQueue$ActivePool;)I

    #@35
    .line 341
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@37
    invoke-virtual {v1}, Landroid/net/http/RequestQueue$ActivePool;->startConnectionThread()V

    #@3a
    .line 343
    new-instance v3, Landroid/net/http/RequestHandle;

    #@3c
    move-object v4, p0

    #@3d
    move-object v5, p1

    #@3e
    move-object v6, p2

    #@3f
    move-object v7, p3

    #@40
    move-object/from16 v8, p4

    #@42
    move-object/from16 v9, p6

    #@44
    move/from16 v10, p7

    #@46
    move-object v11, v0

    #@47
    invoke-direct/range {v3 .. v11}, Landroid/net/http/RequestHandle;-><init>(Landroid/net/http/RequestQueue;Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Ljava/io/InputStream;ILandroid/net/http/Request;)V

    #@4a
    return-object v3
.end method

.method public queueRequest(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;
    .registers 15
    .parameter "url"
    .parameter "method"
    .parameter
    .parameter "eventHandler"
    .parameter "bodyProvider"
    .parameter "bodyLength"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/http/EventHandler;",
            "Ljava/io/InputStream;",
            "I)",
            "Landroid/net/http/RequestHandle;"
        }
    .end annotation

    #@0
    .prologue
    .line 300
    .local p3, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Landroid/net/WebAddress;

    #@2
    invoke-direct {v2, p1}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    #@5
    .local v2, uri:Landroid/net/WebAddress;
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move-object v3, p2

    #@8
    move-object v4, p3

    #@9
    move-object v5, p4

    #@a
    move-object v6, p5

    #@b
    move v7, p6

    #@c
    .line 301
    invoke-virtual/range {v0 .. v7}, Landroid/net/http/RequestQueue;->queueRequest(Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method protected declared-synchronized queueRequest(Landroid/net/http/Request;Z)V
    .registers 6
    .parameter "request"
    .parameter "head"

    #@0
    .prologue
    .line 499
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p1, Landroid/net/http/Request;->mProxyHost:Lorg/apache/http/HttpHost;

    #@3
    if-nez v2, :cond_1e

    #@5
    iget-object v0, p1, Landroid/net/http/Request;->mHost:Lorg/apache/http/HttpHost;

    #@7
    .line 501
    .local v0, host:Lorg/apache/http/HttpHost;
    :goto_7
    iget-object v2, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@9
    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_21

    #@f
    .line 502
    iget-object v2, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@11
    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Ljava/util/LinkedList;

    #@17
    .line 507
    .local v1, reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :goto_17
    if-eqz p2, :cond_2f

    #@19
    .line 508
    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_2c

    #@1c
    .line 512
    :goto_1c
    monitor-exit p0

    #@1d
    return-void

    #@1e
    .line 499
    .end local v0           #host:Lorg/apache/http/HttpHost;
    .end local v1           #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :cond_1e
    :try_start_1e
    iget-object v0, p1, Landroid/net/http/Request;->mProxyHost:Lorg/apache/http/HttpHost;

    #@20
    goto :goto_7

    #@21
    .line 504
    .restart local v0       #host:Lorg/apache/http/HttpHost;
    :cond_21
    new-instance v1, Ljava/util/LinkedList;

    #@23
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    #@26
    .line 505
    .restart local v1       #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    iget-object v2, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@28
    invoke-virtual {v2, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2b
    .catchall {:try_start_1e .. :try_end_2b} :catchall_2c

    #@2b
    goto :goto_17

    #@2c
    .line 499
    .end local v0           #host:Lorg/apache/http/HttpHost;
    .end local v1           #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :catchall_2c
    move-exception v2

    #@2d
    monitor-exit p0

    #@2e
    throw v2

    #@2f
    .line 510
    .restart local v0       #host:Lorg/apache/http/HttpHost;
    .restart local v1       #reqList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    :cond_2f
    :try_start_2f
    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_2c

    #@32
    goto :goto_1c
.end method

.method public queueSynchronousRequest(Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Landroid/net/http/EventHandler;Ljava/io/InputStream;I)Landroid/net/http/RequestHandle;
    .registers 21
    .parameter "url"
    .parameter "uri"
    .parameter "method"
    .parameter
    .parameter "eventHandler"
    .parameter "bodyProvider"
    .parameter "bodyLength"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/WebAddress;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/http/EventHandler;",
            "Ljava/io/InputStream;",
            "I)",
            "Landroid/net/http/RequestHandle;"
        }
    .end annotation

    #@0
    .prologue
    .line 378
    .local p4, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Lorg/apache/http/HttpHost;

    #@2
    invoke-virtual {p2}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p2}, Landroid/net/WebAddress;->getPort()I

    #@9
    move-result v3

    #@a
    invoke-virtual {p2}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    invoke-direct {v2, v1, v3, v4}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@11
    .line 380
    .local v2, host:Lorg/apache/http/HttpHost;
    new-instance v0, Landroid/net/http/Request;

    #@13
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@15
    invoke-virtual {p2}, Landroid/net/WebAddress;->getPath()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    move-object/from16 v1, p3

    #@1b
    move-object/from16 v5, p6

    #@1d
    move/from16 v6, p7

    #@1f
    move-object/from16 v7, p5

    #@21
    move-object/from16 v8, p4

    #@23
    invoke-direct/range {v0 .. v8}, Landroid/net/http/Request;-><init>(Ljava/lang/String;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Ljava/lang/String;Ljava/io/InputStream;ILandroid/net/http/EventHandler;Ljava/util/Map;)V

    #@26
    .line 385
    .local v0, req:Landroid/net/http/Request;
    invoke-direct {p0, v2}, Landroid/net/http/RequestQueue;->determineHost(Lorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;

    #@29
    move-result-object v2

    #@2a
    .line 386
    iget-object v1, p0, Landroid/net/http/RequestQueue;->mContext:Landroid/content/Context;

    #@2c
    iget-object v3, p0, Landroid/net/http/RequestQueue;->mProxyHost:Lorg/apache/http/HttpHost;

    #@2e
    new-instance v4, Landroid/net/http/RequestQueue$SyncFeeder;

    #@30
    invoke-direct {v4}, Landroid/net/http/RequestQueue$SyncFeeder;-><init>()V

    #@33
    invoke-static {v1, v2, v3, v4}, Landroid/net/http/Connection;->getConnection(Landroid/content/Context;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)Landroid/net/http/Connection;

    #@36
    move-result-object v12

    #@37
    .line 391
    .local v12, conn:Landroid/net/http/Connection;
    new-instance v3, Landroid/net/http/RequestHandle;

    #@39
    move-object v4, p0

    #@3a
    move-object v5, p1

    #@3b
    move-object v6, p2

    #@3c
    move-object/from16 v7, p3

    #@3e
    move-object/from16 v8, p4

    #@40
    move-object/from16 v9, p6

    #@42
    move/from16 v10, p7

    #@44
    move-object v11, v0

    #@45
    invoke-direct/range {v3 .. v12}, Landroid/net/http/RequestHandle;-><init>(Landroid/net/http/RequestQueue;Ljava/lang/String;Landroid/net/WebAddress;Ljava/lang/String;Ljava/util/Map;Ljava/io/InputStream;ILandroid/net/http/Request;Landroid/net/http/Connection;)V

    #@48
    return-object v3
.end method

.method declared-synchronized requestsPending()Z
    .registers 2

    #@0
    .prologue
    .line 412
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mPending:Ljava/util/LinkedHashMap;

    #@3
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_e

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_c

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_a

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public requeueRequest(Landroid/net/http/Request;)V
    .registers 3
    .parameter "request"

    #@0
    .prologue
    .line 488
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/net/http/RequestQueue;->queueRequest(Landroid/net/http/Request;Z)V

    #@4
    .line 489
    return-void
.end method

.method public shutdown()V
    .registers 2

    #@0
    .prologue
    .line 495
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@2
    invoke-virtual {v0}, Landroid/net/http/RequestQueue$ActivePool;->shutdown()V

    #@5
    .line 496
    return-void
.end method

.method public startTiming()V
    .registers 2

    #@0
    .prologue
    .line 516
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@2
    invoke-virtual {v0}, Landroid/net/http/RequestQueue$ActivePool;->startTiming()V

    #@5
    .line 517
    return-void
.end method

.method public stopTiming()V
    .registers 2

    #@0
    .prologue
    .line 520
    iget-object v0, p0, Landroid/net/http/RequestQueue;->mActivePool:Landroid/net/http/RequestQueue$ActivePool;

    #@2
    invoke-virtual {v0}, Landroid/net/http/RequestQueue$ActivePool;->stopTiming()V

    #@5
    .line 521
    return-void
.end method
