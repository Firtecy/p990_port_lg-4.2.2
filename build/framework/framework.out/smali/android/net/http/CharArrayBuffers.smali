.class Landroid/net/http/CharArrayBuffers;
.super Ljava/lang/Object;
.source "CharArrayBuffers.java"


# static fields
.field static final uppercaseAddon:C = ' '


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static containsIgnoreCaseTrimmed(Lorg/apache/http/util/CharArrayBuffer;ILjava/lang/String;)Z
    .registers 13
    .parameter "buffer"
    .parameter "beginIndex"
    .parameter "str"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 41
    invoke-virtual {p0}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@5
    move-result v4

    #@6
    .line 42
    .local v4, len:I
    invoke-virtual {p0}, Lorg/apache/http/util/CharArrayBuffer;->buffer()[C

    #@9
    move-result-object v2

    #@a
    .line 43
    .local v2, chars:[C
    :goto_a
    if-ge p1, v4, :cond_17

    #@c
    aget-char v9, v2, p1

    #@e
    invoke-static {v9}, Lorg/apache/http/protocol/HTTP;->isWhitespace(C)Z

    #@11
    move-result v9

    #@12
    if-eqz v9, :cond_17

    #@14
    .line 44
    add-int/lit8 p1, p1, 0x1

    #@16
    goto :goto_a

    #@17
    .line 46
    :cond_17
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@1a
    move-result v6

    #@1b
    .line 47
    .local v6, size:I
    add-int v9, p1, v6

    #@1d
    if-lt v4, v9, :cond_3d

    #@1f
    move v5, v7

    #@20
    .line 48
    .local v5, ok:Z
    :goto_20
    const/4 v3, 0x0

    #@21
    .local v3, j:I
    :goto_21
    if-eqz v5, :cond_41

    #@23
    if-ge v3, v6, :cond_41

    #@25
    .line 49
    add-int v9, p1, v3

    #@27
    aget-char v0, v2, v9

    #@29
    .line 50
    .local v0, a:C
    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    #@2c
    move-result v1

    #@2d
    .line 51
    .local v1, b:C
    if-eq v0, v1, :cond_3a

    #@2f
    .line 52
    invoke-static {v0}, Landroid/net/http/CharArrayBuffers;->toLower(C)C

    #@32
    move-result v0

    #@33
    .line 53
    invoke-static {v1}, Landroid/net/http/CharArrayBuffers;->toLower(C)C

    #@36
    move-result v1

    #@37
    .line 54
    if-ne v0, v1, :cond_3f

    #@39
    move v5, v7

    #@3a
    .line 48
    :cond_3a
    :goto_3a
    add-int/lit8 v3, v3, 0x1

    #@3c
    goto :goto_21

    #@3d
    .end local v0           #a:C
    .end local v1           #b:C
    .end local v3           #j:I
    .end local v5           #ok:Z
    :cond_3d
    move v5, v8

    #@3e
    .line 47
    goto :goto_20

    #@3f
    .restart local v0       #a:C
    .restart local v1       #b:C
    .restart local v3       #j:I
    .restart local v5       #ok:Z
    :cond_3f
    move v5, v8

    #@40
    .line 54
    goto :goto_3a

    #@41
    .line 57
    .end local v0           #a:C
    .end local v1           #b:C
    :cond_41
    return v5
.end method

.method static setLowercaseIndexOf(Lorg/apache/http/util/CharArrayBuffer;I)I
    .registers 8
    .parameter "buffer"
    .parameter "ch"

    #@0
    .prologue
    .line 66
    const/4 v0, 0x0

    #@1
    .line 67
    .local v0, beginIndex:I
    invoke-virtual {p0}, Lorg/apache/http/util/CharArrayBuffer;->length()I

    #@4
    move-result v3

    #@5
    .line 68
    .local v3, endIndex:I
    invoke-virtual {p0}, Lorg/apache/http/util/CharArrayBuffer;->buffer()[C

    #@8
    move-result-object v1

    #@9
    .line 70
    .local v1, chars:[C
    move v4, v0

    #@a
    .local v4, i:I
    :goto_a
    if-ge v4, v3, :cond_21

    #@c
    .line 71
    aget-char v2, v1, v4

    #@e
    .line 72
    .local v2, current:C
    if-ne v2, p1, :cond_11

    #@10
    .line 80
    .end local v2           #current:C
    .end local v4           #i:I
    :goto_10
    return v4

    #@11
    .line 74
    .restart local v2       #current:C
    .restart local v4       #i:I
    :cond_11
    const/16 v5, 0x41

    #@13
    if-lt v2, v5, :cond_1e

    #@15
    const/16 v5, 0x5a

    #@17
    if-gt v2, v5, :cond_1e

    #@19
    .line 76
    add-int/lit8 v5, v2, 0x20

    #@1b
    int-to-char v2, v5

    #@1c
    .line 77
    aput-char v2, v1, v4

    #@1e
    .line 70
    :cond_1e
    add-int/lit8 v4, v4, 0x1

    #@20
    goto :goto_a

    #@21
    .line 80
    .end local v2           #current:C
    :cond_21
    const/4 v4, -0x1

    #@22
    goto :goto_10
.end method

.method private static toLower(C)C
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 84
    const/16 v0, 0x41

    #@2
    if-lt p0, v0, :cond_b

    #@4
    const/16 v0, 0x5a

    #@6
    if-gt p0, v0, :cond_b

    #@8
    .line 85
    add-int/lit8 v0, p0, 0x20

    #@a
    int-to-char p0, v0

    #@b
    .line 87
    :cond_b
    return p0
.end method
