.class Landroid/net/http/IdleCache;
.super Ljava/lang/Object;
.source "IdleCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/http/IdleCache$1;,
        Landroid/net/http/IdleCache$IdleReaper;,
        Landroid/net/http/IdleCache$Entry;
    }
.end annotation


# static fields
.field private static final CHECK_INTERVAL:I = 0x7d0

.field private static final EMPTY_CHECK_MAX:I = 0x5

.field private static final IDLE_CACHE_MAX:I = 0x8

.field private static final TIMEOUT:I = 0x1770


# instance fields
.field private mCached:I

.field private mCount:I

.field private mEntries:[Landroid/net/http/IdleCache$Entry;

.field private mReused:I

.field private mThread:Landroid/net/http/IdleCache$IdleReaper;


# direct methods
.method constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x0

    #@3
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 46
    new-array v1, v3, [Landroid/net/http/IdleCache$Entry;

    #@8
    iput-object v1, p0, Landroid/net/http/IdleCache;->mEntries:[Landroid/net/http/IdleCache$Entry;

    #@a
    .line 48
    iput v2, p0, Landroid/net/http/IdleCache;->mCount:I

    #@c
    .line 50
    const/4 v1, 0x0

    #@d
    iput-object v1, p0, Landroid/net/http/IdleCache;->mThread:Landroid/net/http/IdleCache$IdleReaper;

    #@f
    .line 53
    iput v2, p0, Landroid/net/http/IdleCache;->mCached:I

    #@11
    .line 54
    iput v2, p0, Landroid/net/http/IdleCache;->mReused:I

    #@13
    .line 57
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    if-ge v0, v3, :cond_22

    #@16
    .line 58
    iget-object v1, p0, Landroid/net/http/IdleCache;->mEntries:[Landroid/net/http/IdleCache$Entry;

    #@18
    new-instance v2, Landroid/net/http/IdleCache$Entry;

    #@1a
    invoke-direct {v2, p0}, Landroid/net/http/IdleCache$Entry;-><init>(Landroid/net/http/IdleCache;)V

    #@1d
    aput-object v2, v1, v0

    #@1f
    .line 57
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_14

    #@22
    .line 60
    :cond_22
    return-void
.end method

.method static synthetic access$100(Landroid/net/http/IdleCache;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget v0, p0, Landroid/net/http/IdleCache;->mCount:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/net/http/IdleCache;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 30
    invoke-direct {p0}, Landroid/net/http/IdleCache;->clearIdle()V

    #@3
    return-void
.end method

.method static synthetic access$302(Landroid/net/http/IdleCache;Landroid/net/http/IdleCache$IdleReaper;)Landroid/net/http/IdleCache$IdleReaper;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput-object p1, p0, Landroid/net/http/IdleCache;->mThread:Landroid/net/http/IdleCache$IdleReaper;

    #@2
    return-object p1
.end method

.method private declared-synchronized clearIdle()V
    .registers 7

    #@0
    .prologue
    .line 130
    monitor-enter p0

    #@1
    :try_start_1
    iget v4, p0, Landroid/net/http/IdleCache;->mCount:I

    #@3
    if-lez v4, :cond_30

    #@5
    .line 131
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@8
    move-result-wide v2

    #@9
    .line 132
    .local v2, time:J
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    const/16 v4, 0x8

    #@c
    if-ge v1, v4, :cond_30

    #@e
    .line 133
    iget-object v4, p0, Landroid/net/http/IdleCache;->mEntries:[Landroid/net/http/IdleCache$Entry;

    #@10
    aget-object v0, v4, v1

    #@12
    .line 134
    .local v0, entry:Landroid/net/http/IdleCache$Entry;
    iget-object v4, v0, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@14
    if-eqz v4, :cond_2d

    #@16
    iget-wide v4, v0, Landroid/net/http/IdleCache$Entry;->mTimeout:J

    #@18
    cmp-long v4, v2, v4

    #@1a
    if-lez v4, :cond_2d

    #@1c
    .line 135
    const/4 v4, 0x0

    #@1d
    iput-object v4, v0, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@1f
    .line 136
    iget-object v4, v0, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@21
    invoke-virtual {v4}, Landroid/net/http/Connection;->closeConnection()V

    #@24
    .line 137
    const/4 v4, 0x0

    #@25
    iput-object v4, v0, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@27
    .line 138
    iget v4, p0, Landroid/net/http/IdleCache;->mCount:I

    #@29
    add-int/lit8 v4, v4, -0x1

    #@2b
    iput v4, p0, Landroid/net/http/IdleCache;->mCount:I
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_32

    #@2d
    .line 132
    :cond_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_a

    #@30
    .line 142
    .end local v0           #entry:Landroid/net/http/IdleCache$Entry;
    .end local v1           #i:I
    .end local v2           #time:J
    :cond_30
    monitor-exit p0

    #@31
    return-void

    #@32
    .line 130
    :catchall_32
    move-exception v4

    #@33
    monitor-exit p0

    #@34
    throw v4
.end method


# virtual methods
.method declared-synchronized cacheConnection(Lorg/apache/http/HttpHost;Landroid/net/http/Connection;)Z
    .registers 10
    .parameter "host"
    .parameter "connection"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    .line 69
    monitor-enter p0

    #@3
    const/4 v2, 0x0

    #@4
    .line 75
    .local v2, ret:Z
    :try_start_4
    iget v5, p0, Landroid/net/http/IdleCache;->mCount:I

    #@6
    if-ge v5, v6, :cond_38

    #@8
    .line 76
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v3

    #@c
    .line 77
    .local v3, time:J
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v6, :cond_38

    #@f
    .line 78
    iget-object v5, p0, Landroid/net/http/IdleCache;->mEntries:[Landroid/net/http/IdleCache$Entry;

    #@11
    aget-object v0, v5, v1

    #@13
    .line 79
    .local v0, entry:Landroid/net/http/IdleCache$Entry;
    iget-object v5, v0, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@15
    if-nez v5, :cond_3a

    #@17
    .line 80
    iput-object p1, v0, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@19
    .line 81
    iput-object p2, v0, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@1b
    .line 82
    const-wide/16 v5, 0x1770

    #@1d
    add-long/2addr v5, v3

    #@1e
    iput-wide v5, v0, Landroid/net/http/IdleCache$Entry;->mTimeout:J

    #@20
    .line 83
    iget v5, p0, Landroid/net/http/IdleCache;->mCount:I

    #@22
    add-int/lit8 v5, v5, 0x1

    #@24
    iput v5, p0, Landroid/net/http/IdleCache;->mCount:I

    #@26
    .line 85
    const/4 v2, 0x1

    #@27
    .line 86
    iget-object v5, p0, Landroid/net/http/IdleCache;->mThread:Landroid/net/http/IdleCache$IdleReaper;

    #@29
    if-nez v5, :cond_38

    #@2b
    .line 87
    new-instance v5, Landroid/net/http/IdleCache$IdleReaper;

    #@2d
    const/4 v6, 0x0

    #@2e
    invoke-direct {v5, p0, v6}, Landroid/net/http/IdleCache$IdleReaper;-><init>(Landroid/net/http/IdleCache;Landroid/net/http/IdleCache$1;)V

    #@31
    iput-object v5, p0, Landroid/net/http/IdleCache;->mThread:Landroid/net/http/IdleCache$IdleReaper;

    #@33
    .line 88
    iget-object v5, p0, Landroid/net/http/IdleCache;->mThread:Landroid/net/http/IdleCache$IdleReaper;

    #@35
    invoke-virtual {v5}, Landroid/net/http/IdleCache$IdleReaper;->start()V
    :try_end_38
    .catchall {:try_start_4 .. :try_end_38} :catchall_3d

    #@38
    .line 94
    .end local v0           #entry:Landroid/net/http/IdleCache$Entry;
    .end local v1           #i:I
    .end local v3           #time:J
    :cond_38
    monitor-exit p0

    #@39
    return v2

    #@3a
    .line 77
    .restart local v0       #entry:Landroid/net/http/IdleCache$Entry;
    .restart local v1       #i:I
    .restart local v3       #time:J
    :cond_3a
    add-int/lit8 v1, v1, 0x1

    #@3c
    goto :goto_d

    #@3d
    .line 69
    .end local v0           #entry:Landroid/net/http/IdleCache$Entry;
    .end local v1           #i:I
    .end local v3           #time:J
    :catchall_3d
    move-exception v5

    #@3e
    monitor-exit p0

    #@3f
    throw v5
.end method

.method declared-synchronized clear()V
    .registers 4

    #@0
    .prologue
    .line 118
    monitor-enter p0

    #@1
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    :try_start_2
    iget v2, p0, Landroid/net/http/IdleCache;->mCount:I

    #@4
    if-lez v2, :cond_26

    #@6
    const/16 v2, 0x8

    #@8
    if-ge v1, v2, :cond_26

    #@a
    .line 119
    iget-object v2, p0, Landroid/net/http/IdleCache;->mEntries:[Landroid/net/http/IdleCache$Entry;

    #@c
    aget-object v0, v2, v1

    #@e
    .line 120
    .local v0, entry:Landroid/net/http/IdleCache$Entry;
    iget-object v2, v0, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@10
    if-eqz v2, :cond_23

    #@12
    .line 121
    const/4 v2, 0x0

    #@13
    iput-object v2, v0, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@15
    .line 122
    iget-object v2, v0, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@17
    invoke-virtual {v2}, Landroid/net/http/Connection;->closeConnection()V

    #@1a
    .line 123
    const/4 v2, 0x0

    #@1b
    iput-object v2, v0, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@1d
    .line 124
    iget v2, p0, Landroid/net/http/IdleCache;->mCount:I

    #@1f
    add-int/lit8 v2, v2, -0x1

    #@21
    iput v2, p0, Landroid/net/http/IdleCache;->mCount:I
    :try_end_23
    .catchall {:try_start_2 .. :try_end_23} :catchall_28

    #@23
    .line 118
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_2

    #@26
    .line 127
    .end local v0           #entry:Landroid/net/http/IdleCache$Entry;
    :cond_26
    monitor-exit p0

    #@27
    return-void

    #@28
    .line 118
    :catchall_28
    move-exception v2

    #@29
    monitor-exit p0

    #@2a
    throw v2
.end method

.method declared-synchronized getConnection(Lorg/apache/http/HttpHost;)Landroid/net/http/Connection;
    .registers 7
    .parameter "host"

    #@0
    .prologue
    .line 98
    monitor-enter p0

    #@1
    const/4 v3, 0x0

    #@2
    .line 100
    .local v3, ret:Landroid/net/http/Connection;
    :try_start_2
    iget v4, p0, Landroid/net/http/IdleCache;->mCount:I

    #@4
    if-lez v4, :cond_27

    #@6
    .line 101
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    const/16 v4, 0x8

    #@9
    if-ge v2, v4, :cond_27

    #@b
    .line 102
    iget-object v4, p0, Landroid/net/http/IdleCache;->mEntries:[Landroid/net/http/IdleCache$Entry;

    #@d
    aget-object v1, v4, v2

    #@f
    .line 103
    .local v1, entry:Landroid/net/http/IdleCache$Entry;
    iget-object v0, v1, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@11
    .line 104
    .local v0, eHost:Lorg/apache/http/HttpHost;
    if-eqz v0, :cond_29

    #@13
    invoke-virtual {v0, p1}, Lorg/apache/http/HttpHost;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_29

    #@19
    .line 105
    iget-object v3, v1, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@1b
    .line 106
    const/4 v4, 0x0

    #@1c
    iput-object v4, v1, Landroid/net/http/IdleCache$Entry;->mHost:Lorg/apache/http/HttpHost;

    #@1e
    .line 107
    const/4 v4, 0x0

    #@1f
    iput-object v4, v1, Landroid/net/http/IdleCache$Entry;->mConnection:Landroid/net/http/Connection;

    #@21
    .line 108
    iget v4, p0, Landroid/net/http/IdleCache;->mCount:I

    #@23
    add-int/lit8 v4, v4, -0x1

    #@25
    iput v4, p0, Landroid/net/http/IdleCache;->mCount:I
    :try_end_27
    .catchall {:try_start_2 .. :try_end_27} :catchall_2c

    #@27
    .line 114
    .end local v0           #eHost:Lorg/apache/http/HttpHost;
    .end local v1           #entry:Landroid/net/http/IdleCache$Entry;
    .end local v2           #i:I
    :cond_27
    monitor-exit p0

    #@28
    return-object v3

    #@29
    .line 101
    .restart local v0       #eHost:Lorg/apache/http/HttpHost;
    .restart local v1       #entry:Landroid/net/http/IdleCache$Entry;
    .restart local v2       #i:I
    :cond_29
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_7

    #@2c
    .line 98
    .end local v0           #eHost:Lorg/apache/http/HttpHost;
    .end local v1           #entry:Landroid/net/http/IdleCache$Entry;
    .end local v2           #i:I
    :catchall_2c
    move-exception v4

    #@2d
    monitor-exit p0

    #@2e
    throw v4
.end method
