.class public Landroid/net/http/ErrorStrings;
.super Ljava/lang/Object;
.source "ErrorStrings.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Http"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getResource(I)I
    .registers 5
    .parameter "errorCode"

    #@0
    .prologue
    const v0, 0x10400df

    #@3
    .line 45
    packed-switch p0, :pswitch_data_5c

    #@6
    .line 95
    const-string v1, "Http"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Using generic message for unknown error code: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 96
    :goto_1e
    :pswitch_1e
    return v0

    #@1f
    .line 47
    :pswitch_1f
    const v0, 0x10400de

    #@22
    goto :goto_1e

    #@23
    .line 53
    :pswitch_23
    const v0, 0x10400e0

    #@26
    goto :goto_1e

    #@27
    .line 56
    :pswitch_27
    const v0, 0x10400e1

    #@2a
    goto :goto_1e

    #@2b
    .line 59
    :pswitch_2b
    const v0, 0x10400e2

    #@2e
    goto :goto_1e

    #@2f
    .line 62
    :pswitch_2f
    const v0, 0x10400e3

    #@32
    goto :goto_1e

    #@33
    .line 65
    :pswitch_33
    const v0, 0x10400e4

    #@36
    goto :goto_1e

    #@37
    .line 68
    :pswitch_37
    const v0, 0x10400e5

    #@3a
    goto :goto_1e

    #@3b
    .line 71
    :pswitch_3b
    const v0, 0x10400e6

    #@3e
    goto :goto_1e

    #@3f
    .line 74
    :pswitch_3f
    const v0, 0x10400e7

    #@42
    goto :goto_1e

    #@43
    .line 77
    :pswitch_43
    const v0, 0x1040008

    #@46
    goto :goto_1e

    #@47
    .line 80
    :pswitch_47
    const v0, 0x10400e8

    #@4a
    goto :goto_1e

    #@4b
    .line 83
    :pswitch_4b
    const v0, 0x1040007

    #@4e
    goto :goto_1e

    #@4f
    .line 86
    :pswitch_4f
    const v0, 0x10400e9

    #@52
    goto :goto_1e

    #@53
    .line 89
    :pswitch_53
    const v0, 0x10400ea

    #@56
    goto :goto_1e

    #@57
    .line 92
    :pswitch_57
    const v0, 0x10400eb

    #@5a
    goto :goto_1e

    #@5b
    .line 45
    nop

    #@5c
    :pswitch_data_5c
    .packed-switch -0xf
        :pswitch_57
        :pswitch_53
        :pswitch_4f
        :pswitch_4b
        :pswitch_47
        :pswitch_43
        :pswitch_3f
        :pswitch_3b
        :pswitch_37
        :pswitch_33
        :pswitch_2f
        :pswitch_2b
        :pswitch_27
        :pswitch_23
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method public static getString(ILandroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "errorCode"
    .parameter "context"

    #@0
    .prologue
    .line 37
    invoke-static {p0}, Landroid/net/http/ErrorStrings;->getResource(I)I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method
