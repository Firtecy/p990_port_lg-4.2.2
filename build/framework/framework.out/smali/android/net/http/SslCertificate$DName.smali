.class public Landroid/net/http/SslCertificate$DName;
.super Ljava/lang/Object;
.source "SslCertificate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/http/SslCertificate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DName"
.end annotation


# instance fields
.field private mCName:Ljava/lang/String;

.field private mDName:Ljava/lang/String;

.field private mOName:Ljava/lang/String;

.field private mUName:Ljava/lang/String;

.field final synthetic this$0:Landroid/net/http/SslCertificate;


# direct methods
.method public constructor <init>(Landroid/net/http/SslCertificate;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter "dName"

    #@0
    .prologue
    .line 366
    iput-object p1, p0, Landroid/net/http/SslCertificate$DName;->this$0:Landroid/net/http/SslCertificate;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 367
    if-eqz p2, :cond_4a

    #@7
    .line 368
    iput-object p2, p0, Landroid/net/http/SslCertificate$DName;->mDName:Ljava/lang/String;

    #@9
    .line 370
    :try_start_9
    new-instance v3, Lcom/android/org/bouncycastle/asn1/x509/X509Name;

    #@b
    invoke-direct {v3, p2}, Lcom/android/org/bouncycastle/asn1/x509/X509Name;-><init>(Ljava/lang/String;)V

    #@e
    .line 372
    .local v3, x509Name:Lcom/android/org/bouncycastle/asn1/x509/X509Name;
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/asn1/x509/X509Name;->getValues()Ljava/util/Vector;

    #@11
    move-result-object v2

    #@12
    .line 373
    .local v2, val:Ljava/util/Vector;
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/asn1/x509/X509Name;->getOIDs()Ljava/util/Vector;

    #@15
    move-result-object v1

    #@16
    .line 375
    .local v1, oid:Ljava/util/Vector;
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    :goto_17
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    #@1a
    move-result v4

    #@1b
    if-ge v0, v4, :cond_4a

    #@1d
    .line 376
    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@20
    move-result-object v4

    #@21
    sget-object v5, Lcom/android/org/bouncycastle/asn1/x509/X509Name;->CN:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_34

    #@29
    .line 377
    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@2c
    move-result-object v4

    #@2d
    check-cast v4, Ljava/lang/String;

    #@2f
    iput-object v4, p0, Landroid/net/http/SslCertificate$DName;->mCName:Ljava/lang/String;

    #@31
    .line 375
    :cond_31
    :goto_31
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_17

    #@34
    .line 381
    :cond_34
    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@37
    move-result-object v4

    #@38
    sget-object v5, Lcom/android/org/bouncycastle/asn1/x509/X509Name;->O:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_4b

    #@40
    .line 382
    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@43
    move-result-object v4

    #@44
    check-cast v4, Ljava/lang/String;

    #@46
    iput-object v4, p0, Landroid/net/http/SslCertificate$DName;->mOName:Ljava/lang/String;

    #@48
    goto :goto_31

    #@49
    .line 391
    .end local v0           #i:I
    .end local v1           #oid:Ljava/util/Vector;
    .end local v2           #val:Ljava/util/Vector;
    .end local v3           #x509Name:Lcom/android/org/bouncycastle/asn1/x509/X509Name;
    :catch_49
    move-exception v4

    #@4a
    .line 395
    :cond_4a
    return-void

    #@4b
    .line 386
    .restart local v0       #i:I
    .restart local v1       #oid:Ljava/util/Vector;
    .restart local v2       #val:Ljava/util/Vector;
    .restart local v3       #x509Name:Lcom/android/org/bouncycastle/asn1/x509/X509Name;
    :cond_4b
    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@4e
    move-result-object v4

    #@4f
    sget-object v5, Lcom/android/org/bouncycastle/asn1/x509/X509Name;->OU:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v4

    #@55
    if-eqz v4, :cond_31

    #@57
    .line 387
    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    #@5a
    move-result-object v4

    #@5b
    check-cast v4, Ljava/lang/String;

    #@5d
    iput-object v4, p0, Landroid/net/http/SslCertificate$DName;->mUName:Ljava/lang/String;
    :try_end_5f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_5f} :catch_49

    #@5f
    goto :goto_31
.end method


# virtual methods
.method public getCName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mCName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mCName:Ljava/lang/String;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    const-string v0, ""

    #@9
    goto :goto_6
.end method

.method public getDName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mDName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mDName:Ljava/lang/String;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    const-string v0, ""

    #@9
    goto :goto_6
.end method

.method public getOName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 415
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mOName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mOName:Ljava/lang/String;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    const-string v0, ""

    #@9
    goto :goto_6
.end method

.method public getUName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 422
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mUName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Landroid/net/http/SslCertificate$DName;->mUName:Ljava/lang/String;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    const-string v0, ""

    #@9
    goto :goto_6
.end method
