.class public Landroid/net/http/SslError;
.super Ljava/lang/Object;
.source "SslError.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field public static final SSL_DATE_INVALID:I = 0x4

.field public static final SSL_EXPIRED:I = 0x1

.field public static final SSL_IDMISMATCH:I = 0x2

.field public static final SSL_INVALID:I = 0x5

.field public static final SSL_MAX_ERROR:I = 0x6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SSL_NOTYETVALID:I = 0x0

.field public static final SSL_UNTRUSTED:I = 0x3


# instance fields
.field final mCertificate:Landroid/net/http/SslCertificate;

.field mErrors:I

.field final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 25
    const-class v0, Landroid/net/http/SslError;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/net/http/SslError;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(ILandroid/net/http/SslCertificate;)V
    .registers 4
    .parameter "error"
    .parameter "certificate"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 91
    const-string v0, ""

    #@2
    invoke-direct {p0, p1, p2, v0}, Landroid/net/http/SslError;-><init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@5
    .line 92
    return-void
.end method

.method public constructor <init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V
    .registers 5
    .parameter "error"
    .parameter "certificate"
    .parameter "url"

    #@0
    .prologue
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 114
    sget-boolean v0, Landroid/net/http/SslError;->$assertionsDisabled:Z

    #@5
    if-nez v0, :cond_f

    #@7
    if-nez p2, :cond_f

    #@9
    new-instance v0, Ljava/lang/AssertionError;

    #@b
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@e
    throw v0

    #@f
    .line 115
    :cond_f
    sget-boolean v0, Landroid/net/http/SslError;->$assertionsDisabled:Z

    #@11
    if-nez v0, :cond_1b

    #@13
    if-nez p3, :cond_1b

    #@15
    new-instance v0, Ljava/lang/AssertionError;

    #@17
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@1a
    throw v0

    #@1b
    .line 116
    :cond_1b
    invoke-virtual {p0, p1}, Landroid/net/http/SslError;->addError(I)Z

    #@1e
    .line 117
    iput-object p2, p0, Landroid/net/http/SslError;->mCertificate:Landroid/net/http/SslCertificate;

    #@20
    .line 118
    iput-object p3, p0, Landroid/net/http/SslError;->mUrl:Ljava/lang/String;

    #@22
    .line 119
    return-void
.end method

.method public constructor <init>(ILjava/security/cert/X509Certificate;)V
    .registers 4
    .parameter "error"
    .parameter "certificate"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 103
    const-string v0, ""

    #@2
    invoke-direct {p0, p1, p2, v0}, Landroid/net/http/SslError;-><init>(ILjava/security/cert/X509Certificate;Ljava/lang/String;)V

    #@5
    .line 104
    return-void
.end method

.method public constructor <init>(ILjava/security/cert/X509Certificate;Ljava/lang/String;)V
    .registers 5
    .parameter "error"
    .parameter "certificate"
    .parameter "url"

    #@0
    .prologue
    .line 129
    new-instance v0, Landroid/net/http/SslCertificate;

    #@2
    invoke-direct {v0, p2}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    #@5
    invoke-direct {p0, p1, v0, p3}, Landroid/net/http/SslError;-><init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@8
    .line 130
    return-void
.end method

.method public static SslErrorFromChromiumErrorCode(ILandroid/net/http/SslCertificate;Ljava/lang/String;)Landroid/net/http/SslError;
    .registers 5
    .parameter "error"
    .parameter "cert"
    .parameter "url"

    #@0
    .prologue
    const/16 v1, -0xc8

    #@2
    .line 143
    sget-boolean v0, Landroid/net/http/SslError;->$assertionsDisabled:Z

    #@4
    if-nez v0, :cond_12

    #@6
    const/16 v0, -0x12b

    #@8
    if-lt p0, v0, :cond_c

    #@a
    if-le p0, v1, :cond_12

    #@c
    :cond_c
    new-instance v0, Ljava/lang/AssertionError;

    #@e
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@11
    throw v0

    #@12
    .line 144
    :cond_12
    if-ne p0, v1, :cond_1b

    #@14
    .line 145
    new-instance v0, Landroid/net/http/SslError;

    #@16
    const/4 v1, 0x2

    #@17
    invoke-direct {v0, v1, p1, p2}, Landroid/net/http/SslError;-><init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@1a
    .line 151
    :goto_1a
    return-object v0

    #@1b
    .line 146
    :cond_1b
    const/16 v0, -0xc9

    #@1d
    if-ne p0, v0, :cond_26

    #@1f
    .line 147
    new-instance v0, Landroid/net/http/SslError;

    #@21
    const/4 v1, 0x4

    #@22
    invoke-direct {v0, v1, p1, p2}, Landroid/net/http/SslError;-><init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@25
    goto :goto_1a

    #@26
    .line 148
    :cond_26
    const/16 v0, -0xca

    #@28
    if-ne p0, v0, :cond_31

    #@2a
    .line 149
    new-instance v0, Landroid/net/http/SslError;

    #@2c
    const/4 v1, 0x3

    #@2d
    invoke-direct {v0, v1, p1, p2}, Landroid/net/http/SslError;-><init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@30
    goto :goto_1a

    #@31
    .line 151
    :cond_31
    new-instance v0, Landroid/net/http/SslError;

    #@33
    const/4 v1, 0x5

    #@34
    invoke-direct {v0, v1, p1, p2}, Landroid/net/http/SslError;-><init>(ILandroid/net/http/SslCertificate;Ljava/lang/String;)V

    #@37
    goto :goto_1a
.end method


# virtual methods
.method public addError(I)Z
    .registers 5
    .parameter "error"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 177
    if-ltz p1, :cond_10

    #@3
    const/4 v2, 0x6

    #@4
    if-ge p1, v2, :cond_10

    #@6
    move v0, v1

    #@7
    .line 178
    .local v0, rval:Z
    :goto_7
    if-eqz v0, :cond_f

    #@9
    .line 179
    iget v2, p0, Landroid/net/http/SslError;->mErrors:I

    #@b
    shl-int/2addr v1, p1

    #@c
    or-int/2addr v1, v2

    #@d
    iput v1, p0, Landroid/net/http/SslError;->mErrors:I

    #@f
    .line 182
    :cond_f
    return v0

    #@10
    .line 177
    .end local v0           #rval:Z
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_7
.end method

.method public getCertificate()Landroid/net/http/SslCertificate;
    .registers 2

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/net/http/SslError;->mCertificate:Landroid/net/http/SslCertificate;

    #@2
    return-object v0
.end method

.method public getPrimaryError()I
    .registers 4

    #@0
    .prologue
    .line 205
    iget v1, p0, Landroid/net/http/SslError;->mErrors:I

    #@2
    if-eqz v1, :cond_1c

    #@4
    .line 207
    const/4 v0, 0x5

    #@5
    .local v0, error:I
    :goto_5
    if-ltz v0, :cond_12

    #@7
    .line 208
    iget v1, p0, Landroid/net/http/SslError;->mErrors:I

    #@9
    const/4 v2, 0x1

    #@a
    shl-int/2addr v2, v0

    #@b
    and-int/2addr v1, v2

    #@c
    if-eqz v1, :cond_f

    #@e
    .line 216
    .end local v0           #error:I
    :goto_e
    return v0

    #@f
    .line 207
    .restart local v0       #error:I
    :cond_f
    add-int/lit8 v0, v0, -0x1

    #@11
    goto :goto_5

    #@12
    .line 213
    :cond_12
    sget-boolean v1, Landroid/net/http/SslError;->$assertionsDisabled:Z

    #@14
    if-nez v1, :cond_1c

    #@16
    new-instance v1, Ljava/lang/AssertionError;

    #@18
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@1b
    throw v1

    #@1c
    .line 216
    .end local v0           #error:I
    :cond_1c
    const/4 v0, -0x1

    #@1d
    goto :goto_e
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/net/http/SslError;->mUrl:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hasError(I)Z
    .registers 7
    .parameter "error"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 191
    if-ltz p1, :cond_13

    #@4
    const/4 v3, 0x6

    #@5
    if-ge p1, v3, :cond_13

    #@7
    move v0, v1

    #@8
    .line 192
    .local v0, rval:Z
    :goto_8
    if-eqz v0, :cond_12

    #@a
    .line 193
    iget v3, p0, Landroid/net/http/SslError;->mErrors:I

    #@c
    shl-int v4, v1, p1

    #@e
    and-int/2addr v3, v4

    #@f
    if-eqz v3, :cond_15

    #@11
    move v0, v1

    #@12
    .line 196
    :cond_12
    :goto_12
    return v0

    #@13
    .end local v0           #rval:Z
    :cond_13
    move v0, v2

    #@14
    .line 191
    goto :goto_8

    #@15
    .restart local v0       #rval:Z
    :cond_15
    move v0, v2

    #@16
    .line 193
    goto :goto_12
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "primary error: "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0}, Landroid/net/http/SslError;->getPrimaryError()I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v1, " certificate: "

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p0}, Landroid/net/http/SslError;->getCertificate()Landroid/net/http/SslCertificate;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, " on URL: "

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {p0}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    return-object v0
.end method
