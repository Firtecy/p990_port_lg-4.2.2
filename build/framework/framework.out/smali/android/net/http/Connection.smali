.class abstract Landroid/net/http/Connection;
.super Ljava/lang/Object;
.source "Connection.java"


# static fields
.field private static final DONE:I = 0x3

.field private static final DRAIN:I = 0x2

.field private static final HTTP_CONNECTION:Ljava/lang/String; = "http.connection"

.field private static final MAX_PIPE:I = 0x3

.field private static final MIN_PIPE:I = 0x2

.field private static final READ:I = 0x1

.field private static final RETRY_REQUEST_LIMIT:I = 0x2

.field private static final SEND:I = 0x0

.field static final SOCKET_TIMEOUT:I = 0xea60

.field private static STATE_CANCEL_REQUESTED:I

.field private static STATE_NORMAL:I

.field private static final states:[Ljava/lang/String;


# instance fields
.field private mActive:I

.field private mBuf:[B

.field private mCanPersist:Z

.field protected mCertificate:Landroid/net/http/SslCertificate;

.field mContext:Landroid/content/Context;

.field mHost:Lorg/apache/http/HttpHost;

.field protected mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

.field private mHttpContext:Lorg/apache/http/protocol/HttpContext;

.field mRequestFeeder:Landroid/net/http/RequestFeeder;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 54
    const/4 v0, 0x4

    #@3
    new-array v0, v0, [Ljava/lang/String;

    #@5
    const-string v1, "SEND"

    #@7
    aput-object v1, v0, v3

    #@9
    const-string v1, "READ"

    #@b
    aput-object v1, v0, v4

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "DRAIN"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "DONE"

    #@15
    aput-object v2, v0, v1

    #@17
    sput-object v0, Landroid/net/http/Connection;->states:[Ljava/lang/String;

    #@19
    .line 82
    sput v3, Landroid/net/http/Connection;->STATE_NORMAL:I

    #@1b
    .line 83
    sput v4, Landroid/net/http/Connection;->STATE_CANCEL_REQUESTED:I

    #@1d
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V
    .registers 6
    .parameter "context"
    .parameter "host"
    .parameter "requestFeeder"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 106
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 59
    iput-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@6
    .line 67
    iput-object v1, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@8
    .line 84
    sget v0, Landroid/net/http/Connection;->STATE_NORMAL:I

    #@a
    iput v0, p0, Landroid/net/http/Connection;->mActive:I

    #@c
    .line 107
    iput-object p1, p0, Landroid/net/http/Connection;->mContext:Landroid/content/Context;

    #@e
    .line 108
    iput-object p2, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@10
    .line 109
    iput-object p3, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@12
    .line 111
    const/4 v0, 0x0

    #@13
    iput-boolean v0, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@15
    .line 112
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    #@17
    invoke-direct {v0, v1}, Lorg/apache/http/protocol/BasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    #@1a
    iput-object v0, p0, Landroid/net/http/Connection;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@1c
    .line 113
    return-void
.end method

.method private clearPipe(Ljava/util/LinkedList;)Z
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/net/http/Request;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 329
    .local p1, pipe:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    const/4 v0, 0x1

    #@1
    .line 332
    .local v0, empty:Z
    iget-object v3, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@3
    monitor-enter v3

    #@4
    .line 334
    :goto_4
    :try_start_4
    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_17

    #@a
    .line 335
    invoke-virtual {p1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/net/http/Request;

    #@10
    .line 338
    .local v1, tReq:Landroid/net/http/Request;
    iget-object v2, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@12
    invoke-interface {v2, v1}, Landroid/net/http/RequestFeeder;->requeueRequest(Landroid/net/http/Request;)V

    #@15
    .line 339
    const/4 v0, 0x0

    #@16
    goto :goto_4

    #@17
    .line 341
    .end local v1           #tReq:Landroid/net/http/Request;
    :cond_17
    if-eqz v0, :cond_24

    #@19
    iget-object v2, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@1b
    iget-object v4, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@1d
    invoke-interface {v2, v4}, Landroid/net/http/RequestFeeder;->haveRequest(Lorg/apache/http/HttpHost;)Z

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_26

    #@23
    const/4 v0, 0x1

    #@24
    .line 342
    :cond_24
    :goto_24
    monitor-exit v3

    #@25
    .line 343
    return v0

    #@26
    .line 341
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_24

    #@28
    .line 342
    :catchall_28
    move-exception v2

    #@29
    monitor-exit v3
    :try_end_2a
    .catchall {:try_start_4 .. :try_end_2a} :catchall_28

    #@2a
    throw v2
.end method

.method static getConnection(Landroid/content/Context;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)Landroid/net/http/Connection;
    .registers 6
    .parameter "context"
    .parameter "host"
    .parameter "proxy"
    .parameter "requestFeeder"

    #@0
    .prologue
    .line 127
    invoke-virtual {p1}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "http"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 128
    new-instance v0, Landroid/net/http/HttpConnection;

    #@e
    invoke-direct {v0, p0, p1, p3}, Landroid/net/http/HttpConnection;-><init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V

    #@11
    .line 132
    :goto_11
    return-object v0

    #@12
    :cond_12
    new-instance v0, Landroid/net/http/HttpsConnection;

    #@14
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/net/http/HttpsConnection;-><init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V

    #@17
    goto :goto_11
.end method

.method private httpFailure(Landroid/net/http/Request;ILjava/lang/Exception;)Z
    .registers 9
    .parameter "req"
    .parameter "errorId"
    .parameter "e"

    #@0
    .prologue
    .line 429
    const/4 v2, 0x1

    #@1
    .line 436
    .local v2, ret:Z
    iget v3, p1, Landroid/net/http/Request;->mFailCount:I

    #@3
    add-int/lit8 v3, v3, 0x1

    #@5
    iput v3, p1, Landroid/net/http/Request;->mFailCount:I

    #@7
    const/4 v4, 0x2

    #@8
    if-lt v3, v4, :cond_1b

    #@a
    .line 437
    const/4 v2, 0x0

    #@b
    .line 439
    if-gez p2, :cond_26

    #@d
    .line 440
    iget-object v3, p0, Landroid/net/http/Connection;->mContext:Landroid/content/Context;

    #@f
    invoke-static {p2, v3}, Landroid/net/http/ErrorStrings;->getString(ILandroid/content/Context;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 445
    .local v1, error:Ljava/lang/String;
    :goto_13
    iget-object v3, p1, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@15
    invoke-interface {v3, p2, v1}, Landroid/net/http/EventHandler;->error(ILjava/lang/String;)V

    #@18
    .line 446
    invoke-virtual {p1}, Landroid/net/http/Request;->complete()V

    #@1b
    .line 449
    .end local v1           #error:Ljava/lang/String;
    :cond_1b
    invoke-virtual {p0}, Landroid/net/http/Connection;->closeConnection()V

    #@1e
    .line 450
    iget-object v3, p0, Landroid/net/http/Connection;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@20
    const-string v4, "http.connection"

    #@22
    invoke-interface {v3, v4}, Lorg/apache/http/protocol/HttpContext;->removeAttribute(Ljava/lang/String;)Ljava/lang/Object;

    #@25
    .line 452
    return v2

    #@26
    .line 442
    :cond_26
    invoke-virtual {p3}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    #@29
    move-result-object v0

    #@2a
    .line 443
    .local v0, cause:Ljava/lang/Throwable;
    if-eqz v0, :cond_31

    #@2c
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .restart local v1       #error:Ljava/lang/String;
    :goto_30
    goto :goto_13

    #@31
    .end local v1           #error:Ljava/lang/String;
    :cond_31
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    goto :goto_30
.end method

.method private keepAlive(Lorg/apache/http/HttpEntity;Lorg/apache/http/ProtocolVersion;ILorg/apache/http/protocol/HttpContext;)Z
    .registers 12
    .parameter "entity"
    .parameter "ver"
    .parameter "connType"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 465
    const-string v3, "http.connection"

    #@4
    invoke-interface {p4, v3}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lorg/apache/http/HttpConnection;

    #@a
    .line 468
    .local v0, conn:Lorg/apache/http/HttpConnection;
    if-eqz v0, :cond_13

    #@c
    invoke-interface {v0}, Lorg/apache/http/HttpConnection;->isOpen()Z

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_13

    #@12
    .line 488
    :cond_12
    :goto_12
    return v2

    #@13
    .line 472
    :cond_13
    if-eqz p1, :cond_2d

    #@15
    .line 473
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    #@18
    move-result-wide v3

    #@19
    const-wide/16 v5, 0x0

    #@1b
    cmp-long v3, v3, v5

    #@1d
    if-gez v3, :cond_2d

    #@1f
    .line 474
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->isChunked()Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_12

    #@25
    sget-object v3, Lorg/apache/http/HttpVersion;->HTTP_1_0:Lorg/apache/http/HttpVersion;

    #@27
    invoke-virtual {p2, v3}, Lorg/apache/http/ProtocolVersion;->lessEquals(Lorg/apache/http/ProtocolVersion;)Z

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_12

    #@2d
    .line 482
    :cond_2d
    if-eq p3, v1, :cond_12

    #@2f
    .line 484
    const/4 v3, 0x2

    #@30
    if-ne p3, v3, :cond_34

    #@32
    move v2, v1

    #@33
    .line 485
    goto :goto_12

    #@34
    .line 488
    :cond_34
    sget-object v3, Lorg/apache/http/HttpVersion;->HTTP_1_0:Lorg/apache/http/HttpVersion;

    #@36
    invoke-virtual {p2, v3}, Lorg/apache/http/ProtocolVersion;->lessEquals(Lorg/apache/http/ProtocolVersion;)Z

    #@39
    move-result v3

    #@3a
    if-nez v3, :cond_3e

    #@3c
    :goto_3c
    move v2, v1

    #@3d
    goto :goto_12

    #@3e
    :cond_3e
    move v1, v2

    #@3f
    goto :goto_3c
.end method

.method private openHttpConnection(Landroid/net/http/Request;)Z
    .registers 13
    .parameter "req"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v10, 0x2

    #@3
    .line 351
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v3

    #@7
    .line 352
    .local v3, now:J
    const/4 v1, 0x0

    #@8
    .line 353
    .local v1, error:I
    const/4 v2, 0x0

    #@9
    .line 357
    .local v2, exception:Ljava/lang/Exception;
    const/4 v7, 0x0

    #@a
    :try_start_a
    iput-object v7, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@c
    .line 358
    invoke-virtual {p0, p1}, Landroid/net/http/Connection;->openConnection(Landroid/net/http/Request;)Landroid/net/http/AndroidHttpClientConnection;

    #@f
    move-result-object v7

    #@10
    iput-object v7, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@12
    .line 359
    iget-object v7, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@14
    if-eqz v7, :cond_2b

    #@16
    .line 360
    iget-object v7, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@18
    const v8, 0xea60

    #@1b
    invoke-virtual {v7, v8}, Landroid/net/http/AndroidHttpClientConnection;->setSocketTimeout(I)V

    #@1e
    .line 361
    iget-object v7, p0, Landroid/net/http/Connection;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@20
    const-string v8, "http.connection"

    #@22
    iget-object v9, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@24
    invoke-interface {v7, v8, v9}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    #@27
    .line 404
    :goto_27
    if-nez v1, :cond_48

    #@29
    move v6, v5

    #@2a
    .line 414
    :goto_2a
    return v6

    #@2b
    .line 367
    :cond_2b
    const/4 v7, 0x2

    #@2c
    iput v7, p1, Landroid/net/http/Request;->mFailCount:I
    :try_end_2e
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_2e} :catch_2f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_2e} :catch_33
    .catch Landroid/net/http/SSLConnectionClosedByUserException; {:try_start_a .. :try_end_2e} :catch_39
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_a .. :try_end_2e} :catch_3d
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_2e} :catch_44

    #@2e
    goto :goto_2a

    #@2f
    .line 370
    :catch_2f
    move-exception v0

    #@30
    .line 372
    .local v0, e:Ljava/net/UnknownHostException;
    const/4 v1, -0x2

    #@31
    .line 373
    move-object v2, v0

    #@32
    .line 396
    goto :goto_27

    #@33
    .line 374
    .end local v0           #e:Ljava/net/UnknownHostException;
    :catch_33
    move-exception v0

    #@34
    .line 376
    .local v0, e:Ljava/lang/IllegalArgumentException;
    const/4 v1, -0x6

    #@35
    .line 377
    iput v10, p1, Landroid/net/http/Request;->mFailCount:I

    #@37
    .line 378
    move-object v2, v0

    #@38
    .line 396
    goto :goto_27

    #@39
    .line 379
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :catch_39
    move-exception v0

    #@3a
    .line 382
    .local v0, e:Landroid/net/http/SSLConnectionClosedByUserException;
    iput v10, p1, Landroid/net/http/Request;->mFailCount:I

    #@3c
    goto :goto_2a

    #@3d
    .line 385
    .end local v0           #e:Landroid/net/http/SSLConnectionClosedByUserException;
    :catch_3d
    move-exception v0

    #@3e
    .line 388
    .local v0, e:Ljavax/net/ssl/SSLHandshakeException;
    iput v10, p1, Landroid/net/http/Request;->mFailCount:I

    #@40
    .line 391
    const/16 v1, -0xb

    #@42
    .line 392
    move-object v2, v0

    #@43
    .line 396
    goto :goto_27

    #@44
    .line 393
    .end local v0           #e:Ljavax/net/ssl/SSLHandshakeException;
    :catch_44
    move-exception v0

    #@45
    .line 394
    .local v0, e:Ljava/io/IOException;
    const/4 v1, -0x6

    #@46
    .line 395
    move-object v2, v0

    #@47
    goto :goto_27

    #@48
    .line 407
    .end local v0           #e:Ljava/io/IOException;
    :cond_48
    iget v7, p1, Landroid/net/http/Request;->mFailCount:I

    #@4a
    if-ge v7, v10, :cond_5b

    #@4c
    .line 409
    iget-object v7, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@4e
    invoke-interface {v7, p1}, Landroid/net/http/RequestFeeder;->requeueRequest(Landroid/net/http/Request;)V

    #@51
    .line 410
    iget v7, p1, Landroid/net/http/Request;->mFailCount:I

    #@53
    add-int/lit8 v7, v7, 0x1

    #@55
    iput v7, p1, Landroid/net/http/Request;->mFailCount:I

    #@57
    .line 414
    :goto_57
    if-nez v1, :cond_5f

    #@59
    :goto_59
    move v6, v5

    #@5a
    goto :goto_2a

    #@5b
    .line 412
    :cond_5b
    invoke-direct {p0, p1, v1, v2}, Landroid/net/http/Connection;->httpFailure(Landroid/net/http/Request;ILjava/lang/Exception;)Z

    #@5e
    goto :goto_57

    #@5f
    :cond_5f
    move v5, v6

    #@60
    .line 414
    goto :goto_59
.end method


# virtual methods
.method cancel()V
    .registers 2

    #@0
    .prologue
    .line 148
    sget v0, Landroid/net/http/Connection;->STATE_CANCEL_REQUESTED:I

    #@2
    iput v0, p0, Landroid/net/http/Connection;->mActive:I

    #@4
    .line 149
    invoke-virtual {p0}, Landroid/net/http/Connection;->closeConnection()V

    #@7
    .line 152
    return-void
.end method

.method abstract closeConnection()V
.end method

.method getBuf()[B
    .registers 2

    #@0
    .prologue
    .line 517
    iget-object v0, p0, Landroid/net/http/Connection;->mBuf:[B

    #@2
    if-nez v0, :cond_a

    #@4
    const/16 v0, 0x2000

    #@6
    new-array v0, v0, [B

    #@8
    iput-object v0, p0, Landroid/net/http/Connection;->mBuf:[B

    #@a
    .line 518
    :cond_a
    iget-object v0, p0, Landroid/net/http/Connection;->mBuf:[B

    #@c
    return-object v0
.end method

.method getCanPersist()Z
    .registers 2

    #@0
    .prologue
    .line 500
    iget-boolean v0, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@2
    return v0
.end method

.method getCertificate()Landroid/net/http/SslCertificate;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@2
    return-object v0
.end method

.method getHost()Lorg/apache/http/HttpHost;
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@2
    return-object v0
.end method

.method getHttpContext()Lorg/apache/http/protocol/HttpContext;
    .registers 2

    #@0
    .prologue
    .line 456
    iget-object v0, p0, Landroid/net/http/Connection;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@2
    return-object v0
.end method

.method abstract getScheme()Ljava/lang/String;
.end method

.method abstract openConnection(Landroid/net/http/Request;)Landroid/net/http/AndroidHttpClientConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method processRequests(Landroid/net/http/Request;)V
    .registers 16
    .parameter "firstRequest"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v11, 0x0

    #@2
    .line 159
    const/4 v8, 0x0

    #@3
    .line 161
    .local v8, req:Landroid/net/http/Request;
    const/4 v2, 0x0

    #@4
    .line 162
    .local v2, error:I
    const/4 v3, 0x0

    #@5
    .line 164
    .local v3, exception:Ljava/lang/Exception;
    new-instance v6, Ljava/util/LinkedList;

    #@7
    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    #@a
    .line 166
    .local v6, pipe:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/net/http/Request;>;"
    const/4 v5, 0x2

    #@b
    .local v5, minPipe:I
    const/4 v4, 0x3

    #@c
    .line 167
    .local v4, maxPipe:I
    const/4 v9, 0x0

    #@d
    .line 169
    .local v9, state:I
    :cond_d
    :goto_d
    if-eq v9, v10, :cond_105

    #@f
    .line 176
    iget v12, p0, Landroid/net/http/Connection;->mActive:I

    #@11
    sget v13, Landroid/net/http/Connection;->STATE_CANCEL_REQUESTED:I

    #@13
    if-ne v12, v13, :cond_1e

    #@15
    .line 178
    const-wide/16 v12, 0x64

    #@17
    :try_start_17
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1a
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_1a} :catch_102

    #@1a
    .line 180
    :goto_1a
    sget v12, Landroid/net/http/Connection;->STATE_NORMAL:I

    #@1c
    iput v12, p0, Landroid/net/http/Connection;->mActive:I

    #@1e
    .line 183
    :cond_1e
    packed-switch v9, :pswitch_data_106

    #@21
    goto :goto_d

    #@22
    .line 185
    :pswitch_22
    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    #@25
    move-result v12

    #@26
    if-ne v12, v4, :cond_2a

    #@28
    .line 186
    const/4 v9, 0x1

    #@29
    .line 187
    goto :goto_d

    #@2a
    .line 190
    :cond_2a
    if-nez p1, :cond_38

    #@2c
    .line 191
    iget-object v12, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@2e
    iget-object v13, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@30
    invoke-interface {v12, v13}, Landroid/net/http/RequestFeeder;->getRequest(Lorg/apache/http/HttpHost;)Landroid/net/http/Request;

    #@33
    move-result-object v8

    #@34
    .line 196
    :goto_34
    if-nez v8, :cond_3b

    #@36
    .line 197
    const/4 v9, 0x2

    #@37
    .line 198
    goto :goto_d

    #@38
    .line 193
    :cond_38
    move-object v8, p1

    #@39
    .line 194
    const/4 p1, 0x0

    #@3a
    goto :goto_34

    #@3b
    .line 200
    :cond_3b
    invoke-virtual {v8, p0}, Landroid/net/http/Request;->setConnection(Landroid/net/http/Connection;)V

    #@3e
    .line 203
    iget-boolean v12, v8, Landroid/net/http/Request;->mCancelled:Z

    #@40
    if-eqz v12, :cond_46

    #@42
    .line 207
    invoke-virtual {v8}, Landroid/net/http/Request;->complete()V

    #@45
    goto :goto_d

    #@46
    .line 211
    :cond_46
    iget-object v12, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@48
    if-eqz v12, :cond_52

    #@4a
    iget-object v12, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@4c
    invoke-virtual {v12}, Landroid/net/http/AndroidHttpClientConnection;->isOpen()Z

    #@4f
    move-result v12

    #@50
    if-nez v12, :cond_5a

    #@52
    .line 219
    :cond_52
    invoke-direct {p0, v8}, Landroid/net/http/Connection;->openHttpConnection(Landroid/net/http/Request;)Z

    #@55
    move-result v12

    #@56
    if-nez v12, :cond_5a

    #@58
    .line 220
    const/4 v9, 0x3

    #@59
    .line 221
    goto :goto_d

    #@5a
    .line 229
    :cond_5a
    iget-object v12, v8, Landroid/net/http/Request;->mEventHandler:Landroid/net/http/EventHandler;

    #@5c
    iget-object v13, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@5e
    invoke-interface {v12, v13}, Landroid/net/http/EventHandler;->certificate(Landroid/net/http/SslCertificate;)V

    #@61
    .line 235
    :try_start_61
    iget-object v12, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@63
    invoke-virtual {v8, v12}, Landroid/net/http/Request;->sendRequest(Landroid/net/http/AndroidHttpClientConnection;)V
    :try_end_66
    .catch Lorg/apache/http/HttpException; {:try_start_61 .. :try_end_66} :catch_80
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_66} :catch_84
    .catch Ljava/lang/IllegalStateException; {:try_start_61 .. :try_end_66} :catch_88

    #@66
    .line 246
    :goto_66
    if-eqz v3, :cond_8e

    #@68
    .line 247
    invoke-direct {p0, v8, v2, v3}, Landroid/net/http/Connection;->httpFailure(Landroid/net/http/Request;ILjava/lang/Exception;)Z

    #@6b
    move-result v12

    #@6c
    if-eqz v12, :cond_75

    #@6e
    iget-boolean v12, v8, Landroid/net/http/Request;->mCancelled:Z

    #@70
    if-nez v12, :cond_75

    #@72
    .line 251
    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    #@75
    .line 253
    :cond_75
    const/4 v3, 0x0

    #@76
    .line 254
    invoke-direct {p0, v6}, Landroid/net/http/Connection;->clearPipe(Ljava/util/LinkedList;)Z

    #@79
    move-result v12

    #@7a
    if-eqz v12, :cond_8c

    #@7c
    move v9, v10

    #@7d
    .line 255
    :goto_7d
    const/4 v4, 0x1

    #@7e
    move v5, v4

    #@7f
    .line 256
    goto :goto_d

    #@80
    .line 236
    :catch_80
    move-exception v0

    #@81
    .line 237
    .local v0, e:Lorg/apache/http/HttpException;
    move-object v3, v0

    #@82
    .line 238
    const/4 v2, -0x1

    #@83
    .line 245
    goto :goto_66

    #@84
    .line 239
    .end local v0           #e:Lorg/apache/http/HttpException;
    :catch_84
    move-exception v0

    #@85
    .line 240
    .local v0, e:Ljava/io/IOException;
    move-object v3, v0

    #@86
    .line 241
    const/4 v2, -0x7

    #@87
    .line 245
    goto :goto_66

    #@88
    .line 242
    .end local v0           #e:Ljava/io/IOException;
    :catch_88
    move-exception v0

    #@89
    .line 243
    .local v0, e:Ljava/lang/IllegalStateException;
    move-object v3, v0

    #@8a
    .line 244
    const/4 v2, -0x7

    #@8b
    goto :goto_66

    #@8c
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :cond_8c
    move v9, v11

    #@8d
    .line 254
    goto :goto_7d

    #@8e
    .line 259
    :cond_8e
    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    #@91
    .line 260
    iget-boolean v12, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@93
    if-nez v12, :cond_d

    #@95
    const/4 v9, 0x1

    #@96
    goto/16 :goto_d

    #@98
    .line 266
    :pswitch_98
    iget-object v12, p0, Landroid/net/http/Connection;->mRequestFeeder:Landroid/net/http/RequestFeeder;

    #@9a
    iget-object v13, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@9c
    invoke-interface {v12, v13}, Landroid/net/http/RequestFeeder;->haveRequest(Lorg/apache/http/HttpHost;)Z

    #@9f
    move-result v12

    #@a0
    if-nez v12, :cond_b5

    #@a2
    const/4 v1, 0x1

    #@a3
    .line 267
    .local v1, empty:Z
    :goto_a3
    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    #@a6
    move-result v7

    #@a7
    .line 268
    .local v7, pipeSize:I
    const/4 v12, 0x2

    #@a8
    if-eq v9, v12, :cond_b7

    #@aa
    if-ge v7, v5, :cond_b7

    #@ac
    if-nez v1, :cond_b7

    #@ae
    iget-boolean v12, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@b0
    if-eqz v12, :cond_b7

    #@b2
    .line 270
    const/4 v9, 0x0

    #@b3
    .line 271
    goto/16 :goto_d

    #@b5
    .end local v1           #empty:Z
    .end local v7           #pipeSize:I
    :cond_b5
    move v1, v11

    #@b6
    .line 266
    goto :goto_a3

    #@b7
    .line 272
    .restart local v1       #empty:Z
    .restart local v7       #pipeSize:I
    :cond_b7
    if-nez v7, :cond_c0

    #@b9
    .line 274
    if-eqz v1, :cond_be

    #@bb
    move v9, v10

    #@bc
    .line 275
    :goto_bc
    goto/16 :goto_d

    #@be
    :cond_be
    move v9, v11

    #@bf
    .line 274
    goto :goto_bc

    #@c0
    .line 278
    :cond_c0
    invoke-virtual {v6}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@c3
    move-result-object v8

    #@c4
    .end local v8           #req:Landroid/net/http/Request;
    check-cast v8, Landroid/net/http/Request;

    #@c6
    .line 283
    .restart local v8       #req:Landroid/net/http/Request;
    :try_start_c6
    iget-object v12, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@c8
    invoke-virtual {v8, v12}, Landroid/net/http/Request;->readResponse(Landroid/net/http/AndroidHttpClientConnection;)V
    :try_end_cb
    .catch Lorg/apache/http/ParseException; {:try_start_c6 .. :try_end_cb} :catch_f6
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_cb} :catch_fa
    .catch Ljava/lang/IllegalStateException; {:try_start_c6 .. :try_end_cb} :catch_fe

    #@cb
    .line 294
    :goto_cb
    if-eqz v3, :cond_e0

    #@cd
    .line 295
    invoke-direct {p0, v8, v2, v3}, Landroid/net/http/Connection;->httpFailure(Landroid/net/http/Request;ILjava/lang/Exception;)Z

    #@d0
    move-result v12

    #@d1
    if-eqz v12, :cond_dd

    #@d3
    iget-boolean v12, v8, Landroid/net/http/Request;->mCancelled:Z

    #@d5
    if-nez v12, :cond_dd

    #@d7
    .line 299
    invoke-virtual {v8}, Landroid/net/http/Request;->reset()V

    #@da
    .line 300
    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    #@dd
    .line 302
    :cond_dd
    const/4 v3, 0x0

    #@de
    .line 303
    iput-boolean v11, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@e0
    .line 305
    :cond_e0
    iget-boolean v12, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@e2
    if-nez v12, :cond_d

    #@e4
    .line 310
    invoke-virtual {p0}, Landroid/net/http/Connection;->closeConnection()V

    #@e7
    .line 312
    iget-object v12, p0, Landroid/net/http/Connection;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@e9
    const-string v13, "http.connection"

    #@eb
    invoke-interface {v12, v13}, Lorg/apache/http/protocol/HttpContext;->removeAttribute(Ljava/lang/String;)Ljava/lang/Object;

    #@ee
    .line 313
    invoke-direct {p0, v6}, Landroid/net/http/Connection;->clearPipe(Ljava/util/LinkedList;)Z

    #@f1
    .line 314
    const/4 v4, 0x1

    #@f2
    move v5, v4

    #@f3
    .line 315
    const/4 v9, 0x0

    #@f4
    goto/16 :goto_d

    #@f6
    .line 284
    :catch_f6
    move-exception v0

    #@f7
    .line 285
    .local v0, e:Lorg/apache/http/ParseException;
    move-object v3, v0

    #@f8
    .line 286
    const/4 v2, -0x7

    #@f9
    .line 293
    goto :goto_cb

    #@fa
    .line 287
    .end local v0           #e:Lorg/apache/http/ParseException;
    :catch_fa
    move-exception v0

    #@fb
    .line 288
    .local v0, e:Ljava/io/IOException;
    move-object v3, v0

    #@fc
    .line 289
    const/4 v2, -0x7

    #@fd
    .line 293
    goto :goto_cb

    #@fe
    .line 290
    .end local v0           #e:Ljava/io/IOException;
    :catch_fe
    move-exception v0

    #@ff
    .line 291
    .local v0, e:Ljava/lang/IllegalStateException;
    move-object v3, v0

    #@100
    .line 292
    const/4 v2, -0x7

    #@101
    goto :goto_cb

    #@102
    .line 179
    .end local v0           #e:Ljava/lang/IllegalStateException;
    .end local v1           #empty:Z
    .end local v7           #pipeSize:I
    :catch_102
    move-exception v12

    #@103
    goto/16 :goto_1a

    #@105
    .line 321
    :cond_105
    return-void

    #@106
    .line 183
    :pswitch_data_106
    .packed-switch 0x0
        :pswitch_22
        :pswitch_98
        :pswitch_98
    .end packed-switch
.end method

.method setCanPersist(Lorg/apache/http/HttpEntity;Lorg/apache/http/ProtocolVersion;I)V
    .registers 5
    .parameter "entity"
    .parameter "ver"
    .parameter "connType"

    #@0
    .prologue
    .line 492
    iget-object v0, p0, Landroid/net/http/Connection;->mHttpContext:Lorg/apache/http/protocol/HttpContext;

    #@2
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/net/http/Connection;->keepAlive(Lorg/apache/http/HttpEntity;Lorg/apache/http/ProtocolVersion;ILorg/apache/http/protocol/HttpContext;)Z

    #@5
    move-result v0

    #@6
    iput-boolean v0, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@8
    .line 493
    return-void
.end method

.method setCanPersist(Z)V
    .registers 2
    .parameter "canPersist"

    #@0
    .prologue
    .line 496
    iput-boolean p1, p0, Landroid/net/http/Connection;->mCanPersist:Z

    #@2
    .line 497
    return-void
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 513
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@3
    invoke-virtual {v0}, Lorg/apache/http/HttpHost;->toString()Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result-object v0

    #@7
    monitor-exit p0

    #@8
    return-object v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method
