.class public Landroid/net/http/X509TrustManagerExtensions;
.super Ljava/lang/Object;
.source "X509TrustManagerExtensions.java"


# instance fields
.field mDelegate:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;


# direct methods
.method public constructor <init>(Ljavax/net/ssl/X509TrustManager;)V
    .registers 4
    .parameter "tm"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    instance-of v0, p1, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 46
    check-cast p1, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@9
    .end local p1
    iput-object p1, p0, Landroid/net/http/X509TrustManagerExtensions;->mDelegate:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@b
    .line 50
    return-void

    #@c
    .line 48
    .restart local p1
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v1, "tm is not a supported type of X509TrustManager"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0
.end method


# virtual methods
.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .parameter "chain"
    .parameter "authType"
    .parameter "host"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/security/cert/X509Certificate;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Landroid/net/http/X509TrustManagerExtensions;->mDelegate:Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/harmony/xnet/provider/jsse/TrustManagerImpl;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
