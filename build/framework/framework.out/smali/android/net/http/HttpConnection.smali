.class Landroid/net/http/HttpConnection;
.super Landroid/net/http/Connection;
.source "HttpConnection.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V
    .registers 4
    .parameter "context"
    .parameter "host"
    .parameter "requestFeeder"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/net/http/Connection;-><init>(Landroid/content/Context;Lorg/apache/http/HttpHost;Landroid/net/http/RequestFeeder;)V

    #@3
    .line 40
    return-void
.end method


# virtual methods
.method closeConnection()V
    .registers 3

    #@0
    .prologue
    .line 74
    :try_start_0
    iget-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@2
    if-eqz v1, :cond_11

    #@4
    iget-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@6
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClientConnection;->isOpen()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 75
    iget-object v1, p0, Landroid/net/http/Connection;->mHttpClientConnection:Landroid/net/http/AndroidHttpClientConnection;

    #@e
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClientConnection;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 83
    :cond_11
    :goto_11
    return-void

    #@12
    .line 77
    :catch_12
    move-exception v0

    #@13
    .line 81
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@16
    goto :goto_11
.end method

.method getScheme()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 93
    const-string v0, "http"

    #@2
    return-object v0
.end method

.method openConnection(Landroid/net/http/Request;)Landroid/net/http/AndroidHttpClientConnection;
    .registers 8
    .parameter "req"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/net/http/Request;->getEventHandler()Landroid/net/http/EventHandler;

    #@3
    move-result-object v1

    #@4
    .line 53
    .local v1, eventHandler:Landroid/net/http/EventHandler;
    const/4 v4, 0x0

    #@5
    iput-object v4, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@7
    .line 54
    iget-object v4, p0, Landroid/net/http/Connection;->mCertificate:Landroid/net/http/SslCertificate;

    #@9
    invoke-interface {v1, v4}, Landroid/net/http/EventHandler;->certificate(Landroid/net/http/SslCertificate;)V

    #@c
    .line 56
    new-instance v0, Landroid/net/http/AndroidHttpClientConnection;

    #@e
    invoke-direct {v0}, Landroid/net/http/AndroidHttpClientConnection;-><init>()V

    #@11
    .line 57
    .local v0, conn:Landroid/net/http/AndroidHttpClientConnection;
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    #@13
    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    #@16
    .line 58
    .local v2, params:Lorg/apache/http/params/BasicHttpParams;
    new-instance v3, Ljava/net/Socket;

    #@18
    iget-object v4, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@1a
    invoke-virtual {v4}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    iget-object v5, p0, Landroid/net/http/Connection;->mHost:Lorg/apache/http/HttpHost;

    #@20
    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getPort()I

    #@23
    move-result v5

    #@24
    invoke-direct {v3, v4, v5}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    #@27
    .line 59
    .local v3, sock:Ljava/net/Socket;
    const-string v4, "http.socket.buffer-size"

    #@29
    const/16 v5, 0x2000

    #@2b
    invoke-virtual {v2, v4, v5}, Lorg/apache/http/params/BasicHttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    #@2e
    .line 60
    invoke-virtual {v0, v3, v2}, Landroid/net/http/AndroidHttpClientConnection;->bind(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V

    #@31
    .line 61
    return-object v0
.end method

.method restartConnection(Z)V
    .registers 2
    .parameter "abort"

    #@0
    .prologue
    .line 90
    return-void
.end method
