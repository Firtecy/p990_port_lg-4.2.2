.class public Landroid/net/http/HttpAuthHeader;
.super Ljava/lang/Object;
.source "HttpAuthHeader.java"


# static fields
.field private static final ALGORITHM_TOKEN:Ljava/lang/String; = "algorithm"

.field public static final BASIC:I = 0x1

.field public static final BASIC_TOKEN:Ljava/lang/String; = "Basic"

.field public static final DIGEST:I = 0x2

.field public static final DIGEST_TOKEN:Ljava/lang/String; = "Digest"

.field private static final NONCE_TOKEN:Ljava/lang/String; = "nonce"

.field private static final OPAQUE_TOKEN:Ljava/lang/String; = "opaque"

.field private static final QOP_TOKEN:Ljava/lang/String; = "qop"

.field private static final REALM_TOKEN:Ljava/lang/String; = "realm"

.field private static final STALE_TOKEN:Ljava/lang/String; = "stale"

.field public static final UNKNOWN:I


# instance fields
.field private mAlgorithm:Ljava/lang/String;

.field private mIsProxy:Z

.field private mNonce:Ljava/lang/String;

.field private mOpaque:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mQop:Ljava/lang/String;

.field private mRealm:Ljava/lang/String;

.field private mScheme:I

.field private mStale:Z

.field private mUsername:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "header"

    #@0
    .prologue
    .line 116
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 117
    if-eqz p1, :cond_8

    #@5
    .line 118
    invoke-direct {p0, p1}, Landroid/net/http/HttpAuthHeader;->parseHeader(Ljava/lang/String;)V

    #@8
    .line 120
    :cond_8
    return-void
.end method

.method private parseHeader(Ljava/lang/String;)V
    .registers 4
    .parameter "header"

    #@0
    .prologue
    .line 265
    if-eqz p1, :cond_f

    #@2
    .line 266
    invoke-direct {p0, p1}, Landroid/net/http/HttpAuthHeader;->parseScheme(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 267
    .local v0, parameters:Ljava/lang/String;
    if-eqz v0, :cond_f

    #@8
    .line 269
    iget v1, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@a
    if-eqz v1, :cond_f

    #@c
    .line 270
    invoke-direct {p0, v0}, Landroid/net/http/HttpAuthHeader;->parseParameters(Ljava/lang/String;)V

    #@f
    .line 274
    .end local v0           #parameters:Ljava/lang/String;
    :cond_f
    return-void
.end method

.method private parseParameter(Ljava/lang/String;)V
    .registers 7
    .parameter "parameter"

    #@0
    .prologue
    .line 336
    if-eqz p1, :cond_2c

    #@2
    .line 338
    const/16 v3, 0x3d

    #@4
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v0

    #@8
    .line 339
    .local v0, i:I
    if-ltz v0, :cond_2c

    #@a
    .line 340
    const/4 v3, 0x0

    #@b
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 341
    .local v1, token:Ljava/lang/String;
    add-int/lit8 v3, v0, 0x1

    #@15
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v3}, Landroid/net/http/HttpAuthHeader;->trimDoubleQuotesIfAny(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 350
    .local v2, value:Ljava/lang/String;
    const-string/jumbo v3, "realm"

    #@24
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2d

    #@2a
    .line 351
    iput-object v2, p0, Landroid/net/http/HttpAuthHeader;->mRealm:Ljava/lang/String;

    #@2c
    .line 359
    .end local v0           #i:I
    .end local v1           #token:Ljava/lang/String;
    .end local v2           #value:Ljava/lang/String;
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 353
    .restart local v0       #i:I
    .restart local v1       #token:Ljava/lang/String;
    .restart local v2       #value:Ljava/lang/String;
    :cond_2d
    iget v3, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@2f
    const/4 v4, 0x2

    #@30
    if-ne v3, v4, :cond_2c

    #@32
    .line 354
    invoke-direct {p0, v1, v2}, Landroid/net/http/HttpAuthHeader;->parseParameter(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    goto :goto_2c
.end method

.method private parseParameter(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "token"
    .parameter "value"

    #@0
    .prologue
    .line 366
    if-eqz p1, :cond_f

    #@2
    if-eqz p2, :cond_f

    #@4
    .line 367
    const-string/jumbo v0, "nonce"

    #@7
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 368
    iput-object p2, p0, Landroid/net/http/HttpAuthHeader;->mNonce:Ljava/lang/String;

    #@f
    .line 392
    :cond_f
    :goto_f
    return-void

    #@10
    .line 372
    :cond_10
    const-string/jumbo v0, "stale"

    #@13
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_1d

    #@19
    .line 373
    invoke-direct {p0, p2}, Landroid/net/http/HttpAuthHeader;->parseStale(Ljava/lang/String;)V

    #@1c
    goto :goto_f

    #@1d
    .line 377
    :cond_1d
    const-string/jumbo v0, "opaque"

    #@20
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_29

    #@26
    .line 378
    iput-object p2, p0, Landroid/net/http/HttpAuthHeader;->mOpaque:Ljava/lang/String;

    #@28
    goto :goto_f

    #@29
    .line 382
    :cond_29
    const-string/jumbo v0, "qop"

    #@2c
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_39

    #@32
    .line 383
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    iput-object v0, p0, Landroid/net/http/HttpAuthHeader;->mQop:Ljava/lang/String;

    #@38
    goto :goto_f

    #@39
    .line 387
    :cond_39
    const-string v0, "algorithm"

    #@3b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_f

    #@41
    .line 388
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    iput-object v0, p0, Landroid/net/http/HttpAuthHeader;->mAlgorithm:Ljava/lang/String;

    #@47
    goto :goto_f
.end method

.method private parseParameters(Ljava/lang/String;)V
    .registers 4
    .parameter "parameters"

    #@0
    .prologue
    .line 316
    if-eqz p1, :cond_f

    #@2
    .line 319
    :cond_2
    const/16 v1, 0x2c

    #@4
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v0

    #@8
    .line 320
    .local v0, i:I
    if-gez v0, :cond_10

    #@a
    .line 322
    invoke-direct {p0, p1}, Landroid/net/http/HttpAuthHeader;->parseParameter(Ljava/lang/String;)V

    #@d
    .line 327
    :goto_d
    if-gez v0, :cond_2

    #@f
    .line 329
    .end local v0           #i:I
    :cond_f
    return-void

    #@10
    .line 324
    .restart local v0       #i:I
    :cond_10
    const/4 v1, 0x0

    #@11
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {p0, v1}, Landroid/net/http/HttpAuthHeader;->parseParameter(Ljava/lang/String;)V

    #@18
    .line 325
    add-int/lit8 v1, v0, 0x1

    #@1a
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1d
    move-result-object p1

    #@1e
    goto :goto_d
.end method

.method private parseScheme(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "header"

    #@0
    .prologue
    .line 284
    if-eqz p1, :cond_36

    #@2
    .line 285
    const/16 v2, 0x20

    #@4
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v0

    #@8
    .line 286
    .local v0, i:I
    if-ltz v0, :cond_36

    #@a
    .line 287
    const/4 v2, 0x0

    #@b
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 288
    .local v1, scheme:Ljava/lang/String;
    const-string v2, "Digest"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_2a

    #@1b
    .line 289
    const/4 v2, 0x2

    #@1c
    iput v2, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@1e
    .line 292
    const-string/jumbo v2, "md5"

    #@21
    iput-object v2, p0, Landroid/net/http/HttpAuthHeader;->mAlgorithm:Ljava/lang/String;

    #@23
    .line 299
    :cond_23
    :goto_23
    add-int/lit8 v2, v0, 0x1

    #@25
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    .line 303
    .end local v0           #i:I
    .end local v1           #scheme:Ljava/lang/String;
    :goto_29
    return-object v2

    #@2a
    .line 294
    .restart local v0       #i:I
    .restart local v1       #scheme:Ljava/lang/String;
    :cond_2a
    const-string v2, "Basic"

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_23

    #@32
    .line 295
    const/4 v2, 0x1

    #@33
    iput v2, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@35
    goto :goto_23

    #@36
    .line 303
    .end local v0           #i:I
    .end local v1           #scheme:Ljava/lang/String;
    :cond_36
    const/4 v2, 0x0

    #@37
    goto :goto_29
.end method

.method private parseStale(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 399
    if-eqz p1, :cond_e

    #@2
    .line 400
    const-string/jumbo v0, "true"

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 401
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/net/http/HttpAuthHeader;->mStale:Z

    #@e
    .line 404
    :cond_e
    return-void
.end method

.method private static trimDoubleQuotesIfAny(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "value"

    #@0
    .prologue
    const/16 v2, 0x22

    #@2
    .line 412
    if-eqz p0, :cond_21

    #@4
    .line 413
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v0

    #@8
    .line 414
    .local v0, len:I
    const/4 v1, 0x2

    #@9
    if-le v0, v1, :cond_21

    #@b
    const/4 v1, 0x0

    #@c
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v1

    #@10
    if-ne v1, v2, :cond_21

    #@12
    add-int/lit8 v1, v0, -0x1

    #@14
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v1

    #@18
    if-ne v1, v2, :cond_21

    #@1a
    .line 416
    const/4 v1, 0x1

    #@1b
    add-int/lit8 v2, v0, -0x1

    #@1d
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@20
    move-result-object p0

    #@21
    .line 420
    .end local v0           #len:I
    .end local p0
    :cond_21
    return-object p0
.end method


# virtual methods
.method public getAlgorithm()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 230
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mAlgorithm:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mNonce:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOpaque()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mOpaque:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 154
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mPassword:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getQop()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mQop:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRealm()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mRealm:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getScheme()I
    .registers 2

    #@0
    .prologue
    .line 185
    iget v0, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@2
    return v0
.end method

.method public getStale()Z
    .registers 2

    #@0
    .prologue
    .line 193
    iget-boolean v0, p0, Landroid/net/http/HttpAuthHeader;->mStale:Z

    #@2
    return v0
.end method

.method public getUsername()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/net/http/HttpAuthHeader;->mUsername:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isBasic()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 168
    iget v1, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@3
    if-ne v1, v0, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isDigest()Z
    .registers 3

    #@0
    .prologue
    .line 175
    iget v0, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isProxy()Z
    .registers 2

    #@0
    .prologue
    .line 126
    iget-boolean v0, p0, Landroid/net/http/HttpAuthHeader;->mIsProxy:Z

    #@2
    return v0
.end method

.method public isSupportedScheme()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 241
    iget-object v2, p0, Landroid/net/http/HttpAuthHeader;->mRealm:Ljava/lang/String;

    #@4
    if-eqz v2, :cond_2b

    #@6
    .line 242
    iget v2, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@8
    if-ne v2, v0, :cond_b

    #@a
    .line 253
    :cond_a
    :goto_a
    return v0

    #@b
    .line 245
    :cond_b
    iget v2, p0, Landroid/net/http/HttpAuthHeader;->mScheme:I

    #@d
    const/4 v3, 0x2

    #@e
    if-ne v2, v3, :cond_2b

    #@10
    .line 246
    iget-object v2, p0, Landroid/net/http/HttpAuthHeader;->mAlgorithm:Ljava/lang/String;

    #@12
    const-string/jumbo v3, "md5"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_29

    #@1b
    iget-object v2, p0, Landroid/net/http/HttpAuthHeader;->mQop:Ljava/lang/String;

    #@1d
    if-eqz v2, :cond_a

    #@1f
    iget-object v2, p0, Landroid/net/http/HttpAuthHeader;->mQop:Ljava/lang/String;

    #@21
    const-string v3, "auth"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_a

    #@29
    :cond_29
    move v0, v1

    #@2a
    goto :goto_a

    #@2b
    :cond_2b
    move v0, v1

    #@2c
    .line 253
    goto :goto_a
.end method

.method public setPassword(Ljava/lang/String;)V
    .registers 2
    .parameter "password"

    #@0
    .prologue
    .line 161
    iput-object p1, p0, Landroid/net/http/HttpAuthHeader;->mPassword:Ljava/lang/String;

    #@2
    .line 162
    return-void
.end method

.method public setProxy()V
    .registers 2

    #@0
    .prologue
    .line 133
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/net/http/HttpAuthHeader;->mIsProxy:Z

    #@3
    .line 134
    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .registers 2
    .parameter "username"

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Landroid/net/http/HttpAuthHeader;->mUsername:Ljava/lang/String;

    #@2
    .line 148
    return-void
.end method
