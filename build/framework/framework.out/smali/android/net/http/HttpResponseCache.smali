.class public final Landroid/net/http/HttpResponseCache;
.super Ljava/net/ResponseCache;
.source "HttpResponseCache.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/net/ExtendedResponseCache;


# instance fields
.field private final delegate:Llibcore/net/http/HttpResponseCache;


# direct methods
.method private constructor <init>(Ljava/io/File;J)V
    .registers 5
    .parameter "directory"
    .parameter "maxSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 159
    invoke-direct {p0}, Ljava/net/ResponseCache;-><init>()V

    #@3
    .line 160
    new-instance v0, Llibcore/net/http/HttpResponseCache;

    #@5
    invoke-direct {v0, p1, p2, p3}, Llibcore/net/http/HttpResponseCache;-><init>(Ljava/io/File;J)V

    #@8
    iput-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@a
    .line 161
    return-void
.end method

.method public static getInstalled()Landroid/net/http/HttpResponseCache;
    .registers 2

    #@0
    .prologue
    .line 168
    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    #@3
    move-result-object v0

    #@4
    .line 169
    .local v0, installed:Ljava/net/ResponseCache;
    instance-of v1, v0, Landroid/net/http/HttpResponseCache;

    #@6
    if-eqz v1, :cond_b

    #@8
    check-cast v0, Landroid/net/http/HttpResponseCache;

    #@a
    .end local v0           #installed:Ljava/net/ResponseCache;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #installed:Ljava/net/ResponseCache;
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public static install(Ljava/io/File;J)Landroid/net/http/HttpResponseCache;
    .registers 8
    .parameter "directory"
    .parameter "maxSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 184
    invoke-static {}, Landroid/net/http/HttpResponseCache;->getInstalled()Landroid/net/http/HttpResponseCache;

    #@3
    move-result-object v0

    #@4
    .line 185
    .local v0, installed:Landroid/net/http/HttpResponseCache;
    if-eqz v0, :cond_28

    #@6
    .line 187
    iget-object v3, v0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@8
    invoke-virtual {v3}, Llibcore/net/http/HttpResponseCache;->getCache()Llibcore/io/DiskLruCache;

    #@b
    move-result-object v1

    #@c
    .line 188
    .local v1, installedCache:Llibcore/io/DiskLruCache;
    invoke-virtual {v1}, Llibcore/io/DiskLruCache;->getDirectory()Ljava/io/File;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_25

    #@16
    invoke-virtual {v1}, Llibcore/io/DiskLruCache;->maxSize()J

    #@19
    move-result-wide v3

    #@1a
    cmp-long v3, v3, p1

    #@1c
    if-nez v3, :cond_25

    #@1e
    invoke-virtual {v1}, Llibcore/io/DiskLruCache;->isClosed()Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_25

    #@24
    .line 199
    .end local v0           #installed:Landroid/net/http/HttpResponseCache;
    .end local v1           #installedCache:Llibcore/io/DiskLruCache;
    :goto_24
    return-object v0

    #@25
    .line 193
    .restart local v0       #installed:Landroid/net/http/HttpResponseCache;
    .restart local v1       #installedCache:Llibcore/io/DiskLruCache;
    :cond_25
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@28
    .line 197
    .end local v1           #installedCache:Llibcore/io/DiskLruCache;
    :cond_28
    new-instance v2, Landroid/net/http/HttpResponseCache;

    #@2a
    invoke-direct {v2, p0, p1, p2}, Landroid/net/http/HttpResponseCache;-><init>(Ljava/io/File;J)V

    #@2d
    .line 198
    .local v2, result:Landroid/net/http/HttpResponseCache;
    invoke-static {v2}, Ljava/net/ResponseCache;->setDefault(Ljava/net/ResponseCache;)V

    #@30
    move-object v0, v2

    #@31
    .line 199
    goto :goto_24
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 286
    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    #@3
    move-result-object v0

    #@4
    if-ne v0, p0, :cond_a

    #@6
    .line 287
    const/4 v0, 0x0

    #@7
    invoke-static {v0}, Ljava/net/ResponseCache;->setDefault(Ljava/net/ResponseCache;)V

    #@a
    .line 289
    :cond_a
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@c
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getCache()Llibcore/io/DiskLruCache;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Llibcore/io/DiskLruCache;->close()V

    #@13
    .line 290
    return-void
.end method

.method public delete()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 296
    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    #@3
    move-result-object v0

    #@4
    if-ne v0, p0, :cond_a

    #@6
    .line 297
    const/4 v0, 0x0

    #@7
    invoke-static {v0}, Ljava/net/ResponseCache;->setDefault(Ljava/net/ResponseCache;)V

    #@a
    .line 299
    :cond_a
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@c
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getCache()Llibcore/io/DiskLruCache;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Llibcore/io/DiskLruCache;->delete()V

    #@13
    .line 300
    return-void
.end method

.method public flush()V
    .registers 2

    #@0
    .prologue
    .line 235
    :try_start_0
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getCache()Llibcore/io/DiskLruCache;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Llibcore/io/DiskLruCache;->flush()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 238
    :goto_9
    return-void

    #@a
    .line 236
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;
    .registers 5
    .parameter "uri"
    .parameter "requestMethod"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/net/CacheResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 204
    .local p3, requestHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Llibcore/net/http/HttpResponseCache;->get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getHitCount()I
    .registers 2

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getHitCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNetworkCount()I
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getNetworkCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getRequestCount()I
    .registers 2

    #@0
    .prologue
    .line 263
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getRequestCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public maxSize()J
    .registers 3

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getCache()Llibcore/io/DiskLruCache;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Llibcore/io/DiskLruCache;->maxSize()J

    #@9
    move-result-wide v0

    #@a
    return-wide v0
.end method

.method public put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;
    .registers 4
    .parameter "uri"
    .parameter "urlConnection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0, p1, p2}, Llibcore/net/http/HttpResponseCache;->put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public size()J
    .registers 3

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->getCache()Llibcore/io/DiskLruCache;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Llibcore/io/DiskLruCache;->size()J

    #@9
    move-result-wide v0

    #@a
    return-wide v0
.end method

.method public trackConditionalCacheHit()V
    .registers 2

    #@0
    .prologue
    .line 273
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0}, Llibcore/net/http/HttpResponseCache;->trackConditionalCacheHit()V

    #@5
    .line 274
    return-void
.end method

.method public trackResponse(Ljava/net/ResponseSource;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0, p1}, Llibcore/net/http/HttpResponseCache;->trackResponse(Ljava/net/ResponseSource;)V

    #@5
    .line 269
    return-void
.end method

.method public update(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V
    .registers 4
    .parameter "conditionalCacheHit"
    .parameter "connection"

    #@0
    .prologue
    .line 278
    iget-object v0, p0, Landroid/net/http/HttpResponseCache;->delegate:Llibcore/net/http/HttpResponseCache;

    #@2
    invoke-virtual {v0, p1, p2}, Llibcore/net/http/HttpResponseCache;->update(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V

    #@5
    .line 279
    return-void
.end method
