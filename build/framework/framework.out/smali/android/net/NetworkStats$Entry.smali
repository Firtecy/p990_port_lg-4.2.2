.class public Landroid/net/NetworkStats$Entry;
.super Ljava/lang/Object;
.source "NetworkStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/NetworkStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public iface:Ljava/lang/String;

.field public operations:J

.field public rxBytes:J

.field public rxPackets:J

.field public set:I

.field public tag:I

.field public txBytes:J

.field public txPackets:J

.field public uid:I


# direct methods
.method public constructor <init>()V
    .registers 16

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const-wide/16 v5, 0x0

    #@3
    .line 86
    sget-object v1, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@5
    const/4 v2, -0x1

    #@6
    move-object v0, p0

    #@7
    move v4, v3

    #@8
    move-wide v7, v5

    #@9
    move-wide v9, v5

    #@a
    move-wide v11, v5

    #@b
    move-wide v13, v5

    #@c
    invoke-direct/range {v0 .. v14}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@f
    .line 87
    return-void
.end method

.method public constructor <init>(JJJJJ)V
    .registers 26
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"
    .parameter "operations"

    #@0
    .prologue
    .line 90
    sget-object v1, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@2
    const/4 v2, -0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v4, 0x0

    #@5
    move-object v0, p0

    #@6
    move-wide/from16 v5, p1

    #@8
    move-wide/from16 v7, p3

    #@a
    move-wide/from16 v9, p5

    #@c
    move-wide/from16 v11, p7

    #@e
    move-wide/from16 v13, p9

    #@10
    invoke-direct/range {v0 .. v14}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@13
    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIJJJJJ)V
    .registers 15
    .parameter "iface"
    .parameter "uid"
    .parameter "set"
    .parameter "tag"
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"
    .parameter "operations"

    #@0
    .prologue
    .line 95
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 96
    iput-object p1, p0, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@5
    .line 97
    iput p2, p0, Landroid/net/NetworkStats$Entry;->uid:I

    #@7
    .line 98
    iput p3, p0, Landroid/net/NetworkStats$Entry;->set:I

    #@9
    .line 99
    iput p4, p0, Landroid/net/NetworkStats$Entry;->tag:I

    #@b
    .line 100
    iput-wide p5, p0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@d
    .line 101
    iput-wide p7, p0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@f
    .line 102
    iput-wide p9, p0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@11
    .line 103
    iput-wide p11, p0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@13
    .line 104
    iput-wide p13, p0, Landroid/net/NetworkStats$Entry;->operations:J

    #@15
    .line 105
    return-void
.end method


# virtual methods
.method public add(Landroid/net/NetworkStats$Entry;)V
    .registers 6
    .parameter "another"

    #@0
    .prologue
    .line 117
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@2
    iget-wide v2, p1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@4
    add-long/2addr v0, v2

    #@5
    iput-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@7
    .line 118
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@9
    iget-wide v2, p1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@b
    add-long/2addr v0, v2

    #@c
    iput-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@e
    .line 119
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@10
    iget-wide v2, p1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@12
    add-long/2addr v0, v2

    #@13
    iput-wide v0, p0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@15
    .line 120
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@17
    iget-wide v2, p1, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@19
    add-long/2addr v0, v2

    #@1a
    iput-wide v0, p0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@1c
    .line 121
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->operations:J

    #@1e
    iget-wide v2, p1, Landroid/net/NetworkStats$Entry;->operations:J

    #@20
    add-long/2addr v0, v2

    #@21
    iput-wide v0, p0, Landroid/net/NetworkStats$Entry;->operations:J

    #@23
    .line 122
    return-void
.end method

.method public isEmpty()Z
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 112
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_22

    #@8
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@a
    cmp-long v0, v0, v2

    #@c
    if-nez v0, :cond_22

    #@e
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@10
    cmp-long v0, v0, v2

    #@12
    if-nez v0, :cond_22

    #@14
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@16
    cmp-long v0, v0, v2

    #@18
    if-nez v0, :cond_22

    #@1a
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->operations:J

    #@1c
    cmp-long v0, v0, v2

    #@1e
    if-nez v0, :cond_22

    #@20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public isNegative()Z
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 108
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@4
    cmp-long v0, v0, v2

    #@6
    if-ltz v0, :cond_20

    #@8
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@a
    cmp-long v0, v0, v2

    #@c
    if-ltz v0, :cond_20

    #@e
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@10
    cmp-long v0, v0, v2

    #@12
    if-ltz v0, :cond_20

    #@14
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@16
    cmp-long v0, v0, v2

    #@18
    if-ltz v0, :cond_20

    #@1a
    iget-wide v0, p0, Landroid/net/NetworkStats$Entry;->operations:J

    #@1c
    cmp-long v0, v0, v2

    #@1e
    if-gez v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 127
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "iface="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget-object v2, p0, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 128
    const-string v1, " uid="

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget v2, p0, Landroid/net/NetworkStats$Entry;->uid:I

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    .line 129
    const-string v1, " set="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget v2, p0, Landroid/net/NetworkStats$Entry;->set:I

    #@23
    invoke-static {v2}, Landroid/net/NetworkStats;->setToString(I)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 130
    const-string v1, " tag="

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    iget v2, p0, Landroid/net/NetworkStats$Entry;->tag:I

    #@32
    invoke-static {v2}, Landroid/net/NetworkStats;->tagToString(I)Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    .line 131
    const-string v1, " rxBytes="

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    iget-wide v2, p0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@41
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@44
    .line 132
    const-string v1, " rxPackets="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-wide v2, p0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@4c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4f
    .line 133
    const-string v1, " txBytes="

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget-wide v2, p0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@57
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5a
    .line 134
    const-string v1, " txPackets="

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    iget-wide v2, p0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@62
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@65
    .line 135
    const-string v1, " operations="

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    iget-wide v2, p0, Landroid/net/NetworkStats$Entry;->operations:J

    #@6d
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@70
    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    return-object v1
.end method
