.class public Landroid/net/ProxyProperties;
.super Ljava/lang/Object;
.source "ProxyProperties.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/ProxyProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mExclusionList:Ljava/lang/String;

.field private mHost:Ljava/lang/String;

.field private mParsedExclusionList:[Ljava/lang/String;

.field private mPort:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 198
    new-instance v0, Landroid/net/ProxyProperties$1;

    #@2
    invoke-direct {v0}, Landroid/net/ProxyProperties$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/ProxyProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/net/ProxyProperties;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    if-eqz p1, :cond_1b

    #@5
    .line 56
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@b
    .line 57
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getPort()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/net/ProxyProperties;->mPort:I

    #@11
    .line 58
    invoke-virtual {p1}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@17
    .line 59
    iget-object v0, p1, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@19
    iput-object v0, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@1b
    .line 61
    :cond_1b
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter "host"
    .parameter "port"
    .parameter "exclList"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    iput-object p1, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@5
    .line 42
    iput p2, p0, Landroid/net/ProxyProperties;->mPort:I

    #@7
    .line 43
    invoke-direct {p0, p3}, Landroid/net/ProxyProperties;->setExclusionList(Ljava/lang/String;)V

    #@a
    .line 44
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V
    .registers 5
    .parameter "host"
    .parameter "port"
    .parameter "exclList"
    .parameter "parsedExclList"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 47
    iput-object p1, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@5
    .line 48
    iput p2, p0, Landroid/net/ProxyProperties;->mPort:I

    #@7
    .line 49
    iput-object p3, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@9
    .line 50
    iput-object p4, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@b
    .line 51
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/net/ProxyProperties$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/net/ProxyProperties;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private setExclusionList(Ljava/lang/String;)V
    .registers 9
    .parameter "exclusionList"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@2
    .line 87
    iget-object v3, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@4
    if-nez v3, :cond_c

    #@6
    .line 88
    const/4 v3, 0x0

    #@7
    new-array v3, v3, [Ljava/lang/String;

    #@9
    iput-object v3, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@b
    .line 99
    :cond_b
    return-void

    #@c
    .line 90
    :cond_c
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    const-string v4, ","

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    .line 91
    .local v2, splitExclusionList:[Ljava/lang/String;
    array-length v3, v2

    #@17
    mul-int/lit8 v3, v3, 0x2

    #@19
    new-array v3, v3, [Ljava/lang/String;

    #@1b
    iput-object v3, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@1d
    .line 92
    const/4 v0, 0x0

    #@1e
    .local v0, i:I
    :goto_1e
    array-length v3, v2

    #@1f
    if-ge v0, v3, :cond_b

    #@21
    .line 93
    aget-object v3, v2, v0

    #@23
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 94
    .local v1, s:Ljava/lang/String;
    const-string v3, "."

    #@29
    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_34

    #@2f
    const/4 v3, 0x1

    #@30
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    .line 95
    :cond_34
    iget-object v3, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@36
    mul-int/lit8 v4, v0, 0x2

    #@38
    aput-object v1, v3, v4

    #@3a
    .line 96
    iget-object v3, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@3c
    mul-int/lit8 v4, v0, 0x2

    #@3e
    add-int/lit8 v4, v4, 0x1

    #@40
    new-instance v5, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v6, "."

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    aput-object v5, v3, v4

    #@55
    .line 92
    add-int/lit8 v0, v0, 0x1

    #@57
    goto :goto_1e
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 165
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 148
    instance-of v2, p1, Landroid/net/ProxyProperties;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 157
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 149
    check-cast v0, Landroid/net/ProxyProperties;

    #@9
    .line 150
    .local v0, p:Landroid/net/ProxyProperties;
    iget-object v2, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@b
    if-eqz v2, :cond_19

    #@d
    iget-object v2, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@f
    invoke-virtual {v0}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_5

    #@19
    .line 151
    :cond_19
    iget-object v2, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@1b
    if-eqz v2, :cond_2f

    #@1d
    invoke-virtual {v0}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    if-eqz v2, :cond_2f

    #@23
    iget-object v2, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@25
    invoke-virtual {v0}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_5

    #@2f
    .line 154
    :cond_2f
    iget-object v2, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@31
    if-eqz v2, :cond_37

    #@33
    iget-object v2, v0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@35
    if-eqz v2, :cond_5

    #@37
    .line 155
    :cond_37
    iget-object v2, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@39
    if-nez v2, :cond_3f

    #@3b
    iget-object v2, v0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@3d
    if-nez v2, :cond_5

    #@3f
    .line 156
    :cond_3f
    iget v2, p0, Landroid/net/ProxyProperties;->mPort:I

    #@41
    iget v3, v0, Landroid/net/ProxyProperties;->mPort:I

    #@43
    if-ne v2, v3, :cond_5

    #@45
    .line 157
    const/4 v1, 0x1

    #@46
    goto :goto_5
.end method

.method public getExclusionList()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPort()I
    .registers 2

    #@0
    .prologue
    .line 76
    iget v0, p0, Landroid/net/ProxyProperties;->mPort:I

    #@2
    return v0
.end method

.method public getSocketAddress()Ljava/net/InetSocketAddress;
    .registers 5

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    .line 66
    .local v0, inetSocketAddress:Ljava/net/InetSocketAddress;
    :try_start_1
    new-instance v1, Ljava/net/InetSocketAddress;

    #@3
    iget-object v2, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@5
    iget v3, p0, Landroid/net/ProxyProperties;->mPort:I

    #@7
    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_a} :catch_c

    #@a
    .end local v0           #inetSocketAddress:Ljava/net/InetSocketAddress;
    .local v1, inetSocketAddress:Ljava/net/InetSocketAddress;
    move-object v0, v1

    #@b
    .line 68
    .end local v1           #inetSocketAddress:Ljava/net/InetSocketAddress;
    .restart local v0       #inetSocketAddress:Ljava/net/InetSocketAddress;
    :goto_b
    return-object v0

    #@c
    .line 67
    :catch_c
    move-exception v2

    #@d
    goto :goto_b
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 173
    iget-object v0, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@3
    if-nez v0, :cond_f

    #@5
    move v0, v1

    #@6
    :goto_6
    iget-object v2, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@8
    if-nez v2, :cond_16

    #@a
    :goto_a
    add-int/2addr v0, v1

    #@b
    iget v1, p0, Landroid/net/ProxyProperties;->mPort:I

    #@d
    add-int/2addr v0, v1

    #@e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@14
    move-result v0

    #@15
    goto :goto_6

    #@16
    :cond_16
    iget-object v1, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@18
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@1b
    move-result v1

    #@1c
    goto :goto_a
.end method

.method public isExcluded(Ljava/lang/String;)Z
    .registers 8
    .parameter "url"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 102
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v4

    #@5
    if-nez v4, :cond_10

    #@7
    iget-object v4, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@9
    if-eqz v4, :cond_10

    #@b
    iget-object v4, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@d
    array-length v4, v4

    #@e
    if-nez v4, :cond_11

    #@10
    .line 114
    :cond_10
    :goto_10
    return v3

    #@11
    .line 105
    :cond_11
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@14
    move-result-object v1

    #@15
    .line 106
    .local v1, u:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    .line 107
    .local v2, urlDomain:Ljava/lang/String;
    if-eqz v2, :cond_10

    #@1b
    .line 108
    const/4 v0, 0x0

    #@1c
    .local v0, i:I
    :goto_1c
    iget-object v4, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@1e
    array-length v4, v4

    #@1f
    if-ge v0, v4, :cond_10

    #@21
    .line 109
    iget-object v4, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@23
    aget-object v4, v4, v0

    #@25
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v4

    #@29
    if-nez v4, :cond_37

    #@2b
    iget-object v4, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@2d
    add-int/lit8 v5, v0, 0x1

    #@2f
    aget-object v4, v4, v5

    #@31
    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_39

    #@37
    .line 111
    :cond_37
    const/4 v3, 0x1

    #@38
    goto :goto_10

    #@39
    .line 108
    :cond_39
    add-int/lit8 v0, v0, 0x2

    #@3b
    goto :goto_1c
.end method

.method public makeProxy()Ljava/net/Proxy;
    .registers 6

    #@0
    .prologue
    .line 118
    sget-object v1, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@2
    .line 119
    .local v1, proxy:Ljava/net/Proxy;
    iget-object v3, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@4
    if-eqz v3, :cond_17

    #@6
    .line 121
    :try_start_6
    new-instance v0, Ljava/net/InetSocketAddress;

    #@8
    iget-object v3, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@a
    iget v4, p0, Landroid/net/ProxyProperties;->mPort:I

    #@c
    invoke-direct {v0, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    #@f
    .line 122
    .local v0, inetSocketAddress:Ljava/net/InetSocketAddress;
    new-instance v2, Ljava/net/Proxy;

    #@11
    sget-object v3, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    #@13
    invoke-direct {v2, v3, v0}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V
    :try_end_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_16} :catch_18

    #@16
    .end local v1           #proxy:Ljava/net/Proxy;
    .local v2, proxy:Ljava/net/Proxy;
    move-object v1, v2

    #@17
    .line 126
    .end local v0           #inetSocketAddress:Ljava/net/InetSocketAddress;
    .end local v2           #proxy:Ljava/net/Proxy;
    .restart local v1       #proxy:Ljava/net/Proxy;
    :cond_17
    :goto_17
    return-object v1

    #@18
    .line 123
    :catch_18
    move-exception v3

    #@19
    goto :goto_17
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 132
    .local v0, sb:Ljava/lang/StringBuilder;
    iget-object v1, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@7
    if-eqz v1, :cond_35

    #@9
    .line 133
    const-string v1, "["

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 134
    iget-object v1, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 135
    const-string v1, "] "

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 136
    iget v1, p0, Landroid/net/ProxyProperties;->mPort:I

    #@1a
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 137
    iget-object v1, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@23
    if-eqz v1, :cond_30

    #@25
    .line 138
    const-string v1, " xl="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget-object v2, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 143
    :cond_30
    :goto_30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    return-object v1

    #@35
    .line 141
    :cond_35
    const-string v1, "[ProxyProperties.mHost == null]"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    goto :goto_30
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 183
    iget-object v0, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 184
    const/4 v0, 0x1

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@8
    .line 185
    iget-object v0, p0, Landroid/net/ProxyProperties;->mHost:Ljava/lang/String;

    #@a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 186
    iget v0, p0, Landroid/net/ProxyProperties;->mPort:I

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 190
    :goto_12
    iget-object v0, p0, Landroid/net/ProxyProperties;->mExclusionList:Ljava/lang/String;

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 191
    iget-object v0, p0, Landroid/net/ProxyProperties;->mParsedExclusionList:[Ljava/lang/String;

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@1c
    .line 192
    return-void

    #@1d
    .line 188
    :cond_1d
    const/4 v0, 0x0

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@21
    goto :goto_12
.end method
