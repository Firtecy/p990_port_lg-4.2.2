.class public final Landroid/net/DnsPinger;
.super Landroid/os/Handler;
.source "DnsPinger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/DnsPinger$1;,
        Landroid/net/DnsPinger$DnsArg;,
        Landroid/net/DnsPinger$ActivePing;
    }
.end annotation


# static fields
.field private static final ACTION_CANCEL_ALL_PINGS:I = 0x50003

.field private static final ACTION_LISTEN_FOR_RESPONSE:I = 0x50002

.field private static final ACTION_PING_DNS:I = 0x50001

.field private static final BASE:I = 0x50000

.field private static final DBG:Z = false

.field public static final DNS_PING_RESULT:I = 0x50000

.field private static final DNS_PORT:I = 0x35

.field private static final RECEIVE_POLL_INTERVAL_MS:I = 0xc8

.field public static final SOCKET_EXCEPTION:I = -0x2

.field private static final SOCKET_TIMEOUT_MS:I = 0x1

.field public static final TIMEOUT:I = -0x1

.field private static final mDnsQuery:[B

.field private static final sCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final sRandom:Ljava/util/Random;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mActivePings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/DnsPinger$ActivePing;",
            ">;"
        }
    .end annotation
.end field

.field private final mConnectionType:I

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mCurrentToken:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mDefaultDns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field private mEventCounter:I

.field private final mTarget:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 63
    new-instance v0, Ljava/util/Random;

    #@2
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    #@5
    sput-object v0, Landroid/net/DnsPinger;->sRandom:Ljava/util/Random;

    #@7
    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@9
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@c
    sput-object v0, Landroid/net/DnsPinger;->sCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@e
    .line 312
    const/16 v0, 0x20

    #@10
    new-array v0, v0, [B

    #@12
    fill-array-data v0, :array_18

    #@15
    sput-object v0, Landroid/net/DnsPinger;->mDnsQuery:[B

    #@17
    return-void

    #@18
    :array_18
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x3t
        0x77t
        0x77t
        0x77t
        0x6t
        0x67t
        0x6ft
        0x6ft
        0x67t
        0x6ct
        0x65t
        0x3t
        0x63t
        0x6ft
        0x6dt
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Looper;Landroid/os/Handler;I)V
    .registers 9
    .parameter "context"
    .parameter "TAG"
    .parameter "looper"
    .parameter "target"
    .parameter "connectionType"

    #@0
    .prologue
    .line 121
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@3
    .line 66
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/net/DnsPinger;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@6
    .line 74
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@b
    iput-object v0, p0, Landroid/net/DnsPinger;->mCurrentToken:Ljava/util/concurrent/atomic/AtomicInteger;

    #@d
    .line 96
    new-instance v0, Ljava/util/ArrayList;

    #@f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v0, p0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@14
    .line 122
    iput-object p2, p0, Landroid/net/DnsPinger;->TAG:Ljava/lang/String;

    #@16
    .line 123
    iput-object p1, p0, Landroid/net/DnsPinger;->mContext:Landroid/content/Context;

    #@18
    .line 124
    iput-object p4, p0, Landroid/net/DnsPinger;->mTarget:Landroid/os/Handler;

    #@1a
    .line 125
    iput p5, p0, Landroid/net/DnsPinger;->mConnectionType:I

    #@1c
    .line 126
    invoke-static {p5}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_3b

    #@22
    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@24
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "Invalid connectionType in constructor: "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v0

    #@3b
    .line 130
    :cond_3b
    new-instance v0, Ljava/util/ArrayList;

    #@3d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@40
    iput-object v0, p0, Landroid/net/DnsPinger;->mDefaultDns:Ljava/util/ArrayList;

    #@42
    .line 131
    iget-object v0, p0, Landroid/net/DnsPinger;->mDefaultDns:Ljava/util/ArrayList;

    #@44
    invoke-direct {p0}, Landroid/net/DnsPinger;->getDefaultDns()Ljava/net/InetAddress;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4b
    .line 132
    const/4 v0, 0x0

    #@4c
    iput v0, p0, Landroid/net/DnsPinger;->mEventCounter:I

    #@4e
    .line 133
    return-void
.end method

.method private getCurrentLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 289
    iget-object v0, p0, Landroid/net/DnsPinger;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 290
    iget-object v0, p0, Landroid/net/DnsPinger;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "connectivity"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/net/ConnectivityManager;

    #@e
    iput-object v0, p0, Landroid/net/DnsPinger;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@10
    .line 294
    :cond_10
    iget-object v0, p0, Landroid/net/DnsPinger;->mConnectivityManager:Landroid/net/ConnectivityManager;

    #@12
    iget v1, p0, Landroid/net/DnsPinger;->mConnectionType:I

    #@14
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@17
    move-result-object v0

    #@18
    return-object v0
.end method

.method private getDefaultDns()Ljava/net/InetAddress;
    .registers 5

    #@0
    .prologue
    .line 298
    iget-object v2, p0, Landroid/net/DnsPinger;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v2

    #@6
    const-string v3, "default_dns_server"

    #@8
    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 300
    .local v0, dns:Ljava/lang/String;
    if-eqz v0, :cond_14

    #@e
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_21

    #@14
    .line 301
    :cond_14
    iget-object v2, p0, Landroid/net/DnsPinger;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v2

    #@1a
    const v3, 0x104003b

    #@1d
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 305
    :cond_21
    :try_start_21
    invoke-static {v0}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_24
    .catch Ljava/lang/IllegalArgumentException; {:try_start_21 .. :try_end_24} :catch_26

    #@24
    move-result-object v2

    #@25
    .line 308
    :goto_25
    return-object v2

    #@26
    .line 306
    :catch_26
    move-exception v1

    #@27
    .line 307
    .local v1, e:Ljava/lang/IllegalArgumentException;
    const-string v2, "getDefaultDns::malformed default dns address"

    #@29
    invoke-direct {p0, v2}, Landroid/net/DnsPinger;->loge(Ljava/lang/String;)V

    #@2c
    .line 308
    const/4 v2, 0x0

    #@2d
    goto :goto_25
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 328
    iget-object v0, p0, Landroid/net/DnsPinger;->TAG:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 329
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 332
    iget-object v0, p0, Landroid/net/DnsPinger;->TAG:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 333
    return-void
.end method

.method private sendResponse(III)V
    .registers 6
    .parameter "internalId"
    .parameter "externalId"
    .parameter "responseVal"

    #@0
    .prologue
    .line 285
    iget-object v0, p0, Landroid/net/DnsPinger;->mTarget:Landroid/os/Handler;

    #@2
    const/high16 v1, 0x5

    #@4
    invoke-virtual {p0, v1, p1, p3}, Landroid/net/DnsPinger;->obtainMessage(III)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@b
    .line 286
    return-void
.end method


# virtual methods
.method public cancelPings()V
    .registers 2

    #@0
    .prologue
    .line 275
    iget-object v0, p0, Landroid/net/DnsPinger;->mCurrentToken:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@5
    .line 276
    const v0, 0x50003

    #@8
    invoke-virtual {p0, v0}, Landroid/net/DnsPinger;->obtainMessage(I)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@f
    .line 277
    return-void
.end method

.method public getDnsList()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 245
    invoke-direct {p0}, Landroid/net/DnsPinger;->getCurrentLinkProperties()Landroid/net/LinkProperties;

    #@3
    move-result-object v0

    #@4
    .line 246
    .local v0, curLinkProps:Landroid/net/LinkProperties;
    if-nez v0, :cond_27

    #@6
    .line 247
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "getCurLinkProperties:: LP for type"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    iget v3, p0, Landroid/net/DnsPinger;->mConnectionType:I

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " is null!"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {p0, v2}, Landroid/net/DnsPinger;->loge(Ljava/lang/String;)V

    #@24
    .line 248
    iget-object v2, p0, Landroid/net/DnsPinger;->mDefaultDns:Ljava/util/ArrayList;

    #@26
    .line 257
    :goto_26
    return-object v2

    #@27
    .line 251
    :cond_27
    invoke-virtual {v0}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@2a
    move-result-object v1

    #@2b
    .line 252
    .local v1, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    if-eqz v1, :cond_33

    #@2d
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_3b

    #@33
    .line 253
    :cond_33
    const-string v2, "getDns::LinkProps has null dns - returning default"

    #@35
    invoke-direct {p0, v2}, Landroid/net/DnsPinger;->loge(Ljava/lang/String;)V

    #@38
    .line 254
    iget-object v2, p0, Landroid/net/DnsPinger;->mDefaultDns:Ljava/util/ArrayList;

    #@3a
    goto :goto_26

    #@3b
    .line 257
    :cond_3b
    new-instance v2, Ljava/util/ArrayList;

    #@3d
    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@40
    goto :goto_26
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 23
    .parameter "msg"

    #@0
    .prologue
    .line 137
    move-object/from16 v0, p1

    #@2
    iget v15, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v15, :pswitch_data_214

    #@7
    .line 237
    :cond_7
    :goto_7
    return-void

    #@8
    .line 139
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v7, Landroid/net/DnsPinger$DnsArg;

    #@e
    .line 140
    .local v7, dnsArg:Landroid/net/DnsPinger$DnsArg;
    iget v15, v7, Landroid/net/DnsPinger$DnsArg;->seq:I

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v0, v0, Landroid/net/DnsPinger;->mCurrentToken:Ljava/util/concurrent/atomic/AtomicInteger;

    #@14
    move-object/from16 v16, v0

    #@16
    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@19
    move-result v16

    #@1a
    move/from16 v0, v16

    #@1c
    if-ne v15, v0, :cond_7

    #@1e
    .line 144
    :try_start_1e
    new-instance v11, Landroid/net/DnsPinger$ActivePing;

    #@20
    const/4 v15, 0x0

    #@21
    move-object/from16 v0, p0

    #@23
    invoke-direct {v11, v0, v15}, Landroid/net/DnsPinger$ActivePing;-><init>(Landroid/net/DnsPinger;Landroid/net/DnsPinger$1;)V

    #@26
    .line 145
    .local v11, newActivePing:Landroid/net/DnsPinger$ActivePing;
    iget-object v6, v7, Landroid/net/DnsPinger$DnsArg;->dns:Ljava/net/InetAddress;

    #@28
    .line 146
    .local v6, dnsAddress:Ljava/net/InetAddress;
    move-object/from16 v0, p1

    #@2a
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@2c
    iput v15, v11, Landroid/net/DnsPinger$ActivePing;->internalId:I

    #@2e
    .line 147
    move-object/from16 v0, p1

    #@30
    iget v15, v0, Landroid/os/Message;->arg2:I

    #@32
    iput v15, v11, Landroid/net/DnsPinger$ActivePing;->timeout:I

    #@34
    .line 148
    new-instance v15, Ljava/net/DatagramSocket;

    #@36
    invoke-direct {v15}, Ljava/net/DatagramSocket;-><init>()V

    #@39
    iput-object v15, v11, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@3b
    .line 150
    iget-object v15, v11, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@3d
    const/16 v16, 0x1

    #@3f
    invoke-virtual/range {v15 .. v16}, Ljava/net/DatagramSocket;->setSoTimeout(I)V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_42} :catch_be

    #@42
    .line 154
    :try_start_42
    iget-object v15, v11, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@44
    invoke-direct/range {p0 .. p0}, Landroid/net/DnsPinger;->getCurrentLinkProperties()Landroid/net/LinkProperties;

    #@47
    move-result-object v16

    #@48
    invoke-virtual/range {v16 .. v16}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@4b
    move-result-object v16

    #@4c
    invoke-static/range {v16 .. v16}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    #@4f
    move-result-object v16

    #@50
    invoke-virtual/range {v15 .. v16}, Ljava/net/DatagramSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V
    :try_end_53
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_53} :catch_d2
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_53} :catch_be

    #@53
    .line 160
    :goto_53
    :try_start_53
    sget-object v15, Landroid/net/DnsPinger;->sRandom:Ljava/util/Random;

    #@55
    invoke-virtual {v15}, Ljava/util/Random;->nextInt()I

    #@58
    move-result v15

    #@59
    int-to-short v15, v15

    #@5a
    iput-short v15, v11, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@5c
    .line 161
    sget-object v15, Landroid/net/DnsPinger;->mDnsQuery:[B

    #@5e
    invoke-virtual {v15}, [B->clone()Ljava/lang/Object;

    #@61
    move-result-object v4

    #@62
    check-cast v4, [B

    #@64
    .line 162
    .local v4, buf:[B
    const/4 v15, 0x0

    #@65
    iget-short v0, v11, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@67
    move/from16 v16, v0

    #@69
    shr-int/lit8 v16, v16, 0x8

    #@6b
    move/from16 v0, v16

    #@6d
    int-to-byte v0, v0

    #@6e
    move/from16 v16, v0

    #@70
    aput-byte v16, v4, v15

    #@72
    .line 163
    const/4 v15, 0x1

    #@73
    iget-short v0, v11, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@75
    move/from16 v16, v0

    #@77
    move/from16 v0, v16

    #@79
    int-to-byte v0, v0

    #@7a
    move/from16 v16, v0

    #@7c
    aput-byte v16, v4, v15

    #@7e
    .line 166
    new-instance v12, Ljava/net/DatagramPacket;

    #@80
    array-length v15, v4

    #@81
    const/16 v16, 0x35

    #@83
    move/from16 v0, v16

    #@85
    invoke-direct {v12, v4, v15, v6, v0}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    #@88
    .line 174
    .local v12, packet:Ljava/net/DatagramPacket;
    iget-object v15, v11, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@8a
    invoke-virtual {v15, v12}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    #@8d
    .line 175
    move-object/from16 v0, p0

    #@8f
    iget-object v15, v0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@91
    invoke-interface {v15, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@94
    .line 176
    move-object/from16 v0, p0

    #@96
    iget v15, v0, Landroid/net/DnsPinger;->mEventCounter:I

    #@98
    add-int/lit8 v15, v15, 0x1

    #@9a
    move-object/from16 v0, p0

    #@9c
    iput v15, v0, Landroid/net/DnsPinger;->mEventCounter:I

    #@9e
    .line 177
    const v15, 0x50002

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget v0, v0, Landroid/net/DnsPinger;->mEventCounter:I

    #@a5
    move/from16 v16, v0

    #@a7
    const/16 v17, 0x0

    #@a9
    move-object/from16 v0, p0

    #@ab
    move/from16 v1, v16

    #@ad
    move/from16 v2, v17

    #@af
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/DnsPinger;->obtainMessage(III)Landroid/os/Message;

    #@b2
    move-result-object v15

    #@b3
    const-wide/16 v16, 0xc8

    #@b5
    move-object/from16 v0, p0

    #@b7
    move-wide/from16 v1, v16

    #@b9
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/DnsPinger;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_bc} :catch_be

    #@bc
    goto/16 :goto_7

    #@be
    .line 179
    .end local v4           #buf:[B
    .end local v6           #dnsAddress:Ljava/net/InetAddress;
    .end local v11           #newActivePing:Landroid/net/DnsPinger$ActivePing;
    .end local v12           #packet:Ljava/net/DatagramPacket;
    :catch_be
    move-exception v8

    #@bf
    .line 180
    .local v8, e:Ljava/io/IOException;
    move-object/from16 v0, p1

    #@c1
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@c3
    const/16 v16, -0x270f

    #@c5
    const/16 v17, -0x2

    #@c7
    move-object/from16 v0, p0

    #@c9
    move/from16 v1, v16

    #@cb
    move/from16 v2, v17

    #@cd
    invoke-direct {v0, v15, v1, v2}, Landroid/net/DnsPinger;->sendResponse(III)V

    #@d0
    goto/16 :goto_7

    #@d2
    .line 156
    .end local v8           #e:Ljava/io/IOException;
    .restart local v6       #dnsAddress:Ljava/net/InetAddress;
    .restart local v11       #newActivePing:Landroid/net/DnsPinger$ActivePing;
    :catch_d2
    move-exception v8

    #@d3
    .line 157
    .local v8, e:Ljava/lang/Exception;
    :try_start_d3
    new-instance v15, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string/jumbo v16, "sendDnsPing::Error binding to socket "

    #@db
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v15

    #@df
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v15

    #@e3
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v15

    #@e7
    move-object/from16 v0, p0

    #@e9
    invoke-direct {v0, v15}, Landroid/net/DnsPinger;->loge(Ljava/lang/String;)V
    :try_end_ec
    .catch Ljava/io/IOException; {:try_start_d3 .. :try_end_ec} :catch_be

    #@ec
    goto/16 :goto_53

    #@ee
    .line 184
    .end local v6           #dnsAddress:Ljava/net/InetAddress;
    .end local v7           #dnsArg:Landroid/net/DnsPinger$DnsArg;
    .end local v8           #e:Ljava/lang/Exception;
    .end local v11           #newActivePing:Landroid/net/DnsPinger$ActivePing;
    :pswitch_ee
    move-object/from16 v0, p1

    #@f0
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@f2
    move-object/from16 v0, p0

    #@f4
    iget v0, v0, Landroid/net/DnsPinger;->mEventCounter:I

    #@f6
    move/from16 v16, v0

    #@f8
    move/from16 v0, v16

    #@fa
    if-ne v15, v0, :cond_7

    #@fc
    .line 187
    move-object/from16 v0, p0

    #@fe
    iget-object v15, v0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@100
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@103
    move-result-object v9

    #@104
    .local v9, i$:Ljava/util/Iterator;
    :cond_104
    :goto_104
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@107
    move-result v15

    #@108
    if-eqz v15, :cond_15d

    #@10a
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10d
    move-result-object v5

    #@10e
    check-cast v5, Landroid/net/DnsPinger$ActivePing;

    #@110
    .line 190
    .local v5, curPing:Landroid/net/DnsPinger$ActivePing;
    const/4 v15, 0x2

    #@111
    :try_start_111
    new-array v14, v15, [B

    #@113
    .line 191
    .local v14, responseBuf:[B
    new-instance v13, Ljava/net/DatagramPacket;

    #@115
    const/4 v15, 0x2

    #@116
    invoke-direct {v13, v14, v15}, Ljava/net/DatagramPacket;-><init>([BI)V

    #@119
    .line 192
    .local v13, replyPacket:Ljava/net/DatagramPacket;
    iget-object v15, v5, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@11b
    invoke-virtual {v15, v13}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    #@11e
    .line 194
    const/4 v15, 0x0

    #@11f
    aget-byte v15, v14, v15

    #@121
    iget-short v0, v5, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@123
    move/from16 v16, v0

    #@125
    shr-int/lit8 v16, v16, 0x8

    #@127
    move/from16 v0, v16

    #@129
    int-to-byte v0, v0

    #@12a
    move/from16 v16, v0

    #@12c
    move/from16 v0, v16

    #@12e
    if-ne v15, v0, :cond_104

    #@130
    const/4 v15, 0x1

    #@131
    aget-byte v15, v14, v15

    #@133
    iget-short v0, v5, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@135
    move/from16 v16, v0

    #@137
    move/from16 v0, v16

    #@139
    int-to-byte v0, v0

    #@13a
    move/from16 v16, v0

    #@13c
    move/from16 v0, v16

    #@13e
    if-ne v15, v0, :cond_104

    #@140
    .line 196
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@143
    move-result-wide v15

    #@144
    iget-wide v0, v5, Landroid/net/DnsPinger$ActivePing;->start:J

    #@146
    move-wide/from16 v17, v0

    #@148
    sub-long v15, v15, v17

    #@14a
    long-to-int v15, v15

    #@14b
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14e
    move-result-object v15

    #@14f
    iput-object v15, v5, Landroid/net/DnsPinger$ActivePing;->result:Ljava/lang/Integer;
    :try_end_151
    .catch Ljava/net/SocketTimeoutException; {:try_start_111 .. :try_end_151} :catch_152
    .catch Ljava/lang/Exception; {:try_start_111 .. :try_end_151} :catch_154

    #@151
    goto :goto_104

    #@152
    .line 203
    .end local v13           #replyPacket:Ljava/net/DatagramPacket;
    .end local v14           #responseBuf:[B
    :catch_152
    move-exception v15

    #@153
    goto :goto_104

    #@154
    .line 205
    :catch_154
    move-exception v8

    #@155
    .line 209
    .restart local v8       #e:Ljava/lang/Exception;
    const/4 v15, -0x2

    #@156
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@159
    move-result-object v15

    #@15a
    iput-object v15, v5, Landroid/net/DnsPinger$ActivePing;->result:Ljava/lang/Integer;

    #@15c
    goto :goto_104

    #@15d
    .line 212
    .end local v5           #curPing:Landroid/net/DnsPinger$ActivePing;
    .end local v8           #e:Ljava/lang/Exception;
    :cond_15d
    move-object/from16 v0, p0

    #@15f
    iget-object v15, v0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@161
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@164
    move-result-object v10

    #@165
    .line 213
    .local v10, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/net/DnsPinger$ActivePing;>;"
    :cond_165
    :goto_165
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@168
    move-result v15

    #@169
    if-eqz v15, :cond_1c6

    #@16b
    .line 214
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16e
    move-result-object v5

    #@16f
    check-cast v5, Landroid/net/DnsPinger$ActivePing;

    #@171
    .line 215
    .restart local v5       #curPing:Landroid/net/DnsPinger$ActivePing;
    iget-object v15, v5, Landroid/net/DnsPinger$ActivePing;->result:Ljava/lang/Integer;

    #@173
    if-eqz v15, :cond_195

    #@175
    .line 216
    iget v15, v5, Landroid/net/DnsPinger$ActivePing;->internalId:I

    #@177
    iget-short v0, v5, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@179
    move/from16 v16, v0

    #@17b
    iget-object v0, v5, Landroid/net/DnsPinger$ActivePing;->result:Ljava/lang/Integer;

    #@17d
    move-object/from16 v17, v0

    #@17f
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    #@182
    move-result v17

    #@183
    move-object/from16 v0, p0

    #@185
    move/from16 v1, v16

    #@187
    move/from16 v2, v17

    #@189
    invoke-direct {v0, v15, v1, v2}, Landroid/net/DnsPinger;->sendResponse(III)V

    #@18c
    .line 217
    iget-object v15, v5, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@18e
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    #@191
    .line 218
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    #@194
    goto :goto_165

    #@195
    .line 219
    :cond_195
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@198
    move-result-wide v15

    #@199
    iget-wide v0, v5, Landroid/net/DnsPinger$ActivePing;->start:J

    #@19b
    move-wide/from16 v17, v0

    #@19d
    iget v0, v5, Landroid/net/DnsPinger$ActivePing;->timeout:I

    #@19f
    move/from16 v19, v0

    #@1a1
    move/from16 v0, v19

    #@1a3
    int-to-long v0, v0

    #@1a4
    move-wide/from16 v19, v0

    #@1a6
    add-long v17, v17, v19

    #@1a8
    cmp-long v15, v15, v17

    #@1aa
    if-lez v15, :cond_165

    #@1ac
    .line 221
    iget v15, v5, Landroid/net/DnsPinger$ActivePing;->internalId:I

    #@1ae
    iget-short v0, v5, Landroid/net/DnsPinger$ActivePing;->packetId:S

    #@1b0
    move/from16 v16, v0

    #@1b2
    const/16 v17, -0x1

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    move/from16 v1, v16

    #@1b8
    move/from16 v2, v17

    #@1ba
    invoke-direct {v0, v15, v1, v2}, Landroid/net/DnsPinger;->sendResponse(III)V

    #@1bd
    .line 222
    iget-object v15, v5, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@1bf
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    #@1c2
    .line 223
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    #@1c5
    goto :goto_165

    #@1c6
    .line 226
    .end local v5           #curPing:Landroid/net/DnsPinger$ActivePing;
    :cond_1c6
    move-object/from16 v0, p0

    #@1c8
    iget-object v15, v0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@1ca
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    #@1cd
    move-result v15

    #@1ce
    if-nez v15, :cond_7

    #@1d0
    .line 227
    const v15, 0x50002

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    iget v0, v0, Landroid/net/DnsPinger;->mEventCounter:I

    #@1d7
    move/from16 v16, v0

    #@1d9
    const/16 v17, 0x0

    #@1db
    move-object/from16 v0, p0

    #@1dd
    move/from16 v1, v16

    #@1df
    move/from16 v2, v17

    #@1e1
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/DnsPinger;->obtainMessage(III)Landroid/os/Message;

    #@1e4
    move-result-object v15

    #@1e5
    const-wide/16 v16, 0xc8

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    move-wide/from16 v1, v16

    #@1eb
    invoke-virtual {v0, v15, v1, v2}, Landroid/net/DnsPinger;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1ee
    goto/16 :goto_7

    #@1f0
    .line 232
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v10           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/net/DnsPinger$ActivePing;>;"
    :pswitch_1f0
    move-object/from16 v0, p0

    #@1f2
    iget-object v15, v0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@1f4
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1f7
    move-result-object v9

    #@1f8
    .restart local v9       #i$:Ljava/util/Iterator;
    :goto_1f8
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@1fb
    move-result v15

    #@1fc
    if-eqz v15, :cond_20a

    #@1fe
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@201
    move-result-object v3

    #@202
    check-cast v3, Landroid/net/DnsPinger$ActivePing;

    #@204
    .line 233
    .local v3, activePing:Landroid/net/DnsPinger$ActivePing;
    iget-object v15, v3, Landroid/net/DnsPinger$ActivePing;->socket:Ljava/net/DatagramSocket;

    #@206
    invoke-virtual {v15}, Ljava/net/DatagramSocket;->close()V

    #@209
    goto :goto_1f8

    #@20a
    .line 234
    .end local v3           #activePing:Landroid/net/DnsPinger$ActivePing;
    :cond_20a
    move-object/from16 v0, p0

    #@20c
    iget-object v15, v0, Landroid/net/DnsPinger;->mActivePings:Ljava/util/List;

    #@20e
    invoke-interface {v15}, Ljava/util/List;->clear()V

    #@211
    goto/16 :goto_7

    #@213
    .line 137
    nop

    #@214
    :pswitch_data_214
    .packed-switch 0x50001
        :pswitch_8
        :pswitch_ee
        :pswitch_1f0
    .end packed-switch
.end method

.method public pingDnsAsync(Ljava/net/InetAddress;II)I
    .registers 8
    .parameter "dns"
    .parameter "timeout"
    .parameter "delay"

    #@0
    .prologue
    .line 268
    sget-object v1, Landroid/net/DnsPinger;->sCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@5
    move-result v0

    #@6
    .line 269
    .local v0, id:I
    const v1, 0x50001

    #@9
    new-instance v2, Landroid/net/DnsPinger$DnsArg;

    #@b
    iget-object v3, p0, Landroid/net/DnsPinger;->mCurrentToken:Ljava/util/concurrent/atomic/AtomicInteger;

    #@d
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@10
    move-result v3

    #@11
    invoke-direct {v2, p0, p1, v3}, Landroid/net/DnsPinger$DnsArg;-><init>(Landroid/net/DnsPinger;Ljava/net/InetAddress;I)V

    #@14
    invoke-virtual {p0, v1, v0, p2, v2}, Landroid/net/DnsPinger;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@17
    move-result-object v1

    #@18
    int-to-long v2, p3

    #@19
    invoke-virtual {p0, v1, v2, v3}, Landroid/net/DnsPinger;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1c
    .line 271
    return v0
.end method
