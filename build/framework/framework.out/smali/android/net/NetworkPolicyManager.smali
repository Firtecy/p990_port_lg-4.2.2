.class public Landroid/net/NetworkPolicyManager;
.super Ljava/lang/Object;
.source "NetworkPolicyManager.java"


# static fields
.field private static final ALLOW_PLATFORM_APP_POLICY:Z = true

.field public static final EXTRA_NETWORK_TEMPLATE:Ljava/lang/String; = "android.net.NETWORK_TEMPLATE"

.field public static final POLICY_NONE:I = 0x0

.field public static final POLICY_REJECT_METERED_BACKGROUND:I = 0x1

.field public static final RULE_ALLOW_ALL:I = 0x0

.field public static final RULE_REJECT_METERED:I = 0x1


# instance fields
.field private mService:Landroid/net/INetworkPolicyManager;


# direct methods
.method public constructor <init>(Landroid/net/INetworkPolicyManager;)V
    .registers 4
    .parameter "service"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 65
    if-nez p1, :cond_e

    #@5
    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "missing INetworkPolicyManager"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 68
    :cond_e
    iput-object p1, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@10
    .line 69
    return-void
.end method

.method public static computeLastCycleBoundary(JLandroid/net/NetworkPolicy;)J
    .registers 10
    .parameter "currentTime"
    .parameter "policy"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 157
    iget v3, p2, Landroid/net/NetworkPolicy;->cycleDay:I

    #@4
    const/4 v4, -0x1

    #@5
    if-ne v3, v4, :cond_f

    #@7
    .line 158
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v4, "Unable to compute boundary without cycleDay"

    #@b
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v3

    #@f
    .line 161
    :cond_f
    new-instance v2, Landroid/text/format/Time;

    #@11
    iget-object v3, p2, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@13
    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@16
    .line 162
    .local v2, now:Landroid/text/format/Time;
    invoke-virtual {v2, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@19
    .line 165
    new-instance v0, Landroid/text/format/Time;

    #@1b
    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    #@1e
    .line 166
    .local v0, cycle:Landroid/text/format/Time;
    iput v6, v0, Landroid/text/format/Time;->second:I

    #@20
    iput v6, v0, Landroid/text/format/Time;->minute:I

    #@22
    iput v6, v0, Landroid/text/format/Time;->hour:I

    #@24
    .line 167
    iget v3, p2, Landroid/net/NetworkPolicy;->cycleDay:I

    #@26
    invoke-static {v0, v3}, Landroid/net/NetworkPolicyManager;->snapToCycleDay(Landroid/text/format/Time;I)V

    #@29
    .line 169
    invoke-static {v0, v2}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    #@2c
    move-result v3

    #@2d
    if-ltz v3, :cond_4d

    #@2f
    .line 172
    new-instance v1, Landroid/text/format/Time;

    #@31
    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    #@34
    .line 173
    .local v1, lastMonth:Landroid/text/format/Time;
    iput v6, v1, Landroid/text/format/Time;->second:I

    #@36
    iput v6, v1, Landroid/text/format/Time;->minute:I

    #@38
    iput v6, v1, Landroid/text/format/Time;->hour:I

    #@3a
    .line 174
    iput v5, v1, Landroid/text/format/Time;->monthDay:I

    #@3c
    .line 175
    iget v3, v1, Landroid/text/format/Time;->month:I

    #@3e
    add-int/lit8 v3, v3, -0x1

    #@40
    iput v3, v1, Landroid/text/format/Time;->month:I

    #@42
    .line 176
    invoke-virtual {v1, v5}, Landroid/text/format/Time;->normalize(Z)J

    #@45
    .line 178
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    #@48
    .line 179
    iget v3, p2, Landroid/net/NetworkPolicy;->cycleDay:I

    #@4a
    invoke-static {v0, v3}, Landroid/net/NetworkPolicyManager;->snapToCycleDay(Landroid/text/format/Time;I)V

    #@4d
    .line 182
    .end local v1           #lastMonth:Landroid/text/format/Time;
    :cond_4d
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    #@50
    move-result-wide v3

    #@51
    return-wide v3
.end method

.method public static computeNextCycleBoundary(JLandroid/net/NetworkPolicy;)J
    .registers 10
    .parameter "currentTime"
    .parameter "policy"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 187
    iget v3, p2, Landroid/net/NetworkPolicy;->cycleDay:I

    #@4
    const/4 v4, -0x1

    #@5
    if-ne v3, v4, :cond_f

    #@7
    .line 188
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v4, "Unable to compute boundary without cycleDay"

    #@b
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v3

    #@f
    .line 191
    :cond_f
    new-instance v2, Landroid/text/format/Time;

    #@11
    iget-object v3, p2, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@13
    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@16
    .line 192
    .local v2, now:Landroid/text/format/Time;
    invoke-virtual {v2, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@19
    .line 195
    new-instance v0, Landroid/text/format/Time;

    #@1b
    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    #@1e
    .line 196
    .local v0, cycle:Landroid/text/format/Time;
    iput v6, v0, Landroid/text/format/Time;->second:I

    #@20
    iput v6, v0, Landroid/text/format/Time;->minute:I

    #@22
    iput v6, v0, Landroid/text/format/Time;->hour:I

    #@24
    .line 197
    iget v3, p2, Landroid/net/NetworkPolicy;->cycleDay:I

    #@26
    invoke-static {v0, v3}, Landroid/net/NetworkPolicyManager;->snapToCycleDay(Landroid/text/format/Time;I)V

    #@29
    .line 199
    invoke-static {v0, v2}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    #@2c
    move-result v3

    #@2d
    if-gtz v3, :cond_4d

    #@2f
    .line 202
    new-instance v1, Landroid/text/format/Time;

    #@31
    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    #@34
    .line 203
    .local v1, nextMonth:Landroid/text/format/Time;
    iput v6, v1, Landroid/text/format/Time;->second:I

    #@36
    iput v6, v1, Landroid/text/format/Time;->minute:I

    #@38
    iput v6, v1, Landroid/text/format/Time;->hour:I

    #@3a
    .line 204
    iput v5, v1, Landroid/text/format/Time;->monthDay:I

    #@3c
    .line 205
    iget v3, v1, Landroid/text/format/Time;->month:I

    #@3e
    add-int/lit8 v3, v3, 0x1

    #@40
    iput v3, v1, Landroid/text/format/Time;->month:I

    #@42
    .line 206
    invoke-virtual {v1, v5}, Landroid/text/format/Time;->normalize(Z)J

    #@45
    .line 208
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    #@48
    .line 209
    iget v3, p2, Landroid/net/NetworkPolicy;->cycleDay:I

    #@4a
    invoke-static {v0, v3}, Landroid/net/NetworkPolicyManager;->snapToCycleDay(Landroid/text/format/Time;I)V

    #@4d
    .line 212
    .end local v1           #nextMonth:Landroid/text/format/Time;
    :cond_4d
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    #@50
    move-result-wide v3

    #@51
    return-wide v3
.end method

.method public static dumpPolicy(Ljava/io/PrintWriter;I)V
    .registers 3
    .parameter "fout"
    .parameter "policy"

    #@0
    .prologue
    .line 273
    const-string v0, "["

    #@2
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@5
    .line 274
    and-int/lit8 v0, p1, 0x1

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 275
    const-string v0, "REJECT_METERED_BACKGROUND"

    #@b
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@e
    .line 277
    :cond_e
    const-string v0, "]"

    #@10
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@13
    .line 278
    return-void
.end method

.method public static dumpRules(Ljava/io/PrintWriter;I)V
    .registers 3
    .parameter "fout"
    .parameter "rules"

    #@0
    .prologue
    .line 282
    const-string v0, "["

    #@2
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@5
    .line 283
    and-int/lit8 v0, p1, 0x1

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 284
    const-string v0, "REJECT_METERED"

    #@b
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@e
    .line 286
    :cond_e
    const-string v0, "]"

    #@10
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    #@13
    .line 287
    return-void
.end method

.method public static from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 72
    const-string/jumbo v0, "netpolicy"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/net/NetworkPolicyManager;

    #@9
    return-object v0
.end method

.method public static isUidValidForPolicy(Landroid/content/Context;I)Z
    .registers 3
    .parameter "context"
    .parameter "uid"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 240
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 241
    const/4 v0, 0x0

    #@7
    .line 268
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    goto :goto_7
.end method

.method public static snapToCycleDay(Landroid/text/format/Time;I)V
    .registers 4
    .parameter "time"
    .parameter "cycleDay"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 222
    const/4 v0, 0x4

    #@2
    invoke-virtual {p0, v0}, Landroid/text/format/Time;->getActualMaximum(I)I

    #@5
    move-result v0

    #@6
    if-le p1, v0, :cond_17

    #@8
    .line 224
    iget v0, p0, Landroid/text/format/Time;->month:I

    #@a
    add-int/lit8 v0, v0, 0x1

    #@c
    iput v0, p0, Landroid/text/format/Time;->month:I

    #@e
    .line 225
    iput v1, p0, Landroid/text/format/Time;->monthDay:I

    #@10
    .line 226
    const/4 v0, -0x1

    #@11
    iput v0, p0, Landroid/text/format/Time;->second:I

    #@13
    .line 230
    :goto_13
    invoke-virtual {p0, v1}, Landroid/text/format/Time;->normalize(Z)J

    #@16
    .line 231
    return-void

    #@17
    .line 228
    :cond_17
    iput p1, p0, Landroid/text/format/Time;->monthDay:I

    #@19
    goto :goto_13
.end method


# virtual methods
.method public getNetworkPolicies()[Landroid/net/NetworkPolicy;
    .registers 3

    #@0
    .prologue
    .line 127
    :try_start_0
    iget-object v1, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v1}, Landroid/net/INetworkPolicyManager;->getNetworkPolicies()[Landroid/net/NetworkPolicy;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 129
    :goto_6
    return-object v1

    #@7
    .line 128
    :catch_7
    move-exception v0

    #@8
    .line 129
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getRestrictBackground()Z
    .registers 3

    #@0
    .prologue
    .line 142
    :try_start_0
    iget-object v1, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v1}, Landroid/net/INetworkPolicyManager;->getRestrictBackground()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 144
    :goto_6
    return v1

    #@7
    .line 143
    :catch_7
    move-exception v0

    #@8
    .line 144
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getUidPolicy(I)I
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 90
    :try_start_0
    iget-object v1, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/INetworkPolicyManager;->getUidPolicy(I)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 92
    :goto_6
    return v1

    #@7
    .line 91
    :catch_7
    move-exception v0

    #@8
    .line 92
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getUidsWithPolicy(I)[I
    .registers 4
    .parameter "policy"

    #@0
    .prologue
    .line 98
    :try_start_0
    iget-object v1, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/INetworkPolicyManager;->getUidsWithPolicy(I)[I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 100
    :goto_6
    return-object v1

    #@7
    .line 99
    :catch_7
    move-exception v0

    #@8
    .line 100
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [I

    #@b
    goto :goto_6
.end method

.method public registerListener(Landroid/net/INetworkPolicyListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/INetworkPolicyManager;->registerListener(Landroid/net/INetworkPolicyListener;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 109
    :goto_5
    return-void

    #@6
    .line 107
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setNetworkPolicies([Landroid/net/NetworkPolicy;)V
    .registers 3
    .parameter "policies"

    #@0
    .prologue
    .line 120
    :try_start_0
    iget-object v0, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/INetworkPolicyManager;->setNetworkPolicies([Landroid/net/NetworkPolicy;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 123
    :goto_5
    return-void

    #@6
    .line 121
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setRestrictBackground(Z)V
    .registers 3
    .parameter "restrictBackground"

    #@0
    .prologue
    .line 135
    :try_start_0
    iget-object v0, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/INetworkPolicyManager;->setRestrictBackground(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 138
    :goto_5
    return-void

    #@6
    .line 136
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setUidPolicy(II)V
    .registers 4
    .parameter "uid"
    .parameter "policy"

    #@0
    .prologue
    .line 83
    :try_start_0
    iget-object v0, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/net/INetworkPolicyManager;->setUidPolicy(II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 86
    :goto_5
    return-void

    #@6
    .line 84
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public unregisterListener(Landroid/net/INetworkPolicyListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 113
    :try_start_0
    iget-object v0, p0, Landroid/net/NetworkPolicyManager;->mService:Landroid/net/INetworkPolicyManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/INetworkPolicyManager;->unregisterListener(Landroid/net/INetworkPolicyListener;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 116
    :goto_5
    return-void

    #@6
    .line 114
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method
