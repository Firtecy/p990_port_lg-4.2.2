.class public abstract Landroid/net/IThrottleManager$Stub;
.super Landroid/os/Binder;
.source "IThrottleManager.java"

# interfaces
.implements Landroid/net/IThrottleManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/IThrottleManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/IThrottleManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.IThrottleManager"

.field static final TRANSACTION_getByteCount:I = 0x1

.field static final TRANSACTION_getCliffLevel:I = 0x6

.field static final TRANSACTION_getCliffThreshold:I = 0x5

.field static final TRANSACTION_getHelpUri:I = 0x7

.field static final TRANSACTION_getPeriodStartTime:I = 0x4

.field static final TRANSACTION_getResetTime:I = 0x3

.field static final TRANSACTION_getThrottle:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.net.IThrottleManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/IThrottleManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/IThrottleManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.net.IThrottleManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/IThrottleManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/net/IThrottleManager;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/net/IThrottleManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/IThrottleManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 41
    sparse-switch p1, :sswitch_data_ae

    #@4
    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v6

    #@8
    :goto_8
    return v6

    #@9
    .line 45
    :sswitch_9
    const-string v7, "android.net.IThrottleManager"

    #@b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 50
    :sswitch_f
    const-string v7, "android.net.IThrottleManager"

    #@11
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 54
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 56
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v2

    #@20
    .line 58
    .local v2, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    .line 59
    .local v3, _arg3:I
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/net/IThrottleManager$Stub;->getByteCount(Ljava/lang/String;III)J

    #@27
    move-result-wide v4

    #@28
    .line 60
    .local v4, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b
    .line 61
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@2e
    goto :goto_8

    #@2f
    .line 66
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:I
    .end local v3           #_arg3:I
    .end local v4           #_result:J
    :sswitch_2f
    const-string v7, "android.net.IThrottleManager"

    #@31
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@34
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    .line 69
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IThrottleManager$Stub;->getThrottle(Ljava/lang/String;)I

    #@3b
    move-result v4

    #@3c
    .line 70
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    .line 71
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    goto :goto_8

    #@43
    .line 76
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:I
    :sswitch_43
    const-string v7, "android.net.IThrottleManager"

    #@45
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48
    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4b
    move-result-object v0

    #@4c
    .line 79
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IThrottleManager$Stub;->getResetTime(Ljava/lang/String;)J

    #@4f
    move-result-wide v4

    #@50
    .line 80
    .local v4, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@53
    .line 81
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@56
    goto :goto_8

    #@57
    .line 86
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:J
    :sswitch_57
    const-string v7, "android.net.IThrottleManager"

    #@59
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5f
    move-result-object v0

    #@60
    .line 89
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IThrottleManager$Stub;->getPeriodStartTime(Ljava/lang/String;)J

    #@63
    move-result-wide v4

    #@64
    .line 90
    .restart local v4       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    .line 91
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@6a
    goto :goto_8

    #@6b
    .line 96
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:J
    :sswitch_6b
    const-string v7, "android.net.IThrottleManager"

    #@6d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@73
    move-result-object v0

    #@74
    .line 100
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v1

    #@78
    .line 101
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/IThrottleManager$Stub;->getCliffThreshold(Ljava/lang/String;I)J

    #@7b
    move-result-wide v4

    #@7c
    .line 102
    .restart local v4       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7f
    .line 103
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@82
    goto :goto_8

    #@83
    .line 108
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v4           #_result:J
    :sswitch_83
    const-string v7, "android.net.IThrottleManager"

    #@85
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@88
    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8b
    move-result-object v0

    #@8c
    .line 112
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8f
    move-result v1

    #@90
    .line 113
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/IThrottleManager$Stub;->getCliffLevel(Ljava/lang/String;I)I

    #@93
    move-result v4

    #@94
    .line 114
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@97
    .line 115
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@9a
    goto/16 :goto_8

    #@9c
    .line 120
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v4           #_result:I
    :sswitch_9c
    const-string v7, "android.net.IThrottleManager"

    #@9e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 121
    invoke-virtual {p0}, Landroid/net/IThrottleManager$Stub;->getHelpUri()Ljava/lang/String;

    #@a4
    move-result-object v4

    #@a5
    .line 122
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a8
    .line 123
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@ab
    goto/16 :goto_8

    #@ad
    .line 41
    nop

    #@ae
    :sswitch_data_ae
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2f
        0x3 -> :sswitch_43
        0x4 -> :sswitch_57
        0x5 -> :sswitch_6b
        0x6 -> :sswitch_83
        0x7 -> :sswitch_9c
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
