.class public abstract Landroid/net/IConnectivityManager$Stub;
.super Landroid/os/Binder;
.source "IConnectivityManager.java"

# interfaces
.implements Landroid/net/IConnectivityManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/IConnectivityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/IConnectivityManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.net.IConnectivityManager"

.field static final TRANSACTION_captivePortalCheckComplete:I = 0x37

.field static final TRANSACTION_clearTetheringBlockNotification:I = 0x34

.field static final TRANSACTION_establishVpn:I = 0x2c

.field static final TRANSACTION_getActiveLinkProperties:I = 0x8

.field static final TRANSACTION_getActiveNetworkInfo:I = 0x3

.field static final TRANSACTION_getActiveNetworkInfoForUid:I = 0x4

.field static final TRANSACTION_getActiveNetworkQuotaInfo:I = 0xb

.field static final TRANSACTION_getAllNetworkInfo:I = 0x6

.field static final TRANSACTION_getAllNetworkState:I = 0xa

.field static final TRANSACTION_getGlobalProxy:I = 0x25

.field static final TRANSACTION_getLastTetherError:I = 0x19

.field static final TRANSACTION_getLegacyVpnInfo:I = 0x2e

.field static final TRANSACTION_getLinkProperties:I = 0x9

.field static final TRANSACTION_getMobileDataEnabled:I = 0x13

.field static final TRANSACTION_getMobileDataEnabledByUser:I = 0x15

.field static final TRANSACTION_getNetworkInfo:I = 0x5

.field static final TRANSACTION_getNetworkPreference:I = 0x2

.field static final TRANSACTION_getProxy:I = 0x27

.field static final TRANSACTION_getTetherableBluetoothRegexs:I = 0x21

.field static final TRANSACTION_getTetherableIfaces:I = 0x1b

.field static final TRANSACTION_getTetherableUsbRegexs:I = 0x1f

.field static final TRANSACTION_getTetherableWifiRegexs:I = 0x20

.field static final TRANSACTION_getTetheredIfacePairs:I = 0x1d

.field static final TRANSACTION_getTetheredIfaces:I = 0x1c

.field static final TRANSACTION_getTetheringErroredIfaces:I = 0x1e

.field static final TRANSACTION_handleConnectMobile:I = 0x31

.field static final TRANSACTION_handleDisconnectMobile:I = 0x30

.field static final TRANSACTION_isActiveNetworkMetered:I = 0xc

.field static final TRANSACTION_isNetworkSupported:I = 0x7

.field static final TRANSACTION_isTetheringSupported:I = 0x1a

.field static final TRANSACTION_mobileDataPdpReset:I = 0x36

.field static final TRANSACTION_prepareVpn:I = 0x2a

.field static final TRANSACTION_prepareVpnEx:I = 0x2b

.field static final TRANSACTION_protectVpn:I = 0x29

.field static final TRANSACTION_reportInetCondition:I = 0x24

.field static final TRANSACTION_requestNetworkTransitionWakelock:I = 0x23

.field static final TRANSACTION_requestRemRouteToHostAddress:I = 0x32

.field static final TRANSACTION_requestRouteToHost:I = 0x11

.field static final TRANSACTION_requestRouteToHostAddress:I = 0x12

.field static final TRANSACTION_setDataConnectionMessanger:I = 0x35

.field static final TRANSACTION_setDataDependency:I = 0x28

.field static final TRANSACTION_setGlobalProxy:I = 0x26

.field static final TRANSACTION_setMobileDataEnabled:I = 0x14

.field static final TRANSACTION_setNetworkPreference:I = 0x1

.field static final TRANSACTION_setPolicyDataEnable:I = 0x16

.field static final TRANSACTION_setRadio:I = 0xe

.field static final TRANSACTION_setRadios:I = 0xd

.field static final TRANSACTION_setUsbTethering:I = 0x22

.field static final TRANSACTION_showTetheringBlockNotification:I = 0x33

.field static final TRANSACTION_startLegacyVpn:I = 0x2d

.field static final TRANSACTION_startUsingNetworkFeature:I = 0xf

.field static final TRANSACTION_stopUsingNetworkFeature:I = 0x10

.field static final TRANSACTION_tether:I = 0x17

.field static final TRANSACTION_untether:I = 0x18

.field static final TRANSACTION_updateLockdownVpn:I = 0x2f


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.net.IConnectivityManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/net/IConnectivityManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.net.IConnectivityManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/net/IConnectivityManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/net/IConnectivityManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/net/IConnectivityManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/net/IConnectivityManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_518

    #@5
    .line 652
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 47
    :sswitch_a
    const-string v4, "android.net.IConnectivityManager"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 52
    :sswitch_10
    const-string v4, "android.net.IConnectivityManager"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 55
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->setNetworkPreference(I)V

    #@1c
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f
    goto :goto_9

    #@20
    .line 61
    .end local v0           #_arg0:I
    :sswitch_20
    const-string v4, "android.net.IConnectivityManager"

    #@22
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 62
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getNetworkPreference()I

    #@28
    move-result v3

    #@29
    .line 63
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c
    .line 64
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    goto :goto_9

    #@30
    .line 69
    .end local v3           #_result:I
    :sswitch_30
    const-string v6, "android.net.IConnectivityManager"

    #@32
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 70
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@38
    move-result-object v3

    #@39
    .line 71
    .local v3, _result:Landroid/net/NetworkInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 72
    if-eqz v3, :cond_45

    #@3e
    .line 73
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 74
    invoke-virtual {v3, p3, v5}, Landroid/net/NetworkInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    goto :goto_9

    #@45
    .line 77
    :cond_45
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    goto :goto_9

    #@49
    .line 83
    .end local v3           #_result:Landroid/net/NetworkInfo;
    :sswitch_49
    const-string v6, "android.net.IConnectivityManager"

    #@4b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v0

    #@52
    .line 86
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->getActiveNetworkInfoForUid(I)Landroid/net/NetworkInfo;

    #@55
    move-result-object v3

    #@56
    .line 87
    .restart local v3       #_result:Landroid/net/NetworkInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@59
    .line 88
    if-eqz v3, :cond_62

    #@5b
    .line 89
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5e
    .line 90
    invoke-virtual {v3, p3, v5}, Landroid/net/NetworkInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@61
    goto :goto_9

    #@62
    .line 93
    :cond_62
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    goto :goto_9

    #@66
    .line 99
    .end local v0           #_arg0:I
    .end local v3           #_result:Landroid/net/NetworkInfo;
    :sswitch_66
    const-string v6, "android.net.IConnectivityManager"

    #@68
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6b
    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6e
    move-result v0

    #@6f
    .line 102
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@72
    move-result-object v3

    #@73
    .line 103
    .restart local v3       #_result:Landroid/net/NetworkInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@76
    .line 104
    if-eqz v3, :cond_7f

    #@78
    .line 105
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    .line 106
    invoke-virtual {v3, p3, v5}, Landroid/net/NetworkInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@7e
    goto :goto_9

    #@7f
    .line 109
    :cond_7f
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@82
    goto :goto_9

    #@83
    .line 115
    .end local v0           #_arg0:I
    .end local v3           #_result:Landroid/net/NetworkInfo;
    :sswitch_83
    const-string v4, "android.net.IConnectivityManager"

    #@85
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@88
    .line 116
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    #@8b
    move-result-object v3

    #@8c
    .line 117
    .local v3, _result:[Landroid/net/NetworkInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8f
    .line 118
    invoke-virtual {p3, v3, v5}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@92
    goto/16 :goto_9

    #@94
    .line 123
    .end local v3           #_result:[Landroid/net/NetworkInfo;
    :sswitch_94
    const-string v6, "android.net.IConnectivityManager"

    #@96
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@99
    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9c
    move-result v0

    #@9d
    .line 126
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->isNetworkSupported(I)Z

    #@a0
    move-result v3

    #@a1
    .line 127
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a4
    .line 128
    if-eqz v3, :cond_a7

    #@a6
    move v4, v5

    #@a7
    :cond_a7
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@aa
    goto/16 :goto_9

    #@ac
    .line 133
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_ac
    const-string v6, "android.net.IConnectivityManager"

    #@ae
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b1
    .line 134
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getActiveLinkProperties()Landroid/net/LinkProperties;

    #@b4
    move-result-object v3

    #@b5
    .line 135
    .local v3, _result:Landroid/net/LinkProperties;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b8
    .line 136
    if-eqz v3, :cond_c2

    #@ba
    .line 137
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@bd
    .line 138
    invoke-virtual {v3, p3, v5}, Landroid/net/LinkProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@c0
    goto/16 :goto_9

    #@c2
    .line 141
    :cond_c2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@c5
    goto/16 :goto_9

    #@c7
    .line 147
    .end local v3           #_result:Landroid/net/LinkProperties;
    :sswitch_c7
    const-string v6, "android.net.IConnectivityManager"

    #@c9
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cc
    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cf
    move-result v0

    #@d0
    .line 150
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@d3
    move-result-object v3

    #@d4
    .line 151
    .restart local v3       #_result:Landroid/net/LinkProperties;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d7
    .line 152
    if-eqz v3, :cond_e1

    #@d9
    .line 153
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@dc
    .line 154
    invoke-virtual {v3, p3, v5}, Landroid/net/LinkProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@df
    goto/16 :goto_9

    #@e1
    .line 157
    :cond_e1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@e4
    goto/16 :goto_9

    #@e6
    .line 163
    .end local v0           #_arg0:I
    .end local v3           #_result:Landroid/net/LinkProperties;
    :sswitch_e6
    const-string v4, "android.net.IConnectivityManager"

    #@e8
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@eb
    .line 164
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getAllNetworkState()[Landroid/net/NetworkState;

    #@ee
    move-result-object v3

    #@ef
    .line 165
    .local v3, _result:[Landroid/net/NetworkState;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f2
    .line 166
    invoke-virtual {p3, v3, v5}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@f5
    goto/16 :goto_9

    #@f7
    .line 171
    .end local v3           #_result:[Landroid/net/NetworkState;
    :sswitch_f7
    const-string v6, "android.net.IConnectivityManager"

    #@f9
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fc
    .line 172
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getActiveNetworkQuotaInfo()Landroid/net/NetworkQuotaInfo;

    #@ff
    move-result-object v3

    #@100
    .line 173
    .local v3, _result:Landroid/net/NetworkQuotaInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@103
    .line 174
    if-eqz v3, :cond_10d

    #@105
    .line 175
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@108
    .line 176
    invoke-virtual {v3, p3, v5}, Landroid/net/NetworkQuotaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@10b
    goto/16 :goto_9

    #@10d
    .line 179
    :cond_10d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@110
    goto/16 :goto_9

    #@112
    .line 185
    .end local v3           #_result:Landroid/net/NetworkQuotaInfo;
    :sswitch_112
    const-string v6, "android.net.IConnectivityManager"

    #@114
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@117
    .line 186
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->isActiveNetworkMetered()Z

    #@11a
    move-result v3

    #@11b
    .line 187
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11e
    .line 188
    if-eqz v3, :cond_121

    #@120
    move v4, v5

    #@121
    :cond_121
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@124
    goto/16 :goto_9

    #@126
    .line 193
    .end local v3           #_result:Z
    :sswitch_126
    const-string v6, "android.net.IConnectivityManager"

    #@128
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12b
    .line 195
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12e
    move-result v6

    #@12f
    if-eqz v6, :cond_141

    #@131
    move v0, v5

    #@132
    .line 196
    .local v0, _arg0:Z
    :goto_132
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->setRadios(Z)Z

    #@135
    move-result v3

    #@136
    .line 197
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@139
    .line 198
    if-eqz v3, :cond_13c

    #@13b
    move v4, v5

    #@13c
    :cond_13c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@13f
    goto/16 :goto_9

    #@141
    .end local v0           #_arg0:Z
    .end local v3           #_result:Z
    :cond_141
    move v0, v4

    #@142
    .line 195
    goto :goto_132

    #@143
    .line 203
    :sswitch_143
    const-string v6, "android.net.IConnectivityManager"

    #@145
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@148
    .line 205
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14b
    move-result v0

    #@14c
    .line 207
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14f
    move-result v6

    #@150
    if-eqz v6, :cond_162

    #@152
    move v1, v5

    #@153
    .line 208
    .local v1, _arg1:Z
    :goto_153
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->setRadio(IZ)Z

    #@156
    move-result v3

    #@157
    .line 209
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@15a
    .line 210
    if-eqz v3, :cond_15d

    #@15c
    move v4, v5

    #@15d
    :cond_15d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@160
    goto/16 :goto_9

    #@162
    .end local v1           #_arg1:Z
    .end local v3           #_result:Z
    :cond_162
    move v1, v4

    #@163
    .line 207
    goto :goto_153

    #@164
    .line 215
    .end local v0           #_arg0:I
    :sswitch_164
    const-string v4, "android.net.IConnectivityManager"

    #@166
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@169
    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16c
    move-result v0

    #@16d
    .line 219
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@170
    move-result-object v1

    #@171
    .line 221
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@174
    move-result-object v2

    #@175
    .line 222
    .local v2, _arg2:Landroid/os/IBinder;
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/IConnectivityManager$Stub;->startUsingNetworkFeature(ILjava/lang/String;Landroid/os/IBinder;)I

    #@178
    move-result v3

    #@179
    .line 223
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17c
    .line 224
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@17f
    goto/16 :goto_9

    #@181
    .line 229
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Landroid/os/IBinder;
    .end local v3           #_result:I
    :sswitch_181
    const-string v4, "android.net.IConnectivityManager"

    #@183
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@186
    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@189
    move-result v0

    #@18a
    .line 233
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18d
    move-result-object v1

    #@18e
    .line 234
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->stopUsingNetworkFeature(ILjava/lang/String;)I

    #@191
    move-result v3

    #@192
    .line 235
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@195
    .line 236
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@198
    goto/16 :goto_9

    #@19a
    .line 241
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_19a
    const-string v6, "android.net.IConnectivityManager"

    #@19c
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19f
    .line 243
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a2
    move-result v0

    #@1a3
    .line 245
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a6
    move-result v1

    #@1a7
    .line 246
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->requestRouteToHost(II)Z

    #@1aa
    move-result v3

    #@1ab
    .line 247
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ae
    .line 248
    if-eqz v3, :cond_1b1

    #@1b0
    move v4, v5

    #@1b1
    :cond_1b1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1b4
    goto/16 :goto_9

    #@1b6
    .line 253
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v3           #_result:Z
    :sswitch_1b6
    const-string v6, "android.net.IConnectivityManager"

    #@1b8
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1bb
    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1be
    move-result v0

    #@1bf
    .line 257
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@1c2
    move-result-object v1

    #@1c3
    .line 258
    .local v1, _arg1:[B
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->requestRouteToHostAddress(I[B)Z

    #@1c6
    move-result v3

    #@1c7
    .line 259
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ca
    .line 260
    if-eqz v3, :cond_1cd

    #@1cc
    move v4, v5

    #@1cd
    :cond_1cd
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1d0
    goto/16 :goto_9

    #@1d2
    .line 265
    .end local v0           #_arg0:I
    .end local v1           #_arg1:[B
    .end local v3           #_result:Z
    :sswitch_1d2
    const-string v6, "android.net.IConnectivityManager"

    #@1d4
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d7
    .line 266
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getMobileDataEnabled()Z

    #@1da
    move-result v3

    #@1db
    .line 267
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1de
    .line 268
    if-eqz v3, :cond_1e1

    #@1e0
    move v4, v5

    #@1e1
    :cond_1e1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e4
    goto/16 :goto_9

    #@1e6
    .line 273
    .end local v3           #_result:Z
    :sswitch_1e6
    const-string v6, "android.net.IConnectivityManager"

    #@1e8
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1eb
    .line 275
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ee
    move-result v6

    #@1ef
    if-eqz v6, :cond_1fa

    #@1f1
    move v0, v5

    #@1f2
    .line 276
    .local v0, _arg0:Z
    :goto_1f2
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->setMobileDataEnabled(Z)V

    #@1f5
    .line 277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f8
    goto/16 :goto_9

    #@1fa
    .end local v0           #_arg0:Z
    :cond_1fa
    move v0, v4

    #@1fb
    .line 275
    goto :goto_1f2

    #@1fc
    .line 282
    :sswitch_1fc
    const-string v6, "android.net.IConnectivityManager"

    #@1fe
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@201
    .line 283
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getMobileDataEnabledByUser()Z

    #@204
    move-result v3

    #@205
    .line 284
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@208
    .line 285
    if-eqz v3, :cond_20b

    #@20a
    move v4, v5

    #@20b
    :cond_20b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@20e
    goto/16 :goto_9

    #@210
    .line 290
    .end local v3           #_result:Z
    :sswitch_210
    const-string v6, "android.net.IConnectivityManager"

    #@212
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@215
    .line 292
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@218
    move-result v0

    #@219
    .line 294
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21c
    move-result v6

    #@21d
    if-eqz v6, :cond_228

    #@21f
    move v1, v5

    #@220
    .line 295
    .local v1, _arg1:Z
    :goto_220
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->setPolicyDataEnable(IZ)V

    #@223
    .line 296
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@226
    goto/16 :goto_9

    #@228
    .end local v1           #_arg1:Z
    :cond_228
    move v1, v4

    #@229
    .line 294
    goto :goto_220

    #@22a
    .line 301
    .end local v0           #_arg0:I
    :sswitch_22a
    const-string v4, "android.net.IConnectivityManager"

    #@22c
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22f
    .line 303
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@232
    move-result-object v0

    #@233
    .line 304
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->tether(Ljava/lang/String;)I

    #@236
    move-result v3

    #@237
    .line 305
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23a
    .line 306
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@23d
    goto/16 :goto_9

    #@23f
    .line 311
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_23f
    const-string v4, "android.net.IConnectivityManager"

    #@241
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@244
    .line 313
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@247
    move-result-object v0

    #@248
    .line 314
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->untether(Ljava/lang/String;)I

    #@24b
    move-result v3

    #@24c
    .line 315
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24f
    .line 316
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@252
    goto/16 :goto_9

    #@254
    .line 321
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_254
    const-string v4, "android.net.IConnectivityManager"

    #@256
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@259
    .line 323
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25c
    move-result-object v0

    #@25d
    .line 324
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->getLastTetherError(Ljava/lang/String;)I

    #@260
    move-result v3

    #@261
    .line 325
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@264
    .line 326
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@267
    goto/16 :goto_9

    #@269
    .line 331
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:I
    :sswitch_269
    const-string v6, "android.net.IConnectivityManager"

    #@26b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@26e
    .line 332
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->isTetheringSupported()Z

    #@271
    move-result v3

    #@272
    .line 333
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@275
    .line 334
    if-eqz v3, :cond_278

    #@277
    move v4, v5

    #@278
    :cond_278
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@27b
    goto/16 :goto_9

    #@27d
    .line 339
    .end local v3           #_result:Z
    :sswitch_27d
    const-string v4, "android.net.IConnectivityManager"

    #@27f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@282
    .line 340
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetherableIfaces()[Ljava/lang/String;

    #@285
    move-result-object v3

    #@286
    .line 341
    .local v3, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@289
    .line 342
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@28c
    goto/16 :goto_9

    #@28e
    .line 347
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_28e
    const-string v4, "android.net.IConnectivityManager"

    #@290
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@293
    .line 348
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetheredIfaces()[Ljava/lang/String;

    #@296
    move-result-object v3

    #@297
    .line 349
    .restart local v3       #_result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@29a
    .line 350
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@29d
    goto/16 :goto_9

    #@29f
    .line 355
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_29f
    const-string v4, "android.net.IConnectivityManager"

    #@2a1
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a4
    .line 356
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetheredIfacePairs()[Ljava/lang/String;

    #@2a7
    move-result-object v3

    #@2a8
    .line 357
    .restart local v3       #_result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ab
    .line 358
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@2ae
    goto/16 :goto_9

    #@2b0
    .line 363
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_2b0
    const-string v4, "android.net.IConnectivityManager"

    #@2b2
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b5
    .line 364
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetheringErroredIfaces()[Ljava/lang/String;

    #@2b8
    move-result-object v3

    #@2b9
    .line 365
    .restart local v3       #_result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2bc
    .line 366
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@2bf
    goto/16 :goto_9

    #@2c1
    .line 371
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_2c1
    const-string v4, "android.net.IConnectivityManager"

    #@2c3
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c6
    .line 372
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetherableUsbRegexs()[Ljava/lang/String;

    #@2c9
    move-result-object v3

    #@2ca
    .line 373
    .restart local v3       #_result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2cd
    .line 374
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@2d0
    goto/16 :goto_9

    #@2d2
    .line 379
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_2d2
    const-string v4, "android.net.IConnectivityManager"

    #@2d4
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d7
    .line 380
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetherableWifiRegexs()[Ljava/lang/String;

    #@2da
    move-result-object v3

    #@2db
    .line 381
    .restart local v3       #_result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2de
    .line 382
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@2e1
    goto/16 :goto_9

    #@2e3
    .line 387
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_2e3
    const-string v4, "android.net.IConnectivityManager"

    #@2e5
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e8
    .line 388
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    #@2eb
    move-result-object v3

    #@2ec
    .line 389
    .restart local v3       #_result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ef
    .line 390
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@2f2
    goto/16 :goto_9

    #@2f4
    .line 395
    .end local v3           #_result:[Ljava/lang/String;
    :sswitch_2f4
    const-string v6, "android.net.IConnectivityManager"

    #@2f6
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f9
    .line 397
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2fc
    move-result v6

    #@2fd
    if-eqz v6, :cond_30c

    #@2ff
    move v0, v5

    #@300
    .line 398
    .local v0, _arg0:Z
    :goto_300
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->setUsbTethering(Z)I

    #@303
    move-result v3

    #@304
    .line 399
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@307
    .line 400
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@30a
    goto/16 :goto_9

    #@30c
    .end local v0           #_arg0:Z
    .end local v3           #_result:I
    :cond_30c
    move v0, v4

    #@30d
    .line 397
    goto :goto_300

    #@30e
    .line 405
    :sswitch_30e
    const-string v4, "android.net.IConnectivityManager"

    #@310
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@313
    .line 407
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@316
    move-result-object v0

    #@317
    .line 408
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->requestNetworkTransitionWakelock(Ljava/lang/String;)V

    #@31a
    .line 409
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31d
    goto/16 :goto_9

    #@31f
    .line 414
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_31f
    const-string v4, "android.net.IConnectivityManager"

    #@321
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@324
    .line 416
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@327
    move-result v0

    #@328
    .line 418
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@32b
    move-result v1

    #@32c
    .line 419
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->reportInetCondition(II)V

    #@32f
    .line 420
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@332
    goto/16 :goto_9

    #@334
    .line 425
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_334
    const-string v6, "android.net.IConnectivityManager"

    #@336
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@339
    .line 426
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getGlobalProxy()Landroid/net/ProxyProperties;

    #@33c
    move-result-object v3

    #@33d
    .line 427
    .local v3, _result:Landroid/net/ProxyProperties;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@340
    .line 428
    if-eqz v3, :cond_34a

    #@342
    .line 429
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@345
    .line 430
    invoke-virtual {v3, p3, v5}, Landroid/net/ProxyProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@348
    goto/16 :goto_9

    #@34a
    .line 433
    :cond_34a
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@34d
    goto/16 :goto_9

    #@34f
    .line 439
    .end local v3           #_result:Landroid/net/ProxyProperties;
    :sswitch_34f
    const-string v4, "android.net.IConnectivityManager"

    #@351
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@354
    .line 441
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@357
    move-result v4

    #@358
    if-eqz v4, :cond_36a

    #@35a
    .line 442
    sget-object v4, Landroid/net/ProxyProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@35c
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@35f
    move-result-object v0

    #@360
    check-cast v0, Landroid/net/ProxyProperties;

    #@362
    .line 447
    .local v0, _arg0:Landroid/net/ProxyProperties;
    :goto_362
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->setGlobalProxy(Landroid/net/ProxyProperties;)V

    #@365
    .line 448
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@368
    goto/16 :goto_9

    #@36a
    .line 445
    .end local v0           #_arg0:Landroid/net/ProxyProperties;
    :cond_36a
    const/4 v0, 0x0

    #@36b
    .restart local v0       #_arg0:Landroid/net/ProxyProperties;
    goto :goto_362

    #@36c
    .line 453
    .end local v0           #_arg0:Landroid/net/ProxyProperties;
    :sswitch_36c
    const-string v6, "android.net.IConnectivityManager"

    #@36e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@371
    .line 454
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getProxy()Landroid/net/ProxyProperties;

    #@374
    move-result-object v3

    #@375
    .line 455
    .restart local v3       #_result:Landroid/net/ProxyProperties;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@378
    .line 456
    if-eqz v3, :cond_382

    #@37a
    .line 457
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@37d
    .line 458
    invoke-virtual {v3, p3, v5}, Landroid/net/ProxyProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@380
    goto/16 :goto_9

    #@382
    .line 461
    :cond_382
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@385
    goto/16 :goto_9

    #@387
    .line 467
    .end local v3           #_result:Landroid/net/ProxyProperties;
    :sswitch_387
    const-string v6, "android.net.IConnectivityManager"

    #@389
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38c
    .line 469
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@38f
    move-result v0

    #@390
    .line 471
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@393
    move-result v6

    #@394
    if-eqz v6, :cond_39f

    #@396
    move v1, v5

    #@397
    .line 472
    .local v1, _arg1:Z
    :goto_397
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->setDataDependency(IZ)V

    #@39a
    .line 473
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@39d
    goto/16 :goto_9

    #@39f
    .end local v1           #_arg1:Z
    :cond_39f
    move v1, v4

    #@3a0
    .line 471
    goto :goto_397

    #@3a1
    .line 478
    .end local v0           #_arg0:I
    :sswitch_3a1
    const-string v6, "android.net.IConnectivityManager"

    #@3a3
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a6
    .line 480
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3a9
    move-result v6

    #@3aa
    if-eqz v6, :cond_3c3

    #@3ac
    .line 481
    sget-object v6, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3ae
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3b1
    move-result-object v0

    #@3b2
    check-cast v0, Landroid/os/ParcelFileDescriptor;

    #@3b4
    .line 486
    .local v0, _arg0:Landroid/os/ParcelFileDescriptor;
    :goto_3b4
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->protectVpn(Landroid/os/ParcelFileDescriptor;)Z

    #@3b7
    move-result v3

    #@3b8
    .line 487
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3bb
    .line 488
    if-eqz v3, :cond_3be

    #@3bd
    move v4, v5

    #@3be
    :cond_3be
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3c1
    goto/16 :goto_9

    #@3c3
    .line 484
    .end local v0           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_result:Z
    :cond_3c3
    const/4 v0, 0x0

    #@3c4
    .restart local v0       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_3b4

    #@3c5
    .line 493
    .end local v0           #_arg0:Landroid/os/ParcelFileDescriptor;
    :sswitch_3c5
    const-string v6, "android.net.IConnectivityManager"

    #@3c7
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ca
    .line 495
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3cd
    move-result-object v0

    #@3ce
    .line 497
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3d1
    move-result-object v1

    #@3d2
    .line 498
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->prepareVpn(Ljava/lang/String;Ljava/lang/String;)Z

    #@3d5
    move-result v3

    #@3d6
    .line 499
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d9
    .line 500
    if-eqz v3, :cond_3dc

    #@3db
    move v4, v5

    #@3dc
    :cond_3dc
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3df
    goto/16 :goto_9

    #@3e1
    .line 505
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_3e1
    const-string v6, "android.net.IConnectivityManager"

    #@3e3
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e6
    .line 507
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3e9
    move-result-object v0

    #@3ea
    .line 509
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ed
    move-result-object v1

    #@3ee
    .line 511
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3f1
    move-result v6

    #@3f2
    if-eqz v6, :cond_404

    #@3f4
    move v2, v5

    #@3f5
    .line 512
    .local v2, _arg2:Z
    :goto_3f5
    invoke-virtual {p0, v0, v1, v2}, Landroid/net/IConnectivityManager$Stub;->prepareVpnEx(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@3f8
    move-result v3

    #@3f9
    .line 513
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3fc
    .line 514
    if-eqz v3, :cond_3ff

    #@3fe
    move v4, v5

    #@3ff
    :cond_3ff
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@402
    goto/16 :goto_9

    #@404
    .end local v2           #_arg2:Z
    .end local v3           #_result:Z
    :cond_404
    move v2, v4

    #@405
    .line 511
    goto :goto_3f5

    #@406
    .line 519
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_406
    const-string v6, "android.net.IConnectivityManager"

    #@408
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40b
    .line 521
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@40e
    move-result v6

    #@40f
    if-eqz v6, :cond_42a

    #@411
    .line 522
    sget-object v6, Lcom/android/internal/net/VpnConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    #@413
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@416
    move-result-object v0

    #@417
    check-cast v0, Lcom/android/internal/net/VpnConfig;

    #@419
    .line 527
    .local v0, _arg0:Lcom/android/internal/net/VpnConfig;
    :goto_419
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->establishVpn(Lcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;

    #@41c
    move-result-object v3

    #@41d
    .line 528
    .local v3, _result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@420
    .line 529
    if-eqz v3, :cond_42c

    #@422
    .line 530
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@425
    .line 531
    invoke-virtual {v3, p3, v5}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@428
    goto/16 :goto_9

    #@42a
    .line 525
    .end local v0           #_arg0:Lcom/android/internal/net/VpnConfig;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :cond_42a
    const/4 v0, 0x0

    #@42b
    .restart local v0       #_arg0:Lcom/android/internal/net/VpnConfig;
    goto :goto_419

    #@42c
    .line 534
    .restart local v3       #_result:Landroid/os/ParcelFileDescriptor;
    :cond_42c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@42f
    goto/16 :goto_9

    #@431
    .line 540
    .end local v0           #_arg0:Lcom/android/internal/net/VpnConfig;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_431
    const-string v4, "android.net.IConnectivityManager"

    #@433
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@436
    .line 542
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@439
    move-result v4

    #@43a
    if-eqz v4, :cond_44c

    #@43c
    .line 543
    sget-object v4, Lcom/android/internal/net/VpnProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@43e
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@441
    move-result-object v0

    #@442
    check-cast v0, Lcom/android/internal/net/VpnProfile;

    #@444
    .line 548
    .local v0, _arg0:Lcom/android/internal/net/VpnProfile;
    :goto_444
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->startLegacyVpn(Lcom/android/internal/net/VpnProfile;)V

    #@447
    .line 549
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44a
    goto/16 :goto_9

    #@44c
    .line 546
    .end local v0           #_arg0:Lcom/android/internal/net/VpnProfile;
    :cond_44c
    const/4 v0, 0x0

    #@44d
    .restart local v0       #_arg0:Lcom/android/internal/net/VpnProfile;
    goto :goto_444

    #@44e
    .line 554
    .end local v0           #_arg0:Lcom/android/internal/net/VpnProfile;
    :sswitch_44e
    const-string v6, "android.net.IConnectivityManager"

    #@450
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@453
    .line 555
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->getLegacyVpnInfo()Lcom/android/internal/net/LegacyVpnInfo;

    #@456
    move-result-object v3

    #@457
    .line 556
    .local v3, _result:Lcom/android/internal/net/LegacyVpnInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@45a
    .line 557
    if-eqz v3, :cond_464

    #@45c
    .line 558
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@45f
    .line 559
    invoke-virtual {v3, p3, v5}, Lcom/android/internal/net/LegacyVpnInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@462
    goto/16 :goto_9

    #@464
    .line 562
    :cond_464
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@467
    goto/16 :goto_9

    #@469
    .line 568
    .end local v3           #_result:Lcom/android/internal/net/LegacyVpnInfo;
    :sswitch_469
    const-string v6, "android.net.IConnectivityManager"

    #@46b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46e
    .line 569
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->updateLockdownVpn()Z

    #@471
    move-result v3

    #@472
    .line 570
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@475
    .line 571
    if-eqz v3, :cond_478

    #@477
    move v4, v5

    #@478
    :cond_478
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@47b
    goto/16 :goto_9

    #@47d
    .line 576
    .end local v3           #_result:Z
    :sswitch_47d
    const-string v4, "android.net.IConnectivityManager"

    #@47f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@482
    .line 577
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->handleDisconnectMobile()V

    #@485
    .line 578
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@488
    goto/16 :goto_9

    #@48a
    .line 583
    :sswitch_48a
    const-string v4, "android.net.IConnectivityManager"

    #@48c
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48f
    .line 584
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->handleConnectMobile()V

    #@492
    .line 585
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@495
    goto/16 :goto_9

    #@497
    .line 590
    :sswitch_497
    const-string v6, "android.net.IConnectivityManager"

    #@499
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49c
    .line 592
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@49f
    move-result v0

    #@4a0
    .line 594
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@4a3
    move-result-object v1

    #@4a4
    .line 595
    .local v1, _arg1:[B
    invoke-virtual {p0, v0, v1}, Landroid/net/IConnectivityManager$Stub;->requestRemRouteToHostAddress(I[B)Z

    #@4a7
    move-result v3

    #@4a8
    .line 596
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4ab
    .line 597
    if-eqz v3, :cond_4ae

    #@4ad
    move v4, v5

    #@4ae
    :cond_4ae
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@4b1
    goto/16 :goto_9

    #@4b3
    .line 602
    .end local v0           #_arg0:I
    .end local v1           #_arg1:[B
    .end local v3           #_result:Z
    :sswitch_4b3
    const-string v4, "android.net.IConnectivityManager"

    #@4b5
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b8
    .line 604
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4bb
    move-result v0

    #@4bc
    .line 605
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->showTetheringBlockNotification(I)V

    #@4bf
    .line 606
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c2
    goto/16 :goto_9

    #@4c4
    .line 611
    .end local v0           #_arg0:I
    :sswitch_4c4
    const-string v4, "android.net.IConnectivityManager"

    #@4c6
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c9
    .line 612
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->clearTetheringBlockNotification()V

    #@4cc
    .line 613
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4cf
    goto/16 :goto_9

    #@4d1
    .line 618
    :sswitch_4d1
    const-string v4, "android.net.IConnectivityManager"

    #@4d3
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d6
    .line 620
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4d9
    move-result v4

    #@4da
    if-eqz v4, :cond_4ec

    #@4dc
    .line 621
    sget-object v4, Landroid/os/Messenger;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4de
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4e1
    move-result-object v0

    #@4e2
    check-cast v0, Landroid/os/Messenger;

    #@4e4
    .line 626
    .local v0, _arg0:Landroid/os/Messenger;
    :goto_4e4
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->setDataConnectionMessanger(Landroid/os/Messenger;)V

    #@4e7
    .line 627
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4ea
    goto/16 :goto_9

    #@4ec
    .line 624
    .end local v0           #_arg0:Landroid/os/Messenger;
    :cond_4ec
    const/4 v0, 0x0

    #@4ed
    .restart local v0       #_arg0:Landroid/os/Messenger;
    goto :goto_4e4

    #@4ee
    .line 632
    .end local v0           #_arg0:Landroid/os/Messenger;
    :sswitch_4ee
    const-string v4, "android.net.IConnectivityManager"

    #@4f0
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f3
    .line 633
    invoke-virtual {p0}, Landroid/net/IConnectivityManager$Stub;->mobileDataPdpReset()V

    #@4f6
    .line 634
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f9
    goto/16 :goto_9

    #@4fb
    .line 639
    :sswitch_4fb
    const-string v4, "android.net.IConnectivityManager"

    #@4fd
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@500
    .line 641
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@503
    move-result v4

    #@504
    if-eqz v4, :cond_516

    #@506
    .line 642
    sget-object v4, Landroid/net/NetworkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@508
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@50b
    move-result-object v0

    #@50c
    check-cast v0, Landroid/net/NetworkInfo;

    #@50e
    .line 647
    .local v0, _arg0:Landroid/net/NetworkInfo;
    :goto_50e
    invoke-virtual {p0, v0}, Landroid/net/IConnectivityManager$Stub;->captivePortalCheckComplete(Landroid/net/NetworkInfo;)V

    #@511
    .line 648
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@514
    goto/16 :goto_9

    #@516
    .line 645
    .end local v0           #_arg0:Landroid/net/NetworkInfo;
    :cond_516
    const/4 v0, 0x0

    #@517
    .restart local v0       #_arg0:Landroid/net/NetworkInfo;
    goto :goto_50e

    #@518
    .line 43
    :sswitch_data_518
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_30
        0x4 -> :sswitch_49
        0x5 -> :sswitch_66
        0x6 -> :sswitch_83
        0x7 -> :sswitch_94
        0x8 -> :sswitch_ac
        0x9 -> :sswitch_c7
        0xa -> :sswitch_e6
        0xb -> :sswitch_f7
        0xc -> :sswitch_112
        0xd -> :sswitch_126
        0xe -> :sswitch_143
        0xf -> :sswitch_164
        0x10 -> :sswitch_181
        0x11 -> :sswitch_19a
        0x12 -> :sswitch_1b6
        0x13 -> :sswitch_1d2
        0x14 -> :sswitch_1e6
        0x15 -> :sswitch_1fc
        0x16 -> :sswitch_210
        0x17 -> :sswitch_22a
        0x18 -> :sswitch_23f
        0x19 -> :sswitch_254
        0x1a -> :sswitch_269
        0x1b -> :sswitch_27d
        0x1c -> :sswitch_28e
        0x1d -> :sswitch_29f
        0x1e -> :sswitch_2b0
        0x1f -> :sswitch_2c1
        0x20 -> :sswitch_2d2
        0x21 -> :sswitch_2e3
        0x22 -> :sswitch_2f4
        0x23 -> :sswitch_30e
        0x24 -> :sswitch_31f
        0x25 -> :sswitch_334
        0x26 -> :sswitch_34f
        0x27 -> :sswitch_36c
        0x28 -> :sswitch_387
        0x29 -> :sswitch_3a1
        0x2a -> :sswitch_3c5
        0x2b -> :sswitch_3e1
        0x2c -> :sswitch_406
        0x2d -> :sswitch_431
        0x2e -> :sswitch_44e
        0x2f -> :sswitch_469
        0x30 -> :sswitch_47d
        0x31 -> :sswitch_48a
        0x32 -> :sswitch_497
        0x33 -> :sswitch_4b3
        0x34 -> :sswitch_4c4
        0x35 -> :sswitch_4d1
        0x36 -> :sswitch_4ee
        0x37 -> :sswitch_4fb
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
