.class final Landroid/net/RouteInfo$1;
.super Ljava/lang/Object;
.source "RouteInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/RouteInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/RouteInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 182
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/RouteInfo;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 184
    const/4 v2, 0x0

    #@2
    .line 185
    .local v2, destAddr:Ljava/net/InetAddress;
    const/4 v4, 0x0

    #@3
    .line 186
    .local v4, prefix:I
    const/4 v3, 0x0

    #@4
    .line 188
    .local v3, gateway:Ljava/net/InetAddress;
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@7
    move-result v5

    #@8
    if-ne v5, v6, :cond_16

    #@a
    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@d
    move-result-object v0

    #@e
    .line 190
    .local v0, addr:[B
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v4

    #@12
    .line 193
    :try_start_12
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_15
    .catch Ljava/net/UnknownHostException; {:try_start_12 .. :try_end_15} :catch_32

    #@15
    move-result-object v2

    #@16
    .line 197
    .end local v0           #addr:[B
    :cond_16
    :goto_16
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@19
    move-result v5

    #@1a
    if-ne v5, v6, :cond_24

    #@1c
    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@1f
    move-result-object v0

    #@20
    .line 201
    .restart local v0       #addr:[B
    :try_start_20
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_23
    .catch Ljava/net/UnknownHostException; {:try_start_20 .. :try_end_23} :catch_34

    #@23
    move-result-object v3

    #@24
    .line 205
    .end local v0           #addr:[B
    :cond_24
    :goto_24
    const/4 v1, 0x0

    #@25
    .line 207
    .local v1, dest:Landroid/net/LinkAddress;
    if-eqz v2, :cond_2c

    #@27
    .line 208
    new-instance v1, Landroid/net/LinkAddress;

    #@29
    .end local v1           #dest:Landroid/net/LinkAddress;
    invoke-direct {v1, v2, v4}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@2c
    .line 211
    .restart local v1       #dest:Landroid/net/LinkAddress;
    :cond_2c
    new-instance v5, Landroid/net/RouteInfo;

    #@2e
    invoke-direct {v5, v1, v3}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@31
    return-object v5

    #@32
    .line 194
    .end local v1           #dest:Landroid/net/LinkAddress;
    .restart local v0       #addr:[B
    :catch_32
    move-exception v5

    #@33
    goto :goto_16

    #@34
    .line 202
    :catch_34
    move-exception v5

    #@35
    goto :goto_24
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 182
    invoke-virtual {p0, p1}, Landroid/net/RouteInfo$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/RouteInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/RouteInfo;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 215
    new-array v0, p1, [Landroid/net/RouteInfo;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 182
    invoke-virtual {p0, p1}, Landroid/net/RouteInfo$1;->newArray(I)[Landroid/net/RouteInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
