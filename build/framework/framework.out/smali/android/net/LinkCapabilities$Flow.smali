.class public Landroid/net/LinkCapabilities$Flow;
.super Ljava/lang/Object;
.source "LinkCapabilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/LinkCapabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Flow"
.end annotation


# static fields
.field public static final FLOW_DIRECTION_RX:Ljava/lang/String; = "rx"

.field public static final FLOW_DIRECTION_TX:Ljava/lang/String; = "tx"

.field public static final FLOW_EVENT_STATUS_ACTIVATED:I = 0x1

.field public static final FLOW_EVENT_STATUS_DELETED:I = 0x3

.field public static final FLOW_EVENT_STATUS_DISABLED:I = 0x6

.field public static final FLOW_EVENT_STATUS_ENABLED:I = 0x5

.field public static final FLOW_EVENT_STATUS_MODIFIED:I = 0x2

.field public static final FLOW_EVENT_STATUS_SUSPENDED:I = 0x4


# instance fields
.field private mFlowID:I

.field private mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

.field private mRxFlowDescriton:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTxFlowDescriton:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mvecRxFlowFilters:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mvecTxFlowFilters:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/net/LinkCapabilities;


# direct methods
.method public constructor <init>(Landroid/net/LinkCapabilities;I)V
    .registers 4
    .parameter
    .parameter "flowID"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 206
    iput-object p1, p0, Landroid/net/LinkCapabilities$Flow;->this$0:Landroid/net/LinkCapabilities;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 199
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@8
    .line 200
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecTxFlowFilters:Ljava/util/Vector;

    #@a
    .line 202
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@c
    .line 203
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecRxFlowFilters:Ljava/util/Vector;

    #@e
    .line 207
    iput p2, p0, Landroid/net/LinkCapabilities$Flow;->mFlowID:I

    #@10
    .line 208
    sget-object v0, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@12
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

    #@14
    .line 210
    new-instance v0, Ljava/util/HashMap;

    #@16
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@19
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@1b
    .line 211
    new-instance v0, Ljava/util/Vector;

    #@1d
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@20
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecTxFlowFilters:Ljava/util/Vector;

    #@22
    .line 213
    new-instance v0, Ljava/util/HashMap;

    #@24
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@27
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@29
    .line 214
    new-instance v0, Ljava/util/Vector;

    #@2b
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@2e
    iput-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecRxFlowFilters:Ljava/util/Vector;

    #@30
    .line 215
    return-void
.end method

.method private putFlowFilter(ILjava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "idx"
    .parameter "flowDir"
    .parameter "flowFilter"

    #@0
    .prologue
    .line 346
    const-string/jumbo v0, "putFlowFilter(): Not Supported because not used."

    #@3
    invoke-static {v0}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@6
    .line 347
    return-void
.end method


# virtual methods
.method public clearFlow()V
    .registers 2

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 232
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@7
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@a
    .line 233
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecTxFlowFilters:Ljava/util/Vector;

    #@c
    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    #@f
    .line 234
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecRxFlowFilters:Ljava/util/Vector;

    #@11
    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    #@14
    .line 235
    return-void
.end method

.method public getFlowDescription(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "flowDir"
    .parameter "flowDesc"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 294
    if-nez p1, :cond_4

    #@3
    .line 299
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 296
    :cond_4
    const-string/jumbo v1, "tx"

    #@7
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_16

    #@d
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@f
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Ljava/lang/String;

    #@15
    goto :goto_3

    #@16
    .line 297
    :cond_16
    const-string/jumbo v1, "rx"

    #@19
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_3

    #@1f
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@21
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Ljava/lang/String;

    #@27
    goto :goto_3
.end method

.method public getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "flowDir"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 304
    if-nez p1, :cond_4

    #@3
    .line 323
    :cond_3
    :goto_3
    return-object v5

    #@4
    .line 306
    :cond_4
    const/4 v3, 0x0

    #@5
    .line 307
    .local v3, rets:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, "tx"

    #@8
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v6

    #@c
    if-eqz v6, :cond_45

    #@e
    iget-object v3, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@10
    .line 311
    :goto_10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    .line 312
    .local v4, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    #@16
    .line 313
    .local v1, firstTime:Z
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@19
    move-result-object v5

    #@1a
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v2

    #@1e
    .local v2, i$:Ljava/util/Iterator;
    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_57

    #@24
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Ljava/util/Map$Entry;

    #@2a
    .line 314
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_51

    #@2c
    .line 315
    const/4 v1, 0x0

    #@2d
    .line 319
    :goto_2d
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Ljava/lang/String;

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 320
    const-string v5, "="

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    .line 321
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@3e
    move-result-object v5

    #@3f
    check-cast v5, Ljava/lang/String;

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    goto :goto_1e

    #@45
    .line 308
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #firstTime:Z
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #sb:Ljava/lang/StringBuilder;
    :cond_45
    const-string/jumbo v6, "rx"

    #@48
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v6

    #@4c
    if-eqz v6, :cond_3

    #@4e
    iget-object v3, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@50
    goto :goto_10

    #@51
    .line 317
    .restart local v0       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v1       #firstTime:Z
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v4       #sb:Ljava/lang/StringBuilder;
    :cond_51
    const-string v5, ","

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    goto :goto_2d

    #@57
    .line 323
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_57
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    goto :goto_3
.end method

.method public getFlowFilter(ILjava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "idx"
    .parameter "flowDir"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 393
    if-nez p2, :cond_9

    #@3
    .line 395
    const-string v7, "getFlowFilter(): flowDir is null."

    #@5
    invoke-static {v7}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@8
    .line 439
    :goto_8
    return-object v6

    #@9
    .line 398
    :cond_9
    const-string/jumbo v7, "tx"

    #@c
    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v7

    #@10
    if-nez v7, :cond_32

    #@12
    const-string/jumbo v7, "rx"

    #@15
    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v7

    #@19
    if-nez v7, :cond_32

    #@1b
    .line 400
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v8, "getFlowFilter(): flowDir is abnormal value = "

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    invoke-static {v7}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@31
    goto :goto_8

    #@32
    .line 404
    :cond_32
    const/4 v3, 0x0

    #@33
    .line 408
    .local v3, flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_33
    const-string/jumbo v7, "tx"

    #@36
    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v7

    #@3a
    if-eqz v7, :cond_6d

    #@3c
    .line 409
    iget-object v7, p0, Landroid/net/LinkCapabilities$Flow;->mvecTxFlowFilters:Ljava/util/Vector;

    #@3e
    invoke-virtual {v7, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    #@41
    move-result-object v3

    #@42
    .end local v3           #flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    check-cast v3, Ljava/util/HashMap;
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_44} :catch_7f

    #@44
    .line 421
    .restart local v3       #flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_44
    :goto_44
    if-nez v3, :cond_87

    #@46
    .line 423
    new-instance v7, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v8, "getFlowFilter(): TFT Vector ("

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    const-string v8, ") for "

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    const-string v8, " is null."

    #@61
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v7

    #@69
    invoke-static {v7}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@6c
    goto :goto_8

    #@6d
    .line 411
    :cond_6d
    :try_start_6d
    const-string/jumbo v7, "rx"

    #@70
    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@73
    move-result v7

    #@74
    if-eqz v7, :cond_44

    #@76
    .line 412
    iget-object v7, p0, Landroid/net/LinkCapabilities$Flow;->mvecRxFlowFilters:Ljava/util/Vector;

    #@78
    invoke-virtual {v7, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    #@7b
    move-result-object v3

    #@7c
    .end local v3           #flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    check-cast v3, Ljava/util/HashMap;
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_6d .. :try_end_7e} :catch_7f

    #@7e
    .restart local v3       #flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_44

    #@7f
    .line 415
    .end local v3           #flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_7f
    move-exception v0

    #@80
    .line 417
    .local v0, e:Ljava/lang/Exception;
    const-string v7, "getFlowFilter(): Set flowFilter to null due to Exception"

    #@82
    invoke-static {v7}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@85
    .line 418
    const/4 v3, 0x0

    #@86
    .restart local v3       #flowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_44

    #@87
    .line 427
    .end local v0           #e:Ljava/lang/Exception;
    :cond_87
    new-instance v5, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    .line 428
    .local v5, sb:Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    #@8d
    .line 429
    .local v2, firstTime:Z
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@90
    move-result-object v6

    #@91
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@94
    move-result-object v4

    #@95
    .local v4, i$:Ljava/util/Iterator;
    :goto_95
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@98
    move-result v6

    #@99
    if-eqz v6, :cond_c2

    #@9b
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9e
    move-result-object v1

    #@9f
    check-cast v1, Ljava/util/Map$Entry;

    #@a1
    .line 430
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v2, :cond_bc

    #@a3
    .line 431
    const/4 v2, 0x0

    #@a4
    .line 435
    :goto_a4
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@a7
    move-result-object v6

    #@a8
    check-cast v6, Ljava/lang/String;

    #@aa
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    .line 436
    const-string v6, "="

    #@af
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    .line 437
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@b5
    move-result-object v6

    #@b6
    check-cast v6, Ljava/lang/String;

    #@b8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    goto :goto_95

    #@bc
    .line 433
    :cond_bc
    const-string v6, ","

    #@be
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    goto :goto_a4

    #@c2
    .line 439
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_c2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v6

    #@c6
    goto/16 :goto_8
.end method

.method public getFlowFilterCount(Ljava/lang/String;)I
    .registers 4
    .parameter "flowDir"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 328
    if-nez p1, :cond_4

    #@3
    .line 337
    :cond_3
    :goto_3
    return v0

    #@4
    .line 330
    :cond_4
    const-string/jumbo v1, "tx"

    #@7
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 331
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecTxFlowFilters:Ljava/util/Vector;

    #@f
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    #@12
    move-result v0

    #@13
    goto :goto_3

    #@14
    .line 333
    :cond_14
    const-string/jumbo v1, "rx"

    #@17
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_3

    #@1d
    .line 334
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mvecRxFlowFilters:Ljava/util/Vector;

    #@1f
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    #@22
    move-result v0

    #@23
    goto :goto_3
.end method

.method public getFlowFilters(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "flowDir"

    #@0
    .prologue
    .line 444
    invoke-virtual {p0, p1}, Landroid/net/LinkCapabilities$Flow;->getFlowFilterCount(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 445
    .local v0, cnt:I
    const-string v2, ""

    #@6
    .line 446
    .local v2, ret:Ljava/lang/String;
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_34

    #@9
    .line 447
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {p0, v1, p1}, Landroid/net/LinkCapabilities$Flow;->getFlowFilter(ILjava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 448
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ";"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 446
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_7

    #@34
    .line 450
    :cond_34
    return-object v2
.end method

.method public getID()I
    .registers 2

    #@0
    .prologue
    .line 239
    iget v0, p0, Landroid/net/LinkCapabilities$Flow;->mFlowID:I

    #@2
    return v0
.end method

.method public getState()Landroid/net/LinkCapabilities$FlowState;
    .registers 2

    #@0
    .prologue
    .line 251
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

    #@2
    return-object v0
.end method

.method public isFlowEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

    #@2
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->ENABLED:Landroid/net/LinkCapabilities$FlowState;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public putFlowDescription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "flowDir"
    .parameter "flowDesc"
    .parameter "value"

    #@0
    .prologue
    .line 261
    if-nez p1, :cond_3

    #@2
    .line 269
    :cond_2
    :goto_2
    return-void

    #@3
    .line 263
    :cond_3
    const-string/jumbo v0, "tx"

    #@6
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 264
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@e
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    goto :goto_2

    #@12
    .line 266
    :cond_12
    const-string/jumbo v0, "rx"

    #@15
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_2

    #@1b
    .line 267
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    goto :goto_2
.end method

.method public putFlowDescriptions(Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "flowDir"
    .parameter "flowDescs"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 273
    if-nez p1, :cond_4

    #@3
    .line 290
    :cond_3
    return-void

    #@4
    .line 275
    :cond_4
    const/4 v3, 0x0

    #@5
    .line 277
    .local v3, hmFlowDesc:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, "tx"

    #@8
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v6

    #@c
    if-eqz v6, :cond_30

    #@e
    .line 278
    iget-object v3, p0, Landroid/net/LinkCapabilities$Flow;->mTxFlowDescriton:Ljava/util/HashMap;

    #@10
    .line 284
    :cond_10
    :goto_10
    const-string v6, ","

    #@12
    invoke-virtual {p2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@17
    .local v5, len$:I
    const/4 v4, 0x0

    #@18
    .local v4, i$:I
    :goto_18
    if-ge v4, v5, :cond_3

    #@1a
    aget-object v1, v0, v4

    #@1c
    .line 286
    .local v1, flowDesc:Ljava/lang/String;
    const-string v6, "="

    #@1e
    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    .line 287
    .local v2, flow_desc:[Ljava/lang/String;
    array-length v6, v2

    #@23
    if-le v6, v8, :cond_2d

    #@25
    .line 288
    const/4 v6, 0x0

    #@26
    aget-object v6, v2, v6

    #@28
    aget-object v7, v2, v8

    #@2a
    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 284
    :cond_2d
    add-int/lit8 v4, v4, 0x1

    #@2f
    goto :goto_18

    #@30
    .line 280
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #flowDesc:Ljava/lang/String;
    .end local v2           #flow_desc:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :cond_30
    const-string/jumbo v6, "rx"

    #@33
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v6

    #@37
    if-eqz v6, :cond_10

    #@39
    .line 281
    iget-object v3, p0, Landroid/net/LinkCapabilities$Flow;->mRxFlowDescriton:Ljava/util/HashMap;

    #@3b
    goto :goto_10
.end method

.method public putFlowFilters(Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter "flowDir"
    .parameter "flowFilters"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 351
    if-nez p1, :cond_a

    #@3
    .line 353
    const-string/jumbo v10, "putFlowFilters(): flowDir is null."

    #@6
    invoke-static {v10}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@9
    .line 389
    :cond_9
    :goto_9
    return-void

    #@a
    .line 356
    :cond_a
    const-string/jumbo v10, "tx"

    #@d
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v10

    #@11
    if-nez v10, :cond_34

    #@13
    const-string/jumbo v10, "rx"

    #@16
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v10

    #@1a
    if-nez v10, :cond_34

    #@1c
    .line 358
    new-instance v10, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string/jumbo v11, "putFlowFilters(): flowDir is abnormal value = "

    #@24
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v10

    #@28
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v10

    #@2c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v10

    #@30
    invoke-static {v10}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@33
    goto :goto_9

    #@34
    .line 362
    :cond_34
    if-eqz p2, :cond_3c

    #@36
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@39
    move-result v10

    #@3a
    if-nez v10, :cond_54

    #@3c
    .line 364
    :cond_3c
    new-instance v10, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string/jumbo v11, "putFlowFilters(): flowFilters is null or empty string = "

    #@44
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v10

    #@48
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v10

    #@4c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v10

    #@50
    invoke-static {v10}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@53
    goto :goto_9

    #@54
    .line 368
    :cond_54
    const-string v10, "/"

    #@56
    invoke-virtual {p2, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    .local v0, arr$:[Ljava/lang/String;
    array-length v7, v0

    #@5b
    .local v7, len$:I
    const/4 v5, 0x0

    #@5c
    .local v5, i$:I
    move v6, v5

    #@5d
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v7           #len$:I
    .local v6, i$:I
    :goto_5d
    if-ge v6, v7, :cond_9

    #@5f
    aget-object v2, v0, v6

    #@61
    .line 370
    .local v2, flowFilter:Ljava/lang/String;
    new-instance v4, Ljava/util/HashMap;

    #@63
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@66
    .line 372
    .local v4, hmFlowFilter:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, ","

    #@68
    invoke-virtual {v2, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6b
    move-result-object v1

    #@6c
    .local v1, arr$:[Ljava/lang/String;
    array-length v8, v1

    #@6d
    .local v8, len$:I
    const/4 v5, 0x0

    #@6e
    .end local v6           #i$:I
    .restart local v5       #i$:I
    :goto_6e
    if-ge v5, v8, :cond_86

    #@70
    aget-object v9, v1, v5

    #@72
    .line 374
    .local v9, tft:Ljava/lang/String;
    const-string v10, "="

    #@74
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    .line 375
    .local v3, flow_filter:[Ljava/lang/String;
    array-length v10, v3

    #@79
    if-le v10, v12, :cond_83

    #@7b
    .line 376
    const/4 v10, 0x0

    #@7c
    aget-object v10, v3, v10

    #@7e
    aget-object v11, v3, v12

    #@80
    invoke-virtual {v4, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@83
    .line 372
    :cond_83
    add-int/lit8 v5, v5, 0x1

    #@85
    goto :goto_6e

    #@86
    .line 379
    .end local v3           #flow_filter:[Ljava/lang/String;
    .end local v9           #tft:Ljava/lang/String;
    :cond_86
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@89
    move-result v10

    #@8a
    if-nez v10, :cond_a7

    #@8c
    .line 381
    new-instance v10, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string/jumbo v11, "putFlowFilters(): hmFlowFilter size is zero for "

    #@94
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v10

    #@98
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v10

    #@9c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v10

    #@a0
    invoke-static {v10}, Landroid/net/LinkCapabilities;->log(Ljava/lang/String;)V

    #@a3
    .line 368
    :cond_a3
    :goto_a3
    add-int/lit8 v5, v6, 0x1

    #@a5
    move v6, v5

    #@a6
    .end local v5           #i$:I
    .restart local v6       #i$:I
    goto :goto_5d

    #@a7
    .line 385
    .end local v6           #i$:I
    .restart local v5       #i$:I
    :cond_a7
    const-string/jumbo v10, "tx"

    #@aa
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ad
    move-result v10

    #@ae
    if-eqz v10, :cond_b6

    #@b0
    iget-object v10, p0, Landroid/net/LinkCapabilities$Flow;->mvecTxFlowFilters:Ljava/util/Vector;

    #@b2
    invoke-virtual {v10, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@b5
    goto :goto_a3

    #@b6
    .line 386
    :cond_b6
    const-string/jumbo v10, "rx"

    #@b9
    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc
    move-result v10

    #@bd
    if-eqz v10, :cond_a3

    #@bf
    iget-object v10, p0, Landroid/net/LinkCapabilities$Flow;->mvecRxFlowFilters:Ljava/util/Vector;

    #@c1
    invoke-virtual {v10, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@c4
    goto :goto_a3
.end method

.method public setState(Landroid/net/LinkCapabilities$FlowState;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 245
    iput-object p1, p0, Landroid/net/LinkCapabilities$Flow;->mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

    #@2
    .line 246
    iget-object v0, p0, Landroid/net/LinkCapabilities$Flow;->mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

    #@4
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@6
    if-ne v0, v1, :cond_b

    #@8
    invoke-virtual {p0}, Landroid/net/LinkCapabilities$Flow;->clearFlow()V

    #@b
    .line 247
    :cond_b
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 456
    const/4 v0, 0x1

    #@1
    .line 457
    .local v0, firstTime:Z
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    .line 458
    .local v1, sb:Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "ID:"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    iget v3, p0, Landroid/net/LinkCapabilities$Flow;->mFlowID:I

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 459
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "State:"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    iget-object v3, p0, Landroid/net/LinkCapabilities$Flow;->mFlowStatus:Landroid/net/LinkCapabilities$FlowState;

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, " "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 461
    const-string v2, "TxFlowDescriton:"

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    .line 462
    const-string/jumbo v2, "tx"

    #@4a
    invoke-virtual {p0, v2}, Landroid/net/LinkCapabilities$Flow;->getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    .line 463
    const-string v2, " "

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    .line 465
    const-string v2, "RxFlowDescriton:"

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    .line 466
    const-string/jumbo v2, "rx"

    #@5e
    invoke-virtual {p0, v2}, Landroid/net/LinkCapabilities$Flow;->getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    .line 467
    const-string v2, " "

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 469
    const-string v2, "TxFlowFilter:"

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    .line 470
    const-string/jumbo v2, "tx"

    #@72
    invoke-virtual {p0, v2}, Landroid/net/LinkCapabilities$Flow;->getFlowFilters(Ljava/lang/String;)Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    .line 471
    const-string v2, " "

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    .line 473
    const-string v2, "RxFlowFilter:"

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    .line 474
    const-string/jumbo v2, "rx"

    #@86
    invoke-virtual {p0, v2}, Landroid/net/LinkCapabilities$Flow;->getFlowFilters(Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v2

    #@8a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    .line 475
    const-string v2, " "

    #@8f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    .line 477
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v2

    #@96
    return-object v2
.end method
