.class public Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;
.super Ljava/lang/Object;
.source "UrlQuerySanitizer.java"

# interfaces
.implements Landroid/net/UrlQuerySanitizer$ValueSanitizer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/UrlQuerySanitizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IllegalCharacterValueSanitizer"
.end annotation


# static fields
.field public static final ALL_BUT_NUL_AND_ANGLE_BRACKETS_LEGAL:I = 0x59f

.field public static final ALL_BUT_NUL_LEGAL:I = 0x5ff

.field public static final ALL_BUT_WHITESPACE_LEGAL:I = 0x5fc

.field public static final ALL_ILLEGAL:I = 0x0

.field public static final ALL_OK:I = 0x7ff

.field public static final ALL_WHITESPACE_OK:I = 0x3

.field public static final AMP_AND_SPACE_LEGAL:I = 0x81

.field public static final AMP_LEGAL:I = 0x80

.field public static final AMP_OK:I = 0x80

.field public static final DQUOTE_OK:I = 0x8

.field public static final GT_OK:I = 0x40

.field private static final JAVASCRIPT_PREFIX:Ljava/lang/String; = "javascript:"

.field public static final LT_OK:I = 0x20

#the value of this static final field might be set in the static constructor
.field private static final MIN_SCRIPT_PREFIX_LENGTH:I = 0x0

.field public static final NON_7_BIT_ASCII_OK:I = 0x4

.field public static final NUL_OK:I = 0x200

.field public static final OTHER_WHITESPACE_OK:I = 0x2

.field public static final PCT_OK:I = 0x100

.field public static final SCRIPT_URL_OK:I = 0x400

.field public static final SPACE_LEGAL:I = 0x1

.field public static final SPACE_OK:I = 0x1

.field public static final SQUOTE_OK:I = 0x10

.field public static final URL_AND_SPACE_LEGAL:I = 0x195

.field public static final URL_LEGAL:I = 0x194

.field private static final VBSCRIPT_PREFIX:Ljava/lang/String; = "vbscript:"


# instance fields
.field private mFlags:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 274
    const-string/jumbo v0, "javascript:"

    #@3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@6
    move-result v0

    #@7
    const-string/jumbo v1, "vbscript:"

    #@a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@d
    move-result v1

    #@e
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@11
    move-result v0

    #@12
    sput v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->MIN_SCRIPT_PREFIX_LENGTH:I

    #@14
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 283
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 284
    iput p1, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@5
    .line 285
    return-void
.end method

.method private characterIsLegal(C)Z
    .registers 5
    .parameter "c"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 391
    sparse-switch p1, :sswitch_data_62

    #@5
    .line 402
    const/16 v2, 0x20

    #@7
    if-lt p1, v2, :cond_d

    #@9
    const/16 v2, 0x7f

    #@b
    if-lt p1, v2, :cond_17

    #@d
    :cond_d
    const/16 v2, 0x80

    #@f
    if-lt p1, v2, :cond_60

    #@11
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@13
    and-int/lit8 v2, v2, 0x4

    #@15
    if-eqz v2, :cond_60

    #@17
    :cond_17
    :goto_17
    return v0

    #@18
    .line 392
    :sswitch_18
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@1a
    and-int/lit8 v2, v2, 0x1

    #@1c
    if-nez v2, :cond_17

    #@1e
    move v0, v1

    #@1f
    goto :goto_17

    #@20
    .line 394
    :sswitch_20
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@22
    and-int/lit8 v2, v2, 0x2

    #@24
    if-nez v2, :cond_17

    #@26
    move v0, v1

    #@27
    goto :goto_17

    #@28
    .line 395
    :sswitch_28
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@2a
    and-int/lit8 v2, v2, 0x8

    #@2c
    if-nez v2, :cond_17

    #@2e
    move v0, v1

    #@2f
    goto :goto_17

    #@30
    .line 396
    :sswitch_30
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@32
    and-int/lit8 v2, v2, 0x10

    #@34
    if-nez v2, :cond_17

    #@36
    move v0, v1

    #@37
    goto :goto_17

    #@38
    .line 397
    :sswitch_38
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@3a
    and-int/lit8 v2, v2, 0x20

    #@3c
    if-nez v2, :cond_17

    #@3e
    move v0, v1

    #@3f
    goto :goto_17

    #@40
    .line 398
    :sswitch_40
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@42
    and-int/lit8 v2, v2, 0x40

    #@44
    if-nez v2, :cond_17

    #@46
    move v0, v1

    #@47
    goto :goto_17

    #@48
    .line 399
    :sswitch_48
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@4a
    and-int/lit16 v2, v2, 0x80

    #@4c
    if-nez v2, :cond_17

    #@4e
    move v0, v1

    #@4f
    goto :goto_17

    #@50
    .line 400
    :sswitch_50
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@52
    and-int/lit16 v2, v2, 0x100

    #@54
    if-nez v2, :cond_17

    #@56
    move v0, v1

    #@57
    goto :goto_17

    #@58
    .line 401
    :sswitch_58
    iget v2, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@5a
    and-int/lit16 v2, v2, 0x200

    #@5c
    if-nez v2, :cond_17

    #@5e
    move v0, v1

    #@5f
    goto :goto_17

    #@60
    :cond_60
    move v0, v1

    #@61
    .line 402
    goto :goto_17

    #@62
    .line 391
    :sswitch_data_62
    .sparse-switch
        0x0 -> :sswitch_58
        0x9 -> :sswitch_20
        0xa -> :sswitch_20
        0xb -> :sswitch_20
        0xc -> :sswitch_20
        0xd -> :sswitch_20
        0x20 -> :sswitch_18
        0x22 -> :sswitch_28
        0x25 -> :sswitch_50
        0x26 -> :sswitch_48
        0x27 -> :sswitch_30
        0x3c -> :sswitch_38
        0x3e -> :sswitch_40
    .end sparse-switch
.end method

.method private isWhitespace(C)Z
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 371
    sparse-switch p1, :sswitch_data_8

    #@3
    .line 380
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 378
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 371
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xb -> :sswitch_5
        0xc -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
    .end sparse-switch
.end method

.method private trimWhitespace(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 350
    const/4 v2, 0x0

    #@1
    .line 351
    .local v2, start:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v3

    #@5
    add-int/lit8 v1, v3, -0x1

    #@7
    .line 352
    .local v1, last:I
    move v0, v1

    #@8
    .line 353
    .local v0, end:I
    :goto_8
    if-gt v2, v0, :cond_17

    #@a
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@d
    move-result v3

    #@e
    invoke-direct {p0, v3}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->isWhitespace(C)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 354
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_8

    #@17
    .line 356
    :cond_17
    :goto_17
    if-lt v0, v2, :cond_26

    #@19
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@1c
    move-result v3

    #@1d
    invoke-direct {p0, v3}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->isWhitespace(C)Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_26

    #@23
    .line 357
    add-int/lit8 v0, v0, -0x1

    #@25
    goto :goto_17

    #@26
    .line 359
    :cond_26
    if-nez v2, :cond_2b

    #@28
    if-ne v0, v1, :cond_2b

    #@2a
    .line 362
    .end local p1
    :goto_2a
    return-object p1

    #@2b
    .restart local p1
    :cond_2b
    add-int/lit8 v3, v0, 0x1

    #@2d
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@30
    move-result-object p1

    #@31
    goto :goto_2a
.end method


# virtual methods
.method public sanitize(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "value"

    #@0
    .prologue
    .line 302
    if-nez p1, :cond_4

    #@2
    .line 303
    const/4 v5, 0x0

    #@3
    .line 338
    :goto_3
    return-object v5

    #@4
    .line 305
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7
    move-result v3

    #@8
    .line 306
    .local v3, length:I
    iget v5, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@a
    and-int/lit16 v5, v5, 0x400

    #@c
    if-eqz v5, :cond_2b

    #@e
    .line 307
    sget v5, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->MIN_SCRIPT_PREFIX_LENGTH:I

    #@10
    if-lt v3, v5, :cond_2b

    #@12
    .line 308
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 309
    .local v0, asLower:Ljava/lang/String;
    const-string/jumbo v5, "javascript:"

    #@19
    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1c
    move-result v5

    #@1d
    if-nez v5, :cond_28

    #@1f
    const-string/jumbo v5, "vbscript:"

    #@22
    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@25
    move-result v5

    #@26
    if-eqz v5, :cond_2b

    #@28
    .line 311
    :cond_28
    const-string v5, ""

    #@2a
    goto :goto_3

    #@2b
    .line 318
    .end local v0           #asLower:Ljava/lang/String;
    :cond_2b
    iget v5, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@2d
    and-int/lit8 v5, v5, 0x3

    #@2f
    if-nez v5, :cond_39

    #@31
    .line 319
    invoke-direct {p0, p1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->trimWhitespace(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object p1

    #@35
    .line 322
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@38
    move-result v3

    #@39
    .line 325
    :cond_39
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@3e
    .line 326
    .local v4, stringBuilder:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@3f
    .local v2, i:I
    :goto_3f
    if-ge v2, v3, :cond_5c

    #@41
    .line 327
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@44
    move-result v1

    #@45
    .line 328
    .local v1, c:C
    invoke-direct {p0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->characterIsLegal(C)Z

    #@48
    move-result v5

    #@49
    if-nez v5, :cond_53

    #@4b
    .line 329
    iget v5, p0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;->mFlags:I

    #@4d
    and-int/lit8 v5, v5, 0x1

    #@4f
    if-eqz v5, :cond_59

    #@51
    .line 330
    const/16 v1, 0x20

    #@53
    .line 336
    :cond_53
    :goto_53
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@56
    .line 326
    add-int/lit8 v2, v2, 0x1

    #@58
    goto :goto_3f

    #@59
    .line 333
    :cond_59
    const/16 v1, 0x5f

    #@5b
    goto :goto_53

    #@5c
    .line 338
    .end local v1           #c:C
    :cond_5c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    goto :goto_3
.end method
