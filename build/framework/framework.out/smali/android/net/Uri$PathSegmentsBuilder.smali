.class Landroid/net/Uri$PathSegmentsBuilder;
.super Ljava/lang/Object;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PathSegmentsBuilder"
.end annotation


# instance fields
.field segments:[Ljava/lang/String;

.field size:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 995
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 998
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    #@6
    return-void
.end method


# virtual methods
.method add(Ljava/lang/String;)V
    .registers 6
    .parameter "segment"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1001
    iget-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@3
    if-nez v1, :cond_15

    #@5
    .line 1002
    const/4 v1, 0x4

    #@6
    new-array v1, v1, [Ljava/lang/String;

    #@8
    iput-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@a
    .line 1009
    :cond_a
    :goto_a
    iget-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@c
    iget v2, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    #@e
    add-int/lit8 v3, v2, 0x1

    #@10
    iput v3, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    #@12
    aput-object p1, v1, v2

    #@14
    .line 1010
    return-void

    #@15
    .line 1003
    :cond_15
    iget v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    #@17
    add-int/lit8 v1, v1, 0x1

    #@19
    iget-object v2, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@1b
    array-length v2, v2

    #@1c
    if-ne v1, v2, :cond_a

    #@1e
    .line 1004
    iget-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@20
    array-length v1, v1

    #@21
    mul-int/lit8 v1, v1, 0x2

    #@23
    new-array v0, v1, [Ljava/lang/String;

    #@25
    .line 1005
    .local v0, expanded:[Ljava/lang/String;
    iget-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@27
    iget-object v2, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@29
    array-length v2, v2

    #@2a
    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2d
    .line 1006
    iput-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@2f
    goto :goto_a
.end method

.method build()Landroid/net/Uri$PathSegments;
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1013
    iget-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@3
    if-nez v0, :cond_8

    #@5
    .line 1014
    sget-object v0, Landroid/net/Uri$PathSegments;->EMPTY:Landroid/net/Uri$PathSegments;

    #@7
    .line 1021
    :goto_7
    return-object v0

    #@8
    .line 1018
    :cond_8
    :try_start_8
    new-instance v0, Landroid/net/Uri$PathSegments;

    #@a
    iget-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@c
    iget v2, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    #@e
    invoke-direct {v0, v1, v2}, Landroid/net/Uri$PathSegments;-><init>([Ljava/lang/String;I)V
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_14

    #@11
    .line 1021
    iput-object v3, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@13
    goto :goto_7

    #@14
    :catchall_14
    move-exception v0

    #@15
    iput-object v3, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    #@17
    throw v0
.end method
