.class final Landroid/net/LinkAddress$1;
.super Ljava/lang/Object;
.source "LinkAddress.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/LinkAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/LinkAddress;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 130
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/LinkAddress;
    .registers 6
    .parameter "in"

    #@0
    .prologue
    .line 132
    const/4 v0, 0x0

    #@1
    .line 133
    .local v0, address:Ljava/net/InetAddress;
    const/4 v1, 0x0

    #@2
    .line 134
    .local v1, prefixLength:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@5
    move-result v2

    #@6
    const/4 v3, 0x1

    #@7
    if-ne v2, v3, :cond_15

    #@9
    .line 136
    :try_start_9
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@c
    move-result-object v2

    #@d
    invoke-static {v2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@10
    move-result-object v0

    #@11
    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
    :try_end_14
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_14} :catch_1b

    #@14
    move-result v1

    #@15
    .line 140
    :cond_15
    :goto_15
    new-instance v2, Landroid/net/LinkAddress;

    #@17
    invoke-direct {v2, v0, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@1a
    return-object v2

    #@1b
    .line 138
    :catch_1b
    move-exception v2

    #@1c
    goto :goto_15
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 130
    invoke-virtual {p0, p1}, Landroid/net/LinkAddress$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/LinkAddress;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/LinkAddress;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 144
    new-array v0, p1, [Landroid/net/LinkAddress;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 130
    invoke-virtual {p0, p1}, Landroid/net/LinkAddress$1;->newArray(I)[Landroid/net/LinkAddress;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
