.class public Landroid/net/NetworkStatsHistory;
.super Ljava/lang/Object;
.source "NetworkStatsHistory.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/NetworkStatsHistory$ParcelUtils;,
        Landroid/net/NetworkStatsHistory$DataStreamUtils;,
        Landroid/net/NetworkStatsHistory$Entry;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/NetworkStatsHistory;",
            ">;"
        }
    .end annotation
.end field

.field public static final FIELD_ACTIVE_TIME:I = 0x1

.field public static final FIELD_ALL:I = -0x1

.field public static final FIELD_OPERATIONS:I = 0x20

.field public static final FIELD_RX_BYTES:I = 0x2

.field public static final FIELD_RX_PACKETS:I = 0x4

.field public static final FIELD_TX_BYTES:I = 0x8

.field public static final FIELD_TX_PACKETS:I = 0x10

.field private static final VERSION_ADD_ACTIVE:I = 0x3

.field private static final VERSION_ADD_PACKETS:I = 0x2

.field private static final VERSION_INIT:I = 0x1


# instance fields
.field private activeTime:[J

.field private bucketCount:I

.field private bucketDuration:J

.field private bucketStart:[J

.field private operations:[J

.field private rxBytes:[J

.field private rxPackets:[J

.field private totalBytes:J

.field private txBytes:[J

.field private txPackets:[J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 606
    new-instance v0, Landroid/net/NetworkStatsHistory$1;

    #@2
    invoke-direct {v0}, Landroid/net/NetworkStatsHistory$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/NetworkStatsHistory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(J)V
    .registers 5
    .parameter "bucketDuration"

    #@0
    .prologue
    .line 95
    const/16 v0, 0xa

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/net/NetworkStatsHistory;-><init>(JII)V

    #@6
    .line 96
    return-void
.end method

.method public constructor <init>(JI)V
    .registers 5
    .parameter "bucketDuration"
    .parameter "initialSize"

    #@0
    .prologue
    .line 99
    const/4 v0, -0x1

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/net/NetworkStatsHistory;-><init>(JII)V

    #@4
    .line 100
    return-void
.end method

.method public constructor <init>(JII)V
    .registers 7
    .parameter "bucketDuration"
    .parameter "initialSize"
    .parameter "fields"

    #@0
    .prologue
    .line 102
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 103
    iput-wide p1, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@5
    .line 104
    new-array v0, p3, [J

    #@7
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@9
    .line 105
    and-int/lit8 v0, p4, 0x1

    #@b
    if-eqz v0, :cond_11

    #@d
    new-array v0, p3, [J

    #@f
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@11
    .line 106
    :cond_11
    and-int/lit8 v0, p4, 0x2

    #@13
    if-eqz v0, :cond_19

    #@15
    new-array v0, p3, [J

    #@17
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@19
    .line 107
    :cond_19
    and-int/lit8 v0, p4, 0x4

    #@1b
    if-eqz v0, :cond_21

    #@1d
    new-array v0, p3, [J

    #@1f
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@21
    .line 108
    :cond_21
    and-int/lit8 v0, p4, 0x8

    #@23
    if-eqz v0, :cond_29

    #@25
    new-array v0, p3, [J

    #@27
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@29
    .line 109
    :cond_29
    and-int/lit8 v0, p4, 0x10

    #@2b
    if-eqz v0, :cond_31

    #@2d
    new-array v0, p3, [J

    #@2f
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@31
    .line 110
    :cond_31
    and-int/lit8 v0, p4, 0x20

    #@33
    if-eqz v0, :cond_39

    #@35
    new-array v0, p3, [J

    #@37
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@39
    .line 111
    :cond_39
    const/4 v0, 0x0

    #@3a
    iput v0, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@3c
    .line 112
    const-wide/16 v0, 0x0

    #@3e
    iput-wide v0, p0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@40
    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/net/NetworkStatsHistory;J)V
    .registers 5
    .parameter "existing"
    .parameter "bucketDuration"

    #@0
    .prologue
    .line 116
    invoke-virtual {p1, p2, p3}, Landroid/net/NetworkStatsHistory;->estimateResizeBuckets(J)I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, p2, p3, v0}, Landroid/net/NetworkStatsHistory;-><init>(JI)V

    #@7
    .line 117
    invoke-virtual {p0, p1}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    #@a
    .line 118
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 120
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@6
    move-result-wide v0

    #@7
    iput-wide v0, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@9
    .line 122
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@f
    .line 123
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@15
    .line 124
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@1b
    .line 125
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@21
    .line 126
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@27
    .line 127
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@2d
    .line 128
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@33
    .line 129
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@35
    array-length v0, v0

    #@36
    iput v0, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@38
    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@3b
    move-result-wide v0

    #@3c
    iput-wide v0, p0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@3e
    .line 131
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .registers 7
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 146
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 147
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    #@6
    move-result v0

    #@7
    .line 148
    .local v0, version:I
    packed-switch v0, :pswitch_data_e6

    #@a
    .line 177
    new-instance v1, Ljava/net/ProtocolException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string/jumbo v3, "unexpected version: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {v1, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 150
    :pswitch_24
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    #@27
    move-result-wide v1

    #@28
    iput-wide v1, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@2a
    .line 151
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readFullLongArray(Ljava/io/DataInputStream;)[J

    #@2d
    move-result-object v1

    #@2e
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@30
    .line 152
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readFullLongArray(Ljava/io/DataInputStream;)[J

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@36
    .line 153
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@38
    array-length v1, v1

    #@39
    new-array v1, v1, [J

    #@3b
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@3d
    .line 154
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readFullLongArray(Ljava/io/DataInputStream;)[J

    #@40
    move-result-object v1

    #@41
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@43
    .line 155
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@45
    array-length v1, v1

    #@46
    new-array v1, v1, [J

    #@48
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@4a
    .line 156
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@4c
    array-length v1, v1

    #@4d
    new-array v1, v1, [J

    #@4f
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@51
    .line 157
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@53
    array-length v1, v1

    #@54
    iput v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@56
    .line 158
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@58
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->total([J)J

    #@5b
    move-result-wide v1

    #@5c
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@5e
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->total([J)J

    #@61
    move-result-wide v3

    #@62
    add-long/2addr v1, v3

    #@63
    iput-wide v1, p0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@65
    .line 181
    :goto_65
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@67
    array-length v1, v1

    #@68
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@6a
    if-ne v1, v2, :cond_8f

    #@6c
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@6e
    array-length v1, v1

    #@6f
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@71
    if-ne v1, v2, :cond_8f

    #@73
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@75
    array-length v1, v1

    #@76
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@78
    if-ne v1, v2, :cond_8f

    #@7a
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@7c
    array-length v1, v1

    #@7d
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@7f
    if-ne v1, v2, :cond_8f

    #@81
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@83
    array-length v1, v1

    #@84
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@86
    if-ne v1, v2, :cond_8f

    #@88
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@8a
    array-length v1, v1

    #@8b
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@8d
    if-eq v1, v2, :cond_e5

    #@8f
    .line 184
    :cond_8f
    new-instance v1, Ljava/net/ProtocolException;

    #@91
    const-string v2, "Mismatched history lengths"

    #@93
    invoke-direct {v1, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    #@96
    throw v1

    #@97
    .line 163
    :pswitch_97
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLong()J

    #@9a
    move-result-wide v1

    #@9b
    iput-wide v1, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@9d
    .line 164
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@a0
    move-result-object v1

    #@a1
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@a3
    .line 165
    const/4 v1, 0x3

    #@a4
    if-lt v0, v1, :cond_df

    #@a6
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@a9
    move-result-object v1

    #@aa
    :goto_aa
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@ac
    .line 167
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@af
    move-result-object v1

    #@b0
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@b2
    .line 168
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@b5
    move-result-object v1

    #@b6
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@b8
    .line 169
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@bb
    move-result-object v1

    #@bc
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@be
    .line 170
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@c1
    move-result-object v1

    #@c2
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@c4
    .line 171
    invoke-static {p1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->readVarLongArray(Ljava/io/DataInputStream;)[J

    #@c7
    move-result-object v1

    #@c8
    iput-object v1, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@ca
    .line 172
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@cc
    array-length v1, v1

    #@cd
    iput v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@cf
    .line 173
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@d1
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->total([J)J

    #@d4
    move-result-wide v1

    #@d5
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@d7
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->total([J)J

    #@da
    move-result-wide v3

    #@db
    add-long/2addr v1, v3

    #@dc
    iput-wide v1, p0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@de
    goto :goto_65

    #@df
    .line 165
    :cond_df
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@e1
    array-length v1, v1

    #@e2
    new-array v1, v1, [J

    #@e4
    goto :goto_aa

    #@e5
    .line 186
    :cond_e5
    return-void

    #@e6
    .line 148
    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_24
        :pswitch_97
        :pswitch_97
    .end packed-switch
.end method

.method private static addLong([JIJ)V
    .registers 6
    .parameter "array"
    .parameter "i"
    .parameter "value"

    #@0
    .prologue
    .line 627
    if-eqz p0, :cond_7

    #@2
    aget-wide v0, p0, p1

    #@4
    add-long/2addr v0, p2

    #@5
    aput-wide v0, p0, p1

    #@7
    .line 628
    :cond_7
    return-void
.end method

.method private ensureBuckets(JJ)V
    .registers 12
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 384
    iget-wide v3, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@2
    rem-long v3, p1, v3

    #@4
    sub-long/2addr p1, v3

    #@5
    .line 385
    iget-wide v3, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@7
    iget-wide v5, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@9
    rem-long v5, p3, v5

    #@b
    sub-long/2addr v3, v5

    #@c
    iget-wide v5, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@e
    rem-long/2addr v3, v5

    #@f
    add-long/2addr p3, v3

    #@10
    .line 387
    move-wide v1, p1

    #@11
    .local v1, now:J
    :goto_11
    cmp-long v3, v1, p3

    #@13
    if-gez v3, :cond_29

    #@15
    .line 389
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@17
    const/4 v4, 0x0

    #@18
    iget v5, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@1a
    invoke-static {v3, v4, v5, v1, v2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    #@1d
    move-result v0

    #@1e
    .line 390
    .local v0, index:I
    if-gez v0, :cond_25

    #@20
    .line 392
    xor-int/lit8 v3, v0, -0x1

    #@22
    invoke-direct {p0, v3, v1, v2}, Landroid/net/NetworkStatsHistory;->insertBucket(IJ)V

    #@25
    .line 387
    :cond_25
    iget-wide v3, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@27
    add-long/2addr v1, v3

    #@28
    goto :goto_11

    #@29
    .line 395
    .end local v0           #index:I
    :cond_29
    return-void
.end method

.method private static getLong([JIJ)J
    .registers 4
    .parameter "array"
    .parameter "i"
    .parameter "value"

    #@0
    .prologue
    .line 619
    if-eqz p0, :cond_4

    #@2
    aget-wide p2, p0, p1

    #@4
    .end local p2
    :cond_4
    return-wide p2
.end method

.method private insertBucket(IJ)V
    .registers 11
    .parameter "index"
    .parameter "start"

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    .line 402
    iget v3, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@4
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@6
    array-length v4, v4

    #@7
    if-lt v3, v4, :cond_66

    #@9
    .line 403
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@b
    array-length v3, v3

    #@c
    const/16 v4, 0xa

    #@e
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v3

    #@12
    mul-int/lit8 v3, v3, 0x3

    #@14
    div-int/lit8 v2, v3, 0x2

    #@16
    .line 404
    .local v2, newLength:I
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@18
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@1b
    move-result-object v3

    #@1c
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@1e
    .line 405
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@20
    if-eqz v3, :cond_2a

    #@22
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@24
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@27
    move-result-object v3

    #@28
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@2a
    .line 406
    :cond_2a
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@2c
    if-eqz v3, :cond_36

    #@2e
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@30
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@33
    move-result-object v3

    #@34
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@36
    .line 407
    :cond_36
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@38
    if-eqz v3, :cond_42

    #@3a
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@3c
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@3f
    move-result-object v3

    #@40
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@42
    .line 408
    :cond_42
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@44
    if-eqz v3, :cond_4e

    #@46
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@48
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@4b
    move-result-object v3

    #@4c
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@4e
    .line 409
    :cond_4e
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@50
    if-eqz v3, :cond_5a

    #@52
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@54
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@57
    move-result-object v3

    #@58
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@5a
    .line 410
    :cond_5a
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@5c
    if-eqz v3, :cond_66

    #@5e
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@60
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    #@63
    move-result-object v3

    #@64
    iput-object v3, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@66
    .line 414
    .end local v2           #newLength:I
    :cond_66
    iget v3, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@68
    if-ge p1, v3, :cond_b9

    #@6a
    .line 415
    add-int/lit8 v0, p1, 0x1

    #@6c
    .line 416
    .local v0, dstPos:I
    iget v3, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@6e
    sub-int v1, v3, p1

    #@70
    .line 418
    .local v1, length:I
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@72
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@74
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@77
    .line 419
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@79
    if-eqz v3, :cond_82

    #@7b
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@7d
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@7f
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@82
    .line 420
    :cond_82
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@84
    if-eqz v3, :cond_8d

    #@86
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@88
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@8a
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8d
    .line 421
    :cond_8d
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@8f
    if-eqz v3, :cond_98

    #@91
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@93
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@95
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@98
    .line 422
    :cond_98
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@9a
    if-eqz v3, :cond_a3

    #@9c
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@9e
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@a0
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a3
    .line 423
    :cond_a3
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@a5
    if-eqz v3, :cond_ae

    #@a7
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@a9
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@ab
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@ae
    .line 424
    :cond_ae
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@b0
    if-eqz v3, :cond_b9

    #@b2
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@b4
    iget-object v4, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@b6
    invoke-static {v3, p1, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@b9
    .line 427
    .end local v0           #dstPos:I
    .end local v1           #length:I
    :cond_b9
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@bb
    aput-wide p2, v3, p1

    #@bd
    .line 428
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@bf
    invoke-static {v3, p1, v5, v6}, Landroid/net/NetworkStatsHistory;->setLong([JIJ)V

    #@c2
    .line 429
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@c4
    invoke-static {v3, p1, v5, v6}, Landroid/net/NetworkStatsHistory;->setLong([JIJ)V

    #@c7
    .line 430
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@c9
    invoke-static {v3, p1, v5, v6}, Landroid/net/NetworkStatsHistory;->setLong([JIJ)V

    #@cc
    .line 431
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@ce
    invoke-static {v3, p1, v5, v6}, Landroid/net/NetworkStatsHistory;->setLong([JIJ)V

    #@d1
    .line 432
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@d3
    invoke-static {v3, p1, v5, v6}, Landroid/net/NetworkStatsHistory;->setLong([JIJ)V

    #@d6
    .line 433
    iget-object v3, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@d8
    invoke-static {v3, p1, v5, v6}, Landroid/net/NetworkStatsHistory;->setLong([JIJ)V

    #@db
    .line 434
    iget v3, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@dd
    add-int/lit8 v3, v3, 0x1

    #@df
    iput v3, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@e1
    .line 435
    return-void
.end method

.method public static randomLong(Ljava/util/Random;JJ)J
    .registers 9
    .parameter "r"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 573
    long-to-float v0, p1

    #@1
    invoke-virtual {p0}, Ljava/util/Random;->nextFloat()F

    #@4
    move-result v1

    #@5
    sub-long v2, p3, p1

    #@7
    long-to-float v2, v2

    #@8
    mul-float/2addr v1, v2

    #@9
    add-float/2addr v0, v1

    #@a
    float-to-long v0, v0

    #@b
    return-wide v0
.end method

.method private static setLong([JIJ)V
    .registers 4
    .parameter "array"
    .parameter "i"
    .parameter "value"

    #@0
    .prologue
    .line 623
    if-eqz p0, :cond_4

    #@2
    aput-wide p2, p0, p1

    #@4
    .line 624
    :cond_4
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 202
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Lcom/android/internal/util/IndentingPrintWriter;Z)V
    .registers 7
    .parameter "pw"
    .parameter "fullHistory"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 577
    const-string v2, "NetworkStatsHistory: bucketDuration="

    #@3
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@6
    iget-wide v2, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@8
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->println(J)V

    #@b
    .line 578
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@e
    .line 580
    if-eqz p2, :cond_96

    #@10
    .line 581
    .local v1, start:I
    :goto_10
    if-lez v1, :cond_1f

    #@12
    .line 582
    const-string v2, "(omitting "

    #@14
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@17
    invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->print(I)V

    #@1a
    const-string v2, " buckets)"

    #@1c
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@1f
    .line 585
    :cond_1f
    move v0, v1

    #@20
    .local v0, i:I
    :goto_20
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@22
    if-ge v0, v2, :cond_a0

    #@24
    .line 586
    const-string v2, "bucketStart="

    #@26
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@2b
    aget-wide v2, v2, v0

    #@2d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@30
    .line 587
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@32
    if-eqz v2, :cond_40

    #@34
    const-string v2, " activeTime="

    #@36
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@39
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@3b
    aget-wide v2, v2, v0

    #@3d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@40
    .line 588
    :cond_40
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@42
    if-eqz v2, :cond_50

    #@44
    const-string v2, " rxBytes="

    #@46
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@49
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@4b
    aget-wide v2, v2, v0

    #@4d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@50
    .line 589
    :cond_50
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@52
    if-eqz v2, :cond_60

    #@54
    const-string v2, " rxPackets="

    #@56
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@59
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@5b
    aget-wide v2, v2, v0

    #@5d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@60
    .line 590
    :cond_60
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@62
    if-eqz v2, :cond_70

    #@64
    const-string v2, " txBytes="

    #@66
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@69
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@6b
    aget-wide v2, v2, v0

    #@6d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@70
    .line 591
    :cond_70
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@72
    if-eqz v2, :cond_80

    #@74
    const-string v2, " txPackets="

    #@76
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@79
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@7b
    aget-wide v2, v2, v0

    #@7d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@80
    .line 592
    :cond_80
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@82
    if-eqz v2, :cond_90

    #@84
    const-string v2, " operations="

    #@86
    invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@89
    iget-object v2, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@8b
    aget-wide v2, v2, v0

    #@8d
    invoke-virtual {p1, v2, v3}, Lcom/android/internal/util/IndentingPrintWriter;->print(J)V

    #@90
    .line 593
    :cond_90
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@93
    .line 585
    add-int/lit8 v0, v0, 0x1

    #@95
    goto :goto_20

    #@96
    .line 580
    .end local v0           #i:I
    .end local v1           #start:I
    :cond_96
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@98
    add-int/lit8 v2, v2, -0x20

    #@9a
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@9d
    move-result v1

    #@9e
    goto/16 :goto_10

    #@a0
    .line 596
    .restart local v0       #i:I
    .restart local v1       #start:I
    :cond_a0
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@a3
    .line 597
    return-void
.end method

.method public estimateResizeBuckets(J)I
    .registers 7
    .parameter "newBucketDuration"

    #@0
    .prologue
    .line 631
    invoke-virtual {p0}, Landroid/net/NetworkStatsHistory;->size()I

    #@3
    move-result v0

    #@4
    int-to-long v0, v0

    #@5
    invoke-virtual {p0}, Landroid/net/NetworkStatsHistory;->getBucketDuration()J

    #@8
    move-result-wide v2

    #@9
    mul-long/2addr v0, v2

    #@a
    div-long/2addr v0, p1

    #@b
    long-to-int v0, v0

    #@c
    return v0
.end method

.method public generateRandom(JJJ)V
    .registers 26
    .parameter "start"
    .parameter "end"
    .parameter "bytes"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 528
    new-instance v17, Ljava/util/Random;

    #@2
    invoke-direct/range {v17 .. v17}, Ljava/util/Random;-><init>()V

    #@5
    .line 530
    .local v17, r:Ljava/util/Random;
    invoke-virtual/range {v17 .. v17}, Ljava/util/Random;->nextFloat()F

    #@8
    move-result v18

    #@9
    .line 531
    .local v18, fractionRx:F
    move-wide/from16 v0, p5

    #@b
    long-to-float v2, v0

    #@c
    mul-float v2, v2, v18

    #@e
    float-to-long v7, v2

    #@f
    .line 532
    .local v7, rxBytes:J
    move-wide/from16 v0, p5

    #@11
    long-to-float v2, v0

    #@12
    const/high16 v3, 0x3f80

    #@14
    sub-float v3, v3, v18

    #@16
    mul-float/2addr v2, v3

    #@17
    float-to-long v11, v2

    #@18
    .line 534
    .local v11, txBytes:J
    const-wide/16 v2, 0x400

    #@1a
    div-long v9, v7, v2

    #@1c
    .line 535
    .local v9, rxPackets:J
    const-wide/16 v2, 0x400

    #@1e
    div-long v13, v11, v2

    #@20
    .line 536
    .local v13, txPackets:J
    const-wide/16 v2, 0x800

    #@22
    div-long v15, v7, v2

    #@24
    .local v15, operations:J
    move-object/from16 v2, p0

    #@26
    move-wide/from16 v3, p1

    #@28
    move-wide/from16 v5, p3

    #@2a
    .line 538
    invoke-virtual/range {v2 .. v17}, Landroid/net/NetworkStatsHistory;->generateRandom(JJJJJJJLjava/util/Random;)V

    #@2d
    .line 539
    return-void
.end method

.method public generateRandom(JJJJJJJLjava/util/Random;)V
    .registers 36
    .parameter "start"
    .parameter "end"
    .parameter "rxBytes"
    .parameter "rxPackets"
    .parameter "txBytes"
    .parameter "txPackets"
    .parameter "operations"
    .parameter "r"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 547
    invoke-direct/range {p0 .. p4}, Landroid/net/NetworkStatsHistory;->ensureBuckets(JJ)V

    #@3
    .line 549
    new-instance v5, Landroid/net/NetworkStats$Entry;

    #@5
    sget-object v6, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@7
    const/4 v7, -0x1

    #@8
    const/4 v8, 0x0

    #@9
    const/4 v9, 0x0

    #@a
    const-wide/16 v10, 0x0

    #@c
    const-wide/16 v12, 0x0

    #@e
    const-wide/16 v14, 0x0

    #@10
    const-wide/16 v16, 0x0

    #@12
    const-wide/16 v18, 0x0

    #@14
    invoke-direct/range {v5 .. v19}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@17
    .line 552
    .local v5, entry:Landroid/net/NetworkStats$Entry;
    :goto_17
    const-wide/16 v11, 0x400

    #@19
    cmp-long v6, p5, v11

    #@1b
    if-gtz v6, :cond_35

    #@1d
    const-wide/16 v11, 0x80

    #@1f
    cmp-long v6, p7, v11

    #@21
    if-gtz v6, :cond_35

    #@23
    const-wide/16 v11, 0x400

    #@25
    cmp-long v6, p9, v11

    #@27
    if-gtz v6, :cond_35

    #@29
    const-wide/16 v11, 0x80

    #@2b
    cmp-long v6, p11, v11

    #@2d
    if-gtz v6, :cond_35

    #@2f
    const-wide/16 v11, 0x20

    #@31
    cmp-long v6, p13, v11

    #@33
    if-lez v6, :cond_a6

    #@35
    .line 553
    :cond_35
    move-object/from16 v0, p15

    #@37
    move-wide/from16 v1, p1

    #@39
    move-wide/from16 v3, p3

    #@3b
    invoke-static {v0, v1, v2, v3, v4}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@3e
    move-result-wide v7

    #@3f
    .line 554
    .local v7, curStart:J
    const-wide/16 v11, 0x0

    #@41
    sub-long v13, p3, v7

    #@43
    const-wide/16 v15, 0x2

    #@45
    div-long/2addr v13, v15

    #@46
    move-object/from16 v0, p15

    #@48
    invoke-static {v0, v11, v12, v13, v14}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@4b
    move-result-wide v11

    #@4c
    add-long v9, v7, v11

    #@4e
    .line 556
    .local v9, curEnd:J
    const-wide/16 v11, 0x0

    #@50
    move-object/from16 v0, p15

    #@52
    move-wide/from16 v1, p5

    #@54
    invoke-static {v0, v11, v12, v1, v2}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@57
    move-result-wide v11

    #@58
    iput-wide v11, v5, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@5a
    .line 557
    const-wide/16 v11, 0x0

    #@5c
    move-object/from16 v0, p15

    #@5e
    move-wide/from16 v1, p7

    #@60
    invoke-static {v0, v11, v12, v1, v2}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@63
    move-result-wide v11

    #@64
    iput-wide v11, v5, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@66
    .line 558
    const-wide/16 v11, 0x0

    #@68
    move-object/from16 v0, p15

    #@6a
    move-wide/from16 v1, p9

    #@6c
    invoke-static {v0, v11, v12, v1, v2}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@6f
    move-result-wide v11

    #@70
    iput-wide v11, v5, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@72
    .line 559
    const-wide/16 v11, 0x0

    #@74
    move-object/from16 v0, p15

    #@76
    move-wide/from16 v1, p11

    #@78
    invoke-static {v0, v11, v12, v1, v2}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@7b
    move-result-wide v11

    #@7c
    iput-wide v11, v5, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@7e
    .line 560
    const-wide/16 v11, 0x0

    #@80
    move-object/from16 v0, p15

    #@82
    move-wide/from16 v1, p13

    #@84
    invoke-static {v0, v11, v12, v1, v2}, Landroid/net/NetworkStatsHistory;->randomLong(Ljava/util/Random;JJ)J

    #@87
    move-result-wide v11

    #@88
    iput-wide v11, v5, Landroid/net/NetworkStats$Entry;->operations:J

    #@8a
    .line 562
    iget-wide v11, v5, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@8c
    sub-long p5, p5, v11

    #@8e
    .line 563
    iget-wide v11, v5, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@90
    sub-long p7, p7, v11

    #@92
    .line 564
    iget-wide v11, v5, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@94
    sub-long p9, p9, v11

    #@96
    .line 565
    iget-wide v11, v5, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@98
    sub-long p11, p11, v11

    #@9a
    .line 566
    iget-wide v11, v5, Landroid/net/NetworkStats$Entry;->operations:J

    #@9c
    sub-long p13, p13, v11

    #@9e
    move-object/from16 v6, p0

    #@a0
    move-object v11, v5

    #@a1
    .line 568
    invoke-virtual/range {v6 .. v11}, Landroid/net/NetworkStatsHistory;->recordData(JJLandroid/net/NetworkStats$Entry;)V

    #@a4
    goto/16 :goto_17

    #@a6
    .line 570
    .end local v7           #curStart:J
    .end local v9           #curEnd:J
    :cond_a6
    return-void
.end method

.method public getBucketDuration()J
    .registers 3

    #@0
    .prologue
    .line 210
    iget-wide v0, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@2
    return-wide v0
.end method

.method public getEnd()J
    .registers 5

    #@0
    .prologue
    .line 222
    iget v0, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@2
    if-lez v0, :cond_10

    #@4
    .line 223
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@6
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    aget-wide v0, v0, v1

    #@c
    iget-wide v2, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@e
    add-long/2addr v0, v2

    #@f
    .line 225
    :goto_f
    return-wide v0

    #@10
    :cond_10
    const-wide/high16 v0, -0x8000

    #@12
    goto :goto_f
.end method

.method public getIndexAfter(J)I
    .registers 7
    .parameter "time"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 255
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@3
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@5
    invoke-static {v1, v3, v2, p1, p2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    #@8
    move-result v0

    #@9
    .line 256
    .local v0, index:I
    if-gez v0, :cond_16

    #@b
    .line 257
    xor-int/lit8 v0, v0, -0x1

    #@d
    .line 261
    :goto_d
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@f
    add-int/lit8 v1, v1, -0x1

    #@11
    invoke-static {v0, v3, v1}, Landroid/util/MathUtils;->constrain(III)I

    #@14
    move-result v1

    #@15
    return v1

    #@16
    .line 259
    :cond_16
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_d
.end method

.method public getIndexBefore(J)I
    .registers 7
    .parameter "time"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 241
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@3
    iget v2, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@5
    invoke-static {v1, v3, v2, p1, p2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    #@8
    move-result v0

    #@9
    .line 242
    .local v0, index:I
    if-gez v0, :cond_18

    #@b
    .line 243
    xor-int/lit8 v1, v0, -0x1

    #@d
    add-int/lit8 v0, v1, -0x1

    #@f
    .line 247
    :goto_f
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    invoke-static {v0, v3, v1}, Landroid/util/MathUtils;->constrain(III)I

    #@16
    move-result v1

    #@17
    return v1

    #@18
    .line 245
    :cond_18
    add-int/lit8 v0, v0, -0x1

    #@1a
    goto :goto_f
.end method

.method public getStart()J
    .registers 3

    #@0
    .prologue
    .line 214
    iget v0, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@2
    if-lez v0, :cond_a

    #@4
    .line 215
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@6
    const/4 v1, 0x0

    #@7
    aget-wide v0, v0, v1

    #@9
    .line 217
    :goto_9
    return-wide v0

    #@a
    :cond_a
    const-wide v0, 0x7fffffffffffffffL

    #@f
    goto :goto_9
.end method

.method public getTotalBytes()J
    .registers 3

    #@0
    .prologue
    .line 233
    iget-wide v0, p0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@2
    return-wide v0
.end method

.method public getValues(ILandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;
    .registers 8
    .parameter "i"
    .parameter "recycle"

    #@0
    .prologue
    const-wide/16 v3, -0x1

    #@2
    .line 268
    if-eqz p2, :cond_40

    #@4
    move-object v0, p2

    #@5
    .line 269
    .local v0, entry:Landroid/net/NetworkStatsHistory$Entry;
    :goto_5
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@7
    aget-wide v1, v1, p1

    #@9
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->bucketStart:J

    #@b
    .line 270
    iget-wide v1, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@d
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->bucketDuration:J

    #@f
    .line 271
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@11
    invoke-static {v1, p1, v3, v4}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@14
    move-result-wide v1

    #@15
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->activeTime:J

    #@17
    .line 272
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@19
    invoke-static {v1, p1, v3, v4}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@1c
    move-result-wide v1

    #@1d
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    #@1f
    .line 273
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@21
    invoke-static {v1, p1, v3, v4}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@24
    move-result-wide v1

    #@25
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->rxPackets:J

    #@27
    .line 274
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@29
    invoke-static {v1, p1, v3, v4}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@2c
    move-result-wide v1

    #@2d
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    #@2f
    .line 275
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@31
    invoke-static {v1, p1, v3, v4}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@34
    move-result-wide v1

    #@35
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->txPackets:J

    #@37
    .line 276
    iget-object v1, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@39
    invoke-static {v1, p1, v3, v4}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@3c
    move-result-wide v1

    #@3d
    iput-wide v1, v0, Landroid/net/NetworkStatsHistory$Entry;->operations:J

    #@3f
    .line 277
    return-object v0

    #@40
    .line 268
    .end local v0           #entry:Landroid/net/NetworkStatsHistory$Entry;
    :cond_40
    new-instance v0, Landroid/net/NetworkStatsHistory$Entry;

    #@42
    invoke-direct {v0}, Landroid/net/NetworkStatsHistory$Entry;-><init>()V

    #@45
    goto :goto_5
.end method

.method public getValues(JJJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;
    .registers 31
    .parameter "start"
    .parameter "end"
    .parameter "now"
    .parameter "recycle"

    #@0
    .prologue
    .line 480
    if-eqz p7, :cond_83

    #@2
    move-object/from16 v8, p7

    #@4
    .line 481
    .local v8, entry:Landroid/net/NetworkStatsHistory$Entry;
    :goto_4
    sub-long v17, p3, p1

    #@6
    move-wide/from16 v0, v17

    #@8
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->bucketDuration:J

    #@a
    .line 482
    move-wide/from16 v0, p1

    #@c
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->bucketStart:J

    #@e
    .line 483
    move-object/from16 v0, p0

    #@10
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@12
    move-object/from16 v17, v0

    #@14
    if-eqz v17, :cond_8a

    #@16
    const-wide/16 v17, 0x0

    #@18
    :goto_18
    move-wide/from16 v0, v17

    #@1a
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->activeTime:J

    #@1c
    .line 484
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@20
    move-object/from16 v17, v0

    #@22
    if-eqz v17, :cond_8d

    #@24
    const-wide/16 v17, 0x0

    #@26
    :goto_26
    move-wide/from16 v0, v17

    #@28
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    #@2a
    .line 485
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@2e
    move-object/from16 v17, v0

    #@30
    if-eqz v17, :cond_90

    #@32
    const-wide/16 v17, 0x0

    #@34
    :goto_34
    move-wide/from16 v0, v17

    #@36
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxPackets:J

    #@38
    .line 486
    move-object/from16 v0, p0

    #@3a
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@3c
    move-object/from16 v17, v0

    #@3e
    if-eqz v17, :cond_93

    #@40
    const-wide/16 v17, 0x0

    #@42
    :goto_42
    move-wide/from16 v0, v17

    #@44
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    #@46
    .line 487
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@4a
    move-object/from16 v17, v0

    #@4c
    if-eqz v17, :cond_96

    #@4e
    const-wide/16 v17, 0x0

    #@50
    :goto_50
    move-wide/from16 v0, v17

    #@52
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txPackets:J

    #@54
    .line 488
    move-object/from16 v0, p0

    #@56
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@58
    move-object/from16 v17, v0

    #@5a
    if-eqz v17, :cond_99

    #@5c
    const-wide/16 v17, 0x0

    #@5e
    :goto_5e
    move-wide/from16 v0, v17

    #@60
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->operations:J

    #@62
    .line 490
    move-object/from16 v0, p0

    #@64
    move-wide/from16 v1, p3

    #@66
    invoke-virtual {v0, v1, v2}, Landroid/net/NetworkStatsHistory;->getIndexAfter(J)I

    #@69
    move-result v16

    #@6a
    .line 491
    .local v16, startIndex:I
    move/from16 v9, v16

    #@6c
    .local v9, i:I
    :goto_6c
    if-ltz v9, :cond_82

    #@6e
    .line 492
    move-object/from16 v0, p0

    #@70
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@72
    move-object/from16 v17, v0

    #@74
    aget-wide v6, v17, v9

    #@76
    .line 493
    .local v6, curStart:J
    move-object/from16 v0, p0

    #@78
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@7a
    move-wide/from16 v17, v0

    #@7c
    add-long v4, v6, v17

    #@7e
    .line 496
    .local v4, curEnd:J
    cmp-long v17, v4, p1

    #@80
    if-gtz v17, :cond_9c

    #@82
    .line 520
    .end local v4           #curEnd:J
    .end local v6           #curStart:J
    :cond_82
    return-object v8

    #@83
    .line 480
    .end local v8           #entry:Landroid/net/NetworkStatsHistory$Entry;
    .end local v9           #i:I
    .end local v16           #startIndex:I
    :cond_83
    new-instance v8, Landroid/net/NetworkStatsHistory$Entry;

    #@85
    invoke-direct {v8}, Landroid/net/NetworkStatsHistory$Entry;-><init>()V

    #@88
    goto/16 :goto_4

    #@8a
    .line 483
    .restart local v8       #entry:Landroid/net/NetworkStatsHistory$Entry;
    :cond_8a
    const-wide/16 v17, -0x1

    #@8c
    goto :goto_18

    #@8d
    .line 484
    :cond_8d
    const-wide/16 v17, -0x1

    #@8f
    goto :goto_26

    #@90
    .line 485
    :cond_90
    const-wide/16 v17, -0x1

    #@92
    goto :goto_34

    #@93
    .line 486
    :cond_93
    const-wide/16 v17, -0x1

    #@95
    goto :goto_42

    #@96
    .line 487
    :cond_96
    const-wide/16 v17, -0x1

    #@98
    goto :goto_50

    #@99
    .line 488
    :cond_99
    const-wide/16 v17, -0x1

    #@9b
    goto :goto_5e

    #@9c
    .line 498
    .restart local v4       #curEnd:J
    .restart local v6       #curStart:J
    .restart local v9       #i:I
    .restart local v16       #startIndex:I
    :cond_9c
    cmp-long v17, v6, p3

    #@9e
    if-ltz v17, :cond_a3

    #@a0
    .line 491
    :cond_a0
    :goto_a0
    add-int/lit8 v9, v9, -0x1

    #@a2
    goto :goto_6c

    #@a3
    .line 501
    :cond_a3
    cmp-long v17, v6, p5

    #@a5
    if-gez v17, :cond_192

    #@a7
    cmp-long v17, v4, p5

    #@a9
    if-lez v17, :cond_192

    #@ab
    const/4 v3, 0x1

    #@ac
    .line 503
    .local v3, activeBucket:Z
    :goto_ac
    if-eqz v3, :cond_195

    #@ae
    .line 504
    move-object/from16 v0, p0

    #@b0
    iget-wide v10, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@b2
    .line 510
    .local v10, overlap:J
    :goto_b2
    const-wide/16 v17, 0x0

    #@b4
    cmp-long v17, v10, v17

    #@b6
    if-lez v17, :cond_a0

    #@b8
    .line 513
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@bc
    move-object/from16 v17, v0

    #@be
    if-eqz v17, :cond_dc

    #@c0
    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->activeTime:J

    #@c2
    move-wide/from16 v17, v0

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@c8
    move-object/from16 v19, v0

    #@ca
    aget-wide v19, v19, v9

    #@cc
    mul-long v19, v19, v10

    #@ce
    move-object/from16 v0, p0

    #@d0
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@d2
    move-wide/from16 v21, v0

    #@d4
    div-long v19, v19, v21

    #@d6
    add-long v17, v17, v19

    #@d8
    move-wide/from16 v0, v17

    #@da
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->activeTime:J

    #@dc
    .line 514
    :cond_dc
    move-object/from16 v0, p0

    #@de
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@e0
    move-object/from16 v17, v0

    #@e2
    if-eqz v17, :cond_100

    #@e4
    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    #@e6
    move-wide/from16 v17, v0

    #@e8
    move-object/from16 v0, p0

    #@ea
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@ec
    move-object/from16 v19, v0

    #@ee
    aget-wide v19, v19, v9

    #@f0
    mul-long v19, v19, v10

    #@f2
    move-object/from16 v0, p0

    #@f4
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@f6
    move-wide/from16 v21, v0

    #@f8
    div-long v19, v19, v21

    #@fa
    add-long v17, v17, v19

    #@fc
    move-wide/from16 v0, v17

    #@fe
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    #@100
    .line 515
    :cond_100
    move-object/from16 v0, p0

    #@102
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@104
    move-object/from16 v17, v0

    #@106
    if-eqz v17, :cond_124

    #@108
    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxPackets:J

    #@10a
    move-wide/from16 v17, v0

    #@10c
    move-object/from16 v0, p0

    #@10e
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@110
    move-object/from16 v19, v0

    #@112
    aget-wide v19, v19, v9

    #@114
    mul-long v19, v19, v10

    #@116
    move-object/from16 v0, p0

    #@118
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@11a
    move-wide/from16 v21, v0

    #@11c
    div-long v19, v19, v21

    #@11e
    add-long v17, v17, v19

    #@120
    move-wide/from16 v0, v17

    #@122
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->rxPackets:J

    #@124
    .line 516
    :cond_124
    move-object/from16 v0, p0

    #@126
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@128
    move-object/from16 v17, v0

    #@12a
    if-eqz v17, :cond_148

    #@12c
    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    #@12e
    move-wide/from16 v17, v0

    #@130
    move-object/from16 v0, p0

    #@132
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@134
    move-object/from16 v19, v0

    #@136
    aget-wide v19, v19, v9

    #@138
    mul-long v19, v19, v10

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@13e
    move-wide/from16 v21, v0

    #@140
    div-long v19, v19, v21

    #@142
    add-long v17, v17, v19

    #@144
    move-wide/from16 v0, v17

    #@146
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    #@148
    .line 517
    :cond_148
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@14c
    move-object/from16 v17, v0

    #@14e
    if-eqz v17, :cond_16c

    #@150
    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txPackets:J

    #@152
    move-wide/from16 v17, v0

    #@154
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@158
    move-object/from16 v19, v0

    #@15a
    aget-wide v19, v19, v9

    #@15c
    mul-long v19, v19, v10

    #@15e
    move-object/from16 v0, p0

    #@160
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@162
    move-wide/from16 v21, v0

    #@164
    div-long v19, v19, v21

    #@166
    add-long v17, v17, v19

    #@168
    move-wide/from16 v0, v17

    #@16a
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->txPackets:J

    #@16c
    .line 518
    :cond_16c
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@170
    move-object/from16 v17, v0

    #@172
    if-eqz v17, :cond_a0

    #@174
    iget-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->operations:J

    #@176
    move-wide/from16 v17, v0

    #@178
    move-object/from16 v0, p0

    #@17a
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@17c
    move-object/from16 v19, v0

    #@17e
    aget-wide v19, v19, v9

    #@180
    mul-long v19, v19, v10

    #@182
    move-object/from16 v0, p0

    #@184
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@186
    move-wide/from16 v21, v0

    #@188
    div-long v19, v19, v21

    #@18a
    add-long v17, v17, v19

    #@18c
    move-wide/from16 v0, v17

    #@18e
    iput-wide v0, v8, Landroid/net/NetworkStatsHistory$Entry;->operations:J

    #@190
    goto/16 :goto_a0

    #@192
    .line 501
    .end local v3           #activeBucket:Z
    .end local v10           #overlap:J
    :cond_192
    const/4 v3, 0x0

    #@193
    goto/16 :goto_ac

    #@195
    .line 506
    .restart local v3       #activeBucket:Z
    :cond_195
    cmp-long v17, v4, p3

    #@197
    if-gez v17, :cond_1a3

    #@199
    move-wide v12, v4

    #@19a
    .line 507
    .local v12, overlapEnd:J
    :goto_19a
    cmp-long v17, v6, p1

    #@19c
    if-lez v17, :cond_1a6

    #@19e
    move-wide v14, v6

    #@19f
    .line 508
    .local v14, overlapStart:J
    :goto_19f
    sub-long v10, v12, v14

    #@1a1
    .restart local v10       #overlap:J
    goto/16 :goto_b2

    #@1a3
    .end local v10           #overlap:J
    .end local v12           #overlapEnd:J
    .end local v14           #overlapStart:J
    :cond_1a3
    move-wide/from16 v12, p3

    #@1a5
    .line 506
    goto :goto_19a

    #@1a6
    .restart local v12       #overlapEnd:J
    :cond_1a6
    move-wide/from16 v14, p1

    #@1a8
    .line 507
    goto :goto_19f
.end method

.method public getValues(JJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;
    .registers 14
    .parameter "start"
    .parameter "end"
    .parameter "recycle"

    #@0
    .prologue
    .line 472
    const-wide v5, 0x7fffffffffffffffL

    #@5
    move-object v0, p0

    #@6
    move-wide v1, p1

    #@7
    move-wide v3, p3

    #@8
    move-object v7, p5

    #@9
    invoke-virtual/range {v0 .. v7}, Landroid/net/NetworkStatsHistory;->getValues(JJJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public recordData(JJJJ)V
    .registers 24
    .parameter "start"
    .parameter "end"
    .parameter "rxBytes"
    .parameter "txBytes"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 286
    new-instance v0, Landroid/net/NetworkStats$Entry;

    #@2
    sget-object v1, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@4
    const/4 v2, -0x1

    #@5
    const/4 v3, 0x0

    #@6
    const/4 v4, 0x0

    #@7
    const-wide/16 v7, 0x0

    #@9
    const-wide/16 v11, 0x0

    #@b
    const-wide/16 v13, 0x0

    #@d
    move-wide/from16 v5, p5

    #@f
    move-wide/from16 v9, p7

    #@11
    invoke-direct/range {v0 .. v14}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@14
    move-object v1, p0

    #@15
    move-wide/from16 v2, p1

    #@17
    move-wide/from16 v4, p3

    #@19
    move-object v6, v0

    #@1a
    invoke-virtual/range {v1 .. v6}, Landroid/net/NetworkStatsHistory;->recordData(JJLandroid/net/NetworkStats$Entry;)V

    #@1d
    .line 288
    return-void
.end method

.method public recordData(JJLandroid/net/NetworkStats$Entry;)V
    .registers 46
    .parameter "start"
    .parameter "end"
    .parameter "entry"

    #@0
    .prologue
    .line 295
    move-object/from16 v0, p5

    #@2
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@4
    move-wide/from16 v25, v0

    #@6
    .line 296
    .local v25, rxBytes:J
    move-object/from16 v0, p5

    #@8
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@a
    move-wide/from16 v27, v0

    #@c
    .line 297
    .local v27, rxPackets:J
    move-object/from16 v0, p5

    #@e
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@10
    move-wide/from16 v30, v0

    #@12
    .line 298
    .local v30, txBytes:J
    move-object/from16 v0, p5

    #@14
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@16
    move-wide/from16 v32, v0

    #@18
    .line 299
    .local v32, txPackets:J
    move-object/from16 v0, p5

    #@1a
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->operations:J

    #@1c
    move-wide/from16 v21, v0

    #@1e
    .line 301
    .local v21, operations:J
    invoke-virtual/range {p5 .. p5}, Landroid/net/NetworkStats$Entry;->isNegative()Z

    #@21
    move-result v34

    #@22
    if-eqz v34, :cond_2d

    #@24
    .line 302
    new-instance v34, Ljava/lang/IllegalArgumentException;

    #@26
    const-string/jumbo v35, "tried recording negative data"

    #@29
    invoke-direct/range {v34 .. v35}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v34

    #@2d
    .line 304
    :cond_2d
    invoke-virtual/range {p5 .. p5}, Landroid/net/NetworkStats$Entry;->isEmpty()Z

    #@30
    move-result v34

    #@31
    if-eqz v34, :cond_34

    #@33
    .line 344
    :goto_33
    return-void

    #@34
    .line 309
    :cond_34
    invoke-direct/range {p0 .. p4}, Landroid/net/NetworkStatsHistory;->ensureBuckets(JJ)V

    #@37
    .line 312
    sub-long v8, p3, p1

    #@39
    .line 313
    .local v8, duration:J
    move-object/from16 v0, p0

    #@3b
    move-wide/from16 v1, p3

    #@3d
    invoke-virtual {v0, v1, v2}, Landroid/net/NetworkStatsHistory;->getIndexAfter(J)I

    #@40
    move-result v29

    #@41
    .line 314
    .local v29, startIndex:I
    move/from16 v20, v29

    #@43
    .local v20, i:I
    :goto_43
    if-ltz v20, :cond_59

    #@45
    .line 315
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@49
    move-object/from16 v34, v0

    #@4b
    aget-wide v6, v34, v20

    #@4d
    .line 316
    .local v6, curStart:J
    move-object/from16 v0, p0

    #@4f
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@51
    move-wide/from16 v34, v0

    #@53
    add-long v4, v6, v34

    #@55
    .line 319
    .local v4, curEnd:J
    cmp-long v34, v4, p1

    #@57
    if-gez v34, :cond_76

    #@59
    .line 343
    .end local v4           #curEnd:J
    .end local v6           #curStart:J
    :cond_59
    move-object/from16 v0, p0

    #@5b
    iget-wide v0, v0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@5d
    move-wide/from16 v34, v0

    #@5f
    move-object/from16 v0, p5

    #@61
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@63
    move-wide/from16 v36, v0

    #@65
    move-object/from16 v0, p5

    #@67
    iget-wide v0, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@69
    move-wide/from16 v38, v0

    #@6b
    add-long v36, v36, v38

    #@6d
    add-long v34, v34, v36

    #@6f
    move-wide/from16 v0, v34

    #@71
    move-object/from16 v2, p0

    #@73
    iput-wide v0, v2, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@75
    goto :goto_33

    #@76
    .line 321
    .restart local v4       #curEnd:J
    .restart local v6       #curStart:J
    :cond_76
    cmp-long v34, v6, p3

    #@78
    if-lez v34, :cond_7d

    #@7a
    .line 314
    :cond_7a
    :goto_7a
    add-int/lit8 v20, v20, -0x1

    #@7c
    goto :goto_43

    #@7d
    .line 323
    :cond_7d
    move-wide/from16 v0, p3

    #@7f
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    #@82
    move-result-wide v34

    #@83
    move-wide/from16 v0, p1

    #@85
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    #@88
    move-result-wide v36

    #@89
    sub-long v23, v34, v36

    #@8b
    .line 324
    .local v23, overlap:J
    const-wide/16 v34, 0x0

    #@8d
    cmp-long v34, v23, v34

    #@8f
    if-lez v34, :cond_7a

    #@91
    .line 327
    mul-long v34, v25, v23

    #@93
    div-long v12, v34, v8

    #@95
    .line 328
    .local v12, fracRxBytes:J
    mul-long v34, v27, v23

    #@97
    div-long v14, v34, v8

    #@99
    .line 329
    .local v14, fracRxPackets:J
    mul-long v34, v30, v23

    #@9b
    div-long v16, v34, v8

    #@9d
    .line 330
    .local v16, fracTxBytes:J
    mul-long v34, v32, v23

    #@9f
    div-long v18, v34, v8

    #@a1
    .line 331
    .local v18, fracTxPackets:J
    mul-long v34, v21, v23

    #@a3
    div-long v10, v34, v8

    #@a5
    .line 333
    .local v10, fracOperations:J
    move-object/from16 v0, p0

    #@a7
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@a9
    move-object/from16 v34, v0

    #@ab
    move-object/from16 v0, v34

    #@ad
    move/from16 v1, v20

    #@af
    move-wide/from16 v2, v23

    #@b1
    invoke-static {v0, v1, v2, v3}, Landroid/net/NetworkStatsHistory;->addLong([JIJ)V

    #@b4
    .line 334
    move-object/from16 v0, p0

    #@b6
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@b8
    move-object/from16 v34, v0

    #@ba
    move-object/from16 v0, v34

    #@bc
    move/from16 v1, v20

    #@be
    invoke-static {v0, v1, v12, v13}, Landroid/net/NetworkStatsHistory;->addLong([JIJ)V

    #@c1
    sub-long v25, v25, v12

    #@c3
    .line 335
    move-object/from16 v0, p0

    #@c5
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@c7
    move-object/from16 v34, v0

    #@c9
    move-object/from16 v0, v34

    #@cb
    move/from16 v1, v20

    #@cd
    invoke-static {v0, v1, v14, v15}, Landroid/net/NetworkStatsHistory;->addLong([JIJ)V

    #@d0
    sub-long v27, v27, v14

    #@d2
    .line 336
    move-object/from16 v0, p0

    #@d4
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@d6
    move-object/from16 v34, v0

    #@d8
    move-object/from16 v0, v34

    #@da
    move/from16 v1, v20

    #@dc
    move-wide/from16 v2, v16

    #@de
    invoke-static {v0, v1, v2, v3}, Landroid/net/NetworkStatsHistory;->addLong([JIJ)V

    #@e1
    sub-long v30, v30, v16

    #@e3
    .line 337
    move-object/from16 v0, p0

    #@e5
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@e7
    move-object/from16 v34, v0

    #@e9
    move-object/from16 v0, v34

    #@eb
    move/from16 v1, v20

    #@ed
    move-wide/from16 v2, v18

    #@ef
    invoke-static {v0, v1, v2, v3}, Landroid/net/NetworkStatsHistory;->addLong([JIJ)V

    #@f2
    sub-long v32, v32, v18

    #@f4
    .line 338
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@f8
    move-object/from16 v34, v0

    #@fa
    move-object/from16 v0, v34

    #@fc
    move/from16 v1, v20

    #@fe
    invoke-static {v0, v1, v10, v11}, Landroid/net/NetworkStatsHistory;->addLong([JIJ)V

    #@101
    sub-long v21, v21, v10

    #@103
    .line 340
    sub-long v8, v8, v23

    #@105
    goto/16 :goto_7a
.end method

.method public recordEntireHistory(Landroid/net/NetworkStatsHistory;)V
    .registers 8
    .parameter "input"

    #@0
    .prologue
    .line 351
    const-wide/high16 v2, -0x8000

    #@2
    const-wide v4, 0x7fffffffffffffffL

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/net/NetworkStatsHistory;->recordHistory(Landroid/net/NetworkStatsHistory;JJ)V

    #@c
    .line 352
    return-void
.end method

.method public recordHistory(Landroid/net/NetworkStatsHistory;JJ)V
    .registers 23
    .parameter "input"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 360
    new-instance v1, Landroid/net/NetworkStats$Entry;

    #@2
    sget-object v2, Landroid/net/NetworkStats;->IFACE_ALL:Ljava/lang/String;

    #@4
    const/4 v3, -0x1

    #@5
    const/4 v4, 0x0

    #@6
    const/4 v5, 0x0

    #@7
    const-wide/16 v6, 0x0

    #@9
    const-wide/16 v8, 0x0

    #@b
    const-wide/16 v10, 0x0

    #@d
    const-wide/16 v12, 0x0

    #@f
    const-wide/16 v14, 0x0

    #@11
    invoke-direct/range {v1 .. v15}, Landroid/net/NetworkStats$Entry;-><init>(Ljava/lang/String;IIIJJJJJ)V

    #@14
    .line 362
    .local v1, entry:Landroid/net/NetworkStats$Entry;
    const/16 v16, 0x0

    #@16
    .local v16, i:I
    :goto_16
    move-object/from16 v0, p1

    #@18
    iget v2, v0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@1a
    move/from16 v0, v16

    #@1c
    if-ge v0, v2, :cond_82

    #@1e
    .line 363
    move-object/from16 v0, p1

    #@20
    iget-object v2, v0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@22
    aget-wide v3, v2, v16

    #@24
    .line 364
    .local v3, bucketStart:J
    move-object/from16 v0, p1

    #@26
    iget-wide v7, v0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@28
    add-long v5, v3, v7

    #@2a
    .line 367
    .local v5, bucketEnd:J
    cmp-long v2, v3, p2

    #@2c
    if-ltz v2, :cond_32

    #@2e
    cmp-long v2, v5, p4

    #@30
    if-lez v2, :cond_35

    #@32
    .line 362
    :cond_32
    :goto_32
    add-int/lit8 v16, v16, 0x1

    #@34
    goto :goto_16

    #@35
    .line 369
    :cond_35
    move-object/from16 v0, p1

    #@37
    iget-object v2, v0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@39
    const-wide/16 v7, 0x0

    #@3b
    move/from16 v0, v16

    #@3d
    invoke-static {v2, v0, v7, v8}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@40
    move-result-wide v7

    #@41
    iput-wide v7, v1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@43
    .line 370
    move-object/from16 v0, p1

    #@45
    iget-object v2, v0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@47
    const-wide/16 v7, 0x0

    #@49
    move/from16 v0, v16

    #@4b
    invoke-static {v2, v0, v7, v8}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@4e
    move-result-wide v7

    #@4f
    iput-wide v7, v1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@51
    .line 371
    move-object/from16 v0, p1

    #@53
    iget-object v2, v0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@55
    const-wide/16 v7, 0x0

    #@57
    move/from16 v0, v16

    #@59
    invoke-static {v2, v0, v7, v8}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@5c
    move-result-wide v7

    #@5d
    iput-wide v7, v1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@5f
    .line 372
    move-object/from16 v0, p1

    #@61
    iget-object v2, v0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@63
    const-wide/16 v7, 0x0

    #@65
    move/from16 v0, v16

    #@67
    invoke-static {v2, v0, v7, v8}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@6a
    move-result-wide v7

    #@6b
    iput-wide v7, v1, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@6d
    .line 373
    move-object/from16 v0, p1

    #@6f
    iget-object v2, v0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@71
    const-wide/16 v7, 0x0

    #@73
    move/from16 v0, v16

    #@75
    invoke-static {v2, v0, v7, v8}, Landroid/net/NetworkStatsHistory;->getLong([JIJ)J

    #@78
    move-result-wide v7

    #@79
    iput-wide v7, v1, Landroid/net/NetworkStats$Entry;->operations:J

    #@7b
    move-object/from16 v2, p0

    #@7d
    move-object v7, v1

    #@7e
    .line 375
    invoke-virtual/range {v2 .. v7}, Landroid/net/NetworkStatsHistory;->recordData(JJLandroid/net/NetworkStats$Entry;)V

    #@81
    goto :goto_32

    #@82
    .line 377
    .end local v3           #bucketStart:J
    .end local v5           #bucketEnd:J
    :cond_82
    return-void
.end method

.method public removeBucketsBefore(J)V
    .registers 11
    .parameter "cutoff"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 443
    const/4 v4, 0x0

    #@1
    .local v4, i:I
    :goto_1
    iget v6, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@3
    if-ge v4, v6, :cond_11

    #@5
    .line 444
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@7
    aget-wide v2, v6, v4

    #@9
    .line 445
    .local v2, curStart:J
    iget-wide v6, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@b
    add-long v0, v2, v6

    #@d
    .line 449
    .local v0, curEnd:J
    cmp-long v6, v0, p1

    #@f
    if-lez v6, :cond_6c

    #@11
    .line 452
    .end local v0           #curEnd:J
    .end local v2           #curStart:J
    :cond_11
    if-lez v4, :cond_6b

    #@13
    .line 453
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@15
    array-length v5, v6

    #@16
    .line 454
    .local v5, length:I
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@18
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@1b
    move-result-object v6

    #@1c
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@1e
    .line 455
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@20
    if-eqz v6, :cond_2a

    #@22
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@24
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@27
    move-result-object v6

    #@28
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@2a
    .line 456
    :cond_2a
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@2c
    if-eqz v6, :cond_36

    #@2e
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@30
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@33
    move-result-object v6

    #@34
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@36
    .line 457
    :cond_36
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@38
    if-eqz v6, :cond_42

    #@3a
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@3c
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@3f
    move-result-object v6

    #@40
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@42
    .line 458
    :cond_42
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@44
    if-eqz v6, :cond_4e

    #@46
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@48
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@4b
    move-result-object v6

    #@4c
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@4e
    .line 459
    :cond_4e
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@50
    if-eqz v6, :cond_5a

    #@52
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@54
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@57
    move-result-object v6

    #@58
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@5a
    .line 460
    :cond_5a
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@5c
    if-eqz v6, :cond_66

    #@5e
    iget-object v6, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@60
    invoke-static {v6, v4, v5}, Ljava/util/Arrays;->copyOfRange([JII)[J

    #@63
    move-result-object v6

    #@64
    iput-object v6, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@66
    .line 461
    :cond_66
    iget v6, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@68
    sub-int/2addr v6, v4

    #@69
    iput v6, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@6b
    .line 465
    .end local v5           #length:I
    :cond_6b
    return-void

    #@6c
    .line 443
    .restart local v0       #curEnd:J
    .restart local v2       #curStart:J
    :cond_6c
    add-int/lit8 v4, v4, 0x1

    #@6e
    goto :goto_1
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 206
    iget v0, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 601
    new-instance v0, Ljava/io/CharArrayWriter;

    #@2
    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    #@5
    .line 602
    .local v0, writer:Ljava/io/CharArrayWriter;
    new-instance v1, Lcom/android/internal/util/IndentingPrintWriter;

    #@7
    const-string v2, "  "

    #@9
    invoke-direct {v1, v0, v2}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {p0, v1, v2}, Landroid/net/NetworkStatsHistory;->dump(Lcom/android/internal/util/IndentingPrintWriter;Z)V

    #@10
    .line 603
    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 135
    iget-wide v0, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 136
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@7
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@9
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@c
    .line 137
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@e
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@10
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@13
    .line 138
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@15
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@17
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@1a
    .line 139
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@1c
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@1e
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@21
    .line 140
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@23
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@25
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@28
    .line 141
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@2a
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@2c
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@2f
    .line 142
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@31
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@33
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    #@36
    .line 143
    iget-wide v0, p0, Landroid/net/NetworkStatsHistory;->totalBytes:J

    #@38
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@3b
    .line 144
    return-void
.end method

.method public writeToStream(Ljava/io/DataOutputStream;)V
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 189
    const/4 v0, 0x3

    #@1
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4
    .line 190
    iget-wide v0, p0, Landroid/net/NetworkStatsHistory;->bucketDuration:J

    #@6
    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    #@9
    .line 191
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->bucketStart:[J

    #@b
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@d
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@10
    .line 192
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->activeTime:[J

    #@12
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@14
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@17
    .line 193
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->rxBytes:[J

    #@19
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@1b
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@1e
    .line 194
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->rxPackets:[J

    #@20
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@22
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@25
    .line 195
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->txBytes:[J

    #@27
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@29
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@2c
    .line 196
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->txPackets:[J

    #@2e
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@30
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@33
    .line 197
    iget-object v0, p0, Landroid/net/NetworkStatsHistory;->operations:[J

    #@35
    iget v1, p0, Landroid/net/NetworkStatsHistory;->bucketCount:I

    #@37
    invoke-static {p1, v0, v1}, Landroid/net/NetworkStatsHistory$DataStreamUtils;->writeVarLongArray(Ljava/io/DataOutputStream;[JI)V

    #@3a
    .line 198
    return-void
.end method
