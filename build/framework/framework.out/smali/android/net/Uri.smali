.class public abstract Landroid/net/Uri;
.super Ljava/lang/Object;
.source "Uri.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/Uri$PathPart;,
        Landroid/net/Uri$Part;,
        Landroid/net/Uri$AbstractPart;,
        Landroid/net/Uri$Builder;,
        Landroid/net/Uri$HierarchicalUri;,
        Landroid/net/Uri$AbstractHierarchicalUri;,
        Landroid/net/Uri$PathSegmentsBuilder;,
        Landroid/net/Uri$PathSegments;,
        Landroid/net/Uri$OpaqueUri;,
        Landroid/net/Uri$StringUri;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_ENCODING:Ljava/lang/String; = "UTF-8"

.field public static final EMPTY:Landroid/net/Uri; = null

.field private static final HEX_DIGITS:[C = null

.field private static final LOG:Ljava/lang/String; = null

.field private static final NOT_CACHED:Ljava/lang/String; = null

.field private static final NOT_CALCULATED:I = -0x2

.field private static final NOT_FOUND:I = -0x1

.field private static final NOT_HIERARCHICAL:Ljava/lang/String; = "This isn\'t a hierarchical URI."

.field private static final NULL_TYPE_ID:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 110
    const-class v0, Landroid/net/Uri;

    #@3
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Landroid/net/Uri;->LOG:Ljava/lang/String;

    #@9
    .line 122
    new-instance v0, Ljava/lang/String;

    #@b
    const-string v2, "NOT CACHED"

    #@d
    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@10
    sput-object v0, Landroid/net/Uri;->NOT_CACHED:Ljava/lang/String;

    #@12
    .line 127
    new-instance v0, Landroid/net/Uri$HierarchicalUri;

    #@14
    sget-object v2, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@16
    sget-object v3, Landroid/net/Uri$PathPart;->EMPTY:Landroid/net/Uri$PathPart;

    #@18
    sget-object v4, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@1a
    sget-object v5, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@1c
    move-object v6, v1

    #@1d
    invoke-direct/range {v0 .. v6}, Landroid/net/Uri$HierarchicalUri;-><init>(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$PathPart;Landroid/net/Uri$Part;Landroid/net/Uri$Part;Landroid/net/Uri$1;)V

    #@20
    sput-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    #@22
    .line 1759
    new-instance v0, Landroid/net/Uri$1;

    #@24
    invoke-direct {v0}, Landroid/net/Uri$1;-><init>()V

    #@27
    sput-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    .line 1793
    const-string v0, "0123456789ABCDEF"

    #@2b
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    #@2e
    move-result-object v0

    #@2f
    sput-object v0, Landroid/net/Uri;->HEX_DIGITS:[C

    #@31
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 133
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Landroid/net/Uri;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 50
    sget-object v0, Landroid/net/Uri;->NOT_CACHED:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 50
    sget-object v0, Landroid/net/Uri;->LOG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static decode(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "s"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1927
    if-nez p0, :cond_5

    #@3
    .line 1928
    const/4 v0, 0x0

    #@4
    .line 1930
    :goto_4
    return-object v0

    #@5
    :cond_5
    sget-object v0, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@7
    invoke-static {p0, v1, v0, v1}, Llibcore/net/UriCodec;->decode(Ljava/lang/String;ZLjava/nio/charset/Charset;Z)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    goto :goto_4
.end method

.method public static encode(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 1806
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "s"
    .parameter "allow"

    #@0
    .prologue
    .line 1823
    if-nez p0, :cond_4

    #@2
    .line 1824
    const/4 p0, 0x0

    #@3
    .line 1898
    .end local p0
    :cond_3
    :goto_3
    return-object p0

    #@4
    .line 1828
    .restart local p0
    :cond_4
    const/4 v4, 0x0

    #@5
    .line 1830
    .local v4, encoded:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v8

    #@9
    .line 1835
    .local v8, oldLength:I
    const/4 v2, 0x0

    #@a
    .line 1836
    .local v2, current:I
    :goto_a
    if-ge v2, v8, :cond_7d

    #@c
    .line 1840
    move v7, v2

    #@d
    .line 1842
    .local v7, nextToEncode:I
    :goto_d
    if-ge v7, v8, :cond_1c

    #@f
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v10

    #@13
    invoke-static {v10, p1}, Landroid/net/Uri;->isAllowed(CLjava/lang/String;)Z

    #@16
    move-result v10

    #@17
    if-eqz v10, :cond_1c

    #@19
    .line 1843
    add-int/lit8 v7, v7, 0x1

    #@1b
    goto :goto_d

    #@1c
    .line 1847
    :cond_1c
    if-ne v7, v8, :cond_28

    #@1e
    .line 1848
    if-eqz v2, :cond_3

    #@20
    .line 1853
    invoke-virtual {v4, p0, v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    #@23
    .line 1854
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object p0

    #@27
    goto :goto_3

    #@28
    .line 1858
    :cond_28
    if-nez v4, :cond_2f

    #@2a
    .line 1859
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    .end local v4           #encoded:Ljava/lang/StringBuilder;
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    .line 1862
    .restart local v4       #encoded:Ljava/lang/StringBuilder;
    :cond_2f
    if-le v7, v2, :cond_34

    #@31
    .line 1864
    invoke-virtual {v4, p0, v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    #@34
    .line 1872
    :cond_34
    move v2, v7

    #@35
    .line 1873
    add-int/lit8 v6, v2, 0x1

    #@37
    .line 1875
    .local v6, nextAllowed:I
    :goto_37
    if-ge v6, v8, :cond_46

    #@39
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    #@3c
    move-result v10

    #@3d
    invoke-static {v10, p1}, Landroid/net/Uri;->isAllowed(CLjava/lang/String;)Z

    #@40
    move-result v10

    #@41
    if-nez v10, :cond_46

    #@43
    .line 1876
    add-int/lit8 v6, v6, 0x1

    #@45
    goto :goto_37

    #@46
    .line 1881
    :cond_46
    invoke-virtual {p0, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@49
    move-result-object v9

    #@4a
    .line 1883
    .local v9, toEncode:Ljava/lang/String;
    :try_start_4a
    const-string v10, "UTF-8"

    #@4c
    invoke-virtual {v9, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@4f
    move-result-object v0

    #@50
    .line 1884
    .local v0, bytes:[B
    array-length v1, v0

    #@51
    .line 1885
    .local v1, bytesLength:I
    const/4 v5, 0x0

    #@52
    .local v5, i:I
    :goto_52
    if-ge v5, v1, :cond_7b

    #@54
    .line 1886
    const/16 v10, 0x25

    #@56
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@59
    .line 1887
    sget-object v10, Landroid/net/Uri;->HEX_DIGITS:[C

    #@5b
    aget-byte v11, v0, v5

    #@5d
    and-int/lit16 v11, v11, 0xf0

    #@5f
    shr-int/lit8 v11, v11, 0x4

    #@61
    aget-char v10, v10, v11

    #@63
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@66
    .line 1888
    sget-object v10, Landroid/net/Uri;->HEX_DIGITS:[C

    #@68
    aget-byte v11, v0, v5

    #@6a
    and-int/lit8 v11, v11, 0xf

    #@6c
    aget-char v10, v10, v11

    #@6e
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_71
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4a .. :try_end_71} :catch_74

    #@71
    .line 1885
    add-int/lit8 v5, v5, 0x1

    #@73
    goto :goto_52

    #@74
    .line 1890
    .end local v0           #bytes:[B
    .end local v1           #bytesLength:I
    .end local v5           #i:I
    :catch_74
    move-exception v3

    #@75
    .line 1891
    .local v3, e:Ljava/io/UnsupportedEncodingException;
    new-instance v10, Ljava/lang/AssertionError;

    #@77
    invoke-direct {v10, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@7a
    throw v10

    #@7b
    .line 1894
    .end local v3           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v0       #bytes:[B
    .restart local v1       #bytesLength:I
    .restart local v5       #i:I
    :cond_7b
    move v2, v6

    #@7c
    .line 1895
    goto :goto_a

    #@7d
    .line 1898
    .end local v0           #bytes:[B
    .end local v1           #bytesLength:I
    .end local v5           #i:I
    .end local v6           #nextAllowed:I
    .end local v7           #nextToEncode:I
    .end local v9           #toEncode:Ljava/lang/String;
    :cond_7d
    if-eqz v4, :cond_3

    #@7f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object p0

    #@83
    goto :goto_3
.end method

.method public static fromFile(Ljava/io/File;)Landroid/net/Uri;
    .registers 8
    .parameter "file"

    #@0
    .prologue
    .line 443
    if-nez p0, :cond_a

    #@2
    .line 444
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "file"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 447
    :cond_a
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/net/Uri$PathPart;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$PathPart;

    #@11
    move-result-object v3

    #@12
    .line 448
    .local v3, path:Landroid/net/Uri$PathPart;
    new-instance v0, Landroid/net/Uri$HierarchicalUri;

    #@14
    const-string v1, "file"

    #@16
    sget-object v2, Landroid/net/Uri$Part;->EMPTY:Landroid/net/Uri$Part;

    #@18
    sget-object v4, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@1a
    sget-object v5, Landroid/net/Uri$Part;->NULL:Landroid/net/Uri$Part;

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-direct/range {v0 .. v6}, Landroid/net/Uri$HierarchicalUri;-><init>(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$PathPart;Landroid/net/Uri$Part;Landroid/net/Uri$Part;Landroid/net/Uri$1;)V

    #@20
    return-object v0
.end method

.method public static fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 7
    .parameter "scheme"
    .parameter "ssp"
    .parameter "fragment"

    #@0
    .prologue
    .line 810
    if-nez p0, :cond_b

    #@2
    .line 811
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "scheme"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 813
    :cond_b
    if-nez p1, :cond_16

    #@d
    .line 814
    new-instance v0, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v1, "ssp"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 817
    :cond_16
    new-instance v0, Landroid/net/Uri$OpaqueUri;

    #@18
    invoke-static {p1}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {p2}, Landroid/net/Uri$Part;->fromDecoded(Ljava/lang/String;)Landroid/net/Uri$Part;

    #@1f
    move-result-object v2

    #@20
    const/4 v3, 0x0

    #@21
    invoke-direct {v0, p0, v1, v2, v3}, Landroid/net/Uri$OpaqueUri;-><init>(Ljava/lang/String;Landroid/net/Uri$Part;Landroid/net/Uri$Part;Landroid/net/Uri$1;)V

    #@24
    return-object v0
.end method

.method private static isAllowed(CLjava/lang/String;)Z
    .registers 4
    .parameter "c"
    .parameter "allow"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1910
    const/16 v0, 0x41

    #@3
    if-lt p0, v0, :cond_9

    #@5
    const/16 v0, 0x5a

    #@7
    if-le p0, v0, :cond_29

    #@9
    :cond_9
    const/16 v0, 0x61

    #@b
    if-lt p0, v0, :cond_11

    #@d
    const/16 v0, 0x7a

    #@f
    if-le p0, v0, :cond_29

    #@11
    :cond_11
    const/16 v0, 0x30

    #@13
    if-lt p0, v0, :cond_19

    #@15
    const/16 v0, 0x39

    #@17
    if-le p0, v0, :cond_29

    #@19
    :cond_19
    const-string v0, "_-!.~\'()*"

    #@1b
    invoke-virtual {v0, p0}, Ljava/lang/String;->indexOf(I)I

    #@1e
    move-result v0

    #@1f
    if-ne v0, v1, :cond_29

    #@21
    if-eqz p1, :cond_2b

    #@23
    invoke-virtual {p1, p0}, Ljava/lang/String;->indexOf(I)I

    #@26
    move-result v0

    #@27
    if-eq v0, v1, :cond_2b

    #@29
    :cond_29
    const/4 v0, 0x1

    #@2a
    :goto_2a
    return v0

    #@2b
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_2a
.end method

.method public static parse(Ljava/lang/String;)Landroid/net/Uri;
    .registers 3
    .parameter "uriString"

    #@0
    .prologue
    .line 429
    new-instance v0, Landroid/net/Uri$StringUri;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/net/Uri$StringUri;-><init>(Ljava/lang/String;Landroid/net/Uri$1;)V

    #@6
    return-object v0
.end method

.method public static withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "baseUri"
    .parameter "pathSegment"

    #@0
    .prologue
    .line 2290
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@3
    move-result-object v0

    #@4
    .line 2291
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@7
    move-result-object v0

    #@8
    .line 2292
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method public static writeToParcel(Landroid/os/Parcel;Landroid/net/Uri;)V
    .registers 3
    .parameter "out"
    .parameter "uri"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1786
    if-nez p1, :cond_7

    #@3
    .line 1787
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 1791
    :goto_6
    return-void

    #@7
    .line 1789
    :cond_7
    invoke-virtual {p1, p0, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@a
    goto :goto_6
.end method


# virtual methods
.method public abstract buildUpon()Landroid/net/Uri$Builder;
.end method

.method public compareTo(Landroid/net/Uri;)I
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 349
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 50
    check-cast p1, Landroid/net/Uri;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    #@0
    .prologue
    .line 327
    instance-of v1, p1, Landroid/net/Uri;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 328
    const/4 v1, 0x0

    #@5
    .line 333
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 331
    check-cast v0, Landroid/net/Uri;

    #@9
    .line 333
    .local v0, other:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    goto :goto_5
.end method

.method public abstract getAuthority()Ljava/lang/String;
.end method

.method public getBooleanQueryParameter(Ljava/lang/String;Z)Z
    .registers 5
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1715
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1716
    .local v0, flag:Ljava/lang/String;
    if-nez v0, :cond_7

    #@6
    .line 1720
    .end local p2
    :goto_6
    return p2

    #@7
    .line 1719
    .restart local p2
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 1720
    const-string v1, "false"

    #@d
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_1e

    #@13
    const-string v1, "0"

    #@15
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_1e

    #@1b
    const/4 v1, 0x1

    #@1c
    :goto_1c
    move p2, v1

    #@1d
    goto :goto_6

    #@1e
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1c
.end method

.method public getCanonicalUri()Landroid/net/Uri;
    .registers 7

    #@0
    .prologue
    .line 2304
    const-string v3, "file"

    #@2
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_48

    #@c
    .line 2307
    :try_start_c
    new-instance v3, Ljava/io/File;

    #@e
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@15
    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_18} :catch_49

    #@18
    move-result-object v0

    #@19
    .line 2312
    .local v0, canonicalPath:Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_4b

    #@1f
    .line 2313
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    .line 2317
    .local v2, legacyPath:Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_4b

    #@2d
    .line 2318
    new-instance v3, Ljava/io/File;

    #@2f
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@3a
    move-result v5

    #@3b
    add-int/lit8 v5, v5, 0x1

    #@3d
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@47
    move-result-object p0

    #@48
    .line 2326
    .end local v0           #canonicalPath:Ljava/lang/String;
    .end local v2           #legacyPath:Ljava/lang/String;
    .end local p0
    :cond_48
    :goto_48
    return-object p0

    #@49
    .line 2308
    .restart local p0
    :catch_49
    move-exception v1

    #@4a
    .line 2309
    .local v1, e:Ljava/io/IOException;
    goto :goto_48

    #@4b
    .line 2324
    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #canonicalPath:Ljava/lang/String;
    :cond_4b
    new-instance v3, Ljava/io/File;

    #@4d
    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@50
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@53
    move-result-object p0

    #@54
    goto :goto_48
.end method

.method public abstract getEncodedAuthority()Ljava/lang/String;
.end method

.method public abstract getEncodedFragment()Ljava/lang/String;
.end method

.method public abstract getEncodedPath()Ljava/lang/String;
.end method

.method public abstract getEncodedQuery()Ljava/lang/String;
.end method

.method public abstract getEncodedSchemeSpecificPart()Ljava/lang/String;
.end method

.method public abstract getEncodedUserInfo()Ljava/lang/String;
.end method

.method public abstract getFragment()Ljava/lang/String;
.end method

.method public abstract getHost()Ljava/lang/String;
.end method

.method public abstract getLastPathSegment()Ljava/lang/String;
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract getPathSegments()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPort()I
.end method

.method public abstract getQuery()Ljava/lang/String;
.end method

.method public getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    .registers 15
    .parameter "key"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v11, -0x1

    #@3
    .line 1661
    invoke-virtual {p0}, Landroid/net/Uri;->isOpaque()Z

    #@6
    move-result v9

    #@7
    if-eqz v9, :cond_11

    #@9
    .line 1662
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    #@b
    const-string v9, "This isn\'t a hierarchical URI."

    #@d
    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@10
    throw v8

    #@11
    .line 1664
    :cond_11
    if-nez p1, :cond_1c

    #@13
    .line 1665
    new-instance v8, Ljava/lang/NullPointerException;

    #@15
    const-string/jumbo v9, "key"

    #@18
    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v8

    #@1c
    .line 1668
    :cond_1c
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    .line 1669
    .local v5, query:Ljava/lang/String;
    if-nez v5, :cond_23

    #@22
    .line 1702
    :cond_22
    :goto_22
    return-object v8

    #@23
    .line 1673
    :cond_23
    invoke-static {p1, v8}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 1674
    .local v0, encodedKey:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@2a
    move-result v3

    #@2b
    .line 1675
    .local v3, length:I
    const/4 v7, 0x0

    #@2c
    .line 1677
    .local v7, start:I
    :goto_2c
    const/16 v9, 0x26

    #@2e
    invoke-virtual {v5, v9, v7}, Ljava/lang/String;->indexOf(II)I

    #@31
    move-result v4

    #@32
    .line 1678
    .local v4, nextAmpersand:I
    if-eq v4, v11, :cond_57

    #@34
    move v2, v4

    #@35
    .line 1680
    .local v2, end:I
    :goto_35
    const/16 v9, 0x3d

    #@37
    invoke-virtual {v5, v9, v7}, Ljava/lang/String;->indexOf(II)I

    #@3a
    move-result v6

    #@3b
    .line 1681
    .local v6, separator:I
    if-gt v6, v2, :cond_3f

    #@3d
    if-ne v6, v11, :cond_40

    #@3f
    .line 1682
    :cond_3f
    move v6, v2

    #@40
    .line 1685
    :cond_40
    sub-int v9, v6, v7

    #@42
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@45
    move-result v10

    #@46
    if-ne v9, v10, :cond_67

    #@48
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@4b
    move-result v9

    #@4c
    invoke-virtual {v5, v7, v0, v12, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@4f
    move-result v9

    #@50
    if-eqz v9, :cond_67

    #@52
    .line 1687
    if-ne v6, v2, :cond_59

    #@54
    .line 1688
    const-string v8, ""

    #@56
    goto :goto_22

    #@57
    .end local v2           #end:I
    .end local v6           #separator:I
    :cond_57
    move v2, v3

    #@58
    .line 1678
    goto :goto_35

    #@59
    .line 1690
    .restart local v2       #end:I
    .restart local v6       #separator:I
    :cond_59
    add-int/lit8 v8, v6, 0x1

    #@5b
    invoke-virtual {v5, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    .line 1691
    .local v1, encodedValue:Ljava/lang/String;
    const/4 v8, 0x1

    #@60
    sget-object v9, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@62
    invoke-static {v1, v8, v9, v12}, Llibcore/net/UriCodec;->decode(Ljava/lang/String;ZLjava/nio/charset/Charset;Z)Ljava/lang/String;

    #@65
    move-result-object v8

    #@66
    goto :goto_22

    #@67
    .line 1696
    .end local v1           #encodedValue:Ljava/lang/String;
    :cond_67
    if-eq v4, v11, :cond_22

    #@69
    .line 1697
    add-int/lit8 v7, v4, 0x1

    #@6b
    .line 1701
    goto :goto_2c
.end method

.method public getQueryParameterNames()Ljava/util/Set;
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    .line 1558
    invoke-virtual {p0}, Landroid/net/Uri;->isOpaque()Z

    #@4
    move-result v7

    #@5
    if-eqz v7, :cond_f

    #@7
    .line 1559
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    #@9
    const-string v8, "This isn\'t a hierarchical URI."

    #@b
    invoke-direct {v7, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@e
    throw v7

    #@f
    .line 1562
    :cond_f
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 1563
    .local v4, query:Ljava/lang/String;
    if-nez v4, :cond_1a

    #@15
    .line 1564
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    #@18
    move-result-object v7

    #@19
    .line 1585
    :goto_19
    return-object v7

    #@1a
    .line 1567
    :cond_1a
    new-instance v2, Ljava/util/LinkedHashSet;

    #@1c
    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    #@1f
    .line 1568
    .local v2, names:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@20
    .line 1570
    .local v6, start:I
    :cond_20
    const/16 v7, 0x26

    #@22
    invoke-virtual {v4, v7, v6}, Ljava/lang/String;->indexOf(II)I

    #@25
    move-result v3

    #@26
    .line 1571
    .local v3, next:I
    if-ne v3, v8, :cond_4f

    #@28
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@2b
    move-result v0

    #@2c
    .line 1573
    .local v0, end:I
    :goto_2c
    const/16 v7, 0x3d

    #@2e
    invoke-virtual {v4, v7, v6}, Ljava/lang/String;->indexOf(II)I

    #@31
    move-result v5

    #@32
    .line 1574
    .local v5, separator:I
    if-gt v5, v0, :cond_36

    #@34
    if-ne v5, v8, :cond_37

    #@36
    .line 1575
    :cond_36
    move v5, v0

    #@37
    .line 1578
    :cond_37
    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    .line 1579
    .local v1, name:Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@42
    .line 1582
    add-int/lit8 v6, v0, 0x1

    #@44
    .line 1583
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@47
    move-result v7

    #@48
    if-lt v6, v7, :cond_20

    #@4a
    .line 1585
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    #@4d
    move-result-object v7

    #@4e
    goto :goto_19

    #@4f
    .end local v0           #end:I
    .end local v1           #name:Ljava/lang/String;
    .end local v5           #separator:I
    :cond_4f
    move v0, v3

    #@50
    .line 1571
    goto :goto_2c
.end method

.method public getQueryParameters(Ljava/lang/String;)Ljava/util/List;
    .registers 13
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, -0x1

    #@1
    .line 1598
    invoke-virtual {p0}, Landroid/net/Uri;->isOpaque()Z

    #@4
    move-result v8

    #@5
    if-eqz v8, :cond_f

    #@7
    .line 1599
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    #@9
    const-string v9, "This isn\'t a hierarchical URI."

    #@b
    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@e
    throw v8

    #@f
    .line 1601
    :cond_f
    if-nez p1, :cond_1a

    #@11
    .line 1602
    new-instance v8, Ljava/lang/NullPointerException;

    #@13
    const-string/jumbo v9, "key"

    #@16
    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@19
    throw v8

    #@1a
    .line 1605
    :cond_1a
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    .line 1606
    .local v4, query:Ljava/lang/String;
    if-nez v4, :cond_25

    #@20
    .line 1607
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@23
    move-result-object v8

    #@24
    .line 1646
    :goto_24
    return-object v8

    #@25
    .line 1612
    :cond_25
    :try_start_25
    const-string v8, "UTF-8"

    #@27
    invoke-static {p1, v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_25 .. :try_end_2a} :catch_64

    #@2a
    move-result-object v1

    #@2b
    .line 1617
    .local v1, encodedKey:Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    #@2d
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@30
    .line 1619
    .local v7, values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@31
    .line 1621
    .local v6, start:I
    :goto_31
    const/16 v8, 0x26

    #@33
    invoke-virtual {v4, v8, v6}, Ljava/lang/String;->indexOf(II)I

    #@36
    move-result v3

    #@37
    .line 1622
    .local v3, nextAmpersand:I
    if-eq v3, v10, :cond_6b

    #@39
    move v2, v3

    #@3a
    .line 1624
    .local v2, end:I
    :goto_3a
    const/16 v8, 0x3d

    #@3c
    invoke-virtual {v4, v8, v6}, Ljava/lang/String;->indexOf(II)I

    #@3f
    move-result v5

    #@40
    .line 1625
    .local v5, separator:I
    if-gt v5, v2, :cond_44

    #@42
    if-ne v5, v10, :cond_45

    #@44
    .line 1626
    :cond_44
    move v5, v2

    #@45
    .line 1629
    :cond_45
    sub-int v8, v5, v6

    #@47
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@4a
    move-result v9

    #@4b
    if-ne v8, v9, :cond_5f

    #@4d
    const/4 v8, 0x0

    #@4e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@51
    move-result v9

    #@52
    invoke-virtual {v4, v6, v1, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@55
    move-result v8

    #@56
    if-eqz v8, :cond_5f

    #@58
    .line 1631
    if-ne v5, v2, :cond_70

    #@5a
    .line 1632
    const-string v8, ""

    #@5c
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5f
    .line 1639
    :cond_5f
    :goto_5f
    if-eq v3, v10, :cond_7e

    #@61
    .line 1640
    add-int/lit8 v6, v3, 0x1

    #@63
    .line 1644
    goto :goto_31

    #@64
    .line 1613
    .end local v1           #encodedKey:Ljava/lang/String;
    .end local v2           #end:I
    .end local v3           #nextAmpersand:I
    .end local v5           #separator:I
    .end local v6           #start:I
    .end local v7           #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_64
    move-exception v0

    #@65
    .line 1614
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    new-instance v8, Ljava/lang/AssertionError;

    #@67
    invoke-direct {v8, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@6a
    throw v8

    #@6b
    .line 1622
    .end local v0           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v1       #encodedKey:Ljava/lang/String;
    .restart local v3       #nextAmpersand:I
    .restart local v6       #start:I
    .restart local v7       #values:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6b
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@6e
    move-result v2

    #@6f
    goto :goto_3a

    #@70
    .line 1634
    .restart local v2       #end:I
    .restart local v5       #separator:I
    :cond_70
    add-int/lit8 v8, v5, 0x1

    #@72
    invoke-virtual {v4, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@75
    move-result-object v8

    #@76
    invoke-static {v8}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7d
    goto :goto_5f

    #@7e
    .line 1646
    :cond_7e
    invoke-static {v7}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@81
    move-result-object v8

    #@82
    goto :goto_24
.end method

.method public abstract getScheme()Ljava/lang/String;
.end method

.method public abstract getSchemeSpecificPart()Ljava/lang/String;
.end method

.method public abstract getUserInfo()Ljava/lang/String;
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 341
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public isAbsolute()Z
    .registers 2

    #@0
    .prologue
    .line 165
    invoke-virtual {p0}, Landroid/net/Uri;->isRelative()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public abstract isHierarchical()Z
.end method

.method public isOpaque()Z
    .registers 2

    #@0
    .prologue
    .line 147
    invoke-virtual {p0}, Landroid/net/Uri;->isHierarchical()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public abstract isRelative()Z
.end method

.method public normalizeScheme()Landroid/net/Uri;
    .registers 4

    #@0
    .prologue
    .line 1745
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1746
    .local v1, scheme:Ljava/lang/String;
    if-nez v1, :cond_7

    #@6
    .line 1750
    .end local p0
    :cond_6
    :goto_6
    return-object p0

    #@7
    .line 1747
    .restart local p0
    :cond_7
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 1748
    .local v0, lowerScheme:Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_6

    #@13
    .line 1750
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@1e
    move-result-object p0

    #@1f
    goto :goto_6
.end method

.method public toSafeString()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    const/16 v7, 0x3a

    #@2
    const/16 v6, 0x40

    #@4
    .line 364
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 365
    .local v3, scheme:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    .line 366
    .local v4, ssp:Ljava/lang/String;
    if-eqz v3, :cond_6e

    #@e
    .line 367
    const-string/jumbo v5, "tel"

    #@11
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@14
    move-result v5

    #@15
    if-nez v5, :cond_3b

    #@17
    const-string/jumbo v5, "sip"

    #@1a
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1d
    move-result v5

    #@1e
    if-nez v5, :cond_3b

    #@20
    const-string/jumbo v5, "sms"

    #@23
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v5

    #@27
    if-nez v5, :cond_3b

    #@29
    const-string/jumbo v5, "smsto"

    #@2c
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f
    move-result v5

    #@30
    if-nez v5, :cond_3b

    #@32
    const-string/jumbo v5, "mailto"

    #@35
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_6e

    #@3b
    .line 370
    :cond_3b
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@40
    .line 371
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 372
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@46
    .line 373
    if-eqz v4, :cond_69

    #@48
    .line 374
    const/4 v2, 0x0

    #@49
    .local v2, i:I
    :goto_49
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@4c
    move-result v5

    #@4d
    if-ge v2, v5, :cond_69

    #@4f
    .line 375
    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    #@52
    move-result v1

    #@53
    .line 376
    .local v1, c:C
    const/16 v5, 0x2d

    #@55
    if-eq v1, v5, :cond_5d

    #@57
    if-eq v1, v6, :cond_5d

    #@59
    const/16 v5, 0x2e

    #@5b
    if-ne v1, v5, :cond_63

    #@5d
    .line 377
    :cond_5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@60
    .line 374
    :goto_60
    add-int/lit8 v2, v2, 0x1

    #@62
    goto :goto_49

    #@63
    .line 379
    :cond_63
    const/16 v5, 0x78

    #@65
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@68
    goto :goto_60

    #@69
    .line 383
    .end local v1           #c:C
    .end local v2           #i:I
    :cond_69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    .line 397
    :goto_6d
    return-object v5

    #@6e
    .line 389
    .end local v0           #builder:Ljava/lang/StringBuilder;
    :cond_6e
    new-instance v0, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@73
    .line 390
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    if-eqz v3, :cond_7b

    #@75
    .line 391
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    .line 392
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7b
    .line 394
    :cond_7b
    if-eqz v4, :cond_80

    #@7d
    .line 395
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    .line 397
    :cond_80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    goto :goto_6d
.end method

.method public abstract toString()Ljava/lang/String;
.end method
