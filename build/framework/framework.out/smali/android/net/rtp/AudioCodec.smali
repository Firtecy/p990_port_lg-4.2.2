.class public Landroid/net/rtp/AudioCodec;
.super Ljava/lang/Object;
.source "AudioCodec.java"


# static fields
.field public static final AMR:Landroid/net/rtp/AudioCodec;

.field public static final GSM:Landroid/net/rtp/AudioCodec;

.field public static final GSM_EFR:Landroid/net/rtp/AudioCodec;

.field public static final PCMA:Landroid/net/rtp/AudioCodec;

.field public static final PCMU:Landroid/net/rtp/AudioCodec;

.field private static final sCodecs:[Landroid/net/rtp/AudioCodec;


# instance fields
.field public final fmtp:Ljava/lang/String;

.field public final rtpmap:Ljava/lang/String;

.field public final type:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 56
    new-instance v0, Landroid/net/rtp/AudioCodec;

    #@5
    const-string v1, "PCMU/8000"

    #@7
    invoke-direct {v0, v4, v1, v3}, Landroid/net/rtp/AudioCodec;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@a
    sput-object v0, Landroid/net/rtp/AudioCodec;->PCMU:Landroid/net/rtp/AudioCodec;

    #@c
    .line 61
    new-instance v0, Landroid/net/rtp/AudioCodec;

    #@e
    const/16 v1, 0x8

    #@10
    const-string v2, "PCMA/8000"

    #@12
    invoke-direct {v0, v1, v2, v3}, Landroid/net/rtp/AudioCodec;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@15
    sput-object v0, Landroid/net/rtp/AudioCodec;->PCMA:Landroid/net/rtp/AudioCodec;

    #@17
    .line 67
    new-instance v0, Landroid/net/rtp/AudioCodec;

    #@19
    const-string v1, "GSM/8000"

    #@1b
    invoke-direct {v0, v5, v1, v3}, Landroid/net/rtp/AudioCodec;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@1e
    sput-object v0, Landroid/net/rtp/AudioCodec;->GSM:Landroid/net/rtp/AudioCodec;

    #@20
    .line 73
    new-instance v0, Landroid/net/rtp/AudioCodec;

    #@22
    const/16 v1, 0x60

    #@24
    const-string v2, "GSM-EFR/8000"

    #@26
    invoke-direct {v0, v1, v2, v3}, Landroid/net/rtp/AudioCodec;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@29
    sput-object v0, Landroid/net/rtp/AudioCodec;->GSM_EFR:Landroid/net/rtp/AudioCodec;

    #@2b
    .line 80
    new-instance v0, Landroid/net/rtp/AudioCodec;

    #@2d
    const/16 v1, 0x61

    #@2f
    const-string v2, "AMR/8000"

    #@31
    invoke-direct {v0, v1, v2, v3}, Landroid/net/rtp/AudioCodec;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@34
    sput-object v0, Landroid/net/rtp/AudioCodec;->AMR:Landroid/net/rtp/AudioCodec;

    #@36
    .line 82
    const/4 v0, 0x5

    #@37
    new-array v0, v0, [Landroid/net/rtp/AudioCodec;

    #@39
    sget-object v1, Landroid/net/rtp/AudioCodec;->GSM_EFR:Landroid/net/rtp/AudioCodec;

    #@3b
    aput-object v1, v0, v4

    #@3d
    const/4 v1, 0x1

    #@3e
    sget-object v2, Landroid/net/rtp/AudioCodec;->AMR:Landroid/net/rtp/AudioCodec;

    #@40
    aput-object v2, v0, v1

    #@42
    const/4 v1, 0x2

    #@43
    sget-object v2, Landroid/net/rtp/AudioCodec;->GSM:Landroid/net/rtp/AudioCodec;

    #@45
    aput-object v2, v0, v1

    #@47
    sget-object v1, Landroid/net/rtp/AudioCodec;->PCMU:Landroid/net/rtp/AudioCodec;

    #@49
    aput-object v1, v0, v5

    #@4b
    const/4 v1, 0x4

    #@4c
    sget-object v2, Landroid/net/rtp/AudioCodec;->PCMA:Landroid/net/rtp/AudioCodec;

    #@4e
    aput-object v2, v0, v1

    #@50
    sput-object v0, Landroid/net/rtp/AudioCodec;->sCodecs:[Landroid/net/rtp/AudioCodec;

    #@52
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "type"
    .parameter "rtpmap"
    .parameter "fmtp"

    #@0
    .prologue
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 85
    iput p1, p0, Landroid/net/rtp/AudioCodec;->type:I

    #@5
    .line 86
    iput-object p2, p0, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@7
    .line 87
    iput-object p3, p0, Landroid/net/rtp/AudioCodec;->fmtp:Ljava/lang/String;

    #@9
    .line 88
    return-void
.end method

.method public static getCodec(ILjava/lang/String;Ljava/lang/String;)Landroid/net/rtp/AudioCodec;
    .registers 12
    .parameter "type"
    .parameter "rtpmap"
    .parameter "fmtp"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 108
    if-ltz p0, :cond_7

    #@3
    const/16 v8, 0x7f

    #@5
    if-le p0, v8, :cond_8

    #@7
    .line 144
    :cond_7
    :goto_7
    return-object v7

    #@8
    .line 112
    :cond_8
    const/4 v4, 0x0

    #@9
    .line 113
    .local v4, hint:Landroid/net/rtp/AudioCodec;
    if-eqz p1, :cond_6a

    #@b
    .line 114
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 115
    .local v2, clue:Ljava/lang/String;
    sget-object v0, Landroid/net/rtp/AudioCodec;->sCodecs:[Landroid/net/rtp/AudioCodec;

    #@15
    .local v0, arr$:[Landroid/net/rtp/AudioCodec;
    array-length v6, v0

    #@16
    .local v6, len$:I
    const/4 v5, 0x0

    #@17
    .local v5, i$:I
    :goto_17
    if-ge v5, v6, :cond_3c

    #@19
    aget-object v3, v0, v5

    #@1b
    .line 116
    .local v3, codec:Landroid/net/rtp/AudioCodec;
    iget-object v8, v3, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@1d
    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@20
    move-result v8

    #@21
    if-eqz v8, :cond_67

    #@23
    .line 117
    iget-object v8, v3, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@25
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@28
    move-result v8

    #@29
    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 118
    .local v1, channels:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@30
    move-result v8

    #@31
    if-eqz v8, :cond_3b

    #@33
    const-string v8, "/1"

    #@35
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v8

    #@39
    if-eqz v8, :cond_3c

    #@3b
    .line 119
    :cond_3b
    move-object v4, v3

    #@3c
    .line 134
    .end local v0           #arr$:[Landroid/net/rtp/AudioCodec;
    .end local v1           #channels:Ljava/lang/String;
    .end local v2           #clue:Ljava/lang/String;
    .end local v3           #codec:Landroid/net/rtp/AudioCodec;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :cond_3c
    :goto_3c
    if-eqz v4, :cond_7

    #@3e
    .line 137
    sget-object v8, Landroid/net/rtp/AudioCodec;->AMR:Landroid/net/rtp/AudioCodec;

    #@40
    if-ne v4, v8, :cond_61

    #@42
    if-eqz p2, :cond_61

    #@44
    .line 138
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    .line 139
    .restart local v2       #clue:Ljava/lang/String;
    const-string v8, "crc=1"

    #@4a
    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@4d
    move-result v8

    #@4e
    if-nez v8, :cond_7

    #@50
    const-string/jumbo v8, "robust-sorting=1"

    #@53
    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@56
    move-result v8

    #@57
    if-nez v8, :cond_7

    #@59
    const-string v8, "interleaving="

    #@5b
    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@5e
    move-result v8

    #@5f
    if-nez v8, :cond_7

    #@61
    .line 144
    .end local v2           #clue:Ljava/lang/String;
    :cond_61
    new-instance v7, Landroid/net/rtp/AudioCodec;

    #@63
    invoke-direct {v7, p0, p1, p2}, Landroid/net/rtp/AudioCodec;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@66
    goto :goto_7

    #@67
    .line 115
    .restart local v0       #arr$:[Landroid/net/rtp/AudioCodec;
    .restart local v2       #clue:Ljava/lang/String;
    .restart local v3       #codec:Landroid/net/rtp/AudioCodec;
    .restart local v5       #i$:I
    .restart local v6       #len$:I
    :cond_67
    add-int/lit8 v5, v5, 0x1

    #@69
    goto :goto_17

    #@6a
    .line 124
    .end local v0           #arr$:[Landroid/net/rtp/AudioCodec;
    .end local v2           #clue:Ljava/lang/String;
    .end local v3           #codec:Landroid/net/rtp/AudioCodec;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :cond_6a
    const/16 v8, 0x60

    #@6c
    if-ge p0, v8, :cond_3c

    #@6e
    .line 125
    sget-object v0, Landroid/net/rtp/AudioCodec;->sCodecs:[Landroid/net/rtp/AudioCodec;

    #@70
    .restart local v0       #arr$:[Landroid/net/rtp/AudioCodec;
    array-length v6, v0

    #@71
    .restart local v6       #len$:I
    const/4 v5, 0x0

    #@72
    .restart local v5       #i$:I
    :goto_72
    if-ge v5, v6, :cond_3c

    #@74
    aget-object v3, v0, v5

    #@76
    .line 126
    .restart local v3       #codec:Landroid/net/rtp/AudioCodec;
    iget v8, v3, Landroid/net/rtp/AudioCodec;->type:I

    #@78
    if-ne p0, v8, :cond_7e

    #@7a
    .line 127
    move-object v4, v3

    #@7b
    .line 128
    iget-object p1, v3, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@7d
    .line 129
    goto :goto_3c

    #@7e
    .line 125
    :cond_7e
    add-int/lit8 v5, v5, 0x1

    #@80
    goto :goto_72
.end method

.method public static getCodecs()[Landroid/net/rtp/AudioCodec;
    .registers 2

    #@0
    .prologue
    .line 94
    sget-object v0, Landroid/net/rtp/AudioCodec;->sCodecs:[Landroid/net/rtp/AudioCodec;

    #@2
    sget-object v1, Landroid/net/rtp/AudioCodec;->sCodecs:[Landroid/net/rtp/AudioCodec;

    #@4
    array-length v1, v1

    #@5
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, [Landroid/net/rtp/AudioCodec;

    #@b
    return-object v0
.end method
