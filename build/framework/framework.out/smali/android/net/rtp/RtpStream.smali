.class public Landroid/net/rtp/RtpStream;
.super Ljava/lang/Object;
.source "RtpStream.java"


# static fields
.field private static final MODE_LAST:I = 0x2

.field public static final MODE_NORMAL:I = 0x0

.field public static final MODE_RECEIVE_ONLY:I = 0x2

.field public static final MODE_SEND_ONLY:I = 0x1


# instance fields
.field private final mLocalAddress:Ljava/net/InetAddress;

.field private final mLocalPort:I

.field private mMode:I

.field private mRemoteAddress:Ljava/net/InetAddress;

.field private mRemotePort:I

.field private mSocket:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 59
    const-string/jumbo v0, "rtp_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 60
    return-void
.end method

.method constructor <init>(Ljava/net/InetAddress;)V
    .registers 4
    .parameter "address"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 54
    iput v1, p0, Landroid/net/rtp/RtpStream;->mRemotePort:I

    #@6
    .line 55
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/net/rtp/RtpStream;->mMode:I

    #@9
    .line 57
    iput v1, p0, Landroid/net/rtp/RtpStream;->mSocket:I

    #@b
    .line 71
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    invoke-direct {p0, v0}, Landroid/net/rtp/RtpStream;->create(Ljava/lang/String;)I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/net/rtp/RtpStream;->mLocalPort:I

    #@15
    .line 72
    iput-object p1, p0, Landroid/net/rtp/RtpStream;->mLocalAddress:Ljava/net/InetAddress;

    #@17
    .line 73
    return-void
.end method

.method private native close()V
.end method

.method private native create(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation
.end method


# virtual methods
.method public associate(Ljava/net/InetAddress;I)V
    .registers 5
    .parameter "address"
    .parameter "port"

    #@0
    .prologue
    .line 154
    invoke-virtual {p0}, Landroid/net/rtp/RtpStream;->isBusy()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 155
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Busy"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 157
    :cond_e
    instance-of v0, p1, Ljava/net/Inet4Address;

    #@10
    if-eqz v0, :cond_18

    #@12
    iget-object v0, p0, Landroid/net/rtp/RtpStream;->mLocalAddress:Ljava/net/InetAddress;

    #@14
    instance-of v0, v0, Ljava/net/Inet4Address;

    #@16
    if-nez v0, :cond_2a

    #@18
    :cond_18
    instance-of v0, p1, Ljava/net/Inet6Address;

    #@1a
    if-eqz v0, :cond_22

    #@1c
    iget-object v0, p0, Landroid/net/rtp/RtpStream;->mLocalAddress:Ljava/net/InetAddress;

    #@1e
    instance-of v0, v0, Ljava/net/Inet6Address;

    #@20
    if-nez v0, :cond_2a

    #@22
    .line 159
    :cond_22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v1, "Unsupported address"

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 161
    :cond_2a
    if-ltz p2, :cond_31

    #@2c
    const v0, 0xffff

    #@2f
    if-le p2, v0, :cond_39

    #@31
    .line 162
    :cond_31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@33
    const-string v1, "Invalid port"

    #@35
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@38
    throw v0

    #@39
    .line 164
    :cond_39
    iput-object p1, p0, Landroid/net/rtp/RtpStream;->mRemoteAddress:Ljava/net/InetAddress;

    #@3b
    .line 165
    iput p2, p0, Landroid/net/rtp/RtpStream;->mRemotePort:I

    #@3d
    .line 166
    return-void
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 192
    invoke-direct {p0}, Landroid/net/rtp/RtpStream;->close()V

    #@3
    .line 193
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 194
    return-void
.end method

.method public getLocalAddress()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Landroid/net/rtp/RtpStream;->mLocalAddress:Ljava/net/InetAddress;

    #@2
    return-object v0
.end method

.method public getLocalPort()I
    .registers 2

    #@0
    .prologue
    .line 88
    iget v0, p0, Landroid/net/rtp/RtpStream;->mLocalPort:I

    #@2
    return v0
.end method

.method public getMode()I
    .registers 2

    #@0
    .prologue
    .line 120
    iget v0, p0, Landroid/net/rtp/RtpStream;->mMode:I

    #@2
    return v0
.end method

.method public getRemoteAddress()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Landroid/net/rtp/RtpStream;->mRemoteAddress:Ljava/net/InetAddress;

    #@2
    return-object v0
.end method

.method public getRemotePort()I
    .registers 2

    #@0
    .prologue
    .line 104
    iget v0, p0, Landroid/net/rtp/RtpStream;->mRemotePort:I

    #@2
    return v0
.end method

.method getSocket()I
    .registers 2

    #@0
    .prologue
    .line 169
    iget v0, p0, Landroid/net/rtp/RtpStream;->mSocket:I

    #@2
    return v0
.end method

.method public isBusy()Z
    .registers 2

    #@0
    .prologue
    .line 113
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    .line 180
    monitor-enter p0

    #@1
    .line 181
    :try_start_1
    invoke-virtual {p0}, Landroid/net/rtp/RtpStream;->isBusy()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Busy"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 185
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 184
    :cond_12
    :try_start_12
    invoke-direct {p0}, Landroid/net/rtp/RtpStream;->close()V

    #@15
    .line 185
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_12 .. :try_end_16} :catchall_f

    #@16
    .line 186
    return-void
.end method

.method public setMode(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 133
    invoke-virtual {p0}, Landroid/net/rtp/RtpStream;->isBusy()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 134
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Busy"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 136
    :cond_e
    if-ltz p1, :cond_13

    #@10
    const/4 v0, 0x2

    #@11
    if-le p1, v0, :cond_1b

    #@13
    .line 137
    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v1, "Invalid mode"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 139
    :cond_1b
    iput p1, p0, Landroid/net/rtp/RtpStream;->mMode:I

    #@1d
    .line 140
    return-void
.end method
