.class public Landroid/net/rtp/AudioGroup;
.super Ljava/lang/Object;
.source "AudioGroup.java"


# static fields
.field public static final MODE_ECHO_SUPPRESSION:I = 0x3

.field private static final MODE_LAST:I = 0x3

.field public static final MODE_MUTED:I = 0x1

.field public static final MODE_NORMAL:I = 0x2

.field public static final MODE_ON_HOLD:I


# instance fields
.field private mMode:I

.field private mNative:I

.field private final mStreams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/rtp/AudioStream;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 98
    const-string/jumbo v0, "rtp_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 99
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 104
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 94
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/net/rtp/AudioGroup;->mMode:I

    #@6
    .line 105
    new-instance v0, Ljava/util/HashMap;

    #@8
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@b
    iput-object v0, p0, Landroid/net/rtp/AudioGroup;->mStreams:Ljava/util/Map;

    #@d
    .line 106
    return-void
.end method

.method private native nativeAdd(IILjava/lang/String;ILjava/lang/String;I)I
.end method

.method private native nativeRemove(I)V
.end method

.method private native nativeSendDtmf(I)V
.end method

.method private native nativeSetMode(I)V
.end method


# virtual methods
.method declared-synchronized add(Landroid/net/rtp/AudioStream;)V
    .registers 12
    .parameter "stream"

    #@0
    .prologue
    .line 146
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/net/rtp/AudioGroup;->mStreams:Ljava/util/Map;

    #@3
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_58

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_4f

    #@9
    .line 148
    :try_start_9
    invoke-virtual {p1}, Landroid/net/rtp/AudioStream;->getCodec()Landroid/net/rtp/AudioCodec;

    #@c
    move-result-object v7

    #@d
    .line 149
    .local v7, codec:Landroid/net/rtp/AudioCodec;
    const-string v0, "%d %s %s"

    #@f
    const/4 v1, 0x3

    #@10
    new-array v1, v1, [Ljava/lang/Object;

    #@12
    const/4 v2, 0x0

    #@13
    iget v3, v7, Landroid/net/rtp/AudioCodec;->type:I

    #@15
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x1

    #@1c
    iget-object v3, v7, Landroid/net/rtp/AudioCodec;->rtpmap:Ljava/lang/String;

    #@1e
    aput-object v3, v1, v2

    #@20
    const/4 v2, 0x2

    #@21
    iget-object v3, v7, Landroid/net/rtp/AudioCodec;->fmtp:Ljava/lang/String;

    #@23
    aput-object v3, v1, v2

    #@25
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    .line 151
    .local v5, codecSpec:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/rtp/AudioStream;->getMode()I

    #@2c
    move-result v1

    #@2d
    invoke-virtual {p1}, Landroid/net/rtp/AudioStream;->getSocket()I

    #@30
    move-result v2

    #@31
    invoke-virtual {p1}, Landroid/net/rtp/AudioStream;->getRemoteAddress()Ljava/net/InetAddress;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {p1}, Landroid/net/rtp/AudioStream;->getRemotePort()I

    #@3c
    move-result v4

    #@3d
    invoke-virtual {p1}, Landroid/net/rtp/AudioStream;->getDtmfType()I

    #@40
    move-result v6

    #@41
    move-object v0, p0

    #@42
    invoke-direct/range {v0 .. v6}, Landroid/net/rtp/AudioGroup;->nativeAdd(IILjava/lang/String;ILjava/lang/String;I)I

    #@45
    move-result v9

    #@46
    .line 154
    .local v9, id:I
    iget-object v0, p0, Landroid/net/rtp/AudioGroup;->mStreams:Ljava/util/Map;

    #@48
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b
    move-result-object v1

    #@4c
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4f
    .catchall {:try_start_9 .. :try_end_4f} :catchall_58
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_4f} :catch_51

    #@4f
    .line 159
    .end local v5           #codecSpec:Ljava/lang/String;
    .end local v7           #codec:Landroid/net/rtp/AudioCodec;
    .end local v9           #id:I
    :cond_4f
    monitor-exit p0

    #@50
    return-void

    #@51
    .line 155
    :catch_51
    move-exception v8

    #@52
    .line 156
    .local v8, e:Ljava/lang/NullPointerException;
    :try_start_52
    new-instance v0, Ljava/lang/IllegalStateException;

    #@54
    invoke-direct {v0, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    #@57
    throw v0
    :try_end_58
    .catchall {:try_start_52 .. :try_end_58} :catchall_58

    #@58
    .line 146
    .end local v8           #e:Ljava/lang/NullPointerException;
    :catchall_58
    move-exception v0

    #@59
    monitor-exit p0

    #@5a
    throw v0
.end method

.method public clear()V
    .registers 6

    #@0
    .prologue
    .line 195
    invoke-virtual {p0}, Landroid/net/rtp/AudioGroup;->getStreams()[Landroid/net/rtp/AudioStream;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Landroid/net/rtp/AudioStream;
    array-length v2, v0

    #@5
    .local v2, len$:I
    const/4 v1, 0x0

    #@6
    .local v1, i$:I
    :goto_6
    if-ge v1, v2, :cond_11

    #@8
    aget-object v3, v0, v1

    #@a
    .line 196
    .local v3, stream:Landroid/net/rtp/AudioStream;
    const/4 v4, 0x0

    #@b
    invoke-virtual {v3, v4}, Landroid/net/rtp/AudioStream;->join(Landroid/net/rtp/AudioGroup;)V

    #@e
    .line 195
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_6

    #@11
    .line 198
    .end local v3           #stream:Landroid/net/rtp/AudioStream;
    :cond_11
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/net/rtp/AudioGroup;->nativeRemove(I)V

    #@4
    .line 203
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@7
    .line 204
    return-void
.end method

.method public getMode()I
    .registers 2

    #@0
    .prologue
    .line 121
    iget v0, p0, Landroid/net/rtp/AudioGroup;->mMode:I

    #@2
    return v0
.end method

.method public getStreams()[Landroid/net/rtp/AudioStream;
    .registers 3

    #@0
    .prologue
    .line 112
    monitor-enter p0

    #@1
    .line 113
    :try_start_1
    iget-object v0, p0, Landroid/net/rtp/AudioGroup;->mStreams:Ljava/util/Map;

    #@3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@6
    move-result-object v0

    #@7
    iget-object v1, p0, Landroid/net/rtp/AudioGroup;->mStreams:Ljava/util/Map;

    #@9
    invoke-interface {v1}, Ljava/util/Map;->size()I

    #@c
    move-result v1

    #@d
    new-array v1, v1, [Landroid/net/rtp/AudioStream;

    #@f
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, [Landroid/net/rtp/AudioStream;

    #@15
    monitor-exit p0

    #@16
    return-object v0

    #@17
    .line 114
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method declared-synchronized remove(Landroid/net/rtp/AudioStream;)V
    .registers 4
    .parameter "stream"

    #@0
    .prologue
    .line 166
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/net/rtp/AudioGroup;->mStreams:Ljava/util/Map;

    #@3
    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Ljava/lang/Integer;

    #@9
    .line 167
    .local v0, id:Ljava/lang/Integer;
    if-eqz v0, :cond_12

    #@b
    .line 168
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@e
    move-result v1

    #@f
    invoke-direct {p0, v1}, Landroid/net/rtp/AudioGroup;->nativeRemove(I)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    #@12
    .line 170
    :cond_12
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 166
    .end local v0           #id:Ljava/lang/Integer;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0

    #@16
    throw v1
.end method

.method public sendDtmf(I)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 181
    if-ltz p1, :cond_6

    #@2
    const/16 v0, 0xf

    #@4
    if-le p1, v0, :cond_e

    #@6
    .line 182
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Invalid event"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 184
    :cond_e
    monitor-enter p0

    #@f
    .line 185
    :try_start_f
    invoke-direct {p0, p1}, Landroid/net/rtp/AudioGroup;->nativeSendDtmf(I)V

    #@12
    .line 186
    monitor-exit p0

    #@13
    .line 187
    return-void

    #@14
    .line 186
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_f .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public setMode(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 133
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p1, v0, :cond_d

    #@5
    .line 134
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "Invalid mode"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 136
    :cond_d
    monitor-enter p0

    #@e
    .line 137
    :try_start_e
    invoke-direct {p0, p1}, Landroid/net/rtp/AudioGroup;->nativeSetMode(I)V

    #@11
    .line 138
    iput p1, p0, Landroid/net/rtp/AudioGroup;->mMode:I

    #@13
    .line 139
    monitor-exit p0

    #@14
    .line 140
    return-void

    #@15
    .line 139
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method
