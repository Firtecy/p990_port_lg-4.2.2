.class public Landroid/net/rtp/AudioStream;
.super Landroid/net/rtp/RtpStream;
.source "AudioStream.java"


# instance fields
.field private mCodec:Landroid/net/rtp/AudioCodec;

.field private mDtmfType:I

.field private mGroup:Landroid/net/rtp/AudioGroup;


# direct methods
.method public constructor <init>(Ljava/net/InetAddress;)V
    .registers 3
    .parameter "address"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/net/rtp/RtpStream;-><init>(Ljava/net/InetAddress;)V

    #@3
    .line 47
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/net/rtp/AudioStream;->mDtmfType:I

    #@6
    .line 60
    return-void
.end method


# virtual methods
.method public getCodec()Landroid/net/rtp/AudioCodec;
    .registers 2

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mCodec:Landroid/net/rtp/AudioCodec;

    #@2
    return-object v0
.end method

.method public getDtmfType()I
    .registers 2

    #@0
    .prologue
    .line 136
    iget v0, p0, Landroid/net/rtp/AudioStream;->mDtmfType:I

    #@2
    return v0
.end method

.method public getGroup()Landroid/net/rtp/AudioGroup;
    .registers 2

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@2
    return-object v0
.end method

.method public final isBusy()Z
    .registers 2

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public join(Landroid/net/rtp/AudioGroup;)V
    .registers 3
    .parameter "group"

    #@0
    .prologue
    .line 88
    monitor-enter p0

    #@1
    .line 89
    :try_start_1
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@3
    if-ne v0, p1, :cond_7

    #@5
    .line 90
    monitor-exit p0

    #@6
    .line 101
    :goto_6
    return-void

    #@7
    .line 92
    :cond_7
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@9
    if-eqz v0, :cond_13

    #@b
    .line 93
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@d
    invoke-virtual {v0, p0}, Landroid/net/rtp/AudioGroup;->remove(Landroid/net/rtp/AudioStream;)V

    #@10
    .line 94
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@13
    .line 96
    :cond_13
    if-eqz p1, :cond_1a

    #@15
    .line 97
    invoke-virtual {p1, p0}, Landroid/net/rtp/AudioGroup;->add(Landroid/net/rtp/AudioStream;)V

    #@18
    .line 98
    iput-object p1, p0, Landroid/net/rtp/AudioStream;->mGroup:Landroid/net/rtp/AudioGroup;

    #@1a
    .line 100
    :cond_1a
    monitor-exit p0

    #@1b
    goto :goto_6

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public setCodec(Landroid/net/rtp/AudioCodec;)V
    .registers 4
    .parameter "codec"

    #@0
    .prologue
    .line 120
    invoke-virtual {p0}, Landroid/net/rtp/AudioStream;->isBusy()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Busy"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 123
    :cond_e
    iget v0, p1, Landroid/net/rtp/AudioCodec;->type:I

    #@10
    iget v1, p0, Landroid/net/rtp/AudioStream;->mDtmfType:I

    #@12
    if-ne v0, v1, :cond_1c

    #@14
    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "The type is used by DTMF"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 126
    :cond_1c
    iput-object p1, p0, Landroid/net/rtp/AudioStream;->mCodec:Landroid/net/rtp/AudioCodec;

    #@1e
    .line 127
    return-void
.end method

.method public setDtmfType(I)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 154
    invoke-virtual {p0}, Landroid/net/rtp/AudioStream;->isBusy()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 155
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Busy"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 157
    :cond_e
    const/4 v0, -0x1

    #@f
    if-eq p1, v0, :cond_33

    #@11
    .line 158
    const/16 v0, 0x60

    #@13
    if-lt p1, v0, :cond_19

    #@15
    const/16 v0, 0x7f

    #@17
    if-le p1, v0, :cond_21

    #@19
    .line 159
    :cond_19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    const-string v1, "Invalid type"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 161
    :cond_21
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mCodec:Landroid/net/rtp/AudioCodec;

    #@23
    if-eqz v0, :cond_33

    #@25
    iget-object v0, p0, Landroid/net/rtp/AudioStream;->mCodec:Landroid/net/rtp/AudioCodec;

    #@27
    iget v0, v0, Landroid/net/rtp/AudioCodec;->type:I

    #@29
    if-ne p1, v0, :cond_33

    #@2b
    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2d
    const-string v1, "The type is used by codec"

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v0

    #@33
    .line 165
    :cond_33
    iput p1, p0, Landroid/net/rtp/AudioStream;->mDtmfType:I

    #@35
    .line 166
    return-void
.end method
