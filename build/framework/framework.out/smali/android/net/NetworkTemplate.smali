.class public Landroid/net/NetworkTemplate;
.super Ljava/lang/Object;
.source "NetworkTemplate.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/NetworkTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATA_USAGE_NETWORK_TYPES:[I = null

.field public static final MATCH_ETHERNET:I = 0x5

.field public static final MATCH_MOBILE_3G_LOWER:I = 0x2

.field public static final MATCH_MOBILE_4G:I = 0x3

.field public static final MATCH_MOBILE_ALL:I = 0x1

.field public static final MATCH_MOBILE_WILDCARD:I = 0x6

.field public static final MATCH_WIFI:I = 0x4

.field public static final MATCH_WIFI_WILDCARD:I = 0x7

.field private static sForceAllNetworkTypes:Z


# instance fields
.field private final mMatchRule:I

.field private final mNetworkId:Ljava/lang/String;

.field private final mSubscriberId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 66
    const-string/jumbo v0, "ro.afwdata.LGfeatureset"

    #@3
    const-string/jumbo v1, "none"

    #@6
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v1, "SKTBASE"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_24

    #@12
    const-string/jumbo v0, "ro.afwdata.LGfeatureset"

    #@15
    const-string/jumbo v1, "none"

    #@18
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, "KTBASE"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_3c

    #@24
    .line 68
    :cond_24
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@27
    move-result-object v0

    #@28
    const v1, 0x107001d

    #@2b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@2e
    move-result-object v0

    #@2f
    sput-object v0, Landroid/net/NetworkTemplate;->DATA_USAGE_NETWORK_TYPES:[I

    #@31
    .line 90
    :goto_31
    const/4 v0, 0x0

    #@32
    sput-boolean v0, Landroid/net/NetworkTemplate;->sForceAllNetworkTypes:Z

    #@34
    .line 369
    new-instance v0, Landroid/net/NetworkTemplate$1;

    #@36
    invoke-direct {v0}, Landroid/net/NetworkTemplate$1;-><init>()V

    #@39
    sput-object v0, Landroid/net/NetworkTemplate;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3b
    return-void

    #@3c
    .line 72
    :cond_3c
    const-string/jumbo v0, "ro.afwdata.LGfeatureset"

    #@3f
    const-string/jumbo v1, "none"

    #@42
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    const-string v1, "VZWBASE"

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_5c

    #@4e
    .line 73
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@51
    move-result-object v0

    #@52
    const v1, 0x107001e

    #@55
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@58
    move-result-object v0

    #@59
    sput-object v0, Landroid/net/NetworkTemplate;->DATA_USAGE_NETWORK_TYPES:[I

    #@5b
    goto :goto_31

    #@5c
    .line 78
    :cond_5c
    const-string/jumbo v0, "ro.afwdata.LGfeatureset"

    #@5f
    const-string/jumbo v1, "none"

    #@62
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    const-string v1, "SPCSBASE"

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_7c

    #@6e
    .line 79
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@71
    move-result-object v0

    #@72
    const v1, 0x107001f

    #@75
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@78
    move-result-object v0

    #@79
    sput-object v0, Landroid/net/NetworkTemplate;->DATA_USAGE_NETWORK_TYPES:[I

    #@7b
    goto :goto_31

    #@7c
    .line 84
    :cond_7c
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@7f
    move-result-object v0

    #@80
    const v1, 0x107001b

    #@83
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@86
    move-result-object v0

    #@87
    sput-object v0, Landroid/net/NetworkTemplate;->DATA_USAGE_NETWORK_TYPES:[I

    #@89
    goto :goto_31
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "matchRule"
    .parameter "subscriberId"
    .parameter "networkId"

    #@0
    .prologue
    .line 164
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 165
    iput p1, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@5
    .line 166
    iput-object p2, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@7
    .line 167
    iput-object p3, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@9
    .line 168
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 170
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@9
    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@f
    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@15
    .line 174
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/net/NetworkTemplate$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public static buildTemplateEthernet()Landroid/net/NetworkTemplate;
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 157
    new-instance v0, Landroid/net/NetworkTemplate;

    #@3
    const/4 v1, 0x5

    #@4
    invoke-direct {v0, v1, v2, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public static buildTemplateMobile3gLower(Ljava/lang/String;)Landroid/net/NetworkTemplate;
    .registers 4
    .parameter "subscriberId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 111
    new-instance v0, Landroid/net/NetworkTemplate;

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public static buildTemplateMobile4g(Ljava/lang/String;)Landroid/net/NetworkTemplate;
    .registers 4
    .parameter "subscriberId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 120
    new-instance v0, Landroid/net/NetworkTemplate;

    #@2
    const/4 v1, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public static buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;
    .registers 4
    .parameter "subscriberId"

    #@0
    .prologue
    .line 102
    new-instance v0, Landroid/net/NetworkTemplate;

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, p0, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public static buildTemplateMobileWildcard()Landroid/net/NetworkTemplate;
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 128
    new-instance v0, Landroid/net/NetworkTemplate;

    #@3
    const/4 v1, 0x6

    #@4
    invoke-direct {v0, v1, v2, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public static buildTemplateWifi()Landroid/net/NetworkTemplate;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 141
    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifiWildcard()Landroid/net/NetworkTemplate;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static buildTemplateWifi(Ljava/lang/String;)Landroid/net/NetworkTemplate;
    .registers 4
    .parameter "networkId"

    #@0
    .prologue
    .line 149
    new-instance v0, Landroid/net/NetworkTemplate;

    #@2
    const/4 v1, 0x4

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, v1, v2, p0}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public static buildTemplateWifiWildcard()Landroid/net/NetworkTemplate;
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 136
    new-instance v0, Landroid/net/NetworkTemplate;

    #@3
    const/4 v1, 0x7

    #@4
    invoke-direct {v0, v1, v2, v2}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method private static ensureSubtypeAvailable()V
    .registers 2

    #@0
    .prologue
    .line 364
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2
    const-string v1, "Unable to enforce 3G_LOWER template on combined data."

    #@4
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public static forceAllNetworkTypes()V
    .registers 1

    #@0
    .prologue
    .line 94
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Landroid/net/NetworkTemplate;->sForceAllNetworkTypes:Z

    #@3
    .line 95
    return-void
.end method

.method private static getMatchRuleName(I)Ljava/lang/String;
    .registers 2
    .parameter "matchRule"

    #@0
    .prologue
    .line 342
    packed-switch p0, :pswitch_data_1c

    #@3
    .line 358
    const-string v0, "UNKNOWN"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 344
    :pswitch_6
    const-string v0, "MOBILE_3G_LOWER"

    #@8
    goto :goto_5

    #@9
    .line 346
    :pswitch_9
    const-string v0, "MOBILE_4G"

    #@b
    goto :goto_5

    #@c
    .line 348
    :pswitch_c
    const-string v0, "MOBILE_ALL"

    #@e
    goto :goto_5

    #@f
    .line 350
    :pswitch_f
    const-string v0, "WIFI"

    #@11
    goto :goto_5

    #@12
    .line 352
    :pswitch_12
    const-string v0, "ETHERNET"

    #@14
    goto :goto_5

    #@15
    .line 354
    :pswitch_15
    const-string v0, "MOBILE_WILDCARD"

    #@17
    goto :goto_5

    #@18
    .line 356
    :pswitch_18
    const-string v0, "WIFI_WILDCARD"

    #@1a
    goto :goto_5

    #@1b
    .line 342
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_c
        :pswitch_6
        :pswitch_9
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method private matchesEthernet(Landroid/net/NetworkIdentity;)Z
    .registers 4
    .parameter "ident"

    #@0
    .prologue
    .line 317
    iget v0, p1, Landroid/net/NetworkIdentity;->mType:I

    #@2
    const/16 v1, 0x9

    #@4
    if-ne v0, v1, :cond_8

    #@6
    .line 318
    const/4 v0, 0x1

    #@7
    .line 320
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private matchesMobile(Landroid/net/NetworkIdentity;)Z
    .registers 5
    .parameter "ident"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 257
    iget v1, p1, Landroid/net/NetworkIdentity;->mType:I

    #@3
    const/4 v2, 0x6

    #@4
    if-ne v1, v2, :cond_7

    #@6
    .line 261
    :cond_6
    :goto_6
    return v0

    #@7
    :cond_7
    sget-boolean v1, Landroid/net/NetworkTemplate;->sForceAllNetworkTypes:Z

    #@9
    if-nez v1, :cond_15

    #@b
    sget-object v1, Landroid/net/NetworkTemplate;->DATA_USAGE_NETWORK_TYPES:[I

    #@d
    iget v2, p1, Landroid/net/NetworkIdentity;->mType:I

    #@f
    invoke-static {v1, v2}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_1f

    #@15
    :cond_15
    iget-object v1, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@17
    iget-object v2, p1, Landroid/net/NetworkIdentity;->mSubscriberId:Ljava/lang/String;

    #@19
    invoke-static {v1, v2}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_6

    #@1f
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_6
.end method

.method private matchesMobile3gLower(Landroid/net/NetworkIdentity;)Z
    .registers 5
    .parameter "ident"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 270
    invoke-static {}, Landroid/net/NetworkTemplate;->ensureSubtypeAvailable()V

    #@4
    .line 271
    iget v1, p1, Landroid/net/NetworkIdentity;->mType:I

    #@6
    const/4 v2, 0x6

    #@7
    if-ne v1, v2, :cond_a

    #@9
    .line 281
    :cond_9
    :goto_9
    return v0

    #@a
    .line 273
    :cond_a
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesMobile(Landroid/net/NetworkIdentity;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_9

    #@10
    .line 274
    iget v1, p1, Landroid/net/NetworkIdentity;->mSubType:I

    #@12
    invoke-static {v1}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    #@15
    move-result v1

    #@16
    packed-switch v1, :pswitch_data_1c

    #@19
    goto :goto_9

    #@1a
    .line 278
    :pswitch_1a
    const/4 v0, 0x1

    #@1b
    goto :goto_9

    #@1c
    .line 274
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1a
        :pswitch_1a
    .end packed-switch
.end method

.method private matchesMobile4g(Landroid/net/NetworkIdentity;)Z
    .registers 5
    .parameter "ident"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 288
    invoke-static {}, Landroid/net/NetworkTemplate;->ensureSubtypeAvailable()V

    #@4
    .line 289
    iget v1, p1, Landroid/net/NetworkIdentity;->mType:I

    #@6
    const/4 v2, 0x6

    #@7
    if-ne v1, v2, :cond_a

    #@9
    .line 298
    :goto_9
    :pswitch_9
    return v0

    #@a
    .line 292
    :cond_a
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesMobile(Landroid/net/NetworkIdentity;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_19

    #@10
    .line 293
    iget v1, p1, Landroid/net/NetworkIdentity;->mSubType:I

    #@12
    invoke-static {v1}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    #@15
    move-result v1

    #@16
    packed-switch v1, :pswitch_data_1c

    #@19
    .line 298
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_9

    #@1b
    .line 293
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x3
        :pswitch_9
    .end packed-switch
.end method

.method private matchesMobileWildcard(Landroid/net/NetworkIdentity;)Z
    .registers 5
    .parameter "ident"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 324
    iget v1, p1, Landroid/net/NetworkIdentity;->mType:I

    #@3
    const/4 v2, 0x6

    #@4
    if-ne v1, v2, :cond_7

    #@6
    .line 327
    :cond_6
    :goto_6
    return v0

    #@7
    :cond_7
    sget-boolean v1, Landroid/net/NetworkTemplate;->sForceAllNetworkTypes:Z

    #@9
    if-nez v1, :cond_6

    #@b
    sget-object v1, Landroid/net/NetworkTemplate;->DATA_USAGE_NETWORK_TYPES:[I

    #@d
    iget v2, p1, Landroid/net/NetworkIdentity;->mType:I

    #@f
    invoke-static {v1, v2}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_6

    #@15
    const/4 v0, 0x0

    #@16
    goto :goto_6
.end method

.method private matchesWifi(Landroid/net/NetworkIdentity;)Z
    .registers 4
    .parameter "ident"

    #@0
    .prologue
    .line 305
    iget v0, p1, Landroid/net/NetworkIdentity;->mType:I

    #@2
    packed-switch v0, :pswitch_data_10

    #@5
    .line 309
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 307
    :pswitch_7
    iget-object v0, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@9
    iget-object v1, p1, Landroid/net/NetworkIdentity;->mNetworkId:Ljava/lang/String;

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    goto :goto_6

    #@10
    .line 305
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method

.method private matchesWifiWildcard(Landroid/net/NetworkIdentity;)Z
    .registers 3
    .parameter "ident"

    #@0
    .prologue
    .line 332
    iget v0, p1, Landroid/net/NetworkIdentity;->mType:I

    #@2
    sparse-switch v0, :sswitch_data_a

    #@5
    .line 337
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 335
    :sswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_6

    #@9
    .line 332
    nop

    #@a
    :sswitch_data_a
    .sparse-switch
        0x1 -> :sswitch_7
        0xd -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 208
    instance-of v2, p1, Landroid/net/NetworkTemplate;

    #@3
    if-eqz v2, :cond_23

    #@5
    move-object v0, p1

    #@6
    .line 209
    check-cast v0, Landroid/net/NetworkTemplate;

    #@8
    .line 210
    .local v0, other:Landroid/net/NetworkTemplate;
    iget v2, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@a
    iget v3, v0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@c
    if-ne v2, v3, :cond_23

    #@e
    iget-object v2, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@10
    iget-object v3, v0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@12
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_23

    #@18
    iget-object v2, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@1a
    iget-object v3, v0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@1c
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_23

    #@22
    const/4 v1, 0x1

    #@23
    .line 214
    .end local v0           #other:Landroid/net/NetworkTemplate;
    :cond_23
    return v1
.end method

.method public getMatchRule()I
    .registers 2

    #@0
    .prologue
    .line 218
    iget v0, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@2
    return v0
.end method

.method public getNetworkId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 203
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    iget v2, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v2

    #@a
    aput-object v2, v0, v1

    #@c
    const/4 v1, 0x1

    #@d
    iget-object v2, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@f
    aput-object v2, v0, v1

    #@11
    const/4 v1, 0x2

    #@12
    iget-object v2, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@14
    aput-object v2, v0, v1

    #@16
    invoke-static {v0}, Lcom/android/internal/util/Objects;->hashCode([Ljava/lang/Object;)I

    #@19
    move-result v0

    #@1a
    return v0
.end method

.method public matches(Landroid/net/NetworkIdentity;)Z
    .registers 4
    .parameter "ident"

    #@0
    .prologue
    .line 233
    iget v0, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@2
    packed-switch v0, :pswitch_data_32

    #@5
    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "unknown network template"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 235
    :pswitch_e
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesMobile(Landroid/net/NetworkIdentity;)Z

    #@11
    move-result v0

    #@12
    .line 247
    :goto_12
    return v0

    #@13
    .line 237
    :pswitch_13
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesMobile3gLower(Landroid/net/NetworkIdentity;)Z

    #@16
    move-result v0

    #@17
    goto :goto_12

    #@18
    .line 239
    :pswitch_18
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesMobile4g(Landroid/net/NetworkIdentity;)Z

    #@1b
    move-result v0

    #@1c
    goto :goto_12

    #@1d
    .line 241
    :pswitch_1d
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesWifi(Landroid/net/NetworkIdentity;)Z

    #@20
    move-result v0

    #@21
    goto :goto_12

    #@22
    .line 243
    :pswitch_22
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesEthernet(Landroid/net/NetworkIdentity;)Z

    #@25
    move-result v0

    #@26
    goto :goto_12

    #@27
    .line 245
    :pswitch_27
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesMobileWildcard(Landroid/net/NetworkIdentity;)Z

    #@2a
    move-result v0

    #@2b
    goto :goto_12

    #@2c
    .line 247
    :pswitch_2c
    invoke-direct {p0, p1}, Landroid/net/NetworkTemplate;->matchesWifiWildcard(Landroid/net/NetworkIdentity;)Z

    #@2f
    move-result v0

    #@30
    goto :goto_12

    #@31
    .line 233
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_e
        :pswitch_13
        :pswitch_18
        :pswitch_1d
        :pswitch_22
        :pswitch_27
        :pswitch_2c
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "NetworkTemplate: "

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 191
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "matchRule="

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget v2, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@10
    invoke-static {v2}, Landroid/net/NetworkTemplate;->getMatchRuleName(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 192
    iget-object v1, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@19
    if-eqz v1, :cond_2a

    #@1b
    .line 193
    const-string v1, ", subscriberId="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget-object v2, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@23
    invoke-static {v2}, Landroid/net/NetworkIdentity;->scrubSubscriberId(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 195
    :cond_2a
    iget-object v1, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@2c
    if-eqz v1, :cond_39

    #@2e
    .line 196
    const-string v1, ", networkId="

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    iget-object v2, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    .line 198
    :cond_39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 178
    iget v0, p0, Landroid/net/NetworkTemplate;->mMatchRule:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 179
    iget-object v0, p0, Landroid/net/NetworkTemplate;->mSubscriberId:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 180
    iget-object v0, p0, Landroid/net/NetworkTemplate;->mNetworkId:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 181
    return-void
.end method
