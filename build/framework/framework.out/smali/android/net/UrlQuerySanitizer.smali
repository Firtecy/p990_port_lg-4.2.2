.class public Landroid/net/UrlQuerySanitizer;
.super Ljava/lang/Object;
.source "UrlQuerySanitizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;,
        Landroid/net/UrlQuerySanitizer$ValueSanitizer;,
        Landroid/net/UrlQuerySanitizer$ParameterValuePair;
    }
.end annotation


# static fields
.field private static final sAllButNulAndAngleBracketsLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sAllButNulLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sAllButWhitespaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sAllIllegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sAmpAndSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sAmpLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sURLLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

.field private static final sUrlAndSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;


# instance fields
.field private mAllowUnregisteredParamaters:Z

.field private final mEntries:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntriesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/UrlQuerySanitizer$ParameterValuePair;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferFirstRepeatedParameter:Z

.field private final mSanitizers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/UrlQuerySanitizer$ValueSanitizer;",
            ">;"
        }
    .end annotation
.end field

.field private mUnregisteredParameterValueSanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 437
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@6
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sAllIllegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@8
    .line 441
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@a
    const/16 v1, 0x5ff

    #@c
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@f
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sAllButNulLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@11
    .line 445
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@13
    const/16 v1, 0x5fc

    #@15
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@18
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sAllButWhitespaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@1a
    .line 449
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@1c
    const/16 v1, 0x194

    #@1e
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@21
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sURLLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@23
    .line 453
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@25
    const/16 v1, 0x195

    #@27
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@2a
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sUrlAndSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2c
    .line 457
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@2e
    const/16 v1, 0x80

    #@30
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@33
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sAmpLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@35
    .line 461
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@37
    const/16 v1, 0x81

    #@39
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@3c
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sAmpAndSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@3e
    .line 465
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@40
    const/4 v1, 0x1

    #@41
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@44
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@46
    .line 469
    new-instance v0, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;

    #@48
    const/16 v1, 0x59f

    #@4a
    invoke-direct {v0, v1}, Landroid/net/UrlQuerySanitizer$IllegalCharacterValueSanitizer;-><init>(I)V

    #@4d
    sput-object v0, Landroid/net/UrlQuerySanitizer;->sAllButNulAndAngleBracketsLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@4f
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 561
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mSanitizers:Ljava/util/HashMap;

    #@a
    .line 90
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@11
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntriesList:Ljava/util/ArrayList;

    #@18
    .line 96
    invoke-static {}, Landroid/net/UrlQuerySanitizer;->getAllIllegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mUnregisteredParameterValueSanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@1e
    .line 562
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    #@0
    .prologue
    .line 584
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mSanitizers:Ljava/util/HashMap;

    #@a
    .line 90
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@11
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntriesList:Ljava/util/ArrayList;

    #@18
    .line 96
    invoke-static {}, Landroid/net/UrlQuerySanitizer;->getAllIllegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/net/UrlQuerySanitizer;->mUnregisteredParameterValueSanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@1e
    .line 585
    const/4 v0, 0x1

    #@1f
    invoke-virtual {p0, v0}, Landroid/net/UrlQuerySanitizer;->setAllowUnregisteredParamaters(Z)V

    #@22
    .line 586
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->parseUrl(Ljava/lang/String;)V

    #@25
    .line 587
    return-void
.end method

.method public static final getAllButNulAndAngleBracketsLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 548
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sAllButNulAndAngleBracketsLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getAllButNulLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 488
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sAllButNulLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getAllButWhitespaceLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 497
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sAllButWhitespaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getAllIllegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 479
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sAllIllegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getAmpAndSpaceLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 531
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sAmpAndSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getAmpLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 523
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sAmpLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getSpaceLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 539
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getUrlAndSpaceLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 515
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sUrlAndSpaceLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public static final getUrlLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 1

    #@0
    .prologue
    .line 505
    sget-object v0, Landroid/net/UrlQuerySanitizer;->sURLLegal:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method


# virtual methods
.method protected addSanitizedEntry(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "parameter"
    .parameter "value"

    #@0
    .prologue
    .line 790
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntriesList:Ljava/util/ArrayList;

    #@2
    new-instance v1, Landroid/net/UrlQuerySanitizer$ParameterValuePair;

    #@4
    invoke-direct {v1, p0, p1, p2}, Landroid/net/UrlQuerySanitizer$ParameterValuePair;-><init>(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;Ljava/lang/String;)V

    #@7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 792
    iget-boolean v0, p0, Landroid/net/UrlQuerySanitizer;->mPreferFirstRepeatedParameter:Z

    #@c
    if-eqz v0, :cond_17

    #@e
    .line 793
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@10
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_17

    #@16
    .line 798
    :goto_16
    return-void

    #@17
    .line 797
    :cond_17
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@19
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    goto :goto_16
.end method

.method protected clear()V
    .registers 2

    #@0
    .prologue
    .line 909
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 910
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntriesList:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 911
    return-void
.end method

.method protected decodeHexDigit(C)I
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 890
    const/16 v0, 0x30

    #@2
    if-lt p1, v0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p1, v0, :cond_b

    #@8
    .line 891
    add-int/lit8 v0, p1, -0x30

    #@a
    .line 900
    :goto_a
    return v0

    #@b
    .line 893
    :cond_b
    const/16 v0, 0x41

    #@d
    if-lt p1, v0, :cond_18

    #@f
    const/16 v0, 0x46

    #@11
    if-gt p1, v0, :cond_18

    #@13
    .line 894
    add-int/lit8 v0, p1, -0x41

    #@15
    add-int/lit8 v0, v0, 0xa

    #@17
    goto :goto_a

    #@18
    .line 896
    :cond_18
    const/16 v0, 0x61

    #@1a
    if-lt p1, v0, :cond_25

    #@1c
    const/16 v0, 0x66

    #@1e
    if-gt p1, v0, :cond_25

    #@20
    .line 897
    add-int/lit8 v0, p1, -0x61

    #@22
    add-int/lit8 v0, v0, 0xa

    #@24
    goto :goto_a

    #@25
    .line 900
    :cond_25
    const/4 v0, -0x1

    #@26
    goto :goto_a
.end method

.method public getAllowUnregisteredParamaters()Z
    .registers 2

    #@0
    .prologue
    .line 729
    iget-boolean v0, p0, Landroid/net/UrlQuerySanitizer;->mAllowUnregisteredParamaters:Z

    #@2
    return v0
.end method

.method public getEffectiveValueSanitizer(Ljava/lang/String;)Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 4
    .parameter "parameter"

    #@0
    .prologue
    .line 820
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValueSanitizer(Ljava/lang/String;)Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@3
    move-result-object v0

    #@4
    .line 821
    .local v0, sanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    if-nez v0, :cond_e

    #@6
    iget-boolean v1, p0, Landroid/net/UrlQuerySanitizer;->mAllowUnregisteredParamaters:Z

    #@8
    if-eqz v1, :cond_e

    #@a
    .line 822
    invoke-virtual {p0}, Landroid/net/UrlQuerySanitizer;->getUnregisteredParameterValueSanitizer()Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@d
    move-result-object v0

    #@e
    .line 824
    :cond_e
    return-object v0
.end method

.method public getParameterList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/UrlQuerySanitizer$ParameterValuePair;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 652
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntriesList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getParameterSet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 642
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPreferFirstRepeatedParameter()Z
    .registers 2

    #@0
    .prologue
    .line 758
    iget-boolean v0, p0, Landroid/net/UrlQuerySanitizer;->mPreferFirstRepeatedParameter:Z

    #@2
    return v0
.end method

.method public getUnregisteredParameterValueSanitizer()Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 2

    #@0
    .prologue
    .line 420
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mUnregisteredParameterValueSanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    return-object v0
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "parameter"

    #@0
    .prologue
    .line 673
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public getValueSanitizer(Ljava/lang/String;)Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    .registers 3
    .parameter "parameter"

    #@0
    .prologue
    .line 808
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mSanitizers:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@8
    return-object v0
.end method

.method public hasParameter(Ljava/lang/String;)Z
    .registers 3
    .parameter "parameter"

    #@0
    .prologue
    .line 661
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mEntries:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected isHexDigit(C)Z
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 878
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->decodeHexDigit(C)I

    #@3
    move-result v0

    #@4
    if-ltz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected parseEntry(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "parameter"
    .parameter "value"

    #@0
    .prologue
    .line 771
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->unescape(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 772
    .local v1, unescapedParameter:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/net/UrlQuerySanitizer;->getEffectiveValueSanitizer(Ljava/lang/String;)Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@7
    move-result-object v3

    #@8
    .line 775
    .local v3, valueSanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;
    if-nez v3, :cond_b

    #@a
    .line 781
    :goto_a
    return-void

    #@b
    .line 778
    :cond_b
    invoke-virtual {p0, p2}, Landroid/net/UrlQuerySanitizer;->unescape(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 779
    .local v2, unescapedValue:Ljava/lang/String;
    invoke-interface {v3, v2}, Landroid/net/UrlQuerySanitizer$ValueSanitizer;->sanitize(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 780
    .local v0, sanitizedValue:Ljava/lang/String;
    invoke-virtual {p0, v1, v0}, Landroid/net/UrlQuerySanitizer;->addSanitizedEntry(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    goto :goto_a
.end method

.method public parseQuery(Ljava/lang/String;)V
    .registers 7
    .parameter "query"

    #@0
    .prologue
    .line 616
    invoke-virtual {p0}, Landroid/net/UrlQuerySanitizer;->clear()V

    #@3
    .line 618
    new-instance v2, Ljava/util/StringTokenizer;

    #@5
    const-string v3, "&"

    #@7
    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 619
    .local v2, tokenizer:Ljava/util/StringTokenizer;
    :cond_a
    :goto_a
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_37

    #@10
    .line 620
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 621
    .local v1, attributeValuePair:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@17
    move-result v3

    #@18
    if-lez v3, :cond_a

    #@1a
    .line 622
    const/16 v3, 0x3d

    #@1c
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    #@1f
    move-result v0

    #@20
    .line 623
    .local v0, assignmentIndex:I
    if-gez v0, :cond_28

    #@22
    .line 625
    const-string v3, ""

    #@24
    invoke-virtual {p0, v1, v3}, Landroid/net/UrlQuerySanitizer;->parseEntry(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    goto :goto_a

    #@28
    .line 628
    :cond_28
    const/4 v3, 0x0

    #@29
    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    add-int/lit8 v4, v0, 0x1

    #@2f
    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p0, v3, v4}, Landroid/net/UrlQuerySanitizer;->parseEntry(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    goto :goto_a

    #@37
    .line 633
    .end local v0           #assignmentIndex:I
    .end local v1           #attributeValuePair:Ljava/lang/String;
    :cond_37
    return-void
.end method

.method public parseUrl(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 597
    const/16 v2, 0x3f

    #@2
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    #@5
    move-result v1

    #@6
    .line 599
    .local v1, queryIndex:I
    if-ltz v1, :cond_12

    #@8
    .line 600
    add-int/lit8 v2, v1, 0x1

    #@a
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 605
    .local v0, query:Ljava/lang/String;
    :goto_e
    invoke-virtual {p0, v0}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    #@11
    .line 606
    return-void

    #@12
    .line 603
    .end local v0           #query:Ljava/lang/String;
    :cond_12
    const-string v0, ""

    #@14
    .restart local v0       #query:Ljava/lang/String;
    goto :goto_e
.end method

.method public registerParameter(Ljava/lang/String;Landroid/net/UrlQuerySanitizer$ValueSanitizer;)V
    .registers 4
    .parameter "parameter"
    .parameter "valueSanitizer"

    #@0
    .prologue
    .line 689
    if-nez p2, :cond_7

    #@2
    .line 690
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mSanitizers:Ljava/util/HashMap;

    #@4
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    .line 692
    :cond_7
    iget-object v0, p0, Landroid/net/UrlQuerySanitizer;->mSanitizers:Ljava/util/HashMap;

    #@9
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 693
    return-void
.end method

.method public registerParameters([Ljava/lang/String;Landroid/net/UrlQuerySanitizer$ValueSanitizer;)V
    .registers 7
    .parameter "parameters"
    .parameter "valueSanitizer"

    #@0
    .prologue
    .line 703
    array-length v1, p1

    #@1
    .line 704
    .local v1, length:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v1, :cond_e

    #@4
    .line 705
    iget-object v2, p0, Landroid/net/UrlQuerySanitizer;->mSanitizers:Ljava/util/HashMap;

    #@6
    aget-object v3, p1, v0

    #@8
    invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    .line 704
    add-int/lit8 v0, v0, 0x1

    #@d
    goto :goto_2

    #@e
    .line 707
    :cond_e
    return-void
.end method

.method public setAllowUnregisteredParamaters(Z)V
    .registers 2
    .parameter "allowUnregisteredParamaters"

    #@0
    .prologue
    .line 719
    iput-boolean p1, p0, Landroid/net/UrlQuerySanitizer;->mAllowUnregisteredParamaters:Z

    #@2
    .line 720
    return-void
.end method

.method public setPreferFirstRepeatedParameter(Z)V
    .registers 2
    .parameter "preferFirstRepeatedParameter"

    #@0
    .prologue
    .line 747
    iput-boolean p1, p0, Landroid/net/UrlQuerySanitizer;->mPreferFirstRepeatedParameter:Z

    #@2
    .line 748
    return-void
.end method

.method public setUnregisteredParameterValueSanitizer(Landroid/net/UrlQuerySanitizer$ValueSanitizer;)V
    .registers 2
    .parameter "sanitizer"

    #@0
    .prologue
    .line 431
    iput-object p1, p0, Landroid/net/UrlQuerySanitizer;->mUnregisteredParameterValueSanitizer:Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    #@2
    .line 432
    return-void
.end method

.method public unescape(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "string"

    #@0
    .prologue
    const/16 v10, 0x2b

    #@2
    const/16 v9, 0x25

    #@4
    .line 841
    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v3

    #@8
    .line 842
    .local v3, firstEscape:I
    if-gez v3, :cond_11

    #@a
    .line 843
    invoke-virtual {p1, v10}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v3

    #@e
    .line 844
    if-gez v3, :cond_11

    #@10
    .line 868
    .end local p1
    :goto_10
    return-object p1

    #@11
    .line 849
    .restart local p1
    :cond_11
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@14
    move-result v5

    #@15
    .line 851
    .local v5, length:I
    new-instance v6, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@1a
    .line 852
    .local v6, stringBuilder:Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    #@1b
    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 853
    move v4, v3

    #@23
    .local v4, i:I
    :goto_23
    if-ge v4, v5, :cond_60

    #@25
    .line 854
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@28
    move-result v0

    #@29
    .line 855
    .local v0, c:C
    if-ne v0, v10, :cond_33

    #@2b
    .line 856
    const/16 v0, 0x20

    #@2d
    .line 866
    :cond_2d
    :goto_2d
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@30
    .line 853
    add-int/lit8 v4, v4, 0x1

    #@32
    goto :goto_23

    #@33
    .line 858
    :cond_33
    if-ne v0, v9, :cond_2d

    #@35
    add-int/lit8 v7, v4, 0x2

    #@37
    if-ge v7, v5, :cond_2d

    #@39
    .line 859
    add-int/lit8 v7, v4, 0x1

    #@3b
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@3e
    move-result v1

    #@3f
    .line 860
    .local v1, c1:C
    add-int/lit8 v7, v4, 0x2

    #@41
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@44
    move-result v2

    #@45
    .line 861
    .local v2, c2:C
    invoke-virtual {p0, v1}, Landroid/net/UrlQuerySanitizer;->isHexDigit(C)Z

    #@48
    move-result v7

    #@49
    if-eqz v7, :cond_2d

    #@4b
    invoke-virtual {p0, v2}, Landroid/net/UrlQuerySanitizer;->isHexDigit(C)Z

    #@4e
    move-result v7

    #@4f
    if-eqz v7, :cond_2d

    #@51
    .line 862
    invoke-virtual {p0, v1}, Landroid/net/UrlQuerySanitizer;->decodeHexDigit(C)I

    #@54
    move-result v7

    #@55
    mul-int/lit8 v7, v7, 0x10

    #@57
    invoke-virtual {p0, v2}, Landroid/net/UrlQuerySanitizer;->decodeHexDigit(C)I

    #@5a
    move-result v8

    #@5b
    add-int/2addr v7, v8

    #@5c
    int-to-char v0, v7

    #@5d
    .line 863
    add-int/lit8 v4, v4, 0x2

    #@5f
    goto :goto_2d

    #@60
    .line 868
    .end local v0           #c:C
    .end local v1           #c1:C
    .end local v2           #c2:C
    :cond_60
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object p1

    #@64
    goto :goto_10
.end method
