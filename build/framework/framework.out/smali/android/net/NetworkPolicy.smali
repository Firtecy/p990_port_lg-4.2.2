.class public Landroid/net/NetworkPolicy;
.super Ljava/lang/Object;
.source "NetworkPolicy.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Landroid/net/NetworkPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/NetworkPolicy;",
            ">;"
        }
    .end annotation
.end field

.field public static final CYCLE_NONE:I = -0x1

.field private static final DEFAULT_MTU:J = 0x5dcL

.field public static final LIMIT_DISABLED:J = -0x1L

.field public static final SNOOZE_NEVER:J = -0x1L

.field public static final WARNING_DISABLED:J = -0x1L


# instance fields
.field public cycleDay:I

.field public cycleTimezone:Ljava/lang/String;

.field public inferred:Z

.field public lastLimitSnooze:J

.field public lastWarningSnooze:J

.field public limitBytes:J

.field public metered:Z

.field public final template:Landroid/net/NetworkTemplate;

.field public warningBytes:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 183
    new-instance v0, Landroid/net/NetworkPolicy$1;

    #@2
    invoke-direct {v0}, Landroid/net/NetworkPolicy$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/NetworkPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V
    .registers 15
    .parameter "template"
    .parameter "cycleDay"
    .parameter "cycleTimezone"
    .parameter "warningBytes"
    .parameter "limitBytes"
    .parameter "lastWarningSnooze"
    .parameter "lastLimitSnooze"
    .parameter "metered"
    .parameter "inferred"

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    const-string/jumbo v0, "missing NetworkTemplate"

    #@6
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/NetworkTemplate;

    #@c
    iput-object v0, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@e
    .line 61
    iput p2, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@10
    .line 62
    const-string/jumbo v0, "missing cycleTimezone"

    #@13
    invoke-static {p3, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Ljava/lang/String;

    #@19
    iput-object v0, p0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@1b
    .line 63
    iput-wide p4, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@1d
    .line 64
    iput-wide p6, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@1f
    .line 65
    iput-wide p8, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@21
    .line 66
    iput-wide p10, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@23
    .line 67
    iput-boolean p12, p0, Landroid/net/NetworkPolicy;->metered:Z

    #@25
    .line 68
    iput-boolean p13, p0, Landroid/net/NetworkPolicy;->inferred:Z

    #@27
    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/net/NetworkTemplate;ILjava/lang/String;JJZ)V
    .registers 23
    .parameter "template"
    .parameter "cycleDay"
    .parameter "cycleTimezone"
    .parameter "warningBytes"
    .parameter "limitBytes"
    .parameter "metered"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 53
    const-wide/16 v8, -0x1

    #@2
    const-wide/16 v10, -0x1

    #@4
    const/4 v13, 0x0

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move/from16 v2, p2

    #@9
    move-object/from16 v3, p3

    #@b
    move-wide/from16 v4, p4

    #@d
    move-wide/from16 v6, p6

    #@f
    move/from16 v12, p8

    #@11
    invoke-direct/range {v0 .. v13}, Landroid/net/NetworkPolicy;-><init>(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V

    #@14
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 7
    .parameter "in"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 72
    const/4 v0, 0x0

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/NetworkTemplate;

    #@c
    iput-object v0, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@e
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@14
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@1a
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1d
    move-result-wide v3

    #@1e
    iput-wide v3, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@20
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@23
    move-result-wide v3

    #@24
    iput-wide v3, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@26
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@29
    move-result-wide v3

    #@2a
    iput-wide v3, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@2c
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v3

    #@30
    iput-wide v3, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@32
    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_44

    #@38
    move v0, v1

    #@39
    :goto_39
    iput-boolean v0, p0, Landroid/net/NetworkPolicy;->metered:Z

    #@3b
    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_46

    #@41
    :goto_41
    iput-boolean v1, p0, Landroid/net/NetworkPolicy;->inferred:Z

    #@43
    .line 81
    return-void

    #@44
    :cond_44
    move v0, v2

    #@45
    .line 79
    goto :goto_39

    #@46
    :cond_46
    move v1, v2

    #@47
    .line 80
    goto :goto_41
.end method


# virtual methods
.method public clearSnooze()V
    .registers 3

    #@0
    .prologue
    const-wide/16 v0, -0x1

    #@2
    .line 123
    iput-wide v0, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@4
    .line 124
    iput-wide v0, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@6
    .line 125
    return-void
.end method

.method public compareTo(Landroid/net/NetworkPolicy;)I
    .registers 6
    .parameter "another"

    #@0
    .prologue
    const-wide/16 v2, -0x1

    #@2
    .line 136
    if-eqz p1, :cond_a

    #@4
    iget-wide v0, p1, Landroid/net/NetworkPolicy;->limitBytes:J

    #@6
    cmp-long v0, v0, v2

    #@8
    if-nez v0, :cond_c

    #@a
    .line 138
    :cond_a
    const/4 v0, -0x1

    #@b
    .line 144
    :goto_b
    return v0

    #@c
    .line 140
    :cond_c
    iget-wide v0, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@e
    cmp-long v0, v0, v2

    #@10
    if-eqz v0, :cond_1a

    #@12
    iget-wide v0, p1, Landroid/net/NetworkPolicy;->limitBytes:J

    #@14
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@16
    cmp-long v0, v0, v2

    #@18
    if-gez v0, :cond_1c

    #@1a
    .line 142
    :cond_1a
    const/4 v0, 0x1

    #@1b
    goto :goto_b

    #@1c
    .line 144
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_b
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 32
    check-cast p1, Landroid/net/NetworkPolicy;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/net/NetworkPolicy;->compareTo(Landroid/net/NetworkPolicy;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 98
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 155
    instance-of v2, p1, Landroid/net/NetworkPolicy;

    #@3
    if-eqz v2, :cond_4f

    #@5
    move-object v0, p1

    #@6
    .line 156
    check-cast v0, Landroid/net/NetworkPolicy;

    #@8
    .line 157
    .local v0, other:Landroid/net/NetworkPolicy;
    iget v2, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@a
    iget v3, v0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@c
    if-ne v2, v3, :cond_4f

    #@e
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@10
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@12
    cmp-long v2, v2, v4

    #@14
    if-nez v2, :cond_4f

    #@16
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@18
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@1a
    cmp-long v2, v2, v4

    #@1c
    if-nez v2, :cond_4f

    #@1e
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@20
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@22
    cmp-long v2, v2, v4

    #@24
    if-nez v2, :cond_4f

    #@26
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@28
    iget-wide v4, v0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@2a
    cmp-long v2, v2, v4

    #@2c
    if-nez v2, :cond_4f

    #@2e
    iget-boolean v2, p0, Landroid/net/NetworkPolicy;->metered:Z

    #@30
    iget-boolean v3, v0, Landroid/net/NetworkPolicy;->metered:Z

    #@32
    if-ne v2, v3, :cond_4f

    #@34
    iget-boolean v2, p0, Landroid/net/NetworkPolicy;->inferred:Z

    #@36
    iget-boolean v3, v0, Landroid/net/NetworkPolicy;->inferred:Z

    #@38
    if-ne v2, v3, :cond_4f

    #@3a
    iget-object v2, p0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@3c
    iget-object v3, v0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@3e
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_4f

    #@44
    iget-object v2, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@46
    iget-object v3, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@48
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@4b
    move-result v2

    #@4c
    if-eqz v2, :cond_4f

    #@4e
    const/4 v1, 0x1

    #@4f
    .line 165
    .end local v0           #other:Landroid/net/NetworkPolicy;
    :cond_4f
    return v1
.end method

.method public hasCycle()Z
    .registers 3

    #@0
    .prologue
    .line 131
    iget v0, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@2
    const/4 v1, -0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public hashCode()I
    .registers 5

    #@0
    .prologue
    .line 149
    const/16 v0, 0x9

    #@2
    new-array v0, v0, [Ljava/lang/Object;

    #@4
    const/4 v1, 0x0

    #@5
    iget-object v2, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    iget v2, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v2

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x2

    #@13
    iget-object v2, p0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x3

    #@18
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@1a
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1d
    move-result-object v2

    #@1e
    aput-object v2, v0, v1

    #@20
    const/4 v1, 0x4

    #@21
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@23
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@26
    move-result-object v2

    #@27
    aput-object v2, v0, v1

    #@29
    const/4 v1, 0x5

    #@2a
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@2c
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2f
    move-result-object v2

    #@30
    aput-object v2, v0, v1

    #@32
    const/4 v1, 0x6

    #@33
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@35
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@38
    move-result-object v2

    #@39
    aput-object v2, v0, v1

    #@3b
    const/4 v1, 0x7

    #@3c
    iget-boolean v2, p0, Landroid/net/NetworkPolicy;->metered:Z

    #@3e
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@41
    move-result-object v2

    #@42
    aput-object v2, v0, v1

    #@44
    const/16 v1, 0x8

    #@46
    iget-boolean v2, p0, Landroid/net/NetworkPolicy;->inferred:Z

    #@48
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4b
    move-result-object v2

    #@4c
    aput-object v2, v0, v1

    #@4e
    invoke-static {v0}, Lcom/android/internal/util/Objects;->hashCode([Ljava/lang/Object;)I

    #@51
    move-result v0

    #@52
    return v0
.end method

.method public isOverLimit(J)Z
    .registers 7
    .parameter "totalBytes"

    #@0
    .prologue
    .line 115
    const-wide/16 v0, 0xbb8

    #@2
    add-long/2addr p1, v0

    #@3
    .line 116
    iget-wide v0, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@5
    const-wide/16 v2, -0x1

    #@7
    cmp-long v0, v0, v2

    #@9
    if-eqz v0, :cond_13

    #@b
    iget-wide v0, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@d
    cmp-long v0, p1, v0

    #@f
    if-ltz v0, :cond_13

    #@11
    const/4 v0, 0x1

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public isOverWarning(J)Z
    .registers 7
    .parameter "totalBytes"

    #@0
    .prologue
    .line 105
    iget-wide v0, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-eqz v0, :cond_10

    #@8
    iget-wide v0, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@a
    cmp-long v0, p1, v0

    #@c
    if-ltz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "NetworkPolicy"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 171
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "["

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "]:"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 172
    const-string v1, " cycleDay="

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    iget v2, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    .line 173
    const-string v1, ", cycleTimezone="

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget-object v2, p0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    .line 174
    const-string v1, ", warningBytes="

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@36
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@39
    .line 175
    const-string v1, ", limitBytes="

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@41
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@44
    .line 176
    const-string v1, ", lastWarningSnooze="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@4c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4f
    .line 177
    const-string v1, ", lastLimitSnooze="

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget-wide v2, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@57
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5a
    .line 178
    const-string v1, ", metered="

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    iget-boolean v2, p0, Landroid/net/NetworkPolicy;->metered:Z

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@65
    .line 179
    const-string v1, ", inferred="

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    iget-boolean v2, p0, Landroid/net/NetworkPolicy;->inferred:Z

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@70
    .line 180
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 85
    iget-object v0, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    #@4
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@7
    .line 86
    iget v0, p0, Landroid/net/NetworkPolicy;->cycleDay:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 87
    iget-object v0, p0, Landroid/net/NetworkPolicy;->cycleTimezone:Ljava/lang/String;

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 88
    iget-wide v3, p0, Landroid/net/NetworkPolicy;->warningBytes:J

    #@13
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@16
    .line 89
    iget-wide v3, p0, Landroid/net/NetworkPolicy;->limitBytes:J

    #@18
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@1b
    .line 90
    iget-wide v3, p0, Landroid/net/NetworkPolicy;->lastWarningSnooze:J

    #@1d
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@20
    .line 91
    iget-wide v3, p0, Landroid/net/NetworkPolicy;->lastLimitSnooze:J

    #@22
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 92
    iget-boolean v0, p0, Landroid/net/NetworkPolicy;->metered:Z

    #@27
    if-eqz v0, :cond_35

    #@29
    move v0, v1

    #@2a
    :goto_2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 93
    iget-boolean v0, p0, Landroid/net/NetworkPolicy;->inferred:Z

    #@2f
    if-eqz v0, :cond_37

    #@31
    :goto_31
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 94
    return-void

    #@35
    :cond_35
    move v0, v2

    #@36
    .line 92
    goto :goto_2a

    #@37
    :cond_37
    move v1, v2

    #@38
    .line 93
    goto :goto_31
.end method
