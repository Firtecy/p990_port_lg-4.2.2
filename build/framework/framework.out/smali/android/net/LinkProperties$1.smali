.class final Landroid/net/LinkProperties$1;
.super Ljava/lang/Object;
.source "LinkProperties.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/LinkProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/LinkProperties;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 410
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/LinkProperties;
    .registers 10
    .parameter "in"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 412
    new-instance v4, Landroid/net/LinkProperties;

    #@3
    invoke-direct {v4}, Landroid/net/LinkProperties;-><init>()V

    #@6
    .line 413
    .local v4, netProp:Landroid/net/LinkProperties;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 414
    .local v3, iface:Ljava/lang/String;
    if-eqz v3, :cond_f

    #@c
    .line 416
    :try_start_c
    invoke-virtual {v4, v3}, Landroid/net/LinkProperties;->setInterfaceName(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_f} :catch_22

    #@f
    .line 421
    :cond_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    .line 422
    .local v0, addressCount:I
    const/4 v2, 0x0

    #@14
    .local v2, i:I
    :goto_14
    if-ge v2, v0, :cond_25

    #@16
    .line 423
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@19
    move-result-object v5

    #@1a
    check-cast v5, Landroid/net/LinkAddress;

    #@1c
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V

    #@1f
    .line 422
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_14

    #@22
    .line 417
    .end local v0           #addressCount:I
    .end local v2           #i:I
    :catch_22
    move-exception v1

    #@23
    .local v1, e:Ljava/lang/Exception;
    move-object v4, v6

    #@24
    .line 438
    .end local v1           #e:Ljava/lang/Exception;
    .end local v4           #netProp:Landroid/net/LinkProperties;
    :cond_24
    :goto_24
    return-object v4

    #@25
    .line 425
    .restart local v0       #addressCount:I
    .restart local v2       #i:I
    .restart local v4       #netProp:Landroid/net/LinkProperties;
    :cond_25
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v0

    #@29
    .line 426
    const/4 v2, 0x0

    #@2a
    :goto_2a
    if-ge v2, v0, :cond_3a

    #@2c
    .line 428
    :try_start_2c
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@2f
    move-result-object v5

    #@30
    invoke-static {v5}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V
    :try_end_37
    .catch Ljava/net/UnknownHostException; {:try_start_2c .. :try_end_37} :catch_5e

    #@37
    .line 426
    :goto_37
    add-int/lit8 v2, v2, 0x1

    #@39
    goto :goto_2a

    #@3a
    .line 431
    :cond_3a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v0

    #@3e
    .line 432
    const/4 v2, 0x0

    #@3f
    :goto_3f
    if-ge v2, v0, :cond_4d

    #@41
    .line 433
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@44
    move-result-object v5

    #@45
    check-cast v5, Landroid/net/RouteInfo;

    #@47
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V

    #@4a
    .line 432
    add-int/lit8 v2, v2, 0x1

    #@4c
    goto :goto_3f

    #@4d
    .line 435
    :cond_4d
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@50
    move-result v5

    #@51
    const/4 v7, 0x1

    #@52
    if-ne v5, v7, :cond_24

    #@54
    .line 436
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@57
    move-result-object v5

    #@58
    check-cast v5, Landroid/net/ProxyProperties;

    #@5a
    invoke-virtual {v4, v5}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyProperties;)V

    #@5d
    goto :goto_24

    #@5e
    .line 429
    :catch_5e
    move-exception v5

    #@5f
    goto :goto_37
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 410
    invoke-virtual {p0, p1}, Landroid/net/LinkProperties$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/LinkProperties;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/LinkProperties;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 442
    new-array v0, p1, [Landroid/net/LinkProperties;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 410
    invoke-virtual {p0, p1}, Landroid/net/LinkProperties$1;->newArray(I)[Landroid/net/LinkProperties;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
