.class Landroid/net/IThrottleManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IThrottleManager.java"

# interfaces
.implements Landroid/net/IThrottleManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/IThrottleManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 133
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 134
    iput-object p1, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 135
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 138
    iget-object v0, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getByteCount(Ljava/lang/String;III)J
    .registers 12
    .parameter "iface"
    .parameter "dir"
    .parameter "period"
    .parameter "ago"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 146
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 147
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 150
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 151
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 152
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 153
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 154
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 155
    iget-object v4, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v5, 0x1

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 156
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 157
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2e

    #@26
    move-result-wide v2

    #@27
    .line 160
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 161
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 163
    return-wide v2

    #@2e
    .line 160
    .end local v2           #_result:J
    :catchall_2e
    move-exception v4

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 161
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v4
.end method

.method public getCliffLevel(Ljava/lang/String;I)I
    .registers 9
    .parameter "iface"
    .parameter "cliff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 240
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 241
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 244
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 245
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 246
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 247
    iget-object v3, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v4, 0x6

    #@16
    const/4 v5, 0x0

    #@17
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 248
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 249
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_28

    #@20
    move-result v2

    #@21
    .line 252
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 253
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 255
    return v2

    #@28
    .line 252
    .end local v2           #_result:I
    :catchall_28
    move-exception v3

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 253
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v3
.end method

.method public getCliffThreshold(Ljava/lang/String;I)J
    .registers 10
    .parameter "iface"
    .parameter "cliff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 222
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 225
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 226
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 227
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 228
    iget-object v4, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v5, 0x5

    #@16
    const/4 v6, 0x0

    #@17
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 229
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 230
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_28

    #@20
    move-result-wide v2

    #@21
    .line 233
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 236
    return-wide v2

    #@28
    .line 233
    .end local v2           #_result:J
    :catchall_28
    move-exception v4

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v4
.end method

.method public getHelpUri()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 260
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 263
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 264
    iget-object v3, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x7

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 265
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 266
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result-object v2

    #@1b
    .line 269
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 270
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 272
    return-object v2

    #@22
    .line 269
    .end local v2           #_result:Ljava/lang/String;
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 270
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 142
    const-string v0, "android.net.IThrottleManager"

    #@2
    return-object v0
.end method

.method public getPeriodStartTime(Ljava/lang/String;)J
    .registers 9
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 203
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 204
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 207
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 208
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 209
    iget-object v4, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v5, 0x4

    #@13
    const/4 v6, 0x0

    #@14
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 210
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 211
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-wide v2

    #@1e
    .line 214
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 217
    return-wide v2

    #@25
    .line 214
    .end local v2           #_result:J
    :catchall_25
    move-exception v4

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v4
.end method

.method public getResetTime(Ljava/lang/String;)J
    .registers 9
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 186
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 189
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 190
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 191
    iget-object v4, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v5, 0x3

    #@13
    const/4 v6, 0x0

    #@14
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 192
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 193
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result-wide v2

    #@1e
    .line 196
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 199
    return-wide v2

    #@25
    .line 196
    .end local v2           #_result:J
    :catchall_25
    move-exception v4

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 197
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v4
.end method

.method public getThrottle(Ljava/lang/String;)I
    .registers 8
    .parameter "iface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 168
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 171
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.net.IThrottleManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 172
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 173
    iget-object v3, p0, Landroid/net/IThrottleManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x2

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 174
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 175
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 178
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 179
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 181
    return v2

    #@25
    .line 178
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 179
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method
