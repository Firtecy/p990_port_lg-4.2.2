.class public Landroid/net/DhcpInfo;
.super Ljava/lang/Object;
.source "DhcpInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/DhcpInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public dns1:I

.field public dns2:I

.field public domainName:Ljava/lang/String;

.field public gateway:I

.field public ipAddress:I

.field public leaseDuration:I

.field public netmask:I

.field public serverAddress:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 94
    new-instance v0, Landroid/net/DhcpInfo$1;

    #@2
    invoke-direct {v0}, Landroid/net/DhcpInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/DhcpInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/net/DhcpInfo;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 47
    if-eqz p1, :cond_25

    #@5
    .line 48
    iget v0, p1, Landroid/net/DhcpInfo;->ipAddress:I

    #@7
    iput v0, p0, Landroid/net/DhcpInfo;->ipAddress:I

    #@9
    .line 49
    iget v0, p1, Landroid/net/DhcpInfo;->gateway:I

    #@b
    iput v0, p0, Landroid/net/DhcpInfo;->gateway:I

    #@d
    .line 50
    iget v0, p1, Landroid/net/DhcpInfo;->netmask:I

    #@f
    iput v0, p0, Landroid/net/DhcpInfo;->netmask:I

    #@11
    .line 51
    iget v0, p1, Landroid/net/DhcpInfo;->dns1:I

    #@13
    iput v0, p0, Landroid/net/DhcpInfo;->dns1:I

    #@15
    .line 52
    iget v0, p1, Landroid/net/DhcpInfo;->dns2:I

    #@17
    iput v0, p0, Landroid/net/DhcpInfo;->dns2:I

    #@19
    .line 53
    iget v0, p1, Landroid/net/DhcpInfo;->serverAddress:I

    #@1b
    iput v0, p0, Landroid/net/DhcpInfo;->serverAddress:I

    #@1d
    .line 54
    iget v0, p1, Landroid/net/DhcpInfo;->leaseDuration:I

    #@1f
    iput v0, p0, Landroid/net/DhcpInfo;->leaseDuration:I

    #@21
    .line 55
    iget-object v0, p1, Landroid/net/DhcpInfo;->domainName:Ljava/lang/String;

    #@23
    iput-object v0, p0, Landroid/net/DhcpInfo;->domainName:Ljava/lang/String;

    #@25
    .line 57
    :cond_25
    return-void
.end method

.method private static putAddress(Ljava/lang/StringBuffer;I)V
    .registers 3
    .parameter "buf"
    .parameter "addr"

    #@0
    .prologue
    .line 73
    invoke-static {p1}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b
    .line 74
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 78
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 62
    .local v0, str:Ljava/lang/StringBuffer;
    const-string v1, "ipaddr "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a
    iget v1, p0, Landroid/net/DhcpInfo;->ipAddress:I

    #@c
    invoke-static {v0, v1}, Landroid/net/DhcpInfo;->putAddress(Ljava/lang/StringBuffer;I)V

    #@f
    .line 63
    const-string v1, " gateway "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@14
    iget v1, p0, Landroid/net/DhcpInfo;->gateway:I

    #@16
    invoke-static {v0, v1}, Landroid/net/DhcpInfo;->putAddress(Ljava/lang/StringBuffer;I)V

    #@19
    .line 64
    const-string v1, " netmask "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1e
    iget v1, p0, Landroid/net/DhcpInfo;->netmask:I

    #@20
    invoke-static {v0, v1}, Landroid/net/DhcpInfo;->putAddress(Ljava/lang/StringBuffer;I)V

    #@23
    .line 65
    const-string v1, " dns1 "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@28
    iget v1, p0, Landroid/net/DhcpInfo;->dns1:I

    #@2a
    invoke-static {v0, v1}, Landroid/net/DhcpInfo;->putAddress(Ljava/lang/StringBuffer;I)V

    #@2d
    .line 66
    const-string v1, " dns2 "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@32
    iget v1, p0, Landroid/net/DhcpInfo;->dns2:I

    #@34
    invoke-static {v0, v1}, Landroid/net/DhcpInfo;->putAddress(Ljava/lang/StringBuffer;I)V

    #@37
    .line 67
    const-string v1, " DHCP server "

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3c
    iget v1, p0, Landroid/net/DhcpInfo;->serverAddress:I

    #@3e
    invoke-static {v0, v1}, Landroid/net/DhcpInfo;->putAddress(Ljava/lang/StringBuffer;I)V

    #@41
    .line 68
    const-string v1, " lease "

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@46
    move-result-object v1

    #@47
    iget v2, p0, Landroid/net/DhcpInfo;->leaseDuration:I

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, " seconds"

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@52
    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 83
    iget v0, p0, Landroid/net/DhcpInfo;->ipAddress:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 84
    iget v0, p0, Landroid/net/DhcpInfo;->gateway:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 85
    iget v0, p0, Landroid/net/DhcpInfo;->netmask:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 86
    iget v0, p0, Landroid/net/DhcpInfo;->dns1:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 87
    iget v0, p0, Landroid/net/DhcpInfo;->dns2:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 88
    iget v0, p0, Landroid/net/DhcpInfo;->serverAddress:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 89
    iget v0, p0, Landroid/net/DhcpInfo;->leaseDuration:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 90
    iget-object v0, p0, Landroid/net/DhcpInfo;->domainName:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 91
    return-void
.end method
