.class abstract Landroid/net/Uri$AbstractPart;
.super Ljava/lang/Object;
.source "Uri.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "AbstractPart"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/Uri$AbstractPart$Representation;
    }
.end annotation


# instance fields
.field volatile decoded:Ljava/lang/String;

.field volatile encoded:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "encoded"
    .parameter "decoded"

    #@0
    .prologue
    .line 1950
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1951
    iput-object p1, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    #@5
    .line 1952
    iput-object p2, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@7
    .line 1953
    return-void
.end method


# virtual methods
.method final getDecoded()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1959
    iget-object v1, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@2
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    if-eq v1, v2, :cond_e

    #@8
    const/4 v0, 0x1

    #@9
    .line 1960
    .local v0, hasDecoded:Z
    :goto_9
    if-eqz v0, :cond_10

    #@b
    iget-object v1, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@d
    :goto_d
    return-object v1

    #@e
    .line 1959
    .end local v0           #hasDecoded:Z
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_9

    #@10
    .line 1960
    .restart local v0       #hasDecoded:Z
    :cond_10
    iget-object v1, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    #@12
    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@18
    goto :goto_d
.end method

.method abstract getEncoded()Ljava/lang/String;
.end method

.method final writeTo(Landroid/os/Parcel;)V
    .registers 8
    .parameter "parcel"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1965
    iget-object v4, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    #@4
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@7
    move-result-object v5

    #@8
    if-eq v4, v5, :cond_26

    #@a
    move v1, v2

    #@b
    .line 1968
    .local v1, hasEncoded:Z
    :goto_b
    iget-object v4, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@d
    invoke-static {}, Landroid/net/Uri;->access$300()Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    if-eq v4, v5, :cond_28

    #@13
    move v0, v2

    #@14
    .line 1970
    .local v0, hasDecoded:Z
    :goto_14
    if-eqz v1, :cond_2a

    #@16
    if-eqz v0, :cond_2a

    #@18
    .line 1971
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 1972
    iget-object v2, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    #@1d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 1973
    iget-object v2, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@22
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 1983
    :goto_25
    return-void

    #@26
    .end local v0           #hasDecoded:Z
    .end local v1           #hasEncoded:Z
    :cond_26
    move v1, v3

    #@27
    .line 1965
    goto :goto_b

    #@28
    .restart local v1       #hasEncoded:Z
    :cond_28
    move v0, v3

    #@29
    .line 1968
    goto :goto_14

    #@2a
    .line 1974
    .restart local v0       #hasDecoded:Z
    :cond_2a
    if-eqz v1, :cond_35

    #@2c
    .line 1975
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 1976
    iget-object v2, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    #@31
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@34
    goto :goto_25

    #@35
    .line 1977
    :cond_35
    if-eqz v0, :cond_41

    #@37
    .line 1978
    const/4 v2, 0x2

    #@38
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    .line 1979
    iget-object v2, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    #@3d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@40
    goto :goto_25

    #@41
    .line 1981
    :cond_41
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@43
    const-string v3, "Neither encoded nor decoded"

    #@45
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@48
    throw v2
.end method
