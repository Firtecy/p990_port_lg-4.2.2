.class public Landroid/net/NetworkConfig;
.super Ljava/lang/Object;
.source "NetworkConfig.java"


# instance fields
.field public dependencyMet:Z

.field public name:Ljava/lang/String;

.field public priority:I

.field public radio:I

.field public restoreTime:I

.field public type:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "init"

    #@0
    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 65
    const-string v1, ","

    #@5
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 66
    .local v0, fragments:[Ljava/lang/String;
    const/4 v1, 0x0

    #@a
    aget-object v1, v0, v1

    #@c
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Landroid/net/NetworkConfig;->name:Ljava/lang/String;

    #@16
    .line 67
    const/4 v1, 0x1

    #@17
    aget-object v1, v0, v1

    #@19
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1c
    move-result v1

    #@1d
    iput v1, p0, Landroid/net/NetworkConfig;->type:I

    #@1f
    .line 68
    const/4 v1, 0x2

    #@20
    aget-object v1, v0, v1

    #@22
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@25
    move-result v1

    #@26
    iput v1, p0, Landroid/net/NetworkConfig;->radio:I

    #@28
    .line 69
    const/4 v1, 0x3

    #@29
    aget-object v1, v0, v1

    #@2b
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2e
    move-result v1

    #@2f
    iput v1, p0, Landroid/net/NetworkConfig;->priority:I

    #@31
    .line 70
    const/4 v1, 0x4

    #@32
    aget-object v1, v0, v1

    #@34
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@37
    move-result v1

    #@38
    iput v1, p0, Landroid/net/NetworkConfig;->restoreTime:I

    #@3a
    .line 71
    const/4 v1, 0x5

    #@3b
    aget-object v1, v0, v1

    #@3d
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@40
    move-result v1

    #@41
    iput-boolean v1, p0, Landroid/net/NetworkConfig;->dependencyMet:Z

    #@43
    .line 72
    return-void
.end method


# virtual methods
.method public isDefault()Z
    .registers 3

    #@0
    .prologue
    .line 78
    iget v0, p0, Landroid/net/NetworkConfig;->type:I

    #@2
    iget v1, p0, Landroid/net/NetworkConfig;->radio:I

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method
