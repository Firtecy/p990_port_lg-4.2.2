.class public Landroid/net/LinkCapabilities;
.super Ljava/lang/Object;
.source "LinkCapabilities.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/LinkCapabilities$Flow;,
        Landroid/net/LinkCapabilities$FlowState;,
        Landroid/net/LinkCapabilities$Role;,
        Landroid/net/LinkCapabilities$Key;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/LinkCapabilities;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "LinkCapabilities"


# instance fields
.field private mCapabilities:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mFlows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/LinkCapabilities$Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 654
    new-instance v0, Landroid/net/LinkCapabilities$1;

    #@2
    invoke-direct {v0}, Landroid/net/LinkCapabilities$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/LinkCapabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 487
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 481
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@6
    .line 488
    new-instance v0, Ljava/util/HashMap;

    #@8
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@b
    iput-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@d
    .line 490
    new-instance v0, Ljava/util/ArrayList;

    #@f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v0, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@14
    .line 492
    return-void
.end method

.method public constructor <init>(Landroid/net/LinkCapabilities;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 499
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 481
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@6
    .line 500
    if-eqz p1, :cond_12

    #@8
    .line 501
    new-instance v0, Ljava/util/HashMap;

    #@a
    iget-object v1, p1, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@c
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@f
    iput-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@11
    .line 505
    :goto_11
    return-void

    #@12
    .line 503
    :cond_12
    new-instance v0, Ljava/util/HashMap;

    #@14
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@19
    goto :goto_11
.end method

.method static synthetic access$000(Landroid/net/LinkCapabilities;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public static createNeedsMap(Ljava/lang/String;)Landroid/net/LinkCapabilities;
    .registers 2
    .parameter "applicationRole"

    #@0
    .prologue
    .line 514
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    #@5
    return-object v0
.end method

.method protected static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 676
    const-string v0, "LinkCapabilities"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 677
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 521
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 522
    return-void
.end method

.method public containsKey(I)Z
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public containsValue(Ljava/lang/String;)Z
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 579
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 612
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 590
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public get(I)Ljava/lang/String;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 547
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    return-object v0
.end method

.method public getAllFlowInfo()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 715
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 716
    .local v3, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    #@6
    .line 717
    .local v0, firstTime:Z
    iget-object v4, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v2

    #@c
    .local v2, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_35

    #@12
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/net/LinkCapabilities$Flow;

    #@18
    .line 718
    .local v1, flow:Landroid/net/LinkCapabilities$Flow;
    if-eqz v0, :cond_2f

    #@1a
    .line 719
    const/4 v0, 0x0

    #@1b
    .line 723
    :goto_1b
    const-string/jumbo v4, "{"

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 724
    invoke-virtual {v1}, Landroid/net/LinkCapabilities$Flow;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 725
    const-string/jumbo v4, "}"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    goto :goto_c

    #@2f
    .line 721
    :cond_2f
    const-string v4, ","

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    goto :goto_1b

    #@35
    .line 728
    .end local v1           #flow:Landroid/net/LinkCapabilities$Flow;
    :cond_35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    return-object v4
.end method

.method public getFlow(IZ)Landroid/net/LinkCapabilities$Flow;
    .registers 7
    .parameter "flowID"
    .parameter "create"

    #@0
    .prologue
    .line 682
    iget-object v3, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_19

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/net/LinkCapabilities$Flow;

    #@12
    .line 684
    .local v0, flow:Landroid/net/LinkCapabilities$Flow;
    invoke-virtual {v0}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@15
    move-result v3

    #@16
    if-ne v3, p1, :cond_6

    #@18
    .line 695
    .end local v0           #flow:Landroid/net/LinkCapabilities$Flow;
    :goto_18
    return-object v0

    #@19
    .line 687
    :cond_19
    const/4 v2, 0x0

    #@1a
    .line 689
    .local v2, newFlow:Landroid/net/LinkCapabilities$Flow;
    if-eqz p2, :cond_26

    #@1c
    .line 691
    new-instance v2, Landroid/net/LinkCapabilities$Flow;

    #@1e
    .end local v2           #newFlow:Landroid/net/LinkCapabilities$Flow;
    invoke-direct {v2, p0, p1}, Landroid/net/LinkCapabilities$Flow;-><init>(Landroid/net/LinkCapabilities;I)V

    #@21
    .line 692
    .restart local v2       #newFlow:Landroid/net/LinkCapabilities$Flow;
    iget-object v3, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    :cond_26
    move-object v0, v2

    #@27
    .line 695
    goto :goto_18
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 528
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 597
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public put(ILjava/lang/String;)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 557
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 558
    return-void
.end method

.method public removeAllFlow()V
    .registers 2

    #@0
    .prologue
    .line 710
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 711
    return-void
.end method

.method public removeFlow(I)Z
    .registers 5
    .parameter "flowID"

    #@0
    .prologue
    .line 700
    iget-object v2, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1f

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/net/LinkCapabilities$Flow;

    #@12
    .line 702
    .local v0, flow:Landroid/net/LinkCapabilities$Flow;
    invoke-virtual {v0}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@15
    move-result v2

    #@16
    if-ne v2, p1, :cond_6

    #@18
    iget-object v2, p0, Landroid/net/LinkCapabilities;->mFlows:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    .line 705
    .end local v0           #flow:Landroid/net/LinkCapabilities$Flow;
    :goto_1e
    return v2

    #@1f
    :cond_1f
    const/4 v2, 0x0

    #@20
    goto :goto_1e
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 537
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 620
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 621
    .local v3, sb:Ljava/lang/StringBuilder;
    const-string/jumbo v4, "{"

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 622
    const/4 v1, 0x1

    #@c
    .line 623
    .local v1, firstTime:Z
    iget-object v4, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@e
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@11
    move-result-object v4

    #@12
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    .local v2, i$:Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_4c

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/util/Map$Entry;

    #@22
    .line 624
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-eqz v1, :cond_46

    #@24
    .line 625
    const/4 v1, 0x0

    #@25
    .line 629
    :goto_25
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    .line 630
    const-string v4, ":\""

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    .line 631
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@34
    move-result-object v4

    #@35
    check-cast v4, Ljava/lang/String;

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    .line 632
    const-string v4, "\""

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    .line 633
    iget-object v4, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@41
    invoke-virtual {v4}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    .line 635
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :goto_45
    return-object v4

    #@46
    .line 627
    .restart local v0       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_46
    const-string v4, ","

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    goto :goto_25

    #@4c
    .line 635
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    goto :goto_45
.end method

.method public values()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 604
    iget-object v0, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 643
    iget-object v2, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@5
    move-result v2

    #@6
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 644
    iget-object v2, p0, Landroid/net/LinkCapabilities;->mCapabilities:Ljava/util/HashMap;

    #@b
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v1

    #@13
    .local v1, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_36

    #@19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Ljava/util/Map$Entry;

    #@1f
    .line 645
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Ljava/lang/Integer;

    #@25
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@28
    move-result v2

    #@29
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 646
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2f
    move-result-object v2

    #@30
    check-cast v2, Ljava/lang/String;

    #@32
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@35
    goto :goto_13

    #@36
    .line 648
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_36
    return-void
.end method
