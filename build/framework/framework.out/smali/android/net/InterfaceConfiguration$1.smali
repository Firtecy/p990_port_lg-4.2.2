.class final Landroid/net/InterfaceConfiguration$1;
.super Ljava/lang/Object;
.source "InterfaceConfiguration.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/InterfaceConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/net/InterfaceConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 140
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/net/InterfaceConfiguration;
    .registers 7
    .parameter "in"

    #@0
    .prologue
    .line 142
    new-instance v1, Landroid/net/InterfaceConfiguration;

    #@2
    invoke-direct {v1}, Landroid/net/InterfaceConfiguration;-><init>()V

    #@5
    .line 143
    .local v1, info:Landroid/net/InterfaceConfiguration;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    invoke-static {v1, v3}, Landroid/net/InterfaceConfiguration;->access$002(Landroid/net/InterfaceConfiguration;Ljava/lang/String;)Ljava/lang/String;

    #@c
    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    #@f
    move-result v3

    #@10
    const/4 v4, 0x1

    #@11
    if-ne v3, v4, :cond_1d

    #@13
    .line 145
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Landroid/net/LinkAddress;

    #@1a
    invoke-static {v1, v3}, Landroid/net/InterfaceConfiguration;->access$102(Landroid/net/InterfaceConfiguration;Landroid/net/LinkAddress;)Landroid/net/LinkAddress;

    #@1d
    .line 147
    :cond_1d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v2

    #@21
    .line 148
    .local v2, size:I
    const/4 v0, 0x0

    #@22
    .local v0, i:I
    :goto_22
    if-ge v0, v2, :cond_32

    #@24
    .line 149
    invoke-static {v1}, Landroid/net/InterfaceConfiguration;->access$200(Landroid/net/InterfaceConfiguration;)Ljava/util/HashSet;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2f
    .line 148
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_22

    #@32
    .line 151
    :cond_32
    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 140
    invoke-virtual {p0, p1}, Landroid/net/InterfaceConfiguration$1;->createFromParcel(Landroid/os/Parcel;)Landroid/net/InterfaceConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/net/InterfaceConfiguration;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 155
    new-array v0, p1, [Landroid/net/InterfaceConfiguration;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 140
    invoke-virtual {p0, p1}, Landroid/net/InterfaceConfiguration$1;->newArray(I)[Landroid/net/InterfaceConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
