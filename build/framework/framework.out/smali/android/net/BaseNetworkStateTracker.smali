.class public abstract Landroid/net/BaseNetworkStateTracker;
.super Ljava/lang/Object;
.source "BaseNetworkStateTracker.java"

# interfaces
.implements Landroid/net/NetworkStateTracker;


# static fields
.field public static final PROP_TCP_BUFFER_UNKNOWN:Ljava/lang/String; = "net.tcp.buffersize.unknown"

.field public static final PROP_TCP_BUFFER_WIFI:Ljava/lang/String; = "net.tcp.buffersize.wifi"


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected mLinkCapabilities:Landroid/net/LinkCapabilities;

.field protected mLinkProperties:Landroid/net/LinkProperties;

.field protected mNetworkInfo:Landroid/net/NetworkInfo;

.field private mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mTarget:Landroid/os/Handler;

.field private mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(I)V
    .registers 6
    .parameter "networkType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@9
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@b
    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@d
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@10
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@12
    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@14
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@17
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@19
    .line 53
    new-instance v0, Landroid/net/NetworkInfo;

    #@1b
    const/4 v1, -0x1

    #@1c
    invoke-static {p1}, Landroid/net/ConnectivityManager;->getNetworkTypeName(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    const/4 v3, 0x0

    #@21
    invoke-direct {v0, p1, v1, v2, v3}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@24
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@26
    .line 55
    new-instance v0, Landroid/net/LinkProperties;

    #@28
    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    #@2b
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@2d
    .line 56
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2f
    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    #@32
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@34
    .line 57
    return-void
.end method


# virtual methods
.method public captivePortalCheckComplete()V
    .registers 1

    #@0
    .prologue
    .line 101
    return-void
.end method

.method public defaultRouteSet(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 142
    return-void
.end method

.method protected final dispatchConfigurationChanged()V
    .registers 4

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTarget:Landroid/os/Handler;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {p0}, Landroid/net/BaseNetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@e
    .line 72
    return-void
.end method

.method protected final dispatchStateChanged()V
    .registers 4

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTarget:Landroid/os/Handler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p0}, Landroid/net/BaseNetworkStateTracker;->getNetworkInfo()Landroid/net/NetworkInfo;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@e
    .line 67
    return-void
.end method

.method public final getLinkCapabilities()Landroid/net/LinkCapabilities;
    .registers 3

    #@0
    .prologue
    .line 95
    new-instance v0, Landroid/net/LinkCapabilities;

    #@2
    iget-object v1, p0, Landroid/net/BaseNetworkStateTracker;->mLinkCapabilities:Landroid/net/LinkCapabilities;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkCapabilities;-><init>(Landroid/net/LinkCapabilities;)V

    #@7
    return-object v0
.end method

.method public final getLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 90
    new-instance v0, Landroid/net/LinkProperties;

    #@2
    iget-object v1, p0, Landroid/net/BaseNetworkStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    #@4
    invoke-direct {v0, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    #@7
    return-object v0
.end method

.method public final getNetworkInfo()Landroid/net/NetworkInfo;
    .registers 3

    #@0
    .prologue
    .line 85
    new-instance v0, Landroid/net/NetworkInfo;

    #@2
    iget-object v1, p0, Landroid/net/BaseNetworkStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@4
    invoke-direct {v0, v1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@7
    return-object v0
.end method

.method protected getTargetHandler()Landroid/os/Handler;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTarget:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDefaultRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPrivateDnsRouteSet()Z
    .registers 2

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isTeardownRequested()Z
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public privateDnsRouteSet(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 132
    return-void
.end method

.method public setDependencyMet(Z)V
    .registers 2
    .parameter "met"

    #@0
    .prologue
    .line 157
    return-void
.end method

.method public setPolicyDataEnable(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 122
    return-void
.end method

.method public setRadio(Z)Z
    .registers 3
    .parameter "turnOn"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setTeardownRequested(Z)V
    .registers 3
    .parameter "isRequested"

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@5
    .line 152
    return-void
.end method

.method public setUserDataEnable(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 117
    return-void
.end method

.method public final startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 76
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/content/Context;

    #@6
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mContext:Landroid/content/Context;

    #@8
    .line 77
    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/os/Handler;

    #@e
    iput-object v0, p0, Landroid/net/BaseNetworkStateTracker;->mTarget:Landroid/os/Handler;

    #@10
    .line 78
    invoke-virtual {p0}, Landroid/net/BaseNetworkStateTracker;->startMonitoringInternal()V

    #@13
    .line 79
    return-void
.end method

.method protected abstract startMonitoringInternal()V
.end method
