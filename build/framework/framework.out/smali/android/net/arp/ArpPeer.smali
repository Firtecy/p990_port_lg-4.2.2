.class public Landroid/net/arp/ArpPeer;
.super Ljava/lang/Object;
.source "ArpPeer.java"


# static fields
.field private static final ARP_LENGTH:I = 0x1c

.field private static final DBG:Z = false

.field private static final ETHERNET_TYPE:I = 0x1

.field private static final IPV4_LENGTH:I = 0x4

.field private static final MAC_ADDR_LENGTH:I = 0x6

.field private static final MAX_LENGTH:I = 0x5dc

.field private static final TAG:Ljava/lang/String; = "ArpPeer"


# instance fields
.field private final L2_BROADCAST:[B

.field private mInterfaceName:Ljava/lang/String;

.field private final mMyAddr:Ljava/net/InetAddress;

.field private final mMyMac:[B

.field private final mPeer:Ljava/net/InetAddress;

.field private final mSocket:Llibcore/net/RawSocket;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/net/InetAddress;Ljava/lang/String;Ljava/net/InetAddress;)V
    .registers 10
    .parameter "interfaceName"
    .parameter "myAddr"
    .parameter "mac"
    .parameter "peer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x6

    #@1
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 46
    new-array v1, v4, [B

    #@6
    iput-object v1, p0, Landroid/net/arp/ArpPeer;->mMyMac:[B

    #@8
    .line 58
    iput-object p1, p0, Landroid/net/arp/ArpPeer;->mInterfaceName:Ljava/lang/String;

    #@a
    .line 59
    iput-object p2, p0, Landroid/net/arp/ArpPeer;->mMyAddr:Ljava/net/InetAddress;

    #@c
    .line 61
    if-eqz p3, :cond_29

    #@e
    .line 62
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    if-ge v0, v4, :cond_29

    #@11
    .line 63
    iget-object v1, p0, Landroid/net/arp/ArpPeer;->mMyMac:[B

    #@13
    mul-int/lit8 v2, v0, 0x3

    #@15
    mul-int/lit8 v3, v0, 0x3

    #@17
    add-int/lit8 v3, v3, 0x2

    #@19
    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    const/16 v3, 0x10

    #@1f
    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@22
    move-result v2

    #@23
    int-to-byte v2, v2

    #@24
    aput-byte v2, v1, v0

    #@26
    .line 62
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_f

    #@29
    .line 68
    .end local v0           #i:I
    :cond_29
    instance-of v1, p2, Ljava/net/Inet6Address;

    #@2b
    if-nez v1, :cond_31

    #@2d
    instance-of v1, p4, Ljava/net/Inet6Address;

    #@2f
    if-eqz v1, :cond_39

    #@31
    .line 69
    :cond_31
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@33
    const-string v2, "IPv6 unsupported"

    #@35
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@38
    throw v1

    #@39
    .line 72
    :cond_39
    iput-object p4, p0, Landroid/net/arp/ArpPeer;->mPeer:Ljava/net/InetAddress;

    #@3b
    .line 73
    new-array v1, v4, [B

    #@3d
    iput-object v1, p0, Landroid/net/arp/ArpPeer;->L2_BROADCAST:[B

    #@3f
    .line 74
    iget-object v1, p0, Landroid/net/arp/ArpPeer;->L2_BROADCAST:[B

    #@41
    const/4 v2, -0x1

    #@42
    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([BB)V

    #@45
    .line 76
    new-instance v1, Llibcore/net/RawSocket;

    #@47
    iget-object v2, p0, Landroid/net/arp/ArpPeer;->mInterfaceName:Ljava/lang/String;

    #@49
    const/16 v3, 0x806

    #@4b
    invoke-direct {v1, v2, v3}, Llibcore/net/RawSocket;-><init>(Ljava/lang/String;S)V

    #@4e
    iput-object v1, p0, Landroid/net/arp/ArpPeer;->mSocket:Llibcore/net/RawSocket;

    #@50
    .line 77
    return-void
.end method

.method public static doArp(Ljava/lang/String;Landroid/net/LinkProperties;III)Z
    .registers 20
    .parameter "myMacAddress"
    .parameter "linkProperties"
    .parameter "timeoutMillis"
    .parameter "numArpPings"
    .parameter "minArpResponses"

    #@0
    .prologue
    .line 135
    invoke-virtual/range {p1 .. p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@3
    move-result-object v5

    #@4
    .line 136
    .local v5, interfaceName:Ljava/lang/String;
    const/4 v4, 0x0

    #@5
    .line 137
    .local v4, inetAddress:Ljava/net/InetAddress;
    const/4 v1, 0x0

    #@6
    .line 140
    .local v1, gateway:Ljava/net/InetAddress;
    invoke-virtual/range {p1 .. p1}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    #@9
    move-result-object v12

    #@a
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v3

    #@e
    .local v3, i$:Ljava/util/Iterator;
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v12

    #@12
    if-eqz v12, :cond_1e

    #@14
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v6

    #@18
    check-cast v6, Landroid/net/LinkAddress;

    #@1a
    .line 141
    .local v6, la:Landroid/net/LinkAddress;
    invoke-virtual {v6}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@1d
    move-result-object v4

    #@1e
    .line 145
    .end local v6           #la:Landroid/net/LinkAddress;
    :cond_1e
    invoke-virtual/range {p1 .. p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@21
    move-result-object v12

    #@22
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v3

    #@26
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v12

    #@2a
    if-eqz v12, :cond_36

    #@2c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v9

    #@30
    check-cast v9, Landroid/net/RouteInfo;

    #@32
    .line 146
    .local v9, route:Landroid/net/RouteInfo;
    invoke-virtual {v9}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@35
    move-result-object v1

    #@36
    .line 151
    .end local v9           #route:Landroid/net/RouteInfo;
    :cond_36
    :try_start_36
    new-instance v7, Landroid/net/arp/ArpPeer;

    #@38
    invoke-direct {v7, v5, v4, p0, v1}, Landroid/net/arp/ArpPeer;-><init>(Ljava/lang/String;Ljava/net/InetAddress;Ljava/lang/String;Ljava/net/InetAddress;)V

    #@3b
    .line 152
    .local v7, peer:Landroid/net/arp/ArpPeer;
    const/4 v8, 0x0

    #@3c
    .line 153
    .local v8, responses:I
    const/4 v2, 0x0

    #@3d
    .local v2, i:I
    :goto_3d
    move/from16 v0, p3

    #@3f
    if-ge v2, v0, :cond_4e

    #@41
    .line 154
    move/from16 v0, p2

    #@43
    invoke-virtual {v7, v0}, Landroid/net/arp/ArpPeer;->doArp(I)[B

    #@46
    move-result-object v12

    #@47
    if-eqz v12, :cond_4b

    #@49
    add-int/lit8 v8, v8, 0x1

    #@4b
    .line 153
    :cond_4b
    add-int/lit8 v2, v2, 0x1

    #@4d
    goto :goto_3d

    #@4e
    .line 157
    :cond_4e
    move/from16 v0, p4

    #@50
    if-lt v8, v0, :cond_57

    #@52
    const/4 v11, 0x1

    #@53
    .line 158
    .local v11, success:Z
    :goto_53
    invoke-virtual {v7}, Landroid/net/arp/ArpPeer;->close()V
    :try_end_56
    .catch Ljava/net/SocketException; {:try_start_36 .. :try_end_56} :catch_59

    #@56
    .line 165
    .end local v2           #i:I
    .end local v7           #peer:Landroid/net/arp/ArpPeer;
    .end local v8           #responses:I
    :goto_56
    return v11

    #@57
    .line 157
    .end local v11           #success:Z
    .restart local v2       #i:I
    .restart local v7       #peer:Landroid/net/arp/ArpPeer;
    .restart local v8       #responses:I
    :cond_57
    const/4 v11, 0x0

    #@58
    goto :goto_53

    #@59
    .line 159
    .end local v2           #i:I
    .end local v7           #peer:Landroid/net/arp/ArpPeer;
    .end local v8           #responses:I
    :catch_59
    move-exception v10

    #@5a
    .line 162
    .local v10, se:Ljava/net/SocketException;
    const-string v12, "ArpPeer"

    #@5c
    new-instance v13, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v14, "ARP test initiation failure: "

    #@63
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v13

    #@67
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v13

    #@6b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v13

    #@6f
    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 163
    const/4 v11, 0x1

    #@73
    .restart local v11       #success:Z
    goto :goto_56
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 170
    :try_start_0
    iget-object v0, p0, Landroid/net/arp/ArpPeer;->mSocket:Llibcore/net/RawSocket;

    #@2
    invoke-virtual {v0}, Llibcore/net/RawSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 173
    :goto_5
    return-void

    #@6
    .line 171
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public doArp(I)[B
    .registers 16
    .parameter "timeoutMillis"

    #@0
    .prologue
    .line 84
    const/16 v0, 0x5dc

    #@2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v6

    #@6
    .line 85
    .local v6, buf:Ljava/nio/ByteBuffer;
    iget-object v0, p0, Landroid/net/arp/ArpPeer;->mPeer:Ljava/net/InetAddress;

    #@8
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@b
    move-result-object v7

    #@c
    .line 86
    .local v7, desiredIp:[B
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@f
    move-result-wide v2

    #@10
    int-to-long v4, p1

    #@11
    add-long v12, v2, v4

    #@13
    .line 90
    .local v12, timeout:J
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@16
    .line 91
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@18
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@1b
    .line 93
    const/4 v0, 0x1

    #@1c
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@1f
    .line 94
    const/16 v0, 0x800

    #@21
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@24
    .line 95
    const/4 v0, 0x6

    #@25
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@28
    .line 96
    const/4 v0, 0x4

    #@29
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@2c
    .line 97
    const/4 v0, 0x1

    #@2d
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@30
    .line 98
    iget-object v0, p0, Landroid/net/arp/ArpPeer;->mMyMac:[B

    #@32
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@35
    .line 99
    iget-object v0, p0, Landroid/net/arp/ArpPeer;->mMyAddr:Ljava/net/InetAddress;

    #@37
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@3e
    .line 100
    const/4 v0, 0x6

    #@3f
    new-array v0, v0, [B

    #@41
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@44
    .line 101
    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@47
    .line 102
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@4a
    .line 103
    iget-object v0, p0, Landroid/net/arp/ArpPeer;->mSocket:Llibcore/net/RawSocket;

    #@4c
    iget-object v2, p0, Landroid/net/arp/ArpPeer;->L2_BROADCAST:[B

    #@4e
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    #@51
    move-result-object v3

    #@52
    const/4 v4, 0x0

    #@53
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    #@56
    move-result v5

    #@57
    invoke-virtual {v0, v2, v3, v4, v5}, Llibcore/net/RawSocket;->write([B[BII)I

    #@5a
    .line 105
    const/16 v0, 0x5dc

    #@5c
    new-array v1, v0, [B

    #@5e
    .line 107
    .local v1, recvBuf:[B
    :cond_5e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@61
    move-result-wide v2

    #@62
    cmp-long v0, v2, v12

    #@64
    if-gez v0, :cond_d7

    #@66
    .line 108
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@69
    move-result-wide v2

    #@6a
    sub-long v8, v12, v2

    #@6c
    .line 109
    .local v8, duration:J
    iget-object v0, p0, Landroid/net/arp/ArpPeer;->mSocket:Llibcore/net/RawSocket;

    #@6e
    const/4 v2, 0x0

    #@6f
    array-length v3, v1

    #@70
    const/4 v4, -0x1

    #@71
    long-to-int v5, v8

    #@72
    invoke-virtual/range {v0 .. v5}, Llibcore/net/RawSocket;->read([BIIII)I

    #@75
    move-result v10

    #@76
    .line 113
    .local v10, readLen:I
    const/16 v0, 0x1c

    #@78
    if-lt v10, v0, :cond_5e

    #@7a
    const/4 v0, 0x0

    #@7b
    aget-byte v0, v1, v0

    #@7d
    if-nez v0, :cond_5e

    #@7f
    const/4 v0, 0x1

    #@80
    aget-byte v0, v1, v0

    #@82
    const/4 v2, 0x1

    #@83
    if-ne v0, v2, :cond_5e

    #@85
    const/4 v0, 0x2

    #@86
    aget-byte v0, v1, v0

    #@88
    const/16 v2, 0x8

    #@8a
    if-ne v0, v2, :cond_5e

    #@8c
    const/4 v0, 0x3

    #@8d
    aget-byte v0, v1, v0

    #@8f
    if-nez v0, :cond_5e

    #@91
    const/4 v0, 0x4

    #@92
    aget-byte v0, v1, v0

    #@94
    const/4 v2, 0x6

    #@95
    if-ne v0, v2, :cond_5e

    #@97
    const/4 v0, 0x5

    #@98
    aget-byte v0, v1, v0

    #@9a
    const/4 v2, 0x4

    #@9b
    if-ne v0, v2, :cond_5e

    #@9d
    const/4 v0, 0x6

    #@9e
    aget-byte v0, v1, v0

    #@a0
    if-nez v0, :cond_5e

    #@a2
    const/4 v0, 0x7

    #@a3
    aget-byte v0, v1, v0

    #@a5
    const/4 v2, 0x2

    #@a6
    if-ne v0, v2, :cond_5e

    #@a8
    const/16 v0, 0xe

    #@aa
    aget-byte v0, v1, v0

    #@ac
    const/4 v2, 0x0

    #@ad
    aget-byte v2, v7, v2

    #@af
    if-ne v0, v2, :cond_5e

    #@b1
    const/16 v0, 0xf

    #@b3
    aget-byte v0, v1, v0

    #@b5
    const/4 v2, 0x1

    #@b6
    aget-byte v2, v7, v2

    #@b8
    if-ne v0, v2, :cond_5e

    #@ba
    const/16 v0, 0x10

    #@bc
    aget-byte v0, v1, v0

    #@be
    const/4 v2, 0x2

    #@bf
    aget-byte v2, v7, v2

    #@c1
    if-ne v0, v2, :cond_5e

    #@c3
    const/16 v0, 0x11

    #@c5
    aget-byte v0, v1, v0

    #@c7
    const/4 v2, 0x3

    #@c8
    aget-byte v2, v7, v2

    #@ca
    if-ne v0, v2, :cond_5e

    #@cc
    .line 124
    const/4 v0, 0x6

    #@cd
    new-array v11, v0, [B

    #@cf
    .line 125
    .local v11, result:[B
    const/16 v0, 0x8

    #@d1
    const/4 v2, 0x0

    #@d2
    const/4 v3, 0x6

    #@d3
    invoke-static {v1, v0, v11, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d6
    .line 130
    .end local v8           #duration:J
    .end local v10           #readLen:I
    .end local v11           #result:[B
    :goto_d6
    return-object v11

    #@d7
    :cond_d7
    const/4 v11, 0x0

    #@d8
    goto :goto_d6
.end method
